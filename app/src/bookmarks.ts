import type { Kantua } from "./models";
import { createEffect, createSignal } from "solid-js";

export interface Bookmarks {
  bookmarks: string[];
  add: (kantua: Kantua) => void;
  remove: (kantua: Kantua) => void;
  isBookmarked: (kantua: Kantua) => boolean;
}

export function createBookmarks(): Bookmarks {
  const initialBookmarks: string[] = readBookmarks();
  const [$bookmarks, setBookmarks] = createSignal(initialBookmarks);

  // persist new bookmarks
  createEffect(() => {
    if ($bookmarks() !== initialBookmarks) {
      writeBookmarks($bookmarks());
    }
  });

  function isBookmarked(kantua: Kantua) {
    return $bookmarks().includes(kantua.id);
  }

  function add(kantua: Kantua) {
    if (!isBookmarked(kantua)) {
      setBookmarks([...$bookmarks(), kantua.id]);
    }
  }

  function remove(kantua: Kantua) {
    if (isBookmarked(kantua)) {
      setBookmarks($bookmarks().filter((bookmark) => bookmark !== kantua.id));
    }
  }

  return {
    get bookmarks() { return $bookmarks() }, // prettier-ignore
    add,
    remove,
    isBookmarked,
  };
}

function readBookmarks() {
  const json = localStorage.getItem("kantuz:bookmarks") ?? "[]";
  const data = JSON.parse(json);
  return data;
}

function writeBookmarks(bookmarks: string[]) {
  const json = JSON.stringify(bookmarks);
  localStorage.setItem("kantuz:bookmarks", json);
}
