import "./index.css";

import { useContext } from "solid-js";
import { A, useNavigate } from "@solidjs/router";
import { AppContext } from "../app";
import { Layout } from "../layout";
import { slugifyObject } from "../utils";
import { BookIcon, BookmarkIcon, SearchIcon, ShuffleIcon } from "../components/icons";
import { useSearchShortcut } from "../components/search-input";
import { useSearch } from "../search";

export function IndexPage() {
  let input: HTMLInputElement | undefined;

  const { db } = useContext(AppContext);

  const navigate = useNavigate();
  const search = useSearch();

  useSearchShortcut(() => input);

  function handleSubmit(e: SubmitEvent) {
    e.preventDefault();
    if (input?.value) search(input.value);
  }

  function handleRandom() {
    const kantuak = db.getKantuak();
    if (kantuak.length === 0) return;
    const index = Math.floor(Math.random() * kantuak.length);
    const slug = slugifyObject(kantuak[index]);
    navigate(`/kantuak/${slug}`);
  }

  return (
    <Layout>
      <section class="index">
        <div class="logo">
          <img src="/lauburu.svg" width={96} height={96} />
          <h1>KANTUZ</h1>
        </div>

        <form onSubmit={handleSubmit}>
          <input ref={input} type="search" placeholder="Izenburua, hitzak..." autofocus />

          <div class="actions">
            <button type="submit">
              <SearchIcon />
              <span>Xerkatu</span>
            </button>

            <button type="button" onClick={handleRandom}>
              <ShuffleIcon />
              <span>Nahastu</span>
            </button>
          </div>
        </form>

        <div class="menu">
          <A href="/kantutegiak">
            <button>
              <BookIcon />
              <span>Kantutegiak</span>
            </button>
          </A>

          <A href="/ene-kantuak">
            <button>
              <BookmarkIcon />
              <span>Ene Kantuak</span>
            </button>
          </A>
        </div>
      </section>
    </Layout>
  );
}
