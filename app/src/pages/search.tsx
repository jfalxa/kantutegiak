import { Show, useContext } from "solid-js";
import { AppContext } from "../app";
import { Layout } from "../layout";
import { VirtualList } from "../components/virtual-list";
import { KantuaItem } from "../components/kantua-item";

export function SearchPage() {
  const { db, search } = useContext(AppContext);

  return (
    <Layout>
      <VirtualList items={search.results}>
        {(kantua, index) => kantua && <KantuaItem kantua={kantua.document} index={index} />}
      </VirtualList>

      <Show when={db.loading || search.loading}>
        <div class="overlay">
          <img src="/loader.svg" width={96} height={96} />
        </div>
      </Show>
    </Layout>
  );
}
