import "./kantua.css";

import type { PlayerElement } from "html-midi-player";

import { Show, createEffect, createMemo, onCleanup, useContext } from "solid-js";
import { A, useParams } from "@solidjs/router";
import { AppContext } from "../app.jsx";
import { shortenURL } from "../utils.js";
import { Layout } from "../layout.jsx";
import { YoutubeIframe, YoutubeSearch } from "../components/youtube.js";
import { Translations } from "../components/translations.jsx";
import { Bookmark } from "../components/bookmark.jsx";
import { ScrollToTop } from "../components/scroll-to-top.jsx";

export function KantuaPage() {
  let container: HTMLDivElement | undefined;
  let player: PlayerElement | undefined;

  const { db } = useContext(AppContext);
  const params = useParams<{ slug: string }>();

  const $kantua = createMemo(() => db.getKantuaBySlug(params.slug));
  const $kantutegia = createMemo(() => db.getKantutegiaById($kantua()?.kantutegia ?? ""));

  createEffect(() => {
    if ($kantua()?.midi) {
      import("html-midi-player");
    }
  });

  onCleanup(() => {
    player?.stop();
  });

  return (
    <Layout>
      <Show when={$kantua()} keyed>
        {(kantua) => (
          <>
            <section ref={container} class="kantua">
              <section class="lyrics">
                <h1>
                  {kantua.izenburua}
                  <Bookmark kantua={kantua} />
                </h1>
                <A href={`/kantutegiak/${$kantutegia()?.slug}`}>{$kantutegia()?.izenburua}</A>
                <p>{kantua.hitzak}</p>
              </section>

              <div class="separator" />

              <section class="resources">
                <Show when={kantua.jatorria} keyed>
                  {(jatorria) => (
                    <section class="source">
                      <small>Datuen jatorria:</small>
                      <a href={jatorria} target="_blank" rel="noopener noreferrer">
                        {shortenURL(jatorria)}
                      </a>
                    </section>
                  )}
                </Show>

                <Show when={kantua.partitura} keyed>
                  {(partitura) => (
                    <a href={`/assets/${partitura}`} target="_blank" rel="noopener noreferrer">
                      <img
                        class="sheet"
                        src={`/assets/${partitura}`}
                        width="640px"
                        onError={(e) => (e.currentTarget.style.display = "none")}
                        onLoad={(e) => (e.currentTarget.style.display = "")}
                      />
                    </a>
                  )}
                </Show>

                <Show when={kantua.midi} keyed>
                  {(midi) => <midi-player ref={player} src={`/assets/${midi}`} sound-font />}
                </Show>

                <Show
                  keyed
                  when={kantua.youtube}
                  fallback={<YoutubeSearch query={kantua.izenburua} />}
                >
                  {(youtube) => <YoutubeIframe id={youtube} />}
                </Show>

                <Translations kantua={kantua} />
              </section>
            </section>

            <ScrollToTop container={container} />
          </>
        )}
      </Show>
    </Layout>
  );
}
