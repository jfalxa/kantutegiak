import { Show, createMemo, useContext } from "solid-js";
import { useParams } from "@solidjs/router";
import { AppContext } from "../app";
import { Layout } from "../layout";
import { VirtualList } from "../components/virtual-list";
import { KantuaItem } from "../components/kantua-item";

export function KantutegiaPage() {
  const { db } = useContext(AppContext);
  const params = useParams<{ slug: string }>();

  const $kantutegia = createMemo(() => db.getKantutegiaBySlug(params.slug));
  const $kantuak = createMemo(() => $kantutegia() ? db.getKantuakByKantutegia($kantutegia()!.id) : []); // prettier-ignore

  const header = (
    <Show when={$kantutegia()} keyed>
      {(kantutegia) => (
        <header>
          <h1>{kantutegia.izenburua}</h1>
          <p>
            <i>{kantutegia.details}</i>
            <span>{kantutegia.count} kantuak</span>
          </p>
        </header>
      )}
    </Show>
  );

  return (
    <Layout>
      <VirtualList header={header} items={$kantuak()}>
        {(kantua, index) => <KantuaItem kantua={kantua} index={index} />}
      </VirtualList>
    </Layout>
  );
}
