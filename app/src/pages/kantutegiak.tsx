import { useContext } from "solid-js";
import { AppContext } from "../app";
import { Layout } from "../layout";
import { VirtualList } from "../components/virtual-list";
import { KantutegiaItem } from "../components/kantutegia-item";

export function KantutegiakPage() {
  const { db } = useContext(AppContext);

  const header = (
    <header>
      <h1>Kantutegiak</h1>
    </header>
  );

  return (
    <Layout>
      <VirtualList header={header} items={db.getKantutegiak()}>
        {(kantutegia, index) => <KantutegiaItem kantutegia={kantutegia} rank={index} />}
      </VirtualList>
    </Layout>
  );
}
