import { createMemo, useContext } from "solid-js";
import { AppContext } from "../app";
import { Layout } from "../layout";
import { VirtualList } from "../components/virtual-list";
import { KantuaItem } from "../components/kantua-item";

export function EneKantuakPage() {
  const { db, bookmarks } = useContext(AppContext);

  const $kantuak = createMemo(() =>
    db.getKantuak().filter((kantua) => bookmarks.isBookmarked(kantua))
  );

  const header = (
    <header>
      <h1>Ene kantuak</h1>
    </header>
  );

  return (
    <Layout>
      <VirtualList header={header} items={$kantuak()}>
        {(kantua, index) => <KantuaItem kantua={kantua} index={index} />}
      </VirtualList>
    </Layout>
  );
}
