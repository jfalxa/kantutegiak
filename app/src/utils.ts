const END_RX = /[\w-_]+\/?$/;

export function shortenURL(url: string) {
  if (url.length < 50) return url;

  const u = new URL(url);
  const match = u.pathname.match(END_RX);

  if (!match) return url;
  else return `${u.origin}/.../${match[0]}`;
}

export function normalize(str: string) {
  return str
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
}

export function slugify(str: string, suffix = "") {
  return (
    normalize(str)
      .replace(/[^\w- ]/g, "")
      .replace(/[-+ ]/g, "-")
      .toLowerCase() + suffix
  );
}

export function slugifyObject(obj: { id: string; izenburua: string }): string {
  return `${slugify(obj.izenburua)}-${obj.id}`;
}
