/* @refresh reload */

import "./index.css";

import { render } from "solid-js/web";
import { Route, Router } from "@solidjs/router";
import { App } from "./app";
import { IndexPage } from "./pages/index";
import { SearchPage } from "./pages/search";
import { KantuaPage } from "./pages/kantua";
import { KantutegiaPage } from "./pages/kantutegia";
import { KantutegiakPage } from "./pages/kantutegiak";
import { EneKantuakPage } from "./pages/ene-kantuak";

const root = document.getElementById("root")!;

function Root() {
  return (
    <Router root={App}>
      <Route path="/" component={IndexPage} />
      <Route path="/search" component={SearchPage} />
      <Route path="/kantutegiak" component={KantutegiakPage} />
      <Route path="/kantutegiak/:slug" component={KantutegiaPage} />
      <Route path="/kantuak/:slug" component={KantuaPage} />
      <Route path="/ene-kantuak" component={EneKantuakPage} />
    </Router>
  );
}

render(Root, root);
