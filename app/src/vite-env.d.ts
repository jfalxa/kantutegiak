/// <reference types="vite/client" />
/// <reference types="vite-plugin-pwa/solid" />

import { JSX } from "solid-js";
import { PlayerElement } from "html-midi-player";

declare module "solid-js" {
  namespace JSX {
    interface IntrinsicElements {
      "midi-player": HTMLAttributes<PlayerElement> & { src: string; "sound-font": boolean };
    }
  }
}
