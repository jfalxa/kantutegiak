import "./translations.css";

import type { Kantua } from "../models";

const Lang = {
  en: "🇺🇸",
  es: "🇪🇸",
  fr: "🇫🇷",
};

interface TranslationsProps {
  kantua: Kantua;
}

export function Translations(props: TranslationsProps) {
  return (
    <section class="translations">
      <small>Itzuli Google-ekin:</small>
      <ul>
        <Translate lang="en" kantua={props.kantua} />
        <Translate lang="es" kantua={props.kantua} />
        <Translate lang="fr" kantua={props.kantua} />
      </ul>
    </section>
  );
}

interface TranslateProps {
  lang: keyof typeof Lang;
  kantua: Kantua;
}

function Translate(props: TranslateProps) {
  const $url = () => {
    const encoded = encodeURIComponent(`${props.kantua.izenburua}\n\n${props.kantua.hitzak}`);
    return `https://translate.google.com/?op=translate&sl=eu&tl=${props.lang}&text=${encoded}`;
  };

  return (
    <li class="translate">
      <a href={$url()} target="_blank" rel="noopener noreferrer">
        {Lang[props.lang]}
      </a>
    </li>
  );
}
