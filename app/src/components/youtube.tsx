import { SearchIcon } from "./icons";
import "./youtube.css";

interface YoutubeIframeProps {
  id: string;
}

export function YoutubeIframe(props: YoutubeIframeProps) {
  return (
    <iframe
      class="youtube-iframe"
      allowfullscreen
      src={`https://www.youtube.com/embed/${props.id}?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0`}
    ></iframe>
  );
}

interface YoutubeSearchProps {
  query: string;
}

export function YoutubeSearch(props: YoutubeSearchProps) {
  return (
    <a
      class="youtube-search"
      href={`https://youtube.com/results?search_query=kantua+${encodeURIComponent(props.query)}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      <button>
        <SearchIcon size={20} />
        YouTuben xerkatu
      </button>
    </a>
  );
}
