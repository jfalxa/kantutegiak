import type { Kantua } from "../models.js";

import { A, useLocation } from "@solidjs/router";
import { slugifyObject } from "../utils.js";
import { Bookmark } from "./bookmark.jsx";

interface KantuaItemProps {
  index: number;
  kantua: Kantua;
}

export function KantuaItem(props: KantuaItemProps) {
  const location = useLocation();

  return (
    <A href={`/kantuak/${slugifyObject(props.kantua)}${location.search}`}>
      <small>#{props.index + 1}</small>
      <div class="summary">
        <strong class="title">{props.kantua.izenburua}</strong>
        <small class="description">{props.kantua.intro}...</small>
      </div>
      <Bookmark kantua={props.kantua} />
    </A>
  );
}
