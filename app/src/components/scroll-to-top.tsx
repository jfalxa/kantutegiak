import { ArrowUpIcon } from "./icons";
import "./scroll-to-top.css";

import { Show, createSignal, onCleanup, onMount } from "solid-js";

interface ScrollToTopProps {
  container: HTMLElement | undefined;
}

export function ScrollToTop(props: ScrollToTopProps) {
  const [$hidden, setHidden] = createSignal(true);

  function scrollToTop() {
    props.container?.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }

  function onScroll() {
    const hidden = $hidden();
    const scroll = props.container?.scrollTop ?? 0;

    if (hidden && scroll > 0) {
      setHidden(false);
    } else if (!hidden && scroll === 0) {
      setHidden(true);
    }
  }

  onMount(() => {
    props.container?.addEventListener("scroll", onScroll);
  });

  onCleanup(() => {
    props.container?.removeEventListener("scroll", onScroll);
  });

  return (
    <Show when={!$hidden()}>
      <button class="scroll-to-top" onClick={scrollToTop}>
        <ArrowUpIcon />
      </button>
    </Show>
  );
}
