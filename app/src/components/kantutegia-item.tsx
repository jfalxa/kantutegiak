import type { Kantutegia } from "../models.js";

import { A, useLocation } from "@solidjs/router";
import { slugifyObject } from "../utils.js";

interface KantutegiaItemProps {
  rank: number;
  kantutegia: Kantutegia;
}

export function KantutegiaItem(props: KantutegiaItemProps) {
  const location = useLocation();

  return (
    <A href={`/kantutegiak/${slugifyObject(props.kantutegia)}${location.search}`}>
      <small>#{props.rank + 1}</small>
      <div class="summary">
        <strong class="title">{props.kantutegia.izenburua}</strong>
        <small class="description">
          <i>{props.kantutegia.details}</i>
          {props.kantutegia.count} kantuak
        </small>
      </div>
    </A>
  );
}
