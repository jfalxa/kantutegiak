import "./virtual-list.css";

import type { JSX } from "solid-js";

import { createVirtualizer } from "@tanstack/solid-virtual";
import { For, Show, createEffect, createMemo } from "solid-js";
import { ScrollToTop } from "./scroll-to-top";

interface VirtualListProps<T> {
  items: T[];
  header?: JSX.Element;
  children: (item: T, index: number) => JSX.Element;
}

export function VirtualList<T>(props: VirtualListProps<T>) {
  let container: HTMLDivElement | undefined;

  const $virtualizer = createMemo(() =>
    createVirtualizer({
      count: props.items.length,
      overscan: 2,
      getScrollElement: () => container!,
      estimateSize: () => 80,
    })
  );

  createEffect(() => {
    $virtualizer().scrollToOffset(0);
  });

  return (
    <>
      <section ref={container} class="virtual-container" tabIndex={-1}>
        {props.header}

        <ul class="virtual-list" style={{ height: `${$virtualizer().getTotalSize()}px` }}>
          <For each={$virtualizer().getVirtualItems()}>
            {(virtual) => (
              <Show keyed when={props.items[virtual.index]}>
                {(item) => (
                  <li
                    class="virtual-item"
                    style={{
                      height: `${virtual.size}px`,
                      transform: `translateY(${virtual.start}px)`,
                    }}
                  >
                    {props.children(item, virtual.index)}
                  </li>
                )}
              </Show>
            )}
          </For>
        </ul>
      </section>

      <ScrollToTop container={container} />
    </>
  );
}
