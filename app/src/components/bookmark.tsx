import "./bookmark.css";

import type { Kantua } from "../models";

import { useContext } from "solid-js";
import { AppContext } from "../app";
import { BookmarkIcon } from "./icons";

interface BookmarkProps {
  kantua: Kantua;
}

export function Bookmark(props: BookmarkProps) {
  const { bookmarks } = useContext(AppContext);

  function toggleBookmark(e: MouseEvent) {
    e.preventDefault();
    e.stopPropagation();

    if (!bookmarks.isBookmarked(props.kantua)) {
      bookmarks.add(props.kantua);
    } else {
      bookmarks.remove(props.kantua);
    }
  }

  return (
    <button class="bookmark" onClick={toggleBookmark}>
      <BookmarkIcon fill={bookmarks.isBookmarked(props.kantua)} />
    </button>
  );
}
