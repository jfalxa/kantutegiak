import "./top-bar.css";

import { Show } from "solid-js";
import { A, useLocation } from "@solidjs/router";
import { SearchInput } from "./search-input";
import { BookIcon, BookmarkIcon } from "./icons";

export function TopBar() {
  const location = useLocation();

  return (
    <Show when={location.pathname !== "/"}>
      <header class="topbar">
        <A href={`/${location.hash}`} class="logo">
          <img src="/lauburu.svg" alt="Logo" width={32} height={32} />
          <h1>KANTUZ</h1>
        </A>

        <div class="menu">
          <A href="/kantutegiak">
            <button tabIndex={-1}>
              <BookIcon />
              <span>Kantutegiak</span>
            </button>
          </A>

          <A href="/ene-kantuak">
            <button tabIndex={-1}>
              <BookmarkIcon />
              <span>Ene kantuak</span>
            </button>
          </A>
        </div>
        <SearchInput />
      </header>
    </Show>
  );
}
