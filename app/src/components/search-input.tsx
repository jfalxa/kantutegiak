import "./search-input.css";

import { onCleanup, onMount } from "solid-js";
import { useQuery, useSearch } from "../search";
import { SearchIcon } from "./icons";

export function SearchInput() {
  let input: HTMLInputElement | undefined;

  const $query = useQuery();
  const search = useSearch();

  useSearchShortcut(() => input);

  function handleSubmit(e: SubmitEvent) {
    e.preventDefault();
    if (input?.value) search(input.value);
  }

  return (
    <form class="search-input" onSubmit={handleSubmit}>
      <input
        ref={input}
        value={$query()}
        type="search"
        name="q"
        placeholder="Izenburua, hitzak..."
      />
      <button>
        <SearchIcon />
      </button>
    </form>
  );
}

export function useSearchShortcut(getInput: () => HTMLInputElement | undefined) {
  onMount(() => {
    window.addEventListener("keypress", handleSearchShortcut);
  });

  onCleanup(() => {
    window.removeEventListener("keypress", handleSearchShortcut);
  });

  function handleSearchShortcut(e: KeyboardEvent) {
    const input = getInput();
    if (input && e.key === "/") {
      e.preventDefault();
      input.focus();
      input.setSelectionRange(0, input.value.length);
    }
  }
}
