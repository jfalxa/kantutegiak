export interface Kantua {
  id: string;
  izenburua: string;
  hitzak: string;
  kantutegia: string;
  youtube: string | null;
  partitura: string | null;
  midi: string | null;
  intro?: string;
  slug?: string;
  jatorria?: string;
}

export interface Kantutegia {
  id: string;
  izenburua: string;
  urtea: number | null;
  argitaletxea: string;
  count: number;
  slug?: string;
  details?: string;
}

export interface KantuzDB {
  kantutegiak: Kantutegia[];
  kantuak: Kantua[];
}
