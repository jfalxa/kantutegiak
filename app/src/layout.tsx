import "./layout.css";

import type { JSX } from "solid-js";

import { TopBar } from "./components/top-bar";

interface LayoutProps {
  children: JSX.Element;
}

export function Layout(props: LayoutProps) {
  return (
    <>
      <TopBar />
      <main>{props.children}</main>
    </>
  );
}
