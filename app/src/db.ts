import type { Kantua, Kantutegia, KantuzDB } from "./models";

import { Accessor, createMemo, createResource } from "solid-js";
import { slugifyObject } from "./utils";

export interface DB {
  loading: boolean;
  getKantutegiak: () => Kantutegia[];
  getKantutegiaById: (id: string) => Kantutegia | undefined;
  getKantutegiaBySlug: (slug: string) => Kantutegia | undefined;
  getKantuak: () => Kantua[];
  getKantuaById: (id: string) => Kantua | undefined;
  getKantuaBySlug: (slug: string) => Kantua | undefined;
  getKantuakByKantutegia: (id: string) => Kantua[];
}

export function createKantuzDB(): DB {
  const [$kantuz] = createResource(fetchKantuzDB, {
    initialValue: { kantutegiak: [], kantuak: [] },
  });

  const $kantutegiak = createKantutegiak($kantuz);
  const $kantutegiakIndex = createKantutegiakIndex($kantutegiak);

  const $kantuak = createKantuak($kantuz);
  const $kantuakIndex = createKantuakIndex($kantuak);

  function getKantutegiak() {
    return $kantutegiak();
  }

  function getKantutegiaById(id: string): Kantutegia | undefined {
    return $kantutegiakIndex().id[id];
  }

  function getKantutegiaBySlug(slug: string): Kantutegia | undefined {
    return $kantutegiakIndex().slug[slug];
  }

  function getKantuak() {
    return $kantuak();
  }

  function getKantuaById(id: string): Kantua | undefined {
    return $kantuakIndex().id[id];
  }

  function getKantuaBySlug(slug: string): Kantua | undefined {
    return $kantuakIndex().slug[slug];
  }

  function getKantuakByKantutegia(id: string): Kantua[] {
    return $kantuakIndex().kantutegia[id] ?? [];
  }

  return {
    get loading() { return $kantuz.loading }, // prettier-ignore
    getKantutegiak,
    getKantutegiaById,
    getKantutegiaBySlug,
    getKantuak,
    getKantuaById,
    getKantuaBySlug,
    getKantuakByKantutegia,
  };
}

async function fetchKantuzDB(): Promise<KantuzDB> {
  const response = await fetch("/kantuz-db.json");
  const data = await response.json();
  return data;
}

function createKantutegiak($kantuz: Accessor<KantuzDB>) {
  return createMemo<Kantutegia[]>(() =>
    $kantuz()
      .kantutegiak.sort((a, b) => a.izenburua.localeCompare(b.izenburua))
      .map((kantutegia) => ({
        ...kantutegia,
        slug: slugifyObject(kantutegia),
        details: getKantutegiaDetails(kantutegia),
      }))
  );
}

function createKantuak($kantuz: Accessor<KantuzDB>) {
  return createMemo<Kantua[]>(() =>
    $kantuz()
      .kantuak.sort((a, b) => a.izenburua.localeCompare(b.izenburua))
      .map((kantua) => ({
        ...kantua,
        slug: slugifyObject(kantua),
        intro: getKantuaIntro(kantua),
        jatorria: getKantuaSource(kantua),
      }))
  );
}

function createKantutegiakIndex($kantutegiak: Accessor<Kantutegia[]>) {
  return createMemo(() => {
    const kantutegiakIndex = {
      id: {} as Record<string, Kantutegia>,
      slug: {} as Record<string, Kantutegia>,
    };

    for (const kantutegia of $kantutegiak()) {
      kantutegiakIndex.id[kantutegia.id] = kantutegia;

      const slug = slugifyObject(kantutegia);
      kantutegiakIndex.slug[slug] = kantutegia;
    }

    return kantutegiakIndex;
  });
}

function createKantuakIndex($kantuak: Accessor<Kantua[]>) {
  return createMemo(() => {
    const kantuakIndex = {
      id: {} as Record<string, Kantua>,
      slug: {} as Record<string, Kantua>,
      kantutegia: {} as Record<string, Kantua[]>,
    };

    for (const kantua of $kantuak()) {
      kantuakIndex.id[kantua.id] = kantua;

      const slug = slugifyObject(kantua);
      kantuakIndex.slug[slug] = kantua;

      if (!kantuakIndex.kantutegia[kantua.kantutegia]) kantuakIndex.kantutegia[kantua.kantutegia] = [] // prettier-ignore
      kantuakIndex.kantutegia[kantua.kantutegia].push(kantua);
    }

    return kantuakIndex;
  });
}

function getKantutegiaDetails(kantutegia: Kantutegia) {
  let details = "";
  if (kantutegia.argitaletxea) details += kantutegia.argitaletxea + " ";
  if (kantutegia.urtea) details += `(${kantutegia.urtea})`;
  return details ? `${details} · ` : "";
}

function getKantuaIntro(kantua: Kantua) {
  const intro = kantua.hitzak.slice(0, 100);
  const lastSpace = intro.lastIndexOf(" ");
  return intro.substring(0, lastSpace);
}

function getKantuaSource(kantua: Kantua) {
  if (kantua?.youtube) {
    return `https://youtu.be/${kantua.youtube}`;
  } else if (kantua?.id.startsWith("ab-")) {
    return `https://www.eusko-ikaskuntza.eus/eu/dokumentu-fondoa/euskal-kantutegia/${kantua.id}/`;
  } else {
    return "";
  }
}
