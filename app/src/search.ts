import type { Accessor } from "solid-js";
import type { Kantua } from "./models";
import type { Orama } from "@orama/orama";

import { createResource, createMemo } from "solid-js";
import { useNavigate, useSearchParams } from "@solidjs/router";
import { create, insertMultiple, search } from "@orama/orama";

const Schema = {
  izenburua: "string",
  intro: "string",
} as const;

interface SearchResult {
  score: number;
  document: Kantua;
}

export interface Search {
  loading: boolean;
  results: SearchResult[];
}

export function createSearch($items: Accessor<Kantua[]>): Search {
  const $query = useQuery();

  const [$index] = createResource($items, buildIndex);

  const [$results] = createResource(
    () => [$query(), $index()] as const,
    ([query, index]) => searchKantuak(query, index),
    { initialValue: [] }
  );

  return {
    get loading() { return $index.loading || $results.loading }, // prettier-ignore
    get results() { return $results() }, // prettier-ignore
  };
}

export function useQuery() {
  const [searchParams] = useSearchParams();
  return createMemo(() => decodeURIComponent(searchParams.q ?? ""));
}

export function useSearch() {
  const navigate = useNavigate();
  return (query: string) => navigate(`/search?q=${encodeURIComponent(query)}`);
}

async function buildIndex(kantuak: Kantua[]) {
  const index = await create({ schema: Schema });
  await insertMultiple(index, kantuak);
  return index;
}

async function searchKantuak(query?: string, index?: Orama<typeof Schema>) {
  if (!index || !query) return [];

  const results = await search(index, {
    term: query,
    properties: ["izenburua", "intro"],
    tolerance: 2,
    threshold: 0,
    limit: 20,
    boost: { izenburua: 2 },
  });

  return results.hits as unknown as SearchResult[];
}
