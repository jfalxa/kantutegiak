import type { JSX } from "solid-js";
import type { DB } from "./db.js";
import type { Search } from "./search.js";
import type { Bookmarks } from "./bookmarks.js";

import { ErrorBoundary, Suspense, createContext } from "solid-js";
import { createKantuzDB } from "./db.js";
import { createSearch } from "./search.js";
import { createBookmarks } from "./bookmarks.js";

export const AppContext = createContext({
  db: {} as DB,
  search: {} as Search,
  bookmarks: {} as Bookmarks,
});

interface AppProps {
  children?: JSX.Element;
}

export function App(props: AppProps) {
  const db = createKantuzDB();
  const search = createSearch(db.getKantuak);
  const bookmarks = createBookmarks();

  return (
    <ErrorBoundary fallback="">
      <Suspense>
        <AppContext.Provider value={{ db, search, bookmarks }}>
          {props.children}
        </AppContext.Provider>
      </Suspense>
    </ErrorBoundary>
  );
}
