import { defineConfig } from "vite";
import { VitePWA, VitePWAOptions } from "vite-plugin-pwa";
import solid from "vite-plugin-solid";

const pwaOptions: Partial<VitePWAOptions> = {
  registerType: "autoUpdate",

  manifest: {
    name: "Kantuz",
    short_name: "Kantuz",
    theme_color: "#000000",
    background_color: "#ffefd5",
    icons: [
      {
        src: "logo-192x192.png",
        sizes: "192x192",
        type: "image/png",
      },
      {
        src: "/logo-512x512.png",
        sizes: "512x512",
        type: "image/png",
      },
      {
        src: "/logo-192x192.png",
        sizes: "192x192",
        type: "image/png",
        purpose: "any maskable",
      },
    ],
  },

  workbox: {
    maximumFileSizeToCacheInBytes: 10e6,
    globPatterns: ["**/*.{html,css,js,json,svg,png,ico}"],
  },
};

export default defineConfig({
  plugins: [solid(), VitePWA(pwaOptions)],
});
