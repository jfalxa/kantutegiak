---
id: tx-2361
izenburua: Printzesa Kataroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GMmhKqubgDo
---

Iduri askoren artean

bat nabarmena

printzesa katara

bere bitartez hedatzen du amore

edertasun ta bizia.

 

Kantua da bere irria

perlazko den negarra

bere bihotza garbia

uso xuria da.

 

Beretik sortzen dana

biziaren argia.

 

Bere barneko jauzitik

mintzatzen du

printzesa katarak

Ur tantakin biziaren egiaz

haiek entzuten amodiozko hitzak.

 

Bere bihotz zabaletik

kantu berri baten

indarrak sortzen dira.

 

Munduko argia

deitzen dizute

printsesa katara

edertasun baten distira

mundua lortu ezina.

 

Hiru perlak osatzen dute zure koroa:

ontasun, araztasun

ta amodioa.

 

Amodioa da

guztietatik ederrena.