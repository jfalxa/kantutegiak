---
id: tx-2980
izenburua: Karriketan Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UVjbbZUBRNE
---

“EZTA ILEN
ERRONKARI’N
USKARA”
(Ene arreba Feli’ri)
Nafarra'ko
iparraltean
Erronkari
Ibaxa,
mendi andi
lerren artean
daude zazpi
iriak.
Arri-pean
ardi ñoa,
ardi-altean
axurua,
irietan
etxe zarrak,
etxe-barnean
erdara.
ezta ilen, ezta ilen
Erronkari’n uskara.
Almadieroak
negu gorrian
xoaitan dra
egoatxetik,
ardi, txakur
eta artzaiak
Bardean
bidetarik.
Ogei gizon
eta mazte
dra oraiko
uskaldunak,
uskaldun zar
xoanez geroz
¿non daude
berriak?
ezta ilen, ezta ilen
Erronkari’n uskara.
Gore uskara
iltan bada
zore izena
eztakizu,
zore etxe,
izenburia
ezta deus
zoretako.
Gore uskara
iltan dago
¡xoan fite
neskatoa!
matto bero,
matto andi
uskarari
emoitra.
ezta ilen, ezta ilen
Erronkari’n uskara.
José ESTORNES
LASA
Argibideak
Lerra: piñua.
Xoaitan dra: etortzen
dira.
Egoatxetik: urbidetik,
ibaitik.
Bardean bidetarik:
Bardenan bidetatik.
Matto: muxu