---
id: tx-1100
izenburua: Itxoiten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W3kA9lMQ_EI
---

Amari itxoiten, itxoiten
aitak besoetan har nazan, itxoiten

Lehen eskola egunari, itxoiten
titiak noiz haziko, itxoiten

Itxoiten!

Esaminari, itxoiten
lana noiz lortuko, itxoiten
norbaiti, itxoiten

Orgasmora heltzeko, itxoiten
berak nere denbora bete dezan, itxoiten

Haurra noiz jaioko, itxoiten
umea noiz koxkortuko, itxoiten

Itxoiten!

Bera nere bila noiz etorriko, itxoiten
seme-alaben bisitaldiari, itxoiten

Liberazioari, itxoiten
liberazioari, itxoiten