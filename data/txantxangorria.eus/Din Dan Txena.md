---
id: tx-1525
izenburua: Din Dan Txena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ledJ0PTz03s
---

Din dan golo, golo, golo
din dan go, din dan go
Din don golo, golo, golo
din dan go, din dan go
E la, horrelaxe la
horrelaxe la elaio,
e la, horrelaxe la
horrelaxe la elaio.

Xaliuari, xaliuari, xaliuari, xaliuari,
umpa, umpa, umpa.

Din dan golo, golo, golo
din dan go, din dan go
Din don golo, golo, golo
din dan go, din dan go
E la, horrelaxe la
horrelaxe la elaio,
e la, horrelaxe la
horrelaxe la elaio.

Txena, txena, txena, txena
a baratuena saiani, bamosaba
ana, ana, ana, ana
a natiarena ni beltza, zirri zarra.