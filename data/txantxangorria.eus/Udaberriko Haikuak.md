---
id: tx-2440
izenburua: Udaberriko Haikuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5YFRxQ6zEaY
---

Josu Bergara eta Akorde Txikiak taldea zuzenean Bilboko Kafe Antzokian 2018/06/21

Diskoa: Asunak (2018, Taupaka elkartea)
Bideogileak: Tiropun Ekoizpenak

 Ahotsa, gitarra eta musika: Josu Bergara 
Biolina eta konposizioa: Nerea Alberdi 
Pianoa: Mikel Nuñez 
Ahotsa: Irati Bilbao 
Gitarra: Lüi Young 
Kontrabaxua: Dani Gonzalez 
Bateria: Txema Arana 
Produkzioa: David Sanchez Damián


UDABERRIKO HAIKUAK (Hitzak eta musika: Josu Bergara. Konposizioa: Nerea Alberdi) Elurra urtzen hasi da mendian, erabakia hartu duenean, atzera begiratu gabe joan da, beldur barik hegan egitera. Ihesi doa, eta ez daki nora, hemendik urrutira. Ihesi doa, eta ez daki nora, hemendik… urrutira. Udaberriaren iraultza datorrenean, eguzkiaren lehen laztanetan, han goitik itsasoa ikusten da, harra bihurtu da tximeleta ****************************************** Diskoa deskargatu: www.josubergara.eus Kontratazioa: info@taupaka.eus (+34) 688 840 713 CD-a erosi: info@josubergara.eus ************************************ Josu Bergara musikariaren 5. estudioko lana "Asunak. Bide-bazterreko melodia sendagarriak" izenburuarekin argitaratu dena 2018ko maiatzaren 30ean, Taupaka Elkartearen zigilupean.