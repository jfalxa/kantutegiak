---
id: tx-2444
izenburua: Eguzki Gorri Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i_JfZiQ0_DQ
---

Hona hemen Giovanni Falcone Zanon eta biok grabatutako abesti hau. Eguzki gorri bat, moldaketa akustikoa. Josu Eizagirrek grabatuta, 2018ko maiatzean. Ea gustatzen zaizuen.


Eguzki gorri bat
hodei zintan jaiotzen
itsaso gainetik
zeru lainotuan gora
bigun ari da mugitzen
goizalde hau leuntzen

Haur jaio berriaren
garrasia bezala
more koloreko
esku handi bat zabalduz
lainoz-laino datorrena
nagoen lekuraino

Eta joango da
bere bidaia egitera
lehenago zenaren
oroimen lauso bat utzita
zintzilik begietan

Eguzki gorri bat
hodei zintan da jaio
biharkoaren zain
geratu ez zaitezten

Zeru urdin bat
plaza handi bat bezala
non elkartzen diren
izarrak eta planetak
ezezaguna zaigun
plan bat jarraituta

Eta joango dira
musika misteriotsu bat
etengabe dantzatuz
egunero berritzen den
ospakizun batean

Eguzki gorri bat
hodei zintan da jaio
biharkoa agian
ez dut badaikusiko