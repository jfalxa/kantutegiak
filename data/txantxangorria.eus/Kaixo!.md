---
id: tx-1930
izenburua: Kaixo!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Lxu5_IzBdB0
---

Trikitixa dantzatu nahiko nuke
edozein herritako jaietan
trikitixa dantzatu nahiko nuke
geure begiak topatuz plazan.

Maitemindurik nago eta
zu oraindik ez zara konturatu;
egunez mozkorturik nauzu
eta gau aldean ezin loak hartu.

Zu hor zabiltza dantzan eta
ni hemen hurrun ez dakidalako ...
norbaitek erakutsiko balit!
-Kaixo!
-Kaixo!
-Zelan?
-Ondo, ta zu?
-Beno..
-Zer ba?
-Begira.
-Esan.
-Heldu!
-Bihotza da!
-Bihotza da bai!