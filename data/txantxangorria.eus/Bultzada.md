---
id: tx-2325
izenburua: Bultzada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KSVBM4eSudw
---

Abesti hau 2019ko irailean grabatu da Iruñeako Sound of Sirens estudioan, Julen Urzaizen eskutik. Bideoklipa Dani SKSTen bitartez grabatu da. 

Kontaktuak:
Brigade Loco: brigadeloko@gmail.com
Julen Urzaiz: urzaizjul@hotmail.com
Dani SKST: skstdesign@gmail.com


Letra(Euskaraz):

Gogoratzen dut lehenengo aldia, aitaren eskutik sartu nintzenean. 
Bokata eskuan, bufanda lepoan.
Haur baten ilusioa aurpegiko irribarrean.

Hego harmailara begirada bota.
Ikurrak, banderak, zarata eta algara.
Urteak aurrera lagunekin elkartu
Hego harmaila orain gure etxea bihurtu da.

Txuri urdin....koloreak, harrotasunez eramanda!!!
BATERA BAGOAZ GARAIPENA GUREA DA.
ZATOZ GUREKIN ETA EMANTZAZU BULTZADA.

GURE IZANA DEFENDATUZ GOAZ AURRERA, HEGO HARMAILAK EGIN BEHAR DU EZTANDA.
Anoetako partidak, egindako bidaiak.
Euskal derbietako aldarrikapenak.
Satrusen korrikaldiak Agirretxeren golak
Bigarren mailako zulotik Europako izarretara.

Txuri urdin...koloreak, harrotasunez eramanda.

BATERA BAGOAZ GARAIPENA GUREA DA.
ZATOZ GUREKIN ETA EMANTZAZU BULTZADA.
GURE IZANA DEFENDATUZ GOAZ AURRERA, HEGO HARMAILAK EGIN BEHAR DU EZTANDA!


Letra(Gaztelaniaz):

Recuerdo la primera vez, cuando entré de la mano de mi padre
Bocata en mano, bufanda al cuello
la ilusión de un niño en la sonrisa de su cara

Echar una mirada al fondo sur
simbolos, banderas, ruido y gritos
Pasan los años, te juntas con los colegas...
el fondo sur se ha convertido en nuestra casa

Los colores blanquiazules, llevados con orgullo

El triunfo es nuestro si vamos de la mano
Ven con nosotros a dar ese empuje
Avanzamos defendiendo lo que somos
El fondo sur tiene que explotar!

Los partidos de Anoeta, los viajes realizados
las reivindicaciones en los derbys vascos
Las carreras de Satrus, los goles de Agirretxe
desde el agujero de la Segunda División a las estrellas de Europa

Los colores blanquiazules, llevados con orgullo

El triunfo es nuestro si vamos de la mano
Ven con nosotros a dar ese empuje
Avanzamos defendiendo lo que somos
El fondo sur tiene que explotar!


Letra(Ingelesez):
I remember the first time when I got in by my fathers hand
a sandwich in the hands, a scarf round the neck
a child's dream in the smile of his face

Take a look at the south stand
symbols, flags, shouts and noise
Time goes forward, you gather with your friends
and the south stand is now our home

White and blue colours carried with pride

The victory is ours if we go together
come with us and give the push
we go forward, defending our being
the south stand must explode!

The matches in Anoeta, the aways we've done
the claims in the Basque derbies
Satrus sprints, Agirretxe's goals
From the hole of the second division, to the European stars

White and blue colours carried with pride

The victory is ours if we go together
come with us and give the push
we go forward, defending our being
the south stand must explode!