---
id: tx-3248
izenburua: Galdetzen -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZFA3egGQbKI
---

Gauari nahi nioke
galdetu zergaitik ez dituen
ene penak egunaren
argia bezala aienatzen.
Izarrei nahi nieke
galdetu zergaitik ez duten
ene bihotza gauaren
iluna bezala alaitzen.
Haizeari nahi nioke
galdetu zergaitik maitearen
laztanak pago hosto igarrak
bezala ekartzen.
Euriari nahi nioke
galdetu zergaitik ez duen
ene egarria zelai lehorrarena
bezala asetzen.
Etsaiari nahi nioke
galdetu zergaitik ez duen
herriaren garraisia indarkeri
ankerraren ordez aditu nahi.

Etsaiari nahi nioke
galdetu zergaitik ez duen
herriaren garraisia indarkeri
ankerraren ordez aditu nahi.

Oh, oh, oh, oh, oh, oh!
Oh, oh, oh, oh, oh, oh, oh!
Oh, oh, oh, oh, oh, oh!
Oh, oh, oh, oh, oh,...
Oh, oh, oh, oh, oh, oh!
Oh, oh, oh, oh, oh, oh!
Oh, oh, oh,
oh, oh, oh, oh, oh, oh!
Oh, oh, oh, oh, oh,...
Oh, oh, oh, oh, oh, oh!