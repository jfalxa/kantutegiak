---
id: tx-2154
izenburua: Je T'Aime Moi Non Plus
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DDwVUlW4jRQ
---

Serge Gainsbourg euskaraz  "Gaztelupeko Hotsak"-ren eskutik. Euskeraz egindako bertsio hau Mikel Agirre (la Buena Vida)eta bere emazte Amaiak kantatzen dute.