---
id: tx-97
izenburua: Kultura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DNe0iBbcuaw
---

Historia, bakoitzarena, munduarena
Jaio gineneti berari lotuta
Ikusezina den lokarriari gaude saretuta
Ikusezina den lokarriari gaude saretuta

Askotan, ahaztu dugu gure sorterria,
Zein den zuhaitzaren erraia,
Adarrari begira galdu zaigu arima
Adarrari begira galdu zaigu arima

Basoen seme-alabak gara,
Hazietatik jaioak,
Zaindu dezagun zapaltzen dugun lurra,
bertatik loratzen da gure kultura