---
id: tx-2058
izenburua: Ikusi Arte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EH5SFKntO_8
---

Gelditu ginen eta ez zinen agertu
eta gero gainera ez zenidan deitu

Idatzi nizun eta ez zenuen erantzun
nik atera nahi nuen, zuk ordea sartu

Zure atea jo nuen, ez zenuen ireki
etxera itzuli nintzen larrosa batekin

Bainan berdin du jada nik ez zaitut maite
bila ezazu beste bat eta ikusi arte (x3)
ikusi arte (x4)