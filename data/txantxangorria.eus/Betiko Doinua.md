---
id: tx-1742
izenburua: Betiko Doinua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BL4tRKS7Kfc
---

Etorkizuna idazteko prest nago
ezabatuko al dau
zerbait patuaren nahiak ?  aske faltsua ez, ez naiz inor eroateko aurrera beldurrez
aukeratutako bidea alaitasunez
esaidazu zer egin ahal dan !

orain bada, abisatu nondik hartu egunsentia
bideratu nire itxaropen argia
kantaidazu berriz betiko doinua ta
bihotza ixildurik izan 

bihotza ez da lagun onenarik izan
bat-batekotasun hutsak
beti irabazten dio gure sen onari
bai, badira milaka arrazoi saiatzeko berriz
eta akatsetatik heldu iragan oihari
lagunduko didazu aurrera egiten ?

orain bada, abisatu nondik hartu egunsentia
bideratu nire itxaropen argia
kantaidazu berriz betiko doinua ta
bihotza ixildurik izan

iragana buruan lortu geroa, 
haizean hedatuz dator oraina
sentitu segundu guztiak aurrean, 
jare garela 

haizea aurreratu, arnas gabe arnastu
oraina gelditu joana doi bihurtuz
ta lau haizeetatik dastatu datozen
garai ezberdinak izan arren