---
id: tx-1121
izenburua: Kolore Kolore
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8-kGaQ0OEn4
---

Kolore, kolore ta gustu guziak
denontzat, denontzat ez dira berdinak.

Urdina...zuk nahi duzu urdina
nik maiteago horia
bien artean ferdea sortuko da
ezkerra, eskuina, gibel edo aintzina
bakoitzak bere bidea
berarentzat onena!

Kolore, kolore ta gustu guziak
denontzat, denontzat ez dira berdinak.

Amaia, Jonekin maitemindua
zoritxarrez Arantxa da
Jonen amodioa, ze lastima!
alboan beti dauzkat
bai Olga ta bai Olatz
eta nik maitatzen dut Amaia
mundua gaizki egina da!

Kolore, kolore ta gustu guziak
denontzat, denontzat ez dira berdinak.