---
id: tx-130
izenburua: Kontxita Eta Manolita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yqNhA6iTLjI
---

Kontxita eta Manolita, zien hartzak heben dira, Deliberatürik jinik beren espusen heztera, 
Bilaizi zitaie bertan, nahi zütiegü zurratü, 
Orano irus ez beiteiziegü nahi lephua llabürtü ! 
Ah la la ! emazte ixtoria, Kontxita eta Manolita ! 
Gaizo akher ükhatia, bahian pretenzionia, 
Bi andere gazte horien amodioz gogatzia ? 
Nahi izan dütük gogatü, gero indarka bortxatü, Aigü behar deitzat orai, nik, intzaurrak marruskatü ! 
Artzaiñ trixtiagorik ez da ihun ere bortü ondoan, Maxina tzar bat bera da, bethi neskatxa menguan, Ursula, kolpatü naiñ, arrazurik batere gabe, Andere horik dititzüt egün ikhusten lehentze ! 
Ursula, othoi ütz ezazü zure senhar inozenta, Kolpü horren egilia Pelo segürik ez beita, 
Gure senhar horiek, biek gütie zurratü, 
Eta sorthu bestimentan bortüko artzaiñer eskentü !