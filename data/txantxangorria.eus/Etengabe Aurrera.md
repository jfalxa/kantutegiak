---
id: tx-368
izenburua: Etengabe Aurrera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hgoqb43x8Ks
---

Kaleko Urdangak (Bardulia, Basque Country) 2021.

(EUS) "Etengabe aurrera" 2021eko azaroaren  27an argitaratuko den BIZIRIK disko berriko aurrerapen abestia da. Andoaingo Garate Studioetan grabatua, nahastua eta masterizatua Kaki Arkarazoren gidaritzapean. Tough Ain't Enough​ Recordsek banatua. Bideoa Peru Isasi eta Asier Kortabarriaren lana da.

(ENG) "Etengabe aurrera" song from the upcoming album BIZIRIK (27th november 2021). Recorded, mixed and mastered in Garate Studios (Andoain, Basque Country) under the supervision of Kaki Arkarazo. Distributed by Tough ain’t Enough Records. Video piece by Peru Isasi & Asier Kortabarria.

(ESP) "Etengabe aurrera" es una canción adelanto del nuevo album BIZIRIK (27 de noviembre 2021). Grabado, mezclado y masterizado en los Garate Studios de Andoain (Gipuzkoa, Euskal Herria) bajo la direccion de Kaki Arkarazo. Distribuido por Tough Ain`t Enough Records. El video es obra de Peru Isasi & Asier Kortabarria.

-Email: kalekourdangak@gmail.com

ETENGABE AURRERA

Hamar urte luze nahiko ta sobera dira
Kolpe batzuk hartuta
Zutitzen ikasteko

Errealitatea iluna da eta argitzeko
Lau akorde jota aterako dut
Indarra jarraitzeko oooh

Berriz altxatzeko 
Zulotik irtenda
Bizirik sentitzeko

Hey! Gure bidean zuzen
Azken hatsa eman arte
Kateak astindu guzti honen kontra
Haizea alde da probestu aukera
Etengabe aurrera

Gure ametsetan ez da ospe ez loririk
Beste gau batez taula zapaldu
Elkar akabatu barik

Etorkizuna ilun dator eta argitzeko
Herri puta hau geure moldean
Nolabait zerbitzatzeko oooh

Berriz altxatzeko…

—————————————————————-

10 years are more than enough
To take a few knocks
And to learn to raise back again

I'll play four chords 
To  light up this dark reality
and get the strength to carry on
To raise myself up again
To get out of this hole and feel alive

Hey! Straight on our way
Till we take our last breath
Shaking the chains against it all
With the tailwind at our back
Let's grab the chance
Onward without ceasing

In our dreams there's no fame or glory
Just to step on the stage one more night
Without killing each other
So that incoming dark future may brighten up
And in our own way serve this fucking town of ours

To rise up again
To climb out of the hole and feel alive

Hey! Straight on our way....