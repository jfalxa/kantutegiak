---
id: tx-2395
izenburua: Karan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xlJfbiMc3sE
---

Instagram -  intzaunanue


- K A R A N - 

Zenbat malko itzalen atzetik
Zenbat isilune eta hutsune
Zu gabeko egunek zulora naramate 
Zu gabe, zu gabe, zentzugabe. 

Beltzune bakarti batetik
Argitasun totilera
Egunsentiaren epeltasunean 
Nire ahots kordak, leuntzen. 

Baina nigana zatoz
Iluntasuna zapaltzen
Beso karan horiekin
Ni besarkatzen. 

Beltzune bakarti batetik
Argitasun totilera
Egunsentiaren epeltasunean 
Nire ahots kordak, leuntzen. 

Produkzioa: JULEN ALONSO ESTUDIOA
Irudiak: IKER GOZATEGI 

ENTZUN EZAZU - Spotify eta itunes plataformetan! 

Instagram -  intzaunanue