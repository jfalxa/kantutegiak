---
id: tx-1883
izenburua: Txori Ttikia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MUpVkM7wYH0
---

Txori ttikia nintzelarik
esan zidaten,
kaiolan bizitzeko
sortua nintzela, 
gero arrano bihurtuko nintzanean
kaiola hautsita aldegingo
nuen beldurrez,
libro nintzela sinistarazi
nahi zidaten,
horregatik iriki zizkidaten ateak
egin nezan hegaz,
bainan luze gabe ohartu nintzen
hanka harkaitz bati
lotu zidatela
kate motz eta astun batez.