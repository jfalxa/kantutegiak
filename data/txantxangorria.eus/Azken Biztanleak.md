---
id: tx-334
izenburua: Azken Biztanleak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/54f88Q9W8vg
---

AZKEN BIZTANLEAK (EH SUKARRA)
Sarraskikume maltzur orotan,
odoletan ernaltzen dijoa zauritan kiskaltzen dijoa saminetan hedatzen dijoa
seme alaben abizenetan markatutako susto gurutzea
etorkizunik gabeko lurpe ixil baten ikara eta oinazea
baldin bada   ametsak gorririk
baldin bada   gurean aberririk
baldin bada   lokarri sendorik baldin bada  
Grisaren teloi urratuetan historiaren marko minduetan
burdinaren hitz zorrotzetan gure itzaletan miseria arrastoetan
baldin bada arima baldin bada  
Epaituko gaituzten lurretan, ehortziko gaituzte oinutsik
Gure hezurdurek euritan ez dute marratuko lorratzik
Errekasto hutsen paretetan estutuko dituzte zintzurrak
Ebakigarriena dateke justizia ezaren linos maltzurra
ERREPIKA
Jada ez gara munduarenak eta mundua ez da gutarrena
Atlantidako umezurtzak, Atlantidako azken biztanleak gara!x4
baldin bada  ortzeaz harandik bestelako ozeano birjinik
Aizatu gaitzatela biluzik bestelako mendi gailur birjinik
Baldin bada  baso oihan birjinik
Baldin bada  Atlantidako azken biztanleak garax6