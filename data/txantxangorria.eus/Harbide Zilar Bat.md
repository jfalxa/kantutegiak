---
id: tx-685
izenburua: Harbide Zilar Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uSq1vfqz6Po
---

HARBIDE ZILAR BAT
Jose Luis Otamendi - Eñaut Elorrieta

Geltoki hits honetan
garraio gaia norbera denean, oraina ez al dago
geroa baino askoz urrunago?
Harago zer dago?
Albokoa ahaztuta menderik-mende; berrogeialdi luze batean gaude, lur akitua bada bizitzaren sari, galduz nahi dut irabazi.
Egutegi berria
sortuko dugu jende duinarentzat, ekaitz perfektuak,
elkar limurtzeko presazko kantuak, murmurak,
uluak.
Beldur zuriak hartu gaitu barrutik, ez da maitasuna beste sendagairik, nola aterako naiz neure baitatik ez bada zugan sarturik!

Zer dago geroaz harago? Harbide zilar bat borrokarako.
Leiho ertzetatik elkartasuna eske, berrogeialdi luze batean gaude, lur akitua bada bizitzaren sari, galduz nahi dut irabazi.
Zer dago geroaz harago? Harbide zilar bat borrokarako.
Izurri honek bihotza jango al dit?, ez dut maitasuna beste sendagairik, nola aterako naiz neure baitatik ez bada zugan sarturik!
Ez al dira, ekaitzak, uluak … aro berri baten zantzuak?
Zer dago geroaz harago? Borroka zilar bat gaur ekiteko. Gaur ekiteko!