---
id: tx-254
izenburua: Maketoen Iraultza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/84woHjh5qzo
---

Doctor Deseo taldearen "Maketoen Iraultza" abesti berriaren bideoklip ofiziala.


-------------------------------------------
Hitzak:

Iluntzero argia
hiltzen den lekutik,
paradisutik,
ihesean.

Lur aginduaren bila,
ez iragan, ez abizen.
Asko gelditu zen
bide-ertzean.

Aitaren etxea utzi genuen
desioaren kaleetan galtzeko,
berriz jaiotzeko.
Hemen dator…

Zuen lege zaharrari su emanez
espaloian egingo dugun dantza,
maketoen iraultza
hemen dago.

Odolak baditu milaka mintzo,
eta datorrena,
ez da erreza izango…
Goazen bada
gurea den
horren bila…!
Festa has dadila!

Arazoa eta konponbidea
izan ginen eta izango gara,
geroa orain da.
Hemen dator…

Zuen lege zaharrari su emanez
espaloian egingo dugun dantza,
maketoen iraultza
hemen dago.