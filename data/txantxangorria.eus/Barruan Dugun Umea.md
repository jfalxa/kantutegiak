---
id: tx-2579
izenburua: Barruan Dugun Umea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jE_dX2LQOCM
---

Lo hadi txutxurunbele
Dion sehaska kantua
Lokartzekotan nengonean
Gurasoek abestua
Gertutik ahots goxoaz
Laztan eztiz lagundua
Gau hoietako iluntasuna
Argitzen zuen doinua.

Ondoren pintxo txakurra
Izan genuen laguna
Lehen aldiz begi bat isten
Maisua izan genuna
Txuri ta beltza kolorez
Koskik egiten ez duna
Orain koskak non nahi ditugu
Nor ote da erruduna.

Uztaila gora ta gora
Doa lurretik zerura
Huraxe ondo mugitzea
Soilik genuen ardura
Uztail barruan preso
Egotearen gezurra
Salto batez aske izan nahi dut
Haur jokuetan modura.

Heldu izan nahi genuen
Nerabezaro desira
Zahar guztiei eroi modura
Egoten ginen begira
Orain aldiz begiratuz
Umeek duten argira
Sarritan itzuli nahi dugu
Dena eder zen aldira.

Haurtzaroko doinuekin
Iragana berpiztea
Argazkian ikusten dugun
Haurtxo hori oroitzea
Beti heldu izatea
Ez denez inon legea
Mesedez etzazu preso utzi
Barruan dugun umea