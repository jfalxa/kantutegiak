---
id: tx-1881
izenburua: Urtxintxak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CR5h7E1H4rc
---

Urtxintxak oihanian jauzika, 
Aldaxkatik aldaxkarat ... 
Zuk erradazu, ama, zendako 
Ez diren erortzen lurrerat ?

Nik ere nahi nuke 
Izan urtxintxa bezala! 
Hain gora, haín trebe, 
Izaíteko libre !

Urtxintxak oihanian zintzilik, 
Buruz behera buztanetik 
Zuk erradazu, ama, zendako 
Ez duten buruko minik ?
Nik ere... 

Urtxintxak oihanian karruzkaz, 
Neguko bildu heltzaurrekin 
&#145;Zuk erradazu, ama, zendako 
Ez dezaketan egín berdin?
Nik ere