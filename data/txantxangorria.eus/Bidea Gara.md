---
id: tx-2083
izenburua: Bidea Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Lx4CxnTANco
---

Eguna ez bada amaitu
borroka esna liteke
inork ez gaitu itxoiten
inongo helmugan tente.
Bidea ez bada asmatu
ez badakigu zein den
has gaitezen ibiltzen
gure indarrak eskaintzen.

Ez gara inora heldu
ez gara inondik irten
eta ez gara inora helduko
bidea ez bada egiten.

Pausu bat, ondoren hurrena
garaipen bat eta gero bestea
geroa aukeratzerik bada
borrokatu dezagun gurea.

Denok gara herri onen bidea
borroka da bakearen jabea
denok gara borrokaren bidean
bakea da borrokaren xedea.

Ez entzun eta ez hitz egin
norbaiten gura bada
izan bedi gurea
herri honen taupada.