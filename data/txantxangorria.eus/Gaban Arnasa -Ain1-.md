---
id: tx-3206
izenburua: Gaban Arnasa -Ain1-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z75lkEiZOII
---

Luak zela hartzen zaittuen zain dazen,
 logelako sapaixen 
bizi dien ontzak moruen nau ni, 
zuri begira... 
bihotz taupaden taupara-taupa 
hau geldotzen sentitxuz, 
estaltzen zaitxuen azala 
eskuekin bixer arte agurtuz, 
eguneko fan etorrixen 
zorabixua ametsen ataixan ahaztuz. 
Giro dauelako gaban 
arnasa leihoko zirrikitxuetatik 
sartzen isteko, giro...

Kanpoko udazkenan 
olatuen ufadia entzunez 
mingainaz 
zure ezpanak bustiz nabil 
ni zu hezetzen. 
Kanpoko haixiaz 
izerak altzauaz dantzan, 
hiriko marmarran 
arroztasunien galduz, 
babesten gaitxuen 
kristalan alde hontan. 
Izera arteko aitortzen 
epeltasunien gertu 
sapaiko ontzei ondolein deizuela esanez... 
Giro dauelako...