---
id: tx-527
izenburua: Zure Kebapa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zzcIdG1A9OQ
---

Ez naiz zure kebapa
Baina parranda ostean
Nirekin etorriko haiz

Nirekin etorriko haiz
Zorroan gordetako kondoien esperantza
Nirekin etorriko haiz
Nirekik etorriko haiz

Ez naiz zure kebapa
Baina parranda ostean
Nirekin etorriko haiz
Nirekin etorriko haiz
Zorroan gordetako kondoien esperantza
Nirekin etorriko haiz
Nirekik etorriko haiz

Zenbat balio du zure outfitak?
Belarjale eta belarrijaleak gure auzoan
Fuck elizak fuck eneko aritza
Gehiegi ez pentsa ta gordeidak patxana

Zenbat balio du txupitoa garito hontan?
Mantis bat bezala noa egiteko txortan
Non gorde dut boltsa? non galdu dut otsa?
Non galdu dut lotsa? seguraski Rotxan?

Ez dakit ze koxka, ez dakit ze ostia
Nola bueltatuko naiz etxera?
Lo egiteko indarra falta zait

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin

Lololololo

Izerdia ene perfumea palikea ene hitza
Bi begi garbi ikusita eskatu nion piti bat
Tipa ezagutzen nuen baino soilik bistaz
Ongi jokatu nuen ta zu zinen saria

Goazen, goazen beste nunbaitera
Ezer, ezertaz oroitzera

Zu nitaz zale, ni zure satysfayer
Ze ostia egingo nuke nik zu gabe?
A ze talde Chill Mafia ta Margaret
Rolloa diat itsusia naizen arren

Dirurik ez dut karteran
Eta dirua beharrean
Estiloa daukat soberan
Zure arrebak hitz egin beza

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin

Tarantinoren gorra Zuma jainkoarena
Batzokiko surfero baten ametsa
Zelako txuria zeraman
Zelako txuria zeraman

Dönerraren saltsa xuria emaidak
Xurgaindan dena dena dena
Dena dena dena dena
Txibatoak dabiltz Ezpala tabernan

Haiekin bertsotan, ahotik txortan
Bertan txortan bertan daflipank
Bertan txortan bertan daflipank

Kaixo maitia, gordezak behingoz zakila
Sexua ez da sexya baldin bada Windows Vistan

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin

Dautsagu kotxean
Baina nik ez dut kotxerik
Etxera berriz lotan
Kebapera buelta gosekin