---
id: tx-1491
izenburua: Beldurrik Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QuvIg-7SXIQ
---

Edalontzia noraino
dago beteta,
nire ESKU EZ dagoen
zozketa bat da.
Soka gaineko
ibilaldi honetan,
begiratzen dut
biziki etorkizuna.
Kanpoko eraginez
jositako bidean,
errenkan nabil zalantzaz
nora ezean.
Noizko geratu ziren
nire ametsak
besteek esango dutenaren
esperoan?
Behin eta berriz,
ausartu eta ezinik,
LOTSATUTA osorik,
ez dut jarraitzen aurrera.
Margoturik
beldurrezko munduan,
sentitu naiz
bizitza osoan...
Oraingo honetan!
Nire jolasa da!
Eta nire ESKU arauak!
Aske bizitzea!
Erabakia da!
Atzo, gaur eta bihar!
Han geratu dira!
OZTOPO guztiak!
Kaiola erraldoietan!
Atera irriak!
Atera gogoak!
Bizi gaitezen muga-mugan!
So egin dut nire
barneko putzuan,
ikaratu nau
bat-batean sakonerak.
KONTURATUZ UREZ NIK
BETE NUELA
zergaitik ez NAIZ JOAN
HAN BAINU bat hartzera?
Behin eta berriz,
ausartu eta ezinik,
LOTSATUTA osorik,
ez dut jarraitzen aurrera.
Margoturik
beldurrezko munduan,
sentitu naiz
bizitza osoan...
Oraingo honetan!
Nire jolasa da!
Eta nire ESKU arauak!
Aske bizitzea!
Erabakia da!
Atzo, gaur eta bihar!
Han geratu dira!
OZTOPO guztiak!
Kaiola erraldoietan!
Atera irriak!
Atera gogoak!
Bizi gaitezen muga-mugan!
Eta ez,
ez dago irabazterik.
Eta ez,
arriskatu gabe.
Eta ez,
ez dago altxatzerik.
Eta ez,
lurrera JAUSI gabe.
Eta ez,
ez dago irabazterik.
Eta ez,
arriskatu gabe.
Eta ez,
ez dago altxatzerik
lurrera JAUSI gabe!
Oraingo honetan!
Nire jolasa da!
Eta nire ESKU arauak!
Aske bizitzea!
Erabakia da!
Atzo, gaur eta bihar!
Han geratu dira!
OZTOPO guztiak!
Kaiola erraldoietan!
Atera irriak!
Atera gogoak!
Bizi gaitezen muga-mugan!