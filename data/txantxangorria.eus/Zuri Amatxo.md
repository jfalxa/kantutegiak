---
id: tx-1130
izenburua: Zuri Amatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y8aAx8iTupI
---

Handia izanen naizenean, ama
eskailera bat eginen dut
zeruraino bezain gorakoa
izarrak biltzera joateko.

Poltsikoak izarrez eta
kometaz beterik jaitsiko naiz
eskolako lagunen artean
banan-banan banatzera.

Baina zuri, amatxo zuri
ilargi betea ekarriko dizut,
argindarrik gastatu gabe
etxea argi dezazun.