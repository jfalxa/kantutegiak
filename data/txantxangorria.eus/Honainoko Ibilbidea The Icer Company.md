---
id: tx-1630
izenburua: Honainoko Ibilbidea The Icer Company
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9tXSQFEhw-c
---

Ke usaia arropan
lurrun goxo bat airean
barnean eta azalan

Ez dut esnatu nahi
honela geratuko nintzake
zeru honetan bizitzen

Hemen ilunetan 
mamuak entzuten dira kalean
eta oihuak nire barnean

Infinitoan 
beldurra ematen du esnatzea 
begiak irekitzea

Nora joan gara?
Non galdu ginen?

Oraindik batzutan
baina geroz eta gutxiago
pentsatzen dut hegan egiten

Baina zapatak garbitu behar
hondar gehiegi pilatu dituzte
honainoko ibilbidean

"Geroa ez da existitzen eta damua da azkenean geratuko den gauza bakarra"