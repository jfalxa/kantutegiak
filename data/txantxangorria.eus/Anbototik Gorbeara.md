---
id: tx-1762
izenburua: Anbototik Gorbeara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WpKXniYQnZw
---

Anbototik Gorbeara
Ortzian arku bat bada

Estaietan edertzeko
Bizkaia eta Araba.

Jaizkibel eta Aizkorri
Ernion gora Oskorri

Haitzean isladatzen den
Gipuzkoa eder hori.

Aralartik Iratira
izarrak oihandu dira

ilargia pasatzen da
Nafarroari begira.

Ordeka eder Lapurdi
Zuberoan bortu haundi

gorago ta lerdenago
Orhi eta Auñamendi.

Adurretikan Ebrora
Itsasoa ta lehorra

Herri batek kantatzen du
etorkizun hilezkorra.