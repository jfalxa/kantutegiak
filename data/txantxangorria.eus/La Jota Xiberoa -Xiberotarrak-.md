---
id: tx-3205
izenburua: La Jota Xiberoa -Xiberotarrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wP0qde_DdLE
---

Mauletik Atharatzera 
entzunda gure botza 
ohuz kanta dezagun
la? jota xiberuan 
be(r)otuse(r)ik onduan zainak 
ethenik lepuan 
hola gira gozatzen 
usaintxa xaharretan. 
aupa Faiko ta Josefina 
Nafarroa nagusia 
gu b(er) denboran ariko gira 
Xiberoko jotarekilan