---
id: tx-532
izenburua: Bide Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Sm7DVUY9ju0
---

Zutitu eta oinez
hasi baino lehenago
katamar hasi ginen
mundu hau arakatzen

Eta kolpe bakoitza
hartu dugun neurrian
ikasitako dena
gorde dugu barnean

Aukera bakarra norberak
egiten du bere bidea
ez dago besterik
aurrera jo beharrean gara beti
helmugarik ote da?
zein da gure azken helburua?
makina bat galdera
jakin nahiko nuke nik erantzuna

Eta urteak badoaz
gu konturatu gabe
bizitako uneak
garena sortu dute

Denbora dugu lagun
ibilbide honetan
berak argitzen digu
nor den otso nor ardia