---
id: tx-476
izenburua: Maiteari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zT9hZY56Xi8
---

Euskaldun baratzean
jaio zinan lore,
garbi eta usaintsu
larrosa dotore.
Xoriak kanta dizu
hamaika amore,
besarkatutzen zaitu
haize epelak ere.
Zu zaitut katiguan
lotzen nauan sare.

Zu zaitut bihotzaren
erdiko atala,
jakin ezazu gogoz
maite zaitudala,
zure taupaden hotsa
nik daroadala,
eta nere bizia
zugan dagoala,
biok bat izateko
jaioak gerala.

Ez gara maite behar
geuk biok bakarrik,
egoismoai inoiz
ez emon indarrik,
ez dugu ahaztu behar
herriko negarrik,
ez ta ez-entzun egin
anaien didarrik,
gugan dugan artean
maitasun sugarrik.