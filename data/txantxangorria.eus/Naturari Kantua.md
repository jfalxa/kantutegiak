---
id: tx-2237
izenburua: Naturari Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qVCVvmh29bI
---

Nik Natura maite bait dut
bera osoa, bera osoa

Akaberaren kontra bait nago
bizitzaren alde bai oro.

Ehiztariak gizon hutsak baitira
berdin zaie amorio ta herio.

Gure munduan justizia eskatzen dute
eta mendietan dira hiltzaile.

Mendietan nagusitza ez erabil,
abereak hiltzeko.

Abereak Naturaren jabeak dira
armonia zaindu behar dugu.

Ehiztariak gizon hutsak baitira
berdin zaie amorio ta herio.

Akaberaren kontra bait nago
bizitzaren alde bai oro.

Natura errespetatu behar dugu
gi/zon zu/ze/nak na/hi ba/du/gu i/zan.

E/ta ho/lan bi/zi/ko bait ga/ra as/ke
bi/zi/tza/ri la/gun/tzen.

Men/di/e/tan na/gu/si/tza ez e/ra/bil,
a/be/re/ak hil/tze/ko.

E/ta ho/lan bi/zi/ko bait ga/ra as/ke
bi/zi/tza/ri la/gun/tzen.

Nik Na/tu/ra mai/te bait dut
be/ra o/so/a, be/ra o/so/a

A/ka/be/ra/ren kon/tra bait na/go
bi/zi/tza/ren al/de bai o/ro.