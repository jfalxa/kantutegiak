---
id: tx-1733
izenburua: Zubiaren Erdian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/35cr23LEinQ
---

Jainko txiki eta jostalari hura, Kirmen Uribe, Mikel Urdangarin, Rafa Rueda, Bingen Mendizabal

Artibai gaineko zubian,
besoak baranda etzanda.

Lertxuna, olezko ontzi
zaharraren hezurduran.

Gerraostean utzi zuten
usteltzen bertan, zigor gisa.

Urgaineko hostoak
ibaian barrena doaz.

Marea gora den seinale.
Hori dut une gudtukoena.

Uste baitut urak ez doazela
beti norabide berean.