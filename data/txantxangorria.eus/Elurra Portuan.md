---
id: tx-64
izenburua: Elurra Portuan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GCw_1oIoxmk
---

Argazkiak: Martin Ugalde

Geure itxaso maitia,  
gura izan dabe asfaltau 
…bizikletan txilipitxerutan jun behar,  kotzez lebatz bila  
sardina freskuak kamioietan enbarka,  oinez tximinoitan  
Itxasoa baltzaz jantzi da,  
lamien antzera,  
Izan zaitez mundaka marrau galanta  Atorra jantzi elurragaz  
Zurituz txapapotia portuan aratustetan(2) 
…Bizikletan...