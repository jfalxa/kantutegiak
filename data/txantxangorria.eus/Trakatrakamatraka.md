---
id: tx-619
izenburua: Trakatrakamatraka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A7S6JiWppOM
---

Traka, traka, trakamatraka
Traka, traka, trakamatraka
Traka, traka, trakamatraka
Bihotza taupadaka nire soinua bultzaka
Traka, traka, trakamatraka
Traka, traka, trakamatraka
Traka, traka, trakamatraka
Bihotza taupadaka nire soinua bultzaka


Dena doa presaka, nire burua biraka
Lainoak nire zeruan,
gorputza dardarka
Hauxe nire marka,
azken guda dantza
Dantzak bihotzean,
Haizu, egizu dantza!
Nik zure zalantza,
asmakizunen tranpa
Kanta berrien Andra Mari emanoen aurka
Txakurrak zaunka, txerriak oinka
Kalean borrokarako prest gure erronka

Non da? Zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Non da? Zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Ahotsak argi, ideiak garbi
Gure ekimenak bidean ozenki jalgiz
Letrak gure tresna, zuk zure jarrera
Egunerokotasunean
zenbat zuk zer?
Bertan ugari, akzio eskas,
kezkaz beteta egongela berotan
zure aldare ederra
tabernaz  taberna
Dilemaz dilema noizbait kalera

Non da? Zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Non da? Zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Mahai inguruko hizlari sutsuak gehienak
Besteen kontzientza epaitzen ausartzen direnak
Bakoitzak bere borrokaren jaun jabea
Nik nirea, nik gurea, zuk, zuk zeinena
Non da? Zein da benetan betetzen zaizun erronkan izena?
Non da? Zein da besteen zentzurara eramaten zaituena?
Lehenengo praktika, gero berriketa

Non da Bil/bo? Euskal Herria da
Zein da sentitzen duzun mugimendua?
Hemen edo han, hor lan e/ta lan
Bakoitzak bere platerean jan eta jan

Dantzan, tan, tantaka xiri-miria
Pin, pun, pan, gora gazteria!
Kolpez  kolpe egiten da bidea
Trakamatraka eutsiz gure askatasuna

Non da? zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Non da? zein da? Nik zer? Nor? Zu
Herriaren erantzuna badakigu
Non da? Zein da? Nik zer? Nor? Zu
Trakamatraka izerdian daramagu

Dantzan, dantzan
din, dan, dan, dan

Non da? Zein da? Nik zer? Nor? Zu
Nor? Zu. Nor? Zu. Nor? Zu...

Traka, traka, traka/matraka
Traka, traka, traka/matraka
Traka, traka, traka/matraka
Bihotza taupadaka nire soinua bultzaka

Traka, traka, traka/matraka
Traka, traka, traka/matraka
Traka, traka, traka/matraka
Bihotza taupadaka nire soinua bultzaka