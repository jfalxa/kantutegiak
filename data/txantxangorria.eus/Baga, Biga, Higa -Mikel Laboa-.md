---
id: tx-3261
izenburua: Baga, Biga, Higa -Mikel Laboa-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9wfq2ez3x58
---

Baga, biga, higa,
laga, boga, sega,
Zai, zoi, bele,
harma, tiro, pun!
Xirristi-mirristi
gerrena plat,
Olio zopa
Kikili salda,
Urrup edan edo klik ...
ikimilikiliklik ...

(Hitzak: Herrikoia. Musika: Mikel Laboa)