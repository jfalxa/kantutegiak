---
id: tx-3132
izenburua: Gosetea Oskorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hcE7ZUbjUR8
---

Probatu zutenek behintzat,
ondo gogoan edukiko dute,
hemengo gerra ondoren eta
kanpoko gerra bitartean
izan zen eskasia,
eta probatu ez zutenek berriz,
hola hobe dute.
Daukat hizketako gaia
baina memori nagia,
halaxen ere somauko nuke
aldi bat egia;
dago asko familia
go/se/ak gal/du/an i/a,
Ze/ru/ko ai/ta e/man zai/gu/zu
ja/te/ko o/gi/a!
Andrea kara hasarrez,
gizona bajan indarrez,
lan askorekin e/zer gutxi ta
gobiernu txarrez;
umeak eske negarrez
ta emateko ezer ez,
mundu honetan ondo bizitzen
ez da oso errez!
Nolatan garen biziko?
Majo gaituzte heziko!
Jende erdia debilidadez
jartzen da tisiko;
lege hau nork du hautsiko,
ondo zein da ikusiko?
Makinatxo bat hondatu dira
gizaldi guziko.
Nolabait ere mantendu,
ahal duenak jaten du,
iraungo badu biziak zerbait
behar izaten du;
goikoak ezin konpondu,
beti bajua jipondu,
ez daukanari eman ez baina
daukanari kendu.
Langile asko nekean
ari da jornal merkean,
gauza garesti pentsatu nola
paga leitekean;
ezin zuzendu kolpean,
hemen gaude gerrapean
ogi zuririk ez da izango
hau den bitartean.