---
id: tx-2214
izenburua: Ongi Etorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kmb8ThSCoMw
---

Ongi etorria nahi dizuegu eskaini
ta lepoko zapia zuei ipini.

Eskerrik asko Gorka e/ta_ere Natxori
gogoan izan beti Plentzia Kantari