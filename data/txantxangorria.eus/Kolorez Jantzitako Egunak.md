---
id: tx-2256
izenburua: Kolorez Jantzitako Egunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TsFr6u-lrOg
---

Josu Bergara musikariak, Akorde Txikiak bandaren laguntzaz, "Kolorez jantzitako egunak" abestia konposatu du. Covid-19 birusaren eraginez, konfinamendu egoeran, musikari bakoitzak bere etxetik grabatutako bideoaren zatiak batu dituzte, elkarrekin abestia osatzeko.

Kantuaren letrak dioen moduen, batzuetan bizitzak alde ilunak erakutsi arren, modu positiboan aurre egin behar zaio eta une txarretan ere gure onena ematen ikasi behar dugu. Eutsi danok eta aurrera!

KOLOREZ JANTZITAKO EGUNAK

Batzutan bizitza
bapatean iluntzen da,
ustekabean datorren
galernaren antzera
erdian harrapatzen
zaituenean ekaitzak
bizitzarekin dantzan
ikasi behar da.

Kolorez jantzitako
kometa bezala
haizearekin dantzan.

Baina hala ere
den-dena pasatzen da,
ez dago formula magikorik
baina hala da
erdian harrapatzen
zaituenean ekaitzak
bizitzarekin dantzan
ikasi behar da.

Kolorez jantzitako
kometa bezala,
bizitzarekin dantzan.

______________________________________________________________________
Musikariak:

Musika eta hitzak: 
Josu Bergara

Bideoaren edizioa: 
Jabier Bergara

Audioaren edizioa:
David Sánchez Damián

Pantalla 2:

MUSIKARIAK
Nerea Alberdi
Mikel Nuñez
Irati Bilbao
Txema Arana
Dani González
Luï Young 
David Sánchez
Jabier Bergara