---
id: tx-406
izenburua: Aita Nuen Saltzaile
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kj81m1n4EeY
---

Aita nuen saltzaile,
Ama diruren artzaile,
neure anaia Bernardo
moru errira entregatzaile;
neure anaia Bernardo
moru errira entregatzaile.

II.- Saldu nenduen dirutan,
dirutan ere aunitzetan;
neunek pisaala urretan
eztitako bi kupeletan (1).

III.- Neure alaba Miarrez
¿zer dun horrela nigarrez?
ire yauntziak eginik tziauden
urregorriz eta zilarrez