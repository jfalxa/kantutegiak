---
id: tx-2438
izenburua: Agur Xiberoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rdGuLhpofdg
---

Sorlekhia ützirik gazte nintzalarik
Parisen sarthü nintzan korajez betherik
Plaseres gose eta bürian hartürik
Behar niala alagera bizi
Bostetan geroztik
Nigar egiten dit
Xiberua zuri

Agur Xiberua
Bazter güzietako xokhorik eijerrana
Agur sorlekhia
Zuri ditit ene ametsik goxuenak
Bihotzan ersitik
Bostetan elki deitadazüt hasperena
Zü ützi geroztik
Bizi niz trixterik
Abandonatürik
Ez beita herririk
Parisez besterik
Zü bezalakorik.

Palazio eijerretan gira alojatzen
Eta segür goratik aide freska hartzen
Gaiñ behera soginez beitzait üdüritzen
Horri gañen nizala agitzen
Bene ez dira heben
Bazterrak berdatzen
Txoriek khantatzen!

Agur Xiberua...

Ametsa, lagün neza ni Atharratzerat
Ene azken egüna han igaraitea
Orrhiko txoriaren khantüz behartzera
Pharka ditzan nik egin nigarrak
Hots, Xiberütarrak
Aintzinian gora
Üxkaldün bandera.

Agur Xiberua...