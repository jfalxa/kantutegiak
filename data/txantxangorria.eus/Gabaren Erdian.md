---
id: tx-1328
izenburua: Gabaren Erdian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/olg6u11wA58
---

Gabaren erdian Jesus jaio da
Alaiki ta bizkor goazen Belena
Iluna dago ez da agiri izarrik
Baina nik ez det argi beharrik
Gabaren erdian Jesus jaio da
Alaiki ta bizkor goazen Belena
Ile kizkurrak eta begi urdinak
Zerua baino hobeto egina
Zein ederra zauden, Jesus laztana
Begira daukazu lurbira dana
Gabaren erdian Jesus jaio da
Alaiki ta bizkor goazen Belena