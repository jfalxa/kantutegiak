---
id: tx-1203
izenburua: Ci Hè Dinu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HlMjqQmiQwc
---

Ci hè dinù tanti paesi strangeri
Fratelli di la briglia è l'umbasti
Chì traccianu l'idee è li cuntrasti
Sgracinendu cullane di penseri
Ci hè dinù tante razze malavviate
Surelle di l'albore culuritu
Chì piglianu la strada è lu partitu
Di ciò chì ci leia à le storie andate.

Ci sò tutti quessi è po ci sò eo
È lu so passu novu hè dinù meo.

Ci hè dinù tanti populi malati
Fratelli di la frebba è lu dulore
Chì cantanu li so guai affanati
Cù lu vechju suspiru di l'amore
Ci hè dinù tante forze liberate
Surelle di lu mondu chi s'avvia
Vive, è da tante mane purtate
Chì ci mettenu in core l'allegria.

Ci so tutti quessi è po ci sò eo
È lu so passu novu hè dinù meo.

Ci hè dinù tanti associ per furtuna
Fratelli di l'ochji aperti à lu sole
Indiati cum'è squatre di fole
D'ùn vulè più esse cechi à la funa
Ci hè dinù tante voci discitate
Surelle di a fede è di a salvezza
Chì portanu à l'avvene a so carezza
È valenu più chè le fucilate.

Ci so tutti quessi è po ci sò eo
È lu so passu novu hè dinù meo.