---
id: tx-1680
izenburua: Arrainak Eijer Begiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WF88gXC7AGs
---

Arrainak eijer begiak,
oi neskatila falsιa
Txoriño batek erran zereitan,
biga bostetan egia
Biga bostetan egia,
zü etzinela enιa. (bis)
Goiti eta beheiti,
banabilazü ni beti
Libertitzen ere nüzü,
ene gazte lagüneki
Ene gazte lagüneki,
nahiagorik zureki. (bis)
Arren zihauri emeki,
emeki eta segretki
Gai huntan nahi dereizüt,
nihaurek borta ideki
Borta nihaurek ideki,
libertitzeko zureki. (bis)
Erbia dua lasterrez,
ihizlarien beldürrez
Herri huntako neska gazteak,
hurtürik dirade nigarrez
Hurtürik dirade nigarrez,
ezkunt ez direla beldürrez.

Jean Mixel Bedaxagar musikoaren kantu polit bat, bere "Xiberoa" diskotik. 1983. urtean ekoiztu zuen lan hau honako lagunekin...

Garat Arhane, atabala
Gaztelu Etxegorri, txirula
Jaragoihen Jauri, ahotsa
Jean Mixel Bedaxagar, ahotsa

Zuberoako kulturaren eta ahots tradizio aberatsaren ordezkari aparta dugu Jean Mixel Bedaxagar. 1976an abestu zuen lehen aldiz jendaurrean eta 1983. urtean grabatu lehen diskoa. "Xiberoa" izenburuaz. Maiteen zituen eta bere ahotsara ondoen egokitzen ziren kantu zaharrez osatua,"Hartzaren kantoria", "Oi xarmagarria", "Arrainak eijer begia"... Hurrengo urtean, 1984. urtean, Euskal Herriko Soinu Tresnak disko kolektiboan hartu zuen parte, txirula eta tuntuna joz. Osasun arazoak zirela medio, jendaurrean kantatzeari utzi zion hainbat urtez.

Haatik, osasun arazoak zirela medio, kantugintza baztertua eduki zuen hainbat urtez. Zuberoako pastoraletan esperientzia handia du Bedaxagarrek. 1988. urtean Agustin Xaho euskaltzalearen bizitzan oinarritutako pastorala idatzi zuen, eta geroxeago "Ama" ikuskizuna osatu du Gaiarre eta Etxahun Iruri, Bedaxagarren maisua, gogora ekartzen dituen lana.