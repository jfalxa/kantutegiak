---
id: tx-2340
izenburua: Ez Da Ondo Aterako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sNBHk9QApNE
---

Bideoa:  #Mikrouhina

He venido a mentir otra vez,
Subir aquí a decir que estoy bien
y volver a empezar y volver
a tropezar hasta que aprenda a caer.
 
Eta beldur naiz ez ote naizen zure etxeko ateaz ahaztuko.
Ez da ondo aterako.
 
Saiatuko gara imajinatzen
oraindik izarak kendu baino lehen
ezezagunak ote garen,
ez daukaguna eduki nahi gabe.
 
Eta ez naiz inola ere zure etxeko ateaz ahaztuko.
Ez da ondo aterako.
 
Oraindik ez dugu guztia probatu,
lurretik altxatzeko egurra eta belak lotu.
Eskerrak ekaitzak haizea dakarren.
 
Ez da ondo aterako
Sutan dagoen etxe batean argazkiak salbatzea.
Ez da ondo aterako
Amildegitik erori eta besoak astintzea.
Ez da ondo aterako,
galduko naiz ozeanoren batean,
eta bale bat harrapatuko dut zuretzako.