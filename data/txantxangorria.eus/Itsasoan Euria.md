---
id: tx-1411
izenburua: Itsasoan Euria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T0SovgUHd90
---

Itsas bazterrean
eman zenidan hitza
apar gainean bitsa
uhin gailurrean.

Hodeitzan aingura,
itsasoan euria,
maite berba zuria,
promes eder hura.

Oihanean isilik
jausi den arbola.
Zorurik ez sabairik
ez daukan etxola.

Inork inoiz ikusi
ez duen ederra.
Inork entzun gabeko
kantuen alferra.

Malko bat hondarretan,
zulo bat airean.
Beleak elurretan,
eleak haizean.