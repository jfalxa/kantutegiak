---
id: tx-3338
izenburua: Pasodoble
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5fTWkkCdj-g
---

Bat, bi, bat, bi, hiru, lau!

Heldu gera esku soinuarekin, Hernanira eta denak begira
Hartu bada pazientziarekin, saltoa utzi, utzi kalejira

Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Soinujole!

Ordulari zaharrenak bezala, orduari denbora kendu nahirik
adi adi, dantzan segi dezala,baina zuk ez, zuk ez trikitilari

Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Soinujole!

Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Jo ezazu, jo ezazu pasodoble
Jo ezazu, eta lotsa hori gorde
Soinujole!