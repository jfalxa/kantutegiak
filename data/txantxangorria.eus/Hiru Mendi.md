---
id: tx-2587
izenburua: Hiru Mendi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gGDOtfm8ISg
---

Mendi txiki bat.
Mendi txiki bi.
Hiru mendi txiki.
Hiru hankarekin.

Mendi handi bat.
Mendi handi bi.
Hiru mendi handi.
Hiru hankarekin.

Ma, ma, ma.
mo, mo, mo.
mu, mu, mu.
me, mi.

Ma, ma, ma.
mo, mo, mo.
mu, mu, mu.
me, mi.