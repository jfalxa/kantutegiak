---
id: tx-2834
izenburua: Notableen Elizari Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fp5Vw1N5PH4
---

Notable jauna Elizan soinulari
Gostuan dela nola ez da ageri:
Eskuak zalu aireak jotzeari
Begiak erne horko jendetzeari
Kasu eginez plantan egoteari
Badaki ere behatzen apezari
Irri bat lotzen aurpegi izkinari
Gogoz asmatzen ehun bat debrukeri!

Kalonje jauna elizan mintzalari
Talendu gaitza ba omen du ugari:
Hitz hautatuak noiz nola bederari
Balaka goxoz eskuin aberatsari
Maltzurkeriaz langile herrestari
Ederkikara lotzen da frantsesari
Deituz kartsuki puxanten Ortziari
Su bota dezon fededun bastartari!

Zenatur jauna elizan lendakari
Ahantzi ez da noizbait zela kantari:
Aintzin-gibelka lasai ibiltzeari
Ez baitzaio zer ohitua denari
Ele xuri-beltz ahalaz xarmegarri
Askiko zaio gure populuari
Behin lotu-ta lausengutza lanari
Nola eskapa Frantses-Españolari!

Obispo jauna hil-harrien zaindari
Thema gogorrez eman zen mezulari:
Axola handi betiko indarrari
Aulki goratik mintzo da jendeari
Nigar intziri enterramendukari
Hantxetan hara doan lehengoari
Mehatxu frango ta hanbat azpikeri
Gure herritar burrukalarieri!

Bankierro jauna elizan eskelari
Laño-lañoki jartzen da otoitzari:
Eskuak bilduz luzean gorputzari
Begiak ñir-ñir goi bildutasunari
Estatu hortan hain baita hunkigarri
Ez da lotsati humiliatzeari
Bihotz barrutik eskatuz Jainkoari 
Dezaion eman ongi eginen sari! 

Jandarma jauna Elizan txixtulari 
Bere gradoko lagunen ordezkari: 
Sudurra luze krima senditzeari 
Bidez-bidexka Ordrearen beilari 
Nork egin duen zerbait tetelekeri 
Ez da baratzen artaz notatzeari 
Huxtu-lasterka hobendun ez denari 
Milika-sino hauzoko sosdunari! 
Makiñun jauna Elizan makilari 
Uko egin du belaunikatzeari: 
Urratsa zalu ibilki tratulari 
Merkatu plazan nolako elekari 
Izan daiteken gizon xuxenenari 
Errainak hautsi ez zaio urdekeri 
Haztamaztaka moltsa hanpatuari 
Oheratzen da dei eginez loari!