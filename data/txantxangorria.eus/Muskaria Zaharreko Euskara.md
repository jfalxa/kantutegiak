---
id: tx-2750
izenburua: Muskaria Zaharreko Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MgP5tJBKbAY
---

Muskaria zaharreko euskara
abardenetako hats lehena
aspaldiko erroa loratuz dijoa
muskaria zaharreko euskara
abardenetako hats lehena
lur gaziak antzutu ez duena
herri honen gogoa, Erribera!!

Erribera osoan
bere landetan zehar
entzun beharko litzateke euskara
ortzimuga den hartan
lingua nabarrorum izena baitauka
gure hizkuntza izan denaren isla
putzu sakonetik ateratzea da
mejana ureztatuko duten urak

Muskaria zaharreko euskara
abardenetako hats lehena
aspaldiko erroa loratuz dijoa
muskaria zaharreko euskara
abardenetako hats lehena
lur gaziak antzutu ez duena
herri honen gogoa, Erribera!!

Erriberan hasten da
euskara(re)n lurraldea
herri baten ateak izan diren
harezko harresia
naparren bilkura zein izan ote da?
Muskaria zaharreko euskara
putzu sakonetik ateratzea da
mejana ureztatuko duten urak

Muskaria zaharreko euskara
abardenetako hats lehena
aspaldiko erroa loratuz dijoa
muskaria zaharreko euskara
abardenetako hats lehena
lur gaziak antzutu ez duena
herri honen gogoa, Erribera!!