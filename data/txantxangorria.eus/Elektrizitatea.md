---
id: tx-1219
izenburua: Elektrizitatea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cwO_p3kcobs
---

Belar artean, bakardadean biluzik dantzan
oinak bustirik, lurraren erdian belarrarekin jolasean
pentsamenduak joan egin zaizkit egunarekin batera
Iluntasunak, hartu egin nau, negar malkoak

Gau ilun honen azken arnasketaren azkenengo
segunduetan nik ere oihukatu egingo dut
Munduak ahazten banau, lur honek askatzen banau
zerbait utzi beharko dut behintzat euri tanten artean
Ilargiak erakusten duen aurpegiaren aurrean
eskatzen diet oihukatuz izar galduei
nire negar malkoek ernal dezatela lurra
nire arimarekin, energia banaiz

Gau ilun honen azken arnasketaren azkenengo
segunduetan nik ere oihukatu egingo dut
Munduak ahazten banau, lur honek askatzen banau
zerbait utzi beharko dut behintzat euri tanten artean

Badaezpada banoa, hegaka ez bada ez noa
nire gorputzak eskatzen dizkidan ate apurtuetara
Badaezpada banoa, hegaka ez bada ez noa
zeruak eskaintzen didan amildegira

Badaezpada banoa, hegaka ez bada ez noa
ispiluan ikusten dudan elektrizitate lur-jausira
Energiak lur-jausira narama, nie neu ere energia banaiz
lurrean betirako idatzirik geldituko naiz
Banoa!

Elektrizitatea banaiz, elektrizitatean banaiz
Lurrean idatzirik geldituko naiz

Gau ilun honen azken arnasketaren azkenengo
segunduetan nik ere oihukatu egingo dut
Munduak ahazten banau, lur honek askatzen banau
zerbait utzi beharko dut behintzat euri tanten artean
Ilargiak erakusten duen aurpegiaren aurrean
eskatzen diet oihukatuz izar galduei
nire negar malkoek ernal dezatela lurra
nire arimarekin, elektrizitatea banaiz