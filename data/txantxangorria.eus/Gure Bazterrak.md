---
id: tx-636
izenburua: Gure Bazterrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hIBkWk6FCCY
---

Maite ditut
maite
geure bazterrak
lanbroak
izkutatzen dizkidanean
zer izkutatzen duen
ez didanean ikusten
uzten
orduan hasten bainaiz
izkutukoa...
nere barruan bizten diren
bazter miresgarriak
ikusten.