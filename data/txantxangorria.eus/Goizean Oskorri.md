---
id: tx-456
izenburua: Goizean Oskorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nkvHBkqHDc4
---

Asotsa egin duzu hain txikia izanik
Dena aldatu nahian
Erreferentea izan zinen
Eta orain zer zara?
Inork ez dezala esan
Saiatu ez zinela
Askoz gehiago mereziz
Berriz abiapuntuan (Abiapuntuan)

Zu zara nire etxea
Baina baduzu jabea
Ni baita ere triste nabil
Zutaz oroitzean
Ihes egitea zilegi denez
Ihes egingo dugu
Ni eraiki nauen bazterrei
Mesedea bueltatuz (Mesеdea bueltatuz)

Kaleak ilunpеan daude
Ta ez dago inor kalean
Zerk ematen dizu indarra
Horrekin joan beharko aurrera