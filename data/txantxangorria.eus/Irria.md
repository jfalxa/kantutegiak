---
id: tx-1779
izenburua: Irria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4Sba8QP4cl0
---

Eserita egoten DIREN 
Buda gorrien irrien 
Lasaitasuna bilatzen 
Saiatzen naiz 
belar Ebaki berriak 
ematen duen Freskura 
birikietan tatuatzen 
saiatzen naiz 
Arren eskatzen dizut, 
lagundu nazazu 
Grabitate ezan dantzatzen ari naiz 
Arren eskatzen dizut, 
lagundu nazazu 
Itsasoan
 Hegan egiten ari naiz 
Ta beharrak asetuta 
Buda gorrien irria 
Dena bilakatu da 
Nik neuk ALDATU dut burua 
Sartu dena Atera da 
Zatoz nirekin batera, 
Aurkitu dut Behar 
Dudan Lasaitasuna 
Zeru urdinean murgiltzen ari naiz 
Eguzki berotan bustitzen ari naiz 
Euritan azala zikatzen ari naiz