---
id: tx-513
izenburua: Aurkitu Zaitut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IRnFl6QcYm8
---

Ez dut ahaztu izan ginena, zeruko izarrak kontatzen
iIunabarrari begira, itsasoari abesten
Ondo gordea dut memorian, ikasi genu(e)n erortzen
Munduaren beste aldean egunak argitu zuen

Bilatu zaitut aspaldi umetan gordetzen ginen tokian,
aurkitu zaitut abestietan, ametsik ederrenetan

Batu dira gure bideak hainbeste urteren ondoren
Heldutu egin gara baina zu berdin-berdin zaude
Inongo helmugarik ez duen trenera igo gaitezen
Altxa ditzagun gure kopak, badakizu zer datorren

Bilatu zaitut aspaldi umetan gordetzen ginen tokian,
aurkitu zaitut abestietan, ametsik ederrenetan

Ta egindako bideak, gau amaigabeak elkartu gintuen
Ta egindako bideak, biharko haizeak, nora garamate?

Bilatu zaitut aspaldi umetan gordetzen ginen tokian,
aurkitu zaitut abestietan, ametsik ederrenetan (x2)

🚀Jarraitu EMON sarean!

Abestia: EMON
Grabaketa eta nahasketa: Axular Arizmendi (AME Estudioak)
Masterizazioa: Basalte Studio
Bideoklipa: Omar Grana, Ekhi Mancisidor, Manex Moreno eta Mikel Ullibarriarana

Esker bereziak Bergarako itzulera kontzertu berezira gerturatu zineten guztiei!