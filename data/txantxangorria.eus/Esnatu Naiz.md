---
id: tx-943
izenburua: Esnatu Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fm5klu-uCEo
---

Momentu etengabian
Karakolak bere tokian 
Zigarroren hautsa mahai gainian
Ta begirada galdekorren bat
Kaletan maiz, ohi infinituan nago
Ametsetan pantaila beltza ikusteko
Eskua tinko bizitzari eusteko 
Sufrimendua zorionan bihurtzeko

Momentu etengabian
Karakolak bere tokian 
Zigarroaren hautsa mahai gainian
Ta begirada galdekorren bat

Area oinetakoetan
Uradura(?) jasan gabe
Maite zaitut esatea, ezpainak ireki gabe
Leihoak zabaltzea baitangoan geratzeko
Noizbait mezu hiltzailea
Definitu ote zaren

Esnatu naiz
Konturatu naiz
Esnatu naiz
Zoriontsu naiz

Dakusat benetan
Zure hilezkortasunean
Galdeiozu hautsari
hi ginebraki estuari ( ?)
Gaurkoan John Fante
Erail usterak itsuari
Utzi dezagun
Karakolek negar egin dezaten