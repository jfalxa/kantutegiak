---
id: tx-3227
izenburua: Aiton Amonei -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MqY7Krr44QU
---

Amona tori
pa handihori
magalean eseri
ta has gaitezen kantari.

Amona tori
musutxu hori
ipuin bat konta niri
prest nauzu adi-adi.

Auskalo zenbat urte
jakinduriaz bete
baina ez dakizue ez gaur arte
zuek zenbat  nik zaituztedan maite