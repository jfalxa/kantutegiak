---
id: tx-2252
izenburua: Espetxeratua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MzYCeQGVUT8
---

Eguzki izpi batek esnatu nau
gelan isuritako argitasun tipiarekin.
Betazalak pisutsu zaizkit eta
neure pentsamenduetan
galtzen naiz ... noraezean...
Nahikoa da!!!
bada, alperrikako oihu ustelak...
hegaka doazenak.
Alperrik ari naizen
sentsazio ankerraren zama dut
24 ordutan, askatasuna?
Sekula lortuko ez dugun desioa
eta negar tantoak isurtzera daramarkiguna....
Espetxe honetan galduko naiz
nire pena eta guzti,
nire amets eta askatasun nahiez hornitua
Baina dena desagertuko da gerora,
ez dit axola jada, neure bizi bakarra
hemen iragan dut;gosez,negar zotin hutsez,
minutuak zembatzen, astinduak jasotzen...
nire ohiuak ordea ez ditu inortxok ere aditu:
-Ama, sentitzen dut horrelako uneak
pasarazi izana eta zeure alboan egon ezin
izana gehien behar ninduzunean... banoa ordea,
-Agur eta ondo izan
Aurkituko zaitut Euskal Herria,
topatuko zaitut...ikusi arte!