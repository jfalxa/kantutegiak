---
id: tx-2449
izenburua: Oraiko Mutil Zarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nSCxX-ZobOE
---

Aizkora itxean ta kirtena basoan,
Aizkora itxean ta kirtena basoan, 
eztaukazu harturik senarra besoan,
eztaukazu harturik senarra besoan.

                 ♪♪♪♪♪♪

Itxeak zein on duen gainean teilatu,
itxeak zein on duen gainean teilatu,
herrian behar ttugu senarrak bilatu,
herrian behar ttugu senarrak bilatu.

                 ♪♪♪♪♪♪

Oraiko mutil zaharrak zertako dirade?
Oraiko mutil zaharrak zertako dirade?
Sos bat izan-orduko tabernan dirade.
Sos bat izan-orduko tabernan dirade.

                 ♪♪♪♪♪♪

Oraiko mutil zaharrak apo-zangoekin
Oraiko mutil zaharrak apo-zangoekin
ezkonduko lirake balute norekin
ezkonduko lirake balute norekin.