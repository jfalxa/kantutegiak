---
id: tx-2382
izenburua: Larrean Amoroski
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0kKxhU7AYxg
---

Bideo egilea: Bideografik

Zuzenean Cazenave Txokolategian

© Kalapita productions

Musika: Manez (Thierry Biscary)
Letrak: Manez, Aurelia Arkotxa

Kobreak : Vianney Desplantes, Mixel Ducau, Bixente Etchegaray, Mintxo Garaikoetxea

Dantzariak - Dancers : Pantxo Etchegaray, Nelly Guilhemsans

LARREAN AMOROSKI
Lerro lerroka hegalka ta hirukitan 
udaberrian itzultzen dira lertsunak
Ur gazteluko eremuan heze tokitan
larrea guneka loratzen hasia da
Askako apotxaliak apotu eta
plazan agertuko zira lerden neskatxa
mandarina aire goxo lepo xokoan
mutil misteriotsua xoratu arte
Eskuz esku jondoni suaren gainetik 
jauzi erne bat eginez uda pasa da
laster batean zangogorrien urrina 
lurrinduko dute urtxoen hegaldiek
Zure begiek 
amoroski
naute
perekatzen
Zure ezpainek 
amoroski 
nautela
laztantzen 
Zuk 
amoroski 
maitatzen 
nauzularikan... 
nik gaueko inguma ubelak ahanzten ditut 

Beste letren itzulpenak: www.kalapita.com/manez