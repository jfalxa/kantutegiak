---
id: tx-2785
izenburua: Urxaphal Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TznAgjlslII
---

Urzaphal bat

Urzo aphal bat badugu herrian trixterik
nigarrez ari duzu kaiolan barnetik
bere lagun maitiaz beit izan utzirik
kontsola ezazie ziek adixkidik.

Oi ene izatia dolorez beterik
mundian ez ahal da ni bezain trixterik
ni bezala maitiak traditu dianik
abil amodioa urrun ene ganik.

Tunba bat nahi dit lurpian ezarri
ene gorputx trixtia gorde mundiari
ene ixter begiek egin dezan irri
haiek zira zu ere hain sarri.