---
id: tx-2133
izenburua: Oi Lur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TVhD6s03Ws0
---

Euskal kantagintza berriak eman duen musikaririk emankor eta beteranoenetakoa da, Euskal Herrian zein kanpoan miretsia eta laudatua. Oriotarraren abesti asko herri kantutegiko klasiko bihurtu dira; itzulerako bidea egin dute, herri kantutegira ere jo izan baitu Lertxundik, bereziki Zuberoakora. Horrez gain, Bretainia eta Irlandako doinuak izan ditu iturri nagusiak. Bere burua epikotasunaren abeslari gisa ezagutzera eman ondoren, azken hamarkadetan kutsu intimistagoa hartu du haren musikak.

Bederatzi anai-arrebetatik azkena da Benito Lertxundi (Orio, Gipuzkoa, 1942). Musika txikitatik ezagutu zuen etxean, ohitura baitzen familia giroan abestea; hala ere, ez zen musikaririk haien artean. Lertxundi gazteak marrazkirako zuen zaletasuna, eta, herriko eskolako ikasketak amaituta, frantziskotarrek gidatzen zuten Zarauzko Arte eta Ofizio Eskolan hasi zen. Bertan, buztina eta egurra lantzen ikasi zuen, eta tailugile gisa lehen lanak egin zituen.

Hemeretzi urterekin, erloju denda batean hasi zen lanean. Egun batean bertako jabeak, Martin Lizasok, laut bat utzi zion. Bere kabuz jotzen ikasi zuen. Musikarekiko zaletasuna piztuta, gitarra elektriko bat erosi zuen. Gustuko zituen abeslari estatubatuarren bertsioak euskaratu eta jotzen zituen; Elvis Presleyrenak, esaterako. La Voz de España egunkariak antolatzen zuen kantu txapelketa batean izena eman, eta bigarren gelditu zen. Horri esker, Kataluniatik bueltan zegoen Mikel Laboak horren berri izan zuen, eta harremanetan jarri zen Lertxundirekin. Euskal musikariekin zerbait egin nahi zuen. 

1965. urtean sortuko zen Ez Dok Amairu taldearen sortzaileetako bat izan zen. Bertan gauza asko ikasi zituela aitortu izan du. Oso gogoan du izandako lehen bilera, Donostiako Kursaaleko zinema zaharrean egina. "Niretzako surrealista izan zen bilera hura, atera nintzen inpresioarekin ez nuela ezer ulertu, ez nekiela zertan ari ginen. Baina zerbait bazen nigan, sena edo, tiratzen ninduena, horretan jarraitzeko esaten zidana. Bilera batzuk egin genituen, eta hortik sortu zen Ez Dok Amairu taldea. Niretzat berriro jaiotzea, bataioa izan zen. Ordurako kantatzen nuen, baina orduan hartu nuen kontzientzia. Beste nortasun bat, beste kantari bat sortu zen. Garai hartan oso hedatuta zegoen protesta kantutan hasi ginen". (1)

Urte haietan lehen disko txikiak argitaratu zituen. Lehen EPan (Cinsa/Edigsa, 1967), oraindik ere asko entzuten diren bi kantu ezagun zeuden ("Zenbat gera", "Loretxoa"). Eta horiekin batera, Lertxundik idatzitako "Egia" eta "Egun sentia". Urte berekoa da bigarren disko txikia (Cinsa/Edigsa, 1967); horretan Xabier Leterekin egindako bi kantu agertzen dira: "Bihar itxaropen" eta "Gazte sentimental". Horrez gain, Lertxundik konposatutako "Gure bide galduak".

1968an hirugarren eta laugarren disko txikiak agertuko ziren, oraingoan Herri Gogoa diskoetxeak argitaratuta. Horietan kantu tradizionalak ("Ama ezkondu", "Dugun edan") eta berak sortutakoak uztartu zituen ("Irripar bat", "Jabetasuna"). Halaber, bada Xabier Leteren beste kanta bat, "Itzak dira itzak". 

1971. urtean Ez Dok Amairuk Baga Biga Higa sentikaria estreinatu zuen, eta urte hartan bertan lehen disko luzea argitaratu zuen: Benito Lertxundi (Herri Gogoa, 1969). Aurretik ateratako single bilduma zen, gitarra hutsez abestua; maitasuna eta garaiko egoera soziala uztartzen zituen Lertxundi gazteak. Azken horien artean zegoen Gure bide galduak. Kantu batzuk berak sortutakoak ziren, baina baziren beste egile batzuk sortutako hitzak ere: Jose Anjel Irigarai, Xabier Lete, Anjel Lertxundi eta Gaztelu. Nabarmentzekoa da Abel Muniategik idatzitako "Zenbat gera", ikono bihurtuko zen abestia. Soilik herri kanta bat zegoen disko horretan, "Aita saldu nauzu".

Hurrengo urtean Ez Dok Amairu desegin egin zen, eta bertan aritutako hainbat artistak beste ikuskari bat estreinatu zuten: Zazpiribai. Batez ere Ipar Euskal Herrian eman zuten Baga Biga Higa-ren ildotik segitzen zuen ikuskaria; bi orduko emanaldia zen. Lertxundiz gain, honako artista hauek hartzen zuten parte: Ugutz Robles, Iñaki Urtizberea, Xabier Lete, Lourdes Iriondo, Patxika Erramuzpe, Peio Ospital, Pantxoa Carrere eta Manex Pagola.

Proiektu kolektiboetan ez ezik, segitzen zuen bere bakarkako lanarekin. Oro laño mee batek... bigarren diskoak (Artezi, 1974) heldutasun handiagoa erakusten zuen. Xabier Lizardiren hainbat abesti musikatu zituen, horien artean "Oi lur, oi lur". Baita bere ibilbidean garrantzi handia izango zituzten bi kanta ezagun ere: "Herri Behera" jota eta "Txori txikia" metaforikoa, JosAnton Artzeren hitzetan oinarritua.