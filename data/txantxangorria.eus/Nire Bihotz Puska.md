---
id: tx-924
izenburua: Nire Bihotz Puska
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F2k1aaelr04
---

Gaur ezagutu det
egiten dut balet
ikusi dut arina dantzan
izena du Belen
begiradaz Marlen
itxaro saio hau amaitzean.

Da oso maja
etxeko alhaja
bere ondoan nago ni goxo
oso alai nabil
inoiz halako abil
bere sareetan erdi gaixo.

Zu ere jaun
ni berriz zure etxaun
olerkaritza ikasten
nahiena Lin ton taun

Nik berriz Basajaun
saiatuko gera maitatzen.

Nere umeen ama
paseatzeko dama
bietarako balio dit
egingo diot dei
izango ditut sei
dozena erdia nahikoa izango da.

Hau egia balitz
Saiatuko banintz
Ilusio hauek betetzen
Zuk jarri zurea
Nik baita nerea
Bainan utzi saioa hau
Bukatzen