---
id: tx-1294
izenburua: Zintzilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yvSDtx-81SM
---

"Zintzilik" Olatz Salvadorren estudioko lehen lan luzeko aurrerapen kanta da, diskoari izena jartzen diona. Ikus entzunezko lana Islandia ekoiztetxeak eta Julen Idigorasek elkarlanean sortua da. Grabaketa: Eñaut Gaztañaga. Masterizazioa: Estanis Elorza. Musikariak: Julen Idigoras (bateria) Jagoba Salvador (baxua) Borja Anton (gitarra elektrikoa) Olatz Salvador (gitarra akustikoa eta ahotsa)


Amestea zer ote den
Galdetu zenidan zuk behin
Nik jakingo banu bezala

Inork askatu baino lehen
Sarri airean galtzen den
Burbuilu horietako bat da

Inork zapaldu ez dituen
Ostadar guzti haien
Kolore asko doaz hara
Eta ohartu baino lehen
Noraezean berriz ere
Beraiekin galtzen gara

Airean

Zintzilik

Amestea zer ote den
Ahaztu zitzaizun beste behin
Nik jakingo banu bezala
Eta inork azaldu gabe ere
Ezinbestean galdera horrek
Sarri galera bat dakar