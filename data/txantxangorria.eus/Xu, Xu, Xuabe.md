---
id: tx-1553
izenburua: Xu, Xu, Xuabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eL2CuGIWmYc
---

Oso ongi egiten duzulako
mugitu besoak olatuak bezala
xuabe, xuabe, xu, xu, xuabe
eh! Pirritx,(nahi duzun izena) hasi dantzatzen.