---
id: tx-3080
izenburua: Kalera Bittor Aiape
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/STAip-E2hng
---

Kriston martxa badago taberna honetan
oinak gelditu ezinean nabil eta
musika ozenki entzuten den artean
gau osoa pasako dut taberna honetan
Ta zuk non zaude ez zaitut ikusten
telebista aurrean burua makurtzen
apurtu telebista etor zaitez hona
etxean eserita ez da ezer lortzen eta

Atera kalera
ibili gustora
ez geratu etxean
murgildu zaitez gauean

Oraindik indarrak baditugu eta
ostikoka telebista
atera kalera, emaiezu kaña
hori ez dute batere gustoko eta!!

Kalean gauzak mugitzen ari dira
gendea guztiz potrotaraino bizi da
tabernan naiz kalean korrika edo dantzan
atera kalera emaiezu kaña