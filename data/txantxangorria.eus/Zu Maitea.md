---
id: tx-2681
izenburua: Zu Maitea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SUJGWBiiDwk
---

Goizean-goizo irten det kalera
jostaketan umeak egin deadar!
Azokara daramazte salgai
hegazti piñak, sagar gozoak
ta gozoenak
zuk, maitea.

Bazkari mugan, ordu usain/garrian,
ardo pitxarra errege mahai ertzean.

Babarrunak, txitxi gorria,
intxaur ezeak, ardi-mamia
ta nere alboan
zu, maitea.

Irri-parretan, arrats istentzean
mutil polita ugari erromerian.
Dantzan arin, oinak airean,
gorputza kirain, begi biziak
ta bizienak
zuk, maitea