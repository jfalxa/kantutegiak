---
id: tx-11
izenburua: Jagon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RFb7b-uB3C0
---

2023ko ekainaren 4ean Mungian (Bizkaia) ospatuko den Euskal Eskola Publikoaren 30. Jaiaren bideoklipa.

JAGON (STR)
Kantu zaharretan aipatu ez,
baina betidanik hemen gaude.
Hizkuntza batek irakatsi ziola,
herri bati nahigabe.

Urtez urte, zure begitara begira
egon arren, beste hamaika etorriko dira.
Ze argi dago oso harro zaudela,
denona izatearen aukeraz.

Hazten hasi naiz zure desiraz,
ta sortzen hasi naiz zure irriaz,
ta janzten hasi naiz zure ideiaz.
Elkar jagon, zuregatik ni naizelako.

UOOO, ELKAR JAGON!
UOOO, OZENAGO!
UOOO, ELKAR JAGON!
ZUREGATIK NI NAIZELAKO.

Eskutik helduta, asmoak batuta,
elkar elikatzearen ardura.

Nahikoa da guretzat, hau bada denentzat.
Bada leku bat amets egiteko, zuretzat.

UOOO, ELKAR JAGON!
UOOO, OZENAGO!
UOOO, ELKAR JAGON!
ZUREGATIK NI NAIZELAKO