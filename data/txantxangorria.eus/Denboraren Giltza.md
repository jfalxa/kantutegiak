---
id: tx-1536
izenburua: Denboraren Giltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T0F0I5KAMJ4
---

"Maskarak" (2015) diskako lehen singlearen bideoklipa.

Videoclip del primer single del album "Maskarak" (2015).

Music video of the first single from the album "Maskarak" (2015).

Hitzak / Letra / Lyrics:

DENBORAREN GILTZA: 

Himalaya mendizerratik, 
Boswanako oihanetatik, 
Brasilgo hondartzetatik, 
Usuhaiatik... 

Arabako Sorginetxetik, 
Orreagako pasabidetik, 
Urederra iturburutik, 
Talaimendi magaletik... 

Handik hastearen dema, mugitu behar hori, 
mugitu behar horren giltza 
gu danon hiztegietako berbak 
salgai jarri nahi dituztenen telefonoetan? 

Ta bertan egon bagara, 
leku horietan, 
nola ez gara ba egongo, 
(orain, etxean.) 

Ene denboraren giltza dimentsio berri horren 
jaun eta jabe direnek omen dute. 
Ta esandako leku horiek guztiak 
salgai jarri nahi dituzte telefonoetan? 

Ikastearen demaz, 
bidaiatzearenaz, 
harro eta eskertuak.