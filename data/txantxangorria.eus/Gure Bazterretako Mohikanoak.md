---
id: tx-2306
izenburua: Gure Bazterretako Mohikanoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MllTd0wV_8k
---

Maite ditut
maite
geure bazterrak
lanbroak
izkutatzen dizkidanean
zer izkutatzen duen
ez didanean ikusten
uzten
orduan hasten bainaiz
izkutukoa...
nere barruan bizten diren
bazter miresgarriak
ikusten.


 Letraren autorea:
Jose Antonio Artze