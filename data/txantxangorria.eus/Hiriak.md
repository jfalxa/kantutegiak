---
id: tx-1805
izenburua: Hiriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LY5pMN77Dss
---

Zeure gorputza da orain nire hiria
zeu zara orain nahi dudan aberria.
Bizitzeko ez zait horrenbeste ardura:
Paris, Tombuctu,
New York, Bombay, segura
Berlin, Katmandu,
Sidney, Addis Abeba, 
Aljer, Lisboa,
Buda, Pest, Kiev, Ottawa.

Baina orain zeu zara nire hiriburua,
aberri, hilobi, neure sorlekua.

Hiltzeko ere berdintsu zait herria:

Lome, Friburgo,
Quito, Tallin, Luanda,
Tashkent, Mutriku,
Shangai, Istanbul, Praia,
Praga, Kigali,
Bangkok, Amsterdam, Basora

Baina orain zeu zara nire hiriburua,
aberri, hilobi, neure sorlekua.