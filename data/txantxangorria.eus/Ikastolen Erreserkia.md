---
id: tx-1655
izenburua: Ikastolen Erreserkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4QMYylYFlxk
---

Plazara, euskara plazara! euskaldun irakasle eta ikasleok 

dantzara! plazara, dantzara! Geure herriaren geroa bait gara

Bai, arreba ta anai, guk ere herria izan nahi,

geure lurrak laiatuz eta irauliz udaberriko emaitzaren zai!

herri jakintza nahi dugu eta jakintza herriarentzat nahi!

Plazara, euskara plazara! euskaldun irakasle eta ikasleok 

dantzara! plazara, dantzara! Geure herriaren geroa bait gara

Ez, erderarik ez! Euskadin sortu ginanez

Denok Euskara bakar bat nahi dugu Bizi giro ta jakintza bidez

Mundu berri bat nahi dugu Herri bakoitzat beretik emanez

Plazara, euskara plazara! euskaldun irakasle eta ikasleok 

dantzara! plazara, dantzara! Geure herriaren geroa bait gara

Plazara, euskara plazara! euskaldun irakasle eta ikasleok 

dantzara! plazara, dantzara! Geure herriaren geroa bait gara