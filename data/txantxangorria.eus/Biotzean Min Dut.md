---
id: tx-2966
izenburua: Biotzean Min Dut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PAJ-NNLj0UI
---

Biotzean min dut, min etsia,
negar ixilla darion miña.
Amonatxo bat nun: gaur, enarak
etorri-garaiez aldegiña.

Eguzkirik ez dute
leiotan zapiak;
aur-yolasik ez dago
gaur zure kalean
odeiek lits urratu,
itxasoak orro.
Gizadi bat geldirik
gero bat batean:
Ots! Ots! Bizion oñok ... (bis)

Errezka bi gizonek
argizai oriak,
gorputza lau billobok,
atzetik andreak.
Ai ene lor arin au,
zein dudan larria!
Besoek ez lan arren,
biotzak nekeak ...

Mendea ia betea,
ta ain illaun, gorputz xar!
Bezak gogoa goien
i bezain ariña,
ta bezat, nik nerea,
dei nazatenean,
ezin-itzal-argiruntz
egatzeko diña!
Ots! Ots! Bizion oñok ... (bis)

Orain larogei urte
kale poz-gabeok
eguzkiak zitun ta
zure aur-yolasak
alaitzen. Gaur, arriak
ere baitira itzal,
nork lekuskez, ikusi,
urte urdin igesak!

Egun zarrak yoan ta
berriak etorri,
gaur poz biar oñaze,
noiz burni, noiz zillar,
zenbaitek ondamendi
baituten katea,
lore-sorta bekizu
goi-muñoak zear!
Ots! Ots! Bizion oñok ... (bis)

Bat-batean eten da
bizion ots ori.
Gorputza laga dugu
nunbait. Al-dakit nun!
Bultza naute barrura ...,
ñir-ñir aunitz argi ...
ordia antzo sar nazu,
begi-lauso ta urgun.

Amonatxo! Bakarrik
zagokun atean,
otz ta gogor, entzuten
zure azken-Meza!
Zerraldo-olen artetik
(negu gaua iduri)
ez al-datortzu, uluka,
aize erruki-eza?
Ots! Ots! Enegan, biotz! (bis)

Begiok ero ditut,
noranai doazkit,
dantza naasi bat dabilt
irudimenean.
Goruntz egik biotza!
otoika lasa adi!
Ixilla sor badadi
ire barrenean!

Oru bete luzea
igaro zan. Noizbait
berriro lor illauna
besoek ... Urrena,
muño baten gañean
uri ixil bat arki ...
Poz oñaze kateak
emen dik etena.
Ots! Ots! Bizion oñok ... (bis)

Ol bat yaso ta ikusi
dut musu ximela:
parre antxa bizirik
balirau bezela
Ots! Ots! Ler adi biotz! (bis)

Ordun ixar bat biztu
du nere gau beltzak.
Begioi malko ixil bat
dardarka darite.
Noizbait itxaro-kabi
biur dan biotzak:
Ots! Ots! Yausika biotz! (bis)

"Agur! (oiuka dio)
gorputz ximur maite!
Agur, amonatxo!
Egun Aundirarte! ..."
Ots! Ots! Bizion oñok ... (bis)

(J.M. Agirre "Lizardi" / A. Valverde)