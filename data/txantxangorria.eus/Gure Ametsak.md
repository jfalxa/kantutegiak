---
id: tx-3005
izenburua: Gure Ametsak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4-xZCVGRla4
---

Ondoezak han-hemen zurrupatzen bazaitu,
ezbeharra gorpuzten badoakizu,
bidegurutzean inoiz aurkitzen bazara
izan kontu, zure ondarea zeu zara.

Zure eskariek inongo erantzunik ez badute
behin ta berriz ukatzen bazaitue
ez kikildu, ez zaite ahaztu lasai zaude
erretzen bazaitue sugar bihurtu zaitezke.

Inork ez dizu eskurik luzatuko
inork ez ditu zure penak maite
inork ez dizu babesik emango.
bizi diozun hori egiozu aurre.

Bizi poza aldentzean denak urrun aurkitzean
bakarrean, abilduran sentitzean
hustatusuna inork beteko ez dizunean
ez kikildu, zeugan duzu norabidea.

Zeure adorez inoiz ateratzen bazara
jakingo duzu zer den ta nor den nor,
izan kontu, ez ezazu inoiz ahaztu
arrakastan ere ez zaitue barkatuko.

Ilun duzuna argi bihar dakizuke
itxaropena erne lekizuke
zeure ametsa egi bihur dakizuke
gaurko ondoeza biharko poza.