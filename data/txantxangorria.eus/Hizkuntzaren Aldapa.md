---
id: tx-550
izenburua: Hizkuntzaren Aldapa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qf_dOLBwW4k
---

Txistu elektrikoa,
burnizko txalaparta,
joarerik gabeko
ustezko zanpantzarra

Mozorroturik ez den
ihauteri tristea
surikan ez dagoen akelarrea

Antzekoa da ba
gure egoera
luze-luzea baita
bihotzetik ahorako bidea

kemenikan ordea
ez zaigu falta izan,
goranzko malden ondoren
aldapa ia beti behera izaten
baita

Errimarik ez duen 
Egañaren bertsoa
otso gaiztorik (ga)beko
umeentzako istorioa

Prebafrikaturiko
brik-eko sagardoa
ta ametsik gabeko
bakarti loa

Antzekoa da ba
gure egoera
luze-luzea baita
bihotzetik ahorako bidea
goranzko malden ondoren
ez zaigu falta izan
goranzko malden ondoren
aldapa ia beti behera

Antzekoa da ba
gure egoera
luze-luzea baita
bihotzetik ahorako bidea
goranzko malden ondoren
ez zaigu falta izan
goranzko malden ondoren
aldapa ia beti behera