---
id: tx-603
izenburua: Zu Zeu Ta Ni
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sqdEQTwacRI
---

Inoiz gauzatzen ez den
geroaren zai nago sarritan.
Bide bazterretako inguruari so.
Bizitzaren zertzeladetan
geldi une batez
miresten dudan
lore bakanen bat losinduz.

Lo egin nahi dut lo egin
ta ezin gau ilun honetan.
Ta ezin ta nahi
kezka darit bihotzean,
urte luzez hido hortatik igaro naiz
zorion ahaleginaren
gogo handi batean,
eta bideari narraionean
ametsak lorrintzen zaizkit,
eta bideari narraionean
loreak zimelduz doazkit.

Zu zeu ta ni,
ni neu ta zeu,
esperantzaren bidean goaz
hegoak aidean.
Zu zue ta ni,
ni neu ta zeu
geroaren izate baten
irudi berrian.

Zutaz pentsatu dut
zugan adierazi, naizen hori
zugan sinesten dut argi indarra,
eguzkiak beti berdin berdin
laztantzen nau,
ta gero beti berdin berdin
ihes egiten dit.
Ta madarikatzen ditut ene orduak
zurearekin bat ez direnean,
ta madarikatzen ditut ene orduak
monotonia huts bihurtzen direnean.

Zu zeu ta ni...