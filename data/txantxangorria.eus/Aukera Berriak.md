---
id: tx-846
izenburua: Aukera Berriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cnlbx6-yGFM
---

Musika eta letra: Iñigo Etxezarreta (E.T.S.)

Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 

Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Ekoizpen exekutiboa: Baga-Biga


Aurrera eta atzera, ikusi pausu berdinak
errepide zuzen honetan,
marraztutako bidetik isiltasuna
haizearen norabidean.

Puntu bati begira galtzen dugu ingurua
eta bertan dauden koloreak.
Baina bidegurutzeak dira aukera berriak, 
dira aukera berriak, 
aukera berriak.

Ahaztutako ateak azaltzen hasten dira
euren bila goazenean.
Ausardiaren hegoak astinduz hegan,
haizeak laztantzen nau orain.

Aldetara begira sentituz ingurua
eta bertan dauden koloreak.
Nire bidegurutzeak dira aukera berriak, 
dira aukera berriak,
aukera berriak.

Margotuko ditugu kolore berriak, 
orri zuri batetik ipuin ederrenak.
Irekiko ditugu batera ateak,
oihartzunak ekarri ditzala erantzunak…