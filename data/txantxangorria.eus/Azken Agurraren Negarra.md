---
id: tx-2617
izenburua: Azken Agurraren Negarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2Wscq60_wD0
---

Nora zoaz, eskual semea,
harma hori eskutan?
Harmen hartzera deitzen naute
frantsen aldera.
Harmen hartzera deitzen naute
frantsen aldera.
Eskualerritik urrunduz,
ta atzerrira joanak,
a ze negarra entzunen duzu
Eskualerrietan!
a ze negarra entzunen duzu
Eskualerrietan!

Morts pour la patrie,
morts pour la patrie,
eskuara baizik etzakiten haiek,
morts pour la patrie.
Morts pour la patrie,
morts pour la patrie,
eskuara baizik etzakiten haiek,
morts pour la patrie.

Gure historian zehar
zenbat malko ta ezbehar;
Landetaratu gindutenekila
dugu orai hil behar.
Landetaratu gindutenekila
dugu orai hil behar.
Bere ama agurtu du
etxolako atarian;
bere amak bisitatuko du
atzerriko hilobian.
bere amak bisitatuko du
atzerriko hilobian.

Morts pour la patrie,
morts pour la patrie,
eskuara baizik etzakiten haiek,
morts pour la patrie.
Morts pour la patrie,
morts pour la patrie,
eskuara baizik etzakiten haiek,
morts pour la patrie.

Azken agurraren negarra Gorka Knörr abeslariaren abestia da. Bere gaia Ipar Euskal Herritik Lehen Mundu Gerrako guda lekuetara Frantziaren alde borrokatzera joan ziren euskaldunak dira: abestiak dioen bezala, haietako askok frantsesez hitz egiten ere ez zekitelarik, «Morts pour la patrie» («aberriarengatik hilak») izan ziren. Izan ere, Lehen Mundu Gerrak Ipar Euskal Herriko gizon asko hil zituen, eta ia herri guztietan jarri zituzten «combattants morts pour la patrie» («aberriaren alde hildako gudariak») dioten plakak.