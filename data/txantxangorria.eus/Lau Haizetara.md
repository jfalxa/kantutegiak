---
id: tx-1400
izenburua: Lau Haizetara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ErKfLeKLJyI
---

Itsasondoko haize gezala
Marinelen bertsoa
Zoaz urruti aidean
Lau haizetara (tris)
Donostiatik Iruñera.

Uzta garaiko gari usaia
Segarien bertsoa
Zoaz urruti aidean
Lau haizetara (tris)
Iruñetikan Baionara

Ezpain erien portu galdua 
Maitarien bertsoa
Zoaz urruti aidean
Lau haizetara (tris)
Baionatik Vitoriara

Muga zaharrak mahastietan
Gaiteroen bertsoa
Zoaz urruti aidean
Lau haizetara (tris)
Vitoriatik Bilbaora

Burnizko bihotz arrailduna
Gabetuen bertsoa
Zoaz urruti aidea
Lau haizetara (tris)
Bilbaotik Euskal Herrira