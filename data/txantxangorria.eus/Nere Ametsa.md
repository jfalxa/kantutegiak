---
id: tx-801
izenburua: Nere Ametsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n-_1dHpeDDo
---

Bart egin dudan ametsa da
espetxetakoak askatu dituztela,
bart egin dudan ametsa da
atzerriratuak etxeratu direla
bart egin dudan ametsa da
zazpi senideak berriro bat gerala,
Behenafar, Zubero, Lapurdi,
Bizkaia, Gipuzkoa, Nafarroa eta Araba. (bis)

Esnatu eta ametsa dala
naizenean konturatu
nahiago nuen amets horrekin
beti-betiko lo hartu
edo bestela nere ametsa
nolabait egi bihurtu. (bis)

Bart egin dudan ametsa da
espetxetakoak askatu dituztela,
bart egin dudan ametsa da
atzerriratuak etxeratu direla
bart egin dudan ametsa da
zazpi senideak berriro bat gerala,
Behenafar, Zubero, Lapurdi,
Bizkaia, Gipuzkoa, Nafarroa eta Araba. (bis)

Bakoitzak bere eskubideak
norberekin behar ditu
askatasuna geuria da ta
behar dugu eskuratu
edo bestela nere ametsa
nolatan egi bihurtu. (bis)

Bart egin dudan ametsa da ...