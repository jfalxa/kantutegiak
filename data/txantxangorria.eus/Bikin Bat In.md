---
id: tx-2639
izenburua: Bikin Bat In
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q2jzO_48x6o
---

Musika: Igor Telletxea
Hitzak: Estitxu Arozena
Grabaketak eta edizioa: Xumar Altzugarai
Drone gidaria: Andoni Gartzia

Ahotsak: 
Amaia Eneterreaga
Beñat Lopez

Musikariak:
Beñardo Iantzi
Iñigo Etxepare
Jon Koldo Arburua
Joseba San Sebastian
Mattin Arbelaitz
Jon Alberro
Juan Ramon Apeztegia
Mikel Iriarte
Peio Etxebeste
Xabier Fagoaga
Gaizka Sarasola
Igor Telletxea

Abestia Donostiako Elkar estudioan grabatua Victor Sanchez teknikariarekin

'Bikin bat in'
(Letra: Estitxu Arotzena) 
Toberaren soinuak zulatu du Agiña
Mairubaratzeetatik sentitzeko adina
Altzairuzko oinarria intxaurran hosto arina
Zer_izan nahi genuke orain oroimen bagina
Agiña, adina,
Hosto arina, Agiña, adina, bagina…
Bikin batin batin biak ume baten bi sendiak 
bikin batin batin biak errekak eta mendiak 
Oinez Lesakan erditu, 
Tantirumairuk bi ditu 
zortzi zazpi sei bortz lau hiru bikin batin 
Adiskide, bide lagun, ikaskidea
euskara da gure, zure eta nirea
ttiki ttaka ttipi ttapa, oroimenaren bidea
zer ginen jakin behar da izateko librea
bidea, nirea,
ikaskidea, bidea, nirea, librea
Bikin batin batin biak ume baten bi sendiak 
bikin batin batin biak errekak eta mendiak 
Oinez Lesakan erditu, 
Tantirumairuk bi ditu 
zortzi zazpi sei bortz lau hiru bikin batin