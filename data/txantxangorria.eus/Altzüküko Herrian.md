---
id: tx-2498
izenburua: Altzüküko Herrian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_RmKRlm6VeI
---

Altzururkuko herrian
Gira turisten gidatzeko
Urde hanitx bide erditan
Zirkulazionearen egiteko
Lala…

Beharre da   mendikotan
Seroen inganatzeko
Petiurten saxofona
Beharrien gilikatzeko
LALA …

Don Camillo ba Zalgizen
Jaun erretora famatua
Xiberoan dü peredikatzen
Eliz-ostatüetan egia.
Lala…


Etxahunen txirula
Iruri-en  düzüe entzüten
Baita Larraine gainean
Ahal dûzüe bogatzen
Lala….

Gure Xibero gainean
Atarratzen besta handia
Jenteak jiten dira pean
Ase baten tiratzea
Lalala…