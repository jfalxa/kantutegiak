---
id: tx-1118
izenburua: O Peio Peio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0Noa_7rGN0w
---

O Peio Peio logale naiz eta
Jinen niza oherat Har bi muxu edo  Esne-ezti bero
Gero gero gero
Gero gero bai

O Peio Peio logale naiz eta
Jinen niza oherat Bai ta esan gauero Mamurik balego
Gero gero gero
Gero gero bai

O Peio Peio logale naiz eta
Jinen niza oherat 
Bai ta ni espero Behar nauzunero
Gero gero gero
Gero gero bai

O Peio Peio logale naiz eta
Jinen niza oherat
Bai ta nahi ezkero Zain nauzu goizero
Gero gero gero
Gero bai