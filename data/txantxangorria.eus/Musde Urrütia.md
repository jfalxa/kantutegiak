---
id: tx-2687
izenburua: Musde Urrütia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j852gMZBLCc
---

Musde Urrütia leihoti
 eta ni aldiz kanpoti;
 bihotza erdiratü zeitan so ezti bateki
 üdüri ziren haren begiak izar zirela zelüti.
 
 Ene xarmagarria,
 hüillan düzü eliza!
 zonbait aldiz jinen zira, harat meza entzütera:
 begiaz kheiñu eginen dügü ezin mintzatzen bagira.
 
 Españalako bidia
 ala bide lüzia!
 Gibelilat so'gin eta, hasperrena ardüra
 maitettoaz orhit eta nigarra begiala
 
 Hortzak xuri, begiak beltz
 ene maitia zeren ez?
 mündü orok diozie, ni nizala traidore
 orai arren erradazü, hala nizanez bai al'ez.

Amak dio alabari 
kanaila txarra hi! 

Hik behar dun ibili 
ibiltekoz ibil hadi hihaur 
üdüri zonbaitekin.

Ama zaude ixilik 
ez erran otoi gaizkirik 
Gaztetarzüna inozent dela
ez dakizüa
engoitik


zure denboraz orhit 
eta ez egin otoi nigarrik.