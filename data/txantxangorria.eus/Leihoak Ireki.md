---
id: tx-2036
izenburua: Leihoak Ireki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZX5WOfYbl8w
---

Leihoak itxita arnas hotsak
Oraindik entzun ahal ditudanak
Karakolak begiratzean
Nire burua gela ilun batean

Gure bizitzan egun beltzak,
Zorion faltsuzko egun hotzak,
Baina egun hauek utzi kanpoan, utzi kanpoan

Zure indarrak eta nire kemenak, elkartzerakoan,
Eztanda egin eta,
Unibertsoa argiztatuko da.

Ez beldur izan ni joaten naizenean,
Irribarre batekin
Zeruko argi guztiak
Piztuko dira zurekin

Bihotz bakar bat, irri bakar bat

Leihoak itxita gezurra esatean,
Sentimendu honekin aurrera egitean,
Leihoak ireki, leihoak ireki, leihoak ireki...

Zure indarra eta nire kemena elkartzerakoan,
Eztanda egin eta,
Unibertsoa argiztatuko da.

Ez beldur izan ni joaten naizenean,
Irribarre batekin
Zeruko argi guztiak
Piztuko dira zurekin.