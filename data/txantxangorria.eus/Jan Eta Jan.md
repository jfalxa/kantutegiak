---
id: tx-2544
izenburua: Jan Eta Jan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c5Fn4J8JO_U
---

Jan, jan, jan e/ta jan
sagarra sartu dut ahoan.
Edan, edan, edan
egarririk zaudenetan.

BELARRIEK JAN NAHI DUTE,
GOSEZ DAUDELA DIOTE,
SOINUAK JATEN DITUZTE
NIK BEHINTZAT HALA DUT USTE.