---
id: tx-373
izenburua: K.K.Ren Edestia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ip6FwVZ1JD8
---

"K.K.Ren edestia" (Jon Bergaretxe/Motza-Joxean Artze) 1978.


Baserri baten onduan mokordua nun ikusi
Luzian bazituen zazpi arra térdi;
Huraxe mokordua guztiz atsegiña
Bi neskatxa ari ziren elkarri esaten
Batek lodiagua zuela egiten;
Nerón hantxe nintzen guztiak ikusten 
Erroska bat bezela bildua zeukaten.
Kaballeruak kakarik ezin dute egin
Egiten dutena ere gutxi eta zirin.
Jan zazu bildur gabe aza ta babia
Orduan egingo dezu mokordu lodia.
Kaka zuretzako ta kaka harentzako
Gauza biharra da-ta mundu danentzako.
Erico alkatiak eman du ordena
Aurten hilko dala kaka egiten ez duna.