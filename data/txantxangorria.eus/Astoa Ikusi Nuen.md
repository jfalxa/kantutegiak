---
id: tx-1159
izenburua: Astoa Ikusi Nuen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jJHwiDxy2gc
---

Astoa ikusi nuen betaurrekoekin
buruan txapela eta gabardinarekin.
Ile luzea eta bibotearekin,
hizketan egon nintzen atzo berarekin.

Astoak esan zidan triste zegoela
oso zatarra zela bere ikastola.
Berak gure gelara etorri nahi zuela,
gure artean ondo konponduko zela.

Baietz esan nion nik lasai etortzeko,
arkatza eta bloka dendan erosteko.
Astoa izan arren, trankil egoteko,
asko ikasi eta txintxo portatzeko.