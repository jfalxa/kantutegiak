---
id: tx-3252
izenburua: Eguberri Zuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Psh6DR0qbAE
---

Oi, eguberri zuria!
Zilar distiraz jantzia
Leihoan elurra,
gauaren mugan,
negu haize atarian.
Hotz eta ilun gurian
eta goxo sutondoan.
Ospa dezagun sendiak
berriz ere eguberri zuria!