---
id: tx-122
izenburua: Santo Tomas Eguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gCD0vIYGCcU
---

TTUN TTUN BRIGADE - Santo Tomas Eguna
Santo Tomas egunez 
gaua iristean ai ai aiai
Poltsikoak beteta
Goizetik lanian ai ai aiai
Sagardo gehitxo edan nuela agian (nuela agian)x2
Nola bete sabela
Era lazterrian, 4 xoxen trukian
Plazanuevan taloa garesti aukeran ai ai aiai
San Frantzisko kalean
gora eta behera ai ai aiai
Kebab bat aurkitu nuen Hernani kalian (Hernani kalian)x2 Ta hantxe sartu nintzen, lasai ederrean!
Durum entsalada gabe
Arkume eta txahala 
Patata erreekin
menua itzela
Euro bat gehiogatik
Garagardo lata
Saltsa gorri ta zuri
BIZKAIKO INDARRA
Santo Tomas egunez
Bilbotik galduta
Santo Tomas egunez
Durum afalduta
Santo Tomas egunez
txipak txikituta
Orain denok abestu
guztiok batuta
Santo Tomas egunez
goizetik gauera
Santo Tomas egunez
gauetik goizera
Santo Tomas egunez
biharamuna da
Santo Tomas egunez
poltsa bukatu da.