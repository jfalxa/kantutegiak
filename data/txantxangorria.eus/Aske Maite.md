---
id: tx-13
izenburua: Aske Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ezDsJbHGBsQ
---

Kilimak taldearen "Aske Maite" abestiaren bideoklip ofiziala.
Videoclip de "Aske Maite", nueva canción de Kilimak.


-------------------------------------------------------------

Hitzak:

Gaur goizian esnatu naiz
ilun dago logela
zure muxutan pentsatzen
hutsa daukat sabela
baina nik ongi badakit
biok libre garela
ta gure sentimenduak
elkarturik daudela

jakin gabe
zu norekin zauden
muxukatzen
edo ongi pasatzen
ta niri bost
zu zertan ari zaren
nik zu ikusten bazaitut
bizi hontaz gozatzen

aske maite
arau gabe
ta beti izan zure buru jabe (X2)

etzazula inoiz esan
norbaitena zarela
libre baigara guztiak
ta hobe da horrela
inoren menpe sentituz
ez zara ba fidela
sentitzen duzuna egiten
ez baduzu ergela

jakin gabe
zu norekin zauden
muxukatzen
edo ongi pasatzen
ta niri bost
zu zertan ari zaren
nik zu ikusten bazaitut
bizi hontaz gozatzen

aske maite
arau gabe
ta beti izan zure buru jabe (X2)

(Solo)

jakin gabe
zu norekin zauden
muxukatzen
edo ongi pasatzen
ta niri bost
zu zertan ari zaren
nik zu ikusten bazaitut
bizi hontaz gozatzen

muxuka nazazu (muxu xu xu xuabe)
horrela maite dut (xu xu xu xuabe)
muxuka nazazu (muxu xu xu xuabe)
maitatu gaitezen
inungo arauik gabe

aske maite
arau gabe
ta beti izan zure buru jabe (X4)

----------------------------------------------------------------------------------------

Grabación y edición por Borja Suberbiola - @control_ese
Grabazioa eta edizio Borja Suberbiola - @control_ese