---
id: tx-1626
izenburua: Pum Ba Pa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3e3MCR6VR3A
---

Oi txiribiri tonton
Conpentamper timentaza
Lous eta lontximen
Parabi paraboton
Parabi ke montatxon
Kalatxion!
La vita militare
La vita se sifosa
Lou temp de l'amorosa
PUM-BA PUM-BA PUM-BA-PA
PUM-BA PUM-BA PUM-BA-PA
Elsous elsous elsousa
Giro la per mania
Bedola mantenia.