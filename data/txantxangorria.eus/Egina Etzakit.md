---
id: tx-2882
izenburua: Egina Etzakit
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QRGjSPabWR4
---

Hitzak ikasiz eta hizketan hasiz, irudimenean
Geure pentsaerari aske izaten utzi genion aidean
Elkartrukatuz, idatziz ta irratiz hegan geundenean
Hitza nola ebaki daitekeen erakutsi ziguten bidean...
"Egin" hil nahiean!

Eskurik esku "Egin" berria helburu
Isildu gabe berdin hitz egingo dugu.
Eskurik eskuekin geroa seguru
Geurea dugu eta "Gara" egin dugu

Egin ta egin, bultza ta bultza,
Denon ekimen, denon laguntza
Egin behar da, egingo dugu
Denon ekimen, denon helburu
Herririk herri, zaharrak berri
Herriak orri, orriak herri
Ekarpenekin, elkartuz ekin
Orririk orri, egin ta egin.

Bultza ta bultza, indarrak biltzen
Indarrak bultzaz, indar berritzen
Lehen baginen, orain bagara
Indarrak biltzen, jaio da "Gara"
Orain bagara, lehen baginen
Jaio da "Gara", jaioak ginen
Egin nahi zena, ekin ta ekin
Egin da "Gara", "Gara" da "Egin"