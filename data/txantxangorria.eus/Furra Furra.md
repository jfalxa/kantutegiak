---
id: tx-3133
izenburua: Furra Furra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oGz_Zn-mDH8
---

Furra, furra, fandangoa,
hortxe duzu fandangoa
geure gustokoa.

Gaur goizean jeiki naiz
suerte onean,
tanke bat topatu dut
neure kafesnean;
ez dakit zer daukagun
bake ala gerra
bainan nik badaezpadan
egin dut puzkerra.

Zerbait egitekotan
zuzen eta artez
zorri bat garbitu dut
ur pistola batez,
orain galdurik nago
beldurrez beteta
muniziorik gabe
gelditu naiz eta.

Neure arma bakarra
dut akordeoia,
hauspoari eraginez
dirudi leoia;
eskua jaten badit
on egin dezaion
Cervantsei holakorik
gertatu zitzaion.

Eta orain banoa
berriro ohera,
bila ez badatozkit
lolo egitera;
bihar ikusiko da
zer dagoen berri,
jakintsuenak ere
ezin du igerri.