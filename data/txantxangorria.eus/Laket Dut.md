---
id: tx-1980
izenburua: Laket Dut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yOkcwdrz92k
---

Laket dut ene itsasadarra
Oriaren halabeharra
laket dut bere urertzean
jausten zaidan sentipen zirrara,
laket dut itsasbeheran
gailentzen den legartza,
hurruntasun ilunaren lorratza.

Ene oroitzeko bizipenak
han pausatzean
urtutako amets bat naiz,
hunkidura arrotz bat,
baleen kantek sorgindutako gogoa,
mariñel galduen bila
dabilen nomada.

Izkutu jakintza bailitzan,
arraun hotsez,
iragan oiartzunak
hegaldatzen dira
gune umelean
Terralak hartu nau
ur geldien loan
joandako argiñabarrak
biltzen zaizkit gogoan.
laket dut...