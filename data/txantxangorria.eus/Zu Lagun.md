---
id: tx-1941
izenburua: Zu Lagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/weaViuPGJ6k
---

"Zu Lagun" kantaren bideo-klipa, "Iaze" (Gaztelupeko Hotsak 2012) diskatik hartuta dago eta Iñigo Cabacasen oroimenean eginda, lagunen eta senideen parte hartzearekin batera eta Aiora Renteriaren kolaborazioagaz

Maiz eskatzen dit egunak egiteko aurrera
ez dakidanean noiz izango den azkena
bele beltza soinean, trumoiaren soinu
agertu zara iluntasunan, ezin sentitu
esku lohi hori izan dut nire bihotzean joka
herriak oihukatu du, entzun ahotsa, lortu egia
besarka nazazu, ez itxi begiak
sentitu behar dut hurbil utzitako argi guztia
eta ezazu hodeien artean 
zabaldu nire irrifarraren grina euria bailitzan
ez dut denborarik izan, ta agur nuke esan nahi
patuaren besoetan ez erortzearen ernai
irteerarik gabeko kale ilun horretan
odolezko bide batean lotan etzan naiz
esku ezezagun bat bila zuen eta bihotzean topa
aurrera egin dezagun zu gabe bide nire laguna
Non dago orain justizia
hiltzeko askatasunik Pituk ez du izan 
Carpe diem, bere hitzak, balora ditugu nahi zuen heinean