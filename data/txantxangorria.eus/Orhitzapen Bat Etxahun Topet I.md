---
id: tx-28
izenburua: Orhitzapen Bat Etxahun Topet I
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bIgMY7u50Ls
---

Orhitzapen bat ETXAHUN Topet-i 
Sortü giren güziek zor dügü hiltzea, zühauretan dügüa hortxek etsenplüa ? Orano hain bizi da zure ixtoria, hil zinela ez zaikü aisa sinhestüa. 
Izar gaixtoen pean zü zinen sortürik, zure aita et’amaz gaizki maitatürik. Afetsione mentsean gero handitürik, triste bazinebiltzan oso herratürik. 
Primü handia bena bardin miseraule, hontarzüner ez zünüan eskerrik batere, Ümil behar ordüan besteen trüfale, ürgülürik ez zünüan, bena bai fiertate. 
Lehen amodiotik gaixki partitürik, « judio herratü bat » zü zinen geroztik, Gaüaz bidajazale, egünaz gorderik. Azkenekoz hil zinen, denez hügüntürik ! 

Hitzak : ETXAHUN Iruri 
Müsika : Jean ETCHART