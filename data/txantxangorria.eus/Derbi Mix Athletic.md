---
id: tx-3086
izenburua: Derbi Mix Athletic
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tHlOfovqonc
---

ATHLE  eee... eee... eee... eee... eee... TIC!!
Euuup!!
ATHLE  eee... eee... eee... eee... eee... TIC!!
Euuup!!
ATHLE  eee... eee... eee... EEE... EEE... TIC!!
Euuup!!
Ala bi ala ba ala bin bon ba"
Athletic!! Athle/tic!! GEURIA!!

ATHLE  eee... eee... TIC!!
ATHLE  eee... eee... TIC!!
Euuup!!

Athletic, gorri ta zuria
danontzat zara zu geuria.
He/rri/tik sor/tu zi/na/la/ko
mai/te zai/tu he/rri/ak.

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!


Al/tza Gaz/ti/ak.
A/thle/tic, A/thle/tic,
A/thle/tic, A/thle/tic,


A/u/pa mu/ti/lak
au/rre/ra gu/re gaz/ti/ak
Bil/bo ta Biz/kai guz/ti/a
go/ra/tu be/di mun/du/an.
A/u/pa mu/ti/lak
go/ra be/ti Eus/kal/he/rri/a
A/thle/tic go/rri zu/ri/a
ge/u/ri/a.

A/u/pa mu/ti/lak
au/rre/ra gu/re gaz/ti/ak
Bil/bo ta Biz/kai guz/ti/a
go/ra/tu be/di mun/du/an.
A/u/pa mu/til/ak
go/ra be/ti Eus/kal/he/rri/a
A/thle/tic go/rri zu/ri/a
ge/u/ri/a.

A/thle/tic, A/thle/tic, eup!
A/thle/tic, go/rri ta zu/ri/a
da/non/tzat za/ra zu geu/ri/a.
He/rri/tik sor/tu zi/na/la/ko
mai/te zai/tu he/rri/ak.

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Al/tza Gaz/ti/ak.
A/thle/tic, A/thle/tic,
A/thle/tic, A/thle/tic,


A/u/pa mu/ti/lak
au/rre/ra gu/re gaz/ti/ak
Bil/bo ta Biz/kai guz/ti/a
go/ra/tu be/di mun/du/an.
A/u/pa mu/ti/lak
go/ra be/ti Eus/kal/he/rri/a
A/thle/tic go/rri zu/ri/a
ge/u/ri/a.

A/u/pa mu/ti/lak
au/rre/ra gu/re gaz/ti/ak
Bil/bo ta Biz/kai guz/ti/a
go/ra/tu be/di mun/du/an.
A/u/pa mu/til/ak
go/ra be/ti Eus/kal/he/rri/a
A/thle/tic go/rri zu/ri/a
ge/u/ri/a.

A/thle/tic, A/thle/tic, eup!
A/thle/tic, go/rri ta zu/ri/a
da/non/tzat za/ra zu geu/ri/a.
He/rri/tik sor/tu zi/na/la/ko
mai/te zai/tu he/rri/ak.

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Euskalherriaren
erakusgarria.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Al/tza Gaz/ti/ak!!





go/go/a/ren in/da/rra.
A/ritz za/ha/rra/ren en/bo/rrak
lo/ra/tu dau or/bel ba/rri/a.

Au/pa mu/ti/lak
au/rre/ra gu/re gaz/ti/ak
Bil/bo ta Biz/kai guz/ti/a
go/ra/tu be/di mun/du/an.
Au/pa mu/til/ak
go/ra be/ti Eus/kal/he/rri/a
A/thle/tic go/rri zu/ri/a
ge/u/ri/a.

Bil/bo ta Biz/kai/ko
gaz/ti/ak go/ra!
Eus/kal/dun
zin/tzo/ak au/rre/ra!

A/thle/tic, A/thle/tic, eup!
A/thle/tic, go/rri ta zu/ri/a
da/non/tzat za/ra zu geu/ri/a.
He/rri/tik sor/tu zi/na/la/ko
mai/te zai/tu he/rri/ak.

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Gaz/te/di go/rri zu/ri/a
ze/lai or/le/gi/an
Eus/kal/he/rri/a/ren
e/ra/kus/ga/rri/a.
Za/bal/du dai/gun
guz/ti/ok i/rrin/tzi a/la/ia:
A/thle/tic, A/thle/tic,
zu za/ra na/gu/si/a!

Al/tza gaz/ti/ak


go/go/a/ren in/da/rra.
A/ritz za/rra/ren en/bo/rrak
lo/ra/tu dau or/bel ba/rri/a.