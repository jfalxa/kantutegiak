---
id: tx-1458
izenburua: Sueños De Color
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3PfW6WQJQNc
---

En el pupitre
dibujaba corazones
frases de dolor
sueños de color
Como si fueran
huellas que se dejan
en la nieve
llenas de calor
brillando al sol
Por eso al buscarlas hoy
es cuando mas cuenta me doy
que juego a ser el niño que era ayer
Hondarretan idatzitako esaerak
bihotz batekin
inguratuak
komunetako ateetan utzitako maitasunezko mezuak gogoratzean
Ikusten zaitut berriro
laztantzen zaitut astiro
atzoko haurra izango banintz bezala
Ikusten zaitut berriro
laztantzen zaitut astiro
gaur atzo izango balitz bezala
Eta hala bada esaidazu
zuk ere badakizun
elkarrekin zein ondo pasatzen genuen
Umetako jolasak eta
denbora pasak
memorian zizelkatutako
izenak
ez gara berriro izango
izan ginenak
izarretan galdutako maitemineak
ez zu, ez ni
ez gaude iraganean galdurik
ez zuk, ez nik
ez dugu alferrik galdutako unerik
dime,
dime si lo sabes tu tambien
dime,
que quiero tener
el recuerdo de lo bien
que lo pasabamos ayer,
el recuerdo de lo bien
que lo pasabamos ayer!
Eta hala bada esaidazu
zuk ere badakizun
elkarrekin zein ondo pasatzen genuen
Ez zu, ez ni
ez gaude iraganean galdurik
ez zuk, ez nik
ez dugu alferrik galdutako unerik
Y dime si lo sabes tu tambien
dime que quiero tener,
el recuerdo de lo bien
que lo pasabamos ayer
Lalalalalala
Lalalalalala
Lalalalalala
Lalalalalala
Lalalalalala
Lalalalalala