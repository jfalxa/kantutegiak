---
id: tx-311
izenburua: Begietara Begiratu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dvp5icwUDUk
---

PIZTIAKen XXX. urteurrenerako egindako kantaren bideoa EL MONSTRUITOk egina.
Video de la canción de PIZTIAK para su XXX aniversario, realizado por EL MONSTRUITO.

BEGIETARA BEGIRATU                                        

BAZATOZ FRESKOTASUN EDER HORREKIN,
INGURU DENA BETEAZ.
BAZATOZ GAUZAK ARGI ESANEZ,
BETI LAGUNTZEKO PREST.

BEGIETARA BEGIRATU ETA
ESAN ZEIN DEN EGIA, 
BEGIETARA BEGIRATU ETA 
BADAKIT ZEIN DEN EGIA.

BAZATOZ BEGIAK BETE BETE EGINDA,
ONDO EZAGUTZEN GARA.
BADAKIT ZER DEN ETA ZER DUDAN! 
ZENBAT DENBORA DAUKAT?

BEGIETARA BEGIRATU ETA
ESAN ZEIN DEN EGIA,
BEGIETARA BEGIRATU ETA
BADAKIT ZEIN DEN EGIA.

(Gaixotasun larri bat eduki edo izan dutenei eta bere ingurukoei eskeinitako abestia da)


                                                                                                        ©Txomin Uribe Urbieta

KONTAKTUA/CONTACTO/CONTACT: piztiakrock@hotmail.com

JARRAITU/FOLLOW/SIGUENOS