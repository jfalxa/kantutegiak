---
id: tx-1995
izenburua: Big Beñat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PkqPRk-8Ts8
---

Korrika badator
Yeah! yeah! yeah! yeah! yeah!
Badator
Korrika, herri bat mugimenduan
Badator ta Big Beñatentzat
Beste ezinezko misio bat
Mundu bat bildu da kontsigna
Ezin huts egin gure antiheroiak
Big beat-a erritmo erraldoiaz
Nor ez du martxan jarriko horrelako deiak?

Gas-Gasteiztik
Bai-Baionarat
Mende berri bat
Hasteko dugu Korrika
Buruan mundua
Makina jendea
Pentsatu globalki ta
Ekin lokalki ez al da?
Eskualdunak gara
Eta mundukideak
Uniformetasunetik at
Gizakiak
Ez kasko transgenikoak
Ez zenbakiak
Bizirik, mugaegunik ez baitu euskarak
Modernizatzea bada izan gisakoa
Barra kodea tatuatu nire ipurdian

BEGIRA EZAZU... BIG BEAT BEÑAT
BEGIRA EZAZU... BIG MAC BEHERA
KORRIKA BADATOR... BIG BEÑA BURU
UH, UH, UH, UH, UH, UH... MUNDU BAT BILDU!

Macdonalizazioa, kultur globalizatua
Kontsumitu plastikoa, janari azkarra bezala
Uztartzea da bat, Bestea ezarpena
Elkartrukatu edo derrigortu ereduak
Big Beñat, babarrunak eta aza popularra
Suge tako mexikarra eta pa amb tomàquet
Gazta eta intxaurrak, Lurra ez baita zolua
Bazter dezagun behingoz kaka zahar-zaharra

Ttipi ttapa, ttipi ttapa... KORRIKA
Ttipi ttapa, ttipi ttapa... KORRIKA