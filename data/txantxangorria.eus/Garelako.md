---
id: tx-2958
izenburua: Garelako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6IjbH8QQQwg
---

Letra, ahotsa eta pianoa: Eneko Sagardoy
Koroak: Irati Bilbao
Gitarra: Daniel del Valle

Zeru gris lainotuak, ez gaitu itoko
Ihes egingo dugu, eguzkirainoko
Nahi dizkigute jarri lastozko bihotzak
Utzi guri sentitzen zer dugun nahiago

Txoriaren hegoak badira ibiltzeko,
Baleari itsasoa ukatzen bazaio,
Ez dugu ikusiko fikzioa baino,
Hitz guztien gainetik, gezurra badago.

Aurrera egingo dugu,
Zuk nahi ala ez.
Norbait librea bada, denok garelako.
Benetan biziko gara,
Zurekin edo ez.
Konturatuko zara, berandu baino lehen.

Ez gara munstroak, ez dugu inor jaten
Eguzkiak gaitu berotzen ta euriak busten,
Orain konturatu zara? Hezur haragizkoak gara, zu bezala!

Marimutil edo Marineska, esaten digute
Utzi behingoz alferrik hitzegiteari
Ikasi ondo gauza bat, eta gorde buruan
Mari bakarra dela Anbotokoa

Aurrera egingo dugu,
Zuk nahi ala ez.
Norbait librea bada, denok garelako.
Benetan biziko gara,
Zurekin edo ez.
Konturatuko zara, berandu baino lehen.