---
id: tx-1085
izenburua: Eutsi Goiari Ijito
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KEC5KDFOC5o
---

Musika: Xabier Zabala

Zeru urdina gure teilatu
zelai berdea zoru
gurpilen biran, eutsi goiari!
bihotza aske, gorri
ijito, eutsi banderari!

Zeru urdina gure teilatu
zelai berdea zoru
gurpilen biran, eutsi goiari!
bihotza aske, gorri
ijito, eutsi banderari!

Kax-kax-kax eman zartegiari
ñi-ño-ñi-ño-ñi bibolinari
kax-kax-kax orpoekin lurrari
arrastoa utziz gatoz kantari
bidean agur eginez zuri
agur Euskal Herriari.
Bidean agur eginez zuri
agur Euskal Herriari.