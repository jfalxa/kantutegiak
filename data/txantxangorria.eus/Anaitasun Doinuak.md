---
id: tx-2279
izenburua: Anaitasun Doinuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WSm-xoXeGt4
---

2020ko Otsailean, Dani Skt Designen eskutik grabatutako Bideoa.
Alerta gorriak eta Nafarroa 1512ko anaiek grabatutako Anaitasun doinuak abestia.


Letra: 

Bidean elkarrekin, anai zahar ta gaztearen historia.
Piztiaren bihotza, gudariaren mailu ta higitaia.
Beti alertan, beti gorriak, guda orroa kale lahiotzetan.
Amets gaizto bihurtu arte, hamarkadaz garraxirik sakonena.
Etxe bereko hitza, odol bereko borroka griña bera.
Irribarreak, guda, bidaiak, erretinan bizipen ahaztezinak.

Biok elkarrekin, bihotz ta ukabil
bi anaien historia.
Alertan beti, 1512! Nafar ta gorri. (x2)

Gaztetako ametsak, elkarrekin betetzen direnean.
Kolektiboaren indarra, herriaren kontzientzia astintzea.
Euskarak batzen gaituenean, herri libre baten alde borrokan.

Biok elkarrekin, bihotz ta ukabil
bi anaien historia.
Alertan beti, 1512! Nafar ta gorri. (x2)

Laguntasuna herrian batua.
Musika idazten, ideiak idazten.
Errealitate berri bat amesten.

Biok elkarrekin, bihotz ta ukabil
bi anaien historia.
Alertan beti, 1512! Nafar ta gorri. (x2)

Bidean elkarrekin, nafar ta gorri. (x4)