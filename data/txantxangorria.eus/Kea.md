---
id: tx-863
izenburua: Kea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jc4gplQ4KL0
---

KEA, ATERA 2019
Musika eta letra: Zea Mays

Mutila/Chico1: Mikel Losada
Mutila/Chico 2 : Na Gomes
Neska/Chica 3 : Leire Ucha
Umea/niño : Kepa Izagirre
Skaterra/skater : Iñigo Igarza
Ekoizpena/Producción: Txaber Agirre/ Gaizka Izagirre
Edizioa/Edición : Cue ( Bitart new media)
Zuzendaritza/Dirección: Gaizka Izagirre
Eskerrak/Agradecimientos: Amaia Iriondo, Olaia Cano,Epoca sukaldeak, Beñat Zarandona, Gotzon Sierra, Napotz taberna, Iñaki Martinez, Amaia Bengoa, San agustin kultur gunea.

www.sonde3.com
www.zea-mays.com


ATEA ITXITA 
LEIHOTIK BEGIRA 
GORRIAK EZPANAK 
ILEA URDINA 
MALKO TXIKI BAT 
ZU GOGORATZEAN 
IRRIBARRE HANDIA 
ZORIONTASUNA 

OKER IBILIKO NAIZ 
BAINA DENA ALDATU DA 
ORAINGO GEROA EZ DA IZATEN ZENA 
ALDAKORRA NAIZ, AGIAN BURU ARINA 
HOTZ TA BEROA UNE BEREAN 

KEA BEZELA AIREAN 
HEGAN NABIL GAUEAN 
DANTZA LUZE BATEAN 
MIHAZKATUZ HAIZEA 

HORRELA BADA BIZITZA 
HARTUKO DUT BETI DEN MODUAN 
HORRELA BADA BIZITZA 
BETI AURRERA EGIN BEHARKO DA 
ALDAKORRA NAIZ, AGIAN BURU ARINA 
ZURI TA BELTZA UNE BEREAN