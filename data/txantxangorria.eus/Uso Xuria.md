---
id: tx-1174
izenburua: Uso Xuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lAY96ngBzpU
---

Uso xuria, errazu,
nora joaiten ziren zu.
Nafarroako bortiak oro
elurrez betiak dituzu,
gaurko zure ostatu
gure etxean baduzu.

Ez nau izützen elurrak
are gutxiago gau ilunak.
Maitea gatik pasa nezazke
gauak eta egunak;
gauak eta egunak,
desertu eta oihanak.

Usoa eder aidian,
ederrago oihanean.
Maitia, zure parerik ez da
Espaina guzian ezta
ere Frantzian,
iguzkiaren azpian.