---
id: tx-2381
izenburua: Oroimeneko Portua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fmdl2coOnZ8
---

#saveyourinternet #Artículo13


Joseba Sarrionandiaren poesia musikatuta egindako abestien bilduma da Gure Oroitzapenak. 2018an Elkarrek argitaratua. Bilduma horren baitan dago 'Oroimeneko portua' abestia Xanperen Koba Studioetan grabatua. 

Musika: Mikel Urdangarin
Hitzak: Joseba Sarrionandia

#saveyourinternet

Ifar aldeko portu zaharra
ekar dezagun kantara:
kale bustien sentimendua,
edalontzien ikara.

Marinelen hitz arrotzak baina
ginebra onaren dirdira,
gau biluzia sartzen zitzaigun
begi zabalen ninira.

Gau erdiaren eremuetan
laban ahoak aidera:
izar isilen argi azpian
odol beroa lurrera.

Txanpon berriez erosi eta
amodioen jokora,
ohe estutan etzuten ginen
itsas lamien altzora.

Kanta lizunen alegrantzia,
akordeoien tristura:
ez gara egundo, ai, itzuliko
oroimeneko portura.