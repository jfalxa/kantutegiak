---
id: tx-2
izenburua: Ke Bonba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GC-U894dCxg
---

Bideoa:
 Igora Films, Iñaki Gereñu, Nerea Garmendia, Iñigo Etxarri

Audioa: 
Abeslariak: Iker Lauroba eta Lide Hernando
Gitarrak eta sinteak : Iker Lauroba
Bateria, sinteak, grabaketa eta produzioa: Josu Erviti (Erviti Studio)

Letra:

Dantzalekuan bakarrik 
lasai nago
Zu ondoan egon barik 
lasaiago.
Gorde zuretzat sermoiak 
nahiz ta kosta,
hartu zure hitzak ta alde 
egin ospa!

Ez naiz etorri 
erretolikak entzutera, 
ke-bonba jaurtiz
 banoa ziztuan etxera.
Nik egin nahi dut lo,
soilik nahi dut lo.
Behar zaitut ohe oh oh eh! 
Banoa oyea... oh yeah!

Dantzalekuan bakarrik
 lasai nago
Zu ondoan egon barik 
lasaiago.
Gorde zuretzat sermoiak 
nahiz ta kosta,
hartu zure hitzak ta alde
 egin ospa!

Nabaria da 
gerturatzen ari zarela,
 zoaz beste bati 
antzarak perreatzera!
Nik egin nahi dut lo...

Nahi ta ezin ospa egin, bakarrik.
Egin nahi dut lo,
soilik nahi dut lo...