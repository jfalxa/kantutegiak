---
id: tx-1561
izenburua: Mina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-KGH1rDY3d8
---

Hitzak eta musika: Manu Chao
Hitzen moldaera: Amets Arzallus
Musikaren moldaera: Xabier Zabala

Mama ke mina
ma ze mina
maman j'ai mal
qu’est-ce qui va pas?
poupèe panpina
pupu da mina.

Bizitza mina
Hizkuntza mina
Galtzeko mina

Bruja sorgina
Sorgina mina
Zintzurreraino
Sagar urdina.

Haurtzaro mina
Sagar urdina
Hatz muturretan
Mailu burdina.

Gaizto da mina
Haundi da mina
Hatzetan mina

Gaizto da mina
Haundi da mina
Hatzetan mina

Ama sorgina
Sorgina mina
Zintzurreraino
Sugar urdina.

Aita zurgina
Zurgina mina
Hitz muturretan
Mailu burdina.

Haundi da mina
Bihotzeraino
Hitzetan mina.

Hizkuntza mina
Euskara mina
Amiñi bat
Atara mihina.