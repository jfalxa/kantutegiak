---
id: tx-3288
izenburua: Barnetegian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CmDOKrDEBt4
---

Plentzia-Gorlizko NON OTE taldeak Plentziako Euskal Girotze Barnetegirako egindako abestia. Letra eta musika Xabier de la Maza