---
id: tx-2665
izenburua: Bihotza Emango Nioke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5d-YWW-mDFo
---

Bihotza emango nioke orain norbaitek eskatuko balit.
Gaua da, ordu ezjakin bat, eta telefonoa mutu.
Bizitza emango nioke, behar balu,
goibeldura hau konpartitzen lagunduko lidakeenari.
Telefonoa mutu.
Zergatik erretiratzen zarete denok hain goiz?
Zu esna egonik, zergatik ez diozu leihoa irekitzen telefonoari?
Bihotza emango nizuke, badakizu.