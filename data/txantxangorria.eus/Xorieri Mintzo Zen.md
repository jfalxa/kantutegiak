---
id: tx-2928
izenburua: Xorieri Mintzo Zen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LikVtT0igO0
---

Xorieri mintzo zen
Mintzo zen errekari
Oihaneko zuhaitzeri
ta zeruko izarreri
Mintzo zen haizeari
Xoro batentzat zaukaten
Xorieri mintzo zen
Xoro batentzat zaukaten.

Ez zakien irakurtzen gizonen liburutan
Bainan ongi bazakien zeruko seinaletan
Zeruko seinaletan ta jenden bihotzetan.

Xorieri mintzo zen
Mintzo zen errekari
Oihaneko zuhaitzeri
ta zeruko izarreri
Mintzo zen haizeari
Xoro batentzat zaukaten
Xorieri mintzo zen
Xoro batentzat zaukaten.

Haurrek zuten Harrikatzen zahar gaztek trufatzen
Etxekoek berek ongi laneko baliotzen
Laneko baliotzen eta gosez pagatzen.

Xorieri..

Hil da xorua bakarrik bakarrik da ehortzi
Ez zeri han Kristau bat ere salbu lau hilketari
Salbu lau hilketari ta apeza kantari.

Xorieri...

Hil hobian ezartzean haizea zen gelditu
Xoriak ziren ixildu zerua zen goibeldu
Zerua zen goibeldu ta jendea oroitu.

Xorieri...