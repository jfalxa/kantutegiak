---
id: tx-3043
izenburua: Nahiz Eta Heriotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vnnPJ8TYgAM
---

Nahiz eta heriotza
helduko zaidala jakin
ez dut
haatik
horren aurka
buru-bihotzez borrokatzetik etsiko.
ez nau
ustekabean harrapatuko!
bizi naizeno bizitza kantatuko dut.
eta behar dudan
denbora baino lehen
hiltzen banaute
nere hortzetan itzaliko den azken antzia
beste batenetan
loratuko den
lehen irria
izango da.