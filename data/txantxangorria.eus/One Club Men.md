---
id: tx-530
izenburua: One Club Men
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N6u4V7grXDY
---

Betidanik izan naizela
Ta izango naiz
Zuri leial
Txikitatik erakutsi zenidan
Zer den amestea

-One club men!-
-One club men!-

Ideia zahar baten morroia naizela
Esan dezakete
Baina idei horren atzean
Herri oso bat daukagu

-One club men!-
-One club men!-

Ta eskaintzen dizkidazun distira urrunek
Ez dute ezer balio niretzat
Lur zati honetan tinko ditut oinak
Ta ez nauzue mugituko erraz

One club men!
Ez zaitugu inoiz bakarrik
One club men!
Bidea egiten utziko
One club men!
Harmailaren eztarrien
One club men!
Eztanda horrek bultzatzen nau

Orsai - One club men