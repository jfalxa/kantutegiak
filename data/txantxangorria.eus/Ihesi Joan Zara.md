---
id: tx-3020
izenburua: Ihesi Joan Zara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bdk66Ss6qC8
---

Ihesi joan zera, 
biluz utzi nauzu, 
ziur maite zaitut nik,
bakardadean menpe gelditua naiz orain, 
ni naiz, ez al zera ohartzen? 
Bihotz mindua negarretan dario,
sufrimendua ezinaren lantua...
maite zaitut oraindik!!
Zu joan eta ni hemen 
barrua deseginda,
zertan utzi nauzu horrela?
Nitaz desegin nahia 
zure desio dena,
zatoz, andereño laztana.
Bihotz negartua negarretan dario, 
sufrimendua ezinaren lantua...
maite zaitut oraindik!!