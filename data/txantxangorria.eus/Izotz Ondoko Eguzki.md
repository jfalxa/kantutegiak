---
id: tx-2113
izenburua: Izotz Ondoko Eguzki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tevbXrpxEbE
---

Xabier Letek (Oiartzun, Gipuzkoa, 1944-Donostia, 2010) euskal kantagintza modernoan izan duen eragina itzela da. Ez Dok Amairu taldearen sortzaileetakoa izan zen, baina musikaria baino gehiago idazlea zen; horregatik, kantari askok haren hitzak erabili dituzte. Horien artean, birekin elkarlan estua izan du: Lourdes Iriondorekin eta Antton Valverderekin. Poeta nabarmena izaki, bertso zaharrak berreskuratzen ere lan handia egin zuen.