---
id: tx-1955
izenburua: Bide Ertzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iuth-ktbenc
---

bide ertzean topatu ditut 
poesia, kantua eta desioa 
jeloskor gordetako leloa 
anai fidelaren boz herabea 
bide ertzean ikusi ditut 
galduta amets eraitsiak 
eskean bide ertzean 
aukera berri baten esperoan 
bide ertzean irentsi ditut 
bakardadearen argitan 
idatzitako lerroak 
poza eta geroko poemak 
zutaz kantatzeko jaio bide nintzen mundura