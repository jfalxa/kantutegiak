---
id: tx-927
izenburua: Biarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1dTweWASyqk
---

Hemen nagola berriro
berdin berdina eitxen
bixar be hemen egongo naiz
berdin berdina eitxen
Hauxe duk hauxe
Hauxe duk eta  kaka zaharra
eguneroko biharra
Uoh, oh, oh, oh, oh, oh
Bixar be hemen egongo naiz
berdin berdina eitxen
Hauxe duk hauxe
bixar be hemen.
Bixar be hemen.
Bixar be hemen.