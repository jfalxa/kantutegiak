---
id: tx-132
izenburua: Gaur Euria Egin Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0BFmfT2t_Rg
---

Gaur ez zaitut ikusi
Ez didazu deitu
Eta orduak zurekin
Bezala eman ditut.
Gaur maitasunaz eta
Heriotzaz hitz egin dugu
Eta tximeleta eri bat
Gelan sartu zaigu.
Gaur ez zaitut ikusi
Ez didazu deitu
Eta orduak zurekin
Bezala eman ditut.
Gaur maitasunaz eta
Heriotzaz hitz egin dugu
Eta tximeleta eri bat
Gelan sartu zaigu.
Gaur ez zaitut ikusi
Ez didazu deitu
Eta orduak zurekin
Bezala eman ditut.
Gaur euria egin du...
Gaur euria egin du...
Gaur euria egin du...
Gaur euria egin du...
Gaur ez zaitut ikusi
Ez didazu deitu
Eta orduak zurekin
Bezala eman ditut.
Uuuuuuh...
Uuuuuuh...
Uuuuuuh...
Uuuuuuh...
Gaur euria egin du...