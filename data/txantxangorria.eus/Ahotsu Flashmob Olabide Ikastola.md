---
id: tx-3347
izenburua: Ahotsu Flashmob Olabide Ikastola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wk5iDleaOFg
---

Baratzatik ikastolara (Raul Romo-Olabide ikastola)

Egunero ikastolara 
joaten gara dena ikastera
Egunero ikastolara 
lagunekin jolastera

Baratzatik jolastokira
sekulako txalaparta
denon artean euskara bultza
esan denok Olabide aurrera!