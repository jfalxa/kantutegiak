---
id: tx-2132
izenburua: Malen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xaCwt6LdRH4
---

Ken Zazpi (edo Ken 7) pop melodiak eta rockaren indarra uztartzen dituen talde bizkaitarra da. Azken urteetako talderik arrakastatsuenetako bat da, disko bakoitzetik milaka ale saltzea eta emanaldi jendetsuak eskaintzea lortu baitu. Estraineko lana 2001ean kaleratu zuten, Atzo da bihar (Gor), eta arrakasta itzela izan zen. Guztira 20.000 ale baino gehiago saldu ziren, eta taldea Euskal Herrian oso ezaguna egin zen. "Zenbat min" abestiak ate asko ireki zizkion, baita Lugarri taldearen "Ezer ez da betiko" abestiaren birmoldaketak ere. Gainera, beste bi bertsio dakartza diskoak, Pennywise ("Larrun") eta Muse ("Irri bat") taldeenak. Bi urte eman zuten lan hau zuzenean aurkezten eta 2003ko apirilean Bidean (Gor) argitaratu zuten. Aurrekoa baino soinu gogorragoa du, rockzaleagoa, baina Ken Zazpiren musikaren esentzia den melodia albo batera utzi gabe. Esan daiteke aurrerapausoa izan zela bigarren diskoa, taldea bere potentzialean sinesten hasi zen eta kontzertu asko eskaini zituen, "nahi adina", taldekideen hitzetan. Gor diskoetxearen arabera, disko honetatik 18.000 ale saldu ziren.