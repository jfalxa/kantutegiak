---
id: tx-1830
izenburua: Zapato Azule 2013
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zAkhzCGlMRc
---

Gili-gilixak etxestazuz hatzamartxukin ulin 
Hain matxagarri zaz arrantzalin erropa azulin 
Eta biztuxun Radixu 
Pasakorou... Geu bixok Auiminyulinblu!
Faltaten dizenak gogun hartute kalin barrena
Herritxik sorturana baño ez ei de herrixana 
Eta biztuxun Radixu 
Pasakorou... 
Geu bixok Auiminyulinblu!
Eta jaixan "lo bailao bailao esta" klaro gelditxu daixule 
Behintzat ez eixule esan gero aurrez abisa ez netsule 
Seguru na ta zeuri be barrurarte ailako gatsule 
Ondarru kolorez janzten daben Zapato Azule!!
Eta jaixan "lo bailao bailao esta" klaro gelditxu daixule
Behintzat ez eixule esan gero aurrez abisa ez netsule 
Seguru na ta zeuri be barrurarte ailako gatsule 
Ondarru kolorez janzten daben Zapato Azule!!