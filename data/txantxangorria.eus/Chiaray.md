---
id: tx-2464
izenburua: Chiaray
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/14kdsV8RCYI
---

Musika: Herrikoia (Sahara)
Letra: Herrikoia eta Koldo Celestino


Gelt ana ala fejra, deini dana
Bi klam el gaiwana, deini dana
Gulu ya ahel Sahara, deini dana
Gulu ya al fannana gulu emaana

Sahara ya trabi
ya bai ah babi
ya menzel leiyam
ya fali mensabi
enich fi trabi
nesken wa yah
Irribarrez,
poztasunez
bizi gara

Irrintzika eta
hainbat jolasetan
desertuan gabiltza
irribarrez
poztasunez bizi gara

Tfut aalina leyan
Hizkuntza dugu
Sahara!!!