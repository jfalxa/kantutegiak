---
id: tx-1143
izenburua: Piztuko Dugu Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mMqOg0IBkV4
---

PIZTUKO DUGU EUSKARA

Non gaudela galdetzen digutenean
Ez da erraza izaten erantzuna
Euskal herriko Nonbait-en kokatuta
Motorrez heltzeko leku erreza

Gure hizkuntza nahi baduzu dastatu
Mugitu zaitez gure herri honetara
Nahiz eta zati txikitxoetan batu
Uzta ikaragarria izango da

“Ez Dok Amairu”ko mikelen hitzetan
Ezin gara soilik hezurrez osa
Hezurrek bihotza behar duten moduan
Guk Behar dugu hizkuntza

IKASTOLATIK KALERA BIHOTZETIK MINGAINERA
HAUSPOARI ERAGINDA PIZTUKO DUGU EUSKARA
LEIZETATIK LANDETARA BARNEALDETIK KANPORA
HAUSPOARI ERAGINDA BIZITZAZ BETEKO DUGU ARABA... EUSKARAZ

Denok batera eta denok lotuta
Mingainetatikan mingainetara
Euskaraz pentsatu eta euskara maitatu
Eta euskaraz bizi zure eguna

“Ez Dok Amairu”ko mikelen hitzetan
Ezin gara soilik hezurrez osa
Hezurrek bihotza behar duten moduan
Guk Behar dugu hizkuntza

IKASTOLATIK KALERA BIHOTZETIK MINGAINERA
HAUSPOARI ERAGINDA PIZTUKO DUGU EUSKARA
LEIZETATIK LANDETARA BARNEALDETIK KANPORA
HAUSPOARI ERAGINDA BIZITZAZ BETEKO DUGU ARABA... EUSKARAZ

Xuxurlatu, abestu, oihukatu gure altxorra defendatuko dugu
Erabili, Aizu Mari Pili, Araban posiblea da, euskaraz ibili!