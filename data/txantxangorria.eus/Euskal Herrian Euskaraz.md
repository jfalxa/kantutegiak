---
id: tx-3154
izenburua: Euskal Herrian Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MtSsaLAmCxg
---

Euskal Herrian euskaraz
nahi dugu hitz eta jolas
lan eta bizi euskaraz eta
hortara goaz,
bada garaia noizbait dezagun
guda hori gal edo irabaz.
Zabal bideak eta aireak
gure hizkuntzak har dezan arnas,
bada garaia noizbait dezagun
guda hori gal edo irabaz.

Euskal Herrian euskara
hitz egiterik ez bada
bota dezagun demokrazia
zerri askara
geure arima hiltzen uzteko
bezain odolgalduak ez gara.
Hizkuntza gabe esaidazue
nola irtengo naizen plazara,
geure arima hiltzen uzteko
bezain odolgalduak ez gara.

Euskal Herri euskalduna
irabazteko eguna
pazientzia erre aurretik
behar duguna,
ez al dakizu euskara dela
euskaldun egiten gaituena?
Zer Euskal Herri litzake bere
hizkuntza ere galtzen duena.
Ez al dakizu euskara dela
euskaldun egiten gaituena?