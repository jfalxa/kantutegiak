---
id: tx-931
izenburua: Buruz Behera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uFe51r8OfCg
---

Zuzendaritza, kameralaria eta edizioa - AIORA PONCE
Produkzioa - IORITZ IPIÑA

BURUZ BEHERA

Batzutan azkar doa berez geldi dagoena, 
dena desberdina da nor bere arabera.
Ba al dakizu ze forma duen munduak
buruz behera? 

IKUSI NAHI BADEZU DESBERDIN, 
SENTITU NAHI BADEZU DESEGIN. 
TXORIAK BURUAN, HANKAK AIREAN, 
IGO NAHI BADEZU NERE GAINEAN. 

Itsasoa zeru da eta zerua itsaso, 
seia bederatzia izan daiteke akaso. 
Egi berrien bila biziko gera 
buruz behera.

IKUSI NAHI BADEZU DESBERDIN, 
SENTITU NAHI BADEZU DESEGIN. 
TXORIAK BURUAN, HANKAK AIREAN, 
IGO NAHI BADEZU NERE GAINEAN. 

IKUSI NAHI BADEZU DESBERDIN, 
SENTITU NAHI BADEZU DESEGIN. 
TXORIAK BURUAN, HANKAK AIREAN, 
IGO NAHI BADEZU NERE GAINEAN. (x2)