---
id: tx-768
izenburua: Zaldiak Negarrez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/526KiC-kN_k
---

Eskean Kristö taldearen "Beste belaze batzuen gozoan" diskotik ateratako lehen singlearen bideoklipa.

Bideoa: Arrigurik Platanoboligraforen laguntzarekin

Aktoreak: 
Julene Gregorio
Ane Rot

Arrate Estudioan:



"Nire bekokiak ondo daki
Zure pottokaren abenturen berri
Adar bakarra naiz orain trostan
Beste belaze batzuen gosoan
Badakizu badakidala
Bai, badakit badakizula
Haragi ondoko zigarroaren kearen beroa
Ez da zurea
Bitxilore bila pasatu dut gaua
Tentazioari zo ilargiari begira
Arramaska ekarri, arramaska lepora
Bihotza sor dizut baina
Probatu enuen sua
Bihotza sor dizut baina
Arramaska lepora".