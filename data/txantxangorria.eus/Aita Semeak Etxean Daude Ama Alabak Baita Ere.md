---
id: tx-2278
izenburua: Aita Semeak Etxean Daude Ama Alabak Baita Ere
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U8JBB1ejh2k
---

Aita-semeak etxean daude
ama-alabak baita ere
Aita-semeak etxean daude
ama-alabak baita ere
Beti korrika
beti presaka
ezin lasai bizitzea
orain etxean egon behar dugu
hobe aprobetxatzea

Aita-semeak etxean daude
ama-alabak baita ere
Aita-semeak etxean daude
ama-alabak baita ere.