---
id: tx-1160
izenburua: Ez Badugu Sinesten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BvqC8bKhoNk
---

Aitor dezagun
Irakasleak ondo bizi gara.
Bueno ez. Irakasleak oso ondo bizi gara.
Zu adibidez, irakasle, zertaz kexatzen zara?
Gabonak, aste santuak, udara…
Ei! Aurten nora joango zara oporretara?
Beti bezela, entzungo dugu guregatik esaten dutela…
Irakasleak!
Zuek bai bizi zeatela!
Jendeak ez dakiena da hizkuntza teknikariak bizi garela hobeto askoz.
klase orduen ordez apropos
Ulibarri puntu infon sartzen gara gogoz,
ikastetxean euskara indartzeko asmoz.
Eta denok, aitor dezagun, denok, teknikari denok hartzen dugu poz,
libratzen garelako (ordu batzuz) behingoz
granoz eta orgasmoz betetako ikasle panpox mordoz.
MEKAUEN SOS!!
Eta guk, teknikariok, nola dena badakigun eta
nola badugun soziolingustikaz nahikoa ezagupen
irailean hasten gara datutegia erantzuten
Esateko ze arraio egin behar dugun aurten Berritzegunean
belarritik tira ez diezaguten!
Eta bide batez
ea irakasle, guraso eta batez ere ikasleek
euskaraz egiten duten
pentsatuz, gaizki esango dut baina,
ze hostia egin beharko genukeen
euskarak eskolan ere jarraitu dezan bizirauten.
Bide batez… yake asko gean irakasliak hemen…
Zergatik hainbeste azterketa, suspentso, bronka eta negar!
Eskolak ez al du ba zorintsu izaten erakutsi behar?
Ni euskaraz naiz hemen eta orain,
nire orbain, dohain eta nire izerdi usain.
Bai, ni euskaraz naiz hemen eta orain,
eta benetan diot.
Euskararik gabe
ez nintzateke zoriontsu,
orain naizen bezain.
Zaila egiten da euskararekin egiten ari garena miresten
formalidadean eta diskurtsoan ari gara terrenoa irabazten
baina etxean, ai etxean, problemak ditugu arnasari eusten.
Alferrik gabiltza kanpoeder eskaparateak ederresten
ez badugu garena hizkuntzak ematen digula lehenesten!
Baina nola oraindik asko horretaz konturatzen ezten
guk, hemen gaudenok, izan beharko dugu eragile eta ezten
luzaroan segi dezagun euskaraz eta euskara amesten.
Baina esaidazue.
Nola euskaldunduko dugu eskola,
lortu genezakeela ez badugu sinesten.
Gure konpromisoak izan behar du tinkoa:
gurea ere bada erantzukizun politikoa, linguistikoa
Horregatik ari gara honetan lanean
ehunka lagun, ehunka argi, ehunka bihotz
eta denoi egin nahi dizuet hots:
Eskolara itzultzean,
bihar ez bada etzi
sinetsi zuek bezala lanean ari garenak ez garela gutxi.
Eta sinisteko,
sinisteak bai baitu garrantzia,
gu ere bagarela… superpotentzia.
Gurea erokeria bat da.
Ederregia gezurra izateko.
Eta bidaia lagun nahi zaituztet nik.
Alboan,
gogoan,
elkarren abaroan
Geroa euskaraz behar baitugu argizta!
Izan gaitezen ehundaka lagunak, ehundaka bihotzak, ehundaka argiak baikortasun txispa
Izan gaitezen normalkuntza teknikari
eta ilusionista.