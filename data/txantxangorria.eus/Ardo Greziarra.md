---
id: tx-8
izenburua: Ardo Greziarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6ZFNmaiu-Qw
---

@zutikeuskalkantak5674 


Greziar Ardo
Ilundu zuen hiri handiko kale hotzetan
bero bila sartu bainintzen ostatu hartan
gizon bat hasi zitzaidan eleketan.
Erbestean urtetan zela zidan kontatu
herri minak bihotza nota zion samindu
Greziako lur maitea ezin ahaztu.

Hurbil hadi, topa nirekin oroitzapenari
Greziako ardo beltz gozoari
itsasoz bestaldeko sort herriari.
Hurbil hadi kanta nirekin, ez utzi bakarrik
ez duk munduan jauregi ederrik
etxe goxoa balio duenik.

Hondogabeko putzua zen haren tristura
Irrian gorderik zeraman etsipen hura
Umezurtzen sabeleko urradura.
Edan genuen Dionisosen nektar gorria
Baita dantzatu Sirtaki ta pasodoblia
Hura zen bai mozkor ikaragarria

Argitu zuen hiri handiko kale hutsetan
Hamar bat ezkila nituen buruan dantzan
Doinu zahar haren buzukiak bailitzan
Ez dut Greziako lagunaz berririk jakin
Opa diot etxekoekin elkartu dadin
Ulises bezalaxe Penelopekin.