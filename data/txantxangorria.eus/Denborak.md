---
id: tx-385
izenburua: Denborak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bfgE2JhplYM
---

Denborak aldatzen dira
denborak aidatzen doaz
denboragileak ari dira
denbora berria prestatzen;
eta zu, jadanik,
hezituta zaude
zure denboraren ariketarako
gizalege berrien baitan sinismen lausoz.

Eskaparate berria irikita dago,
bertan, izkribu dizdiratsuan
irakur dezakezu:
" etorkizuna zurea da",
eta atzekaldean,
mundu koloretsu bat dago
eta zureganatu nahi zenduke
zure bizitzaren arrastoa uztearren.

Sartuko zara;
eta esku batzuengandik
zure denbora ttipiaren araudia
jasoko duzu;
eta oroimena,
arriskuaz ohartarazteko sortua zena,
garaiko xedeak bereganatzen
lanpetuegia izango da ta
etzaitu ohartaraziko
laberintoaren atarian zarenean.

Eta denborak aldatuz joanen dira;
denboragile berriek
liluramendu berriak sortuko dituzte,
eta zuk,
zure denbora zaharkituaren hatzaparretan,
auzirik gabeko gudaria,
menpeko aberea,
nehoiz ulertu gabeko
askatasunaren kantura
joko duzu,
bizitza iheskorraren
ardo mikatzetik
edaten duzun artean.

Eta zure denboraren ikuspidea
duzula esango duzu.
Bainan, zilegi bekit esatea,
ez dela denboraren morrontzarik
deus adierazi duenik;
eta edozein argi izpiaz
ohartu ahal izan zinenean,
hura, ernaitasun domagaitzetik
jalgia izan zen.