---
id: tx-229
izenburua: Gaztelu Baltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wfSjHHw2F0g
---

Brigade Locok eta Nabarralde Fundazioak Amaiurko konkistaren 500. Urteurrena oroitzeko elkarlanean egindako proiektua. Euskal Herriaren askatasunaren alde bizitza eman dutenei eskainia.

Irudiak 2022ko martxoan Hodei Izarra eta Aingeru Bidaurretaren eskutik grabatu dira. Abestia Iruñeako Sound of Sirens estudioan grabatu eta nahastu da Julen Urzaizen eskutik. Bertan, Skabideaneko Idoia Tapiak kolaboratu du.


Amaiur gaztelu beltz hori
Amaiur gaztelu beltz hori
-berrehun gudari oso sumin-
zaintzen zaituben zaldun onak
Naparra-aldez egin dabe zin.
berrehun  gaztelu beltz hori

Odo/ez betetako herri bat
Baztango lur zabaletan
ELKARTUA
Maite zuten uralagatik
beraien nortasunagatik
Harro ziren, ta burua tente
Sentitzen zutenagatik
Borrokatzen
ez pentsa den handiko daturik
Geroa axola barik
AXOLA BARIK!
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Oroimenean garai beltza beti
Arrano beltza lagun izanik
Etsai traidore guztiei
AURREGIN
Lu/rralde baten arima da besterik
Ta herria defendatzen
Amaiurko zaldunen arimen garra
Ezta inoiz itzaliko
Gu/e artean bizirik egongo dira
Haien ohoreak betiko dira
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Oroimenean garai beltza beti

500 urte ondoren darrai su gar biziak
Amaiurren piztutako argiak
bizirik mantentzen du herria
Batera gara guda honen parte
Borrokan jarraitu ezean ez gara
inoiz izango aske

JO TA KE GARAIPENERA ARTE!
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi
Gaztelu beltza bihurtu zen gorri
Aberriagatik erori eta jaiki
Beraiengatik zutik eutsi

Oroimenean garai beltza beti