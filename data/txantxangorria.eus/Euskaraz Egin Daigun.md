---
id: tx-2839
izenburua: Euskaraz Egin Daigun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AMEvyGjSq6k
---

EUSKERAZ EGIN DAIGUN
Aspaldian Basauriko auzo guzti-guztietan
euskeraz hutsean egiten zen bizimodua,
baina denbora ilunak etorri ziren gero
gure hizkuntza zaharra baztertu guran,
erdara nagusitu kaleetan zehar
Debekatu euskuen geure berbakera ta kultura
euskera izkutatu zen sukaldeetan eta
amama eta aititen kondairetan.

EUSKERAZ BADAKIZU, EUSKERAZ BADAKIGU,
ZERGATIK ERDERAZ BERBA EITEN DOZU?
EUSKERAZ BADAKIGU, EUSKERAZ BADAKIZU,
ZERGATIK ERDERAZ BERBA EGITEN DOGU?

Aizu, ez lotsatu ta egizu berba
eiten ez den euskera da motzena
nahiz ta hanka sartu, egin saiaker,a lagun
gure berben beharra du eukerak.



Lagun zaitudala dakidan arren,
izan zaitez be bai euskeraz lagun,
batu gaitezan danok eta oihukatu daigun
Basaurin be bagara, bagara euskaldun

EUSKERAZ BADAKIZU, EUSKERAZ BADAKIGU,
ZERGATIK ERDERAZ BERBA EITEN DOZU?
EUSKERAZ BADAKIGU, EUSKERAZ BADAKIZU,
ZERGATIK ERDERAZ BERBA EGITEN DOGU?

POZOKO, BERBA LAGUN!!
BASOZEN, BERBA LAGUN!!
KALERON be euskeraz egin daigun!!
SAN MIGELEN, BERBA LAGUN!!
AZBARREN, BERBA LAGUN!!
ARIZEN eta URBI bagara euskaldun!!
SOLOARTEN, BERBA LAGUN!!
ARIZGOITIN, BERBA LAGUN!!
SARRATUN be euskeraz egin daigun!!
LAPATZAN, BERBA LAGUN!!
ARTUNDUAGAN, BERBA LAGUN!!
BASAURIN be bagara, bagara euskaldun...