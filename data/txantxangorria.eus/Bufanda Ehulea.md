---
id: tx-2127
izenburua: Bufanda Ehulea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vaDcfzmzK2A
---

Bufanda ehulea (Hitzak eta musika: Pako Aristi, Mikel Markez)
Mikel Markezek  "Zertarako mugak jarri" argitaratu zuen (Elkar, 1995). Maialen Lujanbiok eta Jexuxmari Irazuk idatziriko abestiak eman zion diskoari izena. Argitalpen horrekin, Pako Aristi idazlearekin elkarlanean hasi zen Markez, hark idatzi baitzion "Gabon arboletako" kantua.