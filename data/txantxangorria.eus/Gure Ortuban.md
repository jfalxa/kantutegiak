---
id: tx-2669
izenburua: Gure Ortuban
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1TwrX-riLSk
---

GURE ORTUBAN

Gure ortuban sagarra loran

 Donianeko eldurik,

 aren azpijan uso bi dantzan

 ederrik eta galantik.

 

 Eskriban zarrak iru alaba

 irurak sedaz jantzirik;

 iruretatik ederrenari

 banentso palabraturik.

 

 Soñeko eder bat eingo neuskio

 urrez ta barraz beterik.

 «Eskerrik asko galant galanta

 eztot neuk gona biarrik».

 

 «Zazpi arkako utxea daukat

 neuk estrenatu bagarik,

 zortzigarrena gorderik daukat

 urrez ta barraz beterik».