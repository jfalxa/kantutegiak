---
id: tx-1291
izenburua: Txoko Iluna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jj45YZHkR6I
---

Garilak 26 erromeria taldearen bideoklipa. Kolaborazioak: Xabi Solano (Esne Beltza) Maria Rivero (Hutsa, Siroka) Urtzi Iza (Enkore) Pello Reparaz (Vendetta) Txufo Wilson Zuriñe Hidalgo (Hesian) www.baga-biga.eus


Ez ahaztu txanpon baten moduan,
bi aurpegi dauzkadala
bata ezagutzen duzun arren,
beste bat existitzen da.
Gauetan izaten naiz dantzari,
kantari,, pozgarri batzuetan.
Era berean alda naiteke, bortizki.
Modalak pikutara.

Orratz zorrotz gainean dantzan,
eta zu, gozamenaren zaindari,
bidaia honetan gidari.
Zure erabakia da, eman pauso bat aurrera!
defenda dezagun elkarrekin maite duguna,
argizta dezagun tristetzen gaituen txoko iluna.

Aspaldi ezagutzen zaitudala, ongi badakizu.
Askotan adarra jo dizudala, onartu ezazu.
Ajean izaten naiz mingarri, 
nekagarri, garesti batzuetan.
Baina hurrengo batean badakit topatuko garena.

Orratz zorrotz gainean dantzan,
eta zu, gozamenaren zaindari,
bidaia honetan gidari.
Zure erabakia da, eman pauso bat aurrera!
defenda dezagun elkarrekin maite duguna,
argizta dezagun tristetzen gaituen txoko iluna.