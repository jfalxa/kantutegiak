---
id: tx-1599
izenburua: Revolta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JXhfvrNgMUA
---

Eh!
Eh!
Eh!

Oo/oo/oo/oohhh! Piz/tu da su/a!
Oo/oo/oo/oohhh! Pa/sa me/zu/a!
Oo/oo/oo/oohhh! Bo/rro/ka/tze/ra



El pueblo no deberia temer a los gobernantes
los gobernan/es deberian temer al pueblo

REVOLTA!

E/ro/so/ta/su/na/ren, mo/rroi zin/tzo ta le/ial
Sol/da/ta/pe/ko es/kla/bu/tzan.
Mc Do/na/li/za/tu/rik
A/hu/la/re/kin ol/dar/tzen di/re/nak,
bo/te/re/tsu/e/kin ki/kil/du

REVOLTA!
REVOLTA!
RE/VOL/TA!

Al/pis/tez e/li/ka/tu, ka/ka/tu/a bi/hur/tu
Sal/du di/gu/ten ge/zu/rra
e/ro/si du/gun mo/to/rra


Oooooooohhh! Piztu da sua!
Oo/oo/oo/oohhh! Pasa mezua!
Oo/oo/oo/oohhh!
Bo/rro/ka/tze/ra kon/de/na/tu/ak

Oo/oo/oo/oohhh! Piz/tu da su/a!
Oo/oo/oo/oohhh! Pa/sa me/zu/a!
Oo/oo/oo/oohhh!
Bo/rro/ka/tze/ra kon/de/na/tu/ak

So/lo un pue/blo or/ga/ni/za/do
po/drä lo/grar sus ob/je/ti/vos

RE/VOL/TA!








Eu/ro-/Do/la/rren men/pe
bu/ru/a ma/kur/tze/a/ri u/tzi
Kon/tzi/en/tzi/ak i/rau/li, al/da/ke/ta e/ra/gin
Ha/se/rre/tu zai/tez, su/min/du, a/sal/da/tu,
as/koz ge/hia/go bai/ka/ra
In/su/mi/so/ak de/kla/ra/tzen ga/ra
bel/du/rra/ren dik/ta/du/ran

Oo/oo/oo/oohhh! Piz/tu da su/a!
Oo/oo/oo/oohhh! Pa/sa me/zu/a!
Oo/oo/oo/oohhh!
Bo/rro/ka/tze/ra kon/de/na/tu/ak

Oo/oo/oo/oohhh! Piz/tu da su/a!
Oo/oo/oo/oohhh! Pa/sa me/zu/a!
Oo/oo/oo/oohhh!
Borrokatzera kondenatuak

RE/VOL/TA!
E/RRE/BOL/TA!

REVOLTA!
R/VOLTA!
REVOLTA!
REVOLTA!