---
id: tx-2250
izenburua: Gauerdi Gorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T6nHBaPUuu8
---

Abestia / Tema: Gauerdi Gorri
Albuma / Album: Izaera (2020)
Musika eta letra / Música y letra: Skabidean
Musikaren grabazioa, nahasketa eta masterizazioa / Grabación, mezcla y masterización de la música: Kaki Arkarazo (Garate Studios, Andoain).
Animazioak / Animaciones: Jon Gesta

-------------------------------------------------------------------------------------------------------------------------
Taldearen kontaktua / Contacto del grupo: 610887684
Posta elektronikoa / Correo electrónico: skabidean.blog@gmail.com

------------------------------------------------------------------------------------------------------------------------

LETRA [EUS]
Goizaldeko lehen argiekin
nabarmentzen naiz ispiluan,
badakit ni neu naizela barruan dagoena,
baina ezin ezagutu nire islada.

Isilpean igarotakoek
lainopean itsu naukate,
ilunetan bihotza nabaritzen dut zurruntzen,
egun oskarbiak noiz iritsiko ote?

Gauerdi gorri, goizetan argi,
noiznahi ere euriaz busti,
arimak loratzen nahi badituzu senti
denetarik beharra da ikusi.

Beharbada barneko ekaitzak
askatzea ez da makala…
Su berriaz piztu bularraldeko kriseilua
ta atseden har dezala premia izatean.

Aspaldian haize bolada batek
honako hau xuxurlatu zidan:
“xima guztiak loratzen dira noizbait bizitzan,
zurea zaintzen baduzu indartsu ha