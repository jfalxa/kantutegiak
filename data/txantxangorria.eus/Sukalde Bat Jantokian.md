---
id: tx-873
izenburua: Sukalde Bat Jantokian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xBWZXDdd4ao
---

Hitzak: Miren Amuriza 
Musika: Txapito Guzman eta Ibarrako langostinoak 
Bideo Produkzioa: Zirriborro 
www.bertonbertokoa.eus
 bertonbertokoa@gmail.com 
Twitter: @bertonbertokoa 
www.facebook.com/bertonbertokoa 
BERTON BERTOKOA, Durangaldeko eskoletako jantokietan gure seme-alabei ematen zaien elikadura-kalitateaz arduraturik gauden durangaldeko hainbat ikastetxetako gurasoak gara.


Lehen tomate gorri-gorriak
eta indaba beltzaranak
ortutik hara jaten zituzten
aititak eta amamak.
Orain eskolan jaten ditugu
garbantzu eta ilar latak,
zaporerik ez duten letxugak
eta poltsako patatak.
Sukalde bat jantokian! Sukalde bat jantokian!
Lapiko-hotsa nahi dugu gure eskolako jantokian!
Marixabelen arrautzak eta
Lorentzoren haragia,
kanpokoa barik nahi dugu hemengo
ekoizleen janaria!
Sagarra azal berde eta guzti,
laranja azala kenduta,
hozkailukoa barik nahi dugu
sasoian sasoiko fruta!
Sukalde bat jantokian! Sukalde bat jantokian!
Lapiko-hotsa nahi dugu gure eskolako jantokian!