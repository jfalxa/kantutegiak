---
id: tx-2742
izenburua: Etxera Itzuliko Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R4uGU56Z4m4
---

Hitz egin nizuelako
berriz etxera itzuli naiz
nahiz eta isilik egon
zure begirada behar dut.

Urruntasun handienak
banatzen gaituen arren
bihotzetik hurbilen
zaudetenak zarete.

Iraganeko ametsetan,
erlojua lo geratzen da
zure ondoan nengoen eta,
ni, han ume txikia.

Zerura begiratzeko
inongo beharrik ez dut
zuek gertu izateko
gauaren beharrik ez dut.

Iraganeko ametsetan
erlojua geratzen da
zuen ondoan nengoen eta,
ni, han ume txikia.

Berriz nator
bide gurutze elurtu batetik
ahaztuta ez dagoen
denboraren etxea.