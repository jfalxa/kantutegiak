---
id: tx-1866
izenburua: Txikitin Parian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Gos-MpkPAl0
---

Soldadu sartu nintzan
zortzi bat urteko,
Errege serbiduta, txikitin parian paina,
ai libre izateko.

Erregek ez dau behar
gizon ezkondurik
baizikan mutil libre, txikitin parian paina,
ai libre-libretxorik.

Mutilik behar eta
mutilik egon ez
Errege silan dago, txikitin parian paina,
ai jarrita negarrez.

Neskak badaude baina
ez dute balio
Erregeri begitik, txikitin parian paina,
ai negarra dario.

Bergaran nindoiala
Brigadearekin
enamoradu nintzan, txikitin parian paina,
ai damatxo batekin.

Etxezuriko dama
ikusten badozu
gorantziak daiola, txikitin parian paina,
ai esango diozu.

Zeinek hori eman dizun
galdetzen badizu
Felix de Aranburu, txikitin parian paina,
ai esango diozu.

Feliz de Aranburu
hain gizon trebea
Bergaran bilatu dau, txikitin parian paina,
ai beretzat andrea.