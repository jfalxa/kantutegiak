---
id: tx-1403
izenburua: Zumarragako Trena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x8KyRVt2XlY
---

Zumarragako trenan ganien
hegaz noie, librea naz.
Inork ez daki nun nabilen
zer dekoten zeren truke.
Gurutze batetetik, itzalen artetik
inork pentsetan ez daben lekutik
agertzen naz.
Sentitzen banozu, begiratu eizu
zuretzat naz morena, aprobetxatu!
Ein kasu! erdu!
Arrano baltz biren moduen
hegaz eingogu, libreak gara
ta begiradak barre artien
argi gorri bet gure geltokixen.
Ta halako baten, elkarri begire
maitasun jokoak hasiko dire,
bai, ziur nago.
Beso bat hemendik, bestea bestetik
konponduko gara, ez dago dudarik.
Ein kasu!
Ahaztu eizuz kontuek
eizu ein biozune
trena behin bakarrik pasetan da,
behin bakarrik bizixen.
Zeure buruen jabe
eizu ein biozune
trenak behin bakarrik ipingo zaittu
behin bakarrik bidien.
Erdu!
Ahaztu eizuz kontuek
eizu ein biozune
trena behin bakarrik
pasetan da,
behin bakarrik bizixen.
Zeure buruen jabe
eizu ein biozune
trenak behin bakarrik ipingo zaittu
behin bakarrik bidien.
Trena behin bakarrik pasetan da,
behin bakarrik bizixen.
Erdu!
Erdu!
Erdu!