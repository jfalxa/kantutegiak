---
id: tx-3157
izenburua: Herriaren Ahotsa -Skatu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JVSeS7pDGEc
---

Mendeetako historia
duen herri txikia
da gure Euskal Herria

Lautada mendizerra
desertu eta hondartza
Bilbotik Tuterara

Batzuk nahi omen dute
defendatu gure herria
porlanez betez mendiak

Baina guk argi dugu
utziko ez diegu 
AHT gelditu!!

Lemoiz gelditu bagenuen
zergatik orain etsitu
indar nahikoa badugu

Batzuentzat utopia
guretzat ordea beharra
AHT gelditu!!

Herriari galdetu
AHT-rik EZ!!
Herriaren ahotsa entzun
AHT-rik EZ!!

Herrian antolatu
elkarlanean aritu
beharrezko armak hartu

Abiadura handia bai
gure harriena zuen aurka
AHT gelditu!!

Herriari galdetu
AHT-rik EZ!!               
Herriaren ahotsa entzun
AHT-rik EZ!!