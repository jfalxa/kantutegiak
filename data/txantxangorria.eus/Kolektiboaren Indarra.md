---
id: tx-2457
izenburua: Kolektiboaren Indarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p3OWiVrAZ0k
---

' Garraxirik Sakonena ' Diskatik ateratako ' Kolektiboaren Indarra' kantaren bideoklipa.




KOLEKTIBOAREN INDARRA:
Lantegiko langile bat, mubiña eginda baztertzen dute
Sindikatua elkartu da, burges kabroia beldurtu da.
Unian dugu plantoia, euskararen alde ikasle borroka!
Euskaraz ikasten da orain, arrain txikiek jan dute handia!

Ez erori! Kolektiboa gaituk!
Elkarrekin boterea gurea duk! Gurea dun!

Hirian sortu baratza, auzo elkartea lanean da
Porlan proiektuen aurrean, gailendu da asanblada!

Norbere zilborrari ez, amuarrainak begi bi, sei eskailuk hamabi,
kolektiboaren indarra!

Ez erori! Kolektiboa gaituk!
Elkarrekin boterea gurea duk! Gurea dun!
Indibidualismo alienatua kapitalaren mesedera da!
Kolektiboa da indarra!