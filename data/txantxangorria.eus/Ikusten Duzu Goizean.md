---
id: tx-1871
izenburua: Ikusten Duzu Goizean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mMDfHpUNzhQ
---

Ikusten duzu goizean
argia hasten denean,
menditto baten gañean,
etxe ttipitto aintzin xuri bat,
lau haitz haundiren erdian,
txakur txuri bat atean,
iturriño bat aldean
han bizi naiz ni pakean.
Nahiz ez den gaztelua,
maite dut nik sorlekua,
aiten aitek hautatua.
Etxetik kanpo zait iduritzen
nonbait naizela galdua.
Nola han bainaiz sortua,
han utziko dut mundua,
galtzen ez badut zentzua.
Ez da lurrean gizonik,
printzerik ez erregerik,
ni baño hobeki denik.
Ba-dut andrea, ba-dut semea,
ba-dut alaba ere nik.
Osasun ona batetik,
ondasun nahiko bestetik...
Zer gehiago behar dut nik?
Goizean hasiz lanean,
arratsa heldu denean,
nagusi naiz mahaiean.
Kristau bat ona det hartu
nik emaztea hartzean.
Ez du me-egunean
sartuko, uste gabean,
txingar hezurrik eltzean.
Etxean ditut nereak,
akuillu, haitzur, goldeak,
uztarri eta hedeak.
Igazko ale ditut oraindik,
txoko guztiak beteak.
Nola igaro urteak
ematen badu besteak,
ez gaitu hilen goseak.
Landako hiru behiak,
esnez haunditu errapeak,
aratxe eta ergiak.
Bi idi haundi kopeta-txuri,
bizkar-beltz, adar-haundiak,
zikiro, bildots guriak,
ahuntzak eta ardiak...
Nereak dira guztiak!
Ez degu behar lurrean,
errez bizirik etxean,
utzi laguna gabean...
Jende beharrek ez dute jotzen
gure etxeko atean,
nun ez duten mahaiean,
otoruntz ordua danean,
lekua gure aldean.
Pello nere semea,
nahiz oraiño gaztea,
da mutiko bat ernea.
Goizean goizik bazken erdira,
ba-darama artaldea.
Baitu bere egitea,
segituz nere bidea,
ez du galduko etxea.
Nere alaba Kattalin
bere hamaika urtekin,
ongi doa amarekin.
Begiak ditu, amak bezela,
zeru-zola bezain urdin.
Oraingo itxurarekin
uste det, denborarekin,
andre on bat dion egin.
Nere emazte Maria
ez da andre bat haundia,
baña bai emazte garbia.
Muxu batentzat etxean ba-det
nik nahi detan guztia.
Galdegiten det grazia,
detan bezela hasia,
bukatzeko bizia.