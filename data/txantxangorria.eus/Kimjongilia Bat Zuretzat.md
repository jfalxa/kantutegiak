---
id: tx-1222
izenburua: Kimjongilia Bat Zuretzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1gywNJkI8xM
---

Kimjongilia bat zuretzat
eta beste bat neretzat
intensiboki maitatzen ditudan
oparirik gorerenak, nuklearraren kemena
bizkar hezur okzidental hontan
opari hutsetik haratago doaz,
hilezkortasunerantz
kimjongilia bat zuretzat 
eta beste bat neretzat
Songbun-ean gora joan nadin
misil erromantiko bat 
plastikozko garaietan
hitz polit baina ekintza ustelenetan
opari hutsetik haratago doaz,
hilezkortasunerantz

ta berreduka daitezen
garaiz dabiltzan denak
nere bendizioa duzue
kondiziorik gabekoa