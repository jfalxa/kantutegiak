---
id: tx-1605
izenburua: Dabilen Herria Itziarren Semeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vg35NhV_XBk
---

Begiak itxi eta zugana bueltatzen naiz
behe lainoaren dama, portuko itsarargia!
gau izartsu batean elkartuko garela
aunitz maite duenak aunitz sofritzen omen


Itsasoa, mendia, batzen diren tokia
Urepeleko artzai, labegaraien kea
Oroimena gordetzen gudarien basoan
Debekatu ziztuzten argazki ederrenak!

Harrotzen gaituen ipar haizea
Belharra olatua 
Euri tantetatik edaten duen
Euskararen aintzira
Mapetan ikusi ezin dugunez
Sasian libre oihuka
oinez ari gara, herri bat dagoelako!


Faltan botatzen zaitut, barruan eroan arren
Aberririk ez duenak ezin du bere ama maite
Izena daukan orok, izana daukalako
Zaharregia agian txikitasunean handi



Harrotzen gaituen ipar haizea
Belharra olatua 
Euri tantetatik edaten duen
Euskararen aintzira
Mapetan ikusi ezin dugunez
Sasian libre oihuka
oinez ari gara, herri bat dagoelako!

Dabilen harria, herri bat garelako!