---
id: tx-2956
izenburua: Horma Ilunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tT1G8b6UxU4
---

Beste egun bat iluntasunean
legearen aurpegiak berdin jarraitzean
sentimendu hotzak zure barrenean
askatasun indarra apurtu denean

Argi duzu behintzat ezin dela eten
borrokaren indarra inoiz gal ez daiten
eman dezagun zerbait gutxi izan arren
gutxi batzuek dena eman ez dezaten

Aaaaaaa lenengo kolpean
Aaaaaaa jipioen artean
Arnasarik gabe ere egon zarenean
borroka beti bide zure barrenean

Sentipenak, akatu ezin direnak
Askaturik, inoiz ere ez loturik
Zure indarra, barruan duzun garra
Aurrera egiteko behar duzuna da


Sentipenak, akatu ezin direnak
Askaturik, inoiz ere ez loturik
Zure indarra, barruan duzun garra
Horma ilunak hausteko behar duzuna da!