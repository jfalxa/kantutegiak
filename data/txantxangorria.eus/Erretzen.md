---
id: tx-12
izenburua: Erretzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WG3x36aglfA
---

Musika eta hitzak: Zinez
Grabazioa eta nahasketa: Josu Erviti (ErvtiStudio)
Bideoa: Iker Perez

Eskerrik asko bihotzez bideo honetan parte hartu duzuen guztioi, ezin bestekoak zarete borroka honetan❤️

Letra [EUS]
Agian, mendekua egin beharko nuke
Zapalduak izan ziren mila kolore ezberdinen
Agian, agian, agian, agian…

Agian, zelda bat logela
Amesten jarraitzeko joan beharra dut ohera

Barruan dudan jarrera zitala
Kontrolatu ezinik ez da apala
Loria izango zen ni eme ta zu arra
Loria izango zen ni hemen ta sugarra
Erretzen 

Agian, begitan ikusten dena,
Ez da gero errealitatean den dena dena
Agian…

Garaitu nauenaren beldur egia
Mila borrokaz, maitasun bizia
Loak hartu gabe, lirain zitalen jantzia
Jantziak

Lirain zitalen, lirain zitalen, lirain zitalen…
Lirain zitalen, lirain zitalen jantziak

Barruan dudan jarrera zitala
Kontrolatu ezinik ez da apala
Loria izango zen ni eme ta zu arra
Loria izango zen ni hemen ta sugarra