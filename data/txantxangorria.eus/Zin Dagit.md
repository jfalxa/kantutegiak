---
id: tx-413
izenburua: Zin Dagit
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6V-7KHkQJDU
---

Bideokliparen zuzendaria: Nagore Muriel
Hitzak eta ahotsa: Eneritz Aulestia
Dantzaria: Garazi Egiguren


“Zerua beltz, ni geldi
Ez itzali esan ilargiari.
Zerua beltz, zu geldi
Ez zaitez joan,
Egon zaitez geldi.

Nire isla ezagun
Behar luke,
Baina da ezezagun.
Hatzak mugi, laztandu.
Ni neu barna,
Nahi nuke amildu.

Oooo... ooo.....

Eta zin dagizut
Zainduko zaitudala,
Hobeto, itsasoak olatuak baino
Unehoro, maiteko zaitudala
Zin dagizut, nik zin dagit
Nork bestela?

Ni nirekin, ta nigan
Amodio berekoien gisan.
Ez mugatu taupadak,
Irten hortik, zabaldu hegalak.

Ooooo... oooo...

Eta zin dagizut
Zainduko zaitudala,
Hobeto, eguzkiak ilargia baino
Unehoro, maiteko zaitudala
Zin dagizut, nik zin dagit
Nork bestela?

Eta zin dagizut
Zainduko zaitudala,
Hobeto, erregeek printzesa oro baino.
Argitan zein gauetan, maiteko zaitudala
Zin dagizut, nik zin dagit
Nork bestela?”