---
id: tx-832
izenburua: Abiadan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ShfX0vHQDP0
---

Abiadan - Saia Goait (2019)

AME estudioetan grabatua (Mutriku)

Kontaktua: saiagoait@gmail.com
----------------------------------------------------------
/// Bideoklipa ///
Zuzendaria: Eneko Elezgarai Urkiza

Esperoaren gasolina usain zakarra.
Bizi ta maitatzea bisita baten mugatzen, bi ziten artean zenbat bizipen.
Errepide latzetan etenpuntuak eten, abiadan.

Segunduz segundu, minutuz minutu
semearen besarkadak zenbatuak ditu, beratu ditu bere etsi ta min guztiak.
Urduritasunak, kezkak izan arren azal latzean,
ama baten laztanak edonor kokatzen du etxean.

Bizi ta maitatzea…

Mendeku zergen zergatia argia, ordaina senideek,
baina saririk ez dute ezagutzen asfaltoan urtu diren oinek.
Ezerk ez du geldiarazten sustraia bere bidean denean,
ezin hormak jasan arrakalak, ezin, ezin.

Esperoaren gasolina usain zakarra.

Bizi ta maitatzea…