---
id: tx-1197
izenburua: Maite Zaitut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/odVDA4WygEk
---

Koloretako ametsak
esna nagoenean
eta ezin loak hartu
gaua iristen denean.

Saltoka hasita hegan
egiteko gogoa,
nire bihotza taupaka
aire ertzera doa.

Elkarrekin hankaz gora
buruz behera jartzean,
tunelean sartu eta
irrintzika hastean,
bitxiloreekin pultsera
egin dizudanean,
begietara begira
hauxe nahi dizut esan:

Maite zaitut,
maite- maite zaitut;
pila, pila, pila patata tortilla!
Maite zaitut. Maite-maite zaitut,
ilargiraino eta buelta maite zaitut

Begietako dirdira,
irribarrea ahoan,
ezin ditut ezkutatu
zu ikusterakoan;
hanka eta eskuetan,
gorputzean, dardara;
nire bihotza taupaka
aire ertzera doa.
Elkarrekin ortzadarra
margotu dugunean,
ispiluan nire izena
idatzi duzunean,
irratia piztu eta
doinu hau entzutean,
begietara begira
hauxe nahi dizut esan:

Maite zaitut,
maite-maite zaitut;
pila, pila, pila patata tortilla!
Maite zaitut.
Maite-maite zaitut,
ilargiraino eta buelta maite zaitut