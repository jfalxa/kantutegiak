---
id: tx-1048
izenburua: Denboratik Ihes
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dBRgJYb1NkA
---

Aste oso-osoa lantegian
biharko egunik ez balitz bezala
enkargatuak bere ipurdian
muxu bat eman diezaiodala
lepoari buelta erdi eman eta
berak berea muxuka dezala
ni banoa ta hasi dadila jaia
azkenik iritsi da ostirala
Erlojuak egunero
gure atzetik, tak, tik, tak, tik, tak, tik
gaur ihes egin dut
denboratik!
Kontzertutik jaitsi ta riki-raka
garagardo batzuk edan taldean
nahi baino lehenago estutu gara
berriz ere taberna zabalean
buruan laino bat eta pareta



zuri bat aurpegiko azalean



astero bezala ohartu gabe
egunak harrapatu nau kalean
Erlojuak egunero...
Amaitu zaigu gorakada


etxera joateko ordua iritsi da
astelehena ate joka da
eta ezin ihes egin erlojuari
Erlojuak egunero...