---
id: tx-843
izenburua: Ez Da Berria Neure Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BNoKzNiPGjQ
---

Ez da berria neure kantua
oinaze hau dut aspaldikoa.

Ez dut ikusten zuzentasunik,

haundiek baitute leherturik.

Ez eni aipa mendi sainduko zer dohatsurik.

Noren eskutan dira makilak?

Eta norentzat beti nigarrak?



Ez dut ikusten zuzentasunik

ez munduan ez Euskal Herrian.

Espainol frantses, bi otso haundi

hor dira nahiz jan gure ardi!

Nun dira hemen herri huntako gizon zuzenak?

Noizko dituzte beren ekintzak

ta noizko ere beren otoitzak?



Ez eni aipa mendi sainduko

prediku baketsu dohatsurik;

presondegitan dagon anaiak

baduke segur bertze ametsik…

Gure xedea izan dadila iratzartzeko,

euskal herria ez da zatitzen,

nun ez gaituzten guziak hiltzen!