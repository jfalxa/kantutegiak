---
id: tx-1167
izenburua: Txolin Txolin Barbero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iZ2k0L1HDTc
---

Txolin, txolin barbero,
Billabonako semea, txin-txin-txin.
Andrako batek, txin-txin-txin-txin,
hantxe zeduzkan, rau-rau-rau-rau,
hiru alaba politak, txin-txin-txin.

Eskatu nion, txin-txin.
neronek bat behar nula, txin-txin-txin.
errepuesta, txin-txin-txin-txin,
eman ziraden, rau-rau-rau-rau,
txaketa zaharra neukala, txin-txin-txin.