---
id: tx-3311
izenburua: Arkakusoak -Bi Deak-Las Dos D-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lbMNjxhusVU
---

Bi arkakuso ezkondu dira
ezkon-bidaia eta guzti
ez Bilbo, ez Paris, ez Washington
ez dira edonoraino _
Leku ederra nahi izan dute
iletsua eta xamurra
apropos hartu dute hoteltzat
bost izarreko txakurra
nork esaten du alaitasunik
jada ez dela munduan
hau zorri onaren zorri ona
txakur sorbaldan mantsoa