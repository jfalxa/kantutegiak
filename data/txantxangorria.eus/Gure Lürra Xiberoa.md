---
id: tx-46
izenburua: Gure Lürra Xiberoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cpL4vT7fuRw
---

Gure lürra XIBEROA
Bazterrak zoin eder, Xibero hontan,
Gützaz bekaitx dira hiri handietan,
Nola den bizia gure etxetan, dira beti galtoz kürütxada orotan.
Kanpotiarrak dira, saldoka üdan,
Herri ttipi horietan pausatü üsnan,
Dioie dügüla gük, Xiberoan, bizitze ederra gure xokoan.
Gure lürra Xiberoa, zü zütügü maitena,
Zük beitüzü batzarria denentako goxoa !
Laborariak badaki, bere lür horietan,
Zuin penus den lana urte orotan,
Üda ala negü, kabalekilan, halere hor irus da bere bizian.
Heben bizitzeko, Xibero hontan,
Botzik ager gitian algarrekilan,
Batek besteak lagünt behar ordüetan, alpo doi bateki arrotzekilan.
Hitzak: Dominika ETCHART
Müsika: Jean ETCHART