---
id: tx-1687
izenburua: Zeru Azpian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rBqw-eJcmVQ
---

Esan amatxo: aingerutxoak
hegoa urrezko badute,
zeru urdiñean urre-disdira
nik nola ikusten ez ote?...


Parre gozo bat amak eginda,
haurrari ala diotsa:
Behetik dakusgun urdiña zeru,
azpia baizik ezpaita.

Ene, ba!... azpia mutillak dio
orren ederra dalarik,
nolakoxea ote-da, gero,
zerua goi-alderditik!...


..--..--..--..--..--..--..--..--..


Gautu zanean, ortzi zabala
bare-ozkarbi zegoen,
ta izar argien zillar-begiak
aurrari par-zegioten.

Aur-kezka oro amets eztitsu
biur oi-diran garaian,
aurrak, amaren biotz-alboan,
berriro onela ziotsan:

Zerua azpitik orren ederra
dalarik, amatxo maite,
ai nere poza, zeru-gainkia
ikustera aal-baningoke!...


..--..--..--..--..--..--..--..--..--..


Aurraren naia zeruratu zan,
(alatsu otoitz garbia),
ta aingerutxoak eska zioten
Yaunari lagun berria.

Eguzkiaren urrezko yaia
berriro izeki zaneko,
begia itxirik zeukan aurtxoak
zeru-azpia lkusteko.

Seaska-aldean ama gaxoak
otoi-zegian, negarrez,
semea igoa baitzun, zeruko
goikia ikusi-bearrez.


--Zeru azpia..."Xabier Lizardi"--