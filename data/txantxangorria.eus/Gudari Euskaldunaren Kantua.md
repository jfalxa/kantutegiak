---
id: tx-1650
izenburua: Gudari Euskaldunaren Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EEEvIFgUISY
---

Zer duk nigarrez, zer duk hain ilun,
eskualdun gudari?
Ator gurekin, pesta duk egun,
gaitezen liberti.
Ez, ez, ez naike alai izan ni
ez baita libro Euskadi,
ez baita libro Euskadi. (Ez, ez, ez naike ...)
Lagunak hilak, gorputz lurpean,
edo desterruan...
Gu berriz hemen, bertzen mendean,
preso gur'etxean... (Ez, ez, ez naike ...)
Gauak goiz alba duela ondoko,
lagun, oroitadi
luza gabe ziur bai libratuko
gur' ama Euskadi.
Bai, bai, bai alai izango naiz ni
libratueta Euskadi, libratueta Euskadi (Bai ...)