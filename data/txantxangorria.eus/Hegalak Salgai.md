---
id: tx-978
izenburua: Hegalak Salgai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/riMBx9Eyo7U
---

HEGALAK SALGAI]

Beldurrak ausardiari hegalak ematen dizkio.
Beldurrak ausardiari hegalak ematen dizkio.

Beldurrak ausardiari hegalak ematen dizkio.
Beldurrak ausardiari hegalak ematen zizkion.

Lumak zerutik erortzen dira
bizitzak itsaso geldoan.

Itsashontziak bihotzetatik
amildegia isilik zeharkatuz.

Ongi etorri errefuxiatuak.
Ongi etorri  ahaztuak.
Ongi etorri neska eta mutilak.

Adi  egon lagunok
lagundu behar  dugu denok
hau ez da abesti bat
hau S.O.S. bat da.

Beldurrak ausardiari hegalak ematen zizkion.
Ongi etorri errefuxiatuak.


" Esan dezatela kontzentrazio eremu eta torturek.
Seguruenik gure sentimentu guztietan egiazki gurea ez den bakarra, itxaropena da. itxaropena bizitzari dagokio, bizitza bera da bere burua defendatzen."  - Julio Cortázar, RAYUELA. -

Zuzendaritza, Edizioa eta Kamara: RAMON ZABALEGI.
Aktorea: ADAMA DIALLO.
Laguntzaileak:  LURGI ZALAKAIN eta NAHIKARI ITURBE.

---------------------------------------

[ALES A LA VENDA]

La por li dóna ales al valor
La por li dóna ales al valor

La por li dóna ales al valor
La por li va donar ales al valor

Les plomes cauen del cel,
les vides en el mar inert
Els vaixells en els cors
travessant l'abisme en silenci.

Benvingudes refugiades
Benvinguts oblidats
Benvingudes noies i nois.
Estigueu atents amics i amigues,
hem d'ajudar tots i totes

La por li va donar ales al valor.

Benvingudes refugiades.
Benvinguts refugiats.
Benvingudes refugiades.

--------------------------------------------

[WINGS ON SALE]

Fear gives wings to courage.
Fear gives wings to courage.
Fear gives wings to courage.
Fear used to give wings to courage.

Feathers fall from the sky
Lifes on a peaceful sea.

Ships silently crossing the cliff through hearts.

Welcome refugees.
Welcome the forgotten.
Welcome girls and boys.

Beware my friends
we must all help
this is not a song
it's an SOS.

Fear used to give wings to courage.
Welcome refugees.

---------------------------------------

[Alas a la venta]

El miedo le da alas al valor.
El miedo le da alas al valor.

Las plumas caen del cielo,
las vidas en el mar inerte
Los barcos en los corazones
atravesando el abismo en silencio.

Bienvenid@s refugiad@s.
Bienvenid@s olvidad@s.
Bienvenid@s chicas y chicos.

Estad atentos amig@s,
tenemos que ayudar tod@s.
Esto no es una canción, es un S.O.S.

El miedo le dió alas al valor.

Bienvenid@s refugiad@s.
Bienvenid@s refugiad@s.
Bienvenid@s refugiad@s.