---
id: tx-2671
izenburua: Gazte Gaztetatikan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j_NsFkcGz2k
---

1.Gazte-gaztetatikan herritik kanpora,
estranjeria aldean pasa det denbora.
Herrialde guztietan toki onak badira, 
baina bihotzak dio: ""Zoaz Euskal Herrira!"" 

2.Lur maitea hemen uztea da negargarria,
hemen gelditzen dira ama ta herria.
Urez noa ikustera, bai, mundu berria,
oraintxe, bai naizela urrikalgarria.

3.Agur, nere bihotzeko amatxo maitea!
Laster etorriko naiz, konsola zaitea.
Jaungoikoak, nahi badu ni urez joatea,
ama, zertarako da negar egitea?