---
id: tx-1062
izenburua: Maitetxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xGYMBWuob50
---

Bihotz osoarekin
maitatzen nuen neska polit bat
bihotz osoarekin
eskiñi nion maitasuna
betiko zoriona.

Nerekin zebilan
neretzat izango zan
irudi gozoa
ez zen galduko
ezelan ere
gure zoriona
ene maitia
ene maitia
bihotzaren poza
itxaropen eztiak
ganeztu zuen nere gogua
itxaropen ura
zan nere zerua
egunez eta gabaz
zure gomutaz ene maitia
atseden gaberik
aurkitzen nintzan ni