---
id: tx-3107
izenburua: Bada Hiri Gorri Bat Lorelei
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RIIqVpZqgYA
---

Bada hiri gorri gorri bat
Non erretzen den ilargia
Kale urtuetan irristrantzian
Zaitut oroitzen ene maitia.

Bada hiri gorri gorri bat
Franken tabernako barrarenpian
Isolatuegi datzan heinian
Gautxori baten doinu tristia.

Hiri gorriz dindatzen ditut
Zure ezpain lehertuak
Ophelie.