---
id: tx-3233
izenburua: Puxika Hegalariak -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bKNOHR8na8Q
---

Puxika hegalari, tori
airean bultza hemendik eta handik
aho miresgarri honetan
gehiago gordeta bertan
Nahiko al huke hik?

Nik bai nahiko nuke
Jolastu nirekin
nola egin?
Biok berdin
Birikak bete eta
putz egin elkarrekin

Puuutz! 
Puuutz! 
Puuutz! 
PUN!