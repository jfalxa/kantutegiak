---
id: tx-1204
izenburua: Intxauspeko Alaba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FBLsO4YYyD0
---

Intxauspeko alaba dendaria, 
goizian goizik jostera joalia, jostera joalia, 
nigarretan nigarretan pasatzen du bidia, 
aprendiza konsolatzailia, konsolatzailia.

Zelian den izarrik ederrena, 
jin balekit argi egitera, argi egitera, 
juan naite juan naite maitiaren ikustera, 
ene penen hari erraitera, hari erraitera.

Mertxikaren floriaren ederra, 
barnian dizu hezurra gogorra, hezurra gogorra, 
gaztea naiz gaztea naiz bai eta loriusa 
ez dut galtzen zure esperantza, zure esperantza.