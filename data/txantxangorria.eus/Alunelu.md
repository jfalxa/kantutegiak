---
id: tx-1004
izenburua: Alunelu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/44Jmc8xoj98
---

#saveyourinternet

Musika: Herrikoia (Errumania)
Letra: Herrikoia eta Koldo Celestino


Alunelu alunelu hai la joc
sa ne fie, sa ne fie cu noroc
Alunelu alunelu hai la joc
sa ne fie, sa ne fie cu noroc
Cine-n hora o sa joace
mare mare se va face
cine n-o juca de fel
sa ramana mititel.

Alunelu jostariak neu ta zeu
alunelu dantzariak zuek ta geu
Alunelu jostariak neu ta zeu
alunelu dantzariak zuek ta geu
Lehenengo alde batera
gero berriz beste aldera.
Besoak elkartuta
haur guztiak batuta.