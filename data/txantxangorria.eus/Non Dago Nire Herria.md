---
id: tx-1838
izenburua: Non Dago Nire Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b2MMsSaT-yc
---

Non dago nire herria
Erreka bazterrean hala gauaren ertzean
Aipatu ezin den iragan batean
Hala zoririk onenean memoriaren desolazioa
orain utsik dauden kalabozoetan
hala mamuen bizitoki diren ziegeten
esku ireki inozenteetan 
hala damuaren besondoetan
Nigan al daramat benetan
Ametsetan, ametsen al dago nire zain
ze ametsetan, ze lainoren azpian
eta zenbat gorpuren gainean
Non dago nire herria
Nigan aldaramat benetan
gorra ote naiz aren zurrumurru zarra aditzeko
itxuaren egunsentiak ikusteko
ta non da ze asturatan geroratua
non dago nire herria
ze destinu edo zein aluzinaziotan
ze itsas argiren dardarizoetan
telefonoaren froteran 
mesfidantzaren banderan 
kontrarioen kimeran
Bitan zatituta
hirutan isildua
bere baitara bialdua
bitan zatituta
hirutan isildua
bere baitara bialdua
hirutan zatitua
bitan isildua
non dago nire herria 
esperantzaren tentsio ia itogarrian
neguan umeen zoko mira larrian 
amnistiaren krabelin gorrian
gulliberren zorretan
hitzera itzultzen direnetan
jada ez daudenen hatsetan 
Non dago nire herria
Nigan dagoela hori da 
Nirekin joan etorri da
ta nirekin batera iritsiko da nire herrira
Bai nigan dagoela hori da 
Nirekin joan etorri da 
ta nirekin batera iritsiko da nire herrira
Iritsiko da nire herrira
Iritsiko nirekin batera