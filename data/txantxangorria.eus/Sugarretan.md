---
id: tx-2276
izenburua: Sugarretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R7dSy5JvgzE
---

Euskal musikaren txo2019.ko Abenduaren 28an, Hatortxu Rock elkartasun jalaldian eskeinitako kontzertuko irudiez osatutako bideoklipa. 

Argazkia eta muntaia: Aitor Mendilibar




Bagoaz, beste behin,
Errepidean,
Bagoaz iheskide,
Sentimenen iraultzan,
Mugatutako gauetan barrena,
Kantauriko hegian zeharka,
Zintzurra kiskaltzear
Eta odola sutan!
Sugarretan
Garenaz harro
Eta garenaz aurrera
Borrokan gara haziak
Borrokan heziak;
Itzalpeko lagunak gogoan,
Joan zirenak gurekin direla,
Hortzak estutzean
Doazen gezi zuzenetan.
Sugarretan,
Maitasun eta gorroto artean
Sugarretan
Barrutik datozen hitzak,
Punk-rock doinuz hornituak,
Bala bihurtuta.
Urteetan elkarrekin,…