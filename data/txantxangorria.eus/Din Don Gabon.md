---
id: tx-2756
izenburua: Din Don Gabon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8gkU9nZZYqo
---

Din don gabon
Din don gabon
Zintzarriak airean

Din don gabon
Din don gabon
Zilarrezko bidean

Kantari mahai ertzean
Aita, ama eta umeak

Din don gabon
Din don gabon
Zuenean eta gurean

Din don gabon
Din don gabon
Gizonaren artean

Din don gabon
Din don gabon
Borondate hobean

Izan daigun pakea
Euskal Herri maitean


Din don gabon
Ta zorion
Zeru eta lurrean