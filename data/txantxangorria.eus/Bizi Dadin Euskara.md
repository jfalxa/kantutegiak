---
id: tx-1492
izenburua: Bizi Dadin Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kekxpwTSBoI
---

1.Haurretik ibilirik mundu zabalean
itsasoan marinel tratulant lurrean
Mintzaira mota asko badut ikasirik,
Eskuarak nihun ere bat ez du parerik.

2.Ardia alhatzen da bazka gizenetan,
han kausitu ondoan mendi hegaletan.
Eskuarak ere badu eremu zabala.
Han, han berma ditake jakinen ahala !

3.Nun da Eskuara bezen mintzaira berorik ?
Munduan batek ez du khar handiagorik !
Maitea maiteari mintzo bada gabaz,
Leihorat itzulia mintzo da... eskuaraz !

4.Haurra bere amari nola mintzo zaio ?
. Ama, ama ., eskuaraz sehaskatik dio.
Amari lakhet zaio horren entzutea,
Gure Eskuara hain da mintzaira maitea!

5.Mintzairetan Eskuara omen da lehena:
Dena deen, Eskuara da segur ederrena.
Othoi atchik dezagun eder bezen garbi,
Eskuaraz mintzatuz noiz bai eta non-nahi !

6.Herritik urruntzean, Eskualdun gaztea,
Ez duzu ahantziko Eskuara maitea.
Ondikotz zenbat ez da ahanzten duenik !
Merezi othe dute Eskualdun izenik ?

7.Eskuara hain da eder, hain da maithagarri,
Non behar baitiogu laguntza ekharri.
Bizi dadin maitea orai eta gero,
Berma gaiten hortarat Eskualdunak oro."