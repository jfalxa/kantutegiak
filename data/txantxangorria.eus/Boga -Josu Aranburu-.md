---
id: tx-3142
izenburua: Boga -Josu Aranburu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nTTZAp7M8ss
---

hitzak eta musika: JOSU ARANBURU
Ondarroako SANTOS RECORDS estudioan grabatua, MIKEL SANTOSek 2011ko otsailaren 4, 5 eta 6an.
masterizazioa: JONAN ORDORIKAk Azkarateko MANIA estudioan egina.
musikoak: JOSU ARANBURU, BREIXO DANBALA, KEPA ARAKISTAIN, JESUS CAMIÑA, LORE LEMA, LOINATZ IRAOLAGOITIA, NAGORE LASARTE eta ZUBI ZAHAR IKASTOLAKO GAZTETXOAK.

BIDEOKLIPA (DVDa)
zuzendaria: AITZOL ARAMAIO
muntaia+pos-produkzioa: ANGEL ALDAROND

Hondar aho batean jaio zen,
garairik onenak ez izan arren,
Gure arbasoen kemenak eta indarrak
Itsasoak, ilargiak eta izarrak,
Babestu dute urte guzti hauetan
Ikastolaren argia.
Zabalik dauzkagu ateak
Zabalik ere bihotzak
Denok BOGA batera eginez
Zabaltzeko gure hizkuntza
Eta ez ahazteko gure izaera
Hartu dezagun erronka!
BADATOR, BADATOR IBILALDIA
ITSASOKO HAIZE BERRIA BEZALA
BERE ABERRIA EUSKARA IZANGO DA
BERE ETXEA ZUBI-ZAHAR IKASTOLA!
BADATOR, BADATOR IBILALDIA
ITSASOKO HAIZE BERRIA BEZALA
BERE ABERRIA EUSKARA IZANGO DA
BERE ETXEA ONDARRUKO KALEAK!
Izarrek argituko gaituzte
Bidea iluntzen den bakoitzean
Eta bidaia luzea izan arren
Ziur egon merezi duela saiatzea
Euskal Herri osoak sentitzeko
Gure arraunen bultzada
BADATOR, BADATOR IBILALDIA
ITSASOKO HAIZE BERRIA BEZALA
BERE ABERRIA EUSKARA IZANGO DA
BERE ETXEA ZUBI-ZAHAR IKASTOLA
BADATOR, BADATOR IBILALDIA
ITSASOKO HAIZE BERRIA BEZALA
BERE ABERRIA EUSKARA IZANGO DA
BERE ETXEA ONDARRUKO KALEAK!
BOGA BOGA BOGA DENOK BATERA
ANTZINAKO ITSAS GIZONAK BEZALA
BETI AURRERANTZ, INOIZ EZ ATZERANTZ
ITSASOKO HAIZE BERRIA GU GARA!