---
id: tx-2345
izenburua: Espus Espusari Topa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A7oQs5zYsaw
---

Espus Espusari Topa

Zuiñen eijer den bedatsian lehen lilia
Baratze xokhuan irus ageri
Zuiñen eijer den goizaldi batez jüntatürik
Bi espus eskik emanik elgarri
Egünko egünak hortara ekharri gütü
Uhure espus eta espusari
Ziren biziko desiratzen dereiziegü
Bier xantza hun eta osagarri

Espus maitiak orai jakin behar düzie
Ziren hunetan zer datin gerua
Batian plazer bestian aldiz arrünküra
Uste gabeko nahi gabekuak
Bena orai danik biga baziratekie
Aisa egarteko ziren eskerniuak
Bai eta phekü guzien kontsolagarri
Egün jüntatü zutien amodiua

Mündü huntan den bekhaxkeriarik handiena
Bethi danik da aberatstarzüna
Ene ustez aldiz huntarzün baliusena da
Osagarri hunin gaztetarzüna
Bizitze berri bati bürüz bagutzalarik
Hartürik espusetako erhaztüna
Amodio xühar batek inganatürik
Hori da hori egiazko fortüna

Zerü gainian ekhirik ederrena beita
Arratsaldiari bürüz itzaltzen
Espus maitiak nik hortan dit zirek bezala
Aita eta ama gaixo horeier phentsatzen
Arratsaldi hori bürüz juranik beitira
Ikhas ezazi-e horen eztitzen
Bakian horiek lagüntüren beitzütie
Ziren haur maiten untsa eraikitzen

Üzten zütiet espus gazte maitagarriak
Hortan debeiazi gabe sobera
Suhetatzen dereiziet xantza hun batekin
Bizi ahal zitaien alagera
Nuixta nuixta jin ere nahi benintzateke
Zirekin kafe hun baten hartzera
Eta mentüraz Jinkuak plazer dialarik
Zazpi geren haurraren egartera