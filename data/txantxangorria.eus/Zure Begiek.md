---
id: tx-2199
izenburua: Zure Begiek
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0mVtQnoOMnM
---

Mikel Markezen abesti zoragarria. 'Hit' bat izan zen abesti hau, batez ere nerabe eta gazteen artean, maitasun deklarazio sutsu eta desesperatua izanik adin horretan maitasuna sentitzen den modua ongi baino hobeto islatzen duelako: 'Zure begiek mintzen naute, zure ahoak beldurtzen nau, niretako nahi zintuzket...' Tonu menorrean idatzia dago baina ez da leuna, maitasun kantua izateko erritmo indartsua dauka, sentimenduaren berotasuna eta tragikotasuna ederki jasotzen duena. Estribiloaren erdian kantariak errezitatu egiten ditu beste bi parrafo, eta efektu berezia lortzen du horrela. 

Hor esaten dituenak, 'Zu gabe ez naiz ezer, zu gabe zertarako bizi, bizitza honetan ditudan helburuak ez dira ezer zurekin konpartitzen ez baditut', bihotzeraino heltzen diren hitzak dira.
Zalantzarik gabe inkunablea