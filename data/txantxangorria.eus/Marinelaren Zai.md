---
id: tx-2207
izenburua: Marinelaren Zai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YWwUfshKtgc
---

Sorotan Beleren abesti melodiko ederra. Onenetarikoa.

Arotzak zuen alaba bat
zen herriko xamangarriena
bi gizonek nahi zuten jabe izan
haren bihotzak nola esan.

-Zer dun, zer dun ene alaba
-Mariñela maite dut aita
-Ez al dun ulertzen, ezinezkoa da
jauntxoa da hiretzat maitia.

Orduan egarriak denak ziren ba
maitasun ukatuarentzat
jauntxoa dator hire esku bila
marinela Irlandara doa

Hala izan zan eskontz behartua
aberatsik ez ziren han
ta guztiak dantzan, guztiak alai
mail ezberdinak ahaztuaz.

-Zer dun, zer dun ene alaba
-Mariñela maite dut aita
-Ez al dun ulertzen, ezinezkoa da
jauntxoaren emaztea zara.

Gaur egun oraindik ikus dezakegu
alaba hura lehioan
mariñelaren zai,zai
itsasoan du itxaropena.