---
id: tx-2212
izenburua: Pentsioduna Har Zazu Zure Bizikleta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3uK2I-BNUuU
---

PENTSIODUNA HAR ZAZU ZURE BIZIKLETA 
Euskal herrian mugimendu bat dago 
bost udaberri dizkigute oparo 
Biltzen dira gure plazetan astero Nagusien arazoak konpontzeko  
Defendiendo los derechos sin descanso en todos los frentes andan ayudando  Contestaron con firmeza a los Gobiernos a Iberdrola y a la Banca se enfrentaron  
y decían!: …no importa quién gobierne los derechos siempre se defienden  
Pensionista coge tu bicicleta 
 Denok batera goaz errepidera 
Eskatzen dugu complemento a 1080   Oraintxe dugu lortzeko aukera. 
Defenderemos Zerbitzu Publikoak Universales y de calidad 
 Eskerrik asko bihotz bihotzetikan 
 Al personal sanitario y esencial 
Mujer-andreak eta hombre-gizonak  Berdintasuna, queremos igualdad Zaintzen eredu berri bat behar dugu 
Y con la brecha de género acabar