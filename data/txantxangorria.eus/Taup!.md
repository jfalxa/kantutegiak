---
id: tx-2503
izenburua: Taup!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cv9ix3MLEXg
---

2018ko Ibilaldiak TAUP! leloa aukeratu du, soinu horrek biziaren indarra ezin hobeto adierazten baitu. Gure jaiari musika jartzeaz Ze esatek! taldea arduratu da, eta letrak aditzera ematen du ikastolak Santurtzin dituen erro sendoak. Ikastola bat, azken finean, ilusioa konpartitzen duen pertsonen taldea da. Talde horretako kidea izateko gonbite alaia da gure ibilaldiaren kanta.


Serantes saltoka dabil
Garabiei lepotik helduta.
Serantes saltoka dabil
Taup, Taup!

Ontziak kantari dabiltz
Dorre denak ez zirela jausi.
Ontziak kantari dabiltz
Taup!, Taup!

Tipi-tapa Taup!, Taup!
Zatoz

Pauso bat eta zatoz
Irri bat eta zatoz
Berba bat eta zatoz
Gure taupadara
Zurea ere bada.

Sardinerak dantzan dabiltz
Semaforo moreen gainean
Sardinerak dantzan dabiltz
Taup, Taup!

Bihotz bat taupaka dabil
Ibaizabal itsasoratzeko
Bihotz bat taupaka dabil
Taup!, Taup!

Tipi-tapa 
Taup, Taup!
Zatoz

Zatoz
Ez galdu denbora
Pauso berriak dira taupada zoroak
Ez zaude bakarrik ibilbide honetan
Gozatu disfrutau zure bidean

Izan libre eta askea
Tipi tapa tipi tapa
Goazen guztiak bai 
Irribarrez berban beti laguna
Pozik bizi
Hauxe da momentua!

Pauso bat eta zatoz
Irri bat eta zatoz
Berba bat eta zatoz
Gure taupadara
Zurea ere bada.  

Hizkuntza bat ez da galtzen ez dakitenek ikasten ez dutelako, dakitenek hitz egiten ez dutelako baizik.

Pauso bat eta zatoz
Irri bat eta zatoz
Berba bat eta zatoz
Gure taupadara
Zurea ere bada. (Bis)

Kantaren letra Sustrai Colinarena da eta La Basuk kolaborazioa egin du.

FITXA TEKNIKOA

Musika: ZE ESATEK!
Letra: SUSTRAI COLINA
Kolaborazioak:  AHOTSA/LETRA: ELENA CABALLERO “La Basu”, AHOTSA: JOANE LARREA
Grabaketa estudioa: 2noisy estudioa
La Basuren ahotsa grabaketa estudioa: Gaua estudioa
Nahasketa: Higain estudioa

BIDEOKLIPA

Bideoklipa 2018ko martxoan, Juantxu Belokiren eta Jone Novoren errealizazio-lanaz, grabatu dugu Sarturtziko portuan eta Bihotz Gaztea  Ikastolan, bertoko ikasleak, gurasoak eta langileak protagonistak izanik. 
 
Irudietan Santurtzirentzako hain garrantzitsua eta hurbila den itsasoak, gizakiak sorturiko muga artifizialak gainditzen ditu, portua eta kaia atzean utziz, eta Bihotz Gaztea osatzen dugun komunitate-partaide guztiok, herriko goiko eremutaraino -ikastolaraino-, olatu urdinen aparrean, eramaten gaitu.