---
id: tx-338
izenburua: Zaldi Emozionalak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SWgtQgAJdB0
---

CD-Manipulazio estrategiak (Zart 2020)
www.zart.eus
www.petti.eus


Hitza/Letra:



ZALDI EMOZIONALAK 

Hipodromo isila kalea bera bada, 
zaldunik gabeko brida hau, norena da? 

Zeri egin aurre, nori eman bizkarra?
Atzera ez begiratu, gaindi ezazu langa!

Zaldunik ezean, akaso karabana? 
Daramagun gurdian, nor deabru doa? 
Zangopea daukat ferraz ongi joa
hiltzeekin josita daude gure asmoak.

Zigorrari zigorkada: 
zaldi emozionalak gara.  

Eguna amaitzean, gauero trantzean: 
lotuko zaituzte –paradisu ustean–, 
tortura goxoenen baratzean.
Zaldia izan arren, zabiltz asto lanean... 

Zigorrari zigorkada: 
zaldi emozionalak gara.  

saria zain duzu orain, haien esku artean,
eskukada batez zeu elikatu ustean:
azukre urtua aurki, zeure sugarrean, 
sugar eta aho biak, urtuz aldi berean. 

Zigorrari zigorkada: 
zaldi emozionalak gara.  

Harkaitz Cano