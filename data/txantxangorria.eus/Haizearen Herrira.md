---
id: tx-2717
izenburua: Haizearen Herrira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LOZI6vk_Bmo
---

Haizearen herrira joan
nintzen behin batean
txoko denak zeuden
usain goxoz beteak.
lalalalalalalalalalalalalalalalalalala
Txoko denak zeuden
usain gozoz beteak.
Haizearen herrian
Loratzen dago bizitza
Loretan zelaiak
Lorez-lore zuhaitzak
Lalalalalalalalalalalalalalalalalalala
Loretan zelaiak
Lorez-lore zuhaitzak
Pozik egoten nintzen
Krabelin denak usaintzen
Han lore artean
Ari naiz jolasean
Lalalalalalalalalalalalalalala
Han lore artean
Ari naiz jolasean
Loretan zelaiak, lorez- lore, zuhaitzak, han lore artean
Ari naiz……….
Jolasean, jolasean, jolasean…..