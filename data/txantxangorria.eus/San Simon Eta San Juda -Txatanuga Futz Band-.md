---
id: tx-3182
izenburua: San Simon Eta San Juda -Txatanuga Futz Band-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g0aQkJcXVcA
---

San Simon eta San Juda
joan zen uda, eta negua heldu da: (bis)
ez baletor hobe, bizi gara pobre
eremu latz honetan
ez gara hain onak benetan

Ez dugu zaldirik, ez gara zaldunak
ez dugu abererik, ez gara aberatsak
euskara guk dugu, gu gara euskaldunak
euskara guk dugu, gu gara euskaldunak..