---
id: tx-542
izenburua: Pirañak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h65XgOzyA20
---

Bideoaren egilea Dani Sanchez "Skst Design".
Abestiaren grabazioa nahasketa Josu Erviti "DrumGrooveStudio"-n.
Masterizazioa Jonan Ordorikaren eskutik "Estudios Katarain"-en.

Bastardix:



Maite neska gazteak zerbait sentitzen hasi da
Jasotako hezkuntzaren kontra egiten duena
Maitek ez du ulertzen zergatik istorio honetan
Bera ez dagoen printzipe urdin baten esperoan

Ez ziguten irakatsi, garena izatera
Gezurrari ezin eutsi pentsa
Nola zabiltzan zure bizitzan, atzoko giltzak gaur ez dabiltza

Ta kostaldea ezin aurkitu ito zaigu kapitaina
Atzo ez nituen ikusten ur hauetako pirañak
Barnera begiratzean, ekaitzak astinduta
Hegalak askatzeko momentua iritsi da.

Maitek neska bat ezagutu du, maitemindu dira
Geldirik zegoen mundua berriro hasi da bira
Maitek neskalaguna aurkezteko ausardiaz bete da
Baina askotan bizitzak lurperatzen ditu heroiak.

Zer esango du jendeak? Zu ez zara horrelakoa!
Aldatu arte norabidea bertan
Behera laguna ikusteko egunak, eta hitz kuttunak zituzten gutunak

Ta kostaldea ezin aurkitu ito zaigu kapitaina
Atzo ez nituen ikusten ur hauetako pirañak
Barnera begiratzean, ekaitzak astinduta
Hegalak askatzeko momentua iritsi da.

Bularrak erretzen dizu
Malkoak duela lehortu zirela
Gela iluna berriz piztu
Moldeak hautsi, deabruarekin dantza egin eta

Bastardix envió Gaur 15:26etan
Nortasuna adierazi OHIUKA!
Nahi duzun bezala bizi, izorra daitezela!

Ta kostaldea ezin aurkitu ito zaigu kapitaina
Atzo ez nituen ikusten ur hauetako pirañak
Barnera begiratzean, ekaitzak astinduta
Hegalak askatzeko momentua iritsi da.

Zatoz lagun, zatoz gurera
Denon artean egin behar dugu Aurrera
Beldurrik ez, lotsarik ez
Intolerantziaren aurka beti prest!