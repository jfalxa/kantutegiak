---
id: tx-1906
izenburua: Agur Euskal Herriari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZiZUn-Zu1NI
---

Gazte gaztetandikan
Herritik kanpora,
Estrajeria aldean
Pasa det denbora.
Gazte gaztetandikan
Herritik kanpora,
Estrajeria aldean
Pasa det denbora.
Herrialde guztietan
Toki onak badira,
Baina bihotzak dio:
"zoaz Euskal Herrira".
Herrialde guztietan
Toki onak badira,
Baina bihotzak dio:
"zoaz Euskal Herrira".
Lur maitea hemen uztea
da negargarria.
Hemen gelditzen dira
Ama ta herria.
Lur maitea hemen uztea
da negargarria.
Hemen gelditzen dira
Ama ta herria.
Urez noa ikustera,
Bai, mundu berria;
Oraintxe bai naizela,
Errukigarria.
Agur nere bihotzeko
Amatxo maitea!
Laster etorriko naiz
Kontsola zaitea.
Agur nere bihotzeko
Amatxo maitea!
Laster etorriko naiz
Kontsola zaitea.
Jaungoikoak ba nahi du
Ni urez joatea;
Ama zertarako da
Negar egitea?
Jaungoikoak ba nahi du
Ni urez joatea;
Ama zertarako da
Negar egitea?