---
id: tx-2173
izenburua: Egun Da Santi Mamiña
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IbQVv1UxS38
---

Mikel Laboaren abesti mitikoa, bera bezalakoa.Atzo abenduaren batean laga gintuen Mikelek. Uda amaieran, biga, laga gintuen beste Mikelek, Errazkin. Mundu honetako kaiolatik, biak, betirako aske.