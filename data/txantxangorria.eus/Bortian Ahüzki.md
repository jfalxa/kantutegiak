---
id: tx-3409
izenburua: Bortian Ahüzki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9fXIMAKrYZM
---

Bortian Ahüzki, hur hunak osoki,
Neskatila ejerrak han dira ageri:
Hirur badirade, oi! bena charmantik,
Bacha-Nabar orotan eztie parerik.

Neskatil' ejerra, oi! begi ñabarra,
Nuntik jin izan zira bortü gañ huntara?
Garaziko aldetik, desir nian bezala,
Ahuzkiko üthürrila, hur fresken hartzera?

Goizetan eder dizu ekhiak leñhürú:
Mundia argitzen dizü, enguru-enguru.
Ni ere zur' ondoan hala nabilazü:
Eia maite naizünez, othoi, erradazü.

Maite zütüdala badizu aspaldi,
Ezpanereizün erran orano zihaurt:
Bihotzian edüki phena handireki;
Zihauri erraitera oi! ezin atrebi!

Gord'ezazü hori ahalaz lüzazki,
Maite ükhenen zütut nik har-gatik bethi,
Berriak egükiren, ezin heltin, hanti;
Gero izanen gira biak algarreki.

Adio Ahüzki eta Nabolegi
Dolü 'giten deiziet Lakharsorhoreki;
Goraintzi erroizie Ziprian Phinorí,
Mil' esker derogüla haren karesari!