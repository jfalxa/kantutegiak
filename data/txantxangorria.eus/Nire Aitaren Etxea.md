---
id: tx-3397
izenburua: Nire Aitaren Etxea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y6b925wRX3E
---

Gabriel Arestik
idatzitako hitzak

Gure aiton-amonei eskenia
zuen guda bizirik darrai.
Nire aitaren etxea
defendatuko dut.
Atearen aurrean
zaindari bat bezala
jardun beti zu

eta ez dago huts egiterik
ez dutenek hau maite
izu alde hemendik
Zuen aurka jokazu
ta doan borroka
Otsoen kontra,
sikatiaren kontra
lurrukeriek ere
nere aurkan dabiltzate
justiziaren kontra,
defendatuko eginen dut.


Nire aitaren etxea
Galduko ditut haziendak,
soloak, pinudiak;
galduko ditut korrituak,
errentak,
fedea eta igandeko meza
interesak!

baina nire aitaren etxea
defendatuko dut.
Atearen aurrean
zaindari bat bezala
jardun beti zu

eta ez dago huts
egiterik
ez dutenek hau maite
izu alde hemendik

Armak kenduko
dizkidate, baina
eskuarekin defendatuko dut
nire aitaren etxea



eskuak ebakiko dizkidate,
eta besoarekin
defendatuko dut
nire aitaren etxea.
Zuri, gorri ta berdea

Besorik gabe,
sorbaldarik gabe,
bularrik gabe
putre menpe
utziko naute,
eta arimarekin
defendituko dut
nire aitaren etxea.

Ni hilen naiz,

nire askazia galduko da,
nire arima galduko da,
dena galdu dut
gauza bat izan ezik
nire aitaren etxeak
iraunen du zutik.
Nire aitaren etxea
defendatuko dut.
Atearen aurrean
zaindari bat bezala
jardun beti zu

eta ez dago huts egiterik
ez dutenek hau maite
izu alde hemendik

Nire aitaren etxea
defendatuko dut.
Nire amaren etxea
defendatuko dut.

Nire aitaren etxea
defendatuko dut.
Nire amaren etxea
defendatuko dut.