---
id: tx-859
izenburua: Behobia Donostia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oOMOWEYaodM
---

Animoak leuntzen du,
berezko bide gogorra.
Ohiuka edo lasterka,


denok eztarri lehorra.

Partehaztzaile ta ikusle,
ekipo bikaina horra.
Izerdi tanta bakoitzak osatzen du errekorra.
Izerdi tanta bakoitzak osatzen du errekorra.

Donostia-Behobia
pausoz-pausoko historia.
Behobia-Donostia,
Korrika mundu guztian.

Hogei kilometro pasan,
hegoa eta iparra.
Haize ta harrotasunak,
betetzen digu bularra.

Kolpera etortzen zaigu,
lehen galdutako indarra.
Txalo beroen artean sentzitzean Boulevarda.
Txalo beroen artean sentzitzean Boulevarda.

Donostia-Behobia
pausoz-pausoko historia.
Behobia-Donostia,
Korrika mundu guztian.
Aea, aea, aea, aea,
aea, aea, aea, aea.
Aea, aea, aea, aea,
aea, aea, aea, aea.

Bideak aurretatzeko,
guztion lana behar du,
Gazte edo heldu izan,
sekula ez da berandu.

Norabait iristekotan,
ezin da pausorik galdu,
Ta herritik hasitakoa,
mundu osora zabaldu.
Ta herritik hasitakoa,
Mundu osora zabaldu.

Donostia-Behobia,
pausoz-pausoko historia.
Behobia-Donostia,
korrika mundu guztian.
Behobia-Donostia,
korrika mundu guztian.

Aea, aea, aea, aea,
aea, aea, aea, aea.
Aea, aea, aea, aea,
aea, aea, aea, aea.
Aea, aea, aea, aea.