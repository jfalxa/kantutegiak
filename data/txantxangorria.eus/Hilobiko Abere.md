---
id: tx-955
izenburua: Hilobiko Abere
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xhnPWP_cRCE
---

ZONA CERO | HILOBIKO ABERE (PET SEMATARY COVER)


Ramones taldearen "Pet Sematary" abestiaren euskal bertsioa.

This is a cover of Ramones' "Pet Sematary" song. It's sung in Basque language, Euskara.

ZONA CERO
Gorka Padrones
Xabier Bilbao
Imanol Rodriguez


Bideoa: Iñigo Acha
Aktorea: Miriam del Prado
Makilajea: Miriam del Prado eta Ainara González

Email: zcerorock@gmail.com

HILOBIKO ABERE

Euripean hilarriak margulduz
jauntxo zaharrak defuntu
Lurrazpitik isil azaldu
Herio usaina gure inguru

Eta gaueko haize hotzagaz
Badakit, inori ez zaio axola

EZ NAIZ ZURE ALA BERE HILOBIKO ABERE
EZ DUT NAHI BIZI BERRIZ ERE

Iluntasuna da nagusi 
Ez dago amets ezta ihesaldi
Hortzak eta hezurrak errauts bihurtu 
Arima sutan hilobiratu

Ta gauan ilargipean, 
Norbaitek egiten du negar

EZ NAIZ ZURE ALA BERE HILOBIKO ABERE
EZ DUT NAHI BIZI BERRIZ ERE

Ilargi bete, haizerik gabe
hotzikarak jaun eta jabe
Gorputza ustelduz, ni irribarre
Egun honetan bizi naiz aske

Eta gauan otsoa uluka
Entzun ozen nire oihuak

EZ NAIZ ZURE ALA BERE HILOBIKO ABERE
EZ DUT NAHI BIZI BERRIZ ERE