---
id: tx-310
izenburua: Xedea Eta Fedea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZeGmqr17SSg
---

Kolaborazioa: Julen Egia (koroak)

Pentsa, zenbat denbora galdu honen zain?
Azkenean ez gozatzeko.
Zarena galtzen duzu
Izango zarena beti jagoteko

Zenbat besarkada galdu duzu bidean
Hunkipena zer den ahazteko?
Elkar ukitzearen, elkar basakeria
Norbanakoan

Zeure bidea apurtu, 
Ildoak ebai, aske izan
Azeria ehizan

Azeria hire izaera