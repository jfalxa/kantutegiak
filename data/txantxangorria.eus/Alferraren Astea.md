---
id: tx-2094
izenburua: Alferraren Astea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AYcFwi_5KbQ
---

Astelehena, jai ondoko alferra,
ezer ez egiteko ez goaz lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.
Asteartea, euria goitik behera,
busti egingo gara eta ez goaz lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.
Asteazkena, osaba ezkontzen da,
jai hartzen dugu eta ez goaz lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.
Osteguna, amonaren eguna,
hori ospatutzeko ez goaz lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.

Ostirala, haginetako mina,
aspirina hartuta bagoaz ohera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.
Larunbata, egun erdiko lana,
egun erdigaitik ez goaz lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera.
Igandea, lantegia itxita,
lan egiteko nahi baina ezin joan lanera,
eta ez goaz lanera, ez goaz lanera,
eta ez goaz lanera, ez goaz lanera