---
id: tx-2960
izenburua: Alduderi Begiratzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Sa_TNqDYKS8
---

Hitzak eta Musika : Peio CHAUVIN
 
Mendi aldetik goizeko argia
Zerutik jautsi bakea
Pentzez pentze dabila
Etxez etxe plazara
Goxotasuna eta dirdira
Zuri eskainiak bezala
 
O Aldudeko egun-hastean
 
Hango bazterrez batzuk diote
Badela zerbait xarmazale
Edertasunaren alde
Fite ikus ditaike
Larrazkenean hainbat kolore
Ez ote dira pare gabe ?
 
O Alduderi so egoitean
 
Bizkar gainttoan izar eder bat
Abiatzen dela dakhusat
Gorde-leku batetarat
Ala oihan barnerat
Ene bihotza hunki balezat
Harekin joan nindaike harat
 
O Alduderi amodioan
 
Urrundik jina niz Alduderi beha amentsetan
Eta hegalez hunatekoan, arimaz gidaturikan
Udaberrian, zure izena, enetako esperantza