---
id: tx-1936
izenburua: Andikona 1936
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3WzwVTS1oIg
---

Mila bederatzirehun eta
hogeta hamaseigarren,
ze egun baltza garagarrilak
hogetabi eukazana.
Jaietan ziren otxandiotarrek
bizia galdu ebena.
Gogoratzeak min emon arren
ahazten itzi ezin dana.
Ahazten itzi ezin dana.



Gasteiz aldetik bi hegazkinek
urratu eben zerumuga,
Andikonako plaza gainean
suertatu zan burrunba.
Papelak die!, papelak die!
diarka umeak zerura.
Andikonako errekatxoan
laster gorritu zan ura.
Laster gorritu zan ura.

Herri zaharra ta barrie
biziguraz betie,
baginen eta bagare
Otxandioko herrie.


Otxandiotarrek jakin eben ze
kolore eukan bengantzak
Bonben txistua ta tanbor hotsa
ekarren herio dantzak.
Gero Durango, gero Gernika...
su berberaren errautsak
Txingar bizitan mantendu ditu
kontatzeko esperantzak.
kontatzeko esperantzak.

Egunsentiak ekarri eban
berrogei urteko gaba.
Bildurra, lotsa ta isiltasuna
izan ziran ugazaba.
Baina orduko diar mutua
gaurko gure abotsa da.
Bihar be entzun dagien gatoz
Andikonako plazara,
Andikonako plazara.

Herri zaharra ta barrie
biziguraz betie,
baginen eta bagare
Otxandioko herrie.

Herri zaharra ta barrie
biziguraz betie,
baginen eta bagare
Otxandioko herrie.
Otxandioko herrie.