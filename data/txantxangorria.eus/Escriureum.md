---
id: tx-820
izenburua: Escriureum
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IhKtW-PlWeo
---

Un nou dia ha començat
Em llevo al teu costat
Respires lentament
Un nou dia t’ha abraçat
Et lleves I no saps
Que anyorarem aquest moment
Les casualitats no son les que ens han portat
A viure aquest present
Que jo no en sóc conscient
Si m’esperes allá afora et cantaré

Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem amb els dies regalats
Iamb el somriure dels que ja no hi puguin ser
Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem caminant per les estrelles
I amb el somriure dels que ja no hi puguin ser

Ta orain dena amaitu dela
Ez zaude hemen nirekin
Oso ezberdin zaude
Kajoi baten gorde ditut
Zuk abesten zenizkidan
Nota txiki hoiek
Zure ahots eder horrekin
Zuretzat kantatuko dut beti

Ta idatzi dena ez zela izan erreza
Gure bizitza kantatu orri baten
Oraindin baditut egunak zuretzat
Jantzita iraganeko irrifarrez

Ta idatzi dena ez zela izan erreza
Gure bizitza kantatu orri baten
Lo hartuko dugu izarrak kontatzen
Atzo joan ziren horiek oroitzen



I ara que tot ha acabat
Que no et tinc al meu costat
Et sento diferent
Amago en un calaix les mirades d’amagat
Les que cançons que em vas cantar
Son les que vull cridar
Si m’esperes allá afora et cantaré

Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem amb els dies regalats
I amb el somriure dels que ja no hi puguin ser
Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem caminant per les estrelles
I amb el somriure dels que ja no hi puguin ser

T’escriuré que sí que va ser fàcil
T’estic cantant la nostra història en un paper
Marxaré recordant tots aquells dies
I amb el somriure que tu sols sabies fer

Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem amb els dies regalats
I amb el somriure dels que ja no hi puguin ser
Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem caminant per les estrelles
I amb el somriure dels que ja no hi puguin ser

Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem amb els dies regalats
I amb el somriure dels que ja no hi puguin ser
Escriurem que tot no va ser fàcil
Cantarem la nostra vida en un paper
Marxarem caminant per les estrelles
I amb el somriure que tu sols podies fer