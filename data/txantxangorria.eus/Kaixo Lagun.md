---
id: tx-2486
izenburua: Kaixo Lagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/991b0UxIxJI
---

Musika: Herrikoia (Nikaragua)
Letra: Herrikoia eta Koldo Celestino


Eskutik lotuta goaz
ukondoak ondo astinduz
ta musikari jarraituz;
giro alaian bizi gara
hemengo beste haurrekin
ta nahi baduzu zurekin.

Kaixo lagun, ongi etorria
altxa besoak gorantza
mugi (e)/zazu ipurdia
ta zatoz dantzatzera.

Kaixo lagun, ongi etorria
altxa besoak gorantza
mugi (e)/zazu ipurdia
ta zatoz dantzatzera.

Gure herriko Pachamama
euskal mendietako Mari
edota Errusiako Mani;
batzutan etortzen dira
oso bihurri izanik
baina maizago txantxetan.

Kai/xo la/gun, on/gi e/to/rri/a
al/txa be/so/ak go/ran/tza
mu/gi (e)/za/zu i/pur/di/a
ta za/toz dan/tza/tze/ra.

Kai/xo la/gun, on/gi e/to/rri/a
al/txa be/so/ak go/ran/tza
mu/gi (e)/za/zu i/pur/di/a
ta za/toz dan/tza/tze/ra.

Kai/xo la/gun, on/gi e/to/rri/a
al/txa be/so/ak go/ran/tza
mu/gi (e)/za/zu i/pur/di/a
ta za/toz dan/tza/tze/ra.

Kaixo lagun, ongi etorria
altxa besoak gorantza
mugi (e)/zazu ipurdia
ta zatoz dantzatzera.