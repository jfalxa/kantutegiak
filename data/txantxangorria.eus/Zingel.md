---
id: tx-1282
izenburua: Zingel
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/py8yXBBS_qA
---

Gozategi, Zingel berria 
Bzi-poza sortzen duen disko berri zoragarria 
Bizitza kalejira handi bat da! 


Ostegun gizen batez maitemindu nintzen
Geroztik ikasi det jan gabe bizitzen
Zenbat maite zaitudan, zer dudan sentitzen


Esan nahi dizut baina ez naiz atrebitzen.

Etxeko ta lagunak denak nazka-nazka
Eginda dauzkat beti gaiari bueltaka
Astero kontu bera egunero kaka
Oraindik hola nabil urteak pasata

Bai "ezez" ostiral honetan bai "ezez", ausarta izango naiz "ezez"
Den dena kontatu baietz

Bai "ezez" larunbat honetan bai "ezez", ausarta izango naiz "ezez"
Den-dena kontatu baietz

Ta parez-pare beida-beida lotsa-lotsa gorri-gorri
Tikiri-tikiri-taka tori ai ai ai ai
Akabo nire ausardiak kopetean izerdia

Bai "ezez" larunbat honetan bai "ezez", ausarta izango naiz "ezez"
Den-dena kontatu baietz

Ta parez-pare beida-beida lotsa-lotsa gorri-gorri
Tikiri-tikiri-taka tori ai ai ai ai
Akabo nire ausardiak kopetean izerdia