---
id: tx-1604
izenburua: Lurraren Blue Note Lurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9F71ICKQ3d4
---

Miss Ekuadorrek zera esan zuen:    “Nire nahia? munduko gosea amaitzea”

Gosea munduan nola deuseztatu?   Nola akabatu jauntxokeria?

 

Nola ezereztu miseria?  Guda guztiak nola bukatu?

Alde ekonomiko basatiak       nola txikitu mundu honetan?

 

Nola akabatu suizidio ekologikoa? Eguneroko diktadurak?

Eta batez ere, edo hasteko, nola suntsitu missen leihaketak?