---
id: tx-88
izenburua: Neska Zahar Bat Tentatzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Uzqtz7YM_zo
---

1
Dozena bat bertso nik nahi nituzke jarri
jaietan kantatzeko entretenigarri,
nere publikatzea izango da sarri
alabantzak emanaz zenbait neska zaharri. 

2
Neska zahar bat tentatzen hasi naiz lehenguan,
izan ere halako polita zeguan,
umil eta serio hitz egin genduan
baina gustu guziak ez dira munduan.

3
Hizketan hasi ginen biak gozo-gozo,
ezkontzeko mandatu egin nion atzo,
leihotikan ujuka ama ta bi atso:
"ez duk harrapatuko uste bezain mantso".

4
Ama preparatu zait hasarrez betea,
lehenbiziko ujua: "zenbat duk dotea?
Hainbeste kostatako alaba maitea,
lastima izango duk hiri ematea".

5
"Zure familiari errespetoz heldu
 inozente daudenak ez daitezen galdu,
diru truke alaba nahi zenuke saldu
le(he)nago erostunik ez al zaio azaldu?"

6
Nire jakinduria ari da atzentzen
errez dela uste du zenbaitek ezkontzen,
promesa on bat eginda ez bazara ontzen,
lanak izango dira zurekin konpontzen.

7
"Ez duzu sinistu  nahi egia esanda_ere
zintzoa nahi genuke pobrea izanda_ere,
gure bazterrak baino hobiak non dare,
 alabari sasoia pixka bat joanda_ere".

8
"Bere alabarentzat horrenbeste fama!
Nik ez dut sinistatzen haizeak darama,
zure bazterrean nik egitea lana...
izan liteke baina ez dut uste ama!"

9
"Ez al dela uste duk horren errespuestik?
Atarramentu onik mutil za(ha)rrak ez dik!
Nire alabak ez dauka ezkontzeko kezkik
hi ez bezalakoak nahi ez ta zekazkik"

10
"Hitz  bat esan behar dut nire borondatez
ez banau inork lotzen grilluz edo katez,
ezkontzeko ideiak banituen ustez,
ardoa maitatzen dut andrearen partez".

11
Mutil malizioso deabruz betea,
ez dizut zabalduko gehiago atea,
behin'e etzattut izandu bihotzez maitea
nigandik libre zara... hor konpon zaitea!"

12
Horra hamabi bertso oraingo berriak
bere andregaiari Txirritak jarriak,
eman dizkit desgustu ikaragarriak
negarra egin dezake nigatik harriak.