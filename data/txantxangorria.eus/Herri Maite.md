---
id: tx-68
izenburua: Herri Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_ls9fMhcOQM
---

Zure mendiak, zure haize garbia 
Itxaso ederra portu kale guztiak 
Nire barrura nire bihotz erdira 
Sartu zara nere herritxo laztana.
 
Herri maite, 
zelan maite zaitudan 
Poz pozez nago zure aterpean 
Urrun banago zerura begiratzen 
Eskatzen dot zu berriz ikustea