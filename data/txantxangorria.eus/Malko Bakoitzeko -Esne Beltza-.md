---
id: tx-3329
izenburua: Malko Bakoitzeko -Esne Beltza-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1r9ny-j7018
---

Bihotza doinuan
eta behatzak panderoan
soinu artean bizi eta
soinurik egin gabe joan

Gauaren babesean
ilargi gorri hortan dago
hegoak astinduz
geroz eta askeago

Eta ni orain
negarrez nago
eta malko bakoitzeko
maite zaitut gehiago

Bidai bat denboran
eta uhain bat itsasgoran
zeru izartsuetan
desirak pizteko geroan.