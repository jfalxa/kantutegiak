---
id: tx-2277
izenburua: Etxean Gaudenoi Jarriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oaI3v5wcatM
---

1.
Koarentenakin hasi
ginen herenegun (bis)
ta oraindik aurretik
horrenbeste egun!
Ez dakit martxa honetan
zer egingo dugun...
Zu ere ni bezala
bazabiltza lagun
gutxinez bertso hauek
kantatu ditzagun!

2.
Hogeita lau orduak
etxean sartuta (bis)
zenbat dira asteko
guztiak batuta?
Irtengo nintzateke
baloia hartuta
baina hortxe ondo dago
pixkat pentsatuta
berak ere atsedena
noizbait behar du ta!

3.
Atetik irten gabe
gaude orain denak (bis)
lagunik ezin uki
eta hartu nau penak...
Baina bukaera du
guretzat kondenak
ta horrela datozkit
galdera zailenak:
nola bizi ote dira
beti hala daudenak?

4.
Hemen gaude txoriak
kaiolan bezela (bis)
denbora gutxirako
dugu, dena dela.
Batzuk ospitalera
gaixotu ta bela,
ez da erraza izango
hango pasarela...
Gure gelan lagun bat
egon zen horrela.

5.
Gaixorik jarri zela
jada aspaldi da (bis)
baina gogoan dugu,
begira, begira:
nola ospitaletik
heldu zen herrira...
Orain gaixo daudenei
aupa eta biba!
Hura sendatu bazen,
sendatuko dira!

6.
Prestatzen garenean
bidai baterako (bis)
kotxeko minutuak
dira betirako!
Gehiago plana bada
oso urrutirako!
Mundua zein handia
den umeontzako,
ta aldiz zein txikia
birus honentzako!

7.
Mundua gaixotu da
batekoz besteko (bis)
etxea motibo bat
al da kexatzeko?
Aprobetxa dezagun
lasai egoteko
bertan ditugunekin
tope gozatzeko...
Etxerik ez dutenak
ez daude hobeto.

8.
Hau pasa beharra du
sendatu nahi dunak (bis)
ta munduan kutsatu
gutxi nahi ditunak
nahiz ta orduak izan
geldoak, astunak,
buelta emango diogu,
izan zentzudunak,
laster gara kalean,
animo lagunak!