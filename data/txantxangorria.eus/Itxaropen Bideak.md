---
id: tx-1057
izenburua: Itxaropen Bideak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ed66XBiCshY
---

Noizbaiten bidean,
galdu egiten bazara
aurrera egin nahiean,
ez ahaztu hitz hauek,
bidai luzeetan,
agur laburretan

Gure bihotzetan
gugatik dena eman dutenak

Argi bat zeruan,
iluntasunean,
dizdira urrunean
pauso bat aurrera
elkartasun hitz hauek
zuentzat direla

Gure bihotzetan
gugatik dena eman dutenak

Gure bizitzan
ireki dituzu
itxaropen bideak
ta beraz, ez ahaztu inoiz
zurekin gaituzula
beti, beti

Oroitu ditzagun
lurrera erori arren
altxatu direnak
zutik dirautenak,
ondoan daudenak
urruti daudenak

Gure bihotzetan…

Oroitu ditzagun, argi bat piztu dutenak
Bidai luzeetan, ondoan daudenak
Oroitu ditzagun, argi bat piztu dutenak
Agur laburretan urruti daudenak