---
id: tx-915
izenburua: Preverten Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fyqXbRCjub0
---

Musika: Serge Gainsbourg / Patxuko Nice
Hitzak (itzulpena): Ibon Irastorza


Nahiko nuke zu oroitzea
Kanta hau zurea zen 
kuttunena, uste dut
Prevert eta Kosmarena.

Beste batzuekin uzten naiz,
Baina aspertzen naute 
pixkanaka, ajolik ez
Honen aurka, alperrikan.

Hosto hilak entzutean, 
zu zatozkit gogora
egunak joan eta amodio hilak
ez dira guztiz hiltzen.

Nahiko nuke zu oroitzea
Kanta hau zurea zen 
kuttunena, uste dut
Prevert eta Kosmarena.
 
Utzikeria jakingo al dugu 
non hasi ta bukatzen den,
udazkena badoa, 
negua badator

Hosto hilak entzutean, 
zu zatozkit gogora
egunak joan, amodio hilak
ez dira guztiz hiltzen.

Hosto hilak oroimenaz 
desagertuz badoa
ta orduan hilko dira
nire amodio hilak.