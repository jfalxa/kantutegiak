---
id: tx-234
izenburua: Mila Beatzireunarekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rbtXZYCykY8
---

Mila beatzireungarrena

berroitamaikagarrena

agorreko bigarrena

Donostiako Kontxatik irten

joaz itsason barrena

an bira eman urrena

sarrerarikan onena

gure oriyotarrena.

 

Getari beti kezketan

Oriyorekin naasketan

aurten ez dabil festetan

guri mutilla kenduta berak

zerbait eiteko ustetan

indarra bear zainetan

ez ibilli berriketan

ezin kabituz galtzetan.

 

Bilbaon aurrena asi

Azkuek arrauna autsi

atzian ginuzten utzi

gure mutilla ankaz gora zan

Sarasuan ziyon: “Ez etsi!”

jarri berriz ta ikusi

oriyotarrak nagusi.

 

Orain berroitamar urte

lendabiziko bandera

ekarri zuten onera

gure aitonek, arraunen gogor

eginaz jo ta aurrera

aurten daukagu jaiera

beraien oroimenera

urrezko eztaietan gera.