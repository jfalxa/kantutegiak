---
id: tx-866
izenburua: Lanik Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6BrdVUBgqSo
---

Lanik gabe den baten

Ezin bizia zer den

Orai behar dut erran.

Izan naiz ate ate

Jaun ainitzen ikusten

Ta lanik ez dut ukan!

 

            Egia da beraz jaunak,

            Nik ditudala hobenak!

            Apezen ganik ere

            Naiz barkamenik gabe!

 

Elizak ez dirade

Falta gure herritan

Bainan bai lantegiak!

Otoitzik nahi bada

Egin eliza hoitan

Hets-kitzue hil hobiak!

 

Lanik gabe naizeno

Denbora ez galtzeko

Krixtok entzun nezala!

Herria lagun dezan

Geroaren bidean

Jaun hoiek ez bezala!