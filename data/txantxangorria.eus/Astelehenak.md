---
id: tx-2778
izenburua: Astelehenak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XqoXG5CKOHI
---

Ilargitan astelehenak,
zenbat astez atzera.
Bidezidorretan barrena,
pilatzenda orbela.

Baina gaur ez da giro.
Gauari kea dario.

Zenbat laztan ta bat bera,
ez du asmatu gauak.
Zenbat leiho hankaz gora
epeltasun erreguan.

Ba/ina gaur ez da giro.
Gauari kea dario.
Maitasun ezak, plazer errezak
ez dute balio.
Maitasun ezak, plazer errezak
ez dute balio.

Zenbat arerio zelatan dabiltzan.
Esaidazu berriro
zein den zure baldintza,
zoaz, ez itzuli.
Hau ez da e/gi/a.
Okela gordina, sentitu ezina.

Maitasun ezak, plazer errezak
ez dute balio.

Maitasun ezak, plazer errezak
ez dute balio.

Maitasun ezak, plazer errezak
ez dute balio.

Maitasun ezak, plazer errezak
ez dute balio.

Ilargitan astelehenak,
zenbat astez ostera.
Bidezidorrtan barrena,
pilatzen da norbera.