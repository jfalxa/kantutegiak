---
id: tx-139
izenburua: Sekretua Entzutea Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gc0XJKQjq_M
---

Larrua jotzea
istorio bat kontatzea da
sekretu bat dago
zenbakian aurkitzen da
ez da zero, ez da bat
gutxienez bi gara
eta istorio hoberenak
benetakoak izaten dira

Entzutea da sekretua
bet ipuin berdina ez kontatu
entzutea da sekretua
beti istorio bera ez kontatu

Horixe pentsatzen nuen
nire ipuina entzun nahi zuela
baina ohartu nintzen
bere taktuaren leuntasunarekin
bere begi berdeekin
bere azalarekin ikusi behar nuela
txortan egiteko sekretu bat dago
honako hau da:

Entzutea da sekretua