---
id: tx-3357
izenburua: Ostadarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Tr_PT_Q2-nc
---

Zirimiria arratsean
hodei gorrien artean
Ostadarrak agurtu du 
eguna ilunsentian
Izpien gurdi ameslari horretan
oso gora igo nahi nuke ta
Izar dizdiratsuak nik piztu
Izpien gurdi ameslari horretan
oso gora igo nahi nuke ta
Izar dizdiratsuak nik piztu

Ilargian kulunka denok kantari
keinuka lotan dagoen guztiari
Egunsentian kukulumuxuak
eguzkiarekin batera
Argituko dugu Euskadi

Ilargian kulunka denok kantari
keinuka lotan dagoen guztiari
Egunsentian kukulumuxuak
eguzkiarekin batera
Argituko dugu Euskadi