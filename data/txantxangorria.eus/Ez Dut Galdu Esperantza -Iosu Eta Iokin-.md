---
id: tx-3163
izenburua: Ez Dut Galdu Esperantza -Iosu Eta Iokin-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1Qq7QzG_Kt4
---

Baditut egunero
milaka zalantza,
bainan sekulan ez dut
galdu esperantza.
Iritxiko zaiola
euskal Herriari
denok espero dugun
egun berri hori.

Langileak lanean,
nausiak bakantza,
bainan sekulan ez dut
galdu esperantza.
Lurreko ondasunak
egunen batetan
ez direla egongo
esku berdinetan.

Herria lotuz dago,
ez jaia, ez dantza,
bainan sekulan ez dut
galdu esperantza.
Heriotza, kateak,
biak apurtuta
dantzatuko dugula
zazpiak batuta.

Morroiak nagusiari
zor dio gorantza,
bainan sekulan ez dut
galdu esperantza.
Batak eta besteak
berdin izateko,
nagusia morroiari
belaunikatzeko.

Euskaldunon etxean
nagusi da arrotza,
bainan sekulan ez dut
galdu esperantza.
Bakoitza bere etxean
nagusi izateko,
eta hauzokoari
bakean uzteko.