---
id: tx-3395
izenburua: Makilakixki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OwwjE1UpxNw
---

MAKILAKIXKI
Makilakixki kixki, kixki.
Makilakixki klas (bis)
Kila makila kila.
Makila kila.
Makila klis klis klas.
Kila makila kila.
Makila kila.
Makila klas. (bis)
Makilakixki buruan.
Makilakixki lepoan.
Makilakixki besoan.
Kila klisklis klas.
Kila klisklas.