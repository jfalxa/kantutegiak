---
id: tx-1225
izenburua: Aldapan Gora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JB5PgXOTd1Y
---

Mendian gora, burua galtzen dut maiz
herriko kaletan sarritan galdu izan naiz.
Nork bereizi zituen kultura, lurra sua ta ura?
Gizakion arteko lotura, ere ez al da natura?


Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago? (x2)



Berriro galduta gabiltza, hau da hau marka
ezin da ulertu aurrean daukagun mapa.
Euskaldun peto peto bai, baina ardi galduen pare
nahiago det ibili halare, norabiderik gabe!


Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago? (x2)

Aldapan gora… 
Pausoz pauso…
Gora... Aldapan gora...

Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago?

Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak...

Mendian gora, burua galtzen dut maiz
herriko kaletan sarritan galdu izan naiz.
Nork bereizi zituen kultura, lurra sua ta ura?
Gizakion arteko lotura, ere ez al da natura?


Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago? (x2)



Berriro galduta gabiltza, hau da hau marka
ezin da ulertu aurrean daukagun mapa.
Euskaldun peto peto bai, baina ardi galduen pare
nahiago det ibili halare, norabiderik gabe!


Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago? (x2)

Aldapan gora… 
Pausoz pauso…
Gora... Aldapan gora...

Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak zerk galtzen gaitu gehiago?

Aldapan gora, pausorik pauso
aldapan behera, auzorik auzo
gaztainondo ta pago
eskultura arraro,
kaleak edo mendiak...