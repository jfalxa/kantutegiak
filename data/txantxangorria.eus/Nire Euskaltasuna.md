---
id: tx-2078
izenburua: Nire Euskaltasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AHOX_UglFFw
---

Nire euskaltasuna baso bat da,
eta ez du zuhaitz jenealogikorik.

Nire euskaltasuna dorre bat da,
eta ez du Alostorrerik.

Nire euskaltasuna bide bat da,
eta ez du zaldizkorik.

Nire euskaltasuna lore bat da,
eta ez du aldarerik.

Nire euskaltasuna itsaso bat da,
eta ez du almiranterik.

Nire euskaltasuna liburu bat da,
eta ez du sotanarik.

Nire euskaltasuna mundu bat da,
eta ez du Amerikarik.

Nire euskaltasuna bertso bat da,
eta ez du txapelik.

Nire euskaltasuna pekatu bat da,
eta ez du mea-kulparik.