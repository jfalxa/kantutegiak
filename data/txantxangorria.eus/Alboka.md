---
id: tx-83
izenburua: Alboka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iWZE3LGXK98
---

@fezemusic
@oihanuribarren
@elimaidagan
@axerii
@an2an_msk
@etxepe_
@gozategi.musika.taldea

--

Grabaketa: Etxepe
Bideoklipa: Jokin Salido
Remix eta masterra: FEZE
Dantza taldeak: Lore Gazteak eta Txikitxu Arrostaitz

Letra:

Bolumena igota martxan jartzen gare
ez gaituzu maite baina gu hemen gaude 
bolumena igota martxan jartzen gare
ez gaituzu maite baina gu hemen gaude

Etxepen gaude gu nora zoaz zu
Jaizalen parranda dau Axeri mozkortu
gurekin bildu zu txiki urra gu!
bazkal ostien inddou kristona julayou

No woman no cry, Badyal flow2000 ni naiz
gaztia ni naz baina burun zabor ugari jak 
gabetan gaz ligatzen BlackBerry joseatzen
enaz jazten karu nau zure poza marrazten

Entzuten Subversion oin naz el mas cabron
ez dozu ulertzen asik hor konpon
txikitatik batera petarduekin kalera
jaixak hasi eta gu ezin heldu etxera

Zezenak harrapau nau asik dale Don dale
asike dale Don dale, dale Don dale
bi egun nau itzarrik tragos en el Jaizale 
asik dale Don dale, dale Eli Don dale

Uo uo uo zuziria piztu dou
zuk zortziko txikia nik 20 barra dauzkot
Uo uo uo zortzitan gaiteruak datoz
Mafren hasten dala ez jakintasunian daz asko

Uo uo uo zuziria piztu dou
zuk zortziko txikia nik 20 barra dauzkot
Uo uo uo zortzitan gaiteruak datoz
Mafren hasten dala ez jakintasunian daz asko

Etxepen gaude gu nora zoaz zu
Jaizalen parranda dau Axeri mozkortu
gurekin bildu zu txiki urra gu!
bazkal ostien inddou kristona julayou

Kalimotxo zerbeza buruan putibuelta
etxetik irten eta etzi nator de buelta
entzierruan atasko gainetik txiribuelta
Eli ta ni barrezka lokatzez nau beteta

Entzuten Subversion oin naz el mas cabron
ez dozu ulertzen asik hor konpon
txikitatik batera petarduekin kalera
jaixak hasi eta gu ezin heldu etxera

Zezenak harrapau nau asik dale Don dale
asike dale Don dale, dale Don dale
bi egun nau itzarrik tragos en el Jaizale 
asik dale Don dale, dale Eli Don dale

Uo uo uo zuziria piztu dou
zuk zortziko txikia nik 20 barra dauzkot
Uo uo uo zortzitan gaiteruak datoz
Mafren hasten dala ez jakintasunian daz asko

Uo uo uo zuziria piztu dou
zuk zortziko txikia nik 20 barra dauzkot
Uo uo uo zortzitan gaiteruak datoz
Mafren hasten dala ez jakintasunian daz asko