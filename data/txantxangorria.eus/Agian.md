---
id: tx-565
izenburua: Agian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XTo7d3pn4OM
---

Agian zuk bazenekien
berak bazekiela
ez zeniola dagoeneko
leialtasunik gordetzen.

Agian berak bazekien
zuk bazenekiela
ordu oroko galderaren
erantzun beti estaliaz.

Orduan udaberriko lore xumeak
ikusten zenituen gauez
mesanotxe gainean
portzelanazko ontzitxoan.

Agian berak negar egiten zuen
malko isilez goizaldean
zure lo sorgorrak
kulparen angustia estaltzerakoan.

Agian horrela behar zuen izan
isiltasuna, loreak, malkoak,
portzelanazko ontzitxoa buruki ondoan,
esker txarraren arauak
baititu bizitzak gehienetan
leial irauten dutenentzat.