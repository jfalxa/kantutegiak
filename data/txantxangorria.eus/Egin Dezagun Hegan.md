---
id: tx-2445
izenburua: Egin Dezagun Hegan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XdXErnG8R1s
---

Letra eta musika: Iñaki Lizaso

Musikariak: San Millango ikasleak.

Kolaborazioa: Bihozkada taldea


Eskola txikiek duten berezko handitasuna:
mila kolore, herri nortasuna!
Neska-mutil, irakasle, lagun-kuadrila, familia:
Uztarri bereko edertasuna!

Ehun lengoaia guztiontzat hamaika esperientzia,
ondo pasaz, ikasiaz; ganoraz.
Aske sentituz, sinistuz, pentsatuz eta eraikiz
maitasun giroan eta euskaraz!

Ezinezkorik, hemen ez da ezin denik!
Gora San Millan (e)ta gora Zizurkil!

Zoriontsu izatea ez al da gu guztiok dugun ametsa?
Egin dezagun hegan denok batera!