---
id: tx-1059
izenburua: Kantuka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-PdsywwV4Ec
---

KANTUKA GABIZ BATU ZAITEZ
KANTUKA BETI HAN EDO HEMEN
KANTUKA GUZTIOK SAIO GABE
EUSKAL HERRIAN KANTUKA BE!

Ekin ta bultza
euskal rap hutsa
Kantu kantari gabiltza bizitzan 
Hitz anitzak jaso ta bota 
Lasai mutil hau duk Revolta

musikaz Blai esai-ozu etsaiari
Hator ta entzun-gailuak jarri 
Festa dugu nahi bakarrik 
HEY GUY JOIN THE PARTY.

Bakarrik bagaude
etxean edo nonnahi
ez izan lotsarik ta
kantuak beti zain
mesedez!
Zure ahotsa entzun dezagun
Egizu kantu gaur ozen eta alai 

Bai? Noski.. Come on!
revolta musikaz blai!
kitarraren soinua jorratuz
eta euskararekin jai
Do you like this baby?
-MUSIC IS LIFE-
euriaren azpian kantuka
ta ez gaude salgai!


KANTUKA GABIZ BATU ZAITEZ
KANTUKA BETI HAN EDO HEMEN
KANTUKA GUZTIOK SAIO GABE
EUSKAL HERRIAN KANTUKA BE!

Be-benetan diñot
ahotik botaka musika
po-pozaz gozatu jorratu soinuak
tronuak baita hautsiko dira
ekiozu gaurkoari
begietan distira!

Tira hator gure albora
Bonbo kaxa dekogu odolan
biraka mundua geu beti finko
Tinko jarraitzen gure arloan

Alboan lagunak badoaz gurekin
txokoan guztiok beti elkarrekin
etekin handiak eskuak gurutza
musikaren menpe ekin ta bultza!
Hori da esana lagunekin bat
altza lekutik festa hasi da, Rap Rock Metal Reggae SKA
Revoltarrak bueltan dira!!!

KANTUKA GABIZ BATU ZAITEZ
KANTUKA BETI HAN EDO HEMEN
KANTUKA GUZTIOK SAIO GABE
EUSKAL HERRIAN KANTUKA BE!