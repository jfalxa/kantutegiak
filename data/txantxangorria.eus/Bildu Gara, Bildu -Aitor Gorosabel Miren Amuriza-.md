---
id: tx-3171
izenburua: Bildu Gara, Bildu -Aitor Gorosabel Miren Amuriza-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2uUtD5bbHJk
---

Amaitu da gaua
ez gaituzte ixilduko
Izarrak zeruan
dardara egin du
sentimenduen hariari
goaz lotuta
amodiozko sare honetan
bat eginda gure ondoan
nahi zaitugulako
Eskutik heldu eta
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
Itxaropen argiak
berpiztu dira.
Irrii egin dugu
goizari begira
Ametsen mundu berri bat
nahi dugu eraiki
borondate onarekin
soilik ez da aski
zu gure ondoan
nahi zaitugulako
Eskutik heldu eta
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara, BILDU
BILDU gara...