---
id: tx-2339
izenburua: Beldurrik Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HKUlEOW7Iwo
---

Xatiro, rapero bilbotarrak abesti berri batekin dator: Beldurrik gabe. 

Bere betiko ekoizlea den Ruben G. Mateosekin batera, Salda Dago abeslariarekin dator eta "Como Deadpool" lanarekin egin zuen bezala, bidai bat proposatzen du, oraingo honetan espaziotik. 
Amets egiteko; espazio-ontziak gidatzeko, ilargian dantzatzeko edota planeta bitxienak aurkitzera gonbidatzen gaituzte.

Zatozte beraz bidai honetara non nostalgia eta, gauza guztien gainetik, beldurra atzera uzten ditugu egoera berriei aurre egiteko. Datorrena datorrela. 

Ez nuke nahi armazoia soilik izan
Ez nuke nahi farrea opari tabernetan 
Ez nuke nahi itxurarik egin behar 
Jendea atsegiteko momentu txarretan  

Ez dakit zenbatetan huts egin dizut 
Zentzurik gabeko txorradak esaten jaietan
ez dakit zenbatetan alde egin nauzu
eskerrak txarra naizela matematiketan

Ikasi dut jada/ zeharkatzen zeruak 
Konprenitu zaitut irakurtzen Jack Kerouac
Idazteko tristurak badu bere zentzua
Baina ez dizut orain kontatuko sekretua   

Dena bizkorregi badoa ah
hurrengo geltokian jaitsiko gara
Dena bizkorregi badoa, ah
salto egingo dugu lapurren erara.

Eta ilargian aurkituko nauzu dantzatzen
Planeta bitxiak aurkitzen 
Espazio-ontziak gidatzen
Batzuetan izar berriak pizten
hemen gure oihuak inork ez ditu entzuten
itxi begiak eta goazen
izatera norberaren burujabe
jarrai dezagun aurrera beldurrik gabe 

Biniloak biraka, lurrak biraka
Ta Leica bat lepoan zu ta biok gara Robert Capa
Sartuko gara binaka 
Apokalipsia ikusteko jarri bi butaka
  
Dena pikutara doa ta ni hegaka 
ez ditugu kontutan hartuko datak
ezta erlojuen orratzen norabidea  
oin gu gara mundu berri onen egileak 

malenkoniaren arerio
iraganak ez gaitu minduko gaur
ez jarri horrenbeste serio 
maite duguna gure baitan dirau

momentua da hemen eta orain 
abiapuntua kokatzen nonahi
jarri zure espazio trajea bai bai!
Hemen egongo naiz zure zain zain

Eta ilargian aurkituko nauzu dantzatzen
Planeta bitxiak aurkitzen 
Espazio-ontziak gidatzen
Batzuetan izar berriak pizten
hemen gure oihuak inork ez ditu entzuten
itxi begiak eta goazen
izatera norberaren burujabe
jarrai dezagun aurrera beldurrik gabe 

ESTR: Beldurrik gabe
Salda Dago:
Apurtu erlojua, gelditu dezagun denbora
Egan egiteko espazioaren kaiolatik kanpora 
Ni eta zu, zu eta ni, 1+1 ez dira bi, 
Zu eta ni 1+1 ehuneko ehunekoa gara zoroen erara
Zoragarria zara hauts-izpiak bezala, 
Hautsi ispiluak ez dudalako ikusi nahi nire ikara
Nire igandeko kafea, nire ipar izarra.
Salda Dago! Xatiro!
Ni ta zu, zu ta ni: 1+1 ez dira bi…
Begira gora!