---
id: tx-447
izenburua: Vinu, Cantares Y Amor
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Eciu40enLB8
---

Putzuzulo amets fabrikaren eraiste neurtuaren bezperetan bertan grabatua. 

Bideoaren zuzendaritza: Anari Alberdi eta Iker Treviňo. 
Anari taldea. 
Koristak: Leire, Mursego, Eider, Maddi, Maialen eta Kattalin. 
Nacho Vegas-en "Vinu, cantares y amor" (Canciones populistas-2015) abestiaren bertsio eta moldaketa.


Oin entzungo duzun hau, oin entzungo duzun hau,
oihukatzen zun auzoak, funtzionariari langileak, tabernariari mozkorrak.
Oin entzungo duzun hau, esan zuen presoak
oihukatu maitaleak, mahats ahienak lurrari, erizainari gaxoak.
Oin entzungo duzun hau, oin entzungo duzun hau,
entzun zen behin Irulegin, entzun zen Oionen entzun zen Erriberrin.
Ta gertatzear zena ez da inoiz gertatuko, gertatzen ari delako
ta bada garaia erreka oso bat edateko.
Kantatu, maitatu eta dantza ezin bada, ez da gure iraultza.
Berreskuratzeko dugunagatik topa egiteko ez bada botila hori
eta ez badu uzten kantu bat ezpainetan inoiz ez dezagun sekula ezer esan.
Kantatu, maitatu eta dantza ezin bada, ez da gure iraultza.
Ez badugu bihotzetan odolik Cavernet Sauvignon bat bezain gorri
eta ez badute mila eztarrik kantatzen ta denak batera eztandan lehertzen.
Kantatu, maitatu eta dantza ezin bada, ez da gure iraultza.
Aaaaaaaaaaaaaaaa… Bezeroari putak esan zion.
Aaaaaaaaaaaaaaaa… Artistak bere bakardadean zion.
Kantatu, maitatu eta zalantza… ez badu ez da gure iraultza.
Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa….
Entzun berri duzun hau, esan zuen herri batek, esan zion munduari
Suhiltzaileak suizidari pizti gautarrak hontzari.