---
id: tx-3025
izenburua: Hitzak Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HRaKeH0ILgU
---

Hitzak dira hitzak
haizeak farrez daramazkienak
Gaueko maitasun hitzak
egunak kolorez hustutzen dituenak.

Mundua hitzetan doa hitzen eta hitzen,
bidean ibiltzen laberinto ilunean.

Hitzak agintariarenak
hain hitz merkeak
ordaina garestian pagatuak.
Sententziako hitzak, ikaragarriak
kartzelarako bidean
eta apezarenak, ahal bezalakoak
herotzaren atean.

Hitzak dira hitzak
haizeak farrez daramazkienak
Gaueko maitasun hitzak
egunak kolorez hustutzen dituenak.

Murmurio galduak
belarri ertzean
arrasto gezurti bat
gure bihotzean.

Hitzak agintariarenak
hain hitz merkeak
ordaina garestian pagatuak.
Sententziako hitzak, ikaragarriak
kartzelarako bidean
eta apezarenak, ahal bezalakoak
herotzaren atean.

Hitzak dira hitzak
haizeak farrez daramazkienak
Gaueko maitasun hitzak
egunak kolorez hustutzen dituenak.