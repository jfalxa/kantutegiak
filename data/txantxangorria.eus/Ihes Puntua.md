---
id: tx-41
izenburua: Ihes Puntua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tV-leJ2uEWA
---

'Ihes puntua' abestiak zabaltzen du Rafa Ruedaren lan berria.
CD. ZART 2020

www.rueda.eus
www.zart.eus



“RUEDA”  deritzo Rafa Ruedaren proiektu berriari. Gitarra, ahotsa, elektronika  eta literatura uztartu ditu zortzi kantuz osatutako bere bakarkako  seigarren lan luzean. Lan berri honetan eta Inge Muller, Joseba  Sarrionandia, Iñigo Astiz, Audre Lorde, Mari Luz Esteban, Josu  Goikoetxea eta Elena Olaberen testuak musikatu ditu.


Jon  Agirrezabalagarena (WAS) izan da ekoizpen artistikoaren, grabaketaren  eta elektronikaren arduraduna. Bilboko “El Tigre” estudioan burutu dute  lana.


Bideokliparen egilea: Oier Aranzabal
2020

*Zart ©


HITZAK (Elena Olave):

IHES PUNTUA

Basoan bilduta baleude
urki ta asun artean
ihardetsi nahiko nituzke
galdera guztiak.
Gauaren epelean,
emozioen kapa mehean
gorpuztasun aldakorra naiz,
orkatz eri bat.
Eta kristal hautsiak agertzen dira
eguna hiltzean.
Sendatu, sentitu aldera
ematu bitartean
trapu ta ukendu artean
agertzen dira kristal hautsiak
zeru oskarbitan
narrazio biluzitan.
Kristal hautsiak, mendaz estaliak,
ninfa baten berba
usaintsuak bailiran
haran brisan libre.