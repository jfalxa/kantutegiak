---
id: tx-2682
izenburua: Lekaixoka!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ke8ZCOhVEmI
---

2017ko irailaren 24ean Oñatin ospatuko den Kilometroak jaiaren bideoklip ofiziala.

Egileak: Kepa Aranburu eta Bernat Alberdi

Eskerrik asko auzolan erraldoi honetan modu batera edo bestera lagundu gaituzuen guztioi.

Info+: www.kilometroak.eus/abestia
---------------------------------------------------------------------------------------------------------------------------------------------------------------

Hizkuntza ta hezkuntza, ekin tira ta bultza
Ez al duzu entzuten geroa?
Bihotzak taupa taupa, gure haurrak prest dauka
Ikastolan sehaska beroa

Jolasgaraian lurra marrazten
Laura, Imanol, Ahmed eta Maite
Etorkizuna izango diren
Katebegi, begi eta ate.

Hitzak kateatzea ta ahalbidetzea
Hizkuntzari zutaz erditzea
Emozioen salda, helburua ez al da
Elkarrekin desafinatzea?

Herri batean mila eztarrik
Segi dezagun musika eginez
Eta kantari infinitorantz
Ginenetik garena izanez

Zu intzirika,
ni irrintzika
Euskaraz gatoz Lekaixoka
Ni xuxurlaka,
zu deiadarka
Euskaraz gatoz Lekaixoka

Goazen denok, helburu berari helduta
Amets egin dezagun, hegan egin dezagun
Bota ditzagun horma guztiak
Eraiki dezagun ikastola berri bat!
Herritik herriaren alde
Amesten dugun etorkizunaren alde!

Zu intzirika,
ni irrintzika
Euskaraz gatoz Lekaixoka
Ni xuxurlaka,
zu deiadarka
Euskaraz gatoz Lekaixo