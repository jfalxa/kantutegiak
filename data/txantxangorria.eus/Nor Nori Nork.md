---
id: tx-2155
izenburua: Nor Nori Nork
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6xoqpCsBz-g
---

Orion sortutako taldea, nagusiki hiru partaide izan dituzte urteotan: Asier Gozategi, Ainhoa Gozategi eta Iñigo Goikoetxea. 1995ean atera zuten lehenengo diskoa, 'Gozategi' izenekoa, eta sekulako arrakasta lortu zuten. Beste hiru disko atera dituzte geroztik, azkena 'Egunon', 2000. urtean. 

Triki-pop bezala bataiaturiko musikan sailka daiteke Gozategiren lana, eta batez ere uda giroan sortzen den musika mota horren inguruan. Reggae edo dub musikak jotzen dituzten bezala astinduko dituzte fandangoak eta arin-arinak. Jai giroko musika, alaia, dantzarako modukoa, euren disko baten aurkezpenean esan zuten bezala: 'Udazken eta negu garaiko giro goibela alaitu asmoz gatoz'. Euren azken diskoaz esan zuen kritiko batek: 'Gozategik dantzarako egarria ematen du, eta 'Egunon' diskoa fruta-koktel bat da eguzkiaren azpian'. 

'Udako kantak' egitea eta merkatuan sartzea lortu dute hainbat aldiz, eta oso ezagunak egin ziren haietako batzuk: 'Emoixtaxux muxutxuek' edo 'Nor-nori-nork', esaterako.