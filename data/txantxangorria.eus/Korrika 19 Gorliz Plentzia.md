---
id: tx-2932
izenburua: Korrika 19 Gorliz Plentzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O_t5n9FWtQ8
---

Euskara da kanpora eta guztiok Korrikara

O Euskara lauda ezan Urepele herria
handikan baitu Korrikak hasiera berria
Bilbon ospatzekotan ba kilometro azkena
Xalbadoren babesean emanen du lehena

Urepeletik Bilbora abia dadin Korrika! (ber)
Euskara da kanpora eta denok
Korrikara.

Euskaldunak herrietan ahaldun gaitezen
eta gure mintzaeraz denok harro gagozen,
Urepeletik Bilbora lekukoa gidatzen
Giza katea eten gabe orok laster goazen

Urepeletik Bilbora...


Euskahaldun den guztiak burua altxa beza,
gorenean ikusteko euskaldunon hizkuntza.
Korrikazale guztiek hauxe eskatzen dute,
nola zabaldua dagon prezia bezate.

Urepeletik Bilbora...

Gogoa konpli dezala euskal jendarteak,
euskahaldun bihurtuz datozen urteak.
10 egun eta gauez Korrika ta Euskara;
euskahaldunei eskerrak pizturik sugarra.

Urepeletik Bilbora...

Batzuk herriaz orroit, euskaraz ahantzi
bertzek euskara maite, herria gaitzetsi;
hizkuntza ta herria berex ez doatzi,
berek nahi daukute konpreniarazi
bata bertzea gabe ez daizkela bizi

Urepeletik Bilbora...