---
id: tx-50
izenburua: Argizagi Ederra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1wQ2s5IlW74
---

Argizagi ederra 
Nekatürik heltü niz bide lüze hontan, ürrats pausa güzian, beti pentsaketan, Atorra bustia düt bular errainetan, idortü nahi nüke sü ondoño hortan. 
Argizagi ederra argi egidazü, bide lüze batean joan beharra nüzü, 
Gaü hontan nahi nuke maitea mintzatü, haren ganatü arte, argi egidazü. 
Atorra busti horri garreia ezazü, nik hola manatürik bustia ez düzü! 
Bidean eman gabe kontü zük egizü, bidearen bürüan nahi zaitüzten zü. 
Ez deia bada pena eta dolorea, nihauren maiteñoa hola suntsitzea, 
Amets bat egin nüan, zük ni maitatzea, zonbat deitoragarri hola enganatzea. 
Hitzak : herrikoak 
Müsika : Jean ETCHART