---
id: tx-483
izenburua: Umetxoaren Ametsak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OFhOV3NO7pE
---

Mutiltxo batek behin galdetu zun
aita noiz etorriko dan
amak negarrez erantzun zion
zeruan dago gure zain
ta geroztik gauean
izarrak ikustean
mutiltxoak pentsatzen du
amaren magalean
laister, laister bai, laister
handia naizean
handia izaten naizenean
aitatxoren bila joango naiz
izar baten gainean
laister, laister, laister
handia naizenean
aitatxoren bila joango naiz
izar baten gainean.

Gero urteak pasa ziraden
mutil koxkorra gizondu
eta jakin zun nola aspaldiz
aitatxo gerran hil zuten
ta negarrez oroitzen da
gora begiratzean
txikitako amets haiek
amaren magalean.
Nola, nola, nola joango nintzen
txikitaz aitatxoren bila
ilunabarrean izar baten gainean
nola baina, nola?
Nola, nola, nola ilunabarrean
aitatxoren bila joango zan
izar baten gainean
Nola....