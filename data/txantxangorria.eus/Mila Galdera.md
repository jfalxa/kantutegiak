---
id: tx-1669
izenburua: Mila Galdera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_Ep809pAiEk
---

Hutsa ez al dago betea?
zuzena ote oker joatea?
"ona" eta "txarra"ren tartea
non dago nere maitea...?

Mina denontzat al da mina...?
negarra, denontzat berdina...?
ez al da gaitz bat maitemina...?
latza eta sendaezina...?

Ai, ai,... mila galdera...
Ai, ai,... baina bat bera
Ai, ai,... mila galdera...
Erantzuna non ote da...?

Zer magia da sinesmena...?
Zer, lur bati lotzen nauena...?
Nork sortu zuen "muga" izena...
... eta hesiz josi dena...?

Taktuak, formak, koloreak,
bizi al ditu nork bereak...?
Non du kabia ahuldadeak...?
almazen mugagabeak...?

Ai, ai,... mila galdera...
Ai, ai,... baina bat bera
Ai, ai,... mila galdera...
Erantzuna non ote da...?