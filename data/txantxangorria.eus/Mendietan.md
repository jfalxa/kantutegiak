---
id: tx-345
izenburua: Mendietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lbKmWubIA3w
---

Abestiaren grabaketa eta nahasketa: Nebula Studios.
Bideoa: Unai Martin.


LETRA:
Arbasoek utzitako ondarea 
bizitza eman diguna,
elikatu eta sortu gaituena...
Zuri esker gu gara.

Noizbait ikasiko dugu zu babesten zuk gu zaintzen gaituzun bezala?

Zuloz bete zaituzte
urratu goitik behera
zure zuhaitzak moztu 
laku guztiak lehortu.
Gurea den haizea
lapurtu nahi digute
gutxi batzuek soilik
poltsikoak bete ditzaten.

Gure lautada 
gordetzen duten mendiak. 
Errotaz josita
burdinezko deabruak...
Dena suntsitzen dutena!