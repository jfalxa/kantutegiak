---
id: tx-2431
izenburua: Korroan Alai Alai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/71IZcRls9Wk
---

Hau da nire eskua,
polita benetan,
hau da beste eskua,
polita benetan;
hau da zure eskua,
po/li/ta be/ne/tan.


Ai! Trula, trulalai,
korroan alai-alai.
Trula, trulalai,
korroan alai-alai.

Hau da nire besoa
polita benetan,
hau da beste besoa
polita benetan;
hau da zure besoa
polita benetan.

Ai! Trula, trulalai,
korroan alai-alai.
Trula, trulalai,
korroan alai-alai.

Hau da ni/re u/kon/do/a
po/li/ta be/ne/tan,
hau da bes/te u/kon/do/a
po/li/ta be/ne/tan;
hau da zu/re u/kon/do/a
po/li/ta be/ne/tan.


Ai! Trula, trulalai,
korroan alai-alai.
Trula, trulalai,
korroan alai-alai.

Hau da nire belauna
polita benetan,
hau da beste belauna
polita benetan;
hau da zure belauna
polita benetan.

Ai! Trula, trulalai,
korroan alai-alai.
Trula, trulalai,
korroan alai-alai.