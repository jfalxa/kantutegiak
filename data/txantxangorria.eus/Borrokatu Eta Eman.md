---
id: tx-313
izenburua: Borrokatu Eta Eman
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v6jRMh-V0LY
---

Hitzak : Maddi Sarasua
Musika : Iker Lauroba
Featuring ….Izaro !!!
 
Hark bizi izan zituen
gerla, gosea ta hotza,
auzolana, etxaldea, 
lege zaharren heriotza.
Gogor pairatu zituen
miseria eta bortxa.
Zaintzailea, babeslea,
langile goxo airosa.
Gure familian nahiz ta 
nehoiz ez egin aitortza
gure amatxi izan zen
eskua eta bihotza.

Zuengandik jaso ditut
horixe da maitasuna:
borrokatzeko indarra
ta emaiteko gaitasuna.

Bere alabak izan nahi
zuen ama birjina,
ta bukatu du izaiten 
estetiken biktima.
Beldurrez zeharkatzen du
gauez kale ixkina,
bizi aukera bakarra 
du merkatu gordina.
Ene amagan ikusten 
dut nahia ta ezina
sistema injustu huntan 
preso den ahalegina.









Zuengandik jaso ditut
horixe da maitasuna:
borrokatzeko indarra
ta emaiteko gaitasuna.

Ama, amatxi ez dakit
zer den emaztetasuna,
ez da Merkel, ez Beyonce
naizenaren oihartzuna.
Guri egokitu zaigu 
zapalduen erresuma,
zapaltzaileen doktrina, 
krisien etorkizuna.

Bizkarrean senditzen dut 
nere erantzukizuna,
zuengandik jaso ditut
horixe da maitasuna:
borrokatzeko indarra
ta emaiteko gaitasuna