---
id: tx-2703
izenburua: Aitatxiren Autoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PAWFLjoOs-Y
---

Gure aitatxik dauka oto bat xarmanta
Urtez aberats eta motorrez marranta.
Jazko neguaz geroz zilo du kapota
Hoberena du naski kautxuzko turruta.

Hauxe da motorra, dena su ‘ta karra
Oi hunen indarra, jausten duelarik patarra.

Urrundik entzun eta otoa eztulka
Zakurrak hasten dira ihesik saingaka
Leherturik baditu oiloak ehunka
Mutxurdinak hamar bat, jandarmak hameka.

Gure aitatxi baino kontentago denik
Bere karroaz ez da amerikanorik
Luzaz iduki ditzan bi biak bizirik
Jaungoikoari otoitz egiten diot nik.