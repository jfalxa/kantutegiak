---
id: tx-3241
izenburua: Euskaraz Bizi Nahi Dut -Alaitz Eta Maider-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qHkwa3HrifU
---

Deika ari da mobila
Nor ote nere bila?
Mezuren bat isila...
Jasotzen dute mila.

Bat gehi bi hiru mezu 
Lazkaotik datoz, aizu
Haserre eta kexu
Entzun, entzun itzazu:

GAUR BIHAR ETA ETZI
NAHI DUT EUSKARAZ BIZI
GOIZ, ARRATS ETA GAUAZ
BIZI NAHI DUT EUSKARAZ
BERRIZ ESANGO DIZUT:
EUSKARAZ BIZI BAHI DUT

Mezua da bakarra
Erantzuna, azkarra:
Nirea, Lazkaotarra,
Da zure deiadarra.

Jakin (e)zazu gaurdanik 
Ez zaudela bakarrik
Zurekin bat eginik 
Hauxe esan nahi dut nik:

Kilometroak egin
Nahi dituzu gurekin?
Zatoz Lazkaora arin
Zugabe ez da berdin.
Egunak badu izen:
Urriak bost, hain zuzen.
Zatoz eta goazen 
Oihukatzera ozen:

GAUR BIHAR ETA ETZI
NAHI DUT EUSKARAZ BIZI
GOIZ, ARRATS ETA GAUAZ
BIZI NAHI DUT EUSKARAZ
BERRIZ ESANGO DIZUT:
EUSKARAZ BIZI NAHI DUT