---
id: tx-2496
izenburua: Maurizia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yK86h1MZLGk
---

Arteako plazako
mahai baten gainean
sentimendua kantu
bihurtzen zenean.

Erritmoz jantzitako
ahots biluzia
irrintziei galdetu
nor zen Maurizia.

Bizitza sortzaileen
etxeko morrontza
apurtzera plazara
zetorren ahotsa

Bizipozen basotik
kantari bazoaz
bidaide izango da
panderoa joaz.