---
id: tx-2766
izenburua: Herriko Festak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pthvaX2VHK4
---

Herriko festak ziran biharamonian
berak nahi zuten txoko txoko batian
harri zabal zabal baten gainian
ari ziran, ari ziran trukian.
Lau atso, hiru neska zahar,
bat alarguna jarririk itzalian
harri zabal zabal baten gainian
ari ziran, ari ziran trukian.
Hasi ziradenian kartak partitzen
eta gainera tantuak kontatzen:
truk jo ta Iñaxik txota,
Mariak txaldun ta Errufinak hirua.
Mari Martin txoratu al zain burua?
Lauarekin hiruaren kinua.
Herriko festak...
Neska egon hadi isil isilikan
hik ez den ikusi nere keinurikan,
ttanta bat edan ezkero
begiak briz-briz, ezin egon isilik.
Eran ezan, eran ezan gogotik
katilua, katilua beterik.
Herriko festak...