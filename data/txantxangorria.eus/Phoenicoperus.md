---
id: tx-1431
izenburua: Phoenicoperus
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MWkPLfcjyMs
---

Ikusten ez diren loturak, estuenak

espero gabean azaldu litezkeenak.

Eztarri erdiko aingura trabatuak:

zauria berbera dugula diotenak.

Horrela egin daigun

zauriak miazkatu,

arnasak elkar lotu.

Horrela…

 

Lotura latzenei pozarren emanik su,

arbola emankor bihurtu ditzakezu.

Gaur dena fruitua, bihar da berriz hautsa

hegoak astindu, errautsak gorantz altxa!
 

Horrela egin daigun

arnasak elkar lotu.

Horrela egin daigun

arnasketatik, hegaldia.

Horrela egin daigun

keinuetatik hegaztia.

Horrela egin daigun

zaurietatik hegan egin,

horrela egin daigun

zaurietatik hegan egin.

Zaurietatik hegan egin.