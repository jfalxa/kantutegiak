---
id: tx-1928
izenburua: Trago Bakoitzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0LIi5SsU6e8
---

zulo iluneneta zure utsa pasiatzen nabil
labankada guztiak alkoholean ito nahirik
jelak urtzerakoan hartzen duten formari
zure ezpainen antza bilatu nahi diot berriz
eta trago bakoitzean
lapurtzen dizut muxu bat
baina zure zaporea jada ez da lehenokua
parrandetako gauetan neska zuk bazeneki
zebat bilatu zaitudan baina ez zinen ageri
zenbat gorputz laztandu zurea laztandu ezinik
zenbat larrua jo dudan
maitasuna eginez zurekin
eta trago bakoitzean
lapurtzen dizut muxu bat
baina zure zaporea jada ez da lehenokua
Tabernetan babesten banaiz
mozkorkeritan banabil maiz
utz nazazue hiltzen lasai
hirugarren egunean berpiztuko naiz!
Biharamuna goizean bila badatorkit
ireki taberna eta atera pare bat whiski
jelak urtzerakoan hartzen duten formari 
zure ezpainen antza bilatu nahi diot berriz
eta trago bakoitzean
lapurtzen dizut muxu bat
baina zure zaporea jada ez da lehenokua
Tabernetan babesten banaiz
mozkorkeritan banabil maiz
utz nazazue hiltzen lasai
hirugarren egunean berpiztuko naiz!