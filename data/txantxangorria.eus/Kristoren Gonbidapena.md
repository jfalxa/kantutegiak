---
id: tx-1299
izenburua: Kristoren Gonbidapena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uuQuI7nGNdI
---

Kantua: Kristoren gonbidapena
Taldea: Don Inorrez
Argitaratzailea: Lizarbakar
Ekoizlea: Paco Loco
Bideoaren gidoia eta ekoizpena: Hiru Damatxo Ideia Faktoria



Kristoren gonbidapena

Halaxe esan zuen: 
Jan ezazue, hau duzue nire gorputza. 
Edan ezazue, hau duzue nire odola. 

Eta haren ingurua bete zen milaka 
hienaz, 
banpiroz.