---
id: tx-2487
izenburua: Mamarigako Larreak Legegabea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m9q27mvL2oE
---

Mamarigako larreak
mamarigako larreak 
asfaltozko mortuak bihurtu dira
arbasoen baserria zutikan ene bihotzian
xori libre nun haziko ditu kumiak.

Bat, bi, hiru, lau

Urruti 
gartzelan
emakume gazte bat dabil dantzan
larailai, larailai, larailailailai

Kristianoz isiltzera
agindu zen gartzeleruak
ta bera ez zen kikildu ta zigortua izan zan.

Emeki emeki
abesten
Xalbadorren heriotzean
garbitzen zituen
zelda
ta komun zikinak
gure amodioa haizean doa
iraultzaile ahaztuentzat
barroteen artean ihesean dohan irria.

Laimixen negar kantak 
aturri
nerbioi
ta ebro ibaietan
nere ziegan aitzen ditut
gor mutuen erreinuan
ihintza izan nahi nuke
Mamarigako lareetan
ezabatzeko betiko gris kolore
hiltzaile hau!