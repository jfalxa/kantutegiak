---
id: tx-43
izenburua: Ez Naiz Ni
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bxhhcorYBDA
---

Una foto de otro tiempo
Un reflejo que se va
Ispiluan galduta
Ez daki datorren edo doan
La madera de este roble
Fue testigo de todo el amor
Maitasunak, loturak
Eta egurrezko sustraiak
Ez naiz ni loturik
Ez naiz ni zurekin
Abriré un gran ventanal
Para que me abrace nueva luz
Leihoak zabalik
Argia sartzea behar dut nik
Ez naiz ni loturik
Ez naiz ni zurekin
Aaaaaa aaaa aaaaa
Aaaaaa aaaa aaaaa
Ez naiz ni loturik
Ez naiz ni zurekin
Ez naiz ni loturik
Ez naiz ni zurekin
Aaaaaa aaaa aaaaa
Aaaaaa aaaa aaaaa