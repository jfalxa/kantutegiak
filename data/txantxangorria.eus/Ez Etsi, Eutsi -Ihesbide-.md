---
id: tx-3330
izenburua: Ez Etsi, Eutsi -Ihesbide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2qJyGC7mR5o
---

hotzez eta urduri
begiak estalirik
ziega naskagarri hartan
gorputza minduta baina
burua galdu barik
orduak aurrera doaz!

garraxiak isiltasuna apurtuz
jipoitua txakur ustelen eskutan
ez etsi,duintasunez jarrai
zure borrokari eutsi
maite duzun eta hargatik
dena eman duzun herria
harro dago zutaz

arma baten kañoia
duzu orain ahoan
barre gaiztoen artean
tortura
eguneroko jokoa
komisarietako gela ilunetan
odoletan zikindutako eskuak
garbitzen dabiltzate
agintari faxisten bulegoetan