---
id: tx-2589
izenburua: Tinitoren Terraza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DkYk6SAnDW4
---

Tinitoren anaiak
goian du terraza.
Igo dut eskailea
Jaitsi dut eskailera
ta han dago terraza.
Ai, ai, ai txoratuta nago 
terrazara joateko.
Jaitsi naiz berriro
etxeraino.
tras, tras, tras
entzuten da eskailera.
trin, trin; trin
nere etxeko txirrina.
tok, tok, tok
jotzen dute atea.
trum, trum, trum
trumoiaren zarata.
Tinitoren anaiak
goian du terraza.
Igo dut eskailera
Jaitsi dut eskailera
ta han dago
Tinitoren terraza.