---
id: tx-1231
izenburua: Ormatxulo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oFHMbUfqxYM
---

Ormatxulo batetik
gau batean ihesi
xagu ttiki polit bat
nuen nik ikusi
zihoan bizkorki!

Salto eta brinkoka
zebilela alaiki,
katu batek heldu zuen
bere hatzaparrekin
ta ezin ihes egin!

Katuzarrak izanik
indar franko haundia
ez zuen askatu nahi
xagutxo gaisoa.
Nahiz egin nigarra!

Xagotxoak orduan
eginik deiadarra
katuari esan zion:
Txatxu eta zakarra,
egingo dizut farra.

Hau entzunez katua
zen guztiz haserretu,
xagua akabatzea
zitzaion bururatu
eta hatzak altxatu!

Instant hortaz xagua
zen ondo baliatu
amenjesus batean
handik abiatu,
ta ormatxuloan sartu!

Ormatxulo barnean
dago xagu gaisoa,
ezin ahantz dezake
gertatutakoa
A ze disgustoa!

Txit ederrak izanik
gauza urrutikoak,
honek orain nahiago du
gertu ta bertakoa:
bere ormatxuloa!