---
id: tx-361
izenburua: Eusko Gudariak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0Z79euRbBB4
---

Irudien (bideokliparen) ekoizpena:


Irrintzi bat entzunda
goiko tontorrean,
goazen gudari danok
Ikurriñan atzean.

Faszistak datoz eta
Euskadira sartzen.
goazen gudari danok
geure_aberria zaintzen

Eusko Gudariak gera
Euskadi askatzeko,
gerturik daukagu_odola
bere aldez emateko.

Gure aberri laztanak
dei egin dausku-ta
goazen gudari danok
izkiluak hartuta.

Arratiarren borroka
izan da Intxortan
Mola ta erreketek
gelditu dira bertan.

Ikurriña goi-goian
daukagu zabalik
ez da iñor munduan
eratsiko dabenik.

Eusko Gudariak gera
Euskadi askatzeko,
gerturik daukagu_odola
bere aldez emateko.


Nahiz ta etorri Mola
ta mila rekete,
ez/ta iñor sartuko
bizirik garen arte.

Agur Intxorta maite
gomutagarria,
Arratiako mutilen
garaitzaren tokia.

Ezta sartuko iñor
ez Euskal Herrian
eusko gudariren bat
zutik dagon artian.