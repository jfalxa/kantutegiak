---
id: tx-1639
izenburua: Bizkaia Gipuzkoa Eta Xiberoa Etxahun Iruri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YI0NqZQD1WE
---

BIZKAIA, GIPUZKOA ETA XÜBERO

 

                1

Goxo da bizitzia Xibero gañian

Mendi horien artian trankilitatian

Hargatik aithortzen dit mügaz bestaldian

Badirela xokho hunak, itxas bazterrian

Khantatzera banua huk phentsamentian

 

                Errepika

Ondarabia ta Donostia ere bai Bilbao

Eskual Herrian ederrez zidie haiñ pare gabeko

Ene bihotza hartü düzie osorik bethiko

Ziek egin batzarri ederra ez dit ahatzeko

 

                2

Xokhota batetanda fierrik Ondarabi

Hurez gerrikatürik erriz Hendaiari

Bere gaztelü zahar misteriosareki

Lehenago güdüetan nula kolpatürik

Santa Maria eliza huntarzünez fundi.

 

                3

Monte Igualdetik zit agur Donostia

Denen artian zira arrosa lilia

Kontxaren üngürian bibil da herria

Bere mila kolorekin espantagarria

Ta herrotik Eskualdün da hanko jentia.

 

                4

Herri handienetarik bat dügü Bilbao

Itxas port famatia da Atlantikako

Indüstriatik bizi da popülia oro

Denak zühür ta agudo eta bihotz bero

Muxi bat begiratüz bethi guretako

 

                5

Kunserba zitzaiela zelüko Jinkuak

Bizkai ta Gipuzkoako probintzia goxuak

Ingana zitzaiela luzaz itxasuak

Guretako begiratüz bethi ber amodiua

Zirekilan beikira oso Xiberua