---
id: tx-1677
izenburua: Negua Datorrenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QvVT_he9SVs
---

Udazkenean jaio berria
hosto lehorren artean;
alaitasunaren hazia
begi gardenetan zehar...

Denari parrez, nola bestela?
Kezkarik gabe bizi naizela....

Ai amatxo, ez dut hazi nahi!!
Ai amatxo, negua dut zai!!

Nagusien moral zaharrak aldatzen gaitu
gezur hutsa egi gisa irakatsiz
eta ezin aske izan mundu horretan
diru-gose hasegaitzak irentsita.

Ez dut inoiz lotsarik izan
Ez dut inor lotsatu nahi
Ez dut mozkortu beharrikan
Pozik izateko naiz gai.

Pauso hotsak oihu latzak, euri artean
gorrotoa kale arrotz bustietan
arnas gabe babes eske lasterka noa
aginduz at, haur bezala bizi nahi dut.