---
id: tx-499
izenburua: Amatxo Laztana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0RH1SaFGHoc
---

Seme bat bere amaren ondoan ainbat

ez da oroitzen zeiñekin daguan

argandik urrutira joaten dan orduan

konturatzen da amak zer balio duan.

 

Aita, ama, lagunak, herri maitatua

ain nago zuengandik urrutiratua

eri daukat bihotza, penaz nekatua

eta falta zait hemen zeñek sendatua.

 

Ama onak, seme bat urrutira joana,

ama gabe iñola ezin bizi dana;

ama, zuk badakizu maite zaitudana,

arren barka zaidazu, amatxo laztana.

 

Hau da tristura eta hau pena barrena,

ai aldegin zaizkit indarrik gehienak.

Sinistu ez dakilla probatu ez duenak

zenbat pixatzen duen egitazko penak.