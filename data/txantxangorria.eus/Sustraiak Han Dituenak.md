---
id: tx-781
izenburua: Sustraiak Han Dituenak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dU8pTD-uzYU
---

Nekez uzten du bere sorterria
Sustraiak han dituenak.
Nekez uzten du bere lurra zuhaitzak
Ez bada abaildu eta oholetan.
Ez du niniak begia uzten
Ez bada erroi edo arrubioen mokoetan.
Nekez uzten du gesalak itsasoa
Ez hareharriak basamortua.
Ez du liliak udaberria uzten
Ez elurrak zuritasuna.
Bere sorterria nekez uzten du
Sustraiak han dituenak