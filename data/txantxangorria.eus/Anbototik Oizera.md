---
id: tx-1763
izenburua: Anbototik Oizera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/clYcTTNUjp8
---

Anbototik Oizera
erratzak airean
sorginak bazebiltzan
bideko librean.
 
Bota zieten ura
Durango parean
erratzak itzalita
erori zirean.
 
Kondenatuak gero
barkamenik gabe
infernuko hauspoa
salbatu halare.
 
Bidekotxo bat haien
arimaren alde
eta erromeria
bizirik badaude.

Hitzak: Xabier Amuriza