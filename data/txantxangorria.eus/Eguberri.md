---
id: tx-2377
izenburua: Eguberri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kefJbMnci6Q
---

Egileak: Iñaki Zabaleta y Yolanda Ibero

Eguberri, egurrerako
bakerako den haurra jaio berri
Eguberri, egurrerako
bakerako den haurra jaio berri

Naturak lurrezko sehaska,
gerrak ezpatazko sehaska.
Iluna nahi diot egin
egurrerako den haurrari.

Eguberri, egurrerako
Abakerako den haurra jaio berri
Eguberri, egurrerako
bakerako den haurra jaio berri

Eta negu hoietan,
maitasunez bizi den gizona,
lastozko sehaska dagio,
lastozko esperantza...
bizi bakerako jaio den haurrari.

Eguberri, egurrerako
bakerako den haurra jaio berri
Eguberri, egurrerako
bakerako den haurra jaio berri

Eguberri, hiltzetarako
bakerako den haurra jaio berri
Eguberri, hiltzetarako
bakerako den haurra jaio berri