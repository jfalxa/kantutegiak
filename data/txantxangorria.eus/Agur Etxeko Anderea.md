---
id: tx-9
izenburua: Agur Etxeko Anderea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eHd2KXIaRFM
---

Agur etxeko-anderea,
Gure diruen jabia,
Berriz ere behar dugu arnoarekin ogia,
Bai eta ere, arratsian, kartekilan argia.

Kartak hiruta kartak Iau,
Kartak eman deraut hau
Hamahiru izan eta ezin galdu hamalau,
Sakelan tudan diruekin afalduren ez naiz gaur!

Utzak jokua burutik
Partitu gabe mundutik!
Aditzia bainan, bainan jokuak adar makurtik
Hik nekez irabaziak ostalerrak juanen tik

Lau arrosa, lau begi,
Bortz arrosa'ta bortz hegi
Gizon deboxaren haurrak maiz gose edo
egarri.
Atalatzeari jarri eta iguzkia janari