---
id: tx-1801
izenburua: Amaren Besoetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HKm6DOawsWQ
---

Oinez zelai berdean
itsaso bazterrean
gailurrak ditut maite
eta kresal usaina.

Itsas bazterreko
ur gazi buztietan
garbitzen dut arima
ametsez betea.

Eukal herri osoko
mendi guztietan
etxean aurkitzen naiz,
amaren besoetan.

Lurrean etxanda
goruntz begira
ni ohartzen naiz
zu maite zaitudala.