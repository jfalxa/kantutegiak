---
id: tx-1922
izenburua: Festarik Festa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/37bx3FmmZoA
---

Nahiz gauza asko ezagutu munduan
Bada zertxobait hartu nahi dut kontuan
Ez dakit nola esplikatu kantuan
Neskatxa bat dut hemen nire onduan.

Gure bizitza juerga eta parranda
Gero egunez lo egiten dugu etzanda
Horren errudun etsaien bat izan da
Gu non gabiltzan seguru bera han da.

Festarik festa lan egin behar gauero
Festarik festa hor nabil ni egunero
Gure etsaia benetan dago ero
Zigortzen gaitu inoiz hutsegin ezkero.

Aizu neskatxa ba al dakizu benetan
Norekin hasi zeran hartu-emanetan?
Hau da egia ziur zaude horretan
Bihurtu leike maitasuna penetan.

Komplikazio anitz bada gurekin
Bizitza honi ezin diogu aldegin
Nire aukera ez bazaizu atsegin
Gure artekoa beharko da desegin

Festarik festa...