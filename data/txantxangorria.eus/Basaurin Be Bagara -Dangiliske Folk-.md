---
id: tx-3293
izenburua: Basaurin Be Bagara -Dangiliske Folk-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IOdMrFhhdBk
---

Basaurin Be Bagara

Eguantz ilun honetan

nagie altxatzean

gautxori bet jarri da 

maindire gainean

berbetan mihia astintzen

hasi nazenean

nigana salto batez

parau da aurrean

gorputzaz berba eginez

dantzan dau airean

Etor zatez laguna

ta eutsi goiari

Basaurin be bagara

hamaika gautxori

euskaraz margozteko 

irribarre ugari

argia emongotzegu

biharko gauari

Atoan urten gara

leihotik hegaka

haizearen laguntzaz

hodeiak zeharka

bidean topau dogu 

txori asko koplaka

abotsagaz bat egin

doinue aparta.

Basaurin be txoriok 

euskaraz txioka

Euskaldunak kalera

eta berba egin daigun

sorginkeriaz sortu

hainbat berba lagun

geuri kanta ta dantza

bai gustetan jakun

horrela ibltzen gara

zenbait gau eta egun

gu elkartzen bagara

Basaurin be ingu

Dangiliske Folk