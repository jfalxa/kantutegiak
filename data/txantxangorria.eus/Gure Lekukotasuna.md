---
id: tx-1190
izenburua: Gure Lekukotasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YuVCA1cqYNM
---

Zuen aurrean, jaun-andereak,
ibiltzen gara askotan...
bozkarioa ezarri nahiz
denen gogo-bihotzetan!
Baina giroa, ez dago naski,
alaitzeko denboretan ...
Oraindik ere gure herria
ari baita negarretan! (bis)

Ez dugu segur gauza haundirik
Herriari eskaintzeko...
Bakar-bakarrik zenbait olerki
hemen zuei kantatzeko.
Guk ere zinez, gure Herria
asko maite dugulako.
Eta kantuaz, gure moldean,
nolazbait zerbitzatzeko... (bis)

Badakigu bai, kantuaz beste,
nahi duela Herriak...
Bainan ez dira denontzat berdin
iraultzaren iturriak
Bakoitzak beraz, bere sailean,
ezar ditzala harriak...
Elkarren lanak, ongi uztartuz,
altxatzen dira etxeak !

Etxe batean, nork ez du maite
beti kantuz ari dena ?
Baina kantua, ez da ez aski,
egin behar ere lana !
Gaurko kantu hau izan dadila
gure lekukotasuna...
Burruka gaiten irabazteko
Euskadin askatasuna! (bis)

(Daniel Landart)