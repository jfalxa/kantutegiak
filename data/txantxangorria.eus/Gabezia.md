---
id: tx-2327
izenburua: Gabezia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/REcu-_54O0g
---

Gure Garaia" Bideoklip ofiziala.

Gure azkenengo diska "HAZI" Urriak 13 kaleratuko dugu CD-an eta Urriak 27 edonon (Spotify, Youtube, iTunes...)

Eskerrik asko bideokliparen parte izan zaretenei! 
Aktoreak: Javi Thomson, Siko .
Zuzendaria: Aitor Zarzuelo.


LYRICS:
Zuen garaia pasa da jada,
gurea berriz, berriz hasi da.

Zuen hitzen aurka, gure ekintza bakanak.
Itsu ginen baina, esnatu gara.

Zuen tintaren aurka, gure porroten odola.

Esnatu, geroaren doinuaren bila,
estilo izengabeko aitzindariak.

Zuen hitzen aurka, gure ekintza bakanak,
itsu ginen baina, esnatu gara behingoz.

Zuen garaia pasa da jada,
gurea berriz, berriz hasi da.
Zuen mezua susmagarria da,
gurea uka, ukaezina.

Hauts bilakatzearen beldurra
haragiaren garrasia da.
Gure gorputza lurrunduko da,
gure eragina, AHAZTEZINA.

Ibiltzen doaz, ibiltzen doaz...
hegan egin dezaketela ez baitakite.

Gure ehorzketaren zain zineten, oker berriz ere, 
haragia zu, gu? ez gara inoiz hilko. 
Musika bilakatu gara.

Eta nork ez du faltan bota komeni ez zitzaiona?
eta inoiz galdu ez duen baten aurrean nago,
galdutakoan ikasi zuelako.


PRE-ORDER NOW