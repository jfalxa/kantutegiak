---
id: tx-938
izenburua: Ametsak Amets
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EepRQQbAyZQ
---

Ilunak ilun begi bietatik
zorionaren sua dariola
ez dakit nik sekula aitortu dizudan
amets hutsa dirudizula.

Laztanak laztan, zorion betean
zelatan dago beti ezereza;
hitzegizu, hitzegin, sinista dezadan
den-dena ez dela ametsa.

Ametsak amets baldin balira ere;
egin nazazu zure;
kixkal nazazu zure su-garretan,
urtu nazazu, maite,
zuregan zu izan nadin arte.

Muxuak muxu, hasperen doi batez
esan didazu bakar-bakarrikan
su guztien suaren itxarongeletan
ez dela hitzen beharrikan.

Jolasak jolas, zalantzak pindar bat
eratxeki dio su festa honi:
nire ametsen ametsa ez ote zaren zu
ta zure ametsen ametsa ni.

Ametsak amets baldin balira ere;
egin nazazu zure;
kixkal nazazu zure su-garretan,
urtu nazazu, maite,
zuregan zu izan nadin arte.