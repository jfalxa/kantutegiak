---
id: tx-1895
izenburua: Goizean Goizik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B99X1J3s5gQ
---

Goizean goiz jeikirik, argia gaberik,
urera joan ninduzun pegarra harturik.

Jaun xapeldun gazte bat jin zautan ondotik,
heia nahi nuenez urera lagunik.

Nik ez nuela nahi aurera lagunik,
aita beha zagola salako leihotik.

Aita beha zagola, ezetz errangatik,
pegarra joan zerautan besotik harturik.

Urera ginenian, biak buruz buru,
galdigin zautan ere: -"Zonbat urthe tuzu?"

-"Hamasei... hamazazpi orain'ez konplitu
zurekin ezkontzeko gaztexegi nuzu.

Etxerat itzultzeko nik dutan beldurra,
ez jakin nola pentsa amari gezurra!"

-"Niska, nahi duzu nik erakuts zuri 
etxerat etortzean zer erran amari?"

"Urzo xuri pollit bat, gabaz dabilana,
hark ura zikindurik, egotu naiz ama."

-"Dakigunaz gerotzik zer erran amari,
dugun pegarra pausa, gaitezen liberti."