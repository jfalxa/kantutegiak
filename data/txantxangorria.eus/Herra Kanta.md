---
id: tx-1720
izenburua: Herra Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h4wzs7pIKQY
---

Ahaztu beharreko gauzen zerrendan, izen bat
zara orain, gaizki egindako gauzen artean, beste
bat. Oraindik bizirik zauzkat barruan eta ehortzi
egin behar. Hau bizitzeko gogo bizi hiltzea bezala
da. Orain gauero oheratzen naiz zure ausentziaz,
eta zutaz aritzen zait, zure gogoekin larrua jo ere,
behin baino gehiagotan, hemendik aurrera
ezezagun bat zara nahiz eta zure fotoa beti izan
ohe alboan. Zurekin egotera ohitzen hasi eta
orain zu gabe egoten ikasi behar, gauzak gaizki
giteari noiz utziko diodan galdetu behar neure
buruari, ostera, gauzak gaizki egiteari noiz utziko
diodan.