---
id: tx-1325
izenburua: Elurra Mara Mara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1X9QV2fEPNk
---

Elurra mara mara ari da,
Zuritu du mendia,
Arditxoak sartu dira hotz-hotzez
Txabol, txabol zaharrean
Nik badaukat bildots txiki bat
Hemen seaskatxoan
Baina haun jango duan otsorik
Ez da sortu lurrian
Elurra mara-mara...
Haurtxoak loak hartu digu
Jango nuke musuka;
Aingerurik baldin bada,
Hemen gure Haurtxoa
Elurra mara-mara...