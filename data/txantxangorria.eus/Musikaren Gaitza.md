---
id: tx-231
izenburua: Musikaren Gaitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0PNB1h2Ai9Y
---

BOTAKANTU - 'Musikaren gaitza'
Lehen konposizioa, konfinamendu denboraldian konposatua eta idatzia.
Première composition, écrite et composée pendant la période de confinement.
___________________________________

Hitzak :

Kaleen isiltasuna, entzuten hasten da,
Gure ostatu maiteenak, lokartuak dira
Gaztetasunaren pazientzia, galtzen bada
Musikaren doinua dugu beti buruan bira

Aire zabalean denboran bidaiatu,
Eguneroko bizia nonbait berreskuratu
Lehen zegoen giroa, urrun dela badakigu
Baina musikaren gaitza, ez dugu galdu.

Gogoratzen ditugun bestak, bereziak dira,
Kantaldi amaiezinak, goizetik gauera
Gastetasunaren sumindura, handitzen da, 
Baina musikaren ohitura, atxikitzen da.

Traduction FR : 
Le virus de la musique

Le silence des rues commence à s'entendre
Nos bars favoris sont endormis
Même si la jeunesse perd patience
Nous aurons toujours cette mélodie dans la tête

Voyager dans le temps à ciel ouvert,
Pour y retrouver quelque part notre quotidien,
Nous savons que l'atmosphère d'antan est encore loin,
Mais nous n'avons pas perdu le virus de la musique.

Les fêtes dont nous nous souvenons étaient spéciales,
Des moments de chant interminables, du matin au soir,
La colère de la jeunesse grandit,
Mais la tradition de la musique perdure.

___________________________________
✍ Hitzak : Julen
🎶 Konposizioa : Julen - Kby
🎤 Interpretazioa : Julen - Kby - Maxime - Ludo - Thomas - Nico - Mikel
🎥 Bideoa : Maitena