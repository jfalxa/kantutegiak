---
id: tx-1293
izenburua: Apurtu Kateak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v0INOMdeVc8
---

Itzali telebista!
eta zatoz Sound Systemera
hau reggae musika da
reggae musika sustraia
eta kultura da

Itzali telebista!
eta zatoz Sound Systemera
hau reggaae musika da
reggae musika sustraia
eta kultura da

Plentziatik natorren artista bat naiz ni
Plentziatik natorren artista bat naiz ni
reggae entzuten dut
baita abestu ere
lagunekaz egiten dut
hori da helburua, burua, burua

Plentziatik natorren artista bat naiz ni
Plentziatik natorren artista bat naiz ni
reggae entzuten dut
baita abestu ere
lagunekaz egiten dut
hori da helburua, burua, burua
apurtu kateak
Goazen denok batera!
Hartzera!
Zilarra ta aurrea reggaearen etxera
bidali faxistak espetxera
haien demokrazia suntzitzailea suntzitzera
Ea! Ea!
Nork sartzen duen hobea gezurra
ipuinan bezala luzatzen zaio sudurra
galdera bat, nor da hemen lapurra?
Nor izan zen kendu ziguna gure lurra?
Haiek altxorra, guk geure buru gogorra
denborak emango dio bakoitzari bere zigorra
Euskal Herria eta España, atzerria
ez dit inork esango zein den nere aberria
baina hau ez da berria
dauden arazoekin ez dakit nondik lortzen duten ziria
jotzeko astia, 
hesiak jarri dizkigute herriaren inguruan
hauek apurtzea da gure helburua

Itzali telebista!
eta zatoz Sound Systemera
hau reggae musika da
reggae musika sustraia
eta kultura da

Itzali telebista!
eta zatoz Sound Systemera
hau reggaae musika da
reggae musika sustraia
eta kultura da

Plentziatik natorren artista bat naiz ni
Plentziatik natorren artista bat naiz ni
reggae entzuten dut
baita abestu ere
lagunekaz egiten dut
hori da helburua, burua, burua

Plentziatik natorren artista bat naiz ni
Plentziatik natorren artista bat naiz ni
reggae entzuten dut
baita abestu ere
lagunekaz egiten dut
hori da helburua, burua, burua