---
id: tx-114
izenburua: Ene Azken Bersetak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O-ETB0GGuR0
---

Kantore horik huntü dititzüt lür lanaren inganio, Sasuen gisa, batian trixte, bestian oro boztario, Denbora hein bat lüze üken düt horien ororen medioz, Nik uste beharko düdan erran laster güzier adio! 
A! mündü huntako izatea, nunbait behar enganatü, Hun-gaixtuk ahal bezala egartez, gero güziak kitatü, Arragretik ez dü ükeiteko hein bat dianak gozatü, Nik ja hortan ez diket herrürik, klarki nahi düt aitortü. 
Betiz geroztik ixtant goxorik gozatzen dü koblakarik, Uhure franko, mahain ederrik, ahal bezain üsü kurri, Adixkide hanitx egiten dü, ere traidore ederrik, 
Bena nihure ez da akabi sail hortan aberastürik! 
Irus bizi niz aide sanuan, laborantxan Xiberoan, Kantore zunbait huntüz mendian edo sütondo xokoan, Horik eskentzen dereiziet, bat begiratüz gogoan, Entrada gisa kanta dezagün Josefateko sohoan. 

Hitzak: ETXAHUN Iruri Musika: Jean ETCHART