---
id: tx-3013
izenburua: Esperantzarik Gabeko Amodioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CSP5RmInWHA
---

Txomin Artola. 
"Esperantzarik gabeko amodioa"

Musika:Txomin Artola.
Hitzak: Fernando Aire "Xalbador".

07.01.1995
Lazkao Txiki bertsolariaren omenez.
Lazkao. ETB.

Txomin Artola: Ahotsa & kitarra.
Joxan Goikoetxea: Akordeoia.
Alan Griffin: Klarinetea.

www.banako.com
www.aztarna.com
www.joxan.com

Amodioa gauza tristea, bihotzen higatzailea,
guziz enea bezala bada esperantzarik gabea.
Gizonarentzat hau baino malur handiagorik badea ?
Ikusten eta ezin eskura maitatzen duen lorea !...
Hunela bizi baino aunitzez hobe litaike hiltzea !

O, Jainko ona, zendako bada egin nauzu hoin maluros,
begien bixtan izar eder bat ezarri dautazulakotz,
ikus orduko maite nuena nik ditutan indar oroz ?
Harek ordainez ez du enetzat ez begi eta ez bihotz...

Oi, ene maite ezin ukana, zoin zaren xarmegarria !
Maite dut zure begitartea, maite zure boz eztia,
halaber zure irri gozoa, zure izaite guzia ;
zuretzat baizik ez dut begirik, zare ene iguzkia,
gau eta egun zuri pentsatzen deramat ene bizia.

Martxoko hormak galduko duen lore pollit goiztiarra,
ta eihartzerat bortxatua den aizeak hautsi abarra :
ene bihotza ez ditaikea gauza horier konpara ?
Nehoiz enea ez da izanen maitatzen dutan izarra,
hortaz oroituz maiz etortzen zaut nigarra begietara.

Aitortzen dautzut, ene maitea, goiz aski banakiela
etzintaizkela enea izan, bertze batentzat zinela ;
bainan halere maitatu zaitut, ez bainezaken bertzela ;
ene bihotza, bizi naizeno, izanen zaizu fidela,
zuretzat bizi ez banitaike, zuretzat hil nadiela !