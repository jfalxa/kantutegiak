---
id: tx-3032
izenburua: Martxa Baten Lehen Notak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JDiPWkiBvTo
---

Eguzkiak urtzen du gohian
gailurretako euria
uharka da jausten ibarrera
geldigaitza den oldarra.

Gure baita datza eguzkia
iluna eta izotza
urratu dezakeen argia
utuko den bihotza.

Bihotza bezain bero zabalik
besoak eta eskuak
gorririk ikus dezagun egia
argiz beterik burua.

Bakoitzak urraturik berea
denon artean geurea
etengfabe gabiltza zabaltzen
gizatasunari bidea.

Inon ez inor menpekorokan
nor bere buruaren jabe
herri guztioak bat eginikan
ez gabiltza gerorik gabe.

Batek goserikan diraueino
ez gara gu asetuko
beste bat loturik deino
ez gara libre izango.