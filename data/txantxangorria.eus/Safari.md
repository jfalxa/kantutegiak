---
id: tx-371
izenburua: Safari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E3KFbsDd650
---

Begi lagunen ehizean
hiri hontan nabil
fragiltasunaren hatzak
hautsen euria solik
hitz itzaliak graffiti zahar batean
ahotsa komunetan
inauteri hutsa, manikien tribu
topaketak busetan
jai alai izan dadin... safari
oh... ehizean nabil.