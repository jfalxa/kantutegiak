---
id: tx-3405
izenburua: Mendiaren Deia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7OIXXVScSxY
---

Ibar zola itoetan gura nuen haize biziz,
errekalde ilunetan egarri nintzen goi argiz.
Gaua mutuen erresuman bihotza naukan garrasiz.
Entzun dut neure baitan mendiaren deia,
entzun dut neure baitan kantari mendia.
Ibili naiz mendiz mendi zoratuz, askatasunez.
Bete ditut beharriak goietako irrintzinez,
bete ditut bi begiak urruti zabal urdinez.
Goiko gainetan dut entzun musikarik ederrena,
goizaren irri alaia arratsean hasperena,
ta nitan sortan senditu herriaren maitasuna.
Goazen, mendigoizaleak ikusterat goiz argitan,
jaungoikoaren bakea gora ibilki airetan,
Euskal Herria estaliz arranoen hegaletan...
Entzun gaztea, mendiaren deia!!
Entzun gaztea, kantari mendia!!

Aire Ahizpak taldearen abesti bat. 2002. urtean kaleratutako "Ipar Euskal Herria Kantuz" bilduman topa daiteke kantu polit hau.

Amai Aire, ahotsa
Paxkalin Aire, ahotsa
Miren Aire, ahotsa

Aire Ahizpak hirukotea Fernando Aire "Xalbador" bertsolariaren bilobak dira. Hego Euskal Herrian horren ezagunak ez diren arren, «oso estimatuak dira Iparraldeko kantaren berri dutenen artean».

Aire Ahizpak hirukotea Hego Euskal Herrian horren ezagunak ez diren arren, «oso estimatuak dira Iparraldeko kantaren berri dutenen artean».