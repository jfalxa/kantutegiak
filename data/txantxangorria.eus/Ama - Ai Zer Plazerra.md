---
id: tx-1248
izenburua: Ama - Ai Zer Plazerra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ph8CpGCu3Zs
---

Ai zer plazerra, bost urten bürüan
sor lekilat itzültzia.
Ene herria etsitü nian,
zure berriz ikustia.
Aski sofrituz nik hanitxetan,
desir nükian hiltzia.
Ez bazüntüdan hain maite üken,
o Ama Eskual Herria!
Tralara...

2
Hanitxen gisa partitü nintzan,
etsarien güdükatzera.
Gogua bero, bihotza laxü,
eta kasik alagera.
Ez bainakian, orano ordian,
zer zen amaren beharra.
Hari pentsa eta biga bostetan,
aiher zitazün nigarra.
Tralara...

3
Erresiñula kantari eijer
libertatian denian
Bena trixtüran higatzen dua
kaiola baten barnian
Gü ere ama hala güntüzün,
tirano haien artian
Zure ametsa kontsolagarri,
 trixtüra handirenian.
Tralara...

4
Sortü berriak, bere kuñatik,
lehen elhia du "ama"
Bere ilusione ederrian,
ez dezazüla abandona.
Zure amodiuan egar itotzü
bizitze huntako penak
Begira dezan azken hatsetan,
azken hitza zure "AMA".
Tralara...