---
id: tx-1380
izenburua: Tabernako Argiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UQxXIkdPJms
---

Zazpi itsasoetako olatuak 
sentitu ditut txalupa zaharrian 
orain hemen gaude 
mozkorturik abesten
kantauriko kai txiki honetako txoko ilun batian.

karibeko gardenetan dzangan 
terranoan hazurrak dardarka
itsas bilak asko 
baina ez naiz iritsi
itsasoa ta zerua batzen diren marra hortara.

ez al dakizu arnasa falta zaidala
errekan gora noan bakoitzian
arrainen antzera itota
itsasotik apartatzian.

harrapatu gaituzte
tabernako argiak
marinel bakartion
bizimodu tristia
kamarero, atara eizu
azkeneko rondia
egunsentiarekin daukagu partitzia.

portuetako lamien lilura
marrazo gosetien hortzkada
lapurtutako bihotz zati txikiak
dauzkat itsas labar ilun eta ozeano azpian.

ez naiz txintxo, pekatu gabia
etxean nahi duten printzipia
inpernuko ateak zabaldu arte
itsasoari zor diot nere gorputz ta izatia.

ez al dakizu arnasa falta zaidala
errekan gora noan bakoitzian
arrainen antzera itota
itsasotik apartatzian.

harrapatu gaituzte
tabernako argiak
marinel bakartion
bizimodu tristia
kamarero, atara eizu
azkeneko rondia
egunsentiarekin daukagu partitzia.

harrapatu gaituzte
tabernako argiak
marinel bakartion
bizimodu tristia
kamarero, atara eizu
azkeneko rondia
egunsentiarekin daukagu partitzia.