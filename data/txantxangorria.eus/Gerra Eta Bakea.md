---
id: tx-36
izenburua: Gerra Eta Bakea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B_Sqen_wP2o
---

Modu analogikoan grabatutako bideoklipa, Imanol G.Gurrutxagaren eskutik. 
Zuzendaritza eta muntaia: Imanol G.Gurrutxaga
Argazki zuzendaritza: Ibai Mielgo Durtán
Ekoizpena: Nagore Muriel Letamendia

Musikariak:

Iker Lauroba: Perkusioa, baxua, ukelelea, mandolina, gitarra eta ahotsa
Nerea Alberdi: Biolina, biola eta sokazko moldaketa
Olaia Inziarte: Ahotsa

Hezi da pazientzia 
berehalakotasunean 
zuhaitza astintzen 
dugun aldiro ez dira
fruitu denak eroriko.

Ontzia zulatzen bada
gu bion babeslakuan,
ito nahi nuke zu/e hondoan
-ondoan-
urpeko bake iraunkorrean.

Itun bat sinatu dut
bakardadearekin
be/rak ni/ri ez dit e/man/go min
e/ta nik
es/kai/ni di/ot bi/zi/tza er/di.

Tristezia erosoa da
Tristezia erosoa da
Tristezia erosoa da
zoriona urrun denean.

Ni/re bu/ru/a/re/kin
su-/e/ten a/mai/ga/be/an.
Ban/de/ra zu/ri/a as/tin/tzen du gaur
hai/ze/ak,
a/mo/re e/man dut he/rra/ren gu/dan.

Zoriona erosoa da
Zoriona erosoa da
Zoriona erosoa da
tris/te/zi/a u/rrun de/ne/an.