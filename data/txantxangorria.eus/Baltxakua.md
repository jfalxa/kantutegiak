---
id: tx-1868
izenburua: Baltxakua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ok3hlchPplI
---

Nire Dultze morena

eskutik helduidazu

eta zure etxera

laguntzen utzidazu

BALTSEAN IRRI; SOROAN XIRRI;

DENA ILUN ZEN

MILA IZARRI; BEGIRA SARRI;

EZ DU ILUNTZEN

............NESKAK LAGUNTZEN

Urrun bizi zarenez

sentitzen naiz zorteko

denbora gehiago dut

zurekin egoteko

BALTSEAN IRRI; SOROAN XIRRI;

DENA ILUN ZEN

MILA IZARRI; BEGIRA SARRI;

EZ DU ILUNTZEN

............NESKAK LAGUNTZEN

Zure aitari esan

lagundu dizudala

baina nire eskuak

boltsikoan neuzkala!!!