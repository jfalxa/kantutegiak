---
id: tx-1719
izenburua: Frantses Euskaldun Bat Etorri Zait
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KUJlxOJUIaI
---

Frantzes euskaldun bat etorri zait 
ipiñitzeko bertsuak, 
deklaratuaz aren etxian 
gertatu diran kasuak; 
danak ez dira errexak baño 
esango ditut batzuak, 
argitasuna ematen badit 
gure Juan amorosuak. 

Nere barrengo sentimentuak 
nai ditut adierazi, 
zer estadutan gelditu naizen 
iñork nai badu ikasi; 
izarra baño ederragoko 
andria juan zait igasi, 
oraiñ bi urte aldegiñ ziran, 
geoztik ez det ikusi. 

Ogei ta amar urte daukazkit, 
oraindik franko gaztia, 
eta andriak bi gutxiago, 
au bizi-modu tristia! 
Ark neri egiñ dizkidan gauzak 
esatekuak eztia, 
txurrero batek engañatuta 
eraman dit emaztia. 

Biar bezela funzionatzen 
ez dago errex fabrika, 
lau urte pasa alkarrekiñ da 
ez degu izan aurrika; 
alegiñ egiñ-faltan ez daukat 
kontzientziyan zorrika, 
orañ Paris'en txurruak saltzen 
an omen dabill korrika?. 

Bi gona motz ta kamisa zar bat 
besterik ez det ikusi, 
ziran diruak arrapatuta 
aldegiñ ziran igesi; 
baita toxa bat ederra ere 
bere tabako ta guzi, 
jayian aita gonbidatzeko 
ez ninduen gaizki utzi! 

Alaxen ere desio dizut: 
izan zazu osasuna; 
non ote dago garai bateko 
gure aiskiretasuna? 
Etsai gaxtuak au da burura 
zuri bialdu dizuna, 
orañ andriak bi toxa eta 
bat ere gabe gizona. 

Txurrerua're lan senzillua 
da asko irabazteko, 
orrek biñipiñ ez du egiten 
mantendu eta jazteko; 
andriarekiñ alkar artuta 
ni modu ontan uzteko, 
bi kristau gaxto naikuak dira 
baztar guztiyak nasteko. 

Andria iges juan zanian 
arren anaiak esana: 
«Nere arreba dek, Patxi, baña 
etziak trapu zuzena». 
Ni naiz orduan ajustadore 
Baiona'n ari nitzana, 
zuk orañ dezun txurreru ori 
ez bezelako gizona. 

Ni emaztiak utzi ninduan 
estadu triste batian, 
beste batekiñ aldegiñ ziran 
anbat beretzat kaltian; 
txurruak janaz ederki dabill 
diruak aitu artian, 
zer esan biar ote dit neri 
Josafatara juatian? 

Gauzik onenak eta diruak 
arrapatuta juan zian, 
nuen trajerik onena ere 
ostu ta eraman zian; 
gosiak zeon txurreru ori 
jarri da taju-antzian, 
orañ Paris'en bizi omen da 
trapero baten etxian. 

Ez det milagro edukitzia 
malkoz beteta muxuak, 
kabi ta guzi iges egiñ dit 
tamañ ortako uxuak; 
nik berriz iñon ikusten badet 
adituko du goxuak: 
Non ote dira garai bateko 
gure izketa goxuak? 

Txurreru ori zer gizona dan 
sinistu entzun deguna: 
lana egiteko gogua falta 
eta mingaña leguna; 
bedeinkatua izan dedilla 
aldegiñ zuten eguna, 
gizarajuak arrapatu du 
bere moduko laguna. 

Jose Manuel Lujanbio, "Txirrita" (1860-1936)