---
id: tx-3058
izenburua: Gau Erdian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nDUEVtsd7Vg
---

Gau erdian, aingeruak
kantari zeruan
gaur gizonentzat (gizakientzat) eguberri
Elkar maite dezaten beti
Baina isildu gaitezen
Haurra lotara dadin.

Gau isila, gau zuria,
kalean euria
eta etxean ospatzen dira
zorian gabon afariak,
Gaur eta jaio zaiguna
Belenen haur txikia.

Gau erdian, aingeruak
kantari zeruan
gaur gizonentzat (gizakientzat) eguberri
Elkar maite dezaten beti,
Baina isildu gaitezen
Haurra lotara dadin.