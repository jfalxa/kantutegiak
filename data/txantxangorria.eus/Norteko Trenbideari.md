---
id: tx-2846
izenburua: Norteko Trenbideari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_XMfwSumaNE
---

Mila zortzirehun hirurogei ta zazpigarrengo urtean,
lehenengo trena ikusi nuen Espainiako partean,
bide pixka bat bistan pasata inoiz sartua lurpean;
perra berriak erantsi dizte handikan orain artean.
 
Modu horretan jarri zuenak Norteko ferrokarrila,
jakinduria haundia zuen, entendimentu abila;
gizonik ezin hasi liteke zaldi hobearen bila:
goizean irten Irundik eta iluntzerako Madrila.
 
Danak alkarri lagun zaiogun ilundu gabe kopetik,
indar haundian dihoala 're gelditutzen da kolpetik;
inor ezertaz behartzen bada Donostiara Madritik,
izugarrizko adelantua jarria dago Nortetik.
 
Linea horrek badauka zenbait tunel, zubi ta errio,
bi biderekin ipini dute sekulan baino berri“,
eletrikako indarra eta hoinbeste guardarraio;
gau eta egun gelditu gabe beti zerbaiten karraio.
 
Trenera lasai balihoake eginagatik zahartzera,
igo-bideak ederrak dauzka, kotxean aixa sartze'a;
hamar legoa bide pasata ordu beteko etxera,
lehengo segunda bezin ederra dago oraingo terzera.
 
Eseri-alki ederrak eta beste gauza bat on-ona:
ezertarako behartzen bada kotxe bakoitzak komona;
lehengoan hantxen ikusi nuen larogei urteko amona,
frantzes euskaldun buru-zuri bat, Bentaberriko Moñoña.
 
Karbonerarik ez dute behar, fogonerorik hain gutxi;
hor behar dana: begiak ernai eta llabiari eutsi;
abiatzean iriki eta ailegatutzean itxi,
gau eta egun gelditu gabe dabil txistu ta irrintzi.
 
Gizon abila zan eletrika sortu zuen maisu hori,
piska-piska bat harrek graduak bajatu zizkan suari;
orain indarra leku askotan kendutzen zaio urari,
garai batean merke zan baino gaur balio du ugari.
 
Honla esanaz ur irakinak egiten zuen negarra:
«Prezisua al da beti kalderan kiskaltzen egon beharra?
Ez daukat beste alimenturik: sua, kea ta txingarra;
ongi miatzen baldin banazu hotzean badet indarra».
 
Berari be'ira jarri zitzaion zabaldurikan besuak,
arrazoi hori ontzat hartu du eletrikako maisuak:
«Badakit zure gau ta egunak dirala lastimosuak,
hori egia esan badezu itzaliko di'a suak».
 
Nere munduko jakinduria izan da oso azala,
Jangoikoari eskatzen diot arren argitu nazala;
garai batean ez genekien eletrikarik bazala,
orain ur hotzak dabilki trena lehen irakinak bezela.