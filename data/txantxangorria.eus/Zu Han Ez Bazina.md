---
id: tx-771
izenburua: Zu Han Ez Bazina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/41YJVCzhTYw
---

Eguberritan zaigu urtea zahartu
Menditik da elurra kalera ausartu
Zuk ere behar zenuke guganaino hartu
Kartzela ospel hartan zintuztena sartu

Tiriki tauki taki
Mailuaren hotsa
Horma guztien kontra
Mailuka bihotza

Itxitako urteak adarra du luze
Aspaldi frogatua zaude urtez urte
Lekutara gugandik urrundu zaituzte
Zuganako bidea tapatzeko elurte

Tiriki tauki taki
Mailuka bihotza
Zu handik gu hemendik
Aho biko trontza

Urteak esaten du bizion adina
Kartzelak nor den bere herriaren dina
Libre nahi dugula da gauza jakina
Nola libre izango zu han ez bazina

Tiriki tauki taki
Bihotzaren hotsa
Geroak urtuko du
Oraingo izotza