---
id: tx-557
izenburua: Herri Urrats 2019
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A_th0kzRY4c
---

Guk sehaskak eraikitzen ditugu ilusio egurrez
Goxo-goxo ziz sailkatu otsiko diren beldurrek.
Ez ditugu apaintzen harri bitxiz, zilarrez eta urrez...
Baina denak betetzen dira hezur ta haragizko haurrez!!

Herri urrats, herri urrats! 50 urte arnas
Herri urrats, sehaskatik hegan goaz!!!

Guk klaseak eraikitzen ditugu oztopo enormekin,
Uholde eta sute denen ondotik lehio berriak egin.
Zurrunbilo artean bizitzeak egin gaitu desberdin
Ta hegoak zabalduko ditugu euskal herriarekin!

Herri urrats, herri urrats! 50 urte arnas
Herri urrats, sehaskatik hegan goaz!!!
Herri urrats, herri urrats! 50 urte arnas
Herri urrats, sehaskatik hegan goaz!!!

Bostale txiki eraiki ziren arrangoizen!
Beleka ere kaioak gara handitzen!!!
Baiona soro ta zriburuz aritzen, laiztabarre ezkoiten!!
Eta segi nahi dugu urratsak egiten...

Herri urrats, herri urrats! 50 urte arnas
Herri urrats, sehaskatik hegan goaz!!!
Herri urrats, herri urrats! 50 urte arnas
Herri urrats, sehaskatik hegan goaz!!!

Herri urrats, herri urrats!
Herri urrats,!!!