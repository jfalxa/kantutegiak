---
id: tx-1761
izenburua: Lore Gorrien Balada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cezya9uc4uc
---

Zelai ertzeko lore gorriak
 neguak zapuztu ditu.
 Neskatxaren ezpain eriak
 zeiñek laztanduko ditu?
 Gure herriko baratzak dira
 lorerik gabe gelditu.
 
 Udaberriko lili garbia,
 gure lurrean eldua,
 gaueko amets bat bezela
 egunsentian galdua,
 gizon dirudun baten etxean
 ain gazterik usteldua.
 
 Loreak zeiñek jarri zituen
 elizako aldaretan,
 nagusiak ekarritako
 zillarrezko ontzietan?
 Orain larrosik ez dut ikusten
 Euskalerriko kaletan.