---
id: tx-2410
izenburua: Burua Ezin Jasota
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m1YHIgOZO2s
---

Ondo da, ondo da, utz ditzagun horrela. 
Gaur betiko legez eztabaida bera. 
Ez al zara konturatzen alkarrekin gaudela 
burua ezin jasota nabilela aspaldian? 
Zure begi horiek antzerako zerbait diote 
ahotik datozkizun hitzek 
beste kontu batzuk darabila bitartean 
Zure asmakizun hutsak dira ez zaitez engaina.
Ibai agor honek jada ez darama urik 
Noraezean lur lehor honen bidetik gabiltza, 
eta ez dut ikusten irtenbiderik. 
Ziurtasun honen jabe ere nago bakarrik. 
Eta ez gara bakarrik egoteko jaio. 
Ez al dugu denok lagunaren desio? 
Baina gure honek zer du balio 
gezurraren lokatzetan baldin bagara ito? 
Aitortzen dizut gaur hamentxe bertan 
poztuko nintzatekeela esango bazenit. 
Konturatu zarela elkarrekin gaudela. 
Burua ezin jasota zebiltzala aspaldian.