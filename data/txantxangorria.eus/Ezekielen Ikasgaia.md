---
id: tx-1851
izenburua: Ezekielen Ikasgaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Nmgv05fIbGE
---

Saiatu nintzen zerumuga eho bat
zipristin gabea eskeintzen
saiatu!
saiatu naiz itxurosoki
barrabasadak estaliz joaten
azpitik
pasapuretu dizkiat bideak
zuretzat.


Badakit zaunkak urruntasunean
iraunen dutena beti
palastak bailira
irmoki;
ezin duzu itsasoan sagarrik erein
duesereezaren hilzori pobrean
bakarrik
ezin
zamalka
haize kopaletik,



hik,
alferrik;
Hire ttipitasun hortan
baretu ditzakek soslai itsuak
hegalak
baina ura nahi badun draia
hobe duzu merengeetan gastatu
altxor
altxor
ene altxor.