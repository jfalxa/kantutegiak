---
id: tx-2590
izenburua: Udazkeneko Fruituak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AQvNK26m28A
---

Jan ditugu sagarrak,
mahatsak, gaztainak
Jan ditugu sagarrak,
mahatsak, gaz/tainak
udazkenko frui/tuak
udazkeneko fruituak.
Oia, oia, oia, oh!
Oia, oia, oia, oh!
Oia, oia, oia, oh!
Oia!