---
id: tx-3366
izenburua: Artzaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y88qidye--U
---

Artzaiak han dabiltza
mendi goienean
egunero ez dakit
baina gehienean
ardi bat taldeakin 
ez dabilenean
bila ertetzen dira
galtzen zaienean

ardi bat galdu bada
azkar haren bila
bordara bildu nahi du
bere ardi pila
txistu eta oihua
txakur da makila
bizirik baldin bada
azaldu dedila

Artzaiak beti dauzka
ardiak buruan
nola hilbeltzean da
ala azaruan
lehenengo ardik jarri
leku seguruan
da gero bera etzan
ardin inguruan

ardi bat joaten bada
taldetik igesi
eta bere artzaiak
ez badu ikusi
konturatzen danean
bere bila hasi
hura bilatu arte 
ez da lasai bizi

atzaiarentzat txarrak
dijuaz urtiak
uholde edo txingor
eta elurtiak
otsoak zai dauzkate
baita ere putriak
bldurra ematen du
artzai izatiak

artzai bakarrak dauzka
ardiak ugari
batentzako gehiegi
direla dirudi
guk ala uste arren
gezurra da hori
izenez deitzen dio
ardi bakoitzari
guk ala uste arren 
gezurra da hori
izenez deitzen dio
ardi bakoitzari