---
id: tx-2931
izenburua: Eskutik Helduta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ESnKggoCva8
---

Eskutik helduta gaude,
borobil bat eginez,
arras pozik gabiltza,
itzul anitz emanez.
Eskuina, ezkerra, urrats bat aintzinka;
eskuina, ezkerra, urrats bat gibelka;
eskuina, ezkerra, urrats bat aintzinka;
eskuina, ezkerra, ipurdiz lurrera.