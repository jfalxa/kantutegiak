---
id: tx-2125
izenburua: 25 Urte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A45G_Sdxxbk
---

Euskal Girotze Barnetegien 25. urteurrenaren ospakizunerako Alkizako begiralea den Imanol Lizardik eginiko bertsoa.