---
id: tx-1654
izenburua: Aitzakia Urtz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6u3w2MQwPzc
---

Zelatan egun latzak igaro ondoren
sarritan galdetzen dut
ea negua datorren
Goizegi da? Berandu da!
Bizitzarekin dantzan itzalen artean
zu nigan eta ni zugan,
maitaleen antzera;
Goizegi da? Berandu da!
Hiri gris honetatik bagindoazela
bart amets egin nuen gauero bezela
baina honetan ere
ekaizaren garrasiak
itzuli nau zuregana.
Azken eguna bihotzean,
ez diren gauzak oroitzean;
tristurak ez du oinazerik
gero arte batez agurtzean.
Goizegi da? Berandu da!
Gau ilunaren babesean,
ilargipeko aitorpenak;
beldurrik gabe esaidazu
ez diren gauzak badirela.
Goizegi da? Berandu da!
Goizegi da? Aitzakia bat!