---
id: tx-721
izenburua: Dantza Gaua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M6lMRZURSbs
---

Ideia eta produkzioa: RuralKids
Grabazioa: Dronetikan AirWorks
Musika: Dupla
Nahasketa eta Masterra: Eñaut Gaztainaga


Letra: 

Dan Dan Dan Dan Dantzagaua
Euskal jaien egitarauan

Hautsi dira tabu kulturalak
Dantzarekiko arazo moralak
Bat-bateko galernan
hasi da perreoa herriko tabernan

Folklorearen hazia
Arbasoen ravetan hasia
Gaueko ordu txikitan
Perreoa blusaz jantzita

Gasteiza irisi da
Bilbora irisi da
Sakanara iritsi da
Durangora iritsi da (x2)

Ailatu  da gaua itzali argik
Askoz hobeto gaude ilunpetan
Beida ze nahi deun egia esan ezer berrik ez
Ezteu gelditu nahi ta muitze eztan gerrik ez
Fuera tabu danak dantzatokitatik
Negua ta uda ta gu denak probatzen bitatik
Folklore ez da arin-arin ta fandango bakarrik
Hau ez da izango folklore bakarrik
Eman egurra, gure gozamena bere beldurra
Gozatu puritano daukezun denbora apurra
astindu hezurrak,
Ezautze deu honen alde makurra
Zerria beti zerri beti bilatzen ezkurra
Esangoizut egia, botako deu hesia
Itaia beti prest eta zorroztuta geziak
Batzokiko moralaren itzala gure harria
Ipurdik muitze ezpau eztata 
Nere Euskal Herria

Baionara iritsi da
Trebiñura iritsi da
Tuterara iritsi da
Tolosara iritsi da