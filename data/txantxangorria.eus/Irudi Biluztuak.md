---
id: tx-714
izenburua: Irudi Biluztuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gDMpMBELXOg
---

Zergatik galdu ziren erantzunak?
Nora egin zuten ihes hitz goxoek?
Noiz irentsi gintuen zalantza ezinen olatuak?
Inoiz ikusi ditut ihesean irudi biluztuak jolasean,
Sinetsi eman nizun muxu bakoitza benetan zela.
Denbora azkar pasa da, biok aldatu gara,
Non sartu zara?
Ze leku gelditu zen bilatzeko
Errudunik ez zela ikusteko
Nola eman genion garrantzi gehiegi ez zeukanari.
Zutaz oroitzen naizen galdetzen didazu begirada batez,
Babesten gaituen muga zeharkatzen.
Beti itxarongo zaitut azken geltokiko iluntasunean,
Amets ezkutuen isiltasunean.
Zergatik galdu ziren erantzunak
Nora egin zuten ihes...
Noiz irentsi gintuen zalantza ezinen olatuak.
Denbora azkar pasa da, ta ziur, biok aldatu gara,
Non sartu zara?
Zutaz oroitzen naizen galdetzen didazu begirada batez,
Babesten gaituen muga zeharkatzen.
Ezin aurkitu hitzik gauean galtzeko, agur esateko.
Zutaz oroitzen naizen galdetzen didazu begirada batez,
Babesten gaituen muga zeharkatzen.
Beti itxarongo zaitut azken geltokiko iluntasunean,
Amets ezkutuen isiltasunean.
Isiltasunean.