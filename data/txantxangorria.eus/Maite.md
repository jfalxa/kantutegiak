---
id: tx-874
izenburua: Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W9xcluIQAHU
---

Gabak zabaltzen ditun
Izarren bidetan,
Zure irudi maitea
Dator amesetan.
 
Egunak galtzen ditu
Izar eta amesak;
Ala ondatzen nau ni
Zu nerekin ezak.
 
Maite! Eguzki eder,
Eguardi beteko argia!
Neke-miñen artean
Nere zorion-iturria.
 
Noizbait illuntzen ba'da
Nigan zure irudi eztia,
Kontu egizu, maite,
Galdu dedala nik bizia.
(bis)
 
Maite !... Maite !!... Maite !!!…