---
id: tx-3402
izenburua: Ezagutzen Zaitut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4BvnmEldb8Y
---

Bideoa: Joxepa behiarena

Bidarraiko bazter hartan emakume garela erabaki genuen, eta ez dakigula zerk egiten gaituen emakume. Baina baietz, hilerokoa gure emakumetasunaren parte dela.

Geuretik idatzi eta kantatua. Geuretzat, eta beraz, zuontzat.



Egun denek usain bera
ez dute izaten gugan
gorputz ezjakin honekin abenturan...
Modu salatarian,kuleroko txurian,
ilusioz ta beldurrez ezagutu zintudan...

Emakume naiz zurekin
gustorik gabeko muxu goxoekin
hatzamarren dantzan gozamenarekin...
Gorriz, gorriz zikindutako ezpainekin.

Makillaje azpiko grano denekin
igande goizetako ajearekin
arrazoi gabeko malko alaiekin...
Gorriz, gorriz zikindutako ezpainekin.

Mina daukat zurekin, ta mina zu gabe...
Zergatik zara nire irrifarraren jabe?
Hilean behin agertuz argi sentitzeko,
nola naizen nire gorputzaren menpeko.

Nahiz eta asko eman diogun elkarri
zergatik dugu mina bizitzaren oinarri?
Egun denek usain bera
ez dute izaten gugan
gorputz ezjakin honekin
abenturan...

Modu salatarian,kuleroko txurian,
ilusioz ta beldurrez ezagutu zintudan...

Emakume naiz zurekin
gustorik gabeko muxu goxoekin
hatzamarren dantzan gozamenarekin...
Gorriz, gorriz zikindutako ezpainekin.

Makillaje azpiko grano denekin
igande goizetako ajearekin
arrazoi gabeko malko alaiekin...
Gorriz, gorriz zikindutako ezpainekin.

Mina daukat zurekin, ta mina zu gabe...
Zergatik zara nire irrifarraren jabe?
Hilean behin agertuz argi sentitzeko,

nola naizen nire gorputzaren menpeko.

Nahiz eta asko eman diogun elkarri
zergatik dugu mina bizitzaren oinarri?

Mina daukat zurekin ta mina zu gabe,
zergatik zara nire irrifarraren jabe?
Hilean behin agertuz argi sentitzeko,
nola naizen nire gorputzaren menpeko.
Denbora pixkanaka aurrera joan ahala
ikasi dut nirekin behar zaitudala.