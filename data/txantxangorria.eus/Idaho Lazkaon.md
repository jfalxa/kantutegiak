---
id: tx-893
izenburua: Idaho Lazkaon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pum2M5zGFvQ
---

Behin batez laztan hori 
gertau omen zen 
nahiz eta nola inork jakin ez
zuk ere zihur ezetz
ez dakizula kontauko dizut
nola izan zan Amerika batzuk
pistola ta guzti agertu ziren
herri hontan bufalo zahar batekin
tiroak botaz, ta Macaihanen
laurak bai dira.

Baina gu Lazkaon ez dira berdin
hemen ez dago bufalorik
gehienez sardina atsa, ez al duk gazta
janez gero jakingo du zein den ona.

Gatza da zahartuta arritu antzean esan ziguten
ez zen txarra, nola egitendek hau?
erreza al da? Bufalatu zidan goazemak artzai
geroztik hor gabiltz Aralar aldean
Idahoko gazta egin nahi ez, behi salgo gaza
hobetu ordez sanwitxerako gaza asmatu zuten.

Idaho Lazkaon ez dira berdin
Baina gu Lazkaon ez dira berdin
hemen ez dago bufalorik
gehienez sardina atsa, ez al duk gazta
janez gero jakingo du zein den ona.