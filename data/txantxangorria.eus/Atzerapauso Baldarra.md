---
id: tx-251
izenburua: Atzerapauso Baldarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IG7xPwiEruk
---

HITZAK: 
Ikatza bezain beltza den ardi beltz bat
Ikusten dut ispiluan
Txakur bortitzenaren zaunken oihartzunaren 
Uhinak nire buruan
Korapiloa eztarrian, lokatza nere irrian
eta mila gezur eskuan
Jada ez dakit zenbagarren azken barne gudaren
hildakoak inguruan

Atzerapauso baldarrak 
Eta erabaki koldarrak
Atzean utzi asmoz
Besarkatu, barren sakonak askatu
Eta esan ezazu berriz
Jaio nintzela atzo