---
id: tx-3355
izenburua: Olagarroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dWoVcAxjljA
---

Olagarroa, olagarroa
bai ibilera arraroa
Olagarroa, olagarroa
hanken gainean du ahoa

Harkaitz azpian du etxea
oso dotorea
Otordua iritsi
ta irten da
Karrakela dator hona
ai zer zoriona
Alga egosiekin
hamaiketako ona

Olagarroa, olagarroa
bai ibilera arraroa
Olagarroa, olagarroa
hanken gainean du ahoa

Harrapatu zortzi hankekin
eta hozka egin
Karrakela dabil
gira bira
Negar zotinka esan diot
"Mesedez, ez ni jan!"
Bat-batean hasi dira
biak pozik dantzan

Olagarroa, olagarroa
bai ibilera arraroa
Olagarroa, olagarroa
hanken gainean du ahoa