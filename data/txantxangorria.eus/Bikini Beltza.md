---
id: tx-1849
izenburua: Bikini Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZelhStCAgLw
---

Josu Bergara kantautorearen "Bikini beltza" abestiaren bideoklipa Iñigo Sbeltzak errealizatua.
Videoclip del cantautor Josu Bergara realizado por Iñigo Sbeltza
Josu Bergara basque singer songwriter´s new video "Bikini Beltza" by Iñigo Sbeltza

Diskoa: "Katiuska gorriak" (2012)
Abestia: "Bikini beltza"
Zuzendaria: Iñigo Sbeltza 


Kontratazioa:

BIKINI BELTZA / Josu Bergara - Katiuska gorriak

Hankaz gora munduari begira,
paperezko sobre baten barruan gordeta,
zure azken mezua ailegatu da.

Balkoian etzanda eguzkipean,
larrosa koloreko geranioen babesean,
bikini beltza eta begietan disdira.

Nora, Bilbora itzuli zara,
eskutik heldu, heldu, heldu zoriona.
Nora, etxean zaude jada,
mila ametsetan eskatu, eskatu genuena.

Irratian "Please don´t go" entzuten da,
"Azken guda dantza" posterraren azpian,
zu ta biok ohe barruan.

Taxi guztietan galdera berbera,
kristaletan ikasi zenuen erantzuna,
altzairuzko hiri honen arima gurea da