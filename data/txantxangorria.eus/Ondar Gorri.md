---
id: tx-578
izenburua: Ondar Gorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HLk1bwEhCDE
---

Berriro igo nauzu ene mendira,
oroigarri zaarrei maitez begira…
Andre Lurrak yaulki ditu igaliak;
zurbil dauka arpegia, itzal begiak.

Gurdibide bakan-ibil ertzetan
untza dago oraindik, nagi. Loretan.
Or-emenka, lore zimel gañetan,
ingumak ari, gozo-miazketan…
Mitxeleta gorri, zarpil egoak:
maitatzeka atsotu diranetakoak.

Kemenak uts-eta nekez bainoa,
zalantza dut zaartu naizelakoa…
Udazkenak aulagotzen dit atsa,
orbelak ozenagotzen oin-otsa,
aldapak larriagotzen biotza…
Leenetan ez bezin autsia natza.

Giroen argia galtzerakoan
leen-oiartzuna dut ozen gogoan.
Iru giro izanok, nik dei ta atozte,
nork-zeren saria ekardazute:
batak itxaro, besteak berbizte;
irugarrenak bizitza-indar bete.
Oro bear zaituzte ene biotzak,
leen-min baidamada neurtitz zaarren otsak!