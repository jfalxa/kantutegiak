---
id: tx-1166
izenburua: Hakio, Hakio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1qQgOn9KvTU
---

Hakio, hakio
Miren gogotik,
ateraz, ateraz
zapata orpotik,
txin, txin demoniena,
eskuina behar den aingeruena.

Miren, buru hori
gogorra daukan!
ikasi al ditun
aingeruen kantak?
txin, txin demoniena,
eskerra behar den aingeruena.

Hakio, hakio
Txomin gogotik,
ateraz, ateraz
zapata orpotik,
txin, txin demoniena,
eskuina behar den aingeruena.

Txomin, buru hori
gogorra daukan!
ikasi al ditun
aingeruen kantak?
txin, txin demoniena,
eskerra behar den aingeruena.

Hakio, hakio
Laura gogotik,
ateraz, ateraz
zapata orpotik,
txin, txin demoniena,
eskuina behar den aingeruena.

Laura, buru hori
gogorra daukan!
ikasi al ditun
aingeruen kantak?
txin, txin demoniena,
eskerra behar den aingeruena.