---
id: tx-917
izenburua: Hitz Ederrena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DSQCJv7zs78
---

Abesti hau zuretzat
olerki bat izango balitz bezala.
Esprai kolpez hormetan
idazten diren olerkiak bezala.

Abesti hau zuretzat
txupetatik zintzilik
egunero agertzen zarelako, zarelako,
deitzen nauzulako
eta oraindik
ezezagunak garelako, garelako.

Zu gabe bizitzak
baliorik ez duelako.
Oihuka daudenak
zuri deika ari direlako.

Hitz ederrena, bai, bai, bai,
Askatasuna!!
Hitz ederrena, bai, bai, bai,
Askatasuna!!

Zu gabe bizitzak
baliorik ez duelako.
Oihuka daudenak
zuri deika ari direlako.

Zugatik bizitza
askok eman dutelako
ta ni hemen
zugatik abesten nagoelako.
Zugatik bizitza
askok eman dutelako
ta ni hemen
zugatik abesten nagoelako.

Abesti hau zuretzat
maite dudan pertsonei
ematen diedan musu
musu bat bezala.
Gorrotatzen ditudan etsaiek
nigandik urrundu
nahi dizkizutelako, dizkizutelako.

Hitz ederrena, bai, bai, bai,
Askatasuna!!
Hitz ederrena, bai, bai, bai,
Askatasuna!!

Zure beherra ez dutenek
"komo Dios" bizi direlako
eta kateak denborarekin
ugartzen direlako.
Zure beherra ez dutenek
"komo Dios" bizi direlako
eta kateak denborarekin
ugartzen direlako.