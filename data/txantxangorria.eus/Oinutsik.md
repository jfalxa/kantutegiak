---
id: tx-1266
izenburua: Oinutsik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NW56KsF0-GE
---

Musika, kitarra, ahotsa: Rolan Garcés 
Letra: Ainhoa Garaikoetxea 
Euskara moldaketa: Leire Markina 
Perkusioa: Juan Prima de Riesgo 
Nahasketa eta mastering: Dani Ulecia 
Grabatua Color Hits estudioan
paroles
Oinutsik zoaz ihesean 
Lagunik gabeko mundura 
Harriz betetako hatuetan 
josi ez duzun urradura. 
Dardaraz pasatzen da 
nork besarkatuko esperoan 
sugarrez beteriko gauak 
ezezagun baten alboan. 
Galdu egin al zara neska? 
Non ote duzu burua? 
Nork eskainiko dizu berriz 
erreginen gaztelua? 
Zeren bila zabiltza 
Guztiz galdurik zentzua 
Ahastu itzazu amets eroak 
ta ordainik gabeko sexua. 
Inoiz baino beteagoa 
ikusten zaizu kanpotik 
hortz beterik duzu ahoa 
barruan lastoz osorik. 
Galdu egin al zara neska? 
non ote duzu burua? 
Nork eskainiko dizu berriz 
erreginen gaztelua?. 
Ez al zara ohartuko 
bizitzak duen zentzua 
ez da errotak akabatzea 
zaldun ero koitadua. 
Galdu egin al zara neska? 
non ote duzu burua? 
Nork eskainiko dizu berriz 
erreginen gaztelua?

Rolan Garcesek bakarlari bezala egindako lehenengo diska da hau, grabaketa 2017. urtearen bukaeran, Color Hits-en, Buenas Compañías eta Ag8st8 Estudiotan egina dago.
Rolan, rock&roll-ean beti murgildu izan da non Iruñeko zeinbat taldeetan partaide bezala aritu izan da.
Diska honetan aldiz, folk, country eta blues estiloetan ere entzun ahal zaio.
Melodietan ahotsari batez ere eman nahi zaio garrantzia, zeintzuk indarra daukate instrumentazio xumeak erabiliz.
Honetarako, Rolanek abestien hizkietan zein konponketa batzuetan kolaborazioak izan ditu. Euskarazko abestietan, zenbait lagunek egindako letrak eta esandako konponketetan, esatekoa da, Oskar Benasek adierazitako hebetasuna, non produkzioaz gain, instrumentazioaren zatiren bat ere grabatu izan du.
Datorren Irailean diskan argitaratuko diren 9 abestiak, inguruko zein norberaren askatasunaz, gure barneko borroketaz, eta izan garena izateko zer ikasi dugunaz hitz egiten du.