---
id: tx-2018
izenburua: Alaken
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BMGBi4RO-OE
---

Alaken, alaken, 
ala pikula tun paire, 
errespunde la pikula tunpa 
Jesus Maria Jainkoa zen. (bis)

Adizkideok! 
Ikusi duzue zelai horietan artzaia, 
artzai txiki, 
artzai handi, 
artzai mutur leuna. 

Alaken, alaken... 

Lo leri lelo, 
goxo dek goxo, 
neguan etxe txokoa. (bis) 

Gaur jaio da, gaur, 
Jesus Haurtxoa. 

Lo leri lelo, 
hiru errege 
izar baten arrastoan. 
Lo leri lelo, 
tranka, trankatran, 
hirurak zaldiz badoaz.

Alaken, alaken...