---
id: tx-1911
izenburua: Koktel Molotov
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d-rhdndvAlI
---

Torturatzaileak 
dagoeneko kalean dira, 
gorriak ala beltzak 
denak berdin dira, 
torturatzaileak 
hasi dira korrikan, 
euskal jendeari 
izutzeko asmotan, 
terrorismoa 
beraiek sortzen dute, 
alde zaharreko 
kale guztietatik, 
ixkin baten ondoan 
dago zauritu bat 
egoera larriko 
momentu batean 
kezko boteak jaurti 
dituzte aurrean 
minutuak pasatuz 
doaz abiadan 
benetako tiro bat 
bota dute airean 
lagunari laguntzen 
nengoen momentuan 
bat-batean abiadan 
joan naiz ziztu bizian 
koktel bat jaurtiaz 
gorantz aurrean 
argi eta garbi 
eduki ideia 
borroka egiterako 
orduan. 

Koktel molotoff 
zalantzarik gabe 
gogor etsaiari. 

Erdarazko hitzak 
soberan geratzen dira 
euskara baitugu 
gure hizkuntza polita 
abestu, abestu 
eta ez zaigu inoiz ahaztu 
gure hizkuntza bakarra 
behar dugula indartu 
ertzaina bateri 
euskaraz galdetu 
berak esango dizu 
ez dizula ulertu 
euskaraz hitz egiten 
egoteagatik 
komisaldegian 
nago atxiloturik 
ez dago inongo 
eskubiderik 
Euskal Herria 
egoteko torturaturik 
explosio galanta 
daukat pentsamenduan 
neure hitzekin 
sortzen dena momentuan 
oparoan, luzaroan, 
bizi guztian gaude 
etsi gabe borrokan 
ideiak defendatu 
defendatu 
zure ingurugiroan. 

Koktel molotoff 
zalantzarik gabe 
gogor etsaiari. 

Penagarria da 
kartzeletan ikustea 
nola euskal presoak 
galtzen duten bizitza 
ura eta ogia dute 
jana bakarra 
hogeita lau orduak 
pasatuz segidan 
egunaren argia 
ez dute ezagutzen 
infernu batean 
daudelako sofritzen 
guzti honen aurka 
aurkitzen naiz ni ere 
etxean nahi ditugu 
jaio eta hil arte 
ikurrina eskutan 
gogor ondo helduta 
helburu bakar batekin 
noa zuzen borrokan 
borrokarik gabe 
ez dago garaipenik 
ez dugu inoiz 
konbate hau galdurik 
ziztrin halakoa 
hau ezta festa giroa 
mikrofonoa eskutan 
beti nago armatua 
neure hitzak dira 
hala formakoak 
estrategia landuaz 
argi dut helburua. 

Koktel molotoff 
zalantzarik gabe 
gogor etsaiari. 

koktel molotoff 
neure hitzekin 
nago sortzen ni