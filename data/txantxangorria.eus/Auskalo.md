---
id: tx-3398
izenburua: Auskalo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qJGkBPurt6c
---

Patxi Zabaletak "Ezten Gorriak " liburuan Gorka Trintxerpe ezizenez argitaratu zuen olerkia, 1976an musikatu egin genuen Xabier Etxeandia eta biok.