---
id: tx-404
izenburua: Nagusia Ta Maizterra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YStX9-tHGg8
---

Orain artean ni izandu naiz,
aitona, zure nagusi,
errenta txintxo pagatu dezu
zere kapoi eta guzi;
zu baiño gizon umillaguak
ez ditut asko ikusi,
orain etxea saltzera goaz
nai bazenduke erosi.

Larogei duro zuen errenta
etxeak orain artian,
zedorrek ala pagatu dezu
berrogeitamar urtian;
lengoan bertan gelditutzia
ez da neretzat kaltian,
bost milla duro balio ditu
ea konpondu gaitian.

Zer familia izandu degun
jauna nai al du aditu?
Amabi aurren aita naiz ni ta
semiak amar baditu,
lan egiteko iñor gutxi ta
maiean ezin kabitu,
baneukake non enpleatua
bost milla duro banitu.

Au aditu ta beste aldera
bueltatu zen nagusiya,
esanaz “nik zer kulpa dadukat
zuk ume asko aziya;
ez dezu asko ixtimatzen nik
egin dizudan graziya,
zuek artu nai ez badezute
bada zeiñek erosiya”.

Eskerrikasko, nagusi jauna,
pakian biaugu bizi,
baiña izketan bukatu arte
ez bedi joan igesi;
klaro dabillen gizonak ez du
arrazoi txarrik merezi,
nai duenari saldu bezaio,
guk ezkentzake erosi.

Aziendaz ta lan-erremintaz
bagenedukan indarra,
aiekin ere ez degu orain
oso alderdi ederra...
zazpi balio luteken gauzak
bostean eman bearra,
au pentsatzian etortzen zaigu
begietara negarra.

Amabi lagun maiean bueltan
juntatzen gera jateko,
geienaz ere aietatik bost
irabazira joateko;
pazientziya artuko banu
anbat ez nuek kalteko,
Jaungoikuari eska zaiogun
osasuna emateko.

Prezisamente martxatu biaugu
baserri edo kalera,
norbaiti egin bearko zaio
bizilekuen galdera
esplikatuaz nola izan den
fameliyaren galera;
gure Jainkuak nai duen arte
nonbait mantenduko al gera!

Txirrita