---
id: tx-210
izenburua: Bi Mundu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qzf8QzA624s
---

Mundua 
itsasontzi batean 
kulunka daiteke 
begirada bat maitasunaren seinale
lurrari agur esan gabe,
gero arte.

Aurrera
olatuetan marrazten da
zerumuga,
itsaso zaharrean
berria da ura
berriz itzultzeko
hasierako portura.

Euskal Herritik abiatu
ametsak gauzatzera joan
urrun egonda ere
izateko ondoan,
euskaraz hitz egiterakoan.
Dena dago han!
Elkanok piztutako argien distira
historian historia eginda
bi mundu hortxe elkarri begira
bizitza da mundubira.