---
id: tx-2827
izenburua: Zaldiaren Kanpaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qkh982f3nFg
---

Caballo prieto azabache
nola ahaztu, zurea nauzu ta,
fusilatzeko orduan
Pancho Villaren gizon arruntak.

Gau lainotu batean
gudari aldrak harrapatuz
nere arma kendu eta
egin ninduten hormaratu.


Galdetu zidaten baita,
hil aurretikan, zer nahi nuen ta,
fusilatzeko orduan
ekar zaldia, haren gainian.

Zure gainian jarrita
dena prest zegon tirotzeko
aginduaren hotsakin
handik bapatin alde-eiteko.

Egun sentiko kanpaiak
neska gazte ta alaiak
zertan hain triste
zergaitik dolu minez.

Kanpaijolea, jaun onak
kanpaijolñea, jaun onak
kanpaijolea, dorrean
hil zaigu, ixilean.

Harek bezain gozo
inork ez ditu joko,
harek bezain gozo,
ezta hegan jarriko.

Kanpaijolea, jaun onak
kanpaijolñea, jaun onak
kanpaijolea, dorrean
hil zaigu, ixilean.

Egun sentiko kanpaiak
neska gazte ta alaiak
ez egin negarrik
ez bazuen berbarik.

Eman nizkien laztanak
eman nizkien musuak
galdu nuen be burua
begira, zertarako!

Harek bezain gozo
inork ez ditu joko,
Harek bezain gozo,
ezta hegan jarriko.