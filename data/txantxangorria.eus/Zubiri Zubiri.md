---
id: tx-2930
izenburua: Zubiri Zubiri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iJs2MKXXibw
---

#saveyourinternet #Artículo13


ZUBIRI ZUBIRI
NONGORI NONGO
NONGO ALKATE ZARA ZU?
FRANZTIAKO ERREGEAREN
SEME-ALABAK GARA GU
ZUBI HON(E)TATIK
PASATZEN DENA
HEMEN GELDITUKO DELA
HEMEN GELDITUKO DELA.