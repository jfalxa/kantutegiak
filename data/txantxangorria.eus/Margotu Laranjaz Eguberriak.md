---
id: tx-3403
izenburua: Margotu Laranjaz Eguberriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Kk_WJ_DUhzk
---

Baina margotu ditzagun aurten
laranjaz Eguberriak
orain ta gero gurekin izan
ditzagun denon irrriak

Guztiok libre, topatu gaitzan
martxoan udaberriak
gorliztar denak Gorlizen noski
nahi ditu gure herriak
Xabier libre Gorlizen noski
nahi du ia Gorliz herriak