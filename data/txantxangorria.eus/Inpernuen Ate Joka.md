---
id: tx-2041
izenburua: Inpernuen Ate Joka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pV1VCaZn8GM
---

Gazte zara
gazte zoro
ta Mister Jai
esaten deutsue.
Bixotz ona dekozun arren
ea espabiletan zaren
pekatuz pekatu zabiz, itsu,
kontrolik barik.
Dan dan! Ate joka
Dan dan! Inpernuko atien
noz sartu itzeraitten
Dan dan! Ate joka
Dan dan! Inpernuko atien
Zeure txandie itzeraitten.
Gaztie naz
gazte zoro
ta Mari Jai
esaten dostie.
3ak eta 10ean
SuTxiki zarratzen danien
bialdu mesu bet eta han geldittuko gara.
Hantxe! egongo naz
Dan dan! inpernuko atien
noiz heldu! itzeraitten
bertan! egongo naz
dan dan! inpernuko atien
trago batzuk konpartitzen nauen artien
disko inpernuko atien lagun artien.
Zerure joatie gatz dago,
bizi bakar bat baino ez tau
horregaittik hilko naz ni bizi nazen lez.
Eta! hemen nago
Dan dan! inpernuko atien
nire txandie itzeraitten
Dan dan! ate joka
Moskor! moskor eginde
disko inpernuko atien lagun artien
disko inpernuko atien bota artien.