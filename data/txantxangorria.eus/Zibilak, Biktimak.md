---
id: tx-285
izenburua: Zibilak, Biktimak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2aLYjJoNu6k
---

Ukraina, maite Ukraina
ez da berria zure mina
Han garia noiztik ez da horia?
Ta zerua ez da urdina
Han garia noiztik ez da horia?
Ta zerua ez da urdina

Herritarrak gorriak ikusten
eta etorkizuna beltza
Europak nola zukutuko zintuzken
nahi duena egin baleza
Europak nola zukutuko zintuzken
nahi duena egin baleza

Ta gainera misil odoltsuek
zulatu nahi zaituzte bonbak
hara nola ustekabean zure
bihotzean lehertu den Donbas
hara nola ustekabean zure
bihotzean lehertu den Donbas

Putin ez da gerrako nagusi
ez Zelenski gerrako heroi
zibilak dira biktima bakarrak
argi gera dakigun denoi
zibilak dira biktima bakarrak
argi gera dakigun denoi

Ukraina, maite Ukraina
sendatu berriz zure mina
izan zaitez garia bezain hori
ta zerua bezain urdina

izan zaitez garia bezain hori
ta zerua bezain urdina