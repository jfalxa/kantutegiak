---
id: tx-2258
izenburua: Ilunetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HwaCV6rQPO8
---

Euriak itoa, etxean ezkutu-gordeka, uholdea oroipenetan
Itsasoa ahaztu du egunotan
Aterki azpian sortu egin den mikromundua
Beltzez tintatu egin da
Beltzez orduan doaz hitzak, ta beltzez sentimenduak doaz
Ta beltzez ilundu egin zaizkio pausu handiak
Argiaren bila doa, ilunak beldurtzen du ta
Baina agian ilunetan, hobeto ikusten da
Hezetasun amaigabea, ta haren hezurrek belakiak bezela
Xurgatu dituzte ur litroak
Ur beltzez beterik hitzak, ta ur beltzez beterik sentimenduak
Ta ur beltzez bete zaizkio ere birikiak
Argiaren bila doa, ilunak beldurtzen du ta
Baina agian ilunetan, hobeto ikusten da
Begi biak itxita, arnas sakon hartuta
Galdutako erantzunak, berriz aurkitzen dira
Hobeto ikusten da, ilunetan