---
id: tx-142
izenburua: Ispiluari Begiratzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZBD5q7cs9MA
---

Islada ispiluan ikustean
Ni agertzen naiz bertan
Ni funtsean, biluzik, bakarrik
Zure begietan aientzen
Dena ni banaiz
Ez dut ezer ikusten
Adi egon, heldu da garaia
Ta presta zaitez, hartzazu indarra
Ez izan beldur, maite
Hau naiz ni ilaundu aurretik
Hau ez beste inor erabat galdurik
Hau esentzian, ni neu
Orain ispiluan begiratzean
Ene buruak ez du
Nahi lukeena ikusten
Izua dut nigan une horretan
Funtsik gabeko izua
Arrazoimen faltsuaren emaitza
Adi egon, heldu da garaia
Ta presta zaitez, hartzazu indarra
Ez izan beldur, maite
Hau naiz ni ilaundu aurretik
Hau ez beste inor erabat galdurik
Hau esentzian, ni neu
Nahi zenuen hori ez naiz ni
Con toda esta mierda primo hemen gatxik
Emaidazu maitasun hori
Y toda esa puta envidia para ti
He dejado esa pastilla
No me dejan dormir
Haien begiradak niretzat dira
Ni aske nagok eta haiek azpian
Ez dira sekula mugituko, rigor mortis