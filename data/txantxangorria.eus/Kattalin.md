---
id: tx-1875
izenburua: Kattalin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UNKi_m0BnD0
---

Etxe txiki txukun baten
zure aitatamakin
zinen bizi, zintzo bizi
ene maite Kattalin.

Ene maite, ene maite
ene maite Kattalin
sekulako ta betiko
galdu zaitut Kattalin.

Baina eder zinen zinez
zinen izar bat bezin
ile gorri baita zorri
zuk bazenun Kattalin.

Ibaiaren zinen joaten
zure ahate beltzekin
goiz batean, bat batean
ito zinen Kattalin.

Hilerrian bada lore
euzki lili, krabelin
zu zaituzte badut uste
ongarritzat Kattalin.