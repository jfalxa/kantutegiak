---
id: tx-1352
izenburua: Bi Tanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dqhC_NltLUM
---

Umetatik beti elkarrekin
anaiak bezala bide bakarretik
bihotz ezberdinaz baina
bizimodu berarekin.

Urte asko pasa zituzten
elkar errespetatuz
une garratz guzti haiek
une gozo bihurtuz
bitanta ezberdin ziren
sentimendu berarekin

Lanbroak arrats batean
bi lagunak banandu zituen
bakoitza bere bidean jarri
hitzik ere esan gabe
egun batetik bestera
etsai bilakatuta
bata bestetik urrun
bizitzera kondenatuta.

Bi gazte ezberdin ziren
ilusio berarekin

Bi gazte ezberdin ziren
ilusio berarekin