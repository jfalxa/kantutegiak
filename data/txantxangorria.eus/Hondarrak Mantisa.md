---
id: tx-3078
izenburua: Hondarrak Mantisa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bGxXusa_4bQ
---

hondar tantak bezela, bat bestearen ondoan, 
lotsaturik gaude, lotsatuta. itsasoaren zain, 
noiz iritsi zain. norantza jakin gabe! 
zertarako galdetzen! 
non egongo gara bihar? ez dakit!

haitz bat ginen, 
harri handi eder bat, 
olatuak gogor jota arrastaka ekarri gaituen arte.
zatitan banatuta, txikituta!
behin izandako haitz hura 
gogoratzen, hondar tanta bilatzen,
 itxasoaren zain. 

berriro batuko al gara? 
elkar aurkituko al dugu?
 zein hondartzetan?

noizbait harria bihurtzeko zain,
olatuak noiz eramango zain.