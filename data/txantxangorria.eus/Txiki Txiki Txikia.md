---
id: tx-2160
izenburua: Txiki Txiki Txikia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l3kpYCktCOI
---

Hirurogeita hamar hamarkadatik aurrera Euskal Herriko ume guztiek kantatu duten Xixupikaren abestia.