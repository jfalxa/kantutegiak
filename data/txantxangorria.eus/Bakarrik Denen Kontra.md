---
id: tx-2411
izenburua: Bakarrik Denen Kontra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fi1IzGeoR10
---

Debako rock kolektiboa


BRUTAL COAST-BAKARRIK DENEN KONTRA
Ez naiz artaldean hazi, 
ipurdi gosez dabiltzalako umemoko ugari. 
Bide erreza hartzea nik ez nuen erabaki, 
nahiago dudalako korrontearen aurka igeri egin. 
Askotan sentitu naiz denetatik desberdina,
herri zikin honetan! 
Beldurrik gabe aurrera egitea bide bakarra, 
bakarrik denen kontra! 
Ez naiz heroi, ez naiz jainko, 
galtzailearen postua niretzat da betiko. 
Bizitzak eman dizkit zenbait ostiko, 
nahiago dudalako galdu, geldi egon baino.