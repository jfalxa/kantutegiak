---
id: tx-2238
izenburua: Erradazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vUeqRaao4cU
---

Noiz arte izanen ditugu ttikitako beldurrak?

Noiz arte izanen gara ttiki arrunt ttiki gure heldutasunean?

Erradazu zure beldurrak nireak ote diren.
Erradazu nire kezkak zureak ote diren.
Erradazu, ahapeka bada ere, eta nik, zin egiten dizut
ez dudala zure sekretua agertuko.

Erradazu zure beldurrak nireak ote diren.
Erradazu nire kezkak zureak ote diren.
Erradazu, ahapeka bada ere, eta nik, zin egiten dizut isilpean izanen dudala.