---
id: tx-2488
izenburua: Barberaren Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Od1ArjY-l_4
---

BARBERAREN KOPLAK

(Juan Etchamendi “Bordel”. Luzaide)




 
 
1-Kantore berri xarmant batzuen  
hastera noa kantatzen,
iñorant denik balin bada 
guziek jakin dezaten:
guziek badakizue badela 
izar berri bat Luzaiden
eztitatsunez betia da eta 
guziak ditu xarmatzen
gaixoa_hasi da tristatzen.



2-Jaun Jainkoaren probidentzia 
ez dea bada haundia?
Irauzketako plaza hortan 
holako argi noblia,
argi horrek merezi luke 
ederki adoratzea
oferendatzat emakozue 
mirra edo intzentsua
ahal duenak urrea.

3-Barber gazte bat jiten omen da 
Espainiako aldetik
artizar horrek kontsolatzerat 
erremedioz beterik
barber gazteak hemen ote du 
oi “mirakuilu haundirik”?
Erremedioak egin du bainan 
deus ez omen du hoberik
gaixoa_utzi du trixterik.



4-Kontseiluño bat banikezu 
xarmant emaiteko zuri:
“barbera gazte horrengatikan 
ezbalinbazira eri,
nahi baduzu kontserbatu 
zeure argi eder hori,
gezurño zonbait errakozute 
adoratzaile hoieri
eta ez fida nehori”.