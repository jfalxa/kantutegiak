---
id: tx-2675
izenburua: '"Txantxangorria-Euskal Karaokeak" Proiektuari Buruzko Hitzaldia'
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XC17DuV5wcA
---

2014an Plentzia Kantaguneak antolatutako "KANTARI" apparen aurkezpenean emaniko hitzaldia.