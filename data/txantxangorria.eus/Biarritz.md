---
id: tx-1582
izenburua: Biarritz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ru7SHhfRftU
---

Denborala amaittu da
gorputzek jausi ein dire
alkarreri heldute.

Bare-bare amaittu da frenesixe
sartu urten hori
ta zure grazixe.

Topera hautsontxixe
hutsik whisky botille
sirena bat urrunten
Europaren erdixen.

Ta gure gorputzek
harezko ohe ganien
alkarreri heldute
itsasoari begire
izerrari begire.

Zu eta ni, ni eta zu
maitaleak gara gu.
Ni eta zu, zu eta ni
betirako Biarritz
Ni eta zu, zu eta ni.

argazki bet haja!
Daroat mobilien
ta zure irudixe
nire buruen.

Bare-bare amaittu da frenesixe
sartu urten hori
itsasoari begire
izerrari begire.

Zu eta ni, ni eta zu
maitaleak gara gu.
Ni eta zu, zu eta ni
maitaleak Biarritz
Zu eta ni, ni eta zu
maitaleak gara gu
Ni eta zu, zu eta ni
betirako Biarritz