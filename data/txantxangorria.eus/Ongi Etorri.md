---
id: tx-1030
izenburua: Ongi Etorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qlxdNrtav0M
---

Musika: Itxaso Azkona
Letra: Koldo Celestino


Kurdistan, Senegal, Nikaragua, Sahara,
Kolonbia, Marruekos, Txina eta Siria;
munduko herriak eta hamaika kultura,
elkarrekin biziz gaur Euskal Herrian.

Ongi etorri lagun, zatoz gurekin,
topa egin dezagun, bai elkarrekin

Saltoka, korrika eta itzulipurdika,
jolasean eta kantuan gabiltza,
mintzaira anitzak ta kultura mordoak,
baina gure artean,
jo ta ke euskaraz