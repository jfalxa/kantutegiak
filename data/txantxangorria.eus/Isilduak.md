---
id: tx-2324
izenburua: Isilduak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eNb3UHGczGg
---

HAZIAK II - Leire Etxezarreta, Eider Garro eta Maider Azkarraga-Urizar

#saveyourinternet

Argi izpia azaltzen ez den
Egun gris hoietan, 
Zuen ahotsa entzunezina 
Bihurtu denetan, 
Soinua jantzi eta plazara
Irten zirenetan, 
Zenbat istori gordetzen diren
Beraien oholtzetan.

Isilduak, zapalduak, 
Doinu galduak, desagertuak.
Irrintzika zabalduko
Dugu oihua.

Tradizioak kantu zaharrari,
Zaharrak berriari.
Mauriziak, Primik, Paulak, Iñaxik
Hasitakoari, 
Bide zahar hontan kide berriak 
Bagatoz kantari,
Kolpe berriak ematen zuen
Larru sendoari.

Isilduak...