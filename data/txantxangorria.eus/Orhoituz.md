---
id: tx-377
izenburua: Orhoituz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vcwlXg5CBYs
---

Zaldi beltzak irrintzika zeuden;
Sutan ziran inguruko zelhai
Gorak. Hartzen genduen atseden,
Alhargunen intziriez alai.
 
So ziraden, bekhaitz, saiak oro
Giza-hiroz ok-egin beharrez.
Garoetan neska sabel naro
Zaurituen odola nigarrez...
 
Gau joriak zithal hotz zerion:
Geure bihotz-min-gai' ta zorion
Orhoitua gizaldiz gizaldi.
 
Lagun! Hilkar-lore baratzetan
Genbiltzanok, ilhargi zuritan,
Noiz dukegu, diztirant, Eguerdi?
 
Jon Mirande