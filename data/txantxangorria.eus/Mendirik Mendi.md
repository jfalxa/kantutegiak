---
id: tx-1420
izenburua: Mendirik Mendi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n2r5-phQtwQ
---

Mendizaleak, aurrera,
goazen mendi gailurrera,
euskaldunak esnatzera,
tralaralai, tralaralai.

Aupa goazen mendi txindorri
beti gora, aldetatik, ederra da gure herria;
maite degu bihotzetik ez da lurra hoberik,
larala, larala, ez da lurra hoberik.

Goazen mendirik mendi
baserririk baserrira, eroanaz alaitasuna.
Laster datoz bai alai gurekin,
lasterka bide-txindorretatik.

Erromerira etorri gera, alai alai abestu
dezagun ta dantza egin dezagun mutilak.
Zure bidea da, maitia, gure alaitasuna,
erromeriara etorri gera tralara, tralara.