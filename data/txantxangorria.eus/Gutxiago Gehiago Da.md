---
id: tx-904
izenburua: Gutxiago Gehiago Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zVwutP1Zz8w
---

Extasis-az hil da
kantu hau entzutean
klimaxa izango da
pasioaren ezinbestea
burutzea.

Bere munduan bizi dela
jakin  badaki etsipenean
Plazerraren zoro tokian
maitasunaren orbainetan.

Gutxiago gehiago da,
gutxiago gehiago da.

Extasis-az hil da
kantu hau entzutean
klimaxa izango da
pasioaren ezinbestea
burutzea.

Hutsuneak estali nahian,
edukitzetik askatuta
lasaiagoa bilakatu da.
Amesteko garaia heldu da.

Gutxiago gehiago da,
gutxiago gehiago da.
Gehiago da, gehiago da.