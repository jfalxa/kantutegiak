---
id: tx-405
izenburua: Bost Ogei Urtetako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RbQCvIslGG4
---

Bost ogei urtetako dontze llatxo bat
oiean daukat gaixorik.
Oilo bat ere il diot bainan
eztauka obekuntze rik.
Beste bat ere ilen nioke
yeikiezpaledi oietik.

¡Ai au bizitze larri erdiragarri
zer ikusteko otenago!
Sudurra ere zinzilizka det
kokotza baino berago.
Nere etxean naiko lukete
len bai len ila banengo."