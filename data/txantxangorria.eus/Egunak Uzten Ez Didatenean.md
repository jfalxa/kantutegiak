---
id: tx-157
izenburua: Egunak Uzten Ez Didatenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ojZvIjmhOwE
---

Zuzendaritza / Dirección: Mattin Zeberio & Virginia Luengo
Produkzio Laguntzailea / Ayudante de producción: Juan De Salas
Estilismoa: Maria Sarriugarte
Foto fija: Maria Sarriugarte
Talentua: Malentxo Zeberio, Maitane Iruiñ, Gorka Lasa
Rental: AVisual Pro, Falco Films

Portada: Miguel Vides

Biolina: Matxalen Erzilla
Biola: Malentxo Zeberio
Biolontxeloa: Irati Sanz

Nahasketa: Asier Errenteria


Egunak uzten ez didanetan
Lotuta banengo bezala
Airea estu eutsi eta

Galderen bila eman nazala

Egun sentian gelditzen zaidan
Usain horiaz azala
Eutsi ezin dut hegoan
Hodeiak ukitu ahala

Une hau bat besterik ez bada
Orain eta hemen ez bagaude
Nork emango digu geroa
Gorpuzterik ez badute

Sokak ez banau estutzen
Ta iraganak beldurtzen
Gure gorputzen gezurrekin
Ez geundeke itxaroten

Une hau bat besterik ez bada
Orain eta hemen ez bagaude
Nork emango digu geroa
Gorpuzterik ez badute

Argi hodia jarraituz
Ukatuko dut iluna
Besteen esku leunen
Zurrumurruekin batera

Egunak uzten ez didanetan
Lotuta banengo bezala
Airea estu eutsi eta
Galderen bila eman nazala