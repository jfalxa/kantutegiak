---
id: tx-1737
izenburua: Gereziondoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uzHcqOHuWi0
---

GEREZIONDOA
Ken Zazpi & Euskadiko Orkestra Sinfonikoa

Bihar, bihar, bihar, behar bada bihar.
Guk eskeini dizugu mundu zaharra,
adreiluzkoa.
Eta hondakinetatik gereziondo bat
loratuko da.
Bihar, bihar, bihar, sinisten dut bihar
izarrak itzaltzean, bideak deusteztean
anfetak isitzean.
Egin kasu bihotzari beldurrak biluztuz
zure begi garden asketan.
Bihar, bihar, ikusten dut bihar
eraiki zubi bat ur gerren gainetik
gereziondorantz.
Eta bere magalean oroitu nazazu
erakutsi nizkizun kantetan.
Bihar, bihar, maitezaitut bihar.