---
id: tx-1140
izenburua: Txirri, Mirri Eta Txiribiton
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ryR9Gn_P39c
---

Txirri ta Mirri ta Txiribiton! 
Sagarra, manzana. Ikatza, carbón. 
Mujer, andrea ta hombre, gizon. 
Xanti, xanti, xanti, 
txiribi, txiribitero. 
Xanti, xanti, xanti, 
txiribi, Txiribiton. 
Txin pon! 
Ni nauzue Txirri, jakintsu ta on. 
Besteak Mirri ta Txiribiton. 
Xanti, xanti, xanti...
Ondo entzun zazue nire izena: 
Mirri da munduko guapuena. 
Xanti, xanti, xanti...
Mok... mok... 
Mok... mok...
Hau dugu gure Txiribiton. 
Xanti, xanti, xanti...
Txirri ta Mirri ta Txiribiton! 
Sagarra, manzana. Ikatza, carbón. 
Mujer, andrea ta hombre, gizon. 
Xanti, xanti, xanti...