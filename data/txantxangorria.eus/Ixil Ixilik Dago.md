---
id: tx-1723
izenburua: Ixil Ixilik Dago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4lwVIfsVOwg
---

lxil ixilik dago, kaia barrenian
untzi xuri polit bat uraren gainean. (bis)

Goizeko ordu bietan esnatutzen gira
arrantzaliarekin joateko urrutira.

Pasatzen nintzanean zure leihopetik
negarrak irteten dit begi bietatik (bis)

Zergaitik (5 aldiz) negar egin?
Zeruan izarrak dagoz itsaso aldetik. (bis)