---
id: tx-2794
izenburua: Aita Et'Ama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_3jjw2OTzTc
---

Aita eta ama etzaten dira,
ganbera baten barnean.
Aita eta ama etzaten dira,
ganbera baten barnean,
ez dira nitaz anitz ohartzen,
berak untsa direnean...
Jeloskeriarik ez izateko,
ni maitiaren aldean!!

Gazte n(a)iz eta ene lorios
ezpiritia kürios
Gazte n(a)iz eta ene lorios
ezpiritia kürios
batez agrados
besteaz jelos
mutil gaztekin gaü oroz.
Mutil gaztekin
gau oroz eta
nundik naiteke malerus.

Ate xokuan makila eta 
leiho azpian zürubil.
Ate xokuan makila eta 
leiho azpian zürubil,
ene maitia handik sartzen da,
sekretuen prudenki,
sekretuen prudenki eta 
amak oraino ez daki!!