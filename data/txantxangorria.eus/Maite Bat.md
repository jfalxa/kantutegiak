---
id: tx-1892
izenburua: Maite Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t8gmN4O9SvI
---

MAITE BAT (KONTSESIRI)
«EGUZKI BEGI»
LAJA ETA MIKEL
TRIKI ELKAR, 2002
 
Maite bat maitatzen det, maitagarria
begi ederra du ta guztiz argia
daukat urruti
bainan ezin kendu det burutik
haren itxura
saldu al baliteke pisura
urriaren truke
nork erosi faltako ez luke.
 
Hogeita lau leguaz nago aparte
bitartean badauzkat miloi bat ate
guztiak itxirik
nahi arren ezin egon ixilik
beti negarrez
nere maite-maitearen galdez
ote dan bizi
bihotz-bihotz nereko Kontxesi!
 
Ai hau bakardadea eta tristura
nere bihotz gaixoa ezin piztu da
hain dago hila!
beti dabil kontsuelo bila
bere artean
banengo maitearen aldean
ordutxo biko
pena guztiak lirake hilko!
 
Esperantzetan bizi, maite gozoa
noizbait kunplituko da gure plazoa
eta orduan
gauza txarrik ez hartu buruan
iehengoari utzi
ez degu pasatzen pena gutxi
preso sei urtez
onduko gaituzte nere ustez.
 
Hitzak: Joan Inazio Iztueta
Musika: Herrikoia