---
id: tx-3353
izenburua: Toaila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w7U601DOivk
---

A ze txurro luzea
ez da jatekoa
zabaldu ta jolastu
hori da gure asmoa.

Toaila aurpegian
Ai! Oihal goxoa
toaila belaunetan
ta lehortu airean.

Poliki, poliki gerritik pasa,
lehortu bizkarra

Beherantz pasa, Tomasa
mugi gaitezen. Txa! Txa! Txa!
Ta gorantz altxa, Tomasa
mugi ta saltsa! Txa!