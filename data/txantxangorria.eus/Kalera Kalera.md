---
id: tx-2759
izenburua: Kalera Kalera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/igTZUC5NiSU
---

Altxa begiak zapaldu orok, deiadarra lagunari
berriro izanen gaituk/n kide indar emanaz iraultzari.
Kalera kalera borrokalari kalera
kalera kalera borrokalari kalera
hire indarraren beharra dinagu gure indarrarekin batera
hire indarraren beharra dinagu gure indarrarekin batera.
Zai dago ama, zai aita, zai kide ta lagunak
hator hator Euskadira, hator hator etxera...
zai dago ama, zai aita, zai kide ta lagunak
hator hator Euskadira, hator hator etxera!

Bultza ta bultza euskal langile, Euskal Herri sufritua
burni kateak herri labetan, danba danba lurrera kartzelak.
Kalera...