---
id: tx-3144
izenburua: Herrira Noa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wOcDaDjCKS8
---

Ateak Ireki webgunearen ekarpena U12ko manifestaziorako. Joseba Sarrionandiaren letra baliatuta Jozu Zabalak egindako "Herrira noa" kantaren bideoklipa. 
Aportación de la web Ateak Ireki a la manifestación del 12 de enero en Bilbo. Videoclip de la canción "Herrira noa", compuesta por Josu Zabala sobre una letra de Joseba Sarrionandia.

Igerian, hegan, oinez...
Pausorik pauso bagoaz.
Lainoa nahinora doa,
Baina gu,
baina gu herrira goaz.

Arrainak saretik ihes,
Txoriak libre dabiltza;
Askatasunaren bila
dabil gutako bakoitza.

Gerra eta zoritxarra,
Ez da ezinbestekoa;
Askatasuna behar da,
denontzat benetakoa.

Hormetan daude ateak
eta muga da pausua;
Oztopoa ez da izango
Guretzat enbarazua.

Kartzela egin nahi dute
Herriak itotzekoa;
Herri bat egingo dugu
Kartzelarik gabekoa.