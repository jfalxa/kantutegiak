---
id: tx-2478
izenburua: Estibadoreen Balada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1AohuSruRwo
---

Hogei tonelada buru gainean, 
txantxetan hartzen dugu arriskua
egur fumigatua daukan pisua
gure dignitatea da astunagoa.
Makiñak, txirrinak, gruak ta zamak
eslaboiak dira gure bizkar hezurra.
Bodegetatik zerura begira
kapatazak oihuka
azken izada!!!

Filipinetako marinerua
kapin mozkortu zen bart gauean.
Txikiak dira baina gogorrak
ring baten gainean Paquiao bezala.
Arbasonen hotzikara sentitu dut noizbait
nere aitona han zendu zan.
erdoildutako munstro hoietan 
gordeta dauden mila bizitza.

Ur lainotan zamauntziak
kalatxoriek garrasika
itzal laranja ta orixak
zamaketarien azal urratua 
urrez beteta daude arrakalak
sua gure bihotz ta buruan.