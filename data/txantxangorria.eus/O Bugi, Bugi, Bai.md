---
id: tx-2547
izenburua: O Bugi, Bugi, Bai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F6JvO75z3Rg
---

O, bugi, bugi, bugi bai, hey!
O, bugi, bugi, bugi bai, hey!
O, bugi, bugi, bugi bai.
Paran-pan, pan, pan, pan!
Hey!


Hau da nire behatza,
hau da beste behatza,
hau da nire eskua,
hau da beste eskua.

O, bugi, bugi, bugi, bugi, bugi bai.
Paran-pan, pan-pan, pan, hey!

O, bugi, bugi, bugi bai, hey!
O, bugi, bugi, bugi bai, hey!
O, bugi, bugi, bugi bai.
Paran-pan, pan, pan, pan!
Hey!

Hau da nire begia,
hau da beste be/gia,
hau da sudur punta,
hau da a/ho zu/lo/a.

O, bugi, bugi, bugi, bugi, bugi bai.
Paran-pan, pan-pan, pan, hey!

O, bu/gi, bu/gi, bu/gi bai, hey!
O, bu/gi, bu/gi, bu/gi bai, hey!
O, bugi, bugi, bugi bai.
Paran-pan, pan, pan, pan!
Hey!

Hau da nire ukodo/a,
hau da beste ukondoa,
hau da nire sorbal/a,
hau da beste sor/balda.
O, bugi, bugi, bugi, bugi, bugi bai.
Paran-pan, pan-pan, pan, hey!

Hau da nire hanka,
hau da beste hanka,
hau da ipurdia,
berriz ipurdia.

O, bugi, bugi, bugi, bugi, bugi bai.
Paran-pan, pan-pan, pan, hey!