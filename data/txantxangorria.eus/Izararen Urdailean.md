---
id: tx-2767
izenburua: Izararen Urdailean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3LROevUkGLo
---

Triste bizi naiz eta horrela
adierazi nahi dut nire kantuetan
amodiotaz hitz egiten, zertaz bestela?
urratsa atzera emanez bihotzen kontuetan
emakume mordo batek erotzen ari naute
ni eztabaidan sumitzen muinak lehertu arte
gaztea naiz eta oraindik arraitxo naiz
olatu hauen artean nabil itotzen maiz

Berez zaila ez den jolasa
korapiloz hornitu da
iskanbiletatik urrun isolatu ezinean

Hilargiak aurpegi bi ditu
ni bezela, bat ilun bestea argitsu
horrietan odolaren aztarna uzten joan naiz
nekez, izorratu du!

Buru makur iraun beharrean nago
izararen urdailean eguna argitu arte
lotsak, kezkak... guda garaitu egin didate
moralaren lurraldean lurperatu naute
argitan galduta
topatzen paretaren kontra
hortzak bertan behin eta berriz suntsitzen ditudala
atseginik merezita