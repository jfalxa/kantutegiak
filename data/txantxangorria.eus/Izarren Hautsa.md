---
id: tx-3046
izenburua: Izarren Hautsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NeXHfdvd7yY
---

Izarren hautsa egun batean bilakatu zen bizigai,

hauts hartatikan uste gabean noizpait ginaden gu ernai.
Eta horrela bizitzen gera sortuz ta sortuz gure aukera,
atsedenik hartu gabe: lana eginaz goaz aurrera,
kate horretan denok batera gogorki loturik gaude.