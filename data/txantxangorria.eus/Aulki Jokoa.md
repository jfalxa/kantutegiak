---
id: tx-519
izenburua: Aulki Jokoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-uSj3_LKGt0
---

Musika/Música: Idoia Asurmendi 
Hitzak/Letra: Maria Oses  

Musikariak/Músicos:  
Joanes Ederra 
Oriol Flores 
Andoni Garcia 
Sergio Valdehita 
Idoia Asurmendi  

Grabazioa eta nahasketa/Grabación y mezcla: Yon Vidaur (Muir Estudioa) Masterizazioa/Masterización: Estanis Elorza (Doctor Master)  

Zuzendaritza/Dirección: Nestor Urbieta 

Artea/Arte:  
Nora Goialde 
Irati Peluaga  

Aktoreak/Actores:  
Martin Abarrategi 
Ione Oyanguren 
Irati Lasaga 
Maialen Ballina 
Angel López de Bergara 
Goretti Ortueta 
Alberto Asurmendi 
Nerea Asurmendi 
Ekain Eriz  

AULKI JOKOA  
Aulki huts bat dago 
sukaldeko izkina hor(re)tan aspalditik 
inork ez du bota nahi, 
ezta bertan eseri ere.  

Atzo hustu zen elizako 
bosgarren lerroko lehen eserlekua, 
ta ja ez du inork beteko 
igandetako mezetan.  

Har ditzagun aulki hutsak 
ta sor dezagun aulki joko bat, 
bihurtzeko zuen hutsuneak 
bizi indar, 
nolabait.  

Ta zenbat okupatu dezakeen 
ez dagoen zerbaitek; 
hutsune batek irensten gaitu 
ekaitzik handiena bailitzan. 
Ta gaur gure txikitik, 
gure izanetik ta ezinetik, 
har dezagun atseden 
aulki hutsetan.  

Har ditzagun aulki hutsak 
ta sor dezagun aulki joko bat, 
bihurtzeko zuen hutsuneak 
bizi indar.