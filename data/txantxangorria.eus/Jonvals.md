---
id: tx-1127
izenburua: Jonvals
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oQ3zEcz07z8
---

Gaur euria ari du
zeure artean diozu
zer ote den matte duzun guztitik
eta bukatu gabeko maitasun istorio horretatik
urruntzera bultzatzen, zaituena.

Eta zer da, zer da ausardia?
ez mendi bat eskalatzeko behar dena
ausardia maite dituzun pertsonen laguntasuna,
maitasun eta goxotasunari uko egitea da.

Txori jaio nintzen eta inbidiaz
begiratzen diot lurrean zoriontsu denari
tristura sortzen dit pentsatzeak
mina ematen dudala
baina kaiolan ezingo nuke... 

Ametsen herririk baldin badago,
gau izartsuetan zure bila hegan joango naiz,
ametsetan, dena hurbil baitago
gure artean, gure artean
ere hala den bezala.