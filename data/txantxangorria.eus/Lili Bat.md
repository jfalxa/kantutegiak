---
id: tx-1690
izenburua: Lili Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l4xqlYdP7Rg
---

Loretan dago aspaldi huntan
gure begien eder delarik,
gure bihotzen gorotzak baitu
ongarriztatu et' ernarazi.
 
Eder da... Haren hatsak, ordea,
zithala ba du, herio-hazi:
ilhuntzen ditu gogo argiak,
ilhauntzen ditu argiak oro.
 
Hilherri-lili horren zithalak,
hezur zaharren hautsak hazirik,
erhanik ditu aiten leinuak
eta zainhiltzen gaitu semeok...
 
Lili haur ene landetan ere
aspaldi huntan ezin hila dut,
nahiz Ipharrak astintzen duen,
nahiz kiskailtzen duen Hegoiak...
 
Ai! goragotik ethorri-haizeek
ailezate jo ene landetan
et' eiharraraz «itxaropena»
izena duen zithal-lili haur!