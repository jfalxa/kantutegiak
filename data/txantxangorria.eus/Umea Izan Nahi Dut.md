---
id: tx-1709
izenburua: Umea Izan Nahi Dut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tslupsLjgwE
---

Ohartu naz gaur
nagusi egin nazela
eta beldurtzen hasia naz
munduan ez da
pentsatzen nuen modukoa
Dibertigarria
zenbat aldatzen ari garen
Eta nik umea izan nahi dut
edozein ordutan libre egoteko
Eta zu Son Gokuren
pare ibiltzeko. Eta hoin...
Ez dago haren moduko
lasaitasunik
Bide berriak hartzeko
garaia heldu da
erabakitzeko maitasuna
non gelditu da etxeko
lanak arazotzat
hartzen zen garaia
Zenbat barre txabola hartan
Umea izan nahi dut
edozein ordutan libre egoteko
Eta zu Son Gokuren
pare ibiltzeko.

Eta hoin...
Ez dago haren moduko
lasaitasunik

Ezeizu ahaztu 
umea izan zinela
eta gurasoak maiteak
beti alboan dituzula

Umea izan nahi dut
edozein ordutan libre egoteko
Eta zu Son Gokuren
pare ibiltzeko.

Eta hoin...
Ez dago haren moduko
lasaitasunik

Haren moduko
lasaitasunik