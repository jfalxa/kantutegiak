---
id: tx-956
izenburua: Arma, Tiro, Pum
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dYo3VtxEzAM
---

Edozein herriko jaixetan

Iluntzean, uraren ertzean

Amets baten bezala musika ixil-ixilean

Bakartien rokaren uhartean

Euritan dantzan

Bang bang txiki bang

Gauero zuretzat ilargia lapurtzea

Zuretzat dira abesti honen lerroak

Ezin dira ito eztarri indartsuak

Hontarako jaiota honengatik hilko naiz

Ta zure eskutik beldurrik gabe bizi

Euskadi rock-ak ez du dirurik ematen baina

Batzuk bat, bi, hiru kolkua bete diru

Txanpon baten truke

Edozein lan egiteko gai

Ertzainak etortzian arma, tiro, pum!

Musika uhinek bortizki kolpatzen didate bihotza

Taupadaka esnatuz lotan dagoena

Hemen naiz zurekin esnatzeko

Berba txikiz egiak esateko

Eta zuretzat mila abesti idazteko

Ikusi, ikasi eta ondo entzun

Eta zaindu ezazu maite duzun hori

Euskadi rock-ak ez du dirurik ematen baina

Batzuk bat, bi, hiru kolkua bete diru

Txanpon baten truke

Edozein lan egiteko gai

Ertzainak etortzian arma, tiro, pum!

Euskadi rock-ak ez du dirurik ematen baina

Batzuk bat, bi, hiru kolkua bete diru

Txanpon baten truke

Edozein lan egiteko gai

Ertzainak etortzian arma, tiro, pum!

Bat bi, bat bi

Arma, tiro, pum!

Bat bi, bat bi

Arma, tiro

Bat bi, bat bi

Arma, tiro, pum!

Bat bi, bat bi

Arma, tiro, pum!