---
id: tx-1265
izenburua: Nor Da Piztia?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fRVn75p9oSE
---

LIHER taldearen 'Tenpluak Erre' bigarren lan luzeko 'Nor da piztia?' abestiaren bideoklipa. 2018ko maiatzean grabatua eta landua Mikel Fernandezen eskutik. Esker bereziak Agustin eta Jaiak Mozorro Dendari.


Basoan ezkutaturik
Nork ehizatzen du nor?

Milagarren eguna beirazko zingira honetan
Letaginetatik daridan hatsak badu zer esan
Kakotik zintzilik gelditu ziren ametsak
Haragi puska naiz edertasunaren mesedetan

Plastikozko ezkutuek, tantaka dituzuen orratzek
Zorizko estadistikek
Salbu mantentzen zaituztela uste duzue
Ereindakoaren ustela gordinik jango duzue
Basoan izkutaturik bada hainbat pizti aske

Nork ehizatzen du nor?

Animalia orok defendatzen du odolez bada ere bere gotorlekua
Animalia orok erasotzen du bortxatu duen hura, galdetzeko:
Nor da piztia?



Tenpluak Erre, released February 7, 2018
Donostiako IN Estudioan Iñigo Etxarrik grabatua.
Bateriak Urdulizko Tio Peten grabatuak.
Bonbereneako Karlos Osinagak nahastua.
Azkarateko Mamia estudioan Jonan Ordorikak masterizatua.