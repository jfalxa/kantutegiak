---
id: tx-458
izenburua: Bueltan Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DmNzNwsRP8Y
---

Itzuli dira
Udaberriko argi izpiak
Liolako zeru gorriak
Erdibana idatziko ditugun istoriak
Pikutara kezka guztiak
Alferrik dira harresiak hegoak dittugunontzat

Heldu orain eskutik
Ta goazen amets berriak idazten
Saiatuko gara elkarrekin

Heldu orain eskutik
Ta goazen bide berriak argitzen
Hasiko dira

Ze bueltan da!
Barruan dugun pasio sugarra
Bueltan da!
Barruan dugun pasio sugarra

Itzuli dira
Presa gabeko orduak elkarri begira
Itzuli dira
Eztarria urratu arte kantatzeko abestiak

Itzuli da!
Baldintza gabe maitatzeko ausardia
Itzuli da!
Gure begietan somatzen zen distira