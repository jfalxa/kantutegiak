---
id: tx-795
izenburua: Itxura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nG5uozS3QDA
---

Musika eta letra: Iñigo Etxezarreta (E.T.S.)
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Zuzenketak: Zuriñe Gil
Ekoizpen exekutiboa: Baga-Biga

Maskararen atzean
ezkutatzen dena garrantzirik gabe.
Jendearen aurrean
ikusi nahi dutena irudikatzen.
Olerkien antzera
errima perfektua zaude bilatzen,
baina zure barruan 
irudi hori ez duzu sinesten.

Beti itxuraren eskutan ohartu nahi ez. 
Errepikatuz guztia arreta eske.
Eraikitako oskolak babesik gabe.
Erositako jarrera… lotsaren truke.
 
Ospearen atean
erakusten duzuna irizpide gabe.
Gezurraren sarean
jarraitzaileekin egunero amesten.
Aktoreen antzera
lehenengo planoa zaude desiratzen,
baina zure barruan
kamararik ez duzu topatzen. 

Zer erakutsi nahi dugu azalaren gainean?
Zerk baldintzatzen du irudia?
Zergatik gaude guztiok maskararen atzean?
Zein da zure aurpegi onena?