---
id: tx-2069
izenburua: Begira Nazazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9eX_YfHSQhY
---

Ez du onartu nahi berriz erori da infernu horretan
ta bakarrik dagonean nola jasan mina? Inork ez daki.
Bere burua ez badu ulertzen nola begiak ireki
eta aurre egin? Ez du gehiago nahi bizi.

Begira nazazu, egin dudan guztia alferrik ez al da?
Agian izan naizena... gezur puta bat.
Barka nazazu oraindik ez dakit
nor naizen, nor nintzen, nor izan nahi dut.

Ezin du sinestu, ezin du ulertu,
horrela bizitzea ez omen da erraza,
eta ezin du gehiago bere burua jipoitu,
izanak ezin dira ukatu.

Ez dut onartu nahi berriz erori naiz infernu honetan
ta zurekin ez banago nola jasan mina? zuk ba al dakizu?
Nere burua ez badut ulertzen nola begiak ireki
eta aurre egin? Ez dut gehiago nahi bizi.