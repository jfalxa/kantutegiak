---
id: tx-2436
izenburua: Gaueko Erregina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OkZ195JpSFs
---

GAUEKO ERREGINA
Haur nintzenetik, antzeman nuen,
emakume jantziak zein gustoko nituen,
makilaje ta karmin, liga bularretako,
Akabo! jendeak jakin ezkeroooo

Lo nengoen, ta ustekabez,
Pristilaren unibertsoak iratzartu ninduen, 
hainbeste urte galdu, nola ez nuen ikusi?
barnean gorderik nuena, neuk etsaia dut beti.

Oker nago, ezin dut egon,
egon ezin hau, ez diot inori desio.
Irudi harrotz baten gatibu izan naiz,
ezin galdu, bide berri baten zain.
Ta orain bai, askatu naiz,
gau txorien erregina, salakeriaren etsai.
Ia ezin dut sinetsi, ia ezin dut sinetsi,
askatu naiz, askatu naiz! ie ie

.....  ..... .... ....

Gauaren ohian beltzak nire atzean,
egun berri baten berria dauket niretzat.
Plataformadun botak, ezapinak su kolore,
ez da izarrik, ni bezain dirdiratsurik.

Izan zaite, ez izutu,
lagunaren eremura eramango zaitut,
milla aldiz esan, guztiz aske zaudela,
ezkutuan duzun emea, barneratu daitekela.

Oker nago, ezin dut egon,
egon ezin hau, ez diot inori desio.
Irudi harrotz baten gatibu izan naiz,
ezin galdu, bide berri baten zain.
Ta orain bai, askatu naiz,
gau txorien erregina, salakeriaren etsai.
Ia ezin dut sinetsi, ia ezin dut sinetsi,
askatu naiz, askatu naiz! ie ie

.....  .....  .... ....

La lala la la, lala lala lala,
lala la la, lala lala lala, 
la la lala lala lala lala la, la (X6)