---
id: tx-899
izenburua: Agur Etxekoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5F7Rz4RDU30
---

AGUR!

 

Agur etxekoak!

Agur herritar!

Bortitza bada bidea,

zenbat ez ederrago,

denak borrokan biltzea!

 

Agur zapalduak!

Eta etsituak!

Ez dugu ez oraindik,

gure lurraren hatsa

osoki suntsiturik!

 

Agur euskaldunak

Agur Euskadi!

Egungo hersturetan,

berpiztuko zerauku

euskal bizia orotan!

 

 

      Ortziken ikusgarrirako egina. Bururatzean emaiten ginuen Peio Ospital, Iñaki Urtizberea-rekin. Oraikoa, airea nahitarat zahar moldean emana, erakusteko Euskal Herriaren zorigaitzak ez direla oraikoak.