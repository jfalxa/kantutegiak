---
id: tx-2501
izenburua: Ortziren Mailukada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r9kzVnEs0Fg
---

Laboak abesten zuen txikia nintzala
nere ohe onduan amaren beharra.
Hilen ta bizien lehia amaigabean
gauaren misterioen oraindik beldur naiz.

Euria beltza da nere erreinuan, 
mutur joka bota ditut traidoreak kampora.
zortzi pinta kabitzen dira nere sabelean
zuretzat badut leku paraje honetan

Ekialde urruneko exodoak bultzatuta
nere anaiak datozte esklabu bihurtzera,
madarikatu zituen Siriar atso aztiak
kazetari, bankeru ta militar guztiak.

Matalaz-en burua Mauleko gazteluan
Ortziren Mailukada nire gainera
esnaezinezko amezgaiztoan.

Sinistera behartua izan zinen mezan.
atabakan bota zenitun zure irabaziak.
atetik sartu zen Basajaun krudela ta
pulpitoan larrantzi zuen abade zikoitza.