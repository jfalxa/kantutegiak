---
id: tx-2594
izenburua: Udazkena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EK06Hc2am1Q
---

Firi-firi, dabil firi-fi
haizea dator mendi goitik.
Udazken, ken, ken txorimaloa janzten (4 aldiz)
Txapel, txapel, txapela, txapel
burua dago epel-epel.
Udazken, ken, ken txorimaloa janzten (4 aldiz)
Din-din nabil gabardinakin
bizkargaina urdina urdin.
Udazken, ken, ken txorimaloa janzten (4 aldiz)