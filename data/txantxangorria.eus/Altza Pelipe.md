---
id: tx-2698
izenburua: Altza Pelipe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IaVJTIkdQcw
---

Altza Pelipe trulalai, trulalai... 
Pello burusoila da ta 
eltxoek heltzen diote. 
Ama beti kexan dabil: 
&#8220; jantzi txanoa arlote 
ttiri, ttiri, ttiri, ttiri, 
ttiri, ttiri ttiri tto. 
Atezainaren semeak 
eskaileretan txiza. 
Alabak kaka egiten du, 
deitzen da Maria Luisa. 
Altza Pelipe trulalai, trulalai... 
Barre eginikan bapo bapo, 
goazen bai plazara, 
hantxen egingo dugu 
gogotik algara. 
Oraindikan, mutilak, 
barretxo bat bota, 
bota bota beste bat, 
oraindikan bota. 
Oraindikan, neskatxak, 
barretxo bat bota, 
bota bota beste bat, 
oraindikan bota. 
Altza Pelipe trulalai, trulalai... 
El pobre Mercurio no tiene camisa, 
yo como la tengo me muero de risa. 
Mercurio gaisoak alkondarik ez, 
nik badaukat eta lehertzen naiz barrez. 
cuatro camisas tengo,
laurak dira zaharrak, 
adobia baneuka konpontzeko denak. 
Altza Pelipe trulalai, trulalai... 
Txalopin txalo, 
txalopin txalo, 
katutxo mispila 
gainean dago. 
badago bego, 
bego badago, 
zapata berriei 
begira dago. 
Altza Pelipe trulalai, trulalai... 
Arrantzalea naiz ta 
ez daukat dirurik, 
hiru alaba dauzkat 
ezkondu beharrik. 
Laugarrena semea 
kapote zaharrarekin, 
konejua dirudi 
bere bizarrakin.