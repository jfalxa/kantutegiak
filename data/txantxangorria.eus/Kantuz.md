---
id: tx-2412
izenburua: Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7jxpMZqIpps
---

Kantuz daramazkit nik gauak,
kantuarekin, 
goxatzen neure penak.
Kantuz kanpotik, negarrez barrutik,
kantuarekin, 
ahazten ditut grinak nik.

Kantuz deramat bizia,
ahazteko hiltzia.
Neuretzat kantuan dago
fortuna guzia.
Eta kantuz, kantuz,
noa botatuz, neure hazia.

Batzuetan triste, besteetan hasarre,
zuen aurrean, 
kantatzea dut lege.
Nahiz gogorik ez izan,
zuen aurrean, 
sartu behar dut martxan.

Kantuz deramat bizia,
ahazteko hiltzia.
Neuretzat kantuan dago
fortuna guzia.
Eta kantuz kantuz,
noa botatuz, neure hazia.