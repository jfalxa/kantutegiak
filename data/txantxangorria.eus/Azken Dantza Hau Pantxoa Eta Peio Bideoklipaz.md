---
id: tx-1494
izenburua: Azken Dantza Hau Pantxoa Eta Peio Bideoklipaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4RSqaS1DXVI
---

Azken, dantza hau,
maitia, zurekin...
Nahi zinduzket
ereman nerekin...
Baina, gaurko xedia
ezin daike betia!...
Badakit, nik ere,
bihar dela joaitia!...

Bego pena hau!
Itzuliren naiz
Bai! Berriz Euskal Herrirat!
Bego urte hau!
Etorriko naiz
betikotz zure ondorat!...

Azken dantza hau,
ez da sekulako...
Zin egina dut,
zin egin betiko,
hemen, gure lurrean
bizi behar dutala!
Hori ez bada,
hil hotz jar nadila!...