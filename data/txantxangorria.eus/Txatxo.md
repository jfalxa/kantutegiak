---
id: tx-2163
izenburua: Txatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IlDTR-GS49M
---

Asturiaseko Los Berrones taldearen abesti arrakastatsua Zarama taldeak euskeratuta bertsionatu zuen. Ederra, inkunablea.