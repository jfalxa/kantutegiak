---
id: tx-3184
izenburua: Urrun Izan Arren -Xabi Solano Eta Dz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PDJkEtgtTRs
---

Xabi Solano eta DZ - Urrun izan arren (Selah Sue - Etzakit cover)

Errefuxiatu bat izatea ez da gauza erreza
Maite duzun herria atzean utzi beharra
Anai maitea mesedez ez zaitez kezka
Ni hementxe nago zure partez borrokatzeko eta

Gauero bezala zu ta zure ingurukoaz oroitzen naiz
Tristurak ezin izango du nere itxaropenarekin
Zeinen urrun izan arren laister hemen egongo zara
Zeinen urrun izan arren zailena pasa da eta...

Hemengo giroa geroz eta beroagoa da
Geroz eta jende gehiagok dauka independentziaren egarria
Kanpoan zaudetenok etxera itzuli zaitezte
Euskal herri libre batian batera bizi gaitezen eta

Gauero bezala zu ta zure ingurukoaz oroitzen naiz
Tristurak ezin izango du nere itxaropenarekin
Zeinen urrun izan arren laister hemen egongo zara
Zeinen urrun izan arren zailena pasa da eta...
Eta...