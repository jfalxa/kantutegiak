---
id: tx-2620
izenburua: Zarako Apretak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q5GBLWWVUuc
---

Ahotik ahora
arotik ahora
oroitzen dut gure
amonen denbora.

Amaren apretak
lehengusinaren jertsea
ta amonaren brotxea.
Erromeriak jartzen
nau esperantzan
norekin egingo dut dantzan?

Akotik ahora
arotik arora
lehen amona zen
ta orain biloba.

Larruzko botinak
Zarako jertseaz alde,
amona agurtu gabe.
Juerga egiteko batek
baino hobe bik
nork idatziko ote dit?

Erromeria lehen
eta parranda orain
egunaknoiz helduko zain.
Denbora asko galtzen
degu guztira
erlojuari begira.

Ahotik ahora
arotik arora
zergatik joaten da
ihesi denbora.