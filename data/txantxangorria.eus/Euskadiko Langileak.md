---
id: tx-838
izenburua: Euskadiko Langileak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zvbMTn45NY8
---

Esperantza kanta, hau guretzat baita

Euskadiko langileak

Esperantza kanta! bildu gaiten guziak.
Lur lanetan edo lantegitan
Norendako hoinbertze lan?
Beti berak dira manatari
Ez dugu guk nahi hori!

Jaunak ados beti jaunekilan
Aspalditik gare hortan
Ttipiak baigara dohakabe
Haundiak ditugu jabe.

Izan hego edo iparreko
Denak Herri berdineko
Ditzagun urratu gaur beretik
Gure bideak mugatik.


      1971.ekoa. Hegoaldeko tira bira batzuen oihartzun egina. Han hasi baitziren abertzale batzu euskal langileriaren indarraz dudatzen eta «españolisman» sartzen. Hemen ez baiginuen oraino langileria abertzalerik ere. Hegoaldetik ukanik, Bokaleko lantegi bateko grebalariek (berrogoi batek) diru laguntza pollita eskuratu zuten.



      Eskaintza eder horren ohoretan egin kantua.