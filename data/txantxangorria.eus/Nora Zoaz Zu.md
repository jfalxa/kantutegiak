---
id: tx-2546
izenburua: Nora Zoaz Zu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oGxf9ur7buE
---

Nora zoaz zu?
Ni etxera noa.
Nora noa ni?
Zu etxera zoaz.
Agur andereño,
agur, agur, agur.
Agur andereño,
agur, agur, agur.
Non/dik za/toz zu?
Ni ikastolatik nator.
Nondik nator ni?
Zu ikastola/tik z/toz.
Kaixo, kaixo, ama.
Kaixo, kaixo, ama.
Kaixo, kaixo, ama.
Kaixo, kaixo, ama.