---
id: tx-1021
izenburua: Eskual Anaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VdutfmFPDmY
---

Xabier Iratzeder


Erdal-itsaso iresle hortan galduak zaizten anaiak,
Euskal-Eguna baitugu egun zueri agur nasaiak.
Euskal-anaiak, altxa burua, bota irrintzin alaiak!
Ari zaizkigu oldarka sartzen uhainak eta hedoiak,
Bainan Euskaldun atxik gaitzala Euskara gure mintzaiak.
 
Etxe-xokoan elgarri tinko, Euskaraz kantuz aitzea,
Ai! zer gozoa, zer bozkario, zer bihotzaren asea!
Etxe xokoan kantuz aitzea paregabeko bakea!
Sistu-marrumaz ari dadila kanpo beltz hoitan aizea,
Guk etxeñoan azkar dauzkagu Euskara eta Fedea.
 
Mundu guziko euskal-etxetan mintza ta kanta Euskaraz:
Aintzinekoen euskal-su hura barnean berritz pitz-araz.
Amerikano ala Paristar, orroit gaur zuen odolaz:
Euskal-hitz bero harrokatsuez zuen bihotza denek haz,
Denak euskaldun eta denak bat, jabe gaitezen Geroaz.
 
Oi Zabiereko Euskal-semea, Saindu hoin bihotz-zabala,
Garen tokian zure anaiak izan gaiten zu bezala.
Beti euskaldun, kartsu eta bat, zu itsas-argi goazila;
Dugun erakuts gure sailean zer den Euskaldun odola:
Euskal-Herria funski maitatuz dugun pitz Mundu-zabala.
 
Pertsu hauk ditut Beloken eman euskal-anaien pizgarri,
Euskal-jendeen ber-pizteari eginez agur ta irri.
Euskal-anaiak gauden osoki egin gaituen Jaunari.
Egun beltzetan ibil gogoan euskal-fraideen hitz hori:
Mundu guziko Euskaldunen zain, zerurat gaude kantari.