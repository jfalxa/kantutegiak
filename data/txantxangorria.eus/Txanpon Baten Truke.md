---
id: tx-2164
izenburua: Txanpon Baten Truke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ju44uWJs-z4
---

Alaitz eta Maiderren abesti arrakastatsuena. Inkunablea

Ezagutzen dut gizon bat kale kantoian Jarrita,
Ipuin harrigarriak kontatzen dituena
Txanpon baten truke
Dragoi eta printzesaren ezinezko maitasuna
Zahartutako basoko lotiaren azken eguna
Ihintz urdinak abandonatuta
Txanogorritxuren dibortzioa
Otsoaren suizidioa
Ezagutzen dut gizon bat kale kantoian Jarrita,
Ipuin harrigarriak kontatzen dituena
Txanpon baten truke
Jendeak presaka dabil ordea
Entzutekoo astirik ez dauka
Aspaldiko garaietan urrutiko lurraldeetan gertatutako
Historioak
Txanogorritxuren dibortzioa…