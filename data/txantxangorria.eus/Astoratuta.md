---
id: tx-2896
izenburua: Astoratuta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ddGhYIZAQgs
---

Mugitu gorputza 
Aspaldian astoratuta daukat 
Gorputz osoa, 
Egunsentitik iluntzerarteko 
Dardarizoa, 
Bihotza ari zait bom, bom, bom 
Etengabean bom, bom, 
Goitik behera eta behetik gora, 
Dantzatzeko gogoa!!! 
Hasi da burua, 
Sorbaldak, ukondoak, eskuak, 
Segidan gerria, 
Aurrera atzera ipurdia, 
Gero oinetara, 
Batetik bestera bai!!! 
Aspaldian astoratuta daukat… 
Hasi da burua…