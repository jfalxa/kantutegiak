---
id: tx-1350
izenburua: Aurkitu Genuen & Herriko Jaixetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KGYa9arPQJg
---

Aurkitu genituen gure ametsak
Aurkitu genituen itzalpean
Aurkitu genituen geure izarrak

Aurkitu genituen aspaldian.

Aurkitu genituen lehen jolasak
geure izpiak egunsentian
Aurkitu genituen lehen kantuak,
lehen sekretu eta irriak
Aurkitu zenituen nire emozioak,
nire beldurrak nire ondoan
Aurkitu genituen mundu berria batera

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Aurkitu genituen lehen zauriak
momentu zailen izpiluan.
Aurkitu genituen azken muxuak
gure bihotzen barrenian.
Gozatu genituen denborapasak
gogoratu genuen ametsetan.
Agurtu genituen geure begiak itxita.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Beti nire ondoan
maite zaitut anaia.
Beti nire arima
maite zaitut laztana.

HERIKO JAIXETAN

Dantza, dantza giroa
gaur espero duguna
gau luzea aurrean

berotzen hasi da

Mundua konpontzeko
lagun arteko eztabaidak
berba-berba ari gara
ahal gara artean

Kontzertuaren ostean
gero arte bai baina
berriz elkartuko gara
herriko jaixetan

Etorri ona egin topa
egin ezazue dantza
mugidu txosnatik-txosnara
alde batetik bestera
etorri ona egin topa
eta jarraitu parrandan
gauak luzeak dira eta
egunak oso motzak

Utzi alboan arazoak
bota kezkak zakarrera
bizi ezazue oraina
ile askatuta

Kontzertuaren ostean
gero arte bai baina
berriz elkartuko gara
herriko jaixetan

Etorri ona egin topa
egin ezazue dantza
mugidu txosnatik-txosnara
alde batetik bestera
etorri ona egin topa
eta jarraitu parrandan
gauak luzeak dira eta
egunak oso motzak

Etorri ona egin topa
egin ezazue dantza
mugidu txosnatik-txosnara
alde batetik bestera
etorri ona egin topa
eta jarraitu parrandan
gauak luzeak dira eta
egunak oso motzak

Etorri ona egin topa
egin ezazue dantza
mugidu txosnatik-txosnara
alde batetik bestera
etorri ona egin topa
eta jarraitu parrandan
gauak luzeak dira eta
egunak oso motzak