---
id: tx-3274
izenburua: Mari Domingi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ae9nlSm6h2A
---

Horra Mari Domingi 
begira horri 
gurekin nahi duela 
Belena etorri. 

Gurekin nahi baduzu 
Belena etorri 
atera beharko duzu 
gona zahar hori. 

HATOZ, HATOZ, ZURE BILA NENBILEN NI ! 
HATOZ, HATOZ ZURE BILA NENBILEN NI! 
HATOZ GOAZEN TA GURTU DEZAGUN 
BELENEN JAIO DEN HAURTXO EDER HORI 
HAURTXO EDER HORI.