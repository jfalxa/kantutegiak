---
id: tx-1079
izenburua: Zerbezie Komandua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YnTsrUFON_E
---

Txikiteuan, mozkor artian, sasi iraultzailia
Nun sartzen gara, trintxera artian, jentia bihar danian


HAGINAK ERAKUSTEN FALTSUEI GABERO
ZERBEZIEN KOMANDUAK TABERNIARI SUA EMONGO DOTZO

Sistema ustel hau, barra eskinan, zetarako kritikatu
Gorrotua antolatu ezean, ezin da ezer aldatu