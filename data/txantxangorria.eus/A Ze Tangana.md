---
id: tx-256
izenburua: A Ze Tangana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QQQW5LEKtqE
---

16 urte Gaztetxeak aurten
Eta nork daki noiz arte jarraitu dezaken
Ez baita erraza egia esaten 
Pijo eta progreen herri baten ijito izaten

Uztailaren 17an
Montatuko dugu kriston festa
Flamenko onaz gozatzeko modu hoberik ezta
Urteurrenean
Montatuko dugu kriston festa
Flamenko onaz gozatzeko modu hoberik ezta

Desjabetuon arteko
Elkartasuna oinarri
Borrokaren bidetik
Mundu berri bat aldarri