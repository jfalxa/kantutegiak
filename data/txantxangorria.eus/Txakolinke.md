---
id: tx-2929
izenburua: Txakolinke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZLkBTFYktg0
---

Txakolin, txakolin,
txakolinak on egin,
txakolin, txakolin, txakolinak
eman dio Maritxuri zer egin

Azunberdi pitxerrean
kuartilu (e)ta erdi sabelean
txakolin, txakolin,
txakolinak on egin.

Txako (e)ta txako
txako (e)ta txako
txako (e)ta txako
txakolinaren gozoa