---
id: tx-1413
izenburua: Working Class
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BC213d4SFEs
---

Sasoi ilunenak noizbait igaro ote ziren,
ke arteko egunak amaitu al dira...

Sasoi ilunenak noizbait igaro ote ziren,
ke arteko egunak amaitu al dira,
hogeitalauan igota, fabrika zahar eta itxiak
nagusitzen direla antzematen bada...

Geltokira iritsi naiz, piztu da argia,
kaleak mutu daude, ordua izango da,
gurasoak ere gauez etxera iristen ziren
gogoratu dit gaur, berriz, arropako usainak.

Working class
the pride is coming back,
duintasunaren alde, aurrera goaz.
Working class
we will never die,
auzoetatik zutabeak abiatu dira.

Sasoi ilunenenak noizbait igaro ote ziren,
ke arteko egunak amaitu al dira...

Kontatzen zidaten gogtsu zeudela,
grebara joateko zalantzarik ez zutela,
kaleratzen aurrean, batasuna behar zela,
klase kontzientzia eta elkartasuna.

Langabezi, murrizketak, prekarietatea...
Gogor jasaten dugun gaurko egun hontan,
konturako al gara batasuna behar dela
klase kontzientzia eta elkartasuna.