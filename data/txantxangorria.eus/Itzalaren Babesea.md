---
id: tx-136
izenburua: Itzalaren Babesea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qllYitZvHN0
---

JULENen Itzalaren babesean kantaren bideoa. 2021eko urrian grabatua. Naia Aizpuruak zuzendua. 

Vídeo musical de Itzalaren babesean de JULEN. Grabado en octubre de 2021. Dirigido por Naia Aizpurua.

Music video of JULEN's Itzalaren babesean song, recorded on October of 2021. Directed by Naia Aizpurua.

2022an Andoni Condek grabatua eta Gaztain Estudioetan nahastuta eta masterizatuta.
Grabado en 2022 por Andoni Conde, y mezclado y masterizado en Gaztain Estudioak por Eñaut Gaztañaga.

DOP: Iñaki Lizarraga
Zuzendari laguntzailea: Uxue Morales
Kamera: Angello Eloizaga
Produkzio burua: Natalia Arteche
Elektrikoak: Ander Zelaia, Diego Álvarez de Arcaya
Muntaia: Iñaki Lizarraga

Hitzak | Letra | Lyrics:

Zenbat miresten zaitudan
Inork ezer ez bezala
Barre egiten duzunean
Negua begitan hartuz
Erakutsi zenidan
Abesti hori bezala, bezala

HOTZIKARAK SENTITZEN DITUT PENTSATZEAN
NIRE BARNEAN JARRAITZEN DUTELA
GALDERA GUZTIEN ERANTZUNAK
ERANTZUNAK
ERANTZUNAK

Biluzik utzi zenidan
Azken kantu honen argipean
Nire ahots isildua
Begirada itsuak
Esperantza galdua
Ta sua, sua, itzali ezinean

HOTZIKARAK SENTITZEN DITUT PENTSATZEAN
NIRE BARNEAN JARRAITZEN DUTELA
GALDERA GUZTIEN ERANTZUNAK
OHOHOH
HOTZIKARAK SENTITZEN DITUT PENTSATZEAN
NIRE BARNEAN JARRAITZEN DUTELA
GALDERA GUZTIEN ERANTZUNAK
ERANTZUN
ERANTZUN
ERATZUNAK

Gauero sentitzen zaitut
Ohearen bestaldean
Nire itzalaren babesean