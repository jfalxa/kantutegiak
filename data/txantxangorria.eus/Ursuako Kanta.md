---
id: tx-645
izenburua: Ursuako Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aMXxkzs5VnQ
---

-"Goiti zazu burua
ene arreba Juana"

-"Eztezaket goratu,
anai Bautista jauna."

-"Izorra omen zara,
ene arreba Juana."

-"Zertan ezautzen nauzu
ene anaia jauna?"

-"Zaldia zelarekin
zuretzat jarria da."

-"Nik eiten dudan haurra
zure besokoa izain da."

Ursuak zazpi leiho,
zazpiak lerro lerro;
Lantainako alaba
Ursuan defuntu dago.

Ursuan defuntua
Santa-Anan kausitua;
adios erran gabe
etxetik partitua.

Zazpi errota berri,
zortzi jauregi xuri;
horien guztiengatik
nik ez Ursura nahi.