---
id: tx-2038
izenburua: Norbera Bere Ametsen Jabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l3wZ4MxxnHU
---

Anai Handiaren begiradak 
goizetik gauera beldurtzen zaitu 
bizimolde diseinatu honetan 
aldiro nahi zaituzte barneratu 
debeku eta izunen munduan 
ordainsari oro behar gainditu 
gizarte honen esigentziak 
ezin dira edonola ekiditu 

Askatu ditzagun kateak 
egin sistema honi aurre 
zaren heldu gazte ta nerabe 
norbera bere ametsen jabe 

Desabantaila eta onurak 
hagian asko edo hagian gutxi 
sare sozialek eta wifiak 
niretzat ez dute hainbeste garrantzirik 

Estresak edo antsietateak 
akatuko nau bihar edo etsi 
teknomenpekotasun honetan 
zer garen dugu guztiz ahantzi 

Askatu ditzagun kateak 
egin sistema honi aurre 
zaren heldu gazte ta nerabe 

norbera bere ametsen jabe 

Jaio berri baten irrifarre zintzoa 
hori da simpleki naturaren mintzoa 
ez dut behar gauza materialen multzoa 
beso artean hartzean nere haurtzoa 

Jaio berri baten irrifarre zintzoa 
hori da simpleki naturaren mintzoa 
ez dut behar gauza materialen multzoa 
beso artean hartzean ... 

Askatu ditzagun kateak 
egin sistema honi aurre 
zaren heldu gazte ta nerabe 
norbera bere ametsen jabe 

Askatu ditzagun kateak 
egin sistema honi aurre 
zaren heldu gazte ta nerabe 

norbera bere ametsen jabe 

Norbera bere ametsen jabe 
Norbera bere ametsen jaaabeee 

Norbera bere ametsen jabe 
Norbera bere ametsen jabe 
Norbera bere ametsen jaaabeee