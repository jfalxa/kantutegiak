---
id: tx-958
izenburua: Zein Ederra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gEzijSInZnw
---

Rosario Flores-en "Que bonito" kantuaren euskarazko bertsioa EtxE musikariak egina, Jagoba Ormaetxea ekoizlearekin batera.

Version en euskera de la cancion "Que bonito" de Rosario Flores realizada por el musico EtxE, junto al productor Jagoba Ormaetxea

Egilea/Autora: Rosario Flores
Moldaketa eta Itzulpena: Jagoba Ormaetxea eta EtxE
Album: EtxE - Supernova (2018)

Zuzendaria/Director: Iñaki Etxezarraga "EtxE"
Kamara eta edizioa/Camara y edicion: Jose Alvarez Burubero



Zein ederra zu hor ikustean
Zein ederra zu sentitzean
Zein ederra hemen zaudela pentsatzea
Zein ederra hitz egiten duzunean
Zein ederra mutu gelditzean
Zein ederra hemen zaudela sentitzean

Ederra litzateke hegan egin
Eta zure ondoan abesten ni jartzea
Elkarrekin iraganean bezala
Nire barnean sentitzen dudala
Zure ariman nigan dagoela
Eta inoiz aldenduko ez dela


Zein ederra zure ile beltza
Zein ederra gorputz osoa
Zein ederra zure izana

Izana

Ederra litzateke hegan egin
Eta zure ondoan abesten ni jartzea
Elkarrekin iraganean bezala
Nire barnean sentitzen dudala
Zure ariman nigan dagoela
Eta inoiz aldenduko ez dela


Zein ederra esku artean
Zure kitarra laztantzen dudanean
zein ederra horrela zu sentitzea
Zein ederra 
Zein ederra
Zein ederra
Zein ederra
Zein ederra
Zein ederra
Zein ederra
Zein ederra
Zein ederra