---
id: tx-3266
izenburua: Bizi Ganoraz Kepa -Junkera Edorta Jimenez-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/huzoU5LoDO8
---

Hona aukera: jalgi hadi euskaraz!
Laga erdara, bizi ganoraz.

Ez egin galde gu zenbat lagun ote;
berekin asko izan gaitezke.

Itsasoan ura ta harea;
euskara ere gurea;
denona da kaleko hondartza;
gerorantza goazen korrika.

Hona aukera: jalgi hadi euskaraz!
Laga erdara, bizi ganoraz. (bis)

Denon arnasaz (e)ta hire izerdiaz
eginak kasik bide erdia.

Basoetan pinu ta pagoa;
euskara da geroa;
haster bedi karrikako oihana
euskalduna goazen korrika!

Hona aukera: jalgi hadi euskaraz!
Laga erdara, bizi ganoraz. (bis)

Eskuz-ezkuveta azkar pausoa
egin dezagun bide osoa.

Haizez doaz kantu ta oihuak;
jaso denok eskuak.
Gure sua euskararen
garrahelmugara goazen korrika.

Hona aukera: jalgi hadi euskaraz!
Laga erdara, bizi ganoraz.