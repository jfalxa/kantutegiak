---
id: tx-2817
izenburua: Hitza, Etorkizunaren Giltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_aP3caa0Iy0
---

Itzartu diren hitzak badatoz
amuko zubiaren azpitik igerian. 
Amuarrain arin bihurturik
Ibaiederreko uretan jausika.

Landetan zelaian barrena
aurkitu dute hitzek bide zuzena
sirena oihu bilakaturik
zeharkatu dute lantegi arte estua

Loiolako etorbidetik Xanjuandeira datoz
ufadetan hostoak haizearen lagun
nora doazen esateko asmoz
hitzak boladetan heltzen dira urrun

Txistuka datoz herri guztia esnatuz
kea dariola Urolan gora
hitzaren trena har dezagun guztiok
igoko al gara denok barrura

Bakoitza bere bidetik dator
aniztasunean elkartuz gatoz
hauxe itxaropen printza
zabaltzera goaz

ETORKIZUNARI ATEA, GURE HITZAREN GILTZAZ (bi bider)

Hitzak badatoz iada hitzartuta
auntzen bizar gris bihurtu dira.
Izarraitzen hegian goitik behera
Azketa iturri ondotik jausika

Zerutik erortzen dira milaka perla
Kantauritik datoz gure hitzak laino
lanbroa ari du eta busti dira
Urrestila Nuarbe eta Lasao

Bakoitza bere bidetik dator
aniztasunean elkartuz gatoz
hauxe itxaropen printza
zabaltzera goaz

ETORKIZUNARI ATEA, GURE HITZAREN GILTZAZ (bi bider)

Gure hitzak izan dezan oihartzun
Euskal Herria iratzar dezagun
mila atabal danbor gainean kolpeka, kolpeka

Entzun dadila hotsa, herritarren ahotsa…

Bakoitza bere bidetik dator
aniztasunean elkartuz gatoz
hauxe itxaropen printza
zabaltzera goaz