---
id: tx-378
izenburua: Infernuko Ilargi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zL1Xi4Hd2ZE
---

INFERNUKO ILARGI
 
Jende pila dago inguruan ezaigu falta ezer
Juerga on bat botatzeko
Osagai guztiak ditugu hemen
Alkohola, tabakoa, droga bigun ta gogorrak
Denetarik daukagu ta ondo pasatzeko gogoaz blai
Zerbait arraroa somatzen hasi naiz ordea
Jendearen begirada gero eta ilunagoa da
 
Jaun beltzaren obra ote da?
Jaun beltzaren obra ote da?
 
Zu infernuko ilargi gorri
Gau zoro hontan nire argi
Izan zaitez nire gidari
 
Indar misteriotsu bat sentitu dut bapatean
Anbiente arraro bezain erakargarri horretan sartzeko
Denok barnean daukagun deabrua deika hasi zait
Limitazioak ahaztu ta aske izateko
Barneko desio guztiak beldur gabe bizi
Aurreiritzi putak beingoz alde batera utzi

Jaun beltzaren obra ote da?
Jaun beltzaren obra ote da?

Zu infernuko ilargi gorri
Gau zoro hontan nire argi
Izan zaitez nire gidari
Gidari
Gidari
Gidari