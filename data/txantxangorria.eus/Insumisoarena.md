---
id: tx-850
izenburua: Insumisoarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7WuL6qKjK1s
---

Postari zintzo horrek / zer dakar karpetan?
amodio gutunik / etzegok giretan

militarren ohar bat? / bitxia benetan

bidali duenari / eramaiok bueltan.

Natural sentitzen dut / egiten dudana

eta natural egin / sentitzen dudana.

Insumiso egin naiz / horitxe da dana

ahaztu gabe guztioi / muxu handi bana.

Nigarrez ari zara / ama maite hori

ba omen nuen beste / ankera ugari.

Nola obedituko / diot eroari

ez badizut jadanik / obeditzen zuri?

Ene kolega maite / beldurtia haiz hi

aholkuak ematen / ez hadila hasi

kartzelara noala / diostak itsusi

etor hadi bisitan / txorizo ta guzti.

Funtzionari zoli / ene kartzelero

juezen sententzia / txikien borrero

ezin nauzu hain errez / bihurtu numero

kantari esnatzen naiz / goizero goizero.