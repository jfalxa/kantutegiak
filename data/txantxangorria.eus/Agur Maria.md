---
id: tx-1045
izenburua: Agur Maria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M9LZG9XBivE
---

Agur Maria
agur Maria
zer gertatzen da
nere herrian
anaitasuna hautsi da eta
batzuk besteei beti aurka.

Agur Maria
agur Maria
entzun egizu nere otoitza
bildu egizu nere herria
agur Maria
lagun nazu.

Agur Maria
entzun egizu nere otoitza
bildu egizu nere herria
agur Maria
lagun nazu.

Agur Maria
agur Maria
lagun nazu.