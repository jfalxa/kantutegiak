---
id: tx-871
izenburua: Lendakari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E79WrKptPrA
---

LENDAKARI

 

 

      I

 

Atzo eman hitzak egun zer balio

Atzo ez da. Bihar dugu ikusiko!

Egunari hurbil gaua, ez da segur oraikoa

Egun zurekin dagona bihar duzu kontrakoa!

 

 

      II

 

Ur zikinak arrainak dakarzke berdin

Bainan gerta zintazke suge batekin!

Eiherako errotari hori guzia zer zaio?

Arrainak ez ditu harek sugetarik berexiko.

 

 

      III

 

Eiherako harriaren pare zonbat

Bihia duteno ez axola hanbat.

Bertzen bihitik kraskatuz

Nunbaiko urak nahasiz,

Erroturik dabiltz batzu

Lendakari izan nahiz.

 

 

            Lendakari

            Herriak ez ikusten argi

            Lendakari

            Esplika zaizkio hobeki!

 

 

      1970-71.ekoa. Iparraldeko abertzaleen arteko lehen eztabaida mingarri batzu ikustean egina. Abertzaleek ere maite bait dugu «boterea» eta ez beti zatitzen inguruan. Ai! botere kori!