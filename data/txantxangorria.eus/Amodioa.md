---
id: tx-818
izenburua: Amodioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4vE9dgPts0Y
---

AMODIOA ZOIN DEN ZOROA
 anonimoa , XVIII-XIX. mendeak

 Amodioa zoin den zoroa mundu guziak badaki,

Nik maiteño bat izaki eta beste batek eramaki...

Jainko maiteak gerta dezala enekin baino hobeki...!

 
Azken bestetan egin nituen izarraren ezagutzak,

Denen artean ageri ziren haren begi urdin-beltzak,

Irri polit bat egin baitzautan, piztu zauztan esperantzak.

 
Geroztik ere mintzatu gira, eia nahi ninduenetz...

Harek bietan baietz erranik, pentsa kontentu nintzanez!

Ez nuen uste haren agintza betea zela gezurrez.

 
Orai bihotza urtua daukat urean gatza bezala,

Izar onaren argien ordain, izigarriko itzala!

Ohoin tzar batek berriki joanik, nola naiteke kontsola!