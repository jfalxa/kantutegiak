---
id: tx-1748
izenburua: Erlojua Ito
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n7T4RS460dE
---

Herrenka dator
denbora gure atzetik,
barreka dator,
inozoen burdinbidetik.

Herrenka goaz,
oina jan digu denborak,
barreka datoz
orratz zurituen serorak.

Ordulariak
mutildu digu eguna,
erauzi digu
atzo oparitu ziguna.

Arnasestuka
lehenengo arnasatetatik,
ito beharrez,
inora bidean tatarrez.

Itzali gara
mustur gainean izara,
herrenka gatoz
udaldien hiletetara.

Erlojua
ito dugu ur zikinetan,
orratz biak urtu,
zenbakiak
lausotu dira ketan...
Barre gatoz etxera.

Baina badator
soseguaren sasoia
lodi heldu da
inurri ludikoen uda.

Ez bilatu gu:
denbora birrindu dugu,
hilerrietan
hasi ginen atzo kartetan.

Erlojua
ito dugu ur zikinetan,
orratz biak urtu,
zenbakiak
lausotu dira ketan...
Barre gatoz etxera.