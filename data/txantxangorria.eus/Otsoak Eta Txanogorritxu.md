---
id: tx-579
izenburua: Otsoak Eta Txanogorritxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xollV4cRmKg
---

Otsoak, uuuuh!
mendi gainean ugari,
xuri beltzak, urdin, gorri
trumilka ziren etorri.
Horregatik Txanogorritxu
urrundu otsoetatik!

Otsoak, uuuh!
goseak ditu ekarri,
errez baita aurkitzea
inguru hontan janari.
Horregatik Txanogorritxu
mintzatzen da otsoekin.

Otsoak, uuuh!
irrifar egiten daki,
hortzak luze, hain ederki,
izan dezagun erruki.
Horregatik Txanogorritxu
Pozik dabil otsoekin.

Otsoak uuuh!
Jan ditu hamasei txerri,
ehun oilo, hogei ardi,
gosea kentzen badaki!
Horregatik Txanogorritxu
bildurtu da otsoekin.

Otsoak uuuuh!
berek dakiten bidetik,
uste gabe, laister berriz
artaldearen erditik.
Zoritxarrez, Txanogorritxu
beti dabil otsoekin.