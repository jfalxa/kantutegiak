---
id: tx-3098
izenburua: Burua Ezin Jasota Markos Unzeta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KFx42mlHctU
---

Ondo da, ondo da,
utz ditzagun horrela.
Gaur betiko legez
eztabaida bera.
Ez al zara konturatzen
alkarrekin gaudela
burua ezin jasota nabilela
aspaldian?

Zure begi horiek
antzerako zerbait
diote ahotik datozkizun hitzek
beste kontu batzuk
darabila bitartean
Zure asmakizun hutsak dira
ez zaitez engaina.

I/bai a/gor ho/nek ja/da ez
da/ra/ma u/rik
No/ra/e/ze/an lur le/hor
ho/nen bi/de/tik ga/bil/tza,
e/ta ez dut i/kus/ten ir/ten/bi/de/rik.
Zi/ur/ta/sun ho/nen ja/be
e/re na/go ba/ka/rrik.

E/ta ez ga/ra ba/ka/rrik
e/go/te/ko ja/io.
Ez al du/gu de/nok
la/gu/na/ren de/si/o?
Ba/ina gu/re ho/nek zer du ba/li/o
ge/zu/rra/ren lo/ka/tze/tan
bal/din ba/ga/ra i/to?

Ai/tor/tzen di/zut ga/ur
ha/men/txe ber/tan
poz/tu/ko nin/tza/te/ke/e/la
e/san/go ba/ze/nit.
Kon/tu/ra/tu za/re/la
el/ka/rre/kin gau/de/la.
Bu/ru/a e/zin ja/so/ta
ze/bil/tza/la as/pal/di/an.