---
id: tx-3028
izenburua: Aingura Hegodunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r70D03UWrdI
---

erori banaiz ere, gorantz erori naiz
nola urazaleratzen diren arrainak hiltzean
oroimena da urperatzen nauen beruna
ametsa, berriz, gorantz naraman zama astuna
aingura hegoduna
erortzeko beldurrez garelako eusten diogu elkarri
oinez ikasi orduko, hegan ahantzi
zu zara orain urperatzen nauen beruna
beste dena, ordea, gorantz naraman zama astuna
aingura hegoduna
erortzen bagara ere, amets berriei helduz gorantz
erortzen gara. gorantz jaitsiko gaituen aingura
hegodunari... arrain hilak bagina, hondoratu ordez
urazaleratzen gara