---
id: tx-1749
izenburua: Soinu Gorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mMAbp0h2JRQ
---

Bizitzak behin eta betirako
mirari bat jartzen du eskura.
Zu be gure zorionerako
etorri zinen mundura.

Egongelako guda zelaian
mutiko bi dabiltza olgetan.
Neu bihurtu naiz ume txikia
zure begi urdinetan.

Egizu barre lotsarik gabe,
doazela minak urrutira.
Hartu, maitia, soinu gorria;
jo dezagun pieza hura.

Saminez nago minez bazaude,
zure poza neurea be bada.
Gorde gure sekretua, gero!
ni zu naiz, ta zu ni zara.

Neurrira egindako trikitrixa,
aulkitxoa eta ur botila,
hara gure artista txikia
biharko soinuen bila.

Egizu barre lotsarik gabe,
doazela minak urrutira.
Hartu, maitia, soinu gorria:
jo dezagun pieza hura.