---
id: tx-2126
izenburua: Zerrailak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c_wxzDAqGGg
---

koloreak preso bizi dira egunez 
hala ere gauez, ihes egiten dute hegan eta 
lo dagoen jendearen ametsetan sartzen dira, 
ordu batzuetan besterik ez bada ere, libre bizitzeko. 
hargatik izaten dira ametsak hain bizi. 
egunak ez du oraindik argitu baina oskarbi dago zerua. 
pentsa, inor gutxik daukala bere herriaren egunsetiaz gozatzeko aukerarik 
ireki ba begiak jaiotzean dagoen egun berrirako, 
jarri gaitezen zirrikitutik harago begira, sarrailatik haragoko argira, gure bidean.