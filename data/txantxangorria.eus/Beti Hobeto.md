---
id: tx-1532
izenburua: Beti Hobeto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v0EMNGM_mkE
---

Jendea kexatzan bizi da egunero,
bizitza zatartzen da kexatuz gero.
Kontua ez da arduratzea gertatu denarekin,
kontua da korrekin zer egin.

Nik badut truko bat hobeto bizitzeko:
esan behar da beti dena dela hobeto.
Irudimena zabald,u, eman zeure onena,
jarrera da axola duena.

Gertatu zaizun zerbaitek ilun jarri bazaitu,
altxatu eta esan "Hobeto"!
Irribarre bat margoztu, eta gogotik esan:
"Gertatzen dena beti da hobeto".

Autoa apurtu baldin bada hobeto!
Mugikorra galduta badaukat: hobeto!
Hola oinez joango naiz ta lagunekin hitz egin
batuago inguruarekin.

Etxekolanak ahaztu baditut: hobeto!
Logelan zigortuta banago: hobeto!
Ze ondo pinturak hartuz pareta margotzea
hobeto da ondo pasatzea.

Gertatu zaizun zerbaitek ilun jarri bazaitu,
altxatu eta esan "Hobeto"!
Irribarre bat margoztu, eta gogotik esan:
"Gertatzen dena beti da hobeto".