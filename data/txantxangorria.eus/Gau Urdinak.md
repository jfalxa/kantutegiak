---
id: tx-2201
izenburua: Gau Urdinak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N1YJuoSVncA
---

Música:   BEÑART SERNA
EÑAUT ELORRIETA LARRUCEA
JON MIKEL ARRONATEGUI
Letra:  EÑAUT ELORRIETA LARRUCEA
JON MIKEL ARRONATEGU

Une hau ta gero dator gauetan urdinena
Eta ez dut oroituko nor nintzen ez nire izena
Bolbora usainak metaforarik banatzean
Gure errealitatea biluztean.
Urtarrila udaberriz bete nahi dut behin da berriz
Ta sekula itxiko ez diren leihoetatik ihes egin.
Hondartza gabeko itsaso ezkutuen gisan
Ez dut nik noraezean guretzat denborarik izan
Beranduegi da egi guztiak aitortzeko
Baina aukera bat dugu barre egiteko.
Gelditu denbora eta eramango zaitut
Kate guztien gainetik
Inoiz amestu bako lurretara
Zergaitiek garrantzirik ez dutenera
Izen bako izarretara
Argitu nahiez duten gauetara.
Urtarrila udaberriz bete nahi dut behin da berriz...
Areak ta denborak bidea estali arren
Ziur aurkituko dugu geroa batera bagaude
Gelditu gaitezen beti gaua balitz bezela
Berriz biluztu gaitezen maitatu gaitezen
Gelditu denbora...
Gau urdinetara(bis)