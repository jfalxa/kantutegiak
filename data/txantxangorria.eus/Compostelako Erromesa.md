---
id: tx-1641
izenburua: Compostelako Erromesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1CpsGR1XVH8
---

Compostelan egon nintzen
Lehengo udan erromes
Noizbait iritsiko nintzen
Joan izan banintz oinez 

Arribatu nintzeneko
zain neukan Maruxiña
A zer serora parea
Bera &#8216;ta lehengusina!

Lagun biak hasi ziren
Gora Euskadi oihuka
Ni Biba Galizia Ceibe
Malkoak ezin xuka

Matxinada zabaldu zen
Tabernarik taberna
Fraga eta bere fragatak
Beldurtzeko galerna!

Santuari ez nion nik 
Ez kaixo ez adio:
Compostelaraino gabe
Astiarran Santio!

Nahiz gaita jotzen ez jakin
Sentitzen naiz gallego
Herritasuna noiz behinka
Aldatzerik balego!