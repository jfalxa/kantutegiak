---
id: tx-1612
izenburua: Nomada 22m2
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kXOnGeZ_9uc
---

NAROA GAINTZA Ahotsa, doinuak, hitzak (2,6,10 eta 11 kantuetako hitzak izan ezik)
DAVID NANCLARES Baxua, gitarrak, musika moldatzailea, ekoizpen musikala.
DAVID GOROSPE Bateria.
ASIER ERCILLA Teknikaria, teklatuak, eskusina.


isolatuz galderak
bizimodu honek ez narama inora

zer izan naiz
eta non gelditu da dena
oztopoz-oztopo
aurrera egin behar
akaso zalantzatik
egin dut ikasketa
maldan gora noa
baina ez dakit nora

zirriborroz 
eraiki dut mundu bat
denbora gaizki kudeatzeak
du erru guztia
zerotik ezin dut
noiztik ezin erabaki

eta nomada naiz nire herrian bertan
iragana eta oraina banatzeko asmoz
eta hauskorra naiz lur eremu honetan
bakarrik nago,
bakarrik nago eta horrela erabaki dut

irrifar bat
baina barrutik udazkena
ispilu honetan
arrakalak milaka
gezurra estandar bat
egiaren atzean
ihes egiteak ez narama inora

eta nomada naiz...
(irrifartsu nago,
irrifartsu nago baina ez naiz zoriontsu)