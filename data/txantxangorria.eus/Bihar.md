---
id: tx-1966
izenburua: Bihar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g2MWSMTSCr0
---

EZ ESKATU GAUARI BERRIZ LAZTANTZEKO
GOIZEZ MOSU TRITE BATEZ AGURTZEKO,
EZ ESAN MAITE ZAITUT EZIN BAZAITUT EUKI,
EZ, EZINGO NAIZ BIZI...

BIHAR, MAITEA EZINGO ZAITUT AHAZTU
BIHAR 100000 ZATITAN HAUTSIKO NAUZU
ZUK UTZITAKO HONDARRETATIK
HASI BEHARKO NAIZ BERRIZ!!

EZ NAZAZU HORRELA BERRIZ BEGIRATU
GOIZEZ EZINGO BANAUZU BESARKATU,
EZ ESAN BIHAR MEZU BATEN EA ONDO NAGOEN,
EZ, EZINGO NAIZ BIZI...

BIHAR, MAITEA EZINGO ZAITUT AHAZTU
BIHAR 100000 ZATITAN HAUTSIKO NAUZU
ETA HONDARRETATIK
HASI BEHARKO NAIZ BERRIZ

NAHIKO NUKE EGUZKIA
BIHAR ZUGAZ ITZALI,
ERLOJUEN ORRATZAK
GELDIARAZI,
MAITASUN ABESTI BAT
ZUGAZ IDATZI,

ETA BERRIZ BIHAR BERRIZ MOSU BATEKIN AGURTUKO NAUZU!!
ETA BERRIZ BIHAR BERRIZ 100000 ZATITAN HAUTSIKO NAUZU!!

BAINA GAUR MAITEA
ZUREKIN IHES EGIN,
TA NAHI DUT EGUZKIA
BIHAR ZUGAZ ITZALI,
ERLOJUEN ORRATZAK
ORAIN GELDIARAZI,
MAITASUN ABESTI BAT
BIHAR ZUGAZ IDATZI