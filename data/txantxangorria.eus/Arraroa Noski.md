---
id: tx-3382
izenburua: Arraroa Noski
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tQDH61Y7-lc
---

Totemo hen da, nihongo nashi ni Tokio
Arraroa noski, japoniera gabe Tokio
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Hou strange not being able to speak english in London
Arraroa noski ingelesa gabe Londres
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Lingua Vasconum
Lingua Navarrorum
Ze(in) arraroa euskararik gabe Nafarroa (berriz)

Mina nujal la ta keliu garabia Rabat
Arraroa noski, arabiera gabe Rabat
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Das ist doch klar, Ohne Deutsch lanfinix in Berlin
Arraroa noski, alemaniera gabe Berlin
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Lingua Vasconum...

Podumayak sinhala harama nokireme Lanka
Arraroa noski, sinhalarik gabe Lanka
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Ihala ralu zenu anbaguista nan Angola
Arraroa noski, anbaguista gabe Angola
Eta hau ere ez al da arraroa?
Euskararik gabe Nafarroa?

Lingua Vasconum...