---
id: tx-1856
izenburua: Boneta Ta Txapela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oNFZbAa2lvY
---

Iragan egun batez egin dugu irri,
hortaz nahi deizat eman zerbait berri:
Parisetik jin zaiku bidajez Phetiri,
urthe bakotx fortuna eginik uduri.

Agur, eta zertan hiz, Phetiri gaizua?
Ene haur denborako khantuko aixua!
Iragan duk orduko denbora gozua,
Eta hi aberastu, espehiz zozua!

Parisetik horra nuk zien ikhustera
Han untsa bizi gutuk, adixkide Pierra
Hiaurrek ikhusten duk egin diat aphera
txapela ezarri diat burutik behera.

Txapela baduk biena arhin barnekura
Nun utzi duk boneta uxkal-herrikua.
txapel horrek eztereik edertzen kaskela
Basta ederragatik, asto duk astua.

Eztiak ez axolik lai duen batere!
Han ezagutzen diat hanixko andere!
Itxurari so-ginez aberats bai ere
Eskunturen ahal nuk, ez erran deus ere.

Arren, baduk ideia hala eskuntzeko?
Lagun franko badutek hortan segitzeko.
Ofizio bat balit Parisen hartzeko
Aberats izan gabe, ahuer bizitzeko!