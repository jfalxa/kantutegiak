---
id: tx-2873
izenburua: Oihanian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BSFqq26rX5g
---

Oihanian zuiñen eijer
Txorriño bat khantari
Eskualdün arbolari
Zazpi probintzier!
Amodio dio gazter
Eskuarari bethi esker
Jarraiki aiten ürhatser
Ez sekülan bazter
Ez deia xarmagarri
Nuri ez dü placer
Txori bat khantan ari
Eskualdün aurhider?

Oi txoria! Hanitxetan
Zurekilan batian
Bozkarioz kantatzia
Nian desiretan.
Sarthü gira plazeretan
Egün plaza eder huntan
Nahiz bozkario ezari
Eskual bihotzetan.
Hots, anaie maitiak,
Bandera eskietan
Eskualdünak beikira
Nausi güzietan.