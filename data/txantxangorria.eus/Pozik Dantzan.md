---
id: tx-2904
izenburua: Pozik Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QnGyzRWtWCI
---

Landare eta animaliak
denak nirekin pozik dantzan
ez pentsa gero ametsa zenik
esna nengoen benetan

Txantxangorriak, sagu bihurriak
katuak eta margaritak
Ezker ta eskuin, mugi gerria
hau bai liluragarria!