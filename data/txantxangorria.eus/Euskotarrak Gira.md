---
id: tx-1505
izenburua: Euskotarrak Gira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o5fJjNPMcck
---

Euskotarrak gira, Euskadi da gure aberria,
Euskotarrak gira, askatu Ipar aldia.

Eskualdun izaitea, ez da hautatzen bozkatzez
Eskualdun izaitea, gure baitan dugu sortzez.
Euskotarrak...

Nork dauka eskubide, biziaren ixiltzeko,
Nork dauka eskubide, hil nahi dugun galdatzeko.
Euskotarrak...

Nor da ba aski Jainko, Euskara ezeztatzeko,
Nor da ba aski Jainko, bai batekin salbatzeko.
Euskotarrak...