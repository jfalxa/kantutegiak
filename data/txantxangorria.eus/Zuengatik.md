---
id: tx-1054
izenburua: Zuengatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XnlqJY211iU
---

Isiltasuna hautsi da 
doinu alaiak gauaren bueltan
su bat piztu da
izerdi ta ke artean
berotasunak erreaz
bidean bizitakoek
eraikitako harremanek pizten gaituzte

estribillo

Jaio ginen jaio bai, barrenak askatzeko
bizi gara bizi, zuen beroagatik
ileak puntan jartzen zaizkigun bitartean
jarraituko dugu zuen artean.
Animoak airean
Plazerrak igurtziz ezpainetan
Parranda usainak
Zoratzen gaituenean
Gogotsu antolatzaileak
Saltoka aurreko denak
Ilusioz sortutako irribarrea
Jaio ginen jaio bai...
Zuentzat gure eskerrik onena
Zuentzat gure bihotzeko taupadak
Jaio ginen jaio bai...