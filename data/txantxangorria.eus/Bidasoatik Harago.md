---
id: tx-236
izenburua: Bidasoatik Harago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_cUO9a8JYjs
---

Bidasoatik harago
Euskal Herri bat ere badago
Bihurtzen ari zaiguna
Geroz eta arrotzago
Mugak sortu zizkiguten
Gu sortu baino lehenago
Ta muga horiek haustea
Gure eskuetan dago

Izango gara besteen
Geroz eta menpekoago
Erdaraz mintzo bagara
Euskaraz baino gehiago
Ezin ahaztu nor garen
Eta nor ginen lehenago
Bidasoatik harago
Euskal Herri bat badago

Bidasoatik harago
Euskal Herri bat badago
Euritan dena ederra dena
Ta eguzkitan gehiago
Nahiz eta urteak joan
Nahiz eta mendeak igaro
Ez da gure nortasuna
Galdu dugunik esango

Elkartzen gaituna dugu
Askozaz ere nahiago
Dantza kantuekin
Gaudelako hurbilako
Nahiz jo hegoalderaino
Nahiz jo iparralderaino
Bidasoatik harago
Euskal Herri bat badago
InprimatuLagun bati bidaliPDF
 Aurreko letra
Hurrengo letra