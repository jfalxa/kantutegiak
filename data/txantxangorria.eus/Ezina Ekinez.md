---
id: tx-2336
izenburua: Ezina Ekinez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a6ljFT2OFNg
---

Ezina eginez. SD Eibarrek eta Sutagarrek egindako abestia. 
Musika eta hitzak: Aitor Gorosabel
Taldea: Sutagar
Grabaketa estudioa: Lorentzo Records
Bideoa: SD Eibar

Zaletasunaz harago denok gaude batuago,
ukabilek bat eginik ilargitik gertuago.
Dugun senari jarraituz, ametsak egi bihurtuz…
gure bidean ekinez…. eginez…
Munduan ezagunak dira eibartarrak…armaginak,
bihotzak pizten dituzten kolore gorri urdinak.
Xumetasuna hedatuz, aurkariak maiteminduz…
bere bidean ekinez….ezina eginez…

Eibar, Eibar gure ametsetan
Eibar, Eibar eztarrietan
Eibar,Eibar dugula zainetan
Eibar, Eibar gure bihotzian

Urteek jaso dituzte gorakada eta gainbeherak,
historia idatziko duten makina bat gertaera.
Haur, nagusi eta gazte, betirako jarraitzaile…
Zuen bidean ekinez…ezina eginez…

Eibar, Eibar gure ametsetan
Eibar, Eibar eztarrietan
Eibar,Eibar dugula zainetan
Eibar, Eibar gure bihotzian

Gure herriko auzo batean
zegoen mendi magalean
futbol zelai bat eraki zuten 
gerrako hondakinen gainean.
Alai, jator eta miresgarri, 
beti garaipenaren egarri
Ozen…garraxi egin Ipurua!! 

Eibar, Eibar gure ametsetan
Eibar, Eibar eztarrietan
Eibar,Eibar dugula zainetan
Eibar, Eibar gure bihotzian
beti bihotzian!