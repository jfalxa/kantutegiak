---
id: tx-3223
izenburua: Lot Gaiten -Bordagarai Anai Arrebak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GV_fndVJ8Pc
---

Lot gaiten lanari

denok elgarrekin

euskara bizi dadin

badugu zer egin.

Gure hizkuntza 

nahi zuten hil

hortan dudarik ez da

eskolan debekatzen zuten

euskaraz mintzatzea.

Lot gaiten lanari

denok elgarrekin

euskara bizi dadin

badugu zer egin.

Sinetsarazi daukute segur

euskarak ez duela

baliorik gure ahoetan

egungo egunean.

Lot gaiten lanari

denok elgarrekin

euskara bizi dadin

badugu zer egin.

Gauza guziak

ezin direla erran

gure mintzairan

hori sartua izan dute

bai gure gogoetan.

Lot gaiten lanari

denok elgarrekin

euskara bizi dadin

badugu zer egin.

Bainan holako

gaitzena ahal da

edozein hizkuntzetan

ideia baten erailteko

hitzak eskas direla.

Lot gaiten lanari

denok elgarrekin

euskara bizi dadin

badugu zer egin.

Eta agian egun batean

euskara izanen da

gure lan guzieri esker

bizi baita gurea.

Lot gaiten lanari

denok elgarrekin

euskararen bizia

gure eskutan da.

Lot gaiten lanari

denok elgarrekin

euskararen bizia

gure ahotan da.