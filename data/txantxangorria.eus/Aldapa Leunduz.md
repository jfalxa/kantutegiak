---
id: tx-1033
izenburua: Aldapa Leunduz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hx9F1IEA4W0
---

"Aldapa Leunduz" Asti Leku Ikastolak antolatutako Portugaleteko Ibilaldiaren abestiaren bideoklipa. Kherau taldea da abesti honen egilea, Alex Sardui, Zuriñe "Hesian" eta Vendettako Pello eta Ruben-en haizeen parte-hartzearekin.
"Aldapa Leunduz" videoclip de la canción del Ibilaldi de Portugalete organizado por la Ikastola Asti Leku. Canción hecha por el grupo Kherau con la colaboración de Alex Sardui, Zuriñe "Hesian" y los vientos de Pello y Rubén de Vendetta.

Aldapa Leunduz
Ibilaldia - Portugalete 2013

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
aurrean dugu euskararen zubia

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
berbarik berba mintzo ezkerraldea

Ai, ai, ai, ai, ai, ai, ai, ai, ai
aldapan gora, aldapan gora noa ni
tontorrera ere bai, baina...
eztauka zentzu bera
helburua da ibiltzea, ta ez heltzea, IBILI!!

(Hauxe da) ibilaldia
ez da inolako paseoa
Eztauka zentzu bera,
eztauka zentzu bera...
jarrillero-eroak
jaiotzetiko mendizaleak
(etor zaitezte) zatozte (e)ta gozatu gure ikastola:

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
aurrean dugu euskararen zubia

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
berbarik berba mintzo ezkerraldea

Ai, ai, ai, ai, ai, ai, ai, ai, ai
Zergatik esan "love"... "maitasuna" barik
Sentitzen dut, baina...
eztauka zentzu bera
maiatzen zaitudala
sentitzen dudane(a)n nik

Eta "amor mio", "je t'aime",
nahiz eta esan esan suabe edo ozen,
Eztauka zentzu bera,
eztauka zentzu bera...
ea maite zaitudan,
beraz, ez da galdera
nik maite dut euskera,
euskeraz dut maitatzen

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
aurrean dugu euskararen zubia

Aldapa leunduz Ibilaldia
itsasadarretik zerumugara
arraunean Porturako bidea
berbarik berba mintzo ezkerraldea

Iker Lope de Bergara: Alboka, koroak
Alex Garcia: Esku soinu txikia, Koroak
Aitor Esteban Etxebarria: Baxua
Gaizka Andollo: Kajoia, kabasa eta beste perkusioak
Ibon Ordóñez: Kitarra akustikoa, ahotsa

Kolaborazioak:

Alex Sardui (Gatibu): Ahotsa
Zuriñe (Hesian) : Ahotsa
Pello eta Rubén (Vendetta): tronboia eta tronpeta 

www.kherau.com / www.ibilaldia.com