---
id: tx-2920
izenburua: Bertsolari Txikia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1BkVUmHpoNg
---

Panpina txiki bat goxatzen dut
ilundutakoan beti.
Kozka, arramazka, ostiko, bultza
ez dut egiten maizegi.
Hizketan ere moldatzeko adina
kostata badut ikasi.

Mahaian bakarrik denetik jaten
jator egoten banaiz ni,
nor arraio da hemen "txiki"?

Harroxko jarri gabe maitia
entzun zaidazu adi adi.
Txikitzat berriz ez zaitut joko
zuk nahi duzuna izan bedi
handitasun bat opa nahi dizut
gorputz tamainaren gaindi
ea egun batez merezi duzun
inork esatea zugatik
Lazkao Txiki bezain handi.