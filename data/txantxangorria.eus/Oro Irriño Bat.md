---
id: tx-631
izenburua: Oro Irriño Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IUjRbfMDago
---

Nere urruneko gogoa hedatuz joango da,
ondura ixuriko da nere eskuetara,
zuri begiratuz besarkatuko dut,
duzun ugats xumea eta agian,
naizen amildegi hotza.

Zure bihotzaren taupada beroen
aurrean agertuko da,
kontaezinezko edertasunez.

Nere asmoaren gerizean maitatuko zaitut,
arrazoi sareen gaindi hegan eginaz,
ilunaren eta beroaren nahaspilean
argi berriez mundu bat sortuz,
nork daki…

Ai! oro irriño bat balitz azkenean,
eta uhinek bezala,
ispazterra ferekatuz, bukatu hiltzean