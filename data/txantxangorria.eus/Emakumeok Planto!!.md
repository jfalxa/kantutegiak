---
id: tx-2508
izenburua: Emakumeok Planto!!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Iw_q8UnpKYM
---

LA KORA. Bilboko koru feminista
 "La lega" abesti italiarratik, grebarako moldatu den testua: Madrilgo Coro Entredós.
Euskarara itzulita: Marienea



Martxoaren zortzian
egingo dugu greba
emakume guztiak
kalea da gurea!

Amama, ikasleak,
hetero, trans, bollerak, 
anitz emigranteak
grebara goaz denak.

Oli, oli, ola,
greba orokorra!
Emakumeak kalera!
Greba feminista da.

Oli, oli, ola,
greba orokorra!
Gustatzen ez zaiguna
aldatzera goaz.

Kaka bat einda dago
gure lur planeta,
gizonak aberastu,
guretzat zaborra.

Kontsumo basatia
ez da bidezkoa,
feminismoa dugu
aurrerabidea.

Oli, oli, ola,
greba orokorra!
Emakumeak kalera!
Greba feminista da.

Oli, oli, ola,
greba orokorra!
Gustatzen ez zaiguna
aldatzera goaz.

Lana topatu eta
behin behinekoa da,
etxetik urrun dago, 
erdia soldata.

Zortzian planto egin
es goaz lanera,
etxetik ere ospa,
gelditu mundua.

Oli, oli, ola,
greba orokorra!
Emakumeak kalera!
Greba feminista da.

Oli, oli, ola,
greba orokorra!
Gustatzen ez zaiguna
aldatzera goaz.

Bortxaketak, tratu txarrak,
gehiegikeriak,
irainak eta isekak,
zoazte pikutara.

Gure gorputzak geureak
hor gure hautua
bizirik nahi ditugu
Gora emakumeak!

Oli, oli, ola,
greba orokorra!
Emakumeak kalera!
Greba feminista da.

Oli, oli, ola,
greba orokorra!
Gustatzen ez zaiguna
aldatzera goaz.