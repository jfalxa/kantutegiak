---
id: tx-340
izenburua: Bbh
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IdTe0VtiV9Q
---

Zuzenean grabatutako soinua eta bideoa Almikako Eleizan TiroPun Ekoizpenaken eskutik. 

Zuzendaria: Iñigo Iriondo 
Bideoa eta Soinua: #TiroPunEkoizpenak -en eskutik nahastua #KostakoVibes estudioetan
Argiztapena eta Produkzioa: Cheaf Deaf eta Witxita.
Abestia: #Etxekalte #BBH
Ideia: Unai Otero

Gure Hizkeran Albumetik:

Hitzak:

Itsaso goitik bizi zen,
oinez larrua zapaltzen.
Berriz aurkitzen zituen
betiko begi haiek.

Airea arnastean agertzen joan ziren.
Zer zioten, zer zioten 
betiko begi haiek?

Heriotzaren alde
zeudela ematen zuen.

Horretako egunen batean,
ur handitan sartu zen…

Arnasa galtzen zuten bertan.
Betiko begi haiek itzaltzen hasi ziren.