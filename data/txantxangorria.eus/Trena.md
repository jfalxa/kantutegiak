---
id: tx-2249
izenburua: Trena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Do-aRvenz4c
---

Ñam, ñam, ñam, ñam bokata tripan
glu, glu, glu, glu, botila hustu
txan, txan, txan, txan trena martxan
txuku, txuku, txu, txu, txu.

Ñam, ñam, ñam, ñam bokata tripan
glu, glu, glu, glu, botila hustu
txan, txan, txan, txan trena martxan
txuku, txuku, txu, txu, txu.


Hasi aldapa poliki
jarraitu gero arinki
txangoan hamar geltoki
ikus hamaika toki.


Ziztuka goaz tututu
tunel beltz batean sartu
denok batera makurtu
lehiotikan agurtu.