---
id: tx-113
izenburua: Dantzatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PdjnZPVGOc0
---

UME DANTZARIAK :

Magali
Helene
Elaia
Malen
Telmo
Larraine
Joanes
Helene
Gari
Nahia

-----------------------------------------------------

Abestia : DANTZATU
Egileak : Ruben G. Mateos eta Yogurinha Borova



IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA



IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA



IREKI ZURE



BURUA, BEGIAK

ESKUAK, BIHOTZA

HAU HASIERA BAINO EZ DA



HARTU ZURE

ONDOAN

DAGOENAREN

ESKUA

GOAZEN DENOK DANTZATZERA!!



ALTXA ZURE ESKUAK

ZURE HANKAK AURRERA

JEITSI ZURE ESKUAK

ZURE HANKAK ATZERA

OLATU HANDIA

ALDE BATETIK BESTERA

BESARKADA HANDIA ETA GERO



IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA

IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA



IREKI ZURE



ZURE BARRUAN

DAGOEN MAITASUNA

ADISKIDANTZAREN BALIOA

ANIZTASUNA

ETA MAITASUNA

DANTZAN JARTZEN

GAITUEN ENERGIA



TXALOTU DENOK

GELDITU GABE

PIZTU GORPUTZA

ETEN GABE

ETA EGIN SALTO

NEKATU ARTE ETA

MUGITU DENOK ETA

MUGITU DENOK ETA

DANTZATU ….DANTZATU….DANTZATU ETA ETA

DANTZATU ….DANTZATU….DANTZATU ETA ETA

DANTZATU ….DANTZATU….DANTZATU ETA ETA





IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA



IREKI ZURE BIHOTZ HANDIA

ETA ALAIA

PIZTU GORPUTZA

ERREZA DA ETA

ENTZUN TAUPADAK

HASI MUGITZEN

DANTZAN SALTOKA

IREKI ZURE BIHOTZ HANDIA