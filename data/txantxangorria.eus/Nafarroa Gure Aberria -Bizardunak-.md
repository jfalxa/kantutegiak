---
id: tx-3172
izenburua: Nafarroa Gure Aberria -Bizardunak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ufSDJSTMidc
---

Eukaristia ondoren gudara noa,
Itzultzerakoan neska guztiak barrezka ari dira neri begira.
Benetako gizona guztiz mozkora.
.
Gu garena gara
gure Nafarroan,
berpiztuko da arima
Euskal Herri osoan.
.
Herrixkako plazan ikusi zintudan.
Lorategiko arroxarik ederrena.
Ezkondu berria haurdun zaudenez
…ardo botila ta berriz gudara.
.
Gu garena gara
gure Nafarroan,
berpiztuko da arima
Euskal Herri osoan.
.
Ohorez beteriko zelaietan.
Galdu ta jeiki egun berean.
Horixe bait da nafar historioa.
Ezinezko ametsa gure sustraia.
.
Gu garena gara
gure Nafarroan,
berpiztuko da arima
Euskal Herri osoan.
.
Gaztelaniarren olerki zikinak.
Ideiak finkatzen joan den mendean.
Aintzinan gudari, orain baita ere.
Ardo botila ta berriz mendira!
.
Gu garena gara
gure Nafarroan,
berpiztuko da arima
Euskal Herri osoan.