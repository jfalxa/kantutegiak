---
id: tx-837
izenburua: Tirauki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l9Xn2-QxOSg
---

Tirauki triki trauki
mailuaren hotsa,
Urkiolako puntan
fraile buru motza.

Dondingilin dangoa
hi ez haiz zoroa,
sardina buru baino
hobe duk oiloa.

Urra txuntxulun berde
mesmeru mokordo,
hortxe goian atso bat
kaka egiten dago.