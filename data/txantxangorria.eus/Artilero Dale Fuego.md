---
id: tx-3160
izenburua: Artilero Dale Fuego
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H3qTT5d8nSE
---

Artillero, dale fuego
ezkontzen zaigula
pastelero.

Eta zeñekin
eta norekin
prasku mozkorraren
alabakin.

Eta zeinekin
eta norekin
opo zikinaren
alabakin.