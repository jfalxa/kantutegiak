---
id: tx-3002
izenburua: Merezi Ote Gontzal Mendibil
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ro-LG9OUrM0
---

Etxetik ihes egiteko kalea zeharkatzea
mutiko batek bakarrik egin lezake.
Baina kaleak egunero ibiltzen dituen
gizon hori jadanik ez da mutiko bat,
eta ez doa etxetik ihes.

Badira udan eguzkia sartzen ari den plazak,
hutsik geratzen direneko arratsalde batzuk.
Eta landare alferrekoen etorbidetik
heldu berri den gizon hori gelditu egin da.

Merezi ote bakarrik egoteak?
geroan ere, bakarrik egoteko.

Kalerik kale bakarrik, plazak eta kaleak hutsik,
gelditzeko esan behar zaio andereren bati.
Eta norberarekin bizi dadila eskatu behar
zaio zeure buruarekin bestela mintzatuko zara.

Merezi ote bakarrik egoteak?
geroan ere bakarrik egoteko.

Zure besoetan, maiia, eguzkiaren irrinoa
mila bider handiago izanen da zazpi egunetako astean.

Loreak ugaluko dira nire etxeko leihatilan,
larrosaz ta klabelinaz apainduko dut baratza.

Horregatik batzutan gaueko mozkortia berriketan
hasi ohi da proiektuak adierazten.
Kalerik kale ibilkiaz ere etxea jadanik,
andere horrenean legoke, ta mereziko luke.

Merezi ote bakarrik egoteak?
geroan ere bakarrik egoteko.

Zu gabe, esperantzarik ez legoke.
zu gabe, ateak itxiko lirateke.
Zu gabe, ilunetan nengoke.
Zu gabe, ezin iraun daitek.
Zu gabe, ametsik ezin dut bete.
Zu gabe, ezin bizi naiteke.