---
id: tx-199
izenburua: Altzürüküko Zeinia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7Cyp8gjnp-o
---

Altzürüküko zeinia 

Agorrilan ütürriak dirade agortzen, odeiek eztirelakoz hurik ekarten, 
Gure begiak dirade ütürritzen, ezpeitirade sekülan agortüren. 
Agorrila egünak ala egün gaixtoak, gaitzerazten beitütü arauz eki beroak, Ekia, gorda itzak, oi, hire leinhürak, bizia beitü galdü gure ama hunak. 
Altzürüküko zeinia nigarrez ari da, 
Gure Ama, hon maitea, goiz hontan hil beita! 
Gaztetik hitzea, bai, ala hitze trixtea! Ekia, hire leinhürak ütxaltzen dik lilia, Ekarten düka, bai, hirekin bizia ‘ta biziarekilan orano hiltzea? 
Zeliak zabaltürik orai dütüt ikusten, aingüriak lehian oro dirade jarten, Aingürü bat goiti, harat da juraiten, gü trixterik girade, bai, heben baratzen. 

Hitzak eta müsika: herrikoak