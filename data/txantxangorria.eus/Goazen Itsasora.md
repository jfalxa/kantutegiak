---
id: tx-504
izenburua: Goazen Itsasora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7gjxb-sUt7g
---

Hitzak: Fernando Artola (Bordari)

Jeiki, jeiki, mariñelak!
      Guazen oro itxasora
      Uartzean euzki ederra
      Laister agertzera doa
      Goiz ñabarra, urdin-ubela
      gorri-gorrituko da.

      Goazen, goazen, itxastarrak!
      Dei egin du patroyak
      Atzar gaiten, jaiki bizi
      Utzi maindire ta oyak
      Itxasotik zeruraiño
      Egiteko otoyak.

      Ariñ, ariñ! Alkar biziz
      Gure indarrez treñera
      Ezkifaya ta ontziya
      Alkar ongi ibiltzen gera
      Jauna lagun, arrantzakin
      biurtuko kaiera.

      Kendu, kendu! Bidetikan
      Olatu eta auiñak
      aize-Iparrak utzi nai du
      Itxas bare berdiña
      Guk errex atzemateko
      Bear degun txardiña.

      Ernai, ernai! I, gaztetxo
      Argitu itzak begiak
      Urrunean bai aldiran
      Multsoan egaztiak
      Zantzun ori aski degu
      An izango arrraiak.

      Entzun, entzun, lemazaina!
      Urrunago jun gabe
      Txardiñ edo beste zerbait
      Emen bertan badaude
      argi-uera bizi-bizia
      Ona begien pare.

      Bete, bete, treñerua!
      Au dek arrai ballara!
      Txopa dena betea ta
      Gañezka antoparak
      Ontzikoak irriparrez
      Dabiltz arat'onara.