---
id: tx-2884
izenburua: Zergaitik Ez-Gurekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fn_UjZljIAY
---

ZERGAITIK EZ

Aska zazu, aska zaitez
zergatik ez
Esan zazu, esan baietz
zergatik ez. 

Sexua gozatzeko da (zergatik ez)
nahi duzunakin gustora (noski baietz)
lotu ahoa ahoan, ezpaina ezpain goxoan
ta mingaina mingainari (mugi hadi)
disfrutatzen da ugari!!

Aska zazu, aska zaitez
zergatik ez
Esan zazu, esan baietz
zergatik ez. 

Pentsamoldeak apurtu (ez beldurtu)
harrotasunez ikutu (ez beldurtu)
norekin berdin dio ta, nori behar dio inporta
hankartedun izatea (hain maitea)
ta maitatu hankartea!!

Neska mutil, neska neska (hau da festa)
mutil mutil, txarra ezta (txarra ezta)
Bizitzeko bizi gera, goazen denok gozatzera 
ez da bakarra aukera (da galdera)
sexu baten arabera!!

GUREKIN

Jaia irria ta poza 
badaukagu nola goza 
tristuraz dugu hala ere 
nahigabe, bihotza. 

Herriko seme direnak 
herritik urrun daudenak 
nahi ditugu elkarrekin 
gurekin, denak. 

Zuk barrutik nik kanpotik tira 
herria indartzen ari da 
lehenballen ekarri behar dira 
presoak, Euskadira. 

Dispertsioa kastigu 
gogorrak badakizkigu 
zein diren eta nolatan 
zeldatan, gatibu. 

Aizu mutil, aizu neska 
aldarrikatzeko presta 
pare parean dago ta, 
borroka ta festa. 

Zuk barrutik nik kanpotik tira 
herria indartzen ari da 
henbaileben ekarri behar dira 
presoak, Euskadira. 

Zuk barrutik nik kanpotik tira...