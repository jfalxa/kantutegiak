---
id: tx-442
izenburua: Mendian Gora Aritza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xF8ygwanw7E
---

Mendian gora haritza,
Ahuntzak aizean dabiltza,
Itsasoaren arimak dakar ur gainean bitxa.
Kantatu nahi dut bizitza,
Usteltzen ez ba zait hitza,
Mundua dantzan jarriko nuke
Jainkoa banitza.
Kantatu nahi dut bizitza,
Usteltzen ez ba zait hitza,
Mundua dantzan jarriko nuke
Jainkoa banitza.
Euskal Herriko tristura,
Soineko beltzen joskura,
Txori negartiz bete da eta
Humorez ustu da.
Hemaidazue freskura,
Ura eskutik eskura,
Izarren salda urdina edanda
Bizi nahiz gustora.
Hemaidazue freskura,
Ura eskutik eskura,
Izarren salda urdina edanda
Bizi nahiz gustora.
Euskal Herriko poeta,
Kanpo santuko tronpeta,
Hil kanpaiari tiraka eta hutsari kolpeka.
Argitu ezak kopeta,
Penak euretzat gordeta,
Goizero sortuz bizitza ere hortxe zegok eta!
Argitu ezak kopeta,
Penak euretzat gordeta,
Goizero sortuz bizitza ere hortxe zegok eta!
Mundua ez da beti jai,
Inoiz tristea ere bai,
Baina ba dira mila motibo kantatzeko alai.
Bestela datozen penai
Ez diet surik bota nahi,
Ni hiltzen naizen gauean beintzat eizue lo lasai,
Bestela datozen penai ez diet surik bota nahi,
Ni hiltzen naizen gauean beintzat eizue lo lasai.