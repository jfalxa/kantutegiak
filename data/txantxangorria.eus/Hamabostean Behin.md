---
id: tx-1834
izenburua: Hamabostean Behin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ylV21DhTWTY
---

Etxetik gutxitan irten arren
Jon Trollope hamabostean behin
abiatzen da labanderiara.
Etxesail pare bat zeharkatu ditu
ta hango kaleak beltzagoak dira
eta disdiratsuagoak.

Posta zentraletik ezkerretara jo
denda greziarrak, billarra eta hospitalea,
hori hospitalea.
Usai exotikoak tabernetatik
eskinatik iragaitean konturatu da
inork bgiratu ere ez diola
inork begiratu ez diola
inork begiratu ez.

Txanponik bai? ugazabari galdegin
diru zikinik gabe
nora joango ginduzkean ba?
horra erantzuna.
Labadora martxan ezarri
errebistarik ekarri ez duenez
leihotik begira zai.

Espaloi lerroari jarraitzen dio
etxalderako bidea buruz ikasia
hamabostean behin.
Etxeko egongela du buruan
bazter oro ditu ezagun eta hura
beti gogoan ageri zaion parajea
beti ageri parajea
beti parajea.

Egongela hartan zenbat alditan
gordeko du Jon Trollope gureak
bere betiko paseoaren oroimina
paseoaren oroimina
oroimina.