---
id: tx-2384
izenburua: Gure Ahotsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZBXoTZm5uac
---

Josu Bergara musikariaren "Asunak" disko berriaren bigarren bideoklipa.

Ahotsa, gitarra eta musika: Josu Bergara
Biolina eta konposizioa: Nerea Alberdi
Pianoa: Mikel Nuñez
Gitarra: Lüi Young
Kontrabaxua:  Dani Gonzalez
Bateria: Txema Arana
Bideogileak: Gaizka Peñafiel eta Asier Olazar
Aktorea: Ion Martinez Esnaola "Txiki"
Makillajea: Arritxu Eizmendi 

Esker bereziak:
Amurrio Antzokia
Gorka Chamorro
Ane Izquierdo

GURE AHOTSA (Letra: Josu Bergara)

Gure ahotsa, 
isildu nahi duen burokratak, 
paper artean ezkutatu du, 
zure eskaera. 

Gure ahotsa, 
kontrolatu nahi duen poliziak, 
atzo gauean irakurri zuen,
 twitterren zure iritzia. 

Gure ahotsa, 
kondenatu nahi duen epaileak, 
ez daki udaberria, 
geldiezina dela. 

Gure ahotsa, 
debekatu nahi duen diputatuak, 
ez daki herri oso bat, 
aurrean duela. 

Gure ahotsa, 
merke saldu nahi duen bankeroak, 
epe luzerako hipotekatu du, 
bere bihotza. 

Gure ahotsa, 
ezabatu nahi duten panfletoan, 
gaur berriz argitaratuko dute, 
azalean gezurra. 

****************************************** 
Diskoa deskargatu: www.josubergara.eus 
Kontratazioa: info@taupaka.eus (+34) 688 840 713 
CD-a erosi: info@josubergara.eus 
www.josubergara.eus
www.taupaka.eus