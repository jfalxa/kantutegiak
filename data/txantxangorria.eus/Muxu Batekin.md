---
id: tx-1310
izenburua: Muxu Batekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O2YptB7TiD0
---

laurobaren "egunarekin" diskoko "muxu batekin" abestiaren bideoklipa.
zuzendaria: Xabi Zabaleta
errealizadorea: Alvaro Manzano


Behin, oraina dela
Etorkizunaren lehenaldia


Esan zenidan
Baina han da zaila
Dena apurtzea, muxu batekin

Agurtzea,
Amaiera batekin
Hasiera bat dator
Baina ez zaude hor
Ondoan nirekin
Gure argazki guztiak
Putzu baten bustiak
Bidean zehar
Atzean utzi behar
Dena amaitu da muxu batekin

Amaiera batekin
Hasiera bat dator
Baina ez zaude hor
Ondoan nirekin
Gure argazki guztiak
Putzu baten bustiak
Bidean zehar
Atzean utzi behar
Dena amaitu da muxu batekin

Muxu bakar batekin

Baina han da zaila
Dena amaitu da muxu batekin