---
id: tx-2262
izenburua: Izarrezko Gauak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Sc0eXCkEvKo
---

Izarrezko gauak nazkatzen zidaten inoiz, 
izarren argiak, bularen izpiak, bikoitz... 
Bide okerretik zuzen joan naiz ni bakar, 
bide oker hartan zeuden makina bat arar... 
Mila miloi urtez zer gerta gaitu?... Ezer!! 
Mila miloi urtez zer izango garen?... Ezer!! 
Hor gelditzen da ba, airean bezala, zutaz!! 
Hor gelditzen da ba, hitz batzuk bezala, akats!! 
Ikusi ez arren sentitzen bat badek joka, 
ikusi ez arren zarata garela topoka, 
amaren besoetan gogoratze bat izan badut, 
amaren besoetan lo hartze bat izan badut... 
Izarrezko gauak suaren ondoan dira,
 izarrezko gauak Irailaren alboan ziran. 
Axola al zaizu nitaz zer izan zen lehen? 
Axola al zaizu gutaz zer izan zen hemen?
 Izarrezko gauak nazkatzen zidaten inoiz, 
izarren argiak, bularen izpiak, bikoitz...