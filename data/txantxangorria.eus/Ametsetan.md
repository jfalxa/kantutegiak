---
id: tx-1253
izenburua: Ametsetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nmxipBzEL3I
---

En Tol Sarmiento (ETS) taldearen 'Ametsetan' bideoklipa, bere azken zuzeneko albumetik ('Ametsetan', Baga-Biga, 2017). Bideokliparen egileak: Artifikzio.

Videoclip 'Ametsetan' del grupo En Tol Sarmiento (ETS), incluido en su último álbum en directo ('Ametsetan', Baga-Biga, 2017). Realizadores del videoclip: Artifikzio.

Jada salgai/ Ya a la venta/ Out now:

Jarraitu gaitzazu! /¡Síguenos!/Follow us!


Bazter txiki bateko sustraiak, erosotasunaren etsaiak
Millaka kilometro amets berri baten bila,
Hartu zure lekua, berezion munduan


ADVERTISING



Iragana uzteko albo batera,
Iñurrien antzera pauso txikiz aurrera, amestera...
Ametsetan ametsetan hamaika urte pasa dira,
Ametsetan ametsetan galdu genuen beldurra.
Ametsetan ametsetan zure mundutik gurera,
Ametsetan ametsetan ilargirako bidean
Bizitzeko garaia orain da,
Arnasteko unea iritsi da,
Baldakizu zein den zure ametsik sakonena?
Hartu zure lekua, berezion munduan
Iragana uzteko albo batera,
Iñurrien antzera pauso txikiz aurrera, amestera...
Ametsetan ametsetan hamaika urte pasa dira,
Ametsetan ametsetan galdu genuen beldurra!
Ametsetan ametsetan zure mundutik gurera,
Ametsetan ametsetan ilargirako bidean!
Ametsetan ametsetan, hamaika urte pasa dira
Ametsetan, ametsetan, ametsetan...
Ametsetan ametsetan zure mundutik gurera,
Ametsetan, ametsetan, ametsetan...
Ametsetan ametsetan hamaika urte pasa dira,
Ametsetan ametsetan galdu genuen beldurra!
Ametsetan ametsetan zure mundutik gurera,
Ametsetan ametsetan ilargirako bidean!