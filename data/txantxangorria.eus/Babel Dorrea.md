---
id: tx-2842
izenburua: Babel Dorrea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gA7LyM4oa_w
---

Zer pentsatzen ari ote den gaixo hori?
jadanik zorabiatuta zegoen
batek zioen bezala
J.C. entzuten
bere anaia txundituta elurrarekin
big one
headache
guten morgen!
teilatuak elurrez estalirik

Ua! Ua! Egiten zuen anbulantzia horrek
Ua! Ua! Egiten zuen anbulantzia horrek

Relax egin ondoren, Madeleine agurtzen da
eta gainera ezin erre hemen!
arazo bat amaitu dela uste dut
auskalo orain zer gertatuko ote den
teilatuak elurrez estalirik
esne eta txokolate herrialdean