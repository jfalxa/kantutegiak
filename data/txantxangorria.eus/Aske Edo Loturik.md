---
id: tx-1376
izenburua: Aske Edo Loturik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vUfXKLT17FQ
---

Bihotzez mila esker bideoklipean parte hartu duten Irati Goroskieta Abinzano, Elur Olabide Izquierdo, Oier Markotegi Zubillaga eta Nagore Ferrando Arakistaini.
LETRA:
Amets egitea oraingoz zilegi da
Begiak irekitzean dena lehen bezala bada.
Baina aspertu gara gauero amesteaz
Ametsak egi bihurtzea da benetan nahi duguna.

Itzalpean bizi, kateatuta hazi
Ezarritako legeei ezin egin ihesi.
Aske edo loturik, nola nahi duzu bizi?
Askatasunerako bidean, etorkizuna erabaki.

Iritsiko da egun bat, non kalera irtengo garen
Munduari gure irrifar handiena erakusten
Izan nahi dugun hori, gure kabuz aukeratzea
Gure eskubidea!

Erabakiz eraiki. 
Eraikiz eraldatu.
Erabaki, geu izateko! 
Erabaki, aske izateko!
DESKARGATU "GURE ORDUA" DISKOA: