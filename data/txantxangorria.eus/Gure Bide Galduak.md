---
id: tx-1746
izenburua: Gure Bide Galduak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uaXqCAfPsnI
---

Gu gazteok gaur gabiltza
mundu galdu honetan,
gerra bat barruan degula
zer egin jakin gabe. 

Gurasoen mundu aldreves hau 
ez dugu onartzen,
diruaren morroi izaten
erakutsi digute. 

Ilunpe honek
bakardade galduan utzi gaitu;
Argirik gabeko bideak
etorkizunik ez du. 

Bizimodu lasai bategatik
idealak galdu.
Bizitzaren gauza ederrenak
dizkigute zapaldu. 

Gizonak kartzelan daude
beren herriagatik.
Jainkoak beharko du lagundu
guk ahaztu baditugu. 

Maitasun hitzak nahi nituzke esan
baina gaur ezin dut;
Gure bide galduak
kalte hau ekarri du.