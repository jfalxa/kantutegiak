---
id: tx-2964
izenburua: Haizea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b6VF1seHRCI
---

Koiuntura talde naparraren kantu bat, beraien "Askatasun haizea" diskotik. 1992. urtean kaleratu zuten

Josu Castillo, ahotsa ta kitarra 
Resu Ansorena, ahotsa ta kitarra 
Laura de Miguel, flauta 
Simón, perkusioa

Koiuntura musika taldea 90. hamarkada hasieran sortu zen Nafarroa aldean. 1990. urtetik 1996. urterarte ibili ziren, buru belarri, Euskal Herria osoan zehar heuren folk estiloko kantuak jotzen. Letra aldarrikatzaileak eta poetikoak nahasten zituzten. Intsumiso gazteen kontrako gartzela zigorren edota Itoiz urtegiak sortutatuko gatazka ekologistaren islada bilakatu ziren. Garai bateko kronika dira heuren diskolanak.

1996. urterarte, Koiunturako kideak lau disko dotore kaleratu zituzten, "Dias turbios", "Askatasunaren haizea", "Eguzki bergiratuz" y "Itoiz bien vale un cable". Tzoku taldea, Zerdeinia (Italia) uharteko talde rockero batek, sardo hizkuntzan bertsionatu ziuen Koiuntura taldearen kantuak. 



Ha llegado una carta a la carcel, 
una poesia que dá alegria y calor,
pregonando noticias del exterior, 
pregonando que una vida comenzo.
El preso va loco buscando la carta, 
la carta le dice que ya es papa,
le lloran los ojos, 
le llora el corazón aprieta la carta 
con todo su amor.
Haizea se llamara,
Askatasunaren Haizea.
Cuando el hijo sea mayor ,
el padre preso le contara, 
como un día expoliaron su tierra
los carceleros de la Paz.
Como un día hasta le prohibieron
el sueño la Idea y el amor.
Como un día encerraron su vida
por amar la Libertad!!