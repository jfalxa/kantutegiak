---
id: tx-2862
izenburua: Berunezko Euri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VZbUCPxG-RE
---

Gure ibileran berunezko euri tantak
erotzen zaikigu konzientzi erdian
hiri-espaloiak zapaltzen gaizki hezita
sexutan dabiltzan haurren antzera
telebistan inspiraturik errenka gabiltza denok
gorotzezko irakaslea
geroz eta urrutiago daude
izarrak ametsetarako.
Neska, uki nazazu espainez!
hartu zuretzat saria
ahatik behera ahari gera
belarrietan dut garia
zizareen moduak lurpetik mugituaz
ur ertzean eserita...
zeru sargori azpian beldurtu ginen
Zure begietatik isurtzen ari da
motela, esperantzaren ale bat
galerazitako esaldia ezpainetaik
ihesi izango zaigu alferrik
sedukzioaren jokoan parte hartzeko paperak
salgai dauzkat aspalditik
agurrera kondenaturik
rutinen esku bahiturik!!

Sorotan Bele talde arrakastatsuaren kanta bat, beraien hirugarren diskotik aterata, "Jonen kezkak" hain zuzen ere. Diskoa hau Hirusta Records zigiluak kaleratu zuen 1996. urtean, honako lagunekin...

Ixiar Amiano, teklatuak
Aitor Etxaniz, bateria
Urbil Artola, gitarra, ahotsa
Mikel Errazkin, teklatuak
Mikel Izulain, biolina
Ritxi Salaberria, baxua
Gorka Sarriegi, ahotsa ta gitarra