---
id: tx-2967
izenburua: Itzulera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b8Ru4brhAdw
---

Banator ene Euskadi
zuregana berriz
mendi eta lekuak
urrundik nabariz, 
ez dakit zorionik
nigan gerta dadin
ene sentimendua
sufrituz da egi. 
Lanbropetik datorkit
ezinesan hori
izan nintzen haur harek
bihotza hunkitzen dit, 
mila gauza ahaztuek
berriz diote «bai»
han eta hemen agertuz, 
han, hemen eta orain. 

Hemen, bai, batez ere
mugagabe ttiki
galtzerik ez duen
zehaztasun bizi, 
jostailuzko lurbira
berde zoriongarri
baserrien kokapen, 
ortzearen hutseski. 

Berriz aurkitzen zaitut
lehengoan Euskadi
izkutuko munduak
zuregana ibiliz, 
beti goaz euskaldunok
etxetik urruti
zure bake haundira
berriro itzuliz. 

Euskadi, ene barne, 
ama jaioterri
neketan datorrena
hartzen duena beti
eta hor zaude hain sendo
zure baitan berri
hain bero eta hain sakon, 
atseden jauregi. 

Nire Euskadi ttikia, 
ene herri, 
solasik behar ez duen
adierazpen garbi
zugan ene iragana: 
zahar nauzu, ibiltari
zauriturik natorkizu
hiltzera, Euskadi.