---
id: tx-2643
izenburua: Karen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4Xbr1nuVJNM
---

Audioaren nahasketa eta masterra: Ruben G. Mateos.
Bideoaren grabaketa eta edizioa: Taom.
Hitzak: Jon Arano.
Musika: Inun.
Koroak: Bihotz Gorospe.

Zu zinen zaurie
eta hil omen zare,
betirako hil ere,
eternidade guztirako,
O, Karen.

Gabonetako putta dultzea,
Jesusen bihotz gozoa:
salba ezank!Eraman nazan hirekin
heriotzaren orenen.

O, Karen.
O, Karen.

Deabruaren,
ogiaren, egiaren,
Eguberrietako
xolomon dultzearen,

Zu zara zaurie,
nigan bizi zare.

O, Karen.
O, Karen.