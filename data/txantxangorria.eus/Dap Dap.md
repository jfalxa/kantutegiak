---
id: tx-116
izenburua: Dap Dap
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jD9O1HTtZrM
---

Jokin Pinatxo - Garazi Esnaola - Ane Bastida - Julen Suarez -  Andoni Arriola - Mattin Arbelaitz - Javi Echarri

Letra:

Goizero irrifarra
Ahoan
Nekea ta negarra
Alboan

Egunak mendea
Dirudi ordea
Zoriona kartzela
Bihurtuz horrela
Hautatu bidea
Badago aldea
Erori edo bestela

Ahaztu eta dap dap
Elkarrekin dap dap
Airean goaz
Zuk nirekin dap dap
Nik zurekin dap dap
Kolosala izango da

Guztiek erantzuna
Ahoan
Baina nire itzala
Alboan

Egunak mendea
Dirudi ordea
Zoriona kartzela
Bihurtuz horrela
Hautatu bidea
Badago aldea
Erori edo bestela

Ahaztu eta dap dap
Elkarrekin dap dap
Airean goaz
Zuk nirekin dap dap
Nik zurekin dap dap
Kolosala izango da

Ahaztu eta dap dap
Elkarrekin dap dap
Airean goaz
Zuk nirekin dap dap
Nik zurekin dap dap
Kolosala izango da

Bidea hasi da
Zulotik argira
Batera goaz
Goruntza begira
Eutsi eta tira
Erori bestela

Ahaztu eta dap dap
Elkarrekin dap dap
Airean goaz
Zuk nirekin dap dap
Nik zurekin dap dap
Kolosala izango da