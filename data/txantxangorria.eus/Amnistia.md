---
id: tx-2452
izenburua: Amnistia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9nkrsnJqfGU
---

eraman zintuzten, silueta ilun arrotzak
gurasoak negarrez, ta ohea utsik utzita
furgonetan sartu, ta kalean oihuka
Madrilerako bidea, hasi zen infernua!

Auzitegi nazionalan, inkomunikazioa
kolpeak eta irainak,
 4 egun torturatuta
behin papera sinatuta
 sartu zuten kartzelara
beste seme bat bahituta
zuek bai terroristak!


Eramanan zintuzten, herritik oso urrun
sakabanaketa, familia zigortuz
salbuespen neurriak apurtuko ditugu
etxean ta bizirik, behar zaituztegu!!

zuk barrutik tiraka, guk kanpotik lagun
espetxetako hormak, hautsiko ditugu
garaipenaren bidean, amestutako eguna
EUSKAL PRESOAK ETXEAN, BIZIRIK ETA ORAIN!


Amnistia lortu harte 
jarraituko dugu jotake
denak ala inor ez
dena ala ezer ez