---
id: tx-2885
izenburua: Ez Dot Sinisten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NyrwwtLKT6g
---

Ezin deu ixen
ez da egixe
eroritte gagozanik
ez dot sinisten.

Altseu aurrera
jausten garen bakotzien
pentsu,geroan
esperantza bizi dala.

Esperantzan bizi naz
desesperatute
esperantza da baina
zutik mantentzen nauena.

Altsu eurrera
jausten zaren bakotzien
pentseu,geroan
esperantza bizi dala.

Ezin deu ixen
ez da egixe
eroritte gagozanik
ez dot sinisten.

Altseu...