---
id: tx-2833
izenburua: Zutik Emakumeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rB-9wzayCq8
---

Zutik, emakumeak
Hautsi gure kateak
Zutik, zutik, zutik

Historia gabe, emazte
Denetan ahantziak
Mende guzietan, emazte
mundutik baztertuak

Zutik emakumeak
Hautsi gure kateak
Zutik, zutik, zutik

Apaldu eta zapalduak
Saldu ta bortxatuak
Etxe guzietan, emazte
Mundutik baztertuak
 
Zutik, emakumeak
Hautsi gure kateak
Zutik, zutik, zutik
 
Bakardadean, emazte
Elgar ezagutu gabe
Berexi gaituzte, emazte
Ahizpak izanik ere
 
Zutik, emakumeak
Hautsi gure kateak
Zutik, zutik, zutik
 
Emazteen haserre oihua
Munduan entzunen da
Indarra dugu emazte
Milaka bil gaitezen
 
Zutik, emakumeak
Hautsi gure kateak
Zutik, zutik, zutik

Elgar ezagutuz, emazte
Elgarri so ta mintzo
Bil gaitezen denak, emazte
Batera oldar gaiten
 
Zutik, emakumeak
Hautsi gure kateak
Zutik, zutik, zutik
 
Elkarrekin emakume
Zapalketa garaitu
Egun guziz armetan gaude
Gora gure iraultza
 
Ez gira esklaboak
Ez dugu kadenarik
Dantza dezagun dantza.