---
id: tx-1518
izenburua: Behar Zaitut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2L3d0O-QH2o
---

Nire zilborretik altxatu ditut begiak, gaur konturatu naiz ze min egiten duen egiak
Ispilu faltsu hontatik kendu dut begirada, ez zait batere gustatzen orain ikusten dudana

Nire zilborretik altxatu ditut begiak, gaur konturatu naiz ze min egiten duen egiak
Ispilu faltsu hontatik kendu dut begirada, ez zait batere gustatzen orain ikusten dudana

Ez zen iguala ere barkamena eskatzea, behar dugunaren bila aldiz ezagutzea
Horrekin zerbait lortuko duela jakinez gero, nire zainak moztuko nituzke zure mina baretzeko

TA GAUR, ONDOAN BEHAR ZAITUT BERRIRO, EZ NAIZ NIRE BIZITZAREN JABE
ZAILA, EGITEN ZAIT AURRERA EGITEA, NEGAR BATEAN ERORI GABE

Nire zilborretik altxatu ditut begiak, gaur konturatu naiz ze min egiten duen egiak
Ispilu faltsu hontatik kendu dut begirada, ez zait batere gustatzen orain ikusten dudana

Ez da gezurra izan sentitzen dudan guztia, ezta zugatik emango nuela nire bizia 
Jakin ezazu inoz gehiago ez dizut hutsik egingo, ta nire barruan izango zarela betiko

TA GAUR, ONDOAN BEHAR ZAITUT BERRIRO, EZ NAIZ NIRE BIZITZAREN JABE
ZAILA, EGITEN ZAIT AURRERA EGITEA, NEGAR BATEAN ERORI GABE

TA GAUR, ONDOAN BEHAR ZAITUT BERRIRO, EZ NAIZ NIRE BIZITZAREN JABE
ZAILA, EGITEN ZAIT AURRERA EGITEA, NEGAR BATEAN ERORI GABE