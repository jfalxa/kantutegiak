---
id: tx-1105
izenburua: Izan Gira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XMPrCcpuAgA
---

Eskual rock&rolling

Izan gira Euskal Herrin
rock&rollin

Gaüa heldü denin
adiskidiekin
juiten g(ar)ela bestaka...

Bal-en urhentzin
ene maitea
eneki jin zite "au coucou des bois"

Zü eta ni
biak goxoki.
dantzan dantzan rock&rolling