---
id: tx-349
izenburua: Oreinak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Gd0WcFagxbE
---

Bonbereneak aurkezten du: "Oreinak", Anari eta bere bandak 2015eko abenduaren 13an Bonbereneako pabilioian "Zure aurrekari penalak" diska aurkezteko eskeini zuten kontzertuan grabatutako kanta.

Behin izan zinenaren eta izango zarenaren
Artean begira duzun testigu bakarra zeu zara
Isilean galdu dituzun gerra guzti horiek denak
Eta zugan oraindik
Ikusi nahi ez duzun hori dena
Ihes egiten diozun
Eta jarraitzen zaituen
Horren artean bizi zara
Bide ertzeko seinaleetan bizi diren orein horien antzera
Beraien bizitzatik
Behingoz salto egitera
Ausartzen ez direla
Eta denborarekin
Oraindik ez hura
Bihurtu zen honezkero ez hartutako erabaki
Gabeko erabakiz
Isilean desagertzen diren
Espezie horien antzera
Itzaltzen ari zara
Bera korrika
Ta zu berriz oinez
Minik egiten ez duten zauri horiek marraztu dizkizuten parentesien artean bizi zara
Mapa zahar bat zara
Eta zure basoetan barna oreinak ihes batetik bestera
Kotxeko argien artean
Eta zuk zeure burua behin
Nola harrapatu zenuen
Oinaze hura ospatzen
Orgasmoei darraien
Tristura pizti horren
Zati bat baino ez bada ere
Egun hartan zerbait
Ulertu zenuen