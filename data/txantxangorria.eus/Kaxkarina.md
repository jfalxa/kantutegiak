---
id: tx-2859
izenburua: Kaxkarina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7vy4zbsuNm0
---

Nabil lorez lore
dastatuz bere usain goxoa
bainan une batez
inguruan euli multzoa

Gustoko ditut nik
jantzi arin koloretsuak
kalean ibiliz
entzuten ditudan txustuak.

Esan haiei
ez naizela neska erreza
esan haiei
nik dut lagunen aukera
esan haiei
edozeinek ez du baimena
esan haiei
pikuak onak daudela
esan haiei
inozoa ez naizela.

Gustora sentzitzen naiz
denen erdian
perejila bezala
saltsa guztietan.