---
id: tx-1853
izenburua: Ezekiel Esnatzen I
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qB8f7lwCXS8
---

Egunon!
Eguberria, egunon!
Agur gaua
Egunon!
Itsasoa eta mendiak
Haize... egunon!
egunaren argia etor
begietara
gizonaren urratsak
zuzendu dezala