---
id: tx-1608
izenburua: Olentzero Travellin' Brothers Big Band
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LeG2iMARAVA
---

Olentzero joan zaigu
mendira lanera
intentzioarekin
ikatz egitera
Aditu duenenan
Jesus jaio dela
lasterka etorriko da
Berri ona ematera.

Horra, horra
gure Olentzero
pipa hortzean duela
eserita dago.
Kapoiak ere baitu
arrautzatxoekin
bihar meriendatzeko
botila ardoekin.

Olentzero, buru handia
entendimentuz jantzia,
bart arratsean
edan omen du
bost arroako zahagia
Bai urde tripa handia!