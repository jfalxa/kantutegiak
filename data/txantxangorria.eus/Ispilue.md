---
id: tx-1988
izenburua: Ispilue
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l1rgPXNuE4o
---

Oso ispilu eder polit bat
didate aurrera ekarri
eman duenak jakingo zuen
mutilzar zar onen berri.
Gazte dan arte au izaten da
gazte guztien pozgarri
zartutakoan ez da beiratzen
au gaztetan bezin sarri.

Aizak: nik iri bota bear dit
bertso koxkor bat edo bi
beingoan jarri geran ezkero
biok arpegiz arpegi
Neri begira ortik daduzkak
alperrikako bi begi
ik ez nauk noski ni ikusiko
bañan nik ikusten aut i,

Neri begira jarrita, motell,
zertarako ago onela?
Ta pentsatzen dit
aspalditxotik
ezagututzen autela.
Mutilzarraren moko orrekin
ez dek ematen motela:
azal-zimurtzen ari aiz motell
Lazkao Txiki bezela.