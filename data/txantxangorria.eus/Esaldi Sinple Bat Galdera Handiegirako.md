---
id: tx-1971
izenburua: Esaldi Sinple Bat Galdera Handiegirako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZO2r6JWi1-c
---

Esaldi sinple batekin hasiko naiz, kafea erre da baina. egun onak eman dizkidazu, hotz egiten du gaur etxean. beste era batetan hasiko naiz, goizero fragantzi ta formak zure ilean, gauza ederra da kaosa gauza txikitan. zer da zure ilusioez gaurkoan? galdera handiegia agian gauza sinple baterako. etxea momentu zoriontsuez bete dugu baina batzutan korapilo bat eztarrian, lasai elkarrekin gaude eta.