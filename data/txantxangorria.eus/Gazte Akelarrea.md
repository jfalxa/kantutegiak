---
id: tx-2699
izenburua: Gazte Akelarrea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2eZFPVfh4n0
---

Congo Square

Galeraz galera idatziz historia
Congo, New Orleans, Maule, Haiti
El Aaiun, Herrera, Kobanê, Idomeni
konspirazio baten mila aurpegi.

Diru ta kataiaz neskame izanak,
lantegian bizi, lanean hil.
Baina memoriak kantu ta dantzetan,
iraultza pizten digu zainetan.

Ongi etorri esklabuon festara,
Behelainotan alperrik da maskara.
Askatasun danborren lurrikara,
Erre gintuen su berbera gara.

Oinak lurra harrotuz, gogoa izarretan,
Txinpartak geroko garren zutarri.
Gorputz ahaztu ta aske guztion gordeleku
Akelarrea dugu.

Hemen ez da magia beltzik,
sokak apurtzeko sua baizik.
Inguratuz mantenduko dugu bizirik.

Ongi etorri esklabuon festara,
Behelainotan alperrik da maskara.
Askatasun danborren lurrikara,
Erre gintuen su berbera gara.

Ongi etorri esklabuon festara,
Behelainotan alperrik da maskara.
Behin hil ginen, baina itzuli gara
Erre gintuen su berbera gara.

SUGARREN DANTZA INDEPENDENTZIARA