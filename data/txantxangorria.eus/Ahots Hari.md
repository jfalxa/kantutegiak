---
id: tx-400
izenburua: Ahots Hari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JUBmRS72hCg
---

🍃 Hitzak/letra | Alaia Martin
🍃 Abestia/canción | Olatz Salvador ft. María Rozalén
🍃 Ekoizpena/producción | Pablo Novoa
🍃 Musikariak/Músicos | Ander Zulaika. Jagoba Salvador. Mattin Saldias. Pablo Novoa
🍃 Grabación y mezcla | Haritz Harreguy (Euskal Herria). Sur Club Estudios (Madrid)
🍃 Master | Ultramarinos Studio
 🍃 Videoclip | Singrima Films



Gaur martxoak hemezortzi
Eta portuak bi barku
Noiz erori da sabaia
Eta nola ez naiz ohartu?
Bizitzak eraman zaitu
Zeneraman lekuraino
Zerua ez dago argi
Zerua ez dago laino
Aho uhalak nahi eta
Hain zabiltza aho ahul
Aho alokatu horrek
Egin zaitu ahots ahul
Eguzkiari uluka
Uluka hotz eta ahul...
Noiztik diozu hain ezkor
Guztia posible dela?
Noiztik duzu eztarrian
Itsasoaren sabela?
Iraganeko esaldiz
Geroaz ari bazara
Lekuz kanpo daude zure
Ahots korden koordenadak
Aho uhalak nahi eta
Hain zabiltza aho ahul
Aho alokatu horrek
Egin zaitu ahots ahul
Eguzkiari uluka
Uluka hotz eta ahul...