---
id: tx-2898
izenburua: Hamabortz Ama Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_gM93bUJ3ic
---

Hamabortz aldiz itsasoratuak

ditugu amaren esne tantak,

hamabortz aldiz lehorreratuak

belarrietan sehaska kantak.

Hamabortz aldiz entzundako hitzak

gaur mingainean dantzan dakartzat.

Kendurak batuz, batugaiak kenduz;

kenduz, batuz… HAMA BORTZ  AMA BAT.

NAFARROA OINEZ

BAZTANDIK MUNDURAT

HAMA BORTZ AMA BAT

ESKUARAZ MUNDU BAT

Literaturaren zenbakietan

zenbat ohar, mezu eta datu.

Hizkuntza zahar honen bizirautea

Baztanen nahi bada ziurtatu ……aitu!

alferrik dira: erro, errokizun,

errotzaile eta erro karratu

Jasotakoa zatitu, banatu…

eta erabilera biderkatu.

NAFARROA OINEZ

BAZTANDIK MUNDURAT

HAMA BORTZ AMA BAT

ESKUARAZ MUNDU BAT

NAFARROA OINEZ HELDU DA GURERA

GUREA DENONA BADA, ZURE DA.

HAMA BORTZ AMA BAT

BAZTANDIK MUNDURAT

HAMA BORTZ AMA BAT

BAZTANDIK MUNDU BAT

Bidegurutzean dabiltzan arren,

erraza zailduz, zaila erraztuz

jarrai dezagun gure hizkuntzaren

bidea pausoz pauso orraztuz

gure bi oin biluziak, txikiak…

milioika kolorez lokaztuz

atzoko urrats aztarnetan sartuz

eta biharko urratsak marraztuz.

NAFARROA OINEZ

BAZTANDIK MUNDURAT

HAMA BORTZ AMA BAT

ESKUARAZ MUNDU BAT