---
id: tx-455
izenburua: 25  Artikulua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ise5A2Zv_94
---

2021ko maiatzaren 9an Donostiako Antzoki Zaharrean grabatua.

Musika/Música: Idoia Asurmendi
Hitzak/Letra: Maria Oses

Kamerak/Cámaras:
Eider Ballina
Asier Castellano

Edizioa/Edición: Eider Ballina


Zeinen zaila den etxe bat sortzea
beste etxe guztiak hor daudenean,
aspaldi, betidanik;
lekurik ez eraikitzeko zerotik hasten den ezer.
Nork ez du merezi aukera bat
lau hormaz harago doan etxe
batean bizitzeko;
sutondo baten epeltasun goxora itzultzeko.
Zalantzaren lurraldean
oinak elkarlotuta dantzatzera kondenatu zituztenei.
Paperezko harresien eta
etengabeko guda baten menpe bizi direnei: herri hau denona da,
zuek gu gara.
Ta ze zaila den etxe bat sortzea
beste etxe guztiak hor daudenean,
batzuentzat eraiki zen
baina denontzat da lekua hemen babestu dezagun elkar
Zalantzaren lurraldean
oinak elkarlotuta dantzatzera kondenatu zituztenei.
Paperezko harresien eta
etengabeko guda baten menpe bizi direnei: herri hau denona da,
Zalantzaren lurraldean
oinak elkarlotuta dantzatzera kondenatu zituztenei.
Paperezko harresien eta
etengabeko guda baten menpe bizi direnei: herri hau denona da,
zuek gu gara.