---
id: tx-2493
izenburua: Ari Du - Baietz Hernanik!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hrIijUSDgwY
---

Euskara ari du Kultur Elkartearen Baietz Hernanik! ekimenaren abestia. 
Egileak: Andoni Tolosa Morau eta Maite Larburu.
Eskerrik asko Morau eta Maite, izugarrizko oparia egin diguzue!


Ari du ari aidu
ari du ari aidu
ari du ari aidu
ari du ari aidu
ari du ari aidu
ari du ari aidu
ari du ari aidu
ari du ari aiduaiiiiii
aidu ariiiiiiiiiiii
duuuuuuuu

Aidu etengabe
Hernanin etengabe
ahotik belarrira
eta aldrebes.
Ttantta ttikiez
itsasoak eginez
laino ilunei
algara eraginez.
Hitz soil leun batez
ezpainak bete printzez,
ahaztu lotsez
busti nazazu hotsez.
Ur esanda, ur izanez
entzunez hitz eginez
ari du
ai gea etengabe


Ari du ari aidu…

Guardasolik gabe
mingainak dantzatzen
euri fresko hori 
zuretzat ez gorde.
Itturriko golde
kutsatzeko bide
errota piztu du
zaparrada horrek.
Ari du bihotz betez
gogoz eta ekinez,
ikusi oraintxe
barrua ozentzen.
Hitza azaleratzen
entzunez hitz eginez
ari du
ai gea etengabe

Ari du ari aidu…