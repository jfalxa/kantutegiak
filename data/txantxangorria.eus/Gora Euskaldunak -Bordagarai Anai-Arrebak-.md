---
id: tx-3226
izenburua: Gora Euskaldunak -Bordagarai Anai-Arrebak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Wa1DOiCpjHg
---

JOJO BERNADETTE, (Elkar 1978)

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Munduko injustizia

ari da beti handitzen

eta gu hemen gira 

euskaldun seme-alabak

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Gure euskaldun semeak 

Parisera joaiten dira

bainan zer ote dute

hemen baino agradable

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Historia gurea

sinple sinplenetarik da

kantatzeko unea

etorri zauku egun bat

Gora, gora euskaldunak!

Gora, gora Euskal Herria!

Gora, gora euskaldunak!

Gora, gora Euskal Herria!