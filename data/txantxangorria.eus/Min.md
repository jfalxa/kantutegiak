---
id: tx-1211
izenburua: Min
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BlUUTQ5BiHQ
---

Odolaren Mintzoa taldeak 2017ko ekainan Zubieta eta Usurbilgo lurretan filmaturiko bideoklipa. 
Grabaketa, produkzioa, zuzendaritza eta montaia Peru Galbeteren eskutik. 

MIN(A)BIZI LYRICS

MIN(A)BIZI

Gaur goiz esnatu naiz atsekabeturik
zaborra dago kaleetan zintzilik.
“Hondakinen Guda” izenez deitua
gerrate zahar baten erretratua.

Ingurumen zentro zitala
zabaldu ezin den testigantza
Ezjakintasunaren edabetik
hartzen du munstroak indarra.

Gaur oheratu naiz atsekabeturik
Atez ateko bilketa horrekin
Haiei men egitera behartuko haute
beranduegi dela ohartu arte

Ingurumen zentro…

EZ HEMEN! EZ INON!

Erraustegiaren barrutik
kanpora begira nagoelarik
Hilgarria den gaitzaren itzala
etxez etxe doa ate joka.