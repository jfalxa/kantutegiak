---
id: tx-2829
izenburua: Euskalduna Naiz Eta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/47PoG_oAr-8
---

Guk taldearen kantu bat, bere "Eskualduna naiz eta" disko txikitik. 70. hamarkadan grabatu zuen, Pier Paul Berzaitz, Joanes Borda, Beñat Davant, Panpi Lacarrieu eta Beñat Sarasola jaunek.

Guk musika taldea Lapurdin (Iparralde, Europa) jaio zen 60. hamarkada bukaeran. Beraien kantetan iraultza, antzerkia eta musika uztartzen zituzten. 1970. urtean, Panpi Lacarrieu hurbildu zen taldera eta, 1972. urtean, Pier Paul Berzaitz eta Beñat Davant bikotea. Azken hauen ekarpen musikala nabaria gertatu zen, hainbat tresna jotzeko gauza zirelako, akordeoia, txirula, pianoa, edota saxofoia. Guk taldearen aurkezpen ofiziala 1972. urtean izan zen, "Aita, seme, laborari" ikuskizunarekin.

Guk taldearen lehen disko luzea 1977. urtean iritsi zen, bigarren ikuskizun baten ondoren, "Laborari, langile eta nagusi...". Hala ere, Guk taldeak ez zuen publiko edota bilgune handietan kantatu. Ikuskizun ponpoxoetatik kanpo aritu izan da beti, langile eta laborari mundua aldarrikatzen, eguneroko bizia, edota gizon eta emakumeen arazoak.

1983. urtean, Eneko, Mixel Labegerieren semea, batu zitzaien eta berarekin, 1985. urtean, "Herria zain" diskolana ekoiztu zuten, "Arbola", "Amodioaz" eta "Papo gorri" kantu ezagunekin. Geroago, "Gerlarik ez" (Elkar, 1987) eta "Arrozten" (Elkar, 1990) iritsi ziren. Guk taldearen azken diskoa "Bidez bide" izan zen. Lan hau aurrekoak baino askoz jantziagoa zegoen (txirula, pianoa, kontrabaxua, trikitia, txeloa, perkusioak...) eta konposizio garaikideagoak biltzen zituen. Halaber, borroka kantuak ez ezik, sentimentalak eta lirikoak ere baditu lan horrek.


Eskualduna naiz eta Euskadi dut maite
Mimin Ré
Horgatik kartzelara eremanen naute.
Mimin Ré
Makilez jo nautenak elizan bai dirade
Sol Ré
Nere oinaze minen eskaintzen ba ote ?
Do Si7
Eskualduna naiz eta Euskadi dut maite
Horgatik kartzelara eremanen naute.

Tiroz hilen naute errana bai dautate
Eta gauean lurrez estaliko naute.
Eskualduna naiz eta Euskadi dut maite
Horgatik kartzelara eremanen naute.
Kartzela leihotik izar bat da ageri
Hura biziko deno Euskadi da biziko.
Eskualduna naiz eta Euskadi dut maite
Horgatik kartzelara eremanen naute.