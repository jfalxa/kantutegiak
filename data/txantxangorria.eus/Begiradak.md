---
id: tx-508
izenburua: Begiradak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ybXUeJxkHZ0
---

Mutrikuko AME estudioetan grabatua (2021)
Bideoklipa: Grabaketa eta edizioa - BEGIPUNTUAN
Kontaktua: errietan@gmail.com


BEGIRADAK

Lasaiak, sakonak, indartzen gaituztenak,
politak, goxoak, liluragarriak...
Beste batzuk noraezean galtzen dira bidean
konplizitatea aurkitu nahiean.
BEGIRADAK

ARIMAREN LEIHOA, BIHOTZAREN HIZKUNTZA,
BILUZTEN GAITUEN ISPILUAREN ISLA.
HITZ BATEN IRUDIA, IRUDI BATEN HITZA,
ISILTZEN DUGUNAREN OIHARTZUNA.
BEGIRADAK

Mingarri, temati eta zelatariak,
goitik beherako erradiografiak.
Misteriotsu, axolagabe, hotzak edota hutsak,
gezurrez mozorrotutako egiak.
BEGIRADAK

ARIMAREN LEIHOA, BIHOTZAREN HIZKUNTZA
BILUZTEN GAITUEN ISPILUAREN ISLA.
HITZ BATEN IRUDIA, IRUDI BATEN HITZA,
ISILTZEN DUGUNAREN OIHARTZUNA.
BEGIRADAK


--------------------------------------------------------------------------------

*** JARRAI GAITZAZUE ***