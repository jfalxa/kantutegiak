---
id: tx-774
izenburua: Errotaberri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EbcD1XA5eck
---

Ikusten da zure kareta hodeien tartetik
Ikertzen ditut eta zure mustupilak argiki
Xarmangarria zara, ilegorria, "very nice".
Lortzen duzu jendeak maitatzea lehendabiziz
Jartzen zure pentsaerak zerrenda prinzipal
Komunikatzen dizkiguzu mila sekreto "all right".
Nola uka guk mugak barnean daudela
Ez direla ibaiak, ez mendiak, ez arroka haundiak
Irudi gaituzu zerurik ez dugula,