---
id: tx-2105
izenburua: Hamaika Holako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lwaLHCW_UJQ
---

Herri guztietan daude 
xelebrekeriak kontatzeko,
gurean ere bai, noski. 

Behin andaluz bat eta oiartzuar bat,
kotxean zihoazela, laino lodikote eta zuri 
batek
irentsi omen zituen. 
Lainoetan lainoena omen. 
Itsumenaren estutasunean hau esan 
omen zion oiartzuarrak besteari : 

"Tur-tur, tur-tur, junai motel
ke aze una niebla 
ke no xe oye ni puta idea". 

Herrikide ilustratua 
non ikasi ote zuen erdera batua?