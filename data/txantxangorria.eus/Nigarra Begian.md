---
id: tx-803
izenburua: Nigarra Begian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E6SaGUq3DQI
---

Sumatuz euskaldunak, ospetsu nahian,
guretik deus ez duen hizkuntza mihian,
sofritzen egoiten naiz horien erdian,
gutaz ahalgetua, nigarra begian.

Nehoiz etzeraukula, nik ez dut etsiko,
gure kontzientzia atzartzen hasiko;
egungo balentriak ez baitu betiko
odolaren mintzoa ixilaraziko.

O, entzule maitea, otoi barkamendu,
ez badut bozkarioz kantaldi hau ondu!
jostetako gogoa dautate kendu,
aunitz maite duenak aunitz sofritzen du.