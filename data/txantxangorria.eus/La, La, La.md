---
id: tx-2983
izenburua: La, La, La
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OYxuAVYtDko
---

La, la ,la, la
la, la, la
la, la, la
la, la , la, la
la, la, la 

Eguzkiaren argia
egunero dator
goizari abesten diot 
gazte naizelako
dena da bizitza
soinu ta olerkia
abestiak jaiotzan
abestiak agurrean

La, la ,la, la
la, la, la
la, la, la
la, la , la, la
la, la, la 

Amari abestia
sortu nindulako
eta sorlekuari
heldu nauelako
pozik ta abesten det
maitemin orduan
bizitzako bidean
ugari abestiak

La, la ,la, la
la, la, la
la, la, la
la, la , la, la
la, la, la