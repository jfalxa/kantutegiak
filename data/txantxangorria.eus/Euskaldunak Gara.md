---
id: tx-1530
izenburua: Euskaldunak Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pdmi-KNZED0
---

Akaso gu ez gara handienak,
ez gara agertzen mapamundian,
akaso gu ez gara politenak,
ez gara ibiltzen pasareletan.
Baina gure herri ederrean
barra-barra ditugu
festa eta umorea.
Nongotarrak garen galdetzean
harrotasunez esan:
euskaldunak gara!
Akaso ez gara karibekoak
ez dugu ongi dantzatzen txa-txa-txa
,
akaso ez gara wonderfulenak
ez dugu atsegin hot-dog saltxitxa.
Baina gure herri ederrean
barra-barra ditugu
festa eta umorea.
Nongotarrak garen galdetzean
harrotasunez esan:
euskaldunak gara!
(bi)