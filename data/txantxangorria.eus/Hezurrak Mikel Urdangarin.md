---
id: tx-3100
izenburua: Hezurrak Mikel Urdangarin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ipjqV_eosqE
---

Ur gozoak
ur gozoak zuen, lurra zaintzen

Lur lausoak
lur lausoak zuen hezurrak zaintzen

Baina gu, bai gu, mutu lotu ginen, berriz
ta orduan, gu, ikaratu ginen berriz
nun da zuen loa

Ipar baltzak, 
ipar baltzak zure arrenak zabaltzen

Hagin zaharrak
hagin zaharrak zure mina gordetzen

Baina zuk iratzarri gintuzun, berriz
ta orduan, guk, galdetu genizun, berriz
nun da zure loa