---
id: tx-2309
izenburua: Beldur Barik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EaQWIm4yRGU
---

Eutsi taldeak, Legazpiko neska talde batek osatuak, "Beldur barik"bideoa egin du Azaroaren 25ean ospatzen den Emakumeen aurkako indarkeriaren nazioarteko egunerako.

Gorrotoa zure begiradan isladatzen da
ezintasuna dakarkit burura,
denbora luzez daramazu bakarrik
zulotik atera ezinik......IXILIK!!
Gorrotoa zure begiradan mantentzen da
irtenbide bat bilatzeko ordua da,
amets gaizto hontatik esnatzen zara
goiza iritsi da ta!!!
Begi disdiratsu negarti horiek ez itzazu itxi,
ordua iritsi da, eguna heltzear da
Berdintasuna nagusituko da egun batean
ez dago urruti,laster amaituko da....
ZURE MOMENTUA IRITSI DA!!!

Gure ametsa laster egi bihurtuko da,
orain arteko sufrimendua bukatzean
mundu berri bat hastear dagooooo
gogoratuuuu.....
EZ ZAITZATELA ZAPALDU!!
Begi disdiratsu negarti horiek ez itzazu itxi,
ordua iritsi da,eguna heltzear da
Berdintasuna nagusituko da egun batean
ez dago urruti, laster amaituko da...
Begi disdiratsu negarti horiek ez itzazu itxi,
ordua iritsi da,eguna heltzear da
Berdintasuna nagusituko da egun batean
ez dago urruti, laster amaituko da...
ZURE MOMENTUA IRITSI DA!!!!