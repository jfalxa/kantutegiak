---
id: tx-3192
izenburua: Trapu Zaharrak Patxi Andion
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TPPt2FzMGbE
---

Egunez trapu biltzen
gabean dantzan lehertzen
beti umore onean
degu pasatzen,
eta goizean goiz
kalean irten da
lo goxoan daudenak
esnatzen dira.

Trapu trapu zaharrak!

Atera atera, trapuak saltzera
nik erosten ditut
modu onean
arditian prakak kuartuan albarkak
ta burni zaharrak txanponean

Erosten ditut trapuak
praka zahar urratuak
gona gorri zaharrak eta
atorratxuak.
Jo zak zurrumuzki
atabak soinu
dantzan egiteko
beti erana.

Eltze elbarriturik
pertza zulorik duenik
arran parrillik edo
trebera zaharrik.
Inork saltzeko badauka
librako txanponean
Ramon Batistak
erosten ditu.