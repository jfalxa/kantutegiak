---
id: tx-1863
izenburua: Banangoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FTCjkFHp0qA
---

Aitte San Migel Ijjurretako zeru altuko lorie agure zarrak jantzan ikusi t´harek eitten jjok barrie. Kaspel handijjen intxaur saltsie
pitxer morkuen txunpletin eragin eta beti eragin zarrok bajjeukek zer egin. Garaiko plazan otie loran Ermuan asentzijjue jantzan ikasi gura dabena barruetara beijjue. Kalabaziek aijjena berde punten lorie beillegi txapel gorridun mutil horrexek krabeliñie dirudi. Perretxikotan nenbillenien Mendiolako basuen dama gazte bat billatu neban elorri baten onduen. Gaztie nintzen zorue nintzen jarri nintzakon albuen amantaltxue zabaldu eta altzuen hartu niñuen. Gora ta gora puntapijjue eta gorago zerue hankarik altzeu ezin badozu makurtu eixu burue. Jantzari-jantza Anbotopeko erromerijjen sanue krabelintxu bet belarrijjen da larrosa bille banue.