---
id: tx-2854
izenburua: Magallanes
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4aLyN7p3ipY
---

Eraztun batek bizitza bat lotzen du
distantziak bi bihotz elkartzen ditu
izar hura nire testigu da
eta kaioak nire konfidenteak
Urruntasunean untzi bat ikusten da
nire helburuaren untzia da
nire helburua salbazioa da
eta itsasoa nire bidea
Arrokaren gailurrean nago
faroaren argia ikusten
berak gidatuko nau salbazioraino
nire perla aurkitu nahian
Berdin zait irla batera iristea
berdin zait edozein lekutara iristea
gustora egongo nintzen
niregandik urrunduko ez bazinen
Magallanes, Magallanes...

Sorotan Bele talde arrakastatsuaren kanta bat, beraien hirugarren diskotik aterata, "Jonen kezkak" hain zuzen ere. Diskoa hau Hirusta Records zigiluak kaleratu zuen 1996. urtean, honako lagunekin...

Ixiar Amiano, teklatuak
Aitor Etxaniz, bateria
Urbil Artola, gitarra, ahotsa
Mikel Errazkin, teklatuak
Mikel Izulain, biolina
Ritxi Salaberria, baxua
Gorka Sarriegi, ahotsa ta gitarra