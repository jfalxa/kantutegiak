---
id: tx-791
izenburua: Ur Azpian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-DAcgJH1XGw
---

Gaueko zure irriak indartu ditu nire begiak
Larrutan ibiltzeak korapilatu ditu gure bideak
Damuturik ez nagoen arren
Gure artean zerbait bazen
Berriro gertatuko den gauzak zailago jarri daitezen
Ur azpian jolasean
Hankak korapilatzen dira
Urrunean, itotzean
Egiak aireratzen dira
Loreak zeruan eta ametsak barruan
Pozoia odolean eta hautsa bihotzean
Bion arteko eztandak elkartu ditu urnibertsoak
Orain eroritako hostoak etorriko diren eta hitzak
Ur azpian jolasean
Hankak korapilatzen dira
Urrunean, itotzean
Egiak aireratzen dira