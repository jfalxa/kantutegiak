---
id: tx-977
izenburua: Non Daude Nire Lagunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e4zzMkViFUY
---

Musika: Herrikoia (Txina)
Letra: Herrikoia eta Koldo Celestino


Yi oh san sh wu lio chi
wod ponyo zai nali?
zai zoli, zai zoli
wod  ponyo zai zoli

Bat, bi, hiru, lau, bost, sei, zazpi;
laguna gaur ez da etorri;
non dago?, non zaude?
Bera dago hementxe!!!