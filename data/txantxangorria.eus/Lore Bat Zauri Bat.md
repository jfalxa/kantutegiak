---
id: tx-575
izenburua: Lore Bat Zauri Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NuEsv8ZZQyo
---

Hire bihotza, xori zauritu,
iheska joan zain atseden bila.
Hari mehe bat hunan bizitza
urratua dun keinu bortitzez.
Oroitzapen labe gorian
gorputz-gogoak tormentu luzez
Kiskalirikan, deseginikan
bizi bahintzenean deserri hortan.

Nahiko hunan, Beatriz
Chile libre batean
adiskide artean
maitasuna dastatu.
Nahiko hunan zentzuak,
lore goiztarren gisan,
zorrik ordaindu gabe
desiotan askatu.

Hire aitaren heriotz hura
bilakatu zain orain heriotz,
oihu lehertu bat utzi digunan
zauri guztien seinaletako.
Ez dun nahi izan iraganaren
faktura latza besteri itzul,
hainbeste beldur, hainbeste odol
isil-isilik pagatu ditun.

Nahiko hunan, Beatriz
poeta baten bertsoz
hire gaztetasuna
ispiluan mirestu.
Nahiko hunan gaur ere
bizitzaren kaliza
ardo gorriz beterik
gozamenetan hustu.

Errugabea hintzenan baina
pizti zordunak ez ditun urrun,
hire eskuen dardar hautsiak
haien ogena oihukatzen din.
Har zan atseden betiko loan,
gu gaitun orain mendekatzaile,
heriotz horren mezu gogorrik
ez baitinagu inoiz ahaztuko.