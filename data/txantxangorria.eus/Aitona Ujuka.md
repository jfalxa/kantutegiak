---
id: tx-684
izenburua: Aitona Ujuka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jmU_Q89SE7Q
---

Baserrian txakurra hasi da zaunkaka,
zaldia irrintzika, katua miauka,
astoa arrantzaka ta behia marruka
denentzat janaririk aitonak ez dauka

Oiloa ta oilarra biak kukurruka
ahateak kuaka, akerrak muturka,
txerritxoa kurrinka, aitona ujuka
lokuluxka egiterik amonak ez dauka
txerritxoa kurrinka, aitona ujuka
lokuluxka egiterik amonak ez dauka

Amonak ez dauka, amonak ez dauka,
lokuluxka egiterik amonak ez dauka