---
id: tx-2530
izenburua: Txakurra Dator
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SpH_DrApmQI
---

Txakurra dator atzetik,
goazen etxera kaletik.
Katua dator atzetik,
goazen etxera kaletik.
Zezena dator atzetik,
goazen etxera kaletik.