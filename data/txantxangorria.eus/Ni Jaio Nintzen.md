---
id: tx-2936
izenburua: Ni Jaio Nintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BLwgztORiTY
---

Haur txikientzako txalo-abestia da “Ni jaio nintzen”.Kantuaren erritmoa jarraitzen da nahi den eran txalo joz.

ABESTIA

Ni jaio nintzen,
txiki-txikia. 
Ni jaio nintzen 
Eguberritan (udaberrian). 
Asko jaten nuen,
ñan, ñan, ñan, ñan, ñan.
Asko jaten nuen 
esne eta taloa. 
Laster egin nintzen 
handi-handia. 
Laster egin nintzen 
ama bezala 
Ama eta aita bezain handia. Lalara, lalara…