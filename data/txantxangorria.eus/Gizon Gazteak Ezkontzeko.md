---
id: tx-2221
izenburua: Gizon Gazteak Ezkontzeko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kFiqVpr9PUI
---

Gizon gazteak ezkontzeko behar ditu lau gauza:
Lehen lehenik, iduria, alegera bihotza,
Dirua frango sakelan eta bizitzeaz ez lotsa.
Horietarik hiru baditut, laugarrena dut falta.
Bai iduria badut eta, alegera bihotza;
Dirurik ez dut, baina ordainez, bizitzeaz ez lotsa.
Aldiz baditut hiru tatxa, batto bainuke aski:
Arno edale, jokolari, lanian ez ari nahi;
Hiru tatxa horien gatik, neskatxek maite naute ni.