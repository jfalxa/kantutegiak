---
id: tx-194
izenburua: Ützül Gitin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zyDjtNr6UNU
---

Oraiko denboretan lana maleruski hirian beita, 
Gazteak harat badoatza, etxen ützirik ait’et’ama. 
Etxen aita eta ama, bibiak haidürü beitira, 
Ützül gitin harien gana, goxoki besarkatzera ! 
Hogei urten altin gazteak, maite dütü jei harroak, Libertatin erekuntriak, irixkü eta plazerak. 
Etxen direno haur maiteak irus dirade ait’et’amak, 
Bena trixtetzen hen bihotzak batzen direlarik berberak. 
Ait’et’amak zer plazerin haurrak etxerat jin direnin, Denak bildürik ber mahainin, algar estimatzen da ordin! 
Hitzak: Dominika ETCHART 
Müsika: Niko ETXART