---
id: tx-2731
izenburua: Bidez Bide
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V7drCOvk3V4
---

Bidez bide nabilena
kantu bat dut laguna
Guziek uste nautena
Johan arin-arina
Euskal zerupen auhena
Bihotzeko samina
Nahasiz elgarrengana
Kantuz daramat mina.

Auhelmina deraitala
Jender diotet hela
Nik eginez xaramela
ez xakinez bestela
Euskal Herrian ahula
Jende honen ixila
Nahiz argitarat zila
Ari pertsutan hola.

Zendako kantuz nabilan
Eskuaraz bazterretan
Nehork ez aditzekotan
Nago maiz gogoetan
Eskualde arroztuetan
Bas'herri hustuetan
Ez dut behatzailerikan
Xoriez besterikan.

Garaziko kantaria
Hoake hil-herrirat
Hire Ama dagonerat
Nigarren ixurtzerat
Nigarrez huntzerat eta
Barkamendu eskera
Etxia eta eskuara
Baitohatze hurrerat.