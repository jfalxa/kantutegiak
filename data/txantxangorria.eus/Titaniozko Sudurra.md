---
id: tx-1311
izenburua: Titaniozko Sudurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SKdrrn1ufKE
---

Prekarietatea bikaintasunaren atzean dagoela erakusten du "Titaniozko Sudurrak" abestiak. 2016an Guggenheim Bilboko Museotik kaleratutakoei buruzko kantua, lan baldintza duinak eskatzeagatik. 80ko hamarkadako soinuak, Miami Vice, Chicagoko House, Carlos Santana eta Negu Gorriak taldeak eraginen artean. Helburua da entzulea dantzan jartzea.

Bideoaren zuzendaritza eta edizioa: Hodei Torres
Arropa eta makillajea: Betitxe Saitua
Zazkel taldea 2016ko Banden Lehia Sariaren irabazlea da.

La canción muestra que la precariedad va buscando su perfección. "Titaniozko sudurrak" habla del despido de trabajadoras del Museo Guggenheim de Bilbao en 2016. Sonidos de los 80, Miami Vice, House de Chicago, Carlos Santana y Negu Gorriak están presentes durante la canción, todo con la idea de poner a bailar al oyente.

Dirección y edición del vídeo: Hodei Torres
Ropa y maquillaje: Betitxe Saitua
Zazkel, premio Banden Lehia 2016

Jarraitu gaitzazu! / ¡Síguenos! / Follow us!:


Hogei urte dira
hogei mila milioi gure kontu
gure kontu ordaindu zela munstroa
Ai ama!
Abangoardiaren muturra
hau da arrakastaren gailurra
euskal logiak eraiki zuen salbazioaren ikurra
abizen oneko beste hainbaten antzera
Platillosen semeari egin zioten harrera
ustelkeriaz boterea lortzea
hau da jeltzaleen afera
 
zuek, zuek zarete legearen jabe
tranpa eta artearen zale
 
hezitzaileak lan baldintza txarrak zituztela eta
Burgerheimaren aurrean jarri ziren builaka
kaleratuak jada eragozpenik ez bada
eta guztiaren atzean hara non dauden
urrutiko etxean makurregi har ’zak eta pakia eman zak
Gora ta gora abesten Bidarteren hankartean
 
zuek, zuek zarete legearen jabe
tranpa eta artearen zale
zuek azpikeriaren errege
tranpa eta artearen zale