---
id: tx-2970
izenburua: Hil Nintzen Eguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x3BprsjOtqY
---

Zeinek eman dit aukera
hemen mikrofono aurrean
biluzik irten ta hitzetan
nahi dudana esan ta ezkutatzeko?

Ez al da eguna gauaren etsai?
Ez al da eguna dena apurtzeko?

Zeinek eman dizu aukera
hona deitu ta aurpegira
merezi ditudan hitzak
nahi duzuna esan ta ezkutatzeko?

Ez al da eguna gauaren etsai?
Ez al da eguna dena apurtzeko?

Nik behintzat ez dut oroitzen
jaio nintzen eguna
negar asko agin nuela,
hori ez da berria
hil nintzen egunaz bai,
oroitzen naiz zure zai
baina jada ez zait axola,
berriz jaio naiz

Zeinek eman dit aukera
hemen mikrofono aurrean
biluzik irten ta hitzetan
nahi dudana esan ta ezkutatzeko?

Ez al da iluna argitasun hau?
Ez al da eguna dena apurtzeko?

eta ez dakit zu oraindik
bizirik zauden
eta negar egin behar badut
arrazoi on bat
bilatzen dihardut

jaio nintzen egunaz
oroitzen naiz zure zai
baina jada ez zait axola
gaur berriz jaio naiz