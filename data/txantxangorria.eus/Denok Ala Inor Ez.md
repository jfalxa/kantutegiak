---
id: tx-2032
izenburua: Denok Ala Inor Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a1sSJPlT6HI
---

Gatibua; nork libratuko?
Leitze zuloan daudenek ikusiko,
zure garraxiak adituko;
gatibuek zaituzte libratuko.

Denak ala inor ez, dena ala ezer ez.
Bakarka ezin da
fusilak ala kateak
denak ala inor ez, dena ala ezer ez.

Gose zaudena; nork asetuko?
ogia nahi duenari,
gose daudenek, ogia emango
bidea diote erakutsiko.

Denak ala inor ez, dena ala ezer ez
Bakarka ezin da
fusilak ala kateak
denak ala inor ez, dena ala ezer ez.

Galtzaile; nork mendekatuko?
zaurituekin alkartuz
guk zaurituek, zapalduek
zu zaitugu mendekatuko.

Denak ala inor ez, dena ala ezer ez.
Bakarka ezin da
fusilak ala kateak
denak ala inor ez, dena ala ezer ez.

Gizon galdua; nork lagunduko?
miseria sofri ezin dutenak,
gaur ez bihar izan dadin eguna
galduekin gara alkartuko.

Denak ala inor ez, dena ala ezer ez.
Bakarka ezin da
fusilak ala kateak
denak ala inor ez, dena ala ezer ez.