---
id: tx-2521
izenburua: Askok Irudipena Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xnkFPfSgQDQ
---

Askok irudipena du...
Askok itxura hutsetik,
ez barruz ezagutzetik...


...ai, bistaratuko banu!

Askok irudipena du
minari naizela zorra,
barea eta gogorra...
...bai, barrua hautsia ez banu!

Askok irudipena du
ziur noala gorengo
bidean pausoa sendo...
...bai, dardararik ez banu!

Askok irudipena du
munduan nagoela tentuz
iraultzaile pentsamentuz...
...bai, etxerako ere banu!

Askok irudipena du
apala naizela oso
hitzez ta jarreraz gozo...
...bai, mikatzetik ez banu!

Askok irudipena du
ni beti naizela grazi
egin ta eginarazi...
...bai, tristurarik ez banu!

Askok irudipena du
polit naizela ta lirain
hain eder, hain berezi, hain...
...bai, euren begirik banu!

Askok irudipena du
egin diodala gaur be
harro bizitzari aurre...
...bai, zertaz harrotu banu!

Barruz mami, larruz mamu.
Zer bistan, hura gabezi.
Zuk ez. Zu hain zara berezi...
...askok irudipena du...