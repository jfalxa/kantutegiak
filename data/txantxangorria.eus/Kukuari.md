---
id: tx-502
izenburua: Kukuari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wyEN5CwE14w
---

Kukuaren bizitza

jakin nahi baduzu

kantatuko det eta

entzun behar duzu.

Udaberri alaia

gustatzen bazaizu

txori artista honek

alaituko dizu

txori artista honek

alaituko dizu.

 

Aurkitzen duenean

gustoko lekua

kantua izaten du

egunerokoa

isatsa harrotuaz

luzatu lepoa

honela jarduten da

kantari kukua

honela jarduten da

kantari kukua.

 

Kantuan ona da bai

alperra lanean

besteren kabi bila

dabil gehienean

han arraultza uzteko

datorkionean,

bestek mantendu dezan

jaiotzen denean

bestek mantendu dezan

jaiotzen denean.

 

Kukua ez da ona

ama izateko

bere ondorengoa

ez bait du maiteko

txantxangorriari utzi

dio mantentzeko

berak hostropetikan

lasai kantatzeko

berak hostropetikan

lasai kantatzeko.

 

Udaberrian kuku

San Pedrotan mutu

gure txori artista

nola den gelditu.

Eztarrian du gaitza

ezin du kantatu

berriz udaberriak

sendatuko zaitu

berriz udaberriak

sendatuko zaitu.

 

Orain baldin bazoaz

gugandik aparte

urteroko ohitura

jator duzu bete.

Kantatu diguzu ta

guk zaitugu maite

agur kuku hurrengo

udaberri arte

agur kuku hurrengo

udaberri arte.

 

Hitzak eta musika: Kaxiano