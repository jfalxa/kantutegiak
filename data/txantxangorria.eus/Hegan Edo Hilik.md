---
id: tx-344
izenburua: Hegan Edo Hilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UtDGG0J-Bhs
---

"Hegan edo Hilik” Ezpalak taldearen "Kolpatu Topatu" diskoan aurkitu dezakezue.

Ane Barrenetxea Sua talde arrakastatsuko Abeslaria da, 2019 an Gaztea maketa lehiaketa irabazi zutenetik gelditu gabe dabilen taldea.

Hitzak:

Ei, hemen dena da oker
baina ni, ni ez naiz ezer gutxi hau kentzen badidate.

Kontrola inoiz ez aurkitzeak beldurtzen nau
hegalak moztea ordea edo hiltzea berdina da

Badakit ulertzea ez dela oso errexa 
baina hau da nire zainetan odola egiten duena.
Ez, ez naiz ezer min hau kentzen badidate.

Kontrola inoiz ez aurkitzeak beldurtzen nau
hegalak moztea ordea edo hiltzea berdina da
.


Jarraitu Ezpalak hemen: 

Bideoa:

Grabazioa: Oier Arregi eta Yuri Agirre.
Edizioa: Oier Arregi.

Audioa:

Musika eta hitzak: Ezpalak.
Gonbidatua: Ane Barrenetxea
Grabazioa: Mattin Epelde (UHINA).
Nahasketak eta masterizazioa: Eñaut Gaztañaga (Gaztain Estudioak).