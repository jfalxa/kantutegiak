---
id: tx-3250
izenburua: Pozkarioz Kanta Zagun -Alaitz Eta Maider-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AWb-7TF6c_A
---

Pozkarioz  kanta zagun gaur Jesusen jaiotzea
Al guziaz gora zagun sortu dan Jainko Semea
O Jesus zure graziak alda gaitzala guziak
O Jesus zure graziak alda gaitzala guziak
Eresika Aingeruak entzuten dira kantatzen
Gu ere hasi gaitezen pozturik Jauna goratzen
O Jesus zure graziak alda gaitzala guziak
O Jesus zure graziak alda gaitzala guziak
Jesus negu gogorrean ohetzat duzu lastoa
Nola bada atseginetan dukegu gure gogoa?
O Jesus zure graziak alda gaitzala guziak
O Jesus zure graziak alda gaitzala guziak
Sortzetik apaltasuna diguzu Jauna irakasten
Zergatik goratasuna dugu bada guk bilatzen?
O Jesus zure graziak alda gaitzala guziak
O Jesus zure graziak alda gaitzala guziak