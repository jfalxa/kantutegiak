---
id: tx-1634
izenburua: Nire Bihotzak Zure Begiak Ditu -Gontzal Mendibil-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j8GlacdR454
---

Zure begietan sartu naiz
ta irudi horren argiaren goxoz,
neure egin zaitut.

Zure begiradaren ziztadan,
zeuk narabizu.

Zure begi hoien distiran
suzko begien ninian
neure egin zaitut.

Zure laztan baten desiran
murgildu natzaizu.

Euria ari du, 
nire bihotzak zure begiak ditu.

Zure begietan sartu naiz
ta irudi horren argiaren goxoz,
neure egin zaitut.

Neure bihotzaren zirraran
barneratu zaitut.

Euria ari du, 
nire bihotzak zure begiak ditu.

Begietara begiratzen dizut
maite zaitut.

Euria ari du, 
nire bihotzak zure begiak ditu.