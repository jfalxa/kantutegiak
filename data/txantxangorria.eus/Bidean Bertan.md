---
id: tx-133
izenburua: Bidean Bertan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TVEQIe7LT_g
---

------------------------------------------------------------------------
Bidegurutze asko dauzkat aurrean
Zein da nahi dudana eta zein
Espero dena nitaz
Eta zer esango dizut
Jadanik ez dakizunetaz
Etsairik onena zu
Ispiluaren beste aldean

Bizi guztia zein naizen ikasten
Barruko ahotsari entzuten
Saiatu eta huts egiten
Hamarretik bostetan
Zoriontasun, zoriontasun zurekin amestu dut

Bidean bertan uoooo
Lagun artean  uoooo
Irribarre baten uooo
Une hauetan daukat
Merezi duen dena

Laino ilunak beste behin pentsamendu iragarpen guztietan
Batzuetan kateatuta gelditzen naiz buruaren gurpilean
Eta zer esango dizut jadanik ez dakizunetaz
Etsairik onena zu ispiluaren beste aldean
Bizi guztia zein naizen ikasten
Barruko ahotsari entzuten
Saiatu eta huts egiten
Hamarretik bostetan
Zoriontasun, zoriontasun zurekin amestu dut

Bidean bertan uoooo
Lagun artean  uoooo
Irribarre baten uooo
Une hauetan daukat
Merezi duen dena


Entzun, entzun bai
Behin biziko naiz
Eta ez dut, ez dut nahi korronteari jarraika bizi
Entzun, entzun bai
Behin biziko naiz
Eta ez dut, ez dut nahi arriskatu gabe bizi

Bidean bertan uoooo
Lagun artean  uoooo
Irribarre baten uooo
Une hauetan daukat
Merezi duen dena

------------------------------------------------------------------------
Zuzendaria: Iratxe Reparaz

Argazki zuzendaria: KORATGE
Kamara laguntzailea: Nestor Costa
Kamara Auxiliarra: Carlos Barroso

Muntaia: Joan Soler
Kolorea: Jaime Venegas

Produkzio zuzendaria: Joseba Razquin

Jantziak: Eneko Aranbarri
Makillajea: Laura Lesark

Grafismoa: Gorka Larcan

Musika eta hitzak: BULEGO
Musika ekoizpena: Eñaut Gaztañaga eta Tomas Lizarazu
Musika grabazioa eta masterizazioa: Gaztain Estudioak