---
id: tx-403
izenburua: Tren Luzea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tgQKL4xBQew
---

EÑAUT ELORRIETA - TREN LUZEA (Live)
Hitzak: Joseba Sarrionandia / Musika: Eñaut Elorrieta
Manuel Lekuona Auditoriumean zuzenean grabatua 2021eko maiatzaren 28an

Musika: Eñaut Elorrieta
Letra: Joseba Sarrionandia

Ahotsa eta gitarra akustikoa: Eñaut Elorrieta
Biola eta koroak: Maite Larburu
Gitarra elektrikoa: Ruben Caballero
Bateria: Ander Zulaika
Baxua: Fernando Neira

Grabaketa eta nahasketa: Haritz Harreguy
Masterizazioa: Victor Garcia (Ultramarinos)
Argiztapena: Javi Ulla
Soinu ekipoa: Soinueder
Produkzio arduraduna: Josune Alboniga

Bideoa: Gaizka Izagirre


Euria ari du
kristalak bustiz,
beti dago tren luze bat
egunsentiz estaziotik abiatzen
badago beti tren luze bat.

Ta beti dago bihotz zatitu bat
erdiz trenean doana.
Ta aldi berean tren estazioan
erdiz geratzen dena.

Andere beltz bat
dago leihotik begira
inori adio esan ezinik.
Euria ari du
bagoiak bustiz,
trenbideak bustiz.

Ta beti dago bihotz zatitu bat
erdiz trenean doana.
Ta aldi berean tren estazioan
erdiz geratzen dena,
ta erdiz badoana.

Trena beti,
trena beti,
trena beti,
beti doa infernurantz,
infernurantz.