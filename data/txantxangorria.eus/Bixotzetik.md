---
id: tx-793
izenburua: Bixotzetik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2WUwr6mkBN0
---

-Hitzak eta musika: Gatibu.

-Produktorea: Carles Campón "Campi".
-Grabaketa: Garate estudioa, Andoain, Euskal Herria. 2019ko abenduaren 16 eta 17an.
-Masterizazioa: Kevorkian Mastering, New York, AEB.

-Bideoklipa:
Zuzendaritza: Dani Llamas.
Kamara: Jesús Perujo.
Kamara laguntzailea: Alejandrina Zoreda.
Montaia: Silvia Moreno.

-Gure esker ona:
Uguzne Uriarte, Asier Foruria, Asier Gomez, Martxel Asteinza, Sara eta Luis (Zumela baserria), eta Ondarruko Portuko Ranpi.

----------------HITZAK-------------------

Bizitza kotxeko ispilutik ikusten
20 urte pasa dire 20 asteburuten
lekuek, hostalak, maletak arazo ta pozak
Kantuek ein ala, bizitza doa
Je! 1000 gaba, 1000 lagun
Je! Batzuk urten, besteak sartu.
Txiste txar bat atzeko partien
Umoriek eitten deu labur bidai luzie
BIXOTZETIK, ZURETZAKO
GARELAKO, POSITIBO

Ahotsak urratute
Atxamarrak bixortute
Baina bihotza beti bizirik dekogu
Ta tripetan kilimak
Olatu gainean batzutan
Tatarrez bestietan
Baia irrifar bat ez da sekula faltako
Ikusten garenien

Katalunya, Galiza, Mexiko
Londres eta Paris
Elantxobe, La Puebla, Altsasu
Oiartzun, Hazparne
Ze txikixe dan mundue
Ta ze txikixek gu.
Zure Herriko kaliek zihur
Zapaldute dekoguz.
Je! Ez iraganik ez gerorik
Ja! Egunien bizi gara,
Ta biluzten gara, bihotzez,
Arropaz batzutan
Egun bakoitza azken eguna
Bai litzan.
BIXOTZETIK, ZURETZAKO
GARELAKO, POSITIBO

Ahotsak urratute
Atxamarrak bixortute
Baina bihotza beti bizirik dekogu
Ta tripetan kilimak
Olatu gainean batzutan
Tatarrez bestietan
Baia irrifar bat ez da sekula faltako
Ikusten garenien