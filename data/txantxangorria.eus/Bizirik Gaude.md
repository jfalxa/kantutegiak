---
id: tx-94
izenburua: Bizirik Gaude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xixbRhyBwA4
---

Txertoa txarto, txortan hobe
Arnasatsa darizu mozalik gabe
Hautsak harrotu, eredu izan
Lubakian egon, eraso
Zirt edo zart edo zer?
Oraindik ere bizirik gaude!
Dili-dale debalde, giroa biraoz bete
Aerosolez aienatu aie hau ta hau ta beste
Amen-omenka, santigituz
Ukazioa legetzat hartuz
Zirt edo zart edo zer?
Oraindik ere bizirik gaude?