---
id: tx-2828
izenburua: Obatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0DyBsU5Tlps
---

“Ekainak zazpi, ba al dozu planik?
Bestela, hemen Bergaran
denon artian, gertatu dogu
ondo pasatzeko aukera.

Esan lagunei eta etorri
autobusian sartuta
elkarrekin gozatzera
gure herria eza(g)utzera
gure herria eza(g)utzera.

Guraso, ume ta irakasle
denok eskutik helduta
Obatu alkarri bultza dezagun
gure eskola publikoa
Euskal Eskola Publikoa