---
id: tx-3362
izenburua: Rocka Willy
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_eWUD0OGmkI
---

Nire auzoan ez dago ezer
bost katu ta bi kotxe berde
sugandilak harri paretan
idatzi du hain bat akorde.
Nire auzoan ez dago ezer
bost katu ta bi kotxe berde
sugandilak harri paretan
idatzi du hainbat akorde
Goazen ireki ditza/gun ba
kutxa zaharrak ganbaran
nik kitarra, hik danborra
hauxe da gure taldea
rocka! Rocka Willi guai!!!
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun
Nire auzoan ez dago ezer
bost katu ta bi kotxe berde
sugandilak harri paretan
idatzi du hainbat akorde.
Nire auzoan ez dago ezer
bost katu ta bi kotxe berde
sugandilak harri paretan
idatzi du hainbat akorde
Goazen ireki ditzagun ba
kutxa zaharrak ganbaran
nik kitarra, hik danborra
hauxe da gu/re tal/de/a
ro/cka! Rocka Willi guai!
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun
Goazen ireki ditzagun ba
kutxa zaharrak ganbaran
nik kitarra, hik danborra
hauxe da gure taldea
rocka! Rocka Willi guai!
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun.
Ra ra ra ra ra
pun pun pun pun
Ra ra ra ra ra
pun pun pun pun