---
id: tx-2985
izenburua: Ay Anselma
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uJNHQpmPbnE
---

Jakin duzu gizon ospetsua naizela,
baita, entzun hori ez dala ona,
pistolarekin naizelako azkarrena,
askok ikaratzen dituen gizona.
Baina, badakit trebetasun hori dela,
bizitzaren sortea niri emona,
baita ere tirokaturik ez dutela,
poztukorik zure bihotzaren sakona.
Ai, Anselma, Anselma, anderetxo laztana,
hurbiltzeko zuregana egingo dut al dudana!!
Bakoitzaren gogo barruan jaiotzen da,
ezin erosi, ezta saldu, ahal dena,
indarkeri guztiak baliatu arren,
ebatsirik aldatutzen ez dena.
Horregatik, gizon gogorra ni izan arren,
eskatzailea banator zuregana,
zu bakarrik zara ta, nere bihotzean, 
nahiaz eta pozaz betetzen dituztenak. 
Ai, Anselma, Anselma, anderetxo laztana,
hurbiltzeko zuregana egingo dut al dudana!!
Jakin duzu gizon ospetsua naizela,
baita, entzun hori ez dala ona,
pistolarekin naizelako azkarrena,
askok ikaratzen dituen gizona.
Baina, badakit trebetasun hori dela,
bizitzaren sortea niri emona,
baita ere tirokaturik ez dutela,
poztukorik zure bihotzaren sakona.