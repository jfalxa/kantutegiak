---
id: tx-1950
izenburua: Gora Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FWkutb4eLPQ
---

Esaera bat bada zaratak hamalau
urrutiko intxaurrak gerturatu ta lau
Gure herrian berriz hogeita hamalau
probatu ez duenak ez daki zer den hau 

Eztarri indartsuak ezin dira ito
gure lepoan soka ez dago betiko
parre egin gogotik neska ta mutiko
kantatzen dun herri bat ez da inoiz hilko 

Gora Herria!

BERTSO:
Esaera bat bada zaratak hamalau
urrutiko intxaurrak gerturatu ta lau
Gure herrian berriz hogeita hamalau
probatu ez duenak ez daki zer den hau 

FERMINEK BERRIRO:
Eztarri indartsuak ezin dira ito
gure lepoan soka ez dago betiko
parre egin gogotik neska ta mutiko
kantatzen dun herri bat ez da inoiz hilko 

Bat, bi
Bat, bi, hiru, lau

Gora Herria!!!