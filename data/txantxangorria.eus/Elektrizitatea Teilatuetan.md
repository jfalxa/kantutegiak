---
id: tx-2439
izenburua: Elektrizitatea Teilatuetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vHxzKUlTPUo
---

Re Sol Re
Eh! Non zaude? Eguzkipean zure bila nabil kalean zehar
Sol Re
Zure berri dakiten bakarrak katu gaiztoak dira.
La sim fal#m
Eta ez didate ezer esango zurekin ondo konpontzen baitira.
mim La7 Sol (2) Re
Hainbeste dibertitzen zarete nire kontura...
Re Sol Re
Eh! Non zaude? Euripean zure bila dihardut kaleetan
Sol Re
Zure berri dakiten bakarrak lanpetuta dabiltza orain.
La sim fa#m
Eta ez didate ezer esango elkar jolasean ari baitira
mim La7 Sol (2) Re
Gaua magiaz betetzen eta teilatuetan elektrizitatea.
mim
Ah! Baina beti dago norbait
fa#m sim fa#m
esku lagun bat luzatzeko prest horma gainean
Sol La7 Sol La7
ilea apaindu, eta badator Bere sekretua errebelatzea...
Re Sol Re
Eh! Hor zaude ezkutatuta, izara artean biluzik etzanda;
Sol Re
Zure berri dakiten bakarrak lagun ditut zeuk bezala.
La sim fa#m
Gauez jolastu dezagun elkar laztanduz azala;
mim La7 Sol (2) Re