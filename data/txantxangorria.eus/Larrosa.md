---
id: tx-2872
izenburua: Larrosa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y3g1hPTCsFQ
---

larrosa kolorez jantzia
beti dezu irria zure ezpainetan
agertu zure sarma fina
polita ta arina besarkatzean
makurtu zure aurpegi ederra
lotsati xamarra muxu ematean
eskeintzen didazu batera
biok jolastea nik abestean

larrosa kolorez jantzia
larrosa kolorez jantzia
zu zera neretzat bizia
zu zera argia zeru urdinean
larrosa kolorez jantzia
larrosa kolorez jantzia
zu zera nere beste erdia
auxe da egia geure artean

zure begi eder URDIÑAK
dira gaur iztunak kantu onetan
nai ditut preso eramateko
neureak izateko nik biotzean
sakonki maite zaitut mutil
aldatzerik ez da inolaz ere
eskeintzen dizundan muxua
besarkada estuak zuretzat dire