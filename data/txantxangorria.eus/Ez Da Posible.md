---
id: tx-1474
izenburua: Ez Da Posible
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dR8bz3Ltk68
---

Egunak 24 ordu
Halaxe ikasi nuen nik
Zeozer sakonago
Esan nahi ta
Bururatu ez besterik
Lekutan egongo nintzen ni
Irratia piztuta
Gau-pasata nengoen eta
Errainuak itsututa

Ez da posible, ezin liteke,
Ondo nago eta hau bakarrik
Behar nuen
Ez da posible, ezin liteke
Ondo nago eta gauza haundirik
Pentsatzeke

Lo egin gabe gizaldean
Beheraldia nabil baina
Berehala aditu dut
Eguneroko partea
48. josu dutela harrapatu
gero gertatzeko zena
orduan zinek asmatu.