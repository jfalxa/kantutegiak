---
id: tx-3118
izenburua: 564 Akeita Hutsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j_snA3JRYhs
---

Hotz amaigabe bat baino latzago
Ta urteak egun baino geiho
Bostehun eta hirurogeitalau ohe huts
Samiña zabaltzen
Batzuen ustez ordaintzen
Dagokien zigorra
Maite dutenentzat berriz
Askatasun haizea
Baina hori nork ulertarazi
Etxean zai bakarrik denari
Bostehun eta hirurogeitalau taupada
Gehiago gure bihotzean.