---
id: tx-2648
izenburua: Araba Euskaraz Wow! 2017
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q7G0uBMSKHc
---

Hitzak Orlando Arreitunandiarenak dira, eta abeslari lanetan aritu dira Betagarriko Iñaki Ortiz de Villalba eta Hesian-eko Zuriñe Hidalgo. Hona hemen abestiaren hitzak:

Mundu berriak, nahi ditugu ikusi

nondik gatozen, ahaztu gabe

gure sustraiak, utzi nahi ditugu

jaio ginenean bezelaxe

Wow esanda bidaia hasten da

Wow izarren artera

Wow bertatik osatu nahi dugu

Wow Araba Euskaraz

Araba Euskaraz

Wo-u-oh, Bastidan

Araba Euskaraz

Wo-u-oh, Errioxan

Bastidan hartu, espazio hontzia

helmuga non den, ez dakigula

Urratsez urrats, osatuko dugu

amore eman gabe, sekula

Unibertsoaren zati, Bastida

zeharkatu dugu, bere oskola

Nahiz espaziotik, txiki ikusi

itzela da gure ikastola