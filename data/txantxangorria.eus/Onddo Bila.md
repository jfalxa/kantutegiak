---
id: tx-1214
izenburua: Onddo Bila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7eZy_9_g30c
---

Mendian barna joaki
Onddo bila ibilki.
Iratze tartean, muga inguruan.
Saski hutsa, kani(be)ta garbia.

Patrol bat ikusi
Ta gu harrituak
eltzetuak ari ziren
makurka onddo bila.
Urde aluak!
Saskia ongi betea.

H(ai)ek ez gintuzten ikusi
gordeta hurbiltzen
Isilik, polliki-polliki.
Onddo altxorra hamar bat metrora.

Eskuko frenoa kendu
bulkada ttiki bat
patrola maldan beheiti
eltzetuak lasterka
Euren saskia gu(re)tzat
Behingoz, zerbitzu on bat.