---
id: tx-2413
izenburua: Nire Izena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bHzpDBpoLxo
---

Hiltzen naizenean egonen da

nire lauzaren gainean eskribu hau:

Hemen datza Gabriel Aresti Segurola. Goian bego.

Pérez y López. Marmolistas. Derio.

Bizkaiko Bibliotekan ere egonen da

(deskomekatzen ezpanaute),

liburu bat (behar-bada, ezta seguru),

inork letuko eztuena,

nire izenarekin. Eta

gizon batek esanen du kardanberak loratzen

direnean:

Nire aitak esaten zuen bezala, nik ere...

(Andre bat etorriko zait Done Santuru oro

lore koroa batekin).

Jainkoak eztezala nahi Bilboko karrika bati

nire izenik eman dezaiotela.

(Eztut nahi bizargile hordi batek esan dezala:

Ni Arestin bizi naiz, anaiaren

koinata nagusiarekin. Badakizu. Maingua).

Batzutan esan zaharrak erratzen dira.

Pentsatzen dut nire izena

nire izana dela,

eta eznaizela ezer ezpada

nire izena.