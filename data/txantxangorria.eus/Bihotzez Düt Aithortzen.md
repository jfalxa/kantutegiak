---
id: tx-2962
izenburua: Bihotzez Düt Aithortzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2wthesGu3GI
---

Maite zütüt ama,
Maite zütüt aita,
Bihotzez dut aithortzen.
Maite zütüt ama,
Maite zütüt aita,
Goxo zen zirekin bizitzia,
Irus algarrekila.

Ama zü zir'ederrena.
Aita zu azkarrena,
lilia eta haitxa.
Ama zü zir'ederrena.
Aita zu azkarrena,
Ene goguan düdarik ez da.
Paregabe zidela.

Amaren itxüra
Aitaren odola,
hilan arrapiztürik
Ene haur maitia
geroko leinhüria
bogatzen diat hire begietan
amodiuaren hatsa.