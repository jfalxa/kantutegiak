---
id: tx-1798
izenburua: Hamalau Heriotzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uxL1q3UIGjM
---

1.Milla zortzieun eta hogeitabostian
au amairugarrena Urri'ko illian
Martin Itzeberri'ko Altzibarrenian,
atiak jo nituen ongi illunian,
baita ireki ere borondate onian.

2.Ixtimatu ninduten konde bat bezela;
sillan exeri eta apaldu nezala;
konformatu ziraden familiyan ala
nagusiya ta biyak etziñen giñala...
berari sartu niyon petxutik puñala.

3.Oitikan saltatu zan eriyo-ihesi;
nigandik halakorik etzuen merezi.
Alabak zuenian aita hala ikusi
apaizaren eskaka bertatik zan hasi;
aizkora trixte batez niyon burua hautsi.

4.Ogeita hiru urteko neskatxa gaztia...
ez zen bada lastima hala tratatzia?
Aizkora trixte batez muñak saltatzia;
ez da milagro izain ni laixter galtzia;
aisa merezi nuen bertan urkatzia.

5.Ikusi zitunian senarra ta alaba,
Katalina gaxuak, ai, hura negarra!
Ez bazuan jarri nahi bestiak bezala
dirua non zeguan esan beriala;
ongi banekiyela dirurik etzala.

6.Santo Kristo, zurekin konfesa nadiyan
zer obra egin nuen nik Kataluniyan.
Lerida'n pasa eta Urbil'en erriyan
bost lagun bizi ziran beren familiyan;
giziyok hil nituen gabaren erdiyan.

7.Gorputzeko ikara, animako lotsa
zeren egin ditudan hamalau heriyotza.
Aditu ezkeroztik diruaren hotsa
bidean atera-ta botatzeko poltsa
aterako niola bestela bihotza.

8.Dirua eman ta gero kentzia biziya
hori egiten nuen gauza itsusiya.
Jaunak bere eskutik nenguen utziya;
orain errezatzen dut ahal dudan guziya
arren alkantzatzeko Beraren graziya.

9.Hogeitabost urte da nere edadia
-nagusi on batentzat ai zer mirabia!-
Ez naiz pekatuaren mantxarik gabia,
beti okerrerako abilidadia
penetan pasatzeko eternidadia.

10.Jainkoaren aurrerako nik daukadan lotsa
zeren egin ditudan hainbat eriyotza;
azkeneko sententziya izain da zorrotza;
lau laurden egin eta kentzeko bihotza
ai! hura urkaberako haiziaren hotza!

11.Jose det izena ta Larreina lombría:
Mendaro'n jayua naiz, bertako semia;
deskalabratua naiz munduan lajia;
amak egin dezala beste bat obia,
hain da izugarriya nere maldadia.

12.Lenguan izandu zait anaya gaztia;
lastima egiten zuen nere ikustia.
-Ai zer nola ote diran aita-ama tristiak!
Nitzaz eskarmentatu balitez bestiak...
Ni ere izutzen nau biziya kentziak.