---
id: tx-3019
izenburua: Reggae Gaua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AzAgjnNcDao
---

Egon hor, lasai, festak elkarrekin egin noiznahi
zuek denok batera, egon hor
zuek denok batera, lasai
segi eta kantu hau entzunda gero parranda egin noiznahi.
Segi, festak elkarrekin egin noiznahi.

Txalupa bat nuen, neuk egina
dudarik gabe hemengo ederra
bi urte baziren, gau eta egun, lan eginda
goiz batez itsasora laister joan nintzan pozik.

Egon hor, zuek denok batera
lasai, zuek denok lasai, festak elkarrekin egin noiznahi.
festak egongo dira, lasai orain
eta kantu hau entzunda gero, parranda egin noiznahi.
Zuek denok elkarrekin segi orain.

Pentsatu nuen, toki eder batera heldu nintzenean
hantxe bertan betiko geldituz
gauza asko baztertzeko zirenak
berriro egongo ziren neure bizitzan lehenak.

Egon hor, zuek denok batera
lasai, zuek denok lasai, festak elkarrekin egin noiznahi
zuek denok batera, egon hor
Oh! lasai, Oh!
Eta kantu hau entzunda gero, parranda egin noiznahi
Festak elkarrekin egin noiznahi
eta kantu hau entzunda gero, parranda egin noiznahi
zuek denok elkarrekin segi orain.