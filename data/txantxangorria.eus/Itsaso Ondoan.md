---
id: tx-1649
izenburua: Itsaso Ondoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/42Eq9s_rQLI
---

Itsaso ondoan uhainer musuka,
ta mendi gainetan zerurat otoizka:
hor Euskal Herria ari da galdeka
Euskaldun guziek dezagun besarka...
Maite duten semek dezatela tinka!
Lehengoen hitza hartan berriz finka!
Oi Euskaldun seme, ez maita erdizka!

Baina gora kanta gure errepika! (bis) 
Idek ditzagun begiak!
Zabal bihotz hertsiak!
Eta ikus zoin amultsua
dagon Euskal Herria. 

Euskaldun semeak Euskara badaki,
erdarean ere trebe da poliki...
Gizon jakintsuna hor bada ageri,
horra Euskalduna nausi ainitzeri!
Etorriko dea Bazko-Berri kari
bere gaintasuna Euskal Herriari?
Oi entzule maite, ihardetsak huni:
Bihar´e Euskaldun nahi duka bizi?... (bis)