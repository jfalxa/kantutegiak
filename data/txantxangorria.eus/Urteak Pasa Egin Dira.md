---
id: tx-857
izenburua: Urteak Pasa Egin Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5qYMVusA9fA
---

Begirada bat
zure gorputzean
Bota dudan bakoitzean
Ile gorriak, gezurrez jantziak
Nire ondoan egon beharra dauka

Aspaldi esango nukeen laztana
Zuria kolorea zela
Izena bihurtzen joan da, Apurka apurka 
Zuria gorputz bat ez da
Zuria lasaitasuna da 
Zuria maitasuna da
Zuria ondo sentitzea da

Mezu pare bat, Mugikorrean
Arratsaldeon eta aurpegi bat
Hitz egin diot, Kolorez blai nago
Ze gertatuko omen da

Aspaldi esango nukeen laztana
Ezagutzen ginela
Laranja kolorea ez da
Fruitua erditik moztua
Begirada bai bakarra 
Koloreak sentipenak
Urteak pasa egin dira eta konturatu egin naiz
Urteak pasa egin dira eta konturatu egin naiz
Urteak pasa egin dira eta konturatu egin naiz uuu
Urteak pasa egin diraa

Urteak pasa egin dira eta konturatu egin naiz
Urteak, urteak, urteak…
Urteak pasa egin diraaa