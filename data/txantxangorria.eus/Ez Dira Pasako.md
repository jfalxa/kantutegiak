---
id: tx-756
izenburua: Ez Dira Pasako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UilKNDbh5qs
---

2020an Dani Skst designen eskutik grabatutako bideoa. Abestia Iruñeko Sound of Sirens estudioan grabatu eta nahastu da Julen Urzaizen eskutik.

(EUS) EZ DIRA PASAKO

Gau honek ehun urte iraungo balitu
Iluntasuna besarkatzen jarraituko nuke
Gau honek ehun urte iraungo balitu
Amets bati lotuta jarraituko nuke

Duintasunez idatzitako ametsa:
Ez dira pasako!
Heriotza baztertuaz alde batera
Askatasuna dator eta

Gau honek ehun urte iraungo balitu,
Goiz argitsu baten ametsak iraungo du
Gau honek ehun urte iraungo balitu
Etsi gabe amaierara arte borrokatuko dugu

Harrotasuna arma bilakatzen da
Gazte borrokalarientzat
Intxortako aldatz malkartsuetatik
Ez dira pasako!

Eusko gudariak ez gara makurtzen
Ukabilak estu, tiroka, odolez bustita
Itxaropentsu, egunsentia doa argitzen
Intxortatik ez dira pasako!