---
id: tx-2528
izenburua: Seaska Hutsa Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p-HYYlgtOhQ
---

Eder goizean txoria

basoan xirulaka

Hain eder ortzian ekia

hostoetan keinuka

Eder zure esku txikia

errekan plisti-plasta

Berdin zait natura guzia

seaska hutsa da

Zeruan izar bat piztu da

seaska hutsa da

 

Alai jaietan herria

kalean gora behera

Hain alai parkeko ontzian

piratatxoen algara

Alai zure zangotxoak

airean dilindaka

Berdin zait poztasun guzia

seaska hitsa da

Zeruan izar bat piztu da

seaska hitsa da

 

Bero supazter xokoa

subilaren goria

Hain bero zartain mailatuan

arto talo horia

Bero zure aurpegia

oheko sargorian

Berdin zait suaren eztia

seaska hotza da

Zeruan izar bat piztu da

seaska hotza da

 

Leun itsas bazterrean

ondar ilun hezea

Hain leun ezponden oinean

harri xapal luzea

Leun zure bulartxoa

arnasaren lezea

Berdin zait udako goxoa

seaska latza da

Zeruan izar bat piztu da

seaska latza da.