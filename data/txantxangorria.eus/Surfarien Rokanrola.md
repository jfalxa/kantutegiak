---
id: tx-3286
izenburua: Surfarien Rokanrola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5B4-h0IbBa0
---

Surfariak uretara doaz
oholak eskuetan hartuta
Surfariak itsas bazterrean
uhin hoiekin harrapaketan

Jarri zutik bi hankak zabalik
oreka mantendu behar duzu
Gurpilik gabeko bizikletan
surfaren dantzaria zara zu

Haiek bezala egin nahi dugu
alai olatuekin jolastu
haiek bezala egin nahi dugu alai Ieieiei

Aupa lagunok denok gora
Ohol gainean rokanrola
Aupa lagunok denok gora
Ohol gainean rokanrola