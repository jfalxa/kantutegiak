---
id: tx-3258
izenburua: Bihar Arte  -Takolo, Pirrutx Eta Porrotx-  Bideoklipaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dH7bdhDyvrU
---

Plazan nengoen
jolasten txirristan
txori misterio/tsu bat
hurbildu zitzaidan
ipuinak kontatzen
ametsaren denboran
goazen etxera
hodei gainean
Ilundu zaigu
laister eguna
izar argitsuak
goaz denok ohera
Bihar arte laguntxo
panpin jostailuak
musu bat aita eta ama
Plazan nengoen
jolasten txirristan
txori misterio/tsu bat
hurbildu zitzaidan
ipuinak kontatzen
ametsaren denboran
goazen etxera
hodei gainean
Ilundu zaigu
laister eguna
izar argitsuak
goaz denok ohera
Bihar arte laguntxo
panpin jostailuak
musu bat aita eta ama