---
id: tx-1163
izenburua: Do Re Mi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eSRBnfM8bIE
---

Do, Re, Mi
Hankoker mon ami
Fa, Sol, La
zuzendu zaiola
Si, Si, Do
mardul eta sendo
Sol, La, Si
Berriz ere hasi
La, Sol, Fa
hortxataren txufa
Fa, Mi, Re
eguzkiak erre
Sol, Fa, Mi, Re
Do, Re, Mi, Fa, Sol, La, Si, Do
Ondo izan.