---
id: tx-511
izenburua: Basoilarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gXHnucU-spA
---

Basoilarak kantatzen düzü
Iratiko basoan;
ihurke lezakezu pentsa
nik zer düdan gogoan:
gayak oro igaraiten tüt
maitearen ondoan."