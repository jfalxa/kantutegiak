---
id: tx-2800
izenburua: Urte Batez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/56mXk8FIhuc
---

Urte batez maite bat izan dut nik
Urte bat betetzen du egun hunetan
Oraitzen naiz nola zure besoetan
Lokartzen nintzen
Fidatu ta bihotza eskainirik;

Oroimen hoi aztu nahi nuke garaiz
Nahi nuke bai samintasunez mozkortu
Nahi zinduzket adimendutikan kendu
Bainan alferrik
Mozkortzean gehiago oroitzen naiz.

Denbora hoi ez litaike bihurtu
Maitasuna zuk urrez ordaindu nauzu
Ta bihotza naigabez bete didzu
Esker gabe
Norbaitek berdin ordainduko zaitu

Zur'epaile denbora da izango
Nahiz eduki daukazun edertasuna
Gaur izanik maite asko dituzuna
Begira gero
Ez ote zaituen nihork maitatuko.