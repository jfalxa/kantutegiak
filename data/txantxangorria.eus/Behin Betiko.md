---
id: tx-1897
izenburua: Behin Betiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jvpxegj4y0g
---

Behin betiko, behin betiko 
sinple bezain tinko, 
ez dugu etsiko! 
Behin betiko, behin betiko 
sinple bezain tinko, 
EUSKARAZ ETA KITTO!

Etxean eta kalean 
berdin-berdin jolasean, 
eskolan, lanean, 
borrokan eta pakean. (tristuran eta pozean)
Dakitenek erabiliz, 
ez dakitenek ikasiz, 
herri bat osaturik 
euskaraz nahi dugu bizi.

Hasi etxetik eta kalera, 
hasi kaletik eta etxera, 
martxan jarri da lege berria: 
Euskaraz Euskal Herrian.