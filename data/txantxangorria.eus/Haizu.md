---
id: tx-1404
izenburua: Haizu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jmyIN7N3Y20
---

Normalak, erotzat bagenitu, 
moralak, bortxatu ez bagintu,  
gurea, zergatik zen delitu, 
haiena, zergatik meritu...     

Aizu, bai zu, haizu zaizu!   
Aske zara, libre zaizu!  

Euskarak, esku beltzak ez balitu 
eskolak, kolpatu ez bagintu,     
zauriak, lotsatu ez bagintu... 
Erruak, erroak luzeak ditu... 
                               
Aizu, bai zu, haizu zaizu! 
Aske zara, libre zaizu!  
Eskiulako, azken ertzean, 
pausatu zara, zure etxean,  
ixil-ixilik, agurtu zara, 
euskara hutsean.  

Aizu, bai zu, kanta zazu! 
Zauri hori konta zazu!  

Aizu, bai zu, dantza zazu! 
Zauri hori haiza zazu!   

Aizu, bai zu, haizu zaizu! 
Aizu, bai zu, haizu zaizu!   

Bihotz begiak, ezpain hegiak, 
zabal itzazu, zure egiak.    

Ixil-ixilik, agurtu zara, 
zure zauriak, zure euskara.  

Lauetan hogoi ta hama piku 
lurpeak balkoi ederrak ditu.
ixil-ixilik, agurtu zara, 
euskara hutsean.