---
id: tx-3323
izenburua: Aurresku -Uretxindorrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QjVl9zbH5N8
---

Aurresku izan degu beti euskotarren dantza,
gure asaben izatearen erakusgarri. 
Dantza garbi, zail, osasun erakusgarri
Bai aupa hi, ta aupa ni, danearen ikusgarri.  (Birritan)


Jantzi txuri, gerriz gorri osto orlegiaren azpian, 
gure besoak zabaldurik gurutze txuria egiñaz. 
Agur hiri nere aintziñako lagun hori,
agur zuri, agur hari, agur ikusle guztioi.﻿  (Birritan)