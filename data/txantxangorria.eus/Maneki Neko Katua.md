---
id: tx-1320
izenburua: Maneki Neko Katua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1KmnvN8260o
---

Japonieraz baneki
ez nintzateke maniki
Tokio tokitan dago eta, 
neska-mutil, jeiki!

Maneki-neko katua
dago beso-altxatua:
hator-hatorka ibili dabil 
eskua askatua. 

Maneki-neko bihurri:
gure ondora etorri
kenduko dizkiogu elkarri
bizpahiru zorri. 

Maneki-neko nekatu: 
besoa gorantz altxatu. 
Zugaz geratzea nahi baduzu 
begi bat kliskatu.

Ongi etorri zozoa 
hizkuntza bat bakarrean...  
Askoz beroagoa ez al da 
dozena batean?

Frantsesak dio “bienvenue!”, “Ongi etorri!”
ingelesez moldatzen naiz ni: 
“Yes I do!” 
"Welcome!" esanda laztan egin 
katu kuttun honi… 
 
Maneki-neko bihurri:
gure ondora etorri
kenduko dizkiogu elkarri
bizpahiru zorri. 

Maneki-neko nekatu: 
besoa gorantz altxatu. 
Zugaz geratzea nahi baduzu 
begi bat kliskatu.

Do, re, mi, fa, sol, la, si, do: 
“¡bienvenida, bienvenido!” 
euskaraz “kabia” esaten da,  
gazteleraz, “nido”. 

Khush amded, powitanie,
Huanying, tarhib, misthoavile… 
Ongi etorria emateko 
daude mila bide. 

Silaba berriak hartu, 
armairuetan ez sartu: 
silabak txilaba bihurtuta
gustura dantzatu.

Katu bat noiztik kabian? 
Txoriak noiztik saskian? 
Ongi moldatu daitezke biak 
kartetan, agian. 

Maneki-neko bihurri:
gure ondora etorri
kenduko dizkiogu elkarri
bizpahiru zorri. 

Maneki-neko nekatu: 
besoa gorantz altxatu. 
Zugaz geratzea nahi baduzu 
begi bat kliskatu.

Katuak badio “hator!”,
zuk hari segitu jator: 
tximistetatik libratu eta
babesa duzu hor. 

Zorte ona biderkatu, 
azti gaiztoak uxatu: 
izurriteak utikan eta 
trumoiak izutu. 
  
Maneki-neko bihurri:
gure ondora etorri
kenduko dizkiogu elkarri
bizpahiru zorri. 

Maneki-neko nekatu: 
besoa gorantz altxatu. 
Zugaz geratzea nahi baduzu 
begi bat kliskatu!

ARKAITZ KANO