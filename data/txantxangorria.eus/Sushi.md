---
id: tx-792
izenburua: Sushi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NMJjSpT9htc
---

Sushi - Saia Goait (2019)
AME estudioetan grabatua (Mutriku)
Kontaktua: saiagoait@gmail.com
----------------------------------------------------------
/// Bideoklipa ///
Zuzendariak: Aritz Larrea Arrizabalaga eta Eneko Elezgarai Urkiza
Gidoia: Aritz Larrea Arrizabalaga
Kamera eta edizioa: Eneko Elezgarai Urkiza.
Grabaketa laguntzailea: Endika Lekube Iturrioz

/// Hitzak ///
Emakume bat odolustuta bere etxean.
Familia bat bankuak botata kale gorrian.
Hiru gazte giltzaperatu dituzte erbestean
eta langile bat erahila izan da lantokian.

Aurrera egin dugulakoaren esperantzan
borroka guztiak amaitutzat ditugu eman.
Indignazioa, azeptazioa bilakatuta
eta bitartean kaleak ez dira sutan.

Iada hemen ez du euririk egiten
zoriontsu gara gure herri desberdin eder honetan.
Sua piztu gabe gordin jango dugu eskaintzen dutena
jipoi eta eraso zital guztiak.