---
id: tx-541
izenburua: Pakia Beyau
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YjSpQZHfung
---

Nelson Mandela
kartzelan zela
zenbat negar, zenbat osti, zenbat urte
orain zandari
beroiek beroien bihurtu dute
bera gidari
aintzakotzat hartu izan balute.

Reggae, punk ska ta fandango
pakea jokoz kanpo dago.

Alkate, pakete alkakahuete
zaindari, kantari, lendakakari
polizi, zorritsu, txanogorritxu
ministro, ze kristo, ministro fistro.
Histori, kontari, parlamentari
begira, mendira, parlamentira
Pepitok, zer diok, Pelipitori
Galindo, Galindo, gatito lindo.

Elkar irentsi gabe ta
elkarri lotuta
hitzak tokirik ez du
behin mingaina kenduta.

Euskal herri euskaldun bat
zazpiak batuta
lortuko degu baina
danok alkartuta.