---
id: tx-2016
izenburua: Gabeko Izar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WPJgNlpD3Is
---

Gabeko izar argitsuena 
Piztutzeakin batera
Haurtxo eder bat etorri zaigu 
Gaur arratsean etxera
Ordu ezkeroz etxeko denak 
Poztu ta zoratzen gara
Haurrra delako zerutik honuntz 
Datorren Jainko berbera
Alai ta pozik kanta dezagun 
haurtxo honen etorrera
gure iluna alaitutzeko 
jaio den argi ederra
Gaurtik aurrera atzerri hontan
Zorionekuak gara
Haurra delako zerutik honuntz
Datorren Jainko berbera