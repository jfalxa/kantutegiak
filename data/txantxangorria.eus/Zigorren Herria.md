---
id: tx-3349
izenburua: Zigorren Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/25eG--jvSbo
---

Vendetta musika taldeak, Ines Osinaga (Gose), Zuriñe Hidalgo (Hesian) eta Xabi Solano (Esne Beltza) kantariak lagun, 197/2006 doktrinaren kontrako kantu bat grabatu du, "Zigorren herria" izenburupean. Zuzenean aurkeztuko dute datorren irailaren 14an, Herrira-k Herritarron Epaia plazaratzeko antolatu duen ekitaldian. Kantuak John Lennon abeslariaren "Give peace a chance" du oinarri musikan, birmoldaketa Pello Reparaz-ek sortu du eta hitzak Jon Garmendia "Txuria" euskal iheslari politikoak idatzi ditu. Iparralden grabatu dute, Paxkal Etxepareren estudioetan, eta Pariseko Source Mastering estudioetan masterizatu. 

Abestiak irailaren 14ko ekitaldiari amaiera emango dio, hainbat musikarik eta sektore desberdinetako kidek kantatuta, dagoeneko Iruñeko Anaitasuna kiroldegian izango direla adierazi baitute. Ikusleek ere parte hartzea pentsatuta dago, eta irudi horiekin bideoklip bat grabatzea, Europara Herritarron Epaiarekin batera bidaltzeko. 

Horretarako, Herrira-k kantua eta hitzak jasotzen dituen bideo hau zabaldu du, eta gonbita luzatzen die herritar guztiei Anaitasuna kiroldegira joateko eta abestia ensaiatzen denbora tartetxo bat emateko. Helburua 2.000 laguneko abesbatza lortzea da, errepika hau batera kantatzeko: "All we are saying is give peace a chance". 

Zigorren herria

All we are saying is give peace a chance

Gure aberria
Zigorren herria
Euskaldunok ez dugu maitatzea libre
Askatasuna ez da
gure eskubide

Besteen legea
Besteen boterea
Zigorraren gainean zigorra jartzeko
Askatasun haizea
kartzelaratzeko

All we are saying is give peace a chance
All we are saying is give peace a chance

Kateak ez dira
Josiko herrira
Ametsak bizirik dirauen artean
Egi bihur ditzagun
Guztion artean

Herri bat bidean
Manera librean
Ez gara inori begira izango
Guk ez dugulako nahi
Itxaron gehiago

All we are saying is give peace a chance
All we are saying is give peace a chance