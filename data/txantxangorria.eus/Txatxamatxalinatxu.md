---
id: tx-2143
izenburua: Txatxamatxalinatxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VDf_s_9QWMg
---

Txatxamatxalinatxu jaio zan urtian
herria ta kofradia hasarratu ziran

Tirikitiena beltxa morena
ontza bat txokolate goizian onena

Txikia izan arren izan leike abila
Biba Elantxobeko tanbolintxerua

Tirikitiena beltxa morena
ontza bat txokolate goizian onena