---
id: tx-2366
izenburua: Ametsen Bidea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gQmCADI1ZYo
---

Mila esker zuen ametsak gure egiteagatik... Ametsen bidea eta Juneren hegoak

Milesker Bereziki
Gorka Urra Bideoa eta musikan produkzioa egiteagatik...zoragarri...
Lezoti, zuen estudio liluragarria uzteagatik

Eskerrak ere  musikariei:
Xabi Solano ahotxak eta trikia
Oscar Portuges gitarra flamenka
Urbil Artola dobroa eta akustikoa
Garbine Sagastibeltza baxua
Gorka Urra gitarra akustikoa 
Maider Zabalegi ahotsak eta konposizioa


Ametsen bidea
Haziak erein dut
zelai berde eder hau loratzea
Ametsen bidea
Elkarri lagun/uz
zailtasunak gainditzea
Zu zara gu
Gu gara zu
Iluna dagoen hori 
argitzen baduzu
Zu zara gu
Gu gara zu
Pauso bat aurrera
eta guztiok batera
Juneren hegoak
Sasi bidean loratzen diren
maitasun hostoak
Juneren hegoak
Bakardadean 
besarkatzen duten besoak