---
id: tx-761
izenburua: Jersey Girl
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ywk8lphv5yo
---

Lagunak ezin ditut ikusi
Karrikan bera behar dut jeitsi
Neskak daude hor izkinean
Zurekin nahi dut izan gauean
Zurekin ibaia zeharkatu
Zurekin ibaia zeharkatu
Bide hori elgarrekin hartu
Laztana ihauterira joan 
Jostatokietara eraman
Lainoa dago ibai hegian
Zu ta ni larunbat gauean
Ametsak egi bilakatzean
Zurekin kaletik ibiltzean

Sha, la, la...
Neskatxa batez nago matemindurik
Sha, la, la...

Badakit! Nauka menperaturik
Bere besoetan nagolarik
Nere neskak dena eskaintzen dit
Baina eraztunik ez dut nik
Ez trabatu ez dut astirik
Neskatxaren bila noalarik
Ez dauka deusek narrantzirik
Neskatxa batez nago maitemindurik
Sha, la, la...
Hor zaude maitea hain nekatua
Zure lanarengatik hustua
Afaltzera goazelarik
Zure nekea ezin gorderik
Apaintzera zoazenean
Hobe bazine amaren altzoan
Dantzalekua da kaltegarri
Otoi ez zaitez nirekin etorri
Lainoa dago ibai hegian
Zu eta ni larunbat gauen
Ez dauka deusek garranztirik
Neskatxa batez nago maitemindurik
 Sha, la, la...
Musika Tom Wiats
Adaptazioa: Anje Duhalde