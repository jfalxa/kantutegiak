---
id: tx-273
izenburua: Hamabostmila /Kai Nakai) Chill Mafia Take Diss
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-RW_BPQD1PE
---

15K (HAMABOSTMILA) - KAI NAKAI
Directed by Julen De La Serna

LETRA EUS:
Twitterrek dio arazo bat duzuela nirekin
Ea zer dudan egin
15000rekin
Beka lortzean nahizta emango nizuen min
Talentu gabe ezin zenuten ezer egin...

Gaur produktore bat duzue lehenago nirea zena
Ahal beste izorratu ta jendea saltzen duena
Harro esan zuen utzi zuela euskal eszena, 
zuen buztanak jaten 
lortu du nahi zuena

15000rekin ni lanean nago
Euskal herriak perrea dezan gehiago 
Segi zuenarekin, hala dut nahiago
Oraindik nire onena iristear dago! (bis) 

Nahiz nire kantak kritikatu badakizkizue denak
Kolpez-kolpe sortzen direnak
Nire kantu oro da sarean nobedade
Inor iraintzeko inongo beharrik gabe
Ni hona heldu nintzen zuek baino lehen
Ez diozue inori esango zer egin behar duen
Ezizena ez dakit miliki frexko ez den... 
Edozein zalantzatarako izango nauzue hemen

Gaztetxeei eskatzen 4k, k, k
Zuen ustez iraultza hau da ta

Auzo bizitza
Nahiko zenituzkete 15.000 zuen mierdetarako balitza
bai jende murritza... 
Hau al da auzo bizitza 
Nahiko zenituzkete 15.000 zuen mierdetarako balitza
Sudurretik sartu duzue hitza

15000rekin ni lanean nago
Euskal herriak perrea dezan gehiago 
Segi zuenarekin, hala dut nahiago
Oraindik nire onena iristear dago! (bis) 

Twitterrek dio arazo bat duzuela nirekin
Ea zer dudan egin
15000rekin
Beka lortzean nahizta emango nizuen min
Talentu gabe ezin zenuten ezer egin...

MUSIKA EKOIZPENA / PRODUCCIÓN MUSICAL: Nare
KONPOSIZIOA / COMPOSICIÓN: The Big Sound
LETRAK / LETRAS: Unai Mendiburu eta Iratxe Aguilera
GRABAKETA / GRABACIÓN: Beñat Oribe
NAHASKETA ETA MASTER / MEZCLA Y MASTER: Koldo Uriarte


Jarraitu nazazu hemen/ Sígueme aquí: 

Nare Music:

Management: joseba@pandaartistmanagement.net

#KaiNakai #15K #HAMABOSTMILA #BEEF #EH