---
id: tx-2791
izenburua: Nevadara Joan Nintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FexUuBqqF9c
---

Nevadarat joan nintzan
Dirurikan gabe,
Handik itzultzekotan, maitia,
Bortz miliunen jabe.

Ai, ai, ai, ai, ai !
Ametsaren lana !
Ait'amen erranetarik barna
Ibili bagina !

Urteak joan ziren
On 'ta txar nahaste,
Gure fortuna heiekin eta
Denak berritz haste !

Ai, ai, ai…

Begia zorrotz eta
Memoria argi,
Ez den ez den tokian, maitia,
Aztaparra garbi.

Ai, ai, ai…

Moltsa arin dut, bainan,
Aberats gogua !
Nere fortuna zare, maitia,
Nere sosegua !

Ai, ai, ai…