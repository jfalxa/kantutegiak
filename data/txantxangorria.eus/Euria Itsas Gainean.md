---
id: tx-658
izenburua: Euria Itsas Gainean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n3k1jqpi7rI
---

Re           * sim         mim          La
Gorrotoak mundua du kabi heriotzak dirua du sega
Re           * sim         mim             La
euskaldunok deserria sari atzo eta gaur beti kontu bera.
Re         * sim        mim        La
Ta kulturari erreparatu gure pasio eta beharra
Re         * sim 
berriz ote gara deskuidatu? 
mim         Mi                    La (2)
sartu digute    lehengo asto zaharra.
Re              Sol     Mi     La
Hau duk hau komeria azken finean
 Re        Sol     La      Re
berriz ere euria itsas gainean
Re          *sim          mim           La
Banketxeek diru aterkirik gogoz dizute alokatuko
Re          *sim        
Kentzeko ba dute aitzakirik 
mim               La 
  euri zaparra hasten den orduko.
Re         *sim          mim          La
Militante mozorroarekin asko negozioaren mundura

Re          *sim          
merke- merke erosteari ekin
mim         Mi                 La (2)
bai eta saldu   nahi duten modura.
Re              Sol     Mi     La    Re        Sol     La      Re
Hau duk hau komeria azken finean berriz ere euria itsas gainean
Re                *sim          
Herri zahar honen tristurarako 
mim           La 
Odolak bazterrak busti ditu,
 Re             * sim          
Baietz kamarak  etortzerako 
mim            La 
gorbatadun putreak azaldu!
 Re         * sim          
Baimen eske  ohituak gara 
 mim             La 
nahiz jakin inoiz ez zaigula eman
Re         * sim          
aurrera jarraitu beharko da 
mim        Mi              La 
lehengo idi   zaharrekin deman.
Re              Sol     Mi     La    Re        Sol     La      Re
Hau duk hau komeria azken finean berriz ere euria itsas gainean
Re              Sol     Mi     La    Re        Sol     La      Re
Hau duk hau komeria azken finean berriz ere euria itsas gainean