---
id: tx-379
izenburua: Hitzekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3gwHwJ-J5og
---

FITXA TEKNIKOA
Korrika edizioa: 22 (2022)
Leloa: Hitzekin
Zuzendari artistikoa: Nerea Urbizu

ABESTIA
Izenburua: Hitzekin
Hitzak: Sustrai Colina
Musika: Nerea Urbizu
Abeslariak: Ane García, Anari, Nerea Urbizu
Musikariak:
Ander Zulaika: bateria
Itsaso Etxebeste: baxua
Eñaut Gaztañaga: gitarrak
Nerea Urbizu: pianoa
Grabaketa estudioa: Gaztain estudioa (Zestoa)
Nahasketa eta masterizazioa: Eñaut Gaztañaga

BIDEOKLIPA
Ekoizpena: Tripulante Produce
Errealizazioa eta edizioa: Eymard Uberetagoena
Kamerariak: Diego Azanza
Laguntzaileak: Itsasne Ezkerro
Ekoizle laguntzaileak: AEK-Korrika
Aktoreak: Lekunberriko Ibarberri eskolako 4. mailako ikasleak, Ainara Unanua Arizmendi,
Mikel Unanua Arizmendi, Omar Diop, Maddi Egiguren, Laia Igerategi, Itziar Ituño, Ramon
Agirre, Begoña Ajuria Blanco, Mikaela Elizalde.
Estrak: Baionako Bernat Etxepareko ikasleak (Laia Igerategi, Maddalen Duny-Pétré,
Maddi Egiguren, Unax Egiguren, Itzal Aranzabal, Kattalin Harrispuru Diribarne, Garikoitz
Agirre, Ainara Larralde, Lore Larralde), Aizarotz herria
Bideoko irudiak honako tokietan grabatu dira:
Aizarotz
Baiona
Basauri
Baztan
Donostia
Iruñea
Lekunberri
Ondarroa
Eskerrak: Lekunberriko Haur Eskola eta Elena, Ibarberriko 4. maila,
Ion Arretxea Unanuari belazea eta traktorea uzteagatik, Aizarotz herria,
Ondarroako Kofradiako Angel, Antiguako AEK.


HITZAK.
HITZEKIN, HITZEKIN, HITZEKIN, HITZEKIN, HITZEKIN...



OINETAKOAK LOTU, (E)TA UTZI OINEI LIBRE
BELARRETAN ORTOZIK BILAKATZEN BIDE.
MAPETATIK HARAGO MUNDU ASKO DAGO,
SENTITUTAKOA, SENTITU (E)TA HITZEKIN    



(E)TA BERRIRO EGIN KORRIKA ZUREKIN
ARANTZAK ERAUZTEKO ELKARREN EDERREZ.
(E)TA BERRIRO EGIN MUNDUA ZUREKIN... 

EURIPEAN KANTARI IBILTZEN IKASI
EZ EGON ATERTU ZAIN,  (E)TA KORRIKA HASI.
BERDIN DA NON HASTEN DEN, ORAIN BETI DA HEMEN.
PENTSATUTAKOA, PENTSATU (E)TA HITZEKIN. 

(E)TA BERRIRO EGIN KORRIKA ZUREKIN
ARANTZAK ERAUZTEKO ELKARREN EDERREZ.
(E)TA BERRIRO EGIN MUNDUA ZUREKIN
BETI BAITAGO NORBAIT HITZ EKITEKO PREST. 


HITZEKIN, HITZEKIN, HITZEKIN, HITZEKIN...


NEGUKO EGUZKITAN ERANTZI AZALA;
BETI ZIUR DAGO(E)NAK DUDA SOR DEZALA.
BIZITZA JENDEA DA, (E)TA JENDEA GARA.
SENTITU, PENTSATU... SENTITU, PENTSATU HITZEKIN!

HITZ EKIN, HITZEKIN, HITZEKIN, ZUREKIN... 
HITZ EKIN, HITZEKIN, HITZEKIN, ZUREKIN... HITZEKIN.