---
id: tx-1273
izenburua: Zein Erraza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qwtnXmWTXSo
---

En Tol Sarmiento (ETS) taldearen Zein Erraza' bideoklipa. Bideokliparen egileak: Taom Videoclip
 'Zein Erraza' del grupo En Tol Sarmiento (ETS). 
Realizadores del videoclip: Taom. 
LETRA (Zein erreza) 
Jaio nintzen kutxa baten barruan 
barneratuz errutinaren arau guztiak. 
Bizitza bera hau dela, lau horma itxien artean. 
Eskuak lotu, bira eta bira, jarraitu zure egoeran. 
Beharrezkoa duzun guztia kutxa honetan. 
Zein erraza den ulertzea, pentsatu behar ez dena 
Zein erraza irabaztea, bakarrik zaudenean 
Zein erraza den jarraitzea, gehiengoaren joera 
Zein erraza defendatzea betikoa, betikoa.
 Esan didaten guztia egiten aske sentitu naiz giro garbi honetan. Morfinazko goxokiak, pozik mantentzen nautenak. 
Huts egiteko eskubidea, 
sokak apurtzeko aukera, 
noranzko berrien geziak galtzaileentzat. 
Ni prest nago apurtzeko 
eta nahi dugunaren hura sortzeko. 
Zein ez dago igotzeko? 
TRADUCCION: 
Zein erraza (Que fácil) 
Nací dentro de una caja 
Aceptando todas las leyes de la rutina. 
Que la propia vida es así, dentro de cuatro paredes cerradas. 
Átate las manos, gira, sigue en tu situación. 
Todo lo que necesitas, está en esta caja. 
Qué fácil es entender, lo que no hay que pensar. 
Qué fácil es ganar, cuando compites solo. 
Qué fácil es seguir, la tendencia de los demás. 
Qué fácil defender lo de siempre, lo de siempre. 
Haciendo todo lo que me han dicho, me siento libre en este limpio entorno. 
Caramelos de morfina me mantienen contento. Derecho a fallar, opción a romper cuerdas, nuevas direcciones de flechas, para los perdedores. 
Yo estoy preparado para romper, 
Y para crear lo que queramos. 
¿Quién no está para subirse?