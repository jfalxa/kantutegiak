---
id: tx-1957
izenburua: Olatuz Olatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/omBPrmn9wz8
---

Urriko arrats hartan
ez nizun agur esan
hitzez hitz
akordatzen naiz

Izan giñen haur haien 
ikara gabeko
irriak non dauden
ez zara oroitzen

Ondarrezko dorretan
defendatzen giñen
marea igotzean
era berean...

...Imaginatzen zaitut
olatuz olatu
borrokatuz
eta zu?.. non zuade zu??..
eta zu?.. non zaude zu??..

Eta zenbat alditan 
irudikatu dudan
egunaz
akordatzean

Beldurrik gabeko
irrifare hura
azaleratzen zait
sinetsiz noizbaiten

Dena aldatuko dela
burnizko dorrea
eroriko dela
oroitzen zara

umetan itsasoak
arezkoak hautsi
zituen bezala...

...jausiko dira
olatuz olatu
lortuko dugu 
zihur pausorik pausu

Garen guztia
berriz proban jarrita
eta zu?..eta zu?..
eta zu?..eta zu?..

Nitaz askotan
oroitzen al zara