---
id: tx-1006
izenburua: Begibakarraren Bidaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uQFxAz3NGqY
---

www.xabibandini.com

Videoclip oficial de "Begibakarraren bidaia", de Xabi Bandini.
Gor Discos.
Producido por Serie B Vídeos.
Realizado por Borja Lezaun Mas.
2018.

-Begibakarraren bidaia-

Denborarekin begibakarraren nondik eta norakoaz 
jakin genuen kostatik joan zen ontzia galtzean

Bere buruari galdetzen zioten beste bidaiariek, 
zein ote zen espedizio horren zama eta pena
zori, nahia eta

bidaiaren erdibidean
itsasora salto egin zuen
ezer utzi gabe.
Bitakora notak zioen
"artikoan ur hotzetan desagertu zen"

"y que sople el viento del noreste
y que arrastre todo contra el mar,
y el océano acoja a los valientes
a mis valientes"

Bidaiaren erdibidean
itsasora salto egin zuen
ezer utzi gabe
bitakora notak zioen
"desioak salbatuko du."

Espera su momento, tic tac.

El viaje del hombre de un sólo ojo

Con el tiempo tuvimos noticias sobre el hombre de un sólo ojo, 
Cuando naufragó el barco que partió de la costa.

Los demás pasajeros y pasajeras se preguntaban, 
¿Cuál es la pena y la carga que lleva esta expedición?
¿Cuál su destino, deseo y…

A mitad del viaje saltó al mar
sin dejar nada atrás.
Decían las notas de bitácora
“desapareció en las frías aguas del ártico”

y que sople el viento del noreste
y que arrastre todo contra el mar,
y el océano acoja a los valientes
a mis valientes.

A mitad del viaje saltó al mar
sin dejar nada atrás.
Decían las notas de bitácora
“lo salvará el deseo”
Espera su momento, tic tac.

#saveyouinternet