---
id: tx-307
izenburua: Izurdeen Lekua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p0UiW9bt9cY
---

Izurdeen lekua izena du Mikel Urdangarinen azken disko luzeak. Eta honakoa da diskoari izena ematen dion abestiaren bideoklipa. 


Senak aginduta etorri ziren bisitan izurdeak euskal kostaldera maiatza hasieran, justu Urdangarin lan berria Izango zenarekin aurrera egin ala ez zalantzan zebilenean. Berak ere senari kasu egitea erabaki eta aurki labetik aterako den  kantu sorta berriari heldu zion irmo. Ebokazioz beteriko lana, orain arte ibili gabeko paisaiek koloreztatua, gertuko galeren argia daramana bere baitan.

Hitzak:

2020ko maiatzean
gure uretan haien uretan
urratu zuten azal berdea
iratzarri zen itsas barea

Hor azaldu ziren gure aurretik bizi zirenak
ezjakitun ziren gu harriduraz beha ginena

haiek zegiten ohiko zuten eran
killak ur azpian eta belak aidean
ohiko zuten eran nahiz guk ez jakin
Zarauzko badian, Ondarrun Atxazpin

hor azaldu ziren oin milaka urte bezala azaltzen zirenak
ez jakitun ziren, ezer ez zekiten, ez zekiten gutaz
ohiko zuten eran, killak ur azpian ta belak aidean
itsasargiak bailiran hor azaldu ziren izurdeak

itsasargiak bailiran hor azaldu ziren izurdeak
hor bihotz oneko piratak, marmoka erraldoiak, kantu zaharretako baleak
killak ur azpian ta belak aidean izurdeak

:::::



:::::
Zuzendaria: Alphax
Argazki zuzendaria: David Herranz
Kabuto estudioak egindako bideoklipa.

*ZART 2020
©Mikel Urdangarin