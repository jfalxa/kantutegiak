---
id: tx-3090
izenburua: Oroitzak Lupe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LYqWVE8jC2M
---

Gogoratzen naz beti
Itsas ertzean
esan neutzun "maitia"
Eta bihotza itzartu zan.
Ez, ez dot gu/ra a/haz/tu
Ez mai/ta/sun hau
nigana etor zana.
Ta bihotzak itxi ezin dau
Gu barriro itzuliko gera
Gogoratzen ordu atseginenak
Eskuak lotuta itsas ertzean
esango deutzut "maite zaitut"

La la la la la la la la la la
la la la la la la la la la la

Eskuak lotuta itsas ertzean
esango deutzut "maite zaitut"