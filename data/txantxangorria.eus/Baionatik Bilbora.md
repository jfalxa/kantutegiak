---
id: tx-2046
izenburua: Baionatik Bilbora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cJz2bwuFQsw
---

Badu nora heldu,
ez du zeri heldu,
itsaso hutsa da
nire herria.

Baionatik Bilbora
itsaso errea
putzu honek
ez du itoko
gure haserrea.

Baionatik Bilbora
itsaso barea
maria behetik gora
zer bakardadea.

Baionatik Bilbora
itsaso antzua
han zuen harro utzi
Okendok zantzua.

Baionatik Bilbora
lapa ta lanperna
handikan honeraino
Okendoren pena.

Baionatik Bilbora
itsaso petrala
aspaldi esan zidaten
besterik ez zala.

Baionatik Bilbora
itsaso aingura
nadila zure albora
oraindik ingura.

Baionatik Bilbora
itsaso zakarra
baldin baneki nora
kamino bakarra.

Baionatik Bilbora
itsaso pobrea
non du aurkitu behar
Euskadik hobea.

Baionatik Bilbora
itsaso euskara
Baionatik Bilbora
galduak ez gara.

Baionatik Bilbora
itsaso gorria
Baionatik Bilbora
gure memoria.

Bilbotik Baionara
alua itsasoa
iheri ez dakiena
hondarrera doa.