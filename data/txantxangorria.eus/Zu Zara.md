---
id: tx-420
izenburua: Zu Zara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8OrjR2nqRvk
---

Hitzak / Musika: Gorka L.P.

Zu baitzara Usurbetik sumatzen den izarra,
eki-malko-txirbilez jositako ortzadarra,
gau beltzetan ostargi den itsasoaren oldarra,
zu zara.

Zerua hatzekin ukitu genuen,
horma-goiko ezpainak musuka gesalduz.
Mostradore atzeko irriñoa salgai,
begi-kolpe bakarrez mundua baratuz.

Zu baitzara…

Zirrara-tanta bi masailak margotzen,
itsas brisaren kanten saminak arinduz.
Ihintza estaltzen duen gerizaren etsai,
kilimak bihotzean, hotzikarak sortuz.

Zu baitzara…