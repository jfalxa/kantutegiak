---
id: tx-1819
izenburua: Hau Duk Umorea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fYbD35LO_9o
---

Hau dek, hau dek,
hau dek umoria,
kontsolatzeko, kontsolatzeko,
euskaldun jendia

Kalian gora, kalian behera,
kalian gora zezena.
ai, ai, ai, ai
kalian gora, kalian behera,
kalian gora zezena.

Kalian gora, kalian behera,
kalian gora, kalian behera,
kalian gora, kalian behera,
kalian gora zezena.