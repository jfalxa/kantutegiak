---
id: tx-1945
izenburua: Dale Candela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KjXSLjI0bU4
---

Beti bidean aurrera, basamortuko lorea! Borrokatuz sortu, lortu duzuna!
Lurra da Anbotoko Mari, akelarreko zaindari! Sorgin festa!

Fruitu guztiak dastatu, suge morea laztandu Goza zazu gazte, aske ta libre
Dantza egin ezin bada, hau ez da zure iraultza! Gogor neska!

DALE CANDELA! Mundua zure eskutan daukazu!
DALE CANDELA! Maitia ez zaitu ezerk geldituko!

Sigue así, solo piensa en ti! 
Solo quiero que te quieras, yo lo digo así!
Lucha de frente ante cualquiera!
Lo que piensen de ti, lo que digan de ti!
No te importa, no te afecta, tu te sientes feliz!
Nunca paras de bailar, ni tampoco de luchar!
Porque tu eres guerrera, guerrera, guerrera

Itsuen herrian begia, Zarelako!
Eguzki izpien indarrez jo!
Gidatzen gaituen izarra, zarelako!
Olatuek bezela kolpatu!