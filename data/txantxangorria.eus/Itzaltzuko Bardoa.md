---
id: tx-1257
izenburua: Itzaltzuko Bardoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5GU-40Nk3vg
---

Ehunka uso 
Elkorretako inguruetan
Itzaltzuko bardoaren 
bila etsipenetan.
Gari ttanta 
bana dute mokoetan 
ezarri ahal izateko 
Gartxoten ezpainetan. 

Elkorretaren gailurrean 
irakatsi huen,
nor bere lurrean 
maizter izatea 
zein garesti 
ordaintzen den. 

Lur minak 
eragindako zina, 
lur ikararen 
mugikortasunean ere
zein iraunkorra den. 

Atzaparka bat lurrez 
hire seme maitearen
abots gardena itoaz 
nola herri bakoitzak 
bere defuntuak 
lurperatu ohi dituen. 

Bainan hik Gartxot 
ez huen Abraham baten 
ohorerik eta Mikelot 
euskal epikaren 
erresinola lurrari 
eskeni behar izan 
hioken opari. 
Atzerriko latin 
zaleen uztarpean 
biraozko hizkuntza 
basatiaz kantatzea 
ez baizen zilegi. 

Ez duk eremu hauetan 
aingeru zerutarrik
Otsaburu zakur anaikorra 
ez besterik,
nork lehunduko dik bada 
hire nahigabea 
gure lur gozoak ez ezik. 

Arrano igarlea hor hago 
zeharka begira
heriotzaren getari. 
Hire hegalaren azpian 
joan nahi nikek,
hire xirularrutik 
iheska irriztan 
joandako eresien bila 
gure adimena 
hire oroitzapenez 
sendotu dedin. 

Ai Frai Martin, 
hire zitalkeriak 
bardoa hilen dik 
bainan haren 
sumindura hilezkorra 
ondotik izango duk 
eta hire maitinak 
deabruak kantatuko 
dizkik. 

Gaur mende 
batzuen ondotik
kondairaren ohiartzuna 
entzun nahirik
lur honi so, 

hire Altabizkarko kantuaren
egarri nauk, Gartxot, 
nere oharmenaren 
ilunabarretan 
noiz entzungo zai. 

Elkorretako elurrak 
urtzen direlarik
errekan behera datozte, 
garden bezain bihurri, 
lurraren negarra iduri. 

Txori kantazale ederra 
nun ote haiz kantatzen? 
Harrien negarra haizeak darama 
Zaraitzuko ibarretan barna.