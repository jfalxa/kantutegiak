---
id: tx-847
izenburua: Assentiré De Grat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9QdyVBV43zg
---

Assentiré de grat, car només se'm donà
d'almoina la riquesa d'un instant.



Si podien, però, durar

la llum parada, l'ordre clar

dels xiprers, de les vinyes, dels sembrats,

la nostra llengua, el lent esguard

damunt de cada cosa que he estimat!



Voltats de por, enmig del glaç

de burles i rialles d'albardans,

hem dit els mots que són la sang

d'aquest vell poble que volem salvar.



No queden solcs en l'aigua, cap senyal

de la barca, de l'home, del seu pas.

L'estrany drapaire omplia el sac

de retalls de records i se'n va,

sota la fosca pluja, torb enllà,

pels llargs camins que s'esborren a mar.



Itzulpena

Gogoz onartuko dut, zeren ez baitzitzaidan,

limosna bezela, instantearen aberastasuna, baizik eman.



Iraun zezakeen, ordea, argi geldituak

altzifreen, mahastien, soroen ordena zehatzak,

gure hizkuntzak, maite izan ditudan

gauzei buruzko begirada mantxoak !



Ikaraz inguraturik, burla eta

bufoien irri kaskarren jela artean,

salbatu nahi dugun herri zahar honen

odol diren hitzak esan ditugu.



Ez da ildorik gelditzen uretan,

gizonaren, bere pausoen, txaluparen

inolako seinalerik ez.



Trapero arrotza, oroitzapenen adabakiekin

zakua bete eta joan egiten da

euri zakarraren pean, zurrunbiloez haruntz,

itsasoan ezabatzen diren bide luzeetan barrena.