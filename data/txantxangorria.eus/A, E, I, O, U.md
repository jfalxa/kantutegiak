---
id: tx-487
izenburua: A, E, I, O, U
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L_hA-a7F0lw
---

A, e, i, o,u,
ama meriendea behar dogu,
arrautze bi ta koipetsu,
aita etorri orduko dana jango dogu.
A, e, i, o, u,
ama meriendea behar dogu
txokolate ta opiltxu
bestela eskolan ikasiko ez dogu.
A, e, i, o, u,
ama meriendea behar dogu
baso esne ta pasteltxu
ha janda be ondino gehiago gura dogu.