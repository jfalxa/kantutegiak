---
id: tx-2773
izenburua: Zirt Zart
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ArkBttiQumU
---

Zirt zart,
gogoz egin zan par
pardel ezkontza baiñon
hobe den neskazahar.

Hamaika andre gaztek
etxian badute
giltza gabe pardela
salgai eta merke
baldin erostunikan
baldin ba luteke
eskerrik askorekin
salduko luteke.

Lalaralalala...

Zirt zart,
gogoz egin zan par
pardel ezkontza baiñon
hobe den neskazahar.

Hamaika andre gaixo
ikusten dittut nik
beren bizi moduaz
odoi asperturik
senarraren pardela
eraman eziñik
neskazaharrak ez dute
horrelako lanik.

Lalaralalala...

Zirt zart,
gogoz egin zan par
pardel ezkontza baiñon
hobe den neskazahar.

Ez gaude garai hontan
ezkongairik gabe,
ugari dittut baiñan
gustokorikan ez
Luis ta Anbrosio
Tomas eta Andres
ja, jai, gehiago ere
badabiltza galdez.

Lalaralalala...

Zirt zart,
gogoz egin zan par
pardel ezkontza baiñon
hobe den neskazahar.