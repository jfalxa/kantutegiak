---
id: tx-939
izenburua: Arraun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TTNA0zJtcx0
---

Noizbait helduko gara
Arraun arraun
Atzeraka goaz
Noizbait helduko gara
Arraun arraun
Atzeraka goaz
Ez dirudi erraza
Ez dirudi erraza
Mugaz muga helmuga jotzea
Mugaz muga helmuga jotzea
Ahots bat entzuten da
Indarrak ahultzean:
Utzazu arrauna utzazu
Zoaz haize alde
Zur hila badabil
Uraren azalean...
Baina nork ez daki
doala hondora?
Ez gara inoiz helduko
Arraun arraun
Deus ez da mugitzen
Behar dugula itzuli
Erdia egin eta?
Arraun arraun
Ezin gara geldi
Arraun arraun
Arraunlari gara.