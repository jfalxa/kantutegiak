---
id: tx-2835
izenburua: 1987 Urriak 3
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hC1j5ao4Mlw
---

Urdin ilunez jauntzirik, argisentiko mamuak
Etxetan, ate leihoak puskatuta, sartu dira...
Ilunpean kortxilatu emazteak eta haurrak
Gizonak aldiz eraman Judu gaixoen modura...

Oroitmen ilun-hitsako urrats-soinu neurkatuak
Aterbeak suntsiturik, kaleaz jabetu dira...
Urriak hiru larunbata: Herri hunen Historian
Dolorezko hizkietan idatzirik gelditu da!

Hegoaldeak Madrileri errepresioaren zerga.
Iparraldeak Pariseri eskerrenonaren murduka...
Salkeriaren eskukada: ezpataren nabel hotza!
Estado arrazoiari ukatzen diogun eskaintza!

Gure_etorkizun bakarraz gira gu jabetu nahi!
Bortizkeri, torturari indar guziz kendu nahi!
Gure semeeri eman sortu izanaren gogoa!
CASSIN seme famatuaz, zer egin duk, oi

BAIONA???