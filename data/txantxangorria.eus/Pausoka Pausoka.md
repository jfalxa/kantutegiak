---
id: tx-1558
izenburua: Pausoka Pausoka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n3J_2s_X-ko
---

pausoka pausoka bagoaz aurrera
poliki poliki betez gure ametsak

lagun eta adiskide, denak bat eginik
barre eta algara, ba al zatoz gurekin
parranda eta dantza, ez da inoiz amaitzen
urtez urte zureekin jalgune altxatzen

pausoka pausoka bagoaz aurrera
poliki poliki betez gure ametsak

zeru eta lautadak, doinuz betetzen
eguzkiaren izpiek den dute sendatzen
gaur laguntzen banauzu zure maitasunez
gu bion artean pentsakerak aldatzen

pausoka pausoka bagoaz aurrera
poliki poliki betez gure ametsak

eskuak zeruraaaa....jeeeei
eskuak lurreraaaa....jeeeei
eskuak alde bateraaaaa.....jeeei
eskuak beste alderaaaa.....jeeei

pausoka pausoka bagoaz aurrera
poliki poliki betez gure ametsak