---
id: tx-1357
izenburua: Maltzeta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XSKtXLJFgrk
---

Kantari hasi behar gaurko neskatxakin
kontu izan bihar oingo batzuekin.
ez pentsau giro danik holakuekin


Kantari hasi behar gaurko neskatxakin
kontu izan bihar oingo batzuekin.
mutilak engainatzen diran hoiekin.

Galtza estuak dituzte telaz oso finak
kolore zuri gorri berde urdinak,
nabarmen dituzte ipurdi izkinak

Galtza estuak dituzte telaz oso finak
kolore zuri gorri berde urdinak,
mugitzen dituzten mutilaren grinak.

Akordatzen al zera lehengoko eguna
pixka bat esan arren ez esan dena,
hura zen momentua pasa genduna

Akorda/tzen al zera lehengoko eguna
pixka bat esan arren ez esan dena,
juntatu ginanian biok elkarrena.



Bat, bi,_hiru, lau, hiru, lau, hiru, lau ta
mutil zahar hobe degula ta baita
bost, sei, zazpi,
ez gabiltza batere gaizki.

Bat, bi,_hiru, lau, hiru, lau, hiru, lau ta
mutil zahar hobe degula ta baita
bost, sei, zazpi,
ez gabiltza batere gaizki.