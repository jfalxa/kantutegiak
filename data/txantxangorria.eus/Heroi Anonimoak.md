---
id: tx-489
izenburua: Heroi Anonimoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J-GoIkIGH9Y
---

Gorbata estutu
keinuak orraztu
arnasa sakon hartu
maskara ostean

Baina arren utzi alde batera
heroi anonimoak
paparrean dominik ez dute nahi
soilik elkartasuna

Ardi izutuak
artzainen txistupean
jokatu (e)zazue nahi beste
otsoaren irudiaz

Baina arren utzi alde batera
heroi anonimoak
paparrean dominik ez dute nahi
soilik elkartasuna

Begirada umelak
eman edo kendu
zapaltzailea izan
edo zapaldua
putakumentzat
neurrira eginadako
jantzi bat baino ez da
mundu argi eta ilun hau