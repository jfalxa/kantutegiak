---
id: tx-3216
izenburua: Aita, Aita -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pmh4cfSwAV4
---

Lotara joaten naiz
haren besoetan
argitxo bat dago
gela ilunean
ohean sartuta
hura ondoan
irakurtzen hasten da.

Aita, aita
ipuin bat kontatu
aita, aita
amets egin nahi dut.

Ipuina entzuten
panpinen artean
txikitxoa goxo
nire besoetan
handitxoa dago
beti ondoan
nire panpina handia.

Aita, aita
kontatu beste bat
aita, aita
panpitxo handia.
Ipuinarekin 
hasi naiz amesten
poliki poliki
begitxoak ixten
aita bera ere
ixilduz doa
oso goxo dago eta.

Ama, ama
begira aitatxo
ama, ama
lotan gelditu da.