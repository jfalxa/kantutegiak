---
id: tx-1990
izenburua: Nazi De Fresa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xuzYdyeoOB8
---

Triste bizi naiz eta hilko banintz hobe".
Euskal grungea amerikarra baino lehen
Arzallusen teoria psicodelikoak, "si Teruel existe Zuberoa también".
.
Sodomiten hogeitabatgarren mendeko espainiar mistika berria modan badago,
bizkaitarrak berriro saiatuko dira eskupilota jolasten frontoietan.
.
Baditut bongiak eta kriston kojoiak,
Ez didazue kenduko maite dudan guztia.
Defendatuko dut nire arbasoen ideia botila beteta dagoen bitartean.
.
"Barkatu gure zorrak" idei xelebrea.
Bizardunen tabernan ozenki entzuten zen izugarrizko batzoki festa bat zegoela.
Hori bai ikara hara hurbiltzerakoan!
.
Baziren beltzaranak ketaminaz beteta, aurresku elektronikoak,
DANTZAN! DANTZAN! DANTZAN!
Jeltzaleak saltoka neskatxaz jantzita, piruletak oparitzen santa Agedan.
.
Baditut bongiak eta kriston kojoiak,
ez didazue kenduko maite dudan guztia.
Defendatuko dut nire arbasoen ideia botila beteta dagoen bitartean
.
Ohean etzanda lo ezinean, gotelea zenbatzen eta orain komeriak.
Amesgaiztoz inguraturik izerdi patsetan, nahiz ta begiak ireki beti hortxe daukat.
.
Nafar Judas batek, irrifartsu ta trebe Gernikako estatuto astazakila sinatzen.
Mondragon Cooperativa "el cuarto Reich" finanziatzen, Xabier Perez de Euskitze jotak kantatzen.
.
Baditut bongiak eta kriston kojoiak,
ez didazue kenduko maite dudan guztia.
Defendatuko dut nire arbasoen ideia botila beteta dagoen bitartean
.
Meliton manzana, ikatza karbon,CABRON! mujer andrea eta hombre gizon.
Ez ziguten irakatsi nola zen el maricon, auskalo Txirri Mirri edo Txiribiton.
.
Marxismoa, speed, txistorra bokatak, misoginia, darwinismo, peste bubonikoa.
Edozein medizina herenegun gosaltzeko Junkeraren onanismoa ez gaixotzeko