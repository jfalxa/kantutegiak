---
id: tx-1993
izenburua: Gaberako Aterbea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4qXfMzKqocY
---

Kontatu didate Nueva York-en
Broadway eta 26 karrikaren kantoian,
Negu gorrian, gizon batek gabero
jendeari otoi eskatzen
aterbea bilatzen duela
bilutsirik daudenentzat.

Mundua ez da era hortan aldatzen
Gizonen hartu-emanak ez dira hobekitzen
zama-aroa ez da hola laburtzen
Baina gizon batzuek gau batez, ohea dute,
aterbean haize otzik ez eta
bereri zijoakien elurra, karrikan ari da.
Liburuaren irakaspenik ez ahaztu gizona!

Gizon batzuek gau batez ohea dute,
aterbean haize otzik ez eta
bereri zijoakien elurra, karrikan ari da.

Bainan mundua ez da era hortan aldatzen
Gizonen hartu-emanak ez dira hobekitzen
zama-aroa ez da hola laburtzen.

(Hitzak: Bertolt Brecht; Musika: Mikel Laboa)