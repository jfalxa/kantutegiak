---
id: tx-1171
izenburua: Lau Krabelin Gorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EpgLzvOtpI8
---

HISTORY (EUSKERA - ENGLISH)
1984ko martxoaren 18a
Rosa Jimeno modu ilegalean atxilotu zuen Polizia Nazionalak. Ez zuten atxiloketaren berri eman eta, tortura bidez, Iparraldean zeuden Komando Autonomo Antikapitalistetako kideekin hitzordua jartzera behartu zuten.
1984ko martxoaren 22a
Rosa Pasaiara eraman zuten. Badia poliziaz inguratuta zuten. Hanka sokaz lotu zioten seinalea egitera behartu zuten. Gaueko hamarretan iritsi ziren bost kideak Iparraldetik, zodiac batean. Rosa zegoen harkaitzetara iristear zirela tirokatu egin zituzten eta Pedro Mari Isart “Pelitxo” eta Jose Maria Izura “Pelu” momentuan hil zituzten. Bizirik harrapatu zituzten Dionisio Aizpuru “Kurro”, Rafael Delas “Txapas” eta Joseba Merino . Identifikatu ostean, Merino alde batera eraman eta “Kurro” eta “Txapas” harkaitzetan bertan fusilatu zituzten. Guztira 113 bala zulo aurkitu zituzten lau eraildakoen gorputzetan.

32 urte beranduago kasuak judizialki argitu gabe jarraitzen du. Hiltzaileak ez dira inoiz epaituak izan.

EGIA, JUSTIZIA ETA ERREPARAZIOA!
PASAIKO SARRASKIA ARGITU!

18th of March 1984
Rosa Jimeno, was illegally arrested by the National Police Force of Spain. The arrest was not announced and the police forced Jimeno to make an appointment with the members of Komando Autonomo Antikapitalistak (Anticapitalist Autonomous Commandos), from Iparralde (North Basque Country) under torture.
22nd of March 1984
Rosa was taken to Pasaia surrounded by 100 police officers. She was tied up and threatened with death unless she made the pre-established signal. The signal was lighting a torch three times on the rocks in the bay. The other members arrived at 10 PM on an inflatable boat from Iparralde. When they were about to reach Rosa, they were shot and Pedro Mari Isart, a.k.a. Pelitxo, and Jose Maria Izura, a.k.a. Pelu, died. Dionisio Aizpuru, a.k.a. Kurro, Rafael Delas, a.k.a. Txapas, and Joseba Merino were caught alive. After identifying them, Merino was taken apart and Kurro and Txapas were shot from a one-meter distance. 113 bullet wounds were counted in the four corpses.

32 years later, the case is still unsolved and the murderers have not been prosecuted.

TRUTH, JUSTICE AND REPARATION!
SOLVE THE MASSACRE OF PASAIA!
Gaztain estudioak-en Eñaut Gaztañaga teknikariak grabatua.
Bideokliparen egileak: Yuri Agirre, Xabier Otamendi eta Erik Aznal
Dantzaria: Maitane Turrillas
Txistulariak: Martin Alegria eta Jose Inazio Elias
Letra:
LAU KRABELIN GORRI
Ezkutuan gatibu hartuta
Inork ez daki ezer.
Pizti urdinen eskuetan
Ordu luzez infernuan.
Kideekin hitzordua
Lotzera behartua.
Gizon urdinen eskuetan
Pasaiako bidean.

Badiako harkaitzean
Soka bat lotuta hankan.
Seinalea egin behar
Kañoien mehatxupean.

Iparraldetik txalupan
Kideak agertzean.
Denbora gelditu da
Foko denak piztean.
Norabide bakarrean
Tiroak zaparradan.
"Cuatro menos" diote
Ordenak beteta daude

4 krabelin gorri, zuen irudiaren gainean.
4 krabelin gorri, jarriak zuen oroimenean.

Albisteak iristean
Malkoak begietan
Herriko semeak hilotz
Herrira ekartzean
Sua herriaren bihotzetan
Sua kale eskinetan
Gaueko ohiua ke artean
Herriak ez du barkatuko!

Agintariak bitartean
Ospatzen bere bulegoetan
Eskuak garbitzen dituzte
Egia zikinduaz

4 krabelin gorri, zuen irudiaren gainean
4 izar berri, euskal herriko zeru gorrian!