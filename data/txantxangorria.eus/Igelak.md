---
id: tx-1433
izenburua: Igelak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fk9nC7sLUTI
---

Salmenta txarra egin zendun
arimie diruaren ordez
errukirikez dozu euki
erpiek atara dozuzenien.

Bizio danak hatrrapeu dozuz
zelan urten zeuk jakingo dozu
eskorpioie ikutute zagoz
arrain haundidxex txikidxe jan dau.

Lagunik barik,
dirurik barik,
demoniñoak daroan arima
gerorik barik agertzen zara
zuretzat dana zerotik hasten da.

Bidxer eguzkidxek urtengo dau
gabak ez deu ardure
ta kantu berri bet entzuten
esnatuko zara.
Nork maiteko zaitu ibai erdidxen
eta zugaz erori
ta azken melodi hau dantzan egin
amaitzen doan artien.

Hatrapeute zagozela uste dabe
ez da egidxe
leihdxo bat zabalik dago
aukera bat baino ez dago.

Jefie, jefe eta patroi, patroi
hemen goitik behera mailak dagoz
diruaren jonkie putie
lagun bako eskorpioie.

Lagunik barik,
dirurik barik,
demoniñoak daroan arima
gerorik barikihesi zoaz
zuretzat dana zerotik hasten da.

Bidxer eguzkidxek urtengo dau
gabak ez deu ardure
ta kantu berri bet entzuten
esnatuko zara.