---
id: tx-1660
izenburua: Una Paraula
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-V9-WNYFmi4
---

UNA PARAULA

Des de les blanques parets,
que dibuixen el destí,
avui és onze de setembre,
catorze mesos de setge.

Vaig sortir de casa
amb gairebé vint-i-cinc anys,
per defensar
una bandera i un carrer,
una mirada, una ciutat.
un amor prohibit, un somni,
una canço...

Terra lliure
terra brava
dues llengües,
una paraula: llibertat...askatasuna

Va ser dilluns de mercat,
al costat de l'arbre mil•lenari,
ocells de foc,
destrossaren el silenci.

Vaig arribar a casa
vint anys després,
per defensar
una bandera i un carrer,
una mirada, una ciutat.
un amor prohibit, un somni,
una canço...

Bi hizkuntz, hitz bakarra,
bi herri, hitz bakarra.