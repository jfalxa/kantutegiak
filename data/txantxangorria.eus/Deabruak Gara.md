---
id: tx-964
izenburua: Deabruak Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6kq3eTBxPig
---

Huntzatarrok Glasgowen izan ginen Celtic Connections izeneko jaialdian. Hemen duzue laburpen bideoa.

Gaizka Peñafiel argazkilari eta trikililari Lezamarrak burututako bideoa.

Huntza - Deabruak gara
'Xilema' bigarren diskoko lehenengo kantua. 

Haritz Harreguy estudioan grabatua eta nahastua.
Victor Garcíak masterizatua Ultramarinos Mastering-en.

DEABRUAK GARA

DEABRUAK GARA,
DEABRUAK GARA,
DEABRUAK GARA!

Hautsez betetako zerua 
zerua bada,
beltzez jantzitako hodeia 
hodeia bada,
ta ezin badegu margotu 
gure erara
koloretako maskarekin 
gauden deabruak gara.

DEABRUAK GARA,
DEABRUAK GARA,
DEABRUAK GARA!

Zuloz betetako mendia
mendia bada,
sugeak nonahi ibiltzea
zilegi bada,
ezin al dira gauzak dauden
bezela laga…
lurrak hankaz gora jarri ta
zure deabruak gara.

DEABRUAK GARA,
DEABRUAK GARA,
DEABRUAK GARA!

Zuek jarritako aukera
onena al da?
Beste bidetik joatea
okerra bada,
oker egiten den bidea
zuzena bada, 
gauzak hala diren artean,
zuen deabruak gara!

DEABRUAK GARA,
DEABRUAK GARA,
DEABRUAK GARA!