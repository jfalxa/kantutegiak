---
id: tx-3003
izenburua: Bagare
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/usnAq0od7f8
---

Araban bagare
Gipuzkun bagera
Xiberun bagire
Ta Bizkaian bagara
Baita ere, Lapurdi ta Nafarran.

Guziok gara eskualdun
guziok anaiak gara
Nahiz eta hitz ezberdinez
Bat bera dugu hizkera.

Bagare, bagera
Bagire, bagara
euskera askatzeko
oraintxe dugu aukera

Araban bagare...

Herri bat dugu
osatzen
eta gure zabarkeriz
ez daigun utzi hondatzen.

Bagare, bagera,
bagire, bagara
Euskadi askatzeko
oraintxe dugu aukera.