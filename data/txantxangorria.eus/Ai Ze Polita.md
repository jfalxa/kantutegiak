---
id: tx-1185
izenburua: Ai Ze Polita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sU2VONdbGE4
---

Arrantzaleak ta baserritarrak,
langileak, danok anaiak,
itxas ertzetik ta legorreraino
izan gaitezan danok alaiak.

Lekeitio, Ondarru, Bermioko portua,
Uribe Kosta eta Ezkerralde,
gure jendea bai dala sanua,
beti dago euskararen alde.


Ai, ze polita dan gure Bizkaia!
Ai, ze polita Anbotoko Dama!
Busturi, Lea-Artibai ta Arratia,
Bizkaia osoa da gure ama. (bi)


San Antonio Urkiolan dago eta
mutil zaharrak daukaguz pozik,
Bakioko txakolina hartuta
neska zaharrak engainaturik.

Pelotariak, baita aizkolariak,
txinga-erute ta sokatira,
arraunlari ta txirrindulari,
ta gora beti Athletic-ari.


Ai, ze polita dan gure Bizkaia!...