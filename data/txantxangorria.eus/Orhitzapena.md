---
id: tx-123
izenburua: Orhitzapena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qdaha6AzyJ4
---

Orhitzapena 
Astearte arratsaldian orano oro osagarri, 
« gai hun eta bihar artino » erran zeneikün orori. Astizken goiza jeikitzen, üdüri oro batzarri, 
Txoriak kantan ari ziren bena zü ez iratzarri. 
Heriotze traidore krüdela igaran zen arte hortan, Zützaz jelos zen zinelakoz hanitx gora uhuretan, 
Orai familia hortxek da zuri dolüz nigarretan, 
Eta haiñbeste adixkide harritürik doloretan. 
Urthatsez zük egin suhetak ez dütügü ahatzeko, Esperantxaz betherik ziren denentzat urthe güziko, Posible deia, zü haiñ afable, zü haiñ hardit, zü haiñ sentho, Gütarik phartitü zirela agurak egin ordüko ! 
Haiñbeste lan egin onduan orai badüzü phausia, 
Zure orhitzapen goxua denek dezagün begira. 
Satisfet juraiten ahal zira eginik obra handia, 
Oraikuan zure aldi zen, nuiz dateke guria ? 

Hitzak : ETXAHUN Iruri Müsika : Jean ETCHART