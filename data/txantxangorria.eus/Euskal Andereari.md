---
id: tx-2815
izenburua: Euskal Andereari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ETb4ZABZDxw
---

Zahar hori izan zaitugu
euskal eremuan
zure bularren sua
gozatu genuan
zure bularren sua
gozatu genuan.
 
Ordudanik herriak
izan du zer pentsa
arima estrainu batek
zolatu dio bihotza
arima estrainu batek
zolatu dio bihotza.
 
Zu zara zu andere
zu zara neska gazte
bihotz eskaintza oparoz
herriaren egile
bihotz eskaintza oparoz
herriaren egile.
 
Askatu zaitez aska
izan zaitez libre 
gizaldien kateak
ez baitzaitu ase
gizaldien kateak
ez baitzaitu ase.
 
Zuk eta nik batera
badugu zer egin 
izarren hitzak dio
goazen elkarrekin
izarren hitzak dio
goazen elkarrekin.
 
Gorputz eder batek
badu zer esanik
herri baten burrukak 
badu zer eginik
herri baten burrukak 
badu zer eginik.
 
Gorputzak elkar eza
bihotz ausarta
elkarren maitasuna
herriak du falta
elkarren maitasuna
herriak du falta.
 
Askatasuna herriak
askatasuna guk
giza seme andere
denok behar dugu
giza seme andere
denok behar dugu.
 
Harkaitz baten sendoa
haitzak urtzen du
haitzaren indarra
urak baratzen du
haitzaren indarra
urak baratzen du.