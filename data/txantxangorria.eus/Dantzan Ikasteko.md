---
id: tx-2541
izenburua: Dantzan Ikasteko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bgNr80vt1Vs
---

Dantzan ikasteko
dantzan egi/teko
korroan bildu dira
Olatz eta Eneko.
eskutik loturik
gerritik heldurik
kili-kili egin dio
Madalentxok Manuri.

Antxon!
jozak tronpeta!
Txomin!
non duk konketa!
Laran, laran, laran,
laran, laran,  laran, larala