---
id: tx-3309
izenburua: Zarena Zarelako -Sugan-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-G8PC_T9qAM
---

Eutsi, borroka bakarrari
izatera heltzeko, zarena zarelako
hutsez, bakar bat gehiago.
Nagusit zaitez, zeure lekua baduzu
bermatu, lotu, bilatu, aurki ezazu.

Zuk, zeure kabia hornituz
hitz merke ta txiroez at 
jaten eman buruari, ez inori, zeuri beti.
Jakin zuloan topatzen,
barnetik ateratzen,
zeharko, itzuliko, garrasika.

- Eutsi...-
- Segi, zuzen bide ertzean

gaztigatu dizugu, aurrera jarraitzeko 
atzera ez begiratzeko.
Egunez egun zihur
nahi hura ikusten dugu
ez arduratu, irtenbiderik ez badu.

ustez jakin badakizu
nolakoa zaren zu
galdetzu gero akabo, mututa, ixilik, zero.
Jaklin ezean onartu
uste ere ezin dela
izan, sinestu, aitortu, ezagutu

- Eutsi...-
- Segi...-