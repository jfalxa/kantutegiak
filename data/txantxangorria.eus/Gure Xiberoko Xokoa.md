---
id: tx-167
izenburua: Gure Xiberoko Xokoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qUb9bLsW3KE
---

Xiberotarra ez da ez beti alagera
Erriaz üsü gorde dü barneko nigarra
Gure Herria gisala badoa galtzera
Etsipena bihotzetan ez utz garhaitzera.

Ez da hanbat aberasten Xibero xokoan
Bena heben bizitzea zer goxotarzüna
Mendi ederren emaitza, hau gure fortüna
Ibar amatto maitea, zure ohakoan.

Ama zenaren segida segürta dezala
Esposatü nahi dügü Xibero berria
Gur’ emaztea bezala maitatüz Herria
Bere uduriko haurrak egin ditzogüla.

Etxek dezagün bizirik euskaldun gogoa
Kantu, dantza, maskarada eta pastorala
Zaharren ezpiritiak lauküna gitzala
Eman jeieko bai eta laneko mengoa.

Gure Xibero xokoa, denetarik hurrun
Peko aldetik odeiez ardürenik ulhun
Bena zeliaren aizo goiti so eginez
Izarren harrozale da zinka, irrintzinez

InprimatuLagun bati bidaliPDF