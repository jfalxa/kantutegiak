---
id: tx-2481
izenburua: Zirriz Mirriz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q4L9QtEvnXk
---

Musika: Itxaso Azkona
Letra: Koldo Celestino


Berdin berdinak ez gara denok
ta ez dago inor baztertuta
alaitasunez ibiltzen gara
batuta eta nahastuta,
hainbat kolorez, hainbat kulturaz
daukagu herria osatuta,
ume guztiak sentitzen gara
pozik eta maitatuak.

Zirriz-mirriz elkarrekin biziz,
zirriz-mirriz euskaraz bizi

Milaka hizkuntza, milaka herri
badira gaurko munduan,
Hassan, Tamudi, Dalai ta Itsaso
batera daude eskolan;
bertan jaioak, heldu berriak,
aberastuz gure artean,
umeak pozik bizitzen gara
bat eginda Euskal Herrian.