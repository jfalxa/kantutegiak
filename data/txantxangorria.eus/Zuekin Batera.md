---
id: tx-572
izenburua: Zuekin Batera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yKYVmHF6Ev0
---

Zenbat aldiz marrazten diguten
joan zen hartatik etorkizuna
Zenbat aldiz ekartzen diguten
nahastu nahi dugun oroitzapenak
Zenbat aldiz jarriko digute
epailearen aurreko hitz orduan
baina gu hor egongo gara
zuekin batera

Zenbat aldiz lapurtzen diguten
herri bat eraikitzeko denbora
Zenbat aldiz asmatu ohi duten
artera itzultzeko aitzaki bat
Zenbat aldiz aldatzen dituzten
haien erara gure istorioak
baina gu hor egongo gara
zuekin batera

Ta ez gara sartuko berriro
haiek nahi duten zuloetan
altxatuko ditugu besoak
ukatzeko onartezinak
ta aterako dugu benetan
barruan gelditzen zaigunak
gu beti hor egongo gara
zuekin batera
zuekin batera

Zenbat aldiz ospatuko nuken
juezen eskuak garbi daudela
Zenbat aldiz adituko nuken
hau behingoz amaitzeko aholku bat
Zenbat aldiz esango nizuken
etxean lasai nahi zaitudala
baina gu hor egongo gara
zuekin batera

Ta ez gara sartuko berriro
haiek nahi duten zuloetan
altxatuko ditugu besoak
ukatzeko onartezinak
ta aterako dugu benetan
barruan gelditzen zaigunak
gu beti hor egongo gara
zuekin batera

Ta ez gara sartuko berriro
haiek nahi duten zuloetan
altxatuko ditugu besoak
ukatzeko onartezinak
ta aterako dugu benetan
barruan gelditzen zaigunak
gu beti hor egongo gara
zuekin batera
zuekin batera