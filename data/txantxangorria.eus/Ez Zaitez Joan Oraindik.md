---
id: tx-303
izenburua: Ez Zaitez Joan Oraindik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YZljbotcihY
---

Ez zaitez, ez zaitez,
ez zaitez joan oraindik.
Ez zaitez, ez zaitez
urrun nigandik.

Baditut zuretzat gordeak
bihotz hautsiaren zokoan
irrien negarren sorteak,
milla bat hor dago jokuan.

Baditut zuretzako hemen
abenduko euri uharrak,
haize hegoak duen kemen
hura ebasteko bularrak.

Ez zaitez, ez zaitez,
ez zaitez joan oraindik
Ez zaitez, ez zaitez,
urrun nigandik,

Baditut zuretzat lurrean
atzeman artoen bizarrak
eta gauaren beldurrean
klixkan ari diren izarrak.

Baditut zuretzat emanak
sola zintzoetan urratsak.
ibai gardenetan edanak
maitasunak utzi ur hatsak.

Ez zaitez, ez zaitez,
ez zaitez joan oraindik.
Ez zaitez, ez zaitez
urrun nigandik.

Ez zaitez, ez zaitez,
ez zaitez joan oraindik.
Ez zaitez, ez zaitez
urrun nigandik.

Baditut zuretzat bilduak
mendietan ardi hezurrak,
herri baldarrak ixilduak
dituen egien gezurrak.

Ez duzu sufritu nahi hor
uztailak hitz eman kaldetan:
halatan zaude muturik gor
nere bi negarren maldetan.

Eta nik ez dakit nolatan
behar dudan zurekin juan;
dudarikan gabe holatan
betiko huts hori goguan.

Ez zaitez, ez zaitez,
ez zaitez joan oraindik.
Ez zaitez, ez zaitez
urrun nigandik.