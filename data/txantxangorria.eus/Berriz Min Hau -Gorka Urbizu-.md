---
id: tx-3260
izenburua: Berriz Min Hau -Gorka Urbizu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e818AijabD8
---

Min hau neure-neurea
dudala badakit 
baina zauria ez dut 
topatzen inundik; 
benetan al datorkit 
poza erraietatik 
ala ez ote nabil 
neu bere atzetik? 

Honezkero ikasia 
nago gerizpean 
itzalari azalean 
eusteko bestean 
aldiz uzkurtu eta 
puztu sahiespean 
bihotza somatzen dut 
eguzki eskean 

berriz min hau 
luze dirau (bis) 

Gudu zelai bihurtzen 
denean norbera 
irabaztea zer da 
zer garela? 
Nik ez dut nahi 
hemendik onik atera 
baizik eta bizirik 
sentitzea bera 

berriz min hau 
luze dirau (bis