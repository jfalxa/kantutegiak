---
id: tx-1126
izenburua: Eñaut Bidegorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_eVbHdyzYt8
---

Etzian aberatsa
etzian pobrea...
Bainan haren bihotza
zer urrezko kalitza!...

Jakin duka berria
Enaut bidegorria hil dela!
Atzo dena irria
Nork erran egun hilen zela!
Lurreko itzulia,
Harek egin loreak bezala!

Gurekin herriz herri,
pestaz ibili duk frangotan.
Neskek harentzat begi
hain zukan trebe fandagoetan!
Oto batek moztu dik,
Gaixo Enñaut, hogoi urtetan!

Adixkide bat zer dan
Nik bihar dut hori jakinen.
Bihar bakar bakarrik
Eñaut gabe bainaiz gelditzen,
Ai! Eñaut adixkide
hik baninduk hire ganatzen.