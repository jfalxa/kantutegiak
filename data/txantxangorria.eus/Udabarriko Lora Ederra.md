---
id: tx-1792
izenburua: Udabarriko Lora Ederra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oj6NhBM-HYk
---

Udabarriko lora ederra
non izan zara neguan?
Abazuzien bildurragaitik,
sarturik lurren barruan.
Ai, ei, bildurragaitik,
sarturik lurren barruan,
ai, ei, lora ederra,
non izan zara neguan.

Idi zuriek bajaten daude
mendi altutik otea,
zuriz ta gorriz plegaurik dago,
damatxu, zure dotea.
Ai, ei, plegaurik dago,
damatxu, zure dotea,
ai, ei, bajaten daude
mendi altutik otea.

Udabarriko lora ederrak
erosten dira negura,
beti gustora biziko danik
ez da jaioten mundura.
Ai, ei, biziko danik
ez da jaioten mundura,
ai, ei, lora ederrak
erosten dira negura.

Ez da jaio ta jaioko ez da
holango imajinarik
zureganako amodioa
niri kenduko deustanik.
Ai, ei, amodioa
niri kenduko deustanik,
ai, ei, jaioko ez da
holango imajinarik.