---
id: tx-326
izenburua: Azaroan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0AxWYm5JfmM
---

Intzaren 'Ilundu aurretik' debut diskoko lehenengo kanta, 2021eko azaroaren 22an argitaratua. 

Gitarrak: Aritz Pardina
Teklatua: Ane Bobadilla
Baxua: Jesus Perez
Bateria: Imanol Arruti
Musika eta hitzak: Intza 

Jarraitu eta entzun:
 
AZAROAN
Zurekin dena hasten da
galdera luze batetik.
Galdu egin nintzen
ezin jakin noiz eta zergatik.

Abuztuko agurrari
berandu onartu nion
bere hartan zekarren
gezur bidea.

Zure esanetan gesalduz 
nere uhinak.
Eskuetan deseginaz
eguneroko ahaleginak.
 
Iraileko brisak 
hartu zituen orduan hatsak
astez aste luzatu ziren 
arnas hotsak.

Urriko haizeak
 jakinarazi zidan
atzetik zekarren 
hotz brisaz.

Azaroan heldu nion
zenidan gorrotoari.
Zuk behintzat
jakingo bazenu zergaitik.

Zure esanetan gesalduz 
nere uhinak.
Eskuetan deseginaz
eguneroko ahaleginak.

© & ℗ 2021 Intza