---
id: tx-806
izenburua: Xori Erresiñula
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zWBdJx6xpbc
---

Xori erresiñula üdan da kantari,
zeren ordian beitü kanpuan janari;
negian ezt` ageri; balinban ezta eri!
dan jin baledi,
konsola nainte ni.

Xori erresiñula ororen gehien,
bestek beno hobeki har´k beitü kantatzen;
harek dü inganatzen mündia bai tronpatzen;
bera eztüt ikusten,
bai botza entzüten.

- Amak ützi nündizün bedats azkenian;
geroztik nabilazü hegalez airian.
Gaiak urtuki nündizün sasiño batetara,
han züzün xedera,
oi! ene malürra!

- Xoria, zaude ixilik, ez egin kantürik;
xoria zaude ixilik, ez egin kantürik;
eztüzü profeitürik ni hola penatürik,
ez eta plazerik
ni tunban sartürik.

- Bortiak xuri dira elür dienian,
Sasiak ere ülün osto dienian.
Ala ni malerusa! Zeren han sartü nintzan?
Jun banintz aitzina,
Eskapatzen nintzan.

- Xoria, zaude ixilik, ez egin nigarrik;
Zer profeitü dükezü hola aflijitürik?
Nik eramanen zütüt, xedera laxatürik,
Ohiko bortütik,
Ororen gaiñetik.