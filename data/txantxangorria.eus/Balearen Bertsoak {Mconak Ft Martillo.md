---
id: tx-525
izenburua: Balearen Bertsoak {Mconak Ft Martillo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8ih-mqQwMVw
---

Ozeano ezberdinetan bizi den nomada naiz ni,
plastikozko kontinenteak flotatzen ditut ikusi.
Baina zertan ari zarete? Atzera bueltarik ez da!
Zilborrera begira zuen espeziue madarikatua.

Sovint el blau del cel admiro just quan surto a respirar,
pero m'endinso lluny del sol defugint els seus paranys.
No puc evitar la por, a la seva set de sang.
Es nomes pel seu afany despietat que omplen el mar de crueltat.

ENTZUN NIRE INTZIRIA
ATZERA KONTUA HASI DA
S.O.S GIZATERIARI
ZAURIAK ITXI LURRARI

Itsasoari abesten, romantizismoak jota,
anai arrebak itotzen ikusita zuei bost axola.
Zaborrez betetako hondartzan botako dut azken hatsa,
janaria zelakoan urdaila dut plastikoz beteta.

ENTZUN NIRE INTZIRIA
ATZERA KONTUA HASI DA
S.O.S GIZATERIARI
ZAURIAK ITXI LURRARI