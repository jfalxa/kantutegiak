---
id: tx-2146
izenburua: Itziarren Semea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M9p4-1WMPd0
---

Telesforo Monzonek Andoni Arrizabalagari eskeinitako abestia.

Andoni Arrizabalaga Basterretxea preso ohiari elkarrizketa 
Itziarren Semea Askatua 

1969ko udazkenean, Burgosko Consejo de Guerra batean, euskaraz hitz egiten jeiki zen. Heriotzera kondenatu zuten. Andoni Arrizabalaga Basterretxea, 35 urte, Ondarrutarra. Apirilaren lauean irten da Puerto de Ssnta Mariako espetxetik, 8 urte barruan igaro ondoren. Irtenda, Bilbon eta Ondarrun errezibimendu handiak izan zituen, hain popularra baita, eta euakaraz mintzatu zen haietan ere. Elkarrizketa luzeak izan ditugu gartzelako bizimoduaz, abiadura politikoaz eta bere lehen garaietako errepresioaz: 
1964ean detenitu ninduten lehendabizí pintada batzukaitik. Hiru egun Bilbon torturatua izan nintzen. Handik Larrinage kartzelari eta 22 egunera aske. Bigarrenez, Ondarrun, 1968an harrapatu ninduten eta ezer galdetu gabe, hamar bat goardia zibilen artean kolyeka hasi zitzaizkidan. Zarautzera eraman ninduten, Gipuzkoan "estada de excepción" zegoelatik, detentzio egunak gehitzeka, 8 egunerako. Hidalgo eta hauek torturatu ninduten, kirofanoa e.a. La Salven urkatze simulakroak ere jasan nituen.