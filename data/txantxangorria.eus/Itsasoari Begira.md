---
id: tx-1989
izenburua: Itsasoari Begira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W7DCtgBgmjw
---

Zazpi mendeko gauean gaude.
Gure loreetan sasi,
irrintzi, oihu, ele ta ulu,
marmario ta garrasi,
haize enbata, brisa, galerna,
gaur garbi, bihar nahasi,
xake taularen zuri beltzetan
arrats gozo egun gazi.
Ai itsasorik ez bageneuka 
zeri so negarrez hasi.

Gure zuhaitza landatu dugu
amildegi muturrean,
adarrak daude ertzetik haruntz;
sustraiak, berriz, lurrean,
bihotz esteak estu helduaz
esku baten ahurrean.
Bertso berriak jartzera noa
bere indar laburrean,

Nola malko bat isuritzen den 
Isasoaren aurrean. 
Nola haizea gurazalea
Eguzkiaren irteran
Nola kaioak zorabioan
Itxas embata besperan
Pentsatsen nago gu ere berdin
Ibiltzen ez ote geran
Susmoa dauka gure patoa 
Itsasoa ez ote da 
Libre ta zabal dugu aurrean 
Baina ezin dugu edan

Eldu herria sustraietatik
Tira eta gora jaso
Jarri kantauri aurrean eta 
Mantendu zutikan gizon
Ispillu hortan ikus gaitezen
Herriz herri hauzoz hauzo
Zauriak gatzez itxi ditzagun
Malkoak urez  eraso 
Sano ta libre irla txiki bat
Salbatuko gara kaso

Azken arnasa eman nahi nuke
eguna hiltzen ari da 
Azken arnasa eman nahi nuke
Bertsoak entzuten dira 
Azken arnasa eman nahi nuke
Kantari nator herrira
Azken arnasa ematen dugu
Eguzkitik eguzkira
Azken arnasa eman nahi nuke
Itsasoari begira
Azken arnasa ematen dugu
Eguzkitik eguzkira
Azken arnasa eman nahi nuke
Itsasoari begira
Azken arnasa eman nahi nuke
Itsaoari begira