---
id: tx-2318
izenburua: Keinu Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mCQuDFdzcaI
---

Nola ohartarazi, keinu batez 
Hitz askorekin lortu ez dudana 
Begirada batekin barruko 
Korapiloa askatuko didana 

Irrifarrez batu naiz zuregana 
Xuxurlaz belarrira 
Laztanduz ilea, muxu bat emanaz 
Izotzak urtu dira 

Esan nahi eta ezin, kanporatu 
Barnean gorderik dudan guztia 
Ez da errez esaten 
Zuregatik emango nukela nere bizia 

Sua pizten zait nigan zarenean 
Dizdiraz berehala 
Sinestuidazu doinu hau entzutean 
Zinez maite zaitudala 

Maite zaitudala... 
Oooooh.... 
Maite zaitudala...