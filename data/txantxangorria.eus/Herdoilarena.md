---
id: tx-1251
izenburua: Herdoilarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FVx1q8acJ0o
---

Herdoilaren tristeziarekin batera
ziutate honen soinekoa udazkenetan lanbroa da
eta bere sabaia laino baso bat
non bizi del ilargiaren badaezpadako agonia
eta kalatxorien argi urdinskak
murrail erraldoiaren begi gauero
zubi zaharrenetatik
hibaiari so berripaper saltzailea
hitz ezezagunen hiztegi bati bezala
bus txofer batzu boxeolari hilaz mintzatzen
apatridak bailiran trenak
memoria karrilen fatalitatean galduz
denboraren oihal xinglea arratsezkoa soilik
arrabita baldarren nostalgia kantoietan
eta haruntzago, moskorrak
kalegarbitzaileren beilegi bizia
beste zubi bat, prostitutak.

(Hitzak: B. Atxaga "Etiopía" (1978);
doinua: R. Ordorika)