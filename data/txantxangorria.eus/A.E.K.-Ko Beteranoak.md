---
id: tx-1766
izenburua: A.E.K.-Ko Beteranoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EQaXwX-T45w
---

kaixo bixente! hementxe gaude, 
zu jarrita ta ni tente. 
gezurra esan nun getarin, 
ni baino lehenago zan atarin. 
nahaste borraste, azak eta nahaste. 
amezketarra, tirri-tarra, 
gaztainak jan ta bota puzkarra. 
itzak ederrago, egiñak baino. 
erranak eder, eginak lander. 
esana erraz, egina garratz.
zozoak beleari: "ipurbeltz". 
buruik ez, txapela nahi. 
egia askoren erregarria. 
epelak hartuko dituzu. 
neu nagusi ala muturrak hausi. 

A.E.K.-ko beteranoak. 

kaixo bixente! hementxe gaude, 
zu jarrita ta ni tente. 
inpernuko etsaia. jesus! 
tokitan dago. 
urte askoan, txapela kaskoan. 
ustea, erdi ustela. 
arrain handiak jaten du tipia. 
arraioa! eziank ez du legerik. 
zure etsairik haundiena, 
zure burua lehenena. 
egin zak lo, tan jango dek me. 
jende ederra, jende alferra. 
auskalo jun da buskalo. 
felixen osaba, drabasaren antena. 
urria esan, eta urria izan. 
gureak egin du! 

A.E.K.-ko beteranoak. 

"gaur forjariak lana utzirik, 
gatoz guztiok kalera" 
oskorriren kantak, "drogak a.e.k.-en" 
eta kaxianoren itsua. 
putiña, aldamena, ihaus, alporra... 
"su emango diogu geure fabrikari, 
eta nagusiaren etxe guztiari".