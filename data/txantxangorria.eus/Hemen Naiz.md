---
id: tx-1429
izenburua: Hemen Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0PCRaUFKgbM
---

Ilunpetan
hitz ederrenak hurbiltzen direnean,
ger/tu sen/ti/tzen zai/tut.
Hor zaude,
(e)ta nire itzaletik urrun egon arren,
hor zaude berriro.
Aurrean,
zuri-beltzean dagoen irudi bat,
lehenengo taupada.
Doinu bat,
etsipenaren kontrako garrasi bat,
patuaren algara, arimaren hitza.
Hemen naiz,
zurekin esnatzeko.
Bakarrik,
zure itsasoa ikusteko
Hemen naiz,
malkoak apurtzeko
zugandik,
irribarreak askatzeko.

Bide hau,
izan dadila izan behar/ko.
Nahi zaitut
iluntasunaren isladatik urrun,
orainaren aldean.
Nahi nuke,
egindako negar bakoitzaren truke,
argi bat piztea gure bidean.

He/men naiz,
zurekin esnatzeko.
Bakarrik,
zure itsasoa ikusteko
Hemen naiz,
malkoak apurtzeko
zugan/dik,
i/rri/ba/rre/ak as/ka/tze/ko.

Besarkatu nazazu,
hegan egin dezagun.
Zaindu dezadan udaberri hau,
(e)ta ez da bukatuko,
ez dut alde egingo
malkoak apurtzeko
zugandik,
irribarreak askatzeko.

Hemen naiz.
Hemen naiz.
Hemen naiz.