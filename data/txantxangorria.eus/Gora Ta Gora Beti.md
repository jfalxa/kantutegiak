---
id: tx-2061
izenburua: Gora Ta Gora Beti
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FWgCAqPtjco
---

Gora ta gora beti,
haik gora Lapurdi!
Euskerak bizi gaitu
eta bizi bedi.

Gorantza doa agudo kapitala
eta Lapurdi beti dago apala,
aldrebesturik dago mundu zitala,
konformatzen ez bagara
ze pekatu mortala!

Gurea ez da bizi-modu normala
lurra lantzeko haitzurra eta pala,
lanaren truke kobratzen erreala.
Benetan da miserable
euskaldunon jornala.

Munduan ezin bizi daiteke hala,
merkatuetan dena gora doala,
etxera eta zopa guztiz argala,
kriseiluen oiloa
ez da postre formala.

Obrero eta nekazari leiala,
zuena ez da errenta liberala.
Diru zikinak putre motzak bezala
botatzen dizu gainera
hortxe bere itzala.

Jabeek dute kontzientzi zabala,
kaskatu dute gure lurren azala,
baina Lapurdi ez da hain zerbitzala;
burruka garbian dago
hori da printzipala.