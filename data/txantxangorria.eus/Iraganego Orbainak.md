---
id: tx-730
izenburua: Iraganego Orbainak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Geu_YlpPwuc
---

Zure poza kopa bete ardotan ito zen, 
galtzaileen artean.
Hor pasatzen dituzu egunak, 
gitarra baten berotasunean.

Azkar bizi zara, ez zaizu ezer axola.
Iraganeko orbainak.  Hitzak itota, 
zure bihotza, hautsia.
Non dago zure esperantza?

Begirada galduarekin, tristuraz inguratuta.
Bidegabekeria basan galduta, 
non nagusi den, krudelkeria.

Azkar bizi zara, ez zaizu ezer axola.
Iraganeko orbainak.  Hitzak itota, 
zure bihotza hautsia.
Non dago zure esperantza?

Botila baten beroan, aire zabalean, 
neguko egun latzak jasaten dituzula.