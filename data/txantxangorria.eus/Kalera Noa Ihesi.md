---
id: tx-1514
izenburua: Kalera Noa Ihesi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/US7s9qZ1YWw
---

Ni bizi naiz baserrian
animalien erdian,
bertsotan kontatuko dizuet
mundu guztiak dakian
nola nagoen mendian
bizidunen hilobian.

Jaikitzen naiz egunero
oilarrak jo ta gero,
lanetik eta lanera beti
nola ninteke hain ero!
Kalera aldegin edo
sartuko naiz gerrillero.

Lanez banago asea
ordaina ez da luzea,
mendi goietan egonagatik
hauxe da nire leizea
emaidazu guraizea
urra dezadan haizea.

Kalera noa ihesi
neure gurdi eta guzi,
ez naiz menturaz han ere ongi
ibiliko lehendabizi,
baina ez ninteke bizi
baserrian bezain gaizki.

Agur baserriko mendi,
hil arteko urkamendi,
hiltzeko bada ere zugana
berriro itzul banendi,
mundua ez dabil ongi
edo nauzu ero haundi.