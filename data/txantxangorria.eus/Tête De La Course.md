---
id: tx-3346
izenburua: Tête De La Course
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OufFbMB5pAQ
---

Tourmaleten gora izerdia
San Mamesen gaude gola sartzear,
ze gogo, ze behar.
Ze hotz, Everesten zenbat negar
errazago altxako nuke harri honen erdia.

Tête de la course niri bost nor den,
ezin beti izan Koikili edo Iturregi,
 Pasaban edo Iñurrategi.
Eta harritzar hori edonork ez du jasotzen. 

Hitzetan da aukera
badugu guk geurea,
hizketan selekzionatu zeurea.

Beti ez dira txapeldun 
azkarrenak edo onenak,
hemen bakarrik galtzen du
egiten ez duenak.

Igo mendia, eman pedalkadak, jaurti txutak, 
hartu ziabogak, besarkatu harriak...

Hitzetan da aukera
badugu guk geurea,
hizketan selekzionatu zeurea.

Beti ez dira txapeldun   
azkarrenak edo onenak, 
hemen bakarrik galtzen du
egiten ez duenak. (HIRU ALDIZ)