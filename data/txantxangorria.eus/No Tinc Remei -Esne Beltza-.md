---
id: tx-3331
izenburua: No Tinc Remei -Esne Beltza-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5iU9ZLPajBg
---

No tengo remedio bicha, no tengo remedio amor. La vida va tan deprisa, confunde mi corazón. Y si me dejas varado en la arena que las calles se encarguen de mí, que mis lágrimas sean, mi niña, el reflejo de lo que perdí. Al otro lado del puente un nuevo cielo me espera. Yo voy a cruzar el puente aunque al cruzarlo yo muera. Y si lo logro cruzarlo, mi vida, será mi mayor consuelo, la muerte no sea la muerte, la muerte bajo este cielo. Arriba el moment de deixar-te i ho sento molt, les despedides sempre tenen mal sabor. Arriba el moment de deixar-te i ho sento molt, coneix-en's la vida mai va ser un error. No tinc remei bitxa, no tinc remei amor. La vida va tan depresa que confon el meu cor. Y si m'encalles en la sorra que els carrer se'n recordin de mi, i les llàgrimes siguin ,ma vida, el reflexe del que ja no tinc. Arriba el moment de deixar-te i ho sento molt, les despedides sempre tenen mal sabor. Arriba el moment de deixar-te i ho sento molt, coneix-en's la vida mai va ser un error.