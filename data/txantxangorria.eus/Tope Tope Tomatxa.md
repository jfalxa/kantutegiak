---
id: tx-1821
izenburua: Tope Tope Tomatxa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SpZQ3iWHyBo
---

Tope, tope Tomatxa,
hau da eskuen martxa.
Tope, tope Tomatxa,
basotik doa bildotsa.
Goian dago eguzkia,
errekan zubia.
Kirrikika doa gurdia,
Ssspum! Suziria. (bis)