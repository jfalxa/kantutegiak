---
id: tx-3306
izenburua: Tarrapatata -Takolo,Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9ZdNtDKtL0w
---

Sorbaldan beti berritsu loroa
begi batean partxe bat dauka
eta ezpata tinko hartuta badoa
hankapaloa herrenka
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zurekin
Itsasontzian altxor baten bila
bandera beltza gurekin.
Sorbaldan beti berritsu loroa
begi batean partxe bat dauka
eta ezpata tinko hartuta badoa
hankapaloa herrenka
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zu/ekin
Itsasontzian altxor baten bila
bandera beltza gurekin.
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zurekin
Itsasontzian altxor baten bila
bandera beltza gurekin.
Sorbaldan beti berritsu loroa
begi batean partxe bat dauka
eta ezpata tinko hartuta badoa
hanka/paloa herrenka
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zurekin
Itsasontzian altxor baten bila
bandera beltza gurekin.
Sorbaldan beti berritsu loroa
begi batean partxe bat dauka
eta ezpata tinko hartuta badoa
hankapaloa herrenka
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zurekin
Itsasontzian altxor baten bila
bandera beltza gurekin.
Ooo! Tarrapatata pirata
Ooo! Joan nahi nuke zurekin
Itsasontzian altxor baten bila
bandera beltza gurekin.