---
id: tx-1332
izenburua: Errefuxiatuena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XDtoLRvll0c
---

Utzidazu kontatzen ze ederra den itzultzea.
Utzidazu kontatzen malko urdinen artean.
Utzidazu kontatzen ze ederra den jakitea
maiteko zaitudala itsasoaren beste aldean

Utzidazu kontatzen izarren artean
Utzidazu kontatzen laztantzen hiru akorde hauetan
Utzidazu kontatzen ze ederra den zu ikustea
Utzidazu kontatzen ze ederra izango den itzulera

Ze ederra izango den itzulera,
elkarren besoen zabaleran, ah, ah, ah
Zure fede guztiei eustea,
zure min malkoak baretzean.

Utziezu kontatzen behin ume zirela
amestu zutela itsasoaren beste aldeaz.
Hegan egiteko hegoak bakean bizitzeko,
ez dute besterik nahi gaur gurekin dantzatzeko.

Ze ederra izango den itzulera,
elkarren besoen zabaleran, ah, ah, ah
Zure fede guztiei eustea,
zure min malkoak baretzean.

Ze ederra izango den itzulera,
elkarren besoen zabaleran, ah, ah, ah
Zure fede guztiei eustea,
zure min malkoak baretzean.