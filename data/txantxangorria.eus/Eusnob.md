---
id: tx-819
izenburua: Eusnob
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Xvb8yunodTM
---

Kafea eta galletaken ezagutu zintudan, Kokoshca-k Nicoren Desertshore pintxatu zuen egun hartan, Garrelen filmografia osoa buruz aipatu zenidan, zeure herriko zineklubean mahai inguruak gidatzen zenituela. Nouvelle Vague eta zinema japoniarrean aditua, Fritz Lang, Truffaut, Tati, Duras, Varda, Visconti, Houston, Kaurismaki, Kiarostami, Kusturika, Kawase, Kurosawa, Kim ki duk, Won Kar Wai, Jarmusch, Buñuel, Val del Omar, Rosales, Iriarte, Lakuesta eta Jordáren mireslea… 

Zu euskal intelektuala, 
homo sapiens kontzeptuala, 
ahoa ireki eta berehala 
konturatu ginen eusnob bat zinela. 

Musikazale amorratua, 
soziologian doktoratua, 
makrobiotikan aditua, 
artista frustratua. 
Liburujale aseezina, 
saiakera filosofikoen irakurle fina, 
zutabegile geldiezina, 
blogosferako gatz eta pipermina. 

Berria, Argia, RDL eta Ruta 66-en harpideduna. Carne Cruda, El septimo vicio, Lizardiren baratzaren entzulea. Zabaleta, Ocariz, Rozas, Rodríguez ahizpak, Jeletonen zale sutsua. Galfarsoro, Escohotado, Zizek, Deleuze, Lacanen jarraitzaile petoa. 

Zu euskal intelektuala, 
homo sapiens kontzeptuala, 
ahoa ireki eta berehala 
konturatu ginen eusnob bat zinela. 

Usopop, Primavera Sound, Xiru, Lemon Day, Bebarruko Jardunek, Sonar… Ertz festibalera tapoirik gabe joaten zarenetakoa… Anjel Erroren twittak retwitteatzen dituztenetakoak, berak bezala taktel txandala gorbatarekin gonbinatzen duzu. A topa tolondro, Love of 74, Separata, Lapiko kritikoa… egunero begiratzen dituzu. Berlin eta New Yorkera bidaiak... Ajoblanco zaharrak irakurri, egun Jot down eta Diagonala… 

Zu