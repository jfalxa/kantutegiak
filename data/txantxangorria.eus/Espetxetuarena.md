---
id: tx-1912
izenburua: Espetxetuarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5nRUT2DHdRA
---

Itxasu horren narru nabarra
pantera batena dirudi
espetxeko atezain zarra
itxi nagik goxeko bidari
Tori giltzok, urrezko giltzok
ta bete egikek itxas miña
axioin egal puntan daukok
iñok txastau bako lurriña
Noiz arte lagako nauk aske
zidarrezko itxaso zabalan.
Aske ez gura dokan arte
goiz ederroi maite dagijan.
Neure maitiaren oñotsak
-urretxindor ori ixilik-
bide argijetaz jatozak
atezaina aske laga nagik.
Tori giltzok urrezko giltzok
maitasuna jabilk landetan
esku zurian muin egiok
eta laztan bat begietan.
Maitasun pozgarri orrentzat
aske nabilkek ler artian?
Zintzor maite dagiantzat
giltzik etxagok espetxian

giltxik etxagok espetxian
aberri amaren negarra
neugana jator mendietatik
espetxeko atezain zarra
bein soilik aske laga nagik
Agur giltzok urrezko giltzok
guda otsa dozak landetan
arren itxi ondo atiok
eta galdu zaiteze uñetan.
Baina abeeri ama aldezteko
enauk itxiko kate barik
eure erri ori askatzeko
etxak iriko espetxerik.