---
id: tx-946
izenburua: Hodei Beltzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ijZAbjQY_Mc
---

HODEI BELTZAK 
feat. ZALO URAIN

********
Egilea: Txaber Miravalles Gómez "conserje del universo"
eskerrak Eider Iturriaga eta Bonberenea
(Zuzeneko irudiak  Bonberenea gaztetxean grabatua)

********



Errekak gainez egin du 
Bertan bizi diren arrainak biziki aldenduz 
Bizitza berri bati ekin, aterata behingoz ubidetik mugarik gabe, errealitate bihurturik 

Lotan zeundela, bizirik sentitzean 

Errekak gainez egin du 
Lurrak ezin du euri gehiago irentsi 
Ernetu dira haziak, eta ez da isiltasunik 
Mugarik gabe, errealitate bihurturik 

Lotan zeundela, bizirik sentitzean 
Errealitatea, amets gaiztoa 

Ta hemen daude oraindik, 
geldi 
hodei beltzak 
eta nahigabean hemen geratu dira 

Basoa erre egin da 
Bertan bizi ziren animaliak biziki aldendu dira 
Ez dakite nora joan, ez dakite zer duten esperoan 
Mugarik gabe, esnatu gara azkenean 

Ta hemen daude oraindik, 
geldi 
hodei beltzak 
eta 

Ta hemen daude oraindik, 
geldi 
hodei beltzak 
eta nahigabean hemen geratu dira

+++++++++++++++++

eraso.bandcamp.com
@erasomania