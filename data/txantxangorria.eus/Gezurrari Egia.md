---
id: tx-2313
izenburua: Gezurrari Egia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aQFdKz1wG4M
---

Beldurra artazi loratzen den ideia oro errotik moztu nahi dute oraindik
Gorrotoa izanik guri mintzatzeko duten hizkuntza bakar hori
Garaile eta garaituak, 39-tik gerozko pentsamenduak
Odolez idatzitako istorio faltsua, gure arbasoen hilobi galduak
Loreak moztu arren udaberria ezin da eten

Mendeku egarriz, berpiztu nahi duten gudaz askatasuna ukatzen zuri
Sinestaraziz gezur bat mila aldiz egi bihurtzen dela azkenik
Egia eta gezurra aspaldi ito ziren komunikabideen itsasoan
Manipulazioa; armarik gabe hiltzen duen terrorismoa
Horregatik hemen gaude gezurrak biluztu arte 

Lau pareta, berrehun malko
Zu itxaroten dagoen oro taupada bakoitzaz zugandik gertuago
Etxetik urrun, bihotzetik gertu
Elkarrekin ozenago oihu; ezingo digute irribarrea lapurtu
Begiekin besarkatuz, sentimenduak laztanduz

ZUREKIN GAUDE, GUREKIN ZAUDE
IKASI DUGU MUSIKA GABE DANTZATZEN
ZUREKIN GAUDE, GUREKIN ZAUDE
KATEZ JAIO TA BIZI ARREN ASKE SENTITZEN
ASKE PENTSATZEN, ASKE BIZITZEN, BURUJABE IZATEN