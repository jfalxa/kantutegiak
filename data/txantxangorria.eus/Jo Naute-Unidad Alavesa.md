---
id: tx-1221
izenburua: Jo Naute-Unidad Alavesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kfi5eXGbr_Q
---

Eskalpeloaren ziztada al da hau?
Ala zuek nahi zenuten 
azken droga berri hura?
Egunero amorruarazten naute,
psikoanalisi polimorfo zaharberriekin

Jo naute baina ez naute menperatu
hotzetik epelera noan hontan
zugatik egingo dut azkenengo jauzi hau
alienatu ezin nazazuen
zakurrenen indarra hilda ageri da,
ambizio mugagabearen azken geltokian
instituzionalizatua dagoen 
kontzepzio ridikulu antipolitikoa