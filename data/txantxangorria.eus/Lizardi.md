---
id: tx-2992
izenburua: Lizardi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ukz4KstM5bQ
---

Lizardi, Rimbaud etorri duk hitaz galdezka
eta gu ere hire zain geundela
esan zioagu
ez hitzela aspaldi azaldu
etxetik
eta belatzean eseri gaituk denok
erlojuak janez
baina mezularia bidali diagu Alosko torrea
eskilara luzetan
beleak uxatzen
ote hintzen ikus zezan
gero kanpaiak entzun dizkiagu
zakurrak zaunkaka
orduan sortu haiz bidetik
balantzaka
eta hirekin aurrez aurre jarri garenean
zerraldo erori haiz gure oinetan
eta gorpu gogor hintzen
udazkenaren tronoan
hertsi dizkiagu begiak
adiosik ez
eta goizaldean
muxika hezur batetan sarturik
o petit poete
pirotekniarik gabe lurperatu haugu
baratzan.