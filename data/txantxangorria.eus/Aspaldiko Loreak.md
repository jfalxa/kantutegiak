---
id: tx-858
izenburua: Aspaldiko Loreak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SES7YNMHgfw
---

Maskaren topaleku,
garaituan galtzen nahiz.
Balantzaka arrastaka,
eramaten ditut.
Aurretik dabiltzanak,
Maniki faltsuak,
dirudite argipean,
gizakien larruak.
Aspaldiko loreak,
Asfaltoan simeltzen,
Hiren orduetan
Sorgintzen diraute,
Estizo pentsaldetan
Behin holako musu
Eterna lehenetan.
Barrote ugartuen
lubakira biltzen naiz,
Narrastaka ahapeka
Saihestu ditzaket.
Atzetik ditudanak
Heroi erailak,
dirudite itzalpean
garun hezur horailak.
Aspaldiko loreak,
Asfaltoan simeltzen,
Hiren orduetan
Sorgintzen diraute,
Estizo pentsaldetan
Behin holako musu
Eterna lehenetan.
Euria kondenatuz,
plazara makurtzen naiz.
Estatuko banderarik,
izen tankerrena.
Hasi doz zuritzera
zerurik ez dago,
bandera hura goratzeko,
ekaizpean gurtzeko.
Eta Euria kondenatuz,
plazara makurtzen naiz.
Estatuko banderarik,
izen tankerrena.
Hasi doz zuritzera
zerurik ez dago,
bandera hura goratzeko,
ekaizpean gurtzeko,
ekaizpean gurtzeko,
ekaizpean gurtzeko.