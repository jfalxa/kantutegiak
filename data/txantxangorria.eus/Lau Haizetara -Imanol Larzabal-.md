---
id: tx-3236
izenburua: Lau Haizetara -Imanol Larzabal-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fYgiE4rHe4c
---

Itsasondoko haize gezala.
mariñelen bertsoa,
zoaz urruti aidean;
lau haizetara, (tris)
Donostiatik Iruñera.

Uzta garaiko gari usaia
segarien bertsoa,
zoaz urruti aidean,
lau haizetara, (tris)
Iruñetikan Baionara

Ezpain erien portu galdua,
maitarien bertsoa,
zoaz urruti aidean,
lau haizetara, (tris)
Baionatik Vitoriara.

Muga zaharrak mahastietan,
gaiteroen bertsoa,
zoaz urruti aidean,
lau haizetara, (tris)
Vitoriatik Bilbaora.

Burnizko bihotz arraildua,
gabetuen bertsoa,
zoaz urruti aidean,
lau haizetara, (tris)
Bilbaotik Euskal Herrira.