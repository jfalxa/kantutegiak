---
id: tx-2022
izenburua: Eskolan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/k2Bmo7Gqb7I
---

Txikitan ziguten
aberri bat ezarri;
kontu haiekin batera
bandera bat ipini.
Nik nahi dut aberri bat
juxtu ta gurea
eta ez inola ere
beste horiek ezarrita.

Eskolan dirade
gutaz trufatu
eta bortxa askoaz
hizkuntza bat ukatu.
Nik nahi dut hizkuntza bat
euskera gurea,
eta ez inola ere
beste horiek ezarrita.

Hango gizon haiek
indarkeri bidez
ondo ikasi genuen, bai,
haien justizia zer zen.
Nik nahi dut justizi bat,
justizia, gurea
eta ez inola ere
beste horiek ezarrita.
Nik nahi dut justizi bat,
zurea, nerea,
Euskal Herriko jende xedean
justizia.