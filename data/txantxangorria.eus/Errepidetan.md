---
id: tx-926
izenburua: Errepidetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oi2KTfd6_KE
---

Zuzendaria:
Eloi Colom

Aktoreak:
Iker Bakaikoa
Aritz Etxeberria
Unai Mundiñano
Pello Reparaz

Argazki Zuzendaria:
Toni Rey

Musika eta hitzak:
Pello Reparaz

Ekoizlea:
DeepSoda

Arte zuzendariak:
Iratxe Reparaz
Libe Amunarriz
Iker Gozategi

Muntaia:
Xavi Trilla

Kolorea:
Lluís Velamazan

Musika nahasketa:
Paxkal Etxepare

Grafismoak:
Xavi Mauri

Zuzendari laguntzailea:
Joseba Gracenea

Produkzio burua:
Joseba Razquin

Produkzio laguntzailea:
Lur Larraza

Lehen kamera laguntzailea:
Hector Julian

Bigarren kamera laguntzailea:
Sandra Roca

Argiketaria:
Pau Ramírez

Management:
Panda Artist Management


Bederatziak ta hamar
uda hastera doan goiz batean
ametsekin borrokan
oheraino eguzkia
ta alboan norbait daukat urrumaka
banoa gaurkoa egun handia da.
Ta lagunak tabernan direla dio Maizak
agian berandu noa.
Hi Pello ze ostiya! Aldiro aldiro azkena
Benga ba, bagoaz
Errepidean geldikaitzak gara
leiho bat behera ta oihu bat kanpora.
Errepidean gaur berriz
abesti berri bat idaztekotan
ahaztuko ez dugun hura. (Bis)
Estaneren txiste bat
ta ohartzerako Altsasu parean
zenbat putre zeruan.
&quot;Txarrenari onena&quot;
esan du batek, igo bolumena
oraindik eztugu esan azken hitza
eta atzean utzi dugu jainko paganoa
Beriain, zaindu Sakana.
Bazile bat, barre bat eta kolpe bat bueltan
Bagoaz, bagoaz
Errepidean geldikaitzak gara
leiho bat behera ta oihu bat kanpora.
Errepidean gaur berriz
abesti berri bat idaztekotan
ahaztuko ez dugun hura. (Bis)
Su berrituta, indartuta
bat egin dugu hemen

bidelagunak infernuan
zeruan baino lehen.
Errepidean geldikaitzak gara
leiho bat behera ta oihu bat kanpora.
Errepidean gaur berriz
abesti berri bat idaztekotan
ahaztuko ez dugun hura. (Bis)
Errepidean geldikaitzak gara
leiho bat behera ta oihu bat kanpora.
Errepidean gaur berriz
abesti berri bat idaztekotan
piztuko ez dugun hura
piztuko ez dugun hura.