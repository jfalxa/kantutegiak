---
id: tx-3185
izenburua: Zu Zara Egunkaria -Sutagar-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/McRAvHOarZc
---

Astelehenero gertatzen zait
gauza berbera
dendan etzaudela
esaten didatenean

Hain zera ederra
irakurterreza
euskaldun sentitzeko
behar zaitut
hain zera ederra
irakurterreza
zu zara Egunkaria

Zure barnean
murgiltzen naizen bakoitzean
gogora arazten didazu
benetan naizena

Hain zera ederra
irakurterreza
euskaldun sentitzeko
behar zaitut
hain zera ederra
irakurterreza
zu zara Egunkaria