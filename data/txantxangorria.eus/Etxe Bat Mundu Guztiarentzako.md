---
id: tx-314
izenburua: Etxe Bat Mundu Guztiarentzako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ttMrhZXbLzM
---

Ukraina ta Errusia
Nereak diren bi herri
Guda betean dabiltza
Nahiz nik ez dudan igerri 
Ta nik mina sentitzen dut
hila ia mina bider bi
nere burua ulertzen
bota dut nahiko izerdi
baina izan nuenean
lehen bonba hotsaren berri
negarrez hasi nintzaion 
nik ez nekiela zerri
agintari aberatsek
besteena beti aberri
ta zibil pobrearentzat
etxea beti deserri
Bai, nere sentitzen ditut
Zuen sufrimen ta malko
Nik ere nere barruan
Baditut hainbat asalto
Baina ez ditut nahi ondoan
Ez OTA eta ez NATO
Ez Europa ta ez Zelensky
Ez Putin ez halako
Nik eskatzen dut etxe bat
Mundu guztiarentzako
Non duinki biziko den
Ta ez duten aterako
Baina ez bada posible
Horrela nahi dutelako
Nerea zabalik daukat
Behar duenarentzako
Nerea zabalik daukat
Behar duenarentzako