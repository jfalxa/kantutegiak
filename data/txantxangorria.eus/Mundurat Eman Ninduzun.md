---
id: tx-2654
izenburua: Mundurat Eman Ninduzun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/37qgRjwUpGI
---

Ama, nola zure belaunetan 
nindadukazun denboran, 
utz nezazu zure bularrean 
burua pausa dezadan; 
ene bihotzak zer sendi duen 
nahi baiterautzut eman, 
zure ezpain sakratuetarik 
ikasi nuen mintzairan.

Ene deia amodioaren 
bidez zinuen entzun,
eta gero oinazen erdian 
mundurat eman ninduzun; 
biziaren grazia lehenik, 
gero hoinbertze maitasun, 
berant jakin dut, ama maitea, 
zendako eman dautasun. 

Adierazi zerautanean 
odolak bere mintzoa,
berriz gogoratu zitzerautan, 
o ama, zure altzoa; 
bai ta zure bularretik hartu 
esne on haren gozoa, 
orduz geroztik zurea naukan 
ene izaite osoa.