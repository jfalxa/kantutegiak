---
id: tx-3147
izenburua: Garrasi Egin -Revolta Permanent-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WfxWLia7ceM
---

Aldi batez izan bai zara, aske oihuka egin
kate zikin ta urak apurtzen jadanik
izanik ezik behar bada jarri zutik
txikituko ditugu ta negarrari ez etsi
garrasi egin
Ez egin atzera, garrasi egin,
gehiago baikara, garrasi egin,
ez gera zaitez etxean, garrasi egin,
ez zarelako bakarra, garrasi egin.
Aldi batez guztion alboan
sendatuko ditugu belduriko txokoak
zure bihotzean amaitu dira egondako jokoak
ta izaren artean joan dira atzoko amets gaiztoak.
Izan zenituen erasoak non daude jada
garrasi honek izango da zure ukabilkada
dolumina noizbait izan omen bada
zure kezkarik nagusiak gaur desbideratu da.
Behingoz badoa ekaitza hodei ertzetik
zure oinazeak aldentzen dira zure ondotik
iragana haitzulo batean dago murgildurik
malkoren bat botata baina orain zara pozik.
Horregatikan askatu atxiki zaituztenak
zure latorriko ariman beste nonbaitera
hegan joan dezala inoiz berriz bala azala
zure gorpuan zein loretan zapuzten bazara.
Aldi batez izan bai zara, aske oihuka egin...
Gorputz osotik utzi dizkizu zauriak
baina guztiak bihotzetik atara dira
ta labanak balira zapuztu dizute arima
zaplasteko bakoitzak gau bat utzi du hilda.
Minaren putzuan murgildurik maitasuna
mindurik dago zure amodio bakartsua
lehen biena zena orain bakarrik sentitzen
oso polita izan dena ikusten duzu hiltzen.
Golpe bakoitzarekin birao bakoitzarekin
barrua mintzen ta zikintzen
urte askotan bizia biltzen
eta gogoz gogora ezazu inork ez dizula isiltzen.
Sufrimendua barnean gordeta
maite duzulako ez duzu esaten gertatzen dena
baina ezin izango duzu beti ezkutatu kaltea
egin ezazu oihu egia atera kalera, kalera.
Aldi batez izan bai zara, aske oihuka egin...