---
id: tx-2023
izenburua: Rebeka Linares Urrezko Danborra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F9HsHBv5tbg
---

Alkate jauna aurtengo urrezko danborra
Rebeka Linares
Nor baino berak, mereziko du ba saria?
Rebeka Linares
Ia berrogeita hamar urte danborra banatzen
donostiartasuna urrearekin janzten
postalezko hiri honen enbaxadoreak
Buleko elatuak baina donostiarragoak!

Kazetari, idazle, sukaldari ta militarrak
kirolari, irakasle, politikari, eta apaizak
asko dira merituak egin dituztenak
gutxi batzuk ordea ohoredunak.

Alkate jauna, aurtengo urrezko danborra
Re/be/ka Li/na/res
Nor baino berak, mereziko du ba saria?
Re/be/ka Li/na/res
Re/be/ka Li/na/res
Donostian jaiotako aktoresa ospetsua
hiriaren izena gailurrera igo duena
hamaika pelikula sari eta arrakasta
pornostar artean erregina da!