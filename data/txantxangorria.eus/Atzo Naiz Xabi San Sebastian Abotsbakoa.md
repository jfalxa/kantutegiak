---
id: tx-1609
izenburua: Atzo Naiz Xabi San Sebastian Abotsbakoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6jtz8ZavAmE
---

Audioa: Antton Jauregi (KARANTTON)
Turuteio mai ai ai
Daro maroro
Turu/teio mai ai ai
Harritzekoa da
su/a/ren or/ba/na
a/tze/tik do/a/na
ba/rru/an du/da/na
A/tzo naiz gaur nin/tzen
e/tzi e/gin/go dut has/pe/ren
ahaz/tu/rik lehen bait lehen
Atzo naiz gaur nintzen
etzi egingo dut hasperen
ahazturik lehen bait lehen
Tu/ru/tei/o mai ai ai
Da/ro ma/ro/ro
Tu/ru/tei/o mai ai ai
E/zin/bes/te/ko/a
au/rre/an ge/ro/a
esaidan eroa
ta eman beroa
Atzo naiz gaur nintzen
etzi egingo dut hasperen
ahaztu/rik lehen bait lehen
A/tzo naiz gaur nin/tzen
e/tzi e/gin/go dut has/pe/ren
ahaz/tu/rik lehen bait lehen
Atzo naiz gaur nintzen
etzi egingo dut hasperen
ahazturik lehen bait lehen
Atzo naiz gaur nintzen
etzi egingo dut hasperen
ahaz/urik lehen bait lehen
A/tzo naiz gaur nin/tzen
bi/har zer
A/tzo naiz gaur nin/tzen
bihar zer
Atzo/naiz
bihar zer
Atzo naiz
bihar zer