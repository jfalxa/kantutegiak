---
id: tx-2841
izenburua: Primaderako Baladak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H8bX1CASamw
---

Hodei ilunek eraman du/te
negu beltzeko landua
eguzki printze laztantzen dute
ortu txuriko mandoa
ibar luzea u/rundik dator
kuku gisaren kantua
soineko berriz berde bisita
ez ditua lur osoa
harri tartean leunki dator
erreketan ur gozoa
xoko gordea xomorro bila
aztarrika ur xoxoa
bide ertzeko haritz ondoen
itzal etzanak berrotan


eskulpipita sortu berria
eguzki laztan lelotan
okil berdeen dabiltz zuloan
enborri hartu geldo ta


sakan luzea ageri dira
belardi berde garbia
goizeko ihintza
urretan dauka

eguzkiaren argia
belar freskotan zuri zuria
bazkan dabiltza ardia

iturrirako senda hestua
errenka da etorria
ura edaten ari da aska
behorran da
da gorria

urra sabela erori zaie
pago gainetik gorria

haize freskoa astintzen ditu
basa mazusten lorea
sasi ondoko lahar hostoen
berde arteko morea
arantza artean berrizten dira
tristuraren kolorea

Egun berria urratua da
hiltzen ari da arratsa
errekondoko ur lintzuretan
loretan dago sahatsa
adarrez eta hartu nahi du
udaberria arnasa