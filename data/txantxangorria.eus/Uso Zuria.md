---
id: tx-2664
izenburua: Uso Zuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IkymjHZmsjU
---

Uso zuria errazu
nora joaten zaren zu.
Espainiako bortuak oro
elurrez beteak dituzu;
gaurko zure ostatu
gure etxean baduzu. (Bis)

Ez nau izutzen elurrak
ez eta ere gau ilunak,
maiteagatik pasa nitzake
gauak eta egunak,
gauak eta egunak,
desertu eta oihanak. (Bis)