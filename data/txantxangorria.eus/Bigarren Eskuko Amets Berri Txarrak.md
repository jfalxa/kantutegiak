---
id: tx-3022
izenburua: Bigarren Eskuko Amets Berri Txarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o4Lgrm0f3Ho
---

Antzeko gauzen atzetik denok gauean
sute historiko bat, larrialdi irteera
etxera eramango gaituen norbait
galdera askorik egin gabe ahal dela

hartzazu nire poesia
bigarren eskuko amets

aitortu: buruarekin txortan
zakilarekin pentsatzen
irudikatu duzula jendea
zure ezpainak irakurtzen nenbilen

hartzazu nire poesia
lerro okerretan nire gabeziak
bi mila izen eta zure begiak
ihintzaren ezizen

dena hormatzen
dena argitzen izotz beltz moduko batez

bakardadeari izkin egiten
non bizi garen ez baleki legez

hartzazu nire poesia
lerro okerretan nire gabeziak
bi mila izen ta zure begiak
bigarren eskuko amets