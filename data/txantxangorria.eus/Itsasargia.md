---
id: tx-2030
izenburua: Itsasargia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HSWbYI1OEsU
---

ITSASARGIA

iragana atzeranzko ispiluaz
begiratzean 
oraina disfrutatu egin behar dela
ahaztu zitzaidan
oroitzapen artean bizi nintzen,
nostalgian
norabide garbirik ez duen
geltokian

baina agertu zinen (e)ta
ikusi nuen argia
bizitzaren zentzua
begiak ireki eta
konturatzerakoan,
zein den bidea

zu ezagutu arte ibili nintzen
noraezean
nire bizitza irauli zenuen
bat-batean
nork esango zidan ustekabean
gurutzatzean
nire iparra izango zinela
aurrerantzean

baina agertu zinen (e)ta
ikusi nuen argia
bizitzaren zentzua
begiak ireki eta
konturatzerakoan,
zein den bidea

bide horretan bagoaz
etorkizuna idaztear
hauxe da gure aukera
ametsen itsasoan
bilatu egin behar da
itsasargia

bide horretan bagoaz
etorkizuna idaztear
hauxe da gure aukera
ametsen itsasoan
bilatu egin behar da
itsasargia