---
id: tx-1024
izenburua: Ta Zu, Soilik Zu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T8F7tZwGMgg
---

Bideoaren egilea: Anmorsigol

Egokitzapena: Huntza
Produkzioa eta nahasketa: David Rosell
www.elDiluvi.cat


Nahi zenuen hura izango zara,

hiru aldiz bihurriago.

Hegan doan ukabila,

ta soilik zuk kontinenteak astindu.

Edozein minetik burujabetua,

ausarten bizipozarekin atera zinen.

Nork daki geroaz,

nork daki geroa hobea izango da.

Aske sentitu edozein lekutan,

ez utzi inork zu gelditzen.

Eta izarra izango zara,

askatasunera miratzen duen izarra.

Apirileko ekaitz gogorra zara,

zure etsaientzako oasi garratza.

Apirileko ekaitz gogorra zara,

loreak irekitzen dituen eguzkia.

Zure etsaientzako oasi garratza,

gauez argitzen duena.