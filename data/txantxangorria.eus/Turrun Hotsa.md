---
id: tx-60
izenburua: Turrun Hotsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hgf4UgK_7qk
---

KANTAREN FITXA TEKNIKOA:
Abestia eta melodia: Axular Arizmendi.
Hitzak: Leire Bilbao
Abeslariak: Aiora Renteria - Eñaut Elorrieta.
 
Kolaborazioak:
Zubi Zahar Herri Ikastolako umeak ( Ahotsa)
Leokadi ( Ahotsa)
Larraitz Ormaetxea ( Irrintziak )
Nerea Gartzia ( Irrintzia)
Aiora Ponce ( Irrintzia)
Aitzol Urkiza (Gitarra elektrikoa)
Eneko Benito (Alboka)
 
Konponketak: Fran Carreira.
Grabazioa, nahasketa, masterra :
Axular Arizmendi & Paxkal Etxepare
Masterizazioa: Simon Capony

BIDEOAREN FITXA TEKNIKOA
Zuzendaritza: Aiora Ponce
Kudeaketa: Unai Guenaga eta Zubi Zahar Herri Ikastola

Turrun hotsa portuan,
herria esnatu da
itsas haizeak dakarren
sekretua;
zainetan kresala eta
mingainean olatua,
ematen jarraitzeko
gure kantua.

Zubi zaharretik dator
marea gora
kanpandorreraino
dena bustiko da.
 
Eskutik hartu eta
ez da jausiko zubixe.
Geu ga
eusten daben herrixe!