---
id: tx-2002
izenburua: Teloia Jaisten Denean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dL5L8EEEHHw
---

Gertu bilatu nuen,
eta urrutiko lurraldeetan
profezietan ez zeunden,
ez zeunden mapeta,
ez zeunden psikiatraren
kolorezko piluletan,
kontzeptuetan ez zeunden,
ez zeunden formetan.

Guda-zelaian suak
utzitako errautsetan,
maitaleen bihotz desolatuetan.

Eskuetatik keak
ihes egiten duen bezala
desagertzen zara.

Ez zeunden gurasoen
kajoi debekatuetan,
ez zeunden exiliatuen
oroitzapenetan,
herri indigenetan
eta hitz konkistatuetan,
ispiluetan ez zeunden,
ez zeunden orekan.

Eskuetatik keak
ikes egiten duen bezala.

Joaten zara
nola izua
suiziden azken hitzetan,
nola herri bat 
desertoreen kantetan,
errutina
presoen ihes planetan,
nola egia
teloia jeisten denean.

Izkutatzen zara
desagertzen zara
teloia jeisten denean.