---
id: tx-1300
izenburua: Ekaitzetan Eutsiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sjWgWlk_1YY
---

EKAITZETAN EUTSIZ


Nahiz haztean labana den belarra
Tximeleta bihurtuz joan da beldarra
Tximeletak du orain barruan harra

Zer egin dut gaizki? Zer egin dut ondo?
Beste aukerarik ez al zen egongo?
Joandako trenei ez diet itxarongo.

Nahiz ez dudan ahanzten
Ia ez naiz jolasten
nintzena izatera

Ekaitzetan eutsiz
ta kristalak hautsiz
nire bideak egin nau.
Zauriak igurtziz
ta beratzen utziz
bideak aurrean dirau.

Leziorik ez daukat inorentzat
Panfletorik ere ez neretzat
Soilik desilusio batzuk eta ametsak.

Nahiz kontua ez den hegan egiten ahanztea
Utopia bada berriz hutsetik hastea
Gutxi ote da oinez ibiltzen ikastea?