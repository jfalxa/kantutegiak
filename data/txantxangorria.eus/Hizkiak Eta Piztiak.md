---
id: tx-4
izenburua: Hizkiak Eta Piztiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ksqfwwMEyu8
---

Hitzak: Aitor Bizkarra

Grabaketa eta nahasketa: Oihan Lizaso
Masterizazioa: Jonan Ordorika
Artea: Zoe Martikorena


Kontaktua: 
@desgaraian
desgaraian@gmail.com

Basoa hartu du suak
lehenik hil dira hartzak
haize berriek haritza
jotzen dukete zahartzat
beldur dira azkonarrak
muskerrak ta orkatzak
hizkiok eta piztiok
hirira daramatzat

Uso karraskariak ta
arratoiak hegaldan
gau-hontza desegokiak
saldoka zaldi saldan
txakur errabiatuak
ehundakako samaldan
hauts egindako mandoa
urtarrileko maldan

Hiria ere hartu du
suak besozabalik
hobe nukeen mingaina
erre izan ez balit
euri zirinak kaleak
aton ditzake polit
baina ez dago erabat
garbitzen den odolik

Euskararen hitz bakoitzak
esanahirik badauka
Hori da amona beltza
metroan alarauka
basurdea desgaraiko
basatzetan iraulka
harri estrategikoa
presentearen aurka

Hitzak ez badira bala
ta hiztunak soldadu
zer da ba “irabaztea”
zer da “haiek” zer da “gu”
euskararen hitz bakoitzak
esanahirik ez badu
orduan erabiltzeko
beharrik ez daukagu