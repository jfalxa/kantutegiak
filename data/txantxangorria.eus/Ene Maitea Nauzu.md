---
id: tx-2867
izenburua: Ene Maitea Nauzu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UrZSVNjtrH4
---

Ene maitea nauzu
bihotzetik desterratu
nola jakin duzu
hainbeste zaitudala maitatu
eta nire suaz, eta nire suaz
minez ez zara, ez zara urtu.

Udazkeneko lore
lur gorri ilunen antzu
odol beroaz omen nintzen
nire bihotz labean sartu
baina arratseko itzalaz
baina arratseko itzalaz
soli nia bihurtu, bihurtu negu.

Usteak erran zidan
zutaz nintzela jabetu
baina ederki didazu
arima barruan engainatu
ni naizelako amodioz prest
ni naizelako amodioz prest
hau besterik ez diat, ez diat lortu.