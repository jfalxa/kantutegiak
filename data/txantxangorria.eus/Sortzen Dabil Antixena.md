---
id: tx-2219
izenburua: Sortzen Dabil Antixena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L4MKfq2Rnf0
---

Konfinamenduan GAZTE(AK) ETXETIK ekimenean parte hartutako herriko hainbat artista gaztek sortutako Antixena Gaztetxeko abestixa.
Gaztetxian bertan autoekoiztua.

Letria:

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Gaztien taupaden trena
Ta grinan etxe nomada
Bakantasuna norma da
Artista librien izena
Herri berri baten sena
Atzoko errautsetatik
Gaurko zalantzetatik
Sortzen dabil Antixena

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Erosotasunan portu
Dan kobara ez ohiketako
Lanian jarraiketako
Gogua ez da agortu
Eskuz mundu hau birsortu
Lantzen bertsuen errima
Gure herrixan arima
Ez dein izan basamortu

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Gizartia aurreraka
Doian irudipenian
Pausuak emuten dabil
Baina leku berdinian

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Sortu zan gaztetxetik
Zenbat gazte etxetik
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

Hitzetan ta kantuan
Segi daigun burrukan
Erakutsiaz alkarri
Nork bere alia jarri
Antolakuntza da eta
Askatasunan oinarri

------------------------------------------------------------

Musikari eta abeslarixak:
Beñat Elorza
Pello Uriarte
Ane Irazabal
Aitor Regueiro
Olatz Kortabarria
Josu Zubia
Miren Fernández
Luis Txintxurreta
Maddi Santa Cruz
Aiert Alberdi
Naiara Olalde
Julen Murua
Naia Herraez
Jon Etxarri
Maddi Arlanzon
Unai Murgiondo
Jone Markuleta
Julen Agirre
Laia Polo
Irene Urzelai
Aran Sagarzazu

Soinu teknikarixak:
Imanol Astudillo
Iñaki Zubia

Bideuan arduradunak:
Xanti Ugarte
Mikel Garai
Gontzal Belategi
Jone Markuleta

------------------------------------------------------------

Esker berezixak proiektu honetan era batera edo bestera parte hartu dozuen danoiri: Astu eta Josu zuen lanagatik; Zubia grabaketako materialagatik; Mikel antolakuntzagatik; Xanti ikus-entzunezkuan materialagatik; konposiziño taldia zuen denporiatik; bideo taldia lan ederragatik eta musikari eta abeslari danei proiektu honen parte izatiatik!! Eta noski, Antixena Gaztetxeko asanbladiari!
Zuek danori ESKERRIK ASKO!!!
0 iruzkin