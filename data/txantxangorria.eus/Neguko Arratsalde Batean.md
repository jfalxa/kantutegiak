---
id: tx-213
izenburua: Neguko Arratsalde Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gyF5HaAzR0c
---

Neguko arratsalde batean

Atearen beste aldean agertu zinen,

Aurpegia zikin bezain ernai,

Izate ausarta bezain trebe

Neure maniura joaz

Bere jabetza ziurtatu nahian

Hitzez zin egin arazi zenidan.

 

Txirritak kantatu zituen

Zure aiton-amonen historioak

Nola lapurretan batzuek,

Dantzan besteak

Perkaiztegi kalean ostegunero

Deabruek andereak

Bereganatzen zituzten.

 

Eta zu, eskuak sakelean,

Maltzur begirada,

Azokan paseatzen zaren artean

Zure arreba

Zango bat gona azpian ezkutaturik

Plañitzen topatu dute…

 

Orain zure joan etorrian

Agian ikusiko zaitut

Ametsak behar dituztenei

Zorion menturak kontatzen,

Gitarra batekin kantuan

Agian…