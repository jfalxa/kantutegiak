---
id: tx-3012
izenburua: Maite Ninduzula
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MAEkB4daHHI
---

Maite ninduzula bazinoztasun ere
ez ninduzun maite nik nahi nuen beste

Bihotz minduraren beldurrez
ta eman beharraren hazkurez
une xumeak soilik zenituen maite,
edertasunaren onuren
eskean zeunden
maitasunaren arretak antzu geratu zirian.

Oker zaude maitasuna, hori dela baduzu uste.

Zuk nahi zenuena
izan zenuen adorez
nik eman nahi zenidan, besterik ez.
Zuk zeure gisan
maite ninduzun
ta izan zitekena ezereztu zan.

Oker zaude maitasuna, hori dela baduzu uste.

Dakidana da
harriak hotzez hilko direla, lurra egarriz
dakidana da
ingurua isilik izango dela,gogoa gurariz.

Maite ninduzula esan zenidan
maite ninduzun zeure gisan
maite zintudan maite ninduzun
maitasuna loratu bait zan
maite zintudan maite ninduzun
ta izan zitekena ezereztu zen.