---
id: tx-2522
izenburua: Burusoila!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L5oElktrek8
---

Zartako-K taldearen Burusoila abestiaren bideoklipa, "Alienados" diskatik hartua.


Goitikatu dut nire kotxean
“Euskadi Gaztea” jartzean.
Bere musika kaka zaharra bada,
norbaitek argi eta garbi esan behar

Goitikatu dut nire kotxean
“Euskadi Gaztea” jartzean
Bere musika zaborra bada,
norbaitek argi esan behar

Burusoila!
Buelta dadila!
Burusoila!
Rock erradikala!
Burusoila!
Nora joan da?
Burusoila!
Euskal musika. (x2)

Guzti honen errua jurru-pijoena da!
Guzti honen errua EITB-rena da!