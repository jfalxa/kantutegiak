---
id: tx-3251
izenburua: Lo Dago Gure Haurtxoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tq1MxQMu0fk
---

Lo dago gure haurtxoa
Sehaska goxo beroan
Olentzerok ekarriko
Dionaren esperoan (bis)

Ta bitartean
Gabon gauean
Olentzero dator
Gurdi gainean (bis)

Ikazkinarekin amets
Egiten du haurtxoak
Jostailu berriak eta
Karamelo goxoak (bis)

Ta bitartean
Gabon gauean
Olentzero dator
Gurdi gainean (bis)