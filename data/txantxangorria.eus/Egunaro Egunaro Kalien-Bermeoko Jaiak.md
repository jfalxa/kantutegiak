---
id: tx-3374
izenburua: Egunaro Egunaro Kalien-Bermeoko Jaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ph5b2ihAv9M
---

Egunaro egunaro kalien - 2002
 
Alberto Rodríguez: gitarra akustikoa
Eloy Urrejola: baxu elektrikoa
Borja Barrueta: bateria eta txalaparta
Garikoitz Aldekoa: txalaparta eta didjeridoo
Iñaki Aurrekoetxea: hitzak
Mikel Urdangarin, Joseba Tapia, Iratxe Ibarra, Bingen Fernández, Josu Zabalondo, Ana Tere Bengoetxea, Ander Oleaga, Juan Maguregi, Garazi Barandika, Arantza Arronategi, Aroa Anasagasti, Janire Penades, Gentzane Goienetxea, Marisa Uriarte: ahotsak 

Soinu teknikariak: Alberto Rodriguez eta Alberto Macías. 


Laminen puntetik zuzenien
giruek berbiztu dauenien
mahoie jantzitte gonapien
Lameran sartu da jente artien
1
Irrintzi artien hor Xixili
kendu dau galbanie
zapidxe imini
jai itsosuen jai baserridxen
egunaro egunaro kalien
2
Gaztelugatxeko Doniene
bedeinkatu gagizula
lehorrien be
bermiotarra lagun artien
egunaro egunaro kalien
3
Itsosue, lurre ta harridxe
Bermion topa lekizu
Euskal Herridxe
herri aske ta euskaldunien
egunaro egunaro kalien
Atrakaizu txo batela!
portuen dala martxie
gaur libre dozu galtzie
Xixili topakozu
Andramaidxetan!
4
Izaro zaindari maitagarri 
itsotxoridxek dinotsu 
hamengo barri
nekien zantzurik ez danien
egunaro egunaro kalien
5
Non ete Demiku, non Almike?
eguzkidxe Ogoñotik
dabil kikuke
gaur bai Urdaibai! gu bittartien
egunaro egunaro kalien
Atrakaizu txo batela!
portuen dala martxie
gaur libre dozu galtzie
Xixili topakozu
Andramaidxetan!
Alabatxi! Arrainek be etxerako ordue 
dakoiela!
Zer dinostazu! On trankil, Xixiligaz jungo 
nai-eta!
Atrakaizu txo batela!... (bir)