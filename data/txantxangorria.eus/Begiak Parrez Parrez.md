---
id: tx-1896
izenburua: Begiak Parrez Parrez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N1IPfibhv7c
---

Begiak parrez parrez 
bihotza negarrez (bis) 
Halaxe joaten nintzan maitia 
zugandik dolorez (bis)

Ama zuriak neri 
parian pasata, (bis) 
ez dit agurrik egin maitia 
burua jirata. (bis)

Ama zuriak neri 
ez agur egiteko, (bis) 
zer palazio dauka maitia 
zuri emateko. (bis)

Palazio eder bat 
haitzaren gainian, (bis) 
ez da euririk sartzen maitia 
ari ez dunian 
ez da haizerik sartzen maitia 
ez dabilenian