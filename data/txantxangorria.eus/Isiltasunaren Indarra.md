---
id: tx-546
izenburua: Isiltasunaren Indarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Hk5WfLa60r4
---

Musika: Iñigo Etxezarreta (E.T.S.)
Letra: Daniel de Ayala & Iñigo Etxezarreta (E.T.S.)
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Zuzenketak: Zuriñe Gil
Ekoizpen exekutiboa: Baga-Biga

Ekaitzaren hasiera
sumatzen zen jada herrian,
aldaketaren mamua bazetorrela.
Hegoaldetik helduta
sartu ziren lurraldera,
etxeetan bilatuz alkatea zena.

Berarengana jo zuten
saiatuz hitzak lapurtzen
lege bat eta bakarren izenean.
"Eseri eta erantzun gure galderak".

Ohhh… Isiltasunaren indarra
inguruko ahotsen artean.
Ohhh… Isiltasunaren indarra
azken taupadak entzutean.

Jarri ezazu zerrendan
gure troparen kaltean
bidean harriak jarriko dituztenak,
herrian sartu ezkero 
aurrez aurre ikustean
behar bezala agurtuko ez gaituztenak.

Aukeratu egin zuen,
ez zuen salatu ezer,
hortik ez zen zerrendarik atera.
Orain beraiek ez daude,
gu geratu gara hemen,
musikarekin memoria egitera.

#EnTolSarmiento​ #IsiltasunarenIndarra