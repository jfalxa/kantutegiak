---
id: tx-342
izenburua: Itxuraldatze 1
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D1sdHiVZJ94
---

"Itxuraldatze-1" (Jon Bergaretxe/Joxean Artze) 1978.


Joxer-ri emaztea ostu ziotenean pakeagatik,
Ez zuen ezer esan eta muuu!
Eginaz aldegin zuen.

Bere alaba bortxatu ziotenean pakeagatik, ez zuen
Ezer egin eta kurrinkaz ospo egin zuen.

Bere semeak goseak hil zizkiotenean pakeagatik ez zuen inor jo
Eta beee! Eginaz izkutatu zen.

Galtzak kendu ta astindu zioten: 
Pakeagatik ixilik gelditu zen. 
Orduan, arratoi batek
"amen" batean irentsi zuen Joxe
Zingurria zelakoan.