---
id: tx-951
izenburua: Keari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UCk2qQEEKUI
---

Nøgen – Keari
‘Liv til døden’ albumeko hirugarren singlea.

Donostiako Antzoki Zaharrean emandako kontzertuko irudiekin osatutako bideoklipa.

Grabaketa eta edizioa:
Ander Merino Etxebeste, David Olariaga eta Thomas Breton.

Abestian parte hartzen dute:
Ander Zabala eta Aitor Valcarlos (haizeak)
Garazi Esnaola (teklak)

Haritz Harreguyren estudioan grabatua eta nahastua.
Victor Garcíak masterizatua Ultramarinos Mastering-en.



Egun batez galdu nituen behin
nire azalaren liburuan zeuden
zenbait hitz,
eta zenbat aldiz
erori garen zutitu baino lehen.

Ez zaigu ilunetan sua eta kea besterik,
ez zaigu ezer gehiago geratuko gero.

Ahaztu nituen egunen antzera
igaro den zerbait garela, 
egiazko zerbait, betirako den kanturen bat, kanturen bat edo beste.

Ez zaigu ilunetan sua eta kea besterik, 
ez zaigu ezer gehiago geratuko gero. 

I don’t know if I believe in anything that’s out of here, 
I don’t know if I believe in anything but you. 
You can’t bury all. 
And now we’re here, 
say can you hear, 
how are we singing, singing to fire and smoke. 

Beranduegi da orain eta ez dago ezer 
erortzetik gertu sentitzea lez. 
Beranduegi da orain eta ez dago ezer, 
berdin dela irabazi edo galdu dudan. 

Ez zaigu ilunetan sua eta kea besterik, 
ez zaigu ezer gehiago geratuko gero.