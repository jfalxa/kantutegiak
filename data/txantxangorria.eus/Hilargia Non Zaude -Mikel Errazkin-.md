---
id: tx-3179
izenburua: Hilargia Non Zaude -Mikel Errazkin-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nNf8Yxy4EwU
---

Esaidazu nere hilargi maitea;
Zergatik da bizitza hain krudela?
Behin baino gehiagotan bururatu zait,
bizitzaren muga zeharkatzea.

Inpernua ondo ezagutzen dut,
pare bat aldiz bertan egon naiz!
Satan partikularra daukat nik,
gorputzetik desagertzen ez dena.

Motibo baten bila
nabil egunero.
Larrosaren arantza
barruraino sartua.

Esaidazu nere hilargi maitea;
heriotzaren hegian ikusi gara.
Geru zu okerrago jarri zinen,
ta inportantzirik ez nion nahi eman.

Hasieratik onartu ezina zen,
ta azkenaldi hontan konturatu naiz
agian nere hilargia ez dela egongo
eguzkia ateratzerakoan.

Motibo baten bila
nabil egunero.
Larrosaren arantza
barruraino sartua.