---
id: tx-1438
izenburua: Galdetu Nahi Dugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VSAGgzE0PTY
---

Ez gara hutsetik jaio
Katea kateari lotzen zaio
Galiziatik gatoz, Extremaduratik,
Andaluziatik, Errioxatik,
Marokotik, Keniatik,
Eta erlojuak ditugu tak, tik, tak, tik…



Ondoan nahi dudalako
ez naiz inoren aurkari
behin izan nintzen haur hari
izango dudan haurrari
izena jarri nahi diot
zapaltzen dudan lurrari


3 zatia: estribilloa
Ta zuek zer nahi duzue?
Ahots batek galdetu du
batzuek argi daukate
asko ez daude seguru
galdetu egin nahi dugu
galdetu egin nahi dugu
galdetu egin nahi dugu



RAPa 


Trenean iritsi bazen
behin hona nire amona
nola esango diot nik
nork heldu behar dun hona?
larogeita hamar gradu ta
lur bilakatzen da horma



Ta zuek zer nahi duzue?

Ahots batek galdetu du
batzuek argi daukate
asko ez daude seguru
galdetu egin nahi dugu
galdetu egin nahi dugu
galdetu egin nahi dugu