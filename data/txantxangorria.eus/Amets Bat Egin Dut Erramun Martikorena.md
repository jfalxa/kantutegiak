---
id: tx-3129
izenburua: Amets Bat Egin Dut Erramun Martikorena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/me3NPzswOv0
---

Eskual Herri eder batetaz
egin izan dut ametsa,
ez dela nahoiz bururatuko,
nehork ez dezala pentsa.
Zer zoriona Euskaldun seme
bakotxak sendi baleza,
gau ezti batez ametsak neri,
utzi dautan esperantza.

Ametsak zuen beribilean,
joan nintzan bidez bide.
Herri seinale erdarazkoak
norbaitek zituen gorde.
Euskara batu garbi eder bat,
zagon erdararen orde.
Lehen aldikotz ikusi nuen,
bidea neure haurride.

Bakezko bizi eder bat zoan,
hiri eta herrietan.
Euskara baizik etzen aditzen,
bakotxaren ezpainetan.
Euskal Herria libratua zen,
bere haurrak belaunetan.
Herria ukatu zuten denak,
zoatzila nigarretan.

Arraboteak beteak ziren,
gaztez eta pilotariz.
Ostatuetan entzun zitaiken,
kantu xaramela ainitz.
Lehengo etxe xahar ederrak,
zagotzin elgarri irriz,
ikurriñaren pare zirela,
gorri berde eta xuriz.

Laborariak ikusten ziren,
pentze eta alorretan,
langileak ere goizetik
arrats zirela lantegietan.
Denak gogotik zauden lanean,
xede bera bihotzetan:
indar handi bat egin dezagun,
herria azkartzekotan.

Jenden artean ez zen senditzen,
ez urgulu ez herrarik,
lehenagoko etsaia ere
errespetatzen zelarik.
Gureak ziren seme martirrak,
maitasunez estalirik,
Euskal Herria askatua zen
eta gerlak ehortzirik.

Gure Herriak orai artean,
zonbat odol du ixuri
horrela luzaz bizi laiteken,
eni etzeraut iduri.
Gaurko amets hau agertzen bada
Herriaren salbagarri
Euskal Herriko seme-alabak,
egin zaizte ameslari.