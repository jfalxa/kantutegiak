---
id: tx-2100
izenburua: Lurralde Bakarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OsvzclKlLeI
---

Hegoaldetik idazten dizut
Gaur zugana hurbildu
Nahian banatu gintuztenei
aurka eginez
zu berreskuratu nahian.

Ta gure zelai oriegiek
Elkartzen gaituzte askatasunean

Atzo gaur eta bihar
Beti izando gara
Muga guztien gainetik
Lurralde bakarra.

Lerro batekin soilik banatu gintuzten
Betirako min eginez.
Ez gaituztela suntsitukoesaten dizut
Nik Lapurditik zin eginez

Ta gure arteko mendiek
Elkartzen gaituzte askatasunean.

Atzo gaur eta bihar
Beti izando gara
Muga guztien gainetik
Lurralde bakarra.
Elkarrekin eskutik aurrera egin dugu.
Ta egingo dugu gehiago
Betirako bakarra izango garela
Ziur nago.