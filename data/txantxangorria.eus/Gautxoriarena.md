---
id: tx-104
izenburua: Gautxoriarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LLolj20jRvI
---

Aurrez aurre jarri garenez behingoz
botako dizut
orain arte ezezagun bat izan zarela, lizun.
Eta hurbiltzen zoazen heinean
beldurra ematen didazu
umil-umil nere gerrondoa hartuaz diozu:
"dantzatu dezagun"

Eta gautxori batek kantuz 
dio ikusi zuela ilargia
bere buruarekin bueltaka
bizkarra besarkatu nahi baino ezin helduz

Lotsatzen naiz, galtzen naiz, ez dakit dantzan...;
Zurekin behintzat
Eta kafe beroak eskaintzen dizkidazu
beti lasaigarritzat

Eta ezin dut lorik egin geroztik...
eta ezin dut lorik egin...

Eta gautxori batek kantuz
dio ikusi zuela ilargia
bere buruarekin bueltaka
bizkarra besarkatu nahi baino ezin helduz

Eta ezin helduz eguna erne pasatzera.
11 iruzkin