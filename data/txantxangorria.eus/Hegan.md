---
id: tx-647
izenburua: Hegan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xP4FiWpT9eE
---

Izan ginen desastre bat
Iparrik gabeko mapa zahar bat
Zu nigan ta ni zugan

Zirriborro bat, deskarte zikin bat
Laboaren orrietan
Behe-lainoan galdutako hitzak
Baina...

Hegan, egin genuen hegan
Kantauri itsasotik ikus mugetara, hegan

Izan ginen kaos hutsa
Laurhogeita hamarreko bi bala
Ibilbide gaiztotuaz

Akorde bat, akorde minor bat
Inork entzun ez zuena
Arrano bat zaurituta airean
Baina…

Hegan, egin genuen hegan
Kantauri itsasotik ikus mugetara, hegan 

Ta jarri ginen hego-haizearen esanetara
Ekaitza hastear zela
Ekaitza hastear zela

Begiak itxi ta azken besarkada bat
Erori bitartean
Erori bitartean

Hegan, egin genuen hegan
Kantauri itsasotik ikus mugetara, hegan 

HEGAN by @zetak_org

Directed by  @pedro.artola
Produced by @sararenteriac
Director of Photography  @carlesfgali
1AC  @foquistabarcelona
2AC  @nestorurbieta
Gaffer @lordverd2046
Spark @isaebens
Spark @keko_ak
Direction Assistant @marengonzalez
ZETAK´s Visual Director @iratxe_rb
Art Director @dana_nala
Art Director Assitant @jgracenea
Stylist @julspuig
Makeup & Hair Department Head  @makeup_san
Makeup & Hair Department Assistant  @albahe_makeup
Makeup & Hair Department Assistant @claudiam_mua
Makeup & Hair Department Assistant @eneritzmakeup
Still Photo  @ikergozategi
Graphic Design @practica.design
Editor @askforcherrycola
Colorist @yuliabulashenko
Visual Appreciation @alvagalim
3D Artist @zigor
Sound Mix @paxkaletxepare
Mastering @simoncapony
Production team @katringinea
Production team @joseba_razquin
Production team  @artzai_iraurgi
Production team  - Miguel Escala
Production team  - María Carmen Escala
Production team - Miguel Ángel Reparaz
Production team - Lourdes Navarro
Runner @arammujal

COLLABORATOR
@gaztea_irratia

SPECIAL THANKS
@pandaartistmanagement
Ayuntamiento de Andosilla
Kontxi Marteles Ripa
@Napalmrentals

MAIN CHARACTERS
Main character - @Pello_reparaz
Bride - @olatzlopez
Fiance - @sergeyhernandez25
Lens woman - Encarni Hernandez
Car owner - @josebasacristan
Car woman - Amaia Beaskoetxea
Priest - Rubio
Bride´s father - Miguel Escala
Bride´s mother - Karmele Araña
Bride´s grandfather - Jexux Razkin
Pianist - Mª Paz Barandiaran
Bridesmaid - Miren Berastegi
Bridesmaid - Ramoni Berastegi
Bridesmaid - @andreagamboa_9398
Bridesmaid - @maialen.zelaia
Group of people looking at the end:
Miguel Mendinueta
Ainara Goikoetxea
Amets Asa
Ainetz Lazkoz
Haitz Lazkoz
Janitz Moro

SECONDARY CHARACTERS
Ainhoa Etxebarria
Ainhoa Vitoria
Antton Telleria
Mikel Senar
Endika Mendinueta
Julen Luis
Asier Ieregi
Joseba Ezkurdia
Nahia Galbete
Jon Ander Mendinueta
Ariane Flores
Irati Flores
Camino Lakuntza
Edurne Lakuntza
Mª Carmen Arbizu
Patxi Goikoetxea
Eneko Senar
Maia Ginea
Haizea Moro
Miguel Ángel Reparaz
Inés Reparaz
Kattalin Mazkiaran
Juan Carlos Reparaz
Miriam Beaskoetxea
Irune Ginea
Koldo Mundiñano
Rafa Goñi

ALSO THANKS TO
End song: Mikel Laboa
Andosillako Parrokia
Embutidos Arbizu
Ramos Industrial
Traperos de Emaus 
Geltoki
Willy
Carlos & Carrocerias Maturana 
Txubeldi Zurgintza
Aitor Karasatorre
Manu Gomez
Miguel Escala
Floristería Lorea 
Lighting Equipment - Argilun 
Xabi Maiza
Lurdes Mundiñano
Lonbreen Denda
Ernesto Leiza
Miren Pereda
Razkin Harategia
Anastasio Razkin
Eneko Usandizaga
Josu Agirre
Joxe Javier Berastegi
Rosa Carmen Unzilla
Mari Carmen Senar
Manuel Goikoetxea
Beñat Goikoetxea
Mikel Lakuntza Lakuntza
Aitor Zabalza
Eneko Lazkoz
Ricardo Sanchez
Conchi Zabalza
Josu Reparaz
Beñat Jaka
Mari Nieves Lizarraga

#zetak #hegan
66 iruzkin