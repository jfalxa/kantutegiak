---
id: tx-1268
izenburua: Urrats Bat Marraztuz Etorkizunean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UDDerJ1kdpU
---

Lerro zuzen bat arbelean
gezi bat sartuz gure burmuinean
aspaldi ikasi genuen oinez
aspalditik etzaigu kabitzen gezurrik
arma bat daukagu irudimena da.

Geldirik nahi zaituzte
sumiso behar gaituzte
itzarri, altxatu, bizi, aske izan!

Urrats bat aurrera etorkizunera
helburu, jomuga argi!

Zure proiektuak defendatu
ikasle matxino aztarnak utzi
suntsitzen eta eraikitzen
antolatzen, segi borrokan
helmuga jakinera.

Huts egitetik, saiatu eraikitzen
akatsetatik helmugak argitzen
sustraietatik atera dezagun garra
oldartuz sistema arrotz honen aurka.

Pauso bat aurrera elkartasunera,
urrats bat marraztuz etorkizunean.