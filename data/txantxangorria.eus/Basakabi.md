---
id: tx-84
izenburua: Basakabi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c-cyl5DHgR4
---

Zuzendaria: Mattin Zeberio Larraza
Kamera laguntzailea: Telmo Basterretxea
Dron gidaria: Mikel Bereziartua
Eskerrak: Zuhaizti eskolako lagunei
eta milesker Oso Polita :)

Galerna Estudio eta Andoainen grabatua

Asko, asko maite zaitut baino
Nekatzen ai nazu majo
Zure watxapak eta zure txapak
Zure berri txarrak, zure ezpalak
Mirua, hegan noa zerura
Ta zure hitza zertan geratu da?
Lokaleko leihotik alde iteko gai ez zara
Ta noizbait, noizbait aurki nahi banauzu
Amaiur fondo sur
Hotza daukat ta ez nauzu besarkatu
Ez ukatu
Momentu bat leihopean bi katu
Ilun dago, ilun
Soy una trampa pa' atrapar a dios
Kromeltxaren harri hori (harri hori)
El pitxu del pitxulari (pitxulari pitxulari)
Deus ez zaio zor gauari (gauari)
Hitz egin baino zertako
Hauts bat dugu belarritako
Kantatzen dizut zuri nik
Maitatzen zaitudalako
Soy un guerrero como Julen
Galdua nago baina hainbeste ez dut uste
Atzo egindakoaren deabruek
Lasai lo egiten ez didate uzten
Veo de lejos a esos buitres
Habla con Elías que a mí me da lache
Soy heroe del pueblo, Lute
Eres guapa pero mala en verdad no me cunde
Pastilla hori bezala baina dame dame
Hasta que me queme, Notre Dame
Aunque me duela, aunque me mate
Yo en las puertas de tu infierno, Dante
No quiere follar con Beñat
Quiere follarse al Kiliki
Y yo soy un chico facil
Pero no soy subnormal
Naizena, erre nahi nuke dena
Se me veía venir desde txiki
Yo te ofrecí un katxi
pero tu eres Moet Chandon y yo de anfeta
Asko, asko maite zaitut baina
Nekatzen ai nazu majo
Mirua