---
id: tx-304
izenburua: Lekukoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/f_yBJk9AL90
---

Eskerrik zintzoenak eman nahi dizkizuegu abesti honen bilakaeran honaino iritsi arte lagundu duzuen guztiei. Ideia txiki batetik abestia egin eta forma eman genionetik Josu Ervitiren eskuetatik pasa eta bideoklip honetan amaitu duen arteko bidea zoragarria da. Eskerrik asko Plaza de la Cruz ikastetxeari bertako aretoa bideoklipa graba genezan uzteagatik. Haira eta Beñati abesti hontatik horrelako proiektua eraikitzen laguntzeagatik, y gracias también a Marín, que terminó de darle forma al asunto. Plazer bat izan da zuek denak lankide zein lagun izatea. Mila esker!

Zuzendaritza, kamara eta edizioa/ Dirección, cámara y edición: Beñat Goia
Zuzendaritza artistikoa/ Dirección artística: Haira Ariztegui
Argiztapena / Iluminación: Daniel Marín

Gure txanda da. Zentsuraren guda hontan irabazlerik bada aurkaria ezabatzen duena baino ez baita izango, eta guk ez dugu garaile izan nahi, baina bizirik gaude eta hala jarraitzeko asmoa daukagu, gure hizkuntzak iraun beza abesti berriak ikusteko, gure hizkuntza izan dadin abestear dauden abestien lekukoa.

Abestia / Tema: Lekukoa
Albuma / Album: PARADISUA
Musika eta letra / Música y letra: Burutik
Musikaren grabazioa, nahasketa eta masterizazioa / Grabación, mezcla y masterización de la música: Josu Erviti (Drum Groove Studio, Berrioplano)
Diskako ilustrazioak / Ilustraciones del disco: Miren Huarte

Kontaktua / Contacto: 635 82 17 96
Posta elektronikoa / Correo electrónico: burutikj29@gmail.com



Letra[EUS]

Herri honen nortasuna babesten duen edozer
oztopo bada haientzat,
mehatsu bihurtzen.
Hizkuntza hau ez litzateke egun dena izanen
zapaltzeko beharra ez balute…

Gure hitzen akatsak betiko esaera zaharrak
jende aurren babesteko erabiltzen dituzte.
Debekua, beldurra, ezinegona ta sua
atzotik biharkora gehiagora.

JADA HUSTUTA DATZAN HURA BERRIZ HUSTEN SAIATU,
ERRAIAK HAUTSI TA ARNASIK GABE GERATU,
DENBORA HIZOZTU TA GARRASIA BERA SENTITU.
IXILTASUNA URRATZEKO AHALMENA NAHI DUT!

Gezurrak gezur 
ta egiak egi.
Norberak bereak.
Haienen aurrean bortitzak
dirudite gureek,
baina egiaren aldarri dira,
ta gezurraren etsai.
Bizirik mantenduko dugu abestear
dauden abestien lekukoa.
Ozen eta argia, beti!

JADA HUSTUTA DATZAN HURA BERRIZ HUSTEN SAIATU,
ERRAIAK HAUTSI TA ARNASIK GABE GERATU,
DENBORA HIZOZTU TA GARRASIA BERA SENTITU.
IXILTASUNA URRATZEKO AHALMENA NAHI DUT!