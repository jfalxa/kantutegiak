---
id: tx-1092
izenburua: Sagastiari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tqb3PRLyJM8
---

Goazen danok batera mendira
sagarrak jatera
sagar debekatua
da gozoagoa
gozoagoa da
hobeago da
ez eroria oinpera.
Negua beti hotza
etxea dago hutsa
seme laztana falta
herria du maita.
Maita herria
Gure aberria.
sagarra debekatua.
Gurdi baten barruan
ezin kabituaz
batak bestearen aurka
daukagu burruka.
Gure burruka ezin bakarka
habe gurdiari bultza.
Lehenengoak giñake
sagastira iristen
ikasiko bagenu
gure indarrak biltzen.
Indarrak biltzen
elkar laguntzen
bestela gara akabatzen.
Amatxo huelga hasi da
bi mila ba gera
sartaia gorde zazu
aguanta behar da
behar da segi
behar da bizi
gerrikoa estutua.
Gu langileok seme
ezin huelga zale
beste erara konpondu
behar dugu hobe
huelgarik gabe
haserre gabe
ezin burruka gintezke.
Nere amatxo maite
konturatu zaitez
explotazio dela
denon berdintzaile
berdinak denok
pobre geranok
alkartu behar gintezke.