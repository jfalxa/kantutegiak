---
id: tx-1991
izenburua: Nahi Duzunerako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qh6ZaVfswvw
---

Urrezko gauaren itsasotik nabigatzen duzu
naufragoen oholekin jositako ontzian
izurde urdinen doinuek kantu hau dakarte
ta izarren begi guztiek zuregana naramate

Hain politta izan da baina
bukatu egin da
gure bizitzak bat ziren
berriro bi dira
nahiz eta jakin une hauek
gogorrak direla
sinisten dut zu eta ni
lagin izan gaitezkela

Bizipenengatik eskerrik asko
eskerrik asko, pozik bizi izan naizelako
irria ez dadila bilaktu malko
hemen duzu lagun bat nahi duzunerako

Ez ote zen bakardadeari beldurra niola?
Kostata baina zabalik utzi dut kaiola
Hori nahi dut nik orain denbora batean,
aske naizela sentitu hegalkada bakoitzean...