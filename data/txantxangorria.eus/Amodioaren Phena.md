---
id: tx-134
izenburua: Amodioaren Phena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z7vJiuxOfRA
---

Amodioaren phena 
Amodioaren phena oi phena handia! Orai ezagützen düt zer den haren phena. Amodioa ez balitz den bezain krüdela, ez nezakezü erran maite zaitüdala. 
Mündian zunbat urhats oi düdan egiten, ez ahal dira oro alferrak izanen, Jentek errana gatik guretako elhe, maitia, trüfa nainte zü bazintüt neure. 
Zelian zunbat izar maitia ahal da ? Zure parerik ene begietan ez da, 
Neke da partitzea, maitea, enetzat, adio erraiten deizüt denbora batentzat. 
Nik errana gatik, maitea, adio ! Ez nezazüla üken, zük, otoi hastio, 
Bena bai bihotzetik izan amodio, etzütüt kitatüren tunban sar artio. 
Hitzak: herrikoak 
Müsika: Jean ETCHART