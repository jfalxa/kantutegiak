---
id: tx-503
izenburua: Kolakao Final
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jwBHdOznZ0o
---

Y ves al padre José,...

Gutxi batzuk aterako gara baina 
Sufiziente izan gaitezke lau
Lauetatik bik hiru egunetan ez dute lo egin 
Erremediorik ez dago

Andoni poltsa aterata begirada guztiak 
Dirudite'in dutela bat
Lauetatik bik hiru egunetan ez dute lo egin 
Erremediorik ez dago

Jugando a que el eme es kolakao
Y que las pepas no me dejan kolokao
Te como la oreja y si pudiera la boca el coño las tetas y mas

Neure nobiaren amari tonbolan satisfyer bat tokatu zaio 
Berak ez du erabili nahi lotsa izanen da?
O ze arraio

Ni lotsagabe jaioa erraten nion 
Maria Luisa emaion
Maria Luisa emaion
Mundura gozatzeko ginen jaio

Gu jolastu ginen lehenik
Baina asko dugu egiteko oraindik 
Zuek hiltzeak etzaigu aski
Auzoan urtu ta auzotik hasi x2

Ez dut berriz esanen zuen izena 
Prima a mi me es igual
Antza denez ez dira oso abilak 
Marketing kontuetan

Berandu heldu ginen ta hala ere 
Gu beti gaude aurrean
Bildur dirade bihurtutzeko 
Norbait gure esanean


Jugando a que el eme es kolakao
Y que las pepas no me dejan kolokao
Te como la oreja y su pudiera la boca el coño las tetas y mas