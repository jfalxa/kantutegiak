---
id: tx-982
izenburua: Martxoak 3
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w3fwV5KvKzE
---

Hotza hezurretan, gorrotoa zainetan zenuen egun horietan
oinarrizko eskubideen alde zinen borroketan
langileen duintasun eza eskuratu nahian
eman zituen zure urteak eta zure bizitza.
Elizetan abesten zenuten internazionala
kaleetan oihukatzen zenuten greba orokorra!
Langileen borroka eta indarra,
somatzen zen giroan, hiria zuek bihurtu zenuten
borroka adibidea.
Martxoak hiru, martxoa ilun,
martxoa beltza, egun iluna,
egun kuttuna,
Gasteizko herriaren gogoan.
Bortizkeriak akabatu zituen hiri honen semeak,
bost krabelin adokinetan, herria karriketan
langile erresistentziarena izan zen garaipena
sufrimenduak eta heriotzak gogortu zigun bihotza.
#saveyourinternet