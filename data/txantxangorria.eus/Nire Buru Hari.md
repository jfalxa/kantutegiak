---
id: tx-1468
izenburua: Nire Buru Hari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_RbsySWsKe4
---

Azken aldian begiak bildu
gabe pasa dut hainbat gau
edonor izan daitekelako
bere minaren artisau.
Nire trikuak azkenaldian
barrurantz jirata dirau.
Behin izan nintzen haren hondarrak
harrotzen ez baldin banau
laster mareak irentsiko du
haragizko uharte hau.

Ahul ta txiki sentitzen banaiz
arnastea gehiegi da
ta mantapetik mundua ez da
itzultzen bere neurrira.
Haur izutu bat besterik ez naiz
buruari jira ta bira;
munstro guztiak armairu baten
barruan kabitzen dira
eta orduak joaten zaizkit
armairuari begira.

Zein da bidea? Sendabidea.
Sendatzeko nire senda?
Maite ez dudan jokabide bat
ez nuke zertan lausega;
eztarrian dut korapilo bat
(hitz isilduen zerrenda).
Zerbait pentsatu, beste bat esan
(“egia handia zen ba…”)
eta bidean geratzen dena
minbizigai bihurtzen da.

Masailak bero sumatzen ditut:
malko bat bestean klona;
barregarria izan liteke
negarrez dagoen clown-a.
Zorionaren ataria da
benetako zoriona:
gozoki bat da eskutan, baina
ireki gabe dagona.