---
id: tx-2981
izenburua: Kantauriko Hegian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MFcB3oo529M
---

Iraultzari emandako bizitzak,
ohorezko lerroetan idatziak,
herri zapaldu baten historian,
askatasun egarri den herria.
Adorez betetako bihotzak,
tinkotasunez hartutako bidea,
muturreraino jarraituaz
aske izan nahieran desira.

Zanpatzaileen aurrean
altxatutako gizon-emakumeak,
fusilak eskutan hartuz
eta ukabilak gogor estutuz...

Kantauriko hegian
herri bat izan zen
ez zela esango
zin egin zuten.

Hamarkadetako ibilbidea,
batzutan zail, beti ederra,
hainbeste belaunaldi jasoaz
iraganetik egunera.
Kemenez eta indarrez,
maitasunez eta borrokaz,
Euskal Herri aske ta sozialista
bidezidorretan iparrorratz.

Zorroztasunez ta zuhurtziaz
ezaren gudaz baietza sortuaz,
izandako konpromezua
denontzat eredu bihurtuaz

Kantauriko hegian
herri bat izan zen
ez zela esango
zin egin zuten.