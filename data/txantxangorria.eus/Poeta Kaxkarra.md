---
id: tx-2856
izenburua: Poeta Kaxkarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ear6Rz1gXPQ
---

Ni poeta kaxkar bat naiz
eta kolore urdinez
eskribatzen dut gauez,
desio irrixtakorrak
itsuturik doaz ihes
gauaren leize-zulora,
mundua azpikoz gora,
dena jartzen dut aldrebes.

Zure lurralde oparoa
ikusmiratzen dut lehenik
gauaren zirrikutik zelatan
eta haragiz koloreztatzen
zure gorputzaren figura
irudimenaren ixpiluan,
zure sabel borobilaren
maldan behera amilduz.

Zure lurralde zabala
indarrez goldetuz,
denbora joanen erdoiak
akestutako zure desioak
zorrozten ditut
eta zorion-hazia botatzen
etorkizun lainotuaren tartean
oraino esperantza murriztuaren
hildoa luzatuz.

Nahiaren uholdeek
bortizki astintzen naute
ilunaren tintontzian
hustutzen ditudan arte.

Ametsak gordintzen ditut
gau grinatsuaren labean
eta gero
ixiltasun hezetuaren arnasa
ezinaren eskaparatean
gandutzen da doi-doi.

Azkenik,
ixiltasun bustiaren
oihartzun mutua
bilusik gelditzen da
gau ilunaren apalean
eta nik
azpildurak josten dizkiot
loak hartu arte.

Ni poeta kaxkar bat naiz
eta kolore urdinez
eskribatzen dut gauez.
Imanol jaunaren kantu bat, bere "Joan etorrian" diskotik. 1987. urtean ekoiztu zuen, Elkar estudioetan, Jean Phocas jaunaren laguntza teknikoaz.

Imanol, ahotsa ta gitarra
Karlos Gimenez, pianoa
Michel Longaron, bateria
Txema Garces, baxua
Arantza Irazusta, ahotsa
Jean-Louis Hargous, saxofoia
Iñaki Salvador, sintetizadorea
Josetxo Silguero, saxofoia
Javier Silguero, tronpeta
Juan Jose Ocon, tronpeta
Carlos Hipolito, tronboia
Alberto Lizarralde, gitarra
Luis Camino, perkusioak

Imanol Larzabal abeslariak euskal poesia eta musika uztartu zituen sentikortasun handiko kantetan. Bere ibilbide luzean, Imanol kantariak hainbat musika estilo mota jorratu zituen, kantagintza tradizionala, jazz, rock, edota folk musika. Paco Ibañez edota Gwendal artista ezagunekin lan egin zuen 70. hamarkadan. Beraiekin, Frantzian, Belgikan eta Alemania aldean ibili zen kontzertuak eskeintzen.

80. hamarkada nahiko oparoa izan zen Imanolentzat ikuspegi musikaletik. Ia urtero disko bat kaleratu zuen, "Jo ezan" (1981), Etxahun eta Etxahun" (1982), "Iratze okre geldiak" (1983), "Erromantzeak" (1984), "Oroituz" (1985), "Mea kulparik ez" (1986), "Joan-etorrian" (1987), "Muga beroetan" (1989) eta "Amodioaren berri" (1990). Disko guzti hauetan guztietan euskal poeta garaikideen olerki ederrak musikatu zituen, adibidez, Mikel Arregi poetarenak, Koldo Izagirre, Joseba Sarrionandia, Juan Mari Lekuona, edota Xabier Lete jaunaren hitzak.