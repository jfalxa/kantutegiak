---
id: tx-3053
izenburua: Bilbaoko Kaleak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EJw7TSOs_EY
---

Gabriel Aresti ardatz duen "Gabrielen Lekua" ikuskizunerako sortutako kantua. Bideoa - Josu Venero 

Bilbaoko kaleak,
gora eta behera,
errekatik mendira,
batzuk artezak,
gehienak zeiharrak,
lurra estali da
etsez eta gizonez,
gizonak leku batetik bestera
eramateko
makinez,
bizioz eta bekatuz,
karitatez eta kriminaz,
plaza biribilak,
zumardi zabalak,
zuek
niri
gogora
ekartzen didazue
nire asmo zaharra,
behin egin nahi ukan nuen
eta
inoiz egin ez dudana,
Autonomia,
Adiskidetasuna,
Bakea,
Libertatea,
Foruak,
maitatu ditudan gauzak,
Urkijo ministroa,
Gardoki kardinalea,
Mazarredo almirantea,
Egia jenerala,
Arrikibar ekonomista,
higuindu ditudan gizonak,
kaleak,
kale motzak,
kaleak hemendik, hortik,
handik,
edonundik,
Gorbeiara joateko gutizia sortzen zait barrenean,
bertan organizatzeko euskeraren salbazioa,
baina hemen geratzen naiz,
kale arte honetan,
milagro baten zai,
egunero bizarra kentzeari utzteko
naikoa kurajerik
ez baitdut.