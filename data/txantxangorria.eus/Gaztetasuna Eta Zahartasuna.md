---
id: tx-794
izenburua: Gaztetasuna Eta Zahartasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XS9NZWFRtD8
---

Ene izpirituan bazen zenbait pertsu
oriaio den guzia etzerait hustu.
zertaz dutan solasa emazue kasu
esplikatuko baitut ahal bezain justu
Zahar bezain prestu
odola zaut hoztu
bihotza ere laztu
gorputza beraztu
oraino gazte banintz banuke gostu!

Zahar gazte arteko hau da parabola:
zuzen esplikatzia hein bat gogor da
gaztea ez daiteke adin batez molda,
gorputza sendo eta azkar du odola;
Zaharra ez da hola;
iragan denbora
etortzen gogora,
eta ezin kontsola...
nekez bihurtzen baita zahar arbola!

Egunak badoazi egunen ondotik
ez dira elgar iduri joaiten hargatik:
atzo iguzkiaren distira zerutik,
egun hobela jalgi itsaso aldetik:
euria ondotik
hasi da gogotik
hedoien barnetik
hortakotz badakit
erituko naizela bustiz geroztik