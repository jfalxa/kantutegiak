---
id: tx-1136
izenburua: Sortirria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KuJEJSAyWc4
---

Dena beltz dena ilun diren egunetan
erre ta erre baizik egiten ez dudanean
hor zaude nere tresna guzien artean
eta nik ezin aurki nola zu jarri martxan.

Nahi nuke ulertu ezazun batzutan
nere gitarrak ezetz erantzuten didanean
nere eskua ezin idatziz denean
ez dagola batere ongi egun hoietan

Nitaz zira nardatu lotsatu aspertu
eta hemen nago uzkur nor behar ote otoiztu
zoaz nahi baduzu zoaz ta barkatu
baina ez zaitez nigandik betiko urrundu.

Ahantzi dut aspaldi txoriaren kanta
bertzeekin ibiltzeak ba dakarki bere tranpa
erruduna naizela ezin dut nik uka
mendeka zaite baina noizbait ezazu barka