---
id: tx-2819
izenburua: Summer Of `74
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uziS0guETW0
---

Gogoratzen al duzu

udara ero hura?

hondarra gure oinetan

ekhia begietan

Gogoratzen dituzu

amaigabeko egunak?

munduaren jabeak

gauaren erreginak

ta zu nere egunsentia!

Gogoratzen al duzu

udara ero hura?

biluzik bainatu ginen

ilargi azpian

Gogoratzen dituzu

amaigabeko festak

bakarrik utzitako

lagunen etxeetan?

ez dut nahi hau amaitzea!

(Leloa)

Ooh! emaidazu eskua

ooh! udara hartan bezala

ooh! udazkena etortzean

ooh! biluzik aurki gaitzala

Gogoratzen al duzu

ezagutu ginenean?

begirada bakarra

gezi bat bularrean

berrogei urte ondoren

burua altzatzean

berriro aurkitzen zaitut

gau hartan bezala

Urteak zure alboan

segunduak dira, segunduak dira…

Errepide eder honen amaieran

orain gure negua iritsita

nahiz ta zaila den agur esatea

eramango zaitut nire azalean

betirako zaitut tatuatua

(Leloa)

Gogoratzen al duzu

ezagutu ginenean?

begirada bakarra

gezi bat bularrean

berrogei urte ondoren

burua altzatzean

berriro aurkitzen zaitut

gau hartan bezala