---
id: tx-158
izenburua: Gau Eternal Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sw0k5oXqslE
---

Eskerrik asko “Suak pizten direnean” liburua idatzi eta helarazi dizkiguten esku, buru eta bihotzei. 

Bideoa: Maria Muriedas eta Ximon Agirre 
Grabazioa eta nahasketa: TOC
Masterra: Urtzi Iza

Esker mila Paula Huarteri loratxoagatik

GAU ETERNAL BAT (M.S.)

Minutu baten
Segunduek barriz
Kontata

Ulertu nahi dan
Sinistu ezin dan
Gau eternala

Argie joan arren
Ez da ezer barri
Bat barik
Ez gauz bi (x2)

Zeinen gogorra
Egie zure bile
Heltzea

Ze tristea dan
Ikusi gure ez dan
Dana ulertzea

Argie joan arren
Ez da ezer barri
Bat barik
Ez gauz bi (x2)

Hotza euki arren
Egon zaitez trankil
Hor ondoan nau ni

Ilun egon arren
Euki eizu argi
Hor nauela beti