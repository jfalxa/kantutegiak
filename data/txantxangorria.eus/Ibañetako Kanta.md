---
id: tx-1465
izenburua: Ibañetako Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y1D_dzKMn20
---

Euskal Herrian gu girade nagusi,
baina, besteek düite bazterrak nahasi,
behar üken dugu borrokatzen hasi,
etsaien zanpatzia etzaikü itsusi!!
Ibañetako naban garraitziak
bozkaria gitzala euskaldun güziak,
früitü ekarri dü haien bühurtziak,
holan ziren salbatü zazpi probintziak!!
Jei hunen geia zen aspaldikua,
baina euskaldunentzat, berdin oraikua.
Hau ere günian ohiüskatzekua,
bizi nahian dela gure Zuberoa!!