---
id: tx-2314
izenburua: Lehen Pausoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xocxE76cOrs
---

Grabado, mezclado y masterizado en el estudio Sound of Sirens de Iruña a manos de Julen Urzaiz en Noviembre del 2017.

Colaboración: Simon (ex-The Guilty Brigade) - violín, Ana - voz  


LETRA:

Eguzkia irten arren iluntasuna du barrenean
Whiskey kiratsak soilik besarkatzen duenean
Ubeldurek larrosak ordezkatzean
Errespetu faltak errutina bihurtu bitartean

Iraganeko egunen oroitzapena du droga
Esperantzazko hautsez marraztuz geroa
Ahazturik jada bere bizitzaren leloa
Hotz denean bere buruari eman beharraz beroa

ETENGABEKO NEGUAK IZOZTU DIZKIO MALKOAK
NAZKATU ZEN BIZITZEAZ GEZUR HOSTOEN BASOAN
BERAK IDAZTEN DITU ORAIN GEROAREN LERROAK
EZ DU ONARTUKO TRISTURA BERE ALBOAN

Indarrez bustiz bere itxaropenen sua
Umilazioz sasituz auto-estima soroa
Emakume askea den pentsamendu eroa
Biharamuna aldatzeko behar duen lehen pausoa

BIDEZIDORRIK EZ EGON ARREN DESERTU ZABALEAN
PAUSOZ PAUSO UTZI DITU KATEAK ATZEAN
BURUA TENTE AURRERA EZ DU INOREN BELDURRIK
EZ DA JAIO BERRIZ UKITUKO DUENIK