---
id: tx-878
izenburua: Elizatik Konbenturaino
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-Ov_qRZ2WDM
---

Elizatikan konbenturaino 
egin digute galtzara, 
elizatikan konbenturaino 
egin digute galtzara. 
 
[Azkeneko eguna da eta, 
azkeneko eguna da eta, 
azkeneko eguna da eta, 
goazen guztiok dantzara]. BIR 
 
[Horrela bizi bagina beti, 
horrela bizi bagina beti, 
horrela bizi bagina beti, 
ondo ginake, Katalin.] BIR