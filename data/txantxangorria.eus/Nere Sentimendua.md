---
id: tx-1706
izenburua: Nere Sentimendua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m_XbUDSZcTc
---

Nere sentimendua nahi det deklaratu:
Ameriketan nago hau ezin ukatu; (Bis)
hemen eginagatik ondo gobernatu,
horkuaz egiten naiz asko akordatu.

Honuntz etorri nintzan utzirikan ama,
anai-arrebak eta familia dana; (Bis)
egitiagatikan desio nuena,
geroztik triste nago, ai! nere barrena.

Hamazazpi urtetan banuen segira:
neska gazte guztiak neroni begira; (Bis)
lurrik ikutu gabe, gorputzari jira,
orduko arintasunak asentatu dira.

Gorputz ederra nuen, dantzari arina,
bainan ez da izaten betiko egina; (Bis)
orain eginagatik hemen ahalegina,
ez gera libertitzen orduan adina.

Horko bizimoduaz oso aspertuak,
onerako ziraden gure desiuak; (Bis)
orain etxera berriz emanik pausuak
hemen bizi gerade erdi-mudatuak.

Despedida dijua sei bertso hoiekin,
anaia konformatu horrenbesterekin; (Bis)
nere gustua hau da, nahi badezu jakin:
beste horrenbeste jarri, aio Joxe Juakin. (Bis)