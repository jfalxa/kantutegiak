---
id: tx-2076
izenburua: Haizea Dator Ifarraldetik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bqEzlWp3DX4
---

Haizea dator ifarraldetik, 
hego berotik ekaitza,
ez da nabari lanbro artean 
asaba zaharren baratza...
eguneroko lagunak ditut 
beldurra eta zalantza,
ardo txar honek eragiten dit 
noraezeko balantza.

Itzal gaiztoak isurtzen dira 
mendi hegaletan behera
argi apalak badoaz laster 
bat-batean itzaltzera...
izen zehatzak uzten ditugu 
erortzen zulo beltzera,
prestatzen gera etxe hustua 
arrotz guziei saltzera.

Etxea hustuz badoa ere 
noizbait bazuen biztunik,
ez dut onartzen ordez datorren 
maizter aldrebes hiztunik...
nahi ez badugu denon artean 
eraman karga astunik
gurdi okerra zuzen lezakeen 
dotrinarikan ez dut nik.

Agur oihanak, agur itsaso, 
atariko lizardia
imajinetan izkutatzen zait 
hurbileko izadia
odolarekin pozointzen dugu 
herri baten izerdia
irrintzi gorriz auzika dator 
irabazleen zaldia.