---
id: tx-2047
izenburua: Itsasoa Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3DyiFAuf4ns
---

Begiratu atzean,
ortzi mugak sutan,
gure begien zerutan
 
Begiratu maitea,
amildegi ertza,
biziak hila, ametsa.
 
Ez bilatu gauean,
izar hura,
zure begietan piztu da
 
Ta ez galdetu inoiz zer galdu genuen,
negar egin genuenean,
malko haiei esker,
orain itsasoa gara,
orain itsasoa gara.
 
Orain hemen gaudela, bidegurutzean,
utz ditzagun beldurrak atzean,
ez gara izan onenak, beharbada,
baina gure bizitza izan da.
 
Mendeetako kanta,
erditu da,
gure ordua heldu da
 
Ta ez galdetu inoiz zer galdu genuen,
negar egin genuenean,
malkoa haiei esker,
orain itsasoa gara,
orain itsasoa gara.
 
Ta ez galdetu inoiz zer galdu genuen,
gure ametsaren bidean, izan garelako,
oraindik ere bagara, ta beti izango gara.
 
Ta ez galdetu inoiz zer galdu genuen,
negar egin genuenean,
malko haiei ekser,
orain itsasoa gara,
itsasoa gara.