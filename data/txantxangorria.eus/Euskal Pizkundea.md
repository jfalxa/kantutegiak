---
id: tx-1926
izenburua: Euskal Pizkundea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AUEjwT2Ji5c
---

Gau luzea gau latza
Gau izar gabea,
nekez gendun ikusi
haren azkentzea.
Gauerdian hil zaigu
amona gurea,
esku gogortuetan
dauka gurutzea. 

Gure sukalde zarra
Etxe pizgarria
bart lagunarte goxo
gaur bakar gorria
len epela jario
orain hotz larria.
Amonaren batez 
dezu utzi bizia. 

Amonaren alkia
hago sutondoan
haren ipuirik aldek
hik jaso gogoan?
hik ez? Laratzak? Ezta...
Ez uste gozoan 
geunden ta gure poza
betiko da joan. 

Orduan belaunikatu 
nintzan auts gañera.
Hark gordetako sua
zu ere hil ote zera?
Autsa pitinka urratu 
nuan, beldur bera.
Sua bizirik dago
galduak ez gera.
Geroztik ari nauzu
auts zaharrak astintzen
aiton-amon guztien
ipuiak jasotzen
herriaren pitxiak 
magalean biltzen
ez baita hil gure herria
zutitu gaitezen. 

Hil ez da ta ezta hilko
guk ez badegu nahi.
Bizi irrikaz zegoen
gure maitasun zai,
haren susper beharrez
goazen nora nahi.
Pizkor urra ditzagun 
malkar eta zelai. 

Egizute nerekin
aberri bidea.
Bildu dezagun nun nahi
asaben lorea.
Ta gaur danik gorritu
gure sukaldea.
Ni nor naizen? Asmatu.
Euskal pizkundea.