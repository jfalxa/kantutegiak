---
id: tx-3291
izenburua: Tantaka Yotanka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aYAoqKrXI1I
---

APATXE SIOUX KOMANTXE
KIOWA TXEYENNE

Indioak tipietan
gustora ai! ene!

Aoae Oeoe
Aoae Oeoe

Mendian keak seinale
danborrak tun-tun
Arkoa gezia lumak
Tomahawk kirtendun

Aoae Oeoe
Aoae Oeoe

Bi/son/te/a i/ku/si du
Tan/ta/ka Yo/tan/kak
iz/pi/ri/tu dan/tza su/an
mu/gi Si/oux han/kak

Aoae Oeoe
Aoae Oeoe

Zal/di/ak ta/ka/tan tan/tan
e/hi/za bu/ka/tu/a
a/pa/txe e/gin bi/ri/bi/le/an
pi/pa sa/kra/tu/a

Aoae Oeoe
Aoae Oeoe