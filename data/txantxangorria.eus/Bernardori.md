---
id: tx-1091
izenburua: Bernardori
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YgaR919ze2s
---

Bernardo zen bere izena
Euskadi bere ama.
Langileak anaiak
iraultza laguna
Borrokalari osoa
burkide sakona
Etxalar mendian,
arnasa kendua.
Euskadirentzat
zen bere bizia
hogeitairu urtekin
goiztar bait galdua.
Bere asmoak ziren
Euskadi askatzea,
guk iritsi beharko
hori betetzea.
Txirrita
zure bizia guretzat
exenplu bat
izan dedila.
Beti, beti gure artean
egongo zera.