---
id: tx-2285
izenburua: Abha
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vvU4bGme86Q
---

Goizean goiz errainutan 
Itsasora irten nahi, baina 
itsasontzirik ezean.

Aukera nire aurrean eta
ihes egitean, egitean. 

Mila ametsen leizezuloan 
Ene burua preso, zabarrean. 

Hau bada askatasuna 
Eraman nazazu barrura. 
Amaiera baten hasiera 
berriz ere hasteko. 
Eguzkirik ikusten ez duenaren 
lainopea argitzeko ilargirik ez denean. 

Gauean, argitan, itsasoan. 

Baina gauean berriz ere
etorriko naiz, bila. 
Itsasontzia hartuta 
desiren irlara.

Amaiera baten hasiera 
berriz ere hasteko. 
Eguzkirik ikusten ez duenaren 
lainopea argitzeko ilargirik ez denean.