---
id: tx-1603
izenburua: Trikitilariak Gera Kepa Junkera & Sorginak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YgHDdQOgyNA
---

Trikitilariak gera
Euskal Herrian sortuak
batez ere Bizkai ta
Gipuzkoako fruituak.
Nahiz urte batzutan egon
pixka bat atzeratuak,
berriro zorionian
gaude indarra hartuak.

Mendi altuen edurre
Mendi altuen edurre
bota sutara egurre
alaba galnten amak
mutil zantarren beldurre.
mutil zantarren beldurre.
Mendi altuen edurre

Soinu txiki eta pandero
eskutan dabilzkigunak;
hoien bidez gozatu nahi
inguruan dauzkagunak
inoiz aiton ta amonak
dantzan jartzen dakigunak,
gu gera tristura danak
uxatu nahi ditugunak.

Aitek gureak amari
aitek gureak amari
gona gorrie ekarri
luze dala labur dala
garri garritik ebagi
garri garritik ebagi
aitek gureak amari