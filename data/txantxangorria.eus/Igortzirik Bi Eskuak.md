---
id: tx-1418
izenburua: Igortzirik Bi Eskuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gtWNKAhImNc
---

Igurtzirik bi eskuak
igurtzirik belarriak
bero, bero, bero
jarri naiz
jolastera noa orain.

Goiko mendian elurra
zuritzen ari da lurra
zuri, zuria gailurra
gorri, gorria sudurra.