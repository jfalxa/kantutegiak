---
id: tx-312
izenburua: Lorearen Arantzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s6Nplbuk81c
---

Kontratazioak/ Contrataciones: info@taupaka.eus

Poztasuna konpartitua izanik gehiago gozatzen da. Norberak bere burua ezagutzea ezinbestekoa da baina bakardadeak iluntasuna ekartzen ez duen bitartean. "Lorearen Arantzak" du izena abesti berriak, alde politenak baloratuak direlako alde txarrak ere dituenean. Oraingoan, estilo desberdin batera jo dugu erritmo eta ahotsa, eraikuntza handi eta grisekin osatu dugu bideokliparen estetika, "ElezettaProd" filmakerrak gionizatua eta grabatua.


Ahotsa/ Letra/Produkzio/ Nahasketa: Ziztada & Rlantz
Master: Antxon Sagardui

- Lorearen Arantzak (Letra) -
Soilik gu, lasai gaua
Begietan paisai laua
Jolastu, bion pauta
Ezin da egin, gure aurka

Esnatu ta alboan zaitut jada
Entzuten zure bihotzen taupada 
Mila pentsamendu ezin dut laga
Hegoa izango da gure iparra

Hileak pasa ditugu
Lehenengo egunak izango balira
Ikusten zaitut ta urrun
Distantzi honetan gerturaezina
Burua dabil biraka, pentsamendu txarrak gaizki esaka
Momentu txarretan baita, beti ahalko da onena besarka

Bilatu, zure ardatza
Eginzazu aurrerantza
Sobran dut nik balantza
mezutan ez zait falta

Zoramena dugu gure ardatza
Behetik hasita joan da gorantza
Nola aurkitu zertan dugun antza
Ez badaikutu lorean arantza

Musikak elkartu gaitu
Mikrofonotan orduak bizita
Kontzertuetan alaitu
Bapatean egindako bisitak
Elkar izan gara zaindu elkartu gaitu bizitzak,
Marka berria omen da, lagunartean ez bada ezin da.

Lokartu ohi gara biharkoaren menpe 
Ez gara gure bizitzaren jabe
Bakardadez inguratuta gaude
Barnetik hozten berotutako labe

Bakarrik egon denak, ezagutzen du bere burua
Negar egin duenak, baloratuko du poztasuna
Famatu izan nahian, noizbait amaituko da saldua
Pobreziaren baitan, nork ez du nahi bizitzan luxua

Utzi nahi ez duzun hori
Euki itzazu bi begi
Gozatu eguna beti
Ausartu egiten berri

Zure ametsak egi bihurtzen dira
Horretan sinisten bada egin ezazu tira

Bizkarra eman beharrean aurpegira begiratu
Inork ez gaitu salbatzen, bízitza egin behar dugu, gidatu
Jendearen gezurrak entzun beharrean albokoak maitatu
Bide luzea eginda jada, etorkizuna zure eskutan daukazu

Zure eskutan daukazu, ezin zaitu inork aldatu
Etzaitez nahastu, maite duzun dena hartu ta borrokaezazu
Mundua aldatu, legearen kontra behar gara batu,
Bide luzea eginda, etorkizuna zure eskutan daukazu