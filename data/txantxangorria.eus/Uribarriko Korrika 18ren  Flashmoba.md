---
id: tx-1905
izenburua: Uribarriko Korrika 18ren  Flashmoba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cRM0qAoUsYA
---

Bagoaz Euskal Herritik mundura
eskua emanaz elkarri
bideak direla gure lotura
urratsak direla gure oinarri
bagoaz korrika
elkar entzunaz elkar ulertzera
bagoaz korrika
euskara emanda elkar bizitzera.

Euskara da euskaldunon ezaugarri
guztion lur eta guztion zeru
horregatik maite dugu izugarri
eta gehiagok maita dezakegu
bagoaz korrika
ikastera eta erakustera
bagoaz korrika.

BAGOAZ KORRIKA
EGIZU HITZ
HERRI ZAHAR HERRI BERRI
BAGOAZ KORRIKA
EMAN EUSKARA ELKARRI

HARTU ETA EMAN EUSKARA ELKARRI

Maule, Donibane Garazi, Baiona
Donostia, Bilbo, Gasteiz, Iruñea
herri guztietan euskara, bai ona!
hizkuntza baita gure bilgunea
bagoaz korrika
eta elkarri euskara ematera
bagoaz korrika!

BAGOAZ KORRIKA
EGIZU HITZ
HERRI ZAHAR HERRI BERRI
BAGOAZ KORRIKA
EMAN EUSKARA ELKARRI

HARTU ETA EMAN EUSKARA ELKARRI

Maule, Donibane Garazi, Baiona
Donostia, Bilbo, Gasteiz, Iruñea
herri guztietan euskara, bai ona!
hizkuntza baita gure bilgunea
bagoaz korrika
eta elkarri euskara ematera
bagoaz korrika!

BAGOAZ KORRIKA
EGIZU HITZ
HERRI ZAHAR HERRI BERRI
BAGOAZ KORRIKA
EMAN EUSKARA ELKARRI

HARTU ETA EMAN EUSKARA ELKARRI