---
id: tx-1477
izenburua: Kosta Ala Kosta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ORebqbSUPZ8
---

Pozik jaiki naiz, iritsi zait nomina
hau da gure Jaungoikoa,
alderdian emandako urteak ongi sartzen dira
hau gure lege zaharra!

NATURGAS, PETRONOR, IBERDROLA eta REPSOL
EUSKALTEL, KUTXABANK, txiringitoak lepo.

Zuzendari postu bat nahi baduzu,
bide azkarrena hemen daukazu!

Kosta ala kosta
etengabe puztu ditut nire kontu korronteak
egizu topa!
Gora Aginagako angulak!
Dirua nora
Legeak hara, interesa
baita aberri bakarra
egizu topa!
Nire aberri bakarra!

Ortu zaharretan, uzta beti da ona,
jasotzeko erein behar da,
pribatizatu ahalduzun guztia,
murriztu prestakuntzak,
lotu hedabideak.

Esleipen publikoak laguntxoen neurrira,
ate birakariak, lege ez idatziak

Zuzendari poztu bat nahi baduzu,
bide azkarrena hemen daukazu.