---
id: tx-1508
izenburua: Isidrorena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lDOEfxREDws
---

Beñardo du izena baina luzaroan
Isidro deitzen zuten denek auzoan,
entzun ezazue bere istorioa,
patuak bizitza eskutik daroa.

Mila bederatzirehun laurogei ta bian
polizia autoan preso nindoan,
SanIsidro eguna zen, ondo daukat gogoan,
jauzi egin nuen autotik martxan.

Korrika ihesi Errenterian barna,
orduan jakin banu gero helduko zena!
Argi nuen behintzat ez nuela deus esan,
inoren izenik ez niela eman.

Lapurdi aldean hiru urte iheslari,
beste hiru Frantzian barrena oso larri,
arrazorik banuen ibiltzeko negarrez
izenak eta maiteak laga halabeharrez.

Herbestera bidean bizi behar dena!
Suitza, Errusia, Kanada, Habana,
Nikaraguan, nintzen geratu azkenean
gobernua aldatzean berriro ere Kuban.

Neure atarian eguzki lorerik ez,
zein da zure izena? Isidro Duran Baez,
hogeitabi urte alde egin nuenean,
beste hogeitamabi urte desterruan.

Hara, bihotzeko nire Zubieta,
gazte denboretako Orereta,
Herrira itzultzea bainuen gogoan,
orain batzuetan ez hemen, ez han.

Kantu hau jarri dut, Isidro zuretzat,
eta lagunen lagun diren haientzat.
Jende onak, horra nire azken hitzak:
nork du dohainik ematen bizitza?