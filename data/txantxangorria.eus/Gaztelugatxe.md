---
id: tx-2149
izenburua: Gaztelugatxe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XF6jyqJH1AQ
---

Seirehun urte inguru
Kosta aldean zen gertatu,
Itsasgizon zahar batek
Behintzat hala zidan kontatu,
Enbat itzelak Gaztelugatxen
Untzi eder bat zuen hondatzen,
Euskaldunontzat barruan
Altxor haundiak ei zekartzan.
Igaz udako goiz batez
Eguzkiaren lehen printzez
Jo nuen itsas barrena
Bihotza dardara eginez,
Murgildu nintzen ur gardenetan,
Arakatu ongi hare-haitzetan,
Han zegoen tinkaturik
Goroldioak estalirik.
Indar guztioz bultzatzen
Altxor kutxa zabaldu nuen,
Atzera egin ninduen
Uhin haundia altxatu zen,
Inor ez zapaltzeko gogoa,
Zapalduak ez izatekoa,
Hau da gure urguilua
Altxor guztien iturria.
Eta hau hola ez bazan
Sar nazatela kalabazan,
Ipui txit barregarriak
Kontatu nizkizuen plazan.