---
id: tx-3254
izenburua: Gau Erdian Soinua -Zurriola Ikastola-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VsY3zRn-wko
---

Dena ilunpean dago, gabean, gabean
elurtian baltzo, baltzo.                               bis
Zerutik lurrera, gabon, gabon.

Hiru errege artzaitxo pilo bat
Belengo poz zurian
aita San Jose, ama Maria
eta haur jaio berria.

Gora Jainkoa zeru zabalean
ta pakea lurrean
zuek eta gu gaurtik aurrera
ondo maitatzen ba gera.

Zazpi aingerutxo datoz, etxera, etxera
esnatu zaitezte danok                               bis
gau erdian gera, gabon, gabon.