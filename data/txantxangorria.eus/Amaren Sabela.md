---
id: tx-351
izenburua: Amaren Sabela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MSgZCqE_0bU
---

Amaren Sabela" (Jon Bergaretxe/Joxean Artze) 1978.


Amaren sabela utzi bezain azkar
gorapilo bat egin zidaten
zilborrean, 
ez nendin ahaz
lagatako lehen presondegiaz.
Nere iritzia jakin gabe
kristautu ninduzuten.
Neri galde egin gabe
eskoletan erial jakintxu egin,
gero makina baten morrontzari 
lotzeko...