---
id: tx-1568
izenburua: Limosnatxo Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r5qeYxeEjhs
---

Mutil koxkor bat itsu aurreko
zuela aldamenian
gizon burusoil bizar zuri bat
bi makuluren gainean,
kale ertzian ikusi nuen
inora ezin joanian
ta bera nor zan jakin nahian
iInguratu nintzanian...
limosnatxo bat eskatu zidan
Jainkuaren izenian.

Zerbait emanaz galdetu nion
al zan modu onenian,
jaiotzatikan al zeukan ala
gaitzak hartua mendian;
erantzun zidan: ez semia, ez,
nik sasoia nuenian,
ez nuen uste iritxitzerik
honetara azkenian...
gaur limosna bat eskatutzen det
Jainkuaren izenian.

Eta segitu zuen esanaz:
lehengo gerrate denian,
nih aurren xamar ibiltzen nintzen
beti edo gehienian.
nekatu gabe aisa igoaz,
aldapik luzenian,
etzan burura asko etortzen
gu ala genbiltzanian...
gero limosna eskatutzerik
Jainkuaren izenian.

Odola bero eta burua
harrua dagoenian
edozein moduz sartutzen da bat
halako itsumenian:
atsekabe ta negar saminak
badatoz ondorenian,
nola ere bai batek bizia
utzitzen ez duenian
limosnatxo bat eskatu behar
Jainkuaren izenian.

Inoiz pentsatzen nere artian
ni jarritzen naizenian,
gaur ere asko dirala joango
lirakenak zuzenian.
ez luke inor sinistatuko
zer pena ematen didan.
ai! horla nintzan ni ere sano
ta gazte nengoenian...
gaur limosna bat eskatutzen det
Jainkuaren izenian.

Ai! txorakeri asko egiten
da zentzurik ez danian;
sinista zazu ur hau pasia
daukanaren esanian:
hobiago da sasoin dan arte
saiatutzia lanian.
negar eginaz ibili gabe
gero denbora joanian...
limosnatxo bat bildu ezinik
Jainkuaren izenian.