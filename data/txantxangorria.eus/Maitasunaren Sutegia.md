---
id: tx-1684
izenburua: Maitasunaren Sutegia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sZkGvywmacs
---

Goizuetatik behera
gindoazenerako
maitasunaren sutegiko
sutondoan
bildurik neduzkan
begirada oharkabeen ezpalak,
keinu kilikatzaileren batek
su eman zezaien.

Lainoek beren langa
ideki ordurako,
haize euritsua,
kolpatzaile itsua,
gure gorputz nagituak
astintzen hasia zen,
ikutu bakoitzean,
loxintza bakoitzean,
gure haragi geldoa
freskotasunez kilikatuz.

Goizuetatik behera
Arano parean
gindoazenerako,
dena prest zegoen,
baina…

Baina…
keinu jostalari bat
falta zen,
lotsak gorritutako
begirada txingartu bat,
arruntkeria zitatzaile bat,
ausartkeria korriente bat,
sentimendu ezpalduek
su har zezaten.

Halako batean,
lotsa zarrakatu,
hitzak erantzi
eta bilutsik
utzi nituen
zigarroaren ketik
zintzilik,
herabe ez zitezen,
eta suari
haize emanez
bertso zaharra
kantatu nuen:
"Txingi! Txanga! Sutegiko
mailuaren hotsa,
ola-zaharreko neskatxak
ezkontzeko poza".