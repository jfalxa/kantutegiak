---
id: tx-1586
izenburua: Euri Tanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vyVnMLLzUiQ
---

Euri tanta batek apaindu zuen
uhinez putzu bat,
lausotu zituen pixkanaka
bertako errainuak.

Nor nintzen ezin nuen bereizi
argi bizien dantzan;
egiaren bila nenbilen ni
urezko ispiluan.

Denborak ahitutako ideiak utzita
nortasun berri baten bila.
Izan nahi nuke ura bezain malgu gaitzespenetan,
loria banatu hitzetan.

Aurkitu nuen bide berrian
itsu-itsu korrika,
ohartu gabe noranzkoa aurrez
hautatu behar dela.

Denborak ahitutako ideiak utzita
nortasun berri baten bila.
Izan nahi nuke ura bezain malgu gaitzespenetan,
loria banatu hitzetan.