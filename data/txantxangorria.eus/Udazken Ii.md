---
id: tx-426
izenburua: Udazken Ii
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FGgf6FJsJww
---

Iratze etzanen musika,
hego haizetan korrika,
horbelak dantzan eskribitzen du
udazkenaren kronika,
iratze etzanen musika.

laino gorrixka luzeak,
gau-atarian goseak,
ilunabarrez mila moretan
pintatzen ditu haizeak,
laino gorrixka luzeak.

Uzta biltzeko tenorez,
alor luzeak laborez.
negu hotzaren atalburua
akaberan kolorez,
uzta biltzeko tenorez.

Ilunabarren tristura,
asmo lizunen pintura.
Itxas  gainean dardaraz dago
argiaren partitura,
ilunabarren tristura.

Sasi gainean loxuxta,
baxaran eta masusta.
Haize epeletan kulunkatzen da
arantza tarteko uzta,
sasi gainean loxuxta.