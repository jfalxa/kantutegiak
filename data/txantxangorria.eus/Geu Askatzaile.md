---
id: tx-1162
izenburua: Geu Askatzaile
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ycYBecKKI8k
---

Baina gu inork ez gaitu
inoiz askatuko,
Zer nahi dugu guk zinez
gizon izan edola
makurtuta bizi.

Egin, egin lana
hori da bidea,
ez itxoin inoren
laguntzarik.
Inorena hutsa da
hauxe dugu egia:
jo te ke mutila aurrera.

Beti gara kexaz
gure zoritxarrez,
inork salba gaitzan
itxaroan,
noiz etorriko ote
urrungo izarra
salba gaitzan indarra?

Bidez jausi ohi da
gogoz alper dana
itzar anai gazte
hire garra,
hauxe baiduk bide
benetan bakarra:
askatzaile geu gara.