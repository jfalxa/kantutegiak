---
id: tx-1547
izenburua: Alai Elkarrekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9ZbGT5ruZ9M
---

Musika: Xabier Zabala

Abesti hau zurekin nahi nuke nik dantzatu,
muxu muxu zure muxu gorriak laztandu,
belarrira hitz goxoak xuxurlatu,
bion taupadak elkartu.

Zurekin bai salto eginez parapentean,
euripean besoak zabal gira-biran,
mendi gailurrean iluntze oskarbian,
izarrei izena jarriz banan banan.

Biok dantzan, biok hegan,
elkarrekin, ni zurekin zu nirekin,
biok alai laralalai,
elkarrekin, ni zurekin zu nirekin.

Zu gogoan, dantza ta dantza ari naiz alai,
zu ta biok bat eginik itsaso eta ibai,
ur lasterretan jolasean bi arrai,
igerian etxetan blai.

Ikusezin hego haize nahi nuke bihurtu,
putz eginez ziztu batean zugana hurbildu,
nire berotasunaz zu inguratu,
zu ere nirekin haize bilakatu.

Biok dantzan, biok hegan,
elkarrekin, ni zurekin zu nirekin,
biok alai laralalai,
elkarrekin, ni zurekin zu nirekin.( X3)