---
id: tx-2624
izenburua: Ze Krisi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/12c1e0fL7UY
---

Sarritan badaezpada ere
aurrez negarrez hasi
Krisian gaude baina, ze krisi?
Pa/ga ex/tra ken/du di/zu/te, ba/ina
Realak irabazi
A/gi/rre/txe/ren go/la, ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si? Ze kri/si?
A/gi/rre/txe/ren go/la, ze kri/si?

A/H/T/ak au/rre/ra da/rrai
ez/tan/da ta lu/i/zi
Hor/ta/ra/ko ba/du/te, ze kri/si?
ta eus/kal he/rri/tar ba/koi/tze/ko
i/a bos/te/hun po/li/zi
la/na so/be/ran da/go, ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si? Ze kri/si?
e/gu/rra de/bal/de, ze kri/si?

Ber/tso E/gu/ne/ra/ko e/re
sa/rre/ra bi/xi-/bi/xi
he/me/zor/tzi eu/ra/zo, ze kri/si?
ta a/fa/ri/a u/mil-/u/mil
bost ku/bi/er/to ta gu/zi
Be/ra/sa/te/gi/ne/an, ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si? Ze kri/si?
he/me/zor/tzi eu/ra/zo, ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si? Ze kri/si?
ta a/fa/ri/kin be/rro/gei/ta bost, ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si? Ze kri/si?
Ze kri/si?
Kri/si/an gau/de ba/ina,
ze kri/si?