---
id: tx-3117
izenburua: Akerraren Larrea -Raperos De Emaus-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l1-5yzegkQo
---

Ikusten duzuen bezala eguraldi txarra dago
Akerraren larrean
Gure kontra dago
hodei beltzak eguzkia zapaldu nahian dabiltza
Akerraren larrean nabil ni
Zugarramurdi, Nafarroako kobazulo baten barnean
Hik al dakik?
Zergatik betidanik 
erakarri nauen herririk
Ederrena, politena
misteriotsuena
                        sortua da
Ordua da
guazen ba
larrean sartzeko
momentua heldu da
 
Kondairak dioen bezala
sorginak leizeen barnean 
biltzen omen ziren 
deagruarekin
Satanas, Luzbel, Belzebu
Akerra itxurakoa zen
erdi gizon
erdi pizti
Ezin sinetsi 
Baga, biga, higa,
laga, boga, sega,
Zai, zoi, bele,
harma, tiro, pun!
 
Ume kristauak
jaten ditugu
OPUSeko jendea usaitzen dugu
kilometrotara
Zuen pekatuak ordaintzeko 
ordua heldu da
Lasai ez dizugu minik egingo
ez duzue ezer sentituko.
Zure arima salduko dugu,
txanpon baten truke.
Hireak egin du
Xirrixti-mirrixti,gerrena,plat
Olio-zopa,kikili-salda,
Urrup edan edo klip
Urrup edan edo klip
 
Ikimilikiliklip!
Ooohhh!
Akerraren larrean zara zu
Nola Belzebu
bai badakigu gu
Sorginek Akerra Belzebu
goazen akelarrera
Gure gorputzaz gozatzera
Musikaz blai
ilargiak lagunduko digu
nonhai 
baai baai
Kateak apurtzera bagoaz orain
Ez sinetsi, ez sinetsi
Herriaren indarra
irakiten dabil
nola geldituko gaituzte galdetzen badut
nik isiltasuna erakarri
baino ez dut egingo
eta tinko mantenduko diogu
 
Kobazulo gaztetxeetan elkartzen gara, 
akelarreak, asnabladak
dira, ziren eta izango dira
Gazteak hazten garen bitartean
Torturak, hilketak, atxiloketak
Orain bezala ez nabil txantxetan
Egoera ez da ezer aldatu
beti preso gaudela
ez al duk nabaritu?
 
Autogestioaren alde
inkizizioaren kontra
Herri mugimenduaren alde
Errepresioaren kontra
 
Akerrak adarrak okerrak dituelako
gure borroka ez da geldituko
 
Akerrak adarrak okerrak ditu ta
Gure borroka ez da gelditukooooooooo
 
EZ!!!