---
id: tx-2106
izenburua: Lanikan Ezin Egin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Rv9ntNDCtBg
---

Lotsagabekoa dun kopetan azala
lanikan ezin egin lenago bezala
desapio egin nion beiro dun bestela;
saltatu da neska ta hautsi du kristala.

Ama nuen Joakina, aita Joxe Fermin,
borda berriko alaba ni naiz orain berriz,
ariak amarratzen hartu degu griña
Frantziska maistra degu, emakume fiña.