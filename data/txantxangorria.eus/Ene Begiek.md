---
id: tx-3127
izenburua: Ene Begiek
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RjEGbEIwjCM
---

Sortuko dira besteak

Bilbo - Arriaga antzokia
2012.12.01

Kantariak:
Petti
Eñaut Elorrieta

Abestia:
Ene begiek - Ruper Ordorika

Musikariak:
Xanet Arozena - Gitarra
Oriol Flores - Bateria
Txema Garcés - Baxua
Miren Zeberio - Biolina
Luis Fernandez - Txeloa
Joxan Goikoetxea - Teklatuak

Soinu teknikaria - Joxean Ezeiza

ETB 2012

AM Kultur promotra


Ene begiek ez dute malko isuritzeko gogorik, 
denik eta bizitza bakarra bizi dutelako. Entzun, 
astiro doa denbora, idi gorrien gurdi hori. 
Begira erlojuetako orratzak uneka herdoiltzen. 
zenbat gauza utzi dudan estarta bazterretan 
edo laku urdinen hondoan, bizitza bakarrean, 
eta zenbat aldiz galdu naizen behelainotan 
basoilar izateko ametsetan dabiltzan kurloiekin. 
Ene desirak iada ez dira habia abandonatuak besterik 
ene bihotzean. eguzkia gurpil ahul bat da. 
Ez dezadan gehiago entzun, begiratu, ez pentsatu, 
bizitza hau bakarra delako, gal ez dadin, ibili ez 
eta senditu ere ez, harriak legez izan nahi dut, 
mendea eta segundua gurutzatzen diren lekua, 
lekua besterik ez izan, hurrin deritzan aberrian.