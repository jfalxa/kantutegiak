---
id: tx-2009
izenburua: Gurasoak Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7k2V8W0mo0k
---

Gaurko egoera 
xelebrea da beraz
harritzen naiz guraso 
askoren joeraz
ai, oi, ene
askoren joeraz
kalean gora Euskadi
etxean erderaz

Gurasoei nahi 
nieke adierazi
lehengo lege zaharran
zuen haurrak hazi
ai, oi, ene
zuen haurrak hazi
sehaskan ez zenuten
erderaz ikasi

Frutua etortzen da
hazia ereinda
semeak ez badaki
erruduna zein da
ai, oi, ene
nola ikasi zuek
erderaz eginda

Hizkuntzak asko dira
mundu zabalean
Bakoitzak gorde dezagun
modu apalean
ai, oi, ene
modu apalean
amak erakutsia 
bere magalean

Guk behintzat maite degu
jaio ginanetik
mundu honetan sortu 
ginan egunetik
ai, oi, ene
ginan egunetik
ama euskara gauza
guztien gainetik