---
id: tx-2852
izenburua: Lo Ez Mexicon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Bj9L43g8db0
---

La la laral lara la
(La la la la lara la)
La la laral lara la
(La la la la lara la)

Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!
lagunekin bai.

Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!
lagunekin bai.

Euskal Herriko gazteak!
lo ez, lo ez,
lagunarekin hortan bai,
lo ez.

Sexua askatu
gaztedi alkartu
gaztediak lo ez
lagunekin bai!

Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!
lagunekin bai.

Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!
lagunekin bai.

Mexiko, ooh (Mexiko, ooh)
Mexiko, ooh (Mexiko, ooh)
Me (Me) xi (Xi)
Me-xi-ko
Mexiko (Mexiko)
Mexiko (o o ooo)
Mexiko (Mexiko)

Mexiko.
Behin joan ginan lurralde hartara
eta ez zait damutu oraindaino hantxe gu egona.
Nahiz laburki pasa han denbora

pasatzen dugu biziki ekarriz gogora.
Kitar, kantu, tekila mordo bat
beti festarako presto ziran guztiak.
Txapel handi, bibote luzeak,

inoiz lanerako ote ziren jaioak.
Mexiko, Mexiko, ooh
Mexiko, Mexiko,

Mexiko, ooh

Gure sexua askatu
(La la la la lara la)
gure porrua garbitu
(La la la la lara la)
bakarrikan inoiz lo ez
(La la la la lara la)
lagunarekin hortan bai
(La la la la lara la)
Lagunekin hortan bai (Lo ez)
Lagunekin hortan bai (Lo ez)

Lagunekin hortan bai
(Lo ez) (Lo ez)
Sexua askatu
porrua garbitu

gaztediak lo ez
lagunekin bai!
Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!
lagunekin bai.
Bakarrik lo ez!
Bakarrik lo ez!
Bakarrik lo ez!

Esna lo ez, esna lo ez,
esna lo ez, esna lo ez.
Trala larala lara la
la la la la lara la!