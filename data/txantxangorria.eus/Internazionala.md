---
id: tx-2085
izenburua: Internazionala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TdUhBDMH-pw
---

Zutik lurrean kondenatu zaren langile tristea,
nekez ginen elkarganatu indazu albiristea.
Gertatuak ez du ardura, jende esklabua jeiki,
aldaketak datoz mundura, nor den herriak badaki.

Oro gudura ala! Bihar izan dadin
Internazionala pertsonaren adin.