---
id: tx-2788
izenburua: Ez Zaitut Inoiz Ahaztu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PE4efwkXglc
---

Ez zaitut inoiz ahaztu
gaur gauean zurekin ameztu dut
egun batean esan gabekoak oraindikan
buruan bueltaka ditut

Ez dakit zertaz kexatu
beti errudun sentitu bait naiz
inoiz bidali ez dizkizudan hitzak
hauek dira ez dituzu inoiz entzungo

Ez naiz gai izan
zurekin hitz egiteko
inoiz izan ditudan
kezkak ardierazteko
zurekin emandako
gertakari eta urteak
zuri lotuta naukate
maitasuna baino gehiago

Eta zer esan nezake
jadanik inork ez danienik
gaizki erakustsita gaude bizitzan
artaldearen mesederako

Eta ez dakit zertaz kexatu
beti errudun sentitu bait naiz
inoiz bidali ez dizkizudan hitzak
hauek ziren ez dituzu entzungo