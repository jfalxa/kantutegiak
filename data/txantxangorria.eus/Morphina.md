---
id: tx-2045
izenburua: Morphina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JvW5rnEoyDs
---

Ez dut hitzik, ez dut ikusi nahi
Ta horrek ez du dena gelditzen
Aspaldi ikusitakoak
Errepikatzen zaizkit buruan
Min hori gordetzen saiatzen naiz
Pentsatuz sentituko ez dudala
Indarrez begiak izten ditut
Irudiak ditut barruan sartuak.
Bildu ditut bizkar hezurrean
Sorbaldako gihar guztietan
Bortxatutako zuritasunak
Esaldi luzeak ta hitz hutsak
Minduta dut gorputza,
Ta arima apurtua,
Gaur ez dut korrikarrik egin nahi,
Har dezagun arnasa.
Morphina arren, nire arimarentzat
Berandu bada ere, nire arimarentzat.
Suizidatu zaizkit gogoak
Blokeatu zaizkit irribarrak
Kateaturik ditut negar malkoak
Bekainetara lotuak
Besarkatu nazazu,
Ez dut minik sentitu nahi,
Gizakiak galdu du
Gizatasuna.