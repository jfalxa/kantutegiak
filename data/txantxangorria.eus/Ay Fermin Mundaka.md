---
id: tx-55
izenburua: Ay Fermin Mundaka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DTR8SHM22D8
---

Bideoa: Joseba Batiz eta Gabi

Karibeko irletan agertzen da      
aratuste gauean mamu zahar bat      
Zuriz jantzitxe ta ``oriundo´´ abestuten, sustotxuek emoten zirriak egitean  (bis) 
Ay, ay, ay, ay !  Fermin Mundaka, Aratustetan bizi izan zara 
Piraten mozorroa erunzu gaztetan  baina orain atorrak dozuz nahiau 
Ay, ay, ay, ay !  Fermin Mundaka, Altxorrak nahi badozuz atrapa 
Idxen balkoietara han dekozu eta  
urre koloreko hainbat tostada 
Karibeko irletan agertzen da      
aratuste gauean mamu zahar bat      
``La trigueña-k´´ bihotza eintzon apurtu,  Ta orain bidau lapurtu lamin polit batena (bis) 
Ay, ay, ay, ay !  Fermin Mundaka, andran irlatik bizu eskapa 
Paraisue hau da laminez betea,  
erdu ta abestiekaz ataka 
Ay, ay, ay, ay !  Fermin Mundaka, hamen kitarra da geure ezpata Abenturak nahi bazuz, atorrekin erdu ta aratustetan eizu parranda