---
id: tx-3287
izenburua: Nola Bizi Hala Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ysNSvcxUV3w
---

Üdako egünaldi bero argietan
aideak ekarri indar güzietan
kantü berri zaikü sortü gogoetan
eskentzeko negüko gau lüzetan

Ainerak bezala biltzen da gizona
baten ahaidea denen egarria
algarretatzea zuin miragarria
etxen ondoan piztüko giro hona

Nola bizi hala kanta dio üskaldünak
Maitarzün emanez hola adio lagünak

Aiten aitek ützi guri oparia
idatzian ere zer dote ederra
egün hedatu nahiz züer sü garra
agor ez dadin hitzen ütürria

Nola bizi hala kanta dio üskaldünak
Maitarzün emanez hola adio lagünak