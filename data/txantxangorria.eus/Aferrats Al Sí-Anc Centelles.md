---
id: tx-1359
izenburua: Aferrats Al Sí-Anc Centelles
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_WZWF6mV_0E
---

Video de l'ANC local de Centelles a la Via Ferrada de les Baumes Corcades en suport al Referèndum 1-Oct.


Una vintena de membres del Centre Excursionista de Centelles han protagonitzat un espectacular vídeo a favor del "sí". Amb la cançó "Agafant l'horitzó" s'enfilen per la via ferrada de les Baumes Corcades i van penjar estelades al seu pas. Al final acaben a dalt de tot del Puigsagordi on hi ha un centenar de persones esperant-los amb un "sí" i una estelada gegants.




'Agafant l'horitzó' és un tema que Txarango hem composat pel sí al referèndum de Catalunya, per un sí transformador. La cançó ha comptat amb la col·laboració de Gemma Humet, Aspencat, Cesk Freixas, Les Kol·lontai (Montse Castellà, Sílvia Comes, Meritxell Gené i Ivette Nadal) i Ascensa Furore. 

El tema ha estat inspirat en la tornada final del tema 'Article 1.1' de Cesk Freixas i en els versos d'Ovidi Montllor. Des del món de la música no ens quedem amb els braços plegats. Perquè començar de nou és una oportunitat per reconstruir tots els aspectes de la nostra societat. Perquè aquells que somiem en un altre món possible, sabem que cal canviar-ho tot.

LLETRA:

Tenim futur, tenim memòria.
Foc a les mans per teixir la història.
Portem en elles un llarg camí.
Viure vol dir prendre partit.

No volem fum, no volem dreceres.
Aquí no venim a fer volar banderes.
Comptem amb tu, ara no pots fallar.
Un dia u per tornar a començar.

Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.
Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.

Anem lluny, serem molts empenyent endavant.
Qui sembra rebel·lia, recull la llibertat.
Que no ens guanyi la por. El demà avui és nostre.
Tu i jo agafant l’horitzó.

Ara i aquí és el moment del poble.
No serà nostre si no hi som totes.
És part de tu, també és part de mi.
Viure vol dir prendre partit.

No tenim a les mans els problemes del món.
No hi tenim totes les solucions.
Però venim amb coratge i amb somnis gegants.
I pels problemes del món tenim les nostres mans.

Res per nosaltres; per a totes, tot.
A la por i al racisme, calar-hi foc.
Qui treballa la terra se la mereix.
El poble mana, el govern obeeix.

Nuestras manos seran nuestro capital.
Quien mueva el engranaje debe decidir.
Crear, construir consciencia popular.
Eterna divisa que nos guia; vivir llibres o morir.

Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.
Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.

Anem lluny, serem molts empenyent endavant.
Qui sembra rebel·lia, recull la llibertat.
Que no ens guanyi la por. El demà avui és nostre.
Tu i jo agafant l’horitzó.

Serem llum, serem molts empenyent endavant.
Som futur i alegria seguint el pas dels anys.
Que no ens guanyi la por. El demà avui és nostre.
Tu i jo agafant l’horitzó.

Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.
Gent de mar, de rius i de muntanyes.
Ho tindrem tot i es parlarà de vida.