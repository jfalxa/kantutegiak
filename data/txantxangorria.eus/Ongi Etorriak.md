---
id: tx-1958
izenburua: Ongi Etorriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PepxLEPBhrM
---

Gure bizitzen norabideen, jaun ta jabe izan zarete
Txontxongiloen papera egiten, urte luzez euki gaituzue
 
Ohartu gara zuen jokoaz, zuen sistema zikin honetaz
Orain zeozer mugitzen dabil, laizter gauzak aldatzera doaz
 
 
Ongi etorriak infernura kapitalismoaren lurraldera
 "          "                "          amets galduen paradisura
       (bis)
 
Kontu izan, prest bai gaude
Kontu izan prest bai gaude
                (bis)
 
Ongi etorriak infernura kapitalismoaren lurraldera
 "          "                "          amets galduen paradisura
       (bis)