---
id: tx-2663
izenburua: Ene Maitea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NsKPYjZNKp4
---

Ene maitia,
oi, zoragarria
kantatzen dizut penak hartuta
ez zaitut osorik ikusten
oso urruti zera-ta
nahiz eta zure gorputza
ene ondoan eukita.

Zure begiek
sorginen antza
deraukate preso ene bihotza
baina ene bihotz tristerik
kaiolan sartuta
zure begien ura edaten
aski ez zaiola.

Itsas gainean
gau ilunean
oroimenak ditut ene argia
izarbideetan badoaz
ene nahi eta ametsak
Hego-haizeak dakarkit
maitasun promesa.