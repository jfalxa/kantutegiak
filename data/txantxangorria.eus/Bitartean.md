---
id: tx-776
izenburua: Bitartean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SzamZdus0GY
---

Mundua galdu dugu, ez gaitezen galdu gu
Itsaso izan gintezke, tanta bakarrean
Ta dena bitartean sutan urrutian.

Balizkoari begira usteldu da oraina
Ahaleraren kate estuak mendean du ahalmena
Erlojuari begira geunden bitartean
Joan zaigu geroa

DENA DATOR, DENA DOA ABAILA BIZIAN, OHARKABEAN,
BAINA NORANTZA? ZERK GIDATUTA?
LANBRO ITXI HAU UXATZEAN ESAN GURE ALDE ZER GERATUKO DA?
KANTU ZAHAR HURA TA ZURE IRRIA.

Eskuak bete bitartean hustu zaigu bihotza
Abiada bizian bizi gabe ari gara
Biharra ametsez jantziz biluztu dugu oraina ie ie ie 

Mendeen mende, orratzei so, segundoak doaz ihesi
Mendeen mende, orratzei so

DENA DATOR, DENA DOA,…

Eutsi nire alboan. Ez zaitzatela eraman.
Esan gezurra atzera. Salbatuko garela esan.

DENA DATOR, DENA DOA,…

Tik-tak, tik-tak. Esan gezurra atzera
Tik-tak, tik-tak,…salbatuko garela.