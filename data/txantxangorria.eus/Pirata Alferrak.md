---
id: tx-1548
izenburua: Pirata Alferrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-Bgy56D9YNA
---

Musika moldaera: XABIER ZABALA

Piraton bizitza hoberena da!
Eskolarik gabe!
Lanik gabe!
Ron botilarekin!

Fu ha, fu ha!
Bat, bi, arnasa!
Fu ha, fu ha!
Hiru, lau, biriketara!
Fu ha, fu ha!
Kresala badoa, badoa, badator,
gora, behera, eskuin, ezker,
ezker, eskuin, behera gora,
badoa, badator, itsas ondotik!

Piraton bizitza hoberena da!
Eskolarik gabe!
Lanik gabe!
Dutxatu gabe!
Araurik gabe!
Ron botilarekin!

Fu ha, fu ha!
Bat, bi, arnasa!
Fu ha, fu ha!
Hiru, lau, biriketara!
Fu ha, fu ha!
Kresala badoa, badoa, badator,
gora, behera, eskuin, ezker,
ezker, eskuin, behera gora,
badoa, badator, itsas ondotik!

Piraton bizitza hoberena da!
Eskolarik gabe!
Lanik gabe!
Dutxatu gabe!
Araurik gabe!
Gurasorik gabe!
Beti parrandan!
Ron botilarekin!