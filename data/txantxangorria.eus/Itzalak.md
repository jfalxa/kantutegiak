---
id: tx-120
izenburua: Itzalak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Tp-YSfdiNvY
---

www.kokein.com

Zuzendaria / Director: Hannot Mintegia
Argazki Zuzendaria / DoP: Oier Plaza
Kamara Laguntzailea 1/ Camera Assistant 1: Kerman Goikouria
Kamara Laguntzailea 2/ Camera Assistant 2: Mikel Etxebarria
Make Up eta Estilismoa: Inma Elvira
Muntaia eta Postprodukzioa / Editing and Postproduction: Hannot Mintegia
Plato Laguntzailea / Stage Assistant: Jon Kepa Urlezaga

Hitzak / Lyrics

Zerk zoratzen gaituen
Ez digu inork esango
Odola ilun ageri da
Zainetan barrena

Beltza naiz ezin dut ukatu
Beltza baino beltzago 

Zerk beldurtzen gaituen
Guk esango dugu
Pauso bakoitzarekin 
Bidean aurrera goaz

Itzalak ditugu inguruan dantzan
Eta guk behingoz lortu dugu
Haiekin batera bat egitea
Elkarri begira

Zatozte
Hurbildu
Ikusi
Zer garen
Nor garen

Itzalak ditugu inguruan dantzan
Eta guk behingoz lortu dugu
Haiekin batera bat egitea
Elkarri begira...maitatuz