---
id: tx-1600
izenburua: Going Down Kasernarat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3ovYXmtH8Ok
---

Otso guztiak bilduko balira, gaur 
Endarlatsako zubi leherrarazi gainean. 
Ametsak hondoratuz behera, bai 
ibaiaren azpian uluka, beste enbata bat. 
Down, down, down, down, going down, down 
ametsak hondoratuz behera, going down, down. 
Down, down, down, down, going down, down 
ibaiaren azpian uluka, beste enbata bat. 
Otso guztiek baleukate, gaur 
zure ohearen ondoan egoteko parada. 
Ahots zartatu baten kordekin, hey 
sortzeko berriz ere kantu heze bat. 
Ez dut etsi, ez dut etsi ez, 
ametsak ur azpitik gora ekartzen, 
ez dut etsi ez. 
Gora, gora, gora, azpitik gora, bai 
ametsak nire sarraila zulatzen garrasika. 
Down, down, down, going down, down 
zure irain zikin guztiak going down, down.