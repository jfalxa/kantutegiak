---
id: tx-890
izenburua: Nire Bihotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/31tn5vU4xhw
---

Zutitu nintzenean
Itzali nuen
Ametsik gaiztoena
Hitzik hartu gabe
Konturatu nintzen
Ez dakit gorrotatzen
Negua maitatzen
Ez oraindik

Nire bihotzak ez du jaberik
Ez arau ta ez izenik
Ez daki nora doan
Nora doan

Gauetan jolasean
Nahi ta naizena eta izan nahi dudana
Hitzik eman gabe konturatu nintzen
Izanaren giltza izaren itsasoan dela galduta

Nire bihotzak ez du jaberik
Ez arau ta ez izenik
Ez daki nora doan
Nora doan