---
id: tx-1363
izenburua: Zerua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9F1DF1EBDHY
---

Enkore taldearen Zerua abestiaren bideoklipa, E diskoko bigarren single kantua. 

Artista: Enkore (Xabi Hoo, Urtzi Iza, Aitor Gallastegui, Alvaro Olaetxea)


Bat gara zeruertzari begira
bi gorputz bihotz bakarrean
hiru izar uxo gu jagoten
 
lau hodei zuriren artean
 
Bostak dira…
 
Eta gauak jada ez dauka zentzurik
eguzkia portutik agertzen denean
 
Zerua
 
Bat egin dugu sekretuekin
bi musu agur esatean
hiru mezu etxera bidean
lau esan ez genituenak
 
Bostak dira…
 
Eta gauak jada ez dauka zentzurik
eguzkia portutik agertzen denean
iraultzaren koloreko zerua
iragana galduta, geroa eskuetan
 
Zerua
 
Eta gauak jada ez dauka zentzurik
eguzkia portutik agertzen denean
iraultzaren koloreko zerua
iragana galduta, geroa eskuetan
 
Zerua, gorria
zerua, iraultza
 
Eta gauak jada ez dauka zentzurik
eguzkia portutik agertzen denean
iraultzaren koloreko zerua
iragana galduta, geroa eskuetan
 
Zerua, gorria,
zerua, iraultza