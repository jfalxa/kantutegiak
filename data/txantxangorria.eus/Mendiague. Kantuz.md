---
id: tx-2425
izenburua: Mendiague. Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4ZRqcbGvIv4
---

KANTUZ

        Jose Mendiage , XIX. mendea

 

 

Kantuz sortu naiz eta kantuz nahi bizi,

Kantuz igortzen ditut, nik penak ihesi,

Kantuz izan dudanean zerbeit irabazi,

Kantuz gostura ditut guziak iretsi,

Kantuz, ni bezalakoak, hiltzea du merezi.

 

Kantuz pasatu ditut gau eta egunak

Kantuz izan dirade ardura ene lanak,

Kantuz biltzen nintuen aldeko lagunak,

Kantuz eman daitate obra gabe famak,

Kantuz hartuko ahal nau, gure Jeinko-Jaunak.

 

Kantuz eman izan tut, zenbeiten berriak,

Kantuz gustatu izan zait erraitea egiak,

Kantuz eman baditut ainitzi afruntuiak,

Kantuz barka ditzaten ene bekatuiak,

Kantuz egiten ditut nik penitentziak.

 

Kantuz eginez geroz, mundura sortzia,

Kantuz e'in beharko dut ene ustez hiltzia,

Kantuz emaiten badaut Jeinkoak grazia,

Kantuz idokiko daut San Pedrok atia,

Kantuz egin dezadan, zeruan sartzia.

 

Kantuz ehortz nezaten, hiltzen naizenian,

Kantuz ene lagunek harturik airian,

Kantuz ariko dira ni lurrean sartzian,

Kantu frango utziko diotet munduian,

Kantu egin dezaten nitaz oroitzian.