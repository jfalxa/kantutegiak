---
id: tx-979
izenburua: Memoriaren Zeruan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rBxnMKJhSDg
---

Bide bazterretan, gaua lekuko, lurperatu zituztenak,
edozein hormaren kontra akabatutakoan
erdiminez zeuden neskak bahitu, bortxatu, umiliatu
jainkoaren baimenez, jaio ahala umeak ostu.

Errepublikanoak zirelako bakarrik jazartuak
ehunka maistra haiek lanetik kaleratuak,
greban zeuden langileen kontra tiro, gero hilak ahaztu,
tiro egin zutenak saritu eta kondekoratu.

Izar berri bat, izar gorri bat.
Distiratsu dirau memoriaren zeruan.

Eskolan euskaraz galarazi, kalean zigortuak,
abertzale, anarkista, gorria, sozialistak,
denak etsai eta denak susmagarri, presogai, hilgarri.
Denak mundu, gizarte, herri berri baten egarri.

Izar berri bat, izar gorri bat.
Distiratsu dirau memoriaren zeruan.

Galtzaileen saminarekin batera, memoria eraiki.
Izar ahaztuez josiko dugu zerua, egia berreraikiz.