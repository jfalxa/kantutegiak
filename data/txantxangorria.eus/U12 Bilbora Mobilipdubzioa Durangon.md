---
id: tx-1938
izenburua: U12 Bilbora Mobilipdubzioa Durangon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qzr-2jPWbm8
---

Igerian, hegan, oinez...

Pausorik pauso bagoaz.
Lainoa nahinora doa,


Ba/ina gu, ...
Ba/ina gu he/rri/ra go/az

Arrainak saretik ihes,
Txoriak libre dabiltza;
Askatasunaren bila
dabil gutako bakoitza.

Aizu lagun hunat jin zira
Besoak zabalik dituen herrira
Eta ez zira ohartu
Hemen lagun anitz falta ditugu
Baina ulertuko duzu
Otoi egidazu k/su
Entzun ezazu ta e/repikatu
Denek elkarrekin lortuko dugu
Baina ulertuko duzu
Otoi egidazu kasu
Entzun ezazu ta errepikatu
Denek elkarrekin lortuko dugu
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra
HE/RRI/RA!
EUSKAL HERRIRA!
Zea zauz zara zire Herri/a begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire gure distira
Maite duguna bihotzez
Urrundua herri arrotzetan
Gauero amets eginez
Elkartzen gira izarretan
Izar gau/tako zita
Utz dezagun oraintxe bertan
Elkar iku/siko dugu
Gure herriko kaleetan

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra





Argi urteak joan
ta urte argiak datoz
Orain ezin da jarrai
Itsasoa betetzen malkoz
Gaur, herrira oihuka
garaipenaren hitza
Mihi gainean daukagu
Askatasunaren giltza

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra

HE/RRI/RA!   HE/RRI/RA!
Hasi da soka tira

HE/RRI/RA!   HE/RRI/RA!
Bukatzeko tira-bira

HE/RRI/RA!   HE/RRI/RA!
hiru urtetik argira

HE/RRI/RA!   HE/RRI/RA!
kalean izanen gira

Orain ulertu al duzu?
Kantatzen segi ezazu
Mundu guzian mezua zabaldu
Laster herrian izanen ditugu

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra

HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re He/rri/ra be/gi/ra
HE/RRI/RA!
EUS/KAL HE/RRI/RA!
Ze/a za/uz za/ra zi/re gu/re dis/ti/ra

HE/RRI/RA!   EUS/KAL HE/RRI/RA!


I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!

Pau/so/rik pau/so ba/go/az.
La/ino/a na/hi/no/ra do/a,
E/ta ni, ... e/ta ni, ...
he/rri/ra no/a!

Pau/so/rik pau/so ba/go/az.
La/ino/a na/hi/no/ra do/a,
E/ta ni, ... e/ta ni, ...
he/rri/ra no/a!

A/rrai/nak sa/re/tik i/hes,
Txo/ri/ak li/bre da/bil/tza;
As/ka/ta/su/na/ren bi/la
da/bil gu/ta/ko ba/koi/tza.

Ge/rra e/ta zo/ri/txa/rra,
Ez da e/zin/bes/te/ko/a;
As/ka/ta/su/na be/har da,
de/non/tzat be/ne/ta/ko/a.

He/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!

Pau/so/rik pau/so ba/go/az.
La/ino/a na/hi/no/ra do/a,
E/ta ni, ... e/ta ni, ...
he/rri/ra no/a!

Pau/so/rik pau/so ba/go/az.
La/ino/a na/hi/no/ra do/a,
E/ta ni, ... e/ta ni, ...
he/rri/ra no/a!

Hor/me/tan dau/de a/te/ak
e/ta mu/ga da pau/su/a;
Oz/to/po/a ez da i/zan/go
Gu/re/tzat en/ba/ra/zu/a.

Kar/tze/la e/gin na/hi du/te
He/rri/ak i/to/tze/ko/a;
He/rri bat e/gin/go du/gu
Kar/tze/la/rik ga/be/ko/a.
He/rri bat e/gin/go du/gu
Kar/tze/la/rik ga/be/ko/a.

EUS/KAL PRE/SO/AK E/TXE/RA!
EUS/KAL PRE/SO/AK E/TXE/RA!


I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!

I/ge/ri/an, he/gan, oi/nez...
he/rri/ra no/a!
I/ge/ri/an, he/gan, oi/nez...


EUS/KAL PRE/SO/AK E/TXE/RA!
EUS/KAL PRE/SO/AK E/TXE/RA!

EUS/KAL PRE/SO/AK E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
E/TXE/RA!
PRE/SO/AK KA/LE/RA!
AM/NIS/TI/A O/SO/A!