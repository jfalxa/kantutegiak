---
id: tx-3031
izenburua: Musikarien Ametsak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ywOuH2EiEMU
---

Hei! gizon musikari zatoz honera
alai dezagun gu biok gau ilundu hau
hei! gizon musikari zatoz honera
nekatzeko ez da bakarrik, hemengo mundu hau.

Egon hor beti bizkor, jendeari laguntzen
bideak bai argitzen, kaiolak irikitzen
hasberriri erakusten, egon zaitez hor
baztartu etsipen guztiak
bakarrik inoiz ezin da horma bat zutitu
laguntza behar du, kemena ezin kendu
egon hor, gogor bai jokatuaz.

Hei! gizon musikari zatoz honera
alai dezagun gu biok gau ilundu hau
hei! gizon musikari zatoz honera
nekatzeko ez da bakarrik, hemengo mundu hau.

Biok egongo gera jarrita bidean
eguna argitzean, lagunen babesean
lainoaren parean, ematen
gu biok, berri on guztiak
bakarrik inoiz ezin da horma bat zutitu
laguntza behar du, kemena ezin kendu
egon hor, gogor bai jokatuaz.

Hei! gizon musikari zatoz honera
alai dezagun gu biok gau ilundu hau
hei! gizon musikari zatoz honera
nekatzeko ez da bakarrik, hemengo mundu hau.