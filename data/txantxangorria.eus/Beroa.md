---
id: tx-1526
izenburua: Beroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iMK53M322NM
---

Hitzak: Odei Barroso
Musika: Xabier Zabala

Begiratu gaitezen begietara
deus erran gabe mintzo da begirada
Lotzen gaituen argi-izpi bat bada,
ez zara zu, ta ez naiz ni: gu gara.

Beroa da harrera, egunon erratean,
anaia ta arreba etxera iristean.
Beroa da muxua masailaren gainean,
hola pizten da sua egurrik ez denean.
Beroa da eguzkia gure zure partean,
argitzen du guztia besarkatzen hastean.
Beroa da etxea, guztiak gaudenean,
hitzordua jartzea elkartasunean.

Eskua gox hartuz bidean ibiltzean,
begiak itxi eta lagunak segitzean,
irabazlerik gabe jolasten aritzean,
bihotzaren ateak zabal irekitzean,
lotsarik izan gabe masailak gorritzean,
elkarrekin suaren inguruan biltzean,
berotasuna dago harremanen haizean,
senti-sentiarazi, senti-sentitzean.