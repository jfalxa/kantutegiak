---
id: tx-111
izenburua: Ihia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CncpeTsqDNg
---

Emadan (2021) diskoko Junco kantutik Emaizkidan (2022) bildumako Ihiara, Mono Irazi, Paxkal Irigoyen eta Usorekin, Azkoitiko Matadeixen.

Bideoa: Badator


Hitzak:
Ihiak amets du lo hartu nahi dudala
haren hil hurrenean ikus ez ditzadan
haren zurtoinak jausten.

Deus ez didazu esaten ihesari ematean
nire pausoen atzetik etortzen zara
isiltasun bortitzeko leku horretara
isiltasuna darion.

Gaua dator ibai gris honetara
diluitu du iluntasuna
ni ongi naiz, zu ez zaitez kezka
soilik nahi dut errealitate eza.