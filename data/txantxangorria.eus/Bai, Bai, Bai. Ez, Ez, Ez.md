---
id: tx-2704
izenburua: Bai, Bai, Bai. Ez, Ez, Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xVHv3oqQPG4
---

Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez
Bai, bai barkatu pantera
lapur itxura du
baina zintzoa da bera

Ez, ez
ez izan hortera
zintzoa izateko
pasa zaio aukera

Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez

Bai, bai
esaten da errez
barkatzen duzuela
esan denok mesedez

Ez, ez
ta inola ere ez
ez dugu barkatuko
esan dute denok ez

Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez

Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez
Bai, bai, bai
Ez, ez, ez