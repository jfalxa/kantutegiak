---
id: tx-66
izenburua: Aratuste
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wpgaRS-C0ck
---

Bideoa: Joseba Batiz
Argazkiak: Martin Ugalde

Aratuste  zara Aratuste
Mundakarrentzat
Egun obarik ez
Marrau atorra zurijakaz
Buruan pañuelo sedazko
Ederrakaz

Aratuste zara Aratuste
Tostada egunak,
Ederrak onexek
Onek jan, eta neskak artun da
Plazara dantzan
Iluntzerarte.

Baita atso zarrakaz
Goiko kalian
Ziri ta zara
Arrasta eziñik
Gure elorriyua
¿Arek ziran
Aratustiak!