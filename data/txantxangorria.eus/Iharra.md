---
id: tx-3123
izenburua: Iharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sqx8zFJWhBM
---

Josetxo ezpainak gorri 
eta begiak urdin
Santa Barbara bezperan 
hemeretzi urte egin 
Zibilak ereman dute! 
Ereman dute?
Ereman dute, eta jo dute! 
Eta jo dute!
Xeatu dute deabruan griñakin! 
Zergaitik ereman zuten
eta ilunpean sartu? 
Herria maite bait zuen 
hartan zuen bekatu 
Josetxo ezpainak gorri 
eta begiak urdin
Santa Barbara bezperan 
hemeretzi urte egin. 
 
Josetxo ez da bakarra 
Badira zenbat holako 
Bainan guk, beste guztiok 
Egiten al dugu nahiko? 
Mendian mendiko bordan 
Mikel zaharra artzai 
Atea zabalik dauka 
iheslariaren zai
Zibilek ereman dute! 
Ereman dute, eta jo dute! 
Eta jo dute!
Xehatu dute deabruen griñakin! 
Zergatik ereman zuten
eta hilunean sartu? 
Atea zabalik zeukan 
Hartan zeukan pekatu. 
Mendien mendiko bordan 
Mikel zaharra artzai. 
Atea zabalik dauka 
iheslariaren zai.
 
Mikel ez da bakarra 
ba dira zenbat holako 
Bainan guk, etxeko atea 
Zabaltzen al dugu nahiko? 
 
Lesakatik heldu dira 
Serarat hiru mutil 
Sasietan izkutatuz 
bidezka bati jarrai 
Zibilek tiro eman dute! 
Tiro eman dute?
Tiro eman dute, eta jo dute! 
Zauritu dute?
Bai! hila dute bere txispa zaharrekin! 
Zergatik tiro eman zuten
eta lau bala sartu? 
Zillarrezko errekaiño bat 
Nahi bait zuen pasatu. 
Lesakatik heldu dira, Sararat, hiru mutil 
Sasietan izkutatuz
bidezka bati jarraiz. 
 
Euskadi barneko muga 
odola eta sua... 
Belauniko entzun dezagun 
Sasi horien mintzoa! 
 
Euskadi barneko muga 
odola eta sua...
Bihar ikusi nahi zaitut 
Euskal Herri batua!
 
 
© Telesforo Monzon