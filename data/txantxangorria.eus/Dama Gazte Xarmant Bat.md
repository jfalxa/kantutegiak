---
id: tx-617
izenburua: Dama Gazte Xarmant Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oayaj0eOKtI
---

Dama gazte xarmant bat jiten zaut bixtara,
Iduritzen baitzeraut zeruko izarra!
Ama batek hazteko, hori zerbait bada
Gure salbatzaileak kontserba dezala!
Trala tralara tralara, kontserba dezala!

"Gabon gizon gaztea, konpañiarekin!
Gostu du egotiak ixtant bat zurekin
Badakizu mintzatzen errespetuarekin,
Jainkoak har zitzala hil-ondoan berekin!"
Trala tralara tralara, hil-ondoan berekin!

"Ene maite pollita non ote den bizi,
Bazinakikeea zuk haren zerbait berri?
Aspaldi ikustera joan nahia bethi...
errozu ene partez, milaka goraintzi."
Trala tralara tralara, milaka goraintzi.

"Ez zinuke ia hobe zauhaurrek joaitea,
Nitaz igorri gabe komisionea?
Samurturen ote den ez zaitela fida;
Zatoz enekin eta, biak joanen gira."
Trala tralara tralara, biak joanen gira.