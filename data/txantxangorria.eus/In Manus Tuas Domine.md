---
id: tx-215
izenburua: In Manus Tuas Domine
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z5NrpvbOOJ0
---

jaso nazazu, maitea, azken egunean,
zatoz nere bidera eta adeitsu, irriz
dei nazazu nere izenez
ni zure erruki handian salbatua izan nadin,
salbatuak elkarrekin eta maitasunean glorifikatuak betirako.