---
id: tx-90
izenburua: Joxepa Antoni
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SlLqpvKmL0Q
---

Josepa Antoni, koloregorri
kapitan baten alaba,
aitak dotorik emon ez arren
bilatuko dau sanarra.

Astelehenean ezkondu eta,
martitzenean basora,
eguaztenean orbela batu,
eguenean etxera.
Bariekuan bendeja prestau
zapatuan be plazara,
domekan bere apaindu eta
hamarretako mezara.