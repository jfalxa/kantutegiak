---
id: tx-2943
izenburua: Harri Kozkor Batekin Pirritx Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DhfQlVIsw-o
---

Harri-kozkor batekin,
hor dabil Martin, tin, tin.
Jotzen ditu kax, kax, kax!
Bata bestearekin.
Bata bestearekin,
jotzen ditu kax, kax, kax!
Harri kozkor batekin,
hor dabil Martin, tin, tin.