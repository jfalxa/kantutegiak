---
id: tx-67
izenburua: Astuizu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3PmUw3fVdyI
---

Bideoa: Joseba Batiz
Argazkiak: Martin Ugalde

Ailega da betikolez aratuste eguna, 
gabonak pasa ta segiduen etorri da. Hemendik ez dana juten, 
aspaldiko krisia da 
Alkatie be jun jaku etxe handidxau batera. 
Aztuizu krisidxe, diruen faltie, 
aztu errekortiek, 
jantzi atorra ta urtezak mutil kalera. 
Topaizu lamiñe, eta ein zirridxe, goxo, goxo, goxue, 
arrotu zaitez ''Mundakarra "geuriez (2)