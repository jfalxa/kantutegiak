---
id: tx-2723
izenburua: Ukabil Bakarrean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_De4lu-vTaQ
---

martxan kronoa, 
martxan herri bat 
barroteek erdibituta igaro ditzagun, 
40 minututan horma ta harresi guztiak 
kilometro bakoitzean 
lurralderik arrotzenetan 
kate herdoilak hautsi nahiean 
bide luzeak aurrean bat eginez 
elkartasunean urrun dauden heinean 
gatibu diren artean zulo ilun hoiek 
maitasunez argiztatuz barruko hotz hori 
berotuko duten sugarrak hauspotuz 
lotuz denok batean 
ukabil bakarrean atera ditzagun 
barruak eta kaleak 
bultza zentzu berean 
ta ekarri ditzagun etxera 
ez da hesirik, 
ezta inongo murrurik 
ez da halako mugarik 
inolako espetxerik 
lokarria eten eta 
geldituko gaituenik 
zulo ilun hoiek... 
lotuz denok batean... 
irribarrez, mila sentimendu 
bihotzera iristen direnak ilusioz, 
kemen hutsez 
harro sentiarazi gaituztenak 
gugandik urrun bezain gertu 
etsipena aukeratzat ez duena eredu 
barrenean dispertsatutako taupada bakoitzean 
lotuz denok batean...