---
id: tx-140
izenburua: Harro Bazterrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KDVcG6y_beA
---

it’et’Amen besuetan ikhasi dügü elhestan, 
Gure mintzajia bertan eho die eskoletan, 
Telebixt’eta radio, hoiek direla medio, 
Uskaldüngua adio, debria sar balakio ! 
Uskal-Herrian zunbat khantore ezinago ederrak, 
Eman ditzagün aitzina ere, harro ditin bazterrak.
Muda berritan sartürik, xaharrak bazter ützirik, 
Uskaldünak arroztürik bizi dirade trixterik. 
Gaztek nun nahi pipatzen, ostatietan asetzen, 
Neskatilez baliatzen, politikan güti sartzen. 
Tiercé ‘ta rugby direno axolik gabe gireno, 
Arrünküra gütiago, gobernia trankilago ! 
Zunbat ardi bazterretan dabiltzanak bi patetan, 
Ilhe lüziak espaldetan, txintxingomak müthürretan.
Lehen holakorik etzen bena bardin libertitzen, 
Orai zer zaikü agitzen, gazten artin debeatzen.
Izanik ere bakantza gaztiak trixte gabiltza, 
Arren ikhas bagenetza üskal khantore ‘ta dantza. 
Hitzak : Roger IDIART  
Müsika : Jean ETCHART