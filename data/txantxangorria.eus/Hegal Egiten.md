---
id: tx-657
izenburua: Hegal Egiten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/07sRwwufUUU
---

Neukan guzti zen amets bat, amets bat,
beti egoten nintzen
haren bizita zai (Bis)
Ai! ene ama euriak eta baso ilunak
seinalatzen zizkidanez
ez, ez dira etorriko inoiz
Etzazu holako ametsik egin ez
Mantzo zihoan denbora niretzat
lehioak jarrita euriari begira
eta jendeak zion: "mutil horrek
amets bat bakarrik dauka"

Baina egun bate esnatzerakoan
txantxangorri bat ikusi nuen gelan
ta gero beste beste bat eta beste bat
inguratua nengoen ...

OH TXANTXANGORRIAK
GELAKO SAPAIAN
GELAKO SAPAIAN
OH TXANTXANGORRIAK
GELAKO SAPAIAN

Orain herritik mutil bat falta da
ez dago inor bere etxeko lehioan
baso ilunetan dabil hegaletan
txantxangorriak bezala
Orain ba du bihotz nimino bat
eta bi hegal bi hegal euriz bustiak
Bularra du ere gorri kolorekoa eta
eta txantxangorria da

TXANTXANGORRIA NAIZ
BASO ILUNETAN
BETE ZAIT AMETSA
BASO ILUNETAN
TXANTXANGORRIA NAIZ
BASO ILUNETAN
BETE ZAIT AMETSA
TXANTXANGORRIA NAIZ
TXANTXANGORRIA NAIZ
HEGAL EGITEN HEGAL EGITEN
HEGAL EGITEN