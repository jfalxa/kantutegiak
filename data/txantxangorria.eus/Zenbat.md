---
id: tx-2362
izenburua: Zenbat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-R_D0lDFzM4
---

Zenbat gauz aldatzeko, ta zenbat gai ikasteko, zenbat hesi apurtzeko.
Zenbat gau bizitzeko, ta zenbat malko erortzeko, zenbat soka lotzeko.
Ta zenbat agur.
Zenbat gauz ikasteko, ta zenbat gai aldatzeko, zenbat hanka sartzeko.
Zenbat bidaiatzeko, zenbat jende ezagutzeko, zenbat bihotz puskatzeko.

Aurrera begira, atzokoa atzean utzita,
denbora agortzen bait da,
motxila sorbaldan, melodia bat pauso bakoitzean,
ta aurrera banoa.

Zenbat aho isiltzeko, zenbat gezur entzuteko, zenbat lege erretzeko.
Zenbat kanta egiteko, zenbat paper idazteko, zenbat taupada sentitzeko.

Nire gitarra, zure falta somatzean, negarrez hasi da,
ta hala ere, bere barrenean milaka doinu ezkututa.
Azken malkoa, nire ezpainetan, berriro hastera noa.
Ta beti, ahoan irria, begitan disdira.