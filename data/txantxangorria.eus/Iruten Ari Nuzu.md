---
id: tx-1497
izenburua: Iruten Ari Nuzu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hO8WFlMH4KQ
---

Iruten ari nuzu, kilua gerrian 
ardura dudalarik nigarra begian. 

Nigar egiten duzu oi suspirarekin 
kontsolaturen zira oi denborarekin. 

Ezkon mina dudala zuk omen diozu 
ez dut amore minik, gezurra diozu. 

 
Jendek erraiten dute hala ez dena franko 
ene maite pollita, zure ta enetako. 

Ezkon mina dutenak seinale dirade: 
matel-hezurrak idor, koloriak berde.