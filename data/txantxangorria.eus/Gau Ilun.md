---
id: tx-1676
izenburua: Gau Ilun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cW4enn2T04w
---

Gau ilun honetan nire ezinean ohartu naiz,
ezerrek bete dezakeela zuk utzitako hutsa.
Harresi hotz hauek betirako gaituzte banatzen
itxaropen eta geroa haizeak eramanez.

Oroit zaitut, iraga ezin ahaztuz
bihotzean, zure irudia izanik alboan
ez diot beldurrik itzalari...ez.

Ez kateek ezta hormek
ez dute inolaz ere
gure barne odol sua
sekula itzaliko.

Baina preso hil ezkero
etzazu nigarrik egin
jaio ginen ama lurran
bait gara elkartuko.