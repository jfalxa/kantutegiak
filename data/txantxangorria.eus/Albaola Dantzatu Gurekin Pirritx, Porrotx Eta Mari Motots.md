---
id: tx-1537
izenburua: Albaola Dantzatu Gurekin Pirritx, Porrotx Eta Mari Motots
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/usFl-3pm5ck
---

Gure ametsak 
esna betetzen dira
lagunekin itsasora begira

Mugarik ez daukagu
denok itsasoa gara
gainditu erronkak elkarrekin
arraunak gorantza
belak altxa eta ekin

Gure ametsak 
esna betetzen dira
lagunekin itsasora begira

Mundu bat daukagu zain
itsasoa begietan
sentitu kresala haize hori.
Ontziak eraiki, erori jaiki bizi!

BOGA

Gure ametsak 
esna betetzen dira
lagunekin itsasora begira