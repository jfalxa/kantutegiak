---
id: tx-3359
izenburua: Bihotzetatik Zintzilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lr632Emb0Y4
---

Ilusioaren bidetik gauza biak lagun izan zenuen
Etorkizuna irabazteko aholkua egin zenuen

Sentimenduen bidetik errepidea ezagutu genuen
Urruntasunean gertu sentitzen gaituzue, gaituzue

Argazki bihurtu zaituzte, isilarazi zaituzte
Itsututa gorde zaituzte, 
debekatuta zaude, debekatuta gaude, debekatuta zaude
gaude, zaude, gaudeeee

argazkiak hormetatik zintzilik, zuen irudiak lapurtu dituzten arren
gure artean jarraitzen duzue bizirik
gure bihotzetatik, gure bihotzetatik zintzilik
gure bihotzetatik zintzilik, zintzilik, zintzilik

irriak, laztanak, musuak, besarkadak, epaiketak, kolpeak, tortura,
malkoak, isolamendua, txanponaren bi aldeak, borrokaren bi aldeak, bi aldeak

Lehiotikan begira, zeuen argazkiak gure bihotzetatik zintzilik bizi dira
uholdeei aurre egiten dion harriak eraikitzen du sustraietatik herria
errepide gorrian zure aurpegia 
egun latzetan zuentzat gure hitzen babesa 
libre izan arte, nola? libre izan arte. Goaz! (debekatuta zaudegaude, zaude, gaudeeee)


Argazkiak hormetatik zintzilik, zuen irudiak lapurtu dituzten arren
gure artean jarraitzen duzue bizirik
gure bihotzetatik, gure bihotzetatik zintzilik (ez izan beldurrik)
gure bihotzetatik zintzilik, zintzilik, (gure bihotzaren taupadak entzun ditut bakarrik) zintzilik

Borrokaren bidetik etsipenaren etsaia 
Agertu zinen barroteen arteko duintasuna bihurtu zinen
debekatuta zaude, debekatuta gaude, 
zaude, gaude, zaudeeeee

Argazkiak hormetatik zintzilik, zuen irudiak lapurtu dituzten arren (lapurtu dituzte arren)
gure artean jarraitzen duzue bizirik
gure bihotzetatik, gure bihotzetatik zintzilik
gure bihotzetatik zintzilik, zintzilik, zintzilik