---
id: tx-2969
izenburua: Mirestu Beza Munduak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dIPDadLosd0
---

Entzun kristauek, espantu gabe oraingo notizi fina
periodikuan irakurri dut ez da neronek egina:
Espainiako Gotzai batzuek ezarri dute dotriña
Santa berritzat izendaturik Gaztelako erregina
gutaz kupitzen ez baldin bada zeruko Ama Birjina

Konta ditzagun hitz gutxirekin Isabel zenaren amak
bide onetik joan ditezen gaurko ta biharko damak
moru batzuek erre zituen judutarrak ia danak
bostehun urte kunplitu dira Ameriketan emanak
bertako asko esklabizatuz, lehenago libre ziranak.

ta ta/ra ta ta ta...

Imperio bat eraiki zuen espatarekin guduan
bere promesa kunplitu gabe garaipenaren orduan
zilegi bada fede santua ezarri haren moduan
erlijiorik ez da biziko bakez elkarren onduoan,
ejenplu ona jarriko dute santa horrekin munduan.

Ez naiz sartuko Fernandorekinizan zuen harremanaz
larru kontuak santifikatuz seme-alabak emanaz;
hori entzunda oroitu nintzan segituan nere amaz,
bera bezela etxekoandre jatorrak izan diranaz,
inork ez die hori ordaindu gerora santa eginaz

ta ta/ra ra ta ta...

Esaten dute erregin hauek bazuela piedadea
hereje eta pagano txarrak hiltzeko abilidadea
eraiki zuen katolikoen zilarrezko edadea
eulirik ez da hemen mugitu, hau da trankilidadea,
zezen korridak, putaetxeak, pattarra eta kafea.

Ez da makala, hala Jainkoa, obispu hoien arreta,
nik uste nuen santutasunak altu zuela pareta,
errege denak jeneralian sutan zeudela erreta,
milagro haundia gertatuk da Erromak hala nahi eta,
inpernutikan igoko dute pertsona bat aldareta

Hau miraria, hau marabilla, etortzen bada eguna
Santa Isabel Kastillakoa otoiztu behar duguna;
haren ondoren bat bakarra da gero faltako zaiguna,
gizon ttiki bat, bigote duna, noizbait ezaguna,
berrogei urtez hemen agintzen segidan izan genduna.