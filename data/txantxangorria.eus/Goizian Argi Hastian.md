---
id: tx-2142
izenburua: Goizian Argi Hastian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/STNluqf7kXo
---

Goizian argi hastian,
ene leiho-hegian
txori bat pausatzen da eta
goratik hasten kantan.

Txori eijera, hain alegera,
entzuten haidanian,
ene bihotzeko tristura
laister duak aidian.

Ene txoririk maitena,
zertaz jin hiz ni gana

Iratzarrazi nialarik
ametsik eijerrena.
Jinik hain goizik, uste hiena
baniala hanitx pena?

Ez, ez, habil kontsolatzera
malerusago dena.