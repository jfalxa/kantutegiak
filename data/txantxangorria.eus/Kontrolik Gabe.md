---
id: tx-1183
izenburua: Kontrolik Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EXqSDI1O8LQ
---

------------------------------------------
(Letra: Iulen Romaratezabala «Pruluns», Andoni Goitia
Musika: Eneko Dorronsoro)

Herri zahar honen ispilu
hitza denean iskilu
Atsasun gertatua.
Frankismoaren ajuak,
berdezko jendilajeak
bortxaz okupatua.
Inperio baten sobrak,
gerra zibilaren zorrak.
Bakarra ta batua,
herri hau zapaltzen duen
ta frankista omenduen
Estatu armatua.

Aske nahi dugu ospatu,
kontrolik gabe gozatu
mendi, kale, tabernak.
Ez isun eta ez epai,
ez ditugu aditu nahi
Guardia Zibilen penak.
Duintasunaren atzean,
haien gezurren aurrean,
euko dituzte denak
muntaia polizialen,
bertsio ofizialen
ateoak garenak

Maitasun dena maletan.
Izango ditugu bueltan,
ongi dakigu jakin,
Oihan, Adur eta Jokin
ta beste guztiak berdin
Altsasuko kaletan.

Justiziaren zirkoa,
zirko mediatikoa
liskar baten harira.
Egi erdiak osatuz,
gezur osoak zabalduz
guztien belarrira,
bakerako mugarria.
Alde Hemendik aldarria
indarra hartzen ari da
eta daudela seguru
geuk bidaliko ditugu
joaten ez badira.