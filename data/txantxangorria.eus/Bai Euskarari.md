---
id: tx-2008
izenburua: Bai Euskarari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z5wnu8v1N7Y
---

ETA BAI, ETA BAI
NIK EUSKARARI BAI TA BAI

Uxo xuria, uxo gorria
dantzan dabil suaren gain
uxo gorria, uxo xuria
herriak euskarari bai
neure kabia daukat garretan
neure umeak neure zai
nago odoletan, garretan,
txinparta gainetan
neure umeak neure zai

Uxo xuria aurkitu dute
hotzak dardaraz erdi hila
uxo gorria garretan dator
Euskal Herriaren bila
uxo xuria lehengoa duzu
uxo gorria geroa
Euskal Herritik uxo bakar bat
iguzki aldera doa

Uxo xuria, uxo gorria
dantzan dabil suaren gain...

San Juan Loreek usaia dute
eta herriek arima
Euskal Herria euskara barik
irin gabeko ogia
Euskal Herria euskara gabe
hegalgabeko usoa
uxo gaztea aidatu hadi
eta iguzkiraino hoa

Uxo xuria, uxo gorria
dantzan dabil suaren gain...

ETA BAI, ETA BAI
NIK EUSKARARI BAI TA BAI

ETA BAI, ETA BAI
GUK EUSKARARI BETI