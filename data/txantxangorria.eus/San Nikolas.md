---
id: tx-1327
izenburua: San Nikolas
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g2uvaJg1dn4
---

Urte berri berri,
nik atorra berri.
maukarik ez,
nik axolik ez.

San Mikolas coronado
confesor es muy honrado.
Ale, ale, alegría,
todo el mundo alabaría.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de dios.
Aingeruak gara,
zerutik gatoz,
poltsa badugu, baina
eskean gatoz.

San Mikolas coronero,
confesero Mari Andrés.
Alarguna dontzellea
cantaremos alegría.
Bost etxean sei ate,
zazpi etxean su ete.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de dios.
Aingeruak gara,
zerutik gatoz,
poltsa badugu, baina
eskean gatoz.