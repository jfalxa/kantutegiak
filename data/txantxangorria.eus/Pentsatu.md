---
id: tx-1022
izenburua: Pentsatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PtSjRmbolAw
---

Askatasuna, 
Demokraziak eman 
Eta legeak kentzen badizu, 
Nahiago esklabu izan 
nik erabakitako bizitzan 

Ozen ari diren 
mila ahots 
isiltzeko, 
Ez dugula eman 
Inongo baimenik, 
Ez duzuela asmatuko, 
Gure irududimena, 
Geldituko duen armarik 

Beldurrik gabe oparitu, eskuz idatzitakoak, 
Itsuen erreinuan 
Pentsamenduak jaurti, misilak bailiran. 
begietan egia. 

Esku bat luzatzen 
Ta bestean labana duenarekin 
Bakea zertarako 

Hitzak, suak, 
Berdina Erretzen 
Urak ez dio 
Egarririk kentzen 
Indiferentziaren Basamortuetan 
itotzen dagoenari