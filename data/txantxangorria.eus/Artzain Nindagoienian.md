---
id: tx-2491
izenburua: Artzain Nindagoienian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GWAli5yqjss
---

Artzain nindagoienian
mendi gorenian, 
gogoetan jartzen nintzen
ni haur nintzenian. 

Artzaina gogoetan
denbora joan eta,
antxurik ederrena
bordatik gal eta.
        ♪♪♪♪♪♪
Joaiten naiz elizara
mezaren entzutera,
ene maitea ikusi
niri so zagola.

So_eiten diot hari,
hark ere bai niri,
gero biek irri;
nik neure buruari:
Marka_ona duk hori.
        ♪♪♪♪♪♪
Aitak alabari:
Zer debru dun hori? 
Ez niganat ekarri
arima galgarri.

Artzainak alferrak ditun,
ase ta_ongi nahi,
hobe dun bertzetarik
edozein nahi.
        ♪♪♪♪♪♪
        ♪♪♪♪♪♪
Artzain nindagoienian
mendi gorenian,
gogoetan jartzen nintzen
ni haur nintzenian.