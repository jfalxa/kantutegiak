---
id: tx-2024
izenburua: Albo-Kalteak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F3L69FwXaPg
---

Erderaz idatziak izan dira gure ideien autopsia denak 
elefanteen amnesia guztia
ortografia garbizaleetan . Nekea begietan 1D-n ikusteko
antiojuak jantzita
eta haiek eustea beste egitekorik ez euren belarriak
Hauek dira , izan , hauek dira finean .... Albo-kalteak
Kamerez josita daude kaleak , halakorik ez galdeketetan
ikararen koordenada berriak Europa zaharreko estoldetan
Emaitza sinesgaitzak silogismoen olgeta ilunak
 orrialde erauziak
Zuzenbiden liburukote astunetan
Hauek dira , izan , hauek dira , nola esan
gu izatearen albo-kateak ez izatearen ajeak
Gutxiespenaren dialektikari : eppur si muove
komeni den legediari : si muove ,
norbait izateko hurkoa beti zapaldu behar
duenari akordeoetatik doazkio bi hitz : eppur si muove