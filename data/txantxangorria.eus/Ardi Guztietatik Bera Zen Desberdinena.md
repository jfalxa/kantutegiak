---
id: tx-2874
izenburua: Ardi Guztietatik Bera Zen Desberdinena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mOau9IJdNws
---

Beste ardi guztietatik bera zen desberdinena,
etzen txuria,
etzen beltza,
ez handiena,
ez txikiena,
baina bazeukan zerbait beste guztiek etzeukatena,
artzainak bera zuen maiteena

Ardi txakurrei eta makilei etzien obeditzen,
Sortze beretik ikasi bai zuen larrean libre bizitzen,
Itsaso eta mendien arteko lurretan bizi zen,
Askatasunari irri bat eskeintzen.

Hitzak:Txuria