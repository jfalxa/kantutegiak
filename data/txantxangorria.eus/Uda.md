---
id: tx-328
izenburua: Uda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/68QaJNsWGX0
---

Joan zinen gau batez 
Joan zinen Iasterrez 
Amodioa zinez 
Herritarsunez betez. 

Aurten gure Uda lilia 
Goizik histua 
Ekainaren hogoitabatean 
Erori fruitua.
 
Maddi era lu so nome 
È Uda lu so cugnome 
Uda vole di l’estate 
Quand’e pianure so affiurate 
È e rosule spannate 

Ma di ghjugnu ottanta sette 
So ghjunte e mille saette 
U primu ghjomu d’istatina 
Funesta fù la matina 
Oscura l’alba matutina 

In francia hè ghjornu di festa 
Ma quandu l’orcu s’intesta 
Di l’umanu ùn hà primura 
Uda era basca è pura 
È a so lotta era si cura … 

Ùn hè fatalità 
Anu tombu Uda 
Maddi era lu so nome 
È Uda lu so cugnome