---
id: tx-912
izenburua: 100 Alargunen Dantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/THcllshLD0Y
---

Musika : Manez (Thierry Biscary)
Letrak : Uxue Alberdi

© Kalapita productions

Bideo egilea : Etxeko prod


100 ALARGUNEN DANTZA
Bermioko plazaren beltza,
Ohe hutsaren beltza
Edertasun handia, beltza ;
Bizi minaren beltza
Zerua beltza, bihotza beltza,
soineko beltzaren beltza
Dantzari alargun beltza,
maitasun beltzaren beltza.

Itsasoko marmar isila,
bare, barkamen bila,
dantzarien azal zurbila ; 
esku epelen bila,
senarra hila, semea hila...
ilargia, biribila,
dantzaren murmur isila
itsaso berrien bila.

Elkartasun malkoen dantza, 
izar berrien dantza,
egunsenti argien dantza,
sare moreen dantza, 
andreen dantza, suaren dantza,
sortuko direnen dantza,
minaren, pozaren dantza : 
bizitza beraren dantza.