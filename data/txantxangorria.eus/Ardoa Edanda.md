---
id: tx-3301
izenburua: Ardoa Edanda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lJ7nUmCUDrw
---

Ardoa edanda mozkortzen naiz
pipa erreta zoratzen naiz
kortejatzean lotsatzen naiz
nola demontre biziko naiz?
Tra le ra...

Pipa hartuta zoratzen naiz,
ardo edanda mozkortzen naiz,
morkortu eta erortzen naiz...
Nola demontre biziko naiz?
Tra le ra...