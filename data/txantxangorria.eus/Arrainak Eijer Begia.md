---
id: tx-2880
izenburua: Arrainak Eijer Begia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gfnhFFMonUY
---

Arraiñak eijer begia, oi neskatila faltsia.
Xoriño batek erran zerautan biga bostetan egia,
Biga bostetan egia, zu ez zinela enia.

Goiti eta beheiti banabilazu ni beti.
Libertitzen ere huneski ene gazte laguneki,
Ene gazte laguneki, nahigorik zureki.

Herbia doa lasterrez ihiztariaren beldurrez.
Herri huntako neska gaztiak hurturik daude nigarrez,
Hurturik daude nigarrez, ezkont ez diten beldurrez.