---
id: tx-1302
izenburua: Sismografoa Eta Lurrikara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Eb9oZH8pA8s
---

Bideoa: ARTIFIKZIO

Liher gara:

Iker Vazquez (Bateria)
Iñaki Zubia (Zuzeneko soinu teknikaria)
Iñigo Etxarri (Gitarra, koroak)
Joshka Natke (Baxua)
Lide hernando (Ahotsa eta gitarra)


Bareak badu ezinegon bat
Ertzean bizi nahi dugunongan
Gure lurrean dardarizoak
Matxura dakar sismografoan

Arrakaletan bizi den urak
Badu zer konta bizimoduaz
Basamortutzen diren orduak
Fruitua jaso nahi ta ezinduak

Egunen zauri lehorrez gozatzen ikasiak
Galerak utzitako hutsaz zer ospa daukagunak
Patua lurrikaretan errausten dugunak
Gara.

Senaren plaka tektonikoak
Aspaldi irla bihurtuak
Elkar batzeko intentzio errudun itsuaz
Porrotak gure mendilerroak

Sismografoa ta lurrikara
Bando berean indartu gara
credits
from Tenpluak Erre, released February 7, 2018
Donostiako IN Estudioan Iñigo Etxarrik grabatua.
Bateriak Urdulizko Tio Peten grabatuak.
Bonbereneako Karlos Osinagak nahastua.
Azkarateko Mamia estudioan Jonan Ordorikak masterizatua.