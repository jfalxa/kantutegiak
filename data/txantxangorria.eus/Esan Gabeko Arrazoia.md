---
id: tx-815
izenburua: Esan Gabeko Arrazoia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YRG3N0XRbjc
---

Ixilik zaude, non da zure ahots laguna? 
Ozenki mintzo zait gaur zure ixiltasuna:
Egiazko ipuinetan legez, 
Zientzia liburuan ere ez da
Hain ozen mintzo egia,
Hain nabarmen, hain garden, egia.
Zure ordez ote da mintzo mundua?
Esan gabea ere ulertu behar al dut, ba? 
Sekretoa ostoz osto biluztu, 
Gezur batean nahastu,
Egia hori ez baita maitagarri,
Zure egia ez baita entzungarri.
Laster nauzu utziko,
Haizeak ere susmoa dakarkit.
Aldean ez daukat
Esan gabeko arrazoia baizik.
Argi printza batzuk etsi etsian, 
Sartu nahiean dabiltza zure etxean.
Laster dena ahaztuko
Pazku goiz ederrean ote
Ala berantago,
Besta berri bezpera batez,
Argi printza azkenak etsi etsian
Sartu nahiean dabiltzatela zure etxean.
Ixilik zaude, non da zure ahots laguna?
Ozenki mintzo zait gaur zure ixiltasuna.