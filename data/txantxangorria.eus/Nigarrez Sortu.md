---
id: tx-2792
izenburua: Nigarrez Sortu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9Ebqp-f75wQ
---

Nigarrez sortu nintzan, nigarrez hiltzeko;
Ez ordean munduan luzaz bizitzeko :
Jendek ainitz dute erraiten frango,
Aita ama maiteak, zuen biendako.

Aita ama maiteak, mundura zertako
Eman izan nauzue zorigaitz huntako ?
Nik ez nuen hobenik hola tratatzeko ?
Federik eman sabe abandonatzeko.

Sortu nintzen ordutik hiru oren gabe,
Bero-xoko batean gaixoa eman naute;
Nihaur bakarrik bizi, xoriak haurride,
Ene kontsolatzerat kantuz heldu dire.

Ni goizeko aroan hementxe paratu;
Geroztik eguzkia gainetik pasatu :
Aita gorde da eta amak nau ukatu ;
Ez ote naute behar norbaitek altxatu ?

Sorte trixte huntan naiz... mundura etorri,
Oi hemen niagozu/ bildotx bat iduri :
Lurra sehaska eta zerua estalgi,
Norena ote naizen Jainkoak badaki

Zeruan eguzkia zelarik apaldu,
Andre gazte bat zaitan aldetik pasatu;
Aingeru bat nintzela zenean ohartu,
Amultsuki ninduen besoetan hartu.

Zato enekin, zato, aingeru maitea !
Dolugarri laiteke zu hemen uztea...
Bozkario enetzat zutaz jabetzea;
Erakutsiko dautzut zeruko bidea...

Bataiatu ondoko goiz alba gorritan
Jainkoak bildu zuen agur ederrean :
Haurra ez da galduko munduko bizian :
Aingeruekin dago zeruko lorian.