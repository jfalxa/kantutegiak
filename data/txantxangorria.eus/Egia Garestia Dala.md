---
id: tx-1810
izenburua: Egia Garestia Dala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/72qsZFb39lM
---

Heldu zen udaberria
kantuz hasi henaria.
Lagunak preso jausi direla
iritsi zait gaur berria.

Urtu ziraden elurrak
narrotu otso-zakurrak
oraino ere minberaz daude
ene lagunen hezurrak.

Lainoak dire odoltu
gizon prestuak ixildu
behin berriro ere gure Sisifo
maldaz behera amildu.

Kontzientziak urratu
Bakea dute saltu
lau horma artean egin dituzte
bizirikan enterratu.

Indarra dela morala
ordena dela kabala
agertu dute gizonarentzat
egia garesti dala.