---
id: tx-2344
izenburua: Gurea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Kow-jFdPdLg
---

Talde sortu genuenean, bideo bat igo genuen instagramera abesti hau jotzen bi gitarra klasikoekin.
Erantzun oso ona izan zuen bideoak eta pentsatu genuen ondo egin behar genuela oraingoan. Beraz, 2018ko Martxoan, Koban estudioetan egon ginen akustiko hau grabatzen. Ostean bideoklip hau grabatu genuen eta hona hemen emaitza!!
----LETRA----
Hau dala gure herria
Guztiok bat gara
Ta maitatu behar dala
Eta hau dala ama hizkuntza
Munduan bakarra
Erabili behar dala Euskara!
Txikiak gineneko Esaera zaharrak
Erabiltzeko garaia Tradizio ta ohiturak
Ez ditzagun utzi
gaur egun eutsi
Ta maitatu Dezagun beti gure herria
Ez ahaztu Bertan bizitako guztia
Askatu
Barnean daukagun dana eta
Altxatu eta borrokatu, borrokatu!
Gure herrikoa dugun
Hizkuntza zaharra
Erabiltzeko garaia Euskara desagertzen
Ez dezagun utzi
Eta gaur egun eutsi