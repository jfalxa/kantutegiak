---
id: tx-2337
izenburua: Mundua Aldatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FUmzPFhhp-s
---

Onintza Enbeitaren bertsoetan oinarritutako abestia, 2018ko uztailan Iriondoan grabatua.


LETRA (Euskara)

Neska gazte bati egin 
ziotenak enboskada.
Imajina genezake 
garraxi eta taupada,
imajina genezake 
bortxatzaileen begirada.

Guztia argi zegoen,
argiegi beharbada.
Salatzeak merezi du?
Hartua nuen patxada.
Ta gaur berriro diogu:
honezkero nahikoa da!

KALERA ATERA GARA
EZ BEHAR BEZAIN SEGURU
GUK, NOSKI, SINISTEN DIZUGU.
MUNDUA ALDATZEA DA 
AMESLARION HELBURU,
TA LORTUKO DUGU!

Emakume ta gizonak
zein dugu konbibentzia?
Ezetza ezetza bada, 
non dago tolerantzia?
Bost gizon eta neska bat
ez badugu biolentzia...

Amorruak hartzen gaitu,
nik galdu dut pazientzia.
Nire desioan dago 
aurrekoen lizentzia
eta berdin digu zein den
epaileen sententzia!

KALERA ATERA GARA
EZ BEHAR BEZAIN SEGURU
GUK, NOSKI, SINISTEN DIZUGU.
MUNDUA ALDATZEA DA 
AMESLARION HELBURU,
TA LORTUKO DUGU!

KALERA ATERA GARA
EZ BEHAR BEZAIN SEGURU
GUK, NOSKI, SINISTEN DIZUGU.
MUNDUA ALDATZEA DA 
AMESLARION HELBURU,
TA LORTUKO DUGU!

KALERA ATERA GARA
EZ BEHAR BEZAIN SEGURU
GUK, NOSKI, SINISTEN DIZUGU.
MUNDUA ALDATZEA DA 
AMESLARION HELBURU,
TA LORTUKO DUGU!