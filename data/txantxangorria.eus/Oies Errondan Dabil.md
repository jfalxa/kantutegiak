---
id: tx-644
izenburua: Oies Errondan Dabil
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IA9jBmfFxHs
---

Oies errondan dabil
berak daki nundik.
Karmen gerri politen
ate ondotxutik. Oies:
sarritxutan errondan
ibili zaitekez.

Itsaso aldetik
dauko bentanea,
handixek erosten dau
Karmenek sokea. Oies:
dirua emon nahi ta
inork hartu nahi ez.