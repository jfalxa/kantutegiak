---
id: tx-1903
izenburua: Arrosako Zolan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9xQ9qtPUBwY
---

Arrosako zolan,kantu zirimolan 
Nahiz eta telebixtan ez gaituzten modan 
Euskararen gara,lokarri azkarra, 
Gau ilun honetako gure artizarra 
Zazpiehun langile,sutsu ta ekilez 
Oraina ta geroa ez ditaizke berez, 
Kontzertu hunen bidez,musika eztiez, 
Euskal izen ta izana,betikotz bat bitez.

Kola ta hanburgesa,euskaldun baldresa 
Gustua galtzea ere zer dugun errexa! 
Kendu nahi digute,bihotz ta bertute, 
Izaki klonikoak egin nahi gaituzte. 
Jo dezagun bada,goraki aldaba 
Multinazional hoien herria ez gara, 
Bretoi ta kortzikar,kanak okzitandar 
Nor bedera izaiteko har dezagun indar

Munduko herrien ta irrati libreen 
Bilgune goxoa guk dugu urtero hemen
Uhainen ildoan,asmoak geroan 
Elkarren ezagutza daukagu gogoan. 
Munduko erronkak,herrien borrokak 
Bildu gara hausteko zapalkuntzen sokak 
Dantza ta irria,kantuz ilargia, 
Gau hontan zoriontsu da Euskal Herria