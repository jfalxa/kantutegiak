---
id: tx-1312
izenburua: Emaiogu Bostekoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0Dq1Z7Fg2Kk
---

KORRONTZI BENEFIC proiektua Agus Barandiaranek ideiatu eta zuzendu
du. “Emaiogu bostekoa” abestiaren musika eta letra Agus Barandiaranek
eta Xabier Zabalak egin dute. 2016ko azaroaren 3 eta 4an
grabatu du Donostiako Elkar estudioan Victor Sanchez soinu teknikariak.
Nahasketak eta masterizazioa Cesar Ibarretxek egin ditu (Keep
Rollin´ studios, Bilbo). Testuak: Agus Barandiaran eta Izaskun Iturri.
Haurren marrazkiak eta argazkiak: TDH-Gizakien Lurra eta Aitzina-Aefat.
Ondoren kantuak izan dituen laguntzaileak: Xabier Berasaluze “Leturia”:(panderoa
eta koruak), LM Moreno “Pirata” (tronpeta, tronboia,
saxofoi tenorea eta baritonoa). Donostiako Ekintza ikastolako “Txiki”
abesbatzaren prestatzaileak Marina Lerchundi Arileta eta Elena Arandia
Jauregui izan dira. Pirritx eta Porrotx -Aiora Zulaika eta Joxe Mari
Agirretxe (ahotsak). Xabier Zabala (zuzendaritza musikala eta konponketak).
KORRONTZI
Agus Barandiaran: trikitixa eta ahots nagusia
Ander Hurtado de Saratxo: bateria eta txalaparta
Kike Mora: baxu elektrikoa
Alberto Rodríguez: gitarra eta oktabatutako mandolina
web: www.korrontzi.net


Zurrun zurrunkatuz, ametsak abestuz
gora behera kaletik doa,
bi begi txikien tristura


alaitasuna da behar dutena.

Herri oso baten hauspoa
bihurtzen da neure trikitixa,
Kantauritikan hegoaldera
ume txiki baten irrifarra.

Tiri kiti triki, urru kutu truku
pandero zaharraren soinua,
laguntza behar duen esku bat
emaiogu denok bostekoa.

Herri oso baten hauspoa
bihurtzen da neure trikitixa,
Kantauritikan hegoaldera
ume txiki baten irrifarra.

Kanta bat kantatzen, letra bat abesten
firin faran mila zorion,
agur esan eta bagoaz
kopla zahar batekin aio gabon.

Herri oso baten hauspoa
bihurtzen da neure trikitixa,
Kantauritikan hegoaldera
ume txiki baten irrifarra.

Herri oso baten hauspoa
bihurtzen da neure trikitixa,
Kantauritikan hegoaldera
ume txiki baten irrifarra,
ume txiki baten irrifarra,
ume txiki baten irrifarra.