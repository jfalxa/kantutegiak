---
id: tx-3148
izenburua: Killin -Mikel Urdangarin-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I0JKbAFnDUQ
---

banoa bakarrik
ikusi arte
ez nuke hasi nahi
adio eske
ez dira pasako
urteak hainbeste
nire ausentzian
zaindu zaitezte

olerki batzukin
nator zugana
errekiema aspaldi
idatzi zidana
zugandik jasoa
zuri emana
sortu ez dakidan
halako zama

behin batean zure
kaleetan sortu
zertarako baina
semetzat hartu
gaztetan nintzaizun
gogoz oldartu
kitana batekin
nintzen elkartu

baita piztu ere
zure amorrua
pobre izateak
zuen errua
hura sorgina eta
ni deabrua
han sortu genuen
deskalabrua

gero jaio ziren
semealabak
haiekin batera
barru ikarak
laister ixildu ere
herriko aldabak
han amaitu ziren
killin-go gabak

orduz hasi nintzen
erruz edaten
onoan nuenari
gaizki esaten
ito izan banintza
ardo upel baten
sorginak eurekin
eroan nintzaten

agurrik ez, soilik
ikusi arte
denbora joan zait
osasun truke
sorginak urduri
hainbeste urte
nire bila edo
esango nuke