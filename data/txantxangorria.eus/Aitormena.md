---
id: tx-1725
izenburua: Aitormena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HkBHt_SVLE4
---

Ez dira betiko garai onenak
azken finean gizaki hutsak gara
Barearen ostean dator ekaitza
Udaberri berririk ez guretzat
Denborak aurrera etengabian
ta orain ezin eutsi izan giñana
rutinaren morroiak bihurtu gara
laztana lehen baino 
lehen aska gaitzetan.

Ohartu gabe arrunt bilakatuta
Ohartu gabe heldu gara mugara
Mundua jautsi zaigu gainera
maitia lehen baino 
lehen aska gaitzetan.

Ez dakigu non dagoen hoberena
Bila dezagun beste lekuetan
Ba, zin dagizut ez dizudala 
inoiz gezurrik esan eta
zaude zihur ezin izango 
zaitudala ahaztu inoiz
aitortzen dut izan zarela 
ene bizitzaren onena
baina orain, maitia lehen baino 
lehen aska gaitzetan.