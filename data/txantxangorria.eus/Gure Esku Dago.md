---
id: tx-3387
izenburua: Gure Esku Dago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NT9qw2RriTM
---

Kanta zaharrak dioen bezala, 
ura berebidean doa. 
Goi mendietako urxirripa
kanpo emariz ibaitu da.
Mugarik ez jarri urei. 
Utzi libre beheratzen, 
milaka urtez egin legez..
Guk ez dugu aldarri egiteko
heroi edo konkistarik. 
Borondateak elkartzen gaitu, 
herri izan nahiak.
Hormak eta harresiak ez ditugu maite, 
elkarrekin bizi nahi dugu, soilik. 
Ez dugu beste inor mendean hartu nahi,, 
etorkizuna hautatu, ez besterik.
Har dezagun bidea, gure esku dago eta. 
Milaka lagun oinez, eskutik oratuta. 
Jende zoriontsua, herri libre batean. 
Herri libre batean!
Rosa, Arane, Omar, 
Izaro, Maddi, Carlos,, Antoine......
Har dezagun bidea, 
gure esku dago eta. 
Milaka lagun oinez, 
eskutik oratuta. 
Jende zoriontsua, 
herri libre batean. 
Herri libre batean!
Hiritar desberdinak ametsak ditu biltzen:
etorkin, arrantzale, langabetu, 
ikasle, nekazari,, langile,, 
musikari,, idazle,, erretiratu, ume.
Garazi, Jabier, Bittore, Lur, 
Amaia, Gloria, Mamadu...


Kirmen Uribe