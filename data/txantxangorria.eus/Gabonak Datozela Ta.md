---
id: tx-1329
izenburua: Gabonak Datozela Ta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KMKJbiQtjm0
---

Gabonak datozela ta
jendearen tarrapata
erosketa da nagusi,
adornua ta zarata. (2)
Gabonak datozela ta
zelofan paper de plata
nahiz epetan, nahiz zorretan
kapritxorik ez da falta. (2)
Txanpo-txanpanak botaka,
itxura hala dela-ta
letra ederrik helduko
zaigu, Gabonak pasata. (2)
Gabonak zetoztela ta
bota genuena falta,
ez da erraza izango
urtarrileko aldapa. (2)
Gabonak zetoztela ta
zelofan paper de plata
gastatu dugu soldata
bukatu da tarrapata. (2)
Gabonen pozarren gatoz
pozik barruz eta kanpoz
zuei ere Gabon-poza
opa dizuegu gogoz. (2)