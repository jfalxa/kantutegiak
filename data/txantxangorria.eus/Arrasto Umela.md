---
id: tx-2011
izenburua: Arrasto Umela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wnIDZjB8A9c
---

Pizti arrunten modura 
senarra dator barrura
joaren joaz harrapatu du 
haziak helmuga
gero betiko ohitura
nekea edo logura

azken musu bat ahaztu ostean
seko geratu da
nahiz bakarrik nauen utzi
kulerorik ez dut jantzi
asetu ezak tentaldiari 
ezin dio eutsi
lehengoa zen horren gutxi
trenetik zergatik jeitsi
gozamenaren geltokiraino
ez bada iritsi

behin haitzuloan sartuta
samurtasunaren punta
hatzamarraren eskutan dago
helburu arrunta
orgasmoraino pausuka
eraman beharra nauka
alboko pendoi artega horrek
ezin izandu ta

sumendiaren epela
berotzen doan bezela
izaretara bideratzen da
arrasto umela
hau bai harreman fidela
sexuzko autonobela
laztan bakoitzak bere txikian
asetzen nauela

igurtzi baten ostean
hurrengoaren eskean
gozamen suak hastera doaz
nire hankartean
dardara luze batean
murgildu naiz bapatean
ta sekretua ezin izan dut
gorde isilpean

asperenak salaturik
albokoa kezkaturik
lasai gizona ez baita izan
ametsa besterik
ondoren isil isilik
buelta erdia emanik
loak hartu nau irribarretsu
ta kulero barik.