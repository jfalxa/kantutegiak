---
id: tx-1768
izenburua: Musikaren Doinuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ynf1KO7j7Js
---

Musikaren errotik, 
sortutakoa, azaldu
ezinezko sentimendua.
Musikaren itzalean
dantzalekua.
Musikaren doinua...

Herri arteko loturan,
eragin dezala. Gure 
barneko grinak,
konpondu ditzala.
Azaleko liskarretan
laguntzazkoa.
Musikaren doinua...

Irabazi edo galdu,
beti zurekin doa.
Alde egin edo geratu
beti zuren barnean.
Punk doinuak,
zein blusa, edo ska musika.
Berdin du erabakia, 
musikaren zentzua.

Mundu libre bat dago,
zure aurrean. 
Erabaki anitzezko sorta.
Emozio sortzaile
badugu alboan.
Musikaren doinua...

Irabazi edo galdu,
beti zurekin doa.
Alde egin edo geratu
beti zuren barnean.
Punk doinuak,
zein blusa, edo ska musika.
Berdin du erabakia, 
musikaren zentzua.

Irabazi edo galdu,
beti zurekin doa.
Alde egin edo geratu
beti zuren barnean.
Punk doinuak,
zein blusa, edo ska musika.
Berdin du erabakia, 
musikaren zentzua.