---
id: tx-1460
izenburua: Lore Polit Baten Bila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ny25MCHlsQs
---

Lore polit baten bila herritik herrira noa,
zurekin elkartu arte, bihotzeko andereñoa.
Kutun kutun estaltzen du mendi gainea lainoak,
bilatzen zaitudanean berdinak nere asmoak.
Mendi tartean haizea zirika doan antzera,
zure gorputza maitea nahi nuke zirikatzea.
Mendi gainea lainoak estaltzen duen antzera,
egin nahi nuke berdina zurekin ene maitea.
Atseden gabe lasterka egiten nago bidea,
gidaria dut zure usaina dakarren haizea.
Mendilero ta zelai bazter guztiaren jabe,
haizea bezala nik izan nahi nuke zurea.