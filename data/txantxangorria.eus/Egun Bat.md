---
id: tx-786
izenburua: Egun Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2Nz-iaJk9Vo
---

Alperrik nau oge ganien

jantzi ta kalera noie

kanpoan begi tristiek,

dan-danak ilun-ilunek.

Kafe Olé bat baino

gehixau biherko dabe

espabiletako.

Joder, joder

zelan dagoen

perkala maittie

perkala kalien.

Joder, joder

zelan dagoen

jentiek ez daki

egunon esaten.

Erreka ondotik buelta bat

bisigu bi aurkitu dodaz

zubi barrixen aurrekontue

udaletxeko zurrumurruek.

Joder zenbat dakizuen

hurrengo hauteskundeetan

zeuek alkate.

Joder, joder

zelan dagoen

perkala maittie

perkala kalien.

Joder, joder

zelan dagoen

jentiek ez daki

ixilik egoten.

Uuuh... uuuh... uuuh...

Etxerako bidien

txakur kakak zapaltzen

portaleko atien,

zeu bakarrik falta zinen.

Uuuh... ene maittie

hain hurbil

hain urrun...

zenbat...

faltan botaten zaittuten.

Agur...

agur, agur

ogera noie

danak popatik hartzen

bota baino lehen.

Agur, agur

ene maittie

jarri eizu berriz

Arethan kantue

Joder, joder

zelan dagoen

perkala maittie

perkala kalien.

Joder, joder

zelan zagozen

jarri zaitez berriz

nire ganien.