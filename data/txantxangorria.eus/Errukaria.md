---
id: tx-2689
izenburua: Errukaria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nugWQbKUB1M
---

Aspaldian ez da gure etxean 
ogirik ikusi 
bainan gaur txiki ta parra 
izango da noski.
Oso arlote bizi gera
ezin osaturik.
Zenbat aldiz eguerdian
oraindik baraurik!

Gaur bai jana ta edana
labiru lana,
jango ote degu dena
labiru la. E/up!

Marto opilla
egiteko
aitak atzo saldu zuen
anega bat arto.
Talo esnea jateko
gaude zaleturik;
sei talo jango nituzke
neronek bakarrik.

Gaur bai jana ta edana
labiru lana,
jango ote degu dena
labiru la. E/up!