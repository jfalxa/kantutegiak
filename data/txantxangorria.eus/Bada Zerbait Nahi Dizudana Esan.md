---
id: tx-2409
izenburua: Bada Zerbait Nahi Dizudana Esan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pcX1mIu64BY
---

So egin nazazu begietan
geldi geldirik eta 
ea asmatzen duzun zer dudan
zuri esateko
tira!
ez da horren kontu zaila
ezaguna duzu begiradan
ondotxo dakizula
nora eraman nahi zaitudan
txinpartak darabiltzagu airean.

Bada zerbait nahi dizudana esan
ez da gero sekretua.

Heldu nazazu eskuetatik
neure parean geldirik
zure arnasa nirearekin batera
bi bihotz estu taupaka
askatu korapilo hau
bat bateko laztan batekin.

Bada zerbait nahi dizudana esan
ez da gero sekretua.
Bada zerbait nahi dizudana esan
ez da gero bekatua
Gaua dugun gure lagun
aukera hau gal ez dezagun
piztu dezagun amodioaren sua

Bada zerbait nahi dizudana esan
ez da gero sekretua.
Bada zerbait nahi dizudana esan
ez da gero bekatua