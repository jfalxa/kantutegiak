---
id: tx-3119
izenburua: Hartu Nahi Dut Arnas Gipuzkoa Zero Zabor
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eBvk8e9LgRM
---

Oh-oh-oh-oh-oooh! hartu nahi dut arnas (bis)
Ederra ah aaah! Broma broma da ba kontainer aurrean hartzazu ba arnas (bis)
Plastikoa dut sudur zulotan, papel albala berriz enpastetan, begietan, ke beltza begietan
Ez nau halare itsutuko, atez ate bilatu ta azkenean ikusi det, argiya ikusi det.
Lanean nahi zaitut, etxean behar zaitut, zero zabor, nire arnas…
Orain bai ongi alde ederrean guztiok batera hartuaz arnas oh oh oh… Orain bai ongi alde ederrean guztiok batera hartuaz arnas
Oh-oh-oooh-oh-oh! hartu nahi dut arnas (bis)
Ederra ah ah-aaah! broma broma da ba kontainer aurrean hartzazu ba arnas Ua ua ua…
Iritsi da, hau da momentua, elkar gaitezen, berandu baino lehen, iritsi da, hau da momentua, erabaki, ez pasatzen utzi, ez pasatzen utzi, berandu baino lehen, hartu nahi dut arnas, hartu nahi dut arnas
Oh-oh-oh-oh-oooh! hartu nahi dut arnas (bis)
Orain bai ongi alde ederrean guztiok batera hartuaz arnas oh oh oh oh orain bai ongi alde ederrean guztiok batera hartuaz arnas
Oh-oh-oooh-oh-oh! hartu nahi dut arnas (bis)
Ederra ah ah-aaah! broma broma da ba Gipuzkoa garbi, errausketarik EZ!