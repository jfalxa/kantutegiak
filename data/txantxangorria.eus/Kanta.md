---
id: tx-2707
izenburua: Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zyKcTUOEShw
---

Kanta hizkuntzaren hatsa
kanta gure hitzen gatza
kanta “Guk euskaraz” ereman urratsa
kanta, kanta, kanta.

Kanta “poeten arima”
kanta arbasoen garima
kanta eten ez den doinuaren errima
kanta, kanta, kanta.

Kanta gogotik kanta!

kanta nork bere herria
kanta ikasi berria
kanta sustengatzen gaituen giltzarria
kanta, kanta, kanta.

Kanta bipil edo ezti
kanta goiz ala astiri
kanta izaitearen zaurien goxagarri
kanta, kanta, kanta.

Kanta gogotik kanta!

Kanta bizia garaile
kanta geroan ereile
kanta eraikitzen ari denaren alde
kanta, kanta, kanta.

Kanta gure erroak hegal
kanta gure mugak estal
kanta gurean berme bester esku zabal 
kanta, kanta, kanta. (hiru)