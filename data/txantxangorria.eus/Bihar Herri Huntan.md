---
id: tx-799
izenburua: Bihar Herri Huntan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mqCadNkzE4E
---

Bihar Herri huntan zorion eta bake

Herritar guziendako!

Bihar Herri huntan denentzat bozkario

Denek bizi behar baitute!

 

Ezagutzen ditugu bertzetaz baliatzen direnak

Bertzen izerdiz dituzte egiten aise ontasunak

Bainan lanik gabe dago hortxe gure auzo lehena

Barne oro ehailia: bihotz barneko iluna.

 

Gure herri askotan berexgoa sartu nagusi

Kanpotik ta barnetik batzuk daukute dena hautsi!

Jaunxkila batzu lan hortan ari baititugu ikusi

Herritarrak xutitzean joan dirare ihesi.

 

Lanbideak moztu nahiz baderamate ele zuri

Laguntzak oro emanez beren kideko horieri.

Bainan azkenez jendea ohartzen joko hits horreri

Lantegiak berak sortuz esku emanez elgarri.

 

Guri hurbil hurbilean zabaltzen da mundu nasaia

Horren zoriontzen gaiten guziak bihotzez lehia:

Gure haurrek eta gaztek eder ukan dezen bizia...

Gizonaren helburua hortan sartzen da guzia...

 

 

      1978 ekoa. Kanta-Bi ikusgarriarentzat egina, bururatzean emaiteko. Bakea behar dugula, bai, bainan nolakoa? Eta noren aldekoa?

      Ahantzi gabe, askotan, barnetik ere, ari zaukula gerla sortzen «bake ustela». Egiazko bakeari oihu bat denen onetan.