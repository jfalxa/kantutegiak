---
id: tx-3018
izenburua: Iji Taja
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QH1wl3CoP-Q
---

Iji taja, atorrak gaur Mundakan
berdinik ez da mundu zabalean
gazte ta zar, marrau jantzu gura dogu
denok aratustetan.

Hator neskato aratuzte egunean,
atorrak gabiltz hemen goiko kalean,
goizetik kanta, iluntzez dantza,
gero zirriak gauerantza.

Kontuz gero, jausi barik portura,
legorrean atrapa lei takarta;
itto orduko hartu neska jatorrena
ta joan tala-goira.