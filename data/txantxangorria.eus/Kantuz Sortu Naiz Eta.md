---
id: tx-1370
izenburua: Kantuz Sortu Naiz Eta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OONk8TmgS2I
---

Kantuz sortu naiz eta kantuz nahi bizi
Kantuz igortzen ditut nik penak ihesi
Kantuz izan dudana zerbait irabazi
Kantuz gostura ditut guziak iretsi
Kantuz ez duta beraz hiltzea merezi.

Kantuz iragan ditut gau eta egunak
Kantuz ekarri ditut grinak eta lanak
Kantuz biltzen nintuen aldeko lagunak
Kantuz eman dauztate obra gabe famak
Kantuz hartuko nauia zeruko jaun onak.

Kantuz ehortz nezate hiltzen naizenian
Kantuz ene lagunek harturik airean
Kantuz ariko zaizkit lurrean sartzean
Kantu frango utziko diotet munduian
Kantuz har diten beti nitaz orroitzean.