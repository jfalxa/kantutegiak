---
id: tx-2908
izenburua: Hamaika Lehoi Borrokan Txipli Txapla Otxotea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pPm03BacLIQ
---

Arkuaren itzalean
Hamaika lehoi borrokan
Zintzoki ohiuz gartuaz
Armailak sutan.

Herria dugu harrobi
Odola zuri ta gorri
Euskal Herria aberri
San Mames kabi.

Aurrera Athletic!
Inolaz ez etsi!
Ausarki lehia hadi!
Ta kiroltasunez irabazi!

Aupa Athletic
Gogor eutsi!
gora euskal gaztedi
Zuri gorri!

Gora Bilbo!
Gora Euskadi!
Aupa Athletic!

Katredaleko orroa
Zaharretik berrira doa
Etsaiak kikil du bitza zelai berria
Hamaika lehoien grinaz,
mila zalen hats biziak
Betiko filosofiaz
Tinko eutsiaz