---
id: tx-1619
izenburua: Euriak Atertu Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4CaPjuM1064
---

Euriak atertu du, euriak atertu du 
nire sabaitik jausten, bere itzala aurkitu dut. 
Desira agortu dut, desira agortu dut 
kale iskin batean, barrenak hustu ditut. 
Eta nire arropen barnean, hezur pila bat soinean 
haragia agortu nuen, urrutiko etxe batean. 
Elikatu naiz berriro, nire penak zugan itoz 
etor zaitezke berriz nigana, ahaztuz galderak betiko.