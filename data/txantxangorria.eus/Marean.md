---
id: tx-923
izenburua: Marean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1nYF5XrFAFE
---

Atlantikaldiarekin elkarlanean sorturiko kantua, 2019ko ediziorako.
Canción creada para la edición 2019 del Atlantikaldia Festibala.

Musika: Nøgen
Hitzak: Beñat Gaztelumendi

Haritz Harreguy-rekin grabatua eta Ultramarinos-en masterizatua.

----------------------------  LETRA  ----------------------------

Oinutsik sartu nintzen
marea bizitan
orain Atlantikoa
daukat haragitan.
Korronteek urpera
sakon naramate
hitz berriek birikak
bete dizkidate.

Tanta naiz itsaso betean,
itsaso bete daukat tanta bakoitzean
azala urtzean
berriz janzten direnak.

Natorrenean banoa
Eta noanean nator
Ni marean
Bizi naizelako
Birikak eta airea
Nire barruan dauzkat
Baina
Ur azpira
Noa arnasteko

Kanta naiz itsaso betean,
itsaso bete daukat kanta bakoitzean
azala urtzean
nahasten direnak mareekin batera.

Natorrenean banoa
Eta noanean nator
Ni marean
Bizi naizelako
Birikak eta airea
Nire barruan dauzkat
Baina
Ur azpira
Noa arnasteko