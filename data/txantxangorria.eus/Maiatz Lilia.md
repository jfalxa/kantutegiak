---
id: tx-507
izenburua: Maiatz Lilia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yxXpw3qNXo8
---

Maiatz-lilia erradak norat juan diren elurrak

 Othoi erradak nun diren dehengo nexkatx ederrak

 Ote-lilia erradak nun diren lagun onak

 Norat juan zaizkidan aments arinak

 Nun othe diren lagunen arimak.

 

 Maiatz-lilia erradak norat juan diren elurrak

 Othoi erradak nun diren lehengo nexkatx ederrak

 Izar urdin hik erradak nun diren irrintzinak

 Eustakioren abesti dolamenak

 Nun othe diren lagunen arimak.

 

 Maiatz-lilia erradak norat juan diren elurrak

 Othoi erradak nun diren lehengo irri gotorrak

 Haizia hik erranzkidak mendien auheiminak

 Eustakioren herresto peregrinak

 Nun othe diren lagunen arimak.

 

 Maiatz-lilia erradak norat juan diren elurrak

 Othoi erradak nun diren oi lehengo ortzadarrak

 Algorta errak Algorta haren hatsbehapena

 Nigarrak odolez behatu huena

 Errak norat iruan diok arima.