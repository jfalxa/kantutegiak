---
id: tx-885
izenburua: Surikatoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PkbOqhYquWI
---

" Hotz egin duen arren" lan berriko "Surikatoak" abestiaren bideoklipa.

Hitzak eta musika: Belatz

Grabaketak eta edizioa: Ibi Life

Beti begire
Ta hantxe isilik
Zetan gabizen , ulertu ezinik
Euren taldien ondo sarturik
Surikatoak bakarrik!

Eguzkixek urten deu eta badatoz gora
Geurkoan be ez dabe kontuten hartu
Presa handixena …

Danak alkartu eta hasi dire txutxumutxuke
Zeren truke?
Ondotik paseu gara
Kitarra elegantie!

Beti begire
Ta hantxe isilik
Zetan gabizen , ulertu ezinik
Euren taldien ondo sarturik
Surikatoak bakarrik!

Buelta eta buelta!
Zer ein jakin barik
Egune luzie da,
Geur ez dago saltsarik!
Oihaneko errege sentitzen zarie
Konturetu badau eta
Animali handixaurik

Beti begire
Ta hantxe isilik
Zetan gabizen , ulertu ezinik
Euren taldien ondo sarturik
Surikatoak bakarrik!