---
id: tx-1595
izenburua: Fenneh Lurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UUoaq2T4YEE
---

Begiratu itsasoa nire begien aurka lehertzen.

Malkoek alderantziz egiten dute bidea,

Atzera iturbururantz.

 

Hartu nire tristura

Orain zurea ere bada.

 

Memoria hauskorrenaren hegian

batzen zaizkit

nire zauriak eta orbanak azal bakarrean.