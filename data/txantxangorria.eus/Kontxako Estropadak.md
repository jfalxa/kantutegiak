---
id: tx-3354
izenburua: Kontxako Estropadak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PRWRafs-YeM
---

Kaiarriba-Donostiarra arraun elkarteak donostiako estropaden abestia egin du musikari aunitzen laguntzaz. Laurobak egin du musika eta Unai Elizasuk letra, eta hainbat abeslarik hartu dute parte: Leire Martinez (la oreja de van gogh), Asier Gozategi (gozategi) eta Xabi Solano (esne beltza). Hortaz gain musikari hauek ere kolaboratu dute: Xabi Solano (trikitixa eta panderoa), Fredi Pelaez (hammonda) eta Oier Zeberio (saxofoia).

Letra:
Zer gertatzen da irailaren 
lehenengo bi igandeetan,
Zenbat kolore Donostiako
badia eta kaleetan.

Kontxan, arraun hotsa
boga boga mutil ta neska
arraunzale denak, gozatzen dutenak
Donostian arraun festa.

Paladaz palada, uberaz ubera
arraunari tira, guztiok batera,
bandera,
ederrena da bera,
Donostiako estropaden
perla zu zera.

Arraunlariak irteten dira
gogo biziz indarrean,
Giroa piztuz Urgullen, irlan
portuan, alde zaharrean

Ranpara, iritsi eta hara
zenbat zale ta lagun,
bandera bakarrak astintzen du baina...
den-denak gara txapeldun.

Bandera,
ederrena da bera,
Donostiako estropaden
perla zu zera.

Paladaz palada, uberaz ubera
arraunari tira, guztiok batera,
bandera,
ederrena da bera,
Donostiako estropaden
perla zu zera.

Kon... kontxako estropada
kon... kontxako estropada