---
id: tx-2757
izenburua: Bardeetatik Kanpora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A8JSvx1Xld0
---

Zeruan zehar askotan negar 
ibiltzen da eguzkia 
gaueko izarren distira itzali da 
egunean bere edertasuna lur hauen isiltasun lasaia 
egun emankorrak 
hareak eraikitako gaztelua 
Nafarroako Erriberan 
Bardeetatik kanpora, Bardeetatik kanpora 
militarrak Bardeetatik kanpora !! 
Bardeetatik kanpora, Bardeetatik kanpora 
utzi behingoz bakean gure bideak !! 
Bardeetatik kanpora, Bardeetatik kanpora 
utzi (e)zazue libre gure zerua !! 
Bardeetatik kanpora, Bardeetatik kanpora 
Gure izarrak garrasika 
bai egun bai gauean 
atzo piztutako sutea itzali da 
Erribera ahots goran 
animali (e)ta loreak 
oihuka (e)ta kexaka 
hareak eraikitako gaztelua 
Nafarroako Erriberan 
Bardeetatik kanpora, Bardeetatik kanpora 
militarrak Bardeetatik kanpora !! 
Bardeetatik kanpora, Bardeetatik kanpora 
utzi behingoz bakean gure bideak !! 
Bardeetatik kanpora, Bardeetatik kanpora 
utzi (e)zazue libre gure zerua !! 
Bardeetatik kanpora, Bardeetatik kanpora