---
id: tx-3125
izenburua: España Laztana Bukatu Da Geurea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0X9ShUi-M_k
---

Etxarri Aranatzen auzolanean egindako bideoklipa

Zenbat hitz ta zenbat gau
Muga eta arau
Dibortzio baten historia da hau
Zure harrokeriak, zure zelo gehiegiak
Zure arnasak ito egiten nau.
Non da zure garra, zure grina
Brasiletik ez zara berdina
Itzali zen pianoa, gurea ez da sanoa
Borboia joan zen, ni ere banoa…
Hara:
Belauniko nahi nauzu ezin
Zutituta
Zuri helduta nagoenero
Nago zatituta.

Ai España, laztana, bukatu da gurea
Ai España, laztana, bukatu da gurea

Ondora etorri zinen
Zezen baten gisan
Ukatzerik ez nuen izan…
Zer dudan ta nora noan
Hitzean eta larruan
Diktadoretxo bat duzu barruan.
Hain gara bateraezinak
Ni hotz, zu irakiten
Ez dugu hizkuntza bera_egiten
Ni ez naiz zure lorea
Altzaria, folklorea
Beti izan zara konkistadorea.

Belauniko nahi nauzu ezin
Zutituta
Zuri helduta nagoenero
Nago zatituta
Ai España, laztana, bukatu da gurea
Jereza zaizu mikaztu
Ai España, laztana, bukatu da gurea
Ez nire gauzarik nahastu
Ai España, laztana, bukatu da gurea
Zakurra etzazu ahaztu…
Ai España, laztana, bukatu da gurea.

-tu què en penses, Mali?
-home, jo crec que Espanya no va bien
-que no nos va bien
bai, bai, guk hemen ez dugu nahi haien trenik haien zezenik
Zuzenbide okerrik eta okerbide zuzenik
Zentsura ozenik eta inposatutako izenik
tenim un dèficit d'identitat
-no rima
-però és veritat

Zuk ere badakizu
Gurea bukatu dela
Arren eskatzen dizut
Etxetik joan zaitezela
Hartu gauzak
Hustu gela
Ezin zen segi horrela…
Etzazunegarrik egin.
Mesillan dauzkazu zure
Senideen argazkiak
Galindo, X Jauna
Franco ta beste guztiak
Osborneko zezenak ta
Lentejueladun piztiak
Behinola izan zinen horren azken
Izpiak.

Ai España, laztana, bukatu da gurea
Jereza zaizu mikaztu
Ai España, laztana, bukatu da gurea
Ez nire gauzarik nahastu
Ai España, laztana, bukatu da gurea
Zakurra etzazu ahaztu…
Ai España, laztana, bukatu da gurea.

Eraman zure konkistak, kuadrilako falangistak,
Manolo Escobarren diskak
Agur, agur betiko!
Nahi dut etxe bat librea
Bakoitzak bere bidea
Zuk zurea, nik nirea
Lagunak eta kito.

Ai España, laztana,
Ai España, laztana,
Ai España, laztana,
 Ai España, laztana,bukatu da gurea
Ai España, laztana,
Ai España, laztana,
Ai España, laztana,bukatu da gurea.