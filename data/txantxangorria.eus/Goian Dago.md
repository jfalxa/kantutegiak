---
id: tx-2539
izenburua: Goian Dago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fn_1WqcRefE
---

Goian dago lainoa,
behean dago otsoa.
Goian dago mendia,
behean dago zubia.
Goian dago txoria,
behean dago lorea.
Goian dago eguzkia,
behean dago etxea.