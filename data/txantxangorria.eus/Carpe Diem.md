---
id: tx-1736
izenburua: Carpe Diem
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N9pC6lfSohQ
---

Oraina iraganaren ondorena
gerora iristeko beharrezkoa
askotan bukatzea nahi
eta bestetan bukatzea nahi gabe 
alde egiten duena.

Iragana orainaren zain dagoena
tristurak eta pozak
ekartzen dizkiguna
tristurak eta pozak
oroimenak gogoan
tristurak eta pozak
oroimenak iraganean
orainaren zain dago.
Geroa beti beldurtzen gaituena
ezer jakin gabe
misterioa
proiektuz betea
desioz gainezka
denborarekin oraina bihurtuko dena.

Carpe Diem oihukatu
atzera ez begiratu
Carpe Diem oihukatu
denetaz ahaztu
momentua bizitu.