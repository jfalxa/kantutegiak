---
id: tx-286
izenburua: Aita Gurea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MxrjxtMTnWE
---

Aita Gurea,
izarretan lo hagoena,
goraipa bedi hire izena,
iraun beza hire leinuak,
gera bekigu hire arnasa,
gogoan bezala, ondoan ere.

Gure eguneko kemena
igukegu,
etsaiak gorrota ditzagun,
haiek gorrotatzen gaituzten heinean.

Aita Gurea,
izarretan lo hagoena,
goraipa bedi hire izena,
iraun beza hire leinuak,
gera bekigu hire arnasa,
gogoan bezala, ondoan ere.

Ez baitzat suntsitzen utz,
baina babes gaitzetik.
Ezen hirea duk,
gizatasuna,
prestutasuna,
etorkizuna,
hemen,
amen.

Aita Gurea,
izarretan lo hagoena,
goraipa bedi hire izena,
iraun beza hire leinuak,
gera bekigu hire arnasa,
gogoan bezala, ondoan ere.