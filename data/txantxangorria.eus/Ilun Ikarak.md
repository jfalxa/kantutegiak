---
id: tx-2099
izenburua: Ilun Ikarak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wZ9NyNakVV8
---

Ilun-ikarak azken argia
uxatu dizu urrutira,
zeru-sapaian izar bakanak
itzal etzanei begira,
ilun-txintako sorgin-usaiak
espazioan dirdira.(Bis)

Mendi-hegitan argi-oroitza
eguzkiaren keinua,
baso beltzean hosto-firfira
gauezkoen erreinua.

Itsas-txoriak txio-txioka
harat-honako haizetan,
berde argien edertasuna
more motelduz uretan,
txalupa geldi kulunkarien
isladarekin hizketan.(Bis)

Parke ixileko banku txuria
ezpain beroen lankide,
antsi zaharrak zantzu berritan
bihotz gordinen senide,
tristura hegan paseatzen da
arratsaren solaskide.(Bis)

Gau-ezkilaren oihartzun hotsa
irrixtatzen da kolokan,
zirrrara mutu baten gisara
maitearen soinekoan,
bakardadea sekretuekin
korapiltzen den zokoan.(Bis)

Itxas-aideak gesalez dakar
murmuriozko solasa,
suspiriotan ezin jarioz
pena lurrinduen hatsa,
haize boladak txikordatzen du
maiteminduen jolasa.(Bis)