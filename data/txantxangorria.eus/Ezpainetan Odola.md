---
id: tx-872
izenburua: Ezpainetan Odola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dgk3y1GLMxU
---

Skakeitan - "Ezpainetan Odola"
Skakeitanen "Nola Galdu Denbora" (2019, Mauka Musikagintza) diskoko lehen singlea.


Bideoa: Arriguri // Jon Ander Urresti, Asier Renteria, Aitane Goñi Landa, Hasier Goikolea, Xabi Ojinaga Juarez eta Ander Mateos Totxo.

Aktoreak: Maider Rezabal, Yaiza Goikoetxea Luno, Alan Porolan Witxita, Xabier Gutiez, Aitor Marcos

Eskerrik asko: Naia Ormaza, Izaro Baza, Jon Ander Urresti, Xabier Batis, Aitor Duo, Amaia Zapiain Egaña, Yaiza Goikoetxea Luno, Unai Olaeta, Aitor Marcos, Javi West, Gaizka Camara Barrenetxea, Xabier Gutiez, Unai Pertika, Beñat Goitia Arketa, Xanti Agirrezabala


[EUS] Hitzak: 
Izoztuta, geldi zaude sabaiari begira
dantzan aurreikusten zen panorama.
Izerditan bizi ala hil, ametsgaizto okerrenak,
iltzeak gure gorputz profanoetan.

BABESIK GABE GAUDE 
LURRA BESARKATZEN 
EZPAINETAN ODOLA
TA ILUSIOEK, BARREZ TA NEGARREZ
GALDU DUTE KONTROLA

Ni ta nire zirkunstantziak, amnesiak jota esnatu
buruaren ziega isolatuenean.
Drama zelai erraldoi bat sortu du azken batean
zoriontsu izan beharraren lemak.