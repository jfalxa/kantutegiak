---
id: tx-422
izenburua: Auzo Trenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pHIMr55zwtw
---

DO SOL DO SOL
 Lan egun bat, azken orduak,
DO SOL LAm RE
 bagoia erdi hutsik doa.
DO SOL LAm SOL
 Haren begiak, ezer gehiago ez balego,
DO SOL LAm RE
 ni ixilik, berari begira.
DO SOL DO SOL
 Hor kanpoan, denda argiztatuak.
DO SOL LAm RE
 Erdialdetik auzo trenean noa.
SIm SOL SIm SOL
 Ni ixilik, haren begiak,
SIm DO LAm RE
 tren horretan, nigana begira.
MIm RE MIm RE
Nora joango da? Bakarrik ote da?
MIm RE DO RE
 Bihar ere ikusiko al dut?
DO SOL DO SOL
 Nongoa naizen esango banio...
DO SOL LAm RE
 Erakutsi ezkero etxekoen fotoak.
DO SOL DO SOL
 Zenbat galdera erantzuteko naizen,
DO SOL LAm RE
 zenbat gauza esateko ditudan.
Ni ixilik…
MIm RE MIm RE
Gizon legala naiz, nik ere banuen lan bat.
MIm RE DO RE
 Erdialdetik auzo trenean bakarrik noa.