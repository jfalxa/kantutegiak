---
id: tx-79
izenburua: Lorategi Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FamfsPxdgGo
---

LORATEGI BATEAN
                                                               Egilea: Mikel “Txistulito”

Egun baten gazte nintzela
lorategi polit batean
amodioz beteta
ta nik hura ikusten ia zoratuta.

Salto saltoka loretik lorera
musuak banatzen eta maitasuna
larrosa klabelin gorri ta Lilia
poliki poliki loratzen dira.

Ai ene joan den denbora ederra
lorategi beti bezain polita
eta ni lelo lelo ezkondu gabe
ai ene eta loreak kasurik ez.

Eta loreak kasurik ez.