---
id: tx-1704
izenburua: Izarapean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lX_-FAAvByc
---

Izar azpian mundua dago
izarapean zerua
zure laztanen haize hegoan
galtzen da nire burua.

Errekastoak ur murmurio
itsaso handiak orru
zentzun guztien zoramenera
muxuz-muxu, larruz-larru.

Kimuarena ote lorea
ta fruitua lorearen?
elkar urtuaz jakin nahi dugu
ni zu naizen, zu ni zaren.

Su gar harroa iraungitzean
ilintiak gori dirau
ase ondoko lotan gautzala
ailedi betirako gau !

Izar azpian mundua dago
izarapean zerua
zure laztanen haize hegoan
galtzen da nire burua.

Kimuarena ote lorea
ta fruitua lorearen?
elkar urtuaz jakin nahi dugu
ni zu naizen, zu ni zaren.

Su gar harroa iraungitzean
ilintiak gori dirau
ase ondoko lotan gautzala
ailedi betirako gau !

(Joxerra Garzia - Karlos Jimenez)