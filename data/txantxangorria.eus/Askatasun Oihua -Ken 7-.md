---
id: tx-3272
izenburua: Askatasun Oihua -Ken 7-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N7aXj2Bpqww
---

ASKATASUN OIHUA - KEN ZAZPI 

Askatasun oihu bat 
Olatu bat bezala 
Entzun da Kantauri itsasoan 

Lur zatitu honetan 
Itxaropen doinu hau 
Hegalean zabalduz goaz 
Kimu berriak landatuz 
Bide zail luze honetan 

Aurrera goaz 
Ta gaua 
Argitzen doa 

Mila aldiz 
Hesien artetik 
Maitea mila aldiz 
Etxera itzuli naiz zu barik 
Badakit 
Iritsi da eguna 
Gure etorkizuna 
Askatasuna 

Batera iraun dugu 
Ekaitz bortitzenetan 
Berunezko garai latzetan 
Gure ilusioekin 
Gure akats guztiekin 
Azken arnasera arte libre 

Itxaropen hitz hauek 
Eskeini nahi dizkizut 
Bi esku xume mindu hokin 
Garena babestera 
Behartutakoekin 
Laztantzen dakiten berekin 

Bidean 
Aurrera goaz 
Ta gaua 
Argitzen doa 

Mila aldiz 
Hesien artetik 
Maitea mila aldiz 
Etxera itzuli naiz zu barik 
Jadanik 
Ez dut nahi agurrik 
Iritsi da eguna 
Askatasuna 

Etxean behar zaitugu 
Gurekin behar zaitugu 

Mila aldiz 
Hesien artetik 
Maitea mila aldiz 
Etxera itzuli naiz zu barik 
Jadanik 
Ez dut nahi agurrik 
Iritsi da eguna 
Askatasuna