---
id: tx-2365
izenburua: Hemeretzi Primadera Rock
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qVMf84txto8
---

Créditos del disco "La piel transparente" (Iratxe Mugire, 2016) 

Iratxe Mugire: música y letra de todos los temas; guitarra, voz y percusiones; producción artística y ejecutiva. 
Pedro de la Osa: arreglos de todos los temas, música en “Nana del despertar”; guitarra, bajo, mandolina, arpa de boca y percusiones; producción artística. 
Josu Korkostegi: batería en “Hemeretzi primadera rock”. 
Xabi Fernández: bajo en “Hemeretzi primadera rock”. 
Ibon Larruzea (estudios Euridia): grabación, mezcla y masterización. 



HEMERETZI PRIMADERA 

Hemeretzi urtetako tristura berriekin 
deskubritzen ze zoragarria 
ta asimilatzen ze zaila 
ta asimilatu ezean ze tristea den hau guztia 

Diario berri bat hasi gura dut, 
aurreko ezerekin zerikusirik ez duena, 
zu barik adibidez 

Ismaelen doinuak, zaurietan hatzamarra; 
barriro abestu gurot, barriro zure itzala 
sasieguzkitsu diren urtarril egunetan, 
hilabete askoren ostean 
gure munduaren katastrofearen, 
nere hitz egokien eskemaren bila, 
zure erbestea, zure hautsa bilatuz 
beste logelatan 

Diario berri bat hasi gura dut, 
aurreko hari guztiak 
lotzen dizkidana 

Aurtengo primadera 
ez da aurrekoa bezala(koa), 
barriro ez dago gerrarik, 
ez dute egunkaririk itxi 
ta hondartzak garbi daude 
beti-beti moduan 
Aurten ez zara etorriko Bilbora bisitan 
ta zelakok diren gauzek, 
eninduzun topatuko han 

Diario berri bat hasi gura dut, 
gori baina ehortziak 
zure kontraesanak 

Aurtengo primaderan 
elurrak estaltzen du Regenta, katedrala, 
ta ni nere leihotik 
hemeretzi urtetako tristura berriak 
engainatzen ikasten 
Hemeretzi primadera, 
zioen kopla zahar horrek, 
hemeretzi primadera 

Hemeretzi urtetako tristura berriekin 
deskubritzen ze zoragarria ta asimilatzen ze zaila 
ta asimilatu ezean ze tristea den hau guztia 

Diario berri bat hasi gura dut, 
ez dut barkamenik nahi, 
ez dut barkamenik behar 
ez bada nirea neure buruari, 
barkamenik behar... 
ez dut...