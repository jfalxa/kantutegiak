---
id: tx-3215
izenburua: Gurea Da -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tueW-0c47Q4
---

Gurea da, da gurea,
gurea da, da gurea,
gurea da, da gurea,
gurea da, da, da, 
Euskal Herria
gurea da ta gurea da,
gurea da ta gurea da,
gurea da ta gurea da,
gurea da ta
Euskal Herria
Gurea da, da gurea,
gurea da, da gurea,
gurea da, da gurea,
gurea da, da, da, 
Euskal Herria