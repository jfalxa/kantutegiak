---
id: tx-2485
izenburua: Iruñeko Ferietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iZA5xQp2IE8
---

IRUÑEKO FERIETAN

(J. Oxalde Etxezuri. Bidarrai)

 
 
 
1/Iruñeko ferietan
iragan Sanferminetan
ehun zaldi arribatu 
Andaluziatik tropan
merkatu eder bat zautan,
zaudelarik bi lerrotan.

         ♪♪♪♪♪♪

2/Bat zen pikarta-xuria:
hartan bota dut begia.
Andaluz batek egin daut
bi untz-urreren galdia.
Eskain orduko erdia,
-Hartzak, hire duk zaldia.

         ♪♪♪♪♪♪

3/Han nintzen harrapatua, 
aginduaz dolutua;
urruntzeko izan banu 
hirur arditen lekua
gizonez inguratua,
iduri preso hartua. 

         ♪♪♪♪♪♪

4/Zeruko Jainko Jaun ona 
Zer ekarri nau ni hona?
Andaluz bat zazpi urtez
presondegian egona,
Laburzki mintzo zautana:
-Konda niri hitzemana.

         ♪♪♪♪♪♪

5/-Jauna, nahi dut pagatu,
bainan lehenik miatu.
-Zaldiaren miatzeko
denborarikan ez duzu,
lehenik soma kondazazu, 
gero miaturen duzu.

         ♪♪♪♪♪♪

6/Iruñeko ferietan 
iragan Sanferminetan.