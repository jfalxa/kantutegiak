---
id: tx-3000
izenburua: Haika Mutil
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qDn3Bmqrp8s
---

"Haika mutil, jeiki hadi
argia den mira hadi."
- Bai, nausia, argia da,
gur oilarra kanpoan da.

"Haika mutil, jeiki hadi
uria den mira hadi."
- Bai, nausia, uria da,
gure orra bustia da.

"Haika mutil, jeiki hadi
surik baden mira hadi."
- Bai, nausia, sua bada,
gur gatoa beroa da.

"Haika mutil, jeiki hadi
hortxe zer den mira hadi."
- Bai, nausia, haizea da,
gur lehioa ideki da.

"Haika mutil, jeiki hadi
kanpoan zer den mira hadi."
- Bai, nausia, edurra da,
lurra xuriz estali da.

"Haika mutil, jeiki hadi
urean zer den mira hadi."
- Bai, nausia, ardia da,
aspaldian itoa da.

"Haika mutil, jeiki hadi
zer oinon den mira hadi."
- Bai, nausia, egia da,
mutiltto hau unatu da.