---
id: tx-1088
izenburua: Irabazi Dugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CYcuzkvJ00k
---

Irabazi, irabazi dugu,
odolez ta malkoz irabazi dugu.
Irabazi, irabazi dugu,
burdinen atzetik irabazi dugu.
Hi, Gudari, Herriko zaindari,
Gernikako sua begietan argi...
hau loria, Burgosko auzia,
espatari nausitu zaio Herria.

Ekin, bidean ez jarri, bideari ekin.
Dator, odoletan dator argi-hastea.
Mugarik gabeko ludi zabal bat dugu nahi.
Armada suntsituz, urrea bere bai,
askatasuna dugu nahi.
Euskadi berri bat dago zure zai.

Irabazi, irabazi dugu,
odolez ta malkoz irabazi dugu.
Irabazi, irabazi dugu,
odolez ta malkoz irabazi dugu.
Hi, Gudari, Herriko zaindari,
Gernikako sua begietan argi...
hau loria, Burgosko auzia,
espatari nausitu zaio Herria.

Bultza, bidean ez jarri, izerditan bultza,
bultza, eskuak odolduz, bultza ta bultza.

Gure lurrazpitik hilek irabazi dute.
Txakurtegietan, oinazetan
deiadarka dabiltzatenok
Aberri gazte bat sortu digute

Irabazi, irabazi dugu,
burdinen atzetik irabazi dugu.
Irabazi, irabazi dugu,
odolez ta malkoz irabazi dugu.
Irabazi, irabazi dugu,
odolez ta malkoz irabazi dugu.
Irabazi, irabazi dugu,
burdinen atzetik irabazi dugu.