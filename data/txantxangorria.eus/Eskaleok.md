---
id: tx-386
izenburua: Eskaleok
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zy16FQ4IYjs
---

Gorputz arrotzen jabe
Nola izan daiteke
Orri txuri batean
Idatzita dago hainbeste
Azala sentitzen dut
Zure eskuen eske
Eskalea ez banintza iada
Bihurtuko nintzateke
Ikusterik behar ez duen
Begien umela
Erretzetik izoztera
Begirada bat magalera
Biluztu nazazu arretaz
Aurkituko duzu eta
Zure izena idatzita
Nire bular beko azalean
Zure faltaren mina
Tiraka ta zatiezina
Utzidazu esaten
Egia dela gure egarria
Ta notarik gabe
Abestuko dizut gauez
Udako ilargi beroetan
Itxaron zaitzaket
Madisonen zubietan
Zure faltaren mina
Tiraka ta zatiezina
Utzidazu esaten
Egia dela gure egarria
Ta notarik gabe
Abestuko dizut gauez
Udako ilargi beroetan, 
udako ilargi beroetan 
udako ilargi beroetan
Itxaron zaitzaket
Madisonen zubietan