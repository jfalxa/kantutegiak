---
id: tx-1560
izenburua: Amets Egiteko Leihoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9GwlPv0YCw8
---

Musika: Xabier Zabala

Bokale eta Baiona.
Ange.lu ondoren.
Miarrtz, Bidarte,
Getaria baino lehen,
Donibane. Lohizune,
Ziburu. Urruña.
Hendaian salto Hondarribira.
Pasaia. Donostia.

Igeldotik Oriora
aldapan beh.ra.
Zarautz. berriz Getarla,
Zumaia, ltziar, Deba.
Mutriku. Ondarroa,
Mendexa hortxe,
Lekeitio. Ea, Ibarrangelu,
hurrena Elantxobe.

Mundaka eta Bermeo,
Bakio, Lemoiz,
Barrikaren aurretik
Plentzlarekin Gorliz,
Sopela eta Getxo.
Portugalete,
Santurtzi, Zierbana, Muskiz. Aio,
aio.ikusi arte.
amets egiteko hamaika leiho,
itsasorako ate.