---
id: tx-2946
izenburua: Ni Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d1BaHZba4bY
---

Et Incarnatus, Eñaut elorrieta

Musika eta hitzak: Xabier Lete
Moldaketak/Arrangements: Juantxo Zeberio Etxetxipia
(recorded in Tolosa-Basque country /2012-1-27)




Conductor: Migel Zeberio Etxetxipia


Sound engineer: Angel Aguero
Recording engineer: Aurelio Martinez (Transforma audio)
Imagen: Antoni Pallares/Klara Oria

Ni naiz
erreka zikinen iturri garbiak
aurkitu nahi dituen poeta tristea.
Ni naiz
beste asko bezala neguko eguzkitan
hotzez hiltzen dagoen gizon bakartia.
Ni naiz
hostorik gabe gelditzen
ari den ardaska lehorran.
Ni naiz
pasio zahar guztiak kiskali
nahi dituen bihotz iheskorra.

Ez zaidazu galdetu gauza ilun
guztien arrazoi gordea,
nora ote dijoan denbora
aldakorra daraman bidea.
Daraman bidea.

Ni naiz
borrokaren erdian ilunpetan etsita
amur ematen duen pizti bildurtia.
Ni naiz
ezerezetik ihes munduaren erdian
ezin aurkitutako amets urrutia.
Ni naiz
irrifar bakoitzean gaztetasun
hondarrak galtzen dituena.
Ni naiz
itsasolo haizeak gogor astintzen duen
lainoen negarra.

Ez zaidazu galdetu gauza ilun
guztien arrazoi gordea,
nora ote dijoan denbora
aldakorra daraman bidea.
Daraman bidea.

Ez zaidazu galdetu gauza ilun
guztien arrazoi gordea,
nora ote dijoan denbora
aldakorra daraman bidea.
Daraman bidea.