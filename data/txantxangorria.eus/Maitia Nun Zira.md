---
id: tx-2855
izenburua: Maitia Nun Zira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XevF5eMzsQc
---

Maitia, nun zira?
Nik ez zaitut ikusten
ez berririk jakiten.
Nurat galdu zira? (bis)
 
 Hala kanbiatu da
zure deseina?
Hitz eman zenereitan
ez behin, bai berritan
enia zinela! (bis) 

                                                              Ohikua nauzu
                                                           ez nauzu kanbiatu
                                                         bihotzian beinin hartu
                                                             eta zu maitatu.(bis)

                                               Aita jeloskor batek dizu kausatu. 
Zure ikustetik
gehiago mintzatzetik
hark nauzu pribatu.(bis)

Aita jeloskorra!
Zuk alaba igorri
arauz ene ihesi
komentu hartara.(bis)
 
Are eta ez ahal da sarturen serora!
Fede bedera dugu
Alkarri eman tugu
Gauza segurra da.(bis)

Zamariz iganik
jin zauzkit ikustera
ene konsolatzera
aitaren ixilik. (bis)
 
Hogoi eta laur urte baditut beterik.
Urte baten burian
nik ez duket ordian
Aitaren axolik.(bis)

Alaba diener
erranen dit orori:
"So'gidaziet eni
beha ene erraner.(bis)
 
Gaztetto direlarik, untsa diziplina!
Handitu direnian
berant date ordian
nik badakit untsa".(bis)