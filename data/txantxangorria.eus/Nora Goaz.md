---
id: tx-2140
izenburua: Nora Goaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q0-uovob8Bs
---

'Errobi' ibaiak Iparraldea zeharkatzen du oso-osorik. Bere izenak dioen bezala, bi erro edo sorburu dauzkan ibaia da. 'Errobi' taldeak ere bi sorburu izan zituen: Anje Duhalde eta Michel Ducau. Musikoekin batera letragilea ere aipatu beharrekoa da: Daniel Landart. 1970an ezagutu zuen Duhaldek Landart, eta honek letra batzuk egin zizkion. Horiekin disko txiki bat atera zuen Duhaldek bere izenean, eta hor ezagutu zuen, bide batez, Duhaldek Ducau. Geroztik Errobiren kanta askotako letrak egin zituen Daniel Landartek. 1975ean atera zuten lehen diskoarekin, 'Errobi' deitu zutenarekin, euskal kantagintzako lehen rock taldea aurkeztu zuten gizartean.

'Ez dok amairu' desegina zen, eta kantari asko gitarra soil batekin zebilen kantaldietan; beste batzuk talde egoki bat osatzeko esperimentuak egiten. Errobik gogor jo zuen giro horretan. Ahots jokoak eginez, rock formazio klasikoa zen haiena, erritmodun kantak eta distorsioaren ertzean geratzen ziren gitarrak erabiliz.

1979an desegindu zen arte bost disko atera zituzten. Salduenak eta arraskatatsuenak 'Errobi' eta 'Bizi bizian' izan ziren. Jaialdi asko eman zituzten urte apur horietan, gehienak Hegoaldean, eta haien kanta asko berbena taldeen errepertorioan derrigorrezko bilakatu ziren, hala nola 'Telebista', 'Lantegiko hamar manamenduak' edo 'Gure lekukotasuna'. Belaunaldi batenzat modernitatearen himno bilakatu ziren beste hainbat kantu, 'Perttoli', 'Nora goaz' edo 'Gure zortea' esate baterako.

Taldea berriro berpizteko eginahal bat burutu zuten 1985ean, azken disko bat ateraz, 'Agur t'erdi', baina ez zuten arrakastarik izan. Ordurako beste musika joera batzuk zeuden gizartean.