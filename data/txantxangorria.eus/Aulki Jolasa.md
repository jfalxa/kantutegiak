---
id: tx-2935
izenburua: Aulki Jolasa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BHRrNWreAWM
---

Saltoka eta brinkoka korro korroan (bira-biraka),
den denak gabiltza aulkien ondoan.
Lalara, lalara…
Musika pozgarri hau isiltzen bada,
eseri behar duzu aulkitxo batean.
Lalara, lalara…
Guztiok aulkietan eseritzean,
nor geldituko da jokutik kanpora.
Lalara, lalara...