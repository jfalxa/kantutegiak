---
id: tx-1278
izenburua: Izan Puntu!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W7x-zHNEAAQ
---

Musika: Katxiporreta
Hitza: Ane Labaka
Bideoaren grabaketa eta edizioa: Isa F. de Agirre.



Armairutik kanpora
ahobizi
euskarazko munduan
nahi du bizi.

Belarriprest ondoan
adi-adi
oraina bihurtzeko
Euskaraldi

Kale bazterretan, parkeetan,
denda , eskola, tabernetan...
bihotz, belarri nahiz buruetan
baina batez ere mingainetan.
More, gorri, euforian
urdin hemen, hori han
elkarrekin dantzan
gure herrian.

Batu, mugitu...
Ta izan puntu!

Bihurtu ditzakegu, 
positibo,
azken puntuak puntu
suspentsibo.

Puntu batek beste bat
du jarraika
inoiz baino gehiago
da hamaika.

Kale bazterretan, parkeetan,
denda , eskola, tabernetan...
bihotz, belarri nahiz buruetan
baina batez ere mingainetan.
More, gorri, euforian
urdin hemen, hori han
elkarrekin dantzan
gure herrian.

Batu, mugitu...
Ta izan puntu!