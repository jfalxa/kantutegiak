---
id: tx-2971
izenburua: Oihu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/87Cfe14VMaA
---

Haien berri onak gure
berri txarrak direlako
haien ametsak guretzat
ametsgaizto direlako

Oihu egin nahi dut
arima urratu arte
ixiltasunaren ozenez
gure egiak ere
haienak bezainbeste
balio dezake

Haientzat berria dena
ahituta zaigulako
haien hitz politek beti
iraintzen gaituztelako

Oihu egin nahi dut
arima urratu arte
ixiltasunaren ozenez
gure egiak ere
haienak bezainbeste
balio dezake

Gurtzen ari diren jainkoetarik inork
ez gaituelako inolaz salbatuko
ulertezin zaien oinazetik:
libertade ezatik.