---
id: tx-1009
izenburua: Hartu Kontrola, Aukeratu Rola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UaBmZAkUiDg
---

Euskaraldia: 11 egun euskaraz ariketa sozialerako kanta. 

Egileak:
Maddalen Arzallus: hitzak
La Basu: hitzak, ahotsa
Ines Osinaga: musika, ahotsa
Las Tea Party dj's: musika

Bideoklipa: Xabi Hoo


Hartu kontrola, aukeratu rola,

esadazu nola, egin dezagun froga!

 

Mihiz mihi hemen nago ni goiz, arrats eta gau.

Ahoz aho narabilenak bizi ahala sortzen nau.

 

Hartu kontrola, aukeratu rola,

esadazu nola egin dezagun froga!

 

Aktibatu mingaina, hauxe da gure oraina

Euskaraldia prest Euskal Herrian baita.

Hizkuntza gure indarra, ez galdu zure iparra

jarraitu bidean tiraka ta tiraka.

Kalean ta etxean, herriko dendetan

jende guztia dau alaitasunez berbetan

herriko jaixetan, oholtza gainean

euskaraz bizi nahi dugu 11 egunetan!

 

Euskaraldia bertan da!

 

Ahobizi naiz ni, belarriprest adi

zabalduz mezu beti herriz herri,

buelta bat emanez betiko ohiturei

puntu izanez milaka milaka aldiz.

Behin eta berriz ez dau mugarik

aske izan baimen barik

asko, gutxi nahiz ezer ez jakin

disfrutau, gozatu jendearekin,

neska edo mutil, adiskideekin

irribarre, berba zatoz gurekin

bota belarrira mingainan puntatik.

 

Itxi ditzagun begiak eta

bi belarriak zabaldu,

nire ahotsak bihotz barruan

dudana esaten al du?

 

Euskaraldia bertan da!

 

Mihiz mihi hemen nago ni goiz, arrats eta gau.

Ahoz aho narabilenak bizi ahala sortzen nau (bis).

 

Mihiz mihi hemen nago ni, Euskaraldia bertan da!

Ahoz aho narabilenak, Euskaraldia bertan da!

 

Hartu kontrola, aukeratu nola, esadazu nola