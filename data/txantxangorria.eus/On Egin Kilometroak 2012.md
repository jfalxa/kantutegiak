---
id: tx-3153
izenburua: On Egin Kilometroak 2012
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XcepHYqmMIA
---

Euskara jalgi hadi mundu guztira 
hitz zaharrak berri bilakatu dira. 
Andoaindik plazara, kanpora, mundura 
zauden tokira etorkizuna dugu ikastolari begira 

Zure hitza, nire berba, solas zaparrada 
ezagutzen ez dutenentzat hemen lekurik bada. 
Urrun iritsiko da euskararen abiada 
irri formako uhinak zabaltzen ari gara 

Begirada berriekin ON egin 
etorkizunari bidea ireki 
eskuz esku sarea osatu 
euskaraz munduratu 
sortu, piztu, ON egin! 

Euskaraz ON egin 
hitza uhinez hedatu dadin 
lekukoa urratsez urrats zaigu iritsi 
Ausartu ON egin 
Mugak gaindituz elkar eragin 
hitzetatik ekintzetara 
piztu, ON egin!

Begirada berriekin ON egin 
etorkizunari bidea ireki 
eskuz esku sarea osatu 
euskaraz munduratu 
sortu, piztu, ON egin! 

Kilometroak 2012 
euskararen printzak 
egin du argi 
Kilometroak, urriak 7 
Andoainera 
zatoz gurekin.

Begirada berriekin ON egin 
etorkizunari bidea ireki 
eskuz esku sarea osatu 
euskaraz munduratu 
sortu, ..., ON egin!
eskuz esku sarea osatu 
euskaraz munduratu 
sortu, piztu,