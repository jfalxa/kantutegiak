---
id: tx-2202
izenburua: Eskutitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b2U9_piv4HE
---

Euskal Teknoren kanta ezagunetarikoa. Taldea: Hemendik at

Idoia:
Zer moduz?
Gaur beti bezala zutaz oroituz.
Badator udara
Laster egonen gara.

Itsasoko olatuek
Eskutitza ekarri dute
Zure hitzak irakurtzen
Gehiago naiz maitemintzen.

Zuretzat muxu bat,
Botila hau doakizu hemendik at
Itsasoan zehar
Bidai luze batean.

P.D. Hau ez da erreala
Soilik amets bat da
Desio hau azkenik
Betetzea nahi dut nik.