---
id: tx-1721
izenburua: El Buey De La Barranca
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/InKCv8BxqTs
---

Larri zegon hats haundiko 
Federiko mutil zaharra,
jauzi zion busturriko idi handia trokara, 
erdaldun talde bat etorri zen hara 
eta laguntza eskeini honela esanez. 
Sacaremos a ese buey de la barranca, 
de la barranca sacaremos a ese buey 
Idi haundia trokaren sakonean, 
zamararte zegoen lupetz artean, 
kantu labur honek bazuen piztu.
Sacaremos a ese buey de la barranca, 
de la barranca sacaremos a ese buey 
Hartu zituzten egur eta palanka, 
jaitsi ziren putzu hortara labanka, 
dena zen esku, 
adarra eta hanka, 
eta lanean hasi ziren kantatuz.
 
Sacaremos a ese buey de la barranca, 
de la barranca sacaremos a ese buey 
Kanporatu zutenean idi zaharra, 
harroputz agertu zitzien mutil zaharra, 
erdaldunek bota zuten pikutara 
eta berriro ere honela kantatu. 
Meteremos a ese buey a la barranca, 
a la barranca meteremos a ese buey