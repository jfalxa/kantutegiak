---
id: tx-1946
izenburua: Gure Hitzak Mikel Laboa)
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6l-jGRG2ThI
---

Gure hitzak
Esan berriz esan
Ez daitezela ahaztu
Ez daitezela gal,
Elur gainean
Txori anka arinek
Utzitako arrasto sail
Ederra bezalaxe

Txoritua norat hua
Bi hegalez airian

Zer dio isiltzan denak
Isiltzen denean?

Gizon galduak, nork lagunduko?
Miseria sufri ezin dutenak

Oi lur, oi lur, ene lur enea...

Maitea nun zira
Nik ez zaitut ikusten
Ez berririk jakiten
Nurat galdu zira