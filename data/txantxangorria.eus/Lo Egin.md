---
id: tx-1848
izenburua: Lo Egin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qpaiRBPlF0U
---

Baloi kalboak direla agentzien berri
"Maradona productions" orrazien egilea
off siden geratu
eta sheriffa Wembleyko belarra erre
zuten
rasten atzetik dabil
lo egin lo egin lo egin... kume
txalo pintxalo

Lemoiz inguratu pirata eta sioux
Bizargorri ta Asterix jake erregeari
ontzi eta zaldiz
tronpetak "under my thumb" airean
Jeriko hautsi
lo egin lo egin lo egin... kume
txalo pintxalo

Mafalda eta Zakili enamoraturik
zine baten atarian diskutitzen ari dira
zer pelikula ikusi hori da kuestioa
"El Gran Dictador" edo
"Emmanuelle en el frontón"
lo egin lo egin lo egin... kume
txalo pintxalo