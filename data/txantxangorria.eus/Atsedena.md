---
id: tx-2679
izenburua: Atsedena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D1PVtGPqNho
---

Horrenbeste behar 
Denak ase ezin 
Izateko asmoz, barnea entzun, kasu egin 

Onartu hala dela 
Hartu emanaren ibaia 
Ezin topatu bion nahiak, pena da baina akitu da 
Ezin naiz hala egon 
eder zenaren hilobian lo 

Atea ireki 
Triste den horri 
Sendatzeko prest, bizi legez, jantzi nekez 

Zerk du balioa? 
Hitzen gainetik 
Bada zer ikasi higadura honetatik 

Umezurtz, bizi laiak 
Hala ere gogoan ditut 
Zer, noiz eta non 
Hainbat une on 
Zure begi 

Iluntasuna baztertu dut, 
ez da urrun 
Bizi berri hori 

Entzun adi 
Izar da gurea 
Ta argituko du 
Eman nizuna baina bizi bat hobea