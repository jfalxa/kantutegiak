---
id: tx-464
izenburua: Ametsetan Gaude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/X_YxBoq1aLI
---

PEIO ALDAZen Ametsetan gaude kantaren bideoa. 2021eko maiatzean Lekunberrin grabatua. Aner Ansorenak zuzendua. 

Vídeo musical de la canción Ametsetan Gaude de PEIO ALDAZ. Grabado en Lekunberri en mayo de 2021. Dirigido por Aner Ansorena.


Hitzak | Letra de la canción:

Gaua iristean
Basapiztia ateratzen da.
Dantzatzerakoan
Bihotzaren erritmora.
Barnean piztu da
Sua gu bion artean.
Kolore eztanda
Zure ahoan sartzean.

Orgasmo batean
Hondoratuta gaude.
Gorputz biluztuak
Ohean dira kide.
Zure begi biak
Pasioaren bide.
Biok izerditan,

Ametsetan gaude (x5)

Izaren azpian
Arnasestuka gaudela.
Muxu bat lepoan
Ezpain ezeak.
Horma hauen artean
Beroa arnasten da airean.
Jasanezina da
Bion arteko tentsioa.
Azkenik heldu da
Gozatzeko garaia.
Bi pertsona gara
Baino arima bakarra.

Azken minutua
Joko dugu kanpaia.
Adia egitean,

Ametsetan gaude (x5)

Begiak itxita
Jolasten gabiltza.
Begiak itxita
Hegaka gabiltza.

Ametsetan gaude (x5)