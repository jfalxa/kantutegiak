---
id: tx-32
izenburua: Kantari Bizi Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uVAB3pXkxqw
---

(Pa para para papa…)
(Pa para para papa…)

Bide galduan zear, zenbat gau zure billa
Zuri itzegin nairik, nairik eta ezinda
Orain urrean zera eta odola ta ametsa piztuak dira

Zugatik biurtu dira nere malkoak
Amets argian, kanta gozoan
Bañan, zerbait gertatzen da gure artean
Ikarak nabil zure ondoan

Zugatik biurtu dira nere malkoak (Hei)
Amets argian (Hei), kanta gozoan (hei)
Ez dakit zure maitasun jaio berria
Noiz arteraño gordeko detan

(Pa para para papa…)
(Pa para para papa…)

Zugatik biurtu dira nere malkoak (Hei)
Amets argian (Hei), kanta gozoan (hei)
Bañan, zerbait gertatzen da gure artean
Ikarak nabil zure ondoan

Zugatik biurtu dira nere malkoak
Amets argian, kanta gozoan
Ez dakit zure maitasun jaio berria
Noiz arteraño gordeko detan
Noiz arteraño gordeko detan

(Pa para para papa…)
(Pa para para papa…)