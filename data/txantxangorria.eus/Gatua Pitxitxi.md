---
id: tx-466
izenburua: Gatua Pitxitxi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WWRulsG81GY
---

çEz du uzten supazterra badu lepoko superra
Da gizen bezain ederra iduri du kalonjera

Gure gatua Pitxitxi da dena ile ta txitxi

Eguna derama lotan ase ondo ametsetan
Iduri du bai egitan kalonjer bat bezperetan

Jaun Kalonjera barkatu zuri baitut konparatu
Pitxitxik iliak baditu etzaio kaskoa pelatu

Lana baitzaio itsusi eztu ihizin ikasi
Saguno bati ihesi nihaurk atzo dut ikusi

Pitxitxi hiltzen delarik ezpaitu egin gaizkirik
Zazpi izpirituetarik bat baduke salbaturik

Nola bizi den lur huntan hala gatuen zerutan
Biziko da ametsetan sekulorum seku...lotan.