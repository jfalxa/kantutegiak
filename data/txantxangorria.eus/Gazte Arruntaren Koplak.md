---
id: tx-551
izenburua: Gazte Arruntaren Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GyjDkMiLIMs
---

Kantatzera noazu bertso bat edo bi
Herenegun jarriak gazte arruntari
Julen, xabier, iñaki, ander edo mattin
Gauza ederra denik ez ukatu neri

Goizian jeikitzian lenbiziko gauza
Instagram ireki ta nolako zalantzak
Neska gazte politten argazkiak latzak
Reakzio pare bota ta sartzen naiz saltsan

Ohetikan altxatu ta armairua ireki
Prozedura hura ultertu ezinik nabil
Lurrean hautsa hartzen arropa ugari
Usai txarra isurtzean ez naiz jartzen larri
Kolonia pixko bat ta kalera oso manin

Memi bat egiteko ez noa oso mantso
6 kolektibo jo egin nahi ninduten atzo
Ronaldinho bezala ez begira ta ixo
Nahiko daukat burutik izateaz gaixo

Kalera irtetean hau da komeria
Munipa pare batek geldi nautenian
Porruak, armak edo beste zerbait bila
Nire biosolana aurkitu ezinan
Bitamina falta nabarituko agian

Ez nijoan oso gaizki etxe ta ikastolan
Gauza txarrak eginik lapurtzen altxorrak
Futbolera jolasten ez banintzen ona
Atezain jarriko naiz ez sartzeko gola
Merendoletan drogak fanta kokakola

Lana ederra egin det unira joateko
Drogetan sartzeko ta ere ikasteko
Ikasi  eta gero berriz ikasteko
Aiten etxera bueltan lanik ez balego
Azkenean familiakin badaukat apego

Semeen semetatik badator semiak
Batzuk nahiago keta bestiak emia
Batzuek maria alkol mota ugariak
Horrela jarri zuen jaunaren legiak

Gaurko eguna ere bota degu bada
Gazte mugimenduak egiten pintadak
Marin berriz lehiatu nau bere freestayla
Igual mozkor banabil saiatuko gara
Igual mozkor banabil saiatuko gara

Ez naiz ni gizon txarra baina zer arraio
Fidel kastro izateko ez bainintzan jaio
Estudioan egiten det makina bat saio
Kuadrillaren onduan afaldu det iaio
Orain ohera nua bihar arte 
aio 
Bihar arte aio
Bihar arte aio