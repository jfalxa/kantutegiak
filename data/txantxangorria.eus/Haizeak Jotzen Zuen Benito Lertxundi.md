---
id: tx-448
izenburua: Haizeak Jotzen Zuen Benito Lertxundi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ssoCm84b7XE
---

Haizeak jotzen zuen
Ta hostoak erortzen
Herri bakarti haretan,
Gauak luzeak
Ta egunak luzeagoak
Oraindik ere gehienetan.
Plazak hutsak isilean
Zabalak kaleak
Pasaden mende bateko
Tabernak, tabernariak.
Jendea eta trenak
Eta penak...
Jendea eta trenak
Eta penak...
Badoaz ta badatoz
Badatoz eta badoaz
Sasizko noriak kaletan
Gauargi zaharrak
Promestu biziaren
Aztarna bilaketan
Geltokiko agurearen antzera, antzera
Mendeak ta mendeak
Zain egon denaren
Tristura nabari zen
Herri haretan.
Tristura nabari zen
Herri haretan.
Geltokiko agurearen antzera, antzera
Mendeak ta mendeak
Zain egon denaren
Tristura nabari zen
Herri haretan.
Tristura nabari zen
Herri haretan.
Mendeak ta mendeak
Zain egon denaren
Tristura nabari zen
Herri haretan.
Mendeak ta mendeak
Zain egon denaren
Tristura nabari zen
Herri haretan.
Mendeak ta mendeak
Zain egon denaren
Tristura nabari zen
Herri haretan.