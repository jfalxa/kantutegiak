---
id: tx-1715
izenburua: Ihes
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hJJGLSP6L5g
---

Bide zuzenetik oker

bide laburretik ezer ez

galdu denaren dudak besterik ez

 

Haran berrien bila dabil

zauriak garbitzekoa

bere bihotzeko leku izkutua

 

Ta arrazoia urratu dion orbanetik ihes

malkorik ez, malkorik ez

 

Gelditzen diren arrastoetatik ihes

sentimenduetatik ihes,

min sakonetatik

ta badoa ihes

 

Ezer ez duenarentzat lapurren beldurrik ez da,

biluztasun osoz,

aurrera doa

 

Dena galdua duenaren,

desesperazio hutsez,

salto egiteko, beldurrik ez