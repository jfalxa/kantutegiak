---
id: tx-1499
izenburua: Giza Aberea Xabier Lete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QEpknmUO2mc
---

Makurka ta poliki
arrastaka bidean
bizkarrezurra oker
zerbitzuko lanean
zerbitzuko lanean
bururik altza gabe
munduan zehar doa
giza aberea.