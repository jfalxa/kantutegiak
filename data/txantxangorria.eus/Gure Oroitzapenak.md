---
id: tx-779
izenburua: Gure Oroitzapenak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B3iO6UTjOwQ
---

Gure oroitzapenak
Itsas galeretako oholak bezala
Ez dira
Itxas hondoan ezabatzen
Ez dute
Inongo porturik helburu.
Gure oroitzapenak
Itxas galeretako oholak bezala
Ur gainean doaz kulunka
Uhainek eraginak
Ezabatu ezin
Eta xederik gabe