---
id: tx-2871
izenburua: Ahaide Delizius Hontan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/62m_PRu39ow
---

1. Ahaide delezius huntan bi berset gei tit khantatü,
Ene bizitze mulde gaitza münd’orori deklaratü:
Ihun sos bat ebatsi gabe ez eskandalik txerkhatü,
Hamar urtheren galeretan nahi ükhen naie sarthü.
2. Ebili nüzü kharriketan, ene bidaje handitan,
Amorioz nindaguelarik xarmagarriren huñetan,
Ixterbegiak zaudelarik eia nun sarthüren nintzan,
Bena haien ororen gatik, ni maitiaren lekhian.
3. Zelietako Jinko Jauna, zützaz nüzü estonatzen
Zerentako hain desbardin gütüzün heben egiten;
Batak indarrik gabe, bestiak zentzü gabe, bethi praube agitzen
Mündü huntaik bestila baiko, han denak gira bardintzen.
4. Mündü huntan hanitx persuna bada malerus izanik,
Bena ez beren etxekuak nik bezañ krüdel ükhenik,
Ene amak bost lekhütako primü nündüzün sorthürik,
Igorten naie mündüz mündü deüs nütinak idokirik.
5. Jinkuak maradika beza Gaztelondo Topetia
Eta neskatila praubetan amorio ezartia!
Batetan ezarri nilakoz, izan niz desprimütia,
Egüzaitak egin zereitan ordeñiaren haustia.
6. Hazi behar nian phüntian traballatzen nintzan hasi,
Desprimü-eraz nentzan lotsaz, nahiz aita trenderazi;
Osagarria hari khausitü nahiz benin galerazi:
Orai agitzen zitadazüt segür hiltzia presuntegin.
7. Ene aitak destinatia ükhen nian espusatü,
Erranik nahi zeitadala primajia errendatü.
Ene maite (fidel) ezagütia nik horre gati kitatü,
Eta beste maiterik zian bat ene phena gei hartü.
8. Bi hilabetez egon niz presu ene emaztia gati,
Ta hura xalanteki etxen ni han nintzanez jelosi;
Ber denboran ene anaiek (defautez) prozesa jüja-erazi,
En’hunaren juitü nahiz eta ni kundenerazi.
9. En’anaie erüsatia, menjatü hitzaiket aski,
Ene lehen eritajia orotan fraudaz ebatsi;
Orai khorpitzeko xangriek behar nie eihar-erazi:
Behar diat presuntegian hil edo amuinan bizi.
10. Ene seme nik primütia, egin deitak traditzia,
En’anaiak zeitadanian kita-erazi Frantzia.
Adixkidez libreraz nentzan egin neian bilaiztia,
Eta hik eni hen ixilik egin kundeneraztia.
11. Musde Deffis jaun presidenta, eta Musd’Ündürein jauna,
Galdürik nintzan enialakoz semiak zian deseiña;
Zien leterak eraman baleizt Paueko jaun jüjen gana,
Libratzen zian bere aita, bizia zor zereiona.
12. Paueko jaun jüjek balie ene jüjatzen jakintü,
Nun etzien ene etxaltia frauderiaz akesitü
Enündia kundeneraziren, bena semik ni traditü
Eta ni kundenerazi nai nahiz hunaren juitü.
13. Oi jüstizia injüstua, igorri naizü herriti,
Eni ebatsi etxaltian den anaiaren ihesi!
Ene semik ez nahi hazi, nik hari eman huneti:
Behar diat presuntegian hil edo amuinan bizi.
14. Bi urthe igaran ditiat Españan pelegri gisa,
Hire ganik deüseren ezin ükhenez biziaren bilha;
Orai hiri sokhorri galthoz jin nük ezin ebilila,
Hik igorri ene etxerik erranik han deüs enila.
15. Bi hilabete igaran tiat sabaietan hotzez ikhara,
Ezpenündia nahi ützi ene sükhaltin sartzera,
Ez hik ez hire emaztiak ene ohian lotzera,
Jüstuago zia lo zedin hire küñata izorra!
16. Ene etxalte zabalian ezari ninak hireki,
Gai oroz barnen sar enendin, borthak dereitak esteki,
Ezpeitzeion phenaik egiten hunak ützirik zieki,
Ene amuinan igortiak, bi athorra xiloeki.
17. Adio erraiten dereizüt, ene phena-gei hartia,
Bena orai zü zütüdano, ene kontsolü guzia;
Athorra bat eman deitazü destrenpien erekeitia
Ikhara zinandialarik errenak jakin, trixtia.
18. Zortzi urtheren behar deizüt erran dolorez adio;
Haboro bizitzen ezpaniz, Josafatera artino.
Inkas bizitzen bagirade biak mündian haboro
Algarreki izanen gira Jinkuak pharti artino.
19. Semiari nereionin egin ene photeren üztia,
Beste haurrer niezün egin ene borthen zerratzia,
Eta zü ezari neskato etxen, malerus trixtia,
Aita amak so egizie zer den haurrer bilaiztia.
20. Adio erraiten deiziet ene haur dotherik gabik,
Ene süberte malerusak, trixtik, mündin ezarririk
Ene gisa har-itzazie, pazentziaz zien kürütxik
Eta salbamentia txerkha, hetzaz imita Jesüs Krixt.
21. Adio dereñat erraiten, ene alhaba barkhotxa;
Etxeki-ezan familian orai artinoko kundüta,
Küñat küñater eman beriak, haurrer Jinkuaren kreinta
Senharraren hun izan eta zaharraguak errespeta.
22. Etxahun, nausi famatia, galdü dük libertatia;
Orai hiri erriz ari dük oi hire ixterbegia;
Eta orai hir’ezin bestia, harekila trixtezia,
Behar amuinan hasi edo pelegri juan bestela.
23. Zelietako Jinko Jauna, zuri gomendatzen nüzü,
Ezi ene best’erresursak or’akabi ziztadatzu;
Libertate, hunak, uhurik galdürik agitzen nüzü,
Eta khorpitzaren gal lotsaz, mündüz mündü banuazü.
24. Izan nüzü Jundane Jakan, Loretan eta Erruman;
Jente hunetzaz beinündüzün zük hazi ene errundan.
Orai ere phartitzen beniz zure othoitzez ber gisan
Haietzaz bizi-eraz nezazü malerus nizan denboran.
25. Khantore hoiek huntü nütin Ünhürritzeko olhetan,
Errumarat juiten nizala erraiten beitüt hoietan;
Maleruski ni hil banendi bidaje lazgarri hortan,
Ziberuan khanta-itzazie, ene orhitzapenetan.