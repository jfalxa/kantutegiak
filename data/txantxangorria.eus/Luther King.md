---
id: tx-2768
izenburua: Luther King
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TkQvgpkAv3A
---

Ezin isilik gorde
minez urraturik
lotsaz gizonengatik
burua makurturik.



Ez zun dirurik maite,
gizon pobrea zan,
besteak askatzearren
erori da burrukan.

Ez zuan gorrotorikan
gizona librea zan,
loturik eraman dute
gauaren ilunean.

Gauaren ilunean.

Gauaren ilunean.