---
id: tx-382
izenburua: Urte Zaharreko Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cG67mtZD8Wc
---

Kitarra eta ahotsa: Joaquín Díaz
Trikitixa: Kepa Junkera
Ekoizlea: Julio Palacios
Soinu teknikoa: Luis Delgado


Urte zaharreko koplak
Hau haiziaren epela
Airian dabil orbela;
Etxe hontako jende leialak
Gabon Jainkoak diela.
Lezo eta Errenteriya
Hirugarrena Oiartzun
Nere lagunak Dios te salve
Garbuarekin erantzun.
Dios te salve Ongi etorri
Gabon Jainkuak diela
Legiarekin kompli dezagun
Urteberriren sarrera.
Urteberriren bezpera da ta
Eskian gatoz atera,
Ama Birjiñan Seme garbiya
Dijualako pontera.
Etxekoandre zabala
Toki oneko alaba;
Birian nator informaturik
Emaile ona zerala.
Dios te salve Ongi etorri
Gabon Jainkuak diela
Legiarekin kompli dezagun
Urteberriren sarrera.
Horroko hor goian oilo bi
Batek bestia iduri;
Etxe hontako etxekoandriak
Ama Birjiña dirudi.
Ematekotan emazu
Bestela ezetz esazu;
Ate-onduan hotzak hiltzera
Amak ez gaitu bialdu.
Dios te salve Ongi etorri
Gabon Jainkuak diela
Legiarekin kompli dezagun
Urteberriren sarrera.