---
id: tx-22
izenburua: Harririk Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3_vySOeY0Ok
---

Sopelako Meñakoz kultur elkarteak 20 urte betetzen ditu aurten. Horren arira sortu da "Harririk Gabe" kantua, eta berarekin batera bideoklipa ere. Kultur elkarteko talde denek hartu dute parte proiektu berezi honetan. Gozatu!

MUSIKARIAK:
Zuzterrak Taldea: 
alboka eta txirula: Mikel Aldekoa-Otalora
panderoa: Zuriñe Gonzalez
soinua eta ahotsa: Joanes Atxa

Nonbait erromeria:
ahotsa: Libe Urrutia
ahotsa: Aitzol Iraola
gitarra: Guille Gil
baxua: Mikel Serra
bateria: Ander Hernández

LETRA: 
Naroa Torralba Rodriguez

KANTUAREN GRABAZIOA:
Tio Pete Estudioak

BIDEOKLIPAREN GRABAKETA:
Gorka Granado Terrones eta Jon Castro Zabalegi

ESKER BEREZIAK:
Sare Rasines De la Hoya
Laura De la Hoya
Olatz Terrones Arrate
Gloria Orbes Martínez

Bi hamarkada igaro dira 
lehen argi printzetik antza, 
denbora azkar joan izana 
ez delako inoiz txantxa.
 Lanaren fruitu loratu honek 
galdu gabe esperantza: 
kimua hazi du erdian 
jarriz kantua, herria, dantza! 
Harririk gabeko herririk bai, 
olaturik sortzeko gai? 
Datorrenaren keinu ta bisai, 
hauspo askoren erregai. 
Itsasorako ateetako bat dena 
Sopelan Meñakoz, 
Herrian giltza du 
bilguneak sakon harritsua askoz. 
Harriek kolpe, harriek lana, 
harriek negar ahala poz: 
ez zaizu damutuko sekula, 
animatu eta zatoz! 
Harririk gabeko herririk bai, 
olaturik sortzeko gai? 
Datorrenaren keinu ta bisai, 
hauspo askoren erregai. 
Dena sarritan argiago da 
ortzimugara begira, 
Atzetik eraiki dugun honek 
aurretik badu dizdira. 
Olatuek gora, erritmoak gora, 
lurra mugitzen ari da? 
Azkena balitz bezala zatoz orduan, 
plaza erdira! 
Harririk gabeko herririk bai, olaturik sortzeko gai? Datorrenaren keinu ta bisai, hauspo askoren erregai.