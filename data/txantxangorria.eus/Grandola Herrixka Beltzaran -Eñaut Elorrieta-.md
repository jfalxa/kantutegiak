---
id: tx-3345
izenburua: Grandola Herrixka Beltzaran -Eñaut Elorrieta-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tw4tCS8Cjss
---

Grandola Herrixka Beltzaran
(Grândola, Vila Morena)

Musika eta hitzak: José Afonso
Itzulpena eta egokitzapena: Eñaut Elorrieta

Sortuko dira besteak - SDB2012
Arriaga Antzokia - bilbo - 2012.12.01

Eñaut Elorrieta - Ahotsa eta Kitarra
Joxan Goikoetxea - Sintetizadoreak
Xanet Arozena - Kitarra
Txema Garcés - Baxua
Oriol Flores - Bateria

Soinu arduraduna - Josean Ezeiza
AM Kultur Promotora - Joxeanjel Arbelaitz Irastorza

Grandola herrixka beltzaran
elkartasunaren lurrean
herriak du gehien agintzebn
oi uri, zure barnean
oi uri, zure barnean
herriak du gehien agintzen
elkartasunaren lurrean
grandola herrixka beltzaran

Berdintasuna aurpegietan
lagun bat bazter bakoitzean
grandola herrixka beltzaran
elkartasunaren lurrean
elkartasunaren lurrean 
grandola herrixka beltzaran
berdintasuna aurpegietan
herri bat zure barnean

Adin ezezaguneeeko
arte baten itzalean
zin egin nuen zure nahia
nire egingo nuela
bide izango nuela
zure nahia herrixka beltzaran
zin egin nuen adineko
arte baten itzalean

gehien agintzen duena
herria da zure barnean
elkartasunaren lurrean
Grandola, Euskal Herria herrixka beltzaran