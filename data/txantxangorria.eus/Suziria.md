---
id: tx-2922
izenburua: Suziria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/13eRyangLbo
---

Gorputza luze-luze
buruan txapela
Bizi-bizi metxa
isatsean duela
Piztean kontuz
jo atzera
eskuak belarrietara
Zerura zuzen
ke artean
Suziria ziztu bizian

Ahoa, izterrak,
txaloak eta Pum!
Hau da jai giroaren
iskanbila

Ahoa, izterrak,
txaloak eta Pum!
Lasterka makilaren bila