---
id: tx-1601
izenburua: Txuri Beltzean Urtz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rKxZq-GS4-4
---

Irudiz, josi den gau honetan

galdu naiz, izara buztietan.

Urduri, izerditan blai

orduak aurrera doaz

Ihesi, egun sentiaren bila, noa

egun berri baten berora,

berotzear naiz baina.

ASPALDI HONTAN TXURI-BELTZEAN

IZATEN ARI DIREN, AMETSAK

KOLOREZTATU NAHIAN.

Umezurtz senti arazten nau, zirrara

adieraezin honek

erantzunik ezin dut eman,

jakinminak ito arren banoa

baina ezin ikusi,

nora zulo beltz batetik kanpora

erotzear nahiz baina...

ASPALDI HONTAN TXURI-BELTZEAN

IZATEN ARI DIREN, AMETSAK

KOLOREZTATU NAHIAN.

Ez ustean, arnas beharrak

zabaldu dizkit begiak

zoramena sortu dezan paretak

Etorkizuna amestuz

iraganaren zama, bidertzean galtzen.

eee oooo ooo

Noa, ilunpetik argira

noa, eskura dut geroa

ASPALDI HONTAN TXURI-BELTZEAN

IZAN DIRA BAINA,

AMETSAK KOLOREZTATU DITUT.