---
id: tx-1305
izenburua: Garun Antzuen Festa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4BXbBBUPoh0
---

Labarraren Ertzean" diskaren "Gogoan" abestiaren bideoklipa.
Videoclip de la canción "Gogoan" del disco "Labarraren Ertzean".

Deskargatu "Labarraren Ertzean" diska dohainik hemen! 
¡Descárgate el disco "Labarraren Ertzean" gratis aquí! 





Garun antzuen festa hasi da eta,
bozgorailutik entzuten dira
zarata ergelak mezu hutsen artean,
zentzurik gabeko kantu ustelak.

Soinuaren zirrararekin batera
ideiak dira soberakinak.
Mugimenduak dirauen bitartean
amua irensten du arrainak.

Eta zer kanpoaldeaz
baztertzen bagara?
Hauxe ez da
pentsatzeko garaia.

Festa, hasi da garun antzuen festa
bertan atzera bueltarik ez da
obedientziaren mesedetan (x2)

Pasibotasunaren lema 
berriz kantatzera goaz.
Hauxe ez da
pentsatzeko garaia. 

Ezinegonaren eragin bortitza
zainetan zehar zabalduz doa;
ta bitartean onespenaren printzak
urratzen ditu gure erreguak. 

Festa, hasi da garun antzuen festa
bertan atzera bueltarik ez da
obedientziaren mesedetan (x2)

Jarraitu dantzan, begiak itxita,
altzairuzko ukabila zetaz jantzi baita.
Jarraitu dantzan, oihalaren atzean.
Korronteak daramatzan oihuen artean,
gutxinaka hondoratuz doa kontzientzia.

Festa, hasi da garun antzuen festa
bertan atzera bueltarik ez da
obedientziaren mesedetan (x2)