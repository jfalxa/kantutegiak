---
id: tx-900
izenburua: Malkoak Euritan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7WmQyDR2hsM
---

Berriz ere ilunpean
bihotz jorraten kalean,
laino behean euria
jaisten den artean
proposamena lasaitzekoa bada
ezetza bertan behera uztekoa bada
konturatu al zara?

Zigarroak kotsolatuta
negar malkoak aurpegian behera
ez dut inork kea lainotan ikusten
ezta malkoak euritan ezberdintzen.

Esan didate batzuk
berandu ibili naizela
laino behean euria
jausten den artean.