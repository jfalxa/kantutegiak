---
id: tx-3303
izenburua: Eh Txo -Doktor Deslai-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_a2zqu685SQ
---

Gogoratzen haiz ze ondo
pasatzen gintuen 
kriston martxa zegon
Sasoi ilun hoietan 
gaur dirudi
Demokraziak utzi 
haula pott egina
Ipurdi hartzeari 
gustua hartua Dioala 
inorako bidean doazen
Hanka motel hoiek nork esango
Zian hireak direla leku guztitatik
Korrika ibiltzen hintzen, egunero
Ta orain ikusten haut zintzilik eta
Guztiz aiko-maiko.

Eh txo! Gehiegi itxoiten duk
Hire begirada herabetiak agertzen
Dik zer egin jakingabetasunak
Indargabe haula benetan zeharo
Akabatita hagola 
ematen dik ta
Ezin duk konformagaitz bat izan
Hintzela gogoratu

Eh txo! Gehiegi itxoiten duk
Atera hadi kalera, ta berreskura
Hire martxa itxoiten ezin duk ezer
Lortu

Eh txo!