---
id: tx-2515
izenburua: Pello Joxepe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mhuyeYkDDGs
---

Pello Joxepe tabernan dala
haurra jaio da Larraulen (Bis)
etxera joan da esan omen du:
-Ez da neria izanen,
beraren amak topa dezala
haur horrek aita zein duen.

Hau pena eta pesadunbria!
Senarrak haurra ukatu (Bis)
Pello Joxepe bihotz neria
haur horrek aita zu zaitu
haur horrentzako beste jaberik
ezin nezake topatu.

Zure semia besuan daukat
senarra aldamenian (Bis)
orain denborik eztaukat eta
zuazkit ordu onian
neronek abisatuko zaizut
garaia datorrenian.

Fortunosoa nintzela baina
ni naiz fortuna gabia, (Bis)
abade batek eraman dizkit
umea eta andria:
haurra berea bazuen ere
andrea nuen neria.

Patzientzia hartu bihar da
munduan Pello Josepe, (Bis)
andria ere zuria izanda
haurra bestena diote;
gizonen faltan igarotzen da
basoan zenbait larrarte.