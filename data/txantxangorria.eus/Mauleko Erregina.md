---
id: tx-2506
izenburua: Mauleko Erregina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rFw4tHjthX4
---

Oi espartiña eijerra Lextarren josüa
Non higatüren zira, noren aztaletan
Pariseko plazetan alagera dantzan
Cayenneko karzeletan ipar maietzetan.

Goizetan jarten gira bankaren gibelean
Ehünez goiti bertan lau saski errasan
Aita ikasi zeokün behar adrezia
Lan bakotxak enganazten bere trebezia.

Xinesek ere nahiko lüke egin bera
Bena pare gabea Maule'k erregina
Xuri ala gorria beltx' edo xuria
Elegantena zirela badakigü ontsa