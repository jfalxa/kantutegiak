---
id: tx-2402
izenburua: Ezina Ekinez Egina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o5I2CTd2LSc
---

Pausorik pauso beti bidea eginez, 
geroa irabazi behar dugu oinez!! 
Egatik hegan dator gure irrintzina, 
ezina ekinaren ekinez egina, 
Lizarratik euskera dator indar berriz, 
Navarra Nafarroa dela iragarriz... 
Pausorik pauso beti bidea eginez, 
geroa irabazi behar dugu oinez!! 
Bidean egon arren horrenbeste traba, 
garena izatera iritsiko gara, 
bihotzeko tristura, z
ango nekatuak, 
gaitz guztiak sendatzen dizkigu kantuak... 
Pausorik pauso beti bidea eginez, 
geroa irabazten ari gara oinez!! 
Zatoz, lagun, gurekin, ez egin negarrik, 
euskaraz ez du ta malkoren beharrik, 
badakigu non gauden eta nora goazen, 
kanta dezagun beraz, alai eta ozen... 
Pausorik pauso beti bidea eginez, 
geroa irabazi oinez, oinez!! 
Kantu kantari baina lanean su ta gar, 
gaurko gure ametsa gauza dadin bihar, 
Egatik irrintzina dator hegalari, 
erantsi zeure boza gure kantuari... 
Pausorik pauso beti bidea eginez, 
geroa irabazten Nafarroa Oinez!! 
Euskararen erreka aldean Lizarra, 
euskaltzaleok aurten han dugu biltzarra, 
zatoz zu ere oinez gurekin batera, 
ezina ekinaren bidez egitera... 
Pausorik pauso beti bidea eginez, 
geroa gurea da oinez eta ekinez!! .................................................................................... 1990. urteko Lizarra hiriko Nafarroa Oinez jaiako kantu ederra. Bertan euskal artistak ugari parte hartu zuten, opera edota jota kantariak. Iñaki Perurena, ahotsa Cuco Zigandai, ahotsa Enrique Villarreal "El Drogas", ahotsa Aurera Beltra, ahotsa Miguel Indurain, ahotsa Maestro Turrillas, ahotsa "Nafarroa Oinez" jaia Nafarroa Garaiko ikastolen jaia da. Urtero ospatzen da, euskara bultzatzeko eta horrelako ikastetxeak sortu edo mantentzeko asmoarekin. Nafarroako Ikastolen Elkartea da antolatzailea. Nafarroa Garaiko D ereduko ikastetxe publikoek (Euskararen Foru Legean zehaztutako zonalde euskaldunean eta mistoan baizik ezin dira ezarri) urtero beste jai bat antolatzen dute, Sortzen izenekoa. Ekimena 1981ean agertu zen, Nafarroa Garaiko ikastolen alegalitateak eta haien jarduera arautua aurrera eramateko diru laguntzarik ezak eraginda, Kilometroak ospakizunean egiten zena imitatuz. Denborarekin euskararen aldeko ekimen jendetsua bilakatu da; beste hainbat jarduera egiteaz gainera, urtero 100.000 pertsona inguru biltzen ditu aurretik finkatutako ibilbide batean, zeinetan hainbat jarduera eta ikuskizun gauzatzen diren.