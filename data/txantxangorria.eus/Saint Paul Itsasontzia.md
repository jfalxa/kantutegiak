---
id: tx-1356
izenburua: Saint Paul Itsasontzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aMCqNtBbEzU
---

Bordeletik joan ginen uraren gainean
Saint-Paul izena zuen untzi ederrean,
Heurtin kapitaina zen gurekin batean,
Calcotan arribatu osasun onean.
Gure jaun kapitaina han zaugun eritu
Sukar batek baitzuen lanjeroski hartu,
Noiz ere baitzen aphur bat sendatu,
Marseillako alderat ginen abiatu.
Hazilaren hogoia partitu ginena,
Astizkena baitzuen egun hark izena,
Itsaso zabalean baikinuen lana,
Orduan hautsi zaukun untziaren lema.
Abendoaren seian kaloba trenkatu,
Eta untziaz laster bide makur hartu,
Ahal nola baitugu geroztik gidatu
Aitzeak ez baigaitu hainitzik lagundu
Sekulan orhoitzeko Eguberri eguna
Hartan galdu ginuen gure etxe ona,
Ihurtziri, tenpesta, aize fanfarona,
Bela masta guziak hautsi daukiguna.
Laster eman ginuen hurerat xalanta,
Eta ordu berean harekin kanota,
Uretarat jautsi ginen guziak saltoka,
Herioari buruz hartzera borroka.
Oro akabatuak ez jakin zer egin,
Zorte egin ginuen zoin behar ginen hil,
Hartarat erori zen Julien Girardin
Bera presentatu zen bihotz onarekin.