---
id: tx-2691
izenburua: Zugana Manuela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YIHbdI3JDTU
---

Testua: J.M.Iparragirre. Musika: J.M. Iparragirre. 


Zugana Manuela
nuanëan pentsatu
uste det ninduela
deabruak tentatu;
ikusi bezainekin
naiz enamoratu.
Ojala ez bazina
sekulan agertu!

Amodioz beterik,
esperantza gabe!
Zergatik egin zinan
bihotz honen jabe?
Zuk esan be(h)ar zenuen:
"hemendikan alde!"
Egiazki ez naiz ni
bizardunen zale.

Barkatu be(h)ar dituzu
nere erokeriak,
zure begira daude
nere bi begiak.
Garbi-garbi esan dizut,
nere ustez, egiak.
Zoraturikan nauka
zure aurpegiak.

Gauean ibili naiz
guzizko ametsetan,
Donosti(a)n nengoela
Andra Marietan,
(e)ta ikusi nuela
herri (h)artako plazan
erdian zebilela
Manuelatxo dantzan.