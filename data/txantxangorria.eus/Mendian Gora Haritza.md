---
id: tx-1158
izenburua: Mendian Gora Haritza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HYVDmlk3E30
---

Mendian gora haritza
ahuntzak haitzean dabiltza
itsasoaren arimak dakar
ur gainean bitsa.
Kantatu nahi dut bizitza
usteltzen ez bazait hitza
mundua dantzan jarriko nuke
Jainkoa banintza. (Bis)

Euskal Herriko tristura
soineko beltzen joskura
txori negartiz bete da eta
umorez hustu da.
Emaidazue freskura
ura eskutik eskura
izarren salda urdina edanda
bizi naiz gustura. (Bis)

Euskal Herriko poeta
kanposantuko tronpeta
hil kanpaiari tiraka eta
hutsari topeka.
Argitu ezak kopeta
penak euretzat gordeta
goizero sortuz bizitza ere
hortxe zegok eta. (Bis)

Mundua ez da beti jai
iñoiz tristea ere bai
bainan badira mila motibo
kantatzeko alai.
Bestela datozen penai
ez diet surik bota nahi
ni hiltzen naizen gauean behintzat
egizue lo lasai. (Bis)