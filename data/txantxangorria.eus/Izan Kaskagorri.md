---
id: tx-3320
izenburua: Izan Kaskagorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lASLlM7yRCw
---

Kaskagorri konpartsaren 25. urteurrenerako Pentatonix Folk taldeak eta Patrol Destroyers-ek egindako kantua.

LETRA:

Urte luzetako sormena
proiektu baten izana ta izena,
ilusioaren emaitza
batzuen amets gaizto ta gaitza

Panoramixen osagaiek
ta auzotarrok sortutako
jai herrikoiek,
utopia egi bilakatzen dute

Kaskagorrik mende laurden (jada mende laurden)
ta bagoaz hazten (badoa, bazoaz, bagoaz hazten)
ametsetan sinisten ta
ametsak sinistarazten, (zure, nire gure, ametsak sinistarazten)
25 urte gazte (urte luzez gazte)
ta gorri izaten (gorri, aske, gazte izaten)
atzekaldeko akelarrean (gure akelarrean)
oraindik dantzatzen (dantzatzen)

Zenbat juerga, zenbat aje
mekanotubo ta montaje
gazteen musu sekretu ta
bihurrekirien topaleku

Konpartseroen joan etorriek
urrun baina gertu dauden
izar gorriek
utopia egi bilakatzen dute

Kaskagorrik mende laurden (jada mende laurden)
ta bagoaz hazten (badoa, bazoaz, bagoaz hazten)
ametsetan sinisten ta
ametsak sinistarazten, (zure, nire gure, ametsak sinistarazten)
25 urte gazte (urte luzez gazte)
ta gorri izaten (gorri, aske, gazte izaten)
atzekaldeko akelarrean (gure akelarrean)
oraindik dantzatzen (dantzatzen)

Kaskagorrik mende laurden (jada mende laurden)
ta bagoaz hazten (badoa, bazoaz, bagoaz hazten)
ametsetan sinisten ta
ametsak sinistarazten, (zure, nire gure, ametsak sinistarazten)
25 urte gazte 
ta gorri izaten 
atzekaldeko akelarrean 
oraindik dantzatzen