---
id: tx-1138
izenburua: Zapi Zuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nH7YWfk02hk
---

Zapi zuri honekin 
jolasten gera. 
Orain alde batera 
gero bestera. 
Zapi zuria aurrera 
zapi zuria atzera. 
Zapi zuri honekin 
jolasten gera. 
Pailazo aldrebesena 
Mirri kirtena. 
Barre eta isekarako 
badu kemena. 
Zoro ttiki ta bihurri 
honelakoa da Mirri. 
Txiribitonekin beti 
puilaka elkarri! 
Zapi zuri honekin... 
Zerbait asmatzen badugu, 
berak apurtu. 
Guk zuzena nahi badugu 
berak okertu. 
Inongo grazirik ez du, 
guztiak lokartzen ditu. 
Inork nahi badu aspertu, 
Txirriri deitu! 
Zapi zuri honekin... 
Eguzki ta hodeiak 
daude zeruko. 
Mirri ta Txiribiton, 
mundu-munduko. 
Hauek ezagutu orduko, 
lasaitasunari uko. 
Herri honetako inor 
ez da aspertuko! 
Zapi zuri honekin...