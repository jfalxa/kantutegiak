---
id: tx-189
izenburua: Elüe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/knCDGvHbgMQ
---

Merkatzale bat Elüe deitürik, tximinotürik, bidea galdürik, 
Lerratü eta errekan sartürik, hantik elki zen untsa freskatürik ! 
Biharamen goizan jaun Erretora zen, hasi berhala Elüez trüfatzen : 
« ari zite orai eskerren ützültzen, Jinkoak dütü moxkorrak salbatzen ! » 
Ordian zeion Elüek aitortü :« Jauna, nik eztit Jinkorik otoiztü, 
Nihaur ezpanintz han kaparer lotü, behar nizün bai errekan ürhentü!”