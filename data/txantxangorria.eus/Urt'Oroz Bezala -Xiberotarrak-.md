---
id: tx-3328
izenburua: Urt'Oroz Bezala -Xiberotarrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rK7JnAuUcyY
---

Urt'oroz bezala xiberotarrak
Bildürik beikira oro plazarat,
Gogotik hartürik imur zaharrak,
Kantan eta dantzan ari beharrak.
Zeren ohartürik herrian sortürik,
Etxen bizitürik, edo partitürik,
Gure sor-lekutik, aurhiden artetik,
Amaren altzotik, herriko axolbütik.

Urt'oroz bezala gure plazetan,
Zaharrak gaztiak boztariotan,
Maxkadetan nula pastualetan.
Kantaldietan herriko bestetan.
Zer alegrantzietan ari giren kanta,
Bai eta dantzetan, gure üjantxetan,
Izanik lanian edo gabezian,
Herruak mendian Xiberoko aldean.

Urt'oroz bezala biltzen bagira,
Jei eder horien süstengatzera,
Aitek guri eman indar azkarra,
Eztükegü üzten ahal galtzera.
Aitzina badua mente hau osua,
Hor da andokoa, zer düke gozoa ?
Zelüko jinkoa, gük zuri galtoa :
Zer duke geroa gure Xiberoak ?