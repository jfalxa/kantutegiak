---
id: tx-1731
izenburua: Sorlekuaren Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g9_tftj3FKk
---

Sorlekura, iturrira,
laino gisa soilik,
itzultzen dira urak.

Sorlekura, iturrira,
oroiminak soilik,
itzultzen dira bideak.

Oi lur eta lainuak,
bideak eta oroiminak.
Zer izanen naiz ni bihar, iturrira,
sorlekura itzultzean?...

Oi lur eta lainuak,
bideak eta oroiminak.
Zer izanen naiz ni bihar, iturrira,
sorlekura itzultzean?...

"Oroimina izanen naiz,
lainua izanen naiz"(bis)