---
id: tx-2925
izenburua: Hamabostarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YCr9dxjPI_Q
---

Jolas(tu) gaitezen denok 
soka jokoan, soka jokoan, 
haurtxo euskaldunoi, Ei! 
dagokigun eran, dagokigun eran, 
euskara hutsean. 
Bat eta bi, hiru, lau eta bost, 
hamar gehiagorekin 
beti dira hamabost.