---
id: tx-2977
izenburua: Jainko Ateoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TCJ6GIccwJg
---

Beharrezko galderak bakarrik ekarri badizkizut
ez esan ezer asmatu dudanik
arrakasta eta zoriona nahasten dituzulako
ez pentsatu ziur nabilenik
amildegi honetan
beti hegian dantzari

Esan dezaket damu gabe
ez nukeela aldatuko
urteak galtzeko modu hau
hunkitzearen truke

Hona igotzen naizela maiz
zugana heltzeko besterik ez
Lokatza dabilkidalako zain hauetan

Baina gehienez ere zure jainko ateoa naiz
bai: gehienez ere horren moduko zerbait
eta bakardadea izaten da sarritan
azken akordea ematen ohi duena

Beti zorionaren bezperatan

Baina gehienez ere zure jainko ateoa naiz
bai: gehienez ere horren moduko zerbait
eta bakardadea izaten da sarritan
azken akordea ematen ohi duena
-txalotzen duzun hori-

Beti hegian dantzari