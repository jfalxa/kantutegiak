---
id: tx-584
izenburua: Kantu Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jpu5bZSFhIk
---

Ahaztuko ez dugun udara
hartako koloreak bezala zara
hamaika ertera hamaika algara
elkarrekin

Berde marroi zuri
zure begiradetako batekin
zalantza guztiak urtzen zaizkit
bapatean

Belarrira goxo goxo esan zenidan
mundua konpontzea lortu bitartean
Segi dezagun dantzan
Segi dezagun ametsetan
zu ta ni batera geldiezinak gera

Kantu bat
Bizitako guztiari
Kantu bat
Aurretik dugunari

Bada zerbait aidean
bihotza aurretik jartzen denean
barru barrutik hitz egitean
gu gara

Agian badakizu
gaur ezingo gaituzte gelditu
beldurra atzean utzi dugu
betirako

Belarrira goxo goxo esan zenidan
mundua konpontzea lortu bitartean
Segi dezagun dantzan
Segi dezagun ametsetan
zu ta ni batera geldiezinak gera

Kantu bat
Bizitako guztiari
Kantu bat
Aurretik dugunari
Kantu bat
bihotzaren erditik
kantu bat

Berriz atzera itzuli beharko bagina
ni zu gabe enoa inora
Dena hutsetik sortu elkarrekin
Ta den dena gure erara (x2)

Kantu bat
Bizitako guztiari
Kantu bat
Aurretik dugunari
Kantu bat
bihotzaren erditik
kantu bat
Guretzat bakarrik

Kantu bat
Bizitako guztiari
Kantu bat
Aurretik dugunari
Kantu bat
bihotzaren erditik
kantu bat

-------------------------------
Zuzendariak:
Jaime Venegas
Iratxe Reparaz
Joseba Razquin

Aktoreak:
Tomas Lizarazu
Ariane Aldalur
Xabier Arrieta
Haitz Munizio

Musika eta hitzak: BULEGO

Grabazioa, ekoizpena eta muntaia:
Musika ekoizpena: Eñaut Gaztañaga
Musika grabazioa: Gaztain Estudioak
Musika masterizazioa: Doctor Master