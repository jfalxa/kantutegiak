---
id: tx-2924
izenburua: Magila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hiZapPqtpCc
---

Atzo Magila ikusi nuen,

Magila hasi zen dantzatzen.

Dantzan bugi-bugi,

dantzan bugi-bugi

eta ondo pasa!