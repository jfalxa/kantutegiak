---
id: tx-554
izenburua: Emoiztazu Bihotz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XoTisbIoa94
---

Directed by Julen De La Serna
Management: joseba@deepsoda.com


Letra: 
Zergatik hurbiltzen zea distira horrekin begietan
Muturretan hainka egin gutzudanean.

Zergatik hainbeste izotz zeure izerdien gatzez urtzean  
Zergatik itxoingogeu nahi badegu ta

Gorputza kiribiltzen like a rapapampam
Zeure pozoia edanez goxo goxo jantzan
Apurka apurka jaiki, hazi, goraka
Zeure suaren garra
Horixe bai gorputzarra

Gorputza kiribiltzen like a rapapampam
Zeure pozoia edanez goxo goxo jantzan
Apurka apurka jaiki, hazi, goraka
Zeure suaren garra
Horixe bai gorputzarra

Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz
Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz

Emoiztazu bihotz
Rapapampam
Emoiztazu bihotz
Rapapampampam
Emoiztazu bihotz
Rapapampam
Emoiztazu bihotz
Rapapampampam

Eztau inor geure etxian
Izarak ximurtu daiguzan
Etzazu aldein maitia
Eztau inor geure etxian
Izarak ximurtu daiguzan
Etzazu aldein maitia

Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz
Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz

Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz
Emoiztazu bihotz
Babes bila nator zugana
Emoiztazu bihotz

Emoiztazu bihotz
Rapapampam
Emoiztazu bihotz
Rapapampampam
Emoiztazu bihotz
Rapapampampam
Emoiztazu bihotz
Rapapampampam

#KaiNakai #EmoiztazuBihotz #Reggaeton #EuskalHerria