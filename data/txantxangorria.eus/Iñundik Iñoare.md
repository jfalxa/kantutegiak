---
id: tx-2647
izenburua: Iñundik Iñoare
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jwPz8WxHEDM
---

Haurtzaro ikastolaren oroitzapen jaialdian grabatua 2017ko maiatzaren 6an Oiartzunen.

Egileak: Gaizka Peñafiel & Asier Olazar