---
id: tx-2697
izenburua: Eguberri! Bozkario!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p0LCo5tZ4KE
---

Eguberri! Bozkario!

Boztu bedi mundua!

Gora zagun misterio

zeruan asmatua!

Gizonaren erosteko,

Jainko gizon egina!

Gure zorrak ordaintzeko,

(or)daintzeko zordun jarrita!(Bis)




Goazen laster artzainekin,

Jesusen oinetara;

Berekin dauka zeruko

ondasunen bilduma;

Bera behartsu egin da

guri aberasteko.
Negarrez hasi bizitzen,

gure dohatsutzeko (Bis)

Kanta zaiogun alairik

gaur sortu den Jaunari;

Agur, ohore, ospea

heldu den erregeri.

Kur bitez zeru ta lurrak,

Jesus, zure aurrean.

Satanen indar guziak

hondatu su lezean (Bis)

Negarrez hasi bizitzen,

gure dohatsutzeko (Bis)