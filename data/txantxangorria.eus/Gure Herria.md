---
id: tx-2167
izenburua: Gure Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3LEroMwood4
---

Bernardo Atxagaren letra Iñaki Eizmendik musikatua 1977an.

Gure herria andere zabala da, 
morez jantzitako andere zabala,
bere zainetan odola 
gasolinaz nahasten da,
bere bularrak bi botil berde dira... 
Eta hauetako egun batean, 
suak pizten direnean, 
bere bihotza molotov koktel bat bezala 
hautsi eta "Plaf!", lehertuko da!!