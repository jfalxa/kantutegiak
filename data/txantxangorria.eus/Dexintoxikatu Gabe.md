---
id: tx-2343
izenburua: Dexintoxikatu Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q7C-7siF9ZM
---

Kontaktua: 697 51 48 70 // akabutaldea@gmail.com Instagram:
Gure ondoan dauden pertsona garrantzitsu hoientzako eskeinia. Abestia 2018ko udan grabatu da Xanperen Koba estudioan. Videoclip-a Txaber Miravallesen eskutik. Disfrutatu eta zabaldu!! AKB


Ezagutu zenun erantzuna, adikzio baten moduan. Indarra ematen zizuna, goibel zeunden bakoitzean. Zainetan sentitu zintudan, eta orain esnatu naizela. Pentsatzen jarri ta ohartu naiz, bizio bihurtu zarela. Erlojuari begira, orratzak ikustean, denbora gelditzen da zugaz nagoen bakoitzean. Zure usain ederra, irrifar rebeldea, begirada maltzurra katxondo jartzen nauena. Ondo ezagutzen nauzu, badakizu soberan, zugaz egongo nazela momentu on ta txarretan. Mesedez ez zaitez joan, nire droga baitzara, desintoxikatu gabe pasa dezagun gaua. Atzo izan balitz bezala, gogoratzen dut gau hura. Zure ezpainen zaporeaz, erotu ninduzula. Bapatean agertu zana, garrantzi handikoa bihurtu zan. Inoiz lo hartuko ez duen, sua piztu zenidan.