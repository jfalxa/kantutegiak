---
id: tx-1303
izenburua: Aurkitu Genituen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NjkbnoM-1Xs
---

En Tol Sarmiento (ETS) taldearen "Aurkitu Genituen" kantua zuzenean, Oinkari Dantza Taldeko Ane Urruzola eta Iker Sanz dantzarien parte-hartzearekin. Bideoa "Ametsetan" disko + DVD-aren parte da (Baga-Biga, 2017). Bideokliparen egileak: Artifikzio.


Aurkitu genituen gure ametsak
Aurkitu genituen itzalpean
Aurkitu genituen geure izarrak


Aurkitu genituen aspaldian.

Aurkitu genituen lehen jolasak
geure izpiak egunsentian
Aurkitu genituen lehen kantuak,
lehen sekretu eta irriak
Aurkitu zenituen nire emozioak,
nire beldurrak nire ondoan
Aurkitu genituen mundu berria batera

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Aurkitu genituen lehen zauriak
momentu zailen izpiluan.
Aurkitu genituen azken muxuak
gure bihotzen barrenian.
Gozatu genituen denborapasak
gogoratu genuen ametsetan.
Agurtu genituen geure begiak itxita.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela.

Ezetz, ez garela
ur tanta hotzetaz apalduak.
Ezetz, ez zaudela
bakarrik gure begiraden artean.
Ez zaitut ba nire bihotzetik ahaztu nahi
eta ez, ez garela, ez garela.

Beti nire ondoan
maite zaitut anaia.
Beti nire arima
maite zaitut laztana.