---
id: tx-880
izenburua: Romeo Eta Julieta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/akW4Dq8zlgE
---

Romeo zan mutila. Julieta zan neska.
Romeok begiz jo zun festan Julieta.
Hurbildu zitzaion balantzaka atzera,
neskak ez gogaitzeko esan zion bela.

Trago batera gonbidatuko, txupito
Whisky, patxaran, vodka….” Aizak, nahiko! Kitto!”
Bigarrenez, joateko.” Neska, baño ze kristo!”
Romeok.” Gaurkon honekin diat bustiko!”

Taberna lepo zenez oraindik,
Rozea aitzakitzat harturik…..

Julieta(re)n ipurdi(a)n eskua Romeok
Musika(re)n erritmo(a)n ukitu-magreo.
Hirugarrengo abisua, zaplazteko
Ez ezetz da behingoz ondo ulertzeko.

“Ulertu ez zuela zirudi(e)n”. Mutila
Julietaren oinatzen jarraitzaile fina.
“ Izorratu dit neukan parrandako grina”
Polizia(r)i aipatuz nekea, ezina.

“Ez den bitartean deliturik,
Guk ez daukagu zer egiterik….”