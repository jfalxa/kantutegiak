---
id: tx-2020
izenburua: Ni Olentzero Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kwboaeuOGI4
---

Anai-arrebak 
ni Olentzero naiz 
ni Euskalerriko Olentzero 
atzoko eta biharko opariak 
dakartzkitzuena 
traizioz hildako karlisten 
gibel freskoak 
geldirik dauden burni oletako 
maillu eta auspoak 
mehatzariak, ikazkinak, 
benakeroak, zurginak 
eta olagizonak 
esku eta aurpegiak garbi 
begiak malko 
biraoka eta zikin 
mendiari behera 
iheska kaietara 
gari eta oihal 
bila doazenak, 

Espainiako opari 
garestia dakartzuet 
espaineratutako Bizkai 
mehatzetako burnia 
anai-arrebak aditzen? 

Orai hemen nago 
etxeko sutondoan zerraldo 
zomorroak ahoan, 
lerdea dariodala 
hemen nago. 

Orduñatikako 
bidez datozen 
hiltzailei begira 
eta ikusten 
nola ari diren 
gure kaiak lelo-kabiak 
bihurtzen ikusten 
orain txizpaz armatutako 
kaioen ega askea, 

hemen nago, 
Leonard Cohen bezala 
sentitu nahirik 
trintxeretako 
gudariei jauna 
deramaioten apez 
euskaldun bat izan nahian, 

a, ni plazerra naiz 
ni basauntz bat naiz 
Jaizkibeletik 
itsasora jauzika doana 
eta gauez beren burua 
hiltzen duten 
euskal olerkariekin 
eta haien gorputzak 
laztanduko dituzten 
lupin eta legatzekin 
Laga, Bakio eta Ondarraitzeko 
ondartzetan mintzatzen naiz 
ni haur jaioberria naiz 
Aturri ibaian barna 
ene arbasoen 
laztan eta fereken 
billa dabillena, 

a, ni izanen naiz 
ene alaben 
arto, maitale eta seme 
eta euri denean 
haiekin joanen naiz 
bertsoak entzutera 
Xenpelarrenera, 

Irlandako emakumeok, 
Euskalerriko emakumeok 
gure abestiz 
beteko dugu geroa 
belarrezko arparen 
itzalean eginen loa 
eta zuen semeak 
bretoi izanen dira 
Bizkaiko eukalitoen antzera, 
eta inork, inork ez du 
honen berri izanen 
debekatua bait dago 
gure amodioa, 

eta orain banoa 
eta barka kantu triste hau 
barka txoriekin besarkaturik 
gure itsasoko ur sakonetan 
murgiltzen banaiz 

Agur ene, Agur seme 
Agur itsaso, Agur Garazi 
Agur, Agur anai-arrebak 
Agur ene, Agur seme 
Agur anai-arrebak 
Agur ene, Agur seme 
Agur ene guztiak.