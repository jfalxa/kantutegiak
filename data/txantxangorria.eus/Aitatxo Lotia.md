---
id: tx-3379
izenburua: Aitatxo Lotia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VaqbmRemtTk
---

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Kanpaia astintzen
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Txirrinaz deika
-Kanpaia astintzen
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Txirula jotzen
-Txirrinaz deika
-Kanpaia astintzen
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Sofan saltoka
-Txirula jotzen
-Txirrinaz deika
-Kanpaia astintzen
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Kaseta martxan
-Sofan saltoka
-Txirula jotzen
-Txirrinaz deika
-Kanpaia astintzen
Txoriak txio txioka
baina ezin esnatu

Aitatxo sofa gainean lotan
nik berarekin jolastu nahi dut
-Kukua orduan
-Kaseta martxan
-Sofan saltoka
-Txirula jotzen
-Txirrinaz deika
-Kanpaia astintzen
Txoriak txio txioka
Esnatu da lotia!