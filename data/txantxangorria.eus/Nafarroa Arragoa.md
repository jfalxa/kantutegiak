---
id: tx-1453
izenburua: Nafarroa Arragoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VoDzkGUFs4Y
---

Nafarroa, arragoa,
argiaren pausaleku
haizearen ibil-toki zabal,
hesirik gabea.

Nafarroa, arragoa,
sua eta ura,
kondaira urratu baten oihartzun oihukatua.
Amets bat baino gehiago,
ez oparotasun osoa:
nahiaren eta ezinaren 
burrukatoki ekaitzez betea.

Nafarroa, arragoa
mahats eta gari
maitasun eta gorroto
gezur eta egia gurea.
Nafarroa, arragoa
lur beroen sabel emankorra
eta goiko mendi hotzen mendetako loa.

Nafarroa, zuhur eta eroa,
lurrik maitatuena,
hormarik gabe utzitako etxea:
erreka gardenen negarrez
ibai lodietara ixurtzen ari den
odol bustikoarraren iturri bizia.

Nafarroa, arragoa
eta oinarri, eta oinaze
eta bidean zure bila ibiltzea ...
Pinudiek usaintzen dituzten
harri txigorrezko bidetan gora
o, Nafarroa, Nafarroa betikoa.