---
id: tx-1504
izenburua: Bixitxi Marabilli Re
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jZyUUVnlnCQ
---

PIZTIAK + MARABILLI ALL STARS ( Gari, Aiora Renteria, Petti, Gorka Urbizu, Nerea Sanchez) Marabilli Sormen Festibalerako abestiaren bideoklipa.
Videoklip de la cancion de PIZTIAK + MARABILLI ALL STARS del Festival MARABILLI
By Josu Aranbarri Aramaio
El Monstruito, Olee eta Buztenbaltzak koproduziute.

BIXITXI MARABILLI RE


Nahitte batzutan dana baltz-baltza ikusi
Hamen ez dala zereginik pentsa
Hauxe da gobernu
Ez errieik emon!

Lista baltz ofizialetan egon arren
Eta antxoa txiki baten morun sentiu
Kontraxuai aurreik ezin eindde
Dana ezta galdute

Lagun egokixak
Lotsa tantaik be ez
Negar eta barre
Bixitxi marabilli re!

Nahitte batzutan bakarrin topa
Arrotzez inguratute egon arren
Inork ez zaittule maitte pentsa
Paparra ata eta buru altza!

Tunela pentsa baino gexa luzatu arren
Eta nundik urten suma be ez ein
Zabaldu begixak, hartu arnasi
Ipar baltzetan aurrea lagun

Errieik ez emon, ixe ezta galdute
Paparra ata eta buru altzata ibili
Ipar baltzetan aurrea lagun
Txingor eurittan aurrea lagun
Mendebaletan aurrea lagun
Aurrea beti aurrea lagun!

Josu Aranbarri Aramaio