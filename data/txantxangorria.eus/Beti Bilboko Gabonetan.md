---
id: tx-2376
izenburua: Beti Bilboko Gabonetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/efZGbTTTEZ4
---

#saveyourinternet #Artículo13

Beti Bilboko gabonetan
giro ederra kaleetan.
Jendea pozik dabil
argiak zintzilik
Hainbat lagun Alde Zahaharrean
Denok dendaz denda
Gabon kantak haizean
Haurtxo txikia seaskan

Beti Bilboko gabonetan
gure Gran Vian apaingarriak
ilobak eta aitonak
erregeentzako gutunak.
Hortik dator Olentzero
etxekoen poztasunaz
Astoak lepoan duena
bufanda zuri-gorria.

Gabon!! Gabonak!!!

Nireak zureak, eta besteenak
aurten saiatuko gara
hobeak izaten, bilbotarrak garela
jakin dadila.

Beti Bilboko gabonetan
bidele hori gixajoa
talo, sagardo nahiz txakolina
lagunarten Santo Tomasetan

Urrutik dauden seme-alabak
berriz bueltan etxera
mahai inguruan bildutak
hamabietako kanpaiak

Beti Bilboko gabonetan
Santiagoko belenak
beti kopla listoa
loteria bezalakoa
agur eta ongi etorri
urte berriari
hona belako itxaropenaz
hona hemen kanpai soinuak.

Gabon!! Gabonak!!!

Nireak zureak, eta besteenak
aurten saiatuko gara
hobeak izaten, bilbotarrak garela
jakin dadila.

la, la, la, la, la, la, la, la/ra
la, la, la, la, la, la, la, la/ra
la, la, la, la, la, la, la, la/ra
la, la, la, la, la, la, la, la/ra