---
id: tx-1910
izenburua: Upn West
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/he7qthmMV94
---

Seamos conscientes donde queremos ir
Seamos conscientes de lo que significa normalizar
El vascuence
Seamos conscientes que modelo de Universidad queremos
Y seamos conscients, también, 
que tipo de comunidad Foral queremos
con el horizonte puesto siempre en la Unión Económica 
y monetaria que está al caer
Badira hogei urte U.P.N. jaun ta jabe dela
Euskal Herria pairatzen  dugun
Arrotz, inperialimoaren adibiderik garbiena, mingotsena zital hoiek gure cultura baztertu
Ta kanpotik oin inposatzen digute
Gu geu gure herrian arrotz sentiaraziz

Ah! Zeintzuk eta Yolanda Barcina, Rafa Gurrea,
Del Burgo, Cervera Cancerbera, 
Ah! Ah! Ah!Hoiek denak Buff!
Ah! Nola ez, Miguel Sanz
Aizpun txispun euskal kultura
Arduragabe ardura, eta eskura
Ez itxurak egin betiro,
Zura: arbolek beren baitan dutena,
Gorrotoa: U.P.N.ren ideien sena.
Zezena, Iruñean "karrikiria" zenari
Orain zital horiek "Osborne" paratu
Diote izena
Torero fatxak adarkatzen zituena,
Azkena, espainiako gerran hil baitzuten.
Eta egun, biharamun; ezin sinetsiz
Zer pairatu behar dugun; nafarrok ezin dugu etsi.
Bildotsez osatutako gizarte batean bizi
Baikara; arrano beltza nahi digute bihurtu enara.
Eta politika mailan, mara-mara arrotz-herria
Sortuz... "comunidad diferenciada"
Zer ote da hori! Zer ote da!
Hiru puskatan banandu dute gure herria:
-Iparraldea, vasca
-Erdialdea, mixta
-Erribera, espainola, baina orotan,
erdalduna gustura; eta euskaldunok eroso?
Gu ditxoso? Euskara noizbait legezkoa
Izanen ote da...
Euskaraz ta zital solasten ari
Naiz; harroputza!
Harroputzik zitalenak, U.P.N.ko burkide,
Espainiako erkide horiek denak.
Lege arrotzak haien esku, dirua haien
Sari; haien bidegabekeriak denon marmari; eta
Gure aurkari nagusi izanik haien laido eta
Isekak pairatu behar; beti haiengandik eraso,
Baina gu erasotzaile.
Poliziek torturatu naute
Jaun epaile!!!!!
Gaixtoa zara, gaixtoa nafar; zure ideia
Lizunak zuzendu behar. Nafarroa beti espainola,
Frankorekin bezala, edo bertzenaz zulora!
...diote fatxa guziek nire herrian, baita
torturatzaile den gure gobernariak.
Eta ni bildotsez inguraturik, deus ere ez
Ezin eginik... "Diario de Navarra"
Erostearekin denak dira galdurik.
Ateak hertsirik, ni ezin geldirik;
Frantzisko Xabierren izpiritua nire baitan
Kexu: aztertu egoera!
Erraten dit haserreturik
Aintzinako nafarron senak nahi du ikusi
Zintzilik Sanz gure lehendakari
Horren karietarat gaude denok borrokaz Ernari
Lur amari eska listu parrasta, eta
U.P.N. higuindu: aska zeure nazka!

Cuando escucho a Sanz o a otro ... de U.P.N. 
me dan ganas de cortarme el ... y meterselo en la boca
FATXAS (estribilloa)
Harroputzak horiek denak. Ia nafarron
Herenak bere du ikurrina, zerbait natural
Dena. Gure historia hurbila, gure bihar
Hurrena, herren-herrena utzi dute U.P.N..
Zeinek? Del Burgo Zeinek? Aizpunek Zeinek ukatuko du
Ukaezin dena? U.P.N.
Beti berdin!
Gezur urrin gaiztoa dario haien politikari.
Eta ari eta ari, sukaldeko altzari oro
Bezala: beti geldo baina gure energia
Xahutzen, ezta hala?
Anitzek ezpala soilik dakusa, baina
Arbola hor dago, den dena made in U.S.A.
Europako alderdi popularra, G-7, F.M.I.
Ezberdinak dira geruzan, baina mamian
Berdin. Zein zitalak, kapitalista denak,
Unionista espainiarrak gehiago, gu zapaltzen
Gaituztenak.
"U.P.N. " letrak, eta U.Z.I metrailetak.
Biek sortzen dizkigute ahalpetak; alabaina
Ere ez gara kokiltzen, izan ere gure
Herrian arrotz sentitze honek nafarrok
Gaitu hiltzen
Has daitezela Sanz isiltzen; nire mordoiloak
Ahoan itotzen duenean, ea nola agintzen
Duen
Euskaraz eta zital solasten ari naiz;
Harroputzak horiek denak!
__________estribilloa