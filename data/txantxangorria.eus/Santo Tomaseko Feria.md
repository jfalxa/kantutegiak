---
id: tx-2380
izenburua: Santo Tomaseko Feria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TAyIsqqzSTY
---

#saveyourinternet #Artículo13

Santo Tomaseko feria,
txorizoa eta ogia.
Santo Tomaseko feria,
txorizoa eta ogia.
goizean eta gauean,
eguerdian eta iluntzean,
txistorra ala txorizoa,
ikastolan, etxean, kalean.
Santo Tomaseko feria,
txorizoa eta ogia.