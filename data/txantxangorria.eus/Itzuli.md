---
id: tx-332
izenburua: Itzuli
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sC7fqnPSC9o
---

Mila esker DAMA BELTZA eta
KAYAK PRODUKZIOAK


Itzuli

Segi bidean,izan nahi dugu aske
Eutsi borrokan etxera itzuli arte

Segi bidea ez da amaitu
izan nahi dugu aske
Eutsi borrokan gaur ez daudenak
Etxera itzuli arte.

Dena lasai dagoela saltzen digute
Ta hori hoiek bakarrik sinisten dute
Ez da aldatu bat ere egoera
Ta ondo aprobetxatu egin dute aukera
Diotenez urrats bat egin da bakera
Jakin nahi dut hori noren arabera
Argi dago baina ez noa erantzutera herri askea ahal gara horixe da galdera

Segi bidea ez da amaitu
izan nahi dugu aske
Eutsi borrokan gaur ez daudenak
Etxera itzuli arte.