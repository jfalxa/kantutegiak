---
id: tx-1012
izenburua: Katuen Testamentue
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y2TpsZxLNAc
---

KATUEN TESTAMENTUA
Hil da, hil da
gure katue
Ein dau, ein dau
testamentue
Hil da, hil da
gure katue
Ein dau, ein dau
testamentue:
Amumantzako, amumantzako
hankak eta burue
Aititenzako, aititentzako
albarkak eitteko narrue
Mantxolintzako, Mantxolintzako
tripa barrue
horixe dala, horixe dala
katuen testamentue. 
KATUAREN TESTAMENTUA
Hil da, hil da
gure katua,
egin du, egin du
testamentua.
Hil da, hil da
gure katua,
egin du, egin du
testamentua:
amonarentzat, amonarentzat
hankak eta burua,
aitonarentzat, aitonarentzat
abarkak egiteko larrua.
Mantxolirentzat, Mantxolirentzat
tripa-barrua,
horixe dela, horixe dela
katuaren testamentua.