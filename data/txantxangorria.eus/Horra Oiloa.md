---
id: tx-2459
izenburua: Horra Oiloa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-yrQxf4nluE
---

I.Orra oiloa mahain gainean
arrautzea zen amaren pean,
gero txitoa sortu zenean
arrautzerik ez mundu astean.
Lehen oiloak errun artean
¿nork du lehena eman munduan?

II.Mendi gaineko Piarres ttiki
lotsarik gabe zitzaion yaiki:
isilik zaite or, ez mintza gaizki,
yakintsun zira, bainan ez aski,
arras txorotzat gauzkizu naski;
tontoak ere zerbait badaki."


Jakintsun hura hasi zen kantuz,
Bere bizarra bihurdikatuz,
Gehiago deus ezin asmatuz...
Lehen erranak gero ukatuz,
Bere bunua ezin garbituz:
Hura zen, hura, tonto-Pilatus!