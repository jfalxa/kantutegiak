---
id: tx-3047
izenburua: Toki Alai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5v2oVoM0Jq8
---

Musika: Iñigo Muguruza
Letra: Juanma Sarasola
Moldaketak: Julio Bravo
Mandolina: Julio Bravo
Bibilina: Iñigo Salaberria
Baxua: Miguel Angel Garcia
Pianoa: Maite Elizondo
Bateria: Hector Martinez
Trikitixa: Iñaki Aranaga
Abeslariak: Toki Alaiko ikasle, irakasle eta gurasoak
Estudioa: Phanton
Kamarak: Felepe Camara, Carmelo Ballador, Esther Olarbide y Marian Fernandez
Muntaia: Txema de los Frailes