---
id: tx-2803
izenburua: Paradisuko Atean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/or3qI0FfANw
---

1.
Paradisuko atean
abere danak batean.
Pozez zebiltzan beren izenak
Adanengandik hartzean.
Paradisuko atean,
abere danak batean.

2.
—Ia orain kukurruku,
zuk izena nola dezu?
—Esango dizut berehalaxe,
Oilarra esan didazu.
—Ondo dago, kukurruku,
zuk izena horrela dezu.

3.
Nola da, berriz zurea?
—Kurrin-kurrinka nerea.
—Esango diot berehalaxe
nerea, jauna, Urdea.
—Ondo dago jaun Urdea
horrelaxe da zurea.

4.
Etorri hona, kakari,
nola esan dizut nik zuri?
—Esango dizut berehalaxe,
Oiloa esan dit neri.
—Ondo dago kakakari
horrelaxe esan dizut zuri.

5.
—Ea zu, lepogizena,
nola da zure izena?
—Esango dizut berehalaxe,
nerea ezin dit Zezena.
—Ondo dago lepogizena,
horrela da zure izena.

6.
—Esne ta adardun gurea
nola dezu zuk zurea?
—Esango dizut berehalaxe
Behia da, jauna, nerea.
—Esne ta adardun gurea,
horrela dezu zuk zurea.

7.
Belarri-luze mukitzu,
zuk izena nola dezu?
—Ai neri jauna, ahaztu, ahaztu,
ahaztu egin zait, barkatu.
—Ondo dago jaun mukitzu
Astoa zera gaurtik zu.