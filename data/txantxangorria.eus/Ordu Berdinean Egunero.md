---
id: tx-2177
izenburua: Ordu Berdinean Egunero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5YT57TcZdJ8
---

Markos Untzetaren "Gaua basamortuaren" diskaren balada.
Markos Untzeta herrialde anglofonoetan hezitako musikaria da. Ingalaterran igo zen lehen aldiz taula gainera. Londres eta inguruko hirietan ibili ez ezik, Bahametara ere iritsi zen bere gitarrarekin. Euskal Herrira bueltatzean berriro euskaraz idazten hasi zen.

Bere musikak garbi erakusten du zein den bere bidea: pop-rock estiloa, blues-folk ukituekin. Eta zeintzuek izan dute eragina berarengan: Bob Dylan, Neil Young, Bruce Springsteen, Van Morrison..