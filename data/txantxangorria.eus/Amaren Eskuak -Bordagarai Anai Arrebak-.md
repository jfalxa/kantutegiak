---
id: tx-3224
izenburua: Amaren Eskuak -Bordagarai Anai Arrebak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MTlNKal8By8
---

JOJO BERNADETTE (1978, Elkar)

Amaren eskuak

esku maitatuak

amaren eskuak

esku maitatuak
amaren eskuak

trebe ta sanuak

amaren eskuak

usu zaurituak

amaren eskuak

trebe ta sanuak

amaren eskuak

usu zaurituak

Amaren eskuak

guregatik eta

guretzat luzaz

asko pairatuak

amaren eskuak

lanaren lanaren

lanaren bortxaz

zinez higatuak

amaren eskuak

esku maitatuak

amaren eskuak

esku maitatuak

amaren eskuak

esku sakratuak

amaren eskuak

esku sendu-senduak

amaren eskuak

esku sakratuak

amaren eskuak

esku sendu-senduak