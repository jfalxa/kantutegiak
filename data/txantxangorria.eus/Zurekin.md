---
id: tx-1340
izenburua: Zurekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lv1uibKXLaY
---

PAITITI (2017 LP)
Luberri Estudioetan grabatua, nahastua eta Masterizatua 





joan zinen, bapatean 
guztia hau ahazturik 
esan laztana, zer egin dut ba 
zu mintzeko 

zurekin, eguzkiak ez du hitzalik egiten 
zurekin, denbora ez da sekula amaitzen 

zoriontasunean edo zoritxarrean 
elkarrekin bai, egongo gara 
betirako 

zurekin, eguzkiak ez du hitzalik egiten 
zurekin, denbora ez da sekula amaitzen 

biok gera bat, bat gera bi 
nola esan, zinegin 
gure arteko historioa 
benetakoa benetako dela… 

zurekin 
zurekin, eguzkiak ez du hitzalik egiten 
zurekin, denbora ez da sekula amaitzen