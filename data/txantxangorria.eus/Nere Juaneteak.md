---
id: tx-848
izenburua: Nere Juaneteak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V4OzrRJkUyg
---

Zapalketa gorriaren
erahilketa bilutsiaren aitzinean

entuzngo diezu erruki hitza aipatzen

xurgatze amorratuaren

jende gosetien aurrean

entzungo diezu karitate hitza goraltzen

lantegiak geldieraziz

herria kaleratzen denean

entzungo diezu bakea, bakea deihadarkatzen

zapalduak oro batzen direnean eta lurra ikaran ezarri

entzungo diezu:

-ai, nere juaneteak!