---
id: tx-3168
izenburua: Ttipi Ttapa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Adtg1PR3wtM
---

Azkenean ostika
kataiak apurtuta
ikusi dezakegu berriz
Euskadi askatuta

Hainbeste sufrimendu
izan du amaiera
gudariak herrira datoz
garaipena ospatzera

Ttipi ttapa ttipi ttapa
presoak kalera
Ttipi ttapa ttipi ttapa
presoak etxera

Heriotzaren menpe
urte askotan zehar
odolezko errekak orain
ura bakarrik dakar

Mendirik mendi dabil
txalapartaren hotsa
taupadatu nahiean edo
herriaren bihotza

Ttipi ttapa ttipi ttapa
presoak kalera
Ttipi ttapa ttipi ttapa
presoak etxera

Txakurrak joan dira
zipaioak berdin
zapalkuntza gehiago hemen
gertatu ez dadin

Salduaren gainean
lehenengo aldia
eraiki behar dugulako
Euskal Herri berria

Ttipi ttapa ttipi ttapa
presoak kalera
Ttipi ttapa ttipi ttapa
presoak etxeraaaa..

GORA EUSKADI ASKATUTA!