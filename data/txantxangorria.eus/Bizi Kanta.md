---
id: tx-2396
izenburua: Bizi Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2UiIyWwtPIY
---

IZENBURUA: BIZI kanta

LETRA: Jon Maia

MUSIKA SORTZAILEAK:Eneko Gartziandia eta Jon Mortzillo

BIDEO EGILEAK: Oier Albiztur eta  Aitor Beramendi

EDIZIOA: Oier Albiztur

MUSIKARIAK:
Eneko Gartziandia
Jon Mortzillo
Haitz Bujanda
Xabi Seko
Lohitzune Berastegi
Beñat Larrauri
Will Mud Candies
Goiatz Mooshine Wagon

AHOTSAK:
Aixa Sanatano
Jon Mortzillo 
Iñigo Aritza Ikastolako ikasleak

PRODUKZIOA: Manu Gomez
     Beñat Agirre
     Jone Areta
     Asier Beltza

                          

Iñigo Aritza Ikastolako ikasle ohiek elkarlanean sortutako kanta

GRABAZIOA: Itxura estudioan

SOINU TEKNIKARIA: Fran Pérez



 BIZI KANTA

Badator euskararen
Haize zirimola
Oztopoen gainetik
Dakar bizi bola

Gaztain zuri ta beltzak
Ardi gurpildunak
Erbi geldoak eta
Bustitako lumak

Bizi, bizi euskararen hazi
Sakanako lurretan bizipozez bizi
Bizi bizi ta biziarazi
Nafarroa oinezen zu lehendabizi!

Haritz adar okerrak
Lore dardartiak
Kilker erremerreak
Izar basatiak

Datozen beleak ta
Bertako zozoak
Erle hegal bakarrak
Gor dauden usoak

Atxitamutxita bat
keinuka kaletan
Armiarmak esan du
Sare sozialetan

Bizi, bizi euskararen hazi
Sakanako lurretan bizipozez bizi
Bizi bizi ta biziarazi
Nafarroa oinezen zu lehendabizi!

Bi hostodun hirusta
Kontatzen ikasten
Bidertzeko masusta
Nora doan ahazten

Sator makil zuridun
Kale kantoi katu
Zapaldutako onddo
Txori kaiolatu

Bizi, bizi euskararen hazi
Sakanako lurretan bizipozez bizi
Bizi bizi ta biziarazi
Nafarroa oinezen zu lehendabizi!