---
id: tx-202
izenburua: Egañak Mañukortari Gorlizko Jaietako Bertso Saioan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uUjMMx22qVU
---

Bertsolariak:
Saioa Alkaiza, Andoni Egaña, Aitor Etxebarriazarraga y Maddi Ane Txoperena.

Gai jartzailea: Beñat Vidal Gurrutxaga