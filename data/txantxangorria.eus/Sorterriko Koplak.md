---
id: tx-2028
izenburua: Sorterriko Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dw2k_pZ-rxY
---

Ohianeko pagoak blai
aideratuz hiru elai.
Ostadarraren harian dira
hirurak pausatu nahi.

Larrosak dira gorriak
edo bestela xuriak.
Hagitzez dira ederragoak
neskatilaren irriak.

Maldan behera urratsa
betiko bidean latsa.
Hodei ilunak gaua belzten du
udazkena ez da bedatsa.

Usapalak ostantzean
huts bat badut bihotzean.
Zatoz maitea gure etxera
dostatzera arratsean.

Eder basoan haritza
haran aldean garitza.
Fruitu antzera ontzen da hemen
gizasemeen bizitza.

Sagar gorriak dilindan
damea dago ganbaran
maite laztanez hartzeko eta
elkartzeko ohantzean.

Iluntasuna abailtzen da
luma galdu bat bezala
begiak uso, mingaina arana
zure bihotza ene aingura.

Hurrun gauhontzak uluka
zakur deslaiak zaunkata.
Mende oso bat iraganen dut
zure ondoan koblaka.