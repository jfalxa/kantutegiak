---
id: tx-2909
izenburua: Gure Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CWnXB4bnHv8
---

Entzun eizu gure oihua,
entzun eizu gure kanta,
harrobia eta herria,
bihotza zuri-gorria!

Geuria! Geuria! San Mames! San Mames!
Geuria! Geuria! Katedrala! Katedrala!
Athletic bakarra munduan!
Athletic beti geuria! Geuria!

Letra eta musika: Josu Bergara
Audioaren grabazioa: David Sanchez (Bigorringo estudioa)
Bideoaren egilea: Jabier Bergara