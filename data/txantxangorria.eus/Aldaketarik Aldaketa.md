---
id: tx-1686
izenburua: Aldaketarik Aldaketa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lmASfZj0FZg
---

Larru berrituz dator 
sugegorri hori.
Ilea urdindu zaio  
atso zaharrari
Egunik egun datoz 
egunak aldatuz,
biharrak gaurtzen eta 
etziak atzotuz.
Eguzkiak badoaz 
ilargiak heldu,
eguna amaitzean 
gaua da zabaldu.
Itsasoan gora ta 
behera mareak
Bidaztiak sarritan 
aldatzen bideak
Aldatzen da mundua 
aldatzen norbera
aldatzen erantzuna 
aldatzen galdera.
Den dena da aldatzen 
ez bada momia
Aldatzen da azala 
aldatzen mamia
Mundu honetan dena 
sortzen da aldatzeko
Ni ere aldatzea 
ez da harritzeko.
Erabat aldatu naiz 
hamaika urteotan
Baina ez da aldatzen 
zerbait ene baitan.