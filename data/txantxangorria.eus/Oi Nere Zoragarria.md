---
id: tx-3084
izenburua: Oi Nere Zoragarria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aSEciBgYCgk
---

Oi!, nere zoragarria
animaren galgarria,
oro baten gaña garria;
nundik zera etorria.

Angela, maitia,
ederra zerala, diozu, diozu,
nere maite polita, zera zu, zera zu;
ederra zerala diozu, diozu,
nere maite polita,
zera zu, zera zu.

Arlote baina galaia,
beti nabil zure bila,
ner biyotzaren harria...
nere gaixoen sendakia.

Angela, maitia...