---
id: tx-2104
izenburua: Gogo Eta Gorputzaren Zilbor-Hesteak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v0rRkg8NYiE
---

Gogo eta gorputzaren
zilbor hesteak:
bi kate.

Bi kate,
biak ebaki beharrezkoak:
bat gorputzaren bizitzeko,
bestea gogoaren askatzeko.
Adizu, ama
badakizu
sortze berean
zuri gorputzez lotzen ninduen
zilbor-hestea
sendagileak nola eten zuen.
Lehenengo eten beharra izan zen:
bizitzaren
bizitzeko lehen legea.
Haurtzaroan
titia eman zenidan,
mutil-zaroan
eskoletara bidali
bizitzarako armak hartzearren.
Dena eskertzen dizut
duen balore guztian,
nik ahal dutan neurrian
nainan, gaztaroan
amatxo maite!
Ohar zaitezte
benetan maite nauzun ama
izan nahi baduzu
eta nik zu maitatzea,
ni naizena
nik izatea nahi dutana izatera
utzi behar nauzula,
hau baita bide bakarra
biok alkar sanoki maitatzeko,
biok alkar osoki eta betikoz
maitatzeko,
zuk zure nortasunaz,
nik nereaz,
zuk zure nortasunaz,
nik nereaz.
Ama!
Eten dezagun
lehen gorputzarena bezela
orain,
gogoaren zilbor-hestea.
Ama!
Gogoaren zilbor-hestea.