---
id: tx-3126
izenburua: Ilargia Non Zaude -Rikardo Uretari-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bzHUsBFK0WQ
---

Esaidazu nere ilargi maitea
Zergatik da bizitza hain krudela?
Esaidazu gure Rikardo maitea
Gaur ere hor zurekin dagoela
Euskaraz irakatsi dizu ziur
Gurekin egin zuen bezela
Jakin zazu guztiaren gainetik
Euskal lurra zinez maite zuela
Motibo baten bila 
Nabil egunero
Larrosaren arantza
Barruraino sartua


Esaidazu nere ilargi maitea
Ederto gogoratzen duzula
Gauza bera direla bai Rikardoren
Eta euskararen pelikula
Laurogeita bian ikasten hasi
Bi urtera irakasle gendula
Begoñan, Indautxun, Gorliz, Plentzin
Inork ez du ez ahaztuko sekula
Motibo baten bila 
Nabil egunero
Larrosaren arantza
Barruraino sartua

Jakin zazu nere ilargi maitea
Gure Rikardo joan zenetik
Egun goibel hartan azken arnasa
Etxeko sofan hartu zuenetik
Gu ez gara gu,  Gorliz ez da Gorliz
Bere katuak zain daude oraindik
Esaidazu nire ilargi maitea
Noiz bidaliko diguzun bueltan hortik
Motibo baten bila 
Nabil egunero
Larrosaren arantza
Barruraino sartua


Jakin zazu nere ilargi maitea
Ez garela ez jarriko triste
Rikardorengatik tori mila irri!!
Bihotzak zaizkigu-ta pozez bete
Umorez, alai, kantuan garela
Bera beti gurekin presente
Eta euskaraz berba egitean
Orduan bai dugula Rikardo maite
Motibo baten bila 
Nabil egunero
Larrosaren arantza
Barruraino sartua