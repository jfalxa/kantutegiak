---
id: tx-2081
izenburua: Borreroak Baditu Mila Aurpegi -
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ow9jaqLpFng
---

Europak gerra dakar
badakar
gora paris-dakar
gilotina beti dator
hegoaren iparraldetik
gainetik.

Gezurrezko ohitura
kultura
non dago tortura ?

Esistitzen ez dena
ez da esistitzen
ta kito txikito.

Kazetari zipaio tokaio
kartzelero arraio
sindikatuan dabil
funtzionari gizarajoa
majoa.

Goikoa beti ona
funtziona
non dago komona ?
burokrazia beltza
burokrazia abertzalea
ere bai.

Telebistako parlantxina
kotxina
"gaizki dabil txina"
begiratu zure salan
nonahi dago Estatua
sartua.

Gasteiz edo Washington
Washington
Bush edo Bushclinton
nik ere nahi bozkatu
gure hurrengo borreroa
eroa.