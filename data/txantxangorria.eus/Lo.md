---
id: tx-1920
izenburua: Lo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q9J5r2heLfQ
---

Bazen behin
lastozko gaztelu bat
printzesa alai ta urdin
printze lodi eta arrosa

Herensuge
zahar, su gabekoa
asto gainean doan
zaldun baten laguna

Egizu lo... lo... lo...

Bazen behin
txokolatezko dorre bat
ipotx garai batek
gau ta egunez zaindua

Igel-printze
zeharo nazkatuta
ezin loak hartu
Loti ederraren zurrunkaz

Egizu lo... lo... lo...