---
id: tx-1466
izenburua: Konfeti Kolore
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zO8PnfrjfBM
---

Ohetik jeiki eta oraindik ametsetan
ezin hobea izango da.
Paperezko mundu bat datorkit gaur burura
egunkarizko hegazkina.

Aero neska urdina, astronauta koadrila,
putz egin eta airera
Munduan zehar bira, lurreratze garbia,
goazen hau marraztera

Gurpilak dituzten bozgorailuak
Egunak argitzeko dauden muxuak

Berdintasuna jainko den lekua
Goserik gabeko hirugarren mundua

Amestu aske
sentitzeko hobe
konfeti kolore

Abestu aske
sentitzeko hobe
konfeti kolore