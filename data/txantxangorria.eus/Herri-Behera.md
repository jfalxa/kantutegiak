---
id: tx-2193
izenburua: Herri-Behera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8ZwcwkgCTzs
---

1974an Benito Lertxundik bere bigarren diska luzea argitaratu zuen. Honetan dauden abestien artean Txori txikia, Oi Lur, oi lur,... eta mitikoa bihurtu den Herri-behera