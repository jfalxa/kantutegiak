---
id: tx-1049
izenburua: Orkatz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kkk0JHMD_0c
---

Topaleku bilakatu dugu
guztiok batera
Gu denok bere parte
garen moduan bera gure parte da
Arranoa ta ikurrina
balkoian zintzilikatuta
Askatasuna dugu helmuga eta bandera
Herritik eta herriarentzat
jada hogei urte
Zerbait eman diogu
beragandik jaso dugunaren truke
Iraultzeko hainbeste mundu
sentituz irauli ditugu
tarteka bertako hormek hitz egingo balute
Aurrerantzean ere
behar dugu indartu
Bi hamarkadaz egunero
gaitu eta elkartu
Maite omen dugu Orkatz
saiatu gaitezen hortaz
gure egin ta parte hartu
Nahiz eta sarri egiten dugun
parranda ta festa
Nork ez dizkie bota
barrako psikologoei hainbat kezka
Egunero zenbat arazo
konpondu ezinak akaso
eztabaidatuz gozatzea gisakorik ezta
Aurrerantzean ere...
ARRANOAREN OIHUAK
ELKARTU GAITZALA
BESTE HAINBESTE HAMARKADAZ