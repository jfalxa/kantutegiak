---
id: tx-2031
izenburua: Zaude Lasai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LXcoq5KoKfU
---

Askatuko ZAItuen
ZAI bazaude
zaude laSAI,
zaude ZIUR
askatuko ZAItuela
Kateak itsusiak baitira
hilotzaren gorputzetan.