---
id: tx-1498
izenburua: Lagun Tristea Arkaitz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YP_GWeJmmzs
---

Abesti bat dut, eta oso polita,
zuei esateko, zuek ulertzeko, egi haundi bat,
mundu honetan gauza ederrena dela 
alaitasuna zuen bihotzean beti edukitzea...
Zure hurbil, edo urruti, 
beti izango da lagun triste bat,
zuen lana da kontsolatzea, 
eta betiko bere laguna izango zara!!
Zer egin, une hontan, bera alaitzeko?
Bere isiltasuna orain ulertzeko?
Izango dut alaitasuna behar dena, 
ez esateko ez dena, ez dena, 
saiatuko naiz, behintzat, ni...
Zure hurbil, edo urruti, 
beti izango da lagun triste bat,
zuen lana da kontsolatzea, 
eta betiko bere laguna izango zara!!
........................................­.........................
Arkaitz plaza taldeak The Police talde famatuaren kantu bat euskaratzen. "Gogoz" diskoa 1994. urtean kaleratu zuten, Lagin zigilupean..

Fernando, bateria
Steve, kitarra
Txuri, ahotsa
Pedro, kitarra
Mae, pianoa eta ahotsak
Epi, baxua eta ahotsak