---
id: tx-2462
izenburua: Bentatik Nator
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6YHEx39MQxo
---

1.Bentatik nator,
bentara noa,
bentan da nere gogoa.
Bentako arrosa
krabelinetan,
hartu dut amorioa.

2.Zuk ederretan,
nik galantetan,
zuk ederretan gogoa.
Ederretan dan 
galantena da
Ana presentekoa.

3.Horrek galai bat
behar omen du,
bera dan bezalakoa.
Bera ona ta
hobea luke
Joxe Mari presentekoa."