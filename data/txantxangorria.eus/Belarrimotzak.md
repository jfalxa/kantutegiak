---
id: tx-128
izenburua: Belarrimotzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W7uARQ-bmlg
---

BELARRI MOTZAK 
Hitzak: Jon Maia
Musika: Gorka Hermosa

Lorca poetak dio: aurpegi ederreko neska olibak biltzen ari da, haizeak, dorreen galai, oratzen dio gerria. 
Eta gogora datorkit galdera: zu ote zinen abuela olibak biltzen ari zen haurra?

Itsasorantz zetozen ibaiak ziren trenak, 
leku baino hutsuneago, 
olibondorik gabeko norterantz zetozenak..
Eta euskal lurretara iritsi zinen gazte, belarrimotz, hezurbeltz, maketa, coreana, baina madre zinelarik, bihurtu zinen ama.
bizitza berrirako maletak hartzean
arropa zaharrez beteak
eguzkia urruntzen atzean
elurtutako lur batean

Extremadura, Galizia
Gaztela, andaluzia
Zein zamoratik          

Maite zenutena utzi ta
Lurralde arrotz hontara
esku hutsik Heldu
zinaten

Belarri motz bideluze
Sasi, euri ta laino 
Olibondotik gatoz
Kantu honetaraino

zer eman ziezaguken 
bizitzak ala lurrak
ez bada oliba batzuk eta 
Hil arte munduan leku bat

Oinetakoak gerratean
Urratu arren
Bide luzea egin zuten
honarte

Eta Sarrionandia poetak dio: “Nekez usten du bere sortherria sustraiak han dituenak”. Eman gabeko besarkadaren aurrean, zorra kitatuz, hona hemen presente lurra, hona hemen presente olibondoa, ahazturaren aurka landatua, maite duzun lurrean bertan, hil eta bizi izan zaren honetan!

Hil arte beti euskalduna naiz ni
Haiei, esker euskaldun.

Gu ere herri
zahar eta berri
zuei eskerturik