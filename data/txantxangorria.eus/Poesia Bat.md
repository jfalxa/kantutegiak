---
id: tx-164
izenburua: Poesia Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3myYfAqCrRo
---

Musika: Ibil Bedi x Lil Buruleun
Letra: Amets Aranguren
Nahasketa: Asier Renteria
Argiztapena: Lander Amorrortu
Esker bereziak: Txoko Gorri eta Elorrioko Gaztetxea


Hutsune honendako
ez dago zulorik
zulo gehiagorendako
ez dugu lekurik

Ahantzi dugu zer den hau
eta nor den hori
neguaren gordinean
ba al dago zubirik? 

Hau dena ez da
Hau dena ez da
Hau dena ez da

Gari soro oso berak
hartu du gaur berriz su
gero dioen berberak
bego baitio usu

Aurrerantza goazela
atzera begiraturik
betiko argia sartzen da
gaztelu beltzean zutik

Hau dena ez da
hau dena ez da
superestruktura hutsa

Honek denak du
honek denak du
oraindik forma kamutsa

Izan bedi hau
herriarendako poesia bat