---
id: tx-170
izenburua: Kateari Tiraka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0baXDQRuiRg
---

Orain dela aspaldi ezagutu genuen elkar
Leku batetik bestera elkarren alboan
Erlojuaren orratzak aurrera doaz
Harro eta pozik egindakoaz
Honaino egindakoaz!
Hasierako momentutik
Ukabilak estu, bihotzak gertu
Hitz gehiegirik esan gabe
Soberan ginen ulertu
Errepidean egindako lagunak
Eta bidetik geratu zaizkigunak
Bihotzean daramatzagunak
Elkarrekin pasatako uneak
Beldur, barre, algara, poz ta malkoak
Betirako gorde ditugunak
Ahaztu ezin diren egunak
Eta hemen gaude
Zurrunbilo baten erdian
Berriz ere
Patuak idatzitako tokian
Kateari tiraka
Eta taupada hotsak bihotzean
Eta hemen gaude
Berriz ere!
Leku batetik bestera
Elkarren alboan
Harro eta pozik
Egindakoaz!