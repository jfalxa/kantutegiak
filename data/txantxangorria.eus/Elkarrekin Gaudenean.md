---
id: tx-118
izenburua: Elkarrekin Gaudenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iixl95xtVz4
---

▪ Contacto: info@airakamusic.com


▪ Letra:

Argazki bat aterako dut une hau izozteko
Ez naiz inoiz maitemindu 
ta beti gogoratzeko

Uda hontako gauak
Zure begi urdinak
Dena emango nuke 
zu berriz hemen bazina

Hilak 29 ditu ta gu ilargi argitan
Dena ondo dago hemen elkarrekin gaudenean
Nire ama telefonoz deika non nagoen galdezka
“Dena ondo dago ama, ez esan amets bat dela”

Urte batzuk aurrera egin, ta berriz topatu gara
Badakit ez dela berdin, orain hobeak gara

Atzean utzi dena
ez berriro ekarri
Eskutik heldu eta
bagoaz urrun hemendik

Hilak 29 ditu ta gu ilargi argitan
Dena ondo dago hemen elkarrekin gaudenean
Sukaldean elkarrekin dantzan ta urtebetetzetan
Dena ondo dago hemen elkarrekin gaudenean

Ez dugu esan betirako denik
baina hau zerbait dela badakit
goazen biok urrun hemendik

Ez dugu esan betirako denik
baina hau zerbait dela badakit
goazen urrun gu biok bakarrik

Hilak 29 ditu ta gu ilargi argitan
Dena ondo dago hemen elkarrekin gaudenean
Sukaldean elkarrekin dantzan ta urtebetetzetan
Dena ondo dago hemen elkarrekin gaudenean

Elkarrekin gaudenean…