---
id: tx-248
izenburua: Askatutako Korapiloak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/f_NsO2trDYM
---

Lorratzen atzetik bizi eredu berri baten bila

Nire historia idaztekoa den egun hauetan

Arrastaka lurrean, iragan denak ezabatzeko asmoz

Buru-makur aurkitu nauzuenetan

 

Askatasun irudimen faltsu bat eraiki dut

Nire mundua sortu, distantzia guztiak gainditzeko

Zenbatetan maite, zenbatetan erre

Ilusioek ilusio izaten jarraitzen badute

 

TA NOIZ ARTE?

ASKATUTAKO KORAPILOETAN

ERDIGUNEAN ZU, NI BEHARREAN

 

Egutegiko egunak badoaz eta ni hemen

Iparrorratzaren orratzak konpotzen berriz ere

Zenbatetan kalte, zenbatetan gorde

Ilusioek ilusio izaten jarraitzen badute

 

TA NOIZ ARTE?

ASKATUTAKO KORAPILOETAN

ERDIGUNEAN ZU, NI BEHARREAN

SENTIMENDU GUZTIAK DANTZAN

TA… BELDURRAK BALANTZAN

GARENAREN ZATI ERRENAREN

ZAMA ASTINTZEAN

 

Ezbaian naizena         

Zure laudorio             

Bakardadea eta         

Miñen desafio           

Geldi ezin bihurtu      

Aurrera bidean          

Eta indartu                 

                          

 

Lorratzen atzetik bizi eredu berri baten bila

Nire historia idaztekoa den egun hauetan

Arrastaka lurrean, iragan denak ezabatzeko asmoz

Galdu, erori, jaiki eta berriz hasi

 

TA NOIZ ARTE?

ASKATUTAKO KORAPILOETAN

ERDIGUNEAN ZU, NI BEHARREAN

SENTIMENDU GUZTIAK DANTZAN

TA… BELDURRAK BALANTZAN

GARENAREN ZATI ERRENAREN

ZAMA ASTINTZEAN