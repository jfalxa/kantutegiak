---
id: tx-2034
izenburua: Berandu Dabiltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7kOJES70eDo
---

Eguneroko bizitza ikatza bezelako
labexomorroak ixurtzen hasi zenean etengabe,
Otis Redding badiako kaian eseri zen
aideplano eroriei kantatzeko blues band.
Sagarraren loreek su hartu zuten eta
niri odola hasi zitzaidan sudurretatik
baldosa zuriko komuna publikotan
pornografia irakurtzen ari nintzenean love,
eta alkimistek txosten bat eman zuten
argitara plusvaliaren alde azalduz erabat,
Errestaurantetik bi homosexsual bota
zituzten arrazoi komertzialak zirela merio.
Bakardadearen milaka kopia inprimatu
ziren neguaren ateetan geundelako,
ene osaba batek postale bat izkribatu
zidan Stop ene urtebetetzea zelako Stop iloba,
eta bide batez bere seguruetxea kontseilatu
nahi zadalako inoiz ez da jakiten noiz,
ene maitaleak telefonoa jo zidan kobre
zaporeko maitasunez gauaren erdian.
Eguneroko bizitza ikatza bezelako
labexomorroak ixurtzen hasi zenean etengabe.
Gero ernari gelditu zen ibaia zerutik
atergabe jautsitako espermaz, alkandora
arrosak loredunak xuriak gorribeltzak
multikoloreak herrestan eramanez,
eta aste berean ni puta bat naiz oihuztatu
zuen ezkontza despedidan Champagne,
Olivetti batetan bere lehen gutun erromantikoa
osatu zuen sekretaria lotsatiak;
eskaleek halere trapuzko etxeak jaso
zituzten zubipean mitxeletak babesteko,
kattagorriek supermerkatuari eraso zioten
altxa besoak non da intxaur kaxafuertea.
Negua heldu zen azkenez eta
antzerrek V bat egin zuten zeruan,
dispensariotan antitristezi pildorak
banatu zituzten ugari
Ejiptoko zazpi izurriteren edizio
ugaldu eta korrejitua usmatuz;
zoriona gramoka saltzeari ekin
zioten trono aingeru eta kerubinek;
beturte kardinek igotze desnaturala
eman zuten Erromako boltsan,
mila liburu erregalatu zituzten bankuek
Ora eta bereziki Labora lemapean;
baina alferrik zen berandu dabiltza
ez dute ezer lortuko honez gero,
palazioak lurrera doaz gartzeletako
ormak pitzatu egin dira, 
eta gu libro gabiltza kaleetan tirantezko
galtza laranjaz jantzirik, libro libro.