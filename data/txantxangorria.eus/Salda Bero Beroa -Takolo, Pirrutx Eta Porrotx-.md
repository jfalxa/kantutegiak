---
id: tx-3220
izenburua: Salda Bero Beroa -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AABds8ESMlM
---

Salda bero-beroa gaur dago guztiontzat, 
goilara sartu orduko denok hasten gara ia dantzan

Lapikoa, sutan jarri eta gero ura bota
haragiak, ez du faltan izan behar
Orain porru, azenario eta gatz pixka bat
dena nahastu, igeri egiten dabil perrexila