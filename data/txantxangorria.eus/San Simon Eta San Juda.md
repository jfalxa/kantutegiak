---
id: tx-1703
izenburua: San Simon Eta San Juda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6CKBPFOo5-o
---

San Simon eta San Juda 
joan zen uda eta 
negua heldu da. (bis)

Ez baletor hobe, 
bizi gera pobre, 
eremu latz honetan, 
ez gera hain onak benetan.

Ez dugu zaldirik, ez gera zaldunak; 
ez dugu abererik, ez gera aberatsak. 
Euskara guk dugu, gu gera euskaldunak, 
euskara guk dugu, gu gera euskaldunak.