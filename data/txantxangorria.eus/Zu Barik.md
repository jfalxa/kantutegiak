---
id: tx-922
izenburua: Zu Barik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GxNEf3sIWCY
---

Kanta bat egitera noakizu
nere ametsetako laztana maitea.
Pentsamenduak ezin hobeak ditut
zure begiak, zure irrifarra alaia.
Dardarak ditut bihotzean
ikusten zaitudanean
Beroak nere gorputzean
zu ikutzean.
Badakit
zu barik
bizitza ez dela berdin
benetan ziur nago.
Badakit
zurekin
bizitza dela atsegin
ez zaitut inoiz utziko.
Zu zera ene bizitzako argia
kezkaz ditudanean nere gidaria.
Sentimenduak dira agerian
gure gorputzak
el/artzen diren unean.
Ametsak egi bihurtu dira
zurekin bat egitean.
Itxa/on nuen hura
orain betetzean.
Badakit
zu barik
bizitza ez dela berdin
benetan ziur nago.
Badakit
zurekin
bizitza dela atsegin
ez zaitut inoiz utziko.
Badakit
zu barik
bizitza ez dela berdin
benetan ziur nago.
Badakit
zurekin
bizitza dela atsegin
ez zaitut inoiz utziko. OHHH...
Badakit
zu barik
bizitza ez dela berdin
benetan ziur nago.
Badakit
zurekin
bizitza dela atsegin
ez zaitut inoiz utziko.