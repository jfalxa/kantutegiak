---
id: tx-3396
izenburua: Txantxangorria Libre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VKne0ojPQEw
---

Bertsoak: Nerea Ibarzabal

Hitza dutelako bide
gaur epaiketen maindire
baten azpian harrapatuta
zenbat adiskide
Gu geu gara konponbide
ta egingo dugu posible
herri libre izan gaitezen
noizbait ta ez erdi libre

Gu geu gara konponbide
ta egingo dugu posible
herri libre izan gaitezen
noizbait ta ez erdi libre

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, lai, lara

Herria mintzo da jada
"Bukatu da, nahikoa da"
eta ahotsak batuz
urruti iritsiko gara
Europara, Espainiara
epaitegi geletara
ta oihu hau entzun
gura ez duten belarrietara

Europara, Espainiara
epaitegi geletara
ta oihu hau entzun
gura ez duten belarrietara

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la raila la

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la raila la

Txantxangorria askatasuna
birigarroa menpea
epaiketa politikoetara
zuzen daramate

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la raila la

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

Txantxangorria abertzaleak
birigarroa txakurrak
guk paparra ta eurek eskuak
odolez dauzkate

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai



Zapaltzen duen Estadu
baten jomuga gara gu
epaileen mailu azpian
urte lar daramatzagu
baina gero bat daukagu
ta izaten segi nahi badu,
txantxangorriak derrigor
LIBRE izan beharra du

Baina gero bat daukagu
ta izaten segi nahi badu,
txantxangorriak derrigor
LIBRE izan beharra du

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

Sentimendu bat gidari
zoaz hegaldiz hegaldi
ihes eginez jarraitzen zaitun
mamu ilunari
Baina herria duzu habi
ez gara bat, ez gara bi
denok zurekin gaituzu e/ta
eutsi gogor, Xabi.

Baina herria duzu habi
ez gara bat, ez gara bi
denok zurekin gaituzu e/ta
eutsi gogor, Xabi.

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai



Txantxangorria askatasuna
birigarroa menpea
zizaren buztan batengatikan
preso garamate

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai


Txantxangorria abertzaleak
birigarroa txakurrak
zizaren buztan batengatikan
preso garamate

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai



la/rai, la/rai, la/rai, la/rai
la/rai, la/rai, la/rai,
la/rai, la/rai, la/rai, la/rai,
la/rai, la rai ra ra/i

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai

larai, larai, larai, larai
larai, larai, larai,
larai, larai, larai, larai,
larai, la rai ra rai