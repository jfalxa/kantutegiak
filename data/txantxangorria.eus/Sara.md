---
id: tx-2374
izenburua: Sara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bVUBK3hFkUo
---

Gaur duela 15 urte, Espainiar eta Frantziar estatuek euskal presoei ezarritako sakabanaketa politika dela eta, Iruñeko Alde Zaharrak bizilagun bat galdu zuen, Sara Fernández. 
Dispertsioarekin behingoz amaitu! 
Sarari eskainia.

SARA

Onberatasunaren lagun
auzoan ezagun
amaigabeko bidaietan
gidari gau ta egun.

Hamabost urtetan zehar
beti Aldapa kalean
zara zu
urtero
berpiztu.

Zirt edo zarteko emakume
herriaren maitale
bidegabe den zigorraren
nahigabe ordaintzaile.

Zergatik eman beharra,
maitasuna dela-eta,
trukean
samina
bizitza?

Argi eduki ezazu
eman duzun guztia
ez dela
alferrik
suertatu.

Sara, ooo,
auzoak maite zaitu,
dena merezi duzu!

#saveyourinternet #Artículo13