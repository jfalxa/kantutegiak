---
id: tx-1727
izenburua: Pernando Amezketarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ElYf3FeBZg0
---

Lagun on batek enkargatu dit
bertso berriyak jartzeko,
neronek ere badet gogua
aren itza mantentzeko:
auek izango dira Fernando
Amezketarrarentzako
festa-denboran kanta ditzagun
tristura denak kentzeko.

Tolosa aldera maiz bajatzen zan
deus gutxiren aitzakiyan
ostatu zarrak bisitatuaz,
ango berri baizekiyan;
usai gozua zegon tokitik
nekez joango zen plakiyan,
bertso ederrik kantatzen zuen
allegatzen zan tokiyan.

Bederatzi seme-alabarekin
bere emazte ta guzi
ez da euskaldun abillagorik
sekulan iñon ikusi;
ardi batzuek baituagatik
nekez joango zan igesi,
juezetara deituta ere
askotan bera nagusi.

Bein juez batek otsegin ziyon:
"Atoz, amigo maitia,
zurekin itz bi egin biaitut,
sillan eseri zaitia.
Kostako zaizu oraingo teman
irabazi ta joatia".
Erantzun ziyon: "Bai bedorri-re
ez dan tokitik jatia".

Bein praile zar bat etorri zen da
zorrotz artu ziyon kargu:
"Errota-arriyak zerutik bera
zenbat denbora bear du?"
Pernando zenak erantzun ziyon:
"Jauna, ez noski bi ordu".
Arro etorri zitzaiyon baiño
laister aurretik bialdu.

"Zenbat denbora biar lukian
orain nua esatera,
errota-arriya prailia balitz
konparaziyo batera:
gaur amaiketan botia balu
Jainkuak zerutik bera
amabitako etorriko zan
bikariyuen atera".

Pernando nola bizitzen baizen
bikariyuen auzuan ...
egun batian abisatu ta
etxera ekarri zuan,
biyok izketan egondu ziren
ordu bete bat osuan
txerriya il da partiziyua
nola guardatuko zuan.

Ta esan ziyon: "Pernando, laster 
il bear degu txerriya,
baiña badago nere barrenen
kezka bat izugarriya:
igaz nundik-nai etorri zaigu
odolkiya ta urdaia,
orain ordaiñak partitzekotan
besterentzat det erdiya".

Erantzun ziyon: "Il beza, jauna,
neri esan didan gisan,
baiña odolko gutxi partitu,
ez bedi tontua izan ...
gero balkoian jarri bear du
jendiak ikusi dezan,
gaberdirako barrena sartu
ta ostu diyotela esan".

Kriara laister bialdu zuben
arakiñaren galdian,
il ta balkoain ipiñi zuten
ta odolkiak baldian;
Pernandok berak eraman ziyon
gaueko amarrak aldian,
apenas asko partitu zuben
Amezketako kalian.

Pernandok berak eraman ziyon
gaueko amarrak aldian,
apenas asko partitu zuben
Amezketako kalian.

(Jose Manuel Lujanbio "Txirrita")