---
id: tx-2129
izenburua: Zergatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ex0SNC3c5p0
---

Imanol Larzabal kantariak (1947-2004) poesia eta musika uztartu zituen sentikortasun handiko abestietan. Parisen bizi izan zen, eta han, besteak beste, Paco Ibañez eta Gwendal artistekin lan egin zuen. Bere ibilbidean hainbat musika mota jorratu zituen: kantagintza, jazza, rocka, musika tradizionala... Euskara izan zuen hizkuntza nagusia abesteko, baina gaztelania ere erabili zuen.