---
id: tx-3176
izenburua: Hegazti Errariak -Mikel Laboa-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WrMsyLHlVLo
---

Hegazti errariak
 pausatu dira
 leihoan
 argia eta itzala
 bereizten diren lekuan
 argia eta itzala
 leihoan
 pausatu dira
 hegazti errariak
 
(Hitzak: Joseba Sarrionandia. Musika: Mikel Laboa)