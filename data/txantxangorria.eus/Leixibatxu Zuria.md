---
id: tx-1864
izenburua: Leixibatxu Zuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qcOukyBobgk
---

Leixibatxoa zuria dozu,
andratxu gaztia
Ondo gobernatuta,
bai aita frailia.
Eskutxu zuriak dozuz,
andratxu gaztia.
Guantepian ibilite,
bai aita frailia.
Familiarik edo bozu,
andratxu gaztia?
Seme bi ditudaz,
bai aita frailia.
Senarrik edo bozu,
andratxu gaztia?
Amerike urrunetan,
bai aita frailia.
Ezeutuko zenduke,
andratxu gaztia?
Lehengo arropakin bada,
bai aita frailia.
Laztan bat emon behar deutsut,
andratxu gaztia.
ibaia dau bitarte,
bai aita frailia.
Bi saltuan pasatukot,
andratxu gaztia.
ez jat komenidetan,
bai aita frailia.
Suak erreko al zaitu,
andratxu gaztia.
Berori ilenti dala
aita frailia.
Urek eroango al zaitu,
andratxu gaztia.
Berori txalupa dala,
bai aita frailia.