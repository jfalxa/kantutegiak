---
id: tx-2951
izenburua: Zezenak Dira Takolo, Pirritx Eta Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ffJ4zM5NuwM
---

Dira, dira, zezenak dira
beltz-beltzak dira
adarrak motzak
punta zorrotzak
Harrapatzen bazaitu,
harrapatzen bazaitu,
jo ta bertan hilko zaitu
jo ta bertan hilko zaitu

Ieup! Ieup!
Ieup! Ieup!

Zezena dator arkupetik
txinparta dariola begietatik
Zibuluka ta zanbuluka
jendearen gainetik

Ieup! Ieup!
Ieup! Ieup!

Euskal Herriko zezensuzkoa
izan zaitez zorionekoa
Euskal Herriko zezensuzkoa
izan zaitez zorionekoa

Dira, dira, zezenak dira
beltz-beltzak dira
adarrak motzak
punta zorrotzak
Harrapatzen bazaitu,
harrapatzen bazaitu,
jo ta bertan hilko zaitu
jo ta bertan hilko zaitu

Ieup! Ieup!
Ieup! Ieup!

Ieup! Ieup!
Ieup! Ieup!