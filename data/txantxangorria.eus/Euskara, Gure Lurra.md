---
id: tx-2407
izenburua: Euskara, Gure Lurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5amaINokLL8
---

Hasiera » bideoak » ‘Euskara, gure lurra’ abestia eta bideoklipa
‘Euskara, gure lurra’ abestia eta bideoklipa
2018(e)ko maiatzak 7 bideoakErantzunak desaktibatuta daude ‘Euskara, gure lurra’ abestia eta bideoklipa sarreran
Udalerri Euskaldunen Egunaren harira, ‘Euskara, gure lurra’ izeneko abestia eta bideoklipa egin dute dimoztarrek, elkarlanean.
Ibon Izak idatzi du letra, eta Kepa Elgoibar musikariak jarri dio letra.
Grabatu, berriz, Dimako eta Arratiako herritarren eta elkarteetako ordezkarien ahotsekin grabatu dute.

Hemen abestia eta bideoklipa:


EUSKARA, GURE LURRA

Zazpi itsaso ditut nabigatu
Gau beltzenean bihurtu naiz katu
Berdin gara soka edo zaku
Baina beti zaitut nik maitatu

Hartu zaitut lurretik tatarrez
Oker-oker eta erdi trabes
Pauso goxo-goxo eta baldarrez
Maite zaitut betiko indarrez

Euskara zu barik ez gara
Euskara zu barik zer gara?
Ai Euskara, zure taupada
Gure bihotza, nire lurra da

Ezin gaitezke sekula banandu
Goiz argietan nahi zaitut laztandu
Maitasunez josi eta ilbandu
Berbaz berba nahi zaitut garandu.

Erraietatik zatoz azalera
Barru barrutik erdu kalera
Aire-aire, ale-ale ba
Goazen biok mundu zabalera

Euskara zu barik ez gara
Euskara zu barik zer gara?
Ai Euskara, zure taupada
Gure bihotza, nire lurra da…

Euskara zu barik ez gara
Euskara zu barik zer gara?
Ai Euskara, zure taupada
Gure bihotza…

Euskara zu barik ez gara
Euskara zu barik zer gara?
Ai Euskara, zure taupada
Gure bihotza, nire lurra da…

· Musika: Kepa Elgoibar.
· Hitzak: Ibon Iza.
· Abeslariak:
Olar herri kirol alkartea
Tubi eta Jon Loiola
Abizarie euskara alkartea:
Bego, Matxalen, Maitane, Ińigo, Eider, sendoa, Naiara, Nekane.
Udaletxeko langileak: iratxe, Arantzi, Nagore
Kultura eta euskara zinegotzia: Ibon
Eskolako umeak: Txomin, June, Olatz, Aimar, Ilargi, Enara, Jare.
Jentiltxu eta Jentili.
· Tronpeta eta tronboia: Roberto eta Garikoitz.
· Irudiak, kamara eta edizioa: Joseba Rodriguez.