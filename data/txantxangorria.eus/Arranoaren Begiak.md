---
id: tx-2825
izenburua: Arranoaren Begiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EvzpywdzMOs
---

1
Aireak narama,
batetik bestera
hostoak bultzatuz,
goizeko dantzara
eguzkia zuzen zerura,
argia datorkit burura.

2
Errekan kantua
gau egun betiko
gu harekin jaio
bizi eta hilko
guztientzat da beharrezko
ta kantu hau ez itzaliko

3
Mendi tontorrean,
jarri naiz geldirik
inora begira
galdera hau eginik
kantua ura janari
ba ote da bizi hoberik.

Aireak narama....