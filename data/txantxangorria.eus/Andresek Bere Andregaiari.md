---
id: tx-1584
izenburua: Andresek Bere Andregaiari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Kl14V3hBALc
---

ANDRESEK BERE ANDREGAIARI
(Uztarri)
1. Milla urte pasata
bederatzireun
bederatzireun,
aurten zazpigarrena
broman pasazagun,
dama gazte polit bat
ustez nekan lagun
gazte gaztia zela
egiña ezagun,
mingañaz alegria
amorioz illun.
2. Zelestina daduka
Dama orrek izena
Dama orrek izena,
Aranon bertan jaio
Ta an bizitzen dena.
Andres orreri kantak
Jartzen asi dena,
Baña ez izan balitz
alaja zuzena,
Nik ola mendratzen det
Merezi dubena.
3. Ezkontza zerratutzen
San Juan bezperan
San Juan bezperan,
Gabeko amaiketan
Kanpuan intzetan
Bidez azpiko aldetik
Leku polita zan,
Aingeru bat bezela
Izketan ari zan,
Ni arront engañatu,
Gazte xamarra izan
4. Eskutikan elduta
Asi zan esaten
Asi zan esaten:
-Andres zuregandikan
ez naiz apartatzen;
lore ederragorik
ez det nik ikusten,
esposatu bitarte
liberti gaitezen,
jai bat señalatuta
Donostira goazen.
5. Etxetik Ernanira
Joan giñan oñez
Joan giñan oñez,
Birian itz potxolo
Ederrak esanez;
Donostira tranbía
Noiz zegoen galdez
Frantzesarekin igual
Asitzen zan ingles;
Erderaz ere fuerte
Naiz bate jakiñ ez.

6. Tranbi ortan sartuta
Jo degu Donosti
Jo degu Donosti,
Bulebarrera joan ta
Pasiatzen asi,
Gero Bretxatik bueltan
Kaliak ikusi,
Eskuiñeko besotik
Ein zidan imurtzi:
Zapata pare on bat
Bear niyola erosi.
7 Zapata oiek artuta
Bazkaltzera fonda
Bazkaltzera fonda,
Galdezka abaniko
Almazena nun da;
Urare erosi nion
Neskatx orri jun da,
Burua freskatzeko
Izugarri on da,
Nere kontura orrek
Pasatu du broma.
8. Urrengo egunian
Jo degu Ernani
Jo degu Ernani,
Tranbian mano-mano
Begira alkarri;
An erosi nituen
Sei pañuelo zuri,
Agua koloniatik
Beste botilla bi,
Orduan zapuztu zan
Nere birjiñ ori.
9. Santiyagotan ziran
Azkeneko planak
Azkeneko planak,
Zuzena okertzen du
Ortarako danak;
Neska tranposa orren
Orduko esanak,
Etziyotela uzten
Aitak eta amak:
Akabatu dirala
Ezkontza onen lanak.
10. Aranon jaioa naiz,
Askok dakitena
Askok dakitena,
Ongi egin ta gaizki
Pagatu nautena.
Igualak zerate
Familiya dena,
Zelestinak ez dauka
Nik aiñako pena,
Ollinden bizitzen da
Andres Mitxelena.