---
id: tx-2811
izenburua: Ados
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pIT8Dl8bxT4
---

Amets bat gure izatearen, inguruan dabil
Eta kaiolatik gure  dau, berak ihes ein
Irrista daigun gure pausua,sortutako
Ilusioaren gainean ibiltzeko.
Lotu daiguzan eskuak indarraz
Eingo dogu bidea alkarregaz.

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEAREN AROA
GURE ESKU DAGO, GURE ESKU DAGO

Gure nortasuna,defenda dezagun

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEKO ORDUA 
GURE ESKU DAGO, GURE ESKU DAGO

Ados gaude 
Gogoz eta tinko
Ados gaude
Erabakitzeko

Bihotzak bere taupadakaz,gidatzen gaitu 
Peoi bat izango balitz bezala, xake, partida baten .
Isiltasunaren katea apur dezagun
Garrasi isil bategaz....eingodu.
Hitzak haizeak eramaten ditu
Ekintzek aztarna, uzten dute.

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEAREN AROA
GURE ESKU DAGO, GURE ESKU DAGO

Gure nortasuna , defensa dezagun

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEKO ORDUA 
GURE ESKU DAGO, GURE ESKU DAGO

Ados gaude 
Gogoz eta tinko
Ados gaude
Erabakitzeko

Ados gaude 
Gogoz eta tinko
Ados gaude
Erabakitzeko

"Gure izerdiaz ureztatuko dogu hazia 
Eta basamortuan Lora haziko da"

Irrista daigun gure pausua,sortutako
Ilusioaren gainean ibiltzeko.
Lotu daiguzan eskuak indarraz
Eingo dogu bidea alkarregaz.

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEAREN AROA
GURE ESKU DAGO, GURE ESKU DAGO

Gure nortasuna , defenda dezagun

GURE ESKU DAGO, GURE ESKU DAGO
ERABAKITZEKO ORDUA 
GURE ESKU DAGO, GURE ESKU DAGO

Ados gaude
Gogoz eta tinko
Ados gaude
Erabakitzeko

Ados gaude
Gogoz eta tinko
Ados gaude
Erabakitzeko