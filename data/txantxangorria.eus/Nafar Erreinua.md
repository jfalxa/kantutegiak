---
id: tx-2025
izenburua: Nafar Erreinua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CkG-GydHx5s
---

Nire aitaren etxea
defendatuko dut
nire amaren etxea
defendatuko dut
Orreagan jarri
zen lehen harria
Navas de Tolosan
kate armarria
Amaiurko guda,
une mingarria
Nafar erreinua
Gaurko Euskal Herria,...
Gure Euskal Herria!!
Nire aitaren etxea
defendatuko dut
Nire amaren etxea
defendatuko dut.

Nire aitaren etxea
defendatuko dut
Nire amaren etxea
defendatuko dut.
Oh! oohh! Oh! oohh!!
Oh! oohh! Oh! oohh!!
Oh! oohh! Oh! oohh!!

Oh! oohh! Oh! oohh!!
Nire aitaren etxea
defendatuko dut
Nire amaren etxea
defendatuko dut.