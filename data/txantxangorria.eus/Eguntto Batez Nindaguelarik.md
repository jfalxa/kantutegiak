---
id: tx-813
izenburua: Eguntto Batez Nindaguelarik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-TGdfEZjtSA
---

Egüntto batez nindaguelarik maitenareki leihuan,
Erran ükhen niriozün hura niala goguan,
Ene phena doloretzaz pietate har lezan.
 
—Zure phena doloretzaz pietate badit nik;
Ene khorpitz tristiareki eztirot eman plazerik;
Zelin promes emanik nago Jinkoari lehenik.
 
—Oro eijer, oro pollit, zü zira, ene maitia;
Zure eskütik nahi nikezü bizi nizano ogia,
Eta gero ni nükezü zure zerbütxaria.
 
—Eniz, ez, ni haiñ eijerra, nulaz erraiten deitazü?
Mündü huntako eijerrena berthütia lükezü;
Hari ogenik egin gabe, othoi, maitha nezazü.
 
—Baratzian zuiñen eijer jirofleia loratü!
Aspaldian desir nian orai dizüt gogatü;
Hura gogatü eztüdano gaiik eztizüt mankatü.
 
—Baratzian eijer deia jirofleia loratü?
Aspaldian desir züniana orai düzia gogatü?
Zerbütxatü zirenian berria khunta ezazü.
 
—Ene maite bihotz gogor, ezpiritü zorrotza;
Orai dizüt orai ikhusten etzitzakedala goga;
Amodiua ützi eta indarreze dügün boroga.
 
Jaon gaztia, othoi, pharka, haur gazte bat ni nüzü:
Zuri arrapostü emaitez debeiatürik nüzü;
Errandelako indartto hura bestetan enpleg'ezazü.