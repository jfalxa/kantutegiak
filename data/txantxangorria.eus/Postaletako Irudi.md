---
id: tx-272
izenburua: Postaletako Irudi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A-feHnGdaN0
---

Zidazun (2021), gure iraupen luzeko lehenengo diska, entzungai plataforma guztietan. Urtzi Izak grabatu, nahastu eta masterizatua. Diseñua Estibaliz Gutierrezen, Candela Azpiroz eta Ángela Rodriguezen eskutik.



Hitzak:

Zein zan
Goizetako aurpegig
Zein zan
mahaian aurreko aulki?
Zein zan
irrien lapur beti?
Zein zan
agurraren musu bi? 

ZEIN ZAN?
ESAN
ZEIN ZAN?
ESAN

Zein zan?
Gure dantzan abesti?
Zein zan?
ezpanetako ezti?
Zein zan
osotasunan zati?
Zein zan 
txikitasunan handi? 

ZEIN ZAN?
ESAN
ZEIN ZAN?
ESAN 

Bere akordue zan (x3)

ZEIN ZAN? 
ESAN 
ZEIN ZAN?
ESAN 

Zein zan
postaletako irudi?
Zein zan
beti egoan adi?
Zein zan
nire galdutako erdi? 
Zein zan
gure itxaropen argi?