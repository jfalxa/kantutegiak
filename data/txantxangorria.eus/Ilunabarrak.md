---
id: tx-754
izenburua: Ilunabarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z3sz3MUKhD8
---

Luis Eduardo Auteren abestiaren itzulpena. 
Letren itzulpena: Jon Martin
Musikaren interpretazioa: Iker Lauroba
Ilustrazioa: Ane Narbarte 

'Nork ostu dit apirila?' proiektuko bigarren abestia da 'Ilunabarrak' izeneko hau. Proiektu honek berrogeialdiak eragin dituen egoerak abesti bilakatzea du helburu. 



Luis Eduardo Auteren 'Al alba' abestiaren itzulpena da 'Ilunabarrak'. Jon Martinek itzuli eta Iker Laurobak musikatu duen bertsio hau Ane Narbarteren margolan batekin ilustratruta dago. Hiru sormen espresio horiek bateratzen ditu 'Nork ostu dit apirila'k: musika, letrak eta marrazkiak. Datorren astelehenean estrenatuko dira Berria Aretoan bi kanta berri: 'Erizainarenak' (Eñaut Aiartzaguenaren ilustrazioarekin) eta 'Gezurrezko eztaiak (Ane Narbarteren lanarekin).  Apirilaren 18an, berriz, hurrengo biak. 



Isolamenduak egoera bitxiak eragin ditu. Birusaren aurrean denak berdinak garela esaten bada ere, denek ez diogu testuinguru berean aurre egingo itxialdiari. Egoera sozioekonomiko, laboral eta afektiboek erabat zeharkatuko dute gure esperientzia. Horietako batzuk kanta bihurtzea da proiektuaren helburua. Konspiranoiak, berri faltsuak, tratu txarrak, harreman berriak, balkoietako poliziak.... hurrengo kantatarako gai izango dira. Musika estiloak ere denetarikoak izango dira: doinu alai herrikoiak, rocka, popa, rapa jorratuko dituzte. 



Berrogeialdian zehar gertatu da Auteren heriotza eta tematikoki proiektuarekin bete-betean lerratzen ez bazen ere, bertan txertatzea erabaki zuten Laurobak eta Martinek. Bste hainbat euskaldun bezala, Filipinatan jaiotako artista honen miresle da oiartzuarra. 



Zentsurarik modu distiratsuan izkin egin zioten abestien adibide da 'Al alba'. Frankismoak Angel Otaegi eta Juan Paredes 'Txiki' ETAko militanteak eta Jose Luis Sanchez Bravo, Ramon Garcia Sanz y Humberto Baena FRAPeko kideak fusilatu aurreko egunean idatzi zuen Autek.


«Fusilatzeen aurreko egunetan idatzi nuen Al alba, urgentzia handiz. Azkarren sortu ditudan kantuetako bat izango da, baina jendeak hura kantatzea nahi nuen. Egia esan, ez nuen askorik pentsatu behar izan; minetik sortu zen». Frankismoaren azken fusilatzean izan ziren, eta 1975eko irailaren 27n izan ziren. Egun batzuk lehenago, gerra kontseilu batek heriotzara kondenatu zituen, eta ezinegon giro horretan sortu zuen Autek Al alba, ziur aski haren kanturik ezagunena izango zena.

«Zentsura azkar pasa zezan nahi nuen. Horregatik egituratu nuen maitasun kantu bat bezala, betirako agur baten eta heriotzaren alegatu gisa. Baina badira kantuan exekuzioekin oso lotuta dauden bi elementu». Zentsura igarota, Rosa Leonek grabatu zuen aurrenekoz kantua, eta 1978ra arte ez zuen diskora eramango Autek.

 Ez gara egunsentiak
izutzen ditun bakarrak
amenazuak bezala
iltzatzen zaizkit izarrak
gaur igitai bat dirudi
ilargiaren zeharrak

Ilunabarra ta gero,
badatoz ilunabarrak
ez nazala zutaz hustu
bihar zoritxarrak

Estoldatan gorde dira
izan ez ditugun haurrak
azken loreak irensten
badaki haien negarrak
atzoko_odolaren gose
segitzen baitu biharrak


Ilunabarra ta gero,
badatoz ilunabarrak
ez nazala zutaz hustu
bihar zoritxarrak


Ixilik, bere hegalak
zabaldurik, putretzarrak
balada makabro hortan
bildu ditu gizatxarrak
polbora usaina dakar
goizeko lehen sugarrak


Ilunabarra ta gero,
badatoz ilunabarrak
ez nazala zutaz hustu
bihar zoritxarrak