---
id: tx-3001
izenburua: Zuri So
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kvoWaEJCnbw
---

Zerua bezain argiak dira zure begiak,
izaren begiradan dirdir zure irria,
zeure baitan bila zazu nahirik sakonena,
ta euria ari du, ari du,
ta estalperik ez dut.

Han nago, zuri so,
han nago, zugan, zugan gau ilunbean, zugan, zuri so.

Zure soinaren bila dabil nire oina, 
zure sena ikutu nahi dut arnasaren grinan,
ikusi nahi zinduket bizi pozen lorian,
senti nahi zaitut zeu zaren egian.

Han nago, zuri so,
han nago, zugan, zugan zeure gogoan,
zugan, zuri so.

Izarrak ari dira, ari dira geuri begiratzen.
Izarrak ari dira, ari dira geuri begiratzen.
Zure begien dirdirak hain dira nireak,
zure begien dirdirak neuretzat eginak.

Bideari daragoiot ta ilusioa berbizten jat,
eguzki izpi ederrak dakarzta haizeak,
bizi gogoa emon daust, laztantzera jatort,
neuri so, neuri so, maite nauela dinost.

Han nago, zuri so, 
han nago, zugan, zugan egunsentian,
zugan, zuri so.

Izarrak ari dira, ari dira geuri begiratzen. 
Izarrak ari dira, ari dira, geuri begiratzen.
Zure begien dirdirak hain dira nierak,
zure begien dirdirak neuretzat eginak.