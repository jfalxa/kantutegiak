---
id: tx-3337
izenburua: Barne Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uxTHm3H5Zjs
---

Non zaude kuku, Maite?
Emen nuzu mingulin.
Oreiñak antzo, iota
nadukazu maitemin.
Atzetik oska nuzu,
ta Zuk neri aldegin.
 
Sarobitik muñora
ardien zai oana,
baldiñ ikusten ba-duk
nik maiteen dudana,
esaiok nola nagon
min, illun, ill-urrana!
 
Zure billa niazu,
piztien bildur gabe
ez loreak biltzera,
mendira, ibarrera
arresi ta gaztelu
bizkor igarotzera.
 
Oi nere Maite orrek
landa zaituten oian
eta baso itsuak,
esaidazute, esan,
belardi loretsuak,
emendik igaro dan!
 
 
          Izakiak
 
Ariñ igaro zaigu
zoramen dariola,
ta begiratuz utsez,
gu ikusiz doala,
daukan edeitasunez
apaindu gaitu ola.
 
 
          Anima
 
Ai, nork senda nazake!
eman zakit egitan!
Ez ibili geiago
neri mandataritan,
ez dakitze mintzatzen
nik nai nuken neurritan!
 
Andik emendik, Zure
zoramen esanekin
zauritzenago naute
eziñ-esanarekin:
iltzear nadukate
bar-bar totel orrekin!
 
Zelan bizirik iraun
ez-leku ontan biziz,
eta maite-guziak
amuts geldieraziz,
Arek egin-ukitu
guziak illeraziz?
 
Zauritu baitidazu
biotz au, esaidazu,
ez al duzu sendatzen?
Lapurtu ba'didazu,
orrelan utzi bage
lapurtu au ar zazu!
 
Arren, begion argi,
ez eduki mingarri
nereok, senda itzezu
zaitzaten ikusgarri
Zutzat bakarrarentzat
nahi ditut ekarri!
 
Maitemiña sendatzen
ba-dakit neke dala;
aurpegi eder ori
agertu dakidala!
Zu begiz ikusteak
erioz il nazala!
 
Oi iturri gardena
zillar-leiar orretan
agertuko ba'lira
nik nere erraietan
irarririk daukadan
begi oiek bet-betan!
Ken, Maite! Ba-niazu egan.
 
 
          Kristo
 
Itzul, usoa,
orein zauritu onek
gainduko du muñoa.
Zure egal oietatik
arturik otxaroa!
 
 
          Anima
 
Nere Maitea!: baso,
zelai gorde, oiantsu,
urrutiko izaro,
erreka txurrutatsu,
aize maiteen xixtu:
ori ta are zera. Zu.
 
Gau ixil paketsua,
goiz-argitik urbilla,
bakardi ozenean,
musikaren ixilla,
maitasuna dakarren
apari zuzpergilla!
 
Matsa lore daukagu,
ta axariak atzeman,
egin dezagun xorta
arrosaz bitartean,
ez baita nabariko
iñor gure landetan.
 
Ixo! Ipar, erio!
Ego, maite-ekargarri!
io nere baratzean
usaien zabalgarri;
an aseko Maiteak
loreetan egarri.
 
Iudar neskax-lagunak!
lili ta arrosa artean
usai onek dirauño,
zaute aldamenean;
arren, ez egin oles
gure etxeko atean!
 
Kuka zaite, Maiteño,
menditara begira,
gorde ixil-gordea,
neskax-lagunak dira:
izaro galduetan
dabillanaren billa.
 
 
          Kristo
 
Arren, egazti ariñ,
leoi, orein txantxari,
ibar, mendi, ugalde,
euri, aize, sargori,
esnai ta amets artean
gau latzaren zaindari!.
 
Sirenan zora-kantuz
dagizue arago, (1)
gorde asarre ori;
ez txintik ein geiago
andregaiak lo egin
dezan seguruago.
 
Sartua da laztana
gogo zun baratzean;
bere gogara datza
loa bete-betean,
lepoa makurturik
Maiteren besartean.
 
Elkartuak gituzu
sagar-ondo azpian,
an, eskuak emanez
senar-emazte giñan;
osasuna arki duzu
amak galdu tokian
 
 
          Anima
 
Gure oia lore da,
leoi-zuloz ertsia,
purpuraz iñaurririk,
pakeak eraikia;
bizkorren iskiloez
inguruan iantzia.
 
Zure urratsen segi
nexka gazteak doaz,
txinpartak ukituak,
birbiz, on dar ardoaz,
biotza bizkorturik
Iainko-usai gozoaz.
 
Maiteren ardangelan
edana naiz barnean;
berriz landa ontara
atera nintzanean,
saldoa galdu nendun
ezer ez oroitzean.
 
An eradoski nindun
iakite txit guriaz;
an eskeñi nintzaion
nerau, dudan guziaz;
bere nindula agindu
nion, danak utziaz.
 
Anima, dudan oro
eman nion mendeko;
enaiz geio saldo-zai,
ez dut deus egiteko;
ontarako bakar naiz:
Maitea maitatzeko.
 
Aurrera larraiñean
ikusten ez nautela,
esanen dute, naski,
maitez galdu naizela:
galgarri nintzalarik
irabazi nutela.
 
Goiz otxean bilduzko
lore ta pitxiekin,
egiñen dugu xorta
Zure maitasunekin,
eta nere buruko
ille eio batekin.
 
Lepoan egan bakar
nik nenkarren illean;
so zenegion eta
bertan lotu ziñean;
zauri ziñan erori
nere begi batean.
 
Begi oiek neretan
eder ziran ixuri;
ala nintzan txit maite,
ala zuten merezi
zuretan zekustena
nereetan iauretsi.
 
Ez esker gaizto neri,
nizaizulako beltxun;
so egidazu, beintzat,
begiz io baininduzun,
ederrez, zoramenez,
apain iantzi ninduzun.
 
 
          Kristo
 
Ba-datar uso txuri
mokoan muskilâkin
kutxara; uxapala
maite zeukan Arekin
urondo urdiñetan
batu dira alkarrekin.
 
Bakarrean bizi zan,
bakarrean kabia
egin du, bakarrean
du maite gidaria;
bakartasun aretan
artua du zauria.
 
 
          Anima
 
Maita igun elkar, Maite,
goazen oian-barnera,
Zure edertasuna
mendian ikustera,
muñoari darion
uretik edatera.
 
Andik gorago dauden
artzulo gordetara,
aietan bi-biñook
ixil sartuko gara,
ta puni-sagardoa
dedakegu gogara.
 
An erakutsiko Zuk
nik gogoan dudana,
emain ere didazu.
—nere bizi zerana—
aurreko egun artan
an eman zenidana.
 
Aizearen arnasa
txindorraren txioa,
izar dagon gauean
inguru on-giroa,
min gabe erretzen dun
sugarraren lanboa.
 
Etzegon iñor beha,
Txerren leize zuloan;
esi-barrua pake
iabalean zegoan,
eta zalditeria
ur-begitik zioan.