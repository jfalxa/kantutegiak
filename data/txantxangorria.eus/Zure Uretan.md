---
id: tx-2257
izenburua: Zure Uretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iWNkXtjrwIc
---

Stop, stop! aurreiritzi barik, probatu nahi nauzule esan barriro kurtzeu garenetik nire begien bila atrapau zaitut behin eta berriz eta hemen nago ni, orain.
Burbuil artean berba zikinak ze gozoak diren belarrira zelan paretan zaren zure azalaren kontra estutzen nazenean.

Esan berriz: emon gehiago baby! eskatu egizu baby ez gorde ezer, ezer.

Sex momentuak, zapata gorriak, zugaz edozein mendi igon leike.

Izotza urtu, erreka egin, zure itsasora heldu arte emon gehiago baby zure uretan ito nahi dot.

Azal zuri zenbat gustetan jaten zure aho gaineko orban hori emon eiztazu emon naz zure egarri zure ezpain gozoak ekarri.

Sex momentuak, zapata gorriak, zugaz edozein mendi igon leike.

Izotza urtu, erreka egin, zure itsasorarte heldu arte emon gehiago baby zure uretan ito nahi dot.