---
id: tx-802
izenburua: Amonatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xIOqZwk4sBw
---

Oilarra kukurruka etxean da ari
Dagoneko amatxo otoitzez Jaunari
bi eskuak  ikaran so kurutzeari;
Jaunak beira dezaion egun berriari!

Igandean ederrik, iduri panpiña,
ziñez esan liteke:"Orra Erregiña"
Begia  irriz dago, orobat ezpaña:
Oi, zer atso polita, Jainkoak  egiña!

Intxaur, piko, gaztanak, zarete  aunditu,
zuen landatzaillea da aldiz xahartu;
ainitz neguetako alurra gelditu,
eta ille  urdiñak zaizkio txuritu.
Agur Amona, agur, agur ene haurra
bidean orok ari bihotzez agurka
amatxo geldi geldi ez dadien lerra
tapa tapa badoa xendrari behera.
Herrira heldu eta gona du astintzen zapatari ere hautsa bere eskuz xahutzen
txanotxoa burutik laster beheititzen
eta ixil ixilik elizan da sartzen.

Ollarra kukurruka etxean da ari
Dagoneko amatxo otoitzez Jaunari
bi eskuak  ikaran so kurutzeari;
Jaunak beira dezaion egun berriari!

Igandean ederrik, iduri panpiña,
ziñez esan liteke:"Orra Erregiña"
Begia  irriz dago, orobat ezpaña:
Oi, zer atso polita, Jainkoak  egiña!

Intxaur, piko, gaztanak, zarete  aunditu,
zuen landatzaillea da aldiz xahartu;
ainitz neguetako alurra gelditu,
eta ille  urdiñak zaizkio txuritu.