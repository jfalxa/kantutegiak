---
id: tx-2461
izenburua: Margulili Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jEHrsMnu_Os
---

Margulili artean
erleak naro ebiltan dra;
korren eztia gore aurrak
ñola ezin doke murtxa.
Erleak: aurkoi bazra de,
fanzte beste lilitra.

Oil oil erle kuek
kartxeritik gain kartra,
larre liliak ostadar
margo polliz xaztan dra;
olli ta ddunddukoi dud
loxka dagon nore aurra.