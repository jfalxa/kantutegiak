---
id: tx-1408
izenburua: Erre Zenituzten Sorgiñen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pe4cea_WGTI
---

Larrabetzuko Bizkargi Dantza Elkarteko gazteek egindako koreografia Xabi Solanoren "Erre Zenituzten" kantan oinarrituz.

Musika: Xabi Solano
Hitzak: Jon Garmendia "Txuriya"


Bideoaren egileak: Gaizka Peñafiel & Asier Olazar

Hemen bideo originala: 


Erre zenituzten sorgiñen
oinordeak gara gu
erre zenituzten sorgiñen

erratzak guk dauzkagu
erre zenituzten sorgiñen
arima ez da galdu
erre zenituzten sorgiñen
irrifarra gara gu

Sasien gainean
oihu eta irrintzi
lainoen azpian
aske nahi dugu bizi

Erre zenituzten sorgiñen
irrifarra gara gu
erre zenituzten sorgiñen
arima ez da galdu
erre zenituzten sorgiñen
erratzak guk dauzkagu
erre zenituzten sorgiñen
oinordeak gara gu