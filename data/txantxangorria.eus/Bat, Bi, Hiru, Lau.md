---
id: tx-1574
izenburua: Bat, Bi, Hiru, Lau
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fx1fWFba9yo
---

Bat, bi, hiru, lau,

bost, sei, zazpi, zortzi,

bederatzi, hamar,

hamaika, hamabi,

hamahiru, hamalau,

hamabost, hamasei,

hamazazpi, hemezortzi,

hemeretzi, hogei.


Hogei, hemeretzi,

hemezortzi, hamazazpi,

hamasei, hamabost,

hamalau, hamahiru,

hamabi, hamaika,

hamar, bederatzi,

zortzi, zazpi, sei, bost,

lau, hiru, bi, bat