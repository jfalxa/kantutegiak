---
id: tx-438
izenburua: Arimak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xWJGsvDe-dc
---

Single 7’’ko Edizio mugatua + Bira! 
ETXEKALTE eta HABI banda berri interesgarrienetako bi dira. Single bat partekatuko dute. Bi kantak joan den urtarrilean grabatu zituzten Elkar estudioan, Víctor Sánchezen zaintzapean. 
Bira bat elkarrekin egin behar dutela eta, bi kanta hauek grabatu dituzte, banda bateko zein besteko musikariak elkarren artean nahastuta (bateria-jotzaileak salbu). Gitarra bizi-biziak, erritmo sendoak, errepika gogoraerrazak eta egungo eszenako bi ahots onenetako bi. Ondorioa? Bi kanta bikain!

"Arimak Denboran" Laguntasunak eta gogoak bultzaturiko lana, garagardo artean hasi eta zuen memorian iltzatuta amaitu nahi duena.

ETXEKALTE:

HABI:

Elkar estudioetan grabatua eta masterizatua Victor Sanchez soinu ingeniariaren eskutik.

Eskerrik handienak proiektu honetan parte hartu duzuen guztioi:

Zuzendaria eta ideia: Alba Lozano
Zuzendari laguntzailea: Iker Maguregi
Aktoreak: Julen Barturen eta Gaizka Sarasua
Argazki zuzendaritza: Andrea Martinez
Kameraria: Iñigo Iriondo eta Jon Santamaria
Gafter: Antonio G. Albalate
Muntaia: Iñigo Iriondo eta Alba Lozano
Soinu ingenieritza: Victor Sanchez
Ekoizpena: Tiro pun, Etxekalte eta Habi
Arte zuzendaritza: Claudia Alejandra eta Ainhoa Atxaga
Jantzien diseinua: Karla Biondini
MUAH eta jantzi laguntzailea: Araitz Pil
Argitaletxea: Elkar

Nitya Lopez
Arantza
Felipe Tony G
Andres Camio "Jitu"

Hitzak/Letra:

Batak bestearen etorrera miresten
Berba zaharren magalean
Eklektikoa bihurtzen, azala berritzen
aldi berean

Inspirazioa bada, gure benetako lehengaia
Saiatuko gara ereiten
Ezpanak, zaporea galdu ez dezan

Bizitzaren akordea
eskatu zenidalez
Arimaren beste aldea
emateko gai nintzen
Nor naiz?
Eta esan zenidake  “norbait zara”
Agerian ezkutatzen, isilpean


Isilpean, agerian izkutatzen duzun

akorde baten bultzadak behartzen nau elkartzera eta
haizearen babesaz, murgildu naiteke zure arimaren barrura
Ihintzaren antzera, azaleratzen dugun horri disdira ematera
(Airea edukitzeko abestu egin nuen)

Bizitzaren akordea
eskatu zenidalez
Arimaren beste aldea
emateko gai nintzen
Nor naiz?
Eta esan zenidake “norbait zara”
Agerian erakusten, isilpean