---
id: tx-1688
izenburua: Zu Zira Zu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4S3WDFDIA1E
---

Zü zira, zü, ekhiaren paria,
        Liliaren floria,
Eta mirail ezinago garbia!
Ikhusirik zure begithartia
Elizateke posible, maitia,
        Düdan pazentzia,
Hanbat zirade glorifikagarria!