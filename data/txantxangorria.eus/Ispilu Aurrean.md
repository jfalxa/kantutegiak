---
id: tx-2171
izenburua: Ispilu Aurrean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wgH2EqYHhmg
---

Alaitz eta Maiderren lehenengo diskaren abesti oso ezaguna eta arrakastatsua.

Ispilu aurreko isladan
Beti nahi dudana naiz.
Baina, zure aurrean
Galdu itten naiz.
Zuregan aurkitu nahi dut,
Nire ispilua.
Eta orduan, beste bat naiz,
Zu zarena... zarena...
Zuregan aurkittu nahi dut,
Nire ispilua.
Eta orduan, beste bat naiz,
Zu zarena... zarena...
Zarenaaaa...
Zarenaaaa...
Zarenaaa...
Zarenaaa...
Isilezko sekretutan
Norbera da sekretu handiena,
Norbera da, nahi duena,
Norbera da, izan nahi duena.
Isilezko sekretutan,
Norbera da sekretu handiena,
Norbera da, nahi duena,
Norbera da.
Niregan aurkittu nahi dut,
Izan nintzena.
Baina, naizen horretan,
Galdu itten naiz.
Zuregan aurkittu nahi dut,
Nire ispilua,
Eta orduan beste bat naiz,
Zu zarenaaaa...
Garenaaa...
Zarenaaaa...
Zarenaaa...
Zarenaaaa...…