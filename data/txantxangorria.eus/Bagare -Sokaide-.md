---
id: tx-3177
izenburua: Bagare -Sokaide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qdzWzNF8R_g
---

Araban bagare
Gipuzkun bagera
Xiberun bagire
Ta Bizkaian bagara
Baita ere, Lapurdi ta Nafarran.

Guziok gara eskualdun
guziok anaiak gara
Nahiz eta hitz ezberdinez
Bat bera dugu hizkera.

Bagare, bagera
Bagire, bagara
euskera askatzeko
oraintxe dugu aukera

Araban bagare...

Herri bat dugu
osatzen
eta gure zabarkeriz
ez daigun utzi hondatzen.

Bagare, bagera,
bagire, bagara
Euskadi askatzeko
oraintxe dugu aukera.