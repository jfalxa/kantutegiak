---
id: tx-238
izenburua: Kattalina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pa5ysNOxjgA
---

Elizako bort'aitzinin bada haurñi bat ützirik
Ezpada ihur jabetzen hartuko dugu gogotik,
Izanen düzu Allande, ükhen eztügün alhaba, 
Gure semeki dateke, hurak bezala bezala 
Neskatxuna eijer bat da, zelüko izarren pare, 
Hoztürik üzteko orde hartzen dügü, orai, gure.
Aski handitü onduan, erranen deiogü egia . 
Nahi-dina eginen dü, berak hartatüz bizia 
lzan gitin Atet'Ama. zügünki handi dadin, 
Gaixo haurñi horentako. gure hiru semen artin.
Emanen deiogu izen bat: Kattalina deithuko
Batheiaturik denez, gero dügü ikhusiko. 
Guatzen aren etxerat. Kattalina ttipiarekin, 
lkhus dezen gure semek beren lagun bem hori.