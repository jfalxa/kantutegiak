---
id: tx-1061
izenburua: Euskharan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CjIipZh1kCA
---

Hitz asko galtzen dira egunero hirietan
Hitz asko falta dira oraindik karriketan
Falta zaizkigun hitzak aurkitzeko asmoz
Astintzen has gaitezen gure 'linguae navarrorum-a'
EUSKARA EUSKHARANEN GUZTIOK LOTUTA
EUSKARA EUSKHARANEN KULTURA GUZTIONA
EUSKARA EUSKHARANEN ATEAK IREKI DIRA
EUSKARA EUSKHARANEN ETOR ZAITEZ GOZATZERA
Aurkitu nahi nituzke galdutako esaldiak
Bilatuko ditugu gure ezpainen artean
Korapilatu eta ekingo diogu gogoz
Astintzen has gaitezen gure 'linguae navarrorum-a'
Hura zurekin zaizkit nirekin
Euskara atera
Zaizu harekin duzu gurekin
Euskara kalera