---
id: tx-1000
izenburua: Nora Zoazen Erran Daztazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VTNa1Eg9YVs
---

#saveyourinternet

Nora zoazen erran daztazu,

Andre Dena Maria.

Ni banoaie Belenera,

Beleneko hirira,

Beleneko hirira eta

seroren ospitalera.


Nora zoazen erran daztazu,

Andre Dena Maria.


Arara bidea luze dago,

erortzeko peligro da.

Ama Birjina erditzen sarri

borda idiki batean,

borda idiki batean eta

idi-ganbela batean.

Arara bidea luze dago,

erortzeko peligro da.


Amak Semea maiolatzen du

harri otzaren gainean,

idiak atsez berotzen eta

mandoak ostiko emaiten;

idia benedikatu eta

mandoa madarikatu.


Amak Semea maiolatzen du

harri otzaren gainean.


Nora zoazen erran daztazu,

Andre Dena Maria.

Ni banoaie Belenera,

Beleneko hirira,

Beleneko hirira eta

seroren ospitalera.


Nora zoazen erran daztazu,

Andre Dena Maria.