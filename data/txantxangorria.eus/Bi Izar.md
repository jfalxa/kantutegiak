---
id: tx-1527
izenburua: Bi Izar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ji93URIgFjM
---

Musika: Xabier Zabala

Pirritx eta Porrotx pailazoek atera duten Eskerrik asko izeneko diskoan ere parte hartu zuen Ane Perezek. Istorio erreala kontatu dugu, pertsonaia errealekin azaldu digu Pirritxek "eta Anek, Anerena egiten du.
Lehen unetik, energia eta indar berezia trasmititu zien elgoibartarrak. Aneri aurpegira begiratuta, bi izar (bi begi distiratsu) eta ekia (irria) ikusi genituen. Pailazoarena egitera sartu ginen, eta berak egin zigun guri pailazoarena Ane ez da gaixo bat, sendatzeko eta bizitzeko ilusioa duen pirata bat baizik. Beragan inspiratzen dira aurtengo ikuskizuna eta Mari Mototsen papera.
Diskoko lehen abestia, Bi Izar izenekoa ere, Ane ezagutu ondoren sortu zuten pailazoek. Honez gain, Aneren ahotsa ipuinetan eta sendatu naiz eta Bokalen koroa abestietan ere badago.