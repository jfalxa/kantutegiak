---
id: tx-1467
izenburua: Ilundu Aurretik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B6mZYeIX8Dw
---

Gaur goizean, zu aurkitu zaitut
nola esan, nola adierazi
ni han nintzen, bakarrik.

Hau duk hau, handik eta hemendikan
ikusi zaitut eta
zu bakarrik zinen berebil handi hartara sartzen
ez zegoen inor barruan, bainan hau berdin da
norbait itxoiten ariko zinen
baina norbait hori ni ez nintzelakoan, zihur nago

Bestela zergaitikan, atzo arratsean
ez zenidan deitu, zergaitikan
esplika iezaidazu, ilundu aurretik
ilundu aurretik ilundu aurretik.