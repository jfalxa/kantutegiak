---
id: tx-75
izenburua: Ixilduak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9Ni-1PcFhNk
---

@julen3090 
JULENen Ixilduak kantaren bideoa. 2021eko urrian Bakio eta Busturian grabatua. Naia Aizpuruak zuzendua. 

Vídeo musical de Ixilduak de JULEN. Grabado en Bakio y Busturia en octubre de 2021. Dirigido por Naia Aizpurua.

Music video of JULEN's Ixilduak song, recorded on October of 2021. Directed by Naia Aizpurua.

2020ko otsailaren 29an Gaztain Estudioetan grabatua.
Grabado el 29 de febrero de 2020 en Gaztain Estudioak por Eñaut Gaztañaga.

Zuzendari laguntzailea: Uxue Morales
Kamera: Angello Eloizaga
DOP: Iñaki Lizarraga
Produkzioa: Natalia Arteche
Elektrikoak: Ander Zelaia, Diego Álvarez de Arcaya

Hitzak | Letra de la canción | Lyrics:

Ixilduak diren hitzen artean 
Ukabilak altxatzen dira 
Emakumez betetako kaleak

Ixilduak diren ume oihuak 
Urrutitik entzuten dira 
Anbulantzien sirena doinuak 

Ez duzu sentitzen 
Ez duzu sentitzen

Ta bapatean
Dena beltza da
Ezintasuna
Gailentzen da

Ixilduak diren eraso kutsuak 
Azalera ateratzen dira 
Lehortutako malkoak

Ez duzu sentitzen 
Ez duzu sentitzen 

Ta bapatean
Dena beltza da
Ezintasuna
Gailentzen da

Ohohohoh… (ixilduak) (ixil) (ixilduak…) 

Ta bapatean...