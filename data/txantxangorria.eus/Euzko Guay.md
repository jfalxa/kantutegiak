---
id: tx-2050
izenburua: Euzko Guay
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V59fYFSyI6U
---

Euzko Mcdonalds disko festa
Audi A5 ta spa
kezkatutea ei da kezka

Telebista irratietan
txibiriteri kantetan
Herri zapaldu honetan

Txarri zaratari dirulaguntzak 
EITBn top ten 
Geuk gantxoa sartu arten 

Euskal idazle sarituez 
Yankien kopia lez
Hobe idatzi portugeses

Euskal kulturako enpresak
Eurotuta dauen amets bat
boteredunen printzesak

Euzko guay, euzko guay....

Ikastola kolegio vasco
Euskal hiria Consumer da
Gu ezkoaz kajatik zer ba?

Konpromisu gabeko kantu
Mezu hutsez beteak
Ezer esaten ez dabenak