---
id: tx-3273
izenburua: Zurekin -Mikel Urdangarin-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j7FCfF-iFfg
---

Zurekin egon nahi nuke
hondartzako itzalean
marteko iturrietan
sumendien azalean
maitasunaren kalean
gauaren lehen portalean
egotea delitu den audientzi nazionalean

Ez dakit joango zaren edo zatozen bueltan
ametsak kondenak diren amets gaizto honetan (Bis)

Zurekin egin nahi nuke
mundu bat ondarralean
barrikada bat elizan
altare bat magalean
maitasunaren kalean
gauaren lehen portalean
egotea delitu den audientzi nazionalean

Ez dakit joango zaren edo zatozen bueltan
ametsak kondenak diren amets gaizto honetan (Bis)

......

Zurekin izan nahi nuke
liburu bat apalean
korsario bat baineran
olatu bat kresalean
maitasunaren kalean
gauaren lehen portalean
egotea delitu den audientzi nazionalean
gauaren lehen portalean
egotea delitu den audientzi nazionalean

Ez dakit joango zaren edo zatozen bueltan
ametsak kondenak diren amets gaizto honetan