---
id: tx-2730
izenburua: Giza Eskubideak Dira Zure Tresnarik Onena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BNz1BE9u2Ao
---

Haurren giza eskubideei buruzko abestia, Maite Ruiz de Austrik zuzendu eta Iñigo Silvak ekoitzitako LUCIUS DUMBEN BEREBIZIKO BIDAIA animazio filmeko Jatorrizko Soinu Bandakoa.
Euskaraz egokitu eta zuzendu Miren Aranburuk egin du. Parte hartzen duten abeslariak dira: Mikel Antero, Iker Bengotxea, Izaskun Zurutuza, Marga Altolagirre, Aitor Gabilondo, Sure Arakama, Ane Gabarain eta Miren Aranburu. Jatorrizko abestia Pedro Calero eta Mili Vizcainorena da.

ABESTIA

Zoriontsuago izateko tresnaren bila
Zer asmatu pentsatzen
Huts egin genuen ia

Baina sekretua
Barnean daukazu ta
Giza eskubideak
Zure tresna dira

Amets egiteko libre sortu gara
Lor dezagun munduan berdintasuna
Krudelkeriarik ez dezala inork jasan
Egiatan gu denok bat baino ez gara


Haurrak jolasten direnean
Fantasia bilakatzen da egia


Hauxe da hartu behar den bidea
Atseginagoa izan dadin mundua

Gurasorik gabe bizi diren haur asko
Gizarteak ahaztu ditu zeharo
Lanean leher eginda gau-ta egun
Babestu ditzagun

Bizitzak batzen gaitu denok
Denona dugu eguzkia
Entzun ondo, Ez ahaztu
Bat gara ni eta zu

Ni zu bezalakoa ez naizelako
Ez ni epaitu,ikusi harago
Hazi adimena,ez txikia izan
Berdin-berdinak bait gara
Denok funtsean

Balio handiko altxorra da haurtzaroa
Geroaren itxaropena bertan da
Ederra bezain hauskorra da ordea
Zaindu behar dugun aurreneko gauza.

Ikastea eskubidea da
Aukeratzeko nor izan bihar
Liburuak gure hegoak dira
Urruti bidaiatzeko bizitzan zehar

Entzun, izena izateko eskubidea duzu
Ez zara zenbaki bat,pertsona bat zara zu
Akatsak gaindituz aurrera doana
Gizatasuna hobetuz

Pertsona bat zara
Ez gauza bat
Izena duzu
Zoaz katerik gabe nortasunaren bila
Zu bezalakorik inon ez dago eta

Bizitzak batzen gaitu denok
Denona dugu eguzkia
Entzun ondo,ez ahaztu
Bat gara ni eta zu