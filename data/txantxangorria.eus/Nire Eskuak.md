---
id: tx-2988
izenburua: Nire Eskuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z5p6EFFs3tQ
---

Ametsak sehaskatu zituzten nire eskuek
umeak sehaskatu
Asko laztandu zute nire eskuek
lurra erein zuten

Egunak besarkatu dituzte nire eskuek
orduak besarkatu
Asko maitatu zuten nire eskuek
aurre egin zuten

Egun baten nire eskuak hustu egin ziren 
Isilune izugarriz bete egin ziren.

Baina egun baten nire ukabil itxiek indarra eta ametsak itzuli zituzten
Nire eskuekin gaur nire seme , seme-alabei idazten diet..

Egun baten nire eskuak ...