---
id: tx-159
izenburua: Gora Bilbo Kantari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4vIYDH5htmM
---

Adi! egon adi!,
hemen gatoz pozik,
eta gora gure,
Bilbo Kantari.
Adi! egon adi!,
hemen gatoz pozik,
eta gora gure,
Bilbo Kantari.

Kalian gora, eup!
Kalian behera, eup!
Jende guztia,
kanta ta kanta.
Kalian gora, eup!
Kalian behera, eup!
Jende guztia,
kanta ta kanta.

Kanta bat edo bi, edo_hamar,
tipi-tapa, tipi-tapa,
goazen aurrera.
Kanta bat edo bi, edo_hamar,
tipi-tapa, tipi-tapa,
goazen aurrera.

Hauspoaz, txistuaz,
o a capella bien medido,
hor dago,
ta go/ra alaitasuna.

Hauspoaz, txistuaz,
o a capella bien medido,
hor dago,
ta go/ra alaitasuna.

Adi! egon adi!,
hemen gatoz pozik,
eta gora gure,
Bilbo Kantari.
Adi! egon adi!,
hemen gatoz pozik,
eta gora gure,
Bilbo Kantari.