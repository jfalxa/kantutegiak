---
id: tx-2706
izenburua: San Antolin Ereserkia Plentzia Kantari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8jb1hz8Eugk
---

(Zortziko) 
La la la ra ra la la
la la la ra ra la la la ra
la la la ra ra la la
la la la ra ra la

1. Jai giroan murgilduta
aurresku eta arin-arin,
suzirien artean
kantari, txistu ta danbolin.
Zer dugun ospatzen?
Gaur da SAN ANTOLIN!

(Habanera) 
2.Plentzia, esmeralda bitxia,
zilarrezko ibaia
itsaso harrotura isuria.
Zu, kolorezko melodia
altzoa ta habia.

(Zortziko) 
3. Zatozte plentziarrok,
baita udatiar ta etorkin,
jai giroan elkartuta
patroiaren omenarekin:
guztiok bat egin,
gaur da SAN ANTOLIN!

(Habanera)
Plentzia, esmeralda bitxia...

La ra la ra la ra ra ra ra
la ra la ra la ra ra ra ra
la ra la ra la ra ra ra ra
la ra la ra la ra ra ra ra
guztiok bat egin:

GORA SAN ANTOLIN!