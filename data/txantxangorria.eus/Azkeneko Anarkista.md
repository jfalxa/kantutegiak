---
id: tx-415
izenburua: Azkeneko Anarkista
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O54r0gqDgn4
---

Hire lagunarik hoberena ardo botila korriente bat. Estazioaren itzalean pasatzen dituk egunak.

Gaztetatik hasi hintzen hire gogoz borroka egiten.
Bainan erantzunik aurkitu gabe. Bainan erantzunik aurkitu gabe.

Zertarako bizi ote geran galdetzen diok hire buruari. Zertarako hainbeste neke gizon bat libre izateko.

Nahiago duk ardo botila bat itxurazko mila hitz baino.
Hi askatasunean bizi haiz prezio gabeko askatasuna.

Zertarako bizi ote geran…

Berrogeitamar urte badituk bizkarrean zama handiegia. Hezur guztiak mindurik dauzkak hezur guztiak mindurik.

Hamaika aldiz ikusten dituk hire lagun zaharrak pasatzen. Guztiak gizen gizenak daude sistemak jaten ematen die.

Zertarako bizi ote geran…