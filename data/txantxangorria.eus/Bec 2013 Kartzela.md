---
id: tx-1755
izenburua: Bec 2013 Kartzela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RtwYsDaF5D4
---

Frankismoan ibilia
bide ezkututan barna
Espainiko espetxetan
utzi dut nere aztarna
Neretzat ere jo zuten
ateratzeko alarma 
ta geroztik gozatu dut
bizi askearen xarma 
Baina patuak berriro 
mamu zaharrera narama 
sofan eserita gaude
ni ta umearen ama
gaur gure umea baita
auzitegira doana
gaur gure umea baita
auzitegira doana

Amak korapilatua
ta goibel dauka barrena
ni berriz ni zulo berean
egin behar dut errena
horrelakotan sentitzen
da bat hutsaren urrena
zer esanik ez badaki
audientziak dakarrena
kartzela nik igaro nuen
urte pila nabarmena
ta orain bete behar dut
senarra ta aitarena
eta egia esan ez dakit
bitan zein den okerrena
eta egia esan ez dakit
bitan zein den okerrena

Nekeza bat asumitzen
bizitzak ipini rola
nik ere negar egiten dut
eta galtzen dut kontrola
eguna ailegatu da
espero gabe inola
semea audientziara
doa borborka odola
nahiz ta begiratzen didan
fuerte ia ez axola
nik eskutik heldu diot
umea zenean nola
semea jakin ezazu
zurekin harro nagola
semea jakin ezazu
zurekin harro nagola