---
id: tx-1090
izenburua: Donostiako Hiru Damatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/igsh9Bue56c
---

Donostiako hiru damatxo
Errenderian dendari. (Bis)
Josten ere badakite, baina
ardoa edaten hobeki.
Eta kriskitin, kraskitin,
arrosa krabelin,
ardoa edaten hobeki.

Donostiako Gaztelupeko
sagardoaren goxua. (Bis)
Hantxen edaten ari nintzala
hautsi zitzaidan basua.
Eta kriskitin, kraskitin,
arrosa krabelin,
basua kristalezkua.

Donostiako neskatxatxoak
kalera nahi dutenean: (Bis)
Ama piperrikez dago eta
banoa salto batean.
Eta kriskitin, kraskitin,
arrosa krabelin,
banoa salto batean.

Arrosatxoak bost hosto ditu,
krabelintxoak hamabi, (Bis)
Mari Josepa nahi duen horrek
eska bihotza amari.
Eta kriskitin, kraskitin,
arrosa krabelin,
eska bihotza amari.

Donostiako hiru damatxo
hirurak gona gorriak. (Bis)
Sartutzen dira tabernara ta
irtetzen dira hordiak
eta kriskitin, kraskitin
arrosa krabelin,
irtetzen dira hordiak.

Donostiako hiru damatxo
ein omen dute apustu. (Bis)
Zeinek ardo gehiago edanda,
zein gutxiago mozkortu.
Eta kriskitin, kraskitin,
arrosa krabelin,
zein gutxiago mozkortu.