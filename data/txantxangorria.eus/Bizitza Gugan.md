---
id: tx-1081
izenburua: Bizitza Gugan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8j3IEXgbRxk
---

Gugan bizitza gugan
gure eskuetan loratzean
Non gu, gu garen mundu
mundu segundu bakoitzean.

Bere lurrazalean
gure gorputzean.
Pasa bizi-arnasa
eskuetatik eskuetara.

Jende, kultura mende
guztiak geure eskuetan.

Jaso dugun mundua
datozenei pasa.

Pasa bizi-arnasa
eskuetatik eskuetara.

Gugan bizitza gugan
gure eskuetan loratzean
Non gu, gu garen mundu
mundu segundu bakoitzean.

Bere lurrazalean
gure gorputzean.

Gugan bizitza gugan.