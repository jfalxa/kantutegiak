---
id: tx-2780
izenburua: Aurkezpen Bideoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pxiz_48ucIQ
---

Nahiz eta egon, badaude hamaika modu erak
sortuko honen bezala kanta honen antzera
zortzi lagun ezberdin baina proiektu bera
tabernak hustu eta plazak betetzera

Asetzen saiatuaz musikari gosea
pausoz aurrera goaz sortuz gure bideak
bidaia hasi dugu ilusioz betea
jo muga dugu gerri oro astintzea