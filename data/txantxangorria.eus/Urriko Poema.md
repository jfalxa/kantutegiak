---
id: tx-3378
izenburua: Urriko Poema
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0uJDZUezWYY
---

Iritsi dira notiziak
iritsi dira eskutitzak
baina ez zureak.

Igaro ziren hegazkinak,
erre zituzten zubi denak,
isildu bazterrak.

Galdetu diet hegaberei,
galdetu pago biluziei,
ez dakite ezer.

Etorri hona,
Etorri laster niregana.
Izotza dut zainetan.

Soineko gorria iazko Sanjuanetan.
Suaren beroa gure gorputzetan.
Amantala zabaldu da zelaian.
Argazki zaharra ene oroimenean.