---
id: tx-2307
izenburua: Ai Zer Plazerra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Exn1xtVEC_k
---

1.Ai, zer plazerra, bost urten buruan sor lekurat itzultzia,
ene herria etsitu nian, zure berriz ikustia.
Aski sufrituz nik anitzetan, desir nukian hiltzia,
ez bazintudan hain maite uken, oi, ama Euskal Herria.

2.Anitzen gisa partitu nintzan, etsaien gudukatzera,
gogoa bero, bihotza laxo, eta kasik alagera.
Ez bainakian, oarino orduan, zer zen amaren beharra,
hari pentsa eta biga bostetan, aiher zitazun nigarra.

3.Erresinola kantari eder libertadean denean,
baina tristuran higatzez doa kaloia baten barnean.
Gu ere ama hala gintuzun, tirano haien artean.
Zure ametsa kontsolagarri, tristura handirenean.