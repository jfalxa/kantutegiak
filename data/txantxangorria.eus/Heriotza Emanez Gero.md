---
id: tx-1179
izenburua: Heriotza Emanez Gero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AY6U2tY0FuY
---

Hiltzen banaiz edo hiltzen banaute
Gutun bat utziko dut zuretzako
Maitia nahiz berandu iritsi
Egunero zu nitaz oroitzeko

Inoiz preso eramaten banaute
Espetxean ere maiteko zaitut
Barrengo nere bihotza odolduz
Paretan zure izena idatziko dut

Heriotza emanez gero
Zure bihotza baztartzean
Ni baino zoriontsurik ez da
Izango hilez gero zure besoetan

Itsasoratzen banaiz laztana
Ta denbora luzez ikusi ezinda (ezin eta)
Ni non nahi aurkitzen naizela
Nere maitasuna zuretzako da

Nahiz urruti nahiz urteak joan
Zure begi ederrak ikusi gabe
Neure barrengo sentimenduak
Bizirik betiko daukat hala ere.