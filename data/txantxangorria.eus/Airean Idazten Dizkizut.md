---
id: tx-235
izenburua: Airean Idazten Dizkizut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cj8Qy_L-nKo
---

Itsasoko olatuen gainean

nabigatzen dihardut

harkaitz honetan hotz dago

urrunean

argi gorridun itsasontziak

agur esaten dit

eta hemen nago ni

agur esaten

zerura begira

airean idazten

airean

hotz dago.