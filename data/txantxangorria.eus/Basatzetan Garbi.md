---
id: tx-401
izenburua: Basatzetan Garbi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y-HLIcjVdDQ
---

Dotore esnatu zara
Urre gorrizko adar nagiak
Txanpainez eder aurpegia
Egurra eman behar duzu
Sinestarazi ez dakizuna
Ortzadar bihurtu behazuna
Amets gaiztoa zaitut
Goiz orotako ispilua
Itzuli nazazu ahalketua
Ahalketua
Ubela dut maailean
Neronek ukabilkatua
Prediku maiztuen ordaina
Inoiz kargutuko ote naiz
Bakantasunetik ihesian
Arrunt ikusgarri naizena
Amets gaiztoa zaitut
Goiz orotako ispilua
Itzuli nazazu ahalketua
Ahalketua
Amets gaiztoa zaitut
Goiz orotako ispilua
Itzuli nazazu ahalketua
Ahalketua