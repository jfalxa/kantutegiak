---
id: tx-2770
izenburua: Atzeko Sutea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U9EOORzIOf4
---

Ilusioak isurtzen ari dira asero
izerdi ta edari-alkoholikoen lokaletan
ilargi-eremuan bihurtzen ari naiz ero
logika daramat izter-pareen artean

Alde batetik bestera bultzaka naramate
aupegian isladatzen zait lelo tankera
bila nazazu, nire begietan dago aukera
lore artean lurreratzeko biok batera

Gordeta daukat sapaian zure eztia
desioaren trena gainetik igaro zait
inpultsoari ezin izan diot jarri hesia
oin-hutsik nabil dantzan kristaletan
zure zai

Gutxiren esku dago aldatzea
garraiotako istorioen amaiera
ispiluetako begiradek ez bait dute
eraginik itzaltzeko atzeko sutean