---
id: tx-1150
izenburua: Dantzan Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XoVuSynQgXU
---

Takoiak eta rimmela, ostiral gaua da eta. 
Asteburua hasi da, zure ihes bidea. 
Itzalen mundu honetan, gauaren erregina zara. 

Dantzaleku erdian geldituko zaituenik ez da, 
bihar arte dantzan, dantzan! 
Pista gau honetan da zure xarmen jauregia 
eta noiz arte dantzan, dantzan? 

Alkohola eta taxiak, errutinaren isla. 
Eguzkiarekin batera errealitatea. 
Itzalen mundu honetan, gauaren erregina zara. 

Dantzaleku erdian geldituko zaituenik ez da, 
bihar arte dantzan, dantzan! 
Pista gau honetan da zure xarmen jauregia 
eta noiz arte dantzan, dantzan? 

Gauaren erregina zarelako, 
takoiak eta rimmela. 
Gauaren erregina zarelako, 
alkohola eta taxiak. 
Gauaren erregina zarelako, 
dantzan, dantzan! 

Dantzaleku erdian geldituko zaituenik ez da, 
bihar arte dantzan, dantzan! 
Pista gau honetan da zure xarmen jauregia 
eta noiz arte dantzan, dantzan? 

Eta noiz arte?