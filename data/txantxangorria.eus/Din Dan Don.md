---
id: tx-2560
izenburua: Din Dan Don
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/clxOJYOWIwo
---

DIN DON DAN
GABONAK DIRA
TXISTUA, DANBORRA
GOAZEN JOTZERA
BAI!

DIN DON DAN
GABONAK DIRA
TXISTUA, DANBORRA
GOAZEN BELENA
BAI!