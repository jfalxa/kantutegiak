---
id: tx-217
izenburua: Laino Ilunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gxxqdyku_X8
---

Laino ilunak ikusten
Goizean goizez zeruan
Leihoak bustirik daude
Hotzak hiltzen jeiki naiz
Berririk ez dakar egun honek
Kezka gabe hartuko det
Beste bat lasai lasai
Oraindik erdi lotan naiz
Norbaitek ba ote du esan
Gaur zerbait gertatuko dan
Gure bizimoduak poztutzeko
Edo agian aldatzeko
Eguzkia alde batetik irten
Eta bestetik sartzen da
Baña bien bitartean
Inork ez daki zertan dan
Bakarrik berriz etorriko dela, jaja
Laino ilunak, laino ilunak ikusten...
Goizean goizez zeruan
Leihoak bustirik daude
Hotzak hiltzen jeiki naiz
Egun berri bat daukagu hemen
Orain bakarrik behar dugu
Itxaropena zaindu eta inoiz ez galdu
Egun denak ez dira berdin!!!