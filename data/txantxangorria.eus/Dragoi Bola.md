---
id: tx-2463
izenburua: Dragoi Bola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sY0h-MqU_ho
---

Goazen Lagunok!
Amets betean
Dragoiaren Bola bila batera

Orain badakit
nongo zuloan
dagoen altxorrik ederrena.
Gure mundu hau irla haundi bat da
eta hortxe ostenduta altxor eder bat.

Bihotzean badut nik amets on ederrik
ta ez da eguzkirik distiratsuagorik,
goazen bihotza betean
aurki dezagun, goazen, goazen, goazen, goazen, goazen.

Tximistaren bidetik Dragoi Bola bila,
itsaso eta lurrez bihotz bete ametsez
ta altxorra geuretzat izango da.

Ekin lagun bideari,
Son Gokurekin ez da beldurrik,
zeru goian nahiz lurrean
hegan eginez laster altxorra dugu.