---
id: tx-1489
izenburua: Bazkaldurikan Bapo Bapo Oskorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yhLyWX15KfM
---

Bazkaldurikan bapo-bapo
goazen bai plazara,
hantxen egingo degu
gogotik algara.

Oraindikan, mutilak
tragotxo bat bota,
bota, bota beste bat,
oraindikan bota.

Tragoarekin zaigu, zaigu,
barrena pixkortu,
ez zan gizon makala
hau zuena sortu.

Oraindikan, neskatxak
tragotxo bat bota,
bota, bota beste bat,
oraindikan bota.

Oraindikan, mutilak
tragotxo bat bota,
bota, bota beste bat,
oraindikan bota.