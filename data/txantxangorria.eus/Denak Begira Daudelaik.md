---
id: tx-10
izenburua: Denak Begira Daudelaik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EWveifYXh1U
---

Kantua: Olaia Inziarte
Grabaketa eta Produkzioa: Josu Erviti eta Olaia Inziarte
Musikariak: Josu Erviti (bateria), Ander Unzaga (teklatuak), Joseba San Sebastian (baxua) eta Eric Deza (kitarrak).
-------------------------------------------------------------------
Zuzendaria @an_txoa
Argazki zuzendaria @asier03
Ekoizpen burua @ellenvald
Makillajea eta ile-apainketa @lihoa.art
Eider Etxeandia @heitxi
-------------------------------------------------------------------
-------------------------------------------------------------------

HITZAK:

Denak begira daudelaik
ixilik gelditzen denaren elegantziaz
armairua erropa beltzez betea dut eta aurpegia xuria sorora jautsi dira beleak eta ni zerura begira

Azken ordua balitz
atia nori
ireki pentsatzen ari naiz

Arrastaka arrakasta irinak sobran ta falta duzue mamia
ferraturiko antzarek liberatu egin dituzte zure zaldiak
deusere ez dute baino ze goxo hartu duten libertadia
deusere ez dut baino ez didazu berriz giltzaz itxiko atia

Badakienak badaki: ni ez nintzake honetan sartuko
bazterrak nahasteko ez balitz

A ze mundua iragarri den
emaidazu besarkada bat
agintarien dolua
hemen erne segitzen duzulako

Azken ordua balitz atia nori
ireki pentsatzen ari naiz

Jainko maitiak egin banindu zeruetako giltzari
azken orduan jakingo nuke atia nori ireki

Eguzkia atera da zutaz galdetzian