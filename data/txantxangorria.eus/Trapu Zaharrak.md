---
id: tx-2879
izenburua: Trapu Zaharrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yG5MDGcMNAE
---

Egunez trapu biltzen
gabean dantzan lehertzen
beti umore onean
degu pasatzen,
eta goizean goiz
kalean irten da
lo goxoan daudenak
esnatzen dira.


Trapuak! Trapu zaharrak!


Atera atera, trapuak saltzera
nik erosten ditut
modu onean
arditian prakak kuartuan albarkak
ta burni zaharrak txanponean

Erosten ditut trapuak
praka zahar urratuak
gona gorri zaharrak eta
atorratxuak.
Jo zak zurrumuzki
atabak soinu
dantzan egiteko
beti erana.


Eltze elbarriturik
pertza zulorik duenik
arran parrillik edo
trebera zaharrik.
Inork saltzeko badauka
librako txanponean
Ramon Batistak
erosten ditu.