---
id: tx-681
izenburua: Ez Kanta Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YUfljdojF_A
---

Kaleak sutan jarri ziraden
eta zakurrak katerik gabe
beren bizitzen nagusi.
Ordu larriak urbil daude ta
libertadeko gorritasuna
zeñek ez du nai ikusi?

Iges doazen ur lodi hoiek
ludi osoa artuko dute,
aserrearen eskubideak mintzo
gogorrez altxa baitira.

Ez kanta, beltza, alperrik da ta,
indarren kontra jaso indarra
zuen bizitza oso motza da
egin zazute librea.

Goseen Harlem, doñu zakarra,
izen gabeko mixeri zarra,
bular legorrak zintzilik eta
irentsitako karraxi latza.

Ez kanta, beltza, alperrik ez kanta,
indarren kontra jaso indarra,
Moises zerutik jetxiko ez da,
urreagatik saldu da.

Baiñan zu, beltza, asten bazera,
gu ere beltzak izango gera,
bein betiko burruka hortan
nausi berdiñen legeen kontra.

Ez kanta, beltza, berriz ez kanta,
itxi aboak, jaso ezpata,
gure bizitza gurea da ta,
iñork ez beza lapurtu.