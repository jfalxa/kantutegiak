---
id: tx-1238
izenburua: Dena Ondo Zihoan, Argia Piztu Den Arte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/soG_uToWpTE
---

Trago bat eta beste trago bat,
gure kaletan barrena
(e)ta piztea da alkoholak eta
sexu grinak dakarrena.
Zure etxera nire etxera
betiko gauzak aurrena,
zure logelan sartu gara-eta
hara hemen ondorena.
Argia zegoen itzalita (e)ta
bertan biluztu naiz dena,
(e)ta bat-batean piztu egin da
argi txiki xumeena
nire gerrira begira zaude,
ez naiz uste zenuena.
 
 
Estali arte izan dugunak
nahiko irri nahiko broma,
ahorik aho pasa dugunak
Gin-tonic barruko horma.
Bular parea zut neukan eta
zuk eman didazu forma,
galtza estuak ne(ra)bilzkin arren
ez takoi eta ez gona.
Hau eta hura naiz, hartzen ari naiz
zenbait botika hormona,
gizona eta andrea nauzu,
ez andrea ez gizona.
Nire inon ez egon nahia ote
da zure ezinegona.

Gizarte honen estereotipo
(e)ta gizarte honen usteen
tranpan zu ari zara erortzen,
eta hori ez dizut uzten.
Zuk badakizu nola zizelka
eta marraztu gaituzten,
ze inporta du gaur ari bainaiz
zure aurrean biluzten,
eta neroni ni neu naizena
hori bakarrik gorpuzten.
Nire gerripe(ko) zintzilikako
desira ari da puzten,
itzali ezazu argia eta
utzi azalari ikusten.