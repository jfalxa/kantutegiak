---
id: tx-2807
izenburua: Amaiur Gazteluko Baltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U32I87fDmpo
---

Amayur gaztelu beltz ori
-berreun gudari oso sumin-
zaintzen zaituben zaldun onak
Naparra-aldez egin dabe zin.

Izkillu gorriz zenbat gazte
bildur-bako menditar lerden.
Eta horreik, Yatsu jaun horreik,
Jabier'eko zaldun gure.

Ikurrin bat -kate ta lili-
torre goitijan zabal dago.
Bera salduko daun semerik
mendi honetan ezta jayo.

Amayur'ko ate-zain horrek
zidar-turutaz oyu egik.
Baztan ibarran zenbat etsai,
arrotz-gabe eztago mendirik!

Etsiturik Amayur dauko
Miranda'ko konde Españarrak.
Horreik bai burnizko janzkijak,
ta urrezko ezpata-sagarrak!

Berekin dator, bai berekin,
Lerin'go eto horren semia.
Txakur txarrak jango al dabe,
herri-bako zaldun dongia.

Goiko aldetik hasten dira,
hasten dira subaga-otsez.
Horma-kontretan zenbat zurgu,
eta gezi zorrotzak airez!

Baña torrean zabal dabil,
ikurrin bat -kate ta lili-.
Nok zapaldu ete dagikez
berrehun gudari orok zoli?

Amayur'ko gaztelu baltza
-iausi yatzuz torre goitijak-.
Baña, arrotza, etzadi geldu,
napar-seme dira guztijak.

Harresi gorri, zubi ausi,
-zein gitxi diran zaldun horreik.
Erijo, samurño zakije
aberri-min baitabiltz eureik.

Miranda'ko konde gaizto horrek
jo egixuz zidar-turutak.
Ikurrin bat -kate ta lili-
eztau laztanduko axeak.

Berrehun gudari oso sumin
gaztelu-pian dagoz illik.
Ordutik ona -zenbat laño-,
Naparruan ezta aberririk.