---
id: tx-700
izenburua: Agurrik Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6D4wWQP4ydc
---

Ilunaren etorreran joaten dira
barroetatik ihesi
egunsentirarte itxaroteko gai
izan ez diren ametsak...
Zurekin!

Orduan galtzen
itzaltzen dira iraultzak kartzeletan
borrokarik egin gabe

Orduan, agurtzean galtzen
itotzen dira ametsak kartzeletan
borrokarik egin gabe

Agurtzeko garaian bagenekien
ez genuela ikasi
Maitasunari
agur esaten eta
bihotza kartxelan uzten...
Zurekin!

Urteak
zugana joan eta zugandik joaten
esperantzari helduta
tristezia irribarreekin estaltzen
errepidean galduta

Eta hilabeteek badute egun bat
zoriontsua naizena:
Zu ikusten zaitudan eguna da!