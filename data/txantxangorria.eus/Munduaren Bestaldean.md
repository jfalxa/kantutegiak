---
id: tx-1364
izenburua: Munduaren Bestaldean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3z7o5Ul_vQ8
---

Lauroba - IV
Munduaren Bestaldean

Osatzen dute:
Iker & Haritz Lauroba
Andoni Garcia
Ane Bastida
Iñigo Asensio

Grabazio, edizio eta zuzendaritza:
Roman Goenaga

Cast:
Laida Goenaga


KONTRATAZIO IREKIA: KRKT Prods.
608 240 824
krkt@krktproducciones.com


Lekuan geldi, mugitu gabe,
haizeak ere ez zaitzan eraitsi.
Babes bila, bakiak egin,
burua makur,
nondik ihesi.
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.
Ze zaila den lurretik bereizi,
hegan egitea berunezko zapata hoiekin.
Nahi zenuena non gelditu ote da,
zenbat egiteko, zenbat bizitzeko.
Bakardadean igarotzen dituzun,
minutu eta ordu, egun eta gau…
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.
Ze zaila den lurretik bereizi,
hegan egitea berunezko zapata hoiekin.
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.
Lekuan geldi, mugitu gabe,
haizeak ere ez zaitzan eraitsi.
Babes bila, lubakiak egin,
burua makur,
nondik ihesi.
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.
Ze zaila den lurretik bereizi,
hegan egitea berunezko zapata hoiekin.
Nahi zenuena non gelditu ote da,
zenbat egiteko, zenbat bizitzeko.
Bakardadean igarotzen dituzun,

minutu eta ordu, egun eta gau…
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.
Ze zaila den lurretik bereizi,
hegan egitea berunezko zapata hoiekin.
Ez dakit nola bizi zaitezkeen,
buruz behera munduaren bestaldean.