---
id: tx-1909
izenburua: Argi Zirrinta Zenian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LFXyXFoj2ac
---

Argi zirrinta zenian
Kalian, Donibane hirian,
Treina hartzera nindoan
Gogoan pena nuela alan
Nexkatxaño bat bidian
Abian ikusi nuenian,
Haren zama nik hartzian, eskian,
Hunki nuen erian...

Ai, ai, ai nexkatxa,
Goizeko lanbrotsan,
Zure soak zauztan
Goi-izarrak eman,
Barnea dirdiran,
Bai eta ikaran
Jarri nintzan bertan
Zure ondo hartan.

Baionarako zirare, ni ere,
Baditut hogoi urte...
Uztailez ditut nik bete,
Zuk ote baduzuia hoinbeste?
"Zer adin dutan jakitek,
Zer dikek hiretako egite.
Xuxenago litzakidek, entzute,
Bai ala ez naukan maite..."

Zure ta ene ixtorio, maiteño,
Izan zen holaxeño
Ezkondu eta ondoko urteko,
Baginuen ñimiño.
Geroztik orai artino, hiruño
Jin zaizkigu oraino,
Bainan dolurik hortako, nehoizño,
Etzaut jin gogoraino.