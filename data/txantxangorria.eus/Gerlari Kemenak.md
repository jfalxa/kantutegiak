---
id: tx-615
izenburua: Gerlari Kemenak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mJAcDZcunq4
---

Gerlari kemenak (eh!)
Kalean hezitako jende gaztea, sua piztera (eh!)
Gure akatsetatik ikasita egin genuen aurrera (eh!)
Bere hankatartean murgiltzeak dakarren zoramena
Negar ta barreak
Ni naiz nire askatasunaren jabea

Aurrera goazela
Behetik begira daukagu jendea
Inbidia jaten dutenak tripako miñakin joaten dira komunera
Bala bategaz, making your headshot
Euskarak tantaka arrakala zabalak ta zaharrak dakartza zuon metralletan

Erantsi karetak, dekozuezan gezur danak errementa
Oindik trankil gabizela; baie jodite arazukaz beteta
Superbibentxixe topaten nau sistema honetan
Haizie be usteldute dakule gure planetan
Superheroiak gariela zarratzen tabernak
Microphone checka, gure letrak: gure tresna


(Ayayayayay)
Klaru dakot: zer gudotan ta zelan bizi
(Ayayayayay)
Iñok etzu eskatu zein den zure iritzi
Itxi trankil ta segi zurera mutil


Gerlari kemenak (eh!)
Kalean hezitako jende gaztea, sua piztera (eh!)
Gure akatsetatik ikasita egin genuen aurrera (eh!)
Bere hankatartean murgiltzeak dakarren zoramena
Negar ta barreak
Ni naiz nire askatasunaren jabea

Trago garratz ta zorrotzen zalantzan
Hitzen baldintzak, larrosen arantzak, miñ atsegiña: nire maitte kuttuna
Norantza? Ba horrantza
Orratzak blai daude denboraren saltsan
Jakin deizun ez dela egin ezina nik oin dela asko ematen nizuna...

(Uah) Iñoiz enaiz sentitu gaur sentitzen naizen moduan
Mundu ziztrin hau jateko ordua heldu da;
Seguru nau
Burua zeruan
Baina hankak beti lurrean
Eskutuan gordetzea gure arteko sekretuak


(Ayayayayay)
Klaru dakot: zer gudotan ta zelan bizi
(Ayayayayay)
Iñok etzu eskatu zein den zure iritzi
Itxi trankil ta segi zurera mutil

[Chorus: xSakara]

Gerlari kemenak (eh!)
Kalean hezitako jende gaztea, sua piztera (eh!)
Gure akatsetatik ikasita egin genuen aurrera (eh!)
Bere hankatartean murgiltzeak dakarren zoramena
Negar ta barreak
Ni naiz nire askatasunaren jabea
(X2)