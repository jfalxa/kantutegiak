---
id: tx-2576
izenburua: Hamaika Egun, Hamaika Lagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RtoCx9BBvNc
---

2017ko Euskara eguna ospakizunean Deustuko Unibertsitatean prestaturiko bertsoa "Habanera" doinuaz.