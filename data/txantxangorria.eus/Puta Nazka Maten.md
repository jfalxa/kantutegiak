---
id: tx-74
izenburua: Puta Nazka Maten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W1yvxMP2OBI
---

@txiltxoko






#Txiltxoko #Altzibar 

Txiltxoko Records, Oiartzun ©₂₀₁₉


Puta nazka maten
Puta nazka maten
Betiko bostak gaude
Taberna zikin baten
Berriro gaude puta nazka maten

Herrira jetxi nintzen
Zurekin hobeto nintzen
Bajo al barrio ke no se
Zeba ostia jetxi nazen
Kotxikin esik iten
Bukatunin berrize

Aizkoakin mozteit enborra
Segakin mozteit belarra
Mierdakin mozteizut droga
Ardo beltza gosaltzen ez porrak
Uañe bilduyeu aparte, nik ezdut afaltzen, kilo ugari azket