---
id: tx-299
izenburua: Ernesto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0-EQBquJICM
---

Galtzairu oker ezina
Kondairaren erdi-mina
Gerlaz suntsitu beharra dela
Leheno gerlaz egina

Ezker nagia Europan
aberatsa ametsetan
Hiretzat erruz laudorioak
Parisko snak-barretan

Izarrak oro itzali
Vietnam dena kiskali
egia faltsuz ohi bezala
begiak zaizkit estali.

Uso bila kabiroia
basoan kexu lehoia
Bizi direnak jantziko ditek
hildakoaren koroia

Harri arotik burnikora
injustizia beti gora
Ez duk zilegi zapuztutzea
haundi-maundien loa

Posible dadin ezina
gizonaren larri-mina
nundikakoa ote diagu
paradisu baten grina

Hezur-haragi lurrean
Txe heriotzan atzean
Testamendua banatu ditek
kide-etsaiak zotzean