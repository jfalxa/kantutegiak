---
id: tx-1777
izenburua: Olaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E0FLZTz0X18
---

Amodiozko kanta bat egin nahi dut

Zaila egiten zait zu ez zaudenean

Erromantiko adierazi nahi dut burua

Hitz polit hauek bihotzez zuretzako

Maitea, maitea

betiereko, maitea

Nahiz eta urrun izan bolada honetan

Burdin hesiak ez dit aldenduko

Ni bakarrik sentitzen naiz gauero

Faltan bota zaitut, tira, luzatu

Eskuak, eskuak

Betiereko, eskuak

Ezagutu zintudanean hemen

Bihotza taupadaka nuen

Orain atxilotuta zaudela han

Nire taupadak bidaltzen dizkizut

Airetik, airetik,

Betiereko, airetik

Egunen batean etsi egiten badut

Hilda nagoelako izango da

Ispiluan begiratzen naizenean

Zure irudia besterik ez dago

Olaia, Olaia

Betiereko, Olaia

Olaia

Betiereko, Olaia

Betiereko, Olaia