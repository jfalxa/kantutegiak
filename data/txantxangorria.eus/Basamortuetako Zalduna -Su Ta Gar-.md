---
id: tx-3310
izenburua: Basamortuetako Zalduna -Su Ta Gar-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LIt-mEx7WkY
---

Zure bete behar bakarra
dala zaldian banatzea
ez esan, zu zara!!
zaldun!!

Gogoratzen zera gaztetan
zuk ere behar zenuenean
orain ez, zu zara!!
zaldun!!

Basamortuetako zalduna
zure gainean tortura
Basamortuetako zalduna
akabatuko zaitugu.

Zuk ezagutu zenuen hori
itzala ta guzti
beste batzuen kalterako
mundua aberastu.

Baina iritsiko da eguna
edozein agerpenetan
norbaitek hilko zaitu!!
zaldun!!

Basamortuetako zalduna
zure gainean tortura
Basamortuetako zalduna
akabatuko zaitugu.