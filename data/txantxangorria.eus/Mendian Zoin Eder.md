---
id: tx-2310
izenburua: Mendian Zoin Eder
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0r-_1uX94Lo
---

Mendian zoin den eder eper zango gorri!
Ez da behar fida/tu itxur’eder horri.
Ene maiteak ere besteak iduri:
niri hitzeman eta gibelaz itzuli.
Niri hitzeman eta gibelaz itzuli.


Gaztena, ontu eta lurrean ihaurri:
ene bihotza duzu zuganat erori,
zurea berriz aldiz harria iduri:
ene begi gaixoak nigarrez iturri.
Ene begi gaixoak nigarrez iturri.

Airea zahar eta kantorea berri:
ene maite pollita, zira xarmagarri:
kolore xuri-gorri, arrosa iduri,
mundurat jina zira ene ihar/garri.
Mundurat jina zira ene ihargarri.

Heldu naiz zureganat, arrosa ederra,
ezin bertze huntarik nezazun atera.
Xangrin huntan hiltzeko jin balait malurra,
begian bazinuke betiko nigarra.
Begian bazinuke betiko nigarra.
Begian bazinuke betiko nigarra.
Betiko nigarra.