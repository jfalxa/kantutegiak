---
id: tx-1535
izenburua: Esaidazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B-8QdIrdVoE
---

Hitz hauek idazten ditudan bitartean 
zure berririk jasotzen ez dudan hontan 
zauri bat zabaltzen ari zait bihotzean 
argi bat itzaltzen ari zait barrenean (BIS) 

Bizi nahiz esperantza faltsu baten menpe 
itzulera triste baten bidea eraikitzen 
ezintasun puta honen malkoak batzen 
amorruaren harria berriz kolpatzen 

Esaidazu, aurrera pauso bat emango banu 
emango banu 
beharbada bizitzak zentzu bat izango luke 
izango luke 

Ohitu beharko naiz zu gabe bizitzera 
sinesten dudan guztia ukatzera 
sentitzen dudan hori ez sentitzera 
ta maitatzean sufritzera 

Esaidazu, aurrera pauso bat emango banu 
emango banu 
beharbada bizitzak zentzu bat izango luke 
izango luke (BIS)