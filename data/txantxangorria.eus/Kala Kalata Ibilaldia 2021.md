---
id: tx-520
izenburua: Kala Kalata Ibilaldia 2021
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PJUwnRh3zls
---

IBILALDIA planteatzeko orduan nahiko argi izan dugu Betiko eta Ander Deuna ikastoletako komunitatea osatzen dugun pertsonek erarik ahalik eta kooperatiboenean egin behar genuela Ibialaldiaren egitaraua eta baita Ibilaldiak bereak dauzkan gainerako osagaiak ere; kanta, esate baterako. Egia da ere, are gehiago sortze lana den kasuetan, zaila dela hainbat bururen iritzi eta proposamenak batzearekin batera sortze lan batek behar duen kalitatea bermatzea. Zentzu horretan, eta ezer baino lehen, hasiera hasieratik kantaren osaketa lan honetan parte hartu duzuenoi gure eskerrik zintzoenak emotea; ekarpenak egin dituzuenoi eta borondaterik onenarekin hurreratu zareten guztioi.
Dakizuenez kala-kalata dugu aurtengo Ibilaldiaren leloa eta ez dago esan beharrik kantaren letra egileak inor baino hobeto jakin duela berba gutxitan biltzen lelo horrekin   adierazi nahi dena, hots, bidai honetan, goitik behera eta hezur-mamiraino, busti beharra dagoela. Busti beharra euskara ezagutu eta ikasteko, busti beharra euskaraz mintzatzeko, busti beharra euskara edonorekin edonon eta edonoiz erabiltzeko, busti beharra, azken batean,  euskaraz bizitzen ikasteko. Ez da ez erronka makala geure buruei eskatzen dieguna eta lortu nahi duguna; euskaraz Ibilaldian, euskaraz etxean, euskaraz ikastolan eta euskaraz herrian!   

KALA KALATA! 
Sopelan eta Leioan 
Ibilaldian aurten, 
bidea egin nahi dugu
euskaldunak izaten, (bis)
iturri zaharreko ura
ez dadin inoiz eten. 
 
Maringora kikunbera,
dzanga geurera, boga!
Euskara indartsu dator
zuzenean golkora, (bis) 
Bizkaiko kostaldekora
eta bularrekora. 
 
Aspaldi ikasi genuen
putzutik ateratzen, 
zubi gabeko ibaiak 
igerian pasatzen, (bis)
eta korronteen aurka
beldur barik ibiltzen. 

Kala-kalata gure etxean,
Kala-kalata gure herrian,
Kala-kalata ikastolan,
Kala-kalata ibilaldian. 

Ikasi nahi dugu zer den
euskaraz bizitzea, 
edonorekin edonoiz
lasai erabiltzea, (bis)
busti ezean, ordea, 
zaila dugu lortzea. 
 
Euskara ailega dadin
ostertzetik harago, 
mintzoa eta mingaina
berdin dira zeharo: (bis)
mintzatzea nahi badugu
busti beharra dago. 
 
Abentura honetarako
ez zaigu ezer falta, 
hizkuntza da iparrorratza, (bis)
bizinahia da mapa, 
denok amaituko dugu
goitik behera kalata. 
 
Kala-kalata gure etxean,
Kala-kalata gure herrian,
Kala-kalata ikastolan,
Kala-kalata ibilaldian. 

KANTAREN FITXA TEKNIKOA
Letra: Xabier Paya
Musika: Xabi Aburruzaga
Musikariak:
Xabi Aburruzaga: Trikitixa eta programazioak
Eriz Perez: Gitarra akustikoa
Jose Urrejola: Pianoa, perkusioak, programazioak, gaita eta whistle
Xabier Zeberio: Biolina eta Biola
Alberto Urretxo: Tronboia
Andrzej Olejniczak: Saxoa
Nikolas Zubia: Tronpeta
Aiora Renteria: Ahotsa
Maitane Salvador: Ahotsa
Iholdi Beristain: Ahotsa
Itziar Ituño: Ahotsa
Ander Deuna eta Betiko ikastolako ikasleak: Ahotsak
Grabaketa: Xabi Aburruzagak 2021ko otsailean zuzendu eta ekoitzia.
Produkzioa eta moldaketak: Xabi Aburruzaga eta Jose Urrejola
Grabaketak: Unai Mimenza, eta Xabi Aburruzaga Xabi’s Road eta Tio Pete
estudioetan
Edizioa: Unai Mimenza, Jose Urrejola eta Xabi Aburruzaga
Nahasketak eta masterizazioa: Unai Mimenza
Dantza sortzaileak: Aitzol Barbarias, Gorka Granado, Haizea Hormaetxea eta Larraitz Aldazabal.
Bideo sortzaileak IN-komunikazioa agentzia.
Grabaketa eta edizioa IN-komunikazio agentzia, aireko irudiak Hiruka.