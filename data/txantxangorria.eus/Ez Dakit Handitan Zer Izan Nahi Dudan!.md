---
id: tx-3015
izenburua: Ez Dakit Handitan Zer Izan Nahi Dudan!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6Zeg7dhapz0
---

Oskar Estanga jaunaren kantu eder bat, "Ez dakit handitan zer izan nahi dudan". 2014. urtean ekoiztu zuen "Errimatuz, garenera arrimatuz" izeneko musika proiektu barruan. 

Txikitan zalantzak ezpain itxitan,
galdetu ezgero zer izan nahi 
ote nuen handitan...
Geroztik hasi nintzen gogoz ikertzen 
niretzat moduko lanbide hori zein zen,
futbolean baloirik ez zidaten pasatzen,
ez nuen besteek adina korrikatzen.
Nahi ez nuenez baserrian,
trebatu behar nintzen zehozer berrian,
zeharo galduta, zer nolako kasketak
aukeratu behar zirenean ikasketak!!
Esaten zuen nire barneko nahasketak,
"jada nago mugan eta oraindik nago dudan..."
Ez dakit handitan zer izan nahi dudan!! (bis)
Ikasten segitu nuen horren helburuan,
nahiz eta ez izan lan postu on bat, oporrak, dirua, 
gomendioen aurka, a ze pekatua,
ez nuen izan nahi mediku edo abokatua.
Hortara, ez nuen federik fededun on gisan,
elizan, apaiza ere ez nuen nahi izan,
aldiz, perkusioa, kontzertuak, kitarra, 
bertsoa, kantua, jendearen irriparra,
ziren bihotzaren iparra, 
nire poz barneko jator hoiek esnatzeko mobideak,
baina ze ideak, lanbide txukun bat aukeratzeko,
"jada nago mugan eta oraindik nago dudan..."
Ez dakit handitan zer izan nahi dudan!! (bis)
Haunditzen joan naiz gizarte honetan,
sortu eta saldu naiz uste dudanetan, 
gauza asko izan naiz, ilusioz, benetan,
ikasi dut geletan, oholtzetan, 
festetan, pozetan, penetan...
Pozaren iturria ez dela alkohola,
bakoitzaren bidea idatzi gabe dagola,
zer izatearenak ez duela axola,
ez delako hainbeste zerbait, zergatik eta nola.
Bizi nahi dudala umorez, jendeez, 
inguru dotorez, kolorez, 
ondo ez delakoa nahastuz ohorez,
saiatuaz pozak landatuaz, tristuraren ordez,
"zer bizi nahi dudana dago nigan eta zugan..."
Badakit bizitzan zer bizi nahi dudan!!