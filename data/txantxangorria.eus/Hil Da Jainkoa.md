---
id: tx-1728
izenburua: Hil Da Jainkoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CZGTD6Ryyfs
---

Gizakia pentsatzen hasi zenetik
asmatu behar izan zuen zerbait azaltzeko
zergaitik zuen galdetzeko gaitasuna,
azken batean zergaitiaren zergaitia.
Sinestu ezinik zebilen norbaitek nahi gabe
begiak ixterakoan bide ezezagun baten
amaieran zegoela...berarekin hil zan ametsa.
Hori bai asmakuntza ona
une latzak ahaztu arazten dizkidana.
Betikotasunaren bila luze hori
botoia off-en jarri ta amaitua dago.
Hezitu egin gaituzte begi honen pean
ona eta txarraren patroia inposatuz,
baina botoia off-en jarri bezain pronto,
ez onik ez txarrik...hil da jainkoa!!
Egia da zerbait behar dugula
zalantzaren etekina garestia delako.
Galdera honen erantzuna nik ez dakit
nahiz eta piloa asmatzeko kapaza izan.
Argi daukadana honako hau da:
heriotzaren mamuarekin manipulatu gaituztela
ta manipulazioa gorrotatzen dut.
Horregaitik...hil da jainkoa!!