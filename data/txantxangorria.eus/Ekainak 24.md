---
id: tx-1812
izenburua: Ekainak 24
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2HxsvzVbVtY
---

Ekainak 24, gabon. Herrian zer berri?
Neurea malkoz busti ta zeuena egin
Idatzi soilez eztut min hau Inola hanpatzerik
Orbela daraman bezala, haizeak narama ni

Haizeak eman ditu, milaka abesti
Haizeak eman ditu, nahi nuke nik jakin

Idatzi soilez eztut min hau inola hanpatzerik
Orbela daraman bezala, haizeak narama ni

San Juan suak senti nahirik
Bero haren falta dut nik
San Juan suak senti nahirik
Urrun zaituztedanetik

Eskutitzetan zuek orain berdin sentitzea
Ondo dakizu ama eztela hain erreza
Ametsak zirriborro baten idaztea zer da
Gero garbira pasatzeko astirik ez bada?

Haizeak eman ditu, milaka abesti
Haizeak eman ditu, nahi nuke nik jakin

Ametsak zirriborro baten idaztea zer da
Gero garbira pasatzeko astirik ez bada?

San Juan suak senti nahirik
Bero haren falta dut nik
San Juan suak senti nahirik
Urrun zaituztedanetik

Negarraren negarrez eztut muxu ematerik
Ta enuke inolaz orain hau esan beharrik:
Bide luzea dugun arren, goazen elkarrekin
Izarrak gurekin baitaude, ametsen zaindari

Beldurrik ezz den bihotzean
Aitaren malkoetan
Memoria gazte garaietan
Lagun minen besarkadetan
Udako gau luzetan
Urrun sentitzen dudan herriko kaleetan

San Juan suak senti nahirik
Bero haren falta dut nik
San Juan suak senti nahirik
Urrun zaituztedanetik