---
id: tx-3008
izenburua: Nola Liteke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s4LFL9Lw2Cg
---

Ezustean, atsekabez
heldu zitzaigun berria
ez zeundela, joan zinela
lora bidean zimeldu zela.

Gizatasun. erraldoi bat
zeure gorputz txikian
grina osoz, bihotz betez
eman zenigun bizia.

Nola liteke, nola liteke
hutsune hau betetzea
nola liteke, nola liteke
zu gabe gelditzea.
nola liteke, nola liteke

joan zinen, joan zinen

uda arrats baten

jainkoen olinpora.

Joan zinen, joan zinen
ustekabean
agur saminaren zirraran.

Joan zinen, joan zinen
ta hustasunak
atzeman du bizipoza.

Joan zinen, joan zinen
gauaren izarrek
argi egin diezaguten.