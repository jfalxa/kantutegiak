---
id: tx-2866
izenburua: Korrika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SkdK5qISd-Y
---

Korrika goaz, iritsi arte
gure herriko helmugara;
ta honetarako, lehen urratsa,
indartu behar euskara.
Alferrikan da, ikastoletan, hizkuntza bat
ikastea gero kalean, gure artean,
inoiz egiten ez bada.
Horregatik herriko ekintzak, noizean behin,
esnatzeko. Hain beharrezkoak baitira
nahiz izan batzuk aurkako.

Ea segi bada iritsi artekilometroz kilometro,
gure herriko bide guztiak nahiz korrika,
nahiz geldixeago. Kilometroak, Ibilaldia,
eta Araba Euskaraz, Nafarroa Oinez,
Herri Urratasa, Beteginik ta Korrika.
Ekintza hauek eta euskara alderdi
denen gaineatik, protagonismo ta harrokeria
alde batera utzirik.

Horregatik herriko ekintzak, noizean behin,
esnatzeko. Hain beharrezkoak baitira nahiz
izan batzuk aurkako. Ea segi bada iritsi arte
kilometroz kilometro, gure herriko bide guztiak
nahiz korrika, nahiz geldixeago.