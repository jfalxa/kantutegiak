---
id: tx-2497
izenburua: Denboraren Naufrago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RA-fwnZEWS8
---

Hartu soinua eta martxan furgoneta eman muxu bat ama Ingalaterrara noa Hor zihoan mutila munduaren bila bi begi bahitzaile preso hartu zuten arte. Hura neska polita jarri nion zita nire ingelerarekin barre egin zuen goxo gero batean irri ta bestean zirri holan joan zen uda eta udarekin bera. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Maiteminduen udan baitatu zintudan zure olatuetan ez nintzen alperrik galdu agurren kostaldera joarren hiltzera zure olatu guztiak nire oroimenean daude. Gaur nire bihotzean kajoiren batean hango argazkiak daude ta muxuak remidean errua ez da dena diztantziarena ez al da korrontea urrundu egin gaituena. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia.



Hartu soinua eta martxan furgoneta eman muxu bat ama Ingalaterrara noa Hor zihoan mutila munduaren bila bi begi bahitzaile preso hartu zuten arte. Hura neska polita jarri nion zita nire ingelerarekin barre egin zuen goxo gero batean irri ta bestean zirri holan joan zen uda eta udarekin bera. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Maiteminduen udan baitatu zintudan zure olatuetan ez nintzen alperrik galdu agurren kostaldera joarren hiltzera zure olatu guztiak nire oroimenean daude. Gaur nire bihotzean kajoiren batean hango argazkiak daude ta muxuak remidean errua ez da dena diztantziarena ez al da korrontea urrundu egin gaituena. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia. Zeharkatu ninduen errekakoa omen nigana ekarrita eraman zintuen hurak geroztik hemen nago denboraren naufrago zure oroitzapenetan bustia.