---
id: tx-1240
izenburua: Orratzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rEpI39zU-9M
---

Musika eta Hitzak Hibai Etxebarria

Oiartzungo Mecca Recording Studios-en grabatua
Nahasketa eta Masterizazioa: Mikel Eceiza eta Hibai Etxebarria
Estudioko laguntzailea: Igor Eceiza
Logoaren diseinua: Itsasne Zarrabeitia
Zuzendaritza eta Ekoizpena: Hibai Etxebarria


Musikariak:

Pianoa, Gitarrak eta Ahotsa: Hibai Etxebarria
Ahotsa: Izaro Andres
Kontrabaxua: Ander Garcia
Bateria: Carlos Sagi
Perkusioa: Iraide Ansorena
Zeharkako flauta: Naia Mandaluniz
Bibolin bakarlaria: Maddi Arana
Biolontxelo bakarlaria: Pello Ramirez

Bibolin I: Leire Hipólito June Bergés Pedro Aginaga Txabeli Sierra
Bibolin II: Leire Pikabea Tomás Ruti Iraia Bereziartua
Biola: Begoña Urtiaga Mónica Peinó
Biolontxeloa: Itziar Lertxundi Jokin Garmendia
Kontrabaxua: Lerman Nieves



Bideoaren muntaia: Hibai Etxebarria

Irudiak: Oier Plaza Beñat Bonilla Hibai Etxebarria Gorka Bilbao Zigor Etxebarria FILMAK

Zuzendaria eta Ekoizpena: Hibai Etxebarria


Gurekin harremanetan jartzeko jarraitu gaitzazu sareetan:



TRIPTICS I-II Diska berria Durangoko Azokan salgai!

Gaztelupeko Hotsak-en stand-ean


Hitzak:

Orratzak
Zauritzen nauen einean
Sendatzen zaidan bihotzak
Errautsak
Emeki ene magalean
Pausatzen direnean

Bizitzak
Zentzurik balu bezala
Ehizan, nire zai
Belatzak
Gau beltzak
Egunsentira bidean
Naramanean

Zure irla txiki hortara
Hurbildu nahi nuke lotara
Babesleku bat
Itsasoaren erdian
Baina nire iparrorratzak
Galdu baitu zure lorratza
Babesleku bat
Itsasoaren erdian

Orratzak
Hari luzetan herdoilduak
Sendatu gabe zauriak
Orduak
Geldi dagoen erlojuan
Zenbatu ezinean

Amorruak
Itzaltzen duen
Argiaren dizdirak
Narrada noraezean
Biluzik
Kalerik-kale-argitan
Noanean

Zure irla txiki hortara
Hurbildu nahi nuke lotara
Babesleku bat
Itsasoaren erdian
Baina nire iparrorratzak
Galdu baitu zure lorratza
Babesleku bat
Zure besoetan
Nire bihotzean
Laztana