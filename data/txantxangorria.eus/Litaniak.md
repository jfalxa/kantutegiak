---
id: tx-29
izenburua: Litaniak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rB5--Duf7B8
---

Jente zühürrak eta perestüak dirade orotan maitatüak,
Jente auherra’ta küdiziata zuin hügünkari harien ürhatsa !
Tralalai tralalai tralalai ! Tralalai tralalai tralalai !
Tralalai tralalai tralalai ! Tralalai tralalai tralalai !
Erreüstarzünak bihotza higatzen, ele on goxo batek azkartzen,
Lan gabeziak jentea kexarazten, soberak noiztenka ertzoarazten!
Agortzen delarik ütürri hona, han ezagützen uraren balioa,
Hontan ürrentzen’tügü litaniak, ez higatzeko züen pazentzia!