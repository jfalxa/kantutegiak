---
id: tx-937
izenburua: Xoxoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p3Y3Te3evps
---

Xoxoak galdu du burua
Burua, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du begia
Burua, begia, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du lepoa
Burua, begia, lepoa, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du mokoa
Burua, begia, lepoa, mokoa, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du hegal
Burua, begia, lepoa, mokoa, hegala, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du buztana
Burua, begia, lepoa, mokoa, hegala, buztana, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?

Xoxoak galdu du bihotza
Burua, begia, lepoa, mokoa, hegala, buztana, bihotza, xoxua, gaixoa
Nola kanta, nola xirula, nola kanta xoxuak?