---
id: tx-3336
izenburua: Harresiak Eta Zubiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hVrrt-Pgrxo
---

HARRESIAK ETA ZUBIAK

Harresia aintzina

Babeserako zen.

Han eta hemen hainbat

Altxa genituen.

Erasuen aurrean

Beharrezko ziren

Harriak hesian-ta

Ea nor gogorren.

Harresien atzean

Gorde gintezkeen.

Harresien atzean

Trankatu gintuzten

Harresien azpian

Lozorrotu ginen.

Harresien atzean

banatuta geunden.

Harresiekin ohitu

Garen arren ia,

Ez al da Euskal Herrira

Goazen bidaia?

Haraino nola heldu

Hutsaren gainetik?

Harresia, bihurtu

Dezagun zubia.

Harresiak bihurtu

Ditzagun zubiak,

Euskal Herria bide

Zabal irekiak.

Harresien artetik

Elkartzen bagara,

Laster zubi gainetik

Pasatuko gara.

Bagoaz herri modu

Batetik bestera

Euskal Herri berria

Ja eraikitzera.

Harresiak bihurtu

Ditzagun zubiak,

Euskal Herrira bide

Zabal irekiak.

Denona den herria,

Denontzat egina!

Denon arteko baizik

Antolatu ezina!

Euskal Herrira goaz,

Bideak ireki!

Harresien artetik

Zubiak eraiki!

Joseba Sarrionandia