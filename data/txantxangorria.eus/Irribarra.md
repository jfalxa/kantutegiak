---
id: tx-224
izenburua: Irribarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eWjYxKGxCng
---

Metro karratu ikusezinetatik
Lurra landuz eskuez, bihotzetatik
Etorkizunaren lehen sustraietik
Kimu bihurtu gara hazi txikitik

Uoo

Kimu bihurtu gara hazi txikitik
Herritik
Bertatik

Ibarra da
Irribarra
Irriz ase alaitasun beharra

Ibarra da
Irribarra
Lehena, oraina eta biharra

Ibarra da
Irribarra
Euskara da, ikastola gu gara

Ibarra da
Irribarra

Munduko mila txokoetatik dantza
Aniztasunak onarpenean datza
Ez al da ingurugiro aberatsa,
Gure mila koloretako baratza?

Uoo

Gure mila koloretako baratza,
jolasa
ardatza

Ibarra da
Irribarra
Irriz ase alaitasun beharra

Ibarra da
Irribarra
Lehena, oraina eta biharra

Ibarra da
Irribarra
Euskara da, ikastola gu gara

Ibarra da
Irribarra

Ibarra da
Irribarra
Irriz ase alaitasun beharra

Ibarra da
Irribarra
Lehena, oraina eta biharra

Ibarra da
Irribarra
Euskara da, ikastola gu gara

Ibarra da
Irribarra