---
id: tx-3264
izenburua: Txu Txu Txorimaloa -Takolo Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PyVKwtRr7d0
---

Txa txa txe txe txi txi
txo txo txorimaloak txu txu
Txantxangorriak txantxetan
ta txapela dantzan
Txepetxak txe
txermena jan du
Txirrintxoek
txin txin txalo egin

Txa txa txe txe txi txi
txo txo txorimaloak txu txu
Txantxangorriak txantxetan
ta txapela dantzan
Txepetxak txe
txermena jan du
Txirrintxoek
txin txin txalo egin

Gorputza lastoz beteta
makilaz hezurrak tente 
ta orain
Ibili nahi dut,
oinak astindu,
dantzaria izan,
milaka zelaitan

Korrika, hegan egin
aurrera
Korrika, txoriekin
denok batera

Korrika, hegan egin
aurrera
Korrika, txoriekin
denok batera korrika!