---
id: tx-407
izenburua: Fueruak Kendu Zituenari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GK0zl5vyFKQ
---

Fueruak kendu zituenari
Zeruko Aita, barka itzazu
pekatariyen pausuak,
Kantabriyako deskalabruak
dirade lastimosuak;
arbola eder bat dela mediyo
jatzera nua bertsuak,
milla urtean mantendu gaitu
lau probintzia osuak.

Gutxienez izan bear du
milla urte pasatua
arbola aundi bat bada Gernikan
aitona batek sartua;
aren azpiyan egin omen zan
fueruen juramentua;
orain argatik deitzen diogu
arbola bedeinkatua.

Biba arbola maitagarria,
biba kantabriatarrak
biba Bizkaya, Gipuzkuakin
Araba eta Naparrak!
Biba kortean gure fabore
dauden ministro bakarrak!
Beste gauzarik ez degu eskatzen
biba lengo fuero zarrak!

Arbola dago igartua ta
euskaldunak gaude triste,
ez det pentsatzen pasatu danik
munduan beste orrenbeste;
orain gertatu zitekeanik
ere ez nuen nik uste,
milla urtean segitu duten
fueruak kendu dituzte.

Milla zortzireun irurogei ta
zortzigarren urtian
izan eder bat estali zuten
oroi askoren  tartian;
lañoak fuerte korritu zuten
ura tapatu artian,
geroztik ez da eguraldi onik
Kantabriako partian.

Ara izar au tapatu eta
zer egin zuten lenbizi;
fraileak eta monjak bialtzen
konbentuetatik asi;
jesuitak ta misionistak
saltatu ziran igesi;
esaten zuten; " Gu gaituk orain
Españiako nagusi".

Lendabiziko fueruen kontra
firmatu zuen gizona
eskeleto bat bezela eginda
ez dago oso gizena;
dudik gabe gogoratzen zaio
etzuela egin gauza ona,
kastigatzera bildur ezpanitz
esango nuke izena.