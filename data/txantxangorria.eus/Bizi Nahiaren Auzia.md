---
id: tx-2613
izenburua: Bizi Nahiaren Auzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gyT7y9WIZM0
---

Ez dakit norena naizen,
ez dakit nora noan
zoriaren ipar orratz
arrotz eta eroan
Bakardadezko egun horietan
murgiltzen naiz nire bizian
haizea mendian bezala
aske sentitzen naiz nire orduan
Bakardadezko egun horietan
murgiltzen naiz nire bizian.

Menpekotasun bategatik
libre izate arren
gustokoa duzu nortaz
bizia emate arren
zoriontasunaren mugetatik
haratago joan nahiean
zuek emandako sentimenduagatik
bizia emango dut nik

Etorkizunak, etorkizunak,
etor dadin nahi duzuna
gozoa edo gazia, 
maite dut ene bizia
asmo berriak, kantu berriak,
zuentzat dira guztiak
sarri utsegingo du gure bizian
erotutako traizioak
bizi nahiaren auzia
kostata irabazia

Menpekotasun bategatik
libre izan arren
gustokoa duzu nortaz
bizia emate arren
zoriontasunaren mugetarik
haratago joan nahiean
zuek emandako sentimenduagatik
bizia emango dut nik