---
id: tx-1984
izenburua: Gau Iluna Amaitu Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_5-20Zu9lB0
---

Gazte zinen lagun maite
zakurren hitz-zaunkak
esnatu gintuzten,
kolpe artean
azkenengo besarkada
zulo hotza, lagun gabe
espetxe guztiak
gorroto berdinaz
gizon bihurtuz,
lagun eta senide gehiegi
bidean gelditu dira
oroimen oinazea.
Hau al da euren pakea?
Joan da beste urte beltz bat.
Sakabanaketa
beraien zigorra
ez zaitu isutu
eta aitaren etxea
zutik da
azkenik askatasuna
poza eta beldurra
zer egongo da kanpoan?
Senideak
malko artean
besarkadak
maitasunaren gainetik.
Gau iluna
amaitu da
iritsi da
amnistiaren eguna.