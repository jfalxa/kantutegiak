---
id: tx-2890
izenburua: Urri Urri Bihurriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZR_bSAlXgro
---

Atsegin dut lagunak
zirikatzea
Putzuan salto egin
ta zu bustitzea
Amonari ileak
nahastea
Bost axola denek
bihurri deitzea

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!

Xaboi potea hustuta
dutxan apar saltsa
Ohe gainean jauzi,
erori ta altxa
Laranjekin bete
pipi poltsa
Ganbaz mozorrotu
eta galdu lotsa

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!

Maindirepean burukoa
ipini eta
siesta garaian
plazan jo turuta
Azukrearen potoan
sartu gatza
Jogurt goxoaren
zapore mingatza

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!

Bihurria naiz eta
urri, urri, bihurri!
Ala esaten didate
urri, urri, bihurri!