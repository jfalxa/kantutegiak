---
id: tx-1924
izenburua: Ixil Ixilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t3Ra_GJUzBg
---

Isil-isilik dago
kaian barrenean
ontzi zuri polit bat
uraren gainean.

Goizeko ordu bietan 
esnatutzen gera
arrantzaleak beti
joateko urrutira.

Pasatzen naizenean
zure leihopetik
negarra irteten zait
begi bietatik.
Zergatik, zergatik,
zergatik, zergatik?
Zergatik negar egin?
Zeruan izarra dago
itsaso aldetik.

Bat, bat, bat,
bart parrandan ibili,
bart parrandan ibili.
Bi, bi, bi,
ez naiz ondo ibili,
ez naiz ondo ibili.
Hiru, hiru, hiru,
golkoa bete diru,
golkoa bete diru.
Lau, lau, lau,
sardina bakalau.

Anteron txamarrotea,
Sinkerren bibotea,
haretxek ei dauko,
preso tximinoia.

Hau duk, hau duk,
hau duk humorea,
kontsolatzeko,
kontsolatzeko
euskaldun jendea.
Kalean gora 
kalean behera 
kalean gora
zezena,
ai, ai, ai, ai!

Kalean gora 
kalean behera 
kalean gora zezena."