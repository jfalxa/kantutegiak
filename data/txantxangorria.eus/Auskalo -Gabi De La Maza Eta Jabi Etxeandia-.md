---
id: tx-3120
izenburua: Auskalo -Gabi De La Maza Eta Jabi Etxeandia-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5oiEDemMVg4
---

Patxi Zabaletak "Ezten Gorriak " liburuan Gorka Trintxerpe ezizenez argitaratu zuen olerkia, 1976an musikatu egin genuen Xabier Etxeandia eta biok.