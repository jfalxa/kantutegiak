---
id: tx-2848
izenburua: Trebiño
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wBwOzCkBH_s
---

Maite zaitut, lurra
zu zera nire babesa;
eta biziki maite bazaitut ere.
gaur zaitut maiteago.

Trebiño, Trebiño
zure erreka garbien
bazterretatik
kendu dizkizute
errezalak
errezal sendoak.

Ez omen dituzte maite
errezalak
zure jabeek
jabe berriek, gezurrezkoek.

Baiñan mendi sabeletatik sortzen dira berriz
errezalak,
pagoak, pago berriak,
oraiño, Trebiño.

Ibai, uda, Ibita
zeinen garbi den
zure ura!
Nora galdu ote
zure herritarren asmo hura?
Asmo horrek iraungo du
eurak bizi diren artio.
Trebiño