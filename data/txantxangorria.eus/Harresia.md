---
id: tx-3371
izenburua: Harresia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lXIVaREzRY4
---

Airean negarren oihartzunak
zure bihotza dute samintzen.
Su atzean zapia aurpegian,
inoiz izango al zara zu libre?

Tanke hotsak kaleak izutzen,
etxe guztiak miatzen.
Kaleak hildakoez jantzita,
erailketa zibila.

Harrizko horma etxeko atarian.
Zauri sakona basamortuan.
Herri baten bihotz erdibitua.
Arima gabeko harresia.

Mugaz bestalde bizi direnen
aieneak arintzeko horma.
Tortura arma bihurtu dute
herri ahul baten aurka.

Bonben erasoa umeentzat
su artifizialak dira,
jende artean eztanda egiten duten
suziri hiltzaileak bailira.

Harrizko horma etxeko atarian...

Euren arbasoen lurra zena,
bizitzaz bada ere ez galtzea.
Odol ta harriz defendatua izatea,
hormarik gabeko Palestina.