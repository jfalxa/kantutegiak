---
id: tx-1405
izenburua: Zurea Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZuWVXan9D28
---

Nik dudana zurea da, hartzazu
nik dudana soberan da, eroaizu
nik dudana jasoa da, emaizu.

Etxe zaharra zurea da, pasatu
mahaian dena soberan da, geratu
gau isila jasoa da, sendatu. .

Oinutsik, porlanaren gainean eta oinutsik,
hego-haizeak bularrean laztan dagi,
den-dena bere lekuan da/go.

Oinutsik, azalean harritxoak iltzatu zaizkit,
ez dut teilatu zabalaren beharrik,
den-dena bere le/kuan dago...
irriz kanta(t)uko dizut,
berriz kanta(t)uko dizut,
beti kanta(t)uko dizut.

Bakarrik, izarren pean oinez eta bakarrik,
ez izu, ez ikara, ez amets txarrik,
ta behelainoa aingeru guardako.

Bakarrik, zuhaitzek hartu naute beso zabalik,
ez zor, ez erru, ez pena, ez zigor luzerik,
den-dena bere lekuan eta,
banoa, banoa aspaldiko basamortu ezezagun horretarantz