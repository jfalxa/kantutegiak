---
id: tx-182
izenburua: Maitea Han Dago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E5fYxWWj9TU
---

Eskerrak bizitza  

Violeta Parra-ren kantua - Amets Arzallus-ek itzulirik

Aire Ahizpek kantaturik


Eskerrak bizitza eman didalako

begien kabi bat, nik zabaltzerako

kolore guziak bazituelako

zeru altuetan izar gorri asko

eta haien artean maitea han dago


Eskerrak bizitza eman didalako

entzumen oso bat ni ohartzerako

gauak kilker baten hotsa zuelako

mailuen oihuak eta oihu gehiago

ta haien artean maitea han dago


Eskerrak bizitza eman didalako

ama hizkuntza bat nik ulertzerako

pentsatzeko hitzak bazituelako

sehaska kantuak ta lanturu frango

ta haiek entzuten maitea han dago


Eskerrak bizitza eman didalako

hankara pausoa ni ibiltzerako

mendi ta hiriak hor zituelako

desertu urdin bat zenbat portu galkor

ta haien artetik maitea noiz dator


Eskerrak bizitza eman didalako

irrifar goxo bat ta putzu bat malko

bide lokaztu bat ez dakit norako

txori zauritu bat bihotz barnerako

ta haren oinetan maitea han dago


Bide lokaztu bat ez dakit norako

txori zauritu bat bihotz barnerako

ta haren oinetan maitea han dago