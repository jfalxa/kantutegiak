---
id: tx-2174
izenburua: Haize Hegoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MLKrArDHHok
---

Gaur abenduaren batean laga gaitu Mikelek. Uda amaieran, biga, laga ginduen beste Mikelek, Errazkin. Mundu honetako kailatik, biak, betirako aske.