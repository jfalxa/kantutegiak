---
id: tx-2666
izenburua: Lilurarik Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/edo8lqKWqjM
---

Liluraren kontra

Lilularik ez!
ez dago itzultzerik
eguna atean dago
haize hotza dakar
ez da izango beste goizerik.(bis)

Tronpatzerik ez!
bizitza ez da huskeria,
edan ase arte beretik
ez da aski izango
galtzear zaudelarik.(bis)

Kontsolatzerik ez!
denbora ez da luzea,
ustelak lurpera
bizitza da haundiena
galtzea, litzake galtzea dena.(bis)