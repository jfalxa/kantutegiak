---
id: tx-596
izenburua: Itsu Bati
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z4Z6nHJ7GfQ
---

Begietatik nola zauden
laguna elbarria
esplikatzera nihoakizu
nor den gure aberria.
Bere mendietan hazten doa
bizitza, kimu berria,
denetarik ederrena dauka
bere udaberria.
Zuhaitzeko orria,
langileen izerdia,
ifarraldetikan hegoraino
zuri berde ta gorria.

Eguzki zuriz esnatu eta
eguzki gorrriz lokartu,
txalupa batez bere sabelan
nintzen atzo barneratu.
Uso zuriak nire alboan
oraintxe dira geladitu
baina laguna gauza bat zuri
nahi nizuke argitu.
Adi-adi aditu
eta ez zaitez harritu
nik ere herria ez dut ikusi
baizik soilik sentitu.
Bere zainetan ere badauka
nolabaiteko kaltea
ikusten ditut fabriketako
zikin, zabor eta kea.
Erdaldundurik zoritxarrean
ditu oin eta ankea,
denetarik dauka falta zaion
gauza bakarra bakea.
Ezin hautxi ordea
lotzen gaitun katea
zaila izaten da ere niretzat
askatasuna ikustea.