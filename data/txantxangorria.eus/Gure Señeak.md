---
id: tx-1131
izenburua: Gure Señeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q4HBbXvI9tg
---

Letra: Miren Amuriza
Musika: Xabier Zabala

Txunditu ditu itsaso urdinak, 
maite dituzte amaren ipuinak 
eta oinpean sentitzen dituzte kilimak
kafea egiten zaie mingotsa,
eta pozgarria kriskitin-hotsa,
bularrean taupa-taupaka dute bihotza.

Nahiz eta sarri joaten diren ospitaleetara,
nahiz eta ahul daudenean
hartu botika eskukada,
haientzat sendagairik onena
gure irribarrea da.

Gure txikiak kementsuak dira,
gure niniak etxetik herrira,
gure señeak daude geroari begira.
Eder bezain hauskorrak diren lumak,
maitasun handiz zaintzen ditugunak,
eurak dira gure herriko ume kuttunak.

Irakasle, lagun, amona,
auzokide, osaba...
Tantaz-tanta elkartasuna
olatu bilakatua da,
banan-banan gutxi gara baina
elkartuta asko gara!

Txunditzen ditu itsaso urdinak,
maite dituzte aitaren ipuinak,
eta oinpean sentitzen dituzte kilimak.
Kafea egiten zaie mingotsa,
eta pozgarria kriskitin-hotsa,
bularrean taupa-taupaka dute bihotza.

Gure txikiak kementsuak dira,
gure niniak etxetik herrira,
gure señeak daude geroari begira.
Eder bezain hauskorrak diren lumak,
maitasun handiz zaintzen ditugunak,
eurak dira gure herriko ume kuttunak.

Eurak dira gure herriko ume
kuttunaaaaak!