---
id: tx-3121
izenburua: Baratzatik Ikastolara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AR_gEJG2obc
---

Egunero ikastolara 
joaten gara dena ikastera
Egunero ikastolara 
lagunekin jolastera

Baratzatik jolastokira
sekulako txalaparta
denon artean euskara bultza
esan denok Olabide aurrera!