---
id: tx-124
izenburua: Denbora Ez Da Pasatzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iJQfhBPkas0
---

Denbora ez da pasatzen

Musika: Jon Maia
Hitzak: Jon Maia

Azalak memoria du
ta memoriak azala
ez dut gogoratzen behingoz
ahaztu behar zaitudala

Momentutxo bat sartu naiz
zure besoen artean
eta betirako preso
sentitu naiz bapatean

Ez det inora joan nahi
zurekin nagoenean
baina ez dakit non egon
doana zu zarenean

Denbora ez da pasatzen
nahiz ta egunak igaro
zu gabe min ematen dit
baina zurekin gehiago

Ohe bat iruditzen zait
zure eskuaren ahurra
azpian ezkutatzen den
monstruoai diot beldurra 

Erretzaile oiha berriz
jausten da bat probatuta
Neri hori gertatzen zait
Zuri behin begiratuta

Ez dakit egiten dezun
nahi gabe edota nahita
baina zure perfumea
daukat atzetik jarraika

Denbora ez da pasatzen
nahiz ta egunak igaro
zu gabe min ematen dit
baina zurekin gehiago

Taberna hortan oroitzen
zaitut barrezka, dardotan
ohartzerako malkoek
diana jo dute ardotan

Egon naiz argazki denak
ezabatzeko zorian
baina inprimatu dizkit
nostalgiak memorian

Ze min ematen didaten
bagenduk eta  bazinak 
mingainean iltzaturik
daramatzadan piercinak

Denbora ez da pasatzen
nahiz ta egunak igaro
zu gabe min ematen dit
baina zurekin gehiago

Beti izango da hobeto
Badaezpada esatea:
Ondo nago, ez kezkatu
Ez zaitut maite, maitea

Denbora ez da pasatzen
Nahiz ta egunak igaro
Zu gabe min ematen dit
baina zurekin gehiago
askatu ninduzunetik
korapilatuta nago