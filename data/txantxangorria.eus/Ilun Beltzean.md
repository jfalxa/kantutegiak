---
id: tx-2294
izenburua: Ilun Beltzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zzHDUuC5q-A
---

Nork erran du ilun beltzean
bizi hau itzaltzen denean
dena hortan bukatzen dela?
Oroimenean, bihotzaren magalean
joan zirenak badiraute airean
usainak bezala.

Etsiturik nagoenean
hutsik sentitzen naizenean
badakit hemen dagoela.
Ibiltaria joanik ere
hemen da bidea eta makila.

Nik badakit gau ilunean
bizi hau itzaltzen denean
izar berri bat pizten dela.

(Aitorrek, Piarres Laskarai bere aitaginarrebaren heriotzean idatzitako hitzak)