---
id: tx-2903
izenburua: Herria Mugarri Gazteok Martxan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-K7AxKHjfys
---

Gu gure erabakitan
Mim ReM
burujabe garelako
SolM DoM
maiz kanporako adina
Mim ReM
behar denez barnerako
SolM
burujabetzaren bila
DoM
martxan gara aterako
SolM
pertsona askeak sortzen
ReM
herri libre baterako x2
SolM DoM ReM
Nire burujabetzatik
/Herri mugarri
SolM DoM ReM
Gure burujabetzara
/gurea erabaki
Mim DoM ReM
martxan goaz
SolM DoM ReM
martxa gara
SolM DoM
Herriek eta pertsonek
Mim ReM
Dute bere kolorea
SolM DoM
Erabakia norberak
Mim ReM
Hartu behar du ordea
SolM
Etorkizun bat nahi dugu
DoM
Orain hau baino hobea
SolM
Libre eta euskalduna
ReM
Gorri berde ta morea
SolM
burujabetzaren bila
DoM
martxan gara aterako
SolM
pertsona askeak sortzen
ReM
herri libre baterako
Instrumentala: SolM – DoM – mi m – ReM x 2
Aldaketa: SolM – FaM – Sol M – FaM
SolM – FaM – SolM x2
DoM FaM
Gazteok martxan gabiltza
DoM SolM
Bidea ez da amaitu
DoM FaM
Motxilak ametsez bete
DoM SolM
Eta aurrera jarrraitu
DoM FaM
Bidea dagoenak ez
DoM SolM
Doanak egiten baitu
DoM FaM
Iraultza egingo dugu
DoM SolM
Iraultzak egingo gaitu
Instrumentala: SolM – DoM – mi m – ReM x 2
SolM
burujabetzaren bila
DoM
martxan gara aterako
SolM
pertsona askeak sortzen
ReM
herri libre baterako x2
Azkenengo akordea: SolM


Gitarra Elektrikoa: Itsaso Gutiérrez (ON, Donosti)
Gitarra Akustikoa: Ane Martínez (Bilbo)
Bateria: Izaskun Cuevas (Barakaldo)
Dultzaina: Lorea Argarate (LAS TEA PARTY, Bilbo)
Baxua: Eire Larrea (Bilbo)
Trikia: Izar Mendiguren (ULUKA, Laudio)
Alboka: Naroa Larriba (Barakaldo)
Teklatua: Olatz Salvador (SKAKEITAN,Donostia)

Ahotsak:
Itsaso  Gutierrez (ON, Donostia)
Ane Martínez (Bilbo)
Izar Mendiguren (ULUKA, Laudio)
Irati Mendiguren (ULUKA, Laudio)
Garazi Abrisketa (ULUKA, Zollo)
Iratxe Gorostizaga (ULUKA, Arrankudiaga)
Hegoa (Hazparne)
Armelle (Senpere)
Naroa Larriba (Barakaldo)
Oihana Fernández (AFU, Barañain)

Musika: Naroa Larriba (Barakaldo)
Letra: Beñat Gaztelumendi

Grabaketak:
BALEA MUSIKA (Bilbo)
PSYCOCYBENEA (Hondarribi)