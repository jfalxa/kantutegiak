---
id: tx-1306
izenburua: Ez Zaudenetik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T1OfrgAC7Qc
---

Lauroba taldearen "Ez zaudenetik" bideoklipa. "Iglu bat basamortuan" diskoan jasotako kantua da. 


Egun euritsu haiek ohe barrenean igarotzen genituen, ez dakit zer gertatu den gure artean...
Ea zer dioen kutxa beltzak!
Maitatzea baino ederragoa maitatua sentitzea zen, orain berriz ez zaitut ikusten begiratzen dizudanean
Bala galdu batek bihotza zulatu ta odolustutzen ari naiz, gaur egun hemen ondoan zaudela bakarrik nago zurekin
Aurkituko ez dudanaren bila, kaletik noraezean...
Entzumenak ez du ezer ikusten edo dena beltza ikusten du
Ez zaudenetik, zaila egiten zait dena...
Ez zaudenetik.