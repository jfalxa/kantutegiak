---
id: tx-2477
izenburua: I'Ll Be Your Mirror
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bVIEAMyI-Ms
---

Ni zure ispilu, zarena isladatuz,
ahantzi baduzu. 
Ni haizea, euria eta arratsa, 
bidea zehazten dizun argia... 
Barnea iluntzen zaizunean, 
zeken eta zital sentitzean, 
utzi erraiten itsu zaudela, 
otoi, jeitsi eskuak, ikusten bazaitut!! 
Sinesgaitza da ez onartzea zure edertasuna, 
hala bada, nagizu zure begiak, 
eskuak ilunpean, beldur ez zaitezan... 
Barnea iluntzen zaizunean, 
zeken eta zital sentitzean, 
utzi erraiten itsu zaudela, 
otoi, jeitsi eskuak, ikusten bazaitut... 
Ni zure ispilu, ni zure ispilu!!