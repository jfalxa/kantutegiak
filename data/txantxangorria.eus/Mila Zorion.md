---
id: tx-1797
izenburua: Mila Zorion
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qhBSfSabSGE
---

Putz egin, ez etsi
kandelak itzali
Goxokiak eman zure 
lagun guztieri
Tira gero belarritik 
urte bakoitzeko
Urtebetetzea gaur
ongi ospatzeko

Lagun, lagun
mila zorion lagun!
Denok egun alaia
izan dezagun

Urte bat osoa
igaro da iada
Eskuzabal eta jatorragoa
zu zara
Horregaik gara zure
adiskide honak
Gora zu eta tori
muxu handia

Lagun, lagun
mila zorion lagun!
Denok egun alaia
izan dezagun