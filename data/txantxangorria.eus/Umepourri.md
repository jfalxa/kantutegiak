---
id: tx-2053
izenburua: Umepourri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y744ckNpM4A
---

-Do, re, mi,
hankoker mon ami.
Fa, sol, la,
zuzendu zaiola.
Si, si, do,
mardul eta sendo.
Sol, la, si,
berriz ere hasi.
La, sol, fa,
hortxataren txufa.
Fa, mi, re,
eguzkiak erre
Sol, fa, mi, re, (bis)
Do, re, mi, fa, sol, la, si, do.
-Marijane kanta zan,
hik dakinan moduan,
ororen aintzinean.,
bai mosiu, bai madam,
nik badautzut kanta eginen
dakidan moduan,
ororen aintzinean.
bai mosiu, bai madam,
nik badautzut kanta eginen.
Adi Marijane, Marijane
beldurrik gabe kanta zan;
kanta, kanta zan Marijane,
lotsarik ez ukan.
-Pin! Pan! Pin!
Tiroak hil du lapin (bis)
Pin! Pan! Pin!
Gaixoak ote du min? (bis)
Pan! Pin! Pan!
aitatx ik behar du jan (bis)
Pan! Pin! Pan!
saltsa ona mahaian. (3 aldiz))
-Txinkirrikitxin txinkirrikitxin
Txinkirrikitxin txinkirrikitxin
Txinkirrikitxin,
atzo nintzen Bilbora,
txinkirrikitxin,
tomate berdetan. (bis)
Txinkirrikitxin,
Bilbora noa,
txinkirrikitxin,
zer eiten?
txinkirrikitxin,
makailautxoa,
txinkirrikitxin,
ekarten. (bis)
-Ponpixola ta ponpixola
euliak txepetxa jan dabela
euliak txapetxa jan badeu
tripe zitela bete deu. (bis)
Tanparrantai, tanparrantai
Tanparran-tanparran-
Tanparrantai,
Tanparrantai, tanparrantai,
Tanparran-tanparranton.(bis)
-Bat, bi, hiru, lau,
bost, sei, zazpi,
zortzi,
bederatzi, hamar,
hamaika, hamabi,
hamahiru, hamalau,
hamabost, hamasei,
hamazazpi,
hemezortzi,
hemeretzi, hogei