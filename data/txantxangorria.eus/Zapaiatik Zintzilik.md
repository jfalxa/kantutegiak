---
id: tx-1478
izenburua: Zapaiatik Zintzilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q--Hu2ZPi5Q
---

Egutegian x bat, eguna iritsi da
ez dago inor etxean eta ezin zara falta.
Fou roses da nahi dudan lore sorta bakarra.
Ez du urik behar.

Biharko egunik ez bailitzan apurtuko gara,
leihotik botako ditugu buruhausteak,
ez ditugu entzungo bizilagunen kexak,
igo bolumena!

Zapaiatik zintzilik, belauniko komunean,
logelan baina ez lotan,
(Managaizotaaaakkk)
Beti gazte eta eroak, berriro ere mozkorrak
ajea latza izango da.

Egutegian x bat, eguna iritsi da
ke artean barreak, less than jake sutan,
txupitoak bikoizten dira edo nire gauza da?
Hartu erdikoak!

Zapaiatik zintzilik, belauniko komunean,
logelan baina ez lotan,
(Managaizotaaaakkk)
Beti gazte eta eroak, berriro ere mozkorrak
ajea latza izango da.

Eskutik joan zaigu munipa jauna,
barkatu su señoria,
noski nahi gabe izan dela...
(Managaizotaaaakkk)
Beti gazte eta eroak, berriro ere mozkorrak
ajea latza izango da.