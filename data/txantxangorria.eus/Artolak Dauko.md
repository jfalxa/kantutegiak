---
id: tx-7
izenburua: Artolak Dauko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l18j4yV-s-s
---

Artolak deuko, Artolak deuko,

Artolak deuko famia

berea deuela, berea deuela,

mundu guztiko jendia.

Goxean Parisen, gaubean Parisen,

Parisko kalean luzea.

Auxe kalea paseu ezkero,

atzeko zaldia neurea.

Durangon bazkaldutä
Mañariän gora,               
Urkiolara goaz, morena,
San Antoniora, ai, ai,
Urkiolara goaz, morena,
San Antoniora.

Urkiolara joan da

zapatak urrutu (bis)

Aite San Antonio, morena,

barriek engoitzuz. (bis)

Urkiolara joan

Mañariti gora (bis)

trokotza sartu nauan, morena,

eperditi gora. (bis)

Aite San Antonio

Urkiolakua (bis)

askoren biotzeko, morena,

santu debotoa. (bis)

Askok egoten dotzo

San Antoniori (bis)

egun batian joan, morena,

bestian etorri. (bis)