---
id: tx-3341
izenburua: Basajaunaren Babesean - Sembrando Kaos -
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gYC3FrssOl4
---

Ezagutu ginen gau hartaz oroitzen pasatzen ditut egunak
Izerditan esnatzen naizeneko ilusio momentuak

Eta orain basajaunaren babesean sorginak lagun nora ezean, edonon sentitzen zaitut
Gogoan zure irribarre maltzurrak, gau haietan bizitako abenturak, ondoan sentitzen zaitut

Gaur ere bakardade mugagabean, isiltasun eta negar artean, irri bat ateratzen zait
Gogoan zure indarra eta kemena, aurrera egiteko ahalmena, ez ahaztu maite zaitut

Gure eguna helduko da esaten dit nire senak
Iluntasuneko argia argituko du bidea, harrotasunez betetzen nau zure irribarreak
Neguko egun hotzetan hotzikara bakoitzean zure falta somatzen dut
Egun argi izar beltzetan muxu eta besarkada laster ikusiko gara

Beste behin bakardade mugagabean, isiltasun eta negar artean, irri bat ateratzen zait
Gogoan zure indarra eta kemena, aurrera egiteko ahalmena, ez ahaztu maite zaitut

Gure eguna helduko da esaten dit nire senak
Iluntasuneko argia argituko du bidea, harrotasunez betetzen nau zure irribarreak
Neguko egun hotzetan hotzikara bakoitzean zure falta somatzen dut
Egun argi izar beltzetan muxu eta besarkada laster ikusiko gara