---
id: tx-2043
izenburua: Esnesaltzailearena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KC46y9uzKXA
---

Akordatzen naiz txikitan
Oraindik lotan nintzela
Aita goizeko bostetan
Behiak jezten hasten zela
Ama ondoan zeukala
Amak laguntzen zuela
etxez etxe aritzeko
esne saltzaile bezala
Kuatrolatas zahar batean
aita aurrean gidari
ama atzean joaten zen
esne marmiten zaindari
Amak ez zion, ordea,
Inoiz ekiten lanari
muxutxo bat eman gabe
bere seme bakoitzari
Etxez etxe tinbre joka
portaletik portalera
Kaixo, egun on, zer moduz?
kortesiazko galdera!
irribarre bat eskainiz
esnearekin batera
eta beraiena galduta
itzultzen ziren etxera
Lege berrien sartzeak
belztu zien bekokia
esne saltzaile langintza
ez omen zen egokia
Tetrabrikak hartu zuen
esne marmiten tokia
eta pena da garai haiek
akordatzen egotea!

heldu gera eskusoinuarekin, 
hernanira, eta denak begira,
hartu bada, pazientziarekin
sueltia utzi, utzi kalejira

jo ezazu, jo ezazu pasodoble
jo ezazu eta lotsa hori gorde
jo ezazu, jo ezazu pasodoble
eta lotsa hori gorde, soinujole!

ordulari zaharrenak bezala,
orduari denbora kendu nahirik
adi adi, ta zer segi dezala
baina zuk ez, zuk ez trikitilari

jo ezazu, jo ezazu pasodoble
jo esazu eta lotsa hori gorde
jo esazu, jo ezazu pasodoble
eta lotsa hori gorde, soinujole

jo ezazu, jo ezazu pasodoble
jo esazu eta lotza hori gorde
jo esazu, jo ezazu pasodoble
eta lotsa hori gorde, soinujole