---
id: tx-2746
izenburua: Bide Elektrikoa I
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VT6J4xsg4PA
---

Zapalduta utzi ditut,
ihesean joan ziren
gudari haien oinatzak,
guztia galduta
zegoelako.

Zapalduta utzi ditut,
nahigabeturik zeuden
mehatzarien oinatzak,
aurreko txandan
jasotakoagatik.

Zapalduta utzi ditut,
aitonak laga zituen
nire aitaren oinatzak,
non jagole berdeak
arrenka dauden.

Asko maite zintudan,
eta nik inguru hori
erakutsi nahi nizun,
bide elektriko bat
izan baino lehen.