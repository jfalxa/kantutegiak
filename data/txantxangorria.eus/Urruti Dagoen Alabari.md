---
id: tx-1770
izenburua: Urruti Dagoen Alabari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nRhi1LFxV2o
---

Alabatxo zuretzat
lehenik bi muxu
urruti zaude ta
hartu izkidazu
zu etxetik joatea
nuen pixu
aita baten bihotzak hitz
egiten dizu.
 
Zein lore polita
zu zintugun etxean
zein bakarti ginan
urrutiratzean
hortaz ohartzen naizen
une bakoitzean
arantza bat zaztaka ari
zait bihotzean.
 
Mundura joan zinan
zinelarik gazte
aingeru polit bat
hemezortzi urte
aitak esan zizun ba
zatoz laixte
egunak eta gauak oso
luze doazte.
 
 
Ingelesa ikasten
maiteñoa Londresen
nik nahi baino gehio
ari da luzatzen
eta orain maite bat
dezula hain zuzen
ez dakit urrunagora ez
ote zoazen.
 
Bidea aukeratu
lezake bakoitzak
lore ta arantza
hor ditu bizitzak
ez dizkizut jarriko
nik baldintzak
baina ez gorrotatu inoiz
aitaren hitzak.
 
Ongi dago batek zu
zaitulako maita
alaba bakarra
gertu zaitut nahi ta
egunsentiro nago
beti zure zai ta
mundua da lekuko aita
da beti aita.
 
Hitzak: Nikolas Zendoia
Musika: Bernardo Alberro