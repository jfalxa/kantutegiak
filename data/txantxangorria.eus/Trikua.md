---
id: tx-45
izenburua: Trikua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CCzLX2KkZUw
---

Gelditu arnasa hartzeko
Arantzak barrurantz irten baino lehen
Gelditu erreparatzeko
Kanpoan aterri dagoen
Gelditu arnasa hartzeko
Arantzak barrurantz irten baino lehen
Gelditu erreparatzeko
Kanpoan aterri dagoen
Beti da gaua olatu azpian
Arrokarik ez guretzako
Ezer ez bada betirako
Ez espero goiz alba arrastian
Ez espero goiz alba arrastian
Gelditu arnasa hartzeko
Sinisteko izan dela istripua
Gelditu erreparatzeko
Oraindik bizi den trikua
Gelditu arnasa hartzeko
Sinisteko izan dela istripua
Gelditu erreparatzeko
Oraindik bizi den trikua
Beti da gaua olatu azpian
Arrokarik ez guretzako
Ezer ez bada betirako
Ez espero goiz alba arrastian
Ez espero goiz alba arrastian
Oraindik bizi den trikua
Oraindik bizi den trikua
Mikel