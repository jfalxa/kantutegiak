---
id: tx-2161
izenburua: Amets Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5RzHaw2xFA4
---

Abestia maitasun balada bat da, dudarik gabe, Maider Zabalegiren ahotsaren inozentziak ederki eramanik, halako batean, leloa hurbiltzerako, erritmo bikoiztu egiten da eta oso pop kantu alaia ageri da, Alaitz Telletxearen ahotsaren duetoaren laguntzaz. Musikalki oso senziloa dirudien arren (bateria, gitarra, trikia, ahotsa, panderoa), sonoritate berri bat ekarri zuten bere garaian, pop musika maite zuen belaunaldi gaztearentzat oso etnikoak ziruditen soinu txikia eta panderoa errekuperatuz, alegia, edukin etnikoa gordeaz, lenguaia moderno, atsegin eta sinesgarrian hurbiltzen ziren publiko gaztearengana, nolabait oroimen historikoaren trasmisioan kate begi garrantzitsua erikiz. 

Azken hau izan da, nere ustez, Trikitixa talde gazte guzti hauen balorerik garrantzitsuenetakoa. Abesti honetan , konkretuki, gaztearentzat garrantzitsuak diren hainbat gai jorratzen ditu taldeak: maitasuna eta zibilizazioaren ustelkeria, naturaren hondamendia. Emaitza hain da eztia, hain zuzena, zeren mezu biak ederki uztartzen ditu, azkenik kantu disdiratsu bezain itxaropentsua suertatuz; amodioa, ametsetan besterik ez bada, beti da irabazle.