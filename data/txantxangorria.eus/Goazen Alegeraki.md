---
id: tx-883
izenburua: Goazen Alegeraki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rCYPArrgkSo
---

Goazen elegaraki,

            Bide haundian beti,

            Goazen beti!

            Goazen alegeraki,

            Bide haundian beti,

            Mila xori iduri!

 

 

      I

 

Ez dira ez beldur xoriak,

Iragaiteko urrungo mendiak!

Ez gira ez beldur gu ere,

Bidea harritsu bada ere!...

 

 

      II

 

Izan dadin euria edo

Gure mendi gainetan haize hego!

Gu bagoaz beti aintzina,

Gora botatuz irrintzina!

 

 

      1963-64.ekoa. Milafrangako «Euskaldun Xoriak» deitu udalekuarentzat egina, kantu berri beharretan baiginen!