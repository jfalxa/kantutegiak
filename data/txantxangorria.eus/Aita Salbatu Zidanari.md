---
id: tx-354
izenburua: Aita Salbatu Zidanari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IdhM-4KyMsE
---

"Aita Salbatu Zidanari" (Jon Bergaretxe/Joxe Azurmendi) 1978.


Arratsero pasatzen ziren
soldaduak frenterako.
Nun ote zen aita?
 
Gizonen arimaraino
sartuta zegoen gudua.
Arratsalde gorriak
ez zuen ederrik,
bildurra zekarren.
Gabak ez zuen atsedenik,
bildurra zekarren,
goizaren bildur.
Nun ote zen aita?
 
Eskuak altuan, horma luzearen kontra,
fusil biren begiek zainduta
dago aita.
Gau da, goizean hilko dute.
 
Arratsero pasatzen ziren
gudariak frentera.
 
Ez ziren etsaiak,
anaiak ziren,
eta anai arteko gudua.
 
Goizaldean,
hilak deskargatu dituztenean
ez zegoen gure aita.