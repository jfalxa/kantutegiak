---
id: tx-887
izenburua: Kantu Hau Zuretzat Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kuNe59t2Ckk
---

Olerkaria izan banintza
Aurki nezake xuxen hitza
Bideratzeko nere eskaintza
Zuri nere aitortza

Aspaldidanik nauzu jasaiten
Beti fidelki sustengatzen
Oker banago nauzu zuzentzen
Eta beti barkatzen

Baina kantari soil bat naiz eta
Dena kozka ta kezka
Ahal guttiko sasi profeta
Ilusioz beteta
Har zazu otoi nere eskaintza
Nahiz gutti bada
Kantu hau zuretzat da

Margolari bat izan nahi nuke
Zure itxura marrazteko
Kolore guziz apain nezake
Argienak gogoko

Zor dizudana nola ordaindu
Da nere kezka ta ezina
Ni ez naiz ezer zu ez bazaude
Hau da egi gordina