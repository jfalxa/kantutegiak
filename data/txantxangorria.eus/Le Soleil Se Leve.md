---
id: tx-216
izenburua: Le Soleil Se Leve
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M3i1HKM-4c4
---

Audioa eta bideoa Lezoti estudioan grabatu ditugu 2020 urtean, Gorka Urraren eskutik. Audioaren nahasketa Beñat Igerabidek egin du.

Musika: Iker Lauroba
Hitzak: Jon Garmendia "Txuriya"

Musikariak:

Iker Lauroba: Gitarra akustikoa, ahotsa
Leire Berasaluze: Ukelelea, ahotsa
Urbil Artola: Dobro, ahotsa
Olaia Inziarte: Pianoa, ahotsa
Maria soriazu: Baxua ahotsa
Andoni Etxebeste: Bateria, ahotsa

Oroitzen zaitut inoiz
karrikak zeharkatzen, 
Gare du Midin behera, 
euritan, oinez...

Neguko arropetan
desiraz betetako 
mundu bat bizi 
dela oroitarazten

Hiri arrotzen toki ezezagunetan
babesten ginen, maitatzen ginen.

Eguzkia esnatzean
haizea pausatzen zen
esku dardartietan
le soleil se l.éve.

Hiri arrotzen toki ezezagunetan
babesten ginen, maitatzen ginen.

Beharbada amaitu gabe
utzitako ipuinak
korapilatzen dira nire baitan.

Itxi gabeko ateek
ireki dizkidate
arrakala berriak zaurietan.

Lokartu, itzartu
le soleil se léve
Ekia ari da esnatzen.