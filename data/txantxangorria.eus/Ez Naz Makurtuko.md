---
id: tx-913
izenburua: Ez Naz Makurtuko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-QrEb8vJOQM
---

Hitzak eta musika: Gatibu

𝗚𝗮𝗿𝗲𝗻𝗮 𝗴𝗼𝗿𝗮

Garena maitatu eta izateko nahiz bizitzeko inposatu nahi diguten modu bakarraren aurrean ez makurtzeko aldarria zabaldu gura dugu lau haizetara. Zaborretara aurreritzi eta konplexuak, ospatu ditzagun gure identitateak, gure gorputzak, gozatzeko aukera anitzak. Ez gaitezen makurtu ikustezin bihurrarazi eta umiliatzen gaituzten patroi eta jarreren aurrean. Ez etxean, ez kalean, ez ikastetxean, ez lanean, ez jaietan…Ez naz makurtuko!

·GURE ESKER ONA:
Murueta Lorategia 
Etxegabe 
Biltegi 
Iñigo Etxebarrieta
Bilboko Udala 
Bilbao Bizkaia Film Comission 

·Dantzariak:
Udara Zubillaga
Tony Viser
Ian Bericiartua 
Ernesto Miari 
Isma Dieguez
Koro Euba 
Izaskun Sarabia 

·ROSS DANTZA ESKOLA:
Maria L. Torrado González
Ainhoa Astuy Gabantxo
Patri Macho de la torre
Leire Etxeandia Solaguren
Itziar Agirre Zubiaur
Nile Ansotegi Arrien
Eider Bermeosolo Perez
Ane Sarduy Kanalaetxebarria
Irantzu Onaindia Arteach
Marina Gomez Garcia
Amaia Agirre Larrea
Lucia Bericiartua Asteinza
Leire Gordo Badiola
Ismael Dieguez Morales
Naroa Arandia Calzacorta
Maddi Gerrikabeitia Uribarri
Itxaro Erezuma Eiguren
Irantzu Altzibar Llona
Oihane Ruiz Arruzazabala
Izaro Kobeaga Arrien
Maeva Morlesin Beares
Ane Iturriarte Basterretxea
Elena Jiménez Morillas
Iraide Macías Berzal
Laura Macías Berzal
Iraide Alzibar Llona
Arale Lejarzegi Azurmendi
Iñigo Cano Ibarra
Idurre Onaindia Arteach
Maria Egiarte Barrainkua
Alaitz Elorrieta Barrena
Anne Magunagoikoetxea Torrico
Sara Iturriarte Basterretxea
Olatz Barayazarra Garay
Monika Oteo Borda
Aintzane Albizu Almeida

·Ekoiztetxea:
FMK FILMAK 

·Zuzendaritza:
Oier Plaza 
Koreografia 
Rosa Rementeria del Villar 

·Produkzioa:
Ivan Batty

·Make up eta jantziak:
NOVÉREK 

·Irudia eta muntaia: 
Oier Plaza
Mikel Etxebarria 
Kerman Goikouria
Daniel Asua 
Ager Galarza

Hemen nire gelan sartunde nago ni kantak buruz ikasten bakarrik ta zuek kalean egongo zarie ze ja ez nago whatsappeko taldean.
Desberdina naz eta badakit, eta zer! ez gehiago, ez gitxiago, bardin! zure bildurrak, zure akatsak nire begietan ikusten dozuz ez dekot lotsatu beharrik hauxe naz ni, eta gustoko ez banozu, jodidu zaitez!

Bizitza maite dot nik! goizeko eguzkia, eta inoren aurrean ez naz makurtuko!

Uh! Ez! Ez naz makurtuko! Txikia banaz ni, lodia banaz ni, gonak edo prakak niretzat bardin nahiz eta barre egin artista bat naz ni nire burue maitatzen badakit.

Ez naz amore emoten dabenetakoa eta inork ez doste kenduko gogoa nahi dodan moduen hegaz egiteko edo lore bi belarrian ipinteko neure margoak neuk aukeratuz kolorez zikindu horma zuriak ez dekot lotsatu beharrik, ez hauxe naz ni, eta gustoko ez banozu, jodidu zaitez!

Bizitza maite dot nik! errespetua, eta inoren aurrean ez naz makurtuko!

Uh! Ez! Ez naz makurtuko! Txikia banaz ni, lodia banaz ni, gonak edo prakak niretzat bardin naiz eta barre egin artista bat naz ni nire burue maitatzen badakit.

Iee, iee… maitatzen badakit.

Zabaldu atea, emon egizu pausu bat zeurea da eta kalea eta zeure buruengan konfidantza osoa izan ez itxi inori zapaltzen.