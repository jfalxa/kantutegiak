---
id: tx-2296
izenburua: Hilezkorrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cz1KE-iEO3o
---

"Hilezkorrak" Azken Sustraiak-en "Utopian Gara Aske" bigarren diskatik hartua.

Canción "Hilezkorrak" de Azken Sustraiak, tomada de su segundo disco "Utopian Gara Aske".

Kolaborazioak/ Colaboraciones:
- Amaiur (Odolkiak Ordainetan, Rotten XIII) - punteoa / punteo
- Xabi (Odolkiak Ordainetan) - kontrabaxua / contrabajo
- Iñaki, Urko eta Amaiur (Odolkiak Ordainetan), Txusmi eta Ekaitz (Against You), Soto eta Olli (Brigade Loco) - koroak / coros

Kontaktua/contacto: 
azkensustraiak@gmail.com 

LETRA:

[Euskera]
Bide honen amaieran, berriro lur izatean
Argi ta itzalen borroka betiko amaitzean aztertuz gure urratsak
OHARTUKO GARA ORAIN DUGUN GUZTIAZ
GARRASIKA ELKARREKIN SORTU DUGUN BATASUNAZ
AKORDE HAUEN ATZEAN, ELKAR MAITATU ETA ZAINTZEAN
HILEZKORRAK GARA

Ta guzti hau zertarako, galdetzen diguten oro
Oroitzen naiz familia honetaz, bizi izan dugunaz malko zein barre artean
HORREGATIK ELKAR TINKO BABESTURIK
GARRASIKA ORAIN GURE ARTEAN EZ DAUDENENGATIK
AKORDE HAUEN ATZEAN, ELKAR MAITATU ETA ZAINTZEAN
HILEZKORRAK GARA

GURE AZTARNA UZTEN, BARNEKO GRINA LANTZEN
GURE BIZI OKERREI NORABIDEA EMATEN
BATERA AHOTSAK URRATZEN SOKAK APURTU ARTE
BIDELAGUN GARENOI LEIAL IZATEN