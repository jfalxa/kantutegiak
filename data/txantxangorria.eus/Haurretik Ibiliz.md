---
id: tx-255
izenburua: Haurretik Ibiliz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bAkCOQgVa64
---

Haurretik ibiliz ni mundu zabalean,
hitzegin nahirik jende artean,
hizkuntza mota asko nik baditut ikasirik,
euskarak ez du inon parerik.
Mintzairetan euskara omen da lehena,
dena den, seguru dala ederrena,
otoia segi dezagun,
une eder bezain argi mintzatuz,
noiz nahi ta non nahi.
Hizkuntza era asko baditut ikasirik,
euskarak inon ez du parerik!!
Herritik urruntzean euskaldun gaztea,
ez ahaztu hizkuntza maitia,
oraingoz asko da ahaztu duenik,
merezi ote euskaldun izenik?
Hizkuntza hau da hain eder, hain da maitagarri,
ez dadin izan oroitgarri,
bizi maitea, orain eta gero,
berma gaiten euskaldunak oro.
Hizkuntza era asko baditut ikasirik,
euskara inon ez du parerik!!
............................................................................
Jon Bergaretxe jaunaren kantu eder bat, bere "Gaur ere goizegi" diskotik. 1990. urtean kaleratu zuen Elkar zigilupean, honako lagunekin...

Jon Bergaretxe, ahotsa ta gitarra
Aitor Amezaga, teklatua
Pako Diaz, bateria
Mikel Artieda, baxua
Ainhoa Mujika, ahotsa

80. hamarkada hasieran hasi zen Jon Bergaretxe jauna Denbora batez, AZALA folk taldean aritu zen, abeslari ta gitarrajole bezala. Talde horrekin, "Zaldiko maldiko" disko ederra ekoiztu zuen 1987. urtean, kontzertu ugari eskainiz, bai Euskal Herrian, bai atzerri aldean, Madril, Guadalajara, Alemania, edota Parisen ere. 1989. urtean, Jon Bergaretxe Euskal Herriko Kantu Txapelketa irabazi zuen eta, ospe hori lorturik, kantaldi asko ematen hasi zen bakarlari gisa. Elkar diskoetxeak disko bat grabatzeko aukera eman zion 1990. urtean. "Gaur ere goizegi" izenburuko diskolan aparta kaleratu zuen, musiko talde itzel baten laguntzarekin, Mikel Artieda, Angel Unzu edota Aitor Amezaga.

ADI EGON... Izen berdineko beste Jon Bergaretxe kantari euskaldun dotore bat dago. Honek 70. hamarkada bukaeran, disko bat kaleratu zuena, "Jon Bergaretxe" (1978) izenburuarekin. Ez nahastu, biak artista desberdinak bait dira. Honako Jon Beregatxe, Oiartzun herrikoa da, beste, Jon Bergaretxe Areizaga, Eibar herrikoa.