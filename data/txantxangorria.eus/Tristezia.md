---
id: tx-35
izenburua: Tristezia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6VPTuHP2bI0
---

Bazter orotan tristezia, hau da gaüza harrigarria,
Hiltzera doa mündüa, bürüa galtzen dügüa ?
Ikusi dügü argizagia, eia ! bazenez bizia ?
Heben aldiz bada jendea, ezin eman oroer asea.
Sosak mündüa erabilten, damürik herra orotan pizten,
Zenzüa deikü galarazten, malürra aitzina ereiten.
Mündü triste hau zentako, ifernüa bezalako,
Oihü baten egitea, egün da gure mania.

Hitzak : Altzükütarrak
Müsika : Jean ETCHART