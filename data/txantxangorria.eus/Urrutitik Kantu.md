---
id: tx-1769
izenburua: Urrutitik Kantu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gacy6TgjzzQ
---

Urrun dauden guzti hoiei
Kantaiotzezu ozenki
Eta entzun dezatela
Abesten gaudela eurekin.

Urrun dauden guzti hoiei
Kantaiotzezu ozenki
Eta entzun dezatela
Abesten gaudela eurekin.



Gertu dauden guzti hoiei
Esan ozen abestu arren
Eurek baino ozenago
Abestuko dugula hemen

Gertu dauden guzti hoiei
Esan ozen abestu arren
Eurek baino ozenago
Abestuko dugula hemen

Tarte dauden guzti horiek
Bidean dabiltza kantari
Eta eurek dira hemen
Urrun daudenen abeslari.

Tarte dauden guzti horiek
Bidean dabiltza kantari
Eta eurek dira hemen
Urrun daudenen abeslari.

Urrun dauden guzti hoiei
Kantaiotzezu ozenki
Eta entzun dezatela
Abesten gaudela eurekin.

Gertu dauden guzti hoiei
Esan ozen abestu arren
Eurek baino ozenago
Abestuko dugula hemen

Hemen! Hemen!

Tarte dauden guzti horiek
Bidean dabiltza kantari
Eta eurek dira hemen
Urrun daudenen abeslari.

Tarte dauden guzti horiek
Bidean dabiltza kantari
Eta eurek dira hemen
Urrun daudenen abeslari.

MAITE DUZUN HORI
ESAN ZIGUTEN ZAINTZEKO
ETA DISTANTZI/AK
EZ DITU ALDENDUKO
ZUENGATIK GARA
ILUNPETATIK ARGIRA
GARELAKO
BIHAR ERE IZANGO DIRA

MAITE DUZUN HORI
ESAN ZIGUTEN ZAINTZEKO
ETA DISTANTZI/AK
EZ DITU ALDENDUKO
ZUENGATIK GARA
ILUNPETATIK ARGIRA
GARELAKO
BIHAR ERE IZANGO DIRA

MAITE DUZUN HORI
ESAN ZIGUTEN ZAINTZEKO
ETA DISTANTZI/AK
EZ DITU ALDENDUKO
ZUENGATIK GARA
ILUNPETATIK ARGIRA
GARELAKO
BIHAR ERE IZANGO DIRA

MAI/TE DU/ZUN HO/RI
E/SAN ZI/GU/TEN ZAIN/TZE/KO
E/TA DIS/TAN/TZI/AK
EZ DI/TU AL/DEN/DU/KO
ZU/EN/GA/TIK GA/RA
I/LUN/PE/TA/TIK AR/GI/RA
GA/RE/LA/KO
BI/HAR E/RE I/ZAN/GO DI/RA

MAI/TE DU/ZUN HO/RI
E/SAN ZI/GU/TEN ZAIN/TZE/KO
E/TA DIS/TAN/TZI/AK
EZ DI/TU AL/DEN/DU/KO
ZU/EN/GA/TIK GA/RA
I/LUN/PE/TA/TIK AR/GI/RA
GA/RE/LA/KO
BI/HAR E/RE I/ZAN/GO DI/RA

U/rrun dau/den guz/ti ho/ri/ei
Kan/ta/io/tze/zu o/zen/ki
E/ta en/tzun de/za/te/la
A/bes/ten gau/de/la eu/re/kin.

U/rrun dau/den guz/ti ho/ri/ei
Kan/ta/io/tze/zu o/zen/ki
E/ta en/tzun de/za/te/la
A/bes/ten gau/de/la eu/re/kin.

MAI/TE DU/ZUN HO/RI
E/SAN ZI/GU/TEN ZAIN/TZE/KO
E/TA DIS/TAN/TZI/AK
EZ DI/TU AL/DEN/DU/KO
ZU/EN/GA/TIK GA/RA
I/LUN/PE/TA/TIK AR/GI/RA
GA/RE/LA/KO
BI/HAR E/RE I/ZAN/GO DI/RA