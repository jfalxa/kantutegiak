---
id: tx-318
izenburua: Ipuinetako Erregiña
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HHq3l1oFraA
---

Bart iluntzean ohean
ibili nintzen ametsetan
biok eskutik helduta
irribarrea ezpainetan
maitasuna nabari zen
hitz sinple,txiki guztietan
nire islada ikusten nuen
zure begi ilun hoietan

Ipuinetako erregiña,
kuxkuxeroa eta sorgiña
beltzak bihutu ziren ilunabarrak
argitzen ditu zure irrifarrak

Bizitzan pasako dugu
mila irri,malko,apuru
baina batera egonda
ondo irtengo da seguru
ametsak errealitate
bihurtzeko pausu bat badu,
biok begiak itxita
batera amestuko dugu


Ipuinetako erregiña,
kuxkuxeroa eta sorgiña
beltzak bihutu ziren ilunabarrak
argitzen ditu zure irrifarrak

Gazteizko erregiñarentzat,Larrabetzuko printzesak egina! ^^