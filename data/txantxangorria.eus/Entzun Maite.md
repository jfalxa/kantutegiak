---
id: tx-93
izenburua: Entzun Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AplUK_E0lu8
---

Taldekideak
Enrike Ruiz de Gordoa
Sabin Salaberri
Xabier Galdos
Gaspar Ruiz de Gordoa
Juanjo Leza
Xabier Castillo "Xeberri"

Entzun maite bakarrik hor zaude
zugandik aparte ekarri naute
baina nori ez dugu guk kalte
nik benetan zaitut maite
nik benetan zaitut maite

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Entzun maite bildurturik zaude
zer gertako den ikusi arte
eginkizun badaukagu hemen
eta ezin utzi lortu arte
ta ezin utzi lortu arte

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

E, eh!
E, eh!
E, eh!

Zoriona herrian, herrian
omen zera maite
egizalea izan behar dozu
eta ni poztuko nauzu
eta herria poztuko duzu

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin

Ez, ez, ez nigarrik egin
ez nigar egin
herria da ta gurekin
ez nigar egin