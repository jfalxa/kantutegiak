---
id: tx-1326
izenburua: Belenen Sortu Zaigu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zWPb04dxemY
---

Belenen sortu zaigu Jainkoa
arratseko gauerdian.
Hotzez dardaraz dago gaisoa,
lasto pixkaren gainean.
Bero ahalez bero dezagun,
apa emanez musuan.

2
Ainitz maite dut nik bere ama
ni sortu ninduelako.
Baina haurtxo hau maiteago dut,
nigatik jaio delako,
ta gurutzean gero odola
isuriko duelako.

3
Askatxoan etzanda zaude Zu,
Jaingoikoaren Semea.
Zerua utziaz hartu dezu
gure gizon izatea.
Zure maitasun ordainez
izango naiz ni Zurea.

4
Ordainez har zazu, Jesus Haurra,
dudan gauzarik onena:
tori bihotza, tori arima,
hau da noski nahi duzuna.
Agur haurra, Zu zera gaurdanik
maite dudan maitena.