---
id: tx-667
izenburua: Zorionak Kutuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1GmkaTEcvwA
---

Zorionak kuttuna
badakit malen zuretzako dela
egun pozgarria, benetan handia;
urte bat gehiago bete duzu laguna,
bihotz-bihotzez esaten dizut:
zorionak kuttuna