---
id: tx-941
izenburua: Beltzi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qhD7kgmt4qQ
---

Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozga/rri
Beltzi guau guau
Txakurtxo saltaria
jaten du okela
ontzia hustu du ta
bete du sabela
orain ezin mugitu
nola bestela
ase dagoenean
jartzen da honela
belarri belarri
muturran hanka jarri
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
Hezurra urruti bota
ekartzen du beti
katua ikusten badu
badoa atzetik
arkakusoek kili kili
egiten diotela
urduri egoten
jar/zen da honela
belarri belarri
muturran hanka jarri
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
tapolan eserita
aldatu ilera
egun eguzkitsuan
salto bat aurrera
euria egiten badu
beste bat atzera
trumoiak entzutean
jartzen da honela
belarri belarri
muturran hanka jarri
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozga/rri
Beltzi guau guau
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozgarri
Beltzi guau guau
Beltzi guau guau
Beltzi guau guau
lagun maitagarria
denontzat pozga/rri
Beltzi guau guau