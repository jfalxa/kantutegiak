---
id: tx-1080
izenburua: Gamusinoen Bila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bfvyXGEi1bU
---

Kanpin-denda, lo-zaku,
ta linternak hartuta,
abenturan abiatu gara.
Ilundatakoan plazan 
egin dugu zita.
Zartegina eskutan,
goazen denok mendira.
Txoko guztietara begira,
gamusinoak gure zain dira,
bai!!

Gamusinoen bila,
zeruan izar pila,
ilargia guri begira,
Astindu zartaginak,
zabal ongi begiak,
adi-adi bi belarriak.

Urratsei jarraituz 
lerroan goaz urduri
eskua emanez aurrekoari.
Itzal handi bat
sumatu dugu bat-batean
kontuz-kontuz
segi diezaiogun bidean,
baina harengan hurbiltzean
astoa da arrantza batean
aii!!

Gamusinoen bila,
zeruan izar pila,
ilargia guri begira,
Astindu zartaginak,
zabal ongi begiak,
adi-adi bi belarriak.