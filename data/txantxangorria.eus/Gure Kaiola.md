---
id: tx-2605
izenburua: Gure Kaiola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2jWcULJjPk4
---

BONBERENEA EKINTZAKek aurkezten du: GLAUKOMA "kalima" diskoko bigarren singlea:  "GURE KAIOLA". 2017ko udaberrian Bonberenean grabatua.

Grabaketa eta nahasketa: Karlos Osinaga
Masterizazioa: Jonan Ordorika
Bideoa: Kliska Produkzioak



Ze guapa zauden. Ze guapo zauden.

Zer da narratsa hemen ta zer da elegantea? Zeinek esan behar dizu oain itsusia zeala?

Zeinek marraztu digu koadrantea? Zein dao erotuta ta zein da kabala?

Ilusio bat da errealitatea, ta gizonen bizio bat denai araua jartzea.

Norbeantzako horma bat da normalitatea. Baino nik ez det izan nahi normala.

Ta zuk ezta re! Etzealako artalde hontako zifra bat, etzea sartzen trajen.

Gezurren saren eroitzeie makina bat, goitik datorren papila jaten.

Beldurren aurren, heldu makila ta goazen, pareta mila dauzkeu ta gure paren.

Eta erreza ez dan arren, kareta kenduta zaudenen... ai! Ez dakizu ze sexy zauden.

Ze guapo zauden mozorroik gabe, ta artaldek esan dezala Beeee

Ze guapa zauden horren lotsagabe, estereotipo danak hausten.

Ze guapo zauden mundu hau aldatzen, zure rola zalantzan jartzen.

Ze guapa zauden. Ze guapo zauden.

Gizontasun eredu nik izan nun Action Man, zure emetasun modelo Barbie bat zan.

Eztao generoik genitaletan. Egitura mentala da, aber sartze zaigun gaztan!

Horregatik jartzen gaitue dietan, ta danak die Barbie ta Ken telebista ta aldizkarietan.

Hitzeite ez dun haurran begietan Disney-en maitasunaren fabula. Ipuin honen hasiera.

Nortasunen banaketan, zu woman eta ni man, bakoitza bere ziegan.

Nere heziketan dauden urrezko hesietan. Zure etiketak dauzken gezur ariketak.

Eredu bezela euki diteun figura patriarkalak. Pornografia izan da gure heziketa sexuala.

Heteroarau honen aizkorakaden ezpala. Hau ez da gizarte naturala. Ez du izan nahi.

Kapitalai halaxe komeni zaio, dominazio sistema bereizketan jaio da.

Testikulo gutxigo ta sentimendu gehio. Gizonezko bezela hautsi nahi det gure kaiola.