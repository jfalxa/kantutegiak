---
id: tx-1951
izenburua: Euskeraz Eta Kitto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C8YmkAjJkhc
---

Herri baten 
izate maila
Gorpu batentzako 
bihotza
Nortasun agiri 
dugu hizkuntza.

Euskeraren zapalkuntza
Behar du 
norbaiten laguntza
Guztion artean 
aurrera bultza

Zertarako da gehio?
Euskaraz eta kitto

Espainiak edo frantsesak 
gu ez gera
Harro esan 
goizetik gauera
Noizko bildu 
euskaldun guztiok batera?

Herriaren zapalkuntza
Behar du 
norbaiten laguntza
Guztion artean 
aurrera bultza

Zertarako da gehio?
Euskaraz eta kitto

Gaurko egoera hauxe 
xelebrea da beraz.
Harritzen naiz guraso 
askoren joeraz
Kalean gora euskadi 
etxean erderaz

Hizkuntzak asko dira 
mundu zabalean
Bat behintzat 
gorde dezagun 
modu apalean
Amak erakutsia 
bere magalean.