---
id: tx-3036
izenburua: Nor Da Erromesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dkrextHJL48
---

Nor da erromesa,
Funtsezko gogoan dabilena ez bada?
Oinarrizko higidura bakoitza
Erraietatik darion ur isuria da.

Maite denaz
pentsa daiteke,
maitasunez inoiz ez;
maitasunak ez baitu karirik:
bere eternitatea da.

Ibilkari zoroa,
beti dabil
berez daukanaren bila.
Ibilkari zoroa,
beti dabil
berez daukanaren bila.

Zoaz etxera,
zeure baitan den
etxe horretara,
Ibilbide guztiak
etxera itzultzeko baino ez dira.