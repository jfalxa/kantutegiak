---
id: tx-1271
izenburua: Bide Luze Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5_DbK0FeKqk
---

Isildu Zaitez taldearen bigarren diskako "Bide luze bat" kantua. www.isilduzaitez.com


Har itzazu nire eskuak
zure benetako pentsamendua.
Nekatzen ez zaituena,
nekatzen ez gaituena.

Bidai labur honetan,
bide luze bat.
Maitasun debekatu hau,
aholkuen ate arauen
Kontrakoa.

Bidai labur honetan
bide luze bat.
Bidai labur honetan
bide luze bat.

Har ezazu nire ahoa,
isiltasuna betetzen duen etena.
Borrokatu daigun batera,
izarren islada gure gorputzetan.

Besarkatu gaitezen
lotsarik gabe
Maitasun debekatu hau,
aholkuen eta arauen
kontrakoa.

Bidai labur honetan
bide luze bat.
Bidai labur honetan
bide luze bat.

Har itzazu nire eskuak
zure benetako pentsamendua.
Nekatzen ez zaituena,
nekatzen ez gaituena.



Bidai labur honetan
bide luze bat.
Bidai labur honetan
bide luze bat.

Bide luze bat, bide luze bat.