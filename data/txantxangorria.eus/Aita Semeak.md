---
id: tx-1472
izenburua: Aita Semeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qkMoJ0wMWio
---

Aita semeak tabernan daude
ama-alabak jokoan
Aita semeak tabernan daude
ama-alabak jokoan.

Berriz ikusi beharko dugu
behi gizena auzoan,
berriro ere ez da faltako
trapu zaharrik kakoan.

Aita semeak tabernan daude
ama-alabak jokoan
Aita semeak tabernan daude
ama-alabak jokoan.

Eta lapurrek ohostu dute
guk gendukana etxean,
eta gu gaude erdi biluzik
beti inoren menpean.

Aita semeak tabernan daude
ama-alabak jokoan
Aita semeak tabernan daude
ama-alabak jokoan.

Geurea dugu erru guztia,
geurea dugu osoan,
ez inori ba errua bota
Euskal Herria hiltzean.

Aita semeak tabernan daude
ama-alabak jokoan
Aita semeak tabernan daude
ama-alabak jokoan.

Baina gaztea naiz eta daukat
etorkizuna eskuan,
ez zaigu hilgo Euskal Herria
ni bizi naizen artean.

Aita semeak tabernan daude
ama-alabak jokoan
Aita semeak tabernan daude
ama-alabak jokoan.