---
id: tx-608
izenburua: Abendua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qKG6MPecLzM
---

Abendua da andra mari zuriaren plaza,
Kafea hosten daukat aurrean tabernako
Leihotik ikusten da mundua,
Denak azkarregi doaz,
Dublineko Nereak goizero bezela oparitu
Dit bere irria,
Eskerrak aingeruak badirela gurea,
Zinez bejondeiola.
Hau oraindik ezta amaitu entzui
Dazu maitea, nik eman izan banizu zuk
Behar duzun bakea. Baino oraindik hor
Gordetzen dut azken neguan zeugaz,
Isilpean pasa gendun itsasoari begira
Esaten dute uda iragan ostean,
Baretu direla bihotzak,
Baina nirea erruz gogortu nahi
Didaten haizeak eta hotsak
Parte esna pasatu dut pentsamendua suan,
Aspaldian hau…