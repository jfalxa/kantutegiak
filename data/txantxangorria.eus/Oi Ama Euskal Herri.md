---
id: tx-2185
izenburua: Oi Ama Euskal Herri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dZc_rvQt93E
---

Benito Lertxundik dituen abesti arrakastatsuen artean, abesti hau lore ederra da. Inkunablea

Anaia etxen da ezküntü
Bükatü niz oain joaitera
Ene opilaren egitera
Pariserat banüazü
Oi Ama Eskual Herri goxua
Zutandik urrun triste banua
Adios gaixo etxen dena
Adios Xiberua.
Pariseko bizitzia
Lan khostüzuriaz bagiazü
Bena berantzen zütadazü
Zure berriz ikustia
Oi ama Eskual
Bena denbora da llaburxko
Othoi ama entzün nezazü
Xokho bat haita izadazüt
Azken egun horientako
Oi ama Eskual...