---
id: tx-1808
izenburua: Urretxuko Euskaraz Bizi Nahi Dut Flashmob
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/512x1AYiRsc
---

Gehio, gehiago balio du
milak egiten duten apur banak
batek egiten duen mila baino.

Gau eta egun euskeraz bizi nahi dut
ta gauza bera egin dezakezu zuk
euskaraz bizi nahi dut goizean goiztik
ez da amets bat nire nahia baizik.

Etxean, kalean, lanean, lagun artean
mendira joatean eta ohean sartzean
egin ginen bizitzarik gabe nortasuna eta
eta era gorde gabe
kaleetan bat egin genuen
elkar maite genuela erakuste arren
Uooooooohh!! Oh!!
euskaraz bizi nahi
Uooooooohh!! Oh!!
ta euskaraz bizi nahi
Uooooooohh!! Oh!!
euskaraz bizi nahi
Uooooooohh!! Oh!!
ta euskaraz bizi nahi

Gau eta egun euskeraz bizi nahi dut
ta gauza bera egin dezakezu zuk
euskaraz bizi nahi dut goizean goiztik
ez da amets bat nire nahia baizik.

Etxean, kalean, lanean, lagun artean
mendira joatean eta ohean sartzean
egin ginen bizitzarik gabe nortasuna eta
eta era gorde gabe
kaleetan bat egin genuen
elkar maite genuela erakuste arren
Uooooooohh!! Oh!!
euskaraz bizi nahi
Uooooooohh!! Oh!!
ta euskaraz bizi nahi
Uooooooohh!! Oh!!
euskaraz bizi nahi
Uooooooohh!! Oh!!
ta euskaraz bizi nahi

Euskaraz bizi nahi dut goizean goiztik!!
Ez da amets bat nire nahia baizik!!

Euskaraz bizi nahi dut goizean goiztik
Ez da amets bat nire nahia baizik!!

Gau eta egun euskeraz bizi nahi dut
ta gauza bera egin dezakezu zuk
euskaraz bizi nahi dut goizean goiztik
ez da amets bat nire nahia baizik.

Etxean, kalean, lanean, lagun artean
mendira joatean eta ohean sartzean
egin ginen bizitzarik gabe nortasuna eta
eta era gorde gabe
kaleetan bat egin genuen
elkar maite genuela erakuste arren