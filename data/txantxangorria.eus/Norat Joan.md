---
id: tx-2512
izenburua: Norat Joan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5k1ZLtyIjhI
---

Norat zoaz, neska gaztea,
Hain ilunikan begia?
Norat hoa, mutil gaztea,
Nai duka nik erraitea?

Xoraturik ezarri haute,
Plazerra ez dela kalte....
Erori duk hire mundua,
Eta orai haiz galdua!

Norat doa gure mundua
Hain ardura uzkailia?
Norat doa, hartu bidea
Hain ardura bihurtia?

(Hitzak eta Musika: Manex Pagola)