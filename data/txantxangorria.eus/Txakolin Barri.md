---
id: tx-1700
izenburua: Txakolin Barri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FcqRklj9gAo
---

Txakolin barri, bai
Kostaldeko txakolin barri kupelan
Lagun artean edan
Txakolin barri, bai
Kostaldeko txakolin barri kupelan

 Bertso zaharrak, bai
Bertso barri eta zaharrak dekoguz
Lagun artean kantuz
Bertso zaharrak bai
Bertso barri eta zaharrak dekoguz

 Beharra goizero, bai
Beharra gogor eta goizero lantegin
Lagun artean ekin
Beharra goizero, bai
Beharra gogor eta goizero lantegin

 Izarrak jantzan, bai
Izarrak kantari ta jantzan hainbat gau
Lagun artean kherau
Izarrak jantzan, bai
Izarrak kantari ta jantzan hainbat gau