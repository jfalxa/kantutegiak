---
id: tx-3103
izenburua: Euskara Oi Euskara Gernika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rUJyexR2knM
---

Euskera, oi euskara
zutan da gorderik
mendez mende egoki
euskaldun gogoa.
Euskaldun jaio nintzen
euskaldun hazi
euskara hutsik amak
zidan erakutsi.
Euskara maite, maite
zabiltz ne(re)kin beti,
euskara hil ezkero
ez nuke nahi bizi
Euskera, oi euskara...
Euskara egunaz eta
euskara gabean
zahar ta gazteak beti
aditu gaitan
guztiok mintza dezagun
pozteko euskara
bihotzetik esana
euskaldunak gara.