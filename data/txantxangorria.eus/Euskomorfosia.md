---
id: tx-2850
izenburua: Euskomorfosia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-JNAZfK6Onw
---

Abenduak hiruan
soilik lainoz estaltzen
zitzaigun zeruan 
batzuk mihiei tiraka
zertan ote dabiltza?
arrastaka
Euskararen leihoan
hobe urrunez joan
beldarrak zutitzen hasi
tira leihoan
lotsaren bezeroak
gaur haren borreroak
eta laister haziko
zaizki hegoak

ISIL-ISILIK nahiz hitzontzia
KRISALIDA BAT BADUZU ZUK
ZETAZ JOSIA
BELDURREN AURKA ANESTESIA
BERTAN DUGU ORTZIA
EUSKOMORFOSIA

Liburuzain kantari
lorezain idazkari
sukaldari
Mediku, kazetaru,
autobusen gidari

Beti zelaian botata
egoten den hori
Fisio, pregoilari 
edo futbolinlari
Guztiokin batera,
kalapitxitxatu hadi!


ISIL-ISILIK nahiz hitzontzia
KRISALIDA BAT BADUZU ZUK
ZETAZ JOSIA
BELDURREN AURKA ANESTESIA
BERTAN DUGU ORTZIA
EUSKOMORFOSIA