---
id: tx-2700
izenburua: Garaituko Dugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YQx3XROgVCs
---

Garaituko dugu, garaituko dugu,

garaituko dugu, inoiz.

Bihotz-bihotzean, seguru naiz, 

garaituko dugu, inoiz.

Elkartuko gara, elkartuko gara,

elkartuko gara, inoiz.

Bihotz-bihotzean, seguru...

Bakez biziko gara, bakez biziko gara,

bakez biziko gara, inoiz.

Bihotz-bihotzean, seguru...

Libre izango gara, libre izango gara,

libre izango gara, inoiz.

Bihotz-bihotzean, seguru...

Ez gaude beldurrak, ez gaude beldurrak,

ez gaude beldurrak, egun.

Bihotz-bihotzean, seguru...

Garaituko dugu, garaituko dugu,

garaituko dugu, inoiz.

Bihotz-bihotzean, seguru...