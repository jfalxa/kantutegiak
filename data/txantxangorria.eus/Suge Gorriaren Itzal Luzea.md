---
id: tx-2263
izenburua: Suge Gorriaren Itzal Luzea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-mOp7no6xfE
---

Bazen behin Hondarbiko Jaizubia auzoan…
 
Padura, xenda, urmael, zubitxo eta bidezidorrak, zonalde naturalean hegaztien pausalekua, arrain eta dordoken kabiya eta aldi berean narraztien bizitokia.
Aroia gora, aroia behera, mareen joan etorrietan kanaberak agerian dira, aisialdi eta natur gune bare honetan, kresala eta erreken urgezaren hotsen artetik familia franko inguruaz gozatzen dute.
 
Bapatekotasunetik, arriskua, izua, beldurra, ikara, alarma, albiste harrigarri pentsaezina!!
Sugegorri afrikarraren argazkia bolo-bolo, birala, sarean, mugikorretatik hedabide guztietara heldu da. Berri latzgarriak munduari bira eman dio.
 
Gabon lurraldeko sugegorria Jaizubiko paduran ageri da, gizakiaren mehatxu zuzena!
Jo turuta, jo bozina, ejerzitoa mugiarazi, bunkerretan sartu,eremua hesitu, koarentena, paduran pandemia, kontuz, isolatu, ez sartu, sahiestu, emergentzia suge madarikatua harrapatu eta akatu arte!
 
Suhiltzaile, ikertzaile, AZTIko zientifiko, Ertzain, Udaltzain eta artaburu guztiak bertaratu dira,  ezer txarrik, ezbeharrik gerta ez dakiguken, kodigo gorria, non da suge gorria!?
 
Alkateren, ziraunaren prentsaurrekoa:
-"Lasai, atzemango dugu sugia eta bere sendi guzia, horretako eta gehiagorako prest gaude eta."
 
Isun administratiboa eta hilabete batzuetarako espetxe zigorra emakume bati, fake new hau zabaltzeagatik.

HITZAK

Jaizubiko paduran, omen da agertu
afrikako suge gorria, pozointsua eta hilgarria
argazki batekin izutu ditu, lekuko guztiak
emakume baten txantxa, hedatu da izurria bezala

Eremua hesituak , guztiak haren bila
inguruko mendiak, sutan dauden bitartean
sinesgarritasun eza, ondo aztertu gabe
nola arraio iritsi ote da, narrazti hau hona?!?!
    
Suge gorriaren itzal luzea
hozka egin ezkero egon zaitez lasai
bihotz taupadak azkartzerakuan
pozoina zainetan zabaltzen doa
       
Uuuuyyeee!
        
Gauza arrarotarako, Hondarbi eta New York
Jaizkibelko gune hezeetan, xiraunak daude eguzkitan!

xiraunak daude eguzkitan, ahoa zabaltzen!