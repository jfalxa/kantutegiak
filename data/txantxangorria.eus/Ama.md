---
id: tx-2323
izenburua: Ama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b7zWQSsIGL4
---

Negua heldu zaigu bizkor 
Egia esan goizago 
Abenduak 19 ditu 
Gaur ez zaitut deitu 

Txiste bat, muxu bi pantailan 
“Comete la vida”ren bat 
“Disfruta de la vida txabal” 
Zurea bukatu da 

Bob Dylan ikusi genuen 
Oroitzen zara zeinen txarra? 
Bruce Springsteen nirekin ikusi 
Nahi zenuela badakit 

El canto del locoren bat 
Iruñeko zezen plazan 
Patti Smith Donostian eta 
Green Day Bartzelonan 

Musikarena Aitarena 
“La gracia y el salero” zureak 
Hamaika kontzertu zurekin eta 
Orain malko eta irriak 

Gaur ez zaitut alboan, distantzian, 
eta ezinen dizut besarkadarik, muxurik eman 
Maite zaitudala ez nizun nik sekulan esan 
Hartu kantu hau maite zaitut baten gisan 

Baina hor zaude, nire kantu bakoitzean.