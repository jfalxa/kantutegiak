---
id: tx-3135
izenburua: Lingua Navarrorum -Skatu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/siYGaBWsUoA
---

Mendez mende iraun du herriaren babesaz
Eraso ezberdinei aurre eginez
Arbasoengandik jasotako hizkuntza

Ezin pentsa Nafarroako Erresuman
Nola bizi zitezkeen euskararik gabe
Eutsi diezaiogun gure nortasunari
Eta neska eta mutil euskara erabili!

Herria da gorputza hizkuntza bihotza
Hizkuntzarik gabe ez dago gorputza
Herririk gabe ez dago etorkizunik

Hizkuntz maite altxor polit herriko hitza
Gure barnean dago mantentzeko giltza
Sor dezagun tsunami itzel eta kristorena
Eta euskararekin kukurustan surfeatzera!

Euskal Herrian, badugu altxor bat
Euskara deitzen da Lingua Navarroruma 
Euskal Herrian, badugu altxor bat
Herria bihotzean eta euskara ahoan!

Ez izan belarri motz 
Eta euskaraz mintzatu egin bero edo hotz!