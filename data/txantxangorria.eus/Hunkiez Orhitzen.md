---
id: tx-188
izenburua: Hunkiez Orhitzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7nWUoD9S-SE
---

Urthiak algarreki dütügü iragan, sekülakoz nizate zordün zirekilan, 
Ene gatik düzie hartü hainbeste lan eta nik, arte hortan, zer deiziet eman? 
Zien bihotz hunetik hainbeste ükhenik, ardüra esker gaixto eman deiziet nik, Zer gatik niz hürrüntü ait’et’amen ganik? Haurrek ez ote dire deüsen dolümenik? 
Gü, haurrak, esker gabe, girade handitzen, bena gehitü ondun, hunkiez orhitzen, Ait’et’amak bihotzez deiziet aithortzen, ziren eskaza orai düdala senditzen. 
Nik, ziren ürhatsetan etsenplü dit hartü, eni hun egiteko beitzide higatü, 
Bide xüxenin gainti naizie gidatü, arauz ene haurrek ere dükie profeitü. 
Ait’et’amak zier dit pentsatzen frangotan, ziren haur batzen beniz denbora orotan, 
Ene süstengatzeko behar ordietan, lüzaz egon zitaie bizirik lür huntan. 
Hitzak: Roger IDIART eta Jean ETCHART 
Müsika : Jean ETCHART