---
id: tx-537
izenburua: Emakume
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gok9E-Jyne0
---

EMAKUME / Yogurinha Borova / Lyrics 

Egilea : Iker Sadaba 

LETRA : 

EMAKUME

Zaila gertatzen da
Zuk lana aurkitzea
Haurdun edo ama zaren
Da garrantzitsuena
Aitzakiak mila
Traba, oztopo pila
Hau da nahi dugun gizartea? 
Estereotipo anitz
Zuri ateak itxiz
Itxurak du agintzen
Gaitasunen gainetik
Orain da unea
Altxa emakumea
Feminismoaren garaipena

Emakume
Izan libre
Gizarte feminista eraikitzen 
Emakume
Irabazle
Matxismo zikina alboratzen

UOOOO…

Etxe barruan da
Egoera latzena
Etengabeko oihuak
Kolpe eta mespretxuak
Zure eskuetan da
Ihes egitea
Ta pikutara bidaltzea 

Eskubideen alde
Beti borrokatzen
Biolentzia matxistari
Goaz aurre egiten
Ama, ahizpa, alaba
Denon elkarlana
Autodefentsaren garaia

Emakume
Izan libre
Gizarte feminista eraikitzen Emakume
Irabazle
Matxismo zikina alboratzen

UOOOO…

Emakume…. Izan libre…. Emakume…. Irabazle (errezitatu)