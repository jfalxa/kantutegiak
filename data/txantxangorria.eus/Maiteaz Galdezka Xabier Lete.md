---
id: tx-1502
izenburua: Maiteaz Galdezka Xabier Lete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zPpJpevaN2g
---

Egunsenti batetan maitea zen hila,
kalera irten nintzen Jainkoaren bila,
nerekin aurrez-aurre esplika zedila,
behingoz utzi zezala zeru-goi ixilla.

Zutaz, maite, egina dut galde.
Zutaz, maite, egina dut galde.

Bakardade haundi bat neukan bihotzean,
negar egiten nuen zutaz oroitzean,
galdera egin nion Jaunari hotzean:
gizonok nora goaz mundutik hiltzean.

Zutaz, maite, egina dut galde.
Zutaz, maite, egina dut galde.

Orduak pasa ziren, ezer ez nun entzun,
nik ez dakit, maitea, konprenditzen duzun,
arratsaren ilunak eguna bildu zun.
Jainkoak inundikan ez zidan erantzun.

Zutaz, maite, egina dut galde.

Zutaz, maite, egina dut galde.



Etxeruntz nindoala oihukatzen nuan:

Jainkoa ez da bizi gizonen onduan,

urrutik zegok hura han bere zeruan

gu bakarrik utzirik umezurtz munduan.

Zutaz, maite, egina dut galde.
Zutaz, maite, galdera egin dut
baina Jainkoak ez zidan erantzun.