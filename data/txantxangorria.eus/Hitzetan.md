---
id: tx-2573
izenburua: Hitzetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J4JSK0-gunk
---

Abestiaren mezua herritar guztioi zabaltzeko asmotan, txapelketa batera aurkeztu gara. "Link" honetan sartu zaitezkete bideoa ikusi eta entzun, eta gustatuko izanez gero asko eskertuko genuke zuen denboraren pare bat minutu guri eskeintzen badizkiguzue, eta bozkatzen baduzue.

Produkzioa/Producción: Amaia Arregi, Idoia Asurmendi, Irati Lasaga, Ione Oyanguren

Hitzak/Letra: Xabi Igoa
Musika/Música: Idoia Asurmendi
Ahotsak/Voz: Idoia Asurmendi, Asier Sala
Kitarra/Guitarra: Asier Sala
Biolina/Violín: Angela Cecilia
Irudien muntaketa/Montaje: Eider Ballina


LETRA: "HITZETAN"

Goizetan, ispilu parean
harro irri egitean; 
eme oldarkorretan
negarrari beldurrik ez 
dioten gizonetan

Karminezko ezpainetan
txiste txarrenek erantzun 
dituzten
aurpegietan, serioenetan
erantzukizuna ere diren
erantzunetan

Angela Davisen hitzetan,
Maialen Lujanbioren
azken puntuetan;
Maurizia zenaren irrintzietan
ahots korden kontzientzietan

Berriz esan ez dezaten
ez dagoela gure eskuetan;
gizartearen arazoa den,
ardura komun den honetan

Zilegi diren beldurretan,
gure amonaren ezkutuko aienetan;
Maurizia zenaren irrintzietan
eta haren ondorengoetan

Angela Davisen hitzetan,
Maialen Lujanbioren 
azken puntuetan

Zilegi diren beldurretan, 
gure amonaren ezkutuko aienetan;
Maurizia zenaren irrintzietan
gure ahoz botatakoetan