---
id: tx-2471
izenburua: Fatou Yo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LIv9Q5-gRss
---

Musika: Herrikoia (Senegal)
Letra: Herrikoia eta Koldo Celestino


Fatou yo
sidiadialano
Fatou faye laye
Fatou
Fatou klema oundio
Fatou yo
si diadialano

Ni nauzu
ume polita
ume ederra,
lirain
eta oso jatorra,
munduko
haurrak bezala

Boutoumbele, botoumbele
botoumbele
Pozik gaude, pozik gaude,
pozik gaude

O mami sera, o mami casse,
boutoumbele
Handituz goaz, koskortuz
goaz, pozik gaude

Fatou faye faye
Fatou
Fatou klema oundio
Fatou yo
si diadialano

Ume ederra,
lirain
eta oso jatorra,
munduko
haurrak bezala

Fatou faye faye
Fatou
Fatou klema oundio
Fatou yo
si diadialano

Fatou yo
si diadialano
ni nauzu ume polita


Fatou faye faye
Fatou
Fatou klema oundio
Fatou yo
si diadialano

Ume ederra,
lirain
eta oso jatorra,
munduko
haurrak bezala