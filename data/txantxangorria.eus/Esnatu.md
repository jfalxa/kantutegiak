---
id: tx-997
izenburua: Esnatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1Lao1QgzSLY
---

Nomadak taldearen "Hasieratik +" diskoaren aurrerapen abestia.
Bideoklipa: Gaizka Peñafiel
LETRA:
Goizeko argi izpiek esnatuta
aukera berri batek pausatuta
zertan ari gara? Zertarako?
Egunero altxatzen, norentzako?

Ta erortzen bagara goizero
esnatu behar gara gauero

Kontzientzi galduak uneoro
spam mezu berriak egunero
mugaren bi ertzetan saltoka
norabide  galduan murgilduta

Ta erortzen bagara goizero
esnatu behar gara gauero!

Pausuz pausu bidea egin uooo
biharko ametsen lurraldetan
eroriz hazi ta ikasi uooo
norberaren abenturetan

Gure mugak zeharkatu
irtenbide bat aurkitu
gelditzen zaituen horri
urrunetik begiratu
kontraesanak gogoratu
barnean daramatzagu
errepide galdu honetan
norabide bat badugu!

Pausuz pausu bidea egin uooo
biharko ametsen lurraldetan 
eroriz hazi ta ikasi uooo
norberaren abenturetan

Pausuz pausu aurrera
biharko ametsak
erori ta altxatzean

#saveyourinternet