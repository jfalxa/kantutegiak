---
id: tx-2064
izenburua: Hator Hona
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AsFGAPaTzfU
---

Hator hona plentziarra
Euskararen alde,
Herriaren alde
Hemen dago PLENTZIA KANTARI!
Lagunen kabi
txoria txori
Kalez-kalez
Kantaz-kanta
Alaitu hadi !