---
id: tx-460
izenburua: Esku Hutsik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MmI0b9XuScE
---

Hemen esku-hutsik nator
Hitz egokiak erabiltzeak
besterik ez zait geratzen
Besterik ez

Zer sentitzen da
ereindako guztia uztea eta horrekin sua egiten dutenean
Zer sentitzen da?

Eta ekoizten jarraitzen dut
Badirudi egiten dudan ahalegin handiagatik gehiago nagoela salduta baino

Zer da egia beraientzat onena guztien onerako dela esatea
Zer da egia?

Zer sentitzen da
ereindako guztia uztea eta horrekin sua egiten dutenean
Zer sentitzen da?

Zer da egia beraientzat onena guztien onerako dela esatea
Zer da egia?