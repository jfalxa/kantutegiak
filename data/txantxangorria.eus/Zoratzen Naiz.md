---
id: tx-884
izenburua: Zoratzen Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/17RtCoXpDOw
---

Antton Aranburu - Zoratzen Naiz

Abesti hau entzun eta debalde deskarga dezakezu Antton Aranbururen webgunean.

anttonaranburu.eus

Zoratzen Naiz abestiaren istorioa:

Zerk zoratzen zaitu? Zerk puskatzen du zure barne oreka? Bere lehen saioan psikoanalistak galderok jaurti zizkion, ingurumaririk gabe, saioa hasi eta batera. Galderak beharrean zikutaz pozoitutako dardoak ziruditen. Noski, berehala ez zuen erantzuten jakin, eta bere onetik ateratzen zuten egoera eta gauzen zerrenda idatziz prestatuko ziola esan zion. Oso gustuko zuen edozelako zerrendak egitea, ia obsesiboa izatera heldu arte. Bere zerrendetatik elementuak ezabatzeak unetxo batez estasi antzeko bat sortarazten zion. Orotariko zerrendak zeuzkan: asmoak, irakurtzeke zituen liburuak, irakurritakoak, filmak, egin nahi zituen bidaiak, bete gabeko ametsak, mutil-lagun ohiak, … Baina sekula ez zitzaion otu sena galarazten ziona zerrendatzea. Idazten hasi zen bere zerrendak erregistratzen zituen uztai-koadernoan: zoratzen nauten gauzen zerrenda pertsonala: elkarrekin egoteak zoratzen nau, baina baita elkarrekin ez egoteak ere, zu barik ezinegona eta zurekin desoreka; alegia, ez zurekin ez zu gabe. Zerbait aurkitzen ez dudanean zoratzen naiz, baina bete eta asetzen nauen zerbaiten bila beti ibiltzeak zoratuta eta neka-neka eginda uzten nau. Kaos ordenatua ala kosmosaren lasaitasuna? Idatzita ikusi zuenean bere baitan gordeta zeuzkan kontraesan ugariez konturatu zen. Dena oreka kontua zela pentsatu zuen, bi poloen arteko magnetismo orekatua. Terapia, terapeuta eta bizitza osoan zehar jarraika zeukan dikotomia bizefaloa madarikatu zituen, eta ondoren beste saio baterako hitzordua eskatzeko deitu zuen. 

Zoratzen nau goizetan duzun umoreak
Zoratzen nau zeure ihesalditako logelak
Ta zoratzen naiz kasurik egiten ez didazunean

Zoratzen nau neure nerbixuen desorekak
Baita obsesioen errepikapenak
Ta zoratzen naiz bilatzen dudana aurkitzen ez dudanean

Zoratzen naiz oheko izarek zeure lurrina ez dutenean
Loak hartzen ez nauenean nirekin ez zaudenean
Zoratzen naiz, zoratzen naiz

Zoratzen naiz, barealdien ostean zoratzen naiz
Zoratzen naiz, zoratzen naiz

Zoratzen nau hegazkinak aireratu ta lur hartzeak
Gaueko zorabioak amesgaiztoetan
Zure hutsak dakarren erbestea

Zoratzen nau oheko izarek zeure lurrina ez dutenean
Loak hartzen ez nauenean, nirekin ez zaudenean
Zoratzen naiz, zoratzen naiz

Zoratzen nau etxeko atea zuk noiz zabalduko ote zain
Eta azaltzen zarenian berriz ere pozarren kantetan
Zoratzen naiz, zoratzen naiz

Zoratzen naiz, barealdien ostean zoratzen naiz
Zoratzen naiz, zoratzen naiz

Musikariak:
Asier Yarza: baxua
Iñaki Fdez. de la Peña: bateria eta koruak
Mario  Gomez: gitarra elektrikoa
David Juarez: teklatua
Antton Aranburu: Ahotsa eta gitarra akustikoa

Aktoreak: 
Leizuri Mardones
Mikel Catediano

Bideo egilea:
Julian Cohle