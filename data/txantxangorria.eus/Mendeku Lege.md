---
id: tx-221
izenburua: Mendeku Lege
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JaZM-PHuesA
---

Gaurko egunez oraino mendekua lege 
dutela ikustea ez bide da neke
presoak hurbil ordez urruntzen dituzte
Madriletik bakea horrela dagite
Madriletik bakea horrela dagite.

Mila kilometroz goitik bide egiteko
Izaten dute asko presoen ikusteko
Berrogei bat minutu solas egiteko
Berina bat tartean ez besarkatzeko
Berina bat tartean ez besarkatzeko

Iduri du bai ez dela nehoiz bukatuko
Zer egin ba legea bete arazteko
Estatu arrazoia legeaz gorago
Ikusten dugu dela aldi bat gehiago
Estatu arrazoia legeaz gorago
Ikusten dugu dela aldi bat gehiago