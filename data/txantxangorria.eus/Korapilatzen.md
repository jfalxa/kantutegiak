---
id: tx-2747
izenburua: Korapilatzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Db3cxgHEokk
---

Begitxo horiekin begiratzen zenidan
Esanez bezala, ‘ez zaitez joan’
Eta ni zure gogoz berriz
Buelta ematerakoan
sentituz momentu oro nola
Zure usaina nigandik badoan
Eskuak poltsikoetan eta
Eta zure zaporea ahoan

Korapilatzen jarraitzen badugu
Ez dugu jakingo nola askatu!