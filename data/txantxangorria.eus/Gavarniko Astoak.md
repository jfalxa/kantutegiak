---
id: tx-2349
izenburua: Gavarniko Astoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dn-k-4Uc0eU
---

Iragan denboretan gazte nintzanian 
asto hanitz gunian Xibero aldian 
udan egoiten ziren han bortu gainian 
hilabete goxuak ardien artian. 
Merkatu egunetan argi azkorrian 
Idauzetik zoatzan biesak bizkarrian 
artzaina aitzinian makila eskian 
xakurra gibeletik ebiltez gustian. 
Arresti apalian Mauletik jitian 
lusaz haiduru zauden plaza bazterrian 
haritz bati estekirik algarren kantian 
artzainak aldiz kantuz ostatu bamian. 
0 lhalat utzultzeko gaian berantian 
asto gaixuen gainen etzanik usian 
Ahuzkiko hur hunak bazian ordian 
arrakasta haundia biharamenian. 
Ez da haboro astorik Xibero guzian 
nur duk haien beharra auto badenian 
gaixuak ulhun batez gaierdi altian 
hurrun partitu ziren nigarra begian. 
Jentiak egon dira oi pentsamentian 
nurat ote jun ziren astuk ulhunian 
geroztik artzain batek hiltzeko puntian 
erran zeitan han dütük Gavarnie herrian.