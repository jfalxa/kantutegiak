---
id: tx-1970
izenburua: Orgulloso Nago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kOiVLKim16Y
---

Euskalduna naiz eta
orgulloso nago
Euskalduna naiz eta
orgulloso nago
Euskal Herria maite
dudalako klaro
Ai, oi, ai
dudalako klaro
nahiz eta en erderaz
askozaz gehiago.

Pankartaren atzean
noa deiadarrez
Pankartaren atzean
noa deiadarrez
ez dugu onartuko
horrelakorik ez
ez, ez, ez
horrelakorik ez.
Gero lagun artean
no, no, no, dame tres.

Ez diot huts egingo
ezelako jairi
ez diot huts egingo
ezelako jairi
ni euskararen alde
banabil ibili
JI, JI, JI
banabil, ibili
mozkorra ke pillamos
en el ibilaldi!

Nik euskaraz badakit
ez dakit dakizun
Nik euskaraz badakit
ez dakit dakizun
ohituragatik soilik
erderaren lagun
baina ni orgulloso
porke soy euskaldun!