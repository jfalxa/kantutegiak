---
id: tx-2144
izenburua: Badira Hiru Aste
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qjaJdF6Xxp0
---

iskin hontatik idazten dizut
azken aldian bezala
egia da bai sasoi honetan
hotza egiten duela
neska gazte bat ezagutu dut
hiru aste badirela
esango nuke bere begiek
erraz sorgindu nautela

azken bidai hau ez duzu izan
etxe ondotik ihesa
nigandik hurrun aurkitu arren
hara itzultzeko ametsa
zuekin beti oroitzen banaiz
ahaztu naizenik ez pentsa
sentimenduak esaten baina
ez da hain gauza erreza

orain goizean pozik hartzen dut
mendebalderako trena
harrizko zoru ahaztuetan
biderik eta zaharrena
bizitzak sarri erakusten du
badena eta ez dena
inoiz latz hartu banindu ere
gaur gozo hartu nauena

lantzean baina, egun luzeetan
bihotza egoten da triste
agindutako promesa hura
ezin kunplidu nezake
"loriak udan" kantatzen baitut
inoiz gutxitan ez uste
letrak ez ditut oraindik ahaztu
akordatzen naiz hainbeste