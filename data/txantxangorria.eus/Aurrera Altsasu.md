---
id: tx-2600
izenburua: Aurrera Altsasu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CDE-UZ5wQV8
---

TXIMELETA taldea herriarekin batera abesten! AURRERA ALTSASU!!
Hitzak/Letra: Jon Maia
Musika/Musica: Pello Reparaz
Ahotsak/Voz: Eñaut Elorrieta, Miren Amuriza, Evaristo Paramos, Aiora Renteria, Enrique Villarreal (El Drogas), Julio Soto, JuanRa, Fernando Sapo
Kitarrak/Guitarras: Fran Urias, Imanol Goikoetxea
Grabaketa/Grabación: Haritz Harreguy, Fran Perez, Imanol Goikoetxea
Nahasketa/Mezcla: Paxkal Etxepare
Mastering: Simon Caponi
Bideoklipa/Videoclip: TAOM

LETRA: "AURRERA ALTSASU"

Ez bagenu izanen
dugu bizi griña
ez bagenu arnasik
jaio ez bagina

Ez bageunde kalean
azaldu ez balira
ez balitz borobila
munduko lur bira

Utziko bagenio
bat izateari
ez geundeke sekula
batera kantari

Neguak ernaldu du
haritzen herria
noiz loratu da horren
oparo urria

TXARRENARI ONENA
ATERA DIOZU
ZAURIETAN MUSU
EZEREZEAN ZU

MINARI AURPEGIA
GEZURRARI EGIA
AURRERA ALTSASU!

Bazkaleku berriak
behar du izuak
udaberriak erre
nahi dituen suak

Noiz arte luzatuko
litzateke gaba
goizero ez bagenu
zure besarkada

Utziko bagenio
bat izateari
ez geundeke sekula
batera kantari

Neguak ernaldu du
haritzen herria
noiz loratu da horren
oparo urria.

TXARRENARI ONENA
ATERA DIOZU
ZAURIETAN MUSU
EZEREZEAN ZU

MINARI AURPEGIA
GEZURRARI EGIA
AURRERA ALTSASU!