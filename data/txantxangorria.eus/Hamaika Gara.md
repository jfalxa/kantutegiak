---
id: tx-2356
izenburua: Hamaika Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nf-tnvEBhvE
---

Bideoa/Video: Sergio Elia, www.wefeelm.com
Kontaktua/Contacto: 610887684 Nikolas
www.facebook.com/skabidean
@SkabideanTaldea

-----------------------------------------------------------------------------------------------------------------------------------

LETRA (EUS)

Egunez egun, zentzurik gabe,
Laino ilun artean galduta,
Endekaturiko giro baten,
Belaunaldi oinordekoa…

Ibiltzeaz aspertu gara,
Hegan egiteko garaia da!

Ortzian barrena ohartuta,
Anitzak garela hegaztiak,
Gure batasunaren handitasunari
Eusteko moduko kaiolarik ez da!

Hemendik aitzina hamaika gara,
Saldo bakarra, geldiezinak!!

Heldu da Euskal Herrira
Musika beltzaren loraldia
Kaleak astintzeko odol berriaz
Jamaikar doinuen aldarria!

Gure kabia eratu nahian
Egun oro egiten dugu lan,
Maite dugun guztia bilduz
Zutoihal bakar baten azpian.

Berdintasunez elkarlanean,
Denok batera autogestiorantz!

Errautsetatik,
Berragertuak,
Harrotasunez,
Garrasika!!
Iruñerrian,
Sortuak dira…
SKABIDEAN TA TXORI BELTZAK!

LETRA (ES)
Día tras día, sin sentido,
perdidos/as en la oscura niebla,
generación heredera
de un ambiente degenerado...

Nos hemos aburrido de andar,
¡ha llegado la hora de echar a volar!

Una vez en el cielo nos damos cuenta
de que somos muchos lo pájaros,
¡no hay jaula capaz de contener
la grandeza de nuestra unidad!

¡A partir de ahora somos muchos/somos once (juego de palabras en euskera),
una sola bandada, imparables!

Ha llegado a Euskal Herria,
el florecer de la música negra,
¡para agitar las calles con nueva sangre
el pregón de las melodías jamaicanas!

Trabajamos todos los días
para contruir nuestro nido,
agrupando todo lo que amamos
bajo un mismo estandarte...

¡Trabajando en equipo e igualdad,
todos/as juntos/as hacia la autogestión!

Resurgidos/as
de las cenizas,
¡gritando
con orgullo!
Han nacido
en Iruñerria...
SKABIDEAN TA TXORI BELTZAK