---
id: tx-2119
izenburua: Argi Ibili
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S3xk4jdQ2u8
---

Sarritan gezurretan
ez dute ezer esaten benetan
Komunikabideak

Gehienek sinesten dute
eta onartzen dituzte
Komunikabideak

Argi ibili
eta burua erabili
Argi ibili
Galdetu Husseini