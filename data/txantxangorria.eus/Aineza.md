---
id: tx-1563
izenburua: Aineza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Efu9ZzqztqM
---

Goizak alua ematen du
esan zendun astiro
berandu dator bizitza
igande goizetan
hegoak jantzi behar ditut
izarak eta orbela esnatzeko
nagitu ez gaitezan.

Irudimena nahi dut
armosutan esnetzeko zuku bat
loaren ordez
loaren ordez

Gauak poza ematen du
esan zendun astiro
igurtziak belarrian
azal hozkirria
mundua ohetik entzun
belaunetatik hasita buruz behera
lotsarazi odola.

Irudimena nahi dut
larrutan izerditzeko muxu bat
loaren ordez loaren ordez
aineza loaren ordez
aineza