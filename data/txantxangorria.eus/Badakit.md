---
id: tx-1965
izenburua: Badakit
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hRsgm1gR_XQ
---

triste zaudenean goibelduta
begiak itxi eta amets egiten duzu
kolore artean esnatuaz
ohe hutsaren berotasun faltsuan
kalean barrena ormetako
pintada gorriz laztanduaz eskua
jada ulertu dut behin eta berriz
garbitu arren zikinik dagoela
eta nire artean pentsatzen dut
entzuten diren gezur denen egiaz
dena da polita desberdina
begi aurrean ikusten ez duguna
badakit urruti zu sentitu arren
sarri oroitzen zarela
inoiz ez duzula aukerarik izan
bake eta guda hautatzeko
berandu da horretarako
behar zaitut laztana
bihotzaren taupada.