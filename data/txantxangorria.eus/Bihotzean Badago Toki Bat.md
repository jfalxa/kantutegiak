---
id: tx-2448
izenburua: Bihotzean Badago Toki Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qQ76xiFRCnw
---

Falconek, Josu Eizagirrek eta hirurok bideo hau prestatu dugu Altsasuko gazteen inguruan izan diren gertakariak direla eta. Justizia eta epai proporzionatu bat eskatzen duten adierazpenekin bat egiten dugu.

Bide batez, babesa eta elkartsuna ere eskaini nahi dizkizuegu epaitutako gazteoi, bai eta zuen senitarteko, lagun eta ingurukoei.


Bihotzean badago toki bat

odola hotz dabilena

behin bertan

egon eta gero

gauzak erabat aldatzen dira

 

Lerro zuzen eta okerra

han erraz nahasten dira

batzuetan

ez dago garbi

bien arteko aldea zein den

 

Lur hotz eta anker hori

zugandik ihesik ba ote da?

 

Iraganaren zuhaitz ustela

orainaren lore berriak

lehian

elkarrekin dabiltza

baina jokoa garbia al da?

 

Lur hotz eta anker hori

zugandik ihesik ba ote da?