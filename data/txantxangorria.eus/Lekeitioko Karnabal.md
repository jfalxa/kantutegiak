---
id: tx-1415
izenburua: Lekeitioko Karnabal
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-3Jl2JK3jKo
---

Karnabal, karnabal, karnabal,
Aratuste da pozez beterik
egoteko eguna
Karnabal, karnabal, karnabal,
zahar eta gazte
dantza leikez gaur.

Hau da daukagun umoria
hor gero jateko tostada erdia,
eguaztenean kenduko deutsagu
arenka zaharrari begia.

Hau da daukagun umoria
hor gero jateko tostada erdia,
eguaztenean kenduko deutsagu
arenka zaharrari begia.