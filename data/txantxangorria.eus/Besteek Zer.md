---
id: tx-589
izenburua: Besteek Zer
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XEuyQCXGtFU
---

Music by Merina Gris
Lyrics by Gartxot Unsain
Produced by Ed Is Dead & Merina Gris
Mix & Master by Ed Is Dead

(Airaka, 2020)

Eskerrik asko Alabama, Badator, Arriguri eta gainontzeko ekipazoari 💕
--------------------------------------------

EUS
Kamaleoia zara, azala etengabe neurtzen
azkar itoko dena besteen korrontea jarraitzen
Gaur ere ez zarenaren menpe, betegarri huts
arrunt izateari beldurra ote diozu?

Iritzien beldur, egunez ere gau
itsu da mundua itxura bada arau

Besteek zer esango zain zaude
ta ni zure zain nago
besteek zer esango zain zaude
Besteek zer esango zain zaude
ta ni zure zain nago
ikusteko ea benetako gezur bat zaren

Saiatzen zaren arren eskatzen dizutena izaten
follower gaindosiak ez dizu hutsunea asentzen
Gaur ere zure beldurren menpe, betegarri huts
arrunt izateari beldurra ote diozu?