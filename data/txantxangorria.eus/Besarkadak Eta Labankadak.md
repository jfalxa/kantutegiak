---
id: tx-2626
izenburua: Besarkadak Eta Labankadak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q9RRNrrapu4
---

Bi Bala taldearen Wasabi estudioko lehen laneko bideoklipa.
Egilea: Iker Gurrutxaga Zubimendi

Oharra: irudi gehienak web orrialde honetatik bertatik hartu dira.

1
Ez hormigoizko lur zatituak,
ez ikurrina kurtsiak,
ez ikasteko horren zaila den
ereserki itsusiak,
ez Historiak, ez hizkuntzak, ez
munduko urre guziak.
Euskal Herria batzen du, soilik,
elkar ezin ikusiak

2
Beaumondarrez oroitzen gara,
Bergarako besarkadaz,
Eibarko armek hildako "gorriz"
ta "beltzen" pelotakadaz,
Ahaztu gaitezen anai-arrebez
RH-z ta txorradaz
ta garbi hitz egin gure arteko
traizioz ta labankadaz.

3
San Inazio, Ybarnegarai
Patxi Lopez Lehendakari
Mayor Oreja, Igartiburu
Ezenarro, Alliot Marie,
Munilla, Rosa Diez, Azkuna
edo Alonsotar Xabi.
Kontuz! Inoiz ez bizkarrik eman
besarkatzaile onari.

4
Oinaztar edo Ganboatarrak
zuhaitz bereko famili,
Karlista batek liberalari
teilatupea kiskali
Arzalluz edo Garaikoetxea
mili edo poli-mili
zu gabe ezin inora joan,
zurekin ezin ibili.

5
Aljer, Xiberta, Ajuria Enea
su etenaren garaian
Lizarran, Oslon edo Loiolan
sukaldea standby-an
tripa-zorriak egindakoan
negoziazio mahaian,
euskaldun batek zer egiten du?
Lehen emandako hitza jan

6
Bi bandotako herria da hau:
euskara edo erdara, 
ezker-eskubi, Mirri edo Porrotx,
Diario edo Gara, 
Atxaga-Sarri, Lete-Laboa
ta hau luze joaten bada
zaborragatik elkar hiltzeko
kapazak izango gara

7
Bakoitzak bere memoria ta
gudariei maitasuna
bere batzoki, bere alarde,
bere euskalduntasuna,
bakoitzak bere greba, bakoitzak
bere Aberri Eguna,
orduan ez dut ulertzen zer den
herri egiten gaituna.