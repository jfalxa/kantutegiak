---
id: tx-3294
izenburua: Hau Pittu Hau 25 Urtez Euskarari Bedar Txarrak Kentzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CzGD8oVblCU
---

Indautxun, Santutxun euskararen oihartzun ozena nahi dugu entzun!
Atxurin zein Abandon berdin, euskarari gogor ekin!
Ez da Bilborik euskararik barik!
Hitzez zein gure ekintzez Bilbo euskaldundu baietz!
—
Gure lurra erein dugu hogeita bost urtez
—
Baso edar bat nahi dugu, bedar txarrik bapez!
—
Hau pittu hau, Hau pittu hau!
Berba za(ha)rrei eusten, bedar txarrak kentzen.
Hau pittu hau, Hau pittu hau
izan ginalako, izan gaitezan gaur!
—
Sasiz bete zaizkigu hiriko Alde Zaharra edo Kasilda parkea
Txurdinaga edo Otxarkoaga sastraka handiz betea
Erne egon, gure lorak jagon,
loraz lora euskarak gerora sendo iraun dezan Bilbon!
—
Bedar txarrak kendu doguz hogeita bost urtez
—
Sustrai barik ez gara ezer, konturatu zaitez!
—
Hau da giro itzela daukaguna konpartsan,
urte osoan zehar egoten gara martxan,
inauterietan zein jai guztietan dantzan,
Aste Nagusian zain gauzkazu Areatzan,
Oroitu bedar txarrak kentzea zertan datzan,
Lehendabiziko berba beti euskaraz esan,
Basurtun, Mazarredon zein Errekalde plazan,
euskararen basoak Bilbo edartu dezan!
—
Hau pittu hau, Hau pittu hau!
Berba za(ha)rrei eusten, bedar txarrak kentzen.
Hau pittu hau, Hau pittu hau
izan ginalako izan gaitezan gaur!