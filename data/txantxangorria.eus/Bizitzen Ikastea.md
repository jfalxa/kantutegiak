---
id: tx-751
izenburua: Bizitzen Ikastea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bYVhwsT7nqA
---

Adinean aurrera doan lagun handi bati    
bisita egin diot gaur goizean,    
kopeta zeharo ilun,  begiak triste zeuzkan.   
esperantzaren ezdeusean, 
 
Aurreneko betiko itaun leloa egin diot bertan
zer moduz zaude, zelan zeurean?
bere begirada leunean erantzun dit
ondo, baina zakua lepoan,    2 esp
Bizitzaren zentzuaz, bizitzak ekarri leikenagaz 
mintzatu gara luzaroan,
Jakintzaren Iturri
agortezina dugularik geure baitan.

Bizitzen ikastea, bidean ezagutza hartzea da
Bizitzen ikastea,  ezbeharrak gainditzea da
Bizitzen ikastea,  nork bere bidea aurkitzea da.
Bizitzen ikastea, sentituz sentiaraztea da. 

Lagunaren jakintasunean 
ta neure ez jakintasunean
entzutea otu zait ikasbidean
Dakizuna ez dakizuna baino gutxiago dela dakizunean
jakintsu zara ezjakintasunean.

Bizitzen ikastea, egunoro arriskatzea da
Bizitzen ikastea, erabakiak hartzea da
Bizitzen ikastea norberengan sinestea da
,,          zeure lekua bilatzea da
,,                umiltasunez jokatzea da
,,          zahaar on egitea da
           ,,               beldurrak uxatzea da