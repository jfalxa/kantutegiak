---
id: tx-370
izenburua: Non Nagoen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mbIUD-Dqd-o
---

Jim Morrisonek betetzen zuenean
bere "light my fire" espermaz
munduko gela
bide bat egin zenidan
gero hautsi zenuen eta orain
galdetzen diazu non nagoen...

gaur klarinete soinu batek
ixiltasuna hautsi nahi luke
eta baten batek egunero
parketik loreak mozten ditu
eta munduko gelatik barreiatzen

... denborak sendatzen du dena
... ahazten zaitut...