---
id: tx-1155
izenburua: Zure Bularretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-QTa42ZLrTk
---

Zure bularretan lurperatu nuen eguzkia,
zure begietan urratu nuen goiza.
Zure hortzetan hozkatu nuen haserrea,
zure hitzetan gorde nuen poema.
Zure soinean galdu nituen eskuak,
zure ezpainetan ahantzi nituen neureak