---
id: tx-1783
izenburua: Astero Ostera Hastera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6LC_JOjdjo4
---

Gaua utzi du ostiko batek eta loak iso eta gizonak sai 
zuhaitza errotik atera naute beldurrez zintzo ni bai tandez lai, 
goizeko lautan kale ertz baten nahi ez denean katu beltz baten, 
gauak bi begi bi auto argi hatz azpitik so antza nire sai.

Astero ostera hastera, astero ostera hastera. 

Gaua utzi du erloju batek Madrilez mintzo borok lo darrai
hiru eguneko arropak poltsan amorru zintzo herrirat ez lai 
goizeko lautan kale ertz baten nahi ez denean katu beltz baten
gauak bi begi bi auto argi berriz niri so, berriz nire sai.

Astero ostera hastera, astero ostera hastera, astero ostera hastera, astero ostera hastera.

Gaua uzten dute erlojuek, gaur gehiegitxo erne amets zait, lehen eramanak orain joateko logura zintzo, bidean deslai, saiak amets bat nola jan zigun astero gogorarazten digu. Gauak bi begi, biharko argi, beti guri so beti gure sai.

Astero ostera hastera, astero ostera hastera, astero ostera hastera, astero ostera hastera. 

Gaua utzi zuen ostiko batek, sagardari so, eta bertan darrai,luzaz errepikatzen den gau bat, beti amets gaizto, bera egin gai, 
luzaz mundura gauez irteten, arnasa altzo argi bi ..., nire bi begi, ilunez eri egunari so, argiaren sai.

Astero ostera hastera, astero ostera hastera, astero ostera hastera, astero ostera hastera, astero ostera hastera, astero ostera hastera,astero ostera hastera, astero ostera hastera.