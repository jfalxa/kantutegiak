---
id: tx-3352
izenburua: Bugi Bugi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y0fWPuy9SvA
---

O! Bugi, bugi, bugi, bai! Jei!
O! Bugi, bugi, bugi, bai! Jei!
O! Bugi, bugi, bugi, bai! 
Paran, pan, pan, pan, pan.
Hau da nire behatza
hau da beste behatza
hau da nire eskua
hau da beste eskua.

Hau da nire begia
hau da beste begia
hau da sudur punta
hau aho-zuloa.

Hau da belarri bat
eta hau da bestea
Burua beheraka
burua goraka.

Hau da nire ukondoa
hau beste ukondoa
hau da nire sorbalda
hau beste sorbalda.

Hau da nire hanka
hau da beste hanka.
Hau da ipurdia
berriz ipurdia.