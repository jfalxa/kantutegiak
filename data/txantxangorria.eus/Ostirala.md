---
id: tx-1198
izenburua: Ostirala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iVoz6fzcno8
---

PIZTU ZURE BARRUKO PAILAZOA ! OSTIRALA DA ETA !!!

Bueno bueno bueno... BIYAR OSTIRALA !!! eta bi astetako OPORRAK !!! iuju !
Ostiralak [oraindik eta gehiago] alaitu asmoz, Zarauzko Salbatore Mitxelena ikastolako "Txapas" irakasleak sorturiko abestia jarri dizuet behean. Beheko linketik jetsi dezakezue. Xirula Mirulak [Imanol Urbieta] grabatua duten arren, Takolo Pirritx eta Porrotxen "Patata,patata" diskoko bertsioa ipintzen dizuet, alaiago delako eta bestetik Txapas berak ere abesten duelako.
Eman bolumena honi !

Ostirala, iritsi da,
iritsi da, ostirala
pozez betetako eguna
zatoz gurekin laguna

Mantalak etxera eta
bi egunetako oporrak [oporrak bi egunetan]
astelehenean berriro_ere
bilduko gara ikastolan [bilduko gara gure gelan]

Laralala...


Ostirala, iritsi da,
iritsi da, ostirala
pozez betetako eguna
zatoz gurekin laguna

Mantalak etxera eta
bi egunetako oporrak [oporrak bi egunetan]
astelehenean berriro_ere bilduko gara gure eskolan [bilduko gara gure gelan]