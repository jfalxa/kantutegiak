---
id: tx-2408
izenburua: Askatasunaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t6mNTVvZ8p8
---

Zer buruzbide biyotz gabe,
zer anaitasun zitala,
zer gizontasun neurri gabea,
zer zentzu gabeko ergela;
zer gertatzen da Euskal Erriyan,
zerk garabiltza onela,
zer itxuretan anai jatorrak,
zer pakez ezin geudela,
zer esan nai du jarraikera onek,
zer, zentzu gabeak gerala.

Nun gertatu da onelakorik,
nun jaunak, nun, Azpeitiyan,
jesuita ta erlijiyua
besterik ez dan tokiyan;
Askatasunaz gaudezelako
lau lagun aruntz joan giñan,
gure barrengo fede leyala
aitortutzera itz biyan,
ekin ziguten laueri millak
guztiz modu itsusiyan.

Gugandu ziran atzaparrakin
lepotik ito nayian,
udazkenean nola enarak
alanbrearen gañian;
itz bat egiten utzi nai ezik
modurik okerrenian,
odolean eziñ irukirikan
miñ zutela barrenian,
beldur ziraden biurtutzeko
norbait gure esanian.

Garate jauna Bergarakua
euskaldun jatorra berez,
euskalduna bai ezkerrekoa
apaizak ekusi nai ez;
eldu ziyoten zamarretikan
lau puska egin bearrez,
Santakruz zanen odol beretik
nazi dira nere ustez,
Aita Inaziyo sortu Erriyan
biyotz onekuak daudez.

Gomendio zan onen laguna
jaun on bat Oñatearra,
otzikan gabe laja diyote
gizajoari bizkarra;
tira ta bultza autsi ziyoten
soñian zeukan zamarra,
ekustearren zeñen pizkorra
berez dan Azpeitiarra,
Askatasuna indartutzeko
ez dago kuadrilla txarra.

Milla baziran lau zatitzeko,
ez al da lotsa Euskalduna,
atze-erriyetan ai zer fama guk
iruki bear deguna;
au al da lege erlijiyuak
erakutsitzen dizuna,
modu orretan baldin badabill
maltzur zar biyotz astuna,
bide ortatik ez da etorko
Euzkadin Askatasuna.

 
Askatasunaz
(Irun Republicano, 1931-VII-4)

Herriz herri ibili zen Lopetegi 1931ko ekaineko hauteskunde orokorretan errepublikaren aldeko mitinetan Gipuzkoan eta Nafarroan, eta Azpeitian suertatu zen haietako batean. Herri karlista porrokatua zen orduko Azpeitia, apaizek esku handia zuten herria. Mitinlariei eginiko harreraren ondoko haserreari ezin eutsirik argitaratu zituen bertso hauek Joxe Lopetegik 1936ko uztailaren 4an. Hauteskundeen ondoan azaldu ziren, beraz, bertso hauek. Justo Garate mediku eta idazle bergararrak ere parte hartu zuen mitin-saio hartan, ANV-EAE alderdi abertzalearen ordezkari gisa. Argigarria da Lopetegik egingo zion aurkezpena: Garate jauna Bergarakua / euskaldun jatorra berez / euskalduna bai ezkerrekoa / apaizak ekusi nai ez. Bere burua ere ikusten zuen nonbait definizio hartan. Jose Migel Gomendio Larrañaga zeritzon, bestalde, Gomendio hari; oñatiarra zen eta gestore jardun zuen Gipuzkoako Diputazioan, Errepublikaren garaian. Euskadiko Herri Auzitegiko fiskal izendatu zuen Euzko Jaurlaritzak 1936an.

Ohiko mamu politikoei -Santa Kruz apaiza, errege-erreginak, monarkiazaleak, oligarkia...- eskaini zizkien loreen artean, epiteto berri bat azaldu zuen Joxe Mari Lopetegik lehen aldikoz bertso hauetan, bere zentzu politiko betean: «nazi». Aurreneko aldia da, segurki, bertsolarien historian. Gogoan hartzeko bertsoak dira hauek, inolaz ere.