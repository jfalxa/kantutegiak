---
id: tx-2878
izenburua: Elkarrekin Beti Harrapazank Xabi Hoo  Enkore, Aiora Renteria  Zea
  Mays, Oihana Bartra  Bertsolaria E
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Pfx64FXYCHg
---

Elkarrekin egin dugu bizitzako bidea
Zugandik jaso dut nik gaur egun naizena
Bion artean idatziko dugu istorio bat
Hiri euskaldun honen etorkizuna.

Kaleetan barrena entzun ditut milaka berba
Hizkuntza zaharrak berri egin dituenak
Lehoien orroak baino ozenago
Egingo dugu euskaraz Bilbotarrok.

Begira nazazu esan maite nauzula
Ez naizela zu barik
Ametsetan eta sentitzeko
Behar zaitut nirekin, Euskaraz beti.

Bizirik gaude eta zu zara horren seinale
Bizirik zaude berbetan gu hemen bagaude
Geroan oraina marraztuko dugu bizirik
Zu zara eta gu gara elkarrekin.

Begira nazazu esan maite nauzula
Ez naizela zu barik
Ametsetan eta sentitzeko
Behar zaitut nirekin, Euskaraz beti.

Isilean amesten dut bide malkartsu honetan
Ene alboko itzala dut euskara.
Kalean zaratek dute isiltzeko premia,
Zure ezpainetan hitzen bat irtetzear dagoenean.
Ez dugu beldurrik, ez, Bilbon galdurik,
Euriak duen xarma berezi horren morroi.
Ez gara ezer, elkarrekin ez bagaude
Hizkuntz honek baititu gure bihotzak bere alde.

Begira nazazu esan maite nauzula
Ez naizela zu barik
Ametsetan eta sentitzeko
Behar zaitut nirekin, Euskaraz beti.
Begira nazazu esan maite nauzula
Ez naizela zu barik
Ametsetan eta sentitzeko
Behar zaitut nirekin, Euskaraz beti.


Bilboko Udalak eta Euskaltzaleen Topaguneak elkarlanean egindako Harrapazank egitasmoak 10 urte betetzen ditu aurten, 2015ean. Hori dela eta Harrapazankeko abestia eta bideoklipa sortu ditugu. Abeslaria Jonatan Yélamos da, iazko Harrapazankeko partaideetako bat, eta berarekin batera kolaboratzaile lanetan hurrengo aritu dira: Xabi Hoo ( Enkore), Aiora Renteria ( Zea Mays), Oihana Bartra ( bertsolaria) eta Iker Villa ( Revolta Permanent).