---
id: tx-17
izenburua: Adios Izar Ederra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AVoC3wkkbSI
---

ADIOS, IZAR EDERRA
anonimoa , XVIII-XIX. mendeak
 
Adios, izar ederra, adios izarra!

Zu zare aingerua munduan bakarra!

Aingeruekin

Aingeruekin zaitut konparatzen,

Zenbat maite zaitudan ez duzu pensatzen!

 

Adios, izar ederra, eta karioa,

Neure begietako lili arraroa!

Bihotzez zurekin eta gorputzez banua,

Jarrikiren zautazut zur' amodioa.

 

Izan naiz Aragoan eta Kastilloan,

Hitz batez erraiteko España guzian;

Ez dut ikusi

Ez dut ikusi zu bezalakorik,

Nafarroa guzian zaude famaturik.

 

Jarrikitzen ninduzun izar eder hari,

Nola marinel ona bere orratzari.

Jende onak atentzione ene arrazoin honi,

Ez zieztela fida amodioari!

 

Amodioa duzu arrosaren pare,

Usaina badu eta ondoan arantze;

Maitea, ni ez nainte egon zugana jin gabe,

Hil behar banu ere hirur egun gabe!