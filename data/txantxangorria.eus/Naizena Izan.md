---
id: tx-1964
izenburua: Naizena Izan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/G5BaMTz-CAo
---

Bukatuta, 
berriz nola hasten den
nahi nuke jakin,
izan nahi dudana eraiki 
eta nintzen desegin.
Nekatu nintzen
ezarritako bide zuzen
errazegietan ibiltzez,
guztiak bere horretan
berdin iraun dezan
eraiktako horma antzuez.
Ez naiz jaio itxaroteko,
 egunak seinalatzeko,
eta soilik orduan,
eta soilik orduan,
zoriontsu izateko.
Naizena izan nahi dut
besterik gabe,
nahi dudana esan,
eta guztia eman, 
guztiz hustu arte.