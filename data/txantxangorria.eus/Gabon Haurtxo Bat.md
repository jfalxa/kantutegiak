---
id: tx-3004
izenburua: Gabon Haurtxo Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KzDCnFl0dYs
---

Gabon, haurtxo bat jaio da,
bere etxetik kanpora,re
ama pobre batengandik
ai, ai, ai, ai, ai eta ai
eta aitak ez du lanik.

Aitak ez du lanik eta
erbestean aurkitzen da
latza da hala beharra
ai, ai, ai, ai, ai eta ai
eta haurraren negarra.

Inork ez du ulertu,
premiak ditu behartu,
nonbaitera behar eta
ai, ai, ai, ai, ai eta ai
eta hain daude bakarrik.

Haibat gauzaren beharrez,
ezbeharraren gogorrez
pobreak hala beharrik
ai, ai, ai, ai, ai eta ai
eta hain daude bakarrik.

Historia berria da
noiznahi berritzen da-eta
pobreen anparurik eza
ai, ai, ai, ai, ai eta ai
eta bajuen tristeza.

Eta bajuen tristeza,
bakardade ta bajeza
zenbatek duen sufritzen
ai, ai, ai, ai, ai eta ai
eta gaua ez da argitzen