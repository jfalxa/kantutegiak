---
id: tx-2892
izenburua: Sendagile Laguna Takolo, Pirritx Eta Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dngBmDjB-V4
---

Ni sendagile laguna nauzu
diploma ugari, trebe, jakintsu
Sendatzeko onena emango dizut
mina non duzun esaidazu

Azkura sortu zait buru gainean
atzo zorria izan nuen bisitan

Hori sendatzeko nire xarabea
pupua arintzeko xuabe xuabea

Ni sendagila laguna nauzu...

Sabela gaur daukat bete-beteta
atzo jan nituen hamar piruleta

Hori sendatzeko nire xarabea...

Ni sendagile laguna nauzu...

Belarrietan dun dun dun zarata
atzo musika ozen entzun nuen eta

Hori sendatzeko nire xarabea...

Ni sendagile laguna nauzu...

Errenka ezin da sartu baleta
atzo zapaldu bainuen txintxeta

Hori sendatzeko nire xarabea
pupua arintzeko xuabe xuabea
Xuabe xuabea