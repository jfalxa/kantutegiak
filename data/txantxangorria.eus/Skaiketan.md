---
id: tx-2749
izenburua: Skaiketan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H7qVzs_uzPE
---

Jaiki, begiak ireki, ez da salbuespena, biharra tiraka, nahi ez zenuen eskema. 
Preso prebentiboen inpotentziaz itxoiten, ospatzeko zer edo zer, aitzakiak asmatzen.
Heldu duzu almohada, aski dela zin egiten, zinez amaren promesak ez zirela bete. 
Fantasiazko istorioen amaiera aurreikusi duzu firmatzen lan kontratua
Energiaren haize orratzak eman ditzazke bueltak, hotza kendu dizun suak erre zaitzakeela. 
bizi egarriz, non da ura? putzurik ez da agertzen zoritxarrez bizitza, doa geltoki forma hartzen.
Frustrazioen pasiloan gaude, koreografia zaharren menpe etengabe 
badoaz beste behin burnizko milaka begi herdoiltzen 
zin egidazu amaitu dela, baina bitartean prest da labekada 
berri bat, begi bat zapalketen malkoak pozoi bihurtzen
Zein da festa honetan ausaz bildu den fauna? 
penak, arazo ta neurak euforia bihurtzea 
sexu langile horren zauriek eta mutiko baten ubeldurek 
determinatu dituen sistemak itota
Ez dakigu nola elkartu isolatuta 
dauden arima jipoituak galduta 
nahiago zuten rave batean hegan egin 
konformaturik zerua behin ukitzearekin
Energiaren haize orratzak eman ditzazke bueltak, hotza kendu dizun suak erre zaitzakeela. 
bizi egarriz, non da ura? putzurik ez da agertzen zoritxarrez bizitza, doa geltoki forma hartzen.
Frustrazioen pasiloan gaude, koreografia zaharren menpe etengabe 
badoaz beste behin burnizko milaka begi herdoiltzen 
zin egidazu amaitu dela, baina bitartean prest da labekada 
berri bat, begi bat zapalketen malkoak pozoi bihurtzen