---
id: tx-1591
izenburua: Lili Bat Ikusi Dut Aritzes Lesbordes
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O1TgXIktvBk
---

Lili bat ikusi dut baratze batian,
Desiratzen bainuke ene sahetsian;
Lorea ez du galtzen, udan ez negian
Haren parerik ez da beste bat mundian.
Lorea ez du galtzen, udan ez negian
Haren parerik ez da beste bat mundian!

Deliberatu nian, gai batez juraitia
Lili arraro haren, eskila hartzia;
Ez beinian pentsatzen begiratzen zirela
Gai hartan uste nian han galtzen nintzala.
Ez beinian pentsatzen begiratzen zirela
Gai hartan uste nian han galtzen nintzala!

Etsenplu bat nahi dut eman mundiari,
Eta partikularzki jente gaztiari
Gaiaz ebilten dena ez da zuhur ari;
Ni begiraturen niz, eskerrak Jaunari.
Gaiaz ebilten dena ez da zuhur ari;
Ni begiraturen niz, eskerrak Jaunari!