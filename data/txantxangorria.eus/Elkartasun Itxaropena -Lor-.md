---
id: tx-3102
izenburua: Elkartasun Itxaropena -Lor-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2Y47NkJSwtw
---

Baliteke beldurtzea
begiak ireki eta
etxetik gertu ez zaudela ohartzean
etorkizuna ehortzen gizun ziega beltzean
beltzean

Baliteke abailtzea
arnastu eta zure
gorputz osoak dar dar egitean
berriz ere jasotzean erantzun ezin duzun
galdera


Baliteke samintzea
besarkatu nahi eta
kristal hotza hautsi ezinean
hain hurbil sentituagatik
hain urrunean
urrunean

Baliteke amorratzea
zeurea ez den itzala
zugana hurbiltzen sumatzean
sugeak txoria begiratzen
duen bezala
duen bezala

Zure uneak dira
gure kemena
elkartasuna
zure iraupena

Zure uneak dira
gure kemena
elkartasuna
zure iraupena


Itxaropena izan
hor izango naiz jarrai
gure kemena baita
ondasun handiena
elkartasun guztiaz
oroimenean guztiak
lortuko dugu behingoz
askatasuna

Zure uneak dira
gure kemena
elkartasuna
zuen iraupena

Izango naiz...

Zure uneak dira
gure kemena
zuen iraupena

Uneak dira
gure kemena
elkartasuna
itxaropena
askatasuna