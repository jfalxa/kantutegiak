---
id: tx-1899
izenburua: Zeruko Hauspoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/K-Wj9CqJIf4
---

Botoiak azkartu ta 
burua uzkurtu...
Inpernuko Hauspoa
zer zaigu bihurtu?
Bihurtu burua uzkurtu...
Herriaren ardoa 
ederki da urtu!

Sega zorrotza zena 
apaingarri saldu: 
froga ugari dago
nahi bada azaldu.
Azaldu apaingarri saldu...
Salmentak hondamena
ekarri ez al du?

Ergelkeria hartzen 
dugula ahotan
auzo-lotsa ematen
gabiltza askotan.
Askotan dugula ahotan...
Paga merezi dugu
ez kantatzekotan!

Kamustu zaigulako
koplaren eztena
egiatzat hartu da
gezurra ez dena.
Ez dena koplaren eztena...
Trikitixak horrela
hurbil du azkena.

Aspaldi galdua da
Elgetaren kasta:
edertasuna baino
nahio arrakasta!
Arrastaka Elgetaren kasta...
Halako morrontzatik 
gaitezela aska!