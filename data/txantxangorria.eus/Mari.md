---
id: tx-1985
izenburua: Mari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zUDsyhNpfLw
---

Hire diztira dut maite
Anbotoko erregina
Mortal guztien gainetik
Hegan sua egina

Ta eutsi zan zeruan zehar
Eguzkia joan eta gero.

Libre
Izateko haiz jaioa
Mari
Ez zara izango niretzat

Nire ametsetan giñen
Biok tximisten lagun
Baina argi da hori zela
Soilik umeen ipuin

Ta eutsi zan zeruan zehar
Eguzkia joan ta gero

Lehiotik dut ikusi ekaitza
Ta pentsa bapatean
Hor sugar damatxoarekin
Gaur berriro larrutan

Libre
Izateko haiz jaioa
Mari
Ez zara izango niretzat