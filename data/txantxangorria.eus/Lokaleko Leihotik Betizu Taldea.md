---
id: tx-3072
izenburua: Lokaleko Leihotik Betizu Taldea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U8y6-p9j7yI
---

Kantu berri bat sortzen zenbat ahalegin 
zenbat buruhauste ideien atzetik 
errimak lotu, zer esan nahi den jakin 
eta esan nahi den hori nola adierazi. 

Azkenaldian lehor nabil erabat 
esan ote ditut esatekoak? 
noizbait itzultzan bada inspirazioa 
gu entseatzen egongo gara! 

Kantu beri bat sortzen zenbat ahalegin 
zenbat buruhauste ideien atzetik 
ez da irteten lokal ilun hortatik 
gitartzar bati lotuta, ez du besterik 

Ohi dun moduera gaurkoan ere 
lokalean entseatzen zegoen 
leihopetik pasa naiz egunero lez 
baina ezin jakin ikusi nauen 

Lokaleko leihopetik ikusi zaitut berriz 
fabriketako sirenek ez dute jo oraindik 
hain polita gaur ere 
begietan bi lore 
nora zoazen, zoazen, zoazen, 
jakin nahi nuke. 

Neskatxa fin horrek itxututa nauka 
norabidea zeharo galduta 
modako arropako, eta 
barbie itxura 
burutik ezin dut kendu, zer gertatu da? 

Mutil gaizto irudiaren atzean 
zerbait badu erakartzen nauena 
larruzko jaka eta txima luzeak 
etxean amets gaiztoena. 

Lokaleko leihotik ikusi zaitut berriz.