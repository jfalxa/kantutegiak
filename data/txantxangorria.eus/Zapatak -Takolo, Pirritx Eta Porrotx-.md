---
id: tx-3231
izenburua: Zapatak -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5EYQJsACoEM
---

Zapa zapa zapa zapa zapa zapatak
zapa zapa zapa zapa zapa zapatak
Luzeak, zabalak, 
handiak, koloretakoak

Lokarria askatu askatu egin da
lotu behar, eroriko naiz
ai ai ai ai ai ai ai, balantzaka nabil bai
ai ai ai ai ai ai ai, korapilatu egin zait