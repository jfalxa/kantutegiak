---
id: tx-2265
izenburua: Zu Nor Zaitugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aO5RYmpYjw4
---

ZU, NOR ZAITUGU ?
Gontzal Mendibil 

Nora doa federik gabeko jendea, hiltzen denean?
Nora doa lurrik eta aberririk gabekoa?
Nora doaz justiziaren alde egin ta ezer gutxi lortu dutenak?                                                
Nora doa lanari etekinik atera ez diona?

Nora doa esperantzaz bizi eta desesperantzan hil dena?
Nora doa beldurraren beldurrez ausartzen ez dena?
Nora doa inoiz inongo ametsik izan ez dutena?
Izango ote du saririk lehian tinko ari denak?

Nora doaz beren egiteak ezerezean geratuko direnak?
Nora doa itsaso guztiak igarota bazterrean ito dena?
Nora doa adorez ta indarrez aurrera doana?
Nora doa, hango eta hemengo gizaki errebeldea?

Bada jendea munduan bizipoza ematen digunik
Bada jendea gure bizitza lazten duenik
Bada inoren mesedetan ezer egingo ez duenik
Badira lagun-arteko eta ezku-zabaleko direnik
Bada dauden lekuetatik ihes egin nahi luketeenak
Badira dauden horretatik ihes egin ezin dutenak
Badira dauden lekuetatik ihes egin nahi ez dutenak
Bada jendea isolamenduan zoriona aurkitzen duena
Badira geroaren aintzindari bihurtzen direnak
Badira beren izena estalki onez babesten dutenak
Badira denen argitan bizi direnak
Badira itzalean bizi behar dutenak
Badira lehorpean ondo izkutatzen direnak
Badira ahalgez eta lotsaz bizi direnak.

Badira ausartak, badira koldarrak
Zentzundunak eta zentzugabekoak dira
Badira zoroak, badira zuhurrak
Badira erosoak eta borrokalariak
Zabal begiratzen dutenak eta ikuspegi estuegikoak
Era guztietakoak dira munduan.

Eta zu, nor zaitugu? Eta zu, nor zaitugu?
Eta zu, Nor zaitugu? Eta zu, nor zaitugu?
Badira ausartak, badira koldarrak
Badira lehialak, badira traidoreak
Badira zintzoak, badira zinikoak
Era guztietakoak dira munduan
Eta zu, nor zaitugu? Eta zu, nor zaitugu?
Eta zu, nor zaitugu? Eta zu, Nor zaitugu?