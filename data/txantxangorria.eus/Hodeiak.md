---
id: tx-1512
izenburua: Hodeiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0q-47CF2f1c
---

HODEIA
(Joseba Sarrionandia)

Musika: Xabier Zabala

Oskarbiaren huts urdinean
Hodeibakarti badoa
Airean zehar itxuraldatuz
zeinen hodei arraroa…

Hodeia edo zure gogoa
zein da arrarogoa?
Arraroa da hodeia, baina
gehiago zure gogoa!

Orain dirudi itsasontzi bat,
orain ardi bat zoroa,
orain balea, zakur burua,
zeinen hodei arraroa…

Hodeia edo zure gogoa
zein da arrarogoa?
Arraroa da hodeia, baina
gehiago zure gogoa!

Izar jauzia, gaztelu handi,
arranoaren hegoa…
Orain umetxo bat igerian,
zeinen hodei arraroa…

Hodeia edo zure gogoa
zein da arrarogoa?
Arraroa da hodeia, baina
gehiago zure gogoa!