---
id: tx-44
izenburua: Lehen Lerroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TyfyAosMGSs
---

AUZO ZAHARRA (2023)

Abesti hau 2023ko urtarrilean Iruñako 'Sound of Sirens' estudioan Julen Urzaizen eskutik grabatua izan da.
🎥 Bideoaren grabaketa eta edizioa: Mikel Ulli

------------------------------------------
LETRA:

Negua iritsi da auzo zaharrera
begirada bat bota leihotik kalera
ez da ezer entzuten
ez da haurrik jolasten
barruan daramadan haurra hil ote da

Adabakiz jositako arropa merkea
korrika egin eta lehertu osteko hutsartea
amak deitzen gintuen
berandu heldu ez gintezen
irudimenarekin geldi ezinak ginen
kaleak gintuen hezi
kalean ginen hazi
lege bakarra dago galdu edo irabazi

Auzoa zahartzen doa
zein izango da bere geroa
nork lapurtu zigun lehen zuen beroa

Auzoarekin zahartzen goaz
desio bakar bat bizitzan
arratsalde batez posible balitza, berriz ume izan

ADREILUZKO PARETETAN
AUZOKO ZAHARREN IRRIBARRETAN
AZALEAN MARKA UTZI DUTEN ZAURIETAN
GORDETA DAGO ZER GAREN BENETAN

AUZOAN JAIO, AUZOAN HIL
ZAHARTZAROAN BERTAN BIZI
BIZITZA EMAN DIGUN KALE ETA JENDEARI
AZKEN ARNASA ITZULI