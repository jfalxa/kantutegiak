---
id: tx-1929
izenburua: Kokomero Kapitaina Eta Malkojaleen Piratak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cw-p3kWASvQ
---

Eh! Lagunok!
Ikastola berria eraiki dezagun Zuberoan.

Uhh!!

Kokomero kapitaina
eta malkojaleen piratak
ari dira lagunak biltzen
eraikitzeko ikastola Sohütan

Kokomero kapitaina
eta malkoka lehen piratak
ari dira lagunak biltzen
eraikitzeko ikastola Sohütan

Subi subi subi submarinoa
Laura otarraina eta flora foka
Pepe ezpatarraina, Paula olagarroa
Feliz karramarroa eta ipotx triskaria
Galtzagorri, naturzale, irakaslea
eta zirkotik datozen trapezistak
Takolo, Pirrutx eta Porrotx pailazoak
Banbu, Banbu elefantearekin batera.

Krokodiloak katarro zeukan
metalikoa zen bere eztula
kokondotik erori zen tximua
nik lagundu nahi dut ahal dudan neurrian
kokoratza entzuten da
oiloak datoz eta oilarra
kokolore hipopotamoa
zopa prestatu zuen dordokak

Pingüi, Pingüi, pingüino txiki-txikia
Tantaka Yotanka eta Ebaristo dragoia

Joxe grua gidatzen da oso trebea
edozein makina erabiltzen iaioa
jauzika hurbiltzen da matxinsaltoa
jana prest dauka Amona Mantalgorriak
ikastola ikasiko dugu euskaraz
ez gira gu izanen azken mohikanoak

Kokomarro zetorren ahatea
eta inurriak helikopteroan
kro-kro hasi zen kantan igela
hator ongi etorri, hator gugana
Kokomero kapitaina
eta malkojaleen piratak
ari dira lagunak biltzen
eraiktzeko ikastola Sohütan

Kokomero kapitaina
eta malkojaleen piratak
ari dira lagunak biltzen
eraikitzeko ikastola Sohütan