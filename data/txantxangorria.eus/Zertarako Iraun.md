---
id: tx-1318
izenburua: Zertarako Iraun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p9RZ87uRx-4
---

Ekoizpena: Joseba Baleztena. 
Hitzak eta doinua: Miren Narbaiza. 
Masterizazioa: Jonan Ordorika, Mamia estudioa.
paroles
Zuk, ez duzu iraun nahi 
luzaezinari tiraka. 
Zuk, ez duzu izan nahi 
dastaezina ahoan, behin eta berriz. 

Zuk, ez duzu entzun nahi, 
harmoniarik gabeko kantarik. 
Zuk, ez duzu ukitu nahi 
ukaezinik azalean. 

Eta zertarako iraun?
crédits
de l’album MICE, paru le 15 novembre 2017 
Miren Narbaiza: ahotsa, gitarra akustikoa. 
Joseba Baleztena: gitarrak, bajua, teklatuak, perkusioa. 
Ilargi Agirre: bateria.