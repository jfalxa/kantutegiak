---
id: tx-2379
izenburua: Bilbo Euskaldun Dezagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7QFslGUFiPM
---

#saveyouinternet

Bilbo euskaldun dezagun!!

Goizean, goizetik jeiki eta bizkor
eskolara goaz euskara ikasteko
bidean poz pozik salto eta dantza
orain eta geroan euskararen saltsan.

Ez utzi geroko aitatxo amatxo
biharko egunean euskara beharko.
eskolan gara ari heziz eta haziz
euskaraz bizitzen izaten ikasiz.

Eskolatik hasi, euskaraz ikasi
geroko bidean gaitezela hasi.

Ur tantoak dagi jausi_eta jausi
tantaka tantaka harrian zulo egin.
Biziko da bizi erein den hazia
landatzen has gaitezen geroko bizia.

Dilin dalan dindon mintza euskaraz Bilbon
euskaldun izanez betiko zorion.
Mailaka osatuz tipi tapa joanez
lagun, nahi baduzu zu ere etor zaitez.
Izan nahi dugu izan euskaldun osoan
izango gara ta halaxe geroan.

Eskolatik hasi, euskaraz ikasi
geroko bidean gaitezela hasi.

Bilbo geuk bait dugu bai euskaldunduko
ikusiko duzu geu gara lekuko.
Betiko zorion mintza euskaraz Bilbon
dilin dalan Bilbon inor ez lo egon.

Euskara maitea hizkuntza bizia
izan dadila izan jakintza iturria