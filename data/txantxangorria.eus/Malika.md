---
id: tx-898
izenburua: Malika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hrXBFOQwLf4
---

Ikusiko zaitut, karrika hotzean
keffieh buruan, kemen bihotzean
dantzako gogoa begi zorrotzean
hirietan egunero
lotsa dabil gero.

Zato! Zato! Zato Malika!
euri ttantak epelak dira
mila urrats eman dezagun
xurian beltz urturik lagun
keffieh arina buruan
dugun dantza inguruan!

Ez pentsa malika dena dela hitza
fandarrai honetan jarri dugu hitza
mugak ahanzteko kantarik balitza
bihar gara bai goiz argiz
beteko ilargiz.