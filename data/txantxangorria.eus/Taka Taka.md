---
id: tx-3351
izenburua: Taka Taka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YkIGLvtsimE
---

Takatakatakataka taka
arre zaldi ausarta
Takatakatakataka taka
bidean tarrapata
Takatakatakataka taka
hau da hau kabalgata
Takatakatakataka taka
denok batera kanta

Pailazoen abentura
denontzat onura
bihotzean barregura
guztion gustura


Takatakatakataka taka
arre zaldi ausarta
Takatakatakataka taka
bidean tarrapata
Takatakatakataka taka
hau da hau ka/bal/ga/ta
Ta/ka/ta/ka/ta/ka/ta/ka ta/ka
denok batera kanta

Pailazoen abentura
denontzat onura
bihotzean barregura
guztion gustura

Takatakatakataka taka
denok batera kanta
Takatakatakataka taka
iritsi da hara eta


Pa/ila/zo/en a/ben/tu/ra
de/non/tzat o/nu/ra
bi/ho/tze/an ba/rre/gu/ra
guz/ti/on gus/tu/ra