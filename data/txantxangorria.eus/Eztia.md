---
id: tx-717
izenburua: Eztia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HoeUTrwyYYI
---

Nire ametsetan agertu zara,
hainbeste urteren ondoren /
baldosa arteko belarren antzera, 
erne zara.

Ez nuen gogoratzen
neure burua zurekin
nire ahotsa beste bat zen
eta orain, gogoan gorde nahi zaitut
Ni zurekin, zu nirekin,
ta han, barruan dastatu, poliki
begiak ireki gabe
erdi lo oraindik   

Nire ametsetan…

 
Baina esnatu naizenerako,
Berriz, berriz jon zara gosarian 
Mahai gainean irrist egin duen
ezti tanta batean
berriz berriz joan zara
mahai gainean irrist egin duen 
ezti tanta batean.