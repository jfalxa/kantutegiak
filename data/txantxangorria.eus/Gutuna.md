---
id: tx-1382
izenburua: Gutuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YXp9YWNA7zE
---

urtarrilak 13 ostirala,
urteak ikusi ez zaitudala,
aspaldi hontan ez dizut idatzi
baina behin ere ez zaitut ahantzi.

oroitzen naiz batera geundenean
hain zoriontsu ginen garaietan,
nirekin haserratzen zinenean,
zenbat oroitzapen on bapatean,
lehenengo maitasun historietan
eta igondako mendi magaletan
zenbat irri ta negar txikitatik,
oraindik jarraitzen dute bizirik

faltan botatzen zaitut ilunetan,
barruan ezkutatzen dut gehienetan

nik betilez jarraitzen dut kantatzen
barneko ohiu guztiak askatzen,
gauzei buelta gehiegi ematen,
zuk badakizu nolakoa naizen,
amets bat bete zait pentsatzen dut maiz
baina askotan hutsik sentitzen naiz
bestela dena dihoa antzera,
zail egiten zait zutaz galdetzea.

faltan botatzen zaitut ilunetan
barruan ezkutatzen dut gehienetan

urtarrilak 13 ostirala
urteak ikusi ez zaitudala,
aspaldi hontan ez dizut idatzi
baina behin ere ez zaitut ahantzi,
gutun batean esan beharrean
kantuan ausartu naiz azkenean,
goraintzi asko etxekoen partez
besarkada handi bat laisterarte!

sentipenak jarraituz, min hartuz, egin dugu aurrera
behin lotu ginen eta beti gaude batera,
sentipenak jarraitu noizbait nitaz oroitu,
urrundu gintuenak berriz batuko gaitu