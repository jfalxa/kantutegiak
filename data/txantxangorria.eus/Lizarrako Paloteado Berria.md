---
id: tx-3325
izenburua: Lizarrako Paloteado Berria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-iqrHGlKZME
---

Antzina Nafarroa zen

euskaldunen erresuma

bere buruaren jabe

Euskal Herriaren ama

Albako dukea dator

mila bortzehun ta hamabi

armada handia dakar

lehenbiziko Ziordi

Jauntxo gaztelarren aurka

ozenki nafar ahotsa

hirutan matxinatu da

bidali nahian arrotza

Berreskuratu ondoren

nafar lurraldetasuna

Noainen ostu digute

berriro askatasuna

Berrehun agramondarrek

Amaiurren azken guda

independentzia ametsa

ohore handiz galdu da



Hilik dute mariskala

Simancasko espetxean

erregea ukatu gabe

pairatuz zigor luzean

Nafarrak altzatu gara

Hondarribian azkenik

ez dugu onartzen espainiar

jaunen menpekotasunik

Obanosko infantzoien

leloa dugu irrintzi

aske bizi lur askean

nafarrok ez dugu ahantzi

Nafarren izaera da

gure euskalduntasuna

nafarrek eskatzen dugu

euskaldunen batasuna

GORA NAFARROA GORA

Amaiurren kantutakoak

2012ko urriaren 12an.

Lizarra Nafarroa Bizirik