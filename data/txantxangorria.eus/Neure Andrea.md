---
id: tx-2526
izenburua: Neure Andrea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QCVZ2KiWWyY
---

Neure andrea andre ona da
Auzoan hala diote; (bis)
Erositako prezioetan,
Prezio erositakoetan,
Erositako prezioetan,
Pozikan salduko neuke.

Neure andrea goiz jeikitzen da
Erromeria denean; (bis)
Buruko mina egiten zaio,
Egiten zaio buruko mina
Buruko mina egiten zaio,
Lana manatzen zaionean.

Neure andreak atorra zarra,
Berea balu ezer ez; (bis)
Zazpi astean soinean dauka,
Soinean dauka zazpi astean,
Zazpi astean soinean dauka
Aldatutzeko alferrez.