---
id: tx-125
izenburua: Gaztediarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VHYjcVc8LP8
---

Gizaki oro, egoki badu tokia,
delako gauza igortzeko distira,
gazteon bizitasunaz koipetzeko engranaia
herriaren ardatza izan behar gara.
 
Inoiz helduko bada benetako ongizatea
aukera eman behar zaigu jar dezagun gure alea,
gazteon indarra aintzat daukan gizarteak
erabakitasunez eginen du bidea.
 
Ospitale ta lantegi,
laborategi harrobi,
fabrika, ikastetxe, baserri.
Zeinahi delarik ikastegi,
zeinahi delarik bere lantoki,
gazte langilea harro bedi!
 
Lana ez dadila izan bizitzaren helburu,
soilik gu geu izateko bitarteko duin.
Ezetz erranez lanpostu prekarioari,
gure errealizazioa bilatzeari ekin.
 
Bokazioa eta pasioa, aisia eta gozamena,
memoria eta iritzia, izaera eta sormena,
esparru oro lantzeko daukagun ilusioa
babes dezagun gaurkoan bakoitzaren ekarpenaz!
 
Zenbat urte sarturik gure trebetasunak fintzen,
azkenean ahazteko xedea zein den…
Bizitza daukadalako bakarra,
bizi nahi dut nahi dudan erara!
 
Ospitale ta lantegi,
laborategi harrobi,
fabrika, ikastetxe, baserri.
Zeinahi delarik ikastegi,
Zeinahi delarik bere lantoki,
Gazte ikaslea harro bedi!
 
Ontziola, denda txiki,
azoka eta idaztoki,
kontzertu eta abeltegi.
Zeinahi delarik ikastegi,
zeinahi delarik bere lantoki,
gazte langilea harro bedi!