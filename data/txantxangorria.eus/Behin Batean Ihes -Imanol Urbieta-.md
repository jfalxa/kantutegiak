---
id: tx-3211
izenburua: Behin Batean Ihes -Imanol Urbieta-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3HRSUeddCKc
---

Behin batean ihes 
ikastolatik nintzela,
Autoan joan nintzen
aitaren baserrira.
Autoan nindoala 
po, po, po, po (bis)
Bizikletan nindoala 
trin, trin, trin, trin.
Kamioian nindoala 
mok, mok, mok, mok.
Abioian nindoala uh, uh, uh, uh.
Motorrean nindoala 
brom, brom, brom, brom.
Zaldian, trenean.