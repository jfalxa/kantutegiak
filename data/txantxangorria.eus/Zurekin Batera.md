---
id: tx-664
izenburua: Zurekin Batera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M-XjbvBIrLA
---

ZUREKIN BATERA
Zenbat aldiz marraztuko nuke zure besoetan etorkizuna.
Zenbat aldiz aurkituko nuke zure usainaren oroitzapena.
Zenbat aldiz itxarongo nuke aurrez aurre egoteko hitzordua.
Egunak hobeak baitira zurekin batera.

Zenbat aldiz emango nizuke kafe bat hartzeko dudan denbora.
Zenbat aldiz asmatuko nuke zurekin egoteko aitzaki bat.
Zenbat aldiz entzungo nituzke zure ahotik gure istorioak.
Egunak hobeak baitira zurekin batera.

Ta elkartuko gara berriro, gure betiko lekuetan,
zabalduko ditugu besoak, besarkatuz gure arimak.
Ta amestuko dugu gauean, oraindik gelditzen zaiguna.
Egunak hobeak baitira zurekin batera, zurekin batera


Zenbat aldiz ospatuko nuke behar dudanean zu hor zaudela.
Zenbat aldiz ulertuko nuke zure begiradetan aholku bat.
Zenbat aldiz esango nizuke besterik gabe maite zaitudala.
Egunak hobeak baitira zurekin batera.