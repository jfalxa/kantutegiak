---
id: tx-1283
izenburua: Nagusiaren Negarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OxIOjTt_FUE
---

Gure nagusia, gaixo nagusia 
goizetik arrats ta urte guzia 
nigarrez, nigarrez, nigarrez 
nigarrez ari da.

Nigarrez artzeko, nigarrez artzeko 
arrazoinak, arrazoin haundiak 
ba omen ditu …

Ta nagusiaren nigarrek 
langilearen langilearen 
bihotz"eria" hunkitzen …

Nagusiaren, nagusiaren 
nigarrez artzera ez uzteko 
kontsolatu beharko dugu.

Ta nagusiaren kontsolatzeko 
langile, langileek 
irabazi berarentzat 
lan gehiago egin beharko !

Gure nagusia, gaixo nagusia 
goizetik arrats ta urte guzia 
nigarrez, nigarrez, nigarrez 
nigarrez ari da.

Nagusiaren, nagusiaren 
nigarrez artzera ez uzteko 
langileak, langileak 
ez du lanari baizik pentsatuko !

Lan eta lan, lan eta lan, 
lan eta lan, lan eta lan, 
lan eta lan eta lan eta lan eta lan … 
lana … lana .. lana … lana …

Langileak ez du lanari baizik pentsatuko 
ta greba horiek, greba zikin horiek 
bazterrera utziko.

Gure nagusia, gaixo nagusia 
goizetik arrats ta urte guzia 
nigarrez, nigarrez, nigarrez 
nigarrez ari da.

Nagusiaren nagusiaren 
nigarrez artzera ez uzteko 
langileak, langileak 
beti eta beti, beti eta beti 
haren zerbitzuko egon beharko 
ahalaz, ahalaz 
bakantzarik ere ez hartuko !

Lan eta lan, lan eta lan 
lan eta lan beti lan eta lan, 
lan eta lan beti lan eta lan 
beti lan eta lan beti lan … 
beti … beti … beti … beti …

Gure nagusia, gaixo nagusia 
goizetik arrats ta urte guzia 
nigarrez, nigarrez, nigarrez 
nigarrez ari da.

Gure nagusia probeño bat da 
nagusietan probeena !

Hamar etxe 
hiru kotxe 
zazpi sehi 
ehun zaldi 
ta Suitzako bankoetan, abilki gorderik, 
Dolarrak, dolarrak, dolarrak metaka … 
Hori baizik ez da, hori bakarrik 
gure nagusiaren, nagusi gaixoaren 
miseria … Miseria gorria!

Gure nagusia, gaixo nagusia 
goizetik arrats ta urte guzia 
nigarrez, nigarrez, nigarrez 
nigarrez ari da.