---
id: tx-1336
izenburua: Baltsean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/irreiB-_Zhc
---

Urrutiko hirietan bada kantoi sekreturik
Nire laguna han da nonbait bere pausuak gorderik
Han bizi omen da, baltsian egiten gauez ardaua edaten gauez ikararik gabe gauez, gauez
Batzuetan idazten dit oso pozik dabilela
Jendeak han ez daukala bihotzian benenurik
Ai menditik ibili, baltsian egiten gauez ardaua edaten gauez ikararik gabe gauez, gauez, gauez