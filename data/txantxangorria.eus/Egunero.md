---
id: tx-2101
izenburua: Egunero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4z--qaZgnfU
---

Every day things are getting worse.
Time's so hard, la la la, oh oh
Egunero dena okerrago. 

Merkatura eraman zintudan
eta denok parre ene txakurrari
ez dago kerik surik gabe, badakizu
altxa behar da burua.
Egunero dena okerrago 
sasoi latzak, lalala, oh oh lord!