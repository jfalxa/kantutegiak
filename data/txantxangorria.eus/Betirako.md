---
id: tx-879
izenburua: Betirako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6ZuspXUTesQ
---

Basakabi plaza taldeak Elton John kaunaren "Sacrifice" kantua birmoldatzen euskeraz. "Betirako" diskoa 1991. urtean ekoiztu zuten IZ diskoetxearekin.

Ez dut pentsa nahi
etorkizunean
orain hemen gaude
liluratu/a
azala sentitzen
ekaitza bezala
elkar eta indarrez
gure bihotza.

Taupadak bat
isiltasuna
dago gure artean maite
ez dut pentsa nahi.

Benetan maite zaitut
nere maitea
zu ber/din zau/de
zer e/gin/go de/gu o/rain
be/ne/tan mai/te zai/tut
e/gi/a da
zu/re/kin na/hi dut e/gon
e/gu/ne/ro.

Nik be/har dut zu/taz
zu/re laz/ta/nak
be/sar/ka/da e/man in/da/rrez
zu za/ra ni/re/tzat
ez/pai/nak lo/tu/rik
a/mets be/za/la
ez dut na/hi i/ra/tza/rri
gu/re a/me/tsa da.

Tau/pa/dak bat
i/sil/ta/su/na
da/go gu/re ar/te/an mai/te
ez dut pen/tsa na/hi.

Be/ne/tan mai/te zai/tut
ne/re mai/te/a
zu ber/din zau/de
zer e/gin/go de/gu o/rain
be/ne/tan mai/te zai/tut
e/gi/a da
zu/re/kin na/hi dut e/gon
e/gu/ne/ro.

Tau/pa/dak bat
i/sil/ta/su/na
da/go gu/re ar/te/an mai/te
ez dut pen/tsa na/hi.

Be/ne/tan mai/te zai/tut
ne/re mai/te/a
zu ber/din zau/de
zer e/gin/go de/gu o/rain
be/ne/tan mai/te zai/tut
e/gi/a da
zurekin nahi dut e/on
egunero.


Zurekin gauero
Zurekin gauero
zurekin betirako,
zurekin betirako,
zurekin betirako.