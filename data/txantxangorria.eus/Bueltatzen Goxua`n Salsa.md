---
id: tx-208
izenburua: Bueltatzen Goxua`n Salsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_BvSms7nRyQ
---

Bideoa: Jon Sesma (@jonsesmasara)

---------------------------------------

Mundua ikusi nahi dut
baina zure ohetik
Mundua ikusi nahi dut

Ahazten ez duzun kantu
zure ezpainetan
kantua izan nahi dut

Ta non ezkutatzen zinen orain arte
urte luzeegi hauetan agertu gabe
Helduidazu eskutik estu arren
ilun dago ta ez dakit bueltatzen

Itsaso izan nahi dut
zure ekaitzetan
Itsaso izan nahi dut
Etxerako bidea
soilik zure oinetan
Bidea izan nahi dut

Ta non ezkutatzen zinen orain arte…