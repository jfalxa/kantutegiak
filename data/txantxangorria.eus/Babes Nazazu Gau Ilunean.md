---
id: tx-2447
izenburua: Babes Nazazu Gau Ilunean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zizVAoHymwo
---

Falconek, Josu Eizagirrek eta hirurok bideo hau prestatu dugu Altsasuko gazteen inguruan izan diren gertakariak direla eta. Justizia eta epai proporzionatu bat eskatzen duten adierazpenekin bat egiten dugu.

Beldurrak badauka nire bihotza estututta
zalantzak baditu nire oinak gelditzen
nahasmenak banauka korapiloan lotuta, 
babes nazazu gau ilunean.

Euriak baditu nire bideak lokaztuta,
haizeak badizkit itsasoak haserretzen,
noizbait sentitzen banaiz isolatuta,
babes nazazu gau ilunean.

Gau ilunean, gau ilunean,
galduta banago gau ilunean;
gidatu itzazu nire pausoak.

Ezpaita batere xamurra
gozoa gazi bihurtzen denean;
garai bateko bide ziurra,
orain ezinaren labirintoa.

Nekeak banauka zulo beltzean ondoratuta,
etsipena bada nire baitan errotzen,
sentitzen banaiz zeharo indargabetua,
babes nazazu gau ilunean.

Gau ilunean, gau ilunean,
galduta banago gau ilunean;
gidatu itzazu nire pausoak,
babes nazazu gau ilunean,
babes nazazu gau ilunean.