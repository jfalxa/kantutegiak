---
id: tx-772
izenburua: Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y7YKOuJrOgA
---

Sara Zozaya-ren EP berriaren aurrerapen kanta. 
Thanks to Rockein 
Video: Arriguri 
Actor: Iker @melo_dramatico
Productor: Jon Agirrezabalaga
Grabación y master:  El Tigre Studios 
Guitarras: Antton Goikoetxea


Letra: 

Une batetik bestera hemen ez zeunden.
Ume baten mina ikusi nuen. 

Zure gain utzi nun itzala,
hitza nere menpe
hotz sentitu nuen mina 
joanean, joane han zen. 

Haren bila ibiltzen ginen bakoitzean
haren beldurrez bukatzen nuean ixilean.

(Nik, bi nituen 
eta bat, orain bat, orain bat)

Haren bila ibiltzen ginen bakoitzean
haren beldurrez bukatzen nuean ixilean.

Ixilean.