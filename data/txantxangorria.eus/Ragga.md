---
id: tx-940
izenburua: Ragga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O2BPOCnObFs
---

Nik erlijio huntan sinisten nuela
Nik mundu honi buruz dena nekiela
Ohartu naiz apaizen sotana bezala
Nere zalantzak beltz eta luzeak direla.

Nik iraultzaren alde borroka nuena
Pentsakizun tinkotan mugitu nintzena
Atzera begiratu ta ikusten dena
Intxaur oskol ximel bat ibaian barrena!

Meditazioekin ez nuenez jaten
Beste bide batetik behar nuen irten
Kaletik "Hare Krishna" joan naiz esaten
Baina han ere ezer ez zuten ematen!

Azkenik ziur nintzen kapitalismotik
Bizi behar nintzela, ez dakit zergaitik
Baina zer arraiotan ari naiz oraindik!
"Yuppia" izateko kontatzen ez dakit.

Laberinto huntan nun da bukaera?
Nun dago bidea, nun dago irteera?
Batzuek benetan xelebreak gera
Nola heldu ninteke zorion batera?

"Body Building-a" ere probatu daukat
Baita ere "Jogging" eta "Mountain Bike-a"
Baina kirolik inoiz ezingo dut maita
Egin ala ez egin berdin hilko naiz ta!

Azkenik janaria modaren kontura
Aldatua dut baina hau ez da itxura!
Bitaminak, belarrak eta zanahoriak
Nazkaz hiltzeko ez nintzen etorri mundura!

Laberinto huntan nun da bukaera?
Nun dago bidea, nun dago irteera?
Batzuek benetan xelebreak gera
Nola heldu ninteke zorion batera?

Hainbeste zalantzekin nekatuta nago
Beti naiz azkenean haizeen esklabo
Lasaitu beharko dut bestela akabo
Sinistu zaidazue ezin dut gehiago!

Berriz Euskal Herrira bueltatu nintzan da
Zerbait ikasi nuen aitonak esanda
Trikitrixa, sagardo eta porrusalda
Mundu ero honetan hoberik ba al da?

Laberinto huntan nun da bukaera?
Nun dago bidea, nun dago irteera?
Batzuek benetan xelebreak gera
Nola heldu ninteke zorion batera?