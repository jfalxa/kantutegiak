---
id: tx-2895
izenburua: Musika Ikasten Ari Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7tXPQC-5qSc
---

Musika ikasten ari gera
Musika denok bat batera
Bost marra: PENTAGRAMA!
Aurretik: SOLen ikurra.
Ondoren: biko neurria
eta gero: notak hasten dira.
Marretan: MI SOL SI RE FA
tarteetan: FA LA DO MI
Zuriak: DO-ON! DO-ON!
eta beltzak: DAN DAN DAN 
eta kortxeak: DILIN DALAN! DILIN DALAN!
Eskala: DO RE MI FA SOL LA SI DO
SI LA SOL FA MI RE DO
Bi puntu: Berriz hastera!