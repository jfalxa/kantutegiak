---
id: tx-2542
izenburua: Zirku Bat Datorkigu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RQKhqL9xUiw
---

Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Zirku bat datorkigu,
Kaletik ikusi dugu.
Tzunpa, tzunpa, tzunpa, tzun!
Atzetik joango gara.

Aurretik elefanteak,
mugituz sudur luzeak.
Dunba, dunba, dunba, dun!
Ipurtandiak astinduz.

Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Zirku bat datorkigu,
Kaletik ikusi dugu.
Tzunpa, tzunpa, tzunpa, tzun!
Atzetik joango gara.


Aurretik elefanteak,
mugituz sudur luzeak.
Dunba, dunba, dunba, dun!
Ipurtandiak astinduz.
Lehoiak kaiolan doaz,
begira gaude ikaraz,
haien hagin zorrotzak!
Saltoka daukat bihotza.

Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Zirku bat datorkigu,
Kaletik ikusi dugu.
Tzunpa, tzunpa, tzun!
Atzetik joango gara gu.

Tximino bat komerika,
lurretik dabil jiraka.
Barrez gaude begira.
Gure beldurrak joan dira.

Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Pra-pra! Pra-/!
Tzunpa, tzunpa, tzun!
Zirku bat datorkigu,
Kaletik ikusi dugu.
Tzunpa, tzunpa, tzun!
Atzetik joango gara gu.

Algaraz eta oihuka
hemen datoz pailazoak
haur artean banatuz
puxikak eta muxuak.

Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Pra-pra! Pra-pra!
Tzunpa, tzunpa, tzun!
Zirku bat datorkigu,
Kaletik ikusi dugu.
Tzunpa, tzunpa, tzun!
Atzetik joango gara gu.