---
id: tx-1556
izenburua: Agur Pakito
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qIbxIgryeks
---

Konposizioak, moldaerak zein programaketa: XABIER ZABALA

Udaberriarekin bat, 
bat-batean izan zen, 
urrutitik Euskal Herrira 
igeri etorri zinen, 
gurekin gelditzeko, 
gurekin betirako... Bai!
Salto (e)ta jauzi,
Ni, berriz, urduri deika,
kostaldetik zuri.
Urpera irrikaz
txistu (e)ta jolas,
harrapaketan lasterka
sardinekin zoaz.


Udaberriarekin bat, 
bat-batean hil zinen.
Kantauriko itsasertzean
malkotan bildu ginen
Goxo eman genizun lur
Pa goxo bat eta ...agur... Pa!
Salto (e)ta jauzi,
itzulipurdi.
Ni, berriz, urduri deika
ametsetan zuri.
Urpera irrikaz
txistu (e)ta jolas,
bizkar hegatsetik heldu
(e)ta biok bagoaz,
Pakito izurde maitea!