---
id: tx-1555
izenburua: Irri Egiten Dunari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D8PwToIG30U
---

Irrifar laranja bat margotu
diogu eguzkiari
goizero egin diezagun harek
koloretsuago argi
esnatu dezan bihotz barrutan
lo dugun bizi poz hori.

Irrifar iheskor bat margotu
diogu hegoaizeari
sekula ez da berriz izango
berri beltzen mandatari
Kantu zaharren doinuan orain
Firi firi txistu dagi.


Irrifar pottolo bat margotu
diogu laino beltzari
orain euria milaka tanta
koloretsutan du ari
zipriztinetan bizia emanaz
gure herri zuri-beltzari

Irrifar argitxu bat margotu
diogu ilargiari
orain ilberatan zilarrezko
irrifar fin fina dagi.
zerutik jeikita dabil
amesgaiztoen ehiztari.

Irrifar erraldoi bat margotu
diogu mundu zaharrari
bere buruari biran doan
nahi gabeko bidaiari
munduko herri guztitan irri
zati bat geratu bedi.

Irrifar txikitxo bat margotu
diot neure buruari
eguzki ilargi hegoaize
mundu zahar eta euri
denak keinuka ari baitira
IRRI EGITEN DUNARI