---
id: tx-510
izenburua: Bautista Basterretxe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AkYz-dTFNfg
---

Bautista Basterretxe
mutiko pijoa
neri gurdi-ardatza,
neri gurdi-ardatza
ostuta dihoa.
Beltzak eta zuriak
izango dituzu,
neri gurdi-ardatza
neri gurdi-ardatza
emate ez baidazu.

 Nere idi-pareak
dauzka zintzarriak,
lepotikan zintzilik,
lepotikan zintzilik
zildain jarriak.
Gainera uztarriak
ta kopetekoak,
ontz urre gorri zaharrez
erositakoak.

Buru gogorrak badu
berekin kaltea
ez dedan gauza nola,
ez dedan gauza nola
nahi dezu ematea?
Zentzuak galdurikan
arkitzen zerade,
bestela behintzat horla,
beztela behintzat horla
ariko ez zinake.

Gezur ta abar zabiltza,
Bautista tranpean.
Zu bezain gezurtirik,
zu bezain gezurtirik
ez da Euzkal Herrian.
Zure berri dakite
inguruko denak,
gordetzen dituzula
gordetzen dituzula
gauza besterenak."