---
id: tx-1807
izenburua: Mugimenduan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D_efKvUEm9s
---

Zeru izartsuaren azpian,
non dago bakoitza?
Zeintzuk esnatuko dira?
Beti igaroko du gaua esna
Irauten badakigu,
bizitzea ahazten zaigu.
ikasgai konplikatuegia,
az/ter/ke/tak mo/men/tu o/ro
ta ondoezak ezin kendu.

Mugimenduan geldi
ala geldian mugituz zaude?
zenbat begi,
zein begirada gutxi.

Mugimenduan geldi
ala geldian mugituz zaude?
zenbat begi,
zein begirada gutxi.

Etorri eta joango garen,
non ezagutzerik baden,
zertarako-a erabaki.
Zergatietarako ez dut astirik,
baina badut nola-ra
ausartzeko motiborik.
Ametsen geruza zeharkatu,
bihotzari entzun,
buruarekin lagundu.
Menpekotasunak gainditzean
zoriona badela ikusiko duzu.
uuuhhh!!,  uuuhhh!!,


Mugimenduan geldi
ala geldian mugituz zaude?
zenbat begi,
zein begirada gutxi.

Mugimenduan geldi
ala geldian mugituz zaude?
zenbat begi,
zein begirada gutxi.