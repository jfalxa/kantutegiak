---
id: tx-1887
izenburua: Txiki Txikitik-Salamankara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gy9ukiV27A4
---

Txiki txikitik aitak ta amak
fraile ninduten nonbratu
bai eta ere estudiora
Salamankara bidaldu.
Salamankara nindoalarik
bidean nuen pentsatu
estudiante tunante baino
hobe nuela ezkondu.

Ostatu xume polit batean
gosez gelditu bainintzen
neska xarmant bat ari zitzaidan
mahainean zerbitzatzen.
Begia kartsu, ezpaina lore,
enekin aise mintzatzen
aingeru hori ordu berean
ene bihotz barnean zen.

Hitz erdi batez maite nuela
erran nion belarrira
baina, gaixoa, herabetua
ihes joan zen kanpora.
Ez ahal nauzu, izar ederra,
kondenatzen infernura!
Ez da sekulan ene gogotik
histuko zure itxura.

Aita Jainkoak egin banindu
zeruetako giltzari
orduntxe bai jakinen nuen
atea nori ireki.
Lehenengo aitari, gero amari,
gero anai-arrebari,
ta azken orduan isil-isilik
nire maite politari.