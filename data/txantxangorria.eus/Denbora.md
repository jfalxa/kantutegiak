---
id: tx-2906
izenburua: Denbora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SXmC-VApG3g
---

«Herri Bihurtu Zaidan Deserrian» Poesia eta musika emanaldia.

Begi itxiak begira, 
muxu goxo bat emanaz gaua argitzen ari da, 
arren estutu nazazu, ez joan!
Arren esaidazu berriz atzo zenion bezala
ezin duzula ni gabe bizitza desioz bete.
Aurrera doa dena, gu non gaude
bizi nahian joan da, oharkabe, denbora.
Denborak eraman zuen maite ginenaren xarma
berriro ekarri dugu eta berriro darama.
Aurrera doa dena, gu non gaude?
Bizi nahian joan da, oharkabe, denbora.
Itxi begiak itxi eman muxu goxo bat,
badator berriz gaua,
geneukana galtzetik, daukaguna galtzera
tarte txikia dago... Denbora!