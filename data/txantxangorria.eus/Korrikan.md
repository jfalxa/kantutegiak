---
id: tx-974
izenburua: Korrikan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dmc9XoEmwH4
---

Atzo ginen ume eroak
amets huts bateri lotuak
gizartearen ema galduak
buru bero eta harroak

Gaur aldiz eta milaka
jendea doa lasterka
amets horren atzetik presaka
hauxe da bada korrika.

Korrikan, korrikan
korrikan, sartu hadi korrikan
korrikan, korrikan
euskararen onetan.

Atzo zinen trapu zaharra
botatzeko gauza zatarra
ahalgearen sustagai bakarra
besten artean hain zakarra.

Gaur aldiz zara zuhaitza
mila hosteen emaitza
gure izatearen hatsa
gure herriaren bihotza.