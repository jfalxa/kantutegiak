---
id: tx-2769
izenburua: Bizkaia Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/K7ORdHTBTco
---

Bizkaia maite 
atzo goizean ikusi zindudan 
soineko xuriz jantzia 
buruan orlegi, bihotzean sua 
nere gogoaren ertzatik pasatzen. 
Zure usain gozoa, lana, 
amodioa, itsasoa, 
nere baitan sartzen. 
Atzo goizean entzun nuen 
zure berbaren oihartzuna, 
zure kantaren fereka, 
bihotzean kilika 
eta ohiartzunaren haunditasunean 
murgilduz joan nintzen 
jausika, hegaka. 
Bart arratsean 
arbasoen baratzaren ondoan 
bertsotan eta dantzan 
lotu zinen alai piper eta gatza
sabel emankorra. 
Bizkaia maite 
atzo goizean ikusi zindudan 
soineko xuriz jantzia 
buruan orlegi, bihotzean sua. 
Olerkari 
penatuaren gozo eta mina 
amodio eta kanta 
zure berba leun, 
zure gatzaren bizia, 
zure burdinaren goria dira gaur 
neretzat aterbe. 
Bizkaia maite 
atzo goizean ikusi zindudan 
soineko xuriz jantzia 
buruan orlegi, bihotzean sua, 
lirain, sendo, eder.