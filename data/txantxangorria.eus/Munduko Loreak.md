---
id: tx-1194
izenburua: Munduko Loreak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WCuBy8xFnkc
---

Olerkia: Amets Arzallus

Munduak handientzat bakarrik pentsatu 
zituen eskubideak. Naina haur txikieta eta herri txikiek ere altxatuko dituzte beren koloretako esku libreak.

Buztinez egin zuen mundua lurraren koloreak, 
herritxoen forma hartu zuen eskutan oreak, 
zapi berdeak eta zapi moreak, 
herririk herri oinez dabiltza munduko loreak.

Nire kolorean munduari nahi diot esan:
Loretxoek zergatik ezin ote du tximeleta izan?

Euskal Herriak koloretako haur txikiak baditu,
koloretako Euskal Herriak mundutxoak ditu,
munduak herri asko baldin baditu,
gure herriak ere munduko koloreak ditu.

Nire kolorean munduari nahi diot esan: 
Herritxoek berhar dute libre izan.