---
id: tx-3175
izenburua: Abendua Mikel Urdangarin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NPEaCDPNvaY
---

Abendua da andra mari zuriaren plaza,
kafea hosten daukat aurrean tabernako
leihotik ikusten da mundua,
denak azkarregi doaz,
Dublineko Nereak goizero bezela oparitu
dit bere irria,
eskerrak aingeruak badirela gurea,
zinez bejondeiola.

Hau oraindik ezta amaitu entzui
dazu maitea, nik eman izan banizu zuk 
behar duzun bakea. Baino oraindik hor
gordetzen dut azken neguan zeugaz,
Isilpean pasa gendun itsasoari begira

Esaten dute uda iragan ostean,
baretu direla bihotzak, 
baina nirea erruz gogortu nahi
didaten haizeak eta hotsak
parte esna pasatu dut pentsamendua suan,
aspaldian hau lagun zen loak
atzerako biderik esan laukate sale
Bizitza arana sokan.

Hau oraindik ezta amaitu entzui
dazu maitea, nik eman izan banizu zuk
behar duzun bakea. Baño oraindik hor
gordetzen dut azken neguan zeugaz,
Isilpean pasa gendun itsasoari begira