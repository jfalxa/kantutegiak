---
id: tx-2711
izenburua: Gora Rioja
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PBqgqXX27-Y
---

Biba Rioja, biba Nafarra,
arkume onaren iztarra,
hemen guztiok anaiak gara
hustu dezagun pitxarra.

Glu, glu, glu...

Gure sabela bete behar da
ahal bada gauza onarekin;
dagon lekutik, eragin bapo!
aupa mutilak! gogoz ekin.

Umorea da gauzik onena
nahigabeak ditu ahaztutzen
uju ta aja hasi gaitean
Euskal doinuak kantatzen