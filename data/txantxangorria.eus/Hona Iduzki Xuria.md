---
id: tx-237
izenburua: Hona Iduzki Xuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ApPt5-rv4Q0
---

Huna iduzki xuria: lano artetik uria...

Kantuz asi da neure maitia entzun dut haren irria,

Entzun dut haren irria eta... barna daut egin zauria!

 

Lurra estali elurrez: hozturik nago kaskarez...

Maitia ganik urrun izanez, haren galtzia beldurrez

Haren galtzia beldurrez eta... urten ari naiz nigarrez!

 

Gauaz egonik sasian, xoria goizik airian...

Penetan dena, laster agian, goxatuko da bakian,

Goxatuko da bakian eta... sartuko zorionian!