---
id: tx-2371
izenburua: Alkohol Eta Barre Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YFyRa5hh71Y
---

Letra: 
Denbora dezente pasa da jada
elkar ezagutu genuen egun horretatik. 
Munduak gutxinaka jaten gaitu, 
baina gu mundua jateko gosez gaude oraindik. 
Zenbat amets geratu zaizkigun bidean, 
egunerokotasunaren atzean. 
Ta beste behin, astean zehar lanean 
biltzen joan garen amorrua alkohol artean itoko dugu. 
Ta berriz elkartuko gara pote bat hartzeko tabernaren batean. Kristonak botako ditugu besteei buruz alkohol ta barre artean. Alkohol ta barre artean! 
Berandu da atzera bueltatzeko, 
baina goizegi da oraindik amore emateko. 
Rock and rollak batu gintuen, 
baina orain heriotzak bakarrik gaitu bereiztuko. 
Zenbat amets geratu zaizkigun bidean, 
egunerokotasunaren atzean. 
Ta beste behin, 
astean zehar lanean biltzen joan garen amorrua 
alkohol artean itoko dugu. 
Ta berriz elkartuko gara 
pote bat hartzeko tabernaren batean. 
Kristonak botako ditugu 
besteei buruz alkohol ta barre artean. 
Alkohol ta barre artean! 
Ta noizbait ohartuko gara 
aspergarria dela horrela ibiltzea. 
Gure amorrua mozokor bihurtzeko, 
askoz hobe genuela borrokatzea. 
Borroka da bidea!


#saveyourinternet #Artículo13