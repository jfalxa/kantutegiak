---
id: tx-3280
izenburua: Egia Bat Esateagatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eQdfTRL-5c4
---

Egia bat esateagatik,
alabak
hil behar bazaizkit,
andrea
bortxatu behar badidate,
etxea
lurrarekin
berdindu behar bazait;
Egia bat esateagatik,
ebaki behar badidate
nik eskribitzen
dudan
eskua,
nik kantatzen
dudan
mihina;
Egia bat esateagatik,
nire izena
kenduko badute
euskal literaturaren
urrezko
orrietatik,
inoiz,
inola,
inun
eznaiz
isilduko.