---
id: tx-388
izenburua: Negua Iker
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2bdFPlD8DZg
---

Lainotu didazu zerua.
elurtezina egin zait negua.

Sandaliak ez dituzu kendu
urtaro izoztu honetan zehar...

Baina gaur, agian
Itzuliko zara berriz

Leihoak
zabalik
utzikoditut zuretzat.

Zure begi hotz horietan
islatzen duzu nor zaren benetan.

Haizea bazterretatik sartzen
eta zu berriz ez zara ausartzen.

Baina orain hegoak
astintzen laguntzeko.

Gogor heldu eta
besarkatu nazazu.

Nekatu naiz sabaian izarrak marrazteaz
ondoren zuk brotxakada batez ezabatzeko.
Nekatu naiz sabaian izarrak marrazteaz
ondoren zuk gainetik margotzeko.

Astindu ditu lurrikarak
gure arteko bide bareak.

Bi urte bete dira
hustu ninduzunetik.