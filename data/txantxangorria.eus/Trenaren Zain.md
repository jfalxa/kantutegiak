---
id: tx-1623
izenburua: Trenaren Zain
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YtTmvitpM6w
---

Aurpegi zurbil nekatu hartan 
Irribarre bat margotuaz 
Egunero hamabietan 
Kaleetan bakarrik 
Bere zapata apurtuekin 
Bere soineko ederrenaz 

Denek galdetzen zioten zergatik zoaz 
Amets galduen geltoki zaharrera 

Barrez urrunduz agur zioen 
Ez zela berriz itzuliko 

Beti galdetzen zioten zergatik zoaz 
Amets galduen geltoki zaharrera 

Zer ezkutatzen zuen begi ilunetan 
Zer kantatzen zion haizeari 
Izenik gabeko mila muxu gordetzen zituen 
Itxaropenaren trenaren zain 

Bere soineko ederrenaz 
Zapata hautsiak oinetan 
Isil-isilik bueltatzen zen 
Egunero hamabietan 

Zer ezkutatzen zuen begi ilunetan 
Zer kantatzen zion haizeari 
Izenik gabeko mila muxu gordetzen zituen 
Itxaropenaren trenaren zain 

Baina gaur ez da itzuli 
Non dago, ez da itzuli 
Joan eta ez da itzuli 
Non dago, ez da itzuli