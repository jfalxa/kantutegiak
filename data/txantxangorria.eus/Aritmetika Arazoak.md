---
id: tx-1933
izenburua: Aritmetika Arazoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GH2clCpgEuU
---

Zenbat lurral/de ditu
Gaur Euskal Herri/ak?
Zenbat lurralde ditu
Gaur Euskal Herriak?
Euskadik adina bai
Balitu erdiak!
Euskadik adina bai
Balitu erdiak!

Zenbat euskaldun ditu
Gaur Euskal Herriak?
Zenbat euskaldun ditu
Gaur Euskal Herriak?
Ez li/ra/te/ke gu/txi
Balitu erdiak!
Ez lirateke gutxi
Balitu erdiak!

Zenbat aditu ditu
Gaur Euskal Herriak?
Zenbat aditu ditu
Gaur Euskal Herriak?
Ez ote luke hobe
Balitu erdiak?
Ez ote luke hobe
Balitu erdiak?

Zenbat aurkari ditu
Gaur Euskal Herriak?
Zenbat aurkari ditu
Gaur Euskal Herriak?
Herritarreko bosna
Bali/tu erdiak!
Herritarreko bosna
Balitu erdiak!

Zenbat maitale ditu
Gaur Euskal Herriak?
Zenbat maitale ditu
Gaur Euskal Herriak?
Erruz maitatu izan
Balitu erdiak!
Erruz maitatu izan
Balitu erdiak!

Zenbat bekatu ditu
Gaur Euskal Herriak?
Zenbat bekatu ditu
Gaur Euskal Herriak?
Ez luke barkamenik
Ba/li/tu er/di/ak!
Ez luke barkamenik
Balitu erdiak!

Zenbat gobernu ditu
Gaur Euskal Herriak?
Zenbat gobernu ditu
Gaur Euskal Herriak?
Behar baino gehiago
Balitu erdiak!
Behar baino gehiago
Balitu erdiak!

Zenbat arazo ditu
Gaur Euskal Herriak?
Zenbat arazo ditu
Gaur Euskal Herriak?
Asko samar oraindik
Ba/li/tu er/di/ak!
Asko samar oraindik
Ba/li/tu er/di/ak!

Zen/bat gal/de/ra di/tu
Gaur Eus/kal He/rri/ak?
Zen/bat gal/de/ra di/tu
Gaur Eus/kal He/rri/ak?
Na/hia/go nu/ke bi/har
Ba/li/tu er/di/ak!
Na/hia/go nu/ke bi/har
Ba/li/tu er/di/ak!

Zen/bat e/ran/tzun di/tu
Gaur Eus/kal He/rri/ak?
Zen/bat e/ran/tzun di/tu
Gaur Eus/kal He/rri/ak?
Ai ho/be/ki pen/tsa/tu
Ba/li/tu er/di/ak!
Ai ho/be/ki pen/tsa/tu
Ba/li/tu er/di/ak!