---
id: tx-1979
izenburua: Zuhaitzen Denbora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tCTpLg-aq_s
---

Zugan da zuhaitzen denbora,elkar maitatu ondoren
ohean lo, betazalek soilik zaituzte estaltzen.

Ez jarraitu beldurrari
ez esan beti, ez esan inoiz ez
utzi libre munduari bidea egiten.

Zugan da zuhaitzen denbora,elkar maitatu ondoren
plazer-urak biltzen zaitu malkoak begia legez.

Ez jarraitu beldurrari
ez esan beti, ez esan inoiz ez
utzi libre munduari bidea egiten.