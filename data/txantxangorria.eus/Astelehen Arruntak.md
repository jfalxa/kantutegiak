---
id: tx-2403
izenburua: Astelehen Arruntak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mCb1T9Fxrak
---

Josu Bergara musikariaren "Asunak" disko berriaren lehen bideoklipa. 
Ahotsa, gitarra eta musika: Josu Bergara 
Biolina eta konposizioa: Nerea Alberdi 
Pianoa: Mikel Nuñez 
Ahotsa: Irati Bilbao 
Gitarra: Lüi Young 
Kontrabaxua: Dani Gonzalez 
Bateria: Txema Arana 
Produkzioa: David Sanchez Damián 
Bideogileak: Gaizka Peñafiel eta Asier Olazar 
Aktorea: Ane Pikaza 
Makillajea: Arritxu Eizmendi. Anitzberria. 

Esker bereziak: K2 taberna. Iñigo Navalón. 
Antuñano haziak denda. Eduardo Antuñano. 
Jabier Bergara 
Ainhoa Tirapu 

ASTELEHEN ARRUNTA (Letra: Josu Bergara) 
Hau bai astelehen arrunta, 
hiri grisaren magalean, 
semaforoak dardarka, 
gorria zure ezpainetan. 
Kafea beti ebakia, 
alboko tabernaren barran, 
ederto saldu ziguten, 
asfaltoaren sukarra. 
Begiak itxita, 
hiru segunduko amets laburra. 
Konturatu zara, gaur bai, 
gaur dela eguna. 
Eta mundua biraka, 
goizetik gauera, 
izarrak tatuajean dantzan, 
zeure besoetan. 
Eta urrutira begira, 
harrapatzen zaitudanean, 
bizitza azkarregi doa 
bizitza azkarregi doa 
eta mundua biraka. 

www.josubergara.eus 
Kontratazioa: info@taupaka.eus I (+34) 688 840 713 www.taupaka.eus