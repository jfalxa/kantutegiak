---
id: tx-352
izenburua: Gogoratzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8_FaGyWRiIg
---

"Lo Eza" diska berria eskuragarri plataforma digital guztietan! Joan zaitez disfrutatzera!

¡El nuevo disco de INSÖMNIA, "Lo Eza", ya está disponible en todas las plataformas digitales! Escúchalo ya y compártelo con tus amigxs.


Video grabado por Pello Maudo, Esti Gutierrez, Amaia Santamaria y Patxi Ortega.

Edición de vídeo: Pello Maudo

Gaur zutaz oroitu naiz
Zenbat denbora pasa da?
Gaur faltan bota zaitut
Zure argazkia ikustean
Gogoratzen ahal dituzu gure momentuak?
Gure gaueko konbertsazinuak?
Gogoratzen ahal dituzu zoriontsu ginen
Garai onak?
Garai onak
Itsasertzeko lehen musuak
Ahhh...
Gaur lo eza daukat
Beste egun bat
Esna ametsetan
Zugaz
Zu, ain irribarretsu
Ta ni zoratuta naukazu
Gogoratzen ahal dituzu gure momentuak?
Gure gaueko konbertsazinuak?
Gogoratzen ahal dituzu zoriontsu ginen
Garai onak?
Garai onak
Itsasertzeko lehen musuak
Ahhh...
Bat, bi, hiru!
Uh!
Gogoratzen ahal dituzu gure momentuak?
Gure gaueko konbertsazinuak?
Gogoratzen ahal dituzu zoriontsu ginen garai onak?