---
id: tx-2391
izenburua: Hiri Galduetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5I_6Iev6J5A
---

HIRI GALDUAK - Hiri Galduetan

Bideo hau Irailak 2, Egiako Txondorra Kultur Elkartean (Donostia) grabatua izan da. Teknikari moduan:

- Soinu/Sonido= Beñat Antxustegi
- Bideoa/Imagen: Adrian Serna (Zerrat)

HARREMANETARAKO/DATOS DE CONTACTO:
 
- Email: hirigalduak@gmail.com


KILOMETROZ KILOMETRO 
AKORDEZ AKORDE 
GURE ARIMA USTU DEZAGUN 

KONTZERTU OSTEKO BARREAK 
EGINDAKO LAGUN BERRIAK 
MOMENTU AHAZTEZINAK 

IRUDI MAJIKOAK 

HIRI GALDUETAN BARRENA 
ETORRI ZAITEZ GUREKIN 

DANTZA EGINEZ 
GAU OSOAN 
DANTZA EGINEZ 
HIRI GALDUETAN