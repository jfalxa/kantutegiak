---
id: tx-2741
izenburua: Amets
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E4C30Zpz1Ms
---

Amets, amets egin itsas gizona,
amets, zure ametsa gerta leikela.

Berriz etorriko zera Ondarru'ko kaiera,
han daukazu maitea, itxaroten dezuna,
berriz etorriko zera, zure txaluparekin,
han daukazu maitea ta bihotza.

Han daukazu maitea ta bihotza.