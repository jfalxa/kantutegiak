---
id: tx-165
izenburua: Ta Zer Ez Da Berdin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pyr4EcHaLB4
---

Ez dut ezer, ez dut ezer inoiz
ez dut ezer inoiz zuri esateko
ohiturazko egunkari eta irratiek ere
ez lukete hobe eginen
euskadin rokanrolak ez du inoiz dirurik emanen
inon baino alaiago izanen da
ta zer ez da berdin
ezin zaitut, ezin zaitut inoiz
ezin zaitut inoiz zu bisitatu
munduan ez da kokolorik ez zeruan ere
ni bezain gaizki eginen lukenik
euskadin rokanrolak ez du inoiz dirurik emanen
inon baino alaiago izanen da
ta zer ez da berdin
ez dut ezer esateko
ezin zaitut bisitatu
rokanrolak ez dik dirurik emanen
berdin zait dena, zintzilik nago
ta zer ez da berdin.