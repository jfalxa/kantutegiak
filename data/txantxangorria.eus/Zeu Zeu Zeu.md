---
id: tx-599
izenburua: Zeu Zeu Zeu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a4EafDotudE
---

Zeu, zeu, zeu, zeu, zeu, zeu, zeu, zeu, (Lau aldiz)
Santutxo plantak egiten dozuz
Errez engainetan dozu
Zeure aingerutxu aurpegi horrek
Ez zaittu xalatzen
Baina
Nik badakit zu nor zaren
Ondo baino hobeto ezagutzen zaittut
Zuk niri palu bet baino gehiago
Sartunde dekoztazu
Zeu, zeu, zeu
Printzesa kapritxosie
Zeu, zeu, zeu
Hain hotza eta hain dultzie
Zeu, zeu, zeu
Lotzen nauen aramusarie
Nire pozoie
Kartak markatu egiten dozuz
Eta mahai azpitik jokatzen dozu
Esku baten larrosak dekozuz
Labanak bestien
Lorak nahi dozuz baratzien;
Ay
Printzesa kapritxosie
Baina erein dozuzen hazi guztixek
Sasixek dire
Zeu, zeu, zeu
Printzesa kapritxosie
Zeu, zeu, zeu
Hain hotza eta hain dultzie
Zeu, zeu, zeu
Lotzen nauen aramusarie
Nire pozoie
Barre gorri erantzun honeri
Zer egingo deutzozu hurrengoari?
Suten dagoen edozein untzeri
Heltzen zara bizioagaitik
Printzesa kapritxosie
Pain hotza eta hain dultzie
Lotzen nauen aramusarie
Zara zu
Zeu, zeu, zeu
Printzesa kapritxosie
Zeu, zeu, zeu
Hain hotza eta hain dultzie
Zeu, zeu, zeu
Lotzen nauen aramusarie
Nire pozoie
Zeu, zeu, zeu
Zeu, zeu, zeu
Zeu, zeu, zeu...