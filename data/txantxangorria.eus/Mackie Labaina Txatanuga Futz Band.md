---
id: tx-3076
izenburua: Mackie Labaina Txatanuga Futz Band
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WfFRld1leCE
---

Entzun erne, jaun andreok
kontatuko dudana
txorieder golfo baten
pasadizu ta lana.

Itxuroso ta galante,
trajea ta gabana,
an/draz/ko/ak ba/zi/la/tuz
dan/tza/ri ne/ka/e/zi/na.

Di/ru/a bai, la/pu/rre/ta
be/re jain/ko ba/ka/rra,
gau ta e/gu/nez pen/tsa/tu/az,
zein o/te hu/rren/go/a?....

Pis/to/la bat, por si a/ka/so,
be/ti lis/to ge/rri/an
la/bai/na/ren diz/ti/ra da,
i/lar/gi/a es/ku/an.

Nes/ka/txo/en zo/ra/me/na
be/re jo/an-/e/to/rri/a
Mack i/ku/siz sor/tzen ohi da
ber/tan e/ro/ke/ri/a.

Mack ist Mack
gus ta in txa txa txa

Mack ist Mack
gus ta in txa txa txa

Han/dik ho/na ze/bi/le/la
a/mo/di/o gau ba/tez,
in/gu/ra/tu o/men zu/ten
piz/ti txar ba/ten le/gez.

A/rin as/ko, han/ka lu/ze
Hor kon/pon e/ta al/de
i/he/si/ka be/ti pres/tu,
ez da Mac/ki/ren pa/re.

Mo/du o/nez fi/ni/tze/ko,
ur/ka/men/di/tik li/bre,
nes/ka guz/ti/en a/me/tsa
be/ra du/te de/si/o.

Mack ist Mack
gus ta in txa txa txa
Mack ist Mack
gus ta in txa txa txa

Mack ist Mack
gus ta in txa txa txa
Mack ist Mack
gus ta in txa txa txa

Mack ist Mack
gus ta in txa txa txa
Mack ist Mack
gus ta in txa txa txa

Mack ist Mack
gus ta in txa txa txa
Mack ist Mack
gus ta in txa txa txa