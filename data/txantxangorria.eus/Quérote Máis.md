---
id: tx-1083
izenburua: Quérote Máis
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gD6phVvqCUo
---

Canção: Quérote máis
Disco: A onda sonora (chega do interior)
Grupo: O Sonoro Maxín
Videoclipe: Escola de produção da Farixa (Ourense)

Nota: A canção é uma tradução do tema em euskara 'Maite zaitut', dos palhassos Pirritx eta Porrotx. Musica: Xabier Zabala




Letra:

Tenho sonhos de colores quando não podo dormir,
sigo esperto toda a noite e não deixo de bulir,
dando voltas no meu leito, tenho ganhas de voar,
sai-me o coração do peito, seica poderá estoupar.

Quando facemos o pino, a limpar no céu os pés,
ou botamos aturujos antes de que passe o trem,
quando che dou a pulseira feita com mil alecrins,
miro-te aos olhos de cote para poder-che assim dizer...

Quero-te mais, quero quero-te mais
que a tortilha de patacas com jamão,
quero-te mais, quero quero-te mais,
cada vez que me lateja o coração.

Tenho na boca um sorriso e nos olhos um candil
que ninguém pode apagar quando corre junto a mim,
o meu corpo treme inteiro, tremem pernas, tremem mãos,
sai-me o coração do peito, seica poderá estoupar.

Ao pintar arcos-da-velha de colores polo ar,
quando escreves o meu nome num espelho ou num cristal,
ao acendermos uma rádio este som poder ouvir,
miro-te aos olhos de cote para poder-che assim dizer...

Quero-te mais, quero quero-te mais
que a tortilha de patacas com jamão,
quero-te mais, quero quero-te mais,
cada vez que me lateja o coração.

Maite zaitut, maite maite zaitut,
que a tortilha de patacas com jamão,
maite zaitut, maite maite zaitut,
cada vez que me lateja o coração.