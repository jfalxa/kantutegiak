---
id: tx-2754
izenburua: Kax Kax Atean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NuAXPGcjld0
---

Kas, kas, kas atean
Nor dabil etxean?
Ikusi makusi
Nor naizen ikasi
Praka zabalak
Atorra urdina
Ai ene! ez dakit ezer ez, ez, ez
Ai ene! Kontatu mesedez.
Ai ene! ez dakit ezer ez, ez, ez
Ai ene! Konta/tu mesedez.
Kas, kas, kas atean
Kas, kas, kas atean
Nor dabil etxean?
Nor dabil etxean?
Ikusi makusi
ikusi makusi
Nor naizen ikasi
Nor naizen ikasi

Mutur zikina,
Mutur zikina,
Txapela beltza.
Txapela beltza.
Ai ene! ez dakit ezer ez, ez, ez
Ai e/e! Kontatu mesedez.
Ai ene! ez dakit ezer ez, ez, ez
Ai ene! Kontatu mesedez.

Kas, kas, kas a/te/an
Kas, kas, kas a/te/an
Nor da/bil e/txe/an?
Nor da/bil e/txe/an?
I/ku/si ma/ku/si
I/ku/si ma/ku/si
Nor nai/zen i/ka/si
Nor nai/zen i/ka/si


Pi/pa a/ho/an
Pi/pa a/ho/an
Ar/do/a ber/tan
Ar/do/a ber/tan
Bai, bai, bai, le/hen o/rain e/ta ge/ro
Zu za/ra gu/re O/len/tze/ro.

Bai, bai, bai, lehen orain eta gero
Zu zara gure Olentzero.