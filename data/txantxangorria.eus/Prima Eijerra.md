---
id: tx-2881
izenburua: Prima Eijerra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IVPvaRzoB28
---

Prima eijerra
zutan fidaturik,
hanitx bagira
oro tronpaturik!
Enia zirenez erradazu bai alez;
bestela
banua
desertiala, nigarrez urtzera.

Desertiala,
juan nahi bazira,
arren zuaza,
oi, bena berhala!
Etzitiala jin berritan nigana
bestela
gogua
doluturen zaitzu, amoros gaixua.

Nitan etsenplu
nahi dianak hartu,
ene malurrak
parerik ez baitu.
Xarmangarri bat nik bainian maitatu
fidatu
tronpatu!...
Sekula jagoiti ikhusi ezpanu!

Mintzo zirade
arrazu gabetarik
ezukala nik
zur'amodiorik;
zu beno lehenikbanian besterik:
maiterik
fidelik
hor eztereitzut egiten ogenik.