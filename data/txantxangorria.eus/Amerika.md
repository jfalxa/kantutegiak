---
id: tx-1961
izenburua: Amerika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nbAgkeimOOk
---

Mila bedederatziehun eta hogeita zazpi urte geure bezain krudela.
Abaurrea Gaineko kale hotzetan bilatu nahian suerte bertan.
Espartinak izotzezko basoetan kontrabando etengabetan.
Azal gazte hura zahartuz zihoan azkenengo lau urtetan.
Mixeriaren aurpegi beltz hura aldatu nahi eta ezinean,
Altzurukuko Dominik ezagutu nuen Holtzarteko﻿ baso lanetan.
Ameriketara alde egiteaz hitz egiten hasi zitzaidan eta
bidai berri hura gauzatu genuen Larraineko taberna batetan.
Entzun aita ama lagun on batekin Ameriketara noa.
Negarrik ez egin gaur alde egingo dut gauaren iluntasunean.
Gaur itsasoa, bihar mundu berri bat guretzako.
Amestutako bidai zoragarri hau hastear﻿ da.
Negar malkoak atzera begiratzen dugunetan.
Sorlekuari bertso batzuk idatzi ozeanoan abesteko.
Orbara ondoko larre berdeak gu gobernatzeko zai daude.
Itzuliko gara egunen batean osasunez baldin bagaude.
Besarkada eman lagun guztiei eta maite ditugula esan.
Gure bihotza beti han egongo﻿ da gure lur maitatu horretan.
Entzun aita ama lagun on batekin Ameriketara noa.
Negarrik ez egin gaur alde egingo dut gauaren iluntasunean.