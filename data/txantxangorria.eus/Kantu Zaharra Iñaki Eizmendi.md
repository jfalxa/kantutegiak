---
id: tx-3109
izenburua: Kantu Zaharra Iñaki Eizmendi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KzOl9CIVWxA
---

Nun zirade, ene maitea?
Oi xarmengarria,
ene penan doloren konsolatzailea.
Nik banu gaur zoria
zuri mintza iteko,
ene penan doloren zuri erraiteko.