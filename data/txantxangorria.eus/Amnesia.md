---
id: tx-1369
izenburua: Amnesia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jrti3bH4FsM
---

Musika: Hutsa
Letra: Maria Rivero

www.taupaka.com


Nahiago dut ahaztu
memoriatik ezabatu
inoiz gertatu ez balitz bezala.

Momentu horiek suntsitu
ikasitakoa desegin
amnesia aukeratu dut.