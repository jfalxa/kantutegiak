---
id: tx-831
izenburua: Txakoliña
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w20Dp8vas9Q
---

Xabi Solano Maizaren "BI" lan berrian aurkitu dezakegun Kupela taldearen "Txakolina" kantaren bertsioaren bideoklipa.

Videoclip de la versión de "Txakolina" de la banda Kupela, que encontramos en el ultimo trabajo de Xabi Solano Maiza; "BI".

Txakolina
Getaria daukagu sarri agurtua
Bateko hondartza ta besteko portua
Edari bat badute etxean sortua
Azken urtetan indar galanta hartua
Txakolina, txakolina…
Getariko erregina.

Txakolinak hartu du Elkanon postua
Askorekin baitauka harreman estua
Maite degu beraien doai berezkua
Parrileko arraia ta txakolin freskua
Txakolina, txakolina…
Getariko erregina.

Mendi aldea dauka mahastiz jantzia
Ta gaurko bodegetan zer elegantzia
Beregan urbiltzeko sortzen du “antzia”
Ez da beti erreza neurria hartzia
Txakolina, txakolina…
Getariko erregina.

Txakolina txotxetik tranposua, noski
Seigarren tragurako kantu ta abesti
Hanka sartu genuen zapata ta guzti
Freskatzera jo-an ta barruraino busti

Txakolina, txakolina…
Getariko erregina.

Jendea festarako zegoen jarria
Bakoitzak ase zuen bere egarria
Urtean batekoa ez da mingarria
Gora Getaria ta txakolin berria

Txakolina, txakolina…
Getariko erregina.