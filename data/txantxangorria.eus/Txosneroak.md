---
id: tx-137
izenburua: Txosneroak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MLSQZGOpBmM
---

Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Xelebreak gara; udetan, batez ere.
Sexua? Zer da hori?
Horretaz ezer ere ez.
Baten batek esan zuenaren legez,
Porrua eta salda elkartzea besterik ez.
Neska ikusi eta gelditu naiz kemenez
Lagunak arrautzarik ez dudala esanez
Begiratu eskuetara eta barrez,
Bonbila piztuta daramat larrosen ordez.
Horiek dira gure ideia xelebre
Euskal martxa eta jaia etengabe.
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Kuadrila daukagu Jainkoaren ordez,
Baina nahastu ezinak gara hala ere,
Batzuk sagarra jan, besteak lo aurrez
Ezin ligatu lagunen lagunik gabe.
Ikusi dut jantzita begi beltzez
Patxaranaz banoa erdi artez,
Errosarioa hitzen ordez
Etxera buelta duitasuna gordez.
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!
Euskal Herriko txosnak,
Kaleko giroa,
Gu garelako euskaldun txosneroak!