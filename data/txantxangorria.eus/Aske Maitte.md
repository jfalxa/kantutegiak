---
id: tx-1377
izenburua: Aske Maitte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yTn1FOqYe3o
---

KREDITUAK 


ROSS DANTZA ESKOLA
Dantzariak: 
Rosa Rementeria Del Villar, Zuria Bolaños Llano, Ainhoa Astuy Gabantxo, Ane Gerrikagoitia Zarraonaindia, Naroa Artetxe Urgoitia, Sara Iturriarte Basterretxea, Lexuri Gerrikabeitia Uribarri, Alaine Arruti Onaindia, Ane Alonso Irueta, Alaia Itoño Martin, Naroa Arandia Calzacorta, June Otxandio Basterretxea, Ismael Dieguez Morales, Idurre Onaindia Arteach, Bidatz Bengoa Badiola, Itxaso Jauregizar Muniozguren, Leire Etxeandia Solaguren, Maeva Morlesin Beares, Naia Ordorika Azkarate, Nadia Morlesin Beares, Maddi Gerrikabeitia Uribarri, Olaia Urionabarrenchea Olabarriaga, Asier Martinez Ostolaza, Josu Lopez Iraeta, Leire Gordo Badiola, Elena Jiménez, Nile Ansotegi, Ania Urrutia , Oihana Uribarrena, Maider Etxabe, Maialen Torre, Monika Zelaia Badiola, Alaia Ituño Martin, Soraya Garcia Totoricaguena, Maria Eguiarte Barraincua, Maitane Silva Gallo 
Ane Iturriarte Basterretxea, Amaia Aguirre Larrea, Kattalin Urrengoetxea Uribarri, Maitena Iturribarria Astorkia, Ane Sarduy Kanalaetxebarria, Eneko Amezketa Rodriguez, Olatz Barayazarra Garay, Oihane Lopez Bilbao, Alaitz Elorrieta, Laryssa Almeida


PARTE-HARTZE BEREZIA
Leire Gomez 
Izaro Andres 

Zuzendaritza:
Oier Plaza 
Koreografia: 
Rosa Rementeria Del Villar

Produkzioa: 
Gotzon Bareño
Janire Olabarriaga (Big sense mamma)
Eider Plaza 

Makeup eta Jantziak: 
Ania Cid

Edizioa:
Mikel Etxebarria 

Irudia eta soinua: 
Kerman Goikouria
Daniel Ojanguren
Mikel Etxebarria
Dani Asua
Ager Galarza

Arte zuzendaria: Jon Etxebarrieta 
Arte laguntzaileak: Iñigo Etxebarrieta, Juan Etxebarrieta, Andere Etxegaraia
Argiztapena: Felix Guede


Suziri bi baño ez gara gu
bata bestien aurrien.
Melodi bat entzuten da urrunien
hurbiltzen zara esanez:
gaba deike dekogu
ta bere sekretuek
gu bixontzat dire ie, ie, ie
gabien.

Suaren inguruen
gorputz bi bat eginda
bata bestearena
ze polittek garen
dantzan biluzik
ta izerrak.

Burue bero
begixek gorri
zu eta ni
gau txori bi
ez dago katerik.

Nahi dogunien
nahi dogulako
aske maitte
aske bizi
bizitza zoro hau.

A ze gozoa zaren
izerditan
ur gazi zaporie.
Eta ze ondo nagoen
izerran ganien
zugaz kantetan dodanien,
gabien.

Suaren inguruen
ixil-ixilik gaba,
itzal bi ikusten dire
Venuserako txartela
elkarregaz hartunde.

Burue bero
begixek gorri
zu eta ni
gau txori bi
ez dago katerik.

Nahi dogunien
nahi dogulako
aske maitte
aske bizi
bizitza zoro hau.