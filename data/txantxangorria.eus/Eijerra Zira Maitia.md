---
id: tx-844
izenburua: Eijerra Zira Maitia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D4ifdOLDFoM
---

Eijerra zira maitia, erraiten deizut egia ;
Nurk eraman ote deizü ... zure lehen floria ?



Eztizü egin izotzik, ez eta ere karunik

Ene lehen floriari ... kalte egin dienik.



Mendian eder iratze behi ederrak aretxe ;

Zü bezelako pollitetarik .. desir nüke bi seme



Horren maite banaizü obrak erakats itzatzü :

Elizala eraman eta ... han esposa nezazü.



Apezak dira Españian bereterrak Erruman,

Hurak hanti jin artino, gitian txosta kanberan



Aita dizüt hil berri, amak eztizü urt’erdi ;

Zurekilan libertitzeko ... dolüa dizüt barnegi.



Oihaneko otsua dolü dereiat, gaxua,

Antxuekin lo egin eta .. zeren barurik beihua



Enün ez ni otsua gutiago gaxua,

Antxuekin lo’ginik ere kuntent gabez beinua



Bortü goretan lanhape, ur bazterretan ahate ;

Zü bezalako falsietarik, eztizüt nahi deusere