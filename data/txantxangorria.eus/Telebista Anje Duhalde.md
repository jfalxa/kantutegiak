---
id: tx-2858
izenburua: Telebista Anje Duhalde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aC4smhNjnhU
---

Hitzak: Daniel Landart / Musika: Anje Duhalde.
Gure lekukotasuna (Elkar, KD-027) diskotik hartua.
Grabaketa urtea: 1977.

Gaurko mendeak eskaini digu
gauza bat zinez polita,
polita eta goxoa eta
ozena bezain galanta,
zuri begira pasatzen dugu
zenbait eta zenbait bulta,
gure etxean sartzen baituzu
arras maite dugun besta.(Bis)

Agur, beraz, zuri,
agur, telebista!
Tanti ruri ruri, tanti ruri rura.
Agur, telebista,
profeten profeta!

Dena kolore, lilura, kantu,
ministroak usu mintzo,
bai eta ere jaun presidenta
su ondoan kokoriko.
Horiek denak oso umilki
heldu dira egunero.
Gauza hoberik ezin zen asma
euskaldunen frantsesteko.

Agur, beraz, zuri,
agur telebista!
Tanti ruri ruri, tanti ruri rura.
Agur, telebista,
gezurtari adreta!

Munduko berri hara-hunatak,
kirola eta besteak,
handi-handien faltsukeriaz
dirade bete-beteak.
Jaun aberatsak baitira egun
telebistaren jabeak,
jabeak eta populuaren
lokararazle trebeak.

Agur, beraz, zuri,
agur, telebista!
Tanti ruri ruri, tanti ruri rura.
Agur, telebista,
gu guzien, gu guzien, gu guzien
izorratzaile galanta!
Telebista, telebista