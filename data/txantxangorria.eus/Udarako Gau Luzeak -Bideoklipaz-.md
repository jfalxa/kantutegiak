---
id: tx-1809
izenburua: Udarako Gau Luzeak -Bideoklipaz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZhD5gyaeuiQ
---

Ta zure eskutik kilometroak egin 
Herriaren begietan ospatzerik ikusi 
Gose direnen izenean 
Batikanoa erortzen ikusi 

Ta zure eskutik debekaturikoa egin 
Tiroka dabiltzan haurrak jolasetan ikusi 
Preso direnen izenean 
Harresiak sutan ikusi 

Ta zuretzat 
Udarako gau luzeak 
Abesti honen akordeak 

Ta zuretzat 
Euskal Herriko gailurrak 
Zutik gaudenaren seinaleak 

Ta zure eskutik beldurrik gabe bizi 
Aldaketa bat munduan hil aurretik ikusi 
Herri zapalduen izenean 
Buruak erortzen ikusi 

Ta zure eskutik bidean aurrera egin 
Negarrez bizi direnen irribarrea ikusi 
Zeruko hodei grisen artean 
Argi izpi berriak ikusi 

Ta zuretzat 
Udarako gau luzeak 
Abesti honen akordeak 

Ez utzi malkorik ezpainetara iristen 
Irri hori lausotzen duen zauririk zabaltzen