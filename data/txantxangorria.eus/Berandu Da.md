---
id: tx-564
izenburua: Berandu Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YwIWCQnDg1E
---

THE LIO
(Nafarroa)


2020ko irailean Dani Skstren eskutik grabatu eta editatutako bideoklipa.
Abestia Iruñako Sound of Sirens estudioan grabatuta Julen Urzaizen eskutik.

Videoclip grabado y editado por Dani Skts en septiembre de 2020.
La canción fue grabada y masterizada en el estudio Sound of Sirens en Iruña de la mano de Julen Urzaiz.


Igaro da jada denbora ezagutu ginen gau hartatik
Ni gazteegia nintzen baina ez zitzaizun axola zuri
Ez nintzela inoiz maiteminduko zin egin nion nire buruari
Hori gauza erraza zirudien nork esango zidan neri

ETA ORAIN…
KONTURATU NAIZELA ZURE BEHARRA DUDALA
TA EZ ZAITUDALA ASKATU NAHI
ONARTZEAN…
ZURE OINETARA NAGOLA IRRIBARRE ETA POZAK
ZURI ZOR DIZKIZUDALA

Ikusten dut betiko jendea haien kontuekin doaz aurrera
Aurrera doa familia eta aurrea doaz lagunak
Igaro da jada denbora ezagutu ginen gau hartatik
Baina guk ez dugu aurrera egin atzera egin dugu berriz

ETA ORAIN…
KONTURATU NAIZELA ZULO BATEN NAGOLA
ZU EZ ZAUDEN BAKOITZEAN
ONARTZEAN…
DENA GEZURRA ZELA IRRIBARRE TA POZAK
PARANOIA HUTSA ZIRELA

Nun dago orain betiko jendea nun da familia nun lagunak
Nirea zena ostu didazu baina dirau nortasunak
Badakit ez naizela zure gizona ta zu inondik ez nere neska
Beti bai nabil arazoz josirik ta zu zara erruduna

ETA ORAIN…
KONTURATU NAIZELA AMETS GAIZTO BAT ZARELA
TA EZ ZAITUDALA GEHIAGO IKUSI NAHI
ONARTZEAN…
ENGAINATU NAUZULA ORAIN EZ ZAITUT MAITE
BAINA JADA BERANDU DA