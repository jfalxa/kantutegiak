---
id: tx-1065
izenburua: Gauero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WOEdJs2NfUY
---

Egunabarra da bikaina
Txoriak kantatzen goizero
Eguna polita da baina
Gure bizia da gauero.
Maitagarri zaude goizian
Esaten didazu egunero
Sinisten dut maitea baina
Maitasuna dator gauero.
Ederki nahi badezu pasa
Zatoz neska polita gero
Emango didazu laztana
Nik zuri emango dizut bero.
Kontuz ibili zu nirekin
Hau egiten dut egunero
Zu/re/kin gaur bi/har zei/ne/kin
Ni/re bi/zi/a da ga/ue/ro.
Gu/re bi/zi/a da ga/ue/ro
Gu/re bi/zi/a da ga/ue/ro
Gau/txo/ri/ak be/za/la gu ga/ra
Za/toz gu/re/kin dan/tza e/gi/te/ra
Gu/re bi/zi/a da ga/ue/ro
Gu/re bi/zi/a da ga/ue/ro
I/lar/gi/a be/za/la he/men gau/de
Mai/ta/su/na e/ma/ten di/gu/zu/e
Gu/re bi/zi/a da ga/ue/ro
Gu/re bi/zi/a da ga/ue/ro
Za/toz ga/ue/ro mai/ta/ga/rri/a
Za/toz ga/ue/ro be/ti be/za/la
Za/toz ga/ue/ro gu/re/kin nes/ka
Za/toz ga/ue/ro dan/tza e/gi/te/ra
Zatoz gauero zatoz maitea
Zatoz gauero ederki pasa
Zatoz gauero gure bizia
Zatoz gauero gure bizia