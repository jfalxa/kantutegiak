---
id: tx-2820
izenburua: Igaran Setemez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UW2PStsQ3EY
---

Igaran setemez, aste nabar batez, bakantzaz nintzan goxoki
Emazteareki, sei kunateki, plazer hartü düt deneki.

Emazteak üsü, güntüan itsasoan, botzik pikaraitü ondoan
so egileak, haien miratzen, senditzen beitzütüen saldoan.

So egileak, itsas bazterrean, badirade bai saldoka;
mostra oroetarik, eder eta txarrik, lodi beita begi bazka;

Gizon üzkü mehe eta sabel handi, zanko meheak biloz fundi,
emaztik türtülüz, gantzak aspiltürik, martxandiza nasai lodi.

Zonbat titi handi eta azpi lodi, ipurdia harrigarri;
ur bazterrean, txalapartaz ari, arrain ttipien lotsazgarri.

Bertset egilea, etxe zain egon da, ez da ureko jendea,
hargatik izan da, itsaz bazterrila, oi! begien bazkatzera.