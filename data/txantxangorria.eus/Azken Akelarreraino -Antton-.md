---
id: tx-3156
izenburua: Azken Akelarreraino -Antton-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8B_fmTn6AKM
---

Ikusten zaitudanean irrifarra begietan Larranotik arranora doazen geltokietan sinisten dut harresirik ez dagoela bihotzean, askeagoak garela besarkada bakoitzean.

Sentitzen zaitudanean irri-malkoen hezean arnasaren borrokan ta borrokaren arnasean                                                                  sinisten dut sorginkeriz sor ditzakeela herriak zapatero zaharren partez abarketero berriak.

Ez dago amets ederragorik amets egitea baino itxi begiak ta goazen hegan azken akelarreraino.

Usaintzen dizudanean herri honen izerdia masailean gora doa zure arrazoi erdia. Sinisten dut ez garela korbatekin urkatukoizan nahi genuen hura ez dugula ukatuko.

Entzuten zaitudanean eskutitzak irakurtzen,birikak bizitzaz betez kilometroak laburtzen, sinisten dut maitasunak zure aurpegia duela denbora ez pasa arren zain egoten dakiela.

Ez dago amets ederragorik amets egitea baino itxi begiak ta goazen hegan azken akelarreraino.