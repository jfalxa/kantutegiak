---
id: tx-2342
izenburua: Ia Xoragarria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mWCQ4f8GBjI
---

Ene herrian paseatzea 
ia xoragarria da,
festa honetan batez ere....
Ene herrian paseatzea 
ia xoragarria da,
festa egunetan batez ere...
Plazako kioskoan nahi duzu 
egunkarian erosi,
kontuz, ez dezala inork 
zein hartzen duzun ikusi!!
Eta Patxirekin hasi ez gero 
kalea hizketan,
pekatu, anatema, 
ez da gutarra sekula izan...
Behin jantzi nintzen ausarta 
kolore arrosaz,
lagun batek segituan 
tratatu niduen maxaz,
ta geroxeago 
flamenko diska bat eskatzean,
rock radikala erosteko 
ez zidaten ba esan?
Ba al da ordea 
herri ia xoragarri honetan,
kalean ibili eta ia erabat 
nazkatzen ez denikan,
txartelak ere badaude, 
biba gu ta gutarrrak
bada ni etxera noa, 
niri ez eman tabarra!!!