---
id: tx-1439
izenburua: Lürralde Zilarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U_xsTDTDv4U
---

Ametsik gabeko gaü hotz lüzetan
Zure itzalean galtzen düt izarra.
Badakit haidürü zütüdala nonbait
Ene bihotzeko lürralde zilarra.
Ametsik gabeko gaü hotz lüzetan
Zure auheretan non dago iparra
Badakit bakarrik zütüdala nonbait
Ene bihotzeko lürralde zilarra.

Ardo minberatik edaten düt orai
Erreka ondoan histü da lizarra
Mahasti zaharrak elkirik direla
Ene bihotzeko lürralde zilarra.
Ardo minberatik edaten düt orai
Lehen bezala gaür asmatüz biharra
Txerkatüko zütüt kürütxatüko gira
Ene bihotzeko lürralde zilarra.

Arbelezko hegatzen herriak
Maiatzeko lantzer ixiletan,
Begirada maitekor berriak
Bilatzen dütüt xenda hiletan.

Esküak zendako dütüt erabiltzen
Harrien atetzeko badizüt indarra.
Egoitz altxatzeko ta eraikitzeko
Ene bihotzeko lürralde zilarra.
Esküak zendako dütüt erabiltzen
Zure nigarraren eztizko pindarra
Idortzen hasteko larrüak ürratzez
Ene bihotzeko lürralde zilarra.