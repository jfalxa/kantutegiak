---
id: tx-676
izenburua: Mendigoixaliarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yQTSbzgovj0
---

Mendi eze, ikurrin eder,
aske nahi zaitut axian.
Amar gazteren lerdena
makila luzez bidian

Mendi-bitxidor berdiok,
arin or duaz kantari:
"Dana emon biyar yako
maite den askatasunari".

Gaztedi honen didarra
bai dala didar zolija!
Aberri baten samiñez
urduri dabil erija.

Askatasun-goxalderuntz
sugarra dira basuak.
Sugarra basuak eta
zidar argija itxasuak.

Or duan ozte-aldrea
aberri-miñez kantari!:
"Dana emon biar yako
maite den azkatasunari".

Gazte horreik goruntz duaz
abesti ta ikurriñez.
Lañuan baña tiro hotsak:
bedartza dager odolez.

Ikaraz duaz usuak,
mendija dago ixillean.
Hamar gazteren lerdena
bixitza barik lurrean!

Eta illuntzeko bakian
norbaitek darrai kantari:
"Dana emon biar yako
maite dan azkatasunari"!