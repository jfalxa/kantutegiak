---
id: tx-2633
izenburua: Herritxo Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UzJrThcP3NM
---

Herritxo txiki bat
txindoki babesean
hantxe bizi gera
poz-pozik gure artean
lara, lara, la....

Nahiz txikia izan
gure txoko maitea
ez nuke salduko
urre guztin trukean
lara, lara, la...

Amorrain ugari
baditu ur garbian
baita txori pranko
goiko kanpantorrean
lara, lara, la...

Hain mendi politak
ditu aldamenean
nahi badezu ikusi
guazen nahi dezunean
lara, lara, la...

Eta nahi badezu
mondeju txuriak jan