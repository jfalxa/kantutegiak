---
id: tx-3054
izenburua: Ez Gera Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tt6wM3VvD4Q
---

Ez gera bat, asko gera
pozean eta oinazean
sinistean, pentsatzean
ez gera bat, asko gera
ez gera bat, asko gera

Ez gera bat, zati gera
berek nahi duten bezela
bidean eta helburuan
ez gera bat, asko gera
ez gera bat, asko gera

Zati gera eta ez gera
ezin litekeen bezela
zati izan ta irabazi
esku arteko burruka 
esku arteko burruka

Zati gera, zazpi gera
ez gera zazpi ez gera bat
ehun gera, mila gera
ta nahi dugu ta ez gera
ta nahi dugu ta ez gera

Ta bagoaz ta ezin dugu
otsoak koskatzen gaitu
gezurrak gaitu zatitzen
herri baten gau luzean
herri baten gau luzean

Mintzaira bat, burruka bat
ezin ditugu balia
etsaiak zatitzen gaitu  
berak dakien bezela
berak dakien bezela

Bihar goizez esnatzean
oihu latz bat entzungo da
abots batek esango du
ta ez gera hilko gera
danok batzen ez bagera

Ez gera gu ez gera bat
ehun gera, mila gera
ez gera gu, ez gera bat
ehun gera, mila gera
ehun gera, mila gera
ehun gera, mila gera