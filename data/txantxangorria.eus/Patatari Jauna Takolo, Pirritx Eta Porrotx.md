---
id: tx-3230
izenburua: Patatari Jauna Takolo, Pirritx Eta Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OYZ3lz-QsZ4
---

Patatari jauna 
lapiko artean daukazu lana
Bibote handi-handia 
txano borobila
Patatari jaunak
goxoa prestatzen digu dena
gose-gose gara eta
bagoaz zuregana

Zartaginan du tortila
buelta ematen abila
Indarrez du bota gora
baina lehiotik kanpora

Patatari jauna...

Albondigak egin ditu
eta ezin erdibitu
Goazen denok ze aukera
ping ponera jolastera

Patatari jauna...