---
id: tx-2065
izenburua: Txanton Piperri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h5N4nyzekj4
---

Euskal herriko semeak gera
zor dioguna bizia.
Beraren alde emango degu
zainetako odol guztia (Bi).

Bere lege ta hizkuntza ederrari
diogu maitasun guztia.
Hil arte oihu egingo degu
bizi bedi Euskalherria (Bi).