---
id: tx-3140
izenburua: Athleticen Erreserkia -Beobide / Bernaola-  Abao
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d3jwpDU20oU
---

Athletic, Athletic, Athletic eup! 
Athletic, gorri ta zuria 
danontzat zara zu geuria 
Erritik sortu ziñalako 
maite zaitu erriak

Gaztedi gorri-zuria
zelai orlegian
Euskalerriaren erakusgarria.
Zabaldu daigun guztiok
Irrintzi alaia:
Athletic, Athletic
zu zara nagusia
Altza Gaztiak

Athletic, Athletic,
gogoaren Indarra.
Aritz zarraren enborrak
loratu dau orbel barria.

Aupa mutilak!
aurrera gure gaztiak!
Bilbo ta Bizkai guztia
goratu bedi munduan
Aupa mutilak!
gora beti Euskalerria!
Athletic gorri-zuria
geuria.

Bilbo ta Bizkai guztiak gora!
Euskaldun zintzoak aurrera!