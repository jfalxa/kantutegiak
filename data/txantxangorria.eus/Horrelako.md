---
id: tx-2211
izenburua: Horrelako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oozKJ35pZ6k
---

Zerue beltz dago eta ni bustitze
eta ezin dut sinestu, zelako bendije
datorren itxosotik
horrelako olatu batek hilko nau
traji zikindu bidot, hobeto
tabli soltaten badot
bueltaka norabidea galduta
zerue topatu nahian, hau bai izan dala
kriston marejadie

Itxosu kolpeka eta ni barruen
eta ezin dut sinestu, zelako olatuk
datozen itxosotik
horrelako olatu batek hilko nau
traji zikindu bidot, hobeto
tabli soltaten badot
bueltaka norabidea galduta
zerue topatu nahian, hau bai izan dala
kriston marejadie

horrelako olatu batek hilko nau
traji zikindu bidot, hobeto
arnasi hartzen badot
bueltaka norabidea galduta
zerue topatu nahian, hau bai izan dala
kriston marejadie