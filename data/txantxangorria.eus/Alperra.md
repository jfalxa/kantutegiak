---
id: tx-374
izenburua: Alperra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IMlt4COn8U0
---

1,2,3,4

Astelehen goiza da ta
ohean geldirik
ez naute mugituko 
orain hemendik
Jo dute hamabiak
batzuen kalterako
lana ez baita niretzako
Ohetik jeiki eta 
ondo gosaldu
kalera irteten da
tabernan sartuz
goiza pasatzeko
hartu egunkaria
hau bizitza zoragarria

Alperra naiz, laralalalalalala (bis)

Arratsaldean dena 
musalditxoa 
keinu ta irrifarre
galtzen nijoa
nire ezkerrekoa 
hor dago labur-labur
tragoa oraindik daukagu
mus amaitu ondoren
hurrengokoa
telebistan jokatzen
nire ekipoa
nere ekipoa
partidua irabazi
apustuan ni naiz nagusi

Alperra naiz, laralalalalalala (bis)


Batzuek bizi dira
lanez beterik
ohetik lanera eta
ohera lanetik
astea luzea eta
asteburua labur
horrek dit niri
dirutzat lagun
Ohean egoten naiz
oso ederki maiz
gorrotatzen dut lana
hau duk nire lana
ongi bizitzeko
ez dut behar besterik
nik ez dut emango kolperik

Alperra naiz, laralalalalalala (bis)
laralalalalalala (bis)