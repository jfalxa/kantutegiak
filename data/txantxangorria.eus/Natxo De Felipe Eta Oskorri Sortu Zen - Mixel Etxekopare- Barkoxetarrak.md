---
id: tx-2519
izenburua: Natxo De Felipe Eta Oskorri Sortu Zen - Mixel Etxekopare- Barkoxetarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pQsPMzDiGdM
---

Natxo de Felipe eta Oskorri sortu zen, 2018-02-10an Barkoxeko Hotel Chilon izaniko emanaldia. Natxok Mixelen eskutik Oskorri nola sortu zen kontatatzen du: Izena nola aukeratu, berak nola ikasi zuen euskara, taldea nola osatu zen, Antton, Bixente,... Arestirekin izaniko harremana eta sorkuntza, 70 hamarkada eta hurrengoetan izandako boikotak, bortxikeriak eta abarrekoak, abesti batzuen istorioak,...