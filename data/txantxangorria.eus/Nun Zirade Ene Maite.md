---
id: tx-3386
izenburua: Nun Zirade Ene Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/joYxZtOdVgM
---

Victoria Eugenia Antzokia.
Donostia - Sn.Sn.. 14.10.1994.
Kantuz bizi. EITB - Euskal Telebista.

Iñaki Eizmendi - Ahotsa
Karlos Jiménez - Pianoa
Mitxel Ducau - Kitarra elektrikoa


¿Nun zirade ene maite, oi xarmagarria,
ene penan doloren konsolatzailea?
Nik banu gaur zoria zuri mintzaiteko,
ene penan doloren zuri erraiteko!"