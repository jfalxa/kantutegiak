---
id: tx-712
izenburua: Zure Doinua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4w7aVTxED7k
---

Euri asko egin du neska
Lehenengo laztanetik
Amaigabeko gau sutsuak
Ezin kendu burutik
Alkoholak eraman zituen
Lotsaren azken tantak
Ze zoriontsu izan garen
Denbora guzti hontan
Zure irribarre goxo hura
Non galdu ote da
Ez al zara konturaatu
Gurea hustu dela
Zein gogorra egiten zaidan bukaera onartzea
Zu zoriontsu izatia
Nere azken eskaria
Taupada luzeak
Izan dira maitia
Baina orain nere kantuak
Ez du zure doinua
Jakin inor ez bezala
Maite izan…