---
id: tx-2933
izenburua: Sorginen Leizean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KP0U_pvH8KU
---

Jo ezazue txalaparta
albokaren doinuan bat,
kanpoan gauerdi da ta
izarrak gaienean dira.
Lamiek ondo dakite,
noiz den ilargi bete.

Aker batzuk dabiltza
suaren inguruan
sorgin bat dantzan dabil
kantuka, oihuka.

Sorginen leizean
neska bat dantzari
lizun, biluzik
neska bata dantzari
sorginen leizean...

Astiro, astiro
oihuak bukatzen doaz.
Astiro, astiro
sugarrak hiltzen doaz.
Gauak ere ondo daki
noiz datorkion goiza.

Sorginen leizean...