---
id: tx-1151
izenburua: Muxurik Muxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TWkXDPInACU
---

Usaindu zaitzaket arnas eta usaindu 
hemen zaude nire senak esnatuz 
bihurri aurpegi horrek salatzen zaitu 
eskutik oratu hegan egin dezagun 

Arropak erantzi muxurik muxu 
Biluzik gaudela laztandu nazazu 
Zure ahotik ihesi doazen garrasien jabea nauzu 
maite zaitut. 

Zure ahoa nire lepora lotu 
nire atzazalak zure bizkarra mindu 
gertu zaude, gertu nago, elkarren artean gertu 
eskutik oratu hegan egin dezagun. 

Arropak erantzi muxurik muxu 
Biluzik gaudela laztandu nazazu 
Zure ahotik ihesi doazen garrasien jabea nauzu 
maite zaitut. 

Zure garrasien jabea nauzu. 
Nire garrasien jabea zaitut. 

Arropak erantzi muxurik muxu 
Biluzik gaudela laztandu nazazu 
Zure ahotik ihesi doazen garrasien jabea nauzu 
maite zaitut. 

Arropak erantzi muxurik muxu 
Biluzik gaudela laztandu nazazu 
Zure ahotik ihesi doazen garrasien jabea nauzu 
maite zaitut.