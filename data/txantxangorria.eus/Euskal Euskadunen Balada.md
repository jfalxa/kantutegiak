---
id: tx-1954
izenburua: Euskal Euskadunen Balada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bo5_35aM6HM
---

Ikusten duzu karrikan
Euskara doa korrikan
Ez nabil ni bakarrikan
Gure kultura ondoa dago
Haritz nobleko barrikan
Bero naiz ni negu gorritan
Hizkuntzak ez du harrikan
Oskorrik ez du gorrikan
Ez eman guri harrikan
Entzuten duzu alboka
Hantxe naiz ni ukondora
Enziklopediak erosten ditut
Binaka eta plazoka
Gustatzen zait letra azopa
Erdarari erasoka
Aizkora zuzen jasota
Apurtuko dut nik soka
 
Ez da beltza bai nabarra
Ile luzeko kalparra
Mingaina elebakarra
Euskaraz elkar maitatzen dugu
Bera eme ni arra
Gozo kentzen dit sukarra
Kopetan tente adarra
Esnetan gainez aparra
Gora nire behi naparra