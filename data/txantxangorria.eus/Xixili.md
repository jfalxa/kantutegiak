---
id: tx-485
izenburua: Xixili
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l0tx_421NIo
---

Zenbat marinel hondatu ditu 
zure hots laidogarriak? 
Zure adats-izpietan jitoan zeudenean. 

Harpa bailitzan jotzen zenituen 
Zure orraziaren hortzak 
eta Perbertsioen zuloan horditzen ziren denak. 

Oi Xixili! 

Noraezean zaude, Izaroko harkaitzetan 
aurkitu dut zure gorpua 
Eta kutxa beltz batean 
sartu nahi ditut zure laztan eta musuak 
Eta zure lurrin goxoa.

Atsoen ipuinak omen ziren Bermioko balkoietan, 
poetaren sareetan dekadentziarik aratzena. 

Jada ez ditu inork argitzen zure oroitarria
bakarturik zaude, Poseidonen zain eta zain.

Oi Xixili! 

Noraezean zaude, Izaroko harkaitzetan 
aurkitu dut zure gorpua 
Eta kutxa beltz batean 
sartu nahi ditut zure laztan eta musuak 
Eta zure lurrin goxoa.