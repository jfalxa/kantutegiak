---
id: tx-765
izenburua: Bidaia Handiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yAf5x5UUCC4
---

BIDAIA HANDIAK (Xabier Amuriza Sarrionandia)
Itzali dira argiak
ixtera doaz begiak  
orain abiatzen dira
nire bidaia handiak.
Izan naiz iraganean
gazteago nintzenean
uste dut kasik hobeto
dagola oroimenean.
Haragoko haurtzaroa
oraindik ez igaroa
nire baitan dator bera
pixka bat handiagoa.
Izan nahi nuen geroan
gaurdanik bertara joan
iristear nintzenean
juxtu erori naiz loan.
Iritsi naiz ametsera
azken bidaia da bera
topo egiten badugu
musu bat eta aurrera.