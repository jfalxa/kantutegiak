---
id: tx-1716
izenburua: Altzateko Jaun Xabier Lete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eub0DxZrrcs
---

Altzateko jaun, hor zaude nunbait 
zure lurrera itzultzeko zai. 
Gure_izatearen testigu zuzena 
makurtu gabeko zuhaitz lerdena; 
desertuko haizeak 
legortu ez zuen lorea, 
itxasoaren kolorea; 
hor zaude zu. 
 
Hamasei Salbe ta_hamalau Kredo 
erderaz entzunagatik, 
bazenekizun hori ez zela 
sekulan guretzat egin. 
 
Urrezko dukat eta dirua 
ez dira engainu txarrak 
bazter batera apartatzeko 
lehengo sinismen zaharrak. 
 
Merkatarien aldamenera, 
amildegian goitik behera 
oharkabean eroriko gera. 
Baina guztiok jakin dezagun 
bidea nundik dihoan: 
Altzateko jaun, 
hor zaude zu! 
 
Bide batetik okertu gabe 
esan dezagun bertsua 

ixil-ixilik kanta dezagun 
euskaldunen sekretua: 
Herri zahar baten oharpen haiek 
nola ditugun galduak 
zenbat itxura alda lezazkeen 
eguna hil zuen gauak. 
 
Bidasoaren ezker-eskubi 
udaberrian lore gorriak 
ixilikako ur izkutua; 
mendetan zehar kantu leguna, 
noizbait latinez lurperatua 
laister berpiztu behar duguna. 
 
Hamasei Salbe... 
 
Larun gainean sorginen dantzak 
naba(r)itzen ditut arratsaldean; 
lurrak badaki kontu ho(r)ien berri; 
eta hor dago, ixil-ixilik. 
Altzateko jaun...