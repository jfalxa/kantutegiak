---
id: tx-1008
izenburua: Pixkabatesmucho
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VodBGpYDhGY
---

Uda honetan 
zurekin estrenatuko naiz

Hotel txiki batean,
zure etxean edo nirekin.

Zurekin, Karlarekin, edo edozein girirekin
mundu guztiarekin praktikatu nahi dut
astronauta, fisikari, perretxikolari,
Groseko nudista, errotari, matahari,
punky, yanki, ebribadi, igual dio!

Anima zaitez laguntxo,
ezpainetan euskara
guazen pixkabatesmucho!

Uda honetan bai
pixkabatesmucho!

Hurbildu zaitez mutil
pixkanaka hasiko gara
ta gero guztiekin 
disfrutatu nahi dut pixkabat!

Anima zaitez laguntxo,
ezpainetan euskara
guazen pixkabatesmucho!

Uda honetan bai
pixkabatesmucho!

Jazz dantzari, abeslari edo segalari
idazkari, pelotari, sartu la directa!
Sukaldari, gidoilari eta rastafari
burguer saltzailearekin, zuzendari
chekepare, andra mari, igual dio!

Anima zaitez laguntxo,
ezpainetan euskara
guazen pixkabatesmucho!

Uda honetan bai
pixkabatesmucho!

Kritikari ta muslari, ta txirrindulari
argentinoarekin, "el que dice boludo!"
Joxe Mari, kazetari patata ikerlari
sagarra manzana, ikatza carbón
pixka bat es mucho eta asko mogollón!

Anima zaitez laguntxo,
ezpainetan euskara
guazen pixkabatesmucho!

Uda honetan bai
pixkabatesmucho!