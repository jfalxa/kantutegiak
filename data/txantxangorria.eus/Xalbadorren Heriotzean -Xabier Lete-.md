---
id: tx-3268
izenburua: Xalbadorren Heriotzean -Xabier Lete-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pa6KJr69Y1s
---

Xalbador izenez Ferdinand Aire Etxart, euskal idazle eta bertsolaria zen. Xalbadorren omenez Xabier Letek egin zuen abesti eder eta ezaguna Urepelko artzainari dedikatutakoa.Erramun Martikorena kanta beronen bertsioa ere oso famatua da.

Xalbadorren heriotzean / Erramun Martikorena igo/monter/subir
Hitzak eta Musika: Xabier Lete
“Erramun Martikorena: Olerkarien oihartzun” 
diskotik hartua (Elkar KD-341 / 1993)

Adiskide bat bazen orotan bihotz behera,
poesiaren hegoek sentimentuzko bertsoek 
antzaldatzen zutena.
Plazetako kantari bakardadez josia,
hitzen lihoa iruten,
bere barnean irauten oinazez ikasia.

Nun hago zer larretan 
Urepeleko artzaina,
mendi hegaletan gora,
orhoitzapen den gerora,
ihesetan joan hintzana.

Hesia urraturik libratu huen kanta,
lotura guztietarik gorputzaren mugetarik 
aske sentittu nahirik,
azken hatsa huela bertsorik sakonena
nehoiz esan ezin diren 
estalitako egien oihurik bortitzena.

 Xalbadorren heriotzean / Dans le décès Xalbador

Il y avait un ami, être profond et sensible
tranfiguré par les ailes de la poésie,
par les vers surgis d’un profond sentiment intérieur,
un chanteur qui parcourait les places, transi de solitude,
qui avait appris avec douleur
à tisser des mots et à s’exprimer avec réserve,
à partir de l’incorruptible vérité de son être intérieur.

Où es-tu aujourd’hui, dans quels pâturages,
berger d’Urepel,
toi qui as fui
vers les hautes cimes,
vers les lendemains qui demeurent dans le souvenir...

Tu as líbéré ta chanson en démolissant les barrières,
cherchant avec ardeur la liberté
au-delà des attaches et des limites du corps,
transformant ainsi ton dernier soupir dans le vers le plus
profond
en un cri retentissant

Où es-tu aujourd’hui, en quels pâturages...


Xalbadorren heriotzean / En la muerte de Xalbador

Había un amigo entrañable y sensible
transfigurado por las alas de la poesía,
por los versos surgidos de un profundo sentimiento,
un cantor que iba por las plazas aterido de soledad,
que había aprendido con dolor
a tejer palabras y a expresarse contenidamente
desde la insobornable verdad de su ser interior.

Dónde estás hoy, en qué praderas
pastor de Urepel,
tú que huiste
hacia las altas cumbres,
hacia el mañana que perdura en el recuerdo...

Liberaste tu canción demoliendo el cerco,
buscando la libertad
más allá de las ataduras y los límites de tu cuerpo,
convirtiendo tu último aliento en el verso más profundo,
en el grito contundente
de las verdades ocultas que jamás se pueden expresar.

Dónde estás hoy, en qué praderas...