---
id: tx-2975
izenburua: Folklore
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uE0BI5iTPf0
---

Etxepare zenaren aldarri berbera
dakargu egunera
bost mende atzera

Hargatik agian aurreiritziak
zuretzat gaur eta beti
folklore baikara

Ta zeresan gehiegi ez al dugu ematen
ez existitzeko?
Zaildu gara halabeharrez
gai gara zure hesiak gainditzeko

Begiak itxi arren
hemen bertan gaude
gu isil gaitzakezu
gure kantuak ez

Bitxikeria uste zenuena beste zerbait da
sailkatu ezina

Ta zeresan gehiegi ez al dugu ematen
ez existitzeko?
Zaildu gara halabeharrez
gai gara zure hesiak gainditzeko

John Merrick bezala ikusiak
begira begietan

Hortzak kendu nahi zenizkigun baina hala ere
bestela ikasi genuen kosk egiten
Zeinek behar du froga gehiago?
zeinek behar ote ditu?

Ta zeresan gehiegi ez al dugu ematen
ez existitzeko?
Zaildu gara halabeharrez
gai gara zure hesiak gainditzeko