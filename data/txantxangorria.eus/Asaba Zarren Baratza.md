---
id: tx-935
izenburua: Asaba Zarren Baratza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wXkpNn6BRDg
---

Asaba zarren baratza,
untza dun ormek esia,
eguzkiak len maitea ...
otzaldi zoro baten itzuli
yoka nagokik atea.

Yoka nagokik atea,
ate gorri, zurezkoa,
euri zarrek usteldua,
marraskiloek bide biurri
dirdaitsuek apaindua.

Bai-bainun amona xar bat
zazpi begiko baratzai,
deadarkari yayoa
marrubi-lapur nenkusanetan ...
bizi ote dut gaixoa?

Belar gaiztoak yan ditu
nire bidexka izkutuak ...
ale bakanak dakartzi
bide gañeko mastiak eta
ortziak peitu du iguzki.

Urte yoanen zitala!
zuek zarpildu itzalia
lenera nork lekardake?
Zabal-naroa nuen baratza
gaur ain estu, gaur ain gabe!

Baratz erdian dut arki
amona: alderoka dator,
begi galduak nora-nai ...
Eriotzak dakar besa-lagun,
bere buruz ezpaita gai.

Errukarri! Lausorik du
gizargia begietan ...
Non-nai aldiaren sitsa!
Zuka, zoroki, mintza zitzaidan,
zaldun arrotza bai nintza.

Laztan dut. Bera arrituta,
begi-begira dagokit.
Gero geldiro: "Nor zaitut?"
Euskeraz ark niri galdegiña
goguan besterik ez dut!

Ta asi zan biraka berriz,
il-zarrei oska negarrez,
eriotzaren egarri ...
Bizitzearen ondar-aleak
larraiñean eultzen ari!

Ta, ara, bidean, baldarño,
mutiko bat guregana:
ark birbilloba, nik seme.
Ikus amonak, bertan gelditu,
ta aren eztizko irriparre!

Lena zegoen orañaz
ele gozoka, bitzuok
mintzo-beraz ziarduten.
Ene asaben lokarri zarra
yaunari zor etzan eten!

Ordun eguzkiz yantzi zan
asaba zarren baratza,
ordun zuaitzak igaliz,
ordun baratza leneratu zan
mugak berriro zabaliz.

Ta len ikusi-gabeko
zuaitz berri bat zegoan
erdian, guzien buru ...
aren gerizak erri-baratzak
ezin-ilkor egin ditu.

Nire Tabor-mendi, nire
baratz zarraren antzalda!
egi, mami, biur adi!
Lenaren muñak aldatu beza
baratz zarra baratz berri!

(J.M. Agirre "Lizardi" / A. Valverde)