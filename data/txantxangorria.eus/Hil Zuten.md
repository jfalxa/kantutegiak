---
id: tx-1451
izenburua: Hil Zuten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZldcOp2K8yE
---

Tiro
Tirokatuta hil zuten Iñigo
beraien helburua balitz bezala
istripu bat zela esatea
Tiro
Gezurrak jaurti zituzten astiro
beraien helburua balitz bezala
ahaztea eta isiltzea
Hil zuten Iñigo Cabacas, hil zuten
Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen
Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen
Tiro
ezkutatzen ari dira berriro
beraien helburua balitz bezala
Zalantzarekin mina sortzea
Tiro
Justizia oihukatzen dugun aldiro
Gure helburua bakarra delako
Iñigo Cabacas oroitzea!
Hil zuten Iñigo Cabacas, hil zuten
Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen

Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen

Hil zuten Iñigo Cabacas, hil zuten
Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen

Hil zuten ez zen hil, hil zuten
Eta beti oroituko dugu nola hil zuten
Hil zuten ez zen hil, hil zuten
Eta errugabea zen