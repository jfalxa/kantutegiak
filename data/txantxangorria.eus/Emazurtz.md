---
id: tx-841
izenburua: Emazurtz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9M4qHr44hgk
---

Emazurtz jaio hintzen  
Lurraren erdira,  

pobreen esperantzak  

benetan zer dira?  

Ama urrikariak  

hala zinan erran,  

esperantza guztiak  

hil zirela gerran. 



Nerea Aresti

  

Baina inoren zorrik 

Ez dun ordainduko, 

Eure etorkizunak 

Ditun apainduko.

Sufritua giauden 

hamaika gosete, 

ez genian sabela 

alimentuz bete; 

  

beti gau eta egun 

gogorrik lanean 

Jornal benetan pobre 

Baten afanean



Ezkondu hintzenean 

bost gona hituen, 

bi bahituan jarri 

bi saldu hintuen 

soinean dunan hori 

diagon zaharra 

berriak erosteko 

esperantza txarra



Haurtxo bat sabelean 

Beste bat besoan, 

Senarra erbestean, 

Artoa hauzoan 

Hiru hilendi kentsu 

Suaren ondoan 

Beste argirik ez dun 

Hiretzat munduan.



Hi haiz euskal herria, 

Herri nekatua. 

Inork ezagutzen du 

Hire bekatua? 

Baina inoren zorrik 

ez dun ordainduko, 

heure etorkizunak 

ditun apainduko