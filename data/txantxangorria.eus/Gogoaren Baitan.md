---
id: tx-2789
izenburua: Gogoaren Baitan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cMgZXaE18gI
---

Erabaki nuen, tinko ta fermuki,
Es zitzaidala gertatuko,
Maitasun saretan es nintzela berriz
Uste gabean eroriko.

Bi begi izpitxu, irripar ezti bat
Izan dira nere galtzaile,
Geroztik hor nabil arantzetan dantzan,
Ezinezko amets sortzaile.

Urrezko txanpon bat eman gogo nuke
Sorgin bat bilaka banedi,
Ezina egikor aldatzeko aina
Gezurra bihurtzeko egi.

Zer den ona edo zer ote den txarra
Jakin beharko nukelarik
Sumendi gainean erretzen ari naiz
Ba al dago irtenbiderik ?

Zu hor zaude, dantzan, jauzitan,
Zer debru bada pasatzen zait gogoaren baitan.(bis)

Geroak zer dakar, ez dut pentsatu nahi,
Uneak nahi ditut bizi.
Lau egun dira ta bi omen lanotsu
Galtzera ez ditut nahi utzi.

Hitzaren jokoan, itxura emanez,
Gorde nahiko nuke dena.
Biluzia nauzu izan zarelako,
Egoera hunen sormena.