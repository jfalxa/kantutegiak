---
id: tx-121
izenburua: Galdu Ez Baduzu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J433rcbS9UU
---

Mina gordinegi irentsiz
pasa izan ditut
egunak zu ez zaudenetik.
Ez naiz ohitu, faltan zaitut.

Ahulez jantzi naute berriz.
Dena bortitzegi.
Denbora galtze huts honetan
askatu naiz, ya berdin zait,
kezka barik, biluzten hasita naiz.

Zertarako pausu bat atzera?
Denen ahotan zauz beti zu.
Zu gabe ez zegoela ezer aukeran,
sekula ez nukeela ezer lortuko.

AHAZTU ORAIN ARTE ENTZUN DUZUNA,
ZE HAU DA ZINEZ DANTZATU NAHI DUZUNA.
MOZTU DARAMAZUN HARROTASUNA,
GARAIZ ZABILTZA, GALDU EZ BADUZU
ZURE NORTASUNA.

Irriz beti ikusten banauzu,
zer egingo dut nik?
Tokatu zait bizitzea ekaitza barrutik.
Zein erreza den dantzatzea nahiz eta
euriak busti ni.

Eta nola aukeratu ez banitu(e)n aukerak,
ihesi ibiltzeko hustu behar duzu irteera.
Alde egin zenu(e)n, baina nik hemen irmo
jarraitzen dut.

AHAZTU ORAIN ARTE ENTZUN DUZUNA,
ZE HAU DA ZINEZ DANTZATU NAHI DUZUNA.
MOZTU DARAMAZUN HARROTASUNA,
GARAIZ ZABILTZA, GALDU EZ BADUZU
ZURE NORTASUNA.

Denek esaten zidaten
damutuko nintzela ni ez maitatzearren.
Eta orain ez dut besterik egiten,
ni ez izateak izorratuko ninduke.

AHAZTU ORAIN ARTE ENTZUN DUZUNA,
ZE HAU DA ZINEZ DANTZATU NAHI DUZUNA.
MOZTU DARAMAZUN HARROTASUNA,
GARAIZ ZABILTZA, GALDU EZ BADUZU
ZURE NORTASUNA.