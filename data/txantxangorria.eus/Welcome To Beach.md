---
id: tx-775
izenburua: Welcome To Beach
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ATvVLuPqWy0
---

Itsasoa eta errepidearen
artean dagoen
lur zatian bizi naiz ni
itsasoa, lur zatia ez da gutxi.

Hondarra ondare haitza harri bitxi
itsasgorak jantzi
ta itsasbeherak erantzi
horrela da ez du inork erabaki.

Ezer eskatu gabe eskeini didazu
gauero ohea
mendi bularrak heze ur ezpainak gazi
nere maitalea.

Ilargiz ilargi, basati ta birjin
salneurririk gabe
bizitzeko ederregi
zarelako ezin zara libre bizi.

Erosi zaituzte erreza izan da
egin dezatela orain beren propaganda
yateak, golfa ta ipuin haundi hori.

Eta ezpainak margotu ta dotore jantzi
ta izan barea
ta erabakiko dute noiz igo ta jeitsi
behar den marea.

Ezer eskatu gabe eskeini didazu
gauero ohea
mendi bularrak heze, ur ezpainak gazi
nere maitalea.