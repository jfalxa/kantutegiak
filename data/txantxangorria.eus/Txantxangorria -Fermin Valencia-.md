---
id: tx-3146
izenburua: Txantxangorria -Fermin Valencia-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F4uIvi_-LF4
---

Txantxangorria txantxate
Biligarrua alkate
Zizaren buztan baten gandikan
Preso ba damate

Txantxangorria egia
Biligarrua dirua
Zizaren buztan baten gandikan
Preso ba damate

Lara lala lara lala...

Txantxangorria txantxate
Biligarrua alkate
Zizaren buztan baten gandikan
Preso ba damate

Txantxangorria gaixua
Biligarrua gogorra
Zizaren buztan batengatikan
Preso ba damate

Lara lala lara lala...

Txantxangorria txantxate
Biligarrua alkate
Zizaren buztan baten gandikan
Preso ba damate

Txantxangorria gorria
Biligarrua zuria
Zizaren buztan baten gandikan
Preso ba damate

Lara lala lara lala...

Txantxangorria langile
capitalista alkate
Zizaren buztan baten gandikan
Preso ba damate

Txantxangorria langile
capitalista alkate
Zizaren buztan baten gandikan
Preso ba damate

Lara lala lara lala...