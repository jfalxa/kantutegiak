---
id: tx-2593
izenburua: Trena Dator
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UO_FN8wZcPo
---

a, e, i, o, u!

Mari Mototsek a, a dio!
Mari Mototsek a, a dio!

Ez du entzuten gure Sebek
belarria helduta beti e, e, e!

Trena dator, trenbidetik,
tximinitik kea dario i, i, i!

Oh, oh, oh,
globoa joan zaio!

Bide hortatik joateko
gure nehitxoak adarrez dio u, u, u!