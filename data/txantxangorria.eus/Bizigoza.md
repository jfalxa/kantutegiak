---
id: tx-996
izenburua: Bizigoza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yowFdQV7Bpg
---

MUSIKARIAK: Julen Alonso, Eneritz Aulestia, Joanes Ederra, Ander Herrera eta Iker Telleria.

DANTZARIAK: Julen Badiola, Irati Aranguren eta Alain Sorazu.

LETRA: Iñigo Galdos.

PRODUKZIOA: Julen Alonso.

BIDEOA: Kepa Gonzalez, Gurutze Susperregi eta Iker Gonzalez.




Pentsatzen jarriko bagina
“gero zer izango da?”
ez zaigu berriz iñoiz itzuliko
pasatzen utzitako denbora.
Orainetik pasatu gabe
ezin da joan inora
eguneroko ametsak gozatu
eta ez begiratu gerora, gerora!

Bizitzaren trenak
bakarra du bidaia
ekaitzez ta eguzkiz
osatuz paisaia
Bizitzaren trenak
bakarra du bidaia
heldu aulkia gogor
ta gozatu bizia!

Hegan egin, aske sentitu
zerurantz bidaiatu
nahi duzunarekin egin ezazu
jolastu, maitatu ta gozatu.
Geltokiero zoriona
zure trenera batu
bete gabeko ametsik ez utzi
eta irri batekin esnatu, esnatu!

“Bizitzaren trenak...”

#saveyourinternet