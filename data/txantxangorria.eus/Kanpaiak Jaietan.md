---
id: tx-1606
izenburua: Kanpaiak Jaietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C0IweXbSaDU
---

Kanpaiak jaietan, dindan dindan tintintan.
Izarrak argitan gabon gauetan.
Mara mara elurra, txuri txuri gailurrak.
Jainkoa jaio da, poztu da lurra.

Kanta bat entzun da gauez mendialdean.
Zorion! Zorion! guztioi Gabon
Belengo estalpean sehaskatxo batean
lo dago Haurtxoa gure Jainkoa.