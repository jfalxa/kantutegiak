---
id: tx-3277
izenburua: Ez Ez Dut Nahi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hXYd5uiJEVs
---

Ez, ez dut nahi, ez, ez, ez, 
ez holako zibilizaziorik.

Kaiolaren adarrak ditugu alboan 
Eskuindarrek eskua zabaldu juxtuan. 
Zigor indarren menpe tiroak gogoan 
horrela bizitzerik ez dago munduan.

Ez, ez dut nahi, ez, ...

Poliziak nunahirik zerbait egitean: 
pentsatu arazi nahi makil indarpean. 
Irudi du gerala kaiola batean, 
jarritako txoriak fusilen menpean.

Ez, ez dut nahi, ez, ...

Horregatik ez dut nahi gizonen kutsurik, 
zibilizazioaren sasi Jaungoikorik. 
Libre nahi dut bihotzez libre loturetik, 
basurdearen gisa hortzak estuturik.

Ez, ez dut nahi, ez, ...