---
id: tx-984
izenburua: Esklabu Eta Jabea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p_AidDJdkDo
---

ESKLABUA ETA JABEA

Grabatuta eta masterizatuta: Estudios-K (Alberto Porres)
Bideoklipa: SKST Design ( Dani)


Letra:

Egun berri bat berriz hastera doa
betiko itzal ilunaren alboan
Nekaturik eta indargabeturik
norbaiten edo zerbaiten esanak berregin

Gauero galdetzen diot zeruari
zergatik hau ta nola ihes egin
Nire bizitzak ez dauka preziorik
aske izan nahi dut eta ez besterik, ez besterik

Hau da XXI. mendeko bizitza 
esklabua ta jabearen ipuina 
gaur egungo egoera, berdina
Nor edo zer da ni zapaltzen nauena
nire helmuga aldentzen duena 
burua altxatzen uzten, ez nauena

Ez! ez dut gehiago jasan nahi
Zer! kapitalaren indarra
Zuek! lortutako garaipenaren ondarra

Eta! makina lehertzera doa
Ez! giza eskubideak ez daude salgai
Langileen klasea bidean jarrai

Hau da xxi.mendeko bizitza 
zure amets gaizto bilakatuko dena
zure beldur handiena 
Eta nor edo zer da ni zapaltzen nauena
nire helmuga aldentzen duena
burua altxatzen uzten ez nauena