---
id: tx-2135
izenburua: Ni Hiltzen Naizenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cGuMikqy_Bk
---

70eko hamarkadaren erdialdetik aurrera garatutako euskal folk berriaren talderik esanguratsuenetakoa. Ibilbide laburrekoa baina itzal luzekoa gertatu zen. Plazaratutako bi diskoetan, Izukaitz (Xoxoa, 1978) eta Otsoa dantzan (Xoxoa, 1980, Ingalaterrako folk-rock garaikidearen eragin nabarmeneko euskal herri musikaren ikuspegi berritzailea azaleratu zuen. Gaur egun, bildumazaleentzat harribitxi preziatu bilakatu dira bi diskoak. Taldekideen artean, Bixente Martinez (Oskorri, Hiru Truku), Fran Lasuen (Oskorri, Txatanuga Futz Band, Eguen Banda) eta Luis Camino (21 Japonesas).