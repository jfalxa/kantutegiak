---
id: tx-3344
izenburua: Bihotza Alex Sardui
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wTcQt1dkvCU
---

SDB2012
Sortuko dira besteak

Bilbo - Arriaga antzokia
2012.12.01

Kantaria:
Alex Sardui (Gatibu)

Abestia:
Bihotza - Xabier Lete

Musikariak:
Xanet Arozena - Gitarra
Oriol Flores - Bateria
Txema Garcés - Baxua
Miren Zeberio - Biolina
Luis Fernandez - Txeloa
Joxan Goikoetxea - Akordeoia

Soinu teknikaria - Joxean Ezeiza

ETB Euskal Telebista - 2012
AM Kultur promotra



Munduak gezurra diotsu, bihotza.
Gezurra bere argi indarrez,
negarretan miña iñork ez du aditzen.

Dadukazun miña ixilik negar zazu
munduak haundituko du ta,
iñork ere lagun etzaitu egingo,
iñork erre ez esan.

Zoriak behin zutaz,
parre egin nahi baleza,
eragotz, eragotzi eiozu,
barruko penak hil,
ezpainetan parrea kanpora,
ipini ezazu.

Jainko gorde hoiek,
etzaituztet gurtzen nik,
zuek ere hilkorrak zerate,
eta ezerezera, ziurki zoazte,
ezer ez bai zerate.