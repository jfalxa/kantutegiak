---
id: tx-1740
izenburua: Askatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8h_zUMgf54M
---

Agian
egunen batean
geronen herrian
libre geranian.

Apika ezin errepika
zer da politika
herriko etika.

Indarrak batuta
denak batera
gure etxean
nagusi gera
benetan
alkartzen bagera.

Garaipen garbi bat 
dugu espero
duda izpirik
ez egin gero
benetan
bat egin ezkero.

Akaso
hainbeste frakaso
goazen pausoz-pauso
animoak jaso.

Presoak
heuren gurasoak
heuren erasoak
geure arazoak.