---
id: tx-2281
izenburua: Zubiak Euskal Herria Corsica
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ayW1bOs4LV4
---

Hitzak:
Betidanik heben, Korsikan bezala, jenteek katera gaitz
Mentüraz, han bezala, beitügü gük ere, barneko esperantxa
Pitzik baratzen deno, orano bizi dirate mil' urtetako üsantxak
Gazna hoberenak ez dira txestatzen, eznerik ez balinbada

OOO oooo oooooooooooooo.......
Eüskaldünik eüskararik gabe, da zelüa izar gabe
Korsikarra mintzajerik gabe, da marinel ontzirik gabe
(Eüskaldunak eta korsikarrak betikoz dira biziren
Jüntatzen gitaien zübüa kantorez dügü eginen)

Sakoletan eta kanpoan ez dügüna, badügü bihotzean
Aberastarzünak ez dü lekü orotan, etxenko ber zentzüa 
Kantore eder bat indartzen ahal da, barnetik jiten delarik
Ützi akodinak ützi arrenkürak, eta gurekin kanta

Aspalditik heben indarkan ari dira gure isilarazteko
Bena ez gira ez isiltüren, orozbat botza galdü artino
Irrintzin eder bat hürrün ezagün da, hatsik baratzen deno
Ez gira hatsalbo, esperantxa hor da, indarren handitzeko