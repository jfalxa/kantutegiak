---
id: tx-86
izenburua: Barnetik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vNpesxrA_jc
---

Prod by @Jammybeatz 
Rec & mix by @DensoProductions 
Video by @desertoproductions 


Letra:

Ezin sendatu nire bihotzeko zauriak
Ezin aldatu begi urdin honein argia
Pentsamendu ilun bat aspaldi  buruan
Pertsona asko ta bala gutxi munduan

Besarkada gabeko 
Bizitza baten preso
Burua ia galtzeko 
Ezer zuri emateko
Nana ezer zuri emateko
Nana ezer zuri emateko


Argia ikusi dut bestaldean 
Iluntasuna iada utzi dut atzean
Haizeak janzten nau egunero
Gauetan ilargiak ni pizteko

Pauso bakoitzeko milaka histori
Ezkina danetan ikusi gara zu ta ni
Ur putzuetan saltoka txikitan
Txakurrak atzetik eta gu korrikan


Gaueko tragoak eztarria urratuta
Nire bamba txuriak guztiz zikinduta
Lokalekoekin beti gaua luzatuz
Argia ikusi arte ez goaz etxera gu


Etxepeko musika bolumena goraino
Urrun helduko gara emaiguzu denbora txo
Barnean daukagun altxorra zuei bideraziz
Entzuten gaituzuen guztioi eskerrak bakarrik

Besarkada gabeko 
Bizitza baten preso
Burua ia galtzeko 
Ezer zuri emateko
Nana ezer zuri emateko
Nana ezer zuri emateko

Doinu bat mundu bat 
Etzenuke ulertuko
Begiraden jolasak 
Ez naui inoiz aspertuko 
Saia zaitez asmatzen 
Nik ez dut antzematen
Giltza galdu dut baina biok ebatu ta guazen
Biok ebatu ta guazen
Biok ebatu ta guazen
Biok ebatu ta guazen
Goazen behingoz