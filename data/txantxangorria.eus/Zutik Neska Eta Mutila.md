---
id: tx-218
izenburua: Zutik Neska Eta Mutila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/USynGNWgqPo
---

Zutik neska ta mutilak nator anaitasun bila
Zutik neska ta mutilak nator anaitasun bila
Itxaropenen sabaltzaile da gabonetako ezkila
Datorren udaberria bai, zuentzat izan dadila
zuentzat izan dadila

Gurea den aberria, kantuan dute karia
Gurea den aberria, kantuan dute karia
Kanta dezagun euskara garbiz, 
Zuzentasun egarria
Beti Aurrera joan dadilen 
Zuen zai dagon herria
Zuen zai dagno herria

Beharrezkoa bai zauzun herrimina duzu entzun
Beharrezkoa bai zauzun herrimina duzu entzun
Kanpora bitez zulo hontatik, hainbat indar ta maitasun
Zapalkuntzarik izan dadilen askatasuna erantzun
Askatasuna erantzun
Kanpora bitez zulo hontatik, hainbat indar ta maitasun
Zapalkuntzarik izan dadilen askatasuna erantzun
Askatasuna erantzun