---
id: tx-2640
izenburua: Greziako Itsasontzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yzj2uGTnYFI
---

Itsaso gaindik ikusten baduzu
Gure gogoa daraman ontzia
Ez zazu pentsa galduta doanik
Argitzen baitu goizeko eguzkia

Hainbeste amets negar ta desio
Ez du ekaitzak ondora botako
Salbatuko da goizeko ontzia
Kaiaren bila hegaka joateko

Arraun berriek eramango dute
Bide zaharretik askatasunera
Oihal zuriek haizea bildurik
Gure arnasa biltzen den batera

Ez du ekaitzak ondora botako
Salbatuko da goizeko ontzia