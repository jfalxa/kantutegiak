---
id: tx-534
izenburua: Gailurretatik Gora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N1pwgWeH0QY
---

GAUTAR nire bigarren albumaren kantetako bat duzue GAILURRETATIK GORA. Wild Mountain Thyme abesti irlandar folklorikoaren melodiaz baliaturik, euskal musikari desberdinen artean mimatutako kanta da hau. Gure etxeaz, paisaiaz eta hauenganako maitasunaz hitz egiten duen abestia. Partehartzaileak: Fredi Peláez, Gorka Sarriegi, Karlos Gimenez, Petti eta Tuti.

Hemen abestiaren letra:


GAILURRETATIK GORA (Wild Mountain Thyme)

Udaren iritsiera, zuhaitzak loratzen daude.
Gure mendi basatien larre eta basoetan.

Gailurretatik gora
Sorginen jaioterrira
Elurra urrun joanen da
Euskaldunon sorterrian
Gailurretatik gora

Iturburu, ur ederrak, harribitxizko iturri.
Bertan kontatuko dizut nere sekretuen berri.

Gailurretatik gora
Sorginen jaioterrira
Elurra urrun joanen da
Euskaldunon sorterrian
Gailurretatik gora

Ezinezkoa bada, agur esango diogu.
Ta nik idatziko dittut mila abesti inguru.

Gailurretatik gora
Sorginen jaioterrira
Elurra urrun joanen da
Euskaldunon sorterrian
Gailurretatik gora