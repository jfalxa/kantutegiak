---
id: tx-243
izenburua: Barrun Barrukuk
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x1gxsu6dIsU
---

Feat. Izar Aranbarri Laka


BARRUN BARRUKUK
Barrun barrukuk, barrun
Etxe aurreko adreiluak danak ezagun
Itxi lehioak, zarratu begiak
Eta esan zer ikusten duzun lagun

Giltzapean ateak itxita
barrurago ihes egin dugu
Hemen ez da beldurrik
Ez da paretik hemen
Hemen barrun

Barro-barrun.

Dena aldatuko duen
Giltzaren bila
Kanpora ez behatu
Denborarik ez galdu
Hor duzu barrun

Barro-barrun

Topako nauzu barro-barrun
Ezkutatuko gara barro-barrun
Topako zaitut barro-barrun
Barrez-barrez, barro-barrun

Kartzelak urtu dira
Balkoi-barrak ixildu
Kateak hautsi dira
Barrua aldatu, kanpoa irauli

Barro-barrutik

Aitzaki gutxiago
Beldurrei gatza emon
Ez naiz hainbeste kexako
Inork ezer egiteko zain ez egon
EKINTZA

Barrua aldatu
Kalea irauli/ kanpoa irauli

Topako nauzu barro-barrun
Ezkutatuko gara barro-barrun
Topako zaitut barro-barrun
Barrez-barrez, barro-barrun

Josu Aranbarri Aramaio