---
id: tx-2843
izenburua: Iratze Okre Geldiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8aXs4a2bT-g
---

Maitegoak udazkenean
zuhaitzen hosto gorriak,
horitasunez apainduriko
makal zuzenen gerriak,
hego haizetan lokartutako
iratze okre geldiak.

Maitegoak, azken mugetan,
iraganaren zantzuak,
lore xumeen petako eme
ur izkutuen kantuak,
euri epelen gozotasuna.
gaueko zeru altuak.

Maitegoak sua ta jaia
fruitu berrien haziak,
uda beteko musiketarik
ernaldutako antsiak,
Eros ausartak gorputz gaztetan
ixuritako graziak.

Maitegoak behin joan zirenak
sekulan ez itzultzeko,
begiek zabal dirauten arte
argitan blai itzaltzeko,
sufritu duen gizonak ez du
lotsarik behar galtzeko.