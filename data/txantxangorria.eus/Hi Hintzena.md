---
id: tx-1832
izenburua: Hi Hintzena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YISPmcUz9do
---

Hi hintzena
bi arrasto zaharren gurutzaketa
galdu da, galdu haiz
hautsi da anphora
eta mila ispilutan multiplikatua
ez haiz
azken irudi ezabatua baino
ilunaz zipriztinatuz bekokia
eta hausterrez
laberintoaren harri txintxarretan
zauritzen hire oin birjinak