---
id: tx-2108
izenburua: Xoxo Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5TMcvh5X5Ro
---

Xoxo beltz bat banuen
kaiolan sartua.
Egun batez neguen
hil zen gixajua
Lurpian sartu nuen
zelaian xokuan.
Eguna argitzen deni(a)n
xolu hartan, xolu hartan
xoxuen kantua.