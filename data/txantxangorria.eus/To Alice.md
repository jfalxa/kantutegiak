---
id: tx-1773
izenburua: To Alice
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D8kiD1QjqrM
---

Ikaro laberintoan al haiz Alice?
gogoa herdoildu al zain? Alice itxoin

Saharako eremura marilynen ileak ereintzera
itzuli al da joan zen espedizioa?

kristaleko paradisuan loretzen dira
galderak soilik?

itxoin Alice, ispiluaren gure aldean
norbaitek piztu du ixiltasuna...
...denbora ihesean doa Alice...

horiek al dira bihotza konjeladorean dutenak?
estazioetako itxarongelan dagoen jendea
zeren zai dago?
zure begiek zer dakite
erantzunen hilerriz?
zigarroen keaz zer idatzi duzu
are urdinetan?
ene bada azkenaldean
txuribeltzean amesten dut...
...memoriaren zein aldetan
babesten duzu adokin azalezko
kaleetako jolasak?