---
id: tx-2905
izenburua: Erratza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/igOyiDs5fso
---

Ile punkiak puntan dituen makila
garbitasunerako erabilgarria
Etxea ta kalea erraztatzeko
bazter zikin guztiak txukun-txukuntzeko

Erratza, erratza astindu hautsa
Erratza, erratza astindu mototsa
Erratza, erratza astindu hautsa
Erratza, erratza astindu mototsa

Esku artean gitarra glinglanglinglanglinglon
Rokero petoa naiz glinglanglinglanglinglon
Zango artean zaldia lasterka badoa
saltoka irrintzika, hanka jokoan

Erratza, erratza...

Erratz hegalarian piruliruliru
hegorik gabe hegan piruliruliru
Santa Ageda bezperan ari naiz kantuan
aiai kaskatzekoa aiai behatz punta

Erratza, erratza...
Erratza, erratza...