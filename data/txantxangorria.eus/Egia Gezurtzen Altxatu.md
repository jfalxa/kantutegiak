---
id: tx-1616
izenburua: Egia Gezurtzen Altxatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ezSknUKkZm4
---

Denek dute ogia eta bakea
ta zu sinisten duzu, 
munduan gosez hiltzen dira, 
barkatu hori ez da horrela, 
haurrak gerrara ez dira joaten, 
ta zu sinisten duzu, 
gerrak goikoen interesak dira
egia gezurtzen ari dira! uooooo!!
euskaldunak terroristak gara, 
ta zu sinisten duzu, 
politikariek ez gaituzte maite, 
barkatu hori ez da horrela, 
errege berriak dena aldatuko du, 
ta zu sinisten duzu, 
egia gezurtzen ari dira! uooooooo