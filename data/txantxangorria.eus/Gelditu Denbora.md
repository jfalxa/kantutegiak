---
id: tx-804
izenburua: Gelditu Denbora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BCShsQy4MT0
---

Ameslarien azala soinean
Aurkitu naiz bide gurutzean
Aukera zuzena okerra denean
Barruko galderen erantzuna
Liburuetan agertzen ez dena
Galdu ezin diren tren bakarrak
Erlojurik gabeko bidai honetan
Norabide berriak burua
Gelditu denbora
(E)ta itzali mamuak
Minutu guztiak nire izenean
Gelditu denbora
(E)ta piztu argia
Ongi etorria bide berrira
Biluztasunak orri zurieta
Egunero aukera berriak
Idatzi nahi nuke nire liburua
Istorioak lehenengo pertsonan
Erraztasunak, bide erosoak
Eramaten bazaituzte…