---
id: tx-2272
izenburua: Hegaldaka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p3cN8lquZhI
---

Zuen nostalgiaz ez nintzen ohartua
Zuentzat hain laster nintzela handitua
Ene egunerokoa zait aldatua
Mapa gabe nahi dut kurritu mundua.

Ez noa ihesi, ta nahi dut berriz jin
Bainan bada garaia airatu nadin.

Hazi nauen ohantze goxotik orai
Hegaldatzeak ez du ihesa erran nahi
Ene lumaz nahi dut bizi pobre eta alai
Eroriz izanen bainaiz xutitzeko gai.

Ez noa ihesi, ta nahi dut berriz jin
Bainan bada garaia airatu nadin.

Zuek eman beroarekin umetan
Badakit mundua epeltzeko lanetan
Den dena besarkatu nahi dudanetan
Leku bat badudala zuen besoetan.

Ez egon ene goaitan, behar dut egal egin
Zuek eman hazia loratu dadin.

Ez egon ene goaitan, behar dut egal egin
Zu/ek e/man ha/zi/a lo/ra/tu da/din.