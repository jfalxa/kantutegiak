---
id: tx-2500
izenburua: Señorita Bat Akelarre Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SnW95Vsy1ek
---

Kobanen lehen abestietako baten bertsio akustikoa egiteko gogoa sartu eta gure lagun Adrian Sernarekin - Zerrat (bideo grabaketa eta edizioan) eta Beñat Antxustegirekin (soinua) honako hau egin genuen. "Señorita bat akelarre batean", bertsio akustikoan.



Deskapotablea hartuta joan ginen mendira,
Padeleko lagun batzuk ta ni haize freskoaren bila.
Teniseko zapata beltzak, bisera txuria, 
krema 50 eta Ama Birjiñan argazkia sujetadorian.

Ezkerrean marra txuri-gorri, eskuinen ekixa.
Esan nien itxoiteko, egin behar nuela sastraketan pixa.
Eskuinekotik jun eta, hara! Danbor hotsak!
Urrunean kobazulo bat, hara noa. Zerekin topa?

SUAREN DARDARAN MILA GORPUTZ DANTZAN,
IRRINTZI OIHUAK GIZON ADARDUN BATI ALABANTZAN;
SEÑORITA BAT AKELARRE BATIAN.

Sartu eta berehala eskutik helduta.
Bularrak agerian zituen ta ni zeharo izututa.
Gainontzekoengana eraman arropai tiraka,
Esan zidaten goza momentuaz, ez kezka, senti musika!

SUAREN DARDARAN...

Kaosa ta festa, izerdi patsetan, burua biraka, aldakak balantzan, 
Ezpain mamitsuak, akerraren hatsa, hipnosia martxan,
BAGA BIGA HIGA!
Kaosa ta festa izerdi patsetan, burua biraka, aldakak balantzan, 
Ezpain mamitsuak, akerraren hatsa, hipnosia martxan,
ARMA TIRO PUM!

Osea, nun sartu zara ordubete hontan? 
Guk dagoeneko atera dugu selfia mendi puntan.
Deskapotablera itzuli ta oraindik shockean, 
Ama Birjiñan argazkia hartu eta bota nun airean!

SUAREN DARDARAN…