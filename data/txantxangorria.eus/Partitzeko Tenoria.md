---
id: tx-153
izenburua: Partitzeko Tenoria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HpHeV1us43o
---

Partitzeko tenorea
Orai düzü ene maitea
Partitzeko tenorea
Hasperena bihotzean
Eta nigarra begian
Tristüratan bizi behar düt
Ene denbora güzian
Ene denbora güzian.
Ez zütüt nahi kitatü
Sobera zütüt maitatü
Lili eijerra so egidazüt
Ene bihotza zurea düzü
Eta zurea enea düzü!