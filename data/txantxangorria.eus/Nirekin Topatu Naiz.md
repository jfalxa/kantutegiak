---
id: tx-839
izenburua: Nirekin Topatu Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vuDsNamhbO0
---

Hitzak: Pello Reparaz

Musika: Pello Reparaz eta Gorka Pastor🔥



Neoiezko argia piztu da

Katu beltza kalexkan barna

Nire bila



Inguruan dena siluetak

Eta lurrean mila kristal

Zolak zulatuaz



Ta zu tabernetan etzaitut aurkitu

Mozkor, komuneko atean idatzi dut:

“Aizu, geroak aldatu egin gaitu”



Non zaude zu? Non dira denak?

Non garai bateko promesak:

On brûlera tous les deux



Bero dago dena edo ni naiz?

Berriro bakarrik ohean

Dena nigan, nigan



Ta zu tabernetan etzaitut aurkitu

Mozkor, komuneko atean idatzi dut:

“Aizu, geroak aldatu egin gaitu”

Betirako…

Eta ni nirekin topatu naiz orduan

Dena dardara zerutik infernura

Jakin nahiago dudala bestela



Busti-bustiak

Oroitzapenak hemen baizintudan

Busti-bustiak nigan



Busti-bustiak

Oroitzapenak hemen baizintudan

Busti-bustiak nigan



Ta zu tabernetan etzaitut aurkitu

Mozkor, komuneko atean idatzi dut:

“Aizu, geroak aldatu egin gaitu”

Betirako…

Eta ni nirekin topatu naiz orduan

Dena dardara zerutik infernura

Jakin nahiago dudala bestela