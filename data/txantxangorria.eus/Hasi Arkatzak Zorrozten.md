---
id: tx-2419
izenburua: Hasi Arkatzak Zorrozten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B9e-PJ0MD6w
---

HASI ARKATZAK ZORROZTEN
Ikasturtea hasi berri dugu, eta honekin batera Ikasleok Zorrotzeko abesti eta bideoklipa aurkezten dizkizuegu. Ikasturtea indartsu hasteko prest gaude ikasleok!


---------------------------------------SAREAK---------------------------------------

Ikasle altxa, arkatza zorroztu
Inposaketei bidea moztu
Ikasle borroka gurea bidea
Emango diegu, fire kea

Hezkuntza herritik eta herriarentzat
Heziberri jarri ziguten aitzakitzat
Ikasleok nahi zaituztegu hemendik at
Indarrak batuz izango gara bat

Egin PLANTO errre azterketa
Guztiok batera lortu aldaketa
Apurtzera goaz haien eskema
Eraikiko dugu gure sistema

Hezkuntza Euskaldun feminista
Euskal herriak sistema burujabea
Ikasle, apurtu zure katea
Hori da gaur gure bidea

Ei!
Azterketa arrotz
Erantzungo diegu ikasleok zorrotz

Kapitalak lo nahi gaitu, ikasleria
Sartu nahi digute haien ziria
Antolatu auzoa eta hiria
Ez gara haien artaldeko ardia

Aldaketa beharra dugu egun
Batzeko garaia dugu lagun
Ikasleok izateko ahotsdun
"Gora ikasleon borroka" oihuka dezagun

KLASE BATEK GORDETZEN DUEN FRUSTRAZIOA
AZTERKETA ERRETZEAREN AKZIOA
NAHI DUTE KLASEA IZATEA GURE ETXEA BAINA BIHURTZEN DUTE ESPETXEA

JAIOTZEA
ARNASTEA
IBILTZEA
IKASTEA
ESTRESATZEA
GAINDITZEA
EZKONTZEA
UGALTZEA
EKOIZTEA
KONTSUMITZEA
IKASLEONA: IZATEA