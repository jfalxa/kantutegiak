---
id: tx-3009
izenburua: Begiratu Niri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AiC4bzikmHI
---

Begiratu niri
Zure ezpainetan ez ditut sumatzen
gordean dituzun nirekiko sekretuak.

Jakin nahi dut zergatik ohitu ninduzun
eskuen ukimenik gabeko maitasun honetara,
musu gehiegi itsumustuan etorkizuni gabe
hantxe geratu ziren gau askotako naufragioetan
zure zain, haize-orratzei helduta,
zehazki non zeunden
seinalatzeko ezgauza.

Begiratu niri, geratzen den denboraz axola gabe.
Irentsi ditzagun honela, zur eta lur, urduri,
hainbesteko patxadak eraginik
ezertara ez daramaten bideak,
agian beste lakio batzuetara daramatenak
Zu maitatzen jarrai dezadan
emeki musukatu baino, ez nauzu egin behar,
Oso poliki. Ongi dakizu.