---
id: tx-2055
izenburua: Maui-Maui
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6CPlnCwB74g
---

Baserri aldean ez dugu izaten
denbora librea sobran,
larunbata da ta Tomas hartu behar duk
sekula santako aitzurra,
heldu orduko Seat Ibiza
hartatik jaisterakoan,
tabernan sartu lehenengo trague
hartzeko nago irrikitan.
Begi bietatik sartu zitzaidan
eskumako neska hura,
hankak zituen luze-luzeak,
gona, berriz, zen motzena,
berbetan hasi nintzan agudo
nola dekozu izena.
"Elvira" deitzen didate batzuk
zuretzat gura dozuna.
"Koka-kola" ta "Kuarentaitresa"
edaten iaioa,
esku bat gerrian ipini nion
bestea ezin gelditu.
Kotxe baten barruan
lehenengo aldia ote zan,
txapela ta kondoiak
bolantearen kontra,
gona zeukan motzena
Jainkoaren izenian.
Handik aurrera zer gertatu zan
esatea ere lotsa,
gona azpian gordetzen zuen
sorpresarik handiena,
sartuko zidala atzeko zulo
txikerrenetik barrena,
"Elvira" deitzen zioten baina
Antonio zuen izena.
"Koka-kola" ta "kuarentaitresa"
edaten iaioa,
esku bat gerrian ipini nion
bestea ezin gelditu.
Baserrira heldu nintzanian, hura zen
egonezina,
mozkorra ere kendu zitzaidan eta,
halako trauma larria!
Kotxe baten barruan
lehenengo aldia ote zan,
txapela ta kondoiak
bolantearen kontra,
gona zeukan motzena
Jainkoaren izenian