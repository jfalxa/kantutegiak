---
id: tx-594
izenburua: Balantzaren Jokoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-unEr0D8Zqg
---

Beralde berantza bultzaz epailetzan gantza balatza herrin gatzak ezin gorantza bultzaz
antza.
Beitzak ze dilema damakin epailetzak
beltzan humorea ez omen da humore beltza.
Etzak hartu erregen txisteik humoretzat
behintzat argi izan non baietza non ezetza.
Pentsa hire iritzia haien jabetzan,
beitzak ze dilema damakin epailetzak.
Beralde berantza bultzaz epailetzan gantza balatza herrin gatzak ezin gorantza bultzaz
antza.
Faxismoakin izan den bezala Willy Toledo
borrokan Josep Valtònyc, Pablo Hassel ta #alfreedo,
gure askatasunek ostia bat darama astero.
Komiko, rapero, tweetero edo titiritero
begie mantentzen dunari tolerantzia zero,
zenbaitek oaindikan diktaduran gaudela uste do.
Zelda baten bitarten bida,
kaleko beroak eragin dezakeen dirdira.
Paretan hiltzatutako begiak begira dira
egiarekin egiten dute zingira.
Tira, bultza, banaketa hesia bota arte.
Gure sistemak berak ez al gaitu kritikatzen?
Joku hontan jokatzen azkena diktadura bat zen
oaindela berrogei urte ta ez dugu utzi atzen!
Beralde berantza bultzaz epailetzan gantza balatza herrin gatzak ezin gorantza bultzaz
antza.
Faxismoakin izan den bezala Willy Toledo
borrokan Josep Valtònyc, Pablo Hassel ta #alfreedo,
gure askatasunek ostia bat darama astero.
Komiko, rapero, tweetero edo titiritero
begie mantentzen dunari tolerantzia zero,
zenbaitek oaindikan diktaduran gaudela uste do