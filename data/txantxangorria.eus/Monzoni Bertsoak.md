---
id: tx-1583
izenburua: Monzoni Bertsoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N72jRLcXk9M
---

Monzon gogoan min dagon
ahantzirik nola egon
haren ondoan biltzen ginuen 
nahiko zorion
Gizon argi ta bihotz on,
zinen egiazko gizon,
ezker onezko froga gaur hemen
daukazu De Monzon!

Martxoaren bedatzia
utzi nituen bizia...
Euskal Herriko historian
zu ezi ahntzia!
zure balio guzia
gu baitan duzu utzia,
zuri zor dugu ainitzek
gure euskal kontzientzia!

Eleketari bikaina
olerkiz profeten haina,
sorropila fin utsal gabea zen
zure ezpaina...
ez zinen mokor gordaina,
lur harritsuan eraina,
hartu zaituen lur onak
noizko da zure ordaina?

Español demokrazia
deitzen da hipokrizia,
Frantziakoa iduria da
hori da guzia!
ehunmila polizia,
euskalduna ihizia
tortura bizi den lurrean
da hila Justizia!

Batean frantses eskola
eta bestean española,
bien artean xintxifrikatuz
gure ikastola
Polizak duen pistola
ta batzuen epixtola
madarikatzen ari zaizkigu
euskaldun odola!

Bakeak haundi du fama,
Gezurra dela nork asma?
Euskadin gerlan delako
froga mugak baderama...
Minetan ikusiz ama,
ezin jasan bere zama,
eskuan zerbait hartu beharko, 
makil edo harma!

Batzuentzat buru bero,
erdi xoro, erdi ero
izaiten dira mundu
huntako jakintsuak oro...
zuri ospeka gaur bego
herria orai ta gero,
BERGARA herri
OLASONEKO MONZON TELESFORO!