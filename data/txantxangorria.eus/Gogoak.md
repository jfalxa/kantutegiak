---
id: tx-722
izenburua: Gogoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hnAB5kqU8ro
---

Gogoak
(Hitzak: Jon Garmendia "Txuria"-Musika: Xabier Solano)




It's a new dawn,
It's a new day
It's swiftly gone by
You know how I feel
Emaidazu berriro
Ongietorria
Zu zara nire poz
Guztien iturria
Emaidazu berriro
Ongitorria
Gelditzeko nator eta!
Tren batean
Gurutzatzen diren
Begiradak
Barra ertzean
Ematen diren muxuak
Lo aurreko uneak amesten,
Amesten
Egunak joaten dira...
Hondartza horian
Egindako gazteluak
Ez dutuzte olatuek eraitsiko
Gure eskuekin
Marraztutako loreak
Ez ditu denbora ebakiko
Emaidazu berriro
Ongietorria
Zu zara nire poz
Guztien iturria
Emaidazu berriro
Ongietorria
Gelditzeko nator eta!
Le notti arrivano veloci
Dalle mie mani
Ricordi il treno
Nella nostra stazione
Invece qui la gente
No ci parlate
Loak hartu baino lehen
Esnatu nahi nuke
Loak hartu baino lehen
Amestu nahi nuke
E songi
Aspaldiko garaien
Bidean galdurik…