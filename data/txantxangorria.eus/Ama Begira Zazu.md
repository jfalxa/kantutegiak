---
id: tx-1577
izenburua: Ama Begira Zazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OJppU5leLY0
---

Ama begira ezazu
leihotik plazara;
ni bezalakorikan
plazan ba ote da? (bis)

Plazan ba ote da? (bis)

Lai lara lai tra la...

Begiak erne ditut,
bi zangoak arin,
hoinbeste dohainekin
ez naute mutxurdin. (bis)

Ez nauke mutxurdin (bis)

Lai lara lai tra la...

Mutikoen artean
itzul-inguruka,
gizongaitzat ba duzket
dotzenan hamaika. (bis)

Dotzenan hamaika. (bis)

Lai lara lai tra la...

Heldu denprimaderan
arropa xuritan
ederra izanen naiz
mahai santuetan. (bis)

Mahai santuetan. (bis)


Lai lara lai tra la...

Haurra, ahal badaiteke
hala izan bedi.
Lehen gure aldi zen,
orain zuen aldi (bis)

Orain zuen aldi (bis)


Lai lara lai tra la...