---
id: tx-1982
izenburua: Profezia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BHWU4ONhsXc
---

Profeta batek bere izkribu
Zaharrean jarria
Nola haize ta urek higatzen
Duten armarria
Ez dela uzaz biziko, sutan
dagoen herria
Itsas mendekuz itoko dela
Bere egarria
Ozeanoaren ozena
Izanik gabeko izena
Gazi-gozoen ondoan datza amets
haundi bat izan zena

Ozeanoaren ozena
Izanik gabeko izena
gazi-gozoen ondoan datza
Euskalerri izena
Uren sarrera sendagarri da
Lurrazal errean
Oroitzapenak zuria dakar
Uhin aparrean
Iraganari muga berria
Jarri beharrean
Amorruaren bitsa lehertzen da
E/bro la/ba/rre/an.

Ozeanoaren ozena
Izanik gabeko izena
Gazi-gozoen ondoan datza amets
haundi bat izan zena

Ozeanoaren ozena
Izanik gabeko izena
gazi-gozoen ondoan datza
Euskalerri izena

Borobilera formaratu da
Mendeen frutua
Ozeanoan bortitz ari da
Indar itsutua
Kantaurie/tan isil dabilke
Herri izkutua
Arrainen aho borobiletan
Hizkuntza mutu/a

Ozeanoaren ozena
Izanik gabeko izena
Gazi-gozoen ondoan datza amets
haundi bat izan zena

Ozeanoaren ozena
Izanik gabeko izena
gazi-gozoen ondoan datza
Euskalerri izena
Ozeanoaren ozena
Izanik gabeko izena
Gazi-gozoen ondoan datza amets
haundi bat izan zena

Ozeanoaren ozena
Izanik gabeko izena
gazi-gozoen ondoan datza
Euskalerri izena