---
id: tx-2571
izenburua: Oi! Baldorba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xiKAwUZ3sF4
---

ROTTEN XIII 
(Larraga -Tafalla, Basque Country)

Grabaketa, edizioa eta produkzioa:
Oier Araiz




Oi! Baldorba abestia Rotten Amairu-ren lehen diskatik hartuta.
Iruñako Sound of Sirens estudioan grabatua.
Intro: Benito Lertxundi – Oi lur
Nafarroako Erdialdean egindako bideoklipa.

Canción Oi! Baldorba de Rotten Amairu tomada de su primer disco.
Grabado en los estudios Sound of Sirens de Iruña.
Intro: Benito Lertxundi – Oi lur
Videoclip realizado en la Zona Media de Navarra.

Oi! Baldorba song taken from Rotten Amairu's debut album.
Recorded in Sound of Sirens studios in Iruña.
Intro: Benito Lertxundi – Oi lur
Video clip made in the middle area of Navarre.

Eskerrik asko bertan parte hartu zenuten guztioi!

Contact : rottenamairu@gmail.com 

Letra:

Nekazariak nekatu dira
uzta transgenikoetaz,
naturaren oihua entzun dute.

Ehundaka urtez zutik zirauten
zuhaitz zahar handiek
azken martxari ekin diote.

Ez ditugu inolaz onartuko
liberalen legeak,
irrintzi bat entzun da akelarrean.

Herri guztietan elizak, sutan,
argitzen du bidea.
Basatien garaia iritsi da!

Oi! Oi! Oi!
Oi! Baldorba,
Herriaren amorrua
bere jabearen aurka.
Oi! Oi! Oi!
Oi! Baldorba,
dirua zuek duzue
baina lurra gurea da!

Fabriketako kateak hautsi,
komunalari eutsi
ta ez ezazu inor zapaltzen utzi.

Ardiak zaunka txakurren aurka,
artaldea hautsi da
ta errepidean zizak atera dira.

Oi! Oi! Oi!
Oi! Baldorba,
herriaren amorrua
bere jabearen aurka.
Oi! Oi! Oi!
Oi! Baldorba,
dirua zuek duzue
baina lurra gurea da!


Lyrics:

The farmers are sick of transgenic harvests
they have heard the scream of the nature
The trees that have been stand up for hundred of years 
have started now the last march.

We will never accept the liberal's laws
There is a ancient calling coming from the Akelarre
The burning churches all along the villages lights up the way
The time of the wildness has come.

Oi! Baldorba!
the rage of the people against the owner of the land
Oi! Baldorba!
They have the money, but the land is for us.  

Break the chains of the factories, keep the common property
and dont ever let they smash you down.
The sheeps are barking against the dogs, the flock has broken
and there are mushrooms growing on the road.

Oi! Baldorba!
the rage of the people against the owner of the land
Oi! Baldorba!
They have the money, but the land is for us.