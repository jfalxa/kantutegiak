---
id: tx-2753
izenburua: Elurrez Gainezka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ivW3qCcQO7Q
---

Erabiltzen ari zaitut
jostailu baten antzera
ezinezkoa ikusten dut
aho zinkin hau isiltzea
noizbait zu lasai uzteko
ahaleginetan nabil ni
zuregan dudan begia
beira ilunez dago estalirik
ulermena dut aitzaki (tzat)
bertan gordetzeko akatsa
borroka egizu gogorki
gaiditzeko une garratza
nire pekatuzko olatuen
artean zara ageri
bertatikan salbatzeko
ez dut ahaleginikan igerri

Elurrez gainezka gaude
gure beldurren erdian

Gose greban murgiltzea
ezinezkoa iruditzen zait
pastela eskutan dudala
irentsi egingo dut noizbait
mila bider ikusi dut
damututzeko unea
eta utzi dut nik pasatu
bertara naraman trena
oinez ibiltzen ba dakit
berandu iritsiko naizela
barkamena lortu ezik
jasoko dut okerrena
azokan salgai banago
ez nitan dirurik utzi
konpontzen ba naiz lehenago
ikusiko gera etzi
elurrez gainezka gaude
gure beldurren erdian