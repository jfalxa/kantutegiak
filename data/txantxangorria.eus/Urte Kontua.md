---
id: tx-369
izenburua: Urte Kontua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/K6N2qZBr48E
---

Esker mila jakitunen biltzarrera etorri zineten guztioi, (Chillnobitafilmak) Goldari bereziki. Tasio, Ibon, Salinas I, Beñat eta Julen Mila esker, Maite zaituztegu un kopon. 
Abestia (Sound of sirens) estudioan grabatu da, Eskerrik asko Julen.


Garai bateko ardiak
tabernetatik atera ziren.
Guda amaitu ezina
ikur guztien kontra
suertatu zen.
Aitaren ileak 
haizeak eraman zuen
gaur egun kaleetatik
iraganeko zaldiak
margotzen dituzte.
Ez da ezberdin
egungo egunkari
ta atzako paper zikin.
Kafe huts honek
odolaren zaporea dauka
ziur nago ni.
Labankadak zerutik,
anaiaren parranda
eta amaren negarra.
Ez dago pistolerorik, 
ez erregerik
zu baino hobe denik.
Zein da bidean,
galdetzen dut
etxera bueltan.
Non ote da azken urteotan
ikasi behar genuen
irakaspena.