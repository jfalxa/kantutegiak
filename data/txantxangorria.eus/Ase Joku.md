---
id: tx-71
izenburua: Ase Joku
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tRnk3LSp4AI
---

Ase jokü 
Lagün hoberenak dira orai etsaitürik, 
Biak beitziren ordüan burrat bat edanik, 
Pusako baten gatik itzala hitzemanik, 
Baimena ebatsirik, herri bat irauzirik. 
Sos aisaren ürinak emaiten bürüko mina, 
Bühürri den jentea ez da xüxentzekoa, 
Harek aisa jaten dü oroer hitz emana, 
Mündüan tronpatzeko beitü dohain huna. 
Ostatüan jüntatzea.. plazera! Ase joküak aldiz.. aharra! 
Flakü den kabalea arranoen bazka, 
Akeitü beno lehen egotxi behar da, 
Güti ükena gatik, bortxaz date emana, 
Zerütik jin emaitza debrüaren saria. 
Hitzak: Altzükütarrak 
Müsika: (mexikanoa)… Jean ETCHART