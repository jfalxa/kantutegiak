---
id: tx-783
izenburua: Eklipse
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gVIrlUQ_Rmk
---

Haritz Harreguyk grabatua Usurbilen
Ruben Caballero eta Skakeitanek ekoiztua
Eliot Jamesek nahastua Londoneko Eastcote Studiosen 
Streakyk masterizatua Iver Healtheko Pinewood Studiosen
Argazkiak: Arriguri
Artwork: Markel Idigoras

ipuin amaiezina, buklean nahasia,
komediante sadikoak barrezka idatzia
nor da ba egokia? non ote da zoria?
istripu kateatuan, hor zaude erdi-erdian
ez daukazu ahazterik, berandu da oraindik
dimentsio ezberdinetan, elkarrekin eta bakarrik
ez esan ez duela erretzen, hain gertu distantzia babesten
ez esan ez duela erretzen, ez esan ezin dela bete
gure arteko eklipsea
Bering itsasartean ikusi zaitudanean,
jendez lepo tabernetan itsu gauden izkinan
denbora etetean, kontrako hemisferioetan,
inoiz ez da momentua, galdu dugu kobertura
une oro une bera, behin eta berriz
une oro une bera eta alferrik