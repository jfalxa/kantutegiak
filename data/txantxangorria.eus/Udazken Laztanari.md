---
id: tx-1657
izenburua: Udazken Laztanari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JB6dzrMDQag
---

Udatzen laztan liluragarri
eder zara ta dotore
ezpain gorrizta, begiak urdin
bekain berde edo more
eskuak leun, gorputza lirain
larrua gaztain kolore
haizearen eskola zaharrean
tornuz egindako lore.
 
Uda sargori, negua ilun
arrotz izanik hauzoak
haize gainean zoaz oinutsik
hostoak dira pausoak
poza lumatzen jostatzen dira
zure animan usoak
amodioa hegan zurtzituz
izuak eta lausoak.
 
Sagar gorriak aho eztian
mingain berdean mahatsa
umo usaien zinta horizta
gerri estuan ardatza
larru gorritan gorputza eta
haize zuritan adatsa
izar ixilek zu laztantzeko
erazten dute arratsa.