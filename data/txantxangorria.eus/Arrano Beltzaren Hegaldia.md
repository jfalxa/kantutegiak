---
id: tx-2650
izenburua: Arrano Beltzaren Hegaldia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KJ2o5InKpfc
---

Erresumen garaietatik hona
odolustu nahi izan duten herria
euskaldunon lurralde

nafar gudarien aberria

Kateek batzen gaituzte
katez gaituzte lotuak
bide bazterrak
gorpuz bete zituzten
jauntxo agintari kolonoak

Makurtu ginela salduz
engainatu nahi gaituzte
odola darion lumaz
idatzitako historian

Arrano beltzaren hegaldiak
jarraituz egin genuen bidean
ereindako borroka haziak
gaurko egunetaraino loratu dira

Amaiurko gaztelu beltzetik
sasiko gordelekuetara
belaunaldiz belaunaldiko
iraultza ta matxinadak

Ezpatak ta sua
etsaiaren aurka
zerua itxiz kea
mendetan zehar
eguzkia ukatzen
diguten heinean
boliek aurrera
jarrai dezala

Askatasunaren izenean
menpeko bihurtu gaituzte
odola darion lumaz
idatzitako historian

Arrano beltzaren hegaldiak
jarraituz egin genuen bidean
ereindako borroka haziak
gaurko egunetaraino loratu dira

Bostehun urte
konkista baten menpe
zapalkuntzari hortzak erakusten
seme-alaben irribarrea
izango da gure mendekua

Arrano beltzaren hegaldiak
jarraituz egin genuen bidean
ereindako borroka haziak
gaurko egunetaraino loratu dira

Bostehun urte
konkista baten menpe
zapalkuntzari hortzak erakusten
seme-alaben irribarrea
izango da gure mendekua
mendekua,
izango da gure mendekua