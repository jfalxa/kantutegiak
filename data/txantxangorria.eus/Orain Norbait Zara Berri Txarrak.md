---
id: tx-3024
izenburua: Orain Norbait Zara Berri Txarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0hIFcHFjY2E
---

Susmoa banuen baina
argitu didazu zalantza:
denok baino gehiago dakizu

batzuekin txalo errezeko
beste guztiak larrutzeko
lagunak egoki aukeratu

lezioak ematen disfrutatzen dutenen kerua

pose bat, pose bi
iragan cool bat berreraiki
sutxakurren distira daukazu

sorbalda aldean zimiko
atzetik labana sartzeko
gaur egun etorkizuna duzu

aholkuak ematen disfrutatzen dutenen
gero eta antz handiagoa hartzen zoaz
beste orojakile sinple bat
gabon