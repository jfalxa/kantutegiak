---
id: tx-990
izenburua: Cançom Da Korrika 14
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q7B6OOObtPY
---

gauzak esateko hitzak beharrezkoak dira. 

Elkar ulertzeko sortzen dira hizkuntzak, 

Loreak bezalakoak, egunero zaintzen dira 

Ura bota behar, hiltzen dira bestelan. 

Euskara bihurtuko da loretsu bat 

Guk, denok landatua. 

Goazen ba korrika (bis) 

Euskara mintzatuz gure artian 

Belaunaldiz belaunaldi 

Korrika. Korrika ni ere bai. 

Goazen korrika, gure euskara zaintzera 

Nagusiok ere badugu aukera. 

Ez izan lotsati ez esan berandu dela 

guztion laguntza, eskatzen da korrikan. 

Euskara bihurtuko da loretsu bat