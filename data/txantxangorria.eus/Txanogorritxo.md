---
id: tx-3285
izenburua: Txanogorritxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p87oci8T5p8
---

La, la,
La, la, la, la, la, la, la, la,
La, la, la, la, la, la, la, la
La, la, la, la, la, la, la, la


Bart egindako amets batean
baso erdian nengoen
ilargiaren argipean
festa handi bat ospatzen.

Amonak kitarra jotzen du
saxofoia ehiztariak
txanogorritxu eta otsoa
elkarrekin dantzan biak.

Oh! Txanogorritxu
otsoaren laguna.

Oh! Txanogorritxu
hori da nahi duguna.