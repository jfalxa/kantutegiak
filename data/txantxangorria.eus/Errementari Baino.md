---
id: tx-2235
izenburua: Errementari Baino
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8Kd1mNs0pqA
---

Urruku tuku tuku
ez niri ikutu
ama kamaran dago
sentiduko gaitu

Ama kamaran eta
aita sranpean
neska mutilak dabiltz
eskilarapean

Errementari baino
hoba da zesteru
lepo bete egurregaz
kolko bete diru

Akaberea da eta
ibili kontuen
amaitutera noa
urrengo puntuen