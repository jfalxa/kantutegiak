---
id: tx-1427
izenburua: Trankatran, Arre, Arre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ltg1avTQVpE
---

Trankatran, arre, arre, arre!
zaldiz datoz hiru errege.
baltasar, Meltxor, eta Kaspar
urteberri eta urtezahar .Ai, oi, gabaren on!
Jaio berri hori non degu non?
Trankatran, arre, arre, arre!
opari bat dakar Kasparrek.
Meltxorrek eta Baltasarrek
aldegin dute irribarrez. Ai, oi, gabaren on!
Jaio berri hori non degu non?
Gabonak eta Eguberri
mundu guztiaren pozgarri.
Pipa bat eta bi belarri
zutik aitonak nahi du jarri. Ai, oi, gabaren on!
Jaio berri hori non degu non?
Entzun gaur izarrak diguna,
heldu da errege eguna.
Hauxe da eskaintzen zaiguna
pakea eta osasuna. Ai, oi, gabaren on!
Jaio berri hori non degu non?
Trankatran, arre, arre, arre!
zaldiz doaz hiru errege.
baltasar, Meltxor, eta Kaspar
urteberri eta urtezahar . Ai, oi, gabaren on!
Jaio berri hori non degu non?