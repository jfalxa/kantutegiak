---
id: tx-19
izenburua: Maite Dugun Bizkaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1iGA4OLdFq4
---

MAITE DUGUN BIZKAIA GEROARI KEINU-KANTUKA JENDE ALAIA IRRIBARREZ DAGO MAITE DUGUN BIZKAIA GEROARI KEINU-KANTUKA GORRI -ALAIA
Haran berdeetan sigi-saga konforme ez dagoen ibaia.
Ezker olatuak laztandu du arlauza gris gris baten arrakala.
Gaztelu galdua esnatu du lehoi baten orro suminduak
Garabi gorriak beso biak altxa ditu altxa, Garabi gorriak Beso biak E(g)iteko dantza