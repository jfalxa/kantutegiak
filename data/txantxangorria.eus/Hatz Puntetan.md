---
id: tx-2392
izenburua: Hatz Puntetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HldpHFfWgI8
---

Letra: Harkaitz Cano
Musika: Joserra Senperena

Ekoizpena: Joserra Senperena

Ahotsak: Ramon Agirre eta Marta Agirre, Edurne Garmendia, Unai Elizasu, Koban, Zuriñe Hidalgo, Ze esatek!, Xabi San Sebastian, Txirri-Mirri eta Txiribiton, Dorleta Urretabizkaia eta Asier Hernandez, Antton Valverde, Miren Lazkano, Olatz Salvador, Mikel Karton, Ane Gabarain, Iker Lauroba, Lide Hernando, Fernando Ruiz, Beñat Gaztelumendi, Xabier Euskitze, Olatz Beobide, Urko, Ohiana Maritorena, Itziar Zamora.

Bateria: Pako Díaz
Baxua: Fernando Neira
Gitarrak: Iker Lauroba
Teklatuak: Joserra Senperena
Tronpeta: Ruben Salvador
Saxoa: Julen Izarra
Tronboia: Imanol Iribarren

Txistuak: Jose Ignazio Ansorena, Jon Ansorena, Pello Irigoien, Xabier Muñoz
Atabalak: Agustin Laskurain, Michel Longaron

Txalaparta: Oreka TX

Trikitixa: Iker Dorronsoro
Panderoa: Gurutze Dorronsoro

Danborrada: Joserra Unanue, Alazne Olaziregi, Iñigo Ibiriku, Endika De Miguel, Koro Aldezabal, Elena Alberdi, Fernando Navascues, Joxe Bengoa, Jon Elzo, Mikel Mendizabal, Maitane Krutxaga, Aitziber Ibarzabal, Mikel Torregai.

Teknikariak: Haritz Harreguy Hi Gain estudioan eta Iñigo Etxarri In estudioan.

Nahasketak: Haritz Harreguy

Bideokliparen ekoizpena: Mirokutana

Aktorea: Amaia Irazabal

Zuzendaria: Ander Iriarte
Ekoizlea: Alex Castrillo
Argazkia: Koldo Agirre
Kamerak: Koldo Agirre, Ander Iriarte eta Aitor Bereziartua



LETRA:

Esku artean duguna,
gure esku da benetan?
Nahiago nuke egongo balitz
eskuen ordez hatz puntan...
Hatz puntetan egon dadin,
bai azal eta bai mami,
iritzi eta boza batera
plaza erdira ekarri.
Enbata badator, aizu,
segiko dugu jo ta su!
Mundua aurka omen daukagu:
gu ez al gara ba mundu?
Elkar entzuten ikasi
genuen guk adi-adi:
oihu bakar baten ordez
milaka deiadar txiki;
gure esku egon dadin
bai azal eta bai mami.
Behingoagatik gurea
izango da albistea:
sutan jartzean probatutzen da
nolakoa den eltzea!
Hamaikatxo izateko
ekarri bi hamarreko:
zortziko hauxe osatu dugu
bostekoari bosteko.
Enbata badator, aizu,
segiko dugu jo ta su!
Mundua aurka omen daukagu,
gu ez al gara ba mundu?
Asko entzun, asko mintza,
bizitzeko da baldintza.
Esku hutsik gatoz baina,
eskuetan dugu giltza:
biluzik gatoz mundura,
janzteko daukagu hitza.
Enbata badator, aizu,
segiko dugu jo ta su!
Mundua aurka omen daukagu,
gu ez al gara ba mundu?
Ahotsetik dator poza
eta pozetik ahotsa:
ahots ozen bereiziak,
ez artalde, ez ardiak...
Gurea baita doinua,
ta gu gara dantzariak.
Enbata badator, aizu,
segiko dugu jo ta su!
Mundua aurka omen daukagu,
gu ez al gara ba mundu?
Iritzi aske batera
eskutik helduko gera,
eskuetan daukaguna
sinetsiz badaukagula:
hatz puntetan egon dadin
bai azal eta bai mami.