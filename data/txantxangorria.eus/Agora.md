---
id: tx-2420
izenburua: Agora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6N93ZhKJlVo
---

Ez naiz entzungo irratin inoren bugattin
ez naiz eszena handiaren zati
 eszena jada ez da zena, badakit
ez zena da orain: ez da bilatzen basati-
egiak ote dira nire letrak
azala ere ematen dut ta  z da dudala lepra
x-aren esanahiaren atzeko egiaren edertasunaren
atzetik nabil,  aljebra
agonizatzen nago agoran
harean itsatsita itsasgoran
amets zailak ametralladoran
hilko naiz pilo bat píldora
hartu ditut, erakutsi mapa R2D2
ni ez naiz Leia hau ez da leia ta hala ere ezin gelditu
darth vaderrek lepoa eraitsi dizu
ta hori ez zaituela ukitu
ta berdin jokatzen du sistemak
txikitatik ikasten ditugu  lemak
maitasun eredu ziztrin poemak
inperialsimoak bere du zinema
ideología gailenaz menperatzen geyenak
zure pentsamendu guztiak dira haienak
ta nahiz eta zuri eman horrek  pena
gutaz parrez ari dira yenak
ta hórrela gaude gu, erdiloak
ta ni botatzen oldskul estiloan
bide hontan nahiko poliki noa
baina marka utziz barrazkiloa
 eta bestela sar nazatela plazako kalabazan
materia ta masa eraldatzeagatik bilatzen nau nasak
nibelez pasa naiz tae z da erraza
letra hau ezin du idatzi gabi de la mazak
kepasa lagun, zeba zaude hain triste
badakizu gaur egun, beti txarrak albiste
zer egin dezaket zuregatik bada
inork ez badu jada iraultzan sinisten
agonizatzen nago agoran
harean itsatsita itsasgoran
amets zailak ametralladora
hilko naiz pilo bat píldora
ditugu aukeran: batzuk gorriak besteak  urdinak
izan zaitez gorria ta egin alegina
edo aukeratu ez irekitzea inoiz kortina
ze zapalduak gauden ezin imajina 
mina! Mina! sentitzen dut orain
gillotina daukate nire partez zain
herria zapaldu izan duten guztiek
ta zapalduko dutenek bebai
ez ditut izenak emango, ez naiz fajaua
ninjak nahiago izaten du gaua
ilargi argitan ibiltzea zaila da akaso
baina ikusten ez zaituenak ezin zaitu eraso
inspiratuta nago ez dut saltzen musarik
berandu ibili arren ez dut galtzen busa nik
bizikletaz ziztu bizian gaitzerdi izerdiz bete zaizkit galtzerdik
hitzerdiak esaten daki edozein kiskik
baina gutxik dakite erabiltzen makilakixki
makiña hitz egia esan badakizkit
baina idazteko makina orain arte hilik
agonizatzen nago agoran
harean itsatsita itsasgoran
amets zailak ametralladoran
(nork ulertuko nau orain)
Maketa osorik deskargatzeko: 
Kontaktua: 
@ziakhus_rap
ziakhus@gmail.com