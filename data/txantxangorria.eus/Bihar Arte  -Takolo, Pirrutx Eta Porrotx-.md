---
id: tx-3259
izenburua: Bihar Arte  -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lZaZtdOdpBo
---

Plazan nengoen
jolasten txirristan
txori misterio/tsu bat
hurbildu zitzaidan
ipuinak kontatzen
ametsaren denboran
goazen etxera
hodei gainean
Ilundu zaigu
laister eguna
izar argitsuak
goaz denok ohera
Bihar arte laguntxo
panpin jostailuak
musu bat aita eta ama
Plazan nengoen
jolasten txirristan
txori misterio/tsu bat
hurbildu zitzaidan
ipuinak kontatzen
ametsaren denboran
goazen etxera
hodei gainean
I/lun/du zai/gu
lais/ter e/gu/na
i/zar ar/gi/tsu/ak
go/az de/nok o/he/ra
Bihar arte laguntxo
panpin jostailuak
musu bat aita eta ama