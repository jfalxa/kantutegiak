---
id: tx-809
izenburua: Bentara Noa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4MOQGjAPXJg
---

Bentara noa
bentatik nator
bentan da nere gogoa
bentako arrosa krabelinetan
hartu dut amodioa.

Ez dakit zer dan neure maitea
ene aspaldiko bizia
gurina sutan oi dan bezala
urtu zait gorputz guztia.