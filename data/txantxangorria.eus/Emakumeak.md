---
id: tx-2831
izenburua: Emakumeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AZOK7q30fH0
---

Berdintasunaren legeak
gizonen mundu batean
historia idatzi dutenak
gizon boteredunak izan dira

Boterearen borroka
bizitza esparru guztietan
zapalkuntza egoera ezkutuak
ez da existitzen ikusten ez dena

kateen artean bizitzera kondenatua
lan egin eta birproduzitzeko makina
subjetu objektu, subjetu eraila
subjetu objektu, subjetu eraila

ZUTIK EMAKUMEAK!!
LEHENENGO ZAPALKUNTZAN,
ZUTIK EMAKUMEAK!!
BORROKAREN MOTOREAN
ZUTIK EMAKUMEAK!!
PATRIARKATUAREN AURREAN!!
ZUTIK EMAKUMEAK!!
BAKOITZAK BERE ALDETIK!!

Hitz politak eta faltsuak
hipokresia gure inguruan
sexismo eta matxismoak
kontzientziaren...

Kontzientziaren lanketa
patriarkatuaren aurrean
ikustaraziz ikusten ez dena

Kateak apurtu, eraldatu baloreak
parekidetasunaren jendartearen sorkuntzan

ZUTIK EMAKUMEAK!!
LEHENENGO ZAPALKUNTZAN,
ZUTIK EMAKUMEAK!!
BORROKAREN MOTOREAN
ZUTIK EMAKUMEAK!!
PATRIARKATUAREN AURREAN!!
ZUTIK EMAKUMEAK!!
BAKOITZAK BERE ALDETIK!!