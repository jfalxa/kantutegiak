---
id: tx-826
izenburua: Paris
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lPcq77mf_Pk
---

Amets goxoak izan
Ahal dala nerekin
Ahal dala Parisen
Soineko urdin batekin
Zuk hitz egin bizitzaz
Zureaz ta nireaz
Hiru ordu joan dira
Zure eskuei begira

Dantzan, dantzan
Muxuen hondartzan
Notre Dameko plazan
Dantzan,dantzan
Arimen balantzan
Notre Dameko plazan

Hartu zaitut besotik
Oso gertu nigandik
Elkar eramateko
Ibai aldamenetik
Akaso ez dizut esan
Maite dudala bizitzak
Zure begietan
Egiten duen isla

Dantzan, dantzan
Muxuen hondartzan
Notre Dameko plazan
Dantzan,dantzan
Arimen balantzan
Notre Dameko plazan

Galdetu esan
Ez geratu zalantzan
Zenbat maite zaitudan
Dantzan, dantzan
Notre Dameko plazan

Dantzan, dantzan
Muxuen hondartzan
Notre Dameko plazan
Dantzan, dantzan
Arimen balantzan
Notre Dameko plazan

Galdetu esan
Ez geratu zalantzan
Zenbat maite zaitudan
Dantzan, dantzan
Zena zen da Sena da

Amets goxoak izan
Ahal dala nerekin
Ahal dala Parisen
Soineko urdin batekin
Zuk jarraitu hizketan
Nik muxuak kopetan
Zer egongo ote zaren
Zu amesten benetan.