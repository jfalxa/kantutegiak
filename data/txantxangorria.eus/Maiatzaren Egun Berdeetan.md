---
id: tx-506
izenburua: Maiatzaren Egun Berdeetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qttu4qKrCKU
---

Maiatzaren egun berdeetan
euri-malkoen amoreagatik
erne dira galtzailentzako liliak
mendeetan zehar itzalak
eho duen eremuan
han non eki-errainu
hondarrez elikaturiko
kaden-kalatxoriek
bilerak egiten dituzten
han non urrats
bakanetan soilik bidea
multi/plikatzen den
han non zenbat izar hainbat mezu
gordetzen zaizkien.