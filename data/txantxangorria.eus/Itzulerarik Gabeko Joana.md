---
id: tx-1668
izenburua: Itzulerarik Gabeko Joana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ONjzd3tfae0
---

Jada ez dut ezer sentitzen
jada dena hoztu baita
dena galdu baita jada
ezin dut jasan zu galtzea
ez eta ere urruntzea
baina amaitu da dena.

Zurekin egon nahiko nuke
egunsentia gau egin arte
baina amaitu da dena
ezin dut aurkitu hitzik
barnekoa askatzeko
behingoz atzean uzteko.

Ez du itzulerarik izan
itzulerarik gabeko
joana izan baita
sorginduta utzi ninduzun
ta egunero pentsatu nahi dut
jada ahaztu zaitudala.

Zure irrifar xumeak
dena esaten zidan
begiradarekin soilik
agur esatean.