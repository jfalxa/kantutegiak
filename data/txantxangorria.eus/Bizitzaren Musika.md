---
id: tx-1407
izenburua: Bizitzaren Musika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YsSMaIqpjlc
---

hitzak biluzik daude paper zurian 
kolorerik gabeko margo bat bezala 
melodiak buru barruan 
ate joka ari dira 
askatasunari atea ireki nahian 
hitzak dantzan hasi dira 
kitar sokak astintzean 
doinuz jantzi ditzagun 
hitzak musika bait da 
gure bitzitza aurrera! 
gora bihotzak! ireki! 
zurea da giltza! 
goizetik gauera 
doinuz betetzen naiz 
bizitzaren musika 
daramat zainetan 
hauts artean dauden 
abesti galduak jartzean 
zerbait berezia sentitzen dut 
barnean hitzak 
dantzan hasi dira 
akorde hauek nahastean 
doinuz jantzi ditzagun 
hitzak musika bait da 
gure bitzitza aurrera! 
gora bihotzak! ireki! 
zurea da giltza! 
maitasuna ta gorrotoa adierazteko 
herria eta hizkuntza goratzeko 
gugandik urrun daudenak besarkatzeko 
bizi-bizirik gaudela sentitzeko 
doinuz jantzi ditzagun 
hitzak musika bait da 
gure bitzitza aurrera! 
gora bihotzak! ireki! 
zurea da giltza!