---
id: tx-2622
izenburua: Herri Unibertsitatea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QPaZ0Z1kOEo
---

Gazte ameslari aske izatetik
merkantzia izatera
txontxongilo, lan esku merke
zenbaki hutsak gera.

Eskuzabaltasunaren izenean
gure bizitzak erostean,
ikasi eta sortutakoa
gaude saldu beharrean.

La lara, larala la la la lara lara la...
la lara, la la la lara...

Herritik eta herriarentzat
Herri Unibertsitatea
sorkuntzatik hasi, iraultzaren hazi
elkarlanean bizi!

Eguzkia itzartu degu goizean
haize berriak heltzean
hezkuntza berri baten pausoak
idazten haztean.

Jakintza askatu ta elkarbanatu,
gurea aldarrikatu
gizartearen beharrak asetuz
ikasteak mugarik ez du!

La lara, larala la la la lara lara la...
la lara, la la la lara...