---
id: tx-905
izenburua: Burgos
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3WqELB3H9Dw
---

Gora zuek, abertzale haurride

Gure berri jakin baitu orai mundu guziak!

Kastillatik abendoko hotzetan

Iguzki su gorri gorriz gira piztu denetan.

Berlinetik Burgosera

Baionatik Bilbaora

Entzun dugu oihu bera:

«Eusko gudariak gara».

Gora zuek! iraultzaileen alde

Mundu guzian balire guretzat ere hobe!

 

            Gora Euskadi askatuta!

 

Gora zuek, gizon eta emazte

Hamaseiak bihotz batez xutitu baitzarezte.

Beldur gabe, asarrez ikaretan

Jeiki da gure Herria zuen soka eskutan.

Guda doa urrats haundiz

Burgos etorriko berriz!

Beharrez edo ez nahiz,

Herri hau da ezin biziz.

Gora zuek, Herri hunen zaindari

Eskutan harmarik gabe gaituzue atzarri!