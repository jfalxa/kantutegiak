---
id: tx-2535
izenburua: Txakur Bat Erosi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1rIGrBDLbRQ
---

Txakur bat erosi dut uau, uau, uau,
merke azokan.
Zikina zegoela, Zikina zegoela,
eta sartu dut baineran.
Uau, uau, uau, txakur bat
e/rosi dut merke azokan.
Uau, uau, uau, txakur bat
erosi dut merke azokan.