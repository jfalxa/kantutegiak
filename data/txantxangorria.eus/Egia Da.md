---
id: tx-805
izenburua: Egia Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RFzcUI2J-ik
---

Egia da,
Egunero berresten da argia,
Egia da.
Ahabiak
Eskuetan zubiko bidean,
Etxe aldera.
Larre beteak, arrats distiraz,
Maldan gora noanean.
Baita egun ilunenean ere
Berresten da eguzkia.
Ahabiak
Eskuetan utzi arrastoak
Dirudi odola.
Garbitzean
Entzun ditut gaurko gertaerak
Irratian.
Larre beteak, arrats distiraz,
Bat batean lekutan dira.
Baita egunik ilunenean ere
Berresten da eguzkia.
Eguzkia
Egunero albisteen gainetik doa,
Egia da,
Egia da, egia da.