---
id: tx-752
izenburua: Ez Etsi!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5mAu9O_9n90
---

Noraezean baldin banago
ama goxo datorkit
hitz zuhurrez mintzo:
es etsi!
Ta ilun-aldietan
Bera alboan daukat beti,
hitz zuhurrez mintzo
ez etsi!

Ez etsi. ez etsi. ez etsi, ez etsi!
Hitz zuhurrez mintzo: ez etsi!

Ta lur jota daudenak ados
jartzen badira aurki,
bada irtenbide bat,
ez etsi!
Zail dirudien arren
lortu liteke, ta sinetsi:
bada irtenbide bat,
ez etsi!

Ez etsi. ez etsi. ez etsi, ez etsi!
Hitz zuhurrez irtenbide bat, ez etsi!
Ez etsi. ez etsi. ez etsi, ez etsi!
Hitz zuhurrez marmar: ez etsi!

Gau hodeitsuan ere
argi bat nigan da pizturik
egun-argirarte
ez etsi!
Musikak esnatu nau, eta
ama goxo datorkit
hitz zuhurrez mintzo:
ez etsi!

Ez etsi. ez etsi. ez etsi, ez etsi!
Hitz zuhurrez irtenbide bat, ez etsi!
Ez etsi. ez etsi. ez etsi, ez etsi!
Hitz zuhurrez marmar: ez etsi!