---
id: tx-2631
izenburua: Hautsetatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cqIO8eEoAdM
---

Jendeak egin behar badu guk eragindako 
mixeritik ihes 
inoiz denona den herririk sortuko al degu 
ezin ikusi ta, mesfindantza bidez!

Nolatan sortu ginen ez dakit 
zorte kontua soilik ate?

Batzuk izarren hautsetik eta
besteak gerren errautsetatik.

Nork eta zeren baitan hautatzen du bertakoa 
nor den eta nor ez?
Hitzak bezela sekula beteko al ditugu
plazak uso zuriz eta olibondoez? 

Sobera dugu elkarren antzik
mendelbal hego ipar eki. 
lnmigrantea ez da arrotza 
urrutitik etorria baizik!

Nolatan sortu ginen ez dakit
zorte kontua soilik ote?
Batzuk izzarren hautsetik eta
besteak gerren errautsetatik.