---
id: tx-936
izenburua: Congratulations
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kkytqwm_tQk
---

Congratulations and celebrations,
pozezko egun bat igaro egizue,
congratulations and jubilations,
gaurko jaialdi hau ospatu egizue!!
Zuen lagunak eta adiskide guztiak,
zuek hain pozik eta alai gu ikusteaz,
zuekin bat egitea gogoz batutzen dugu,
hain zoriona da gaurko eguna!
Congratulations and celebrations,
pozezko egun bat igaro egizue,
congratulations and jubilations,
gaurko jaialdi hau ospatu egizue!!
Zuen lagunak eta adiskide guztiak,
zuek hain pozik eta alai gu ikusteaz,
zuekin bat egitea gogoz batutzen dugu...
Hain zoriona da gaurko eguna!!!
.......................................................................
Estitxu Robles eta Daikiris taldearen kantu bat, beraien "Estitxu ta Daikiris" singleletik . 1968. urtean kaleratu zuten, Cinsa zigilupean. 

Patxi Rodeño, bateria ta pianoa 
Anton Garcia, ahotsa ta baxua 
Luis Sanjuan, ahotsa ta gitarra
Jesus Mari "Jerry" Bilbao, saxofoi altua 
Javi Ortega, pianoa ta akordeoia 
Carlos Zubiaga, saxofoia ta biolina