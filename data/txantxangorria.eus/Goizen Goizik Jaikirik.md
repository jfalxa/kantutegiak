---
id: tx-828
izenburua: Goizen Goizik Jaikirik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oClf_P7Ixhs
---

1. Goizean goiz jeikirik, argia gaberik
Urera joan ninduzun, pegarra harturik.
Tra la la la...

2. Jaun chapeldun gazte bat jin zautan ondotik?
Heia nahi nuenez urera lagunik?

3. Nik, ez nuela nahi urera lagunik,
Aita beha zagola salako leihotik.

4. Aita beha zagola ezetz erran gatik,
Pegarra joan zerautan besotik arturik.

5. Urera ginenian, bia musuz musu,
Galdegin zautan ere: zombat urthe duzu?

6. Hamasei...Hamazazpi orain'ez komplitu:
Zurekin ezkontzeko, gazteegi nuzu.

7. Etcherat itzultzeko, nik dutan beldurra,
Ez jakin nola pentsa amari gezurra.

8. Arreba nahi duzu ni erakuts zuri
Etcherat ethortzean zer erran amari?

9. Urtcho churi pollit bat, gabaz dabilana,
Hark ura zikindurik, egotu naiz, ama!

10. Dakigunaz geroztik zer erran amari,
Dugun pegarra pausa: gaitezen liberti!"