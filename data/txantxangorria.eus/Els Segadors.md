---
id: tx-2329
izenburua: Els Segadors
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PpmworBqCEE
---

Katalunia garaile,
aberats eta betea izango da berriz ere.
Atzera jende hau
hain harro eta handigura.

Igitai zartada ona,

Igitai zartada ona,
Herriaren defendatzaileak!
Igitai zartada ona!

Orain da ordua, segalariak.
Orain da erne egoteko ordua.
Beste ekain bat datorrenerako
ongi zorrotz ditzagun tresnak.

Igitai zartada ona,

Igitai zartada ona,
Herriaren defendatzaileak!
Igitai zartada ona!

Egin beza dardar etsaiak,
gure bandera ikustean.
Urrezko galburuak erorarazten ditugun bezala,
komeni denean kateak segatzen ditugu.

Igitai zartada ona,



Catalunya triomfant,
tornarà a ser rica i plena.
Endarrera aquesta gent
tan ufana i tan superba.

Bon cop de falç,

Bon cop de falç,
Defensors de la terra!
Bon cop de falç!

Ara és hora, segadors.
Ara és hora d'estar alerta.
Per quan vingui un altre juny
esmolem ben bé les eines.

Bon cop de falç,

Bon cop de falç,
Defensors de la terra!
Bon cop de falç!

Que tremoli l'enemic
en veient la nostra ensenya.
Com fem caure espigues d'or,
quan convé seguem cadenes.

Bon cop de falç,