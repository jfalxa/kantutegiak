---
id: tx-421
izenburua: Ilun Dagoen Arren
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SRPbsr71des
---

Ilun dagon arren
Gaur olatu baten
Indarra sentitzen dut barruan
Handitzen dabilen
Zurrunbilo baten
Abiadura bizian
Ta erori
Zutitu
Hortako eginak gara
Ez eman atzorik
Horrek gaur berdin du
Hau da momentua
Niretzat
Bertara etorri naizen
Jakin badakit
Iritsi nintzenetik
Barrua mugitzen didan su-gar hori
Bizirik
Ta ileak
Zutitzen
Zaizkit akorde baten
Eindako bideak
Mila oroitzapen
Aldatzekorik ezer
Eta berriz
Sartuko da iparretik
Pizten nauen
Haize bolada hori
Eta berriz
Sartuko da iparretik
Pizten nauen
Haize bolada hori
Nahi dudana egiten
Nahi dudana egiten
Nahi dudana egiten
Nabil
Nahi dudana egiten
Nahi dudana egiten
Nahi dudana egiten
Nabil
Nahi dudana egiten
Nahi dudana egiten
Nahi dudana egiten
Nabil
Nahi dudana egiten
Nahi dudana egiten
Nahi dudana egiten
Nabil
Eta berriz
Sartuko da iparretik
Pizten nauen
Haize bolada hori
Eta berriz
Sartuko da iparretik
Pizten nauen
Haize bolada hori
Ilun dagoen arren
Ilun dagoen arren
Ilun dagoen arren
Ilun dagoen arren