---
id: tx-1787
izenburua: Urruntzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l9KbzFy1Eq8
---

Hegazkinean eserita
Idazten ditut honako hauek
Onena da siniste zazu 
Gure artekoa amaitzean

Urruntzen, aurrera egiteko arrazoiak bilatzen
Urruntzen, bizirik sentitzeko beharrezkoa nuen

Une alaiak izan ziren
Egun ilunez inguratua
Gurea zen kaltegarria
Onena biontzat urruntzea

Urruntzen, aurrera egiteko arrazoiak bilatzen
Urruntzen, bizirik sentitzeko beharrezkoa nuen

Badakit egokiena ez dela
Gutunez esateak
Bakartu gutuna mintze bazaitu
Muxu bat ta zoriontsu izan

Urruntzen, aurrera egiteko arrazoiak bilatzen
Urruntzen, bizirik sentitzeko beharrezkoa nuen