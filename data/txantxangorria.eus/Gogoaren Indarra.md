---
id: tx-535
izenburua: Gogoaren Indarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ryfIbpDeaZM
---

ZURE ALBOAN, AURRERA ATHLETIC!
ZURE ALBOAN, AURRERA ATHLETIC!
ZURE ALBOAN, AURRERA ATHLETIC!
BETI ZURE ALBOAN!

Jaiotzetik daukagu eta ez dugu inoiz galdu,
Athletic sentimendu bat da gure bihotzean.
Ikurriña zelaian, zuri-gorri, orlegi
Herri bat txaloka harmailetan.
Atezainaren bizkartzainak, gola behar bada bultzada
Katedrala, gure etxea, hau da gure SAN MAMES!

BETI ZURE ALBOAN, AURRERA BOLIE!
GOGOAREN INDARRA, BIDE BAKARRA
ATZO GAUR TA BIHAR, ATHLETICZALEAK
IÑIGO CABACAS HERRI HARMAILA

BETI ZURE ALBOAN, AURRERA BOLIE!
GOGOAREN INDARRA, BIDE BAKARRA
ATZO GAUR TA BIHAR, IÑIGO GOGOAN
ALABIN, ALABAN, ALABIN-BOM-BA
ATHLETIC, ATHLETIC GEURIA!

Athletic, gorri ta zuria
Danontzat zara zu geuria.
Erritik sortu zinalako
Maite zaitu erriak.
Athletic, gorri ta zuria
Danontzat zara zu geuria.
Erritik sortu zinalako
Maite zaitu erriak,
Maite zaitu erriak,
Maite zaitu, maite zaitu,
MAITE ZAITU ATHLETIC!

GOGOA, INDARRA,
BURUA TA BIHOTZA!
AURRERA NESKA MUTILAK!

Jaiotzetik daukagu eta ez dugu inoiz galdu,
Athletic sentimendu bat da gure bihotzean.
Zazpiak bat zelaian, zuri-gorri, orlegi
Herri bat txaloka harmailetan.
Atezainaren bizkartzainak, gola behar bada bultzada
Katedrala, gure etxea, hau da gure SAN MAMES!

TXOPO IRIBAR, PIRU, PITXITXI TA ZARRA ERIKA, ADURIZ, IRAIA
Izan zaretelako gara
Hau da gure SAN MAMES!
Hau da gure SAN MAMES!
IÑIGO CABACAS HERRI HARMAILA

TXOPO IRIBAR, PIRU, PITXITXI TA ZARRA ERIKA, ADURIZ, IRAIA
ITURREGI, SUSAETA, MUNIAN, AINHOA TIRAPU
Izan zaretelako izango gara.