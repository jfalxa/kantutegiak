---
id: tx-3225
izenburua: Zuzeneko Taupadak  On
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VcvyI0HKd2U
---

Metro karratuan porlanezko harresiak denbora aldarrian herdoilaren hotsaz, halere aske naiz barne bidaietan, urrunetik taupaden ondareak heltzean.

Oinak oinetan gorputza uztartzen zait, irudien hautsaz hormak zeharkatzean hautuan zalantzak eta geroan begiak. Instanteak, joan etorriaren geltokia.

Begiak herrira, eta herrian Zuzeneko Taupadak.

Bidea malkartsua bada, premian askoren bultzada. Keinu bat, keinu bakar bat, bakartasuna hautsiko duena.

Begiak herrira eta herrian Zuzeneko Taupadak.