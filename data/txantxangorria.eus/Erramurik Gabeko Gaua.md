---
id: tx-808
izenburua: Erramurik Gabeko Gaua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CzvxCQozcxI
---

Recorded and mixed in SHOT grabaciones (Arrasate) by Iñaki Bengoa in 2013 
& KATARAIN estudioa by Jonan Ordorika 


Masterized by Karlos Osinaga "Txap" 

Cover: Miren Karmele Gomez (sinestesia11.tumblr.com/archive) 

Photo: J. A. Areta Goñi (JUXE) www.blackirudifaktoria.com 

Bera 2014
paroles
Erramu koroen zain 
ez gara inoiz egon, 
baina ez esan guri 
zer den txar eta zer on. 

Estatuak eskale- 
entzat bakarrizketa. 
Hezur eta haragi- 
-zkoa da gure festa. 

Ondo hondoratua, 
txarto ohoratua. 
Ahazturak badaki 
zein den gure hautua. 

Gaua putzua bada, 
ezin naiteke alda. 
Gaua samurra bada, 
samurra nire malda... 

Aire garbi apurra, 
zigarroaren kea, 
atxiki eta arnastu, 
aski eta pakea. 


Egunaren nekeak 
kendu arren barrea, 
imintzio samurra 
bedi gure larrea. 

Eginaren ordaina, 
sobera eske al da? 
Norberari berea, 
leun dezagun malda. 

Hitz samurrez biluzi 
egun ilunen kraka. 
Erantzi eta erantzi: 
gauak arrazoi dauka! 

Erantzi hitzak eta 
erantzi atzaparrak, 
jantzi denak ahantzi 
“erantzi” hitza jantzi. 

Ispilua zu zaitut 
gardena eta zintzo: 
benetako islada 
zure begien mintzo. 

Gaua putzua bada, 
ezin naiteke alda. 
Gaua samurra bada, 
samurra nire malda... 

Harkaitz Cano