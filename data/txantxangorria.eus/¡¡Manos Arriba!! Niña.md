---
id: tx-1831
izenburua: ¡¡Manos Arriba!! Niña
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rket9u9Ft04
---

Bihotza geldirik duzu, baina
taupaka hasiko da
Zugan sinisten baduzu
uxatuz beldurrak

Kontatu bizitakoa
hesiak nondik datoz
Samina handia bada ere
Hasi besarkatzen

AY NIÑA
UTZIDAZU MAITATZEN
GURE MODURA
BARREAK MARRAZTEN 

Utzi atzean gau hotzak,
jasandako minak
Oinetan kilima eginez
zure irria eskuratzen

Negar egiten baduzu 
Poztasuna izanen da
Erritmo berriarekin
Elkarrekin dantzan


AY NIÑA
UTZIDAZU MAITATZEN
GURE MODURA
BARREAK MARRAZTEN

GOZATU
IRAGANA AHAZTU
ZURE ONDOAN NAUZU
BERRIZ BIZITZEKO

Eskutik helduta
Une txarrak kenduta
Beltza den guztia margotuta

Utzi zu laztantzen,
Utzi zu maitatzen,
Gertatu behar dena utzi gertatzen