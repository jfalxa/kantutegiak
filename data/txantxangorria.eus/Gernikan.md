---
id: tx-743
izenburua: Gernikan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/td2bLManlpQ
---

Javier Gardoqui
Josefina Solozabal
Aurelio Legarreta
Catalina Arrieta
Maria Luz Fierro
Aguirre (12 urte)
Francisco Aralucea
neskato erre bat, Gernikan

Regina Aldama
ume identifika ezina
Aurelia Candes Lopez
clara zaldumbide
gorputz zatiak
Germana Basabe Ormaechea
Telesforo Elierobarrutia

Maria Santa
Bilbao Uriona
behi zatia
Candida Amias
agure baten gorputz errea
Catalina Barrena Barrena

Agapita Iturralde Zuloaga eta
Maria Iturralde Zuloaga, Gernikan

asto behi gorputz zati
neskato erre bat
ume identifika ezina
Gernikan!

Daniel Ibarzabal
Catalina Arrien jaio
asto
gorputz zati
andreenekin nahastuta

Juana Beotegi Bilbao
Jacinta Gandiaga
eta
ehunka ume
identifika ezinak