---
id: tx-1981
izenburua: 500 Urte Lipdub
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uk2zpsIWj1o
---

Haiek indarra arrazoi eta
guk arrazoia indarra
alfer moduan geneukan
gure lurra babestu beharra
Gure gainetik pasatu denez
ejerzito gaztelarra
Nafarroa den jabe egin dira
modua eta negarra

Odola, malkoak, garrasiak
umezurtzak, alargunak
gure gainetik pasatu dira
zaldunak ezpatadunak
iturri nago erakutsiko
bi bula biharamuna
azkena arnasa eman duena
Nafarroko erresuma

Borrokan dagoen herria
Ez dago garaitzerik
Borrokan dagoen herria
urtero landatzen baitu
bere etorkizun azpian
bere etorkizun azpian
Borrokan dagoen herria


Mendi berdetan
nahiz zelai horietan
edo kutsatutako asfaltoan.
Kateatuak izan ginenetik
bostehun urte luze pasa izan dira.
Nafar eztarri nazionalak
berriz oihuka dezan,
dituen ahots desberdin guztiekin
bortizki, Eh! garrasi berbera.

Zorupeak dakarren ur korrontea,
mendez mende isuritako bera.
Zuhaitz zaharrek errotzen dute,
herri  hontako askatasun nahia.
Nafar eztarri nazionalak
berriz oihuka dezan,
dituen ahots desberdin guztiekin
bortizki, Eh! garrasi berbera.


Eraitsitako gazteluetatik
aterpeak eraiki genituen.
Belaunikaldiek utzitako zuloetan
landatuko ditugu haziak.
Nafar eztarri nazionalak
berriz oihuka dezan,
dituen ahots desberdin guztiekin
bortizki, Eh! garrasi berbera.

Mendi berdetan
nahiz zelai horietan
edo kutsatutako asfaltoan.
Kateatuak izan ginenetik
bostehun urte luze pasa izan dira.
Nafar eztarri nazionalak
be/rriz o/ihu/ka de/zan,
di/tu/en a/hots des/ber/din guz/ti/e/kin
bor/tiz/ki, Eh! ga/rra/si ber/be/ra.


NAFARROA BIZIRIK!!