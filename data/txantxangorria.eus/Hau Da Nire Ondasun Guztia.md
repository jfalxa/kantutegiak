---
id: tx-585
izenburua: Hau Da Nire Ondasun Guztia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5sAj1FWffB8
---

Hau da ene etxea, hau da
ene aulkia, hau da ene ohea,
hau da ene ondasun guzia.
Bakardade eta isiltasun
hau da ene jabego bakarra.
Mila jostorratz herdoildu aldean,
leihorik gabeko begirada,
zafrada garratz bat bihotzean,
hauxe da nik dudan guzia:
hilda nagoela esan dezaket?
Amagandik irten nintzen bezala
irtengo naiz endredo honetatik.
Aterik zabaldu ezin eta
—ba ote da hilobi estuagorik?—
funtzionarioen begi kliskak
mirillan igartzean ohartzen naiz
                        bizirik nagoela.