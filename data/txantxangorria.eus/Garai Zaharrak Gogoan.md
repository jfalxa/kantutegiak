---
id: tx-2311
izenburua: Garai Zaharrak Gogoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AxIYbzup2Zw
---

Hitzak: Robert Burns (1788)/ Musika: Eskoziar herrikoa
(“Auld Lang Syne” Jokin de Pedrok euskaratua)


Ahaztu behar ote dira 
adiskide zaharrak?
Ahaztu behar ote dira
gure garai zaharrak? 

Garai zaharrak gogoan
ene adiskide
topa dagigun gau hontan
garai zaharrak gogoan

Adiskide leial hori 
bihotzeko lagun
emadazu bostekoa
garai zaharrak gogoan 

Garai zaharrak gogoan
ene adiskide 
topa dagigun gau hontan
garai zaharrak gogoan