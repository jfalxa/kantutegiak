---
id: tx-1102
izenburua: Urrun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GannzuyfdBc
---

dena dago urrun
nire etxeko atetik at 
sentikortasunik ez
sortasunean lagata

jo dezagun abendua 
98. urtean 
eebbek berriz
bonbardatu dutela irak 
eta toulousen poliziak
hil duela magrebiar bat
eta madrilen faxistek
aitor zabaleta euskalduna 

dena dago urrun 

ikusle geldo bilakatuta 
kosovo, kongo, sierra leona 
gerra ikuskizun
soila mutatu da
mundializazioaren esklabuak

"all you need is love" g-7an
hiltzaileak abesten ari dira
behar duzun guztia
amodioa omen da

dena dago urrun

odol zipriztinez zikinduta
kasu baina eskaileretan
entzuten baitira urratsak 
geldikeriari agur, banoa

dena dago urrun
nire etxeko atetik at 
sentikortasunik ez 
sortasunean lagata

castro x -ren hitzak:
tutto rimane lontano:
son solo pochi pollici
d'incendi su belgrado.
e tu....senti� ringhiare
la conferma vicino che
sei seduto ancora 
al posto piu' lontano
e sicuro.
"la vita e' sonnolenza 
telecomandata indotta? 
non importa, alza! cambia!"
un morto occupa un giorno
fra la gente del terzo 
millennio?
io esco! perche'....

ti stai sfogando,
cercando un punto fisso
dentro te 
e ti domandi vuoi capire
perche'
il vento soffia in ppoppa 
a chi ha gia' preso tutto...
anche la forza di chi
un tempo faceva movimento:
c'e' chi ha canbiato 
bandiera, chi ha voltato
la faccia, 
chi ha preferito bonaccia
a questo mare in burrasca.
ma io resto, perche'
claudio e' fuori dal giro
della passivita'