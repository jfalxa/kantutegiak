---
id: tx-1189
izenburua: Umetatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o52jiCROHZk
---

UMETATIK BETI NAGUSI IZAN NAHI NUEN
ETA ORAIN NAIZENEAN EGOERAK AGOBIATZEN NAU
DENBORA ETENGABE ETA ARIN PASATZEN DELA
ZORAMEN BIHURTUZ BURU ERO HONETAN

GOGORATZEN DITUT LAGUN ARTEKO UNEAK
KEZKARIK GABEKO ORDU ETA BARREAK
PRESO SENTITZEN NAIZ NIRE OROIMENEAN
BIHARKO EGUNA NOIZ AZALDU ARTEAN

SARRITAN PETER PAN AGERTZEN ZAIT AMETSETAN
GOGORATZEN HEGAN EGIN NAHI NUELA
ITZULIZ BERRIZ “NUNCA JAMÁS”-ERA
HAN IZANGO GARA BETI JOLASEAN

BIZITZEKO MODU ZINTZO BATEN ATZEAN
BIHURRIKERIETAN DABIL NIRE BIHOTZA
DANTZA EGIN NAHI DUT EURIAREN AZPIAN
ERREKA BIHURTUZ IRAGANEKO AKATSAK

SARRITAN PETER PAN…