---
id: tx-327
izenburua: Bietnam 1970
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vwY1kE5Tc2M
---

Bietnam 1970
 
Yanquitarren egunetan,
Bietnamgo herrietan
atzerritarrek nagusitu nahi,
bainan alferrikan.
 
Loreak ximur dirade.
Hanoik kiskali nahi dabe.
Kapitalismorikan ez dago
gerlarikan gabe
 
Non dira bake zaleak,
non demokrazia aldeak
kondenatzeko yanquitarraren
bonba ta kalteak.
 
Armetan dagoz fuerte,
hala ere ezin dute.
Alferrik dabiltz vietnamdar bat
bizirik den arte.
 
Polizi eta armadak
pentsaturik aski dala,
ezin neurtu izandu dute
Herrien indarra.
 
Bietnam ez da bakarra
guziok senide gara.
Inperialisten kontrako dana
gure borroka da!