---
id: tx-518
izenburua: Euria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bZRtgTVfENA
---

💧Sorkuntza/Creación | Olatz Salvador
💧 Ekoizpena/Producción | Pablo Novoa
💧 Musikariak/músicos | Ander Zulaika. Jagoba Salvador. Mattin Saldias
💧 Grabaketa eta nahasketa/Grabación y mezcla | Haritz Harreguy
💧 Master | Ultramarinos Studio

💧 Bideoklipa/Videoclip | Aiora Ponce
💧 Dantzaria/Bailarina | Elene Carreto
💧 Magia | Lolita Gil

Eskerrik asko Donostiako Aquariumari 🐠
Gracias al Aquarium Donostia 🐠



Hiriak hartu du arima
eta bazter guztietan galdu zait errima
nire geografia emozionalean
kaleek kale egin didatenean

Beste zu bat naiz
zure suan zehar

Ezer ez da hermetiko
dena dago heze
kanpoko uholdeek 
kristala kolpatzen dute
orain betikoa berriz
orain A ala B
giltzapean babestu ala ireki
ireki eta urez bete

Orain badakit nor den
ateak erdi irekita uzten abilena
sentimenduen zappingean galdu naiz
eta badaki eguraldiaren iragarpena

Euria zure 
begietan

Ezer ez da hermetiko
dena dago heze
kanpoko uholdeek 
kristala kolpatzen dute
orain betikoa berriz
orain A ala B
giltzapean babestu ala ireki
ireki eta urez bete