---
id: tx-999
izenburua: Izar Baten Gainean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mRzli6edzDI
---

IZAR BATEN GAINEAN 

Izar baten gainean 
datoz hiru erregeak, 
izan baten gainean 
gure zaldun trebeak. 

Kaleko argipean 
ditugu miraz hartu, 
nekeak jota datoz, 
oinetan basamortu. 

Haurtxo bat jaio dela 
esan digute isilik, 
Belengo estalpean 
hurbil zeru-zerutik. 

Opari eta muxu, 
zorionetik goza, 
guk emango diegu 
kantua eta poza. 


#saveyourinternet