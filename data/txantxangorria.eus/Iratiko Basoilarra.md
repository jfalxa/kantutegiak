---
id: tx-376
izenburua: Iratiko Basoilarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j86vhxbQUPw
---

Nik banizun herkide bat
izenez ere kidea
bardo ibiltari herririk herri
ilusio-saltzailea
xirula-joilea
amets-egilea:
partitua da berriki.

Hamelinen haurrik ez da
denak ibaian itoak
erresinulak Zaraitzun barna
tolestu ditu hegoak
Ormazabalgoak
laztanduz erroak:
Garxoten bizi-aztarna.

Iratiko basoilarra
garrazki da isildua
bere burua goroldiotan
ehortzi du etsitua:
zu zinen bildotsa
otsoek gaitzetsa
hilobira bultzatua.

Euskal Herriko semea
Donosti hontan jaioa:
hiri ankerrak erauzi zizun
oihanaren jarioa
karrikan betizu
inork ez ulertu
zure sofrikarioa.

“Poetak debalde gara”
beste batek da esana
xirulak ez dezake deus egin
ordenadore-andana
eta robotekin
behar dugu jakin
dagigula alfer-lana.

Hala ere gau'erdian
zutaz oroit nadinean
tartetxo umila utziko dizut
neure ganbara pobrean:
hantxe entzungo dut
adi, erne'ta zut
zure doinua aidean.