---
id: tx-81
izenburua: Ajangizko Herrian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RX3zDFs6Uz8
---

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara

Ko/be/ta/tik ze/la/ie/ta/ra
gi/nen a/te/ra
eus/kal/du/nak de/nok ba/te/ra;
jo/an ga/ra or/du/tik au/rre/ra.
Ez da/go le/ge/rik
e/do e/rre/ge/rik
he/rri/en ga/ine/an.

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara

Han zegoen gure haritza,
gu/e bizitza;
bertan dugu legeen giltza;
triste ginake galdu balitza.
Ez dago legerik
edo erregerik
herrien gainean.

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara

Hau da gure irabazia,
gure grazia,
hemen erein dugun hazia,
Euskal Herriko demokrazia.
Ez dago legerik
edo erregerik
herrien gainean.

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara


Hortxe duzu etorkizuna,
zuk d/kizuna,
aspaldian ongi entzuna,
Euskal bihotzen gorritasuna.
Ez dago legerik
edo erregerik
herrien gainean.

Ajangizko herrian
zu zara etorriko plazara
eztarrian
dakarzula euskara