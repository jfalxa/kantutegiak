---
id: tx-3255
izenburua: Ilargia -Ken 7-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HSsjUD7jUc8
---

esaiozu euriari berriz ez jauzteko,
esan bakardadeari gaur ez etortzeko.

eusten nauen soka zara eta itotzen nauena,
ametsak sortu zizkidana, galtzen dituena.

zuretzat ilargia lapurtuko nuke gauero,
eta zu itsu zaude bere argia ikusteko,
irribarrez, gero minez, eragin didazu negarra,
nire sua itzali da,
ez zara gaueko izar bakarra, ez zara!!

esan sentitzen dudana ez dela egia,
une baten sinesteko ez garen guztia.