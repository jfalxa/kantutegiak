---
id: tx-2618
izenburua: Zaindu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Vtt31T67kOU
---

2ZIO taldea 2010. urtean sortu zen Odei (Urruña), Jo (Ziburu) eta Gorkaren (Algorta) eskutik. Rapa egiten dute eta 2013an kaleratu zuten lehen diska.



Zu nere sua hotzean, zu nere itzala udan nere pausoa bidean, nere arnasa helmugan.
Ez dugu aski hitz hiztegietan eta gugan zuri adierazteko zenbat maitatzen zaitudan.

Patu dorpeak, ez dudan zorteak, pozaren idorteak utzi dit bisai bustia.
Milioika aldiz atera behar litzake eguzkia nik zuri itzultzeko zuk eman didazun guztia.

Ta dena delarik laino, zugandik gertuen hobe begiak itxi itsu gelditzea baino. Gaurdaino, esperantzan sinesten genuen ta fedea galdu nuen jainko bat sortzeraino.

Segundu bakoitza biziz azkena bailitzan zure azken urratsak zirelarik bizitzan
nik eskutik heldu nizun ume beldurtuen gisan baina joan ez zintezen ez zen aski izan

Zure begien itzala ikustean hala, bihotzean sentitu nuen bakearen bala, ideki zenituen bi leiho handi bezala,
gero ulertu nuen agur erran nahi zenidala.

Nola txoriak kumeak, nola amak umeak zaindu maite duzun hori.
Nola arrainek itsaso zabala, nola umeek amaren magala,
maitatu zaintzen zaituen hori.

Nik gogoan daukat dena, eguna eta orena ondotik joan zinena ama lagunik onena.
Barnean nahasten zaizkit daukadan itxaropena, ta etorriko ez dena itxarotearen pena.
 

Semeak ama galtzea da bizi erregela, baina nola azaldu amatxiri joan egin zarela.
Ulertu dut horrela herio krudela dela, ta hoberenak berekin eramaten dituela.

Bestela erraidazue nola naizen husten, gorpu zarenetik pozak ez dira gorpuzten.
Ikusi naizela urik ez duen battela jakinik hemen zaudela, ez baitzaitut ikusten.

Hutsa betea dela entzun dut lehenago, bai nere pozaren hutsa minez beteta dago. Lehen ibiltzen nintzen hutsunea beztitzen, ta azkenean ikasi dut berarekin bizitzen.

Nere izen abizen, ez nuen uste inoiz eginen nuenik otoitz,
herio jantzi da heroiz eta biluztu arrazoiz guk geuk ez dakigula noiz joateko beti da goiz baina zuretzat goizegi zen.

Nola txoriak kumeak, nola amak umeak zaindu maite duzun hori.
Nola arrainek itsaso zabala, nola umeek amaren magala,
maitatu zaintzen zaituen hori.

Itsasoa laino dago, ta olatuek lehenago bezala jada ez didate gehiago erantzuten hasi zaitezte zuen bihotza entzuten, zerbait galtzean ikusiko duzue zenbat zenuten.

Neri bizitzak kendu dit bizitza eman didana. Ez da erakaspen bana eman nahi dizuedana baina barne drama honek hortara narama, zaindu zaitezte eta zaindu zazue ama