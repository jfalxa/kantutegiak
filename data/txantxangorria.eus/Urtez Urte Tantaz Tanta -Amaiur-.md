---
id: tx-3150
izenburua: Urtez Urte Tantaz Tanta -Amaiur-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7Uy-axtYePU
---

Urtez urte tantaz tanta
Urtez urte tantaz tanta
Urtez urte tantaz tanta
Urtez urte tantaz tanta

Ikasle uholdea sortzen
Eskubideak defendatzen
Euskal eskola jorratzen

Jo eta su borrokatuz
Ametsak egi bihurtuz
Korapiloak askatuz


Urtez urte tantaz tanta
Urtez urte tantaz tanta
Urtez urte tantaz tanta
Urtez urte tantaz tanta

Soka estutuz joan arren
Guk ez du/gu bel/du/rrik
Ez bai/da sor/tu u/hol/de/a
lo/tu/ko du/en so/ka/rik

So/ka es/tu/tuz joan a/rren
Guk ez du/gu bel/du/rrik
Ez bai/da sor/tu u/hol/de/a
lo/tu/ko du/en so/ka/rik


So/ka es/tu/tuz joan a/rren
Ja/rrai/tzen du/gu i/kas/ten
Oz/to/po/ak ja/rri/ta e/re
Es/ku/bi/de/ak bo/rro/ka/tzen

Tan/taz tan/ta la/ne/an
sor/tzen du/gu i/tsa/so/a
He/rri/a/ren hai/ze/a
Tsu/na/mi bat be/za/la
I/kas/le de/nok bil/tzen
es/ko/lak eus/kal/dun/tzen
mi/la/ka so/ka a/pur/tzen


So/ka es/tu/tuz joan a/rren
Guk ez du/gu bel/du/rrik
Ez bai/da sor/tu u/hol/de/a
lo/tu/ko du/en so/ka/rik

So/ka es/tu/tuz joan a/rren
Guk ez du/gu bel/du/rrik
Ez bai/da sor/tu u/hol/de/a
lo/tu/ko du/en so/ka/rik

Ur/tez ur/te tan/taz tan/ta
I/kas/le uhol/de/a sor/tzen
Ur/tez ur/te tan/taz tan/ta
Es/ku/bi/de/ak de/fen/da/tzen
Ur/tez ur/te tan/taz tan/ta
Eus/kal es/ko/la jo/rra/tzen
Ur/tez ur/te tan/taz tan/ta
geu/re ar/ka/tzak zo/rroz/ten

I/kas/le uhol/de/a sor/tzen
Es/ku/bi/de/ak de/fen/da/tzen
Eus/kal es/ko/la jo/rra/tzen
geu/re ar/ka/tzak zo/rroz/ten

Ur/tez!! ur/te!! tan/taz!! tan/ta!!

Ur/tez!! ur/te!! tan/taz!! tan/ta!!