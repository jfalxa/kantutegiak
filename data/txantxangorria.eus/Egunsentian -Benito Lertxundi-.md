---
id: tx-3193
izenburua: Egunsentian -Benito Lertxundi-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JzHiHHFLkg4
---

Eguzkien argia
leihopetik sartzian
poz eta alaitasunez
esnatuko gera.

Txorien abestiekin
alaitutako eguna
gizonen gorrotoakin
ondatuko da eguna.

Ibiliko gera
alkarrik kalte eginaz
ustez launik haundienak 
aurrez ondo hitz eginaz.

Batzuen izerdiekin
besteak diru eginaz
egia esan dutenak 
giltzapera emanaz.

Gauz hoiek ikusteko
nola esnatzen gera?
Gauz hoiek egiteko
nola jaikitzen gera?

Txoriak alaitutako 
eguna ondatzeko
hobe genduke ez esnatu
ta bertan geldi lotan.