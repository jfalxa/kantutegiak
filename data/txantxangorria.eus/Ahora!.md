---
id: tx-2390
izenburua: Ahora!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WAf0g5PG_Z4
---

AHORA

 

ez daukagu aitzakiarik

AHORA

 

Burutik eta bihotzetik

AHORA

 

Hitz egiten ez bada nola ba bestela?

Ahora Tolosa ekarri Euskara (bis)

 

 

Ai maitea zein ederra zure irudia

 

Zein xarmanta Tolosako lurra eta hiria

Mozorro ta konpartsak, txarangak eta inauteriak

 

Zu zara makina bat artistaren sorterria

Halere badaukagu zertan ikasia

Nagusi baita gure kaleetan erdara eta hori da egia

 

Ez da inork esan duen aurreneko aldia

Izan dadila azkena gure garrasia

 

 

Edan,beldurrik gabe edan maitea

 

Putzu sakon ta ilunetik ateratako ur gazia

Eraman ahora betiko botika berria

 

Euskaraz sendatzen da euskararen gabezia

AHORA...

Kilometroak! Kilometroak!

Momentua da! Momentua da!

 

 

Momentua da, badakigu nora

 

Kitatzera goaz daukagun zorra

Berreskuratzera galdutako ganora eta denbora

 

bihotzetik atera orain kanpora

Bazterretik esfera publikora

 

Eraman Euskara zure ahora

AHORA...