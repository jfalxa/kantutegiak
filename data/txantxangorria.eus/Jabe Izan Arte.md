---
id: tx-2084
izenburua: Jabe Izan Arte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8sAY3Job4WQ
---

Planetaren edozein tokitan
Haundiak beti jaten du txikia
Injustiziak eta izerdiak
Jarraitzen dute itotzen herria

Nazioen arteko elkartasuna
Eta jendearen batasuna
Ezinbestekoa suertatzen da
Gure argirik gabeko bidean

Ukabilak altxatuz itxaropena piztuz
Dagokiguna eskura dezagun
Ez zaitezte makurtu borrokatu eta lortu
Gure buruaren jabe izan arte

Estaturik gabeko herriengan
Eta lur gabeko pertsonengan
Urterik urte egon den kataia
Urturik izango da heuren laia

Ez ditzagun anaiak ahaztu
Munduko zelai itzela landuz
Zeren lur zati bat askatzean
Neurri batean askatzen da gurea.