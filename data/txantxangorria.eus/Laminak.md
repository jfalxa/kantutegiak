---
id: tx-52
izenburua: Laminak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YOabfuEWMOc
---

Estudiantina goizez,  
iluntzean bapez, 
zeozer egin behar da,  
pentsaben oin berrogei urte, 
arpegia zuritu ta ilea ezkutatu, 
eta soineko baltzagaz, 
agertu zien lehenengo laminak 
Dantza, salta, oihu, kanta, aratustetan, 
Mundakako Laminak gara. 
Kanta, zirri, farre, irri, aratustetan, 
Mundakako Atorrak gara