---
id: tx-207
izenburua: Sunday
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KzZLMxwe_Wc
---

Bideo zuzendaria: Jon Narvaez
Kamera laguntzailea: Alejandro Martos
Grip eta gaffer: Garikoitz Martinez
Grip eta elektrikoa: Borja Herran

Esan nizun ez zinela damutuko!
Plaza hartan, bi zerbeza ta bi liztor

Oihukatu, ixo, sentitu herriak gu behar gaitu!
Dastatu, askatu ta esan atera gaitezen hegan!

ORAIN ZER? AUSARTUKO GARA!
UDA HONTAN, EGUZKI BATEKIN
ESAN NIZUN, EZ ZINELA DAMUTUKO!

We are dancing on Sunday
on Sunday, on Sunday

Behin biziko ta, behin biziko ta...

Gozatu ez egin kontra!
Gozatu ez egin kontra!
Behin biziko ta behin biziko ta!

Mendi bat, itsasoaren gainean
Besarkadak tantaka

ORAIN ZER? AUSARTUKO GARA!
UDA HONTAN, EGUZKI BATEKIN
ESAN NIZUN, EZ ZINELA DAMUTUKO!

We are dancing on Sunday
on Sunday, on Sunday

Orain zer?
Oihukatu, ixo, sentitu...
Ixo, sentitu, ixo, sentitu ...

Aursartuko gara!
Askatu ta esan, askatu ta esan, askatu ta esan

Gozatu ez egin kontra
Gozatu ez egin kontra Behin biziko ta behin biziko ta!

------
Konposizioa: SÜNE eta Rikki
Produkzioa: Rikki, SÜNE eta Pol Merchan