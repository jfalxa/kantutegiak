---
id: tx-3038
izenburua: Jeiki, Jeiki Etxekoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/42f6teNHJ_o
---

Jeiki, jeiki etxekoak,
Argia da zabala,
Argia da zabala!
Itsasotik mintzatzen da
Zilarrezko tronpeta
Bai eta ere ikaratzen
Olandresen ibarra! (bis)

Jeiki, jeiki etxekoak,
Argia da zabala,
Argia da zabala!
Itsasotik hurbiltzen da
Untzi bat kargatua;
Gaitzen denak mentura
Haren atakatzera! (bis)

Jeiki, jeiki etxekoak,
argia da zabala,
argia da zabala.
Itsasoan agiri da
argi gozo biguna,
bere aurrean beldurtuta
ihes doa iluna,
ihes doa iluna.