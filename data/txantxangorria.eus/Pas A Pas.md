---
id: tx-1935
izenburua: Pas A Pas
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cj-Q1B7IZvM
---

La llibertat que et van robar, les espines que et vas clavar 
Aquell diumenge a Derry, el tres de març a Vitòria
són ferides que et van gravar, que la història mai no ha tancat,
lluitar per viure, i sobreviure, la voluntat de perdurar
Herrialde Katalanak ,Galizia, Andaluzia,
Irlanda eta Eskozia, Bretaina, Gales herria
Sahara edo Sardina, Kurdistan eta Korsika...
Munduan asko bezala bidez bide bagabiltza...
independentzia!!!
Eman eskua bihotza
amets batentzat etxerik
inon eraiki nahi bada
hona neria zabalik.
Hormaz horma, oholaz ohola
ez dago ezineskorik
askatasuna lortzeko
borrokak ez du mugarik
La llibertat que et van robar, les espines que et vas clavar 
pluja de plom a Grozni, ressonen plors de Gernika
pobles del món, pobles germans, pobles vençut i condemnats
Munduan asko bezala bidez bide bagabiltza...
independència
Alça't amor dona'm la mà
és l'hora de somiar el demà
una gran casa per tothom
ferma d'arrels i oberta al món
i cop a cop i pas a pas
tots junts seguirem avançant
cap frontera no aturarà
la llibertat que estem guanyant
Fent camí, pas a pas,
carrer a carrer, pas a pas
barri a barri. pas a pas
Poble a poble pas a pas

Fent camí, pas a pas,
carrer a carrer, pas a pas
barri a barri. pas a pas
Poble a poble fins que guanyem
la independència