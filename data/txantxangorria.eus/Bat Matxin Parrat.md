---
id: tx-490
izenburua: Bat Matxin Parrat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YM_OSnQdrF4
---

Bat: Matxin Parrat.
Bi: Mitxel Eperdi.
Hiru: kolko bete diru.
Lau: iko melau.
Bost: koxkotean koxt.
Sei: korta bete bei.
Zazpi: buru bete bazpi.
Zortzi: katillu bete zorri.
Bederatzi: katun narrue eratzi.
Hamar: mar-mar.