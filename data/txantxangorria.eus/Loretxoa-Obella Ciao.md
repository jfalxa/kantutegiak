---
id: tx-2729
izenburua: Loretxoa-Obella Ciao
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WkyeMnwfvOU
---

Mendian larrartean 
aurkitzen da loretxo bat 
aurrean umetxo bat 
loretxoari begira.

Loreak esan nahi dio: 
umetxo, aska nazazu!, 
jaio naiz libre izateko 
ta ez loturik egoteko.

Umetxoak ikusirik 
lorea ezin bizirik 
arantzak kendu nahi dizkio 
bizi berri bat eman.

Orduan izango bait du 
indarra eta kemena; 
orduan emango bait du 
ugari bere fruitua.

Abertzalea ni jaio nintzen
O bella ciao, bella ciao, 
bella ciao, ciao, ciao
abertzalea ni jaio nintzen
ta abertzalea hilgo naiz.

Egun batean, goiz goizetikan,
O bella ciao, bella ciao, 
bella ciao, ciao, ciao.
Egun batean, goiz goizetikan,
etsaian aurka joan nintzen.

Eta burrukan hiltzen banaute,
O bella ciao, bella ciao, 
bella ciao, ciao, ciao.
Eta burrukan hiltzen banaute,
hartu zuk neure fusila.