---
id: tx-2220
izenburua: Amparo Arangoari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Bhe06iPb2io
---

AMPARO ARANGOARI 
(1976) 
Nere ahotsa mozteko 
jo behar banaute, 
nere gogoak kentzeko 
xehatzen banaute, 
gorde zaitez hiltzaile 
itxoin begiratzen, 
mila ahots gaur badaude 
nere aldez mintzatzen. 
Neu izateagatik 
geu izateagatik 
burua ta hezurrak 
puskatzen banaute 
gorde zaitez hiltzaile 
itxoin begiratzen 
mila ukabil badaude 
askatasuna oihukatzen. 
Egia esateagatik 
giltzapean banaute, 
egia esateagatik 
katez lotzen banaute, 
gorde zaitez hiltzaile 
itxoin begiratzen 
mila ahots gaur ba daude 
amnistia oihukatzen.


Hitzak: Patxi Zabaleta