---
id: tx-716
izenburua: Egun Argian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6oH-U-q-XHw
---

Ilunaren arerio
Egun argian jario
Atzo gauean gozatuari
Zure usaina dario
Zeru gainean bi laino
Zoaz, eguzki, goraino
Zelan ez dekot beste gogorik
Zeuri kantetako baino
Soloa loraz polito
Nor ez da udabarriko
Zain nago zure udagoiena
Barriro noz etorriko
Asuneri udararte
Krabelina batu jake
Hain lotsatia ez bazina bai
Zu biluztuko zinake
Badator gaua astiro
Badeko nahiko motibo
Maite nozula dakidan arren
Esan eistazu barriro
Haizeak ohiko egarriz
Leizarrak dantzan darabiz
Eskuok zure azal gozoa
Omendu gurean dabiz
Kirikolatza karreran
Iturria non ete dan
Laztana maite zaitudalako
Behar dot zuretik edan