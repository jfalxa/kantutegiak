---
id: tx-2499
izenburua: Rebelk Folk
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eZPkiEoJ_H0
---

Gu Nerbioigo ezkerraldeko piratak gara
zure usteak ustel ez gara iruzurtiak.
Dagadaren ondaretatik jaio giñan taberna atzeko kalekantoian
Ez egin negar gugatik Jaun andreak
pizti bat daramagu barnean bor-borka. 

Huntza bezala dorre zaharra goratzen
barruan zaudete preso ez eman amore. 
Munstro zaindaria espataz hildugu ta bere gorpuzkiak suan gertatu,
Ez gaitzazue musukatu jaun andreak
beste abentura baten bila goaz.

whackfolulai!!! ohohohoh