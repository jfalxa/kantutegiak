---
id: tx-1972
izenburua: Bazkalosteko Kafea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_5fS6JNEm0o
---

Esperantzarik gabeko zenbait
debekatuak dezente
mila galduak eta hainbeste
gau bakarreko aterpe
askok laugarren begiradatik
betirako dirudite
bazkalosteko kafeak bezain
usai goxoa daukate
nik zurekiko dudana ere
halakoa litzateke

nire bizitza laburra baina
zein dezberdina izan den
zu ezagutu aurretik eta 
zuk musu eman ondoren
zure iparrak noraezaren
lekukoa hartu zuen
ta denboraren oneritziak
erakusten du ondoren
nire bihotza zure neurrira
nola egina dagoen

zurekin nahi dut Maxian barna
karroa zoro eroan
baita erantzi nahi dizkizudan
prakak erostera joan
bertan zaudela sentitu nahi dut
ohera sartzerakoan
lotsaren mugak lehertuarazi
ta gehiago delakoan
erloju hotsak esnatu eta
zu izatea alboan

momentu batez nire bertsoak
ametsetara bidaliz
zure usaiak atzeman ditut
zoriona jo dut begiz
hankak lurrera ekarri ditut
irribarre salatariz
bazkalostera itzuli eta
hamarmilagarren aldiz
aspaldi hartu dudalakoan
kafea hoztu zait berriz.