---
id: tx-648
izenburua: Gaueko Argiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/u6JTmpRzWuE
---

Argiak piztuta topatzen ditut
Kalera irteten naizen orduan
Iluntzean nork pizten ote ditu
Argi horiek ezkutuan?
Atzo itzaltzen ikusi nituen
Goizean lanerantz nindoala
Gaua erretiratzen hasia zen
Eguzkia jalgitzen zela.
Argiak onak dira,
Jakin nahi nuke nola,
Jakin nahi nuke nork, zerk
Pizten dituen...
Jakin nahi nuke
Nola pizten diren...
Jakin nahi nuke...
Nola pizten diren...
Ametsak pizteko,
Eta esperantza
Ilunaldietarako,
Argiak.
Batak bestearen
Egietan ere,
Desioak ikusteko
Argiak.
Jakin nahi nuke
Argiak pizten...
Jakin nahi nuke...
Jakin nahi nuke...
Nola pizten diren...
Gogoak ere
Hirietako
Argi antzera
Piztu daitezen
Gure gauetan.