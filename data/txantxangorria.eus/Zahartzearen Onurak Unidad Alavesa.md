---
id: tx-241
izenburua: Zahartzearen Onurak Unidad Alavesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0NtyH_Xrczg
---

Metabolismo ezberdin batek gidatzen nau gaur egun
Liluratzen nau zure ahotsak egun bakartietan
Lagun gehiegi hil zaizkit azken hamar urteetan
Oroimenaren argi gorria piztuta daukat orain
Espiritua berritzea ardo beltzaren laguntzaz
¿? etxe bila nabil zahartzearen onurak dira denak
Gorroto selektibo bat goruntz doakit beti eta ez dira bi
Zahartzearen onurak
Amari zor diodan guztia aitortzeko gai naiz
Lagun zaharrak eta berriak ezberdintzeko gauza
Grasa kopuru txalogarri bat daukat neguaren ateetan
Bizi modernoaren aurkako enbajadore gisa
Espiritua berritzea ardo beltzaren laguntzaz
¿? etxe bila nabil zahartzearen onurak dira denak
Gorroto selektibo bat goruntz doakit beti eta ez dira bi
Zahartzearen onurak
(...)
Espiritua berritzea ardo beltzaren laguntzaz
¿? etxe bila nabil zahartzearen onurak dira denak
Gorroto selektibo bat goruntz doakit beti eta ez dira bi
Zahartzearen onurak