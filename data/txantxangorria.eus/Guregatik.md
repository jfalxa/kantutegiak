---
id: tx-2014
izenburua: Guregatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TMBY_Gaf8mk
---

Egun sentia ez dauka sentzurik
ez du argitzen gela ilun hau.
Zure arnasa sentitzerik ez badut,
atzekabea irentsiko nau.

Bakardadea pasan ezina da,
eskutik heldu zenidanetik.
Arren eskatzen dizut behin eta orain,
eutsi nazazu indarrarekin,

Hitzetik, bihotzetik,
sentitzen duda guztiagatik,
niregatik, zuregatik.

Esaten dut, zin egiten dut,
zoriontasuna eskeintzen dizut,
ohiurik ez, malkorik ez.

Ikasi nahi dut beldurra gainditzen,
uda honetan gailenduko naiz.
Nire albotik urruntzen ez bazara,
ez dut bizitzan gehiagorik nahi.

Hitzetik, bihotzetik,
sentitzen duda guztiagatik,
niregatik, zuregatik.

Esaten dut, zin egiten dut,
zoriontasuna eskeintzen dizut,
ohiurik ez, malkorik ez.

Amets hau egi bihurtuko dugu,
maitasunaren erruetatik.
Bizitzak eskaini digun opari hau,
ez da galduko sasietatik.

Hitzetik, bihotzetik,
sentitzen duda guztiagatik,
niregatik, zuregatik.

Esaten dut, zin egiten dut,
zoriontasuna eskeintzen dizut,
ohiurik ez, malkorik ez.