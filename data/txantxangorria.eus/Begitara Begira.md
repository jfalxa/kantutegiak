---
id: tx-2434
izenburua: Begitara Begira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sjOgGOGHj_k
---

Jaitsi zaitez palkutik momentutxo batez
ta hartu lekua ene lagunen artean 
Begiraiguzu denoi zuzen begitara 
azalpen ugari zor dizkiguzu ta 
Esaiguzu hizkuntza hau ez dela geurea 
Herri faltsu bat izan dugula maite 
Geure arteko besarkada ta malkoak 
Lekuz kanpo sentitzen dituzun horrek 
Begitara begira, begitara begira 
Lekuz kanpo gaudela esaiguzu (biss) 
Zeure palku-kideek entzun ez dezaten 
uzkurtu eta esaidazu belarrira 
Bizi-bizirik dela ez al duzun ikusten 
neurea ta neure lagunen herria 
Zoritxarrez badakit galderen kantu hau 
entzungo duen azkena zeu zarela 
Beraz amestuko dut kalean 
topatu ta honakoa oihukatuko dizudala 
Begitara begira, begitara begira 
Lekuz kanpo gaudela esaiguzu (biss) 
Lurretik lorera, loretik bihotzera 
Legetatik urrun, malkotatik gertu 
Esaidak lagun eskutik heldu ta begitara 
Herrian zutik jarraituko diagula... 
Begitara begira, begitara begira 
Lekuz kanpo gaudela esaiguzu