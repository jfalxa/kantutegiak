---
id: tx-135
izenburua: Kantu Bat Gara Gorlizen. Gabi, Zer Da Zuretzat Kantua?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HNDxvQengys
---

2022ko Euskaraldiaren hasieran #kantubatgara Gorlizen izan da. Horren barruan, eta ekimenaren ohitura jarraituz, herritar bati inbitazioa luzatzen diote oholtzara igotzeko. Honetan @jonmaiasoria k niri, luzatu zidan, galdetuz: Zuretzat zer da KANTUA?
Kontzertu bat baino gehiago izan zen. 

Mila esker: @jonmaiasoria@pelloramirezofiziala8112 @gorkahermosa @nerea_quincocesochoa @nachosoto8929