---
id: tx-1017
izenburua: Joan Soseguz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_DyLmZ5nM98
---

Beste sasoi batean, bizitza,
denbora honetan ez, halako batean.
Beste hiri batera abiatu,
ahotsen artean zu ezagutu.
Baina hor ari zaizkit etxeko hotsak,
herriko kanpaiak,
hego haizea.

Gora eta behera dabila ene gogoa,
ez daki zuzen nondik nora joan.

Joan soseguz,
lasai joan,
haizea lagun da
aldaketan.
Joan soseguz,
lasai joan,
pausoa luze eman
leunean.

Ihesi nahian dabila ene burua,
ilargi beteak erakartzen ura.
Gora eta behera dabila ene gogoa,
ez daki zuzen nondik nora joan.

Joan soseguz...