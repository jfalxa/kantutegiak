---
id: tx-2495
izenburua: Zorionak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ltXp581VFxM
---

Egun pozgarri hontan
zorionak zuri
zure urtebetetzean
denon parte/tik
urte askotarako
Zorionak zuri
Zorionak Garrintzi
Zorionak beti