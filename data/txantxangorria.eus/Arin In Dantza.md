---
id: tx-3033
izenburua: Arin In Dantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XQSDwOT94JA
---

Trago bakar batez edan gendu(e)n gaba laburra, bion artean, olgetan
Iratzargailuaren musika: izaren-arteko dantzaren soinu-banda
Napoliko ragazzak bazeukan ariman sartunde tarantella,
bizi zen azkar, arin eta ariniketan... bizi dantzatzen bizitza dantza dala eta

 Questá tarantella che muore di pena
se non la si balla con velocitá
zai zoi bele, zapatan be,
atarako dogu txispa

 Ireki kupela emoi(o)zu kandela
lotu zaitez kalejirara
zai zoi bele, erdu zeu be,
agortuz doa denbora

 Denboraren pozoi maltzurrak ez deko prisa
Erlojuaren armiarmak ziztatuak gisa
Bizitza joaku arin batean inork ez euskun abisa
Arin (eg)in dantza, hasi da, antza, hilbeharraren trikitixa

 Hila bat pasata galdu neban bere bisita, (e)ta zita horren ordean
hauts zuri gramo biren trukean aldatu nindu(e)n, hori bai dok irautea...
Eta Napoliko neskatoa, zen bere izena bezain argia, 
bizi zen azkar, arin eta ariniketan... bizi dantzatzen bizitza dantza dala eta

 Errezitatua: 
Oraingo sasoiak jokatuko dira momentu batean lehenaldian
historia dan hori, historia izango da oroitzeko inork ez dagoenean
Erritmu frenetiko, ez itzi etsiko, ez ei dala betirako gaurkoa
hau kontzeptu klasiko, basiko, tipiko, topiko, hala be ardurazkoa

 Eta DANBA! Eta Big-Bang eztanda! Lur-planetan baztanga! Eta GANBAK! (e)ta txakurraren zaunga! hau tximino luzanga: bere txanda! Kanpai bateko danda!, hau parranda, banda, samba...! edanda: dzanga-dzanga! kriston ganga! Zure unea behin janda...betiko geldi zaitez lurpean etzanda!