---
id: tx-1018
izenburua: Gure Bestak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hmx_zfxj-yI
---

Itsasuko besta sakratuei omenalditxo bat ..belaunaldiz belaunaldi orroitzapen magikoak,...


Gure Bestak
Agorrila undarrean
Herriaren ohoretan
Apaindurak lehioetan
Esperantza bat airean
Aspaldiko edo gaurko
Gazte denbora garaian
Goxatuak hainbat joko
Grabatuak memorian
Bestetan egin irriek
Plazan tinkatu eskuek
Zintzoki eskaintzen direnez
Beiratzen gaitz guziez
Egon nintzan atzerrian
Zorion baten menturan
Konturatuz azkenean
Hemen nuela herrian
Eskutik hartu zintudan
Gau hartako olerkian
Landatua izarren pean
Gaur dantzan gure aurrean
Bestetan egin irriek
Plazan tinkatu eskuek
Zintzoki eskaintzen direnez
Beiratzen gaitz guziez
Ator ( zaia ) xuria soinean
makilarien atzetik
sartu ginen desfilean
bihotzak ximikaturik
Zonbat ezagun aurpegi
zonbat izar zonbat argi
ginauden ilargian kurri
orroitzapen maitagarri
Bestetan egin irriek
Plazan tinkatu eskuek
Zintzoki eskaintzen direnez
Beiratzen gaitz guziez
Kalapita gogorretan
galdu bat baino gehiagotan
hoberena eskaintzekotan
odolak bero askotan
Segituko dugu dantzan
ametsa beiratzekotan
aintzinekoak gogoan
Itsasuko bestetan