---
id: tx-3400
izenburua: Beste Gauza Bat Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eKJAk-_83SI
---

Lotsa kendu duzue, jadanik ez dizue axola. 
Mundua Zuen esku edukitzekotan edozein tranpak balio du. 
Zure jolaskideek badakite. 
Ez da Zure Etxea babesteko Asmoa, 
beste Gauza bat da. 
Ez da errugabeak salbatzeko gogoa, 
beste Gauza bat da. 
Sortarazi duzuen tristuraren berri badugu. 
Gutxi ausartzen dira Zuen kontra eskua altxatzen. 
hainbat isiltzen dira, ausardiak erantzunik badauka. 
Ez da Zure Etxea babesteko Asmoa, 
beste Gauza bat da. 
Ez da errugabeak salbatzeko gogoa, 
beste Gauza bat da. 
Zuen garaipenak orri Beltzak dira 
idatzita historian