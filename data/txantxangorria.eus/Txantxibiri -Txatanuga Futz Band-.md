---
id: tx-3180
izenburua: Txantxibiri -Txatanuga Futz Band-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iX0Y1Cqufe0
---

Txantxibiri, txantxibiri gabiltzanian
Elorrioko kalian,
hamalau atso tronpeta jotzen
zazpi astoren gainian.

Astoak ziren txiki-txikiak,
atsoak, berriz, kristonak!
Nola demontre konpontzen ziran
zazpi astoren gainian (bis).

Saltzen, saltzen,
txakur txiki baten karameluak,
Está muy bien!
esaten du konfiteroak (bis).

Cuando vamos a Otxandiano
karnabal egunian,
me cagüen la mar,
comeremos chocolate
sonbreruaren gainian.