---
id: tx-2453
izenburua: Baskonia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aRC5UESVRCg
---

ROTTEN XIII - Larraga, Tafalla (Basque Country)

"Baskonia" abestia Rotten Amairu-ren  "Oi! Baldorba" lehen diskatik hartuta.

Contact : rottenamairu@gmail.com 


Mendien artean dakusat eguzkia.
Egun berri baten lehenengo argiak
agerian uzten du lurralde zoragarri bat;
harizti, pagadi eta harri gorria.
Zekaleak landa soroetan
eta arrantza ontziak itsasaldean.

Nork azalduko ote digu zure jatorria?
Ez da latindarra ezta arabiarra,
are gutxiago frankoa edo godoa.
Nortasuna eta autogestioa,
auzolana bizimodu bezala
eta natura da gure jainko bakarra.

Oh Baskonia!
Erresistentzia historia,
zure baso ta larreek
gorde dute memorian.
Oh Baskonia!
Gudarien aberria,
zugatik dena eman duten
horientzako, betirako argia.

Mendeetan zehar borrokan ibilia,
sufrimenduarekin askotan hezia,
baina ez da hilko zure izenaren magia
gu mundu honetan gauden bitartian.
Ta zugatik altxatuko dira
herri honetan bizi diren neska-mutilak.

Oh Baskonia!
Erresistentzia historia,
zure baso ta larreek
gorde dute memorian.
Oh Baskonia!
Gudarien aberria,
zugatik dena eman duten
horientzako, betirako argia.

Baskonia, Baskonia
daramat bihotzean.


Lyrics:

I see the sun through the mountains.
the light of a new day shows up a wonderful land
Oak and beech trees, red stones,
cereals in the landscape and fishing boats on the coastline.

Who is going to explain our origins? 
It's is not latin, not arabic, neither frank nor goth, 
Own personality and autogestion,

Comunity-sense working as a lifestyle and nature is our only God.

Oh Baskonia, history of resistance that your forests and fields have kept in their memory.
Oh Bakonia,  fatherland of the warriors, eternal light for those who gave everything for you. 

It has always been fighting all along the centuries,
Often growing up with suffering
But the magic of your name will never die
meanwhile we are in this world
And men and women that live of this country will rise up for you.


Oh Baskonia, history of resistance that your forests and fields have kept in their memory.
Oh Bakonia, fatherland of the warriors, eternal light for those who gave everything for you.