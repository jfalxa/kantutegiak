---
id: tx-1034
izenburua: Baloreak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/geAXSrUYXUM
---

Kalera atera ta plazan naiz 
Lagunen esperoan
Bostekoa eman ta indartsu sentitzen naiz 
Luna agertu zaidan momentuan

Bakarrik ikusi dut mutil bat
Berria omen da herrian 
Enuke nahi nik leku berri batean 
Adiskiderik izan gabe bizi

Uoooooooo
Batera indartsuago gara 
Arazorik baduzu zatoz gurekin 
Denok izango gara baloreak

Irribarre handi hau zuena da 
Mila esker benetan
Sor dezagun ba mundu berri bat hemen 
Denontzat lekua izango duena

Utz ditzagun gailuak etxean 
Ta eraiki txabola bat 
Ametsen jaioterria izango da ta 
Zu ere ongi etorria izango zara

Uoooooooo
Batera indartsuago gara 
Arazorik baduzu zatoz gurekin 
Denok izango gara baloreak

Uoooooooo
Batera indartsuago gara 
Arazorik baduzu zatoz gurekin 
Denok izango gara baloreak

1, 2, 1, 2, 3, 4!

Uoooooooo
Batera indartsuago gara 
Arazorik baduzu zatoz gurekin 
Denok izango gara baloreak


Instagram: @baloreak

By DeepSoda