---
id: tx-583
izenburua: Gaur Gurutzatu Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C0qsd7AdU-E
---

Gaur gurutzatu gara
Sarritan antzera
Su bertan jezarrita
Ni kalean behera
Zure itxura ez da
Betiko berbera
Begien tristezia
Ohikoa ote da
Gaur ondora natortzu
Lagun egitera

Ez al daukazu seme
Inorako asmorik?
Lagunengana noa
Ez dago presarik
Zeuk be bazenduala
Dantza kuadrilarik
Ta urteak doazela
Konturatu barik
Zoaz gazte ez galdu
Nigaz denborarik

Esku hartu deutzut
Egunon amama
Zelan dabil gaurkoa
Gure auzoko dama
Aurpegi zimurtua
Present iragana
Berbarik ez, keinu bat
Zuzendu nigana
Irribarre honegaz
Esan dozu dana

Atzo larogeitabost,
Soinean jantzia
Zeinek esango leuke
Hain maja ikusita
Lobak ein eutzuela
Egunez bisita
Baina gaur bizitzagaz
Eukiela zita
Beitu amama zelango
Oskarbi polita

Denboraren jagole
Ta haren menpeko
Poztasunik balego
Zeugaz banatzeko
Musu bat emon doutzut
Irri bat hartzeko
Biharko lotu gara
Kartetan eiteko
Irabazita ondo
Galduta hobeto