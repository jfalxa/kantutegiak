---
id: tx-288
izenburua: Guantanamera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yKsJiGSeoVQ
---

Palmondoen inguruko
gizon zintzo bat naizenez
Palmondoen inguruko
gizon zintzo bat naizenez

Hil aurrez nahi dut betiko
bihotza gusto atseginez

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera

Gorri sutsu berde argi
dakar zuentzat bertsoa
Gorri sutsu berde argi
dakar zuentzat bertsoa

Orain zauritu bat denez
gorde leku du basoan

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera

Munduko pobre guztiak
ditut bide lagunak
Munduko pobre guztiak
ditut bide lagunak

Itsaso zabala baino
nahiago errekatxoa

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera

Nahiz uda, nahiz negu izan
larrosa zuri bat hazi
Nahiz uda, nahiz negu izan
larrosa zuri bat hazi

Adiskide franko batek
ez du gutxiago merezi

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera

Lehoi nabarrak aterpe
mendi lehor nabarrean
Lehoi nabarrak aterpe
mendi lehor nabarrean

Gorde leku oteatu
nire lagunarena

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera

Guantanamera,
guajira Guantanamera
Guantanamera,
guajira Guantanamera