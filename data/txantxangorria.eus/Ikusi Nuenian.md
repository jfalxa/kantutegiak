---
id: tx-1682
izenburua: Ikusi Nuenian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/u6k4hVZjhZA
---

Ikusi nuenean
nik zure ahoa
arrain txikiei dei egiteko
eman dit gogoa.

Ikusi nuenean
nik zure kokotza
kopla berrien  aide zaharrak
ostu dit bihotza.

Ikusi nuenean
nik zure sudurra
haize garbiak orraztu zuen
hostapeko lurra.

Ikusi nuenean
nik zure masaila
brontze gorritan eguna hasten
ez zegoen zaila.

Ikusi nuenean
nik zure begia
dena alderantziz kausitzen nuen;
hori da egia.

Ikusi nuenean
zure betazala,
olatu bitan zihoan zure
txalupa zabala.

Ikusi nuenean
nik zure kopeta
beste poema hasi beharko
merezi du eta!

I/ku/si nu/e/ne/an
nik zu/re ko/pe/ta
bes/te po/e/ma ha/si be/har/ko
me/re/zi du e/ta!