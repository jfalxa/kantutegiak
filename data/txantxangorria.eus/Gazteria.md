---
id: tx-1362
izenburua: Gazteria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/swqtC1K16rQ
---

Hementxe naiz
aurrean nauzu azkenean
gaztea naiz

baina ez zuk uste adina
zenbat bide zenbat gezur zenbat nire eskubide zenbat etsai zenbat lagun zenbat ideia

bidea sortu kateak moztu borrokaren zentzua dugu esku artean
muga gaindituz  ta lotsa galduz
orain da gure aukera
gazteok garela herriaren gura
gazteok batuta kate guztien koska
gazteon indarra aldatze ahozkoa
gazte mugimendua
segi segi horrela

bidea naiz bidean zuri begira esnatu naiz ta mugitzeko kemenaz
zenbat bide zenbat gezur zenbat nire eskubide zenbat etsai zenbat lagun zenbat ideia

bidea sortu kateak moztu borrokaren zentzua dugu esku artean
muga gaindituz palotsa galduz
orain da gure aukera
gazteok garela herriaren gura
gazteok batuta kate guztien koska
gazteon indarra aldatze ahozkoa
gazte mugimendua
segi segi horrela