---
id: tx-1067
izenburua: Gustuko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bEO9H-AqRtk
---

Nizuri Tazunerik 2017ko martxoan argitaratu zuen bere lehenengo diskoa "Musika Ala Hil". Gaurkoan, diskoaren baitan agertzen den "Gustuko" abestiaren bideoklipa aurkezten digu taldeak, euskal rapan aritu diren talde eta kolektibo ezberdinei omenaldia eskeiniz.

Nortzuk dira humilak, leialak , zintzoak, 
nortzuk lortu zuten irekitzea leihoak
freskatzeko aire geldoak
guuuuu, guuuuu

Guu….. gu….. (x4)


Dng da squad, Oreretakoak
hegazkin honetan hegan dejate de swag
faltsu jatorrak, postureo latz
usaintzen dugu urrunetik beraien kirats

beraien kirats, trepalariak
jokoan gaude zuek atzean hablando de más
gu beti aurre goaz, koño
zaiena egiten erraz
baino badakigula ez degula ezer asmatu trankil, relax


Nortzuk dira humilak, leialak , zintzoak, 
nortzuk lortu zuten irekitzea leihoak
freskatzeko aire geldoak
guuuuu, guuuuu

Guu….. gu….. (x4)


Beste bat, beste bat, beste bat, 
diles ya, diles ya
aro berria, garaiera, dugu gainean etorriko da
gailurrean ez gaude, baina fabrikatzen skills’
eskerrik asko daudenei

Zurekin flipatzen estatu lapurtzen lortzen duzu dana
hemen botatzen zuzenean
egunsentian deitzen, zurekin zoratzen, zer gertatzen da
aunque no sea facil, 
badakit que hay poco como tu ahi fuera

Quieres, que siga y no aguanto lejos como si quisiera
que no estás encima, ni abajo, si al lado
que no est´as encima ni abajo si al lado

lotsagabe goaz, se apropian y no les queremos
cuantas mas seamos, mejor pa’ nosotros, borrokan,
el barrio se entera

Gau ilun-iluna, Nizuri Tazuneri argitzen
DNG la droga del pueblo mira lo que saca esta kitchen’
musikaz ari gara kabron, no vengas pasado no queremos cliches
rulan con esencia amor y por algo será que aquí insisten