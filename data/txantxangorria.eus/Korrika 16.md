---
id: tx-971
izenburua: Korrika 16
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-1HfudNtdjc
---

Rami hasi zen Afrikan
eta Andoni Gernikan
Maddalen berriz Sohütan
Evaristo Amerikan
euskara herrian doa
Euskal Herria korrikan
euskara herrian doa
mundu bat euskal herritan

Mikel oinez gehienetan
Pepe eta Elvira trenetan
Mohamed lanetik bueltan
ta Nekane bizikletan
mundu bat doa korrika
hizkuntzaren bideetan
mundu bat doa korrika
euskararen herrietan..

Karriketan korrikan
korrika karriketan
doa euskara… uhoo
karriketan korrika
korrika karriketan
doa mundua… uhoo

Karriketan korrika
korrika karriketan
doa euskara… uhoo
karriketan korrika
korrika karriketan
doa mundua… uhoo

Ongi etorri Aitor
galdu zuen ta bila dator
ongi etorri Sarai
heldu arren korrika darrai

Kantatu dezagun
Ongi etorri lagun...
Euskara herrira
Zure herrira zatozena...

Orain euskara da soa
euskaraz doa pausoa
euskara da itsasoa
zerua lurra auzoa
euskara herrian dago
euskaraz mundu osoa
euskara herrian dago
euskaraz mundu osoa

Karriketan korrika
korrika karriketan
doa Euskara… uhoo
karriketan korrika
korrika karriketan
doa Mundua… uhoo

Karriketan korrika
korrika karriketan
doa Euskara… uhoo
karriketan korrika
korrika karriketan
doa mundua… uhoo

Bidaia zugan bada
eta bidea zeu bazara
mundua da euskara
zatoz mundu bat Korrikara

Kantatu dezagun
Ongi etorri lagun!
Euskara herrira
Zure herrira zatozena...