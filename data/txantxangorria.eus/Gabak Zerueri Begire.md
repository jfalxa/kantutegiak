---
id: tx-1847
izenburua: Gabak Zerueri Begire
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2nwy3z7-wjo
---

Buelteu dire eskotiek
eta buelteu da pekatu sasoie
Gabak zerueri begire
ta zure marradun minigonie.

Amaitu da ilunien triste bizi beharra
ez dot maite xiri-mirixe.
Eguzkixeri atiek zabaldu egingo deutsodaz
zabaldu eta ku-ku egin.

Eo, eo! Gorputze mobidu
Eo, eo! Odola garbittu
Eo, eo! Zugaz maitemindu
Eo, eo! Zugaz maitemindu.

Hasi dire egun luziek
parkietako kanuto usaine
ze hosti deko hego haiziek
ze hosti polen puta honek.

Salto egin nahi dot,
erropak kendu
biluzik dantza egin,
ehun txiribuelta emon.
Maitte zaittut ilargi azpixen
maitte dodaz lainoak eta
txorixek buruen.

Eo, eo! Gorputze mobidu
Eo, eo! Odola garbittu
Eo, eo! Zugaz maitemindu
Eo, eo! Zugaz maitemindu.

Buelteu dire eskotiek
eta buelteu da pekatu sasoie
Gabak zerueri begire
ta zure marradun minigonie.