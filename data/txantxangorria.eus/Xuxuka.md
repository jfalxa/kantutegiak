---
id: tx-967
izenburua: Xuxuka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1Js_Szpbp9w
---

Musika: Herrikoia (Ameriketako indiarrena)
Letra: Koldo Celestino


Guztiongandik ikasteko,
xuxuka, xuxuka hurreratzen gara;
nos acercamospara aprender,
unos de otros, todos a la vez

Nina puka, yana puka,
ketxua, inka eta aimaraz,
urdin, gorri, zuri, beltza,
dakizkigu euskaraz.

Lagun anitz bildu gara
kanta eta jolasean,
Maddi eta Pachamama
dabiltza gure artean