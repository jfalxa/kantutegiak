---
id: tx-896
izenburua: Poutpurri Mazedonia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fgRurwhPbtY
---

Ba, ba, ba, babarrunak 
ba, ba, ba, babarrunak 
jan behar dia.

Nahi badezu hutsik 
ta ez daude ezer gaizki 
txorixoa eta olki 
azakin nahasturik. 

Ba, ba, ba, babarrunak 
ba, ba, ba, babarrunak 
egunero bai jan behar dira 
txuri ta gorriak 
ta indarra egiteko 
txandaka baita beltzak 

Ba, ba, ba, babarrunak 
ba, ba, ba, babarrunak 
jan behar dia. 
Nahi badezu hutsik 
ta ez daude ezer gaizki 
txorixoa eta olki 
azakin nahastukin. 

Jan zak, jan zak, jan bapo 
jan ta edan ta gero lo 
digestioa egiteko 
dantzan egin bi saio 
bat rockarekin zoro 
biribilketa obeto. 

Jan zak, jan zak, jan bapo 
jan behar bizitzeko 
haurrak jan behar hazteko 
haziak irauteko 
batuzk bizi jateko 
birtutea erdin dago. 

Jan, jan, jan bapo jan ta edan 
ta gero lo digestioa egiteko 
dantzan egin bi saio 
bat rockarekin zoro 
biribilketa obeto. 

Behin batian behi pinttoa 
hegan zan hasi 
oilarra arrantzan kantari 
astua berriz kikiriki 
baitare arrautzak ipini 
hau dena baita egi bizi 
nahiz gezurra dirudi. 

Errenak korrika irabazi 
mutua parlamentutik bizi 
gorrak loiolan jai jai ikasi 
itsuak hauek denak ikusi. 

Behin batian behi pinttoa 
hegan zan hasi oilarra 
arrantzan kantari astua 
berriz kikiriki 
baitare arrautzak ipini
hau dena baita egi bizi 
nahiz gezurra dirudi. 

Seigarren diska amaitu degu 
hemen joan dira denetatikan 
ezer ez kendu zerbait erantsi 
zuk nahi badezu hurrengorako 
zer kantzatzeko eman aholku. 

Gastronomi eta intsumisu 
maitasun dantzak jorra ditugu. 

Zazpigarrena 
ia zai degu 
uste baino lehen 
IZ eguna 
iristen zaigu 
baina hementxe 
orain zin dagizut 
osorik alaba 
izango dela 
ta ez lengusu. 

Seigarren diska amaitu degu 
hemen joan dira denetatikan 
ezer ez kendu zerbait erantsi 
zuk nahi badezu