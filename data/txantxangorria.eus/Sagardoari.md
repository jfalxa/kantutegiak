---
id: tx-1717
izenburua: Sagardoari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-KUVfyZNoCE
---

Bedeinkatua izan dedila
sagardoaren grazia
bai eta ere kupida gabe
edaten duen guzia:
edari honek gizon askori
ematen dio bizia,
hau edan gabe egotea da
niretzat penitentzia.
Lehenengo sartu tabernan eta
ateratzen naiz azkena,
egun guztian zurrutean eta
ez zait betetzen barrena ;
hau da edari maitagarri bat,
arras piztutzen nauena,
joxepa karmen bihotzekoa
bete hamabigarrena.
Astelehena det jai bat hartua
pekatu mortalekotzat,
asteartean beharrerako
gogorik egiten ez zait ;
asteazkena, ai nire pena,
oso pereza sartzen zait,
modu honetan aste guztia
alferkerian joaten zait.