---
id: tx-3324
izenburua: Oñatiarren Esku Ere Badago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9GLi0nqA4RE
---

Kanta zaharrak dioen bezala
ura bere bidean doa
goi mendietako ur-xirripa
kanpo emariz ibaitu da
mugarik ez jarri urei
utzi libre beheratzen
milaka urtez egin legez


Guk ez dugu aldarri egiteko
heroi edo konkistarik.
Borondateak elkartzen gaitu,
herri izan nahiak»,

Hormak eta harresiak
ez ditugu maite
elkarrekin bizi 
nahi dugu soilik
ez dugu beste inor
mendean hartu nahi
etorkizuna hautatu ez besterik


Hormak eta harresiak
ez ditugu maite
elkarrekin bizi 
nahi dugu soilik
ez dugu beste inor
mendean hartu nahi
etorkizuna hautatu ez besterik

Har dezagun bidea
gure esku dago ta
milaka lagun oinez
eskutik oratuta
jende zoriontsua
herri libre batean.
Herri libre batean!

Rosa, Izaro, Olatz, Arane, 
Carlos, Bittoren, Maddi.

Har dezagun bidea
gure esku dago ta
milaka lagun oinez
eskutik oratuta
jende zoriontsua
herri libre batean.
Herri libre batean!

Hiritar desberdinak
ametsak ditu biltzen
etorkin, arrantzale,
langabetu, ikasle,
nekazari, langile,
musikari, idazle,
erretiratu, ume

Amaia, Xabier, Antoine 
Garazi, Gloria,Lur, Amaiur,..

Har dezagun bidea
gure esku dago ta
milaka lagun oinez
eskutik oratuta
jende zoriontsua
herri libre batean.
Herri libre batean!