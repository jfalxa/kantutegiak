---
id: tx-909
izenburua: Nahi Genuena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4h-Q77cToQs
---

Gotzon Barandiaranek idatzitako poemak izan ziren Katamalo ikuskizunaren abiaburua. Poema horiek kantu bihurtu zituzten BBK Bihotz Bakartien Klubeko musikariek eta Gorka eta Nerea Urbizu neba-arrebek. Eta Katamalo izeneko ikuskizuna estreinatu zuten 2007ko otsail akaberan. Izen bereko liburua eta diskoa atera zituzten geroago, 2007ko azaroan. Urtebete geroago, abenturaren lekukotasuna ematen duen Erantzi diskoa argitaratu zuten, eta 2011n azken kontzertuak eman zituzten[1].

2014an Durangoko Plateruenan grabaturiko kontzertua argitaratu zuten.


Ezpainak ardotan, irri aizuna
pozten zarela baina ez nauzula gogoan
ankerra berez,
bihotz gabe ez bagina sinez
ikusi arte egongo gara
zu naiz
urtetan nahi genuena
zu naiz
urte bete gutun ez da iragan
lanbroak hartu dizu esperoa
argi ontzi itsua bularrean daukat
zu naiz
urtetan nahi genuena
zu naiz
nahi genuena
zu naiz