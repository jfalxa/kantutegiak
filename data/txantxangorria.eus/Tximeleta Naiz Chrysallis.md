---
id: tx-1199
izenburua: Tximeleta Naiz Chrysallis
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LqjebUq-a-U
---

Chrysallis  Euskal herria
Musika: Xabier Zabala

CHRYSALLIS...
Crhisallis naiz zientzialarientzat
eta handitzean hegan...
munduak ez du mugarik niretzat

Tximeleta da nire izena
eta pinpilinpauxa naiz
baita pitxita edo marisorgin
maripanpalona ere banaiz
txilipitaina esaten didate
txitxipapa ere berdin
txintxilinpalo edo mitxirrika
txiruliru eta pinpirin
beldar jaio nintzen eta
ametsak betez
orain hegalak astintzen
ditut mila kolorez

Tximeleta da nire izena
eta pinpilinpauxa naiz
baita pitxita edo marisorgin
maripanpalona ere banaiz
txilipitaina esaten didate
txitxipapa ere berdin
txintxilinpalo edo mitxirrika
txiruliru eta pinpirin
beldar jaio nintzen eta
ametsak betez
orain hegalak astintzen
ditut mila kolorez

Askatasuna maite dut
bizi naiz alai
lagun bat behar baduzu
dei iezadazu lasai!!

Tximeleta da nire izena
eta pinpilinpauxa naiz
baita pitxita edo marisorgin
maripanpalona ere banaiz
txilipitaina esaten didate
txitxipapa ere berdin
txintxilinpalo edo mitxirrika
txiruliru, liru, liru eta pinpirin pinpirin

Aizu! beldar jaio nintzen eta
ametsak betez
orain hegalak astintzen
ditut mila kolorez

Askatasuna maite dut
bizi naiz alai
lagun bat behar baduzu
dei iezadazu lasai!!
Dei iezadazu lasai!!

txiruliru, txiruliru, 
Dei iezadazu lasai!!