---
id: tx-3131
izenburua: Langile Bat Naiz -Gabi Eta Jabi-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T5csuRn-GtM
---

Kanta honen bertsioa "Txatxangorri eta Warren", Gabik eta Jabi Etxeandiak duela 32 urte kantatzen zuten. Kanta, berez, Imanol Larzabalek kantatzen zuen "Imanol Etchegaray" ezizenez.