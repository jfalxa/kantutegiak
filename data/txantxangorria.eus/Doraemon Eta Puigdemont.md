---
id: tx-1237
izenburua: Doraemon Eta Puigdemont
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2eNS3b32eQ8
---

Katalunian egunon
eta Belgikan gabon
Bruselako leihoan
bost mamu eta Puigdemont

Zer pozik bizi diren amets batetik bestera
ezker_eta eskuinekoekin batera

Carles Puigdemont magoa ere ba da.
Bai! Egia da hori!

Pirin, pirin, pirin, pron pon pon
gora eta gora beti,
Carles Puigdemont!

Pron pon pon, gurea bukatu da
eta hor konpon!

Pron pon pon, zorte on denei Maulen
eta Durangon.