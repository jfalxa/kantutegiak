---
id: tx-3173
izenburua: Eusko Gudariak -Ibai Rekondo-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j7Y4CuSc45s
---

Eusko gudariak gara
Euskadi askatzeko
gerturik daukagu odola
bere aldez emateko.

Eusko gudariak gara
Euskadi askatzeko
gerturik daukagu odola
bere aldez emateko.

Irrintzi bat entzun da
mendi tontorrean.
goazen gudari danok
Ikurriñan atzean.

Irrintzi bat entzun da
mendi tontorrean.
goazen gudari danok
Ikurriñan atzean.