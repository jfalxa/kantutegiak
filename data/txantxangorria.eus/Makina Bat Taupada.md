---
id: tx-2352
izenburua: Makina Bat Taupada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xiAuhloUtMQ
---

EHIGE - Euskal Herriko Ikasleen Gurasoen Elkartea
 
2019ko ekainaren 2an Errenterian ospatuko den Euskal Eskola Publikoaren Jaiaren bideoklipa.

*Hitzak: Oneka Oliveri.
*Musika: Yon Figueroa.
*Abeslariak: Estitxu Pinatxo eta Yon Figueroa.
*Bideokliparen zuzendaritza: Ayar Salazar, Gotzon Santaolaya.