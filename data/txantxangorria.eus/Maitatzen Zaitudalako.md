---
id: tx-1931
izenburua: Maitatzen Zaitudalako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H3vME1n1dhc
---

Maitatzen zaitudalako
kantatzen dizut zuri
zu zaitut negu beltzean
eguzki epel zuri
sargori denean berriz
maitasunezko euri.

Guz elkarri zenbait kontu
eta zenbait irrino!
iruditzen zait gerala
ilargitan bi laino,
egunez bihurtzen gera
zu ta ni bi txorino.

Elkarren begiratuta
hasten gera kantari
orduan deitzen diogu
maitasun-haizeari
eta harek jartzen gaitu
firi-firi dantzari.

Dantza dezagun, ba, dantza
maiteño bihotzeko!
mundura ez ginen jaio
gu triste bizitzeko
baizik tristura guztiak
kantuz poz bihurtzeko.