---
id: tx-1123
izenburua: Mirentxu Patxaran
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y8T8GYMU6KY
---

Udako gau ero bero bero batean
San Ferminetan Manian ostean
Txosna batean trago bat eskatzen
Patxarana besterik ez zuen edaten

Bere ilegorriaz maitemindu nintzen
Bere begi berdeak begira nituen
Bere izena ez dut gogoratzen
Ezizena zuen.....................MIRENTXU PATXARAN!!

Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!

 Piripipipi............... lorolorololo
Piripipipi............... lorolorololo
Piripipipi............... lorolorololo
Piripipipi............... MIRENTXU PATXARAN!!!

Ezta Alazne, ezta Eneritz,
Ezta Maitane, ta ezta Leire
Ezta Ohiane, ezta Olatz
Bere izena da.................MIRENTXU PATXARAN!!!!!!

Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!


Ez du izenik Euskal herriko neskatxak
Bere gogoa eta indarra!
Bere borrokarako kemena!
irribarre ta alaitasuna!

Begira begira begira geratu giñenean
Txosna batean gure ondoan
Dantzan zenbiltzan gure inguruan
Ta izena zuten...................MIRENTXU PATXARAN!!!!!

Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!
Oooooooooooo..........MIRENTXU PATXARAN!!!!