---
id: tx-150
izenburua: Zazpi Bala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gq7uFj_w9MU
---

Guda Dantza euskal musika taldearen laugarren lanaren aurrerapen abestia. 
Musika eta hitzak: Guda Dantza
Produkzioa: Urtzi Iza
Grabaketa, nahasketa eta masterizazioa: Eten Espazio Sonoro
(Baga-Biga, 2022)


Itzaletan ikusten gara gehiegi,
baina gaur ez dakit zer egin.
Mugitzen zaren inguru usteletatik
deseroso sentitzen naiz ni.
Ezin dizut idatzi,
oraindik ez dut erantzun,
ta zuk guztia argi duzu.
Autoa ta pistola
lortu dituzu.
Aukera ezin dugu galdu.
Mila baldintzen menpe bizi zara zu,
zure beldurren esklabu.
Polizia atzetik badatorkigu,
arren atzera ez aditu.
Ez gara Bonny ta Clyde,
baina izango gara gai.
Patuak batu gaitu.
Erre daigun erregai
esan zenidan noizbait
ta orain ez zaitut entzun.
TA NOLA ESAN
EGIN DEZAKEGU
HIRIA LOKARTZEN DENEAN
ZER ESAN,
EGIN DEZAKEGU,
ZAZPI BALA DITUT KOLKOAN.
Nigandik ihesean nabilela,
aurkitzen zaitut bidean.
Bide okerrean,
bekatutik hiru minutura.
Hor zara gaua
estutzen denean.
Ez gara Bonny ta Clyde,
baina izango gara gai.
Patuak batu gaitu.
Behingoz onartuko dute garena,
kometa bat, argi izpi bat itsuen artean gara.