---
id: tx-1888
izenburua: Markoxen Txerrixe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nXF_sF5k840
---

Markosek egin eban
lastozko zubixe
bertatik pasatzeko
bera ta txarrixe
hasi ziran pasatzen
jausi zan zubixe
Markosek hartu eban
kristoren ostixe.

Errekatik urten zan
goraino bustirik
gainera sikatzeko
ez zan eguzkirik,
egindakoarekin
guztiz damuturik
berriz ez dau egingo
lastozko zubirik.

Horrelaxe hil jako
makina bat pizti,
baina aurrera doa
euri zein eguzki,
berari bardin jako
busti zein ez busti
errekan zehar doa
txarri eta guzti.

Erreka baztarrean
guardarekin kezka
lizentzi barik egin
ete dauen peska,
eskuak gora jasoz
Merkosen protesta:
"Hau neure txarrixe da
arrankarixe ez da".

Erreka pasa eban
berak ozta, ozta,
gero guarda dala-ta
txarri horrek ospa,
Markos bere atzetik
mekaguen dioska:
"Erreka pasatzea
ez jar gitxi kosta".

Markosen txarrixe zan
artoz hasitakoa
San Martinetarako
berebizikoa,
berari alde eginda
hor igesi doa:
Agur solomo eta urdaiazpikoa.

Txarrixe maite eban
eta ez besterik
munduan ez zegoen
holako piztirik,
pentsatzen dago hilda
ala dan bizirik
txarrixe topau ezin
holako ostirik.

Bere txarrixen bila
behera eta gora
igesi egin deutso
ta ez daki nora
eta pentsamentu hau
datorko gogora:
"Gatzelaskaren baten
gordeta egongo da"