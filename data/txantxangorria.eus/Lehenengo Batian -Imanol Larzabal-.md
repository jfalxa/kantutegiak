---
id: tx-3152
izenburua: Lehenengo Batian -Imanol Larzabal-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t2XdAGYAx6w
---

Lehengo batean,
kalearen erdian,
Benta-Handi erdian,
Xabier anaia, hil zuten.
Lehengo batean,
kalearen erdian,
Benta-Handi erdian,
Xabier anaia, hil zuten.
Eta nolaz gu,
gu, eguneroko patxara merkean
oso lasai bizi geran...
geure izerdia xurgatzen duten
beste jende hoiek
bihar ere,
berriro ere,
beste bat hilko dute.
bihar ere,
berriro ere,
beste bat hilko dute.

Lehengo batean,
kalearen erdian,
Benta-Handi erdian,
Xabier anaia, hil zuten.
Lehengo batean,
kalearen erdian,
Benta-Handi erdian,
Xabier anaia, hil zuten.

Eta nolaz gu,
gu, eguneroko patxara merkean
oso lasai bizi geran...
geure herria zapaltzen duten
beste jende hoiek
bihar ere,
berriro ere,
beste bat hilko dute.
bihar ere,
berriro ere,
beste bat hilko dute.