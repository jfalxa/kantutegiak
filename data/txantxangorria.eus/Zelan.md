---
id: tx-654
izenburua: Zelan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3yDWk0iui7M
---

Zelan, nora, noiz, nundik etorko dire
olatuk gaur goizin
nundik etorko dire
olatuk gaur goizin.

Gorputze zatitxute, olatuk edo biharrak eraginde
begijek gorritxute, keiek edo kresalak ziketute.

Zelan, nora, noiz, nundik etorko dire...

Banoie autun osti gustire bidien erreilak galdute
ostabe lanera, beste zortzi ordu inpernuen.

Zelako bizimodue, orduek gero eta luziauek
zoratu ein bidot, egunek gero eta laburrauek.

Zelan, nora, noiz, nundik etorko dire...