---
id: tx-2818
izenburua: Eskolharria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OADFTTZSwaQ
---

Sortu nintzen ur tanta,
sortu ginen ur tanta
Urumeak darama
doanaren kanta

Gure bidean behera
Uholdean nahiz urri
Errioa jazten da
iturriz iturri
Sortzen eskolHarria biharko herria!!!


2.estrofa
Mendian hasi eta
Itsasoan amaitu
Herriz herri bilduak
Aberasten gaitu

Mendian hasi eta
Itsasoan amaitu
Bukaerak hasera
Lotu behar baitu
Sortzen eskolHarria biharko herria!!!


estribilloa
Errioa zaharra da, eta urak berri
Ikastola zaharra da,
Tantaz tanta, harriz harri, biharko herri

Errioa zaharra da, eta urak berri
Ikastola berria da,
Tantaz tanta, harriz harri, biharko herri


3. estrofa
Haizeak dakarzkigu
laino mota guztiak
Tantaz tanta munduan
Bilduta bustiak

Mundu zabal bat doa
Errioan behera
Urtez urte sortu den
Ibilbide bera
Sortzen eskolHarria biharko herria!!!