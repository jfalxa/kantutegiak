---
id: tx-1627
izenburua: Aitarik Ez Dut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_y7wHUVZvbE
---

Nigarra begietan, ahoa zipila
boza kasik galdua, hurbil hetsidura.
Kantatu nai dautzuet neure bihotz-mina;
izigarria baita, gertatzen zautana ...

Aitarik ez dut
ama hiltzen ari.
Haurrideak aldiz
bakoitza bereari ... (bis)

Lehengo denboretan haurrideak oro
aitamekilan ginen aterbe bereko.
Bainan batasun hori ez zen luzarako,
arrotzak baitirade gutaz jabetuko.

Aitarik ez dut
ama hiltzen ari.
Haurrideak aldiz
bakoitza bereari ... (bis)

Urteak eta urteak "haundien" menpean ...
anaiak bizi gara etxe zatituan.
Aspaldikoa da bai gure pairamena,
baina ez dugu galtzen, oi, itxaropena.

Aita bat ukanen dugunean
aita bat ukanen dugunean
ama biziko da
haurrideak ere
herr berriko libertadean
herri berriko libertadean
mail gabeko gizartean ...

Aitarik ez dut
ama hiltzen ari.
Haurrideak aldiz
bakoitza bereari ... (bis)

(Daniel Landart)