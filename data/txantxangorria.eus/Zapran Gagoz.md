---
id: tx-2417
izenburua: Zapran Gagoz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MxjLXWwuFFc
---

Zapran gagoz zapran gabiz
Lekitton zapran goaz!
Gozatu geure eskolaz
Euskal Eskola Publikoaz.

Bitan bat gu bizi gara
herri eta itsaso
gure eskolak batzen ditu
ikasle ta guraso.
Publokoak bardintzen dau
maila ez bardin asko
hemengitxi daukonak be
guztia leike jaso.

Bits zurien kresalean
dator euskal lurruna
ikasteko grina eta
poza sartzen deuskuna.

"Zapran gagoz" Euskal Eskola Publikoaren 27. Jaiaren bideoklipa (Lekeitio, 2018).
Hitzak: Xabier Amuriza.
Musika: Unai Egia eta Iñigito "Txapelpunk"
Ekoizpen artistikoa, grabaketa eta muntaia: Xabier Villalba (Traola produkzioak).

Bideoklipean parte hartu dute Lekeitioko haur eskola, eskola eta institutuko ikasle, irakasle eta gurasoek, baita hainbat herritarrek ere.

Batuz danen ahalegina
eta alaitasuna
lantzen dogu herri baten
mundu baten zentzuna.