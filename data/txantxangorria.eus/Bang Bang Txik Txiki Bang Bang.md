---
id: tx-1388
izenburua: Bang Bang Txik Txiki Bang Bang
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FByfiR0aB_Y
---

Gehiago nahi dot eta
igo bolumena
gorputzek eskatzen deust
martxa, martxa

Bang-bang txik-txiki bang-bang
buelta ta bueltak eman
bang-bang akabu bako
dantzan, dantzan

Segi, segi, dantzan
ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Zelan begiratzen dozun
ta zelan mugitzen zaren
gauaren erregina
martxa, martxa

Bang-bang txik-txiki bang-bang
erritmoz beteta dago
bang-bang akabu bako
dantza, dantza

Segi, segi, dantzan
ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Bang-bang txik-txiki bang-bang
 txik-txiki bang-bang
 txik-txiki bang-bang...



Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua


Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua, sua