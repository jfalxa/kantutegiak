---
id: tx-1020
izenburua: Bart Ikusi Dut Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jSuFLomuVwY
---

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean

Euskaldunen portzentaia
txikia izan arren.
zeu ere kabitzen zara;
paradoxak zer diren.

Bart ikusi dut euskara
tean.teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean.


Berde, abertzale, gorri,
ezker zein eskumako.
hizkuntzaren bila irten
galdu zaizuelako…

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean.
Maiz galdetzen da:
“euska/ak
balio du munduan?”
euskarak eurak munduak
ez balira moduan.

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean

Euskarak bete zituen
atzo sei milioi urte.
beste sei milioi beteta
gaztetuko genuke.

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean

Bart ikusi dut euskara
teilatu laranja batean
gutaz pentsatzen ari zen
saguzar herrenen artean

Bart ikusi dut euskara
teilatu laranja...