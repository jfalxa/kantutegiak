---
id: tx-1191
izenburua: Eserita Mesedez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/txno-bw5-L4
---

Asperturik nago
hau narraskeria
paperez leporaino
zipristinez beterik.

Asperturik nago
hau narraskeria
sekulako kiratsa
merezi ote dut nik.

Ai! Ai! Ai! Ai!
Ai! Ai! Ai! Ai!

Pixari ezin eutsi
Komuna libre behar dut eta.
Atera zaitez hortik!

Ai! Ai! Ai! Ai!
Ai! Ai! Ai! Ai!

Kakari ezi eutsi
Orain nire txanda da.
Atera zaitez hortik!

Asperturik nago
hau narraskeria
paperez leporaino
zipristinez beterik.

Asperturik nago
hau narraskeria
sekulako kiratsa
merezi ote dut nik.

Txukun, txukun
erabilita 
garbitzeko lanik ez
neska nahiz mutil
andre nahiz gizon
eserita, mesedez!

Komunean gaudenean
denok berdin garenez
berriz esango dizut
eserita, mesedez!

Ai! Ai! Ai! Ai!
Ai! Ai! Ai! Ai!

Pixari ezin eutsi
Komuna libre behar dut eta.
Atera zaitez hortik!

Ai! Ai! Ai! Ai!
Ai! Ai! Ai! Ai!

Kakari ezi eutsi
Orain nire txanda da.
Atera zaitez hortik!