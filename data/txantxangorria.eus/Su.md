---
id: tx-103
izenburua: Su
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bPl5Fslf4Rs
---

Kaleak su
Hartu du
Ez naiz beldur
Erretzen ikusiko ahal dugu?
Uuh...
Ez zaitut ezagutzen oraindik
Zertara etorri zaren
Badakit
Inork ez de/zan aldatu
Norbere lekua
Erregeak ikusten du itsuen munduan
Praka beltzak jarri
Estali burua
Ikusteko zein ederra den paradisua
Non zaude zu
Non zaude zu
Non zaude zu
uuuh...
Non zaude zu
Maitasuna eta herra
Gauza bera dira
Bi animali goseti elkarri begira
Bi suge
Bi hegazti
Ta kalea, tira
Lurretik eta zerura su hartzen ari da
Non zaude zu
Non zaude zu
Non zaude zu
Non zaude zu

Su, uuuh...
uuh...
uuh...

Kaleak su
Hratu du
Ez naiz beldur
Erretzen ikusiko ahal dugu?

uuh...


Ez zaitut ezagutzen oraindik
Zertara etorri zaren
Badakit