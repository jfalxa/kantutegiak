---
id: tx-1298
izenburua: Gernika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yelshm4eo7Y
---

GERNIKA Hitzak: Basarri Musika: Herrikoia

 Milla ta bederatzireunetan ogei ta amazazpia, euskotarrentzat urte bellz illun, triste ta negargarria, auts biurturik utzi zutena Gernika zoragarria, eusko reliki maitatuenak gordetzen zitun erria! Apirilleko illarekin zan ogei ta seigarrenean aiñ juxtu ere peri eguna astelen arratsaldean; kale baserri millaka anai gizatasun ederrean, plaza inguruan arkitzen ziran lengo oitura zarrean Kanpantorreko ezkillak asi ziran dinbili danbala, goiko piztien motor burrunda entzuna zan bereala; emakume ta ume gaxoak kurrixka zeriotela eriotzari iges nairikan nora jo etzekitela! Bertsoz nork esan bonba txar aien indar gaiztozko danbarra! Osorik etzuten utzi beintzat bizileku bat bakarra; ta dana txetu zuten garaian alde guzitik sugarra, erru-gabeko gixaxoentzat orra or azken ederra! Bizi ziranak igesten zuten sutegi zakar artatik ta egazkiñak tiro ta tiro eten gabe goietatik; kristau gorputzak alde batetik beso buruak bestetik, odol gorrizko itxaso artan etzan ikusten besterik. Millatik gora izandu ziran arnasa utzitakoak, ez jakin zenbat txit zauriturik eri gelditutakoak; ta danetatik geien-geienak andrezko ta umetxoak, gerra txar ontan erru galantik izango zuten gaxoak! Puska dituzte mendi ta erri apain eta maitekorrak puska dituzte basetxe txuri gure kabi on xamurrak, ta zirtzildurik kaletan utzi bai euskaldunen ezurrak Alemanitik gu txikitzera etorritako maltzurrak! Basati gaizto odoltzaleak atzerritikan ekartzen tokirik zoragarrienari txikituta su ematen; auts egindako gure Gernika antsika dago esaten Franco ta bere laguntzalleak zein biotz ona daukaten. Ikusi zuten ainbat kristau on odolustu zituztela ikusi zuten berdin-gabeko pekatua zeukatela; ikusi zuten mundua kontra jeikiko zitzaietela eta orduan zabaldu zuten gorriak egin zutela. Ezta ez alperrikako izan or ikusi dan odola pentsa oraindik emaitz ederrak ematekotan dagola; eusko gogoa armez ta indarrez ezin il leike iñola Gernika erre ziguten baña zutik daukagu arbola.