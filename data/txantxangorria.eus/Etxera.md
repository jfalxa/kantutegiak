---
id: tx-163
izenburua: Etxera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/loWZgF_4HhE
---

DES-KONTROL - ETXERA (LAGUNAK)

Hemen “Etxera” abestiarekin egindako bideoklip berria! Bertan lagun askok hartu dute parte:

Endika - Rotten XIII
Lander eta Olli - Brigade Loco
Peio - Orreaga 778
Remen - Kaleko Urdangak
Andoni - Rebel Noise
Oxel - Kontralde
Xagu eta Tronpi - Iheskide
Asier Sala
Fermin Muguruza
Garazi Barrena
---------------------------------------------------------------------------
Nahasketak: Haritz Harreguy
Masterizazioa: Antonio J. Moreno
Hitzak: Peru Aranburu
Produkzio eta edizioa: 168 Ekoizpenak

Mila esker guztioi eta, bereziki, Asier Elgezua, Velasko, Leire eta Arrasateko asteartetako enkarteladako kideei, zuen laguntzagatik!!
--------------------------------------------------------------------------------------------------
Hitzak:
Bide zahar eraman nazazu
Sustraiak ditudan lurretara
Erraietan sua eta kea
Norberaren gudu zelaia

Oin hauek badoaz etxerako bueltan
Ibili nahi ote dute gaztetan ibiltzen ziren auzoetan

Sutondoan kate eta kresalak
Lagun zaharrak goizaldeko trenak
Ez da ezer aldatu eta guzti-guztia aldatu da

Hormetan, doinuan, presoak kalera
Helmuga bat ahal dut hori ala ibilera

Behe-laniotan nabil bideak ikasten
Baina oinez ibiltzen ez da inoiz ahazten
Gaur bizirik nago, bihar zain zer dago?
Ez dut behar, ez dut behar gehiago