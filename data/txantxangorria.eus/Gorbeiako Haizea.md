---
id: tx-1543
izenburua: Gorbeiako Haizea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M5C_GazjARM
---

Udagoena eldu da erkien orrietara. 
Sasi gorrinduen ganean, kantari bakar ta zoro, 
txindorra*. 
Eguzki laburrak ez dauka berorik. 
Ez dago orlegi ez ori lantzarra*.

Eskortak igaroz, 
beiak inoztu daben zelai-gueneko 
pagadi zaarrean, 
ezkur-azal baltzak t'orbelak lurrean.

Lorerik, 
iru-lau moretxu banatu besterik, 
ez da gelditu landetan.

Ardi ta zaldiak 
maldan beera dakarz, uleak arrotuz, 
arratsaldeko otzak, 
an-emen labanka.

Txaboletaraiño jatsi da Gorbeiako aizea. [I] 
Ba-nator.

Zugaitzen gerriak, leen baño apur bat 
jausi ta zaarrago 
iruditzen jataz.