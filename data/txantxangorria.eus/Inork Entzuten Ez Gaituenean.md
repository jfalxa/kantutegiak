---
id: tx-501
izenburua: Inork Entzuten Ez Gaituenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cIpfFTHt3wY
---

Esanezinen “Aireratzen” diskako aurrerapen kantuaren bideoklipa, arriki.eus-ek  zuzendua.


Hitzak: Ianire Aranzabe.
Musika: Oriol Flores.
Musikariak: Ianire Aranzabe, Oriol Flores, Guillem Callejón eta Claudi Arimany.
Grabaketa eta nahasketa: Claudi Arimany (Turonet estudioa).
Masterizazioa: Iñaki Illarena (Aberin estudioa).


Eskerrak: Arri Iraeta, Eñaut Garmendia, Beñat Etxeberria, Ordiziarrock, Iban Zubeldia, Saioa Arregi, Nagore Agirre, Leire Garmendia, Elena Zubiaurre, Olatz Ordozgoiti, Irati Jauregi, Maitane Perez, Maria Saldaña de Pablo, Itxaso Orbegozo, Xabi Orbegozo, Ane Mitxelena, Iker Mitxelena, Maite Garmendia, Maddi Lekuona, Gorka Lekuona, Amaia Garmendia, Amaia Labaien, Joseba Garmendia, Joseba Aranzabe, Itziar Garmendia, Eneritz Aranzabe, Periko Garmendia, Lurdes Erauskin.


INORK ENTZUTEN EZ GAITUENEAN


Loratze orok ezkutatzen dituen
iraganeko arantzak agerian daude

Hainbeste erabili eta botaz
nola bihurtu garen zuentzat jolas

Hitzak paper solte baten idatzita ere
lerro hauetan ez dut topatzen, arnasik ez

Berandu denean damuak balio ote duen
galdetu zenidan,
ta nik erantzun: duguna ez estimatzeak
galtzean dakar hutsunea

Hitzak paper solte baten idatzita ere
lerro hauetan ez dut topatzen, arnasik ez

Ta orain ezin gelditu uholdea
zertan gelditu diren hutsuneak

Kantatuko dugu inork entzuten ez gaituenean