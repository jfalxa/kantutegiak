---
id: tx-3319
izenburua: Ai Amatxo -Uretxindorrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d-P_iD-V7GE
---

Ai, nere bihotzeko
 amatxo kutuna.
 Zure aurpegiaren
 hau edertasuna.
 Horren gozoa zeran.
 Horren zoriduna.
 Oi, begiratze xalo
 beti daukazuna.
 
 Zure bi begitxoak
 dirade argiak,
 zure masail koxkorrak
 arrasa gorriak,
 zure ezpainak dirade
 dirdiratzaileak
 eta ahoko hortzak
 urrez estaliak.
 
 Burutik oinetara
 poliki jarria,
 edatuxe daukazu
 buruko ilia,
 zinez ez da ilia
 baizikan urria,
 bada, ile bakoitzatik
 darizu argia.