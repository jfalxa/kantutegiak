---
id: tx-875
izenburua: Els Segadors Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JDnUAxIzYwI
---

ELS SEGADORS – IGITARIAK

Hitzak: Francesc Alió (1892)    

…

Katalunia garaile

berriz jori eta oparo,

atzerat jende hori

hain burgoi eta hain harro.

Igitaiaz!

Igitaiaz gure lurraren alde!

Igitaiaz!

Garaia da igitari

adi eta erne egoteko.

Ekainaren zain berriz,

gure tresnak zorrozteko.

Igitaiaz!

Igitaiaz gure lurraren alde!

Igitaiaz!

Beldurtu du etsaia

gure ikurra ikusteak,

galburuak moztu lez

sega ditzagun kateak.

Igitaiaz!

Igitaiaz gure lurraren alde!

Igitaiaz!



Catalunya, triomfant,
tornarà a ser rica i plena!
Endarrera aquesta gent
tan ufana i tan superba!

Bon cop de falç!
Bon cop de falç, defensors de la terra!
Bon cop de falç!

Ara és hora, segadors!
Ara és hora d'estar alerta!
Per quan vingui un altre juny
esmolem ben bé les eines!

Bon cop de falç!
Bon cop de falç, defensors de la terra!
Bon cop de falç!

Que tremoli l'enemic
en veient la nostra ensenya:
com fem caure espigues d'or,
quan convé seguem cadenes!

Bon cop de falç!
Bon cop de falç, defensors de la terra!
Bon cop de falç!