---
id: tx-2581
izenburua: Bi Esku
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SMbTSf-WoPU
---

Bi beso ditugu 
eta bi esku.
Bietan hamar hatz
honela mugitzeko.
Bi hanka ditugu
zutikan egoteko,
bi oinak ditugu 
pausoak emateko.
Saltoka saltoka,
kalean jolasteko.