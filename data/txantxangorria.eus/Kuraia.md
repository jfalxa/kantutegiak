---
id: tx-147
izenburua: Kuraia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6SjclL6eYdA
---

Nire euri ur-jauzia
Gelditzen dudanean
Eguzki izpiak
Urpetzen du nire azala
Nire euri dorrea
Suntsitzen dudanean
Aire berriak
Gabritzen ditu nire ezpanak
Nire euri pareta
Apurtzen dudanean
Biluzten ditut
Nire bularrak
Nire euri jantzia
Urratzen dudanean
Zabalik uzten dizkizut
Nire bide guztiak
Nire euri mozala
Hozkatu eta
Osorik ikertzen dut
Zure azala
Nire euri mozala
Ahotik kendu eta
Irensten dugu
Oxigeno berbera
Nire euri pareta
Apurtzen dudanenean
Biluzten ditut
Nire bularrak
Nire euri jantzia
Urratzen dudanean
Zabalik uzten dizkizut
Nire bide guztiak
Nire euri mozala
Hozkatu eta
Osorik ikertzen dut
Zure azala
Nire euri mozala
Ahotik kendu eta
Irensten dugu
Oxigeno berbera
Nire euri mozala
Hozkatu eta
Osorik ikertzen dut
Zure azala
Nire euri mozala
Ahotik kendu eta
Irensten dugu
Oxigeno berbera
Osorik ikertzen dut
Osorik ikertzen dut
Zure
Osorik ikertzen dut
Zure azala
Osorik ikertzen dut
Osorik ikertzen dut
Osorik ikertzen dut
Zure azala