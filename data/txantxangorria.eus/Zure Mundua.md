---
id: tx-1637
izenburua: Zure Mundua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/54VvLhXEpGk
---

Munduaren egoerak eskatzen dit esnatzea.
Nire odolaren arima beti bizirik iraultza!

Baina hauxe da alferkeria, hobeto nago etzanda.
Eta borrokak, balioak, baztertua.
biharko kontuetan.

Hau da gure mundua, nahi dutenen modura, 
inertziaren jokoa.
Hau da zure mundua, jada bideratua,
eta ideiak arrstaka.

Hau da gure mundua, nahi dutenen modura, 
ikuspegi lanbrotsua.

Hau da zure mundua, jada bideratua eta aldatzea
biharko kontuetan.

Ez gaitu ezerk zoriontzean, guztiak gaitu aspertzen.
Hotsak, hitzak eta kexak, jendearen ahoetan.

Esanahi gabeko ideiak, bertan geratzen badira,
eta borrokak, balioak, baztertuta,
biharko kontuetan.

Ez utzi zure arimahaizearen menpean,
ez pasa zure bizitza kontraesanetan.