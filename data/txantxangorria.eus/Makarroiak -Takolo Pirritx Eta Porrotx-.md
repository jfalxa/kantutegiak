---
id: tx-3263
izenburua: Makarroiak -Takolo Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JVqjK_qSpEA
---

Jolastu naiz kalean
gose naiz bat-batean
korrika etxera noa ni

Bazkaltzeko moduan
sardeska bat eskuan
zapia lepotik zintzilik.

Motzak eta luzeak
lodiak edo meheak
den-denak beren zuloekin.

Egosita prestatu
azkarrago bukatu
gehiago jango nuke oraindik.


Makarroiak! Makarroiak!
Makarroiak! Makarroiak!
hodi gozoak tomatearekin.
Makarroiak! Makarroiak!
Ma/ka/rro/iak! Ma/ka/rro/iak!

asko gustatzen zaizkit
beti niri.

Jolastu naiz kalean
gose naiz bat-batean
korrika etxera noa ni

Bazkaltzeko moduan
sardeska bat eskuan
zapia lepotik zintzilik.

Motzak eta luzeak
lodiak edo meheak
den-denak beren zuloekin.

Egosita prestatu
azkarrago bukatu
gehiago jango nuke oraindik.


Makarroiak! Makarroiak!
Makarroiak! Makarroiak!
hodi gozoak tomatearekin.
Makarroiak! Makarroiak!
Ma/ka/rro/iak! Ma/ka/rro/iak!

asko gustatzen zaizkit
beti niri.

Gehiago! Gehiago!
Beste platerkada!