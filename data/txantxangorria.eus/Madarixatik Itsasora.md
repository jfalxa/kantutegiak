---
id: tx-1319
izenburua: Madarixatik Itsasora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sVOZw_oe1jA
---

Madarixatik itsas aldera
jarri zentzu denak adi
isiltasunez edertu zara
auskalo zenbat gizaldi
otsoentzako larre malkarrak
azkonar eta saikabi
basapiztien maltzurkeriak
eta gautxorien hegaldi
ez nau harritzen dohain bitxiko
artista izana Sakabi.

Gau oskarbiko ihintzak bustirik
egunsentiko lorea
Izarraitzetik iristen zaio
paregabeko aidea
Attolan behera sartuz gero
horra infernu bidea
zulo barrengo magia horrek
badu indarra ordea
sumatzen dugu zerua ere
badela bertan gordea.

Naturak berak sorgindutako
ingurumari salbaia
akelarretan murgil litekeen
paregabeko paisaia
berdetasunik galtzen ez duen
pago lizarren usaia
oraindik inork ez dizu ostu
bihotz taupada lasaia
zure altzoan ikutzen dugu
zorionaren sabaia.