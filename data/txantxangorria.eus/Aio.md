---
id: tx-1860
izenburua: Aio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p5zu0ZF9vmU
---

Trixte, trixte da
lagunak uztea
laister itzuliko gara
Trixte, trixte da
lagunak uztea
berriz ikusiko gara.
Aio! Aio! Goazen orai
Aio! Aio! Feel awright
bukatu da...