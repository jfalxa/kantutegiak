---
id: tx-3265
izenburua: Izeba Maritxu -Takolo Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wBs2NpUKAVw
---

Ring! Ring! telefonoa
zein izango ote da?
Bai, esan? Maritxu naiz!
Hau poza izeba!
Zer moduz?
Zer nahi duzu?
Bisita egitea!
Gonbidatuta zaude
pastelak jatera!

Oh! izeba,
tripontzia zara zu!
Oh! Maritxu,
dena jaten duzu!

Ipurdia mugituz
badator musika!
Pastel horiek non daude?
Hasi da galdezka.
Ikusi bezain azkar
jan ditu binaka
Agur! Agur! esanda.
Joan zaigu korrika!

Oh! izeba,
tripontzia zara zu!
Oh! Maritxu,
dena jaten duzu!