---
id: tx-1001
izenburua: Urte Berri Egun Ona
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r_uqw3PtdfM
---

#saveyourinternet

Ur goiena, ur barrena
urte berri egun ona,
egun onaren seinalea
hemen dekargu ur berria

Soroan eder goldea,
haren gainean belea
etxe hontako nagusi jauna
nekazaritxo noblea

Ur goiena, ur barrena
urte berri egun ona,
egun onaren seinalea
hemen dekargu ur berria

Etxekoandre giltzaria
ireki zazu atea
badugu lanan ur berria.
har dezagun gosaria

Ur goiena, ur barrena
urte berri egun ona,
egun onaren seinalea
hemen dekargu ur berria

Eguzu, bada, eguzu,
baldin eman nahi baduzu,
baldin eman nahi baduzu eta
bestela bota gaitzazu.

Ur goiena, ur barrena
urte berri egun ona,
egun onaren seinalea
hemen dekargu ur berria

Armairuan dabil sagua
hari segika katua
etxe hontako limosnarekin
ez da beteko zakua.

Ur goiena, ur barrena
urte berri egun ona,
egun onaren seinalea
hemen dekargu ur berria