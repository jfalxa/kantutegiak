---
id: tx-3035
izenburua: Zortzi Nobio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GyxSVq_kVoY
---

Neska politte dago Laukizko herrien; zortzi nobio ein deittuz urte bi t´erdien. Ez ibil bazan beti fineza garbien: zortzigarrena deuko inozko larrien.

Lehelengo nobioa ei zan kuberue, mutil galanta baina bekoki pardue.

Bigarren nobioa zan zapatarie, koxoa ixen ezpalitz konsolagarrie.

Hirugarren nobioa dogu alargune, baztar askon jaubie, erederu jaune. Euki ezpazittuzen etxe bete ume egie da zeozer egingo gendune.

Neska politte dago ...

Laugarren nobioak falta deu andrie, alargune da eta errentadorie.

Bosgarren nobioa dogu (i)Eusebio; egunen pezeta bi deukoz diario.

Seigarren nobioa Eustakio “Eulie”, prakazar inuzente, koittedu pobrie. Euki zendun zuk nigez ezkondu gurie, baina (i)eragin neutsun erretiredie.

Neska politte dago ...

Zazpigarren nobioa dogu Eugenio; hari bere izen neutsen hainbet amodio. Hagaz ezkontzearren larga Eustakio; hara hemen non deuketan hainbeste nobio.

Zortzigarren nobioa Jose (i)euzokoa, hamazazpi urteko mutil ditxosoa. Guztiz nago zeugana amodiosoa; zeuk izen berko dozu neure (i)esposoa.

Neska politte dago ...

Zortzi nobio eta zortzi amodio; Josepa akorda zaittez nor dan Eustakio. Hamaka demonino bajagok munduen, Josepa lehengusinek engaina nenduen.