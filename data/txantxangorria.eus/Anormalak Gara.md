---
id: tx-2315
izenburua: Anormalak Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1e87CSassCI
---

"Anormalak Gara" Azken Sustraiak-en "Utopian Gara Aske" bigarren diskatik hartua.

Canción "Anormalak Gara" de Azken Sustraiak, tomada de su segundo disco "Utopian Gara Aske".

Kontaktua/contacto: 
azkensustraiak@gmail.com 

LETRA:

[Euskera]
Guztiz nazkatu naiz, pikutara, haien normaltasunaren normaz
Denok omen gara libre baina molde sozialen barne
Bakoitzaren mugak zehaztuak dituzte

Gure genero ta zaletasunek rol bat txertatua daramate
Zurea onartu ezazu edo bakarrik geratu
Alternatibetan ere arau hori dugu
Nirekin ez kontatu

DENOK GARA ANORMALAK
BAKOITZA BERE ERARA
HAUTS DITZAGUN ESTEREOTIPO ZIKIN USTELAK
GUREA DA GARAIPENA

Baina benetan kezkagarriena
Elkar epaitzeko zaletasuna
Hau da leloa: gu gara gure otsoa

ZU ERE ANORMALA ZARA
ONARTU ZURE IZAERA
EZ LOTSATU, EZ ZAITEZ BIHURTU ZURE ESKLABU
ANORMALEN IRAULTZA ZAIN DUZU