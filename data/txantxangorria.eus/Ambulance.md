---
id: tx-1726
izenburua: Ambulance
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FX8Wqxucits
---

...ba ni nengoen Erroman
eta jarri nintzen koman
ez galdetu nola
ez dakit izan zen ala ez...

hemen naukazue gero
hospitalean preso
bizi ez dut espero
...halaixe ere hiltzera nator

kupiturik begiratzen zin
erizain -spitosa- batek
urreatzeko esan zidan
-"hi ere koman al hago?
badaezpada ere emaidak
hire telefonoa"-
halaixe eman nion nik
numero bat aldatua (!)

Karrera bat egiten dizut
agure zahar batek esanda
Romatik buelta bat emanda
galtzen duna etxera dator

hemen naukazue gero
hospitalean preso
bizi ez dut espero
...halaixe ere hiltzera nator