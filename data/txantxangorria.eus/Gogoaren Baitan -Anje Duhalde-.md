---
id: tx-3300
izenburua: Gogoaren Baitan -Anje Duhalde-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2zH0rYLHoGM
---

Erabaki nuen tinko ta fermuki
Ez zitzai/dala gertatuko
Maitasun saretan
ez nintzala berriz
Ustegabean eroriko
Bi begi iz/pi/tsu i/rri/far ez/ti bat
I/zan di/ra ne/re gal/tzai/le
Ge/roz/tik hor na/bil
a/ran/tze/tan dan/tzan
E/zi/nez/ko a/mets sor/tzai/le

U/rrez/ko txan/pon bat
e/man/go ...go nu/ke
Sor/gin bat bi/la/ka/dan ne/ri
E/zi/na e/gi/kor al/da/tze/ko ha/i/na
Ge/zu/rra bi/hur/tze/ko e/gi.
Zer den o/na e/do
zer o/te den txa/rra
Ja/kin be/har/ko nu/ke/la/rik
Su/men/di gai/ne/an
e/rre/tzen a/ri naiz
Ba al da/go ir/ten/bi/de/rik.

Zu hor zau/de,
dan/tzan jau/zi/ta
Zer de/a/bru ba/da
pa/sa/tzen zait go/go/a/ren bai/tan.

Ge/ro/ak zer da/kar
ez dut pen/tsa/tu na/hi
U/ne/ak na/hi di/tut bi/zi
Lau e/gun di/ra ta
bi ho/nen la/ino/tsu
Gal/tze/ra ez di/tut na/hi u/tzi
Hi/tza/ren jo/ko/an i/txu/ra e/ma/nez
Gor/de na/hi/ko nu/ke de/na
Mi/hi/lu/zi/a nau/zu i/zan za/re/la/ko
E/go/e/ra ho/nen sor/me/na

E/ra/ba/ki nu/en tin/ko ta fer/mu/ki
Ez zi/tza/i/da/la ger/ta/tu/ko
Mai/ta/sun sa/re/tan
ez nin/tze/la be/rriz
Us/te/ga/be/an e/ro/ri/ko
Bi be/gi iz/pi/tsu i/rri/far ez/ti bat
I/zan di/ra ne/re gal/tzai/le
Ge/roz/tik hor na/bil
a/ran/tze/tan dan/tzan
E/zi/nez/ko a/mets sor/tzai/le

Zu hor zau/de,
dan/tzan jau/zi/ta
Zer de/a/bru ba/da
pa/sa/tzen zait go/go/a/ren bai/tan.
Zu hor zau/de,
dan/tzan jau/zi/ta
Zer de/a/bru ba/da
pa/sa/tzen zait go/go/a/ren bai/tan.

Pasatzen zait gogoa/ren baitan.

Pasatzen zait gogoa/ren baitan..