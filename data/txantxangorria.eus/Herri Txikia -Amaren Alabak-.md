---
id: tx-3186
izenburua: Herri Txikia -Amaren Alabak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vk2gUBZR8sE
---

letrak. Kantua Didier Aguerre eta Mixel Etxekopar

Herri txikia xume eta apala
Mundua zabal baina badu itzala
Europa zahar herritarren botz nahiari bide emanez
Buru jabetza eraikitzen da gurea gurea denez
Etorkizuna erabakitzen eskubidea dugunez
Zapalkuntza da mendez mendetakoa
Asimilatu guk ezin onartua
Aitortza oro bilakatu zen denborarekin errautsa
Herririk gabeko herritarrak izena izana hutsa
Historiaren gure haria kontatzeko gu ez gauza
 
Gora bidea izanikan malkarra
Borroka bizi gure oldez oldarra
Ordain gordina orbain samina beti zutik buruz buru
Beraien legez gure geroa dute indarrez helburu
Bide ilunez erori lagun omenduz kemena dugu
 
Askapen hortan gaude murgilduak
Aldaketaren premiaren ildoak
Denen partea esku hartzea bihurtuz beharrezkoa
Badagokigu indarrak bildu bidea jorratzekoa
Herri hontako jendeak baitu ametsa gauzatzekoa