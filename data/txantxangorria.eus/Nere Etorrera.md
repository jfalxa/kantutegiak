---
id: tx-2694
izenburua: Nere Etorrera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-AN1_Z_UR1w
---

Hara nun diran mendi maiteak!
Hara nun diran zelaiak!
Baserri eder zuri-zuriak,
iturri eta ibaiak.
Hendaian nago zoraturikan,
zabal-zabalik begiak,
nerea, baino lur hoberikan
ez da Europa guztian.

Agur bai, agur Donostiako
nere anaia maitiak,
Bilbaotikan izango dira
aita zaharraren berriak.
Eta gainera hitz neurtuetan
garbi esanez egiak,
Sudamerikan zer pasatzen dan
jakin dezaten herriak.