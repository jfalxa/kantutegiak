---
id: tx-2136
izenburua: Nire Mundura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZGrsDIu1zKQ
---

2008.urteko errebelazio taldea bihurtzeko hautagaia da eta eta pop elektroniko euskaldunaren derrigorrezko erreferentzia, Manentek ideiak garbi ditu eta soinu iradokitzaile batekin.
 
Naturaltasun osoz adierazten dakien abeslariaren mesedegarri diren testu landuen, xehetasunez beteriko konponketen eta mugimendu ondulatzaileen popa da.
 
RDM independente zigiluak editatu duen bere debuteko albumean, Carl Watermarkek, oso mota eta portaera desberdineko musikari ugarirekin egindako lanagatik gure lurraldeko musika ekoizlerik ezagunenetako batek, egindako   kolaborazioari esker, soinu pertsonala lortu da.
Manentek anbizio haundiko proiektu musikal honetan abesti ederrak biltzen ditu, konposizio bikaina energia sortzailearekin uztartzen da, eta bere hurrengo zuzenetakoetan atzeman ahal izango da bere benetako balioa.
 
Ez dugu zalantzarik Manenten "txuribeltzeko amodioa" albumak izango duen arrakastarekin, horretarako nahikoa da "hisia", bere lehenengo single biribila entzutea.