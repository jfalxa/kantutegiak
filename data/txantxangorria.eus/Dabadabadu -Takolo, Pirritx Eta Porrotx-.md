---
id: tx-3232
izenburua: Dabadabadu -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1gaEavJhuAs
---

Gosaldu, bazkaldu 
eta afaldu ondoren beti
garbitu hortzak,
ez ahaztu.

Eskuilan pasta emanda 
ahoa ongi ireki
ispiluan dabadabadu.

Mugi, mugi gora
behera, esker, eskuin
garbi garbiak, 
dabadabadabadu!