---
id: tx-3122
izenburua: Maite Nauzula Diozu -Amaia Uranga-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Aqe-ThZtxsU
---

Ez nau izutzen elurrak
guttiago gau illunak
maiteagatik pasa netzazke
bai gauak eta egunak
bai gauak eta egunak
desertu eta oihanak.
Maite nauzula diozu
nik ere maite zaituz zu
diozun bezain maite banauzu
elizaz feda nazazu
elizaz feda nazazu
gero zurea nukezu.
Eri nagotzu bihotzez
erraiten dautzut bi hitzez
sukar maliñak harturik nago
ez zintuzkedan beldurrez
maitea senda nazazu
hill ez nadin bihotz miñez.
Maite nauzula diozu
nik ere maite zaituz zu
diozun bezain maite banauzu
elizaz feda nazazu
elizaz feda nazazu
gero zurea nukezu.
Ez nau izutzen elurrak
guttiago gau illunak
maiteagatik pasa netzazke
bai gauak eta egunak
bai gauak eta egunak
desertu eta oihanak.
Maite nauzula diozu
nik ere maite zaituz zu
diozun bezain maite banauzu
elizaz feda nazazu
elizaz feda nazazu
gero zurea nukezu.
Maite nauzula diozu
nik ere maite zaituz zu
diozun bezain maite banauzu
elizaz feda nazazu
elizaz feda nazazu
gero zurea nukezu.
Maite nauzula diozu
nik ere maite zaituz zu.