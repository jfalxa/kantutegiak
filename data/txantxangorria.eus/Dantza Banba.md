---
id: tx-1187
izenburua: Dantza Banba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vPHuLy9FROU
---

Dantza gurekin bamba, 
piperrak eta gatza egiteko saltsa,
sartu ezazu saltsa gerrian, aire, aire, 
gona azpian torero, ole, ole, bai horixe...
Dantza bamba!! Kanta bamba!!
Dantza bamba!! Kanta bamba!!
Izarrak ikusteko piperrak eta gatza, 
egiteko saltsa sartu ezazu saltsa gerrian, 
aire, aire, gona azpian torero, ole, ole, bai horixe...
Dantza bamba!! Kanta bamba!!
Dantza bamba!! Kanta bamba!!
........................................­.......................
Jokaldi plaza taldearen kantu alai bat, beraien "Enbido" diskotik. 1992. urtean kaleratu zuten, Elkar zigiluaren eskutik.

Jose Luis Olabarri, kitarra
Juantxu Urkiaga, teklatuak
Asier Muniategi, bajua
Carlos Velasco, kitarra
Elias Barragan, bateria