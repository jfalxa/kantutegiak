---
id: tx-630
izenburua: Lasai Lasai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pDb1gZya76U
---

Noizbait, iratxo bat
Sorbalda gainean eseri zitzaidan ta
Belarrira esan zidan:
Ez det bi aldiz pentsatzerik nahi!
Bihotzetik bihotzera
Esan zidan:
Batera aspertuko gera
Lasai, lasai...
Ze...
Sarritan galdetzen diot
Nere buruari
Hemen ez ote dabilen
Dena azkarregi.
Sarritan galdetzen diot
Nere buruari
Hemen ez ote dabilen...
Presa arerio,
Minutuak momentu bilakatu eta
Zuhaitzak dantzan ikusi,
Dauden alfonbra ederrenetan.
Erlojuak ezkutatuz
Zelaietan,
Batera aspertuko gera
Lasai, lasai...
A... Ze...
Sarritan galdetzen diot
Nere buruari…