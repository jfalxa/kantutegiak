---
id: tx-1636
izenburua: Datorrela, Datorrela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FbgqPnKsPA0
---

Datorrela, datorrela
maitemintzen duen tenorea.

Ene egonarriaren haundia!
ez nuke sekula ahantzia
Izuak eta larriak
Zeruratu dira abian.
Eta egarri gaiztoak
iluntzen du nire odola.

Datorrela, datorrela
maitemintzen duen tenorea.

Hala baita belardia
ahanzturan eroria
Garatzen eta loratzean
irakaz eta intzentsuz,
ehun euli zikinenBurrundara ozenean.

Datorrela, datorrela
maitemintzen duen tenorea.