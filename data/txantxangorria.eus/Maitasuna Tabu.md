---
id: tx-108
izenburua: Maitasuna Tabu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mIinc9VvzoA
---

Jokin Pinatxo - Garazi Esnaola - Ane Bastida - Julen Suarez -  Andoni Arriola - Mattin Arbelaitz

Grabaketa. Gaztain Estudioak
Ekoizpena. Eñaut Gaztañaga
Bideo zuzendaritza eta Muntaia. Jokin Pinatxo
Bideo grabaketak. Nahikari Calleja, Nahia Fagoaga eta Jokin Pinatxo
Esker bereziak: 
Nahikari, Nahia, Joseba, Maider, Mikel E, Oihana, Mikel A, Naia, Bittor, Julen, Ane eta Iñigo.

Letra :

Ohikoa bihurtu da gau bat elkarrekin pasa
Mezu bat bidaltzea hortik bi egunetara
Ta bai, badut, zer esan sarez kanpo nagonean
Ta bai, banaiz, zer izan biok batera esnatzean

Noiz bihurtu da maitasuna tabu
Haizeak eramaten dituen hitz horietan

Kafe bat, muxu bat, benetako begiradak
Larrosaz josiak gure izara ta bularrak
Azkar bizi nahi izateko nahi ta behar dudan oro
Beldur banaiz azkarregi joan ta dena galtzeko

Noiz bihurtu da maitasuna tabu
Haizeak eramaten dituen hitz horietan

Denbora, airean, goitik behera gorputzean
Gu biok, bagara, ordularian harea
Ta bai, nahi nuke hemen pixkat gelditzea
Ta bai, elkarri maite zaitut esatea

Noiz bihurtu da maitasuna tabu
Haizeak eramaten dituen hitz horietan