---
id: tx-1235
izenburua: Toto Pintotxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_BnjOn0rSWY
---

Toto Pintotxo Bilbo aldera 
zihoan bada trenean. 
horrela zioen, kolkorako 
leihotik begiratzean: 
- Ondoko posteak atzerantza 
azkar demonio doaz, bai! 
Bilbotik buelta merke egiteko, 
poste gainean joango naiz. 
Toto Pintotxo ez da txatxua, 
Baizik kalamitate hutsa! 
Aufa labirula, birulera, 
Aufa labirula, birula! 
Txartela eske etorri zaio 
trenetako ispektorea. 
- Ez dizudala nirea emango. 
Eros beste bat zuretzat. 
Txarteltxoa zulatu nahi duzu, 
artazi itsusi horiekin. 
Toto Pintotxo ezetz ta ezetz, 
Topo egin du Ertzantzarekin. 
Toto pintotxo&#8230; 
Bibotedun ertzain lodikotea 
azaldu den istantean. 
- Txapel gorriaz prest festarako 
zabiltza San Ferminetan? 
Ertzainak autoritate handiaz 
atera dio karneta. 
- Madona horri musu pare bat, 
debozioa diot eta. 
Toto Pintotxo zoaz etxera 
hainbat lasterren hemendik. 
Honela jarraituz ez baitzara, 
ondo irtengo trenentik! 
Toto Pintotxo...