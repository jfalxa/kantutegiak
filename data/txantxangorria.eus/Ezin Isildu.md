---
id: tx-2592
izenburua: Ezin Isildu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NZ9qIMTwIno
---

Gaur ez zara itzartu
nirekin goizean
bakarrik utzi nauzu
neure ustertzean
egunari berriro
gerritik heltzean
itzalak eskatu dit
"Utzi ni etxean"

Inor ez mintzearren
isilduko banintz
ez nuke isilduko
denen mina baizik

Bilatu zaitut begi
zabarkoien xarman
lanaren abiadan
uhinen marruman
bakarrik bazkaldu dut
platerekin teman
lehioan ez dut zure
formarik hauteman.

Inor ez mintzearren
isilduko banintz
ez nuke isilduko
denen mina baizik

Zurrutak jabetu nau
zutaz une batez
aparra joaitean
berriz nago lehen lez
bihar ere itzartzen
banaiz zure gabez
ezin dut segurtatu
jeikiko naizenez.