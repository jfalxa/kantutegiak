---
id: tx-1880
izenburua: Zazpiak Oihal Batetik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wgIHHNYt_fA
---

Mundu guztiak aditutzen du
euskaldunaren negarra
alde batera nahiago nuke
ez banintz hemengotarra.
 
Enpeñatzea ez bada libre
alferrik dugu indarra
hau da txoriak gari tartean
goseak egon beharra.
 
Antzerki hontan lau pertsonaia
ta jokalari bakarra
arbolak berak erakusten du
hostoa eta adarra.
 
Enpeñatzea ez bada libre...
 
Sutan pizturik darrai oraindik
sugaiz berriz iehengo gerra
oharkabean ez zen mintzatu
Txirrita poeta zarra.
 
Enpeñatzea ez bada libre...
 
Zazpi ahizparen gai den oihala
ebakirikan erditik
alde batera hiru soineko
utzirikan lau bestetik.
 
Guraiziakin bereizi arren
bakoitza bere aldetik
ezagutzen da jantzi direla
zazpiak oihal batetik.