---
id: tx-823
izenburua: Hauxe Da Despedidea  Gora Ta Gora Beti
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QD3t13WshUM
---

Dibi lada, debele dede, dabala dada, 
hauxe da despedidia. 
Dibi lada, debele dede, dabala dada, hauxe da despedidia. 
 Dibi lada, debele dede ...

Gora ta gora beti, 
haik gora Lapurdi! 
Euskarak bizi gaitu 
eta bizi bedi. (bis)

Gorantza doa agudo kapitala 
eta Lapurdi beti dago apala, 
aldrebesturik dago mundu zitala. 
Konformatzen ez bagara 
ze pekatu mortala!

Gora ta gora beti...

Gurea ez da bizimodu normala, 
lurra lantzeko aitzurra eta pala, 
lanaren truke kobratzen erreala. 
Benetan da miserable 
euskaldunon jornala.

Gora ta gora beti...

Munduan ezin bizi daiteke hala, 
merkatuetan dena gora doala, 
etxera eta zopa guztiz argala, 
kriseiluen olioa 
ez da postre formala.

Gora ta gora beti...

Obrero eta nekazari leiala, 
zuena ez da errenta liberala. 
Diru zikinak putre motzak bezala 
botatzen dizu gainera 
hortxe bere itzala.

Gora ta gora beti...

Jabeek dute kontzientzi zabala, 
kakastu dute gure lurren azala, 
baina Lapurdi ez da hain zerbitzala; 
burruka garbian dago. 
Hori da printzipala!

Gora ta gora beti...