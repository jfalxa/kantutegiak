---
id: tx-2355
izenburua: Loreak Eman Arte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lF0VA999vfk
---

Zeharka erortzen den euriak
busti nau eta itxi ditut begiak. 
Ez dakit erantzuten
zure galderaren lehen hitzari, 
nondik eroriko den inola baneki… 
Heldu orain eskutik. 

Eta hori bada nahi duzuna,
ez hitzik galdu eta ez galdetu zer ote den
dantzan jartzen gaituena inon musikarik gabe,
urak bere bidea zure ilean marraztu badezake,
gure begiek loreak eman arte.

Eta behe-lainoa baino arinago dator orain,
ezin astunago eskuetatik erortzen zait.
Ezin hartu zitekeenik ezin ahaztu.
Zugan bizi den beste inor
edo nigan hilko den amets oro,
ezin heldurik elkar datoz  berdin duelarik...

Eta hori bada nahi duzuna,
ez hitzik galdu eta ez galdetu zer ote den
dantzan jartzen gaituena inon musikarik gabe,
urak bere bidea zure ilean marraztu badezake,
gure begiek loreak eman arte.