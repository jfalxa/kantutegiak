---
id: tx-2755
izenburua: Halleluyah
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o-Qi7Q_0Na0
---

Aire sekretu bat bazen,
Davidek jo, Jaunak laket,
baina zuri musikak ze(r) axola?
Hola doa: laga, boga,
minor be(he)ra, maior gora,
errege_aztoratuen Aleluia

Aleluia! Aleluia!
Aleluia! Aleluia!

"Fedeak ez du balio"
lamiak halaxe dio:
"lotuko zaitut
sukaldeko aulkira...!"
Apurtu dizi tronua,
moztu ilearen jira,
eztarritik erauzi:
Aleluia!

Jainkorik bada? Akaso.
Maitasunak ez dit erakutsi asko,
ene aurkaria
tirokatzen baino...
Ez da lantu gauerdian,
kandelargi(a) ere ez, agian,
aleluia hotz eta apurtua da!

Aleluia! Aleluia!
Aleluia! Aleluia!

Egon naiz hemen le(he)n(ag)o,
gela ezagun, lurra lagun,
zu topatu arte, lurra lagun,
zu topatu arte (nintzen) bakarrik.
Bandera arkuan txutik,
maitasuna ez da desfile baizik,
gauza zirtzil(a) (e)ta ospela,
Aleluia!

Aleluia! Aleluia!
Aleluia! Aleluia!

Behiala azaldu zenidan
zer den gure oin azpian
baina hori jada amaitu da, ala?
Zure baitan nintzenero
uso santua hegaz unero,
jaulkitako hats oro (zen),
Aleluia!

Nire onena, askorik ez,
ezin senti, ukimenez...
Zinez, ez dizut egin iruzurrik.
Halere, zer,
dena oker?
Zure aurrean zutik nager,
mihi puntan dudan oro (da)
Aleluia!

Aleluia! Aleluia!
Aleluia! Aleluia!