---
id: tx-3388
izenburua: Riki Riki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lZLGiBb9qok
---

Xalala!
Riki riki!
Xalala!
Riki! ri!
dutxan dardaraka
ur hotzarekin.

Hartu esponja ta xaboitu
burua busti
igurtziz garbitu
txilbor eta titi
Jira bira ipurdia
mugitu beti
bi oinetan bukaera
ez dago gaizki.