---
id: tx-2628
izenburua: Antilla
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PB2xB6TpeV8
---

Lehendabizikoan ikustean Nerea zinen ni zurea naizen bezala
Oraina, geroa, eraman nazazue betiko Antilla abere honekin.
 
Txuria bezain argia zara Antila
Orlegiaren gainean zaude ezarria
Belar berriak emanaz bizia
Haizearekin batera zatoz niregana
Zure irudia datorkit nire begietara
Ahaztu ez dadin zure izatearen bila nator (x2)

Horrek txikiak disdiretan begi eder horietan
begiratu orduko eskapatzen duten buru txiki horretan
eta suatik erazteko itxi ez du margotzeko margorik
zurekin bizi izan duenak esango du hori beti
zurekin bizi izan duenak esango du hori beti
 
Kresalen arteko bidean zoaz abiada bizian
Bakarrik eta arnasik gabe geratzen bazara
Nire arnasa emango dizut ni zurea naizen bezala
Zu nirea bai zara Antilla.
 
Hor izango naiz orain lehen bezela
Zuri besarkatuaz bai ere laztanduaz
Nire eginkizun honetan
Elkarrekin izan gaitezen eternidade guztian.
 
Zure gainean ezarririk berriz elkarrekin
Itsas mendi guztietatik berriz elkarrekin
Izango garen eternidade guztian.
 
Txuria bezain argia zara Antilla
Orlegiaren gainean zaude ezarria
Belar berriak emanaz bizia (x2)