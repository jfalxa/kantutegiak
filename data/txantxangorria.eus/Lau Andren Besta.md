---
id: tx-2853
izenburua: Lau Andren Besta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/edYuviIa_DU
---

Iragan besta bigarrenean,
Berek dakiten xoko batean,
Lau andre, bat alarguna, hirur mutxurdin,
        jarriak itzalean,
Harri xabal bat belaunen gainean,
Ari dire, ari dire trukean.
 
Zer duten nuke nik jakin nahi
Pitxarrarekin bertze jaun hori;
Zorroa du biribila, moko meharra,
        tente xutik egoki...
Xahako bat ez ote den nago ni,
Hanpatua, hanpatua ederki.

Besta delakotz egun herrian
Apaindu dire handi-handian,
Ederki gazteen gisa panpinatuak,
        ez patxada ttikian,
Bihotz onez partidaren erdian
Irriz daude, irriz daude lorian.

Besta bigarren egun berean
Lau gatu zahar anjelusean,
Bat mainguz, hirur saltoka, sorginak pujes!
        zoatzila bidean,
Ikusi ditut nik amets batean,
Akelarre, akelarre gainean.