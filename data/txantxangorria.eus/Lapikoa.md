---
id: tx-2585
izenburua: Lapikoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0bjxML58q48
---

Lapikoa lotu du eta luzatu zaio
Seberi belarria, ia, ia, lurreraino.
la, la, la, lapiko.
lo, lo, lo lotu du Sebek.
lu, lu, lu lurreraino luzatu.
le, le, le lepoko minez.
li, li, li, liburua irakurri.