---
id: tx-1799
izenburua: Konplize Ditut Eta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/afZa5wesfE8
---

Hitzak: Uxue Alberdi

Doinua: Mikel Markez

I
Emakume zuzenak,
okerrak, kirtenak,
moja txintxoak eta
zerrama irtenak,
itsusiak, politak,
atletak, herrenak:
konplize ditut eta
maite ditut denak.
 

IV
Lotsatiak, ausartak
argalak, gizenak,
iletsuak, soil-soilak,
marimutilenak,
katemeak, gorilak
panterak, zezenak:
konplize ditut eta
maite ditut denak.


 

II
Langileak, alferrak,
lanik ez dutenak,
tuntunak, mediokreak,
klaseko lehenak,
ale desegokiak,
andre txit gorenak:
konplize ditut eta
maite ditut denak.
 

V
Markesaren alabak,
neskame gizenak,
printzesak ta sorginak,
ogroak, sirenak,
Maritxuak, Bartolo
itxura dutenak:
konplize ditut eta
maite ditut denak.
 

III
Ama onak ta txarrak,
ama ez direnak,
lurrera jausi arte
mozkortzen onenak
ta umeen merienda
usaina dutenak:
konplize ditut eta
maite ditut denak.

VI
Lehen munduko emeak,
kapitalistenak,
lur usaia daukaten
andre indigenak,
zuri, gorri, horiak,
beltzetan beltzenak:
konplize ditut eta
maite ditut denak.