---
id: tx-3242
izenburua: Porru Patata -Gozategi-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qwNxSRqtK2I
---

Bideoklipak Ion Lizarazuk egina:


Santur txin da txin datorren artista bat naiz ni
(Santur txin da txin datorren artista bat naiz ni)
Ondo jotzen det
(Baita nik ere)
"Ni golpe" nik jotzen det
Pepepe pepepe pepepepepepepe

Nire kolegi Jaimito da ta
Nire partido Porru Patata
Jaurlaritzan behin botatu
"Sin keriendo" konturatu
Ta tranpetan arrapatu
Nire kolegi Jaimito da ta
Nire partido Porru Patata
Jaurlaritzan behin botatu
"Sin keriendo" konturatu
Ta tranpetan arrapatu

ESTRIBIYUE:
Akordeoia nik jotzen dot
Do menor eta do mayor
Orejarekin entzungo
Gozategi kale jiran
Ta sevillan ekitaldiya
Ipurdiko pelotilla
Zapatari zapatilla
GUARDIA ZIBIYA

Aeroportuan zezenak eta
Hegazkiñak zezen plazan
Esperantzarekin baña
Ez balantzarekin aña
Ta Marianok lurra jo dik
Aeroportuan zezenak eta
Hegazkiñak zezen plazan
Esperantzarekin baña
Ez balantzarekin aña
Ta Marianok lurra jo dik