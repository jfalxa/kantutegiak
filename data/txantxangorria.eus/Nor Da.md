---
id: tx-1839
izenburua: Nor Da?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Wr5FNo83jVI
---

Nor da gogoan daukazuna, nor da?
Lo aurreko zure instanteen jabe, nor da?
Bihotzean daramazuna, nor da?
Zure begietan gorderik, nor da?
Nor da gogoan daukazuna, nor da?

Tangerreko argi urrunak 
Ez dira aski zu ikusteko.
Mendebaldeko haizea lehuna
Nahi nuke nik zu ezagutzeko.

Nor da gogoan?

Zuri buruzko galdera denak
Urtu zaizkit eguzkitan.
Amodioa ikusi dut gaur
Zure leihopetik pasatzean.

Nor da gogoan...