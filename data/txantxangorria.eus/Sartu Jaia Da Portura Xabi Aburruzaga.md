---
id: tx-3091
izenburua: Sartu Jaia Da Portura Xabi Aburruzaga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z5AyGq3BemQ
---

Letra: Iñaki Aurrekoetxea

Abestlariak: Portugaleteko lagunak, Natxo de Felipe, The Uski´s, MIkel Markez.

Rapa: Endika Abella
Beat box: Rufoh
Musikariak: Igor Telletxea: bateria, Alvaro Garcia: gitarra, Steve Makinen: baxu elektrikoa, Jose Urrejola: flauta eta teklatuak, Irkus Ansotegi: panderoa, Xabi Zeberio: bibolina.

Esnatu ta zatoz
jaietan gozatzera
munduko onenak
zer gainera!

balkoira atera
bagatoz jarrillera
maitasun kantu hau
eskaintzera.

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
ederra giroa, 
sartu da jaia Portura

Santa Mariatik dator
alaitasun uholdea
hori-beltzean jendea
ez omen da falta inor.

La Guian gora ta behera
Sanroketan gau ta egun
ongi etorri zuri lagun
zatoz Portugaletera.

ZATOZ PORTUGALETERA!
ZATOZ PORTUGALETERA!

Bizkaiko zubia tente
Abrako ate nagusi
soinu, dantza eta jauzi
gurekin mugi daiteke.

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
ederra giroa, 
sartu da jaia Portura.

Aizu, begira, jaia
lehertzen ari da
atera lozorrotik, 
etxeko sofatik eta gozatu;

ondo pasa, baina ez pasatu
izan zaitez bihotz eta buru,
erne behar zaitugu.

Errepelegatik Canillara,
Canillatik Floridara
emean eta hartu musua,
eskua, errespetua
lagunekin ospatzeko 
hartu jarrilla, bete
beti, GORA PORTUGALETE!

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
Portura sartu da 
boga-boga

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
Portura sartu da 
boga-boga


Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
Portura sartu da 
boga-boga

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
Portura sartu da 
boga-boga

Umorez kolorez
bete dugu pitxerra
arimaz, kilimaz
hustuko da.

Umorez kolorez
bete dugu pitxerra
Portura sartu da 
boga-boga