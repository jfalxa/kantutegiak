---
id: tx-1767
izenburua: Bi Bihotz Bi Ero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lhG_fmu5U5I
---

Plaken itsasoan, Kontrako bidean 
Bi bihotz, bi bihotz 
Eki laberintoan, Talka egin dute 
Bi erok, bi erok 

Energia hesi batekin topa ta 
Jo dute bortitz buruen kontra 
Ta imana bezela, lotu egin dira 
Bi bihotz, bi bihotz 

Igurtziz gertatu da, imantatu dira 
Bi bihotz, bi bihotz 
Eskola heriotza da, oihukatu dute 
Bi erok, bi erok 

Maitasuna eta sexuaren 
Deseraikuntza asmatu dute 
Arritmian bikote, by-pass-en miresle 
Bi bihotz 

Aurkitu dira bi bihotz, su hartu dute bi erok 
Aurkitu dira bi bihotz, su hartu dute bi erok 

Ta gaur da atzo kezkatzen zintuen geroa 
Eta bi bihotzak batera heldu gara