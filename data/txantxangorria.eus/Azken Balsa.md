---
id: tx-766
izenburua: Azken Balsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mv4BqrgtA00
---

Begira argazki honi, akordatzen zara, ai?
Zein hiri ote da? Edozein...
Gure izen bereko bi gazte zoro blai, aterpe lehorren bila lasterka...
Barrerik barre goizez, arratsez ere bai gaua teoría hutsa zen.
Gorputza higatzen doa geruzaz geruza; irrika ez, ez ordea...
Gogoa erne dugula, baina hezur zahar hauek noiz arte?
Noiz arte?
Noiz arte
Gure gorputzak
Litezke gurea aberri?
Gorputza diostadana ez dut niretzat nahi; zein min aukeratu?, Edozein?
Bakartu gara langintzez,
Lagunez ere bai, elkarren eskuak soilik euskarri...
Zergaitik ez aukeratu noiz eta nola dugun nahi esku eta aberri askatu.
Argazki horretan zutik esan nuen "
Nahi dut, bai"; orain ez, ez dut nahi.
Gogoa erne dugula, oskol zahar hauek noiz arte?
Noiz arte?
Gorputzak
Litezke gurea aberri?
Adiorik gabe,
Dantza dezagun azken balsa.