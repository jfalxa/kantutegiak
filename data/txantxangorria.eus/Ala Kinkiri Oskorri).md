---
id: tx-1872
izenburua: Ala Kinkiri Oskorri)
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8Z5JGv7ZAR8
---

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako dama gazteak
ez dira moja sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira,
hola!

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako gizon gazteak
ez dira fraile sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira,
hola!

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako gazte guztiak
ezkontzan dira sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira.
hola!