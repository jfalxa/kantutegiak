---
id: tx-242
izenburua: Nabil Ii
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YbFULbIYCWc
---

Ezer esan gabe hitz egin
Jakin gabe dakizunean
Inoiz ez zaude urrunegi

Bi arnas aldi berean
Bi bekokiren dantza
Bi gerizpa ilargipean

Urtaroetan zehar, elkar maitatu zuten
Beraien erara, elkar maitatu zuten
Desertu dunetan, elkar maitatu zuten
Ozeano hartan

La, lara la la,
La, lara la la,
La, lara lara,la la
La, lara la la,
La, lara la la,
La, lara lara,la la
Doazela pikutara
Adorearen giltza
Lakura bota zutenak
(e)ta nori zaio axola?
Noren ardura da une hau?
Birena,ez beste inorena

Urtaroetan zehar, elkar maitatu zuten
Beraien erara, elkar maitatu zuten
Desertu dunetan, elkar maitatu zuten
Ozeano hartan

La, lara la la,
La, lara la la,
La, lara lara,la la
La, lara la la,
La, lara la la,
La, lara lara,la la

(e)ta nola esan euriak joan zirela
(e)ta  ez dutela berriro nire izena buztiko
Bizi orain, bizi gaua,bizi laztan bakoitza
Azkena balitz bezala

La, lara la la,
La, lara la la,
La, lara lara,la la
La, lara la la,
La, lara la la,
La, lara lara,la la
La, lara la la,
La, lara la la,
La, lara lara,la la
La, lara la la,
La, lara la la,
La, lara lara,la la