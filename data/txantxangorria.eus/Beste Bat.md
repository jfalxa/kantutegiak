---
id: tx-2625
izenburua: Beste Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GRrgOxB5vfE
---

Beste bat ta beste bat,
ohiturak akabo!
Eskatu gabe ez da egongo gehiago!