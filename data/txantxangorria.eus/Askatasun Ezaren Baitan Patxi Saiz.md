---
id: tx-3243
izenburua: Askatasun Ezaren Baitan Patxi Saiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GqUCLbbUs2E
---

ASKATASUN EZAREN BAITAN
Mundu guztiak esaten duenez
askatasuna lortzen da negarrez
Aspaldian... iritzi ezberdinak dira
bata bestearen erruari begira.

Nolabait... Galdua da jada desira
Batak besteari tira eta tira.
Egunero... Zuria ala beltza
borrokatzeko ez gaitezen kezka.

Inork ez du entzun gure kexa.
Hauxe da gure eskatasun eza.
Orain maitea... Joan beharra dudanez.
Mesedez ez hasi negarrez.

Inork ez du entzun gure kexa.
Hauxe da gure askatasun eza.
Herri puta hau maite dugunez
bizi gera sufritu beharrez.
Orain maitea... Joan beharra dudanez,
Mesedez ez hasi negarrez.

Herri puta hau maite dugunez
bizi gera sufritu beharrez.
Orain maitea... Joan beharra dudanez,
Mesedez ez hasi negarrez...

lai lara lara larai