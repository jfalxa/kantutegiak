---
id: tx-329
izenburua: Zenbat Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XA6trJA5Iuo
---

Txekoslobakoren bigarren klipa, "Zenbat dira". 
Pandemia garai honetan "Zenbat Dira" eman gabeko muxuak edota ukatutako besarkada ta laztanak. Hau ere danontzat osasuna da, ume, gazte zein agureentzat.

Bideoa: "Kayak Produkzioak"
Musika: Gian Beat: MP3 Licence

Oh oh oh oh
Zenbat dira eman gabeko muxuak
Uh oh oh oh oh
Besarkada ta laztanak ukatuak

Zure espainetan astiro irristatu
Goxo hondartzan harean jolastu
Zure begi hoiek ta zure gerria
Une oro ikusi nahiko nuke zure irria
Ta orain egia da ez gaudela elkarrekin
Ta ezin dugula horren kontra ezer egin
Laister argi batek argitu gaitzala
zure espainek bidea argitzen duten bezala.

Oh oh oh oh........

Itzali eta apurtu egin diren bihotz bakartiak
Ta nork jasoko ditu orain haien zatiak
Senda ezina den gure egoera larrian
Gehiegi tenkatzen dizkigute gure hariak
Bihotz zahar ta bakarti etxean bakarrik dagona
Gaurkoan argazki bati malko artean So dagon amona.
Oh oh oh oh.....

Sentitu ezkero ni zure ferekak
Begiak itxita galtzen dut oreka
Begira mundua gaur penaz beteta
Musu asko dauzkat zuretzako gordeta
Ta orain egia da ez gaudela elkarrekin
Ta ezin dugula horren kontra ezer egin
Laister argi batek argitu gaitzala
Musuek bidea argitzen dute bezala