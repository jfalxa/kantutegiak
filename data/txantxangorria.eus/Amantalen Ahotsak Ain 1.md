---
id: tx-2273
izenburua: Amantalen Ahotsak Ain 1
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FTuWJln9UTM
---

Badirudi atzo izan zela

tortolox jolasean genbiltzala

sokasaltoan jauzika ta ezkutaketan gordeka

salto ta irri, irri ta salto

zubirik zubiri, nongorik nongo

txorro morro, piko ta joka

Donostiako hiru damatxo kantuan

soinekoa lokatzez zikinduta

putzutik putzura pausoka

Bizitza osua lanian eta

orain atzera begira

Zoriontsu ginen kuleroak astean behin aldatuta

Ispiluaren beste aldean

zimur ederrak agerian

orduko amandre zirenak

gure islada bihurtuak

Ilargira joan-etorri dira

sokaz bilduz gure eskuak

lanez ta minez gogortuak

Gorputza hil du errituak

amantaletan marraztuaz

Bizitzari begiratzeko dittugun moduak

Ezer gutxi genuen esku artean

Trapuzko panpinak eta debekuak ate joka

Bizitzarako giltzak poltsikoetan gordetzen ikasi beharra

Belaunetan eseritako herriari kontu kontatzeko nahia

Eta ametsak, kaxoietan gordeak

zabaldu eta aireratzeko beti zain

Bizitza osua lanian eta

orain atzera begira

Zoriontsu ginen kuleroak astean behin aldatuta

Zubirik zubiri, nongorik nongo

txorro morro, piko ta joka