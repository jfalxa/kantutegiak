---
id: tx-168
izenburua: Ikaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rV8ug4ebLlc
---

Ikaraz
Piztiak
Ez berezi ezpainak ene ezpainetatik
Ez dezadan arratsean
Hodei ihesiak negartzea desira
Ez berezi ezpainak ez berezi ez
Ez berezi ezpaianak ene azaletik
Ez dadin une izpirik igaro
Ez berezi ezpainak ene zainetatik, begietatik
Ez dezadan hortz eskubila siletik sartzen igaro
Zurekin izateko desiru
Zurekin izateko desiru, 
Zurekin izateko ikaraz
Zurekin izateko desiru, 
Zurekin izateko desiru
Zurekin izateko desiru, 
Zurekin izateko ikaraz
Zurekin izateko desiru.

Hitzak: Kirmen Uribe Urbieta
Musika: Txomin Uribe Urbieta

Bideoaren grabazioa, edizioa eta montajea Kulturaz kooperatiba @kulturaz_koop

ASKATASUNA DERITZON ERREPIDE BAT

Kirmen Uribek duela ia hiru hamarkada idatzitako letra honetan, sexu bereko bi gazteren maitasuna kontatzen da, nerabezaroan eta gaztaroan nor bere burua aurkitzeko dauden zalantzak, debekuak eta, azkenik, pozak ere bai. Beldurra eta desira, dena batera, munduan zure tokia bilatu nahia, askatasunaren aldarria.
Azkenean, Piztiak taldearen ibilbidea horixe izan da, bilaketa bat askatasuna deritzon errepidean, hogeitahamar urtez kantatzeko eta kontatzeko modu berrien bila, kontatu ez dena, min ematen diguna, amorrua eta, orobat, bizitzako poz txikiak kantatuz, horiek ere bai.

UNA CARRETERA LLAMADA LIBERTAD

En esta letra que escribió Kirmen Uribe hace tres décadas, se habla de la relación de amor entre dos personas del mismo sexo.  De las dudas, vetos y las alegrias que tenemos en la pubertad para encontranos a nosotros mismos. 

Miedo y deseo todo junto, querer buscar nuestro sitio en el mundo. Grito de libertad.

Al fin y al cabo, el recorrido de Piztiak es ese, una busqueda en la carretara de la libertad durante 30 años, buscando nuevas maneras de cantar y contar lo que no se ha contado, lo que duele, lo que nos da rabia y cantando las pequeñas alegrias que nos da la vida.