---
id: tx-817
izenburua: Zerura Begira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3z1zoZpvWfQ
---

Musika eta letra: Iñigo Etxezarreta (E.T.S.)
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Ekoizpen exekutiboa: Baga-Biga




Hotzak garela esaten digute etxetik kanpo gaudenean,
ez al duzue bada ezagutzen eguraldia Euskal Herrian?
Euriarekin dantzatzen dugu, hamar hilabetetan zehar.
Gaur eguzkia atera zaigu kaleak bete dira jada. 

Zerura begira, lagun artean goaz
korrika eguzkira, izpiak besarkatzera.
Zerura begira, ezin galdu denbora,
korrika eguzkira, hau da gure egun bakarra.

Lekua hartu dugu hondartzan, milaka kolore inguruan.
Tenperatura igo ahala sendatzen ditugu arazoak.
Zein izango da bizitzarekin surfeatzeko sekretua?
Gaur itsasoan aurkitu dugu bizipozaren olatua.

Amaierarik ez duen festa honetan gaua heldu da,
izarren azpian dantzatuko dugu "Musikaren doinua".
Amaierarik ez duen festa honetan askeak gara,
jarraitu dezagun ospatzen batera egunsentia.