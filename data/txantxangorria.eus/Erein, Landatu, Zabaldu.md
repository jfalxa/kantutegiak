---
id: tx-555
izenburua: Erein, Landatu, Zabaldu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pNjG3DV8BPE
---

Abestiaren letra, Joseba Sarrionandiak egin du.

Finantzek, gerrek, negozioek
Mundua dute trabatu.
Nekazarion bizimodua

Ez zaigu gutxi kaltetu.

Gauzak hobeto egin ezean,
Bizi ere ozta-ozta.
Atzoko haziek gaurko fruituak:
Gaurko haziek biharko uzta.

Ereinez gero elkartasuna
Bilduko dugu onura.
Landatuz gero berdintasuna
Zabalduko da kultura.
Denontzat lana, denontzat jana,
Denontzat askatasuna!

Planeta hau dugu etxe bakarra,
Suntsituz gero akabo.
Lurra artatu ezik lasterka
Martera joan beharko.
Gauzak hobeto egin ezean,
Bizi ere ozta-ozta.
Atzoko haziek gaurko fruituak:
Gaurko haziek biharko uzta.
Ereinez gero elkartasuna
Bilduko dugu onura.
Landatuz gero berdintasuna
Zabalduko da kultura.
Denontzat lana, denontzat jana,
Denontzat askatasuna!

Batzuk aberats, gehienak pobre:
Nekazarien kondena.
Baina mundua hobea egiten
Egingo dugu guk lana.

Ereinez gero elkartasuna
Bilduko dugu onura.
Landatuz gero berdintasuna
Zabalduko da kultura.
Denontzat lana, denontzat jana,
Denontzat askatasuna!
Ereinez gero elkartasuna
Bilduko dugu onura.
Landatuz gero berdintasuna
Zabalduko da kultura.

Denontzat lana, denontzat jana,
Denontzat askatasuna!