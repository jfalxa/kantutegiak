---
id: tx-2861
izenburua: Hitzez Hitz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NCDvjiGOIEw
---

Hitzez-hitz
oiñazea tantoka,
kristaletan lanbroa bezela,
hitzez-hitz, tantoka,
kondensatzen utziko dut
hitzez-hitz, tantoka, hitzez-hitz.

Eta hitza oiñaze izanen da,
oiñazeak gure zauriak sendatuko ditu,
zornea kanporatuko,
tantoka, hitzez-hitz, tantoka.
Eta sendatzen egunek emeki-emeki,
bata bestearen gaiñera,
tantoka
ixurtzen jarraiko dute,
tantoka, hitzez-hitz, tantoka.

Baiña ez dago sendatzerik, baiña.

Oiñazeak irrifarrean jarraituko du,
larrosa-mentazko ezpaiñetan,
begirada ukakor batetan
edo, aragiaren dardaka kunpli-eziñetan,
hitzez-hitz, tantoka, hitzez-hitz, tantoka.

Baiña ez dago osatzerik, baiña.

Eziñak oparotasunean jarraituko du
antsia haundienen banalkerietan,
esku itsatsien hoztasunean
edo,
azken mugetako gelditasun kiskalian,
tantoka, hitzez-hitz, tantoka.

Baiña ez dago sendatzerik, baiña.

Hitzez-hitz
oiñazea tantoka,
kristaletan lanbroa bezela,
hitzez-hitz, tantoka,
kondensatzen utziko dut
hitzez-hitz, tantoka, hitzez-hitz, tantoka.