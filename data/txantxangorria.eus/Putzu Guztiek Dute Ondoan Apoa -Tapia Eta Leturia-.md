---
id: tx-3270
izenburua: Putzu Guztiek Dute Ondoan Apoa -Tapia Eta Leturia-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2VJs7mlD8Uk
---

Kazan hiritik zabal pasatzen da Volga
Popeak agurtu du ostikoz apoa
Kosako zuri denak daitezela olga
Pikutara joan da sobieten aroa

Lisboako zubiak edertzen du Tejo
Pessoa lagun zaharra bildu du apoak
Tabernara kantari zabuka dute jo
Poetarik onena ontzen du ardoak

Luisianako lurretan dator
Mississippi Ibai hori begitik sortu du apoak
Zuri ez delako ta badelako ttipi
Kukluxklanen bisita dauka gizajoak

Ai putzuak beteak ez baleude apoz
Nork emango lioke bizitza honi poz?
Penak uxatzen ditut eskusoinua joz
Nahiago baitut bizi ahuspez baino hauspoz!

Parisko jauregiek behar dute Sena
Versallesko loreek bezala apoa
Komunako iraultzan gorri izan zena eskuin aldera
dator oker du zangoa London aberasteko dago Tamesia
Ate denetan jo du lan-eske apoak
Robin Hoodek aspaldi hautsi du gezia
Orain beste beso bat behar du arkoak

Alkairo zikin hartan isurtzen da Nilo
Alaren urrikia otoi du apoak
Pobre sortu den horrek inon ez du giro
Hanka bat lehertu dio
Aliren autoak

Ai putzuak beteak...

Buenos Airesen bada Rio de la Plata
Bazter hartatik dago oihuka apoa
Ura badauka baina zilarra du falta
Piazzolak bezain triste jotzen du tangoa

Baionan itsasora zabalik Aturri
Hemen ere bizitzen lan dauka apoak
Egalitatez labur libertatez urri
Ez baitira zilegi hark dauzkan asmoak

Amazonas Danubio Ganges Po Zanbezen
Mundutxo hau denean nahiko txarto doa
Baina ez dezagun gal esperantza ezen
Putzu guztiek dute hondoan apoa

Ai putzuak beteak...