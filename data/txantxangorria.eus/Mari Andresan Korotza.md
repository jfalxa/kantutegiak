---
id: tx-491
izenburua: Mari Andresan Korotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kCIXW-rpfi4
---

Mari Andresaren
Mari Andresaren
korotza,
zazpi libra ta
zazpi libra ta
bost ontza.
Harek egiten dau
harek egiten dau
demasa:
epel-epel,
lodi-lodi,
punta motza,
epel-epel,
lodi-lodi,
punta motza.