---
id: tx-505
izenburua: Kukua Eta Primadera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v5snbgWBO6o
---

Berritsu-isilak, polit-itsusiak,
argal-lodi, ilun-argi, altu-baxuak,
elbarri eta itsu, gor edo mutuak.
Denak ezberdinak, eskubide berberak.

Gorputz eta izaera bakarrekoak gu,
nork bere burua maitatu,
nik badakit zer nahi dudan, zer egingo dudan,
aukera neurea izango da!

Nire gorputza da
aske eta librea,
nahi dudan moduan,
maite dudan munduan.

Elkartu, sentitu, nahi duzunarekin.
Zer lagunekin batu, aukera zeuk egin.
Zure desio eta zaletasunekin
aurrera egin maitatzen zaituztenekin.

Gorputz eta izaera bakarrekoak gu,
nork bere burua maitatu,
nik badakit zer nahi dudan, zer egingo dudan,
bidea nirea izango da!

Nire gorputza da
aske eta librea,
nahi dudan moduan,
maite dudan munduan.