---
id: tx-1245
izenburua: Hormak Eta Harresiak Ez Ditugu Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1MWoOeHuQlo
---

Ekainaren 10eko giza katearen abestia. Videoclip de la cadena humana del 10 de junio. 
Zuzendaritza www.mirokutana.com 
Abeslariak:
Aneguria eta La Basu 
Koroak: 
Gorka Suaia eta  Ibon Larruzea 
Kitarra: Ibon Larruzea 
Riddim Selektah Stepi Dantzariak


Lurralde honetan
Hamaika biztanle lotu dira etorkizunari begira


Hormak eta harresiak ez ditugu maite
Elkar bizi nahi dugu aske,aske

Pausoz pauso 
Elkarlanean
Han edo hemen 
Ahal den moduan
Gizaki onen
Ekintzen kate

Erreflexu den
Ekimena
Batak besteei
Galderak erantzuten
Posible badan
Nola izan daitekeen
Mugarrien aurre
Guztien artien
Elkarlanean
Indarrez alegie


Gure bidean
Gure eskuetan
Gure alaitasuna indartuz
Herri libre batean
Alde batera utziz superheroiak
Gu gara istoriaren protagonistak
langile, idazle, musikariak
Arrantzale eta nekazari, margolariak
Andre edo gizon
Amona edo umea
Elkar bizi nahi dugu…


Hormak eta harresiak ez ditugu maite
Elkar bizi nahi dugu aske,aske

Gure esku da



Aske gure herrian
Aske etorkizunean
Aske ura bezala
Gure indarra dago 
Gure eskuetan
Aske gure mendietan
Aske gure jaixetan
Aske izan daitezela
Gerlari,borrokalari guztiak

Esan
Zein dan bide zuzena
Gizarte honetan dauden
Malda eta espaloi guztietako
Biztanle anitzen
eskubideen alde
Burujabetasuna
Nola eman daitekeen

Lan egin dezagun
Marka danak bete daitezen
Zalantzen aurre
Aurrean ditugun aukerak ikasiz
Inor galdu ez dadin
Jendearen ongi izatearen
gurari