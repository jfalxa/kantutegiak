---
id: tx-1575
izenburua: Neure Txiki Polita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/39v822_iwxY
---

Neure txiki polita
zelan zara bizi?
zortzi egun honetan maitia
etzaitut ikusi.
Badakit zabizena
neugandik igasi,
eztutzudela artzian maitia
sentimentu gitxi.
Pasetan nazanian
zeure ateetatik,
negarrak urtetan duz maitia
begi bietatik.
Ez dago larrosarik
arantzarik bage,
amodiorik ez da maitia
penarikan bage.
Ama zeuriak neuri
bidean topaute,
ez deuste eiten berbarik maitia
burue buelteute.
Ama zuriak neuri
berba ez eiteko,
ze palazio ete deuko maitia
zeuri emoteko.
Palaziorik ez deuko
neuri emoteko,
baia jenioa deuko maitia
halantxik eiteko.
Palazio eder bat
hatxaren ganian,
ez da euririk sartzen maitia
aterri danian,
ez da haizerik sartzen maitia
ez dabilenian.