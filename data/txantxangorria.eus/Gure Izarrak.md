---
id: tx-566
izenburua: Gure Izarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WF_IZpX4qic
---

Zentzurik
ez duen arren
zure itzalaren
koloreak faltan botatzen
hasi naiz
hamabiak aldera gauero unetxo batez
Gauzak aldatzen
han eta hemen
Itzurunen
eguzkia ukitzeko gai ginen
agian zu ere oroituko zara noizean behin
Aldrebes xamarra naiz batzutan
oraindik
urduri jartzen naiz begitara zuzen
begiratzean
egia esan
aspaldian
bare bare dago dena
urteak pasa diren arren
hau betirako da
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu
gure izarrak
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu
Ze ondo gauden
esan zenun goizalde baten
presarik gabe bizi ginen
udara luze hartan
Gozatzen
zurekin bizia gozatzen
zorameneraino maitatzen
erakutsi zenidan
Aldrebes xamarra naiz batzutan
oraindik
urduri jartzen naiz begitara zuzen
begiratzean
egia esan
aspaldian
bare bare dago dena
urteak pasa diren arren
hau betirako da
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu
gure izarrak
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu
Ta gauzak horrela behar zutelako
kantu bakoitzean
zurekin nago
ta gauzak horrela behar zutelako
Betirako (x4)
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu
gure izarrak
Ta bakarrik bazaude
begiratu gora
hantxe aurkituko dituzu