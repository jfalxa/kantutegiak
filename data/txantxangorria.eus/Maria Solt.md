---
id: tx-2739
izenburua: Maria Solt
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/roKlk1FvBrg
---

Maria Solt eta Kastero
bi amoros zahar bero.
Hirurogei hamarna urtetan hartü die amodio
Kastero jelostü gero Maria Solt ezarri kanpo.

Maria Solt dua nigarrez
izorra dela beldürrez.
Barnets-Bordako anderiak kontsolatü du elez
emazte zaharrik oküpü agitzen eztela ez.

Maria Soltek arrapostü
Santa Elisabet badüzü.
Saintu zahar bateganik oküpü agitü düzü
Kastero ere bada saintü, hala nizan beldür nüzü.

Kastero eztüzü saintü
sobera bürhaüti düzü.
Elizalat juan eta tabarnan egoiten düzü
Kastero denagatik saintü Maria Solt antzü zira zü.