---
id: tx-2458
izenburua: Heriotzatik Ihesi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yDbTVsf-5O4
---

Abuztuaren hasieran grabatuta (Urbasako Pilatos begiratokian) Jon eta Enekoren partez(corresjonmail.com) 
Letra : 
XXI. mendean Europa zaharreko demokrazia berrietan 
aurreko mendearen irudia! 
XXI. mendean Europa zaharreko demokrazia berrietan 
berriz ere gure begien aurrean! 
zentzu gabeko errealitatea ukatzen 
ixiltasunean bizitzen! 
gu beraiek izan ginela 
ahazten historia berriz errepikatzen! 
Giza eskubideak galduta 
Europako herri batean agintarien eskuetan (eskuetan) 
ez ahaztu idei galdutan 
Europako herri batean agintarien eskuetan! 
zer esango diegu ondorengoei 
izan zirelako gara eta garelako ez dira izango 
zer esango diegu ondorengoei 
ondorengoak bagara (X2) 
eustea ezinezkoa den zama 
motxila batean daramazuna 
malkoz betetzen diren hutsuneak 
kanpin denda zikin batean 
herriarentzat gerrak odol tanta dira 
ta zuentzako urrez beteriko altxorrak 
heriotzatik ihesi heriotzara 
denok izan ginen denok izango gara! 
baliogabeko txartela itsaso baten erdian agintarien eskuetan, eskuetan! 
Balio gabeko txartela itsaso baten erdian agintarien eskuetan 
zer esango diegu ondorengoei 
izan zirelako gara eta garelako ez dira izango 
zer esango diegu ondorengoei ondorengoak bagara (X2)