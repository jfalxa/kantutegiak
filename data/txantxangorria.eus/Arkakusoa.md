---
id: tx-1976
izenburua: Arkakusoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_20YMGOc78s
---

Ai! ai! ai! Takolo!
nere belarrian kilimak
egiten dizkidan zerbait daukat

Bai/ta nik e/re
ba/ina su/dur pun/tan

Zertan ibili zarete ba?

Matxinsaltoekin jolasteko
belardira joan gara.

Baina Pirrutxek
arkakusoak hartu ditu


Begira begira!!
Hortxe bat dabil

Ai! ai! ai!
Nire burura salto egin du

Uh! uh! uh  uh!
Goazen arkakusoarekin jolastera!!


Arkakuso bat bazebilen
bazebilen bazebilen
kilimak egiten zituen
Pirrutxen belarrian

O! Pirrutxek belarrian
arkakuso bat dauka!
-Zer???
Arkakuso bat!
-Zer???
Arkakuso bat!
-Aaaa!!!

Arkakuso bat bazebilen
bazebilen bazebilen
kilimak egiten zituen
Porrotxen sudurrean

O! Porrotxek sudurrean
arkakuso bat dauka!