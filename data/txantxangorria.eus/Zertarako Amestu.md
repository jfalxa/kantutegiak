---
id: tx-2973
izenburua: Zertarako Amestu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JRU8I0ZnWak
---

Eta lo bagaude, zertarako amestu?

Eta zertan esnatu hemen ez bazaude?

Eta lo bagaude, zertarako amets egin

etorriko diren egun libreekin?

Zertan amets egin?