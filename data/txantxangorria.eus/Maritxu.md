---
id: tx-914
izenburua: Maritxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YLx5CE27eX4
---

Maritxu nora zoaz eder galant hori?
Iturrira Bartolo nahi badezu etorri
Iturrian zer dago?
Ardotxo txuria
Biak edango degu nahi degun guztia