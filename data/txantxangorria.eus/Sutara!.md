---
id: tx-1016
izenburua: Sutara!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c5ZDxl1qAPk
---

"Gure Naufragioak" gure lan berriko bigarren singlea: "Sutara". 2018ko abuztuan, Usurbilen Haritz Harreguy Studio-an grabatua. Abesti honetan, Irantzu Garamendik (Whistle-a, Clowerwind taldea) eta Iñaki Zabaletak (Hammond-a) parte hartu dute. 

Bideoklip hau, Gernikako Iparragirre Rock Elkartean grabatua izan da, 2018ko urriaren 21ean. Hodei Izarra izan da bideokliparen zuzendaria. 

Grabaketa: Victor Sanchez 
Nahasketa: Haritz Harreguy 
Masterizazioa: Victor Garcia (Ultramarinos studio) 
Bideoaren grabaketan parte hartu dute: 
Maialen Arrinda, Mikel Natxiondo, Paulo Agirre, Endika Martinez, Aingeru Bidaurreta, Ander Marino Elorza eta Hodei Izarra.


LETRA: "SUTARA" (EUS)

Ideien gudu zelaian armak zorrozten 
Bihotza sutan eta zainak kiskaltzen 
Historiari goaz buelta ematera,
Inkisidore guztiak sutara!

Armiarma ari da zentsura ezartzen
Sarean harrapatu eta isiltzen
Odol zaporeaz ditugu hainbat eztarri,
Inoiz baino ozenago egin garrasi!

MINGAINAK MOZTU ETA LUMAK TINTA GABE UTZI
BOTEREAREN PROPAGANDA ARMA HILGARRI
MOZALEK EZIN GAITUZTE GU ISILARAZI
HERRIAREN GARRASIA AIREAN DANTZARI

Adierazpen askatasuna dugu aldarri, 
Autozentsura gainditu ta hitz egin!
Ni naiz Hasel, Strawberry eta Titiritero 
La Insurgencia, Valtonyc eta Alfredo!
 
MINGAINAK MOZTU ETA LUMAK TINTA GABE UTZI
BOTEREAREN PROPAGANDA ARMA HILGARRI
MOZALEK EZIN GAITUZTE GU ISILARAZI
HERRIAREN GARRASIA AIREAN DANTZAN

MINGAINAK MOZTU ETA LUMAK TINTA GABE UTZI
BOTEREAREN PROPAGANDA ARMA HILGARRI
MOZALEK EZIN GAITUZTE GU ISILARAZI
HERRIAREN GARRASIA AIREAN DANTZARI

LETRA: "SUTARA" (CAST)

Afilando las armas en la trinchera de las ideas
Corazón ardiente y venas humeantes
Vamos a darle la vuelta a la historia,
Todxs lxs inquisidorxs a la hoguera!

La araña nos censura
Enredándonos y callándonos en su tela
Todas las gargantas sangrantes,
Que griten más fuerte que nunca!

CORTA LAS LENGUAS Y DEJA LAS PLUMAS SIN TINTA
LA PROPAGANDA DEL PODER; ARMA MORTÍFERA
QUE LOS BOZALES NO NOS HAGAN CALLAR
QUE EL CLAMOR DEL PUEBLO RESUENE AL VIENTO

Reivindicamos la libertad de expresión,
Supera la autocensura y toma la palabra!
Yo soy Hasel, Strawberry y lxs Titiriteros,
La Insurgencia, Valtonyc Alfredo!

CORTA LAS LENGUAS Y DEJA LAS PLUMAS SIN TINTA
LA PROPAGANDA DEL PODER; ARMA MORTÍFERA
QUE LOS BOZALES NO NOS HAGAN CALLAR
QUE EL CLAMOR DEL PUEBLO RESUENE AL VIENTO

CORTA LAS LENGUAS Y DEJA LAS PLUMAS SIN TINTA
LA PROPAGANDA DEL PODER; ARMA MORTÍFERA
QUE LOS BOZALES NO NOS HAGAN CALLAR
QUE EL CLAMOR DEL PUEBLO RESUENE AL VIENTO