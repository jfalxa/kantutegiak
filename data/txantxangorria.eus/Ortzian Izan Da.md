---
id: tx-1244
izenburua: Ortzian Izan Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oxSYwXN9cCI
---

Natxo de Felipe eta Oskorri sortu zen, 2018-02-10an Barkoxeko Hotel Chilon izaniko emanaldian bukatzeko Barkoxetarrek eskeinitako abestia



Bilbon Barkoxen egunkoa
berdin bihar ta etzikoa
Euskara plazan agertua
hau dadin gure bekatua

Ortzian izan da goiz Oskorri
biharko dena hitz iturri

Aberats omen han Bizkaia
euskara aldiz ihesean
Ez ziren urtu Espainian
eta zutitu "Aresti-an"

Zazpi kaletan zazpi kume
bilatzen une eta gune
Otsoa han zen ingurune
baina ez lortu ixilune

Bixente Natxo eta Anton
bizkar hezurra ezin egon
Kantan jarraiki ez hor konpon
otoi Kyrie Eleison!