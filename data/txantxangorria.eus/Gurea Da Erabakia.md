---
id: tx-2823
izenburua: Gurea Da Erabakia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vlCgCP6cb4k
---

Herri, zahar baten ametsa, ezazue pentsa, ahaztuko 

dugunik, gora goierri.

Ia, guztiok elkartu,  gure indarrak batu, ta hartu dezagun, 

erabakia.

Gure, esku dago eta, ez egon zer gerta, denon erronka da, 

gure ta zure

Jada, gurea da hitza, hori da baldintza, konplexurik gabe, 

herri bat gara. 

Unea, iritsi da…….

gurea da…. erabakia….erabakia.