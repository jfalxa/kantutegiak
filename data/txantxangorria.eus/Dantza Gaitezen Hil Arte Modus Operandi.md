---
id: tx-567
izenburua: Dantza Gaitezen Hil Arte Modus Operandi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UyC_rX7wYDY
---

Indartsuak, hilezkorrak, aberatsak eta baikorrak
behartsuak, hauskorrak, ahulak, ezkor eta hilkorrak
Akelarre honetan egin dezagun dantza
akelarre honetan erre bizitza
Sugarretan erreko ditugu injustizia,
desberdinkeria eta faltsukeria
sugarretan erreko ditugu inbidia,
dirugosea eta indarkeria
o! Dantza dezagun hil arte bizitzaren dantza etengabe!
Lizunak, ameslariak, gaiztoak eta gudariak
maitaleak, etsituak, etsaiak, itxaropentsuak
Akelarre honetan egin dezagun dantza
akelarre honetan erre bizitza
Sugarretan erreko ditugu injustizia,
desberdinkeria eta faltsukeria
sugarretan erreko ditugu inbidia,
dirugosea eta indarkeria
o! Dantza dezagun hil arte bizitzaren dantza etengabe!
Ukabilak altxa ditzagun indartzeko
ahotsak altxa ditzagun gu entzuteko
gerriak eta hankak prest erasorako
gorputzak hemen dira gure azken dantzarako