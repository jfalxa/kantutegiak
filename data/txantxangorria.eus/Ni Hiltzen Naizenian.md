---
id: tx-588
izenburua: Ni Hiltzen Naizenian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iC07_b3SUZA
---

1. Ni hiltzen naizenian,
La la la la la...
Ni hiltzen naizenian,
Ez ehortz elizan,
Ez ehortz elizan.

2. Bainan ehortz nazazue
La la la la la...
Bainan ehortz nazazue
Xai zola batian
Xai zola batian.

3. Burutz barrikarat,
La la la la la...
Burutz barrikarat
Ahoz duxularat,
Ahoz duxularat.

4. Duxula eroriz,
La la la la la...
Duxula eroriz
Baduket zer edan,
Baduket zer edan."