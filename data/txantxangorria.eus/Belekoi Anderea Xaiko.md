---
id: tx-1615
izenburua: Belekoi Anderea Xaiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C9uLFzTrwD0
---

Musika: XABIER ZABALA
Letra: UNAI ITURRIAGA

Nik ez ditut maite
egun argia eta eguzkia
Nik gaua dut etxe
naiz itzaletako piztia.
Bele batek noizbait
kabia egin zidan bihotzean.
Zoriontsua naiz
norbait triste 
dagoen bakoitzean.

Belekoi anderea naiz,
zuk zer daukazun 
nik huraxe nahi.
Irriaren etsai,
koloretako ametsena ere bai.
Zure argiaren lapurra,
ni, Beleko andere maltzurra.

Nik ostadarrari
kolore guztiak ostu nizkion,
nik eguzkiari
laino beltz bat itsatsi nion,
haizeari tiro,
euria botila batean gorde...
Halere berriro
Zergaitik lehertzen dira
mila kolore?

Belekoi anderea naiz,
zuk zer daukazun 
nik huraxe nahi.
Irriaren etsai,
koloretako ametsena ere bai.
Zure argiaren lapurra,
ni, Beleko andere maltzurra.