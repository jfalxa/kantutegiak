---
id: tx-780
izenburua: Usaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j-CvXRgP6po
---

Zuretzako da poematxo hau
Sasipoetak idatzia
Sentimendu nahasiek
Bultza egiten naute, bultza 

Zure ondoan sekula ere
ez nauzu imaginatu
ta nik koitadua itxaropentsu
urrunetik laztantzen zaitut

usaiak bialtzen dituzun unean
batenbat niretzat al da?
galtzaileon munduan bizi nahi neska
aldamenean

agian sorte apur batekin 
batera biziko gara
pertsona arrunten gisara
gure munduan