---
id: tx-486
izenburua: Axun Kla Kla
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/f3F2EO3WJvs
---

Axun, kla, kla,
axun, kla, kla,
Josepa Antoni bruji
leatza ostu dabe
Leon Pepenetik.

Leatza ostu dabe
Leon Pepenetik,
Mari mutrikuarra
begira dabil.

Ortuan perejilla
saleruan gatza,
atera platerera
jan daigun leatza."