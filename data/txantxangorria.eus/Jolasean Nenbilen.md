---
id: tx-2994
izenburua: Jolasean Nenbilen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ay0ssKAj9QY
---

Jolasean nenbilen ilundu zenean.
Kukua kantuan hasi zen gauean.
Kuku! Kuku! Kuku! Kuku! Kuku!

    - Otsoa, hor zaude?
    - Bai, baina galtzontziloak janzten ari naiz!

Jolasean nenbilen ilundu zenean.
Kukua kantuan hasi zen gauean.
Kuku! Kuku! Kuku! Kuku! Kuku!

    - Otsoa, hor zaude?
    - Bai, baina kamiseta janzten ari naiz!

Jolasean nenbilen ilundu zenean.
Kukua kantuan hasi zen gauean.
Kuku! Kuku! Kuku! Kuku! Kuku!

    - Otsoa, hor zaude?
    - Baaai… eta banoa… jan egingo zaitut!