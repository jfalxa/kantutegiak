---
id: tx-1023
izenburua: Reggaean Hegan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R6kUcuIt66s
---

Video by: Ibi Life Productions


REGGAEAN HEGAN VENDETTAren azken bideoklipa da.

VENDETTAk datorren abenduaren 28an eszenatokioak utziko ditu Atarrabian izango den kontzertu berezi batean. Bideoklip berri honekin, taldeak agur esaten dio beraien jarraitzaileeri 10 urteren ondoren eta 5 diskekin bizkarrean. Beraien kontzertuetaz gozatu duen orori, famili hau haunditzen lagundu dutenei, eta zati aktiboa eta ezinbestekoa izan diren guztiei maitasunez beteriko agur bat da bideoklip hau. Bizipen ahaztezineko hamarkada bat izan da. Kantuak dioen bezela “AGUR BIHOTZEKO”. Bihotzez eta betirako eskerrez beterik jasotako bero guztiagatik VENDETTAk agur esaten du. Betirarte lagunak.



Agur urruneko
Agur bihotzeko
Otsoen antzera, aldera, uzkurtu gabe, koldartu gabe


Otsoen antzera, izutu gabe
Ta elkarrekin egindako
Bidai luzeak
Markatu zituen aztarnak
Elurra ta lurra
Amildegi hartan
Pausatu den arranoa
Hegan astera doa!
Ta nik igo nahi nuke magalean
Ta nik igo nahi nuke magalean
Agur urruneko
Agur bihotzeko
Arrainen antzera, aldera, damutu, itsutu gabe
Arrainen antzera, amore eman gabe
Ta elkarrekin egindako
Barre ta algarak
Ematen oraindik indarra
Zerua itsasoan. Berriz:
Amildegi hartan
Pausatu den arranoa
Hegan astera doa!
Ta nik igo nahi nuke magalean
Ta nik igo nahi nuke magalean
Ta zeruan bizi, zeruan hezi geroa
Ta zeruan bizi, zeruan hezi herri hau
Ta zeruan bizi, zeruan hezi
Han ez omen baita malkorik
Eta ez omen agurrik
Agur urruneko
Agur bihotzeko