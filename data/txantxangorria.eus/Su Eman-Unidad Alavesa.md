---
id: tx-1220
izenburua: Su Eman-Unidad Alavesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dNhubCK8vB8
---

Atzera aurrera eta gora begiratuz
inor ez dago lehengo toki berean
posizioak hartuz goaz denok
kamuflajearen garaietara
egokitutako haragijale hutsak gara
moralista desnaturalizatuak
baina pozik hartzen ditut egun berriaren
lehen eguzki printzak
azken finean bizi naizenaren seinale dira
negarraren antagonismoa naiz
kompleju denen arerio ta
etorkizuneko gizaseme perfektua
eta orain gauero ibiltzen naiz
bide ilun eta desolatuetan
inongo atxekimendu falta
ikaragarri ari zait baina
batere ez nau arduratzen horrek
zeren pozik hartzen ditut egun berriaren
lehen eguzki printzak
azken finean bizi naizenaren sinale dira
negarraren antagonismoa naiz
kompleju denen arerio ta
etorkizuneko gizaseme perfektua
eta denak galdezka datozkit
zer egun ote dezaketen?
Su eman eta behetik hasi
geruza zahar denak txikitu
ta denboraren aizkora 
hura goran mantendu