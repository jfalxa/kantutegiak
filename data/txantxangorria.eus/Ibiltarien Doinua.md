---
id: tx-2234
izenburua: Ibiltarien Doinua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FoTtmq_7hw4
---

Bide, joateko bide,
ez al dun posible,
joan dena itzultzerik libre?
Bide, oin azpiko bide,
ez al duk posible, 
izaterik zenbat eskubide? 
erantzun ezan bide, 
zein dudan norabide? 
Erantzun ezak bide,
noiz helduko haiz libre?
Etxera, libre? 
𝑰𝒊𝒊𝒊𝒊, 𝑰𝒃𝒊𝒍𝒊𝒂𝒏-𝒊𝒃𝒊𝒍𝒊𝒂𝒏, 𝒅𝒂𝒃𝒊𝒍𝒆𝒏 𝒃𝒊𝒅𝒆𝒂,
𝑬𝒕𝒐𝒓 𝒛𝒂𝒊𝒕𝒆𝒛 𝒈𝒖𝒓𝒆𝒌𝒊𝒏, 𝑰𝒛𝒂𝒏 𝑩𝒊𝒅𝒆𝒂.
Bide, dispertsio bide,
ez al dun posible, 
denak izaterik herrikide?
Bide, zapalduen bide,
ez al duk posible
zutik ibiltzea hi re? 
Erantzun ezan bide, 
ez ote den posible?
Erantzun ezakbide, 
izan gaitezen libre! 
Bidez bide libre! 
𝑰𝒊𝒊𝒊𝒊, 𝑰𝒃𝒊𝒍𝒊𝒂𝒏-𝒊𝒃𝒊𝒍𝒊𝒂𝒏, 𝒅𝒂𝒃𝒊𝒍𝒆𝒏 𝒃𝒊𝒅𝒆𝒂,
𝑬𝒕𝒐𝒓 𝒛𝒂𝒊𝒕𝒆𝒛 𝒈𝒖𝒓𝒆𝒌𝒊𝒏, 𝑰𝒛𝒂𝒏 𝑩𝒊𝒅𝒆𝒂.


Pauso bakoitzan atzetik bestea
bizikidetza jarrita helbide
denak eskubide denen jabe
bide baten zailena da hastea
heri gaztea, luze bidea
Gelditzeko batere astirik gabe
hasia da sentitzen nekea
Halere oinez izan arte libre!