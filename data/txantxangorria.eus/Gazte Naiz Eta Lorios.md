---
id: tx-500
izenburua: Gazte Naiz Eta Lorios
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g4tHv8EKneM
---

Gazte naiz eta lorios, izpiritua kurios,
Batez agrados, bertzeaz jelos, mutil gaztekin gau oroz.
Ama, ez nizan bada uros, gorputza ez baitut maluros?

2. Aita eta ama etzaten dire ganbera baten barnian;
Ganbera baten barnian eta dudarik gabe ohian.
Jelosiarik ez ukaiteko ni maiteñoaren aldian.

3. Athe onduan makila, eta leiho azpian zulubi;
Nere maitia handik sartzen da sekeretuan krudelki;
Sekeretuan krudelki eta amak oraino ez daki.

4. Ama ta alaba biak gogoeta onduan denbora joan eta,
Amak alaba konseilatzen du beria duen bezela,
Mutil gaztiekin ibil ez dadiela hura denboran bezala.

5.-Nere alaba, mutur zabala, zertan abila hi hola?
Ez dun ez pollit gauerdi ondoan mutil gaztekin lanbura.
Ala etzakinat othe abilan jorratu nahiz alorra?"