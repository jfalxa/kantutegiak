---
id: tx-1419
izenburua: Guretzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T6Nhbr8-98A
---

Guretzat berdin dira 
astea eta jaia 
lana bihurtzen dugu 
kantatzeko gaia. 
Har ditzagun eskuan 
giltza eta laia 
gariz eta burdinaz 
lantzeko Bizkaia.

Guretzat berdin dira...

Gitarrarekin aire berri bat 
daramagu kantuz egun, 
abesti libre eta leiala 
prestu eta noblezadun, 
haren medioz zer garen ongi 
izan gaitezen ezagun, 
herri langile nekazaria 
hala defenda dezagun.

Guretzat berdin dira...

Gure gogoa ez bedi otoi 
kantu honekin akaba, 
gure ekintza izan dadila 
gure hitzaren alaba, 
egun batean esan dezagun 
ez hala biz hala da, 
nekea eta lana dirade 
zorionaren aldaba.

Guretzat berdin dira...

Bide honetan baldin bagoaz 
jakin dezagun zergatik, 
zin egin behar dugu zintzoki 
bakoitzak duenagatik, 
sinestunak dudarik gabe 
bere jaungoikoagatik, 
eta sineste gabeak berriz 
bere ohoreagatik.

Guretzat berdin dira...