---
id: tx-1670
izenburua: Hemen Naukazu Urtz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fe0nxTtMcMg
---

Orri zatitxo baten aurrean
ohartu gabe doa denbora
idatzi nahi ez duen arkatza
alboan irrifartsu dudala.
Daukadan onena eman behar
onena jasotzen dudalako
zurekin ezin bada, zu gabe;
zurekin bada, nahiago ...

Hemen naukazu,
agertuko zaren zai
iraganaren orri txuria
betetzeko prest;
zure ahotsa
entzungo nuke nonnahi
belarrira baldin bada ere
xuxurlatuta.

Sinesten ez dutenen mirari
zure eskuetan naukazu sarri;
horrela sentitzen naiz goxoen,
honela egon ninteke beti.

Hemen naukazu,
agertuko zaren zai
iraganaren orri txuria
betetzeko prest;
euria zinen
eta busti egiten nintzen
zure besoetan urtzen zen
izotza nintzen.

Atzo gauean zurekin
egin nuen amets:
euria zinen
eta busti egiten nintzen