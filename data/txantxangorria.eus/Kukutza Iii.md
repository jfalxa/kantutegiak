---
id: tx-2087
izenburua: Kukutza Iii
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2tSAQjdV1V4
---

Lehengotik hirugarrenera
Laugarren pisutik espaziora
Metro karratuetatik kilometroetara


Ezerezetik osotasunera
Txikitasunetik handitasunera
Ametsetatik errealitatera

Eskuak altxatuz erabakia
Eskuak batuz eraikia
Eskuak nahastuz zabaldua

Kukutza bihotz bat gure aurrean, hutsa betetzen duena
Taupadaka esnatuz geldirik gaudenak
Taupadaka esnatuz lotan gaudenak

Taupada bakoitzaz koloreztatu zuri-beltza
Taupada bakoitzaz irribarre bat

Ideiak bultzatuz erabakia
Ideiak batuz eraikia
Ideiak nahastuz zabaldua

Kukutza bihotz bat indarrez betetzen gaituena
Taupadak esnatuz etsita gaudenak
Taupadaka esnatuz minduta gaudenak

Taupada bakoitzaz koloreztatu zuri-beltza
Taupada bakoitzaz irribarre bat

-Taupadaaaaz
-Doinua etengabea
-Taupadaaaa
-Odola berotzean
-Biziriiiik
-Nahiz ta urteak pasata 
Nirea, zuena, zaindu, denona baita

Kaixo!
Zenbat denbora ikusi gabe
Zenbat gau, zenbat gau, ta hauetan zu galde!
Esadazu zer berri eh!
Nola da hau? 

Egunerokotasunaren irudi hau
Bilatzen zaitut gaur
Hamar urteko haur

Zaunkaka datozte hiru munipa, altxau!
Popatik hartzen, fenomeno,
Beti zuen alde
Eskutik helduta daude rap ta Rekalde 
Beti berdin hala ere ezberdin
Goitik behera ta gora beti
Hainbat kultura batuta, barrua.
Errekerre ari, emateko bultza
Ametsak lortzeko konpromezua
Zutik irauntzeko gogortasuna
Okupa, bidea, zuzena, zurea
 Ezarpenaren kontrako jokabide
Okupazioa, jarrera 
Auzoa berreskuratu
Pertsona bat, etxe bat
Kapitalaren menpeko mundu honetan
Hamar jabedun, baina gutxi dira
Zer? Hemen kukutza
Non? Errekaldetik
Nola? Bihotzez, eta ideiak sinisten

Zenbat urte poltsa barruan?
Tinko ta gogor eutsi kukutza!

Taupadaka esnatuz gelditik gaudenak
Taupadaka esnatuz lotan gaudenak
Taupadaka esnatuz etsita gaudenak
Taupadaka esnatuz minduta gaudenak

Taupada bakoitzaz koloreztatu zuri-beltza
Taupada bakoitzaz irribarre bat