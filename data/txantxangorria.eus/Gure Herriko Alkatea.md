---
id: tx-2942
izenburua: Gure Herriko Alkatea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kQXKcC4YvQ0
---

Gure herriko alkatea
kate, kate, katea;
alkate dotorea
tore, tore, torea.
Jazten duen trajeak
traje, traje, trajeak;
tomate kolorea
lore, lore, lorea.
Gure herriko alkatea
kate, kate, katea;
alkate xelebrea
lebre, lebre, lebrea.
Haretxen bibotea
bote, bote, botea;
fideo luzez betea
bete, bete, betea.