---
id: tx-2322
izenburua: Ez Dago Hilda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZGWzPfAVQ1Q
---

Jah Goikoa 
Eta Reggae Zaharra
Iruñerria gaztetxeen alde 
Zen Marabillas 
ta Arrotxapea 
Euskal taldeak gaztetxeen alde 
Zizur, Barañain 
ta Berriozar 
Eutsi Burlata, 
bertze nafarrak 
Iruñerrian 
Jamaica Clash 
Eta Titanians, Skabidean Iruñea Nola!?, 
Sustraian Records 
Aurten urrian 
Ez Dago Hilda 25an ta 26an 
Zarata ta doinuez dantzaldi Ilargi parkean zilipurdi 
Uxue Barkos eta Barzina 
Mola, Sanjurjo, orain Txibite Enrique Maya ez dago hilda 
Ez dago hilda,
Iruñerrian Dantza ilargian, 
berri-zarrean Iruñerria ez dago hilda 
Zabal bideak eta aireak 
Gure hizkuntzak har dezan arnas