---
id: tx-430
izenburua: Askatasunezko Bakea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jwSJpZ4pQoA
---

Donostiako taldea genuen Euripean Sua. 1994 urtean grabatu zuten honako maketa Josean eta Iñaki Garrido "Cibertrack"-eko teknikoen ardurapean. Taldea maketa honetarako honako hauek osatzen zuten Igor Kalzada (bateria eta koro-ahotsak), Iñigo Lurgain (kitarra eta ahotsak) eta Koldo Soret (baxua eta ahots nagusia),


Askatasunezko bakeak zilegi behar du izan, guztion nahia ene herrian.
Urteen poderioz helburu berbera izan degu, ez dadila utopian erori.

Lehiotik begira kafesnea eskuan dudala berriak entzutean hozten zait.
Zerua txikia izango degu euskaldunak gure poza ezartzeko aina.

Alaitu ta ez tristetu inoiz, esperantza badegu
ta hala adierazten degu itxaropen kanta honetan.

Jendean aurrean zorroztuta gure eztarriak, askatasuna aldarrikatzen.
Bihar ene herria egunkaritan agertzeaz gain, mapetan ikustean, hau poza!!

Alaitu ta ez tristetu inoiz…

Bagatoz ta hementxe gaude, bagoaz mugarik gabe. Sarrionandiak, “maite genituen gauzengatik erori ginen preso, baina gure maitasuna oraindik ez dago preso” behin zioen.