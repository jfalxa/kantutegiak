---
id: tx-830
izenburua: Hemen Geratzeko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hiBETg1efrA
---

Eskutik helduta doaz
bidegurutze berri baten aurrean
gure bihotzak.
zer egingo dugu neska?
gaur hartuko dugun bidea
betirako izango al da?
nahiko nuke zurekin pasatzea
momentu oro laztan eta muxu artean.

Nahiko nuke betiko izatea.
eskutik helduta doaz
beldurrari fideltasuna zor dioten
gure zalantzak.
Bestela nola esango nizun
begietara begiratzen “maite zaitut”
ene laztana.

Nahiko nuke zurekin pasatzea
momentu oro laztan ta muxu artean
nahiko nuke betiko izatea,
betirako izan dadila!
zurekin egotea
momentu guztietan,
goxuak zein mikatzak,
etorriko direnak
hemen geratzeko.

Gertu ditut somatzen
pozik eta irrifarrez,
datozen ilargipeko gau luze-luzeak.
gaba soilik izan dadila
bakardadearen testigua,
zeru beltz zoragarria.