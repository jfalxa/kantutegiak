---
id: tx-758
izenburua: Heldu Da Garaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U9zRikGbawo
---

Musika eta letra: Iñigo Etxezarreta (E.T.S.)
Pianoa:  Gorka Pastor
Hariak: Claudia Oses eta Ivan Carmona (Suakai). Drum Groove Studioan grabatuak Iosu Ervitiren eskutik.
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Ekoizpen exekutiboa: Baga-Biga

Hainbeste gauza esan nahi dizkizut denbora gutxian, 
azkeneko urteetan niretzat gorde ditudanak.
Iluntasunean zu izan zinen nire egunsentia,
aireratzeko behar nuen haize bolada.

Heldu da garaia, eskertzeko eman didazun guztia.
Irribarre onenak esnatzen ditugu gaudenean.
Zurekin batera berriz asmatu nahiko nuke bizia.
Maite zaitudalako nire ondoan.

Nagusiegi sentitzen naizenean agertzen zara
gogoratzeko non dagoen izan ginen umea.
Orain arte bidea batera egin dugun bezala
eskutik helduta sortuko dugu unibertso bat.

Ekaitzaren erdian aterperik onena
lortu nuen zure besoetan.
Momentu zailenetan zein une goxoetan
hor zaude zu nire alboan.