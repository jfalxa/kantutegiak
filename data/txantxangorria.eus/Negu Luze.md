---
id: tx-2364
izenburua: Negu Luze
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aMS8LvbeS1k
---

Am        E           Am      E           Am       
lam                                   Mi7         lam
     Negu luze hotzetik
Bakardade sakonetik
                              Mi   lam    rem        Mi       lam
batasun etxetik  eta odoletik
 

Mi                                       lam              Mi7              lam
Gu guztion odoletik jaioko da
                                 Mi            lam                rem  Mi    lam
Euskadi batuko  duen  gazteria      (birritan)

lam                                   Mi7         lam
     Negu luze hotzetik
Bakardade sakonetik
                              Mi   lam    rem        Mi       lam
batasun etxetik  eta odoletik

Mi                                       lam              Mi7              lam
Gu guztion odoletik jaioko da
                                 Mi            lam                rem  Mi    lam
Euskadi batuko  duen  gazteria      (birritan)

lam                                         Mi
     Hala, hala bedi
                                  lam                                         
Hala bedi