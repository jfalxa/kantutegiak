---
id: tx-1557
izenburua: Txis Pun! Korrika!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hjKez88zqbY
---

Hiru, bi, bat, zero! Txis pun! Korrika! 
K-O-R-R-I-K-A 
Korri, korri, Korrika 
Euskaraz martxan alai denok 
Korrika, denok, katxiporreta! 

K-O-R-R-I-K-A 
Korri, katxiporreta! 
Euskaraz, arraio tximista! 
Korrika, denok, korri Korrika. 

Arin pausoak, tipi-taupa bihotza ta zangoak 
Altxa besoak, txalo, txalo, jo txaloak! 
Aio, aio! Hator gurekin, hi, Pelaio! 
Pozik bizi, euskarari heldu, dibi-daba-daba-du 
Egin irrintzi, euskaraz aiaiaijiji!! 

Esnatu gara eta gurekin batera euskara 
Kantari goaz eskolara lara-lara 
Ñam-ñam bapo, mmm! Tori bi muxu, lau piropo! (Eskerrik asko!) 

Arratsaldero plazan elkartu, dibi-daba jolastu! 
Ondo lo egin, koloretako ametsekin.