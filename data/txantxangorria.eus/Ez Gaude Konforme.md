---
id: tx-798
izenburua: Ez Gaude Konforme
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WVgAFFOlFf4
---

Gazte gera gazte,
ta ez gaude konforme
mundu garbiago bat
bizi nahi genduke,
gezurraren kontra
injustizirik ez,
gazte gera gazte
ez gaude konforme

Nere abesti hau ez da politika,
justizia nahi det, egiak agertu
gizonak daduzkan eskubide hoiek
kunplitu ditezen bihotzez
abestu
ez noa inorren kontra egia nahi dut

Gazte gera gazte...

Gazte geralako maitasun batean
bizi nahi genduke, bainan baita ere,
gehiegi dakigu munduan dagozen
gesur da gorroto ta injustizi asko
gauza hoien kontra abeztu nahi degu

Gazte gera gazte...

Bainan aho batez gure herriaren
problemak esaten uzten ez digute
guk nahiago dugu betiko ixildu
gero egun batez beldurraren gaitik
traidore ginala ezan ez dezatengazte

Gazte gera gazte...