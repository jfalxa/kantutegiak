---
id: tx-1843
izenburua: Araba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/82wbAEGcS-U
---

Araba, oi Araba!
zu ote zare
aitaren zazpigarren alaba?
Iparraldeko oihanek
diraute lerdenik
hegoa-aldean, berriz
zelai bilutsik, bilutsik.

Araba, oi Araba!
zu ote zare
aitaren zazpigarren alaba?
Bazegoen bide bat
bazegoen zugaitz bat;
Bainan zugaitz erori hori
bidean oztopoa, oztopoa.

Araba, oi Araba!
zu ote zare
aitaren zazpigarren alaba?
Gure nahia, ekaitza,
haizea, legorra
gizona zapaldua
bizitza, negarra, negarra.

Araba, oi Araba!
zu ote zare
aitaren zazpigarren alaba?