---
id: tx-3381
izenburua: Dr. Jeiki & Mr. Jai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J0jX4wPollI
---

Bertsoak: Unai Iturriaga eta Igor Elortza
Doinua: Josu Zabala

Irrintzika dabil despertadorea
Eta esnatu da Jeiki Doktorea
Galdurik kordea, ordea, utzi du ohea
Burmuinetan garagardo uholdea

Ze deabru! Dutxa bat eta salbu
Eta bi ur tragu ta ikusten dut klaru-klaru
Tartean ispilu zikin bat daukagu
Eta bera da bai, baina nire antza du

Neu naiz bai : Mr. Jai. Zerbait nahi?
Gaur ez ninduzun espero, ai!
Bart ernai, bart alai, bart ze guai...
Ba orain eutsi'ok, eta izorr'ai !

Michael Jackson ta Fred Aster antzo
aire-aireka zenbiltzan atzo
Gaur mantso-mantso jeiki zara, txo!
Lau hankan ibiltzea ere lartxo...

Nor naiz? Non nago? Zer egun da?
Bart bezpera gaur biharamuna!
Non ote dago Espidifena?
Speeda bera igual onena!
Hobe ohera, lo egitera
Ta mundua zakutik hartzera!
Baina hitzordua dut jarrita
Eta ardura al dizu, hipokrita?

Eskilaretan behera aurpegi goibela
hilak eskela batean bezela
Eta auzokoek diote honela:
"Ez dira ordu bi etxeratu dela!"

Oh!, gure kalea. Oh!, kale gurea...
Potroetaraino dago oinezkoen gunea
Hirutik bat txakurra eta beste bat umea
Eta hirugarrena, txakurkumea!

Ta hor datorkit bat oinez lasai-lasai noala
esanez dudala itxura makala. Ala!
Ipiniozu, ipini, berehala
aurpegia plater bat txipiroi bezala

Kaguenlaputa!, trago bat behar dut...
Behingoz zentzuzko zerbait entzun dizut
Hau ez da martxa! Ezin bururik altxa!
Horretarako dago ardao baltza

Hau da, hau, taberna kutrea... kaguendios!
Gainezka koipez, gainezka ardoz
Bai, trago pare bat edango dut... ados!
"- ¿Has dicho uno?" - No, no no... He ditxo dos!

Zergatik begiratzen ote dit gaizki?
Daukadan dardara honegatik, noski
Aski! Ardao txarra, ta gainera garesti
Bota aurpegira edo eman bi hosti

Ez bat ez bi, bertan egin dut txala
Kojo puta horrek garbitu dezala
Ez, ez da normal-normala atzetik dudala
Pena ematen du herrenka doala
Tira, tira... Hau gehiegi da!
Egun batzuk horrelakoak dira
Ez dut sekula gehiago edango...
Matxakea hasi da:ondo, ondo

Hau dardardara! Hau izerdi hotza!
Ta orain zer dator? Pekatuen aitortza?
Beldurra ta lotsa... Topera dut bihotza!
Arakatu patrikak hor daukazu poltsa

Orain goiz da. Hobe utzi geroko, loko!
Zuk jarraitu hain xalo; zuk jarraitu hain tonto...
Hobe lasai ibili: aleggre mas non tropo
Horrela ez duzu larrurik joko

Ta zu, tabernikola, nora begira zaude?
Onena izango dugu egitea alde
Tragoak debalde, presoen alde!
Propina ere utzi dut, badaezpada ere

Ahaztu zait, gedituta negoen!
Hala omen, baina bakarrik ondoen
Gaur ere haserretuko zait koadrila
Popatik hartzera joan dadila!
Total, beti-betiko moduan amaitzeko...
Batere diskutitu barik hobeto
Baina laurak dira, honezkero
Ba orduan kafe, baina kafe torero!

Non datozen bi tipo... Bi pailazo!
azken pintxoa nahi didatenak jaso
Begiekin eraso, begietara so
sartu dizkiegu hamarna balazo!

Kantari ochote bat... Zortzi kalbo
tabernako beste puntan hobeto nago
Oraintxe akabo! Ezin duzu gehiago, klaro...
Eskote hori zeuri begira dago!

Bular artean preso erori orduko
hemendik hanka ospa ihes egin eta punto!
Barraz barra, marraz marraz, komunik komun
gure betiko botika hartu dezagun

Harrotu hautsak, Doktore!
Urrup eta klik ta txartelak gorde
ta tira gogor bonbari, eta segi
Barrura datoz beste bi

Eta esan ozen: zein da hemen kanpeoia?
Gora camboya! Hau da sasoia!
Moralez nago gainezka...
Baskito eta neska: Biba euskal festa!

Tragoa bota diot sportbilly lerdo bati...
Karate daki... basati... egingo nau lau zati!
Ausarta zara-ta kenduiozu kubata
ta taka bi hanka artean zapata

Taberna osoa neure kontra da itzuli: (Uii...)
Rambo, Perurena, Roky, Tyson eta Bruce Lee
Kolpeka... topeka... galdu duzu oreka...
Ez duzu zereginik: bihurtu zaitez kroketa!

Gaizki nago, noan etxera
hilabeterako, gutxienez, ezkutatzera
Berandu zabiltza... bada denbora oraindik!
Nahikoa hostia nik jaso dut jadanik
Kafetegi kutre batean sartuta
azken zerbeza eta azken punta
Beraz, gogor samar gosalduta
Etxe bila zabiltza kalean galduta
Ohean jausi naiz baldar-baldarrik
Horixe da bizitza ez egin negarrik
Eta azkenean lasai, azkenean bakarrik...
Jeiki Doktorea lo... Eta Mister Jai itzarrik