---
id: tx-5
izenburua: Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hOSSzSD8vtQ
---

BELTZA - PIZTIAK-
Musika, hitzak eta irudiak Josu Aranbarri Aramaio

Euria, hiriaren pekatuak, ileari lotuta
Buruan hiri urrutiak, zerua beltza da
Pentsatzen usteltzen naizenean.
Ilargia, bide beltzak
Zorromorroen zaratak buruan
Kalean jendea hila, pozik dirudi
Pozik dirudi euripean
Pozik, euripean pozik
Tentsioa zeropean
Oihu beltzak, begi gorriak
Irrati berriak, bertsioak
Heriotza, heriotza!!
Egunero hiltzen dira katuak gurdien azpian
Egunero hiltzen dira saguak gurdien azpian
Egunero hiltzen naiz ni, hire begien azpian!
Hire begien azpian!
Hire begi, hire begien azpian!