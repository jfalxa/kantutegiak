---
id: tx-185
izenburua: Argi Azkorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TB1vvROycq4
---

Osto berriez dira zühainak beztitü, soho’ta baratzetan liliak zabaltü, Alhor güziak ere elgian berdatü, ekiak ühaitzari hura zilharstatü. 
Oi ! zuinen eder zeitan, goiza bedatsean, mendi küküla sütan argi azkorrian, Ekia da jeikitzen bere leinhürian‘ta gaizak arrapizten ene üngürian. 
Goizik argi zeiniak agur herriari, oilarrak kükürrükü egün berriari, Oilarraren oihia entzün bezain sarri, txoriak habietan bertan iratzarri. 
Ekia beno lehen ohetik jeikirik, Aita alhorrian da palari lotürik, 
Amattok sü eder bat jadanik piztürik, saldaren egiteko eztüke berantik. 
Hitzak: Pierra LANOUGUERE 
Müsika: Jean ETCHART