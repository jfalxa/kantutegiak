---
id: tx-1256
izenburua: Hemen Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NEPwU_CwGK4
---

Atzera begira pasa dezakegu bizitza,
begiak itxita itsutuz gertatzen denak
gu ukituko ez bagindu bezala.
Entzutea ere utz genezake,
hitzegiteaz ahaztu gintezke,
noizbait izan ginela esan dezaten.

Gaur hemen gara,
atzo izan ginena baino askoz gehiago.
Bihar egonen gara,
ta egonen dira, izanen gara!

Sakabanatu gaituzte, 
kilometroen bidez etsipenean ito nahiean,
kondenatuak izan gara,
bizi osoa atzerrian, ziegan,giltzapean.
Noiz nahi bortxatuak, torturatuak,
non nahi jarraitu eta deserriratuak.
Suntsiarazi nahi gaituzten estatu terroristak!

Aurrera begira jarraituko dut,
etorkizun askea gauzatuko da.
Etsaiei hortzak gogor estutu
eta harro azaldu: gaur borrokatu!

Gaur hemen gara....