---
id: tx-2388
izenburua: Feministon Harria Bizia Arnas
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rzc-f9NmPvc
---

2018ko Feministon Herria-rako egindako kantua. 
Berako Atala estudioan Iñigo Irazokirekin grabatua. 
Jonan Ordorikak Mamia estudioan masterizatua.
paroles
Gogor, nahiz heltzen diozuen gogor 
herdoilak aspaldi irentsi zuen aingura zaharrari 
guk, indarrak batuz, azalera ekarriko dugu marea biziak eta gatz uholdeak sortuz 

Babestuko ditugu logika guztiak kolokan jartzen dituzten begiradak, larruak, ahotsak 
inungo geltokietako komunetako istorio iheskor heze herabeak 
zaurgarria kopetan idatzia duten gorputzak 
lurraren eta lurraldeen zaintzaile kartsuak. 
Ihes egin nahi ez luketen destinurik gabeko iheslariak 
ertzetan, ertzetatik mundua osotzen dutenak 
mundua osotzen dugunak. 

Ibilian-ibilian 
indartuko da aldarria 
sabeletik eztarrira 
eztarritik mundura 
zabalduko ulua 

Ibilian-ibilian 
indartuko da aldarria 
sabeletik eztarrira 
eztarritik mundura 
zabalduko ulua 

Ibilian-ibilian 
indartuko da aldarria 
sabeletik eztarrira 
eztarritik mundura 
zabalduko ulua 

Ibilian-ibilian 
indartuko da aldarria 
sabeletik eztarrira, 
eztarritik mundura 
hortzimuga berria 
arnas bizia 

Ibilian-ibilian 
indartuko da aldarria 
zabalduko ulua 
hortzimuga berria 
arnas bizia... bizia arnas!
crédits
paru le 16 octobre 2018 
Ilargi Agirre: bateria 
Ibai Gogortza: gitarrak, perkusioak 
Joseba Baleztena: bajua, gitarrak, perkusioak 
Miren Narbaiza: gitarrak, ahotsa 
Goldatz Berako Emakume Taldea: koroak 
Emma Gascó: artelana