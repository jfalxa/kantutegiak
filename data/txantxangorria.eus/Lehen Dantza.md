---
id: tx-2623
izenburua: Lehen Dantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sdM7LG1Dky8
---

Oroitzen begirada gurutzatzean
irrifar egin zenidan gau hartan.
Faborez eskatuz gerturatu zinen
eskutik heldu ta ateraz plazara.
 
Berriz har nazazu gaur zure besoen artean
gogoratuz lehen dantza
 
Geroztik zenbat dantza egina dugun
goxoki konpasa emanez denari.
Gaur arte alkarrekin egina dugun
Bidea luzatu dezagun betiko.
 
Berriz har nazazu gaur zure besoen artean
Gogoratuz lehen dantza (BIS)
 
Berriz har nazazu gaur zure besoen artean
Gogoratuz lehen dantza (BIS)