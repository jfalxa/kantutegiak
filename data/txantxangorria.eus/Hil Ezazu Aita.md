---
id: tx-1803
izenburua: Hil Ezazu Aita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gdP5BApXF6Q
---

Etzanda nengoela nere gelan
besteak diskutitzen sukaldean
peta bat egin nuen lo egiteko
ta nere hondamenaz ahaztutzeko
zeozerk esan zidan: adi ametsari
eta kontuz ibili aitarekin!

Aita hiltzailea izan zen
aitak ezin zuen gainditu
aitak egin behar izan zuen, zintzilik zegoen,
egin behar zuen.

Kopetaraino zegoen emazte,
seme--alabez eta lanez
kotxe berri baterako diru beharrez.

Gehiegi edan zuen batetan
armariotik atera zuen eskopeta
tiro bat bota anaiari, tiro bat bota emazteari,
beste bi tiro arrebentzat
eta beste bat katuarentzat
Eta pareko tabernan erre eta edaten
guztiz itsu ari nintzen jartzen.

Tabernatik atera eta bi argi doaz nere etxera
bata urdina, polizia da ta,
beste gorria, anbulantzia.

Ulertzen dur iristerakoan
bolbora usaina, odola parketan
argi urdinak aita daroa
eta gorriak bost hilotzak.
Bapatean despertadoreak jotzen du
ta ohartzen naiz dena amets ban izan dela
soilik amets bat, ez da ezer gertatu
zergatik orduan ezin dut lasaitu?
Sukaldean jarraitzen dira oihuak,
bata mozkortia, besteak galduak
eta orduan bost tiro entzuten dira
eta gero soilik ixistasuna.
Pasiloan urrats batzuk irristatzen
aita bere lana amaitu nahiez ote?
Bala bat baino gehiago badu oraindik
zer egin behar dut nik?
Ospa egitea hemendik. (bis)

Bolbora usaina, odola parketan
hil ezazu aita, hil ezazu bertan
ez baduzu hiltzen, hila zaude
ez baduzu hiltzen, hilko zaitu berak.



I was lying in my room
all arguing in the kitchen
I got a joint to sleep
and to forget my ruin
and a voice said: look at the dream
and be careful with your old
my father was a murderer, my father could not help
my dad had to do
hung, had to do
I was fed up with his wife, his children and his
gigs,
We did not have money or new car
One day I drank the more removed the gun cabinet
He shot his wife, one for my brother
Two shots to my sisters and one for the cat
And in the front bar, smoking and drinking,
I was fucking blind Menudo
I leave the bar, and I see two lights that go towards my cas to
One is blue, is wood the other is red, the ambulance
I get to come .. smell of gunpowder, blood on the parquet
Blue light leads to my father, and the red five bodies
Suddenly the alarm clock
And I realize that it was all a dream
It was just a dream, nothing happened ...
Why I can not reassure me then?
In the kitchen are still arguing
One drunk, hysterical others
When five shots ring
And in the house there is silence
You hear footsteps in the hallway
My old wants to finish the little job
Although he is more of a shot
So I decide to give me the pyro
The smell of gunpowder, blood on the parquet
Kill your old matale well
If you kill him, you're dead,
If not matasm will kill you.