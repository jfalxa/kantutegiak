---
id: tx-2893
izenburua: Kutxi Kutxi Marilutxi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GrodrKufEFg
---

Kutxi, kutxi, kutxi, kutxitxi
Kutxi, kutxi, Marilutxi
Marilutxi lotsa gutxi
Kutxi, kutxi, kutxi, kutxitxi
pilaka begiak itxi
Kutxi, kutxi, kutxi, kutxitxi
igo-igo, jaitsi-jaitsi
Kutxi, kutxi, kutxi, kutxitxi
ipurdia erakutsi