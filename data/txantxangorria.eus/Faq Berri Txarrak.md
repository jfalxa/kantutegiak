---
id: tx-1823
izenburua: Faq Berri Txarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PUNCMNJO6P4
---

Neurtu al duzu inoiz nahi duzuna eta behar duzunaren tarteko distantzia?
Egin al duzu hori laburtzeko behar zen guzti-guztia? Inoiz , inoiz ...
Aurkitu al duzu inoiz esan gabekoen hondakinetan bilatzen zenuena?
Isiltasunaren forma ezberdinak hitz higatuetan
Kalkulatu al duzu aukerak dakarren galera horren neurria?
Ta inoiz gogoratzen zara norabidea aldarazi zizun berriaz?
Ta inoiz eman ote duzu urrats ergelen bat ondorioak doitu gabe?
Ta inoiz utzi al dituzu esaera zaharrak bere horretan zahartzen?
Inoiz ihes egitea ezinbestekoa balitz norantz bota lehenengo pausua?
Ta kalkulatu al duzu aukerak dakarren galera horren neurria?
Ta inoiz gogoratzen zara norabidea aldarazi zizun berriaz?
Ta inoiz galdetu al duzu nora joan daitekeen betiko galdu den denbora?
Inoiz erre zintuen su horrek berak argitu zizun jarraitzeko bidea
ibili , erori eta altxatzearen oinarrizko legea
Ikastearen dema ala dema bera irakaspen , ber gauza dira..