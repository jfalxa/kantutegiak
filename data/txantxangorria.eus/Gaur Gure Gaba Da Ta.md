---
id: tx-1963
izenburua: Gaur Gure Gaba Da Ta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hfio-Eee7YA
---

Aidien doan mozue
zeuretzat, politte
hartu eta egin eizu zeurie.
Itxaropenaren truke
dana daroa debalde
zabaldu eiotzazu zeure atie.
Aidien dau galderie
sutan anbientie
ez eiztazu, politte, ezetzik esan.
Noiz itzaliko zai nago
barrutik jatorten su hau
badakizu zer eskatzen etorri nazen.
Gaur gure gaba da ta
zatoz ogera
itzali daigun sue
gaba laburre da ta
besterik ez da behar
zatoz ogera
gaur gure gaba da ta.
Eitteko dekoguzen gauzek
danak egingo doguz
ez einozu gaur gabaz alperrik galdu.
Apurtu eizuz katiek
ta eiztazu barretxu bet
atara dekozun enkantue, joder!
Gaur gure gaba da ta
zatoz ogera
itzali daigun sue
gaba laburre da ta
besterik ez da behar
zatoz ogera
gaur gure gaba da ta.