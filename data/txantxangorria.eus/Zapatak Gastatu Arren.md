---
id: tx-1347
izenburua: Zapatak Gastatu Arren
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DTnUEG-gph0
---

Gazteak baleki eta zaharrak baleza
irakurri dut
mikroipuinak egiz gainezka egin du
garagardo aparrak bezala
atzeko ispiluari begira
oroi ditut erlojupeko lasterketak
gauarekin tranpetan.

Ile gutxiago ajeak luzeago 
zulotik geroz eta gertuago
urruntzen zaitudanean
gauerdiko pailazo bat
malabaristarik txarrena
onespen keinu baten zai

Zabortegietan zoriontasuna bilatu dut
noraezean ikutu eta urperatua 
edalontzi hutsetan
atzeko ispiluari begira
zin dagizut zapatak gastatu arren
berdin naramatela.