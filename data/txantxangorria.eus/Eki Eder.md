---
id: tx-2718
izenburua: Eki Eder
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0cKAEp4awtk
---

Udazkenean mendiak
Urtxoen eho tokiak
Sare 'ta Ihiziariak
Alde guzitan gordeak
Berdin ditugu etsaiak
Gordagian jarriak
Gure seme ta alaben...oi ebasieak

(Eki eder sutan bero
Euskaldun bihotzak oro
Gudukari diten gero 
Bilaka gua egunero)

Su pindarraren indarrak
Duelarik sortzen su garra
Preso ezarri gatua
Nola den amorratua
Berdin dira euskaldunak
gizonak hoberenak
baina agertzen zimizta... ziztatu eta.

(Eki eder.......)
Bururatu da guretzat
Ilusionen denbora
Jaun haundi bat Parisetik
Jinen zela salbatzera
Nunbaiteko zeruetan 
Balin bada jainkorik
Guk es dugu snetsiko... nagusi denik