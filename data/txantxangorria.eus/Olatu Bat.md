---
id: tx-642
izenburua: Olatu Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MTc_5h94xGg
---

Patuaren eraginez
Egur eta ohol finez
Patuaren eraginez
Egur eta ohol finez
Partitzeko munduminez
Haizeari putz eginez
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa
Ez lo kanta ez olerki
Intziriak ereserki
Ez lo kanta ez olerk
Intziriak ereserki
Egunsentiz-egunsenti
Hemen bihar gaur da beti
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa
Zirrikituz-zirrikitu
Mapa dute erdibitu
Zirrikituz-zirrikitu
Mapa dute erdibitu
Baina nahiz ez iruditu
Urak ere mugak ditu
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa
Olatu bat bi olatu
Zenbat bihotz mutilatu
Olatu bat bi olatu
Zenbat bihotz mutilatu
Senatari diputatu
Nola gauden begiratu
Ta eskuak ondoratu
Ez gaitezen hondoratu
Ta eskuak ondoratu
Ez gaitezen hondoratu
Ta eskuak ondoratu
Ez gaitezen hondoratu
Ta eskuak ondoratu
Ez gaitezen hondoratu
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa
Triste dago Lur osoa
Ta lur jota itsasoa