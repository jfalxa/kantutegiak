---
id: tx-1789
izenburua: Hondartza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L0gZlKE0duM
---

arai! Larai!
hondartzan gu alai
Bai! Bai! Bai! Bai!
bainujantziaz blai.

Dantza! Dantza!
Maite dut hondartza
La! La! La! La!
Kuboa eta pala.

Hondarra oinpean
antxetak hegaz goian
korrika joan, 
txonbo egin uretan.

Gaztelu hauskorrak.
Flotagailu airezkoak
itsas ertzean 
txirlen maskorrak

Tori! Krema
goxo eman
jantzi kapela
etzanda egoteaz 
aspertu naiz berehala