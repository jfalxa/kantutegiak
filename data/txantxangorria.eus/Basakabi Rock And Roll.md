---
id: tx-901
izenburua: Basakabi Rock And Roll
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/coiLhWForhE
---

Zer moduz gure lagunak 
Zuekin hemen gaude 
Zer moduz gure lagunak 
Gurekin dantza egiten 
Basakabi daukazue 
Zuek alaitzeko prest.
Gerriak alde batera 
Eta gero bestera 
Dantza ezazu gaur gauean 
Goiza iritsi artean 
Basakabi heldu baita
Hain umore onean.
Rock and rolla baina hoberik 
Gizonak ez du asmatu Ilargira joan aurretik
Nahaigo dut dantzatu Ilargian ezbaitdago
Honelako martxarik.
Musika maite baduzu
Ligatzearekin bat
Etzazu denbora galdu
Aukeratu baten bat
Guk musika egingo dugu
Zuk egintzazu bestea.
Jai hauetan ez daukagu
Giro alaiaren falta
Neska-mutilak aurrera
Ez oraindik nekatu
Gaupasa egitera goaz
Honela irauten badu.