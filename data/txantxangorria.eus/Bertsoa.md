---
id: tx-1507
izenburua: Bertsoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zwjPRJBH-zw
---

Gure herriko sentimendua
ta sentimenduen harri
Bizkaia osoko ikur nagusi
beti zahar beti barri
eguneroko zaharren bizipoz
gazteen amets iturri
lehoi irudi indar eredu
gure zoramen oinarri
zurigorri zoragarri.