---
id: tx-1441
izenburua: Ezin Zaitut Jasan Ft Talco
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tgdlDj_MhXI
---

Ez nuen inoiz imajinatu horrelako peaje bat,
eskolako mutil hura handia zen. egia esan.
Orain, betaurrekorik ez dut, baina argi ikus dezaket
zurekin dudan zigorra, odol pakto bay da, tamalez...

Eztabaida guztietan, zuk beti duzu arrazoia,
lotsagarri uzten nauzu aukera daukazunean,
pelikula filandesak aspergarriak direla
esan nizun, baina, tira! Zineman behintzat isiltzen zara...

Ez dut zu bezalako
lagunik aurkituko,
parekorik ez duzu
ezin zaitut jasan.

Etxera zatozenean, ginebra desagertzen da.
Botea jartzerakoan, beti zaude komunean.
Zenbatetan amestu dut akabatzen zaitudala?
Zementozko zapatekin igeri egiterik al da?

Ez dut zu bezalako
lagunik aurkituko,
parekorik ez duzu
ezin zaitut jasan.

Ez dut zu bezalako
lagunik aurkituko,
parekorik ez duzu
ezin zaitut jasan.

Sareetako argazkiak
barregarri uzten nautenak
ahal guztiduna bera
Uooooohhhh!

Edozertan aditua
Wikipedia biziduna,
harro deman saritua!