---
id: tx-2422
izenburua: E I A
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2n8OUNczpUY
---

basabürüko ikastolak 40 urte !

marrazkiak eta zizelanak alozeko ikastolako eta uztaro ateliereko ikasleek hontürik
les dessins et sculptures ont été réalisées par les élèves de l'ikastola d'Alos et de l'atelier uztaro

soinü grabaketa, nahasketa eta mastering 
enregistrement son, mixage et mastering _____ Oihan Oliarj

filmaketa, muntaketa eta animazioa

esker mila Alozeko ikastolari eta herriko etxeari
Kategoria


Bethi paseatzen partekatzeko nahian
Bestetan ibilten Xiberootsekilan
Giro hona sakatzen lagünekilan
Ez diagü deuse besterik gure gogoan
Gaüaldi goxo baten igaiteko nahian
Gira gaur heltürik züen herrian
Garagardoa presta orai ostatüan
Taldekideekin bethi beikira egarrian 

Errepika:
Gurekin kantatzeko prest ziden eia
Raggamufina maite düzüen eia
Erritman segitzeko trempüan zideia
Ez lotsor izan gurekilan hei

Aste oroz gira biltzen Zinkan errepikatzen
Lanetik elkirik orai müsikari lotzen
Müsika tresnekilan gure salda prestatzen
Garagardo edaten ta sonü berri sortzen
Raggamuffin Hip-Hop müsika
Bürüan aidatzen anai-arreba
Tziauste gurekin bizia llabür beita
Gurekilan profeita oraina egün beita

Egün Alozeko haurrekin gozatzen
Ikastolaren urtebürüa ospatzen
Berrogei urte orai dereiziegü eskertzen
Gure mintzajearen betiko biziazten
Hainbeste urtetan aitetamak borrokatzen
Franzia ta Espainiak beitzien debekatzen
Egiazko mintzajea dügü bai begiatzen
Ez gütüe züen legeak sekülan eküatzen


Gurekin kirol egiteko prest ziden eia

Maite dütüzien bakantzak eia

Eta Eüskaraz aritzeko prest ziden eia

Kilparen ikusteko prest ziden eia

Basabürüarekin kantatü eia

Gurekin idazteko prest ziden eia

Haurrekin dantzatzeko prest ziden eia

Orai txostakatzeko prest ziden eia

Gurekin ikasteko prest ziden eia

Ikastolarekin kantatzeko eia