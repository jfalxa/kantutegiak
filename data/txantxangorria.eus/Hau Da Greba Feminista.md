---
id: tx-2359
izenburua: Hau Da Greba Feminista
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o2NNdUgULLc
---

Musika: Olatz Urkia Larrañaga
Hitzak: Olatz Urkia Larrañaga eta Maialen Oleaga Nuño
Argazkiak: M8 plataformako komunikazio taldea


HAU DA GREBA FEMINISTA
ETORRI GUREKIN AHIZPA
ELKARREKIN SUNTSITZERA
SISTEMA KAPITALISTA
Sistema patriarkala dela diogu
gizonak gizonentzat (hauen alde) dute diseinatu
argi dagoena da hau ez dela justu
ez zentzuzko, ez pareko, ez orekatu.
Nahikoa da nahi dugu orain oihukatu
elkarrekin indarrak batuz borrokatu
gure bizitza askeak pozik dantzatu
lotzen gaituzten kateak guztiz apurtu.
HAU DA GREBA FEMINISTA
ETORRI GUREKIN AHIZPA
ELKARREKIN SUNTSITZERA
SISTEMA KAPITALISTA
Pentsalari, kirolari edo kantari
izan nahi dugun hori aske erabaki
trans, bollera, emakume edo trabesti
naizen izaki eder hau izaten bizi.
Berdintasuna nahi dugu ondo ulertu
zuek baduzuela hemen zer jorratu
zapaltzaile roletik zaitezte askatu
eta elkarrekin dezagun borrokatu.
HAU DA GREBA FEMINISTA
ETORRI GUREKIN AHIZPA
ELKARREKIN SUNTSITZERA
SISTEMA KAPITALISTA
Zaintza gure lana dela sinestarazi
txikitatik hala dela dugu ikasi
patriarkatua da honetatik bizi
baina guk oraindik ere ez dugu etsi.
Zapalketa dela nahi dugu erakutsi
ta guztiok borrokara altxa-arazi
soldata duina behar dugu irabazi
sexismoa, matxismoa guztia hautsi!
HAU DA GREBA FEMINISTA
ETORRI GUREKIN AHIZPA
ELKARREKIN SUNTSITZERA
SISTEMA KAPITALISTA
Edertasuna horra hor abanikoa
barrutik daramagu hori da kontua.
Sistema kontsumistak eraikitakoa
desegiten gabiltza bada ta ordua.
Behar ez duguna erostera bultzatu
kontzientzia hartu zirko horrekin bukatu.
Berreraiki, jeiki ta ohiturak aldatu
gorputza ta Amalurra natural maitatu.
HAU DA GREBA FEMINISTA
ETORRI GUREKIN AHIZPA
ELKARREKIN SUNTSITZERA
SISTEMA KAPITALISTA
PATRIALKAL TA SEXISTA
HOMOFOBO TA MATXISTA!
Olatz Urkia Larrañaga
Maialen Oleaga Nuño