---
id: tx-2738
izenburua: Athenry-Ko Zelaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RFKv2R9VfQ0
---

Kartzela leihotik neska bat entzun dut nik oihuka
Mikel haizatu dute herritik
artoa ebatsirik doluan barna sarturik
ur artean beha dago kartzela

Urrundu dire zelaiak
xoriak hegal kolpez ere bai
amodioa hegalpean
kantatuz gure ametsak
bakartasun handian gelditzen naiz

Kartzela leihotik gazte bat entzun dut nik oihuka
othoi ez izan beldur andrea
goseak oldarturik hemen naiz apaldurik
orain haurra haz dezazu bakarrik

Itsaso hegitik azken izarra etzaten da
zeruak estalirik untzia berekin
maitea egin othoitz ta atxik itxaropena
bakartasun handian gelditzen naiz

Abesti hau Pete St. John-ek konposatu zuen 1970ko hamarkadan. Abestiaren haria XIX. mendeko gosete handian kokaturik dago eta bertan Michael gaztearen zorigaitza kontatzen da, artoa lapurtu ondoren Australiara herbesteratzen dutenean.

Ez da abesti soila, erresistentzia abestia baizik. Hasiera-hasieratik kutsu politikoa hartu zuen eta egun Irlandako rugby eta futbol selekzioetako hinmo ez-ofiziala da. Horrela, 2012.ko Eurokopan irakaspen handia eman zuten irlandarrek kanporatuak izan arren denek elkarrekin abesti hau kantatzean eta haien harrotasuna erakustean.