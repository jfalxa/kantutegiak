---
id: tx-435
izenburua: Argizirrintako Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/icaY1SmLJiI
---

Gazteluko harresietan jeikitzen naiz apirila oroz, 
goizaren hats epela sasietan betidanik zutaz amoros.

Zeruaren arrosa ixur dadila karriketan zehar,
argizirrintaren ederra bihur dakigun amodio behar.

Eki-leinuru  samurrean ezti altxatzean goiza, 
koropilatzen dut ahurrean musu zilarren oroitza.

Ehun urte iragan dira denbora geldirikan dago, 
zeruko hodei arinei begira keinu baten haiduru nago.

Auto hotsek ixila hausten beldurra daukat bihotzean, 
argizirrinta bekaitza errausten dabilan ahanzmen hotzean.