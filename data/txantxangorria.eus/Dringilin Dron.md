---
id: tx-2169
izenburua: Dringilin Dron
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-sftc3w1vCM
---

Dringilin dron, gaur Gabon,
dringilin dron, gaur Gabon,
sabela betea daukat eta
besteak hor konpon.

Mazkelo bete aza egosi,
ori, zuri ta gorriak
bereala iruntsi neutzazan
askenengoko orriak.

Hiru ortzeko tresnatxo batez,
morokil-ore bai litzan
ezti-lapiko haunditxu bati
barrua uts-uts ein neuntsan

Gabon kanta