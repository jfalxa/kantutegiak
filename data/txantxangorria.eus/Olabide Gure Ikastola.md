---
id: tx-2784
izenburua: Olabide Gure Ikastola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1D71A5Tso0c
---

Gu… Gure sustraiak, lurrean ondo lotuaz.
Gu… Gure ikastolan, haziak loratuz doaz.
…
Gu… Gure irria, zorionaren islapena da.
Gu… Gure baratzan, bidean lagun artean.
…
Gu… Gure familia, bidean eusten gaituena.
Gu.. Gure bidea, bukaeran etorkizuna.
…
Gaztetxo nintzela behin landatu nuen hazia
Ondo zaindu nuen emanez ura eta argia
Gaur zuhaitza da behin landatu nuena
Sano eta zuzen bai, biziz betea.
Olabide batuz bagoaz elkarlanean
Zurekin batera eginez gure bidea
Belarritan musika, begietan koloreak
Eta gure ahoan beti izango da Euskara!
OLABIDE BATUZ BAGOAZ ELKARLANEAN
ZUREKIN BATERA EGINEZ GURE BIDEA
BELARRITAN MUSIKA, BEGIETAN KOLOREAK
ETA GURE AHOAN… EUSKARA!
Sustrai zaharrenetatik hosto txikira
elkarrekin eskutik helduta.
Gure zuhaitzaren itzalpean
Aurkituko dugu beti babesa.
Gure bihotzeko taupadek laguntzen digute
bidea egiten
Familia eta lagunen indarrari esker
Urratsak ematen