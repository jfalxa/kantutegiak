---
id: tx-2795
izenburua: Hartu, Tenka, Tira Nafarroa Oinez 2016
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6e86A2eZAO4
---

Hartu, Tenka, Tira!
2016, Lodosa-Viana

Hartu eskuetan,
irribarrea ezpainetan
Tenka hari guztiak,
lotu elkarri hizkiak!

Tira mingainari,
josi zure hitzak sokari!
Hitz egin, kantatu, hasi beldurrik gabe!
Hari bat falta da, zu ez bazaude!

Nafarroa Oinez, nire indarraz, zure aupadaz!
Viana eta Lodosan ere euskaraz!
Hartu Tenka Tira, denon eskuak behar dira!
Har dezagun soka Tenka eta Tira!!

Gari soro, ortu, mahasti...
Inguruko paisaien argazki.
Mahats more, piper gorri...
Denak goxo izugarri!
Lurra beti eder dago,
egon liteke ederrago!

Animo, etorri gurekin batera,
kolore berri bat ereitera!!

"Ebro ibai ertzean bizi garen bi ikastola txiki gara; poz-pozik etortzen gara goizero eta pila bat ikasten dugu, baina guk kalean ere euskaraz bizi nahi dugu".

Nafarroa Oinez, nire indarraz, zure aupadaz!
Viana eta Lodosan ere euskaraz!
Hartu Tenka Tira, denon eskuak behar dira!
Har dezagun soka Tenka eta Tira!!