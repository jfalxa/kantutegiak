---
id: tx-2982
izenburua: Txorinoak Kaiolan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O8JEGLrdIis
---

1. Txoriñoak kaiolan
tristerik du kantatzen.
Duelarik han zer jan, zer edan, 
kanpua du desiratzen.
Zeren, zeren, 
libertatea zoinen eder den.

2. Hik kanpoko txoria
sogiok kaiolari.
Ahal baldin bahedi, bahedi,
hartarik begira hadi.
zeren, zeren,
libertatea zoinen eder den.

Barda amets egin dit
Maitia ikhousirik:
Ikhous eta ezin mintza,
Ezta phena handia?
Ala ezina!
Desiratzen nuke hiltzia..."