---
id: tx-853
izenburua: Keinu Konplizeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QYcjaS8dRjo
---

Abestia: Keinu Konplizeak

Hitzak: Eneko Fernandez

Kolaborazioa: Iker Fernandez, Gaizka Otsoa eta Gaizka San Pedro

Soinua eta Produkzioa: Adrian Vallejo (Musiikki Estudioa)

Irudiak: Goio Barandalla

Bideoa: Alex Sanz


Asteburua heldu ta
Ipuin berdinaren parte
Jendearen olatuan
Sentitzen nintzen Uharte
Ezezagunetatik at
Lagunetatik aparte
Betiko istorioa
Zu azaldu zinen arte

Nik ezin bixta baztertu
Ta zu begiratzen segi
Ola tontor ttikina ere
Bihur luteke sumendi
Lehen pausua ematean
Naiz iruditu zailegi
Hurbiltzea derrigorra
Ta ezezkoa zilegi

Abesti baten doinua
zerbeza baten aparra
azkartutako taupadak
Eta zure irrifarra
Gure Keinu konplizeak
Laztana laztan truke
Gauaren iluna zure
Begiek argitzen dute

Dantza dantzaz muxuz muxu
Joan ginen ezagutzen
Bihotza zabaldu ahala
gure gorputzak estutzen
Saiatua naizen arren
Hitz guttirekin laburtzen
Noizbait erranen dizuet
Nolatan  non bukatu zen