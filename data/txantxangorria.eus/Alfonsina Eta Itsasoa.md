---
id: tx-1188
izenburua: Alfonsina Eta Itsasoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mchf_JK9G_c
---

Urak laztandutako hondartza zuri,
urrats txikitxoak dabiltza haundi,
isiltasunezko samin isila jario itsaso ezkutuan,
garrasi mutu bizi isilak dio bi hitzaren bitsa.
Jainkoak dakizki hotz larriak, 
zure samin zaharren galgarria,
itsas barraskiloen magale unean biziz hiltzeko,
itsas barraskiloen kantu leunean kulunkatzeko... 
Agur Alfontsina, agur laztana,
bizi olerkiak bihotzean, 
oihutzar gaziak bakardadean, 
arima galdu usteldu harekin ametsetan, 
zure bidean lotan, Alfontsina, itsas mugetan!!!
Bost ziren atsoen bizkarrean,
itsas koralezko bideetan,
itsas zalditxoen dizdira biziak bertan pozaren kupira,
itsas ezkutuko lakunek jolasean betiko dira.
Hurbildu itsaso ilargiak lotan egiteko ordutxo bat,
nitaz galdetzean non nagoen ahaztu, 
joan naiz betirako,
nitaz galdetzean, hau mezutxua, 
hauxe esan "agur betirako"...
Agur Alfontsina, agur laztana,
bizi olerkiak bihotzean, 
oihutzar gaziak bakardadean, 
arima galdu usteldu harekin ametsetan, 
zure bidean lotan, Alfontsina, itsas mugetan!!!