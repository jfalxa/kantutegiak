---
id: tx-1391
izenburua: Eutsi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VksgjGO29mc
---

Azken greba, azken erresistentzia.
Azken liskarra poliziaren aurka, azken harrikada.
Azken sindikalistaren azken Audi-Mercedesa.

Azken autobusak daraman, azken eskirola.

Azken etapako azken esprinta.
Azken tangoa Bruselan,
azken aurre-erretiratuarentzat.
Azken unean azken lapurrak
azken gerentearen, azken sekretua osten du.

Azken eslogana
azken putakumearentzat.

Azken enpresaren azken sutea,
…azken tiragoma.
Azken langilea, azken heroia.
Azken langabearen azken lana.

Azken eslogana
azken putakumearentzat.

Azken asanblada,
azken zalditxoa, azken asteazkena.
Azken txirrindularia, azken aurreskua.
Azken trikitixa!!