---
id: tx-1884
izenburua: Topa Dagigun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W4HnLxwYT6E
---

Errioxak Baigorriko
Bakio nahiz Getariko
topa dagigun eta bakean
bizi gaitezen betiko.

Umeak arretaz horni
gazteei hitza eskaini
gaztetxoago eta haurrago
izan gaitezen geroni.

Errioxak Baigorriko
Bakio nahiz Getariko
topa dagigun eta bakean
bizi gaitezen betiko.

Ez uka amonei hitzik
entzun agureei pozik
zaharra ez da izandakoa
luzaroan dena baizik.

Errioxak Baigorriko
Bakio nahiz Getariko
topa dagigun eta bakean
bizi gaitezen betiko.

Hierarkia da lehorra
harremana emankorra
errespetuan dago gordeta
emakumeen altxorra.

Errioxak Baigorriko
Bakio nahiz Getariko
topa dagigun eta bakean
bizi gaitezen betiko