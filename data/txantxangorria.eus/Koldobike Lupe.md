---
id: tx-3114
izenburua: Koldobike Lupe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Wn5qunfzNGE
---

Kai aldeko hegal baten 
abesti hau entzun neban, 
gazte bat zan bermiotarra 
maitea eban bere gogoan. 

Koldobike, neure maite, 
zu zaitut zorion emaile, 
atsegiñez nauzu bete 
gau honetan niganatu zaitez. 

Ezer barik, iñor barik, 
hemen nauzu, neu bakarrik, 
neure bihotza ez dau pozik, 
ez dago iñor zu bezelakorik. 

Koldobike, neure maite, 
zu zaitut zorion emaile 
atsegiñez nauzu bete 
gau honetan niganatu zaitez.