---
id: tx-2236
izenburua: Triste Bizi Naiz Eta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R3CgzDmYlNE
---

Triste bizi naiz eta
hilko banintz hobe
badaukat bihotzian
hainbat atsekabe
Maite bat maitatzen det
bainan haren jabe
sekula izateko
esperantzik gabe (Bis)

Bihotz baten lekuan
mila banituzke
zuretzako maitia
izango lirake
Baina milan lekuan
bat besterik ez det
har zazu ba maitia
bat hau mila bider. (Bis)

Nere maite polita
nola zera bizi?
zortzi egun hauetan
etzaitut ikusi
Uste det zabiltzala
nigandik igesi
ez didazu ematen
atsekabe gutxi. (Bis)