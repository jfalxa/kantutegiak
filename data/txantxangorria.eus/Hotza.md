---
id: tx-2609
izenburua: Hotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j0St57I6quQ
---

Lander Garrok egindako 2ZIO taldearen "Bideak" bigarren diskako "Hotza" abestiaren bideoklipa. Diskoa irailaren 21etik aurrera kalean!


Bizitzaren balantzan hari gainean dantzan gabiltza ta hitza jartzen dugu zalantzan. Hegorik gabeko xori bat zertan datzan, amets baten norantzan iparra da baldintza.

Gizakiak hainbeste min sortu du beregan, nik nahiago dut sinetsi ama naturarengan nahi dudan eran senti dezakedan apurrean, zangoan lurrean ta burua hegan.

Ez didate eman beste hauturik. Begirada batean ez da sekreturik. Ez dut frogarik baina ez dut dudik:
grabitateak ez gintuen egin lurrari loturik.

Guk, hadi, guk hari gurari ugari galdegin dizkiogu ta bizitzea dugu sari. Begiak itxi dizkigute ez ginen itsu jaio, sinesmena zor zaio ikusten ez denari.

Joan dira lagunak, bat, bi eta hiru. Joan dira egunak zientziaren gatibu.
Hitz leunak harrotasunak ebatsi dizkigu
ta ezagutzen ez dugunak beldurra ematen digu.

Hamarkadaz hamarkada nun gauden ikusirik gugandik hasirik noiz ohartuko gara ahoa itxirik mintzatu ezin den gisara ezin dela ikusi begiratzen ez bada.

Hitzak hitz, olerkiak olerki, emeki, begiak itxi, besoak ideki... Guzia ulertuko genuke hobeki, beti,
isiltasuna entzuten bageneki.
 
Ta entzun, zabaldu zure zentzun guziak keriak bilakatzeko tasun.
Zurekiko zer maitasun daukazun jakin dezazun
buruaz galdetu, bihotzez erantzun.

Pentsatzen ezar zaite, nola litaike?
Besteak miretsi ta gu geu ez gara maite ispiluak istilu bilakatu dira
politagoak ginen urari begira

Denbora jakintza dela diote argiek, orduan pentsa zenbat dakiten mendiek, gure miseriek, gure hitz erdiek eragindako mina ikusi duten begiek.

Neguak ez du gau motzik, zoriona izotzaren gainean dabila ortsosik.
Baldin baduzu hotzik, bihotzik, esperantza berant lokartzen da ta esnatzen
da goizik.

Zergatik ez duenak sosik ez du hitzik ta ez duenak fitxik ez du pozik?
Hola ez da izanen pertsona urosik, eman gabe jasotzen duen jendea baizik.

Gaur gau guzieri aio.
Gaur haur berri bat da jaio.
Gaur bizitzak esnatu nau eta galdezka zuzendu natzaio.