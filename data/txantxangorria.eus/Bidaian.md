---
id: tx-568
izenburua: Bidaian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aEko-hP8zw4
---

Motxiladun ume, zuretzat.

Hainbeste gauza ditugu motxilan,
pijama eta istorio pila.
Ehundaka ume gurasoen bila
 bidaiatuz ia mundu erdia.


Ia ezin ikusi mendiak
ziega barruan itxita begiak
Denon artean piztuz argia ez dute itzaliko gure irria.


Bidaian zugan pentsatzen
 bidaian zenbat oroitzapen
zurekin egondako
momentu hoiek (x2)


Urrun sentitzen dugunok senti dezakegu hurbil
zainduz maite dugun hori
Ama aitarenera joaten denean berriz,
gaude bakarrik, amesten elkarrekin.


Bidaian zugan pentsatzen...


Iritsiko al da eguna 
egoerak buelta emango duna.
Gurasoak etxean
 ta koadrozko pakete ta motxilak hemendik at


Bidaian zugan...

Bidaian zugan pentsatzen,
bidaian zenbat oroitzapen
zurekin egoteko gogo horiek. 


-Musika grabaketa: Iker Lauroba
-Hitzak, ahotsa eta musika: Janitz Enparantza Alustiza eta Sua Enparantza Alustiza.
-Bideo egilea: Iker Mateo