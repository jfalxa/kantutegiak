---
id: tx-389
izenburua: Begiratu Ala Ikusi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lePt8Hjz8T4
---

BIREN taldearen "Begiratu ala ikusi" abestia. 

Begiratzea eta ikustea 
gauza bera balitza lez
gauza berbera...

horrela dabiz, horrela doaz
begi pare gehienak
horrela dabiz, horrela doaz

Begiratzea eta ikustea 
gauza bera balitza lez
gauza berbera...

Baina ez da ez da ez da
inola ere ez
baina ez da, ez da!

Esadazu bestela,
begi itsu batzuk nola
ikusi ahal duten 
itsuak ez diren beste gehiengoak 
ikusten ez duena

.....

Begiratzea eta ikustea 
gauza bera balitza lez
gauza berbera...

Baina ez da ez da ez da
inola ere ez
baina ez da, ez da!

Esadazu bestela,
begi itsu batzuk nola
ikusi ahal duten 
itsuak ez diren beste gehiengoak 
ikusten ez duena

Begiratzea eta ikustea
ez da...
begiratzea eta ikustea
gauza bera...

Begiratzea eta ikustea
ez da... gauza bera,

ez da... gauza bera.