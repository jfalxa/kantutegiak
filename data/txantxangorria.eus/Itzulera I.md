---
id: tx-1845
izenburua: Itzulera I
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bG6Y8cwJCiI
---

Olerkariak badatoz
joan ez balira  bezala datoz
Zer dakarte?
Zer dakarte?
Bihotza hautsi ez aparte.

Haserretuak badatoz
Ezez! Ezetz! esanez datoz
Zer dakarte?
Zer dakarte?
Bihotza hautsi ez aparte.



Intelektualak badatoz
arropa garbiekin datoz
Zer dakarte?
Zer dakarte?
Bihotza hautsi ez aparte.

Deserriratuak datoz
beldurrez joan
eta beldurrez datoz
Zer dakarte?
Zer da/kar/te?
Bihotza hautsi  ez aparte.

Bihotza hautsiak badatoz
bila joan zirenaren bila datoz
Zer dakarte?
Zer dakarte?
Eroan zutenaz  aparte.

Bila joan zirenaren bila datoz
Zer dakarte?
Zer dakarte?
Bihotza hautsi  ez aparte.