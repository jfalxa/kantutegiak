---
id: tx-1566
izenburua: Aro Berria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ub5LMFLSyFg
---

Kaiolan lez itxia
ukatuaz bizia,
izan da gure herria.

Heldu zaigu garaia,
nahiz eta izan zaila,
lortzeko gure nahia.

Joan gaitezen,
gure indarrak elkartuz.
Unea da, ezin dugu egin huts!

Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria,
hemen da!
Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria,
hemen da!

Aspaldi dator mina,
aldatuko bagina,
lor daiteke ezina.

Ez al duzu nahiago
gure herria a/keago?
Orain GURE ESKU DAGO.

Joan gaitezen,
gure indarrak elkartuz.
Unea da, ezin dugu /gin huts!

Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria,
hemen da!
Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria.

Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria,
hemen da!
Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria.
hemen da!

Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria,
hemen da!
Esnatzear da
lo zorrotik gure herria!
Hemen da jada aro berria.