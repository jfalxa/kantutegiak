---
id: tx-1243
izenburua: Eiheralarre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EvTFhjz8_uQ
---

Urte guziz uda hastean
amarekin autobusean
Eiheralarrera nindoan
eta hango aroztegian
polliki sartzen nintzenean
zer zoriona bihotzean
ikusiz aitatxi lanean!

Haur denborako
Eiheralarre
ez zaitut ahantziko

Haur denborako
Eiheralarre
maite zaitut betiko

Aitatxi ari zen bortizki
kiski-kaska ta kaska-kiski
burdin gorria joz ozenki
forma berria zezan aurki
goizean goizik bereziki
zaldiak ferratzen fermuki
trosta egin zezaten brauki!

Eiheralarreko bazterrak
mendi-zelai zinez ederrak
jende zintzoak, herritarrak
maite nituen hango haurrak
eta haiekin jan sagarrak
aroztegiko supindarrak
ematen dizkit bai indarrak.