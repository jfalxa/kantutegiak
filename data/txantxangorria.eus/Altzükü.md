---
id: tx-2963
izenburua: Altzükü
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rJvei1C9u3o
---

Altzükün sartzeko badira bizpahiru bide segürik
Arthozubia pettarretik, Sonborren gaiñaiti Arkütxetik
ta Bianda khaparretik, Bagadoy, Laphiztoy altetik,
Ahüzki bortian behera, Üxia basa bazterretik.

Maite dügün xokhuan aiphatzeko
Egün gira jüntatü khantatzeko
Gü beikirade denak Altzüküko
Bihotzez dügü errepikatüko.

Hido gaztelü idorroki, Muxako eta Harregia
Horien altzuan axolbian, bibil Altzüküko kharrika
Jauregiaren onduan, eliza et'arrabotia
Indar hartü nahiantaren, ostatüz hor bada haitia.

Kharrikako bazter bürgiak, Garraby eta Barrikata
Üxia eta Lats-Ibarra bai eta ere Gesalia,
horiek bat eginez dügü, Altzükü amiagarria
Mendi, oihanak üngürian, denentako plazer hargia.

Bethidanik Altzükütarra, bere xokhuan alagera
Khantariak ta dantzariak, hortik agertü izan dira,
Egün ere lehen bezala, khantatzen deiziegü gora,
Frisat harro zunbait emaiten, lehen gure aitek bezala.