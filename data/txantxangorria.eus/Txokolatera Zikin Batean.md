---
id: tx-2484
izenburua: Txokolatera Zikin Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TI2bevpD7eI
---

TXOKOLATERA ZIKIN BATEAN

(Herrikoia. Azpilkueta / Lekarotz / Sunbila)


Txokolatera zikin batean
ekarri zidan kafea
posible bazen etzuela nahi
hura_hartu gabe joatea
harrek zeukan barren guzia
armio sarez betea
amaren seme batayatuak
etzen posible_hura_hartzea.

Egun batean gelditu nintzan
ate xokoan gordia
hango manera_ederra_ikusita
ezin idukiz irria.
Auzoko_etxera lasterka yoanta
ekarri zuen argia:
etzakienak erran zezaken
eroa zala erdia.

           ♪♪♪♪♪♪

Bertze guztia ohera yoanta
gelditu nintzan bakarrik
egundaraino ezpaitut edan
hau bezalako naparrik
gustoa ere txarra zuen ta
batere_etzuen indarrik
hura baino_hoberik ezpalitz
nion elitzake mozkorrik.

Horra Maria, horra Maria
horra Maria Yazinta
mundu guzia gelditua da
gure berriak yakinta.
Egun batean erran zenidan
aldamenean yarrita
biak esposa behar ginduela
bertze guztiak utzita.