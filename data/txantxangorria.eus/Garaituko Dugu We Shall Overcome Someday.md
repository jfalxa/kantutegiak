---
id: tx-2701
izenburua: Garaituko Dugu We Shall Overcome Someday
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QkeDNSiYczQ
---

Garaituko dugu, garaituko dugu,

garaituko dugu, inoiz.

Bihotz-bihotzean, seguru naiz, 

garaituko dugu, inoiz.

Elkartuko gara, elkartuko gara,

elkartuko gara, inoiz.

Bihotz-bihotzean, seguru...

Bakez biziko gara, bakez biziko gara,

bakez biziko gara, inoiz.

Bihotz-bihotzean, seguru...

Libre izango gara, libre izango gara,

libre izango gara, inoiz.

Bihotz-bihotzean, seguru...

Ez gaude beldurrak, ez gaude beldurrak,

ez gaude beldurrak, egun.

Bihotz-bihotzean, seguru...

Garaituko dugu, garaituko dugu,

garaituko dugu, inoiz.

Bihotz-bihotzean, seguru...