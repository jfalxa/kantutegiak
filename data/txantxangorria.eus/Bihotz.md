---
id: tx-3406
izenburua: Bihotz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CEfv9SRi_QI
---

Gela ilun batean
bizi zara bihotz, 
sineskeriaz elikatuz, 
berezia zarela uste duzu.

Zeurekoi asmoak jorratuz, 
hilezkortasuna amets eta... 
zalantzaren itzalak 
ikaratzen zaitu.

Bihotz guztiak zara zugan 
baina, ez zara ohartzen. 
Bihotz ororen poz eta mina 
daramatzazu zure taupadetan, 
baina oraindik ez duzu ikusi, 
balio bilketan lausoturik 
baitzaude.

Hilezkor izan nahi duzu 
eta hilezkor zara, 
hilezkor bihotz guztietan, 
bihotzean...
bizi, irri, nigar, heriotz, 
bihotz.