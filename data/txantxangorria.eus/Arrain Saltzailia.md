---
id: tx-2548
izenburua: Arrain Saltzailia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-Ydweea367g
---

Emakume arrain-saltzaileen inguruko kanta
Ondarroako gizon talde bat kantuan, 1963an; emakume arrain-saltzaileen inguruko kanta.


Ni naiz emakume bat
arrain saltzailia beti.
Marinelak itxasotik 
dakarren guztia.
Nahi legatza eta bisigua,
atun, sardinia, txipiroi,
mielgia, bokarta, aingiria.

Itxaso haundia, handiagorik,
harrapatzen banau azpian, bai,
nago galdurik!