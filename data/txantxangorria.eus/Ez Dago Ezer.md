---
id: tx-367
izenburua: Ez Dago Ezer
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PbnhWp-0r4A
---

Ez dago Ezer kantuaren bertsio sinfonikoa, Fernando Velazquezen moldaketekin eta Mundukide orkestrak lagunduta. Jose Luis Otamendiren hitzak musikatu ditu Eñaut Elorrietak. 
Arteman-ek grabatua, Mundukidek babestua.


ez dago ezer
konpondu edo galdu ez daitekeenik
ez dago inor
salbatu ala hil ez zaitzakeenik
ez dago inon
ez dago inon gure neurriko zerurik
gaur zu eta ni
elkar sortzera gaude kondenaturik
ez dago ezer
ezer konpon edo gal ez daitekeenik
ez dago inor