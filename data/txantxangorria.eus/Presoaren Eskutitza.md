---
id: tx-2075
izenburua: Presoaren Eskutitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xeJL0FQPww8
---

Gau on, ama bi hitz bakarrik
berri on bat zuri emateko;
bukatzen da gure gau luzea
laister nauzu anaien ondoan,
etxe sarrera berriro itzuliz
lengo kaletan borrokatzeko!

Esaiezu nere lagunei
ez noala esku hutsikan:
Maitatu dut gure aberria,
sufritu dut kate lazgarria,
baina hargatik etsipen beltzean,
noraezean ez naiz erori!

Ez nuke nahi ilunpe hontan
anai bakar bat lotuta utzi!
Eska zaiezu aberkide guziei
indar guziz oihu egin dezaten!
Denok batera atera gaitezen
Zulo hontatik libre izateko!