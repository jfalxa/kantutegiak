---
id: tx-2772
izenburua: Nere Maitia Jeiki Ta Jeiki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wd1ULb9jS-8
---

"nere maitia jeiki ta jeiki,
ez al-zerade loz ase (bis)
Zure ondotik gabiltzan
hauek ez baigirade loale,
Ideki zazu bortaño hori,
adiskidiak gerade.

Ez dizut bada nik idekiko
gau ilunian aterik (bis)
Barnean dagonak ez baidaki
kanpokoaren berririk.
Atoz eguna argitzen denian,
egongo gera elgarrekin.

Bihar goizian etorritzeko
urrun bizi naiz, maitia! (bis)
Xakurra saingaz hasitzen bada,
nork egingo du bakia?
Harrekikua nik egingo det,
arren, etorri zaitia.

Hona etorri dama gaztia,
erran zenidan bezela. (bis)
Ongi etorri, galant gaztia,
neronek nai nuen horla,
Hitz bi gustora egin ditzagun,
pasa zaitia salara."

Salara joan ta kutxan eseri,
begiratuaz elgarri. (bis)
"Inposible da ez zaituela
Ama Birjinak ekarri
Beti-betiko hartu nazazu
penaren kontsolagarri."