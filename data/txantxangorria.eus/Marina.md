---
id: tx-1862
izenburua: Marina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W4fW6MMVu6o
---

Marina, Marina, Marina
Bihotzez maite zaitut
Irrifarre eder horrekin
Zoratzen bainauzu zuk

Nik maite zintudan, gaur
Maitatzen zaitut
Ta ziur ere Marina
Bihar maiteko zaitut.

Nik maite zintudan, gaur
Maitatzen zaitut
Ta ziur ere Marina
Bihar maiteko zaitut.