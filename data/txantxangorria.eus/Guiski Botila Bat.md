---
id: tx-1689
izenburua: Guiski Botila Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MOdxJiicNls
---

Askotan dut Oroitzen  kapitainaren gamberan
Urrezko hogei txampon zituen zorroan gordeta.
Nere zizpa atera nuen ta buruan jarri nion, emaidazu
Poltsa orain edo emongo izut tiro.

Musha ring dum a doo dum a da
Whack fol de daddy o
Whack fol de daddy o
Guiski botilla bat.

Txalupa zahar batean itzuli nintzen etxera,
Nere maite ederrari zorroa erakustera.
Berak esan zindan ez zaitut inoiz salduko, 
Baina neska deabruaren menpe zen orduko.
...
Nere amets ilun guztiak oraintxe beteko dira,
Pentsatzen nuen nik ilargiari begira.
amarrukeriaz lapurtu zidan zorroa.
Kapitain haren emaztea zein emakume
Alproja. 
...
Galeoiaren gizonak prest daude kai aldean,
Nere judizioa hasi da eta ni ez naiz traidorea.
Berriro noa ihesi denak ditut atzean, 
Kobazulo batetan hartu nuen babesa.
...
Nere bila bazabiltza ez nago Euskal Herrian,
aurkituko nauzu Dublin izeneko Hirian.
beti Mozkorti ta maitaleen ehiztaria.
Harrapatzen ez banaute Kapitainaren piztiak.
...