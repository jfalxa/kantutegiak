---
id: tx-363
izenburua: Gona Gorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y_jhj5DLUuc
---

AMAK taldearen "Gona Gorria" abestiaren bideoklip ofiziala, taldearen lehenengo single-a (Gona Gorria, Baga Biga, 2021) Erne egon hemendik aurrera!

 (Gona Gorria, Baga Biga, 2021)

Hitzak/Letra/Lyrics

Udako ilunabarra
lausorik gabeko gaua
haizeak dantzaraziaz
soinean daukadan hura.

Udako ilunabarra
eta dantzarako grinaz
aske sentiarazten nauen
oihal zati arinaz.

Gona gorri, gorri-gorria
zazpi jostunek josia
gorri-gorri, gona gorria
sexu guztiek jantzia.

Inork iraindu gabe
jantzi nahi nuke
inor iraindu gabe
libre aske

Aske!

Gona gorri, gorri-gorria
aurre iritzirik gabia
gorri-gorri, gona gorria
sexu guztiek jantzia.

Jarraitu gaitzazu! / ¡Síguenos! / Follow us!

AMAK
Instagram: www.instagram.com/amaktaldea/
Facebook: www.facebook.com/AMAKtaldea/