---
id: tx-2808
izenburua: Geuk Erabaki Ged Ispaster
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fCGEUX3MDvw
---

Musika: Kana (Ileon) eta Oihana Etxabe.
Hasierako testua: Kirmen Uribe.
Musikaren letra eta ahotsa: Irkusne Garate.

GEUK ERABAKI!

Ispa- Ispaster, Ipi- Ipistar,

mila izen, herri bakarra;

neska, mutil, zahar eta gazte,

baserri zein kaletarra.

Nahiz eta izan gure artean

koloreak barra-barra,

pintzel batez margotuko dogu

alkarregaz ostadarra.

Hormak botaz ta trabak gaindituz,

iritzia emanez libreki,

ozen esanez etorkizuna

nahi dogula geuk erabaki.

Hormak botaz ta trabak gaindituz,

aurrera goaz poliki;

indarrak batuz geure geroa,

egingo dogu geuk erabaki!

Plazako haritz zaharraren adarrak

guztiz indarberriturik,

ikusita bere inguruan

herritarrak bat eginik.

Ogellako itsas korrontea

orain ez dago gelditzerik,

giza olatua ikusten da

Otoioko tontorretik.



Hormak botaz ta trabak gaindituz…

( 2 bider)