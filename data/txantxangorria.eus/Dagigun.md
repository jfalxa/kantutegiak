---
id: tx-2824
izenburua: Dagigun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ORGK-4vFPhY
---

KANTAREN FITXA TEKNIKOA
Ander Erzilla Kanta egilea Bartolome Ertzilla kontsebatorioko irakaslea
Gaizka Salazar (Gatibu), batería
Mikel Caballero , (Gatibu), bajista eta gitarra
Irati Ortubia Txelo Bartolome Ertzilla kontsebatorioko irakaslea
Matxalen Erzilla , txeloa
Helene Etxebarria, Txeloa
Jone Erzilla, tronpeta
Izaro Erzilla abeslaria
June Iturriaga abeslaria
Maider Zubizarreta abeslaria
Danel Aranburu abeslaria
Ihintza Zenitagoia abeslaria
Garoa Angiozar abeslaria
Lide Azueta abeslaria
Amaia Araiztegi abestaria
Alejandra Navarro abeslaria
Irati Milikua abeslaria
Magali Camarero abeslaria
Maialen Arteaga abeslaria
Erika Lagoma abeslaria
Igor Elortza abeslaria
Lorentzo Recordseko Aitor Ariño
Hitzak: Igor Elortza
Koreografia: Oiane Azkonizaga
BIDEOKLIPAREN FITXA TEKNIKOA
Iban Gonzalez, errealizadorea
Edorta Barruetabeña, Gidoilaria
Produkzioa: Idurre Cajaraville, Leire Bereinkua
Kamara operadorea: Julen de la Serna
Soinua: Ramon Larrabaster
Argitazpena: Ramon Larrabaster, Eneko Silva
Produktora: Baleuko
DAGIGUN!
batu, bota, bete
jaso, jo, jolas, jardun
nahas, arnas, marraz
kanta, konta dagigun
eman, omen, imin
lan egin, laga, lagun
asma, usna, susma
ikus, ikas dagigun
Mundua mundu bat
mundu da nahinun
gurea hemendik
hasi dagigun
Geroa giroan 
da gero egun!
gurea oraintxe
hasi dagigun
zenbat mezu bigun.... hainbat kexa, kexu ilun!...
ba ote dakiten, ba al darabilgun
dakargun, daramagun, dakigun, dezakegun, dagokigun
hasi, ekin, egin... dagigun!
Azartatu, hanka sartu ta su hartu… aproba ta albora
Eralda, erabat alda, heda ta hegalda 
Ahora ahoz gora… Ahoska harraboska… iraunean irun
har, erre, irri, orro, urra dagigun
mundua mundu bat bada nahinun
Gurea hemendik hasi dagigun
geroa giroan bada egun
Gurea oraintxe hasi dagigun
mundua mundu bat bada nahinun
Durangon euskaraz bizi dagigun
geroa giroan bada egun
Ikastolan gaurtik bizi dagigun