---
id: tx-2121
izenburua: Nafarroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PEM_Ny8dnp8
---

Bai Hotza eta ubela
heriozaren dolu gela.
Gurean dugu gaubela
gaua bezain egun goibela.
Ama kendu digute
jaiotzen ari ginela
gero diote gure
etxea ere ez dela.
Gurean dugu gaudela
gaua bezain egun goibela.

An intruding wind, how scheming
Came and took away our being,
We've no home and we've no mother
Our essence robbed, gone forever.
Sad the home with no mother
With no mother there to rule it,
Oh the pain, the shame's bolder
For this home if we don't own it.
We've no home and we've no mother
Our essence robbed, gone forever.

Gure kondairaren zama
haize arrotz batek darama.
Ez etxe eta ez ama!
Hustu dute gure izana.
Ai zer tristea etxe
ama kendu diotena!
Etxerik gabe ere
oi ama, zer itomena!
Ez etxe eta ez ama!
Husto dute gure izana.

Nafarroa, Nafarroa!
Gure etxeko abaroa
Arranoa, arranoa!
Amarik gabe nora hoa?
Aralartik Irati
haritza eta pagoa.
Bera ere bi zati
eta josirik ahoa.
Arranoa, Nafarroa!
Etxerik gabe nora hoa.