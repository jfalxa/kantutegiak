---
id: tx-2514
izenburua: Hotz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/guTGPQccdLs
---

Barakaldo Antzokian, 2017-5-14 · Teatro Barakaldo, 14-5-2017

Iratxe Mugire: musika eta hitzak, gitarra eta ahotsa · música y letra, voz y guitarra
Pedro de la Osa: konponketak, gitarra · arreglos, guitarra



Ez negarrik egin, 
ez dituzu nahikoak nire besarkadak? 
Neskatxa maite, nik 
ezin dizut nirea besterik eman 

Oso da zaila, neskatxa, 
sortzen dituzun suteengandik gordetzea 
Zailago litzaizuke zuri ordea 
sugar ama zuk duzula ohartzea 

Ez negarrik egin, 
nere kantuekin ere oraindik hotz zara? 
Neskatxa maite, nik 
ezin dizut nirea besterik eman