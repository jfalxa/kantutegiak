---
id: tx-2938
izenburua: Kikuri Bikurika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YrLsUxWYwZI
---

Harrapaketan, izkutaketan, kiriketan,
kikuri bikuri lakirikon,
zinko binko larikon,
tokatzen zaionari, hor konpon!
Kikuri-bikuri lakirikon,
zinko binko larikon.