---
id: tx-2913
izenburua: Nafarroa Berria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VnxNk4-PJys
---

Hizkuntzak ez al dira askeak? 
Ez eta hitzak libreak? 
Muga gabea ez al du behar 
gizakiaren bideak? 

Denon onura ez al du behar 
helburu aginpideak? 
Zuek lapurtu dizkiguzue 
ametsak eta eskubideak… 

Txanpon batzuen truke guztia 
jarri al daiteke salgai? 
inork agindu al diezaguke 
zer ez eta zer bai? 

Ez al gara gure erabakiak 
erabakitzeko gai? 
Gu zoriontsu izan gaitezen 
ez al duzue nahi? 

HAU DA HERRITARRON ORDUA 
ALTXA NAFARROA BERRIA!!! 

Lur zati bat margotu nahi dugu 
hitzentzat, euskararentzat 
Aitona-amona, guraso, ume 
gazte ameslarientzat. 

ikasle, etorkin, langile eta 
esperantza galduentzat 
lur anitz bat, lur goxo bat 
nahi duenarentzat




HAU DA HERRITARRON ORDUA 
HEMEN DA UDABERRIA 
INDAR EGIN DEZAGUN INDAR 
ALTXA NAFARROA BERRIA!!! 

Un cambio para crecer, un pueblo por decidir 
Un mundo para luchar, un lugar para convivir




Iritsi da garaia, kolpe bat emateko 
bidean aurrera, batera, herri bat eraikitzeko 
bidean aurrera, batera, irribarre egiteko 

HAU DA HERRITARRON ORDUA 
HEMEN DA UDABERRIA 
INDAR EGIND EZAGUN INDAR 
ALTXA NAFARROA BERRIA!!! 

KREDITOAK: 

Hitzak: 
Julio Soto

Musika: 
Fran Urias (Egilea - Autor)
Xabi Aldaz (Bateria)
Joseba San Sebastian (Baxua - Bajo)
Imanol Goikoetxea (Gitarrak - Guitarras)
Pello Reparaz (Haize konponketak eta tronboiak - Arreglos de vientos y trombón)
Ruben Anton (Tronpeta - Trompeta)

Ahotsak:
Estitxu Pinatxo
Jon Basaguren
Juantxo Skalari
Maider Ansa
Pello Reparaz
Petti
Sergio Iribarren "Kako"
Silvia Guillen
Zuriñe Hidalgo

Breaker: 
Txino Dantzari 

Grabaketak, nahasketak eta masterizazioa: 
Javi San Martin (SonidoXXI)

Zuzendaritza, grabaketak eta edizioa: 
Xabier Ayerra (Akari Films)