---
id: tx-3087
izenburua: Hilarria Fermin Valencia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DPmQfkKX8jI
---

German Rodriguezen oroimenez (Iruña 1.978)

Krabelin gorri bat zara
Gure bihotzean loratu zena

Txuri beltz argazkia,
Suz grabatua
Areazko ekaitza
Areazko hitza
Zigor gabe errudunak
Noiz justizia egingo da?

Klabelin girri bat zara
Gure bihotzean laratu zana

Maite zenuen hiria
Nahi du zure oritz arria
Baina batez ere maitea
Zure irria

8 de julio, Germán, libertad contra sus balas.
Querían los asesinos matar la aurora anunciada.
Mira tu gente, Germán, herida pero alentada;
el día de tu soñar vendrán a tocar las dianas
para plantar tu sonrisa en las calles liberadas.

Klabelin girri bat zara
Gure bihotzean laratu zana

Maite zenuen hiria
Nahi du zure oritz arria
Baina batez ere maitea
Zure irria