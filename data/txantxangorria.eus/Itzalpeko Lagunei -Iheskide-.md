---
id: tx-3262
izenburua: Itzalpeko Lagunei -Iheskide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wGXIV0oU1FI
---

Abestiaren letra:
Gau erdia, zeru izartsua
lo egin ezinik, leihotik begira
zutaz oroitzen naiz.
Hainbat une bizi izan ditugunak
ahaztezinak diren istorio eta mobidak
zutaz oroitzen naiz.

Harrotasunez betetzen nauen
zure irribarre gaiztua irudikatzean

Utzi gintuzun baina, sekula utzi gabe
bide bat hartu zenuen, errazena ez zen arren
zauden lekuan zaudela, hemen naukazula
badakizu laguna.

Gazte gaztetatik ulertu zenuen
zure lekua non zegoen.
Eta gaur, atzo bezala zutaz oroitzen naiz.

Itzalpean bidea egiten
zabiltzala guztia ematen
zuretaz oroitzen naiz

Harrotasunez betetzen nauen
begirada zorrotza irudikatzean

Utzi gintuzun baina, sekula utzi gabe
bide bat hartu zenuen, errazena ez zen arren
zauden lekuan zaudela, hemen naukazula
badakizu laguna.

Utzi gintuzun baina, sekula utzi gabe
bide bat hartu zenuen, errazena ez zen arren
zauden lekuan zaudela, hemen naukazula
badakizu laguna.