---
id: tx-2524
izenburua: Aske
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bujpJOhmUYs
---

Gaztetasuna lapurtzen digute asteak bikoiztuta kontatzen egunak ziegako lehiotik mendien irribarrea hain hurbil ta urrun sentitzean. Pentsa ezinezkoa zen errealitatea gure azalean Nola egin honi aurre, ez bada elkartasunez? Bidea oraindik egiteke jakitun bagara ere Itxaropena, itxaropena, ez dugu inoiz galtzen! Gaztetasuna lapurtzen digute asteak bikoiztuta kontatzen egunak ziegako lehiotik mendien irribarrea hain hurbil ta urrun sentitzean. Orain amets egitea,gure bidelagunetan pentsatzea, kendu ezin diguten ilusioa da. Askatasunaren haize goxoa, itxaropena galdu ezin aurrera ba, aurrera ba, zaudete ta gurekin! Gaztetasuna lapurtzen digute asteak bikoiztuta kontatzen egunak ziegako lehiotik mendien irribarrea hain hurbil ta urrun sentitzean. Orain amets egitea, gure bidelagunetan pentsatzea, kendu ezin diguten ilusioa da.