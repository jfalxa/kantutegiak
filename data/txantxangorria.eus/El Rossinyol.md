---
id: tx-1628
izenburua: El Rossinyol
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5U0ZuAjFUUE
---

Rossinyol, que vas a França,
rossinyol,
encomana’m a la mare,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.

Encomana’m a la mare,
rossinyol,
i a mon pare no pas gaire,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.

Perquè m’ha mal maridada,
rossinyol,
a un pastor me n’ha dada,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.

Que em fa guardar la ramada,
rossinyol,
he perduda l’esquellada,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.

Jo t’he de donar per paga,
rossinyol,
un petó i una abraçada,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.

Rossinyol, que vas a França,
rossinyol,
encomana’m a la mare,
rossinyol,
d’un bell boscatge
rossinyol d’un vol.