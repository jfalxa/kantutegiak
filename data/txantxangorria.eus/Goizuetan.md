---
id: tx-635
izenburua: Goizuetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DUxLphqnCwc
---

Goizuetan bada gizon bat
deitzen zaio "Trabuko";
itzak ederrak, biotza paltso,
sekula etzaio paltako:
egin dituan dilijentziak
berari zaizko damuko.

Ongi ongi oroitu adi
zer egin uen Elaman;
difuntu horrek izatu balu
iarraikilerik Lexakan
orain baino len egongo intzan
ni orain nagoen atakan.

Nere andreak ekarri zuen
Aranaztikan dotea;
obe zukean ikusi ezpalu
Bedabioko atea;
orain etzuen idukiko
dadukan pesadunbrea.

Nere buruaz ez naiz oroiten
zeren ez naizen bakarra
aztizekoak or uzten ditut
bi seme ta iru alaba
Jaun zerukoak adi dezala