---
id: tx-2183
izenburua: Kanuto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zm4LAWS2xZE
---

Oskorri. Kanta hau Ondarrun jaso zen, "Katuen testamentua" ume diskarentzako materiala biltzen zen garaian. 

Texto orijinala laburra da, bainan badu esan nahi berezi bat ume txikientzako, "ezetz" erantzuten zaiolako amari. "Zilindro" hitzarekin, antza, kaleak egiteko makina-zapaltzaileari deitzen omen zitzaion, katxarro handia eta erakargarria umeentzako. 

Doinua bizia eta alegrea da. Anton Latxak harmonizatu eta moldatu zuen gero kanta osoa, hemen aurkezten den moduan.