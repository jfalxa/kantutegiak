---
id: tx-2578
izenburua: Zure Oroiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y2RcAArYgs8
---

Urretxuko seme ospetsu lerdena,
Euskal Herrietako txindor xamurrena,
goratu nahirik gatoz zure izena,
badakigu irri parrez begira zaudena.

Zauden tokitik, hartu gaur ere gitarra
ia piztutzen dezun gure ele zaharra...
Gitarraren soinua dantzut xarra-xarra
Jose Marin begitan dagusat negarra.