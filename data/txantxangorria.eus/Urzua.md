---
id: tx-162
izenburua: Urzua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sUEAorrsFsg
---

Larrazkenin ürzoa badoa goratik, oihanian papoa janhariez beterik, Ihizlarian tioa kantüan entzünik, han zen hat bortü lepoa ordün dü iganik. 
Agur, agur xoxoa et’eliza txoria, zirekin oihanttuak dirade alagera, Haitxa eta bagoa, zühain ederrenak, berde beti ostoa ziren kantoer beha. 
Txori ttipia hura, jun leite harekin, nahiz denbo gaixtoa igan ekitolzin, Ezin hat itsasoa igan indar mentsin, pena handin gaixoa batürik da oihanin. 
Abil abil xoxoa, hi ere txoria, oihanean sasuan gustin igaitera, 
Ez ingana hakoan pettarin bestela, segür dükek tioa ürzoak bezala! 
Hitzak: Pierra ETCHEGOYHEN 
Müsika: Jean ETCHART