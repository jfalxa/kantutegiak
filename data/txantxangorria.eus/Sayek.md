---
id: tx-146
izenburua: Sayek
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m3mSYYuP19k
---

Bideoa: @lurrafilms
Ekoizpena: @mon.dvy 

Oholtza gañian oihuka
dena pasa delako azkar
Milaka momentu ye,
sayek hemendik ezetz
korrika menditik gora
ezerren bila juten
sayek nun gauden
(sayek nun gauden, nun gauden)
(x2)
Beldurrez josiya,
zinelako atzo
ta oain, hobeto zaudelako
Lurrea, begirada, arima hutsa,
zerua grixa, ostikoka, gora begira
hau dena jasan berrin, berrin
Lurrea begirada
lurrea begirada
Milaka momentu ye,
sayek hemendik ezetz
korrika menditik gora
ezerren bila juten
sayek nun gauden
(sayek nun gauden, nun gauden)
(x2)
Zenbat besarkada
ta zenbat malko
ta oain bira nun gauden
Lurrea, begirada, arima hutsa,
zerua grixa, ostikoka, gora begira
hau dena jasan berrin, berrin
Lurrea begirada
lurrea begirada
Milaka momentu ye,
sayek hemendik ezetz
korrika menditik gora
ezerren bila juten
sayek nun gauden
(sayek nun gauden, nun gauden)
(x2)