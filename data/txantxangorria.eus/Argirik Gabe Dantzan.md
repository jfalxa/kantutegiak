---
id: tx-110
izenburua: Argirik Gabe Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dj6rcqXqooQ
---

ALBUM: Atomika (2022)

Jarraitu gaitzaizu / Síguenos!
WEB: www.kokein.com

Nahasketa / Mix: Eñaut Gaztañaga (Gaztain Estudioak)

Hitzak / Lyrics:

Heldu da unea gurekin batera dena ahazteko, orain.

Heldu da garaia, astindu barrenak, dantza egin dezagun batera
amildu arte!

Dantza egitean, erabat askatzean,
deabruaren kontrolpean, sua barrenean,
kimika hutsean dago gure helburua
gau ahaztezin honetan.

Izerdiz bustiak, erabat galduak, doinuaren senean eroak.

Azala biluztu, zirraraz lehertu, kosmosaren baitan amore eman
amildu arte!

Dantza egitean, erabat askatzean,
deabruaren kontrolpean, sua barrenean,
kimika hutsean dago gure helburua
gau ahaztezin honetan.

Dantza dezagun amildu arte,                          
elkarri helduta bat egin arte.   
               
Dantza dezagun! Utzi gorputzak elkarri helduta
bat izan arte.

Argirik gabe dantzan sutan jarriz gaua.