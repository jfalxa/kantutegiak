---
id: tx-192
izenburua: Heriua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L90vfy71n2Q
---

Seme alaba maiteak, orai jin zerait mezia, 
Partitü behar düdala mündü huntarik besteala, 
Osagarriak ez dereit denbora batez faltatü, 
Bena hura ere düzü nahi nitzaz mendekatü. 
Bizitzea gaiza bat da bena behar da pentsatü, 
Jin delarik tenoria behar dügüla partitü, 
Mündü huntarik bestiala, hori da ürhats handia! 
Mündü huntako ixtoria zorte bat da denentako, 
Irus bai edo malerus, gero denak negatzeko, 
Uhure hanitx ükenik, denetzaz errespetatürik, 
Horik deüsere ez dira heriua heltü delarik. 

Hitzak: ETXAHUN Iruri Müsika : Jean ETCHART