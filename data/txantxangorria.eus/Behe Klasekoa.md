---
id: tx-99
izenburua: Behe Klasekoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/P2eUf_-QKXA
---

Musika eta hitzak: Olaia Inziarte
Musikariak: Josu Erviti (bateria), Ander Unzaga (teklatuak) eta Eric Deza (kitarrak). 

Zuzendaria @an_txoa
Argazki zuzendaria @asier03 
Ekoizpen burua @ellenvald 
Makillajea eta ile-apainketa @lihoa.art 
Kameralaria @urro73 
Edizioa @asiertoledo98 
Kolorea @mrpix3l

Erviti Studion grabatua eta ekoitzia.

Ez dut inoiz ukatuko zer den honaino ekarri nauena.
C           Am              G                    F
Ez dut inoiz atzenduko nor zen, aingerutzat ninduena.
C           Am            G               F
Ostatu zahar baten xokoetan denetatik ikusten hazia.
C        Am               G                     F
Aukeratu ahal izango banu xuria izanen zen nere zaldia.
 
[Chorus]
Dm           G         C             Am     Am7
Behe klasekoa izate aren konplejurik bat ere ez,
Dm           G         C       G      C
erdara traketsetan mintzatzearena ere ez.
 
[Verse]
C        Am           G                F
Ondokoak mires eta ni ez, kondena amaigabea.
C        Am          G                 F
Gorrotoa kudeatuko dut maitasunak izan dezan berak baino leku hobea.
C         Am             G                 F
Amak ez du ezagutzen nere kuadernoaren barren aldea.
C        Am            G                  F
Beretzako hobe bada hola, egin nahiago nuke.
 
[Chorus]
Dm           G         C             Am     Am7
Behe klasekoa izate aren konplejurik bat ere ez,
Dm           G         C             Am
erdara traketsetan mintzatzearena ere ez.
Dm        G        C              Am            Dm       G              C
Galtzerdi zulatuek istorio hobeak dituzte baino entzule onez exkax gaude.
 
[Bridge]
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako, oh oh oh
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako, oh oh oh
 
[Chorus]
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako, oh oh oh
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako, oh oh oh
 
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako, oh oh oh
F                C               G
Eta kontuz ibili gainontzekoen min horrekin, min horrekin
F                    C            G
mina baino lehenago bertzeena delako