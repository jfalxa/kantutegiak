---
id: tx-1274
izenburua: Baga, Biga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ks_0o0ucVzs
---

Euskal Herriko kantu famatu zombait hartzen eta bereganatzen duten bikotea da ZORTZI. Bixente eta Xantik, belanauldi guztiei haien musika zabaltzeko asmoarekin sortu dute taldea. Zubi berri baten gainean doazte kantuz eta alai, portu batetik bestera.


Baga, biga, higa,
laga, boga, sega,
Zai, zoi, bele,
harma, tiro, pun!
Xirristi-mirristi.
gerrena plat,
Olio zopa,
Kikili salda,
Urrup edan edo klik ...
ikimilikiliklik ...