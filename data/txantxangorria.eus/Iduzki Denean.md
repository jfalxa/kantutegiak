---
id: tx-2864
izenburua: Iduzki Denean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qNIuxZ4BABs
---

Iduzki denean, zoin den eder itzala!
Maitia, mintzo zira, plazer duzun bezala:
egiten duzula mila disimula, inorant zirela;
erraiten duzula bertzetaik haizela,
faltsuki mintzo zira.

Zazpi urtez beti jarraiki zait ondotik,
erraiten zautala, ez dut bertze maiterik;
hitzer fidaturik nago tronpaturik,
gaixoa tristerik; ez dut eiten lorik,
ez jan edan onik, maitia zuregatik.

Oi! ene maitia, nik ere zuregatik,
pasatzen dizut, nik zonbeit sabeleko min,
ezkonduz zurekin, samur ait'amekin
behar nindeite jin, ez dakit zer egin,
ez eta nola jin, nik neure etxekoekin.

Oi! ene maitia, nun duzu izpirituia,
fediaren jabe aita deraukazuia?
Atxikazu fedia, maita bertutia,
libra kontzientzia, munduko bizia
labur da, maitia, ez zaitela kanbia!