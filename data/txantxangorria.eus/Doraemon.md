---
id: tx-1258
izenburua: Doraemon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hstozYGCERk
---

Goiz goizetan egunon eta gauetan gabon, ilargiko leihoan nobita eta doraemon, zein pozik bizi diren amets batetik bestera bere lagun eta adiskidekin batera, katu kosmikoa magoa ere bada, BAAAI! EGIA DA HORIIII! Ron pon pon, gora ta gora beti Dorae-moooooooon, ron pon pon, gora ta gora beti DORAE-MOO