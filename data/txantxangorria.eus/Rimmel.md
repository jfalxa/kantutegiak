---
id: tx-812
izenburua: Rimmel
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6ANzLS06Ano
---

Ipuin puta guzti hoietako printsesa arrosak

zeharo nazkatzen nauten maitasun kantuak

isildu behingoz eta sar ezazu mingain hori txuloan

ez baldin banauzu aspertu nahi, eman musu ahoan.



Esaidazu ez jarraitzeko arrazoi bakar bat

baditut ez geratzeko nik beste hamar.

Zaude lasai ez zaitez saiatu ulertzen

egon zaitez lasai

ez dut zugandik ezer nahi.



Ardo beltz hori eran dezagun, zatoz hona

goitik behera erantzi nazazu eta goazen ohera

ei txikito, zaude lasai, zer uste zenuen ba?

zutaz eroki maitemindu egingo nintzela?



Zu pozik zaude euria ari badu. Ni pozik nago euririk ez bada.

Zu pozik zaude euria ari badu. Ni pozik nago zu pozik ez zaudenean.