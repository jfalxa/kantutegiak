---
id: tx-2383
izenburua: Kopla Berriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9UNK4Ul3HLk
---

Eskutxoek jo dute
kriskitin-kraskitin; (Bis)
Haizea kiribiltzen
dabil Mari Zikin;
ala, morena!,
dabil Mari Zikin…
Eskutxoek jo dute
kriskitin-kraskitin.

Behatz txikiak estu,
txakurrak: ai ene! (Bis)
Ezinean da Pintto,
ez kanpo, ez barne,
ai!, oi!, ei!,
ez kanpo, ez barne…
Behatz txikiak estu,
txakurrak: ai ene!

Behatz nagiarentzat
tori eraztuna; (Bis)
donua tinkatzeko
urrezko ttun-ttuna
ala, morean!,
urrezko ttun-ttuna…
Behatz nagiarentzat
tori eraztuna.

Behatz zuzena dugu
begiaren lagun; (Bis)
itsasgaina oin-hutsik
igaro dezagun
ala, morena!,
igaro dezagun…
Behatz zuzena dugu
begiaren lagun.

Eskuak eskuekin
txalopin-txaloka; (Bis)
zeruan hasi dira
izarrak saltoka,
ala, morena!,
izarrak saltoka…
Eskuak eskuekin
txalopin-txaloka.