---
id: tx-692
izenburua: Maitiak Galde Egin Zautan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/miAgLgToCek
---

Maitiak Galdegin Zautan pollit nintzanez (berriz)
Pollit, pollit nintzela bainan larrua beltz, larrua beltz.
Maitiak Galdegin Zautan premu nintzanez (berriz)
Premu, premu nintzela bainan etxerik ez, etxerik ez!
Maitiak Galdegin Zautan moltsa banuenez (berriz)
Moltsa, moltsa banuela bainan dirurik ez, dirurik ez!
Maitiak Galdegin Zautan lanean nakienez (berriz)
Lanean, lanean, nakiela bainan gogorik ez, gogorik ez.
Gaixoa hil behar dugu guk biek gosez (berriz)
Gosez, gosez ez naski bainan, elgar maitez, elgar maitez!