---
id: tx-1055
izenburua: Heldu Besotik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HW0lwGlUXdo
---

Notizi txarrak, albisteak BERRIRO!
Epailea mailua astinduz.
Iraganean iltzatu nahi gaituzte BERRIRO!
Zigortu dute etorkizuna
Agintari faxistaz osaturiko kuadroan
margotua euren legedia
zuen paretik kendu nahi izan gaituzue BAINA!
Ez da makurtuko gure herria
Heldu gogor besotik, kateatu gaitezen
ez diegu utziko inor gehiago eramaten
duintasunez eutsi herri harresiari
inor gehiago e(ra)man ez dezaten
Azken ostikadak, azken aldarriak
azken borroka eguna garaipena gertu da
utzi guztia herriak borrokan behar zaituzte
haur, zahar ta gazte ekintzara zoazte
Heldu gogor besotik, kateatu gaitezen