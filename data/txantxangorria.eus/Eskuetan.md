---
id: tx-1550
izenburua: Eskuetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rQxiqF0Ti8A
---

Konposizioak, moldaerak zein programaketa: XABIER ZABALA

Eskuetan, bizitza eskuetan
eskuetan daude gure hitzak
Eskuetan, bihotza eskuetan
gure esku dago mundu bizi bat.

Esan nahi nizukeen dena,
esango dizudana
desiorik sakonena
ala inoiz ez esana.

Sekreturik isilena
ala berririk onena
desiorik sakonena,
gure esku dago dena, den dena.

Eskuetan, bizitza eskuetan
eskuetan daude gure hitzak.

Eskuetan bihotza eskuetan
gure esku dago mundu berri bat.

Nik zuri kaixo esatea
zuk niri zer moduz lagun
zu zara nire maitea
denok ikasi deazgun.

Esan nahi nizukeen dena,
esango dizudana
desiorik sakonena
ala inoiz ez esana... esana...

Eskuetan, hizkuntza eskuetan
eskuetan daude hitz guztiak
eskuetan, bihotza eskuetan
gure esku dago mundu bizi bat!

Eskuetan, bizitza eskuetan
eskuetan daude gure hitzak.

Eskuetan bihotza eskuetan
gure esku dago mundu bat,,, eskuetan