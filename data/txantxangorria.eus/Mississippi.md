---
id: tx-2426
izenburua: Mississippi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ws8RdV0mlXo
---

Mississippi dut gogoan
Mississippi  urruna
urruneko oinaze oihu
zahar batek ekarrita.
Bidegurutzeko kanta
batek hitz mindu gordinetan
erreguz bakardadetik
salba nazazu Jauna.

Bella Mae etxe aurrean
harensoineko arina haizeak
dantzan darabil goxo nire
oroimenean.
Nire maitea zen baina
zorigaiztoko goiz batean
beso artetik kendu zidaten
eta urrutira eraman.

Mississippi  dut gogoan
esklabo itsasontziak
Afrikako kostaldetik
itsasoz
bestaldera
Mississippi gogoan.

Ibai ertzean oinutsik nago
ilunabar hotz honetan
ur lohitsuak gero eta
gorago hazten Jauna.
Gerriraino murgildu eta gero
ur zikin lohitsuetan
ozenago entzuten dut
odolaren oihua.