---
id: tx-3061
izenburua: Olentzero Begi Gorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Oa-38RNoRsM
---

Olentrero begi gorri
nun arrapatu dek arrain hori?
zurriolako harkatzetan
bart arratseko hamaiketan

Olentzero buru handia
entendimentuz jantzia
bart arratsean edan omen du
hamar arruko zahagia
ai urde tripa handia
tralaralara, tralaralara...