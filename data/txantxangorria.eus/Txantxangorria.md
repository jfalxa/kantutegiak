---
id: tx-3399
izenburua: Txantxangorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6XYYTG7p7w8
---

1976an Gabi de la Maza, Txatxangorri eta Jabi Etxeandia, Warren, abeslari bikotea osatu zuten. Izan zuten abesti arrakastatsuena Fermin Valencia nafarraren egin zuten bertsioa izan zen. Kanta honi Xabier Amurizaren bertso batzuk ezarri zioten.