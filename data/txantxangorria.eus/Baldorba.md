---
id: tx-2147
izenburua: Baldorba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0Z2FmAzArl4
---

1996an "Hitaz oroit" diskan Benito Lertxundik argitaratu zuen abestia. Erribera kantaren ildotik, Nafarroaren beste eskualdeari kantatzen dio. Zalantzarik gabe INKUNABLEA

Ezkil gabeko elizen
Eremu emankorra,
Giza murzuriko
Zure ardientzat.
Oi Baldorba!
Eguzki eta haize idor;
Mahats-ardo ibai,
Galbumarena.
Lur gorri, harri landu,
Mailu eta xixelaren kantu.
Gitarrazko alakiketan
Armeniako kanun ahots urratuan.
Oi Baldorba!
Esazu nor zen bidetik
Baztertu zintuen madarikatua!
Zure irriño erromanikoa
Erdi izoztu eta leloturik
Utzi zuen madarikatuaren
Izena nahi dut.
Oi Baldorba!
Ezazu nor zen bidetik…