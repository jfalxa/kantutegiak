---
id: tx-2786
izenburua: Igoruako Boluan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gep3ZjYMN8s
---

Iguriako boluan ehundaka sorgin gauean,
Iguriako boluan dantza-dantzari kantetan.

Iguriako boluan,
hamahiru sorgin gauean,
Iguriako boluan
dantza-dantzari kantetan.
hareik bai gorputz arinak,
aurpegi argi, oin bizkor,
klin klon klin ta klon soinuaz
"ekiok hik bolu gogor".

Iguriako boluan ehundaka sorgin gauean...

Aldameneko lerdoian
mozolo zaharren kurrinkak;
goiko etxeko egalan
ontzearen erantzunak.
Argi diztirak aidean
hegaka suzko anderak,
Anbototik beherantz, gorantz...
errota ondoko dantzak.

Iguriako boluan ehundaka sorgin gauean...

Anbotoko andereak
batzarren buru nagusi
bolu ondoko zelaian
sorgin ostean nabari.
Sorgin gaua don itxuraz,
ilargi bete, oskarbi,
izarrak diz-diz, haizea
lotan... Hau gau zoragarri!

Iguriako boluan ehundaka sorgin gauean...