---
id: tx-3143
izenburua: Astindu -Esne Beltza-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r-xGNxhI4yc
---

Ibilian ibilian iparra galtzen bada
mendebal edo ekian besarkatuko gara
zorionik ez omen da zeruetan gordea
askatasunak bizitza ematen du ordea.

Astindu hegoak
eta goazen airean zeru zabala zain.
Astindu hegoak
ipar eki mendebalde eta hodeien gain.
Astindu hegoak
begi eta bihotzak ez daukate lain.

Askatasunez bizitzea
lor dezakegu aritzean
zeru zabaletan aske sentitzean.

Ibilian ibilian iparra galtzen bada
mendebal edo ekian besarkatuko gara
zorionik ez omen da zeruetan gordea
askatasunak bizitza ematen du ordea.

Astindu hegoak
eta goazen airean zeru zabala zain.
Astindu hegoak
ipar eki mendebalde eta hodeien gain.

Zeru zabaletan sentitzea
lor dezakegu aritzean
denon sentipenak hegan hegan.

Ez onartu, ez lotu
bizitzak bere bidea jarraitzen du.
Ez onartu ez lotu
askatasunak bizitza ematen du.