---
id: tx-1141
izenburua: Kli, Kla Telebista
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wy-nGwkbAkA
---

Kli kla, kli kla, 
piztu telebista, 
kla kli, kla kli, 
telebista itzali, 
kli kla, kla kli, 
nahi ez baina, 
bera gabe ezin bizi. 
1 
Primerako laguna, 
gogaikarri astuna, 
dibertigarriena, 
nazkatzen gaituena, 
amodiozko lore, 
tiro eta dolore, 
etxeko eta turista 
zara zu telebista. 
2 
Munduko hainbat kide 
ezagutzeko bide, 
herrialde urrunak, 
ohitura ezezagunak, 
zenbat gerra, ordea! 
zauri, min ta gosea! 
Bada bortxakeria 
eta zabarkeria. 
3 
Telea ikusteko, 
baina ez tontotzeko, 
egin ezazu aukera: 
ez ikusi sobera! 
Lagunekin ibili, 
maiz samar irakurri, 
programa onak hautatu 
ta ez hipnotizatu. 
4 
Sarri ikusiz gero, 
bihur zaitezke ero. 
Litxarkeriak janez, 
gizenduko zara errez. 
Zapinga kontrolatu, 
ez ba txorabiatu, 
eta programa hasterako, 
altza txisa egiteko.