---
id: tx-2580
izenburua: Buruan Jantzi Dut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s9qcKPO8qBE
---

Buruan jantzi dut txapela
sorbaldan jarri zait usoa
besoetan lolo panpina
eskuetan hatzamarrak
eskuetan hatzamarrak
 zer joko dut orain?
kitarra, kitarra