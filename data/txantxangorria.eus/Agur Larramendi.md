---
id: tx-2915
izenburua: Agur Larramendi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HOEEHtyQqjY
---

Agur Larramendi, agur gizabizion jakintsu.
Kemen ta adorez goraldu zendun
ugaldu diren hainbat kimu.
Agurtzen dugun izartegiak gorde zaitzala glorian.
Erne da erein zendun hazia,
erne, larran ta mendian.

Haranez haran dabil haizetan gizakion hasperena.
Bi mende-hauetan sortua duzun urkoen miresmena.
Zure desirak hegan doaz mendirik ibar zabaltzen.
Kriseilu zinen gaua ilunetan,
ilargiak orain digu argitzen.

Lur idorrean uzta zinen, egunaren goizabarra,
iturburutik goldez ta moldez sortutako arraba.
Lur honen garra bezain indartsu loratu eta ondua
ur-jarioaren bultzadaz jatorku mendi goietan behera.

Agur Larramendi, agur t'erdi,
lurraren zainetan zara errotu, lurrak eman du,
lurrean landuz zeruak zaitu besarkatu.
Muga-gabeko sentipen berriz apaindu zenduen orubea,
lehena, oraina, geroa uztartuz lur landetan hedatua.