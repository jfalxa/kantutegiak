---
id: tx-452
izenburua: Udazquen Gau Bateco Coplac
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ARFlBKmRnxQ
---

Udazquen ilunabarra
Ixila eta bacarra...
Haice epelac escutan dacar
Maitasunaren dardarra.

Itsas ertzeco harean
Maitale biac parean...
Ezpaiñ gorriac trabatu dira
Maitasunaren sarean.

Ahoac larre guritan
Yolas moeta guzitan...
Amore billa jira biraca
Dabiltza apar zuritan

Mingaiñac ibai urtsuan
Eztizco labe sutsuan...
Barne tristura itto nahi dute
Sentimenduen putzuan

Gorputzen larru aratza
Maite laztanen baratza...
Behatz fiñduac haria dira,
Bular zuriac ardatza

Ceruan laiño gorrantza
Desira miñen frogantza...
Izter zuritan lehunqui doaz
Escu beroac gorantza

Gau farolaren focoan
Escu laztanac jocoan...
Behatz lucea irristatzen da
Atseguiñaren zocoan

Gorputz zuriac lardascan
Hondar gorritan arrastan...
Gogo lizunac escuz garbitzen
Gozamenaren harrascan

Odol iraquiñ gordeac
Bihotz taupada dorpeac...
Maitasunaren sabel gorrian
Haraguiaren colpeac

Arrats gorrico enigmac
Izar zurien esgrimac...
Olatu hotsac marcatzen dittu
Placer arnasen errimac.

Ur gueldiaren dirdira
Gorputzac baretu dira...
Maiteminduac lilluraz daude
Biac elgarri beguira.

Ceruco cristal altuac...
Poz urdiñaren arcuac
Arrats lucean cantari doaz
Elcar escutic hartuac:

Musica amaigabean
Nota eztien clabean...
Amodioac iraun dezala
Oroimenaren labean.