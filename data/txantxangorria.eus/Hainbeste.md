---
id: tx-778
izenburua: Hainbeste
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Djs5LX1D2Ac
---

Gorde dute zerua, epel lainoek
Agur keinua, zela zioen
Ordurik gabeko egunen, izerdiz
Gatzez ordain dirudite, gaur zauriek
Gauak jantzi dira, soinekoz
Mundua biluzik, ezagutzeko
Arnasa elkarbanatzen izarretan
Burua zutik mantentzeko egun luzetan
Maite duenak, maite du maitatzen
Sei zati ditu ilargiak aurten
Maite dudana, maite dut hainbeste
Jasotzen dudalako ematen dudan beste, hainbeste
Lau hormen artean, paradixua
Gauetako amaren azken muxua
Ez nazazu iratza mesedez
Erausi erlojuak, erraiak lez
Maite duenak, maite du maitatzen
Sei zati ditu ilargiak aurten
Maite dudana, maite dut hainbeste
Jasotzen dudalako ematen dudan beste
Hainbeste
Nahibeste
Hainbeste
Nahibeste
Hainbeste
Nahibeste
Hainbeste
Nahibeste
Hainbeste
Hainbeste…