---
id: tx-2029
izenburua: Ez Bedi Galdu Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GJxQfpkMMC0
---

Gure euskera eder maitea
galtzen gaur degu ikusten,
euskaldun onak arritzeko da
nola ez diran lotsatzen!
Larramendirik, Astarloarik
ez da geiago aipatzen...
Ez da utsegin aundiagorik
ondo badegu pentsatzen.
 
Galdu dirade oitura onak,
galdua dugu Euskera...
ola bagaude, eun une barru
galdu da gure izena!
Erro, Aizkibel ziran bezela
erakusterik gaur ez da.
Gure euskera ...ai! galtzen bada
gu... euskaldunak ez gera!
 
Nola isildu eta barkatu?
Etorkizunen Kondairak
arrazoiakin esango digu:
nun dira zuen oiturak?
Damu dute bai... gaur euskaldunak
lotsa pixka bat dutenak,
aspaldi esan duten bezela
gizonik jakintsuenak.
 
Dala Parisen eta Londresen
jakintsu asko badira;
euskerazko liburu zarren
ondoren ibiltzen dira...
Or da Luziano, printze ernaia,
orain guretar deguna,
euskerazale bikain-bikaina,
jakintsuetan aundi dena.
 
Arren, ez bada galdu euskera,
nere anaia maiteak!
Galtzen badegu... galduak gera
gu eta gure semeak.
Beti euskeraz itz egin, bada,
oso zaar eta gazteak
esan ez dedin denok gerala
euskaldun biotz gabeak.