---
id: tx-925
izenburua: Ebamirentxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LsCJ4fT4S8o
---

Ebamirentxu joan zen eguzki bila hondartzara
marradun bikinia eta maleta hartuta
bera joan zen benidor aldera ni bakarrik utzita
inolako erruki gabe
ebamirentxu joan zen

Gauean ametsetan lorik egin ezinean
ta bere argazkiak gelako mezan atzean
zein polita den uretan igeri hondartzan egonean
maitasunik gabe triste
naizen bitartera

Zer egingo det, zer egingo det, zer egingo detEebamirentxu gabe

Zer egingo det, zer egingo det, zer egingo det Ebamirentxu gabe

Ezin bizirik nabil maitatzen nauen galdezka
nire beharra duen bihotza duen dardarka
bera joan zen benidor aldera
ni bakarrik utzita
inolako erruki gabe
ebamirentxu joan zen

Zer egingo det, zer egingo det, zer egingo det Ebamirentxu gabe

Zer egingo det, zer egingo det, zer egingo det Ebamirentxu gabe

Ebamirentxu joan zen eguzki bila hondartzara
marradun bikinia eta maleta hartuta

Ebamirentxu joan zen eguzki bila hondartzara
marradun bikinia eta maleta hartuta