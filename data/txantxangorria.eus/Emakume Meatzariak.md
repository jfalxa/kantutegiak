---
id: tx-2351
izenburua: Emakume Meatzariak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jb7CmYPLx7M
---

Zulatutako lurrean

Ahaztu burdin gogorrean

Emazte meatzariak

Irentsi ditu historiak


Zelan lortu du Bizkaiak daukana?

Beti bezala ilunpeko lana 


Mesprezuen abagune

Ez langile ez emakume

Eskuko babek jakina 

Ez dute uste berdina


Azalduko du noizbaiten

Baleki harriak hitz egiten


Soldatadun lan luzeak

Zain gaitu gero etxeak

Zaindu beharreko ume

Ta dirudunen neskame


Gure arteko kontuak ta kantak

Isiltzen ditu makinen zaratak


Kautxozko zinta parean 

Badatoz etengabean

Harritzar burdinezkoak

Sailkatu beharrezkoak


Ingeles higuingarri

Ibarra edo Chavarri

Porsmouth Belgika , zer guri?

Areeta Getxo Neguri.


Haien estatuak eginda zerez?

Guk ateratako burdin harriez


Lurrundu den izerdiak

Busti ditu memoriak

Harri arteko uretan 

Disolbatuz urteetan


Azalduko du noizbaiten

Baleki harriak hitz egiten

........



Memoriaren putzuan 

Begi ninien itsuan(bis)

Irteteko sartu gara ,hara,

Nahiko iluna da,gaba

Ilargirik ez bada

Irteteko sartu gara ,hara,

Nahiko iluna da,gaba,

Kriseilu argitara.


Zure eskuek amama

Eusten dute iragana(bis)

Garai zaharrez galdetzean,kean

Gure jolasean,behean

Jakiteko gosean

Garai zaharrez galdetzean,kean

Gure jolasean,behean

Zure begi ertzean.


Lehengo lepotik burua

Gaur baina lehengo mundua(bis)

Emakume,eman ume,xume

Zaintza lanen berme eme

Amantala uniforme

Emakume,eman ume,xume

Zaintza lanen berme eme

Zaraten ilsilune.


Eskuz eskuko testigu

Bilobei dagokigu(bis)

Irauli dezagun behetik,goitik,

Ez baita katerik  hortik

Apurtu ezin denik

Irauli dezagun behetik,goitik,

Ez baita katerik  hortik

Haien gatik gu gatik.