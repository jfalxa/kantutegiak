---
id: tx-3221
izenburua: Kaixo Barraskiloa -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KZJ_r6oVTho
---

Kaixo barraskiloa
kaixo lagun maitea
Zer egiten duzu orain
antenak aterata
Eguzkia maite al duzu
noski denok bezalaxe
Errekarantz zoaz bai
Bai motxila hartuta

Kaixo barraskiloa
kaixo lagun maitea
Zer egiten duzu orain
antenak aterata
Eguzkia maite al duzu
noski denok bezalaxe
Errekarantz zoaz bai
Bai motxila hartuta

Hodeiak zeuden zeruan
narratsa zen eguna
Gero eguzkia irten zaigu bero
mendi eta basoan

A! Begira nor dagoen
harri koskor gainean
Zulotik irteten ikusi dut eta
agurtzera banoa

Kaixo barraskiloa...