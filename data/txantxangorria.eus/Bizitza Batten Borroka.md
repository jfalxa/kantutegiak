---
id: tx-39
izenburua: Bizitza Batten Borroka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M3OaOrdZbuk
---

HODEI GAINEAN GORAKA GOAZ
ILARGIRAINO BATERA HEGAN
GURE AHOTSA ZABALDUZ MUNDURA
BIZITZA BATTEN BORROKA
Maitasun keinu ttipiek 
egiten gaituzte haundi
Gure ahotsen babesa, 
zuen kemen ta ausardi
Gaua ez dabeti iluna, 
ikus dezakegu ongi
Ilargiaren izpiak 
gure urratsen zaindari
Ehunka esku lotuek
Sortu duten indarrak
Balia ditzagun orain
Batzeko zeruko izarrak
Hodei baten euri goxoz
Busti daitezen bazterrak
Bihar Loratu daitezen
Esperantzaren adarrak
Ereindako aldarria
Goraka doa zeruan
Zubi eder bat sortu da
Jarraitu dezagun dantzan
Iñor ez dago sobera
Bizitza Batten borrokan
Hodeilargi bakarrik
Inoiz ez daitezen izan




Musika/Letrak XUTIK TALDEA
Kolaborazioak JOSUNE ARAKISTAIN , EÑAUT ELORRIETA, AIORA RENTERIA.
Grabaketa ATURRI STUDIO(PAXKAL ETXEPARE)eta KOBA STUDIO(XANPE)
Nahasketa PAXKAL ETXEPARE
Masterizazioa BASALTE STUDIO
bideoa EHUko IKUS-ENTZUNEZKO KOMUNIKAZIO 4.MAILAKO IKASLEAK


Eskerrak   BILBOKO UDALA, BILBOKO SUHILTZAILEAK (2.TALDEA), BILBOKO SUHILTZAILEEN KULTURA ETA KIROL ELKARTEA, GORKA RENOBALES, BILBOKO ELA SINDIKATUA.