---
id: tx-1224
izenburua: Harro Gaude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_jNB2qg8L10
---

Ertzetatik erdira
atzetik aurrera,
zulorik zulo eta
barreraz barrera,
emakumeak plazan
egin du karrera
izan zirenei esker
gu ere bagera!


Felisa Bermeosolo
lehenengo landare
Frantziska Antonia Irizar
zumarragarrare,
Maurizia Aldeiturriaga
Primi Erostarbe,
emakumeak gera
eta harro gaude!!
     
    
Omendu nahi ditugu
emakume denak
itzaletik irtetzen
ausartu zirenak
florero izateari
utzi ziotenak
jardin haundi hontako
lore ederrenak!!!