---
id: tx-1611
izenburua: Zoroetxeko Harresia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4vm7-6bICBY
---

Ze ederra den Gesalibarra
oinez astiro paseatzeko,
eta agian. estutuz gero,
bizimoduaz gogoratzeko.

Zoroetxeko harresiaren 
alde batean edo bestean
ibiltzen gara batzuk lanean
eta besteak nora ezean.

          Bestaldekoek, hurbiltzeko eta
                egin digute galdera itzela:
            Zein da ba hemen zoroetxea
                     gurea ala zeuen aldea?

Nork daki, jaunok, eta andereok, 
zein den benetan erotegia
hesiaz harunzkaldekoa ala
geure ustezko normaldegia?