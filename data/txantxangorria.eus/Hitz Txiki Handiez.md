---
id: tx-1962
izenburua: Hitz Txiki Handiez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z5izWNCPUfg
---

Zaude ixilik, ez hitzik esan.
Gogaitu naiz entzutez
nola itotzen dituzun
berba jantziegiak.
Jakin, eder direla,
biluzik dauden egiak.

Hitz handiez esan baino
hitz txikiz egia handiak
oihukatzea dugu maiteago.

Badakienak ixil ixilik,
ixiltasunez,
mutu gaurdaino
gehiago esan izan du beti,
ezjakinak hitz jantzidunez baino.

Aspalditik luzeegi dirauen
jauntxo harro ahoberoenak
utzi nahi du ahotsik gabe
ixildi ezin dena.

Hitz handiez esan baino
hitz txikiz egia handiak
oihukatzea dugu maiteago.