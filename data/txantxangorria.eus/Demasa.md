---
id: tx-1457
izenburua: Demasa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Edc3b7KhmhQ
---

Ozen, argi, beldur gabe esan munduari:
nor zaren, zein garen,
euskaraz noraino iritsi ahal den,
gu gara demasak!

Metroz metro egiten
dira kilometroak,
alez ale hondartzak
hitzez hitz hizkuntzak (e)ta
guztion ahaleginez
ikastola demasak!

Ezkutuan ikasten,
ezkutuan aranzaditarrak ernaltzen
euskaraz munduan libre bizitzen
demasak garela, demasak sentitzen
txikiak gara baina xumeak inondik
agresio guztien gainetik zutik
zerbait sortuaz hemendik
kilometroka gora!

Ozen, argi, beldur gabe esan munduari:
nor zaren, zein garen,
euskaraz noraino iritsi ahal den,
gu gara demasak!

Amets zaharrak berrituz
unera egokituz 
(e)ta geroa marraztuz.

Haien ametsak gure errealitate
ereindako haziek fruitua dakarte
hautsiko ez den euskararen kate
kilometroz kilometro infinitura arte!

Herria da hizkuntzaren habia
ikastola handi hasi den txoria
euskara mendeetako testigu bizia.

Ozen, argi, beldur gabe esan munduari:
nor zaren, zein garen,
euskaraz noraino iritsi ahal den,
gu gara demasak!

Aranzadi, beldur gabe esan munduari:
nor zaren zein garen,
euskaraz noraino iritsi ahal den,
gu gara demasak!