---
id: tx-2476
izenburua: Who Am I
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_VNBeVNrOQw
---

"Lou Reed, mila esker" diskan jasotako abestia. Txuma Murugarrenek euskarara itzulia eta Gorka Urbizuk (Berri Txarrak) abestua. 

Hitzak: 
Batzutan nor naizen ez dakit munduak ihes egiten dit 
Gizon gazte bat zahartuz doana 
jakin nahi nuke zer dakarren geroak 
Ispilu aurrean aurpegiak baditu hainbat zimur agerian 
maite zintudala erakusten dute 
eta pasioak arrazoia bitan duela hausten 
Gelditu behar dut eta hausnartu hau oroimenak iluntzen al nau? gogoan dugu gerta ziteken hori 
eta gero aurregin errealitateari 
Batzutan galdetzen dut nor naizen ni zerulurrak nork zituen egin nork ekaitzak nork maitasun eza ez dakit zenbat bizitz har dezakedan 
(Who am I?)
Badakit maite dudala amestea 
Ez diren mundu horiek sortzea 
Gorroto arnas egin beharrari 
Nahi dut libre izan eta gorputz hau betiko utzi 
Hegan egin ume mistiko bat bezela 
Nahi diot aingeru bati muin eman, muin eman kopetan 
Nahi dut asmatu bizitzaren zentzua 
norbaiti lepoa moztuz edo erausiz bihotza 
Nahi zenuke taupaka ikusi begiak oraindik ez itsi 
Eta hilik nagoela jakinda ere izterretatik nahiko nauzu eutsi 
Txarra bada pentsatzea hontan gorde iragan ximela eskuetan Zergatik eman ziguten oroimena? 
Zergatik libre bizi? Gal dezagun burua 
Batzutan nor naizen ez dakit munduak ihes egiten dit 
Gizon gazte bat zahartuz doana jakin nahi nuke zer dakarren geroak 
Jakin nahi 
Jakin nahi 
Jakin nahi nuke nork hasi zuen hau 
Jainkoak maiteminez musu bat emanez gero traizionatu zuen norbaiti eta maitasuna zigun erauzi
 (Who am I?) 
Gero traizionatu zuen norbaiti eta maitasuna zigun erauzi