---
id: tx-1434
izenburua: Ezin Leike
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jxej2ZbfX84
---

Noiz arte jarraitu beharko dugu ikusten 
horrenbeste uniforme kalean zehar lasai pasiatzen 
eta honela ezin leike hemen ibili 
ezin leike bizi, ezin leike inondik inora gustora ibili 
eta honela ezin leike, ezin leike hemen bizi. 

Noiz arte jarraitu beharko dugu ikusten 
jendea izorratzeko gogoa besterik ez daukate 
eta honela ezin leike hemen ibili 
ezin leike hemen bizi, 
horrenbeste pasma, ertzaina eta pikolo ikusi 
eta honela ezin leike, 
ezin leike hemen bizi. 

Eta honela ezin leike hemen ibili 
ezin leike hemen bizi, 
Eta honela ezin leike inondik inora 
gure gauzak gure gustora adierazi 
eta honela ezin leike 
ezin leike hemen ibili 
ezin leike hemen bizi