---
id: tx-944
izenburua: Azken Eztanda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AnInu4RE_e4
---

Azken Eztanda
de l’album Argibideak par Guda Dantza
 
  Partager / Intégrer   Liste de souhaits
00:00 / 03:56
Piste numérique
Écoute en continu + téléchargement
Comprend le téléchargement de haute qualité en MP3, FLAC et plus. Les supporteurs payants obtiennent également l’écoute en continu illimitée au moyen de l’appli gratuite de Bandcamp.
Acheter la piste numérique   proposez un prix
Donner en cadeau 
Acheter l’album numérique complet
paroles
Nora helduko garen 
jakin gabe bada ere, 
aurrera egiten dugu 
puntu berdinera iristen. 
Helmugarik gabeko bidean, 
gabiltz hautsak harrotzen 
ezerezaren suak kiskaltzen 
gaituen arte. 
Argi bat itzaltzen den bitartean, 
haizearekin doan izarren hautsa gara. 

Los páramos yacen muertos, 
sometidos al terror del llanto 
del jornalero que riega 
el campo con su dolor. 

Las aves alzan su vuelo 
dirigiéndose hacia el sol, 
renuncian a la existencia 
para hervirse en su calor. 

Captando el sepulcro 
de la eternidad 
en un plano tan cerrado 
que no deja respirar 

Geratu nirekin batera, 
arren, geratu, amaiera heltzean. 
Munduaren azken erreminetan, 
arren, geratu, nirekin batera. 

Eta azken eztanda zabalduz dihoa, 
izan ezin ginenaren hortzimuga horietan, 
tartean hemen gara elkarri begira.