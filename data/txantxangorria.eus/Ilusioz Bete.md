---
id: tx-757
izenburua: Ilusioz Bete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PptZ9tc91so
---

Huntza - Ilusioz bete
'Xilema' bigarren diskoko bederatzigarren kantua. 

Haritz Harreguy estudioan grabatua eta nahastua.
Victor Garcíak masterizatua Ultramarinos Mastering-en.

Zaila da hastea, hain zaila ahaztea,
nola laburtu esaldi batean...
Nahiz eta banandu bidegurutzean
zuen zati bat daramat gainean. (x2)

Entzun bidelagun, onartu dezagun;
malkoak datoz irribarre eske,
utziko ditugu agurrak aparte,
hartu bi muxu eta gero arte. (x2)

Jatorrak, goxoak, alaiak, zoroak,
beti ondoan egongo zarete.
Datorrenerako ilusioz bete,
jarrai dezagun ametsetan tente! (x2)

Jarrai dezagun ametsetan tente!
Jarrai dezagun ametsetan tente!