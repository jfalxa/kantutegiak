---
id: tx-225
izenburua: Üskaldun Dotia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Gy8eP2tZ7Vw
---

Esküal Herrian, zazpi probintzia
Badakigu ttipiena dela XIBERUA
Horrengatik bizi da iruz hor jentia
Sortzez beitu ükhenik üskaldun dotia.

Khantatzen dügü, oi trenpü d¨günin
Batek aphal`aphala, bestek aidin gaitzin
Ez gira ez, gü lotsa, nur nahi jin dadin
Botzez aberats gira Xiberoko aldin...

Nula ez aipha, gure dantz ederrak
Hoik beitirade ümen paregabekuak
Ahal tügü ikhusten, han hortxe maskadak
Maite dianak ere, nuiztenka phastualak