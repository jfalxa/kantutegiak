---
id: tx-2787
izenburua: Maite Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ffi5q08Nvrw
---

Maite, maite
ez ni utzi hemen kaiean.
Maite, maite
zu nere bihotzean.

Maitetxo itsasora juan da
itsasontzi batean.
Maitetxo itsasoan dago
urrutirako bidean.

Maite, maite...

Maitetxo noiz etorriko da,
nere bihotza zain dago.
Maitetxo noiz etorriko da,
biok alkar bizitzeko.

Maite, maite...