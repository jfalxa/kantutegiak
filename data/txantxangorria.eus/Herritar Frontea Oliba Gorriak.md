---
id: tx-3089
izenburua: Herritar Frontea Oliba Gorriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZCxqNuGgtfQ
---

Tantaz-tanta... 
UHOLDEA!
...Herri boterea!
Kolpez-kolpe... 
borroka frontean...

MATXINADA!

Herriz-herri... 
Harriz-harri...
...Herri boterea!
Pausoz-pauso...
Arloz-arlo...
Herri botererantz!

MATXINADA!

Herri antolatua duin ta harro
Produkzio bideak
Herritik herriarentzako!
Gaitasunen araberako ekarpenak,
beharrizanen araberako bermeak!

MATXINADA!

Formatuz armatu,
sortuz kolpatu!

MATXINADA!

Eraitsiz eraiki,
borrokaz garaitu!

MATXINADA!

Herri botererantz!

MATXINADA!