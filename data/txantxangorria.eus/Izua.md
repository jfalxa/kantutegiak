---
id: tx-429
izenburua: Izua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ApH1eVjY_3M
---

Uooooooo, uooooooo…

Izua da nagusi gure bihotzetan askatasuna falta zaigu guri oinetan. Ingurura begira beldurra begietan, erdi korrika goaz etxerako bidean.
Etxeko andre, sukaldari eta andereño gera besteek egiten dute gure ordez aukera.

Gizarte ustel hontan gizonezkoen baitan bizi izan baikara beti itzaletan.
Gizarte ustel hontan emakume gara
ta erakutsi dezagun barruan dugun indarra.

Uooooooo, uooooooo… Uooooooo, uooooooo…

Dotore jantzi zaitez diote ahotsak tamalez ez ditut gustuko takoi arroxak.
Eguraldiak soilik erabaki dezala zerez estaliko dudan goizero azala.

Gizarte ustel hontan…
Aaaa, indarra, indarraaaaa, indarra, indarra Izua zen nagusi gure bihotzetan
ausardiak kendu dio lekua zainetan!