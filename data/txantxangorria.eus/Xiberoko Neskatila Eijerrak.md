---
id: tx-1047
izenburua: Xiberoko Neskatila Eijerrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d3jI2sNMn98
---

XIBEROKO NESKATILA EIJERRAK
 
                I
New -Yorkerik jin zaizkü bizpa-lau mezajer
Gure Eskualherrilat ikhustera bazter
Jakitera bazenez zunbait nexka eijer
Kapable egiteko aurthen «Miss Univers»
 
                II
Ikhertürik Laphurdi ta Baxe-Nabarra
Ez zaie agradatü hankorik nihure
Azkenekotz Xiberun bat hitzartü dire
Artzaintsa gaztetto bat izarraren pare
 
                III
Galdegin ükhen deiet ahal bezaiti bertan
Zer gatik hartü diren hore etxauetan
Bazakitela franko segur kharriketan
Emazteki propia kasik etx'orotan
 
                IV
Gustatü baginande, kharrikesetarik
Ez günükin segürki hunat jin beharrik
Zeren ta gima harro eta ezpaiñ gorri
Ameriketan badügü, phalatakaz hori
 
                V
Zaragolla lüzekin dütük fanfaruxka
Zer nahi thenoretan kanpuan kuhinxka
Gibela lüxatürik ürhatsa xehexka
A eta ez beldürrez «arrautziak küxka»
 
                VI
Eijer bai bena prenda debriaz egina
Nuiz nahi dükezü ezkuntzeko adina
Ez dezagüla egin hola moxko fina
Kartielian beitügü aski mutxurdina
 
                        1956 urthian egina.