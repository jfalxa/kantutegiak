---
id: tx-187
izenburua: Maddalen Iruriko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PsCBD25a0Vs
---

Oi Maddalen! Oi Maddalen! 
Maddalen Iruriko füian zirenian, zure parerik etzen segür Xiberoan, 
Pollit eta xarmanta, nuiz gogua sütan, mutil gaztiak oro ondotik züntian. 
Oi Maddalen Maddalen! Oi Maddalen! ene pentsamentietan zü baizik ez nian, Oi Maddalen Maddalen! Oi Maddalen! erran bazünü zerbait, erranen neizün bai! 
Beti zuri pentsatzen, lo güti egiten, zü aldiz, arte hortan, bestekin laketzen, Ni ezagütü gabe saihetsilat üzten, zer behar nüke egin ? enüzü ausartzen.. 
Maddalen Iruriko urtiak iragan eta gü zerentako bedere lekietan? 
Ezin deliberatüz, zü, ordü hunetan, hain sari batü zira lagün gabezian! 
Hitzak : Altzükütarrak 
Müsika : herrikoa