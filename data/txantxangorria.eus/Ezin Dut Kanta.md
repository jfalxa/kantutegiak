---
id: tx-3051
izenburua: Ezin Dut Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z3-5dzoj9Mw
---

Kanta behar nuke, bai,
baina ezin kanta:
poza baitzait hain falta
ta samiña sobra..

Ta samiña sobra:
galtzen ikusten bai dut
herriaren ondra.

Ta ezin dut kanta
eta, apenas konta
herriaren pena.

Laido ta bortxa
sufritzen ari da,
abotsa moztuta.

Ta, ezin dut kanta
borreruen kontu
lehunak gorabehera.

Eta apenas konta
herriaren pena,
hain ezina bait da.

Ezin bait du aitortu
bere nortasunik,
hain inor izanda;

ezin bait du eskatu
bere eskubiderik,
rebeldia bait da;

ezin bait du kexatu
itxuragaitik, ze
turistak datoz-ta.

Ta ezinen ezina
pozeraino jeitsi da
Ta ezin dut kanta.