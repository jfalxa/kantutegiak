---
id: tx-3092
izenburua: Zuregana Kalte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RdhYLa3G0VU
---

Uda honen erdian
ezagutu egin gara
denbora gelditu nahian
i/lusioa ari da
oihuka hemen barnean
nire sekretua orain...
zu ezagutzean, zure begietan

Zu/re a/za/le/an gal/du/ta
Zu/re/ga/na,
zu/re be/ro/a sen/ti/tze/ko
Zu/re/ga/na,
zu/re ke/inu guz/ti/ak i/kas/te/ko
Zu/re/ga/na,
ba/ina txi/ki/a e/gi/ten naiz
e/ta e/zi/nez/ko/a da
Zu/re/ga/na,
be/ti/ko or/ma/ren kon/tra no/a
A/ho/a za/bal/tze/an
ir/ten ez di/ren hi/tzak
i/to/tzen di/ra se/gun/du/tan
hu/rren/go be/gi/ra/dan
u/rrez/ko mo/men/tu/an
sen/ti/tzen du/da/na o/rain...

zu e/za/gu/tze/an, zu/re be/gi/e/tan
zu/re a/za/le/an gal/du/ta
Zu/re/ga/na,
zu/re be/ro/a sen/ti/tze/ko
Zu/re/ga/na,
zu/re ke/inu guz/ti/ak i/kas/te/ko
Zu/re/ga/na,
ba/ina txi/ki/a e/gi/ten naiz
e/ta e/zi/nez/ko/a da
Zu/re/ga/na,
be/ti/ko or/ma/ren kon/tra no/a

zu e/za/gu/tze/an, zu/re be/gi/e/tan
zu/re a/za/le/an gal/du/ta
Zu/re/ga/na,
zu/re be/ro/a sen/ti/tze/ko
Zu/re/ga/na,
zu/re ke/inu guz/ti/ak i/kas/te/ko
Zu/re/ga/na,
ba/ina txi/ki/a e/gi/ten naiz
e/ta e/zi/nez/ko/a da
Zu/re/ga/na,
be/ti/ko or/ma/ren kon/tra no/a
A/ho/a za/bal/tze/an
ir/ten ez di/ren hi/tzak