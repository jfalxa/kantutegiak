---
id: tx-2373
izenburua: Eutsi Lagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GyZVxKBTEt8
---

#AltsasukoakASKE 
@alerta_gorria 
EUTSI LAGUN 
Gelako leihotik begiratuz kalera 
Norbaiten falta dudala 
Gogora ekarri ditut nire ondora 
Barruan dauden lagunak 
Bakardadea, iluntasuna 
Ziega zuloan giltzapetua 

HEMEN GAITUZUE, 
ELKARTASUNA EMATEN ALTSASUKOEI 
EUTSI LAGUN,
ZUEKIN GAUDE 

Bizitza baten oroitzapenei helduta 
urruntasuna laburtuz 
Indarrez beteriko uneak dira 
Kristalaren bi aldetan 
Kilometroak, sentimenduak 
Etxe bakoitzean dagoen hutsunea 

HEMEN GAITUZUE, 
ELKARTASUNA EMATEN ALTSASUKOEI 
EUTSI LAGUN,
ZUEKIN GAUDE 
Komunikabide, epaile ta txakurrek 
Eraikitako muntaia 
Zuen azaletan jasan behar duzue 
Herriaren amorrua 
Audientzia, nazkagarria 
Justiziaren minbizia 
Euskal Herritik, Sakanatik 
Garraxika alde hemendik 
HEMEN GAITUZUE, 
ELKARTASUNA EMATEN ALTSASUKOEI 
EUTSI LAGUN, 
ZUEKIN GAUDE 
@alerta_gorria 
Kontaktua : alerta.redsound@gmail.com

#saveyourinternet