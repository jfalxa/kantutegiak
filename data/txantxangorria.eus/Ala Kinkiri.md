---
id: tx-1873
izenburua: Ala Kinkiri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o9IBkw2HZQg
---

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako dama gazteak
ez dira moja sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira,
hola!

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako gizon gazteak
ez dira fraile sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira,
hola!

Erran dut, erranen eta errango,
ez naiz isilik egongo,
plaza hontako gazte guztiak
ezkontzan dira sartuko.

Ala kinkiri, ala kunkuru,
ala kinkiri kunkuru kanta,
ala kinkiri kunkuru kanta
gu bezalakoak dira,
hola!