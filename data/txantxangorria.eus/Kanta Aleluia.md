---
id: tx-2015
izenburua: Kanta Aleluia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mQ_gdOjRhzA
---

Kanta kantari daude zeruan
Kanta kantari aingeruak (2)

Kanta alleluia, kanta alleluia, beti alleluia
kanta alleluia (2)

Epel-epeltsu talde giroan
Aitaren ta amaren ondoan. (2)

Kanta alleluia, kanta alleluia, beti alleluia
kanta alleluia (2)

Gaur jaio zaigu Jesus haurtxoa
Goratu dezagun jainkoa

Kanta alleluia, kanta alleluia, beti alleluia
kanta alleluia (2)