---
id: tx-523
izenburua: Bidaiari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/acaP7JXHFX4
---

Bidaiari

Zergatik
ez du begiratu nahi
leihotik
ikusten dituen lainoez haratago?

Geltokien panela
azkenetan du eta
ezin die
berriz bizia eman anden ahaztuei.

Ez dut ulertzen ama
nola duen nahiago
zerbaitekin ez amestu
parean mortu badago.

Zergatik begiratu
behar du haratago
behin amestu zuen hori
jadanik ez badago?

Zergatik
du erloju zahar hori
jadanik
bere arnasa bezala akitua badago?

Zenbait dantza motel,
ilun bezain goibel
eskaintzen
dizkiete orratzek momentu hautsiei.

Ez dut ulertzen ama
nola duen nahiago
erloju zahar hori ibili
berri bat merke badago.

Zergatik du ordea
behar berriago
momentu gehiago hausteko
astirik ez badago?

Zergatik nahi dut jakin...
Zergatik dagoen horrela.
Zergatik bisai triste hori?
Zergatik ama? Zergatik?

Ez dut ulertzen ama
nola duen nahiago
maleta zahar hori ibili
berri bat merke badago.

Zergatik du ordea
behar berriago
osorik betetzeko
astirik ez badago?

Ez dut ulertzen ama
nola duen nahiago
jaka jantzita ibili
hemen bero badago.

Zergatik du ordea 
azala askatuko
eguzkitan jarriz ere
arima hotz badago?

Zergatik
ez du begiratu nahi
leihotik
ikusten dituen lainoez haratago?
crédits
de l’album URA ETA BAKEA, paru le 15 novembre 2019
Musika Keu Agirretxea / Hitzak Aiert Goenaga