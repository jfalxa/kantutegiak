---
id: tx-1276
izenburua: Biolentzia 1984
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PJP1vqYkp8U
---

Kaleko Urdangak (Bardulia, Basque Country) 2018. (EUS)"Biolentzia 1984" abestia izen bereko 7 hatz betekotik hartua, "Nortasuna" LParen (2018ko irailaren 1ean argitaratuko dena) aurrerapen abestia Fermin Muguruzaren parte hartze bereziarekin.