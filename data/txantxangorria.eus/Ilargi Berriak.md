---
id: tx-316
izenburua: Ilargi Berriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kVHMh6VGXX8
---

Tradiziotik datorren kalejira baten erritmoan, soinu eta estetika apurtzailea dituen kantua da gure lehen single hau: ILARGI BERRIAK


Alaitz Eskudero Unaue, Amets Ormaetxea Ezpeleta, Eneritz Aulestia Mutiozabal, Garazi Otaegi Lasarte, Irati Gutierrez Artetxe, Leire Etxezarreta Learreta eta Maria Lasa Hilario.


SARE SOZIALAK:


EKIPOA:

Bideo Grabazioa eta Edizioa: Bideolan Proyectos Audiovisuales @bideolan_oficial
Audio Grabazioa eta Produkzioa: Haritz Harreguy Studio @haritzharreguy
Masterizazioa: Victor Garcia (Ultramarinos Mastering)
Jantzien Diseinua: Amaia Albes @amaialbes
Estilismoa: Maite Albes @maite.albes
Ile-apainketa: Arima Ile-Apaindegia @arima___ileapaindegia
Makilajea: Nua Estetika Zentroa @nua_estetika_zentroa
Gorputz Adierazpena: Garazi Etxaburu
Kontratazioa: Airaka @airakamusic

LETRA:

Gaua eder dago
Haizea gogor leihoan
Izarrak dantzan ta
Kopla zaharrak alboan. 
Bideak, doinuak
Daramatzagu kolkoan
Ilargi berriak
Lagun zaharren ondoan.

Laralala…

Sua maite dugu
Akelarreen beroan
Emeki, indartsu
Ulu zakarren goxoan. 
Bideak, doinuak
Daramatzagu kolkoan
Ilargi berriak
Lagun zaharren ondoan.

Laralala…

Sugarra piztu da
Ahots indartsuen loan
Aurreko plazandren
Koplak dauzkagu ahoan. 
Bideak, doinuak
Daramatzagu kolkoan
Ilargi berriak
Lagun zaharren ondoan.

Laralala…

Bideak, doinuak,
Kopla zahar berriak,
Sorginen kantuak,
Haien abestiak, 
Sua, akelarrea,
Gure irrintziak,
Aurreko plazandrak…
Ilargi berriak.