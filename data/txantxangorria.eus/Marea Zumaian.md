---
id: tx-1449
izenburua: Marea Zumaian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cVFnkXmfKk0
---

Harriz harri saltoka
itsasertzean
harriz harri saltoka
gu

Harri azpian glu glu
Dago andarika
Bere atzetik
korrika gu

Zoko batian gordea
Maolillua
Uraren kirikibildutan

Txikia ta iletsua
da txoaskiña
brankadetako zulotan

Algorri labarrean
ilgora ilbehera
marea gora eta
marea behera
Algorri labarretan
ilgora ilbehera
beti da desberdina
beti da bera

Iraileko mareetan
dator oldija
itsas belar osasuntsua

Zumaiako erregea
da olarrua
erroa galtzea ez du bere errua

Gomexa da putzutako
kamaleoia
orain urdin
Gero marroia

Harrapatzeko zailena
hor da ixkira
gauean begiek dizdira