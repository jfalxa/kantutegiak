---
id: tx-346
izenburua: Hibernazioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kizXxEOUsIM
---

IZAKI GARDENAK taldearen AURRI GARA lan berrian jasotzen den HIBERNAZIOA kantuaren bideoklipa.

Egunak
triangelu beltz honek
irentsita.
Laguntza
dei berantiarregiak
eskegita

Buru nekatu honen
Hibernazioa,
egun eta gau.
Ezerezarekin
￼urtzera noa,
denborak galdu nau.

Haitzetara,
itsasargi lagunek
bidalita.
Egarriz,
euriei itxaroten
eserita.