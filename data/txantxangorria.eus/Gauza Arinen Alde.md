---
id: tx-1069
izenburua: Gauza Arinen Alde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9MTm1K2ZRc0
---

Letra: Harkaitz Cano
Zuzendaritza eta errealizazioa: Maria Elorza eta Maider Fernandez
Aktoreak: Fariba Sheikhan eta Iker Lauroba
Abestiaren grabazioa: Fredi Pelaez (Pottoko Studio)
Musikariak: Juanma Urriza (Bateria), Tana Santana (Kontrabaxua), Fredi Pelaez (Hammond organoa), Beñat Barandiaran (Gitarrak), Fariba Sheikhan (Ahotsa) eta Iker Lauroba (Ahotsa).
GAUZA ARINEN ALDE
Geriza
haizetan,
zer beste 
behar da, ba bizitzeko?
Aulki bat,
ohe bat, izarak, 
bi esku, neureez aparte...
Nork ez du zurekin 
ezin zenba 
gau oso nahi?
Xerlo hori hau
kiribilduz lokartzea? 
Gerizaren 
gerizan,
itzal bat 
itzal bati so. 
Luma baten pisua 
ezpainez ezpain.
Ontzi bat
lakuan,
zer beste
behar da, ba bizitzeko? 
Arraun bat
edo bi aukeran... 
gozoki, hondoratzeko.
Ametsak
ezin zenba ahala: 
laurogei, ehun edo mila; 
zaharrak 
usteldu orduko
berriak rekamaran prest.
Beude gure zain
Paris, Roma, eta Amsterdam. 
Egin zuk arraun, 
nekatuta naukazu gaur. 
Kareletik zama bota; 
nire kontu
kantu leun hau
etorkizun urrunaren
zamarik gabe.