---
id: tx-1986
izenburua: Aurtxo Txikia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jPnXfuDxpTE
---

Aurtxo txikia negarrez dago
Ama, emaiozu titia;
Aita gaiztoa tabernan dago
pikaro jokalaria.

Iñoiz aitatxo txintxotzen bada
Oi zeñene esker aundia!
Orduantxe nik bai ekarriko
zapata berri txuriak

Joku gaizto, txar, zikin, ziztriña,
nondikan haugu sortua?
Nere maitetxo kuttun polita,
ik dek alperrik galdua