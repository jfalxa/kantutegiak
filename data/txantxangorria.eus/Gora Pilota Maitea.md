---
id: tx-1180
izenburua: Gora Pilota Maitea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kRl5FbQcJnA
---

Euskadin badugu kirol maitatu bat 
Gehienek jokatzen duguna 
Atxiki behar duguna, hori baita hoberena 
Pilota baitu bere izena 
Aupa pilotari gazte ta zaharrak 
Aupa txapeldun pilotariak 
Ahotsa dugun arte, beti kanta dezagun 
Gora pilota mundu guzian 
Adiskide pilotazaleak gara 
Zer guri da Pilotarien Biltzarra 
Gora pilota maite, plazetako pozgarri 
Gora pilota orain eta beti 
Sekulako pilota bizi dadila 
Behintzat hori da gure nahia 
Dena egingo dugu, pilota ez galtzeko 
Beti indarra atxikitzeko 
Zuentzako txapeldun pilotariak 
Munduko txalorik beroena 
Aupa pilotariak, gora jokalariak 
Oroituko zaituzte herriak 
Adiskide pilotazaleak gara 
Zer guri da Pilotarien Biltzarra 
Gora pilota maite, plazetako pozgarri 
Gora pilota orain eta beti