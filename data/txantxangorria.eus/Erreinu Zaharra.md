---
id: tx-230
izenburua: Erreinu Zaharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QG5WHjtg9tE
---

Abesti hau napar erresistentziari eskeinia dago, bereziki duela 500 urte erresumaren aldeko defentsan Amaiurko gazteluan bizitza eman zuten pertsonei.
1522-2022
        ⚔
BETIKO ARGIA!

Abestia Iruñako Sound of Sirens estudioan grabatua eta Masterizatua dago Julen Urzaizen eskutik. Irudiak  Dani SKST eta Albaro Soule lagunek grabatuak dira. Bideoklipa Danik zuzendua eta editatua 2022ko Udaberrian.


LETRA:
Herri honen biziraupena odol tintaz idazten da
Gazteluko harri hauen artean beldur gabe hilko gara
Maitasuna dugu izen, leialtasuna abizen

ZIN EGITEN DUGU BETIRAKO, ERREINU ZAHARRA EZ DA ERRAZ ERORIKO
NAFARROA ALA HIL  BETIRAKO,KATE TA LILI DORRE GOITIAN DAGO

200 bat gudari mindu, noaindik ezin makurtu
Etsaiaren handitasunak ezin du gure bihotza txikitu
Maitasuna dugu izen, leialtasuna abizen

ZIN EGITEN DUGU BETIRAKO, ERREINU ZAHARRA EZ DA ERRAZ ERORIKO
NAFARROA ALA HIL BETIRAKO, KATE TA LILI DORRE GOITIAN DAGO

Sitiatuta, atzo eta gaur erresistentzia.
Sitiatuta, eroriei betiko argia!

ZIN EGITEN DUGU BETIRAKO, ERREINU ZAHARRA EZ DA ERRAZ ERORIKO
NAFARROA ALA HIL BETIRAKO, KATE TA LILI DORRE GOITIAN DAGO

TA HOR IRAUNGO DU, HERRI HONETAN EZ BAITA
ALABA ETA SEMERIK  SALDUKO DUENIK