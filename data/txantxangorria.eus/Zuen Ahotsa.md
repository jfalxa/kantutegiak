---
id: tx-2254
izenburua: Zuen Ahotsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q4mrZGICPiY
---

(EUS) BADATOR ERRELEBOA!

Bizitzen ari garen garai gogor hauek ez gaituzte amilduko, ezta gutxiago ere. Izan ere, musikari, teknikari, manager, … bakoitzaren atzean beti baitago familia handi bat (hitzaren zentzu literalean ere).

Horren adibide dira Anuk eta Nora; Haritz Harreguy teknikariaren eta Keu Agirretxea musikariaren alabak eta Mark eta Aiala; Iñigo Zubizarreta Soziedad Alkoholika-ko gitarra-jotzailearen eta Mauka Musikagintzako 'Ñako' Beato-ren lobak dira, hurrenez hurren. Grabaketak norbere etxean eta nahasketa Haritz Harreguy estudioan egina.

Pink artistaren 'Try' kantu ezagunaren euskarazko bertsioarekin, lau neska hauek,  bere senideengandik jasotako musikarekiko maitasun eta pasioa itzuli nahi izan diete. Belaunaldi berriek zoritxarraren aurrean sortzen jarraitu beharra aldarrikatzen dute.

Krisi honek ez du musikaren sugarra itzaliko, noski ezetz. Eta bitartean, goza dezagun belaunaldi berriez!


MUSIKARIAK:
Anuk Harreguy – Ahotsa eta gitarra
Mark Zubizarreta - Baxua
Nora Agirretxea - Teklatua
Aiala Beato – Bateria

HITZAK - Anuk Harreguy

Nere izatearen ongarri
gure arteko harremana.
Buruan dabilkizuet sarri
baina ezin egon batera.

Momentu hotz eta ilunenetan
zuen ahotsa ez zait falta izan.
Nere helduleku eta irakasle,
itxarongo zaituztet gaur ta bihar. 

Tximeletak gora ta behera
nirekin zaudetenean,
baita zuen irrifar hori
sentitzen dudanean.

Momentu hotz eta ilunenetan
zuen ahotsa ez zait falta izan.
Nere helduleku eta irakasle
Itxarongo zaituztet gaur ta bihar.