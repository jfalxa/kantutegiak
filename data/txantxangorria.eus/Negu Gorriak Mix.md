---
id: tx-916
izenburua: Negu Gorriak Mix
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jj4-AyV6j3E
---

IKUSITA NEUZKAN
AMODIOA ETA GORROTOA
HATZ KOSKOETAN
TATUATURIK.
BAINA UKABILKA
IBILTZEKO
HATZAK JANTZI
BEHAR DITUZU.
ETA BATZUTAN
AMODIOAREKIN
BESTETAN ALDIZ
GORROTOAREKIN.
ETA BATZUTAN
AMODIOAREKIN
BESTETAN ALDIZ
GORROTOAREKIN.

HORRELAKOA DA BIZITZA
EGIN EZAZU BEHAR DUZUNA

BAT-BI, BAT-BI
HORIXE DA
GUSTATZEN ZAIDAN
MUGIMENDUA
RAP-AREKIN
DANTZA ORDUKO
ROSIE PEREZ
IKUSI EZKERO
MIKE TYSON K.O.
UTZI ZUELA
JAKITEAK
EZ ZAITU HARRITUKO

BAT-BI, BAT-BI
RAP-AREKIN
GUSTATZEN ZAIDAN
MUGIMENDUAREKIN

HORRELAKOA DA BIZITZA
EGIN EZAZU BEHAR DUZUNA

ESAERA BAT BADA
ZARATAK HAMALAU
URRUTIKO INTXAURRAK
GERTURATU TA LAU
GURE HERRIAN BERRIZ
HOGEITA HAMALAU
PROBATU EZ DUENAK
EZ DAKI ZER DEN HAU
EZTARRI INDARTSUAK
EZIN DIRA ITO
GURE LEPOAN SOKA
EZ DAGO BETIKO
PARRE EGIN GOGOTIK
NESKA TA MUTIKO
KANTATZEN DUEN HERRI BAT
EZ DA INOIZ HILKO

GORA HERRIA!
"VIVA EL PUEBLO SOBERANO EL PUEBLO QUE NUNCA OLVIDA EL PUEBLO QUE DA LA VIDA POR DERROCAR AL TIRANO" .