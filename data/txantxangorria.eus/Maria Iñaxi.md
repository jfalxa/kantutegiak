---
id: tx-1914
izenburua: Maria Iñaxi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SMWnnz8wWCM
---

Guztiz polita zera Maria Iñaxi 
zergaitikan zabiltza nigandik ihesi 
guztiz polita zera Maria Iñaxi 
ai oi ene Maria Iñaxi 
zure maitasun hori nahi det irabazi. 

Oso ongi dakizu maite zaitudala 
zuregatik edozer egingo nukela 
oso ongi dakizu maite zaitudala 
ai oi ene maite zaitudala 
erleak loretxoa maite dun bezela. 

Egun guztia nago zurekin pentsatzen 
gauez oheratuta ez det lorik hartzen 
egun guztia nago zurekin pentsatzen 
ai oi ene zurekin pentsatzen 
pena det ez zerala 
hortaz konturatzen. 

Zure maitasunikan ez al det merezi 
hola bada luzaro ez ninteke bizi 
zure maitasunikan ez al det merezi 
ai oi ene ez al det merezi 
esan zaidazu baietz Maria Iñaxi.