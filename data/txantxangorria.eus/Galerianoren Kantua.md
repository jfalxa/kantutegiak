---
id: tx-1288
izenburua: Galerianoren Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4qxHOX5-WeA
---

I 
Mila zortzi ehun eta hamabortzgarrena 
Ni Ahazparnen preso hartu nindutena; 
Plumaieneko premu orok dakitena, 
Galeretan higatu beharko naizena.


II 
Kantatzera niazu alegera gabe 
Ez baitut probetchurik trichtaturlk ere; 
Nehun deusik ebatsi, gizonik hil gabe, 
Sekulakotz galerak enetako dire.


III 
Ala zorigaitzeko premu izaitea !
Horrek eman baiteraut bethikotz kaltea, 
Aitari gald'eginik sortzeko phartea, 
Galeretan eman nau, hau da ene dotea.


IV
Ene aita da gizon kontsideratua. 
Semia galeretan du segurtatua. 
Nun nahi othoitzean belaunikatua, 
Saindu iduri, debru madarikatua.


V 
Ene lehen kusia Cadet Bordachuri, 
Fagore bat banuke galdatzeko zuri 
Ongi adreza zaite ene arrebari, 
Ene saltzeko zombat ukan duyen sari.


VI 
Aita aintzinian eta arreba ondoko, 
Osaba buryes hori diru fornitzeko; 
Ez ordian enetako bi sei liberako, 
Galeretan bederen leher egiteko.


VII 
Elizan sartzen dira debozionerekin, 
Iduriz badohazila saindu guziekin 
Beren liburu eta arrosarioekin, 
Debruyak phesta onik einen du heyekin.


VIII 
Zortzigarren bertsuya aneyarendako, 
Konseilu bat banikek hiri emaiteko: 
Ontsa goberna hadi, ez zauk dolutuko; 
Ni baitan etsempluya errech duk hartzeko.


IX
Zuri mintzo nitzaizu, oi Aita zilharra, 
Ardura dudalarik begian nigarra; 
Zure eta ene arraza Bordachuritarra, 
Galeretan naizeno ni bainaiz bakharra.


X 
Kantu hauk eman ditut Paubeko hirian, 
Burdinez kargaturik oi presundegian, 
Bai eta kopiatu dembora berian, 
Orok khanta ditzaten Hazparne herrian.


XI 
Hok eman izan ditut ez changrinatzeko, 
Ene ahide adichkidek kuraye hartzeko, 
Eta partikulazki, aita, zuretako 
Kantu hok aditzean semiaz orhoitzeko.

Martin Larralde sortu zen plumaienean 1782 ko urtarrilaren 7 an, Guillaume Larralde Bordaxurikoa eta Marie Ithurbide Plumaienekoa danik. Ama hil zaionetik, aitari oldartu zaion, bere etxe partea galdatuz , Plumaieneko etxe partea.

 

Eztabada laster gaixtandu zen aita eta semearen artean. Aitak,  Jean Ospital Kaselarenbordako etxetiarrari  emana zion pentze baten belarra mozteko eta lantzeko, han belarretan ari zelarik, tiro bat bildu zuen espaldan, Plumaieneko premuaren danik, auziak agertu duen bezala. Jondonaneko eguna zen ekainaren 27 a.

 

1815 eko uztailaren 4 an Hazparneko Jandarmek preso altxatu zuten Martin Larralde, Baionarat ereman,  azaroaren 28 an auzitaratu zuten Paben eta hor sekulakotz galeretarat kondenatu,  7 epaile agertu ziren erabaki hunen alde eta 5 kontra. Rochefort hiriko galeretarat  eremana izan zen agorrilaren 5 ean 35 urte zituelarik eta han hil zen 1829 eko  abendoaren 19 an 40 urtetan.

 

Hixtorio latz huntaz, Piarres Larzabal antzerki idazlari apezak egin zuen gai bat “Bordaxuri” deitu zuena. Antzerki hunek ez du zinezko ixtorioarekin zer ikustekorik. Hemen agertarazten du, nola, aita hil nahi izan zuen, bertze asko ezberdintasunen artean. Bainan euskal Herriko antzokietan antzerki eder horren zabaltzeak, plumagaineko seme dohakabearen ixtorioa nasaiki jakinarazi du.

 

Berea zaukan lurraren gainean auzo bat belar biltzen ari ez zuelakotz jasan, nahiz aitaren manuz ari zen, belarra bildu zakon pentzetik plumaieneko selaururat, Jean Ospital-ek belar hori berriz bereganatzen ari kausiturik, tiro egin zizaion.

 

Epaileak, hamabietarik bederatzi ontasun jabeak ziren, Auzapez bat,Abokat bat eta arrantier bat. Hazparne Elizaberri Plumaieneko auzo gehienak lekuko izan ziren eta heien lekukotasunak hitz beretaz osatuak zirela idatzia da erdaraz, nahiz ez zakiten ez irakurtzen ez eta idazten ez eta oraino erdaraz mintzatzen. Auzian,  Plumaieneko semeak eta lekukoek xuberutar euskaldun bat zaukaten itzulpenarentzat.

 

Preso zelarik, Martin Larralde Plumaienekoak bertsu batzu idatziarazi zituen. 11 edo 12 bertsu dira, ezagutuenak hameka, guziz bortitzak bainan ere hunkigarriak eta ederki moldatuak, nahiz poto bat dagon bigarrenean (Gabe) hitza bi aldiz agertzen baita.

 

Bertsu horiek oraino kantatzen dira nasaiki Elizaberrin eta gehienen gogoan, Martin Larralde plumai, hobendun baino gehiago, gertakariaren jasaile izan zela diote. Ixtorio hau berena daukate eta kantu hau entzutean injustizia baten sendimendua nagusitzen zaiote.       

                            

Auzi hunen hemen agertzeak ez du nahikari berezi baten gutiziarik, bakarrik Pabeko arxibetan lo dagotzin paper zahar batzuen agertzeaz besterik. Hau gure ixtorio bat baita, gure Elizaberriko auzoan gertatua eta ixtorioa ez bada baitezpadakoa, bertsuak hala dira.