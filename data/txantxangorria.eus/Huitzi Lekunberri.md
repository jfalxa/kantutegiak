---
id: tx-2465
izenburua: Huitzi Lekunberri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WZsSI4Jld3E
---

Huitzi Lekunberri, Etxarri ta Aldatz
probe gertatu dire aurten lurrak babaz;
Arruitz hori bide da guzien burue
han izango omenda saltzeko modue.

Madoiz eta Odoritz, Astiz eta Mugiro
Jaungoikoak hoietan zerbait eman digu;
Barabar ta Iribes, goazen Aillire,
horietan eztue eraiteko diñe.

Errazkindik Albisu, Azpirotz Gorriti;
babak pasea libre kamioi berritik.
Hamazazpigarrena han dago Lezeta:
Ikazkin baterikan horietan ezta.

Baba zaharrik saltzeko inork baldin bahau,
anegak sei ezkutu egiten omendu
ez dut bada nik senti Larraunen bestetan,
Huitziko herrien hiru-lau etxean.