---
id: tx-3200
izenburua: Jaiki Jaiki -Gudari-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e6eIrr5iTZc
---

Jaiki, jaiki, euzkotarrak,
Laster dator eguna,
Laster dator eguna!
Sorkaldetik ageri da
Argi goxo biguna
Bere aurrean bildurtuta
Igesi doa iluna!