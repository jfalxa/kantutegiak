---
id: tx-2950
izenburua: Andre Madalen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v8KOr29mDRs
---

Hasi goiko kaletik 
eta beheraino, 
ez da ederragorik 
Madalentxo baino. 
Txikia naizelako
Ez nauzula utzi
Kapitaina izango naiz
Bihar edo etzi

Ai Madalen, Madalen! 
Ai Madalen! 
Zure bila nenbilen, 
orain baino lehen. (bis)

Andre Madalen, Madalen andre,
laurden erdi bat olio,
aitak saria ekarrizion
amak ordainduko dio.(bis)

Amak ordaintzen ez baldinbadio,
aitak ordainduko dio;
aitak ordaintzen ez baldinbadio,
hor konpon eta adio. (bis)