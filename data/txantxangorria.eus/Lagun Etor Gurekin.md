---
id: tx-591
izenburua: Lagun Etor Gurekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/27OfgF23SsE
---

Batez
mendi eta oihanetan
pentse eta alorretan bizi
nahi dugu gure etxetan.
Batez
laborarien auzia
ezin da irabazia nahi
norbaitekin ta nor ez jakin.
Biez
langile guzien mina
nausientzat gaxtagina senda
litakea gure indarrez.
Biez
nun ez denari gogorka
ezin da egin borroka nahi
norbaitekin ta nor ez jakin.
Lagun, etor gurekin, denok elgarrekin ba dugu zer egin.
Gu ez gira lotsatzen, etsaia nun hazten, han da gudukatzen.
Gure odolez ta ontasunez gure izaitez dirade gosez;
elgartasunez erakuts hortzak elgarri josiz buru-gorputzak.
Hiruez
besteen bizkar bizi dena
utzirik besta lehen lana mozten
ahal gera ari hegalak.
Hiruez
diruaren lege latza
ezin da nihundik kasa nahi
norbaitekin ta nor ez jakin.
Lauez
libertateko bidean
bide luze ta hertsian tinka
eta gogor elgar atxiki.
Lauez
itotzen ari den haurra
ezin da ekar bizira nahi
norbaitekin ta nor ez jakin.
Lagun etor gurekin...
Gure odolez...
Bostez
ardiz bezti den otsoa
barnea ustelezkoa argi
ikus nor den gure ondoan.
Bostez
barkua zuzen eraman
haizeari gogor eman nahi
norbaitekin ta nor ez jakin.

 Lagun etor gurekin...

Gure odolez... (bigarrengo zati hau bis)