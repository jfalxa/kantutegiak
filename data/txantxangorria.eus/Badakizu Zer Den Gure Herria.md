---
id: tx-3006
izenburua: Badakizu Zer Den Gure Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4JiDQ9znnXc
---

Gure herria lokatzeko mapa bat da?
Etsaiari sinetsi genion ipuina?
gure herria, harriak zeinek urrunago jaurti
jokoan ari diren haurrena da?

Gure herria ibiliala orkatiletan korapilatzen
zaizkigun kaleak dira?
gue herria topografia eta marketin
hipotesi oker pare bat da?

Gure herria itsaso gabeko balea da?
abuztuko elurtea?
helbiderik gabeko gutuna?
Hiztegi apurtu eta triste bat ez bai ez ez ez dioena?

Gure herria erditua izateko dago?
Erditze natural ala zesartarra?
eta zer egiten du ba gure herriak hor
geure herriaren zain?

Euskal Herria deitzen den lurralde lekutara
begira gelditzen da goizero,
planeta Lurretik urrunera bailitzen
bere izen propioa ere entelegatu ezinik.