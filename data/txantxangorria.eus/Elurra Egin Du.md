---
id: tx-470
izenburua: Elurra Egin Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CDsdIcwqLOY
---

Elur egin du gau huntan
Kanpoak ixil, xoriak biribil,
Beren lumazko jauntzitan,
Urtsumendi mantalin xuritan…

Elur egin du…

Hego haize urrietan,
Haitzak geroztik, ostoz biluzirik,
Hotzez zauden ikaretan,
Orai kapa xuri espaldetan…

Elur egin du…

Hurbil gaiten su xokorat
Egun goxoen, mila orroitzapen
Su bizian berotzerat.
Gure haur ametsen berpiztera,

Elur egin du…