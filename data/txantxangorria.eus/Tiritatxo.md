---
id: tx-1544
izenburua: Tiritatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bkWDXFLD9j8
---

Konposizioak, moldaerak zein programaketa: XABIER ZABALA

Kaixo, familia,
ongi-etorri
Tiritatxo naiz
Zuek zer berri?
Pupu eta Lore
jaio dira
zugana gatoz,
aurkeztera.

Bai ederrak bikiak
titizaleak biak
Jan ondoren, korroka
Txerritxoak orroka
Pixa-kakak zer moduz?
Puzkerrak ere erruz!
Zurrunga ozenik bai lotan?
Ametsak koloretan!

POZAK, TRISTURAK,
DENON ARTEAN
PARTEKATUTA BIZI GARA
IRRIEN LAGUNAK
EUSKAL IRRIAN,
EUSKARATIK ETA EUSKARAZ

Tiritatxo naiz,
zuen laguna;
opa dizuet
osasuna
Koloretako
tirita bat
opari daukat
nik zuentzat

Bihotzak taupa-taupa
Muxurik ez da falta
Birikak ere sendo.
re, mi, fa, sol, la, si, do!
Larruazala leun.
Laztanak gau ta egun.
Begi-ninietan distira
Irriz guri begira!

POZAK, TRISTURAK,
DENON ARTEAN
PARTEKATUTA BIZI GARA
IRRIEN LAGUNAK
EUSKAL IRRIAN,
EUSKARATIK ETA EUSKARAZ

Agur, familia,
laster arte!
Pupu eta Lore
sano daude.
Aio, Tirita,
Tiritatxo,
besarkada bat
ta bi patxo!

Tori! Tiritatxoa.
Pa eta PA