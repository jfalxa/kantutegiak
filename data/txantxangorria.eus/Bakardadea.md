---
id: tx-3045
izenburua: Bakardadea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/83lc7MQtiMk
---

Sinesten dut
egun batean hilen naizela
hutsean eroria
ezerezean amildu bat
hauts bihurtuko naizela
hauts bihurtuko naizela
Bainan lehenen
gure ametsa arrazoi garbiz
bi/hur/tu e/gin na/hi dut:
GIZON GUZTIOK
MAITA DITEZEN!
Gure Jaun ona lagundu, arren,
be/ti au/rre/ra jo/a/ten!