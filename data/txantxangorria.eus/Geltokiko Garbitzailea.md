---
id: tx-2611
izenburua: Geltokiko Garbitzailea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_gFj7CDmr90
---

20 urte igaro ditu geltokiko garbitzaileak
bidaiarien joan-etorriak zaintzen
ikusi ditu haurrak hankak luzatu eta neskei begira
amak bilobekin pasiran 

gazteak nagusitzen eta nagusiak zahartzen
maitaleen bikote aldaketak
dantzan balebiltzan
edonora bidaiatzen duten jubilatuak eta
edonondik datozen emigranteak

Berak ere, garbitzaileak, 3 seme izan ditu
2 kotxe, maitale bat, 4 ezkontza, 3 heriotza
gutxi gora behera 40 afari bere sozietatean
beste horrenbeste erratz esku artean

20 urte hauetan ikusi du neska gazte bat
beltzez jantzitako gizonaren begietan zerbait esan nahiean
eta zuri, laztana, 20 urte 6 segundutan kabitzen zaizkizu

lehenengoan begiradak
bigarrenean musuak
hirugarrenean maite nauzu
laugarrenean proiektuak
bostgarrenean eta seigarrenean ohe ondoan
agur esaten didazu, besarkatuta
egun haizetsu batean