---
id: tx-920
izenburua: Zatoz Gugaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GzKZv73wnZw
---

Argazkiak:
📷 @guillermourkijo
📷 @aitzolzl_photo
📷 @ikergm76
📷 @eideriturriaga
📷 @zuzenean_bagabiga
📷 @tx4rli

Argiak piztu dira eta hementxe berriz gara,
magia haizeak dakar, goazen!
Zuentzako eta zuekin, sortutako erritmoekin
izerditan galduko gara, goazen!
Musikak egiten gaitu,
musikarekin gara urtez urte, taupadaz taupada.
Zatoz gugaz! Singing with us,
bizi maita momentua.
Zatoz gugaz! Dancing with us,
hil artean bizi! hil artean dantza!
Bizi eta laga bizitzen, zeure itxuran betiko lez,
norabiderik ez dugu behar, goazen!
gorputzen mugimendua eta musika airean da,
izan gaitezen denok bat, goazen!
Zuek egin gaituzue,
zuen ondoan gara urtez urte, taupadaz taupada.
Zatoz gugaz! Singing with us,
…
Bizi dezagun gogotik une oro gaur gauean,
dantza gaitezen gogotik erritmoaren ertzean,
ilunak guk nahi baino goizago argituko du
eta egunsentiak bizitakoa lapurtu.
Zatoz gugaz! Singing with us,