---
id: tx-1422
izenburua: Mugitu Harriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SmEp9V4FlO0
---

Ahoko sabaian errotu diren hitzentzako
pentsamenduak eta begirada
urte luzeetan oihukatutako:
"Euskal Presoak Euskal Herrira" da

Ez
Ez dago harririk
mugi ezin denik
ezta Euskal Herririk galdurik
seme-alabarik
kartzela batean

utziko duenik
Erlojuaren orratzek diote etengabe
denbora gehiegi iragan dela
bidean harri handiak daudela
eta handik kentzeko garaiz gaudela

Ez
Ez dago harririk
mugi ezin denik
ezta Euskal Herririk galdurik
seme-alabarik
kartzela batean
utziko duenik

(utziko duenik)
Fresnesen (…) etxetik ehunka kilometrora
La Santé, (...) Puerto eta Navalcarnero

Alcalá, (...) Alacant edo Albolote…

Kartzelak (...) ixteko garaia da
Zulatu, apurtu, baztertu, kendu, mailukatu
harriak astunegiak baitira
zizelkatu, mugitu, edo tira

ekar ditzagun etxera, herrira
Ez
Ez dago harririk
mugi ezin denik
ezta Euskal Herririk galdurik
seme-alabarik
kartzela batean
utziko duenik.