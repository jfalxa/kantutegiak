---
id: tx-1070
izenburua: Beti Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IMnI6Zl7xd8
---

Beti maite maite degu (berriz)
Donostiarrak maite
Degu zortzikoa 
Donostiarrak maite maite
Degu zortzikoa 
Soñu beste lekuetan
Eztan modukua (berriz)
Kanta bihotz pozgarri
Euskal errikoa
Plazan soñu 
egoki denboretakoa (berriz)
Soñu polit politorik
Beti maite maite degu
Donostiarrak maite degu
(berriz)
Orren gatikan beti 
soñu politorik 
Donegiten degu 
mundu guztiari
Bihotzian toki bat
Merezi dezugu (berriz)
Beti maite maite degu
(berriz)
donostiarrak maite