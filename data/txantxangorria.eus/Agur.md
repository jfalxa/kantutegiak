---
id: tx-2509
izenburua: Agur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yylfyhU_tnU
---

Mutrikuko AME Estudioan grabatua eta nahastua Axular Arizmendiren eskutik.

Lauroba dira:
Iker Lauroba / Haritz Lauroba
Andoni Garcia / Ane Bastida / Antxon Sarasua





Marea biziak honaino ekarri gaitu
Ez ginen irten behar ontzi zahar batean

Liztorrak begietan eta bi malko gazi
Larrosak usainik ez du nahiz eta loratzen utzi

Gaur gauez berriro itzali nazazu
Sua ginen eta
Errautsak eta kea behintzat hor dauzkazu!

Hemen eta han arteko lekuren batean
Ikusiko dugu elkar

Baina hain da zaila muxu batekin agurtzea
Zure ondoan nahi duzun hura

Ezin jakin noiz bihurtzen den “da” zen
Iglu bat eraiki genuen
Nahiz hasi zitzaigun basamortuan urtzen

Hemen eta han arteko lekuren batean
Ikusiko dugu elkar

Milaka geroarte ditut,
Baina ezin esan agur...