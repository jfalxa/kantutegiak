---
id: tx-336
izenburua: Ipuinetan Bezala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iS_2qld8Gx8
---

*** P🟣PATIK bandaren kanta bat, Naizen elkartearentzat sortua ***
Musika: Xabier Crehuet
Hitzak: Aingeru Mayor eta Ekaitz Goikoetxea
Ahotsa eta kitarra: Xabier Crehuet
Haur ahotsa: Lucia
Irudiak Virginia Ogalla
Animazioa: Kiko Romero
Laguntzailea: Eusko Jaurlaritza. Berdintasun, Justizia eta Gizarte Politiketako Saila.
*** NAIZEN ***

✨✨✨✨✨✨✨✨
🐸 Ipuinetan ez bezala 🐸

– Seme, ze guapo zauden –
amak zizun esan...
–  Ama, apo ez, apa – 
erantzun zenezan.
Nahiz ta ez zenekien
guapa esaten,
neska zinela argi
jakin zenezakeen.

Argia ez dago ohiko
liburuen jiran,
nire lagun poetak
hala esan zidan:
apoak ez daudela
printzeei begira,
hemen apoak apa
bilakatzen dira.

Ai bihotzak ikusten
baldin bageneki!
apoak apa izan
direlako beti.
Begiek begirada
dutenez helburu,
transformazioa guk
egin behar dugu.

“Ama, orain arte pitilina ikusi
eta mutila nintzela uste zenuten, 
baina orain nire bihotza ikusten duzue 
eta badakizue neska naizela”

Begirune, onarpen
eta maitasunez,
ikusten zaitugula
ikusten duzunez,
zarenaren ispilu
eder eta lirain,
zoriontsu loratzen
ari zara orain.

Bazen behin, behin batean,
ta hala ez bazan,
dagoeneko ez daude
gure kalabazan.
Nahiz ipuinekin une
ederrak igaro,
bizitza bezalako
ipuinik ez dago.