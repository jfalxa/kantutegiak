---
id: tx-598
izenburua: Lepoan Hartu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ocIUjxgYF7Y
---

Trailara, trailara
La-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!
Gazte bat lurrean aurkitu dugu
Lore gorriz beterik kolkoa...
Burdinen artetik ihesi dator
Euskal gazteriaren oihua!
Mutilak, eskuak elkar gurutza!
Ekin ta bultza denok batera!
Bidean anaia erortzen bazaik,
Lepoan hartu ta segi aurrera!
Trailara, trailara
La-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!
Ez dugu beldurrik, ez dugu lotsarik
Nor geran, zer geran aitortzeko!
Ez gaituk lapurrak, ez eta zakurrak
Kataiaz loturik ibiltzeko!
Gizonak bagera, jo zagun aurrera,
Gure herriaren jabe egin arte!
Askatasunaren hegal azpian
Kabia egiten ohituak gare!
Ibiltzen aspaldi ikasia dugu,
Otsoak eskutik hartu gabe!
Trailara, trailara
La-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!
Arrotzek ezpataz hil nahi ba naute
Izituko gaituztelakoan,
Zutitu ta euskeraz mintzatuko naiz
Nere hiltzailearen aurrean!
Mutilak, ez gero nigarrik egin
Erortzen banaiz gau ilunean,
Izar berri bat piztutzera noa
Euskal Herriko zeru gainean...
Euskal Herriko zeru gainean...
Euskal Herriko zeru gainean...
La-la-ra
La-la-ra la-la la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!