---
id: tx-2804
izenburua: Iragan Eta Geroan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S9u_hf9_NVo
---

Iragana eta geroan
lehertzen den orain hau
heriotzaraino bizitzen
saiatu behar dut.

Ez nuke inoiz orain hau
behin bakarrik biziko dudala, inoiz
inoiz ahantzi nahi.

Heriotza belarrondoan
dantzudalarik biziko dut une hau
nere unea.

Azkenekoa balitz bezala biziko dut
lehenengoa banu bezala biziko.