---
id: tx-3050
izenburua: Izenez Eta Izanez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y0bShn1WovQ
---

"Otsaileko haize zakarrak
zekarren Puertotik
albiste txarra,
ziega ilun eta bakarti hartan
eman zenuen azken arnasa,
sasikumeen hitzetan
espetxean joan zinen,
egiak argi dauka,
kartzelak hil zintuen.

Arkaitz izenez eta izanez.
Agur eta ohore!

Baina eguna argituko da
zaitugula oroitzapenean.
Kantauri itsasoa
harro mintzo da
izar gorri berria
besarkatu nahian.
Gure ondorengoek
goratuko zaituzte
askatasunean.

Gauza handia izan da
zu ezagutu izana,
adiorik ez lagun,
garaipenerarte."

Abestiaren azken hitzak hartzen ditut, "adiorik ez lagun" garaipenaren egunean elkar ikusiko dugu. Zu eta beste hamaika izar gorri. Lakuan zuk botatako harriak sortu zituen olatuak bizirik daude oraindik..