---
id: tx-1207
izenburua: Maiteak Galdu Egin Zautan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FloV7xHe200
---

Maitiak galdegin zautan,
polita nintzanez (bis)
polit, polit nintzela baina,
larrua beltz, larrua beltz.

Maitiak galdegin zautan,
premu nintzanez (bis)
premu, premu nintzela baina,
etxerik ez, etxerik ez.

Maitiak galdegin zautan,
boltsa banuenez (bis)
boltsa, boltsa banuela baina,
dirurik ez, dirurik ez.

Maitiak galdegin zautan,
lana banuenez (bis)
lana, lana banuela baina,
gogorik ez, gogorik ez.

Gaixoa, hil behar dugu,
guk biok gosez (bis)
gosez, gosez hil behar baina,
elkar maitez, elkar maitez.