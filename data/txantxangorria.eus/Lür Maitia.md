---
id: tx-2348
izenburua: Lür Maitia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wGZ9POoqn8c
---

Euskal Herriko lür maitia,
Guretzat lekhu haitia!
Heben irus bizi girela
Plazer diigü erraitia,
Eta denek algarrekila
Kobla baten emaitia!

Eüskal jentia mündian hedatürik aspaldian,
Zunbat eztira hiri hoitan bizi tristüra handian ...
Heben aldiz ez othe gira untsa gure sorlekhian?
Langile ta laborari lotzen dirade lanari ...
Artzainak ere hor negian begiak so bortiari...
Arrantzalik iihaitz hegian kasü hede xuriari...

Gaztiak besta egile, ardiira karrikazale,
Astelehen goizan, lanilat ezin jeikiz aski zalhe ...
Bena neskegi gaietan, segür eztira logale!
Lanthare gazte goxua: hortan da gure gerua
Laketarazten balinbada Herrian gazte saldua,
Lehen bezala bihar ere biziko da Xiberua!