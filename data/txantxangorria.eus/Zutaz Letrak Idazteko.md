---
id: tx-106
izenburua: Zutaz Letrak Idazteko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iTIpJnhXHEA
---

'Zutaz letrak idazteko' abestiaren hitzak Jose Luis Otamendirenak dira.
Aitor Etxebarriak ekoitzi duen FANTASIA disko berriko kantuetako bat da.


ZUTAZ LETRAK IDAZTEKO
Eñaut Elorrieta/Jose Luis Otamendi
 
Nonbait utziko nituen 
zuri buruzko apunteak
paper zurbilen batzuetan
arrats euritsu batean
nonbait utziko nituen
 
Zutaz  letrak idazteko
 
Nonbait utziko zintudan
gaur ni maitatu ostean
nonbait utziko zintudan
arnasa hartzeaz ahaztuta
nonbait utziko zintudan 
 
Zutaz kantak egiteko
 
Nonbait utziko zidaten 
zelofanetan zoriona
haurrek irentsi zezaten 
hamaika eta erdiak jota
nonbait utziko zidaten
 
Zutaz  letrak idazteko
 
Nonbait utziko digute 
oraindik elkar maitatzen
amets hautsien gainean 
ergelki elkar maitatzen
nonbait utziko digute
 
Zutaz  letrak idazteko
zutaz kantak egiteko 
munduratu nintzen