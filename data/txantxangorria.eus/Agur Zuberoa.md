---
id: tx-2184
izenburua: Agur Zuberoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OPbfPpYnP_s
---

Etxahun Iruri, Xiberüako olerkariak. XIX. mendean egindako abestia. Abesti herrikoia bihurtuta. Oskorrik 6. PUB ibiltarian duen musika.