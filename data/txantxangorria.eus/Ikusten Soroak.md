---
id: tx-3115
izenburua: Ikusten Soroak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HR0sYdJuJLI
---

Txabi Villaverde eta Robles-Arangiz anaiek: Ugutz, Irkus eta Iker, 1963an argitaratu zuten diskoan sei abestietko bat hau zen, V. Youngen abesti baten bertsioa.