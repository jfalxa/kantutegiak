---
id: tx-2599
izenburua: Euskadi Castillan Bezala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1WL1DtktGDg
---

Ene Segurako aitonak
ogia behar zuenean
Castillara joaten zen
jornalaritzara,
morrontzara.

Bere igitaia trebeenetakoa omen zen
bai, hori bai…
Hamar haur zituen gosez etxean
bai!, hori bai!

Castillako jornalariek
ogia nahi dutenean
Goierrira joaten dira
jornalaritzara,
fabriketara.

Kanpokoak pizkorrak omen dira peontzan
bai, hori bai…
Hamaika haur badute gosez etxean
bai!, hori bai!

Castillako soroez eta Goierriko tximiniez
ene aitona eta kastellanoak
jabe bazeneza,
euren haurren haseak
ez lukete ihes gehiagorik ikusiko!
ez Castillan, ez Euskadin.