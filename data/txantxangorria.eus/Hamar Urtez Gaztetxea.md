---
id: tx-2638
izenburua: Hamar Urtez Gaztetxea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lGUUxQniJns
---

Zaldibiako Gaztetxearen 10. urteurreneko abestiaren bideoklipa, Gureran taldearen eskutik. Hemen duzue letra ere.

Pausoz pauso ta azpitik, itten delako herri bat
pausoz pauso ta behin ta berriz eroriz
Txindokin magalean, gazterian ohiua
hartu soka ta abordatu, Zaldibi!!

Urtez urte euskaraz, jai parekideen alde
falta direnak oroituz ta maitatzen
ametsen asanbladan, egin amets LIBRE
sentitu, gozatu eta zirikatzen!!

GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTETXEA, AUZOLANEAN HERRIA
GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTERIA, HAMAR URTEZ UTOPIA EGI BIHURTZEN!!

GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTETXEA, AUZOLANEAN HERRIA
GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTERIA, HAMAR URTEZ UTOPIA EGI BIHURTZEN!!

Eskuz esku ametsak egi bihurtu
iraultza txikiz zubiak eraiki
norberak bere eskuekin egina
bere sentitzen duelako beti!!

GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTETXEA, AUZOLANEAN HERRIA
GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTERIA, HAMAR URTEZ UTOPIA!!

GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTETXEA, AUZOLANEAN HERRIA
GAZTETXEA, HERRITIK HERRIARENTZAT
GAZTERIA, HAMAR URTEZ UTOPIA EGI BIHURTZEN!!