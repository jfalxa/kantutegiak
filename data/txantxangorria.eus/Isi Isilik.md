---
id: tx-1780
izenburua: Isi Isilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/abL57ShhaGQ
---

ixil ixilik dago, kaia barrenian
untzi xuri polit bat uraren gainean.



Goizeko ordu bietan esnatutzen gara
arrantzaleok beti joateko urrutira.

Pasatzen naizenian zure leihopetik
negarra irteten zait begi bietatik.



Zergatik, Zergatik, Zergatik,
Zergatik,...
Zergatik, negar egin?
Zeruan izarrak dagoz itsaso aldetik.

Zergatik, Zergatik, Zergatik,
Zergatik,...
Zergatik, negar egin?
Zeruan izarrak dagoz itsaso aldetik.

Bat, bat, bat, parrandan ibili
bart parrandan ibili bart.
Bi, bi, bi, ez naiz ondo ibili
ez naiz ondo ibili.
Hiru, hiru, hiru, kolkoa bete diru
kolkoa bete diru.
Lau, lau, lau, sardina bakalau.

Anteron txamarrotia,
Sinkerren bibotia
Haretxek ei dauka,
preso tximinoia.

Anteron txamarrotia,
Sinkerren bibotia
Haretxek ei dauka,

preso tximinoia.

Anteron txamarrotia,
Sinkerren bibotia
Haretxek ei dauka,

preso tximinoia.

Hau duk, hau duk
hau duk umorea
Kontsolatzeko Kontsolatzeko
Euskal/dun jendea

KALEAN GORA KALEAN BEHERA
KALEAN GORA ZEZENA AI AI AI AI

KALEAN GORA KALEAN BEHERA
KA/LEAN GORA ZEZENA

KALEAN GORA KALEAN BEHERA
KALEAN GORA KALEAN BEHERA...
KALEAN GORA KALEAN BEHERA...
KALEAN GORA ZEZENA

KALEAN GORA KALEAN BEHERA
KALEAN GORA KALEAN BEHERA...
KALEAN GORA KALEAN BEHERA...
KALEAN GORA ZEZENA