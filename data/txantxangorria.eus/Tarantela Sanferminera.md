---
id: tx-2240
izenburua: Tarantela Sanferminera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-7TdqHTIhCs
---

Gorka Benítez ( alboka, saxoa, zeharkako flauta)
Ricardo Boya eta Cesar Cisneros (produkzio eta nahasketa)



Ardo gorrixa, zurixa edo baltza
triste taberna barik
zezen gabe entzierruan 
zapi gorriarekin
Ardo gorrixa, zurixa edo baltza
triste musika barik
zezen gabe entzierruan 
baina gora San Fermin
Bilboko zazpi kaletan
ez da girorik
alde zaharran pintxo pote ai pobre de mi
punto com txosna gunean
kontzertuak zuzenean
eta Zeledon youtubean
kamarada tabernari, gaupasero, musikari
zeuentzako nabil kantari iii
Ardo gorrixa, zurixa edo baltza
triste taberna barik
zezen gabe entzierruan 
zapi gorriarekin
Ardo gorrixa, zurixa edo baltza
dantzan txaranga  barik
Do Re Mi musikari
zapi gorriarekin
Do Re Mi musikari
ai ai ai pobre de ti
San Miguel, San Pedro, San Juanak, Santa Kurtzak
Aste Nagusia, Pelaio ta Andramaixak
instagram ta kalimotxo
balkoian parrandan goxo
txosnetan ez dau kontzerturik
kamarada tabernari, adiskide musikari
zeuentzako nabil kantari iii
Ardo gorrixa, zurixa edo baltza
triste taberna barik
zezen gabe entzierruan 
zapi gorriarekin
Bi bi biralizatuz Mari Jaia
dantzan txaranga barik
Do Re Mi musikari
triste kontzertu barik
Jodete musikari
baina gora San Fermin