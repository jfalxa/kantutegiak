---
id: tx-2961
izenburua: Laguneri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TyWYC0UEmDI
---

Hitzak eta Musika: Patxi AMULET/ Xabi ETXEBERRI
                                      BEGIZ BEGI

Gure bideetan bada jendea itzalean

Segitzen, gidatzen gaituena

Hutsaz geritzatzen gaituena

 

Aingeruak bezala, izpirituak bezala

Egun batez denak ezagutu

Egun batez ditugu kurutzatu

 

Lagun deitzen ditugu

Adixkide anai arreba,

Odolez edo bihotzez

Haiek maite gaituzte

Beti hor izaten dira

Hurbil edo urrun

Kantu hau eskaina zaie

 

Gaur batzuk urrun dira

Kantu hau ez dute entzuten

Nahiz eta oihu egin

Ez dira sekulan etorriko

 

Bainan espero badakitela

Zuek bezala gaur

Azken abiatzeaz geroz

Ez ditut ahantziko