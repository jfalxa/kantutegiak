---
id: tx-2568
izenburua: Izan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C2s2jzaG4vU
---

Erakutsi al duzu bart gauez egina?
Irudien munduan nor izateko adina.
Zein eskematan sartzen da
gure kabitu ezina,
kabitu ezina

Besteen txaloen menpean biziz gero
normala da kostatzea,
zure bizitza ez dela horren interesgarria
onartzea

Gurpil zoro hontan segi ta segi
jarraitzea, noiz arte da zilegi?
Bereiztea, gertatzen da zailegi,
noiz den aski ta noiz den gehiegi

Sarean gaude, baina katean,
lotuta gaude, putzu batean,
sartuta gaude, bete-betean,
uneoro eskaparatean.

Gurpil zoro hontan segi ta segi
jarraitzea, noiz arte da zilegi?
Bereiztea, gertatzen da zailegi
noiz den aski ta noiz den gehiegi

Hitzak: Jon Gurrutxaga Urbieta