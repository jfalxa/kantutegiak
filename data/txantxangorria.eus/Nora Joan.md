---
id: tx-2959
izenburua: Nora Joan?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1xhFKgZEI1E
---

Hitzak eta Musika: Patxi AMULET/ Xabi ETXEBERRI
                                      BEGIZ BEGI
 
Sei hilabete zure menperatzeko,

Hiru zure ahantzeko

Orain ene bizira berriz jiten zira, 
Eni deusik ez erraiteko.

 
Beste eguzki batean

Aski berotasun atxeman nuen

Bainan arimak izozten zait

Beste baten besoetan ikusterakoan

 

Orduan bilatu, Orduan xekatu argia

Ta aintzinera goaz

Ene eskua hartu ta ibili ene ondoan

Gure bakera, Gure bakera

 

 

Orain bakarrik niz ibiliz munduan

Ez dakit nun ziren

Ene bihotzak xekatzen zaitu

Ene arimak argia xekatzen du

 

Otoi! Xekatu enekin,

gogapenik gabe!

Ez dakit zer pentsatzen duzun

Beraz otoi, otoi….

 

 

Orduan bilatu, Orduan xekatu argia

Ta aintzinera goaz

Ene eskua hartu ta ibili ene ondoan

Gure bakera, Gure bakera