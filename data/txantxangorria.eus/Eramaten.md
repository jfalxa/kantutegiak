---
id: tx-14
izenburua: Eramaten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zR83Cx-V9Xs
---

Liher - Eramaten

Zuzendaritza: Ane Berriotxoa
Ekoizpena: BADATOR
Aktorea: Sara Balerdi
Argazki zuzendaritza: Asier Marleza
Argazki finkoa: Iratxe Etxeandia
Zuzendari laguntzainea: Gerardo Tomé
Gaffer: Borja Herrán
Kolorea: Asier Toledo
Kamera laguntzailea: Iyan Altube
Jantziak: Beatriz Riobo
Makillajea eta ile apainketa: Hiart Olea
Bateria eta piano grabaketa: Garate Estudioa - Kaki Arkarazo
Guitarra, baxu eta ahots grabaketa: IN Estudioa - Iñigo Etxarri
Nahasketak: Santi Garcia - Ultramarinos Costa Brava
Masteringa: Victor Garcia - Ultramarinos Mastering

Hitzak:

Ez dezatela esan saiatu ez zinenik
Ez dezatela esan apurtu ez zinenik
zer dakite astun horiek norberaren matxinadez?
Mamuei tiraka gailurrik ez

Ez dezatela esan merezi ez duzunik
Buztana zapaltzen doktore zaren hori
Porrota planifikatzen, akatsak bikoitz ordaintzen
Probatu al duzu zeure burua barkatzen?

Eramaten, eramaten
urak dakarren guztia du gero
Eramaten, eramaten
urak dakarren guztia du gero

Ez dezatela esan apurtu ez ginenik
Ez, ez dezatela esan

Ez dezatela esan
gure bizitzan nola erratu
nor gorrotatu maitatzeko ahulegiak garenean

Zer dakite astun horiek norberaren matxinadez?
Probatu al duzu zeure burua barkatzen?

Eramaten, eramaten
urak dakarren guztia du gero
Eramaten, eramaten
urak dakarren guztia du gero