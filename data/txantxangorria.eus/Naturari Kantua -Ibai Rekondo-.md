---
id: tx-3139
izenburua: Naturari Kantua -Ibai Rekondo-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LIMKQRxi3hs
---

Nik Natura maite bait dut
bera osoa, bera osoa

Akaberaren kontra bait nago
bizitzaren alde bai oro.

Ehiztariak gizon hutsak baitira
berdin zaie amorio ta herio.

Gure munduan justizia eskatzen dute
eta mendietan dira hiltzaile.

Mendietan nagusitza ez erabil,
abereak hiltzeko.

Abereak Naturaren jabeak dira
armonia zaindu behar dugu.

Ehiztariak gizon hutsak baitira
berdin zaie amorio ta herio.

Akaberaren kontra bait nago
bizitzaren alde bai oro.

Natura errespetatu behar dugu
gi/zon zu/ze/nak na/hi ba/du/gu i/zan.

E/ta ho/lan bi/zi/ko bait ga/ra as/ke
bi/zi/tza/ri la/gun/tzen.

Men/di/e/tan na/gu/si/tza ez e/ra/bil,
a/be/re/ak hil/tze/ko.

E/ta ho/lan bi/zi/ko bait ga/ra as/ke
bi/zi/tza/ri la/gun/tzen.

Nik Na/tu/ra mai/te bait dut
be/ra o/so/a, be/ra o/so/a

A/ka/be/ra/ren kon/tra bait na/go
bi/zi/tza/ren al/de bai o/ro.


Kantagintzak "herriaren esnatzaile" izateko zeregina zuelakoan ekin zion Hibai Rekondok (Caracas, Venezuela, 1954) abesteari, 70eko hamarkadaren hastapenetan. Euskal Herriaren egoera politikoari estu loturiko bi disko argitaratu zituen, Aseari goitizenez, Bitoriano Gandiaga, Augustin Zubikarai eta beste euskal idazle batzuen poemei musika jarrita. Ondoren, El proceso de Burgos filmerako musika egin zuen, eta Mackfarland irlandarrarekin egindako disko batekin erretiratu zen.

Caracasen jaio zen Hibai Rekondo, Venezuelan, baina zortzi urte zituela, 1962an, Euskal Herrira etorri zen gurasoekin eta anai-arrebekin. Ama bizkaitarra zuen, aita gipuzkoarra. Seme-alabak Euskal Herrian hezteko irrikan zeuden, eta Zarautzen kokatu zen familia. Imanol Urbieta izan zuen Hibai Rekondok lehen maisu.

1970ean hasi zen kantari, eta Euskal Herriaren egoera politiko eta kulturalaz gero eta gehiago jabetu ahala, kantagintza herritartasuna aldarrikatzeko eta zapalkuntza eta bidegabekeria salatzeko tresna zela barneratu zuen. "Irrika nabarmenez entzuten digu herriak. (...) Jabetu egin behar du herriak bizi duen hil ala biziko kinka larriaz baina guardasola ahazten dugu, Herriaren esnatzaile izan nahi nuke", zioen 1972ko elkarrizketa batean (1). Lehen urte haietan Gipuzkoan eman zituen kontzertu gehienak, horietako bat baino gehiago Hernanin, baina Bizkaian -bizpahiru aldiz-, Gasteizen, Saran eta Zaragozan ere (Espainia) aritu zen. Imanol Larzabalek eraman zuen emanaldi horietako batzuetara. Artean Lourdes Iriondo, Xabier Lete, Mikel Laboa, Benito Lertxundi eta beste batzuen kantuak abesten zituen, baina Aseari goitizena hartuta kaleratutako lehen diskoan -Euskal Herria 1975 (Elkar, 1976)-, berak sortutakoak edota moldatutakoak ziren kantu gehienak. Bitoriano Gandiaga poetaren hainbat olerki musikatu zituen lan hartan. Hori zela eta, 1977ko azaroan Durangoko Liburu eta Disko Azokaren barruan Gandiagari egindako omenaldian parte hartu zuen Rekondok, "Euskaldun jaio", "Bakoitzak" eta "Baina aurrera beharbada" abestuz.

Euskal Herria 76-77: negu luze hotzetik bigarren diskoan (Elkar, 1977) bide beretik jo zuen Rekondok, baina orduan Aranduru, Gorriti, Pablo Neruda eta Augustin Zubikarairen hitzak musikatu zituen. Kantaldiak aldarrikapen politikoak egiteko erabiltzen ziren garaia zen, eta Hibai Rekondok horietariko askotan parte hartu zuen. Esaterako, Herrikoi Topaketetan abestu zuen, Lemoizko zentral nuklearraren aurka Bilbon egindako bileran.

Hurrengo urteetan zinemarako musikaren arloan lan egiteko aukera izan zuen, Imanol Uribe zuzendariaren eskutik. 1977an Rekondoren eta Artze anaien musika erabili zuen Uribek Ez film laburrean, eta bi urte geroago Rekondori agindu zion El proceso de Burgos filmeko musika sortzeko.

Euskal Herritik kanpora, Espainian (Valladolid, Sevilla...), Herrialde Katalanetan (Bartzelona, Valentzia...), Galizian, Belgikan, Frantzian edota Alemanian abestu zuen Rekondok. Halaber, Donostiako Schola Cantorum taldearekin Herbeheretako jaialdi batean parte hartu zuen, Irlanda, Korsika, Okzitania, Galizia, Flandria eta beste herri batzuetako ordezkariekin batera.

Urte batzuk geroago, azken lana argitaratu zuen: Amaiur, gaztelu baltza (Elkar, 1986), Mackfarland irlandarrarekin elkarlanean ondutako diskoa. Estepan Urkiaga Lauaxeta-ren poemak ("Iturri negarra", "Amaiur, gaztelu baltza"...), herri doinuak eta Rekondok zein MackFarlandek sortutakoak biltzen ditu diskoak.

Hala ere, 2009an itzuli zen disko berri batekin, Suaren desira. Euskal Herriaren egoera kontuan hartuta, Hibai Rekondok eduki filosofikoa eta transzendentea duten abestiak eskaintzeko beharra sentitu zuen. Haren hitzetan, "gaur egungo egoera politikoari eta sozialari aurre egiteko jakituria beharrezkoa da. Une honetan, egoera erabakigarrian eta itxaropentsuan gaude murgilduta, eta horrek gizakiaren kontzientzia aldaketa exijitzen du". Diskoa kaleratu arren, zuzenekoetara itzultzeko asmorik ez du Rekondok.   

Testua: Jon Eskisabel