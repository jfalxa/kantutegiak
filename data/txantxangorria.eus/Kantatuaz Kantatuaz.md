---
id: tx-85
izenburua: Kantatuaz Kantatuaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/piqFfwsMErU
---

Kantatuaz, kantatuaz zorion eta penak
Kantatuaz sortzen dira gure itxaropenak

Goizian goiz-argiari kantuz dabil txoria.
Udabarriz eguzkiak sortutzen dau loria.
Euskaldunen biotzari zor yako umoria
kantatuaz penak kentzen dator bertsolaria.

Kantatuaz, kantatuaz ...

Kaiolako txoritxua ez al dozu ikusi,
kantatuaz alaitzen da naiz-eta preso bizi.
Inguruan ikustian ainbeste injustizi,
kantatuaz joan nai dot mundu onen igesi.

Kantatuaz, kantatuaz ...

Kantatuaz, kantatuaz naiz-eta bizi preso,
kantatuaz, kantatuaz nor garan adirazo.

Errekatxuak alaitzen nau bere oiartzunez
itsasuaren deiari kantari erantzunez.
Gazteriak gizartian jokatzian sentzunez,
kantatuaz nasaitzen da naiz-eta iñor entzun ez.

Kantatuaz, kantatuaz ...

Kantatuaz, kantatuaz, nai-eta egurra jaso,
kantatuaz, kantatuaz nor garan adirazo.

Nora ezian gaur dabil euskaldun bidaztia
menderatu eziñian gau illun ta tristia.
Iretargi guzurtiak gure argi guztia,
kantatuaz sortu daigun egizko eguzkia.

Kantatuaz, kantatuaz ...

Kantatuaz, kantatuaz, nai-eta egurra jaso,
kantatuaz Erri zarra biztuko da akaso.

Kantatuaz, kantatuaz ...