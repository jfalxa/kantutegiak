---
id: tx-1861
izenburua: Imajinazazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yiA9x-1AO1s
---

Imagine peña guzti kalin jesarritte 
Besu besuai heldu ta fuerte eutsitte 
Euskaldun gehixa preso hartzen ez itxitte. 

Imagine EAJ Madrilli plantaten. 
Imagine Konsejeri "nehiku're" esaten. 
Imagine zipaiadi mosuk botaten! 

"Tipo hori jun gako" diñozu hainbaten, 
baiña hau be pasako da egunen baten. 

Imagine presu danak etxin herriz herri 
Españako gobiernu albora ixten gerri, 
eta errespetaten euskaldunon berbi. 

Diñozu "hau flipata da flipata ber bi", 
baiña ez da hainbeste falta ta eingo'u kriston juergi.