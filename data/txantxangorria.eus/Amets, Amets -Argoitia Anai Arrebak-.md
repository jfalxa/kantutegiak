---
id: tx-3159
izenburua: Amets, Amets -Argoitia Anai Arrebak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6GgooS913Jo
---

Amets, amets egin itsas gizona, 
amets, zure ametsa gerta leikela. 

Berriz etorriko zera Ondarru'ko kaiera, 
han daukazu maitea, itxaroten dezuna, 
berriz etorriko zera, zure txaluparekin, 
han daukazu maitea ta bihotza. 

Han daukazu maitea ta bihotza.