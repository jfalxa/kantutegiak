---
id: tx-76
izenburua: Aintzirako Soldaduak "Die Moorsaldaten"
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6Y_6C150CUw
---

Zutik euskal kantak @zutikeuskalkantak5674

AINTZIRAKO SOLDADU (Die Morrsoldaten euskaraz)
1930 an sortutako abestia Alemaniako Konzentrazio Esparru baten Eskertiar presoen artian.
Euskaratute: Ibon Ajuriagojeaskoa

Han ta hemen dena berdin,
ez dago aintzira baizik:
txori kantak poztu ezin
ta haritzak biluzik.

Pala bat daroagu
aintzirako soldadu 
orok (bis)

Zelai beldurgarrietan
kanpamentua sortu,
zorionik barik bertan
alanbreetan gotortu.

Pala bat daroagu…

Presoen ilarak goizez
aintzirara lanera,
pentsamenduan beti lez
bueltatzea etxera.

Pala bat daroagu…

Guardiak gora ta behera
ezin eskapaturik,
tirokatzen da hiltzera,
fuertea hesiturik.

Pala bat daroagu…

Ez gara kexu aldiro,
negua ez da beti be;
etxe maitea, barriro
izango zara neure.

Aintzirako soldadu,
pala barik gara 
gu joango (bis)