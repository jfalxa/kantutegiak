---
id: tx-3327
izenburua: Gogoak -Esne Beltza-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2MSCfMasY7M
---

Emaidazu berriro
ongi etorria
zu zara ni/re poz
guztien iturra
Emaidazu berriro
ongi etorria
gelditzeko nator eta!

Tren ba/te/an
gu/ru/tza/tzen di/ren
be/gi/ra/dak
ba/rra er/tze/an
e/ma/ten di/ren mu/xu/ak
lo au/rre/ko u/ne/ak a/mes/ten,
a/mes/ten
e/gu/nak jo/a/ten di/ra
hon/dar/tza ho/ri/an
e/gin/da/ko gaz/te/lu/ak
ez di/tuz/te o/la/tu/ek e/rai/tsi/ko
gu/re es/ku/e/kin
ma/rraz/tu/ta/ko lo/re/ak
ez di/tu den/bo/ra e/ba/ki/ko

E/mai/da/zu be/rri/ro
on/gi/e/to/rri/a
zu za/ra ni/re poz
guz/ti/en i/tu/rri/a
e/mai/da/zu be/rri/ro
on/gi/e/to/rri/a
gel/di/tze/ko na/tor e/ta!

lo/ak har/tu ba/ino le/hen
es/na/tu na/hi nu/ke
lo/ak har/tu ba/ino le/hen
a/mes/tu na/hi nu/ke
as/pal/di/ko ga/ra/ien
bi/de/an gal/du/rik
go/go/ra/tzen zai/tu/dan
be/za/la i/ku/si!!!

Emaidazu berriro
ongi etorria
zu zara ni/re poz
guztien iturra
Emaidazu berriro
ongi etorria
gelditzeko nator eta!

Emaidazu berriro
ongi etorria
zu zara ni/re poz
guztien iturra
Emaidazu berriro
ongi etorria
gelditzeko nator eta!


eta gauak badatoz arin
egunak joaten dira berdin
geltoki zaharretik
pasatzen ez den tren hori
ba/rra er/tze/an
biz/kar e/ma/ten el/ka/rri
lo/ak har/tu ba/ino le/hen
esnatu nahi nuke
loak hartu baino lehen
esnatu nahi nuke
aspaldiko garaien
bidean galdurik
gogoratzen zaitudan
bezala ikusi!!!