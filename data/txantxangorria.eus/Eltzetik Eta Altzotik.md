---
id: tx-1524
izenburua: Eltzetik Eta Altzotik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FRPSIngb0ls
---

Uuuuuu, hitz egizu, uuuuuu, mintza zaitez. 
 Uuuuuu, egizu berba, uuuuuu, geurea da... 
 
 Oroimena baino haratagotik, ahazmenaren zulotik, 
 ohartu gabe badatorkit... 
 
 Badatorkit eta banarama berekin, heldu mihitik eta goazen elkarrekin, 
 askoz gehiago zertarako jakin, hitz egin hitz egin hitz egin hitz egin. 
 Badatorkit eta banarama berekin, hor dator bere zaurizko irribarrearekin, 
 hor dator bere biharko begiekin, hitz egin hitz egin hitz egin hitz egin. 
 
 
 Eltzetik eta altzotik, altzotik eta atzotik, 
 atzotik eta etzitik, etzitik dator... 
 Eltzetik eta altzotik, atzotik eta etzitik, 
 etsitik eta atsotik, atsotik dator... 
 
 
 Atsoen eltze zahar hark oraindik irakin, mundu berri bat badakar berekin, 
 zabaldu elkarri leihoak eta ekin, hitz egin hitz egin hitz egin hitz egin. 
 Badatorkit eta banarama berekin, nik ere ikasi nahi dut eta jakin, 
 eta zer egingo dugu elkarrekin, hitz egin hitz egin hitz egin. 
 
 
 Eltzetik eta altzotik, altzotik eta atzotik, 
 atzotik eta etzitik, etzitik dator... 
 Eltzetik eta altzotik, atzotik eta etzitik, 
 etsitik eta atsotik, atsotik dator... 
 
 
 Hitz egin, hitz egin, hitz egin, 
 hitz egin, hitz egin, hitz egin, 
 hitz egin, hitz egin, bera ere mintza dedin... 
 
 Hitz egin, hitz egin, hitz egin, 
 hitz egin, hitz egin, hitz egin, 
 hitz egin, hitz egin, bera ere mintza dedin...