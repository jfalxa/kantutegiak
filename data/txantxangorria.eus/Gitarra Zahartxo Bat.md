---
id: tx-2690
izenburua: Gitarra Zahartxo Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rsqH9xf-Azk
---

Gitarra zahartxo bat da
neretzat laguna,
honela ibiltzen da
artista euskalduna; (bis)

egun batean pobre,
beste batez jauna,
kantari pasatzen det
nik beti eguna.

Nahiz dela Italia,
orobat Frantzia,
bietan bilatu det
anitz malizia. (bis)

Ikusten badet ere
nik mundu guzia
beti maitatuko det
Euskal Herria.

Jaunak ematen badit
neri osasuna
izango det oraindik
andregai bat ona. (bis)

Hemen badet frantsesa
interesaduna,
baina nik nahiago det
hutsik euskalduna.

Agur, Euskal Herria,
baina ez betiko;
bost edo sei urtean
ez det ikusiko. (bis)

Jaunari eskatzen diot
grazia emateko
nere lur maite hontan
bizia uzteko. (bis)