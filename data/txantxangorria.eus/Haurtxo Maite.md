---
id: tx-2019
izenburua: Haurtxo Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/clTEok3yxQM
---

Haurtxo maite, 
nola zaude? 
seaskan negarrez. 
Atoz, bai, nire bihotzera. 
Atoz, bai, lo lo egitera. 
Abesti maitalez. 
Abesti maitalez.