---
id: tx-2565
izenburua: Zuhaitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9eGmXAcwKEk
---

Hemen duzue Xabi San Sebastianen "Zuhaitza" kantuaren bideoklipa.
"Zuhaitzak ez du beldurrik" diskoan jasota dago kantua.
Bideokliparen egilea: Gorka Urra
Baxua: Matthieu Harambourek
Bateria: Josu Ervitik
Pianoa: Satxa Soriazuk
Gitarrak eta ahotsa: Xabi San Sebastianek


Zula ezazue, adarrok, sapaia. 
Zerua urratuz gora, gora. 
Gora trabarik gabe. 

Altxa burua, luza lepoa, 
ireki bularra, ireki. 
Irten bihotza hegan libreki.