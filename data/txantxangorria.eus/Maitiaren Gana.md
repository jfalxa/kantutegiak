---
id: tx-193
izenburua: Maitiaren Gana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x_61Rvzn0A8
---

Maitiaren gana goizian düt, nik, bi urhats egiten sorhua gaintik, Ene aitzinian lürra bustirik eta zerraillia lili delarik. 
Zuin aisa dütügün oro nahasten, deseginak nekez arra egiten, Ez ote dügia, gük, ahal heben, bizitze güzia irus igaiten? 
Zelü eta lürrak zuin argi diren, bihar eztakigü zer zaikün jinen, Egünko lanari gira lotüren’ta bake goxua arauz gozatüren! 
Hitzak: Junes CASENAVE-HARIGILE eta Roger IDIART Müsika : Jean ETCHART