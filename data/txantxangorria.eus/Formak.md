---
id: tx-1796
izenburua: Formak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6bIVGNvdinY
---

Formak onak dira
haietan galtzen ez zaren artean. (bis) 

Hogei urtez egon nintzen 
eskaparate batean, 
desafiozko begiraden aurrean. 

Segurtasu beharrez
hainbat 
forma saiatu nituen..., (bis)
jatorrizkoez ahaztu nintzen.

Kantari historikotzat omen naukate,
agian bitrinen bat dut itxaroten,
libra nadila horretatik gutxienez
eta urra nezatela
larros-arantzek azkenerarte.

Formak onak dira, bai,
baldin eta haietan 
ez bazara galtzen...
fometan ez bazara galtzen...