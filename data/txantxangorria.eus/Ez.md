---
id: tx-262
izenburua: Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JtvOZfcso2g
---

Zidazun (2021), gure iraupen luzeko lehenengo diska, entzungai plataforma guztietan. Urtzi Izak grabatu, nahastu eta masterizatua. Diseñua Estibaliz Gutierrezen, Candela Azpiroz eta Ángela Rodriguezen eskutik.


Hitzak:

Ni
Zuri adi
Ezin itxi
Gogoz aldarri

Gu
Duden lagun 
Gauan egun
Miña barrun 

Ez zaitez
Mesedez
Izkutatu aurrien
Zure beharrak
Edonora darozte 

Lo
Gauz eta gozo
Arnas hondo
Hotzan sutondo

Gatz
Begi behatz
Laztan garratz 
Bion ardatz

Ez zaitez
Mesedez
Ostondu nire aurrien
Zure  beharrak