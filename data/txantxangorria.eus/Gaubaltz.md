---
id: tx-2328
izenburua: Gaubaltz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vMHPm6RJZi8
---

Arima ahantziak argizagiaren bila, kalabaza ta sua haientzat betiko argia.

Guretzat ez goxorik,
Ahuntz esne ta gaztain erre, iluna luza ez dadin arrats lainotsuen menpe.

Gau beltza hasiera bukaera,
munduen arteko deiadarra.
Hildakoak etorri dira atarira, ur basoa bota dugu lehiotik behera, hey.

Dagda ta Morrigan larrutan ari dira, mosu hilkorrenak jainkoen ohatz gainian.

Eliztarrak izutzeko ase arte edango degu, lotsarik ez dugu izango mozorroa daukagu denok.

Gau beltza hasiera bukaera,
munduen arteko deiadarra.
Hildakoak etorri dira atarira, ur basoa bota dugu lehiotik behera, hey.