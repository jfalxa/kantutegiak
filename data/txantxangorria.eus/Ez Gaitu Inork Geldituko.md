---
id: tx-1672
izenburua: Ez Gaitu Inork Geldituko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t3pNc669RJk
---

Nork esan du,ezinezkoa da?
Nork onartu du,hori hala dela?
Zaila bada,ez da ezinezkoa,
zaila bada,badago lortzea
Eta eroriz ikasten bada oinez
has gaitezen bidea egiten.

Ez gaitu inork geldituko
Ez gaitu inork geldituko

Nork esan du, ez dago lortzerik?
Nork esan du, ez dut gaitasunik?

Zaila dena, zaila bada berez
guk ohi dugu zailado egiten
Eta ibiliz ikasten bada oinez
has gaitezen bidea egiten

Ez gaitu inork geldituko
Ez gaitu inork geldituko(BIS)

Bidairik luzeena,beti
pauso batez hasten da.

Ez gaitu inork geldituko
Ez gaitu inork geldituko(BIS)