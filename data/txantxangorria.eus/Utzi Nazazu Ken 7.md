---
id: tx-1559
izenburua: Utzi Nazazu Ken 7
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FxYANro6fxw
---

hiri ikustezin bat,
bada hiri urdin bat
niretzako;
ez galde niri noizdanik,
ez galde niri zergatik
bada hiri urdini bat
eta banoa

denbora katez urkatu nau
isiltasunean
eta besoetan hartu zaitut
haizeak erantzun dezan

utzi nazazu

korronte erauntsiek
eraman gintuzten
itzuliezinezko
lur urrunetara

malkoz eta musuz
maite izan zaitut,
zuk ondo dakizu

badakit barnean begiak ixtean
bada hiri urdin bat niri zain,
ez dakit noizdanik, ez dakit zergatik
badakit, oraindik
bada hiri urdin bat,
hiri ikusezin bat,
bada hiri urdin bat