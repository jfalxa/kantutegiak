---
id: tx-1836
izenburua: Baserriko Ate Zabalak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tMkeGUYQZE0
---

Baserriko ate zabalak irekiak eta prest daude
uztaileko belar-igarrak mantso doan orga
betetzen du
nabarra eta berde nahasi artean, argia jostari
eta hango sapaian, besatarak pilaka.
Hortxe naiz laguntzen, organ etzana iritsi 
naiz
hanka bat bestearen gainean nuela
dardara gozoak sumatu ditut
orga-hagatik jauziaz, pagotxa eta alpapa
bildu ditut
itzulka noa, jira-biraka,
eta belar idorra dut nere bilo nahasietan.