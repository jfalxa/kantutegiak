---
id: tx-171
izenburua: Gaiaz Eder Da Argizagia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iIfW8FZeswo
---

Gaiaz eder da argizagia, egünaz ere bai ekia, 
Haien pare da ene maitea, arren hain da xarmagarria, 
Igaran gaian, ametsetarik, botz bat entzün dit xarmantik, Eztitarzünez beterik beitzen, haren parerik ez beitzen. 
Xarmagarria lo ziradia, eztitarzünez betea ? 
Lo bazirade, iratzar zite, ez otoi üken herabe. 
Amodioa, gaiza erhoa, jentea trunpa liuna, 
Gaiak lo gabe, egünaz ere, errepausürik batere. 
Zure ganik orai partitzea üdüritzen zait hiltzea, Indazüt pott bat ene maitea, mentüraz date azkena ! 
Hitzak : herrikoak 
Musika : Jean ETCHART