---
id: tx-1804
izenburua: Pakean Utzi Arte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yw9PZ_L5gC8
---

Non bizi zen? Nondik zebilen? 
Noiz egin zituen etsaiak? 
Inoiz zoriontsua izan zen? 
Zer pentsatuko luke gutaz? 
Zerk ematen zion indarra?
Ez gehiago bueltarik eman
dakigu galtzen ditugunean
egun batzuk horrela dira
ahal dakigu gure heroen izenak
joan gaitezke haien hiletara.
Periodiko guztietan, lehen orrian
hilotzaren ondarrak karraxika
estatuari gerra, gerra beti
pakean utzi arte, utzi arte.
Pakean utzi arte, utzi arte.

Maitasuna ederrena ba da 
zergatik hortaz iluntzian 
askatasunarekin larrutan? 
Maitale arriskutsuegia da 
hortik ibiltzeko kontutan. 
Ez zeok bueltarik eman behar 
jazo behar dena jazo data 
ez da inor onik denentzat 
nork ez du maite askatasuna 
nahiz ta leher dedin esku artean? 
ezin zaitut begira gehiegi da 
ilotza karraxika etengabian 
estatuari gerra, gerra beti 
pakean utzi arte, utzi arte. 
Pakean utzi arte, utzi arte. 
Pakean utzi arte, pakean utzi arte.