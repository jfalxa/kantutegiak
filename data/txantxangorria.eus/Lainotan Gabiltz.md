---
id: tx-3017
izenburua: Lainotan Gabiltz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w1w5Gw0w0eI
---

laminak gara
la la la la la la la la la la la la
lainotan gabiltz
la la la la la la la la la la la la
lainotan gabiltz
la la la la la la la la la la la la
laminak gara

Dantzan gura dogu egin
Aratuste egunean
Beteko dogu gaba
Mundaka inguruan.

Dantzan gura dogu egin
Aratuste egunean
Beteko dogu gaba
Mundaka inguruan.

laminak gara
la la la la la la la la la la la la
lainotan gabiltz
la la la la la la la la la la la la
lainotan gabiltz
la la la la la la la la la la la la
laminak gara


Euskaldunak gara danok
herriko lagunak
Horregaitik gura dogu
danontzat bakea

Alkartu gaitezen danok
hemendik aurrera
Aratuste dal ta laminak
gu danok gara