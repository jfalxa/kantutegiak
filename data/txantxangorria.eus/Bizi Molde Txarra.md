---
id: tx-40
izenburua: Bizi Molde Txarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LuTkjlKxJH4
---

Astokeriak atetzen ! ba ba ez ba ba 
Nor nahi salatzen! ba ba ez ba ba 
Ogenak ez aitortzen! ba ba ez ba ba 
Harritürik etxekitzen! ba ba ez ba ba 
Zorrak doi tapatzen! Zorigaitzak bühürtzen! 
Noiztenka aharratzen et’aharrak mingartzen!

Bizi molde txarra! ba ba ez ba ba 
Bizi molde txarra! ba ba ez ba ba

Gose gabe jaten! (gose gabe jaten!) 
Et’arrunt loditzen! (mmm.. loditzen!) 
Mehatzeko pakatzen! (mmm.. pakatzen!) 
Eihartü ondoan eritzen! (mmm.. eritzen!) 
Behare txikokan aritzen! 
Emazteen gatik ertzotzen! 
Gogo txarrak bürüan sartzen! 
Hasper’egotxi behar zen! 
Ahazteko edaten! (mmm.. edaten!) 
Eta aldikal mozkortzen! (mmm.. mozkortzen!) 
Arrunt gaixkiak entzüten! (mmm.. entzüten!) 
Gor-mütü bezala jarten! (mmm.. jarten!) 
Deüsek ez nü axolatzen! 
Aizuer bihotz mina jauzten! 
Mündü oro arrenküratzen! 
Hargatik ez niz eküratzen! 
Hitzak eta müsika: Jean ETCHART