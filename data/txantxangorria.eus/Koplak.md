---
id: tx-2096
izenburua: Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Da3HFqWctF4
---

Lur nintzeen eta lurtuz noa
zugan bat egin arte,
zurekin behin betikoz
ezkontzen naizen arte.

Hasieran bat ginen eta
denborarikan ez zen
lainoak gure gainetik
bezala bizi ginen.

Maite ninduzulako
hemen nago bakarrik,
zerbait eman nahi zenidan
ta utzi nauzu hutsik.

Zuhaitzik gabeko baso
plai hondargabea,
zuen antzekoa dut
mundu hontan suertea.

Zeruan ez dago izarrik
aldegin du haizeak,
negarrez erori dira
basoko landareak.

Orain gaztea naiz eta
lehen berriz mutiko,
ihesi doan kea naiz
ezkutatuz betiko.

Debalde etorri nintzen
eta debalde noa,
dagoeneko prest daukat
hilerriko zuloa.

Kirieleison luzea,
kantu lehor zatarra,
eguzkia badoa ta
egin beza langarra.