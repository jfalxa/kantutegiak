---
id: tx-934
izenburua: Neu Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hdNzZKljzXQ
---

Jausten ginen eta berriz
arin altxatzen beti
Ez nuen espero inoiz
amaituko zenik

Oztopoei aurre eginez
anai bihurtu zena,
gaur bizkarra emanda
dabil mindu nahiean

Gaur bai
Argi geratu zaigu bederen
Gaur bai
Ni zuretzat ez naizela ezer

Neu naiz zure estalpeen
(Neu naiz) giltza eta beroa
Zure ariman tente egon nintzen zaindari
Neu naiz zure irria
(Neu naiz) berpizten zuena
Baina gupidarik ez zurekin jadanik

Ezbeharra gaindituta,
pozarekin batera
tentaldia etorri zen
zu eramatera

Erori zinen berehala
Deabruak daraman
gorrotoa ereiten
Hortan aritu zara!

Gaur bai…

Neu naiz zure estalpeen…

Solo

Gaur bai
Argi izan ezazu bederen
Gaur bai
Zu niretzat ez zarela ezer

Neu naiz zure estalpeen…
Eta orain ez dakit zergatik nauzun aurkaritzat
Baina gupidarik ez zurekin jadanik