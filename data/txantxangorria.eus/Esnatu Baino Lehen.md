---
id: tx-331
izenburua: Esnatu Baino Lehen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/54Ih3D_7nfI
---

Aldentzen diren hodei batera 

goratu nahiko nuke 

baita segundu batez.

Han garaieran 

olerki hilezkorren 

paradisu ezkuturik 

ziur badela.

Ta itzuli baino lehen 

hartu zurekin atseden,

jakin zer moduz zauden.

Berriz nauzu hemen

begiak, begiak,

isten.

Itxarongo zaitut 

esnatu baino lehen, 

esnatu baino lehen.

Naiz, zuregatik naiz.

Hemen, zuregatik hemen.

Katebegi behinena, lehena.

Unibertsoa gurutzatu zenuen  

Zein zaila egiten den, onartzea

Ta erreinurik ez badago?

Bilatuko zaitut ahanzturan,

edonon.

Berriz nauzu hemen

Begiak, Begiak

Isten

Itxarongo zaitut

Esnatu baino lehen                 

Gorantz!             

Gorantz!!

Ostadar batetik noa! 

            Gorantz!     

Gorantz

Hodei artetik banoa

Arauak hausten

Uhhhhhh

Uuuhhhh…

Berriz nauzu hemen. 

                  Lo dago.

Ez da askorik falta, 

                 esnatzeko.

Ez dut itzuli nahi, ez naiz joango. 

Besterik agurtzeko bazatoz, ez nago.

Itxarongo zaitut

esnatu baino lehen

Esnatu baino lehen!!

Esnatu baino lehen!!

Zurekin atseden! 

Elkar besarkatzen

Itxarongo zaitut 

esnatu baino lehen!