---
id: tx-2745
izenburua: Arraunean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WDZGfFH1LVU
---

Duela asko
Euri minarekin dagoen
Zubian, lotan geratu da
Itxaroten inoiz bete ez zaion ametsa
Beti itxaroten.

Kaleetan
Nagusi da iluntasuna 
Argirik ez,bidea aurkitzeko
Leku hartan,
Non,ezer ere, ez den egia
Geroan erantzuna.


Ez joan arren,banoa
Arraunean zure ondora
Zuregana dudanean
 Nirea izango zara bazterrean.


Korrontaren
Aurka borrokatuko duen
Marinela izango naiz
Haizealderantz
Hamar mila kilometrotara
Aurkitzen zara beti.