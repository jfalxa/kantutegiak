---
id: tx-3164
izenburua: Maitasunez Hil Antton Valverde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WcRMkEXgZMA
---

Hamairu urte nituanean,
oraindik galtza motxetan,
bere atzetik ibiltzen nintzen
herriko kale hotzetan;
prinzesa hura behar bezela
kortejatzeko lotsetan
bere etxera lagundu nahirik
eskolatik arratsetan.

"Oh, ez !", esaten zuen,
oraindik ez, gaztetxo naiz.
"Oh, ez !, inolaz ez,
etxera joan behar dut garaiz".

Hemezortzira ailegatuta
romantizismoz beterik
etzan posible eromen hura
bazter batera uzterik;
aingeru harek ez zidan sortzen
sufrimentua besterik,
sekulan ere ez nuen lortu
bi besoetan hartzerik.

"Oh ez !", esaten zidan,
"maitemintzeko gaztea naiz.
Oh, ez! , oraindik ez
lotsa ematen dit ta ez dut nahi".

Lotsaren lotsez pasa ta gero
dozenerdi bat eskutik,
pentsatu zuen komeni zela
sartzea bide hestutik;
eta nik nola oraindik ere
jarraitzen nuen gertutik,
ezkondutzia eskatu nion
ez bazitzaion inportik.

"Oh, ez! ", erantzun zidan,
"aspertzen nauzu, zoaz apaiz.
Oh, ez!, inolaz ez,
beste batekin ezkontzen naiz".

Festa batean ikusi nuen
handik zortzi bat urtera,
ta hotz-hotzean ausartu nintzen
sagar helduen hartzera,
irrifar doble makur batekin
izkutatu zen atzera:
ez zen ausartzen bere zezena
adarrez adornatzera.

"Oh, ez!, ez da posible,
horrelakorik ez nuke nahi.
Oh, ez! , inolaz ez,
ni etxekoandre jator bat naiz."

Mutilzartuta bizi nintzela
laugarren piso batian,
behin uste gabe topatu nuen
gure etxeko atian ...
bere onenak gastatutako
alarguna zen artian,
eta honela erantzun zidan,
oraingoz bion kaltian:

"Oh, ez! , orain ia ez,
nahiz eta zutaz pentsatu maiz.
Oh, ez!, inolaz ez,
oheratzeko zahartu naiz.". (bis)

(X. Lete / A. Valverde)