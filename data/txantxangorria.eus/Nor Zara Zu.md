---
id: tx-2554
izenburua: Nor Zara Zu?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gtFeQj484iw
---

Zu, zu, zu nor zara zu?
Ni, ni, ni Lierni naiz.
Neska alaia eta euskalduna naiz.
Zu, zu, zu nor zara zu?
Ni, ni, ni Andoni naiz.
Mutil bizkorra eta euskalduna naiz.