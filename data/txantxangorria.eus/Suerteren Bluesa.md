---
id: tx-1103
izenburua: Suerteren Bluesa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/u-d2oSB3BEc
---

HARKAITZ CANO

Suerteren bluesa

Bada musikari bat 
izena du Suerte, 
bere izena sarri 
entzungo zenuten 
nola ezaguna zen 
niretzat hainbeste
kontatuko dizuet 
Suerteren albiste. 

Emaztea korista
monja-koru baten
haurdun nork uzten zuen 
zaila zen jakiten, 
izpiritu santua
usoak laztantzen, 
sei alaba txikitxo
sehaskak betetzen.

Tabernako zuloa
duenak hautatzen
oholtzaren gozoa
ez du ezagutzen,
kezko kezken artean 
eztarri-urratzen,
bele batek kaiolan
adina gozatzen. 

Hilkutxan joko luke...
baina ez gitarra...  
Distira piztu zaio
eta irriparra,
kukurrukua ezpada
blues bat oso zaharra, 
gaua askatu arte 
ez moztu bizarra!

Musikari zintzoa, 
gaiztoen maitale,
karta jokalaria, 
horren da dotore,
larruaren eskeei 
emanez amore 
kultibatu zituen 
bizpahiru lore.  

Alkoholaren bermea
dauka txit ezagun, 
gozatu zuen eta, 
aitortu dezagun: 
onak izan zituen 
bizpahiru lagun; 
arratoi eskekoak 
ehun edo berrehun.

Nahiz hamaika kontzertu
kantatzera joan,
patrikan armiarmak
sakon eta loan:
txanpon gehiago ditu 
sartutzen zuloan
toka-jokalariak
igelen ahoan!

Ordu txikietan da 
nagusi ekaitza, 
barruan nahiz kanpoan 
eguraldi gaitza, 
biri bi erantsita 
lau ote emaitza? 
Lau whisky balirake
“istripu” da hitza. 

Pareko parabrisak
hipnotizatuta,
autoa geratu zen 
guztiz birrinduta,
eta bere hilotza 
multiplikatuta:
mila tantek besotan 
euriak hartuta. 

Hementxe bukatzen da
Suerteren ospea, 
blues batekin nahi nuke
hura omentzea, 
borondate horrekin 
kantu hau jartzea: 
Suerte ezagutzea
izan zen suertea.

"Astirtitan" (2014)