---
id: tx-1043
izenburua: Olentz Ero
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1Y-Gcv3PNAA
---

Olentzero juan zaigu
mendira lanera
in/entzio intentzioarekin
ikatz egitera.
Aditu duenean
Jesus jaio dela
lasterka etorri da
berri  ematera.

Horra! Horra! Horra!
Gure Olentz ero!
Pipa hortzetan duela
eserita dago
kapoiak ere baitu
arraultzatxuakin
bihar meriendatzeko
botila ardoakin.

kapoiak ere baitu
arraultzatxuakin
bihar meriendatzeko
botila ardoakin.

Ho/rra! Ho/rra! Ho/rra!
Gu/re O/lentz e/ro!
Pi/pa hor/tze/tan du/e/la
e/se/ri/ta da/go



O/len/tze/ro bu/ru haun/di/a
en/ten/di/men/tuz jan/tzi/a
bart a/rra/tse/an e/dan o/men du
ha/mar a/rru/ko za/gi/a
Ai! ur/de tri/pa haun/di/a
aur/pe/gi zi/ki/na
as/to/a/ren gai/ne/an
da/kar o/pa/ri/a

Ho/rra! Ho/rra! Ho/rra!
Gu/re O/lentz e/ro!
Pi/pa hor/tze/tan du/e/la
e/se/ri/ta da/go

ka/po/iak e/re ba/itu
a/rraul/tza/txu/a/kin
bi/har me/ri/en/da/tze/ko
bo/ti/la ar/do/a/kin.

ka/po/iak e/re ba/itu
a/rraul/tza/txu/a/kin
bi/har me/ri/en/da/tze/ko
bo/ti/la ar/do/a/kin.

Ho/rra! Ho/rra! Ho/rra!
Gu/re O/lentz e/ro!
Pi/pa hor/tze/tan du/e/la
e/se/ri/ta da/go

ka/po/iak e/re ba/itu
a/rraul/tza/txu/a/kin
bi/har me/ri/en/da/tze/ko
bo/ti/la ar/do/a/kin.

kapoiak ere baitu
arraultzatxua/kin
bihar meriendatzeko
botila ardoakin.

Ho/rra! Ho/rra! Ho/rra!
Gu/re O/lentz e/ro!
Pi/pa hor/tze/tan du/e/la
e/se/ri/ta da/go

Horra! Horra! Horra!
Gure Olentz ero!
Pipa hortzetan duela
Ai! Ai! ai!
eserita dago
Beeeeeeee