---
id: tx-2822
izenburua: Xarmagarri Bat Badit
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hxAGNRrBpM8
---

Xarmagarri bat badit maite bihotzeti,
Amodiotan gira biak alkharreki;
Haren aire xarmantaz agrada niz bethi,
Parerik badiala ezpeitzeit üdüri.
 
—Nulaz erraiten düzü maite naizüla ni?
Khorte egiten zaude zü beste orori.
Enezazüla tronpa, mintza zite klarki,
Esparantxa falsürik ez eman nihuri.
 
—Xori papogorriak eijerki khantatzen,
Gaiazko alojia kanpuan xerkhatzen;
Ni ere gisa berian nüzü edireiten,
Maitiak ezpadereit bortha idekiten.
 
—Orai zireia jiten, gaiherdi onduan?
Iratzarririk nintzan eta zü goguan;
Zure botza entzüten düdanin khanpuan, 
Ohetik jeiki eta jartzen niz leihuan.
 
—Oi! ene bihotzeko lili haitatia,
Ene botza entzünik leihuan zaudia?
Aspaldin enereizün ikhusi begia;
Barnerat sar nadin indazüt eskia.
 
—Bortürik gorenetan erorten elhürra:
Tronpatüren naizüla badizüt beldürra; 
Hargatik nahi züntüket ikhusi ardüra,
Eginen badüt ere, oi! ene malürra!