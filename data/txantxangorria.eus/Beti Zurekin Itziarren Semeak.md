---
id: tx-3178
izenburua: Beti Zurekin Itziarren Semeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PaM7QlcdAnE
---

Domeka arratsaldea da eta gaur bere
Gure Athletic olgetan deu San Mamesen
Jende guztia badabil inguruan 
Partidua ikusteko irrikitan

Baloia mugitu da zelai erdian 
Zuri-gorriz jantzitako hamaika gazteak
Zelai orlegiarekin egiten dute
Euskal Herriaren erakusgarria!

Aupa Athletic Athletic club! BETI ZUREKIN!
Aupa Athletic Athletic club! BETI ZUREKIN!

Talde honek badauka zerbait berezia
Ez da hobea ez txarrago baina ezberdina
Gauza guzti honek egiten dute
Athletic hain handi ta hain maitagarri!

Aupa Athletic Athletic club! BETI ZUREKIN!
Aupa Athletic Athletic club! BETI ZUREKIN!

Zale guztiak ohiuka atera dira
Alde zaharretik entzun da euren garrasia
Bardin da zer pasa den irabazi edo galdu
Atheltic beti da gure(tzat) txapeldun(a)!

Aupa Athletic Athletic club! BETI ZUREKIN!
Aupa Athletic Athletic club! BETI ZUREKIN!