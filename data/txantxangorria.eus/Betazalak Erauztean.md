---
id: tx-2150
izenburua: Betazalak Erauztean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0iaTDQtAfcs
---

Zortzi kidek parte hartu dute Katamalo proiektu
musikalean: Alex Arregi (gitarra eta ahotsa),
Gotzon Barandiaran (hitzak eta cuatroa), Itsaso
Etxebeste (baxua), Aintzina Lekue (biolina),
Gorka Mugarza (akordeoia), Jon Piris (kontrabaxua
diskoan), Gorka Urbizu (gitarra eta ahotsa)
eta Nerea Urbizu (pianoa, ahotsa eta kajoia).