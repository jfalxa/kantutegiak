---
id: tx-1157
izenburua: Alostorretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tG96ZseiRNE
---

Alostorrea bai, Alostorrea
Alostorreko zurubi luzea
Alostorrean nenguanean goruetan
bela beltzak kua, kua,
kua, kua leihoetan.