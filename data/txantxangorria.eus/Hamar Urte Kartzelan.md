---
id: tx-674
izenburua: Hamar Urte Kartzelan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p_nXHO80d9o
---

Kanpoan gauza asko ari da gertatzen,
Etsaia beti bezala herria zapaltzen.
Zuk, barruan jarraitzen duzu;
Hamar urte gartzelan daramatzazu.
Hamar urte gartzelan,
Denetatik pasatu duzu.
Hamar urte gartzelan,
Bakardadea ezagutu duzu.
Hamar urte gartzelan,
Herria bihotzean daukazu.
Hamar urte gartzelan,
Noiz bizi behar duzu?
Noiz bizi behar duzu?
Noiz bizi behar duzu?
Noiz bizi behar duzu?
Hamar urte gartzelan,
Legea zure aurka daukazu.
Hamar urte gartzelan,
Askatasuna lapurtu dizu.
Hamar urte gartzelan,
Herriak maitatzen zaitu.
Hamar urte gartzelan,
Gurekin noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Noiz egon behar duzu?
Hamar urte gartzelan,
Hamar urte gartzelan,
Hamar urte gartzelan,
Hamar urte gartzelan,
Hamar urte gartzelan!
Hamar urte gartzelan!
Hamar urte gartzelan,
Hamar urte gartzelan!