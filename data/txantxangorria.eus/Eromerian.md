---
id: tx-558
izenburua: Eromerian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LdyH8Cwd3h8
---

Hitzak: Joserra Gartzia
Musika: J. Tapia


EROMERIAN
«JUERGASMOAN»
TAPIA ETA LETURIA
ELKAR, 1990

 
Munduan beste asko legez
bizi gera bizi geranez
gu bezain ongi bizi denik
gutxi delako ustez.

 Ba dago ta bonbon
ez batego egon
ondorengoak hor konpon
Progresoak zer on
egin ote dion
naturari aspaldion.

Eromerian
Eromerian
bizi gara bizi
guk zer gura, hura uka
Eromerian
EromerianArin-arin arian

Hauteskundeak dira hemen
urtean lau aldiz bederen
aurretik denak irabazle
eta berdin ondoren

 Emazu botoa
ez izan tontoa
horrenbestekin nahikoa.
Izan praktikoa
demokratikoa
bota zazu betikoa

Eromerian
Eromerian
bizi gara bizi
guk zer gura, hura uka
Eromerian
Eromerian
zirko honen erdian

 Abaltzisketatik Moskura
hau da munduaren itxura:
erdiak beste erdiari
egiten dio burla
Gorbatxofek Bushi
milaka goraintzi
kendu ditzagun bi misil
Busek Gorbatxofi
ederra hago hi
kendu ezak hik lehenbizi

 Eromerian
Eromerian
bizi gara bizi
guk zer gura, hura uka
Erornerian
Eromerian
zu hemen eta ni han.