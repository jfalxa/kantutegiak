---
id: tx-2934
izenburua: Haizeder Eskolako Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BveRe16rO3w
---

Itsasoaren kresala
Ta mendiaren magala
Euskalduna mundiala
Haizeder da gure eskola

Bat, bi, hiru, lau,bost,sei, zazpi
Zortzi, bederatzi hamar
Ikasle gehi irakasle
Bider hamar atzamar
Gure herria marrazten
Gure herria margozten
Lehengoengandik ikasita
Etorkizuna janzten

Itsasoaren kresala
Ta mendiaren magala
Euskalduna mundiala
Haizeder da gure eskola

Herri txikiak bizirik
Txikiak herri bizi
Orainetik geroa
Gura dogu irabazi
Eskola da taupada
Herria bihotza bada
Taupa haizeder taupa
Gara ta izango gara

Itsasoaren kresala
Ta mendiaren magala
Euskalduna, mundiala
Haizeder da gure eskola