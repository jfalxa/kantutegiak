---
id: tx-2454
izenburua: Kale Ilunetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w8fpxKndcd4
---

Errotxapeako txoznetan egindako kontzertuan grabatutako bideoklipa.
MILAESKER JON ETA JONERI BIDEO GRABATU TA EDITATZEAGATIK!

LETRA:
Ehundaka gazte, kale ilunetan
ez dute aske izateko tokia
korrika ihesi, beti ahra ta ona
bajer aputa baten ez dutelako sartu nahi
zulo pozoindutan hiltzen baitira
borroka eta gure auzoko bizia!

Oztopoz oztopo, Ametsak eraikitzen
Okupatuz okupatu, Autogestioan
oztopoz oztopo ametsak eraikitzen
matxinatuz matxinatu autogesioan

Ehundaka gazte, kale ilunetan
ez dute aske izateko tokia
korrika ihesi, beti ahra ta ona
bajer aputa baten ez dutelako sartu nahi

Beste mundu bat posiblea da
burujabetza ta askatasuna
alternatibak sortzen dituen herriak
herri librea bihurtuko da.

kontaktua: