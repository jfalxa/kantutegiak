---
id: tx-1533
izenburua: Miñi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BCyt_TVfQjQ
---

Gauaren babesean
ilusioak nagusitzen eta parte hartzen duten une hortan
maitasunaren inguruan lotzen diren sentimenduak
adierazteko gai naiz.
Zuregana heltzen den bidearen erdian nago
nahiz eta asko kostatu,
ziur nago egunen batean helmugaraino
helduko naizela.

Maitasunaren argia
maitasunaren indarra
zu gabe itzaltzen da
sugarra, zu gabe dut nigarra.

Nere barnetik irtetzen zitzaidan indar batek
bultzatu nau, bultzatzen nau
zuganako atrakzioa erakusten.
Nere kontzientzia lasai dago
badakit zer nahi dudan.
Zuregana heltzen den bidearen erdian nago
nahiz eta asko kostatu
ziur nago egunen batean helmugaraino
iritsiko naizela.