---
id: tx-3361
izenburua: Eibar Futbol Taldearen Erreserkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KDYIB1OrHTg
---

Eibarko futbol taldeak 
apala bezain trebeak 
guztioi garaipena eskaintzeko 
borrokatuko du. 

Gure herriko jendeak 
betidanik du esaten 
Eibarrek ez duela sekula 
atzera egiten. 

Gorria eta urdina 
dira guretzat 
garaipenaren koloreak. 

Mutilen a(ha)legina 
animatzeko 
dira eibartarron txaloak. 

Pil-pil dago Ipurua 
garaipena lortu arte 
ez dugu etsiko. 

Eibarko futbol taldeak... 

Gure herriko jendeak... 

Gorputz eta arima 
talde ta zaletu 
eibartar denok 
aurrera goaz. 

Atzean sendo 
aurrean zorrotz 
hamaikakoa 
garaipenaren bila. 

Ipuruan zaletuok 
taldearekin bat eginda 
bultza dezagun 
beti Eibar. 

Eibarko futbol taldeak... 

Gure herriko jendeak... 

Guk, bihotz emango dugu 
Eibar beti aurrera!!!