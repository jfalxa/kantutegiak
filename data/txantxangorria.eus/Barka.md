---
id: tx-2308
izenburua: Barka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XlJe5VvQLaw
---

Haize urrunak hegaldian
gogora hurbilduz oroitzapenak
nahigabe eta pozak askaturik
goi aterbe isil baketan.

Barka nire ikusi ezinak
barka nire ondare gaitzak
barka jaurkitako min guztiak
barka nire gorrotoaren eragina.

Barka nire ikusi ezinak
barka nire ondare gaitzak
barka jaurkitako min guztiak
barka nire gorrotoaren isla.

Gogoak iluntzean
ezindurik
bakartasun malkoak isurtzean
goi urrunetako hegaldira
abiatuko naiz argi bila.


Barka nire ikusi ezinak
barka nire ondare gaitzak
barka jaurkitako min guztiak
barka nire gorrotoaren isla.