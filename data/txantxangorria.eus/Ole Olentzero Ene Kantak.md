---
id: tx-1607
izenburua: Ole Olentzero Ene Kantak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/36fxIc0paLs
---

Ole! Olentzero!
Ole! Olentzero!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!


Olentzero, buru handia
entendimentuz jantzia,
bart arratsean
edan omen du
hamar orruko zahagia
Olentzero, buru handia
entendimentuz jantzia,
bart arratsean
edan omen du
hamar orruko zahagia


Horra! Horra!
Horra! Horra!
gure Olentzero
Kapoiak ere baitu
arrautzatxoakin
Horra! Horra!
Horra! Horra!
gure olentzero
bihar merendatzeko
bo/ti/la ar/do/a/kin.


O/le O/len/tze/ro!
O/le O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!

O/le O/len/tze/ro!
O/len/tze/ro!
la la la/ra! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/len/tze/ro!
O/le! O/le!  O/le! O/le!
O/len/tze/ro!
O/le! O/len/tze/ro!
O/len/tze/ro!
O/le! O/len/tze/ro!

O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!

O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!
O/le! O/len/tze/ro!