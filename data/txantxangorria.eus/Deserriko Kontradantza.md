---
id: tx-1846
izenburua: Deserriko Kontradantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cbdEl7-cXjY
---

Ilargi beteko gau baten
isil-isilik gindoazen
mendizerran barrena
lagun nuela maitea

Ilargia betea bazen
beteago zen gure pena
ilargi beteko gau batez
mendizerran barrena

Barkatu dezatela gerrak
dena odoltzen duen gerrak
muga zeharkatu baino lehen
ilargi beteko gau baten
makurtu nintzen lurra bera
atsekabez musukatzera
barkatu nazala gerrak

Ene sorterrian utzi nuen
bizi erdia lokartua
beste erdia hartu nuen
ez jausteko abaildua

Orain Frantziako lurretan
bihar agian urrunago
ni ez naiz herriminez hilko
herriminez bizi iraun baino

Ene sorlekuko lurretan
hiru muino mendizerran
ta lau pinu baso itxi bat
bost lur sail lur gehiegizkoa
oi ene herriko lurretan
oh Valles!
ene sorlekua!

Itxaropen bat birrindua
oroimina infinitua
ta herri bat hain txikia ezen
amets bakarrean sartzen den
ilargi beteko gau baten

Itxaropen bat birrindua
oroimina infinitua
ta herri bat hain txikia ezen
amets bakarrean sartzen den
ilargi beteko gau baten

Eñaut Elorrietaren bertsio hau Pere Quart kataluniarraren "Corrandes d'exili" poeman oinarrituta dago, Espainiako Guda Zibilaren garaian idatzitako poema, eta hauxe da jatorrizko testua:

Una nit de lluna plena
tramuntàrem la carena
lentament, sense dir re.
Si la lluna feia el ple
també el féu la nostra pena.

L'estimada m'acompanya
de pell bruna i aire greu
(com una marededeu
que han trobat a la muntanya).

Perquè ens perdoni la guerra,
que l'ensagna, que l'esguerra,
abans de passar la ratlla,
m'ajec i beso la terra
i l'acarono amb l'espatlla.

A Catalunya deixí
el dia de ma partida
mitja vida condormida;
l'altra meitat vingué amb mi
per no deixar-me sens vida.

Avui en terres de França
i demà més lluny potser,
no em moriré d'enyorança
ans d'enyorança viuré.

En ma terra del Vallès
tres turons fan una serra,
quatre pins un bosc espès,
cinc quarteres massa terra.
"Com el Vallès no hi ha res".

Que els pins cenyeixin la cala,
l'ermita dalt del pujol;
i a la platja un tenderol
que bategui com una ala.

Una esperança desfeta,
una recança infinita.
I una pàtria tan petita
que la somio completa.