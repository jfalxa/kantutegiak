---
id: tx-1108
izenburua: Kantu Bat Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5NLtFsX3kFs
---

Kantu bat gara.

“Agian, agian egun batez jeikiko dira” 
hasten ziren Matalaz-en azken hitzak.

Gure izaitearen destino krudela eraman gintuzteen agian 
Zugarramurditik:

Baga biga higa, laga boga sega.

Agian Zarautztik: 
Ikimilikiliklik

Urepeleko artzainari 
“Non hago”-ka ari ginela 
erre zizkiguten titi puntak 
eta ipurtzuloak 
burdinez lehertu zizkiguten.

Ez dira betiko garai onenak oihukatzen genuela, 
herriko bat eraman zutenean 
berekin 
zeuden ondarrutarrak 
“Boga boga” kantatzen hasi omen ziren.

Bua!

Altzateko jauna begira genuela, 
hor zela nunbait, 
bere lurrera itzultzeko zai

Orbaizetako armaolako horma zaharrek dardar egin zuen 
Iratiko basoan.

Eta uroilarrak kantatu zigun 
“zaindu maite duzun hori, 
zaindu maite duzun hori”.

Bereterretxe ohetik atera zuten 
argi urdinez borda batetik bertzera zebiltzanak

Gau amaigabe bat baino latzago 
ta ehun urteak baino gehio ezagutu ditugu.

Gernikako arbola da bonbardeatua, 
euskaldunen artean guztiz maitatua

Eta Gernikako hondakinen artetik eraman zuten Lauaxeta.

“Goiz eder hontan erahil behar nabe” 
idatziko zuen ziegan fusilatua izatera atera 
aurretik.

Azken oihua, dana emon bihar jako maite dan askatasunari

Eta itsasontzi baten Euskal Herritik kanpora garamatzatela. 
Ez dakit nora.

Gure seme-alabak itsasertzean eduki ditugu 
aingira eta itsas trikuen esku mendean.

Agur Xiberua, adios gaixo etxen dena.

Zaplaztekoka zirti-zarta aurpegi masailetan bota zuten lurrera.

Ama! 
Oroitzen zaitudanean sukaldean 
egoten zara eta guri begira bi doverman beltz

Bilatzen eta suntsitzen.

Aitarik ez dut, ama hil zelarik .

Eta edozein herriko jaixetan, 
gaupasa egin genuen bitartean, 
hil zuten beste bat, 

lehengo batean, kalearen erdian. 
Hotel Monbar, 
campanadas a la muerte, campanades a 
morts.

Emaidazu eskua ta gertatu zeneko lekura eramango zaitut. 
Baina ez zaidazu galdetu 
gauza ilun guztien arrazoi gordea.

denbora aldakorrak atsedenikan gabe daraman bidea, 
daraman bidea.

Goizeko ordu bietan esnatutzen gera, 
erropaz eta maitasunez beteriko zakuarekin 
joateko urrutira. 
Gure maitea ixil-ixilik dago ziega barrenian.

Lilurarik ez.

Zazpi senideko famili batean arrotza zen gure aita.

Orain sei gara.
Un horrible sueño.

Oi ama Euskal Herri goxua.

Mi cuarto en llamas.

Hernani,1982.

Zai dago ama, zai aita 
milaka aldiz abestu dugu begiak lanbrotuta 
eta halere maite 
ditut maite gure bazterrak.

Maitemintzen jarraitu genuen edozein herriko jaixetan.

Bai, zin dagizut, izan zarela ene bizitzako onena

Txantxibiri txantxibiri jo dugu mendira hamaika manifestaziotan, 
Elorrixoko kalian.

Eta azken arnasa eman dugu itsasoari begira. 
Itsasoan urak haundi dire zatiturik 
gaudenontzat.

Euskara da gure territorio libre bakarra.

Telesforok esan zigun: 
zutitu eta euskaraz mintzatuko naiz 
nere hiltzailearen aurrean.

Mort pour la patrie, 
euskaraz baino ez dakiten hoiei, 
mort pour la patrie.

Gure abuelek etzekiten euskaraz, 
baina bazekiten Lauaxeta hil zuten 
esku berdinek hil 
zutela Federico Garcia Lorca.

Eta gu, vuestras nietas y nietos de Extremadura, 
gallegos, castellanos, maketos, 
mantxurrianos, 
Andalucía entera como Marinaleda. 
Gu, los txatxos de vuestros genes, 
Euskal 
Herrian euskaraz nahi dugu hitz eta jolas.

Bahía de Pasaia da gure Playa Girón, 
Compañeros poetas eta Bobby Sands 
eta irri 
egingo duten irlandar emakumeen haurrak… 
Gure mendekua ere badira.

Eta berriz min hau zeren eta batekots dirauen, 
ez gara gu asetuko. 
Batek loturik deino 
ez gara libre izango. 
Horregatik zutitu eta euskaraz mintzatuko naiz 
nire hiltzailearen aurrean.

Ta “iala iala ramala” dantzatzen dugunean, 
hotz amaigabe bat baino latzago datorkigu 
odolaren usaina, 
biolentziaren erresumena.

Ez, ez dugu nahi horrelako zibilizaziorik. 
Onar genezakeen gure egoera, 
txoriak kaiola 
bezala

Baina hegoak ebaki bagenizkio etzen gehiago txoria izango.

Beraz, arrano bihurtu ginenean, 
kaiola hautsita alde egingo genuen beldurrez 
libro 
ginela sinestarazi nahi ziguten. 
Baina luze gabe ohartu ginen 
hanka arkaitz bati lotu zigutela

kate motz eta astun batez.

Oh, gu hemen bidean galduak ibiltzeari utzi gabe 
esan zuen Txirritak

Larrosa baino ederragoko gure zazpi probintziyak: 
Lau Espainian preso daudenak, 
hiru
han ditu Frantziyak.

Gure ametsak, esperantza, egia, 
askatasuna, bakea, justizia.

Zenbat gera, lau, bat, hiru, bost, zazpi?

Zer egingo degu? Alkar jo? Alkar hil? 
Ez, hori ez!





Negua joan da ta:

Gora herria!



JON MAIA Belodromoan 2016-03-05