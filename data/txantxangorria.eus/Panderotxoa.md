---
id: tx-2529
izenburua: Panderotxoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jn__rS3-7os
---

KORRONTZIren PANDEROTXOA.
Andoaingo Olagain Txistulari Elkartea eta Alberto Agirre Abesbatzaren kontzertua, Ikastolako trikitilariak, Cristina Solano, Oinkari Dantza Taldeko Izaskun eta Aiora, Gitarra-jole, Teklatu eta abeslariekin.
Santa Krutz parrokia.


Lo egin zazu ene panderotxoa, gaua zurekin da, 
lo egin zazu ene panderotxoa iragana joana da.

Pandero zaharrak lo hartu zun laztoaren gainean, 
zoru gainean zurrutadak leiho baten azpian, 
handikan ikusten ditu neska-mutilen barreak, 
gaztetako egun joanak ta aspaldiko erromeriak.

Leia hotzak utzi zion aspaldiko doinua, 
haizeak eragindako kriskitiñen zarata, 
larruari laztanduz kolperik leunena, 
Mauriziaren kantu baten oihurik sakonena.