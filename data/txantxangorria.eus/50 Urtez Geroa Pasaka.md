---
id: tx-2274
izenburua: 50 Urtez Geroa Pasaka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N4X6z3U9YTc
---

Elkarrekin 
ta guztientzat zabalik 
daude ametsak eutsiko 
gaitun hesola 
da Fleming 
herri eskola 



Mende erdiz zenbat kolore jatorri 
eta balore ortzadar, 
berde zein more ... anitzak 
bagara hobe! 



Altxor diren mila 
une pasata 
Berroteita hamar urtean 
geroa pasaka 
elkarren eskutik 
bidea egin 
hemen denok berdin: 

ba al hator gurekin? (/beraz hator gurekin ) 
OIHANA IGUARAN 
(ikasle ohia eta bertsolaria)