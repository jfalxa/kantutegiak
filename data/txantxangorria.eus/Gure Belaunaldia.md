---
id: tx-2225
izenburua: Gure Belaunaldia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tN2AJsqtGJA
---

LARREGI taldearen 1.go bideo ofiziala. HITZAK / LETRA / LYRICS:

* Gure belaunaldia 

XX. mendearen amaieran 
heldu ginen gu mundu
inperfektu hontara

Liburuetan soilik ezagutu dugu
diktadura anker batek suposatu zuena

Ongi etorria zara, beldurrik gabe aurrera

Jolas egiteko kanikak kalera 
atera zituzten azken haurrak izan gara

Non gelditu ote da musika zintetan
altxor gisa genuen garai urruti hura?

Ongi etorria zara, beldurrik gabe aurrera...

Oraina eta geroaren arteko
zubia eraiki dugu
guk aurkitu izan genuen mundua baino 
zerbait hobea behar dugu

Komunikatzeko bide berrienak
zabaldu zizkigun teknologiaren giltzak
botoi bat klikatu ondoren
mundua parez-pare zabaltzen
omen zaigu logelan 

Gure belaunaldia da, erraztasunez josia

Borrokatu gabe nahi digun guztia
eskura jasotzeko izan gara heziak
aldarrikapen hitz ugari ahoan
bete gabe utzitako iraultzen aroan.

Gure belaunaldia da, errastasunez josia...