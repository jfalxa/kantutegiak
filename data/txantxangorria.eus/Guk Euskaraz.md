---
id: tx-2989
izenburua: Guk Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fG4iXYmBNwU
---

Guk euskaraz
Zuk, zergatik ez?

Euskara putzu sakon
eta ilun bat zen,
eta zuek denok
ur gazi bat
atera zenuzten
handik nekez

Orain zuen birtutez
zuen indarrez
euskara
itsaso urdin
eta zabal
bat izanen da
eta guria da.

Nik eta zuk
egin behar dugu
Euskal Herria
gure buru.