---
id: tx-1304
izenburua: Er
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M2ul1Ten4SA
---

IZAROren "eason" diskoko lehen aurrerapen kantaren bideoa. Sara Fantovak zuzendua. Lan taldean: Lola Errando, Júlia Estruga eta Markel Gonzalez. Ero hori izan arte erori.



Zenbat alditan esan ote diot
neure buruari egon hari geldi hor.
Zenbat alditan esan ote diot
neure buruari utzizan gitarra hor.

Baina ezin eutsi luzaroan,
beti agertzen zara azaroan,
ta gero diozu ez zarela sentitzen
errudun azkenaldiagatik.

Dena bildu nuen estu
sekula ez zintezen jausi zu.
Baina bildu nuen estuegi
ta azkenean erori nintzen ni.

Agian ni naiz errudun bakarra
negarrari ateratzen labarretan aparra.
Ta zure bizkarraren aurka nire bularrak
adarrik altuenean desio dudan sagarra.

Iparra zahartzen hasia da,
zaharra ni emea ta zu arra
izatearen botere harremana.
Iparra sartzen koordenada berrietan.

Dena bildu nuen estu
sekula ez zintezen jausi zu.
Baina bildu nuen estuegi
ta azkenean erori nintzen ni.

Ta azkenean ero hori nintzen ni.
Ta azkenean ero hori nintzen ni.

Sekula ez berriz erohori ni.
Sekula ez berriz erohori ni.