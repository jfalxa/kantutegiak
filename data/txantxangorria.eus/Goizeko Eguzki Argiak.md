---
id: tx-1358
izenburua: Goizeko Eguzki Argiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KWRXYB9KdZE
---

Goizeko eguzki argiak
agur egitean
esna da nere biotza
berakin batean.
 
Ixilpean sortu bazait
maitasun bero indarra
zerutik jetxitako
ontasun ederra.
 
Udaberriko loreak,
ongi etorriak!
Atozkit maiterik maite,
guztiz eder zale;
zurekin izatea Jaunak al lezake…
 
Beti zurekin, beti!
Itxaropen pozgarria,
orain ta geroko;
lore aize ta izarretan
alkar izateko.
 
Itxasoan lañua, mendian elurra…
Beste gabe bizi naiz.
Betiko zoruna
ilargiz betea,
datorkit eskura.