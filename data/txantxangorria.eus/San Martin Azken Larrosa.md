---
id: tx-3063
izenburua: San Martin Azken Larrosa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TYYmJyuAbHM
---

Audioa: Antton Jauregi (KARANTTON)

Ez da ilun, ez da beldurgarri
egun joanen itzala
oin galduek zapaldurik duten
bide xahar bat bezala.

Mila negu, mila udaberri
astindu ditu ekaitzak
banan-banan lurrean etzanaz
bizi aroen emaitzak.

Eta bihar, egunsentirako,
prest egongo da jantzia
ta ur-gainean isilik legoke
mugazainaren ontzia.
Zatoz, maite, ta azken larrosa
moz dezagun elkarrekin,
ez da bihar alferrik izango
udazkeneko San Martin.

Eta hala, poliki-poliki,
azken argia itzaliz,
etxe zahar maitagarri hartan
leiho guziak zabalik,
mugaz harat eramaten duen
bidean zehar ibiliz.