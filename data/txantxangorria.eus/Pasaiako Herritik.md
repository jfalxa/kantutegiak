---
id: tx-1710
izenburua: Pasaiako Herritik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JBmQ1m1iShA
---

Pasaiako herritik dator notizia
zezen bat izan dala jenioz bizia
kantatutzera nua bistan ikusia
alegratzeko triste dagoen guzia.

Santiago eguna Pasaian seinale
ailegatu eztanak egin beza galde
hasieran jedia zezenaren alde
azkenean etziran arrimatu zale.

Gure Pasaia beti sonatua dago
errapin gabe ezta urte bat igaro
urrengoan jarriko dituztela nago
barrera hestuago edo zezena handiago.

Torillotik atera zuten lehendabizi
bei zar bat gidaria bandera ta guzi
arrapatzen zuena purrukatzen hasi
azkenean kanpora plazatik igesi.

Hiru komertziante plazatik kanpoan
poxpolo ta barkillo labaina tratuan,
pareta egon arren hamar oin altuan
panparroi pasatu zen putzura saltuan.

Hoiek horrela eta gehiago ere bai
lurrian ziraldoka egin zuten lasai
ezkonberriak eta zenbait dama galai
zezenaren mendian etzuten jarri nahi.

Plazatik itsasora salta da igeri
adarrak besterikan etzuen ageri
bakerua begira bere zezenari
eserita jan ditu hiru libra ogi.