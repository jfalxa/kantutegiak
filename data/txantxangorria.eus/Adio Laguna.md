---
id: tx-1659
izenburua: Adio Laguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JSeYRLEUoKs
---

Adio laguna,
aski ezaguna,
nork bete behar du zuk utzi
diguzun hutsuna?

Inoiz ez naiz damu izango
laguna izana,
gogoan daukat irakatsi
izan zenidana.

Jatorrean jator,
ez zen beste inor,
garen artean sendo dirau
gurekin iraunkor

Bizi indarra daukat, gartsu,
zenaren gorantzan,
berriro elkar ikusiko
dugun esperantzan.