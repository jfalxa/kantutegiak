---
id: tx-1125
izenburua: Hauts
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9J0JbCe3me0
---

Hauts bihurtzen doaz azalak gauez gau 
Geruza ukigarrien holokaustoa 
Hauts dira ezpainak, begiak parez pare 

Hauts dira eskuak dantzalekuetan 
Hauts, masailak etxeko ateetan 
Hauts bihurtzen jaiotzetik irakasten digute 

Eta hauts izatera kondenatua banago 
nahiago urarekin nahastu eta lokatz 
bihurtu betirako zuen oinetan itsasteko 

Pigmalion nabari zuen Caronterekin