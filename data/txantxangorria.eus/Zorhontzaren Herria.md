---
id: tx-2239
izenburua: Zorhontzaren Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zpJoQ-Pnrck
---

Musika: Xabier Zabala
Hitzak: Andoni Egaña
Mikel Urdangarin, Onintze, Inun, Soraia Agirregoikoa eta Eider Etxeandia Zorhontzaren abestia aurkezten.





Zorhontzaren herria
gautxori berria
ikaragarria.
Zorhontzaren herria
gautxori berria
jaiek ekarria.

Ganoraz bizi
Karmenez hasi...
Santanatxuetara...
Zornotzarrak gara.
Nagusi, gazte zein ume
herrian islada.

Urgozo iturri zaharra,
marmitako, tanborrada.
Ligoteoa, sanoa,
gure hizkuntza euskara,
jaiak heldu jakuz eta
goazen danok bertoko
plazara!!!

Umorez beteta gure
txoko eta kale...
txistu-hots, Musika Banda
biak pozaren seinale.

Kuadrilak jaien oinarri
harreman(en) adierazle
betaurrekoak, moreak
ta samako zapia azule!