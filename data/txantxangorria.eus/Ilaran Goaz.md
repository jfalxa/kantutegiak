---
id: tx-2927
izenburua: Ilaran Goaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qJxkGb2VO_c
---

Aurretik doanak egiten duena,
aurretik doanak egiten duena,
zirkin eta oihuak eginez,
zirkin eta oihuak eginez,
neska-mutilak aurrera goaz,
neska-mutilak aurrera goaz.
Aurretik doanak egiten duena,
aurretik doanak egiten duena,
zirkin eta oihuak eginez,
zirkin eta oihuak eginez,
Aurrean dagoena atzera
bigarren dagoena aurrera.