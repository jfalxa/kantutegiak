---
id: tx-383
izenburua: Zorion Beti Besterena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zI7SA0hiVJ8
---

Maite zaitut, zorion
iñun etzeralako,
ezer etzeralako,
maite zaitut, zorion.

Amaitzen zeralako
edfozein gauerditan
kale baten ertzean
maite zaitut, zorion.

Keinu zeken batekin
azkar zoazelako
hertsi  ezin liteken
parentesis haundira,
zauritutako ezpainak
sarri dituzulako
zoritxar zeralako
maite zaitut, zorion.

Ezpain zauritu eta
begi lnabrotsuak,
akasozko keinu bat,
ezer ez gehiago.

Horrelaxe bakarrik
zorion gabe
maite zaitut zorion
beti ukatua.

Hitz soil bat zeralako
poetek asmatua
zorionik asmatzen
etzekitelako,
denona zeralako
ezer etzeralako
maite zaitut zorion
beti besterena.