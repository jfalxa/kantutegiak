---
id: tx-2331
izenburua: Nyc Man
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3WZofFyka9M
---

Arazoak izango dituzu 
ene bihotza apurtuz, 
nahigabe martxoko idus-etan hausten badidazu... 
Nahiago harira bazoaz, 
hau dena ez da beharrezkoa, 
New Yorkeko gizona naiz,
 neska, esan "joan!", ta nahiko da. 
New York City man,
 esan "zoaz!", ta nahiko da!! 
New Yorkeko gizona naiz,
 esan "joan!", ta nahiko da!! 
Zaila da gezurrak asmatzea, 
gero oroitu behar dira eta, zertarako? 
Ez dut nahi egon zurekin, 
nerekin nahi ez badezu egon, 
ni gizona naiz, neska, 
begiak kliskatu ta jada ez nago, maitea... 
New York City man, 
begiak kliskatu ta jada ez nago, laztana!! 
Brutus ongi mintzo arren, 
Zesar traditua izan zen, 
Lady Macbeth erotuta, Macbeth hila, ordea. 
Ofelia eta Desdemona 
hiltzen dute Hamlet, textu utzita. 
Hala ere, ni ez naiz lider itsu bat,
 esan "zoaz", ta jada ez nago, maitea!!
Izarrez begiak itxi dituzte gogor, 
lurrea bidean maiko maiko, 
erreinua bizkarrean duen zaldun beltzak, 
zaldi txuri batera igo. 
Tximeletaz betetako erlojuak, 
milaka sits askatu, esan "utzi!"
eta ez nago, ez naiz arrenguratuko... 
Ez dut phone, fax edo malko, 
ezberdina da txarra edo okerrago. 
New Yorkeko gizona naiz, esan "joan!",
 ta nahiko da!! 
New York City man, esan "joan!" 
eta nahiko da!! 
New York City, how I love you! 
New York City, maite zaitut!!
..............................................................................
 Gari euskal kantariak Lou Reed jaunaren kantu bat euskaratzen, "Lou Reed, mila esker" bildumatik. 2014. urtean, Jonan Ordorika jaunak bultzaturiko omenaldi ederra da.

Gari, ahotsa ta gitarra 
Fernando Saunders, baxua ta programazioa 
Txus Aranburu, Rhodes teklatua 
Dalibor Mraz, bateria