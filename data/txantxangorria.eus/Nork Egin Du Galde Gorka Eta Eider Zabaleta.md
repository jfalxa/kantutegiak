---
id: tx-3030
izenburua: Nork Egin Du Galde Gorka Eta Eider Zabaleta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SRP8Vhnn5PQ
---

Nork egin du gure galde?

Aiako Harritan gaude,

baña beti bezin trebe…

lanean gogoz aritzen gera

soldatarik jaso gabe,

berrehun nagusi badaude

gure artaldean jabe…

Guk umore ona halare!

–II–

Hotz-aldi eta goseak

ta barau egun luzeak

guk baditugu paseak;

gorputzari on egiten nunbait!

gailur hontako haizeak

nola baigeran gazteak,

sano dauzkagu hestek,

ez egon arren beteak.

–III–

Goizean ekin lanari,

egunsentian pozgarri,

txoriak guziak kantari;

ezin atera gentzake orain

inoiz adinbat izardi,

erdietan gaude geldi,

ahal degunean exeri…

zertan nekatu gehiegi?

–IV–

Oiartzungo neskatilak,

muxu-gorri biribilak,

izan zaitezte abilak;

zertan maitatzen dituzute

kaleko ume zirtzilak,

Aitxulei’n daude trankilak

saltzeko dauden opilak,

pare gabeko mutilak.

Zekien modurik onenean, bertsotan azaldu zituen Iñaki Eizmendi Basarri-k esklabo lanetan bizi izandakoak. Gerrara gudari joan eta ezagutu zituen beste egoera asko bezalaxe. Basarri abertzalea zen, izan ere,nabarmen utzia zeukan altxamendu aurreko kultur ekitaldietan eta prentsa abertzalean idazten zituenetan.

Basarri ezaguna zen herrian eta herritik kanpo. 1935eko Bertso Guduan txapela jantzi zuenean, are gehiago. Urtarrilaren 20an izan zen. 21 urte zituen eta ahoa bete hortz utzi zituen entzuleak bere jardunarekin Donostiako Kursaalen. Txapeldun egin eta aste batzuetara omenaldia egin zioten Zarauzko batzokian. 1913ko azaroaren 27an Errezilgo Granada baserrian jaioa zena zarauztartua baitzen ordurako