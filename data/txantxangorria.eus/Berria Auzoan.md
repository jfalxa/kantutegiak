---
id: tx-294
izenburua: Berria Auzoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EfXEsRuaE8I
---

Iker Lauroba & Beste Urtaroak - Berria auzoan

Audioa eta bideoa Lezoti estudioan grabatu ditugu 2020 urtean.

Etorri nirekin,  
maitea zaitut oso:  
berria naiz auzoan. 
Zure lepotik 
nahi nuke goxo-goxo,  
bidean biok alai. 
Auzo honetan, –paradisuan–, 
ez dakit mugitzen:
taupadaka, metronomo bat,  
galeraz  galera. 

Etorri nirekin, 
nik ez dakit bidea: 
berria naiz auzoan. 
Kale berriak 
dira hauek niretzat, 
erakutsi nondik den. 
Itsu nabil, makilik gabe,  
bazterrak kolpatzen. 
Itsu nabil, gidari gabe, 
esku baten bila. 

Zenbat zubi zeharkatuta 
iritsiko naiz 
behingoz, zugana? 
Zenbat zubi erre behar ditut
paradisurantz, zenbat, 
iluna gainditzeko? 
 
Zenbat zubi  
dinamitaz lehertu
beste inor ez heltzeko,  
zugana? 
Zenbat zubi, 
urik ez badator
nire zapaten petik 
batere?  
Zenbat zubi  
zeharkatuta
jiratuko zara 
behingoz, nigana? 
Zenbat zubi,  
hasi aurretik 
baldin banago jada  
zurekin  
zorretan
blai?

(Harkaitz Cano)