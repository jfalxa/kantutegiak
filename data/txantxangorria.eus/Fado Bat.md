---
id: tx-1493
izenburua: Fado Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qt3szkBquB4
---

Ez ginen ametsetan bat egin
bide bera jorratu eta.

Ez duzu sekula ezagutuko
denboraz bestaldera
bilakatzen zabiltzana.

Amets, egia, ahotsa eta ixila
batera armonia eta muga
hutsa, bazterra, kontaezinezko
soslaia nauzu...

Egun batzutan ixiltasunak ere
desafinatzen du
noizbehinka ixiltasunak ere
desafinatzen du.

Gogoz edan nuen presentea
amets posibleak deseginez
ezinezkoak gauez nirekin
hilak agertzen zirelarik.

Amets, egia, ahotsa eta ixila
batera armonia eta muga
hutsa, bazterra, kontaezinezko
soslaia nauzu...