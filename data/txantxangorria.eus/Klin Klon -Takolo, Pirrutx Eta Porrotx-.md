---
id: tx-3219
izenburua: Klin Klon -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_EI9cL2X6eA
---

Rin! Ran! Rin! Ran! Ron!
Txirrinaz klin! klon!
Rin! Ran! Rin! Ran! Ron!
bizikletaz nabil hor.

Aldapa gora!
Aldapa behera!
Eskuak zabalik
eta libre sentitu nahi

Aldapa gora!
Aldapa behera!
Ileak astinduz
nihaize bihurtu nahi.