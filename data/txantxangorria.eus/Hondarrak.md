---
id: tx-995
izenburua: Hondarrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/doZcwqkxVpM
---

Irudiak: Gaizka Peñafiel eta Asier Olazar
Hitzak: Felix Zubia
Musika: Julen Alonso
Ahotsa: Eneritz Aulestia
Perkusioa: Iker Telleria
Bajua: Joanes Ederra
Gitarra: Ander Ederra

Pertsonak hondar aleak
mundua den hondartzan
batzuetan geldi daude
beste askotan martxan
ez uste ezer garenik
ez ibili zalantzan
patua den korronteak
garabiltza dantzan.

Hondarrezko erloju bat da bizitzan, dena.
Jakin ale hau izan litekeena, pena, azkena.

Maitea elkartu ginen
ur askoren tartean
pozik olatu handiak
heldu diren artean
korronteak jarri gaitu
elkarren apartean
aro berri bat hasteko
txokoren batean.

Hondarrezko erloju bat....