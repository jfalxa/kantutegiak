---
id: tx-2991
izenburua: Argia Egin Bedi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GhCAZckSo9g
---

Kontzienteki hezitako gizaki inkonstzienteak
ahalmen kritiko gabeko pieza arduragabeak
Izaera ta sinismen prefabrikatuak
zoriontasun faltzu honetako datuak
askatasun ametsak bizitza erosoan
kondenatuen rutinak dirauen bitartean
zuzendaritzapeko antzerki mundiala
artifizialitatez antzeztutako lana

Gure patua zerua dela
sinistarazi nahi digute
desinformazio bonbardaketak
baketasun bortitz baten baitan

Kontzienteki hezitako gizaki inkonstzienteak
ahalmen kritiko gabeko pieza arduragabeak
gezurra mila bider kontatu digute
ondorio larriak izatera heldu arte

Zuen etikari sua!
Urteak igaro dituzue ardiak hezten
ahotsik gabeko pertsonak marrazten
helburu ilunera garaman bidea argitzen
gure burmuinak zuen masus tirokatzen
sumisioari pan-pan!
telebistari pan-pan!
zuen moralari pan-pan!
zuen etikari pan-pan!
argia egin bedi