---
id: tx-2940
izenburua: Zinuka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p5Yl5dgi_jQ
---

Kopeta gainean gaileta bat jarri,

zinu-zinuka ahora ekarri.

Eskuak atzean, ez erabili,

gaileta jaten duenak, hark irabazi.