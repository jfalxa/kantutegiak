---
id: tx-2563
izenburua: Gabonak Gabon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JonJZZaMz_w
---

Gabonak gabon,
Jesus haurtxoa
sehaskatxoan, 
Gabonak gabon.
Gabonak gabon,
Jose eta Mari
kanta kantari
Gabonak gabon.
Gabonak gabon,
gora gorago,
izar bat dago
Gabonak gabon.
Gabonak gabon
Serio jator
Asto bat dator
Gabonak gabon.