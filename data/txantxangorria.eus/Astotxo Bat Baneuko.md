---
id: tx-1925
izenburua: Astotxo Bat Baneuko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wcdPd9i8VeA
---

Astotxo bat baneuko 
bustan zuriegaz
Ermura joateko 
gona gorriegaz

Txirikitin txainia
biar domekea,
txokolatez beterik 
baneuko tripea

Arrabako landea
balego Gorbeien
antxe egingo geunke 
zirikin zakien

Bat bi iru lau
balego eta balitz,
bost sei zazpi
alkarrekin dabiltz