---
id: tx-2944
izenburua: Dilin Dilinka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n0u9xGAfmaw
---

Dilin-dilinka, balantzaka,

dilin-dalan, ziburuan.

Hankak aurrera, zoaz zerura,

hankak atzera (e)ta popa gora.