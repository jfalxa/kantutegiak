---
id: tx-515
izenburua: Haizederren Lehen Hezkuntza Irailerako!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PWQFB6RmHsY
---

Gure herrietan bizi nahi dogulako; gure herriak bizirik nahi doguzalako; eskola txikiak be euskal hezkuntza sistemaren oinarri izan behar diralako... Haizederren Lehen Hezkuntza irailerako!

Eskerrik asko zelan halan kanta eta bideo hau osotzeko auzolanean ibili zarien eatar, elantxobetar, ibarrangelutar eta ingurukoeri. Mile esker Haizeder handitzeko aldarrikapenagaz bat egin dozuen guztiori. Zuon arnasagaz lortuko dogu!




Hitzak:

Isil-isilik dago
gure barrenean,
egonezin handi bat
eskolaren ganean.

Goizeko zazpietan
esnatutzen dira,
sei urteko umeak
joateko urrutira.

Pasaten nazenean
goizez geltokitik,
negarrak urteten dost
begi bietatik.

Zergatik, zergatik,
zergatik, zergatik?
Zergatik ba ez egin
Haizederren Lehen Hezkuntza
aurton, irailetik.




Fernando Goenaga Iribarren "Isil-isilik" kantaren bertsino librea.