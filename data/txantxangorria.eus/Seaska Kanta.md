---
id: tx-614
izenburua: Seaska Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z5lOzI6Uc_Q
---

Amaren bularrra
Haurraren janari
Haurraren negarra
Beti hunkigarri;
Baratz honen erdian
Goseak nago ni
Zure bular laztanen
Antsiz ta egarriz
Zure buar politak
Zerrien jostailu,
Edertasunak kontuz
Gorderik behar du;
Zaude isilik haurra
Ez egin negarrik
Munduak ez du-eta
Malkoen beharrik
Haunditzen zeranean
Ikusiko duzu
Isilik egoetak
Zenbat balio dun;
Jarrai zure bidetik
Hobe da horrela
Gauza denen gainetik
Haizea bezala
Zaude ixilik haurra
Ez egin negarrik
Munduak ez du eta
Malkoen beharrik
Haunditzen zeranean
Ikusiko duzu
Ixilik egoteak
Zenbat balio du
Segi zure bidetik
Hobe da horrela
Gauza denen gainetik
Haizea bezela
Amaren bularrra
Haurraren janari
Haurraren negarra
Beti hunkigarri;
Baratz honen erdian
Goseak nago ni
Zure bular laztanen
Antsiz ta egarriz
Zure buar politak
Zerrien jostailu,
Edertasunak kontuz
Gorderik behar du;
Zaude isilik haurra
Ez egin negarrik
Munduak ez du-eta
Malkoen beharrik
Haunditzen zeranean
Ikusiko duzu
Isilik egoetak
Zenbat balio dun;
Jarrai zure bidetik
Hobe da horrela
Gauza denen gainetik
Haizea bezala
Amaren bularra
Haurraren janari,
Ateroe samur hortan
Lo hartu dut nik.
Amaren bularra
Haurraren janari
Aterpe samur hortan
Lo hartu nahi dut nik.