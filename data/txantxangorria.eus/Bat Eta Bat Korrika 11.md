---
id: tx-975
izenburua: Bat Eta Bat Korrika 11
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fDr3qHNt9F4
---

Errepideetako marrak
dira  hamaika  bateko,
hamaikak   bat  egiten  dute
urrunago
joateko.
Batekoak   beti  batuz,
Korrika  goaz  gure  bidea  gertatuz;
Pauso  bakoitzeko   hit:  bat
Aspaltoan   txertatuz:
Ni  eta zu,
zu
eta  ni
Bat  eta  bat bi gara,  bi
Goazen  korrika
Marrari  segikal
Bat  eta  bat  hamaika
Zu  eta  ni euskaraz:
Errepideetako   marrak
dira  hamaika  bateko,
hamaikak   bat  egiten  dute
urrunago  joateko.
Marrak  luze-betean,
Aurre ra -atze ra-harunt:  -honunt:
joatean,
Hamaika   herri  lotu  ditu
Korapilo  batean.