---
id: tx-2692
izenburua: Pintto Pintto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D-maafilOs0
---

Pintto Pintto gure zakurra da ta, 
Pintto Pintto bere izena du. 
Txuri-beltza da ta 
ez du koska egiten. 
Begi bat ixten du 
jolastu nahi badu. 
Pintto Pintto... 
Belarriak luze 
isatsa jostakin. 
Kalera irteteko 
beti presarekin. 
Pintto Pintto... 
Txuri-beltza da ta 
ez du koska egiten. 
Begi bat ixten du 
jolastu nahi badu. 
Pintto Pintto...