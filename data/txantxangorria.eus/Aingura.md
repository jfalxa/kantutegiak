---
id: tx-432
izenburua: Aingura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZL2RSshgV1U
---

Maite zaitut, ziur zaude, 
zure odola edango nuke. 
Patuaren, beldurrez,
ez nazazu utzi hemen. 
Itsasopean betirako ahaztua, 
geratuko naiz bestela.

Maite zaitut, ziur zaude, 
zure odola edango nuke. 
Patuaren, beldurrez,
ez nazazu utzi hemen. 
Itsasopean betirako ahaztua, 
geratuko naiz, azkenean.