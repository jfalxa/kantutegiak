---
id: tx-1386
izenburua: Zapalduen Olerkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ncu85PCkzK8
---

Herri ikustezin hontan itzalekin jolasten
neu izaten saitzen
geroa margozten
nire ezintasun denak
behin da berriz kantatzen
egunsentia
bide hau sentitzen

Denok ez dugu berdin
kontatzen istoria
zaila da ulertzea
bestearen egia
baina kantatu nahi dut zapalduen olerkia
estalita duten
samin guzti
ohe hutsei amaren malkoei
lapurtzen diguten denbora iheskorrari
burdin hotsei aitaren beldurrei
sufritzen dugunoi

Txikitatik entzuten
zer izan behar garen
indarrez inposatuz
zer maitatu behar den
baina kantatu nahi dut zapalduen olerkia
estalita duten
samin guztia
lagun minei bakardadeari bizigabe utzitako
une bakoitzari
izan zirenei
gaur gareneri
izango direnei
esaidazu maitea
dena aldatuko dela
bihar ere nirekin egongo zarela
eta ondorengoek
ez dutela sekula kantu hau kantatuko
malkorik botako esan maitea
esan laztana
entzun nahi dudana
esan laguna

Istripuei
berri txarrei
bidean galdu ditugun lagun guztiei
gugatik dena eman dutenei askatasun haizeari
zuei
haizeari
zuei, zuei, zuei