---
id: tx-206
izenburua: Dena Aldatzen Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l_WtEuN-bFY
---

Dena aldatzen da, “Todo Cambia” kantuaren moldaketa

argia, itzalak eta kea: Jessica del Campo
begia, mugimendua eta kolorea: Itziar Bastarrika Madinabeitia

Belun estudioan grabatua, 2022ko udaberriko azken ilargi betearekin

-----------

hitzen itzulpena: Miren Amuriza Plaza
musikaren moldaketa: Ines Osinaga Urizar
musika eta hitzak: Julio Numhauser (Warner Chappell Music-en kortesiaz)

ahotsak, ukelelea, panderoak panderetak eta larru guztiak: ines osinaga
soinu ingeniaria, nahasketak eta masteringa: Victor Sanchez
ekoizpena: ines osinaga

#alutik
#pospartumdepressionsurvivor
-----
aldatzen da azalekoa, / muina ere aldatzen da;
aldatzen da pentsaera, / munduan dena aldatzen da.
klima aldatzen da aroz / eta artaldeak zelaiz;
dena aldatzen da, beraz /ez harritu aldatzen banaiz.

eskuz esku aldatzen du / dirdira diamanteak,
txoriak habia eta / arnasa amoranteak.
ibiltariak bidea / aldatu egiten du maiz;
dena aldatzen da beraz, / ez harritu aldatzen banaiz.

dena aldatzen da, / dena aldatzen da...
dena aldatzen da, / dena aldatzen da…

gauez aldatzen da aldez / eguzkiaren sarea;
udaberrian soinekoz / aldatzen da landarea.
piztiak ilajez eta / aitona zaharrak bisai(a)z,
dena aldatzen da, beraz, / ez harritu aldatzen banaiz.

ordea, ez da aldatzen / ezergatik nire grina;
nire herriaren memoria, / nire jendearen mina.
bart aldatu zenak bihar / aldatzen segi dezala,
paraje urrunotan ni / aldatu naizen bezala.

dena aldatzen da, dena...