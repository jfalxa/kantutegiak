---
id: tx-1481
izenburua: Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XjKHZyxcVxo
---

Iratzea mertxikaz bete da jarriko
labettoa elur xuriz gorrituko
zezen beltza zazpi xahalez erdiko
euskaldunek euskara dute galduko. (bis)

Ur gainean ezarri dute zubia
hartan erein ogiko hazi garbia
han jin arte irin opilno xuria
euskarak ez du galduko mihia. (bis)