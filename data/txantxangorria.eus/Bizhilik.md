---
id: tx-1213
izenburua: Bizhilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PusR5fXMoJ4
---

Bizitzen ahaztu
zaionaren zuloekin,
karrika hutsetan
begirada galdurik.

Askatu ezinik
barreneko borroketan 
preso.

Bizirik ta hilik
linbo batean murgiltzen
geldo.

Ta galdezka zabiz 
posible den berriz
bizi normal bat
eskutik heldu.

Laztanak igarriz
musu ta irriz,
barne zuloak
ixten sentitu.

Egunsenti berriek
harrapa zaitzaten
ametsak marrazten
kafeko hondarretan.

Behin existitu ziren
ilusio haien
izenak idazten
logelako hormetan.

Ta hor dator berriz
hartzen zaitu indar handiz
estu.

Saiatu ta ezin
espirala nola hautsi
asmatu.

Amorrua aldiz
dator azkar haziz,
nahiko bazenu lez
bertan lehertu.


Ta eztandaren ostean
egun bat irrribarrez
bukatu.

Egunsenti berriek
harrapa zaitzaten
ametsak marrazten
kafeko hondarretan.

Behin existitu ziren
ilusio haien
izenak idazten
logelako hormetan.


bizhilik egon ordez...
bizirik egoteko

Egunsenti berriek
harrapa zaitzaten
ametsak marrazten
kafeko hondarretan.

Behin existitu ziren
ilusio haien
izenak idazten
logelako hormetan.

Bizhilik egon ordez
Bizirik egoteko