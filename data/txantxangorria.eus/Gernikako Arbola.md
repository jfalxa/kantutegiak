---
id: tx-3066
izenburua: Gernikako Arbola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z2VjhmjFHyI
---

Gernikako arbola
da bedeinkatua
Euskaldunen artean
guztiz maitatua.
Eman ta zabal zazu
munduan frutua
adoratzen zaitugu
arbola santua