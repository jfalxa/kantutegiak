---
id: tx-650
izenburua: Noizbait
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lDdmwwlO1Ug
---

Isiltasunak non zauden galdetzen dit.
Noiznahi zure izena mila aldiz oihukatuz, noiznahi.
Egutegiko orriak aurpegiratu dit,
Zenbat gau pasatu dudan
Itzarrik zu ez zaudenetik.
Aska gaitezan malkoz esan zenidan,
Noizbait, izango dugu gure aukera, seguru,
Noizbait, geroztik nora ezean
Barneko ekaitzetan
Arraunean ibiltzen naiz
Zure mezu baten zain.
Bila nazazu,
Izar bako gauetan,
Gida nazazu
Zure itsasertzera.
Noizbait...
Iritsiko naiz
Argiak jarraituz,
Iritsiko naiz
Berriz saiatuz, noizbait.
Itzuliko naiz,
Aurkituko zaitut,
Begiak itxiz gertu sentituz
Noizbait...
Bilatu nazazu
Argi ontziaz.
Gidatu nazazu,
Itsasertzera.
Iritsiko naiz argiak jarraituz.
Iritsiko naiz berriz saiatuz,
Noizbait.
Itzuliko naiz, aurkituko zaitut.
Begiak itxiz gertu sentituz,
Noizbait...