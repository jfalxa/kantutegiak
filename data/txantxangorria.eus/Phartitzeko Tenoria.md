---
id: tx-2793
izenburua: Phartitzeko Tenoria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XbNXedNzM8g
---

Orai düzü ene maitia
Phartitzeko tenoria
Hasperena bihotzian 
eta nigarra begian
Tristuratan bizi behar dut
ene denbora guzian.

Ez zütüt nahi kitatü
sobera zütüt maithatü
lili eijerra so egidazü
ene bihotza zuria düzü
eta zuria enia düzü.