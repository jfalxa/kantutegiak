---
id: tx-952
izenburua: Zuretzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yu9m91ogmEo
---

Zuretzat, zuretzat neure maitia,
Kantu berri hau dut idatzia,
Zu baizarelako, neure biotzeko
izar ezti, izar eder, izar betiko.
Zuretzat bakarrik,
Zuretzat, bakar, bakarrik.

Neguko gau hotz batian,
Toki ilun baina goxoan,
Aurkitu zintudalarik, bakar, bakarrik,
Neure bihotza nautzun zabaldu,
Ta zuk onartu neure eskaintza;
Eta geroztik ez gira berexten elgarretarrik.

Zure arpegi ederra,
Zure behako sarkorra.
Zure ezpainen guria,
Zure gogo hain argia,
Ni baitan daude gorde gorderik.
Gau eta egun zutaz naiz gose
Eta hil nahike neure aldetik joaiten bazine.