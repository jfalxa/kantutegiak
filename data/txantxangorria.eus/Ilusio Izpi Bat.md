---
id: tx-372
izenburua: Ilusio Izpi Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RCD26tJZZsY
---

Mina gorde zenuenez
triste zeundenean
laino beltz bat hazi zaizu
barrenean

Lainoa da zure babes
ta zure kaiola
askatu egin nahi duzu
baina.. nola?

tzatzatzatza...

Noizbait
euriak lurra bustitzen badu
Noiznahi
ostadarra barruan daukagu
Orain
ilusio izpi bat gara gu!

Tzatzatzatza...

Aska zaitez tantaz tanta,
ekaitz edo bare
zu prest zauden momentuan
gu prest gaude

Egin negar eta egin
euripean dantza
lagunekin badaukazu
konfiantza!

Noizbait
euriak lurra bustitzen badu
Noiznahi
ostadarra barruan daukagu
Orain
ilusio izpi bat gara gu!

Tzatzatzatza...


Instagram: @baloreak

By DeepSoda