---
id: tx-787
izenburua: Lurrari Lotu 1512 2012
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XcexxqHxTL0
---

Indarkeriaz lurra lapurtu eta zuen kateetan lotu,
Baina ez duzue oroimenaren katea moztea lortu!

Bost mila gorpu bakean bizi, bizi zen erreinuko lurrean,
Bost mila arrazoi gehiago dira inperioaren aurrean!

Ixilarazi nahi gintuzuen, bihurtuarazi erdaldun
Baina oraindik bizirik dago gure lingua navarrorum

NAFARROA, NAFARROA, NAFARROA EZ AL DA NOR?
EZ AL ZAIO, EZ AL ZAIO, EZ AL ZAIO BEREA ZOR?

Anexio gisa saldu zenuten konkista bat izan zena...
Belaunalditan transmititu da liburutan falta dena.


Oroitzen duen herria ez da bizi atzera begira
Nafarroara garai berriak aurki etorriko dira.

Karrika estu, lautada zabal, mendi malkartsuei auskin!
Gure lurrari lotuko gara baina gure kateekin


NAFARROA, NAFARROA, NAFARROA EZ AL DA NOR?
EZ AL ZAIO, EZ AL ZAIO, EZ AL ZAIO BEREA ZOR?
NAFARROA, NAFARROA, NAFARROA EZ AL DA NOR?
EZ AL ZAIO, EZ AL ZAIO, EZ AL ZAIO BEREA ZOR?

Nafar askatasunaren alde, Amaiaurko etxarrian borroka egin zutenei betiko argia.

NAFARROA, NAFARROA, NAFARROA EZ AL DA NOR?
EZ AL ZAIO, EZ AL ZAIO, EZ AL ZAIO BEREA ZOR?

MILA BOSTEHUN ETA HAMABI
BI MILA ETA HAMABI
EMAN HITZA NAFARROARI!
EMAN EUSKAL HERRIARI!