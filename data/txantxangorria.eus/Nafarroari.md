---
id: tx-2490
izenburua: Nafarroari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UvXWU-5Xitk
---

NAFARROARI

(Fernando Aire “Xalbador”. Urepel)
 
 
1-Goizean jeiki ohetik eta 
non nagon eginik galde
diot: “aurrean-Baztan daukazu 
gibelean Erro alde,
ezkerretarat Eugi herria 
eskuinalderat Luzaide
zure guziak Nafarroaren
hegalen azpian daude”.

2-Ene amatxi zendu maitea
Auritzberriko izanik
nafartarraren odol kartsua
zainetan senditzen dut nik,
eta artzaintzan Sorogain mendi
zoragarrirat iganik,
norbaitek: “atzar adi! diola”
entzuten dut urrundanik.
         ♪♪♪♪♪♪
3-Ene sortzeko agerietan
ni naiz baxenabartarra
titulu horrek lehen hitzean
galtzen du bere indarra
ni beheretar zendako deitu,
neurez banaiz orotarra?
Goi ta beherik ez da enetzat
Nafarro bat da bakarra.

4-Bere zainetan sendi duena
hastapeneko odola;
eman dioten izen mendreaz
nekez daiteke kontsola;
ezin onartuz bizi naiz beti
murrizturikan dagola;
bere itzala Baionaraino
hedatzen zuen arbola.
         ♪♪♪♪♪♪
5-Bainan enetzat, o Nafarroa,
ez da ezin igarria
izena nola gorde duzun zuk
eta guk hartu berria;
enbor sendoan sartu baitzuten
aizkora beldurgarria,
zure laurki bat bertzerik ez da
ni bizi naizen herria. 
                                
6-Bi aldetarik nagusi bana
jarriz gero Nafarroan;
ez dakit gure etorkizuna
zoin ateri buruz doan;
ez eta ere gaur zer hainetan
zauden zu euskaldungoan
bat gineneko egun ederrak
ditut bakarrik gogoan.
            ♪♪♪♪♪♪
7-Betititako arrain handien
lege okaztagarriak
gaitu nafartar anai arrebak
bi taldetan ezarriak;
bainan ele huts dira tratuak,
itxura huts zedarriak;
hitzez ta harriz ezin hautsiak
dira gure lokarriak.

8-Oi Nafarroa banatorkizu
otoizka fagore galdez:
urrikal zaite ukatu nahi
zaituzten seme alabez;
bertze koroka batek estaltzen
bagaitu ere hegalez;
Euskaldun orok Ama zu zaitu,
guk nahi izan ala ez (2).