---
id: tx-2660
izenburua: Jendia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SFvtGrYyFME
---

Bi zaldi taldearen beste bertsio bat, Mielotxinek grabatua haizetxen. 


Gazte nintzen
Jendartean lagun bila uneoro
Hala heldu naiz, hala nago nire zai
Ustez jendea maitakorra zen edonon
azkar hazi gara, hemen gaude, baina ez dakigu non
ta zenbat begirada hil, pantallatik aska ezin
ulergaitza zaigunak sarri ematen du min
badakit kritika lehenetsiz
egia nola saldu
ezizen bakarra balu
parean dugunean aldatuko da seguru
badakit, badakit
Egia gustoko dunak, harro darama soinean
ez denean bere aldeko utzi du bazterrean
denbora epaile fina da
bidea beti da eder pausoa nire bada
poliki biziraun norberaren neurrira. bihar
ta gaur


Jakin nahi dut, zein den jende hau, edozerri helduta
zein den jende hau
eurekin zinez zintzoa banaiz,
nire adar dira
Gisako putzuan gaude
sahiestu arren
berdinak hainbat gauzatan
Ta ibili nahi nuke arin, amesten dena egin
Gertuko dena zaintzen enegarren ahalegin
badakit kritika lehenetsiz
egia nola saldu
ezizen bakarra balu
parean dugunean aldatuko da seguru
badakit, badakit
Egia gustoko dunak, harro darama soinean
ez denean bere aldeko utzi du bazterrean
denbora epaile fina da
bidea beti da eder pausoa nire bada
poliki biziraun norberaren neurrira. bihar
ta gaur