---
id: tx-3394
izenburua: Santa Ageda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CWb-CodxWSc
---

Santa Ageda, Ageda;
gure martiri maitea,
etxe honeri eman eiozu:
zoriona ta bakea.

Heriotz eder-ederra,
euki zenduan Ageda
zeruko ateak zabaldurikan
hartu zenduan bakea.

Eskerrik asko jendea
bizi gaitezen bakean
gero zeruan batu gaitezan
Agedarekin batera.