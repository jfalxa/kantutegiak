---
id: tx-2520
izenburua: Saturraran 1938-1944
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VnzIGtOZPTA
---

Segoviako Benedicta, Leoneko Rosalia,
 Zaragozako Joaquina, Iruñeako Emilia…
zeinek seinalatu zuen gauez zure familia?
Avilako Rosario, Villanuevako Adela,
Donostiako Magdalena, Peñafieleko Gabriela…
ba al zenekien non zen Saturrarango kartzela?
Emakumeak iladan, jaio berriak otzaran,
utzidazu Saturraran madarikatu zaitzadan!

Bermioko Agustina, Logroñoko Micaela
Badajozgo Cesarea, Cabraleseko Manuela…
ze monjak lapurtu zizun haurra zu lotan zeundela?
Subijanako Isabel, Miereseko Laudelina,
Zamorako Isidora, Elgoibarko Geronima…
zuek hilda salbatuko al zuen inork arima?
Emakumeak iladan, jaio berriak otzaran,
utzidazu Saturraran madarikatu zaitzadan!

Palenciako Eusebia, Larrabetzuko Lorena,
Valdemoroko Ciriaca, Cudilleroko Elena…
zuen herrian ze kalek darama zuen izena?
Pontevedrako Dolores, Santanderreko Gloria
Toledoko Bernardina, Durangoko Gregoria…
zeinek lapurtu nahi digu berriz ere memoria?
Emakumeak iladan, jaio berriak otzaran… 
utzidazu Saturraran madarikatu zaitzadan!
Eleizaren hatzaparra, esnegabeko bularra,
olatuak estaltzen du, gauez haurraren negarra!