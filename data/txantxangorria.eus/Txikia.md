---
id: tx-3315
izenburua: Txikia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TPKY9R48FXY
---

"Txikia" zuen guda izena,
bera gizon osoa izan arren,
Mendizabal, Sasetaren urrena,
biak txiki, bizkor eta lerden.
Saseta hil zen gudarien aurrean,
Mendizabal hil zaigu bakarrik,
bainan biek daukate herri osoa
atzo ta gaur heien atzetik.
Euskadirentzat hil dire ta
gorputzak arantzaz beterik...
Arro hadi Kantauri Itxasoa
ta heien betiko loa betiko kanta zak.
geroari esaiok olatuetan,
herri izarrak ez direla itzaltzen,
gure haurrek biharko ikastoletan
heien izenak abestu ditzaten,
arantzetan biak hil bait ziren...
Arro hadi Kantauri Itxasoa
ta heien betiko loa betiko kanta zak,
geroari esaiok olatuetan
herri izarrak ez direla itzaltzen,
gure haurrek biharko ikastoletan
heien izenak abestu ditzaten...
Herriarentzat biak hil bait ziren!