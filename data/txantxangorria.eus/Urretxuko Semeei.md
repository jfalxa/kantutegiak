---
id: tx-2680
izenburua: Urretxuko Semeei
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/muQ-yXiy0OU
---

Villarreal de Urretxu 
nere herri maitea, 
seme bat hemen dezu 
amodioz betea. 
Nahi baina, nola ikusi? 
Hau da lan tristea! 
Zuretzat nahi det bizi 
Urretxu nerea.

Bi mila ta seiehun 
legoa badira 
Montevideotikan 
Euskal Herrira. 
Nahiz esperantzetan 
etorri bagera, 
aurreratasun gabe 
urteak joan dira.

Bai, nere adiskideak, 
behar da pentsatu 
guretzat Amerikak 
nola dian mudatu, 
Inork hemen ezin du 
lanikan bilatu; 
orain datorrenari 
behar zaio damutu.

Agur adiskideak, 
ikusi artean; 
zuenganatuko naiz 
egunen batean. 
Esperantzatan bizi 
nahi det bitartean, 
gero hezurrak utzi 
nere lur maitean.