---
id: tx-3106
izenburua: Besterik Ezagutu Ez Eta Mikel Laboa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CbCH73X9TZU
---

Besterik ezagutu ez eta
ase
irrintzi zakar bat
hautsi da zure gelako leihoan

besterik ezagutu ez eta
ez ote zaude kezkati
eta zuk
ez diozu kasorik egin

besterik ezagutu ez eta
ea eskuen artetik
ez ote zaizkizun
geroztik
txistua dena kexu
hots du
zure muinaren bila

besterik ezagutu ez eta
bizitzaren zenbait gauza
eder
eskapatzen ari
eta zuk ez diozu
kasorik egin

besterik
besterik ezagutu ez eta ...
sabelpeko dardar ozena
ez zaizu lainotuko
hilko, hilko sortu artio.