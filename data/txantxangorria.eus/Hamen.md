---
id: tx-209
izenburua: Hamen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nXxZVFPyzMQ
---

Sufrimendutik idatzitako koplek osatzen dute hurrengo kantua. Hainbeste urtez isilpean igarotako bizipenak letra gordinez osatu eta ahotsa nabarmendu nahi izan dugu; tradizioaren alde iluna zalantzan jarriz eta bizi izan diren erreprimitutako garaiak deskribatuz.

HEMEN gaudela oihukatu AMEN egiteari uzteko. 
HAMEN


Alaitz Eskudero Unaue, Amets Ormaetxea Ezpeleta, Eneritz Aulestia Mutiozabal, Garazi Otaegi Lasarte, Irati Gutierrez Artetxe, Leire Etxezarreta Learreta eta Maria Lasa Hilario.


SARE SOZIALAK:


EKIPOA:

Bideo Grabazioa eta Edizioa: Bideolan Proyectos Audiovisuales @bideolan_oficial
Audio Grabazioa: Haritz Harreguy 
Studio @haritzharreguy
Produkzioa: Alejandro “Jimbo” Paez @jimbo_larrata
Masterizazioa: Antonio Javier Moreno
Makilajea: Ane Lizarralde @anelizarralde.mua eta Amaia Urrestilla @amaiaurrestilla
Dantzaria: Garazi Etxaburu
Kontratazioa: Airaka @airakamusic

LETRA:

Soinujoleak plazara datoz
oihuez lagunduta
nire begiak kriskitinei so
zeharo txundituta. 
Musika ozen, 
gizonek txalo, 
ama etxean sartuta
kantu alaien atzean hamaika
inposaketa ezkutatuta. 
Musika ozen, 
gizonek txalo, 
ama etxean sartuta
jaia kondena bihurtu liteke
kate astun hauei lotuta.

Kanpoan hotz da dagoeneko,
eguna itzali da
ni bitartean ispiluari 
parez pare begira. 
Ilea askatu, 
gona altxatu 
ta lotsaz beherantza tira
nik zu eder ikusten zaitut baina 
arauak hala omen dira. 
Ilea askatu, 
gona altxatu 
ta lotsaz beherantza tira 
pekaturik handiena ote da 
norberarekiko desira? 

Pekatuen zamak
zenbat irauten du
damuaren bidean? 
Konfesatuko naiz
pekatuz pekatu
irrintzi bakoitzean. (BIS)

Besteentzako egun handia
heldu da azkenean
amonaren brotxea dirdirka 
darama bularrean. 
Bihotza hutsik, 
barrenak heze,
haren begiak parean
ez daki nola maitatu behar den 
lotura baten izenean. 
Bihotza hutsik, 
barrenak heze, 
haren begiak parean
bere burua maitatzen sekula
erakutsi ez diotenean.   

Besteentzako egun handia
heldu da azkenean
amonaren brotxea dirdirka 
daramat bularrean. 
Bihotza hutsik, 
barrenak heze,
zure begiak parean
ez dakit nola maita zaitzakedan
lotura baten izenean. 
Bihotza hutsik, 
barrenak heze, 
zure begiak parean
nire burua maitatzen sekula
erakutsi ez didatenean.

Pekatuen zamak
zenbat irauten du
damuaren bidean? 
Konfesatuko naiz
pekatuz pekatu
irrintzi bakoitzean. (BIS)