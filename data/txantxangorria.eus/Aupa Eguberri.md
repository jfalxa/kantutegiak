---
id: tx-827
izenburua: Aupa Eguberri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A876z2SlcWs
---

Aupa Eguberri
alaitasun handi
Aupa Eguberri
bihotzean iguzki.
Xutitu da etxean
fite-fite izai bat
eta puntaren puntan
izar handi eder bat.
Zer nahi ote duzu
emaitza eskuetan
bai ikusiko duzu
Zure bi zapetetan.
Afari on bat amak
guri apailatu du
eta gure gomitak
ongi alegeratu.