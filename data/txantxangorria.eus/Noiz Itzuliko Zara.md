---
id: tx-1949
izenburua: Noiz Itzuliko Zara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R0vG1wx7Hj4
---

Beti berdin, iraganean preso bizi gara 
Lurrera begira. 
Bihar ere, berdina pentsatzen jarraitzen badut 
Noiz ikasiko dut? 

Hitzak, etorkizuna 
Bide laguna, itxaropen bizia 

NOIZ ITZULIKO ZARA 
GALDETZEN DIOT GAUR NERE BURUARI 
NON DA ZURE IRRIA, NON ZURE ABESTIA 
BIHOTZAREN TAUPADEK 
ESAN DIDATE ALBOAN ZAITUDALA 
ILUNTASUN HONETAN ILARGIA ZARELA