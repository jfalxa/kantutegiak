---
id: tx-2122
izenburua: Gauaren Erregina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NHdAuYmvFrg
---

Aste guztia lanean
kabroiak entzuten
nagusiaren begi
lizunak somatzen
asteburua heldu da
ta irtengo zara
gau hau zurea da ta,
presta daitezela
ERREGINA,
GAUAREN ERREGINA
tabernan sartu orduko,
jolasa hasi da
alkoholez gorritutako
begiraden dantza
hain gustuko duzun
kanta bozgoragailuan
erritmo bero honekin
mugitu gorputza
ERREGINA,
GAUAREN ERREGINA