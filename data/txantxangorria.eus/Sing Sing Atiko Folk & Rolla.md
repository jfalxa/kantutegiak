---
id: tx-1661
izenburua: Sing Sing Atiko Folk & Rolla
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gBkVYESYufQ
---

Bizilagunen musika
zaratatsua eta aguantaezina
txakurra zaunka dabil
noizbait ixilduko da

Telefonoaren txirrina
goizero-goizero esnatzen nau
burbuxa baten barruan
bizi nahiko nuke

Atzoko ogi gogorra
hortzak puskatzeko moduan
benetan nazka ematen dit 
kutxako kola luzeak
kaña bat edango nuke
baina medikuak galerazi dit

Itsasoaren sabelean
bizi nahiko nuke
"Sing-Sing-ati"ko 
Folk & Rolla
bizitza gartzela da 
eta denok preso gaude

"Sing-Sing-ati"ko 
Folk & Rolla
bizitza gartzela da 
eta denok preso gaude

Hiruak eta laurden
ta orandik nago bazkaldu gabe
telebista pizten dut
betiko marrazki bizidunak

Lagunekin gelditzen naiz
ordu t'erdi itxaron beharra
mendi tontorraren puntan 
bizi nahiko nuke

Beranduago furgonetan
 txorakeritaz diskutitzen
bakoitza bere rolloan 
sartuta dago barruraino

Eta orain Folk & Rolla jotzen
hobeto sentitu nahian
"Sing-Sing-ati"ko gartzelan
bizi nahiko nuke




"Sing-Sing-ati"ko 
Folk & Rolla,
Bizitza gartzela da 
eta denok preso gaude

"Sing-Sing-ati"ko 
Folk & Rolla
Bizitza gartzela da 
eta denok preso gaude