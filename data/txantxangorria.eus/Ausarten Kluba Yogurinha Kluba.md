---
id: tx-573
izenburua: Ausarten Kluba Yogurinha Kluba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bpbDKMXixQA
---

AUSARTEN KLUBA / Yogurinha Borova

LAGUNA MINDU DUZU/
BEGIRA BERE NEGARRA/
ZER DA JARRERA HORI/
GALDU AL DUZU IPARRA?/
ZURE HELBURUA BALDIN BADA/
IZATEA POPULARRA/
ERRATUTA ZAUDE, BAZARA/
ZATARRA ETA KOLDARRA.

GURE LEMA ZABALTZEKO/
ATERA DUGU PANKARTA/
BERBAZ KONPONDU GAITEZKE/
TA IZAN PERTSONA APARTAK/
ZERGATIK HAINBESTE INDARKERIA,/
MEHATXU, ZALAPARTA?/
LAGUNAK ONDO TRATATUZ
GARA BENETAN AUSARTAK

LAGUNAK BABESTUZ SUPER HEROIAK GARA
ZATOZ GUGANA
GORA, GORA, GORA AUSARTEN KLUBA!!


Musika : Mikel Inun
Letra : Bihotz Gorospe

Irudia : Enrique Morente 

Egilea eta Edizioa : Atxa

Postprodukzioa : Niko Vazquez

Umeak : 

Ikasleak:
Aizene Abasolo
Yahya Ali
Izaro Amorrosta
Maindi Barandika
Julen Campano
Aretx De Pablos
Adriana Etxebarria
Oihan Etxeberria
Yehdiha El Balladi
Mohamed El Moussaoui
Eric Grosu
Haizea Intxaurraga
Maixa Iturbe
Sare Murgoitio
Najwa Otxandio
Laida Olea
Irati Perez
Izei Villar
Ibai Zarza

Irakaslea: Aratz Sagarna
 
Ikastetxea: Inazio Zubizarreta / Igorre

"Jazarpenik gabeko eskola nahi dugu. Ez dugu lagun batek ere minik jasatea onartuko,  ez gara horren lekuko izango, ez gara koldarrak. Lagun guztiak babestu eta tratu onak bultzatuko ditugu. Ausartak gara!"

Yogurinha Borovak umeekin lantzeko kantu hau prestatu du, eskoletan eta jendartean jazarpena desagertu dadin, denok arazo honen kontzientzia har dezagun.

Lehenago ere ladu izan ditu elkarbizitza sustatzeko kantuak euskaraz:

Eraso sexistarik ez

Sentipenak Askatu