---
id: tx-2751
izenburua: Oi Kanta Berri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QDjeuAuqW-M
---

Oi kanta berri ixilik nago
orain duela aspaldi,
zuretzat nuen kar bero hura
suntsitu ez da dirudi.
Beste lanetan iragan ditut
hainitz egun ta gau-aldi,
bainan oraino ez dut ukatu
Euskara ez da Euskadi.

Oi kanta berri bihotzetikan
ezpainetarat jalaia,
Euskal airean bihotzen zaitut,
zu neure mezularia.
Jende artean izatu banaiz
beresle xamurgarria,
izan zaite zu ene partetik
bakezale den xoria.

Oi kanta berri izan zaite zu
anai arteko zubia
batasunetik libertatera
gidatzen duen bidea.
Kartzelan den abertzaleari
emozu doi bakargia
etsiturikan den bihotzari
esperantzaun bizia.

Oi kanta berri gure herriak
behar du bide eta zubi,
rtxetan ere baitezpadako
argiarekin, ur garbi.
Horiek denak ez dire aski
bizi badien Euskadi,
hainbat oraino beharko ditut
kanta bat eta kanta bi.