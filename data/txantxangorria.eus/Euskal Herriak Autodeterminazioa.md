---
id: tx-1538
izenburua: Euskal Herriak Autodeterminazioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VHk-9-mNKOo
---

Europa zaharreko herri zaharrena da.
Atturritik Ebrora, auzolana euskara.
Nahiz ta ez garen bizi lehengoen gisara, 
atzo ziren, gaur gara, bihar izango dira.

Espainiar eta Frantziar estatutako putre
nazkagarriak sartu ziren botere truke.
Betirako daudenik ez dezatela uste, 
etorri ziren baina alde egingo dute!

Herri honek izan du beti mila kolore,
sufrimendu, borroka, arantza eta lore.
Behar ditugulako oroimenean gorde,
dena eman dutenei agur eta ohore!

AITORTZA HERRIARI!
HERRIAK ERABAKI!
EUSKAL HERRIAK AUTODETERMINAZIOA!