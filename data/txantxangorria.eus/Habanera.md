---
id: tx-2152
izenburua: Habanera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XnjAQ8mbDh4
---

Oiartzunen jaio zen 1944an. Kantaria, kantugilea, eta batez ere poeta handia da Lete. Ez Dok Amairu taldeko sortzailea, kontzientzia astintzailea, polemista, inkonformista, erosotasun guztien gainetik bere ideiak defendatu dituena eta lan ezberdin askotan beti kalitatezko maila eman duena. Frantziako 'chançon' berriaren eredua jarraitu du nagusiki. Bere eragina handia izan da euskal kulturan. Autodidakta erabatekoa, bai musikalki, bai literarioki, musiko handia izan gabe kantu zoragarriak egin ditu, Doctor Deseok, Mikel Laboak eta beste askok bertsionatu dituztenak. Bereak dira Euskal Herriaren ondare diren kanta asko: 'Izarren hautsa', 'Xalbadorren heriotzean', 'Nafarroa arragoa', 'Gizon arruntaren koplak', 'Euskalerri nerea' eta zenbat gehiago ez! 

1968an atera zuen lehenengo disko txikia, lau kanturekin osatua, eta urte berean ere garrantzia izan zuen olerki liburu bat, 'Egunetik egunera orduen gurpillean'. 

Gai ugari jorratu ditu bere kantagintzan eta poemagintzan: gai sozialak, politikoak, existentzialak, erlijiosoak, Euskal Herria hizpide izan du askotan... 1976tik aurrera, esate baterako, itxaropen politikoen garaian, Lete barrura begirako kanta pesimistak egiten hasi zen. Bere diskoen artean 'Kantatzera noazu' deiturikoa eredugarria da haren norabidea ulertzeko. 1978an utzi izon kantazeari, geroztik beste hainbatetan egin duen bezala, itzuli eta joan. 

Bereziki nabarmentzekoa da bertsolaritzan egin duen lana: Txirritaren bertsoak berreskuratu zituen Antton Valverde eta Julen Lekuonarekin batera, eta berriki bi mendetako bertsoen antologia erraldoi bat argitaratu du, '200 urte bertsotan' deiturikoa, zazpi diskotan barrena. Belaunaldi askorentzat ikerketa lana utzi du bertan. 

Umorea eta dramatismoa nahasten jakin ditu, eta horrek entzulearekiko komunikazio zorrotz baten jabe bilakatu du Lete. Ahots indartsu eta apur bat urratuaren jabea, nostalgiaz zipristindu ditu bere kanta asko. Kultura diputatua ere izan zen Gipuzkoako Foru Aldundian. Antzerkia ere landu du. Azken urteotan osasun arazoekin dabil eta kontzertu gutxi eskaintzen ditu. Bere sormen lana jasotzen duen azken diskoa 'Eskaintza' da, 1991ean argitaratua.