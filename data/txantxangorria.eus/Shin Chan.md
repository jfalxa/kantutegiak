---
id: tx-696
izenburua: Shin Chan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sOYV99ObKCo
---

Shinnosuke da mutiko
Alai-alaia bihurri
Eta azkarra
Ez da geldirik egoten
Ez zait uzten bakean.

SHINNOSUKE!

Maitasun kontuetan
Argia naiz benetan
Neska gazte guztiak
Nik ditut atseginak.
Zatoz txiki, zatoz nirekin,
Ez dut piper atsegin.
Begira zer trompa, elefante trompa
TROMPA! TROMPA!
Mutiko bihurria, shin-chan.

SHINNOSUKE!

Jende guztia oso
Urduri jartzen da
Ni azaltzen naizenean
Etorri nirekin pasatzeko primeran
Nire izena da shin-chan.