---
id: tx-894
izenburua: Ez Naizu Epaitu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UbqwTmm5hYY
---

Ez naz beltz, ez zuridxe
irrifarra nire ikurriñe.
Ez naz cool, ez hipsterra,
ez dot zaintzen nire bixerra.

Olerki gorri zaporedunak
egiten dodaz uda heltzean.

Ez naizu epaitu nire itxuragaitik,
ez libre izateagaitik,
ez arduretu nigaitik.
Ez naizu epaitu nire itxuragaitik,
ein nire bidxotzagaitik,
eta maite naizenagaitik.

Ez nintzen mundure etorri
danan gustokoa izeteko.
Ze laburre dan bizidxe
eta zenbat kosta libre izetie.

Nitaz suposatzen zan bizidxe
errekara bota dot guztidxe.

Ez naizu epaitu nire itxuragaitik…
Biluzik jaidxo giñen,
biluzik hilko gara.
Zergaitik aurre epaitu
ezagutzen ez banauzu.

Ez naizu epaitu nire itxuragaitik…