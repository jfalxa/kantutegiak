---
id: tx-47
izenburua: Orainari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZJibF5wbLYE
---

2023ko Elorrioko Gazte Topagunerako egindako kantua.
2023ko martxoan ekoiztua eta grabatua.

Hitzak: Leire Vargas
Doinua: Süne ft. Moonshine Wagon

Esker bereziak:
Xabier Suarez
Arriguri 
Jimbo Páez
Haritz Harreguy
Epic Films
Borja Herrán 
Saul Huerta 
Heli Suarez 
Jone Arostegi
Izaro Martinez 
Elorrioko Gazteei 
Deustuko Martxingorri konpartsari

Letra:

Norena da idazten
dugun historia?
zeini erauzi gura
zaio memoria?

Diren gauzen erroak
bilatzen ditugu
ez direnak sortzea
baitugu helburu.

Hormetan marraztuko ditugu galderak
erantzuna du ekintzak berak,
Nor dabil zaintzen?
Nor lanean?
Nor dabil dantzan?
Sukaldean, plazan?

Ertzetan guztiontzako
etxeak eraikitzeari ekin,
herri baten barruan
ta hizkuntza batekin, ekin.

Hormetan marraztuko ditugu galderak
erantzuna du ekintzak berak,
Nor dabil zaintzen?
Nor lanean?
Nor dabil dantzan?
Sukaldean, plazan?
 
Bilatu zalantzan,
Gaztetxean, plazan
Sukaldean, plazan
 
Bizitzeko, bizitzeko modu bat
Bizitzeko, bizitzeko modu bat

Hormetan marraztuko ditugu galderak
erantzuna du ekintzak berak
nor dabil zaintzen?
nor lanean?
nor dabil dantzan?

Hormetan marraztuko ditugu galderak
erantzuna du ekintzak berak,
Nor dabil zaintzen?
Nor lanean?
Nor dabil dantzan?
Sukaldean, plazan.