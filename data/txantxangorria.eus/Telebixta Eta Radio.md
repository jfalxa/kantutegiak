---
id: tx-895
izenburua: Telebixta Eta Radio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Hml1IjWn-6g
---

Euskaldunak gira aspaldiko Herri

Bainan nehork gutik dauku irakatsi

Gure Herriaren berri

Kanpoz eta barnez gaituzte hertsatu

Bai eta zenpatu eta guk holaxet

Burua apaldu!

 

            Telebixta eta radio

            Pagatzen ditugu kario

            Eta zeren entzuteko?

            Parise dela denen xilko.

            Telebixta eta radio

            Gu guzien enganatzeko

            Ez baita ez harritzeko

            Balinbagaituzte meneko!



Mugaz bertzaldean komedia bera

Ez naiz ez harritzen han ere jabeak

Pariseren kusi baitira!

Madrilen buztana Parisen muturra

Noiz botako dugu gure zangopetik

Horrelako zakurra!

 
Plazako bidean, amaren eskutik

Haur ttipia doa, bi zengoak arin

Herriko ikastolara...

Haur ttipiño huni

Dugun irakatsi, bai bertzeak bertze

Tresna maltzur horri ez sobera fidatzen!