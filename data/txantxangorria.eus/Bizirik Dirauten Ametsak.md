---
id: tx-2812
izenburua: Bizirik Dirauten Ametsak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VBr1LMZ4Aec
---

Etorkizunerantz begira nago
ahaztuezinezko irudi bizietan.
Udaberria neuk loratuko dut maitea,
ure hitzak zenbat balio didan.
Zure grinak, neureak eginak,
usaintzen ditudan lili
berdeak ditut bertan, ditut nigan,
amets dagit, ta izan bekit.

Bizirik ditudan desirak dira,
bizirik dirauten ametsak dira.

Geroan ere izan zaitzaket zugan
jarriko ditut lorpen hoiek,
garaiz gaude zin dagizut maite,
zin dagigun. Bidean darraigu,
ta ez da berandu. Bidean goaz,
bidean libre, zeure gogoan zerua
dut lore zarenean, zauden hortan
ta ariman zaitut, senean zaitut.

Bizirik ditudan desirak dira.
Bizirik dirauten ametsak dira.

Galdetzen badizute inoizko baten nor zaren,
norantz goazen, esaiezu izanak egin
zaitula zarena pausuz pausu lortu duzula duzuna.
Orain dituzun garaipenak zeureak direla,
zeuk eginak. Ez bazaituzte maite,
gogora eiezu zeu zarela.

Bizirik ditudan desirak dira.
Bizirik dirauten ametsak dira.