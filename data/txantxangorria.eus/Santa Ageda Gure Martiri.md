---
id: tx-3082
izenburua: Santa Ageda Gure Martiri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/POO1eQCult8
---

Santa Ageda, Ageda;
gure martiri maitea,
etxe honeri eman eiozu:
zoriona ta bakea.

Heriotz eder-ederra,
euki zenduan Ageda
zeruko ateak zabaldurikan
hartu zenduan bakea.

Eskerrik asko jendea
bizi gaitezen bakean
gero zeruan batu gaitezan
Agedarekin batera.