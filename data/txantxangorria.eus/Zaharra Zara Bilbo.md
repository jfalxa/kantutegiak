---
id: tx-2917
izenburua: Zaharra Zara Bilbo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dvv1Qb7J-tY
---

Neoizko argiak erdi zintzilik daude taberna gainian
urratuen aurpegi den pareta gorrian
zubiak gora zutitzen ziren portsmouth-eko ontzientzat
atzo euskalduna
gaur palaziua
eta biharko laino beltzak

eta, begiratzen zaitut
zuloaren gainetik
zerbait esan nahi dizut
zugan galdu aurretik
zaharra zara bilbo(bis)

la vieja sartu da nerbioi erriberan
oinak urbeltzetan
loreak baineran
oinak urbeltzetan
loreak baineran
mozkortzen nauzu ta ezin zaitut edan
ze esango det ba?

Neoizko argiak erdi zintzilik daude taberna gainian
ein nazazu zuria kantak dirauen artian
gerritik heltzen ondoeen dakien beso luzearen jabe
guapa etorri !
irri batekin batekin ziurtatuta ez al gaude?
eta, begiratzen zaitut
zerbait esan nahi dizut
eta, begiratzen zaitut
zuloaren gainetik
zerbait esan nahi dizut
zugan galdu aurretik
zaharra zara Bilbo(bis)

La Vieja sartu da Nerbioi Erriberan
oinak urbeltzetan
loreak baineran
mozkortzen nauzu ta ezin zaitut edan
oinak urbeltzetan
loreak baineran
mozkortzen nauzu ta ezin zaitut edan