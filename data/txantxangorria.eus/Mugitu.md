---
id: tx-253
izenburua: Mugitu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vhfdC3-7eYU
---

Ez konformatu
erabakia hartu
Mugitu!!
Beti zaude adostasunean
astoa baitzinen hor lanean
tentel huts itsua politikan
artalde batean
zintzo eta isilik egon beharrean
ardi beltza izan...
Ez konformatu
ez egokitu
erabakia hartu
Mugitu!!
Ez konformatu
erabakia hartu
Mugitu!!
Hormaren kontra jotzea bezala
da zurekin hitz egitea
ezarririk dagoenatik at
Itsutu egiten zara
Ikus errealitatea
ireki begiak
kobazulo honen kanpoaldetik
norbaitek mugitzen ditu hariak
Arazorik ez dela existitzen
egoten zara beti pentsatzen
bitartean herria sufritzen
Noiz arraio arte?
Kutura eta Hizkuntza desegiten
Haiek zuri esker
La, la , lara, la...
MUGITU!!