---
id: tx-1531
izenburua: Manoloarekin Kantari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AODoTGTZS2Q
---

Musika eta letra: Imanol Urbieta
Musika-moldaera: Xabier Zabala

Uztaiak gora-gora lurretik zerura.
jira alde bater eta gero bestera.
Ta behera, behera, behera,
ta behera, behera, bai,
uztai barruan preso gelditu naiz orain. JE!!
Ran Roberran,
po, po, po, po, po, po, po, po
Ran Roberran,
po, po, po, po, po, po, po

Yupi yaya, yupi yupi ya
Yupi yaya, yupi yupi ya
Yupi yaya, yupi
ya ya, yupi yupi ya ya
yupi yupi ya!

Ibiltzen, ibiltzen ari naiz
nire herriko bidetan:
zein ederrak diren mendiak
hor nabil ametsetan.

Ibiltzen, ibiltzen ari naiz
nire herriko bidetan:
ederra bait zara Euskadi
maite zaitut benetan
ederra bait zara Euskadi
maite zaitut benetan.

Txoria nintzela, txoria nintzela.
Sinistu nuen sinistu.
Amets polit hura, amets polit hura.
gaur ere nahi nuke sinistu.

Bakarrik eta libre badoa.
bakarrik eta libre gaztea.
Poltsikoan eskuak
oinak arrastaka.
Luzea da bidea.
Bakarrik eta libre gauean.
GGauea argitzen zaio bidea.
Argitzen zelaia.
Zuhaitzak, mendia.
Gauez badoa gaztea.