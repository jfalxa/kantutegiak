---
id: tx-3083
izenburua: Ehz 2012 Herrira Lipdub
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gUpxL_7S0P4
---

Aizu lagun hunat jin zira
Besoak zabalik dituen herrira
Eta ez zira ohartu
Hemen lagun anitz falta ditugu
Baina ulertuko duzu
Otoi egidazu kasu
Entzun ezazu ta errepikatu
Denek elkarrekin lortuko dugu
Baina ulertuko duzu
Otoi egidazu kasu
Entzun ezazu ta errepikatu
Denek elkarrekin lortuko dugu
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira
Maite duguna bihotzez
Urrundua herri arrotzetan
Gauero amets eginez
Elkartzen gira izarretan
Izar gauetako zita
Utz dezagun oraintxe bertan
Elkar ikusiko dugu
Gure herriko kaleetan

HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira

Argi urteak joan
ta urte argiak datoz
Orain ezin da jarrai
Itsasoa betetzen malkoz
Gaur, herrira oihuka
garaipenaren hitza
Mihi gainean daukagu
Askatasunaren giltza

HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira

HERRIRA!   HERRIRA!
Hasi da soka tira

HERRIRA!   HERRIRA!
Bukatzeko tira-bira

HERRIRA!   HERRIRA!
Urriaren hamahiruan

HERRIRA!   HERRIRA!
Baionan izanen gira

Orain ulertu al duzu?
Kantatzen segi ezazu
Mundu guzian mezua zabaldu
Laster herrian izanen ditugu

HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira begira
HERRIRA!
EUSKAL HERRIRA!
Zea zauz zara zire Herrira distira

HERRIRA!   EUSKAL HERRIRA!