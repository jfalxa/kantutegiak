---
id: tx-1671
izenburua: Hautsa Astinduz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lrgfd2I-KHE
---

Mugarik ez duen kartzela hontan
botere zaindari aunitz ditut segika
Kanpai hots mingotsak hurrunean
borrero haizkorak zai.

Ilargia lagun
hautsa astinduz noa
legetik ihesi

Musika uhinek
bortizki kolpatzen
didate bihotza

Atzera etengabe
doazen erloju orratzak
larrosak arantzik gabe
loratzen diren zelaietan

Askatasunaren taupadetan
sinesteagatik errudun bihurtu naiz
Otsoen ulua zakur zaunkaz
hiltzeko ez dira gai

Udarak urtu ditu
banatzen gintuzten hesiak.
Ulea laztantzen dizun
haizeak zugana zarama
AMETSAK
DAUZKAGUN KATEAK HAUSTEKO
ARMARIK ONENA DA.