---
id: tx-2732
izenburua: Trap Vasco
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xezQdaaM4cw
---

Arraun
Marmitako
Arraun
Bota pinue!

Puntaren puntan
Aldapeko sagarraren puntan
Puntaren puntan
Adarraren puntan 
Puntan uuuhh
Txoria zegoen kantari Xiru-liruli, xiru-liruli
Itsasontzi baten Euskal Herritik kanpora
naramate eta ez dakit nora
eta ez dakit nora eta ez dakit nora
Itsasontzi baten youh
Agur senideak agur lagunari
agur Euskal Herri osoari
Ez egin negar
Ez egin negar
Aita semeak tabernan daude
(Xalala)
seme-alabak jokoan
(Riki riki)
(Xalala riki riki)
Pintxo, Pintxo gure txakurra da ta
gure txakurra da ta uuhh
Sorgina pirulina erratza gainean
ipurdia zikina, kapela buruan
Itsasontzi baten Euskal Herritik kanpora
Sorgina, sorgina, sorgina
Maite zaitut, maite zaitut
pila, pila, pila, pila
Patata tortila
Bocabeats comedia

Itsasontzi baten Euskal Herritik kanpora
pila, pila, pila, pila
Patata tortila
2017
Euskal Trap
Nor Nori Nork
zaizkikarakotakezu
zitzaizkida...
zitzaiizki
KATXIPORRETA!