---
id: tx-516
izenburua: Ipuinik Ederrena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KB-P39t1z9U
---

Ipuinik Ederrena - Kai Nakai (Akustikoa)

Jarraitu nazazu hemen/ Sígueme aquí: 

Management: joseba@pandaartistmanagement.net

Letra:
Zenbat bider
Hain betirako eta eder
Baina ya hor ez dago ezer
Ez dago ezer
 
Begira hemen
Ikusi duzu sua zer den
Azken marra pasa zenuen
Pasa zenuen
 
Gidoia ez da zuk uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago
 
Ez dela uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago
 
Ez dut itxaroten aukera bat
Erloju hori botata daukat
Ederra eta piztia naiz ni biak batera
Nik daukadana beste inork ere ez dauka
 
Ez zazu segi bilatzen gaztelu irreal hortan
Ez kristalezko sabaidun urrezko kaioletan
Ihes egin dut urrun
Nik hormak lehertu ditut
Atera zaitez itotzen zaituen soinekotik ahal baduzu
 
Gidoia ez da zuk uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago
 
Ez dela uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago
 
Ez kontatu ipuin hori benga
Sutan kiskali dut zure mozorroen denda
Sagarra, otsoa eta lotiaren legenda
Ipuin hau spoiler batekin hasten da
 
Gidoia ez da zuk uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago
 
Ez dela uste bezala
Orri txuri bat da nire azala
Hemen idatziko dugu dena
Ipuinik ederrena
Printzesa hau esnatuta dago

#KaiNakai​​ #IpuinikEderrena​ #8M