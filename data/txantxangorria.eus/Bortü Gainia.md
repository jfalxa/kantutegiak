---
id: tx-197
izenburua: Bortü Gainia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0bkojcKzH4s
---

Bortü gainia 
O ho ho!  O ho ho!  O ho ho  ho ho! O ho ho!  O ho ho!  O ho ho  ho ho!

Bortü gainia, negiarekila, elhürrez zeikü beztitzen, 
Xaharrer bilhoa, adinarekila, ber gisa zeizie xuritzen, 
Iragan denborez ohartü eta, nigarrik ez zeizie elkitzen, Eüskaldün higatzez, ziren bizitzean, aitzinekuk züntien segitzen. 
Lehenekuen ganik ikasi zünien kantore emaiten ‘ta jauzkan, Pastoral’egiten eta txülülatzen, maxkaradan’ta koblakan, Eüskal herria, ziek hol’izatez, aidostü zen biga bostetan, Xibero berria, zier enpeltatia, laxüki zeizie zorretan. 
Jente xaharrak beti hartze dü errespetü gazten ganik, 
Bozka püntian hora-zo karesü, esker esküin’ta erditik, 
Egün münd’oro zirekilan da, ziren eüskal baliua gatik, 
Arauz denak, heben, xahartüren gira, igaraitez ziren hexatik. 
Hitzak: Dominika ETCHART 
Müsika: Jean ETCHART