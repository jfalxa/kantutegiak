---
id: tx-918
izenburua: Umiltasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IGsQnU_2bFo
---

ARRIGURIk egindako bideoklipa. Video by Arriguri 

Ekoizpena/prod by: Ibai Bengoetxea (DNG)

Nahasketa/Mix by: Lander Llorens (Eguzkiaren Semea)

Master: James Morgan (Magicbox Musika)

Lan berria entzuteko / Escuchar el nuevo album aquí / Listen the new album here: