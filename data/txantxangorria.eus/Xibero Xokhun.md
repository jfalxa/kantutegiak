---
id: tx-2889
izenburua: Xibero Xokhun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KwOmU-YIPjU
---

Xiberoko gaztia laket bere xokhun
Oi hura da debeatzen hantik elkhi ondun.
Sor herrik amodiua harek beitu kholkun
Aberastiagatik ez leite hantik jun
ERREPIKA
Eskual-Herrlko amodiua denek bethi azkar begira
Egun batez gure sor-lekkhian bizi gitin alagera
Gure herri ttipietan Xibero gainian
Gazten lan gutixko da etxondo maitetan
Hartakoz bebar dugu txerkhatu bestetan.
Hiri handietat phartitüz nigarra begietan
Oi! zer malurra dugun gure Xiberuan
Lanik ez ukheitia sor herri ederretan
Nuiz artino gaiz hori dat Eskual Herrian
Bihotz erdiagari familia orotan?
Sor-herrik amodiua khanta gor·azkarki
Eskual iedia denek erroti jarraiki
Batek bestiak lagünt ahalaz huneski
Egün batez bizi gitin gure herrin goxoki