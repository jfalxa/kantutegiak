---
id: tx-1571
izenburua: Haragizko Estatua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wKnHWPRiyP0
---

Eder basoan haritza
gaztain besangek zumitza,
Gerri ederrak ordu zuritan
gaua luzatzen dabiltza.
Ge/rri e/de/rrak or/du zu/rit/an
ga/ua lu/za/tzen da/bil/tza.

Eder hibaian ahate;
kanpai dorreak lau ate;
Lepoa tornuz biribilduek
musika sutan daukate.
Lepoa tornuz biribilduek
musika sutan daukate.

Eder pilota soroa
kanonigoen koroa
sorbaldak daki errepikatzen
metal bustien orroa

Eder osinak ur futzu
Itsas sokoa amultsu
magalak uda sargoria du
negu gaitzetsa elurtsu

Eder soineko jantziak
olatuaren antsiak
bularrak ziren ondar zuritan
trabaturiko untziak

Eder dilindan guriña
uxoek hegada ariña
beso zangoek eho digute
afaritako iriña