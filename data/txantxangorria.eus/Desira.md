---
id: tx-992
izenburua: Desira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tAJ1gSuesxA
---

Facebook:
Instagram:
Twitter:

www.produlamrecords.es


DESIRA

Zuek hor aurrean
Hemen 6 musikari,
bihotz bakar batean
sua dezazuen senti

Ilunabarraren
isiltasuna
apurtu du tximistak,
gure telonero gisa

Ekingo diogu
Gure garaia da eta

Desira ozen
Zuek, gu eta parranda
Elkarri abesten
gelditu ezinean

Giro ederra
Lotsarik eza
Zuen babesa
… Osagaiak

Ah ah

Jende maitagarria
ezagututa
furgonetan barrezka
herri berri baten xarma

Indarra soinean
Zuen oihua
kamerinotik aditu dugu
Kontzertua hasiko da!

Ekingo diogu…

Desira ozen…