---
id: tx-1908
izenburua: Txikita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6LnQkbZbuaw
---

Hartu, izan, buka, dudan, ezin, nora, denok,
irten, doalako
taberna ilun batean ordu asko igaro ditudalako. 
Bero, nori, uki, goza, bada, buru, buelta, zure,
naizelako
ai ene txikita gutxi zaitut nire ta desio zaitut asko. 

Azukar goxuaren bidaien, usaimena datorkit
zugandik. 
Zaren bezain txikita izanik, besoetan nahi zaitut
lehenbailehen. 
Ta loa gauarentzat bada, zu niretzat
ni zuretzat egin naiz hara, uki nazazu pixkat. 

Lokartzean nago zure sentsazio denen ametsetan
nabil 
eta zein eder zaren sentitzeak beste trago bat
merezi din. 

Azukar goxuaren bidaien, usaimena datorkit
zugandik. 
Zaren bezain txikita izanik, besoetan nahi zaitut
lehenbailehen. 
Ta loa gauarentzat bada, zu niretzat
ni zuretzat egin naiz hara, senti nazazu pixkat. 

Txikita... argi izpiak banarama
Txikita... etorri zaitez niregana
Txikita... gaur bakarrik mozkortu nauzu
Txikita... bakar-bakarrik utzi nahi nauzu.

Azukar goxuaren bidaien usaimena daorkit zugandik...