---
id: tx-281
izenburua: Amerikano Beltzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T6cpz-dKA1c
---

GUK taldea: 
B. Sarasola
J. Borda
J.P. Lacarrieu
P. Bercaïts
B. Davant

Amerikano Beltzak

I.
oi amerikano beltzak
iragaïten dituzue oren latzak
zurien eskubide berdiñak
nahi ditutze eta
herioan edo kartzelan
guzien saria...

(errepika)

beti jarraiki, beti aintzina
guduan anaï zirezte guztiak
zuekin baitago eskual herria
gero hobeago baten begira

II.
oi amerikano beltzak
mundu zabalzabalean gudu bera
zonbat jendaldek ez deramate
bizia eskaintzen dute eta
herioan edo kartzelan
guzien saria