---
id: tx-292
izenburua: Bizkaipourri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nUwmeUkoN9g
---

Gure herrien dago gizon bat
ministrue esateutsena
txindi asko ta aziendatan
oso aberatsa dana.
Azino askon jaubie ei da
adin argidun gizona
entzute handie deko bedorrek
dalako gizon zuzena.

Jakituriek erantzi deutsoz
buruko ule guztiek
errespetaua dala dirudi
bere soineko janzkiek.
Mosuak bere biribilek dauz
gainera bizar zuriek
ta ikusmen ona dirudienak
dekoz bai bere begiek.

Joan nintzan Madrilera
bai etorri bere, bai etorri bere
hango neskatotxuak
ez nabe ni maite, ez nabe ni maite.
Hango neskatotxuak
maite nabenean, maite nabenean
praile sartuko naz ni
komendu batean, komendu batean.

Kerizak heldu dira
bai Kanalekoak, bai Kanalekoak
trapu punta-luzeak
bai Angelukoak, bai Angelukoak
garagar-herrikoak
Natxitu aldekoak, Natxitu aldekoak
abarka-manta zale
bai Ispazterkoak, bai Ispazterkoak.

Nik dot, alkate jauna,
Matxango izena.
Matxango neure aita
eta amarena.
Gu gara Marakaibon
guztizko nobliak
kakao eta azukre
askoren jaubiak
askoren jaubiak
guztizko nobliak

Heldu ginian, bada
gu Lekeitiora
hiru urte osuak
laster-laster dira.
Hemen saldu ninduen
betiko esklaba
hain zuzen, abade bat
dot neure ugazaba
dot neure ugazaba
betiko esklaba

Ipiñaburun dagoz
Bildurrez ikara
Sartu ez dagitezen
Txarriok bertara
Bada entzun ei dabe
Hara ta Undurrara
Laister juango dirala
Zazpi barkukada

Entzunik doazala
Ubideko herrira
Otxandioko plazan
Ikaratu dira
Alkarreri dineutse
Sartuko ete dira
Txarriok Otxandion?
Bai!... Txarritokira!
Txarriok Otxandion?
Bai!... Txarritokira!
Txarriok Otxandion?
Bai!... Txarritokira!

Kanta berri batzuek
Dituguz atara
Aitzen emoten gatoz
Herri honetara
Arioplanuan juateko
Asmuetan gara
Bihar urtetzen degu
Ameriketara

Aparatu hau eiten
Urte bian lana
Hortan gastatu degu
Geure diru dana
Biderako ere, berriz,
Bihar degu jana
Horreatik eskatzen degu
Errialtxo bana
Biderako ere, berriz,
Bihar degu jana
Horreatik eskatzen degu
Errialtxo bana
Horreatik eskatzen degu
Errialtxo bana