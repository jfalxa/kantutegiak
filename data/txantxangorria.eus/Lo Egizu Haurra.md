---
id: tx-2312
izenburua: Lo Egizu Haurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NR3m3Wu41oM
---

Bizitzeko zoriona 
aurkitu nahirik munduan 
urratsez urrats bidean
nigarra ta iluntasuna.

Baina bada esperantzaz
bizitza argitzeko ordua
zure ametsen edertasunez
aurkiturik babes lekua.

Lo egizu haurra
bake gozoan lo egizu
bihar esnatuko baita
zuregan bakearen hazia.
Justiziaren hazia
elkartasun hazia
bihar esnatuko baita
itxaropen egun berria.