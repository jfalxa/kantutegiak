---
id: tx-1487
izenburua: Oi Betleheem
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vCull2KS-oE
---

Oi Bethleem!
Ala egun zure garaia,
¡Oi Betlehem!
Ongi baitu diztiratzen,
Zugandik heldu den argia
Betetezen du bazter guziak.
¡Oi Betlehem! ¡Oi Betlehem!
Artzainekin heldu naiz
zugana legiaz artzainekin,
Ez bezela nahi izenik
Adoratzen zaitut Mesias
Eta maite bihotz guztiaz
Artzainekin, artzainekin.