---
id: tx-2268
izenburua: Autodefentsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7sDZX5_5u4E
---

HERDOIL (Iruñea Euskal Herria) 2018.

Noiz Arte EParen aurrerapen abestia, Sound Of Sirens Iruñeko estudioan grabatua. Bideoaren zuzendaritza eta grabaketa Jon Corresen eskutik izan da, irailaren 6an iruñerriko gaztetxe ezberdinetan.

Cancion de adelanto del EP Noiz Arte. Grabado en el estudio Sound of Sirens en Iruñea. Vídeo dirigido y grabado por Jon Corres el 6 de septiembre por diferentes gaztetxes de iruñerria 

(EUS)
MUNDUA ULERTZEKO MODU BAT DELAKO
DEFENDA DITZAGUN GUNE AUTOGESTIONATUAK
GAZTETXEAK DENA ALDATZEKO

Gure lan indarra xahutzen dute
Emakumeak bortxatzen dituzte
Natura suntsitzen dute, gutxi batzuk
Aberastu daitezen

Pentsaera libre baten beldur
DIRA!
Gizarte honetan nor da errudun
DIRA! ERRUDUNAK!
Boterearen gerratean, batzuk gartzelan besteak heroiak
Dirua ziur.

SITEMAREN PARTE IZANGO GARA
KONTURATZEN EZ GAREN BITARTEAN
AUTODEFENTSA IZANGO DA
GARAIPENAREN BIDEA x2

Itsutzen gaituena
Ez da iluntasuna
Batzutan argi dago
Argi ez dagoena x2


Kontaktuak:
-Beñat (666096904)
-imanol (633471016)
-Instagram: @herdoil_punkoil