---
id: tx-2079
izenburua: Olatu Berdea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1Y3DH5tJMX0
---

Euskal Herriko kaleak
itxaropenez betea
irrintzi edo oihartzunak
izendu doinu berri bat
zortzi puntako izarra
herri zahar honen gainean
Izan ginenaren oroitzapenaren
bihotzean: INDEPENDENTZIA!

Zatoz! ZATOZ! gure artera
Gehitu! Olatu berdera
argia bai da
argia bai da
GURE BIDEA
ta baju jarriak dira
oztopoak bidean
behin eroriz altxatuta
indarra haundituz doa
ideiak sendo
batek argitzen du
amets eroak
lurra hau izerdi kementsuz
elikatzen dugunean
INDEPENDENTZIA!
Zatoz! ZATOZ! gure artera
Gehitu! Olatu berdera
argia bai da
argia bai da
GURE BIDEA

Herri oso baten hitza
ETORKIZUNAren giltza
eraiki dezagun zubi bat
ASKATASUNERA!

INDEPENDENTZIA!

Zatoz! ZATOZ! gure artera
Gehitu! Olatu berdera
argia bai da
argia bai da
GURE BIDEA

Zatoz! ZATOZ! gure artera
Gehitu! Olatu berdera
argia bai da
argia bai da
GURE BIDEA
BIDEA ! BIDEA !