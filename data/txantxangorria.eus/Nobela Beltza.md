---
id: tx-597
izenburua: Nobela Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z0Pdjp_1FdY
---

Oh!!

Zortzi ta erdiak
Sunset, Boulevarden esnak batian
kafe batek bi behatzak epeltzen ditu orain?
begiak ernai, izuak beti
ingurua astertzen

Hiruko argien dardara marine etenak
itzali den biziltza ihes autopsia
uzta g/rria dolarren irla da
berriz galderak
enigma bihurtzerik daukazu

Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!

Erantzunen bila kolpe aurka bila
txakurra da, ia beraren konplexua
hotza bihotzean, agur luzea
bai diren artean, ez,
dago inor zure zai

Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!
Oh!!
Oh Marlowe, Marlowe!

Oh!!
Oh!!
Oh!!