---
id: tx-1099
izenburua: Black Is Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QWbRR_3RMLY
---

Hauxe da
Erraldoien istorio bat
Hauxe da
Erraldoi beltzen istorio bat

Menderaezinak
Martxan badira
Hilik ordea
Ez badira dantzan

Nola esaten da BLACK euskaraz:
BLACK IS BELTZA

Whe we were
Erraldoi/erreginak, Kings/Queens
Flotatzen 
Tximeletak bagina lez

Erleak ordez
Ziztatzeko prest

Pantera Beltzak
Ali eta Malcolm X

How do you say BLACK in Basque?
BLACK IS BELTZA

San Fermin
New Yorkeko kaleetatik
James Brown
Otis Redding, Cheikha Rimitti

Gizateriak
Esan du aski da!
Erraldoiena, “Che”,
Da bere martxa.

Nola esaten da BLACK euskaraz:
BLACK IS BELTZA