---
id: tx-1417
izenburua: Sagarrak Zintzilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iV7y1OwKLHA
---

Sagarra zintzilik
eskuak atzean.
Ahoa, ahoa
luzatu lepoa.
Ahoa, ahoa
hor dago kakoa.