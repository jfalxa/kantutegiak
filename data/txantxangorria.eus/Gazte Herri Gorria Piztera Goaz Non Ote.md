---
id: tx-3105
izenburua: Gazte Herri Gorria Piztera Goaz Non Ote
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_nzQOMb-ZOU
---

Bi es/ta/tu Ka/pi/ta/lis/tek
za/ti/tu/ri/ko he/rri/an
za/pal/du/ak ba/ina ez men/pe/an
Ja/in/ko/a ta le/ge za/ha/rrak,
di/ru/a ta bo/te/re/ak
sun/tsi/tzen du
gu/re es/ku/al/de/a

Ze/men/tuz es/ta/li du/zu/e lur guz/ti/a
Gaz/te/ok ka/le go/rri/an ja/rra/i/tu a/rren
tu/ris/ti/fi/ka/zi/o/a/ren bi/tar/tez
a/be/ra/tsen es/kue/tan da kos/tal/de/a

Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az

Gaz/te he/rri go/rri/a piz/te/ra go/az
Le/moi/ze/tik E/ran/di/o/ra mar/txa
bo/rro/ka e/ta se/gi au/rre/ra

He/rri/a sal/du na/hi du/te/nen az/pi/tik
in/po/sa/ke/ta ho/rren gai/ne/tik
I/zan gai/te/zen gaz/te
I/zan gai/te/zen as/ke

za/pal/kun/tza bi/koi/tza,
so/zi/a/la, na/zi/o/na/la
hi/ru/koi/tza ta/ma/lez
e/ma/ku/me/en ka/su/an

E/rai/ki/tze/ko sun/tsi/tze/a
be/har de/ne/an
Hor/tzak zo/rroz/tu
e/tsai/a/ren au/rre/an

Gaz/te he/rri go/rri/a
da gu/re hel/bu/ru/a
an/to/la/kun/tza bo/rro/ka
tres/na bi/ka/i/na

Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az
Gaz/te he/rri go/rri/a piz/te/ra go/az

Sus/traie/ta/tik haz/ten da zu/hai/tza
gu/re sus/tra/iak a/pur/tu di/ra
ho/ri bai zo/ri/gai/tza
e/guz/kia/ren a/tze/tik
be/ti da/tor e/kai/tza
go/rro/to/a/ren go/rro/to/a
zu/en e/mai/tza
Guk hitze/gin be/har
ge/nu/ke eus/ke/raz
bai/na be/ti er/de/raz
be/raz za/bil/tza es/pa/ño/len an/tze/ra
na/hiz e/ta as/ko/tan
e/ra/bi/li haien hiz/ke/ra
ja/kin be/har du/te
geu/re he/rri/a de/la ber/be/ra
as/ka/ta/su/na go/ra
e/rre/pre/si/oa be/he/ra
Gaur U/ri/be Kos/ta a/te/ra ka/le/ra

Te lo di/go a tu ma/ne/ra
pa/ra ver si a/sí te en/te/ras
Es/pa/ña que me de/ses/pe/ras
tu nom/bre me al/te/ra Fue/ra!!
Lle/na de men/ti/ra
y de ba/su/ra tu ciu/dad
vie/nen con sus tre/nes
de al/ta ve/lo/ci/dad
y lue/go ha/blan
de ca/li/dad de vi/da
y u/na nue/va
cen/tral ter/mi/ca en Le/mo/iz
SON SUI/CI/DAS
De/jad que ca/da u/no e/li/ja
ser lo que se/a

U/tzi ba/koi/tza/ri
e/ra/ba/ki/tzen be/re bi/de/a
nik ar/gi dau/kat zein den ni/re/a
mun/du be/rri bat
he/rri be/rri bat
li/bre eta as/ke/a

Gaz/te he/rri go/rri/a piz/te/ra go/az
Le/moi/ze/tik E/ran/di/o/ra mar/txa
bo/rro/ka e/ta se/gi au/rre/ra

He/rri/a sal/du na/hi du/te/nen az/pi/tik
in/po/sa/ke/ta ho/rren gai/ne/tik
I/zan gai/te/zen gaz/te
I/zan gai/te/zen gaz/te

GAZ/TE

AS/KE

GAZ/TE
GAZ/TE e/ta AS/KE