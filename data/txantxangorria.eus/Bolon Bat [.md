---
id: tx-526
izenburua: Bolon Bat [
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w3i2eIxb7Y0
---

Bolon bat eta bolon bat
bolon putzura erori;
erori bazen, erori,
ez da geroztik ageri.
Bolon bolon bolona,
gure haurra haur ona,
Bolon bolon bolon bolon
bolon bolon bolona.

Gure haurraren haur ona,
balio luke Baiona,
Baiona dirutan baino
nahiago geren haur ona.
Bolon bolon bolona,
gure haurra haur ona,
Bolon bolon bolon bolon
bolon bolon bolona.

Haurra egizu lo ta lo
emain dizkizut bi kokollo
orain bat gero bestea
gauean txokolatea.
Bolon bolon bolona,
gure haurra haur ona,
Bolon bolon bolon bolon
bolon bolon bolona.