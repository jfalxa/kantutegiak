---
id: tx-409
izenburua: Xoriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V-FXWep3Grk
---

Xoriak (Xorien ihesa)
Texto original
Zuen begiak, eta zuen etxeetako leihoak,
Eta zuen eskuak zabaltzen dizkiezue.
Xoriak laudatu egiten dituzue,
Lausengu lirikoak ere dedikatzen dizkiezue.
Baina xoriak zuengandik beti ihes doaz.


Autor letra
Joseba Sarrionandia
Autor música
Mikel Laboa