---
id: tx-1675
izenburua: Bizitzarekin Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ohZ_YiAINaY
---

erreka baten bazterrean
kitar hotsa haizean hedatuz;
suaren argitasupean
itzal gera, gurekin ez daudenen antzera.
Denbora luze doan heinean
ederra dena leun ahitzen da
heriotza baino txiki txikiago den
bizitzarekin dantzatu
Etorkizuna da lekuko
itzaletan sinestu;
mendira igo eta ikusi
lagunekin ederragoa de mundua