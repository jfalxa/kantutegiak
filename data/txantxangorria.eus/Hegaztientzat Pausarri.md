---
id: tx-2968
izenburua: Hegaztientzat Pausarri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RtlZi4oam5g
---

Hegaztientzat pausarri,
arrainentzako uharri,
hegaztientzat pausarri;
ai ninduke lur maiteak
zerbaitetan lagungarri.

2.Maite dut eta banabil
maitea nola zerbitu;
urteak zehar lora ta
-maite dut eta banabil-
ipuinak dizkiot bildu.

3.Giroak landu egurrik
ekarri diot menditik;
uren oldeak luzaro
mizkatu hibai-harririk,
giroak landu egurrik.

4.Nekatzeraino ihardun dut
atsegin eta dolore;
nekatzeraino ihardun dut,
ukabilak erabili
ditut maitearen alde.

5.Maitearen amorez naiz
aritu kalerik kale,
herriaren amorez naiz;
jadetsi diot hiriko
zenbait oroigailu ere.

6.Badakit aski ez dena,
besterik behar duena,
badakit huskeri dena;
semearen falta baita
nire maitearen pena.

7.Bihotzean haurra eta
haurrik esnatu ezina;
bihotzean haurra eta,
gizalditakoa dela
Euskal Herriaren mina."