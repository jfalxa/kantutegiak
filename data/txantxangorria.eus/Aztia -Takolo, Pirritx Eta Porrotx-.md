---
id: tx-3290
izenburua: Aztia -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UDDj3p-aZjM
---

Azti handi bat izan
nahi nuke nik
ikasi ere zuek nirekin
eskuak aurrera batera ahalegin
ma/gi/a/ri e/kin.

Deus ez hemendik
ezer ez handik
hauts majikoak
hartu poltsikoetatik
hautsak jaurtiki
e/ta putz e/gin
e/le/fan/te bat
atera da kapelatik

Azti handi bat izan
nahi nuke nik
ikasi ere zuek nirekin
eskuak aurrera batera ahalegin
magiari ekin.

Azti handi bat izan
nahi nuke nik
ikasi ere zuek nirekin
eskuak aurrera batera ahale/gin
magiari e/kin.

Deus ez hemendik
ezer ez handik
hauts majikoak
hartu poltsikoetatik
hautsak jaurtiki
e/ta putz e/gin
e/le/fan/te bat
atera da kapelatik