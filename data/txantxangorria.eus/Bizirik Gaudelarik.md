---
id: tx-2537
izenburua: Bizirik Gaudelarik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4a9p7R9SrZU
---

Nahiago nuke banitu


hegal bizkor zabalak
joan ahal izateko
aise zure ondora
ta belarrira xamurki esan
nik maite zaitudala
piztu dizudan suziak
zugan iraun dezala.

Kantu hau nahi dizut
eman zuri herria
gu gara iturri zaharreko
ur berria.

Oraindik azi asko dut
zuregan landatzeko
esan ez ditudan hainbat
olerki eta asmo
elkar lotzen gaituzten sokak
ez dira apurtuko
bizirik gauden artean
bizirik gaudelako.

Kantu hau nahi dizut
eman zuri herria
gu gara iturri zaharreko
ur berria.

Ahal zutena eman zidaten
ta erakutsi itzultzen
haundia banago ere
zure albora etortzen
ez baititu semeak amak
inoiz alboratzen
ni zurea zu nerea
beti izan gaitezen.

Kantu hau nahi dizut
eman zuri herria
gu gara iturri zaharreko
ur berria.