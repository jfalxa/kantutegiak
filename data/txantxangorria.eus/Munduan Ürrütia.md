---
id: tx-2774
izenburua: Munduan Ürrütia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/atjmlAMRi7A
---

Musde Urrütia leihoti
eta ni aldiz kanpoti;
bihotza erdiratü zeitan so ezti bateki
üdüri ziren haren begiak izar zirela zelüti.

Ene xarmagarria,
hüillan düzü eliza!
zonbait aldiz jinen zira, harat meza entzütera:
begiaz kheiñu eginen dügü ezin mintzatzen bagira.

Españalako bidia
ala bide lüzia!
Gibelilat so'gin eta, hasperrena ardüra
maitettoaz orhit eta nigarra begiala

Hortzak xuri, begiak beltz
ene maitia zeren ez?
mündü orok diozie, ni nizala traidore
orai arren erradazü, hala nizanez bai al'ez.

Amak dio alabari
kanailla txarra hi,
hik behar diina ibili aitoren seme horieki
ibiltekotz ibil hadi, hihaur iidiiri zonbaiteki.
Ama zaude ixilik
ez erran othoi gaizkirik,
gaztetarzuna inozent dela ez dakizia engoitik
zure denboraz orhoit eta ez egin othoi nigarrik.