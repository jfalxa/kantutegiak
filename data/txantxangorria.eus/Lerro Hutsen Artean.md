---
id: tx-1137
izenburua: Lerro Hutsen Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lNPtiqwwFjU
---

Tantas tierras en el mundo, 
Tantos mares por nacer, 
Tanto viento en mi cabeza, 
Tantas cosas por hacer 

Tantos locos en un loco, 
Cuantas malas por vencer, 
Tanto tiempo sin saber, 
La que algún día va a caer 
Tanta gente sin saber, 
La que algún día va a caer 

Hirian trafiko gehiegi, 
Anbulantzien sirena hotsek lagun baten bila 
Entzun ez al dute? 
Presa jabe 

Txorien afonia, naturaren agonia, 
Bortxatu dute ilargia eta ez da entzun garrasia, eguerdian. 

Zabaldu ditzagun ahoz aho lore bihurtutako istorio horiek pantaila okupatu zutela Kezko errezel ta lobbyek 
Jakingo al dugu inoiz, parabolak iraultzen, lerro hutsen artean egiak ezagutzen? 

Zerua zabal zabal, lekuko da eguzkia, ilargia hilzorian, eklipse baten zai dauka, 
Gerora kontatzeko berri bat. zer zebilen bakarrik udako gau hartan? 
Izarrek onartutako erretorika, zeinek zalantzan jarri gertatutakoa? 
Txoriek egin dutela saiakera baten bat, basoko sarean erori dira oharkabean. 

Argi izpiari so, itsutzen 

Hatza seinalatzen duzuela 
Ilargia duzue atzean