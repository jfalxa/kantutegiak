---
id: tx-1652
izenburua: Zergaitik Ixilik Zaude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tkR9vHaJ4Oo
---

Bizirik sentitzen naiz,
gaua beltza den arren
ez daukat beldurrik;
zure eskutik hartuta
egiten badut aurrera
ez dago galtzerik.

Ez ezer ezta inork
ez du inoiz lortuko
egia hiltzerik.
Musika hitza bada,
eta hitza geroa...
zergatik zaude isilik?

Bidean 
urrun gelditzen dira hodei ilunak;
sarritan
esna amestu dudan etorkizuna.

Zurekin, zurekin, bizirik naiz...
zurekin, zurekin.
Zu gabe, zu gabe, noraezean...
ez dut ikusten bidea.

Denboran
hurrun gelditzen dira ideia ilunak;
sarritan
esna amestu dudan etorkizuna.