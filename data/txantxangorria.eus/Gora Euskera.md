---
id: tx-2684
izenburua: Gora Euskera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1FdDqBNQgSA
---

Iparragirreren bertsoak
Aizkibelen goresgarri
Españian da gizon bat
bear deguna maita,
Frantzizko Aizkibel jauna
euskaldunen aita;
txit da gizon prestua
eta jakintsua,
errespeta dezagun
gure maisua Arabe eta hebreo,
denak denak beera!
Nere adiskideak,
gora, gora euskera!
Biotzean gurutza
eskuan bandera,
esan lotsarik gabe
euskaldunak gera!
Ogei ta ainbeste urtetan
bizi da Toledon,
Izarraizko semea
ez da beti lo egon;
liburuen gaiñean
lanean gau ta egun,
gure euskera maitea
galdu ez dezagun. Pakean bizitzeko gure mendietan
euskeraz itz egin bear da
batzarre denetan,
ta euskaldunen izena
geroko eunkietan
famatua izango da
alde guztietan.