---
id: tx-1835
izenburua: Bake Mina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ovenkALUYjQ
---

Gizadiak maite-mina,
Izadiak arreta-mina,
Etorkinak herri-mina,
Herri umek bake-mina.

Laborari ta langile,
Sinestedun ta bilatzaile,
Bidez-bide, kalez-kale,
Izan gaiten bakegile.

Zatiketak etsipena,
Elkarketak itxaropena,
Gorrotoak pairamena,
Bakermenak askapena.

Solasbide, elkarbide,
Maitabide ta askabide,
Zuzenbide, bakebide,
Oro dire zorionbide.