---
id: tx-1132
izenburua: Zazpikoloroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TPS5CG6AyIA
---

Hitzak: IGOR ELORZA
Musika: Xabier Zabala eta Herrikoia

HORIA, LARANJA,
GORRIA,
JAIO DADILA
ZAZPIKOLORO
BERRIA!

MO/RE/A, A/NI/LA,
UR/DI/NA, BER/DE/A,
HO/RI/A, LA/RAN/JA,
GO/RRI/A,
JA/IO DA/DI/LA
ZAZ/PI/KO/LO/RO
BE/RRI/A!