---
id: tx-267
izenburua: Xorrotzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8e8JmvaLeHA
---

Bata mitila zelarik
Bestea nausia
Bena jokütik kanpo
Zer adiskideak
Mezperan gaüa xuritürik
Gorri edatean
Gorritü igantean
Xuriaren txestatzean

Denbora den bezala
errota üngüratzen
Zahartzen hasirik gira,
bena ez ahültzen
Bi botzetan dütügü
kantoreak gozatzen
Hortan gira laketzen,
kobla oihukatzen

Agitzen zaikü üsü
gibel soegitea
Urxantxaren bürüaren gainen
fite txerkatzea
Ahatzerik beitügü,
Urtoroz txapela
Dohakabez jabegoaz
kanbiatzen züala

Bedatse bat izanik, maskaraden botza
Sekülan ez beitügü ützi ofizioa
Xorrotza behin dena beti odolean
Xorrotza behin dena beti dü gogoan

Bedatse bat izanik, maskaraden botza
Sekülan ez beitügü ützi ofizioa
Xorrotza behin dena beti odolean
Xorrotza behin dena beti dü gogoan