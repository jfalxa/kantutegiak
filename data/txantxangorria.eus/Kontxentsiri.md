---
id: tx-1475
izenburua: Kontxentsiri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mPANCJW9AJI
---

Maite bat maitatzen det maitagarria
Begi ederra du 'ta guztiz argia.
        Daukat urruti,
Bainan ezin kendu det burutik
        haren itxura.
Saldu ahal baliteke pisura
        urrearen truke,
nork erosi faltako ez luke.
 
Hogeita lau legoaz nago aparte,
bitartean badaukat milioi bat ate
        guztiak itxirik;
nahi arren ezin egon isilik;
        beti negarrez
nere maite-maitearen galdez,
        ote dan bizi,
bihotz-bihotz nereko Kontxesi.
 
Egunaz argi gutxi, gauean ilun,
kontsuelorik ez da neretzat inun
        maitea gabe;
egin ote nadin haren jabe
        oroitutzean,
zenbat pena nere bihotzean
        ditut igaro;
maite det eta ez da milagro.
 
Ai, hau bakardadea eta tristura!
Nere bihotz gaixoa ezin piztu da,
        hain dago hila!
Beti dabil kontsuelo bila
        bere artean.
Banengo maitearen aldean
        ordutxo biko,
pena guztiak lirake hilko.
 
Nere maitea, zutzaz ni oroitzen naiz
egunaz ere, baita gauetan txit maiz,
        lotan ere bai:
zu ikusitzera nik joan nahi!
        Libre banengo
hor nintzake egun bigarrengo.
        Nahiz orduan hil,
ez nuke izango batere min.
 
Lehengo gau batean egin det amets,
bainan pentsamensua beti aldrebes
        irtetzen dira;
ustez nengoen zuri begira,
        maite polita,
Kofrearen gainean jarrita
        kontu-kontari.
Nahi nuen baina ez nintzan ari.
 
Maite nerea daukat beti gogoan...
Ai, orain banengo ni haren alboan
        inoiz bezala!
Jaunak amets hau kunpli dezala!
        Balitz komeni,
kontsueloz hilko nintzake ni
        nere ustean,
maitetxoaren bihotza ikustean.
 
Nere maite polita, ez da zer etsi,
bihar ez bada ere hor nauzu etzi,
        lehengo lekuan.
Ailegatutzen naizen orduan
        ai hura poza
nere maite-maitetxo bihotza!
        Zuri begira
pena guztiak ahaztuko dira.
 
Zure gauza polit bat hor det nerekin
izkidaturik dago letra birekin;
        C eta B dira,
askotan nago hoiei begira,
        hain dira finak!
Maitetxo polita, zuk eginak
        sedaz ederki,
kolkoan gorderik dauzkat beti.
 
Esperantzetan bizi, maite gozoa,
noizbait kunplituko da gure plazoa.
        Eta orduan,
gauza txarrik ez hartu buruan
        lehengoai utzi,
ez degu pasatzen pena gutxi
        preso sei urtez!
Ondoko gaituzte nere ustez.
 
©