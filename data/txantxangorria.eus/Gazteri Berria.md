---
id: tx-2180
izenburua: Gazteri Berria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MG_vK4Th44o
---

Mixel Labegeriren abestia.
Uztaritzen sortua, Espainiako Gerrarenak eragin handia izan zuen Labegerierengan. Gerraren ondorioz muga zeharkatu zutenen esperientzia bere kontzientzia hartzearen eragilea izan zen. Eugene Goihenetxerekin batera, Francoren kontra jardun zuen. Eskualzaleen Biltzarra elkarteko lehendakari, Euskaltzain urgazle, Gure Herria kultur aldizkariko kolaboratzaile, "Nafarroa, oi Nafarroa" bezalako olerkien egile, idatzi zituen abestiek famatu zuten Labegerie. Euskal kantagintzaren eraberritzailetzat hartua izan da. Ez dok amairu taldean aritu zen eta gazte askoren erreferentzia bilakatu zen.