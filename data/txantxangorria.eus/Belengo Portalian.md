---
id: tx-1488
izenburua: Belengo Portalian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9EeXhzD0LoQ
---

Mesias sarritan agindu zana
aingeru artian dator gugana (bis)

gloria zeruan bakia hemen 
kantari aingeruak dagoz belenen (bis)

kantari aingeruak dagoz belenen