---
id: tx-82
izenburua: Laboa Is Dead
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PtVysqtbq_Q
---

"mikel laboa hil da,
 izarren ahutsa esnifatua, 
sobredosi psicotropikoa

mikel laboa hil da,
 nietzchek esan zuen bezela, 
hil da jaun goikoa

akelarretako estramonioa, 
amanita muskaria 
ta baztango monguiak

hegoak ebaki nizkion,
 etxean nuen zozoari, 
txori beltz mutilatua

pasaiako herritik dator notizia, 
txakurrak korrika, 
genioz bizia

zer izkutatzen dute, 
antidisturbioen poltsikoek, 
lili bat akaso agian

mikel laboa hil da, 
izarren ahutsa esnifatua,
 sobredosi psicotropikoa

mikel laboa hil da, 
nietzchek esan zuen bezela,
 hil da jaun goikoa

zer esaten du ixilik denak,
 ahotsik gabe utzita,
 zentsura inposatua"