---
id: tx-2821
izenburua: Leheneko Denboretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qk-XWDtWkDM
---

Goizetan izotza xuri, behar ari artherikan,
Argiagabe geiñhatûrik behik üztartzen barrukian,
Athelatzen goldiari haurrek phertika eskian
Egüerdi zeñietan zer plazera alhorretan,
Aitak behiak ekhüazirik, buneta hartuz eskietan,
Anjelusak ahapetik emaiten phausada hartan.
Leheneko denboretan gure etxondo maitetan,
lrus bizi ginen, bai, gure lurretan.
Basa bazterretan eta kharriketan
Udan dallü batekilan belhar ephaiten goizetan,
Ekhia epheltü ordüko arestelatze bedaketan,
Belhar sartzen arrestitan, ehatzünak espaldetan.
Ogi-ephaite sasuetan gazte hanitx alhorretan.
Mothikuak ogi joitetan tasto thiari üsantxetan,
Bazen karkaza ederrik aizo junta goxo hetan.
Mantetxetik begi-tresnak hartürik bazkaltü ondun
Altak zian kaseta hartzen, jarten ixil züzülü xokhun,
Haurrak pelotakan ari eskatz mürrin edo karpun
Amak sütondo xokhuan phüntükan ta bethaxükan
Haurrak üngürian kaxetetan hixtoriakan edo drixokan
Alta xü athitxazale zarthanaren üngürakan.