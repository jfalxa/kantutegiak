---
id: tx-2040
izenburua: Korrika Badator
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/toj8bVvR5ds
---

Korrika badator Yeah! yeah! yeah! yeah! yeah!
Badator Korrika, herri bat mugimenduan
Badator ta Big Beñatentzat
Beste ezinezko misio bat
Mundu bat bildu da kontsigna
Ezin huts egin gure antiheroiak
Big beat-a erritmo erraldoiaz
Nor ez du martxan jarriko horrelako deiak?
Gas-Gasteiztik Bai-Baionarat
Mende berri bat
Hasteko dugu Korrika
Buruan mundua
Makina jendea
Pentsatu globalki ta
Ekin lokalki ez al da?
Eskualdunak gara
Eta mundukideak
Uniformetasunetik at Gizakiak
Ez kasko transgenikoak
Ez zenbakiak
Bizirik, mugaegunik ez baitu euskarak
Modernizatzea bada izan gisakoa
Barra kodea tatuatu nire ipurdian
Begira ezazu
 Big Beat Beñat Begira ezazu
 Big Mac behera Korrika badator
Big Beñat buru Uh,uh,uh,uh,uh,uh
 Mundu bat bildu! 
Macdonalizazioa, kultur globalizatua
Kontsumitu plastikoa,
janari azkarra bezala Uztartzea da bat,
Bestea ezarpena
Elkartrukatu edo derrigortu ereduak Big Beñat,
babarrunak eta aza popularra
Suge tako mexikarra eta pa amb tomàquet
Gazta eta intxaurrak,
Lurra ez baita zolua
Bazter dezagun behingoz kaka zahar-zaharra
Ttipi ttapa, ttipi ttapa