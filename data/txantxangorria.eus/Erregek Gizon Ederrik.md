---
id: tx-2317
izenburua: Erregek Gizon Ederrik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y2OcDsTdFUg
---

rregek gizon ederrik guardetan ba-du segurik;
Lekunberri’ko brigadan batto ororen gainetik,
neskatxen engañatzen ez baitu parerik.

Ene maitia, zer duzu? zerk holakotzen zaitu zu?
Ez du denbora luzia zirela penetan sartu…
Plazeraren ondotik desplazera duzu…

Ogiak dira burutu artoak ere jorratu
Primaderan egin lana uda/kenera/t_agertu
Ene maiteñoari gerria loditu.

Mendiak eder belarrez, begitartiak nigarrez…
Lan eginaz urriki dut, bainan probetxurik ez…
Orai hemen nagozu tronpaturik miñez…

Mendian eder arbola luze lerdena…
Zu zirade mundu huntan nik maitatzen zaitudana…
Bisitaturen zaitut ahalik maizena…

Lekunberri’ko neskatxak, Zuberoa’ra ba-noa;
Zuberoa’ko kolonbak deitzen bainau bere gana;
hautatu bear dela zuzenez lehena.