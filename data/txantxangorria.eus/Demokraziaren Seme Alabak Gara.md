---
id: tx-1921
izenburua: Demokraziaren Seme Alabak Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Mbiecbhb7DI
---

komisaldegiak, zerrenda beltzak
kolpez zikinduta, gela ilunak
estolderietan torturatuz,
eraiki dira
bertsio ofizialak diren
estatu ustelen oinarriak

gezurrez pozoindutako komunikabideetan
ez da lekurik guretzat, zapaldu guztientzako
kaioletan isildut, txoriak txori izan nahia
barnean sentitzen diren askatasun oihuak

demokraziaren seme alabak gara
giltzapean heziak
zuentzat, estatutoaren miseriak
baita lege guztiak

irria ahoan, irabaztera goaz
gelditu gabe
gure askatasunaren eguna helduko da