---
id: tx-581
izenburua: Zirrara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BeeayQPJHvM
---

Zirrara sentitzen dut
zure gorputza laztantzean
zirrara gorputzean
zure muxuez oroitzean
zurekin pentsatzeak
barnea korapilatzen nau
irrikako nahia
barnea ase betetzen nau
baina ausardia, ausardiaren falta
somatzen dut barnean desioen
kutxa zabaldu nahiean
beldurra, beldurrak ematen dit
mugatzea atzea, desioen
kutxa zabaldu nahiean
pentsamendutan zurekin bat izango
naizela
zure begietan urtuko naizela
pentsatzeak
zure sakonean
maitatzen nauzula sinistean
egiaren aurrean
biluztu zarenean
...zarenean

zurekin pentsatzeak
barnea korapilatzen nau
irrikako nahia
barnea ase betetzen nau
baina ausardia, ausardiaren falta
somatzen dut barnean desioen
kutxa zabaldu nahiean
beldurra, beldurrak ematen dit
mugatzea atzea, desioen
kutxa zabaldu nahiean
pentsamendutan zurekin bat izango
naizela
zure begietan urtuko naizela
pentsatzeak
zure sakonean
maitatzen nauzula sinistean
egiaren aurrean
biluztu zarenean
...zarenean