---
id: tx-2266
izenburua: Ama Zer Duzu Nigarrez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IHgXCGB_Sgg
---

Ama zer duzu nigarrez
Matelak bustirik urez
Ala duzu goiz ihitzez?
Argia hastian
Lilien artian
Zintzabilenian
Baratze aldian.

Oi jeiki zira goixerik
Haur maitia zeren gatik?
Aita ez da jin oraindik.
Ikusi nahian
Aittatto bidian
Izan niz goizian
Baratze aldian.

Bidia da luze luze
Han bakarrik dabil haize
Amak bihotza du leize
Aita gau guzian
Gudari mendian
Ama dagonian
Baratze aldian.
WeGetIt logo
Ideable logo
diseinua
garapena