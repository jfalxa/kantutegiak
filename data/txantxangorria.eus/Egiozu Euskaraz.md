---
id: tx-1552
izenburua: Egiozu Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/31t6Zlk6cIE
---

Musika: Xabier Zabala

Ezezagun bat baldin baduzu
mintzakide buruz buru
egintzaiozu euskaraz eta
baharbada jakingo du
atzerritar bat tokatuz gero
hurbildu euskaraz behintzat
zeren bestela ulertuko du
euskara ez dela beretzat.

Euskaraz mantso ari den norbait
aurrez aurre badaukazu
hitz egiozu euskaraz mantso 
opari bat egiozu!
Norbaiten esan badizu 
"euskaraz ez dakit hitz egiten nik"
ohartu zaitez ez dizu esan 
entenditzen ez duenik.

Nahiz asko maite nahiz oso geure
sentitzen dugun euskara
inork agindu gabe erderaz 
sarritan geu hasten gara
etzazu egin hainbeste duda 
zeurea euskaraz esan
Edukazio gehiegizkoak 
euskara galdu ez dezan!! 
 
Nahiz asko maite nahiz oso geure
sentitzen dugun euskara
inork agindu gabe erderaz 
sarritan geu hasten gara
etzazu egin hainbeste duda 
zeurea euskaraz esan
Edukazio gehiegizkoak 
euskara galdu ez dezan!! (bis)