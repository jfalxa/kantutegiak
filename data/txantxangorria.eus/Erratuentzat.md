---
id: tx-2393
izenburua: Erratuentzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5_bXnzAVek0
---

Txikitan ikasle txarra nintzen, 
Mutiko asko bezala. 
Eta irakasleek esaten zuten: 
Zu zertan jardungo zara. 
Ez nuen nahi goizean jaiki, 
Ezta ezer ez ikasi. 
Nahi nuena zen marraztu, 
Ta bizitzaz disfrutatu. 

Gero trikitixa aurkitu nuen, 
Arima pixtu zidana. 
Ordu asko sartu ohi nituen, 
Ikasketei ez bezala. 
Jendeak jarraitzen zuen, 
"Zein alferra den" esaten. 
Ez zekitena oraindik, 
Helburua nuela jadanik. 

Orain nire lana musika da, 
Nik beti nahi nuena. 
Eta ikasle onak non daude? 
McDonals baten jotake! 
Nahi dudanean esnatzen naiz, 
Lanak nik aukeratzen maiz. 
Kontuan behar duzuna hartu: 
Gustatzen zaizuna landu!