---
id: tx-1444
izenburua: Txikotak Solte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9BhO_wwDmXU
---

Eguzkia arinago ezkutaten da
Udazkena ete da ate joka
Telefonoa txirrinka aspertzen da
Zuen ahotsa erantzungailuan.

Bakarrik, libre, pozik.
Aukerak aukera zoriontsua izan naiz.
Aparte-apartin baina ala be urrin,
Bakarrik baina beti zuekin.

Ez dakar zertan inork ulertu
Ni neu topaten be nahiko behar.
Zubiak erre, txikotak solte
Haitzen kontra jo arte.

Badira egun batzuk hil naizela
Idatzi zenidan liburuaren gainean etzanda
Egin-ahalak egin bizitza beti motz
Bakarrik, urrun, hotz.


Josu Aranbarri Aramaio