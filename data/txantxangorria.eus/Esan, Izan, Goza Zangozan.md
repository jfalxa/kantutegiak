---
id: tx-2523
izenburua: Esan, Izan, Goza Zangozan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cZ-Uucsrs-o
---

ESAN aspaldidanik gure hizkuntza sortu zela,
ESAN zaindu zutela eta orain lana gurea dela.
IZAN ginen lau katu komentuan hasieran,
IZAN ginen ausartak Txaparro etxe zaharrean.
GOZA genuen euskaraz guraso auzolanean,
GOZA genuen eraikiz ametsa Zangozaldean.
ESAN, IZAN, GOZA (3 aldiz)
ZANGOZAN!
ESAN euskalduna zarela,
ESAN Zangozakoa zarela,
ESAN gure ikastolak bizirik iraungo duela.
IZAN zaitez eredu ikastolan ta kalean,
IZAN gaitezen denok altxorraren zaintzaileak.
GOZA ezazu Leiretik Peñara mendibeheran
GOZA goza dezagun euskalzaleok Zangozaldean.
ESAN, IZAN, GOZA (3 aldiz)
ZANGOZAN!