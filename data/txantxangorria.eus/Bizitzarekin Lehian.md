---
id: tx-2247
izenburua: Bizitzarekin Lehian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oQbv7mGzC7c
---

Abestiaren egilea: Imanol Urkizu






itsasoa, arkaitzarekin 
izaten duen borroka
bizitzarekin gu ere lehian 
batak besteai mokoka
hegan joan zaigun
gure txoria itzultzen da egunero  
lore artian agertzen zaigu 
eguna argitu ezkero 
ur gainean aurkitzen zaitut 
baita ere lehorrean 
ontzia zu zaitugu eta 
zurekin gu arraunean 
hegan joan zaigun
gure txoria itzultzen da egunero  
lore artian agertzen zaigu 
eguna argitu ezkero 
hegan joan zaigun
gure txoria itzultzen da egunero  
lore artian agertzen zaigu 
eguna argitu ezkero 
larai laraira larai laraira 
larai larai lara lara
larai laraira larai laraira 
larai larai lara lara