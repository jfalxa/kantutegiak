---
id: tx-277
izenburua: Xiburutegian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FHcJ_x7hzJw
---

XIBERUTEGIAN
Intro : Egun on ene hiloba maitiak, zien aitak erran dat otto Ddominiken anarkiaren definizioa entzun nahi zinutela. Eh beh, ene adiskideak soizkitzie, denek biloak zikin eta aurpegiak eri !
Xiberutegian ! Han argia, Anarkia Xiberutegian ! 
Jan futreak gitxu, mozkorti zikinak, itsuski drogazale, aitañi nahi dixit bizitu zale ! bordela jakatuz, bilatu troma etilikoa. Nun gogoa han pogoa! Ehun puten broia lohian, horra gure bizitzeko arrazoina. Xure etorkizuna sistema hipokrita hunen ondakinetan atxeman nahi xinuke… Nun anbizioa han bizioa; perspektiba miserablea, ez nuxu bizituko hola. Zoriontasuna astapitoek asmatu kontzeptu ridikuloa. Oztibarre ospitale psikatriko bat da eta hori dugu maite. Ethanola? Eta nola ! Eta nola!!! Ibiakoitz gau huntan marrantak hartu nau, ez didazue haizatuko deabru hau. Duintasuna astapitoek asmatu kontzeptu ridikuloa. Urka mendietan dantzan, sare anti sozialen parrandan, parrokiari tu eginez, erra ere humanum est.
Xiberutegian ! Han argia, Anarkia Xiberutegian ! 
Outro : Attatta…