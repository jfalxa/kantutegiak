---
id: tx-1128
izenburua: Maitasun Aroak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5hUr8Dx4e_I
---

Haurtxo nintzela kontatzen nuen
nola maitemindua nintzen,
guztiek irri egiten zuten ta ikasi nuen ixiltzen.
Gaztetxo nintzen eta nioen,
"Ez dakit nork maiteko nauen!"
Eta nekien hura oroituz eskolatik alde egin nuen.
Maitale baten ixil oroimena,
entzuten ez den hasperena, 
gogoan ixil dabilena, ahaztean gelditzen dena.
Gazte nintzela harrotzen nintzen,
"Nik ezin dut izan baten gai".
Bainan izen bat ixiltzen nuen, 
hura izanik agian bai.
Heldu nintzela aitortzen nion:
"Zutaz besterik ez dut maite!"
Eta ixilik neraman hari ezin erran urrundu zaite!
Maitale baten ixil oroimena
entzuten ez den hasperena
gogoan ixil dabilena ahaztean gelditzen dena.
Azken-azkenik maite nautenek
naramate beste mundura.
Batek negarrik ez du egiten 
ene hilkutxan dator hura.
Maitale baten ixil oroimena,
entzuten ez den hasperena, 
gogoan ixil dabilena ahaztean gelditzen dena!!
........................................­...............
Anje Duhalde jaunaren kanta eder bat, "Bakezaleak" disko borobiletik. Kaki Arkarazo maisua ibili zen soinu mahaiko kontroletan. 

Jakes Ballue, pianoa eta teklatuak
Kaki Arkarazo, gitarra elektrikoa
Anje Duhalde, gitarra akustikoa, koruak, ahotsa
Joxan Goikoetxea, akordeoia
Angel Unzu, mandolina
Mikel Irazoki, baxua
Jimmy Arrabit, bateria, perkusioa

1992 urtea haserrea erakusteko garaia zen Anje Duhalderentzat. "Nazka eginda nago inguruan hainbeste 'bakezale' ikusita, 'bakezaleak' omen ditugunak eta gainera demokratikoki horrela deitzen direnak. Sinestarazi nahi digute haiek direla benetako bakezaleak, eta horien kontra daudenak terroristak eta beste direla. Kanta hau egiterakoan, egia esan, ni Errobin ari nintzeneko garaiko giroaz gogoratu naiz, orain hamasei edo hamazazpi bat urte. Ordutik hona gauzak ez direla gehiegi aldatu esango nuke, edota okerrera joan direla. Orduan faxistak zirenek, eta gaur aurpegi demokratikoa dutenek, pakearen irudia darabilte, eta besteoi ukatu egiten digute. Guzti hori salatzeko egin dut". 

Disko horretan atzerriko musikari miretsien zenbait moldaketa grabatu zituen Duhaldek: Tom Waits ("Jersey girl" ezaguna), John Hiatt ("Gogoaren baitan"), Brassens berriro ("Osaba Artxibald", "Gaixo Mattin")... Diskoari izena ematen dion abestiaren bertso elektrikoan Fermin Muguruzaren laguntza izan zuen kantatzeko orduan.

Hitzari garrantzi handia ematen dio Duhaldek, sinetsi behar du testuak esaten duen horretan kantura eramateko. Hitzak onak ez badira kantua ere ez dela uste du Duhaldek. Halaber, artistaren engaiamendua aldarrikatzen du. "Maiz erran dut kantari inplikatu bat nintzela eta nere ideien kontra ez nuela deus eginen. Beti inporta izan zait nerekiko koherente izatea. Hala baldin bazara, publikoarekin ere hala zara"