---
id: tx-2713
izenburua: 7 Katu Hamar Urte Akatz Eta La Basu+emausekoritx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aj_RsrQfeJ4
---

Kalean dena zen grisa
titaniozko kaosa
pobreziaren baldosa
gazte prekarioen txisa,
galduta hainbeste katu
kaleko kantoiak haina
independenteak baina
ezin independizatu,
beti beherantza begira
ezin galdu esperantza
eskaileratan gorantza
badago alternatiba

Betiko txakurrek guau, guaau
eurek hori ta guk hau
arauak desarau, haau
egunez eta gau ,
marrua edo miau, miaau
borrokak hor dirau
Bat, bi, hiru, lau… 7 katu baino gehio.

Indibiduoari begira
dago munduan trantzean
7 katu gaztetxean
bat eta bat zazpi dira
antolatzeko librea
prest joateko gerrara
abiapuntu ederra da
10 urteko bidea
10 urtetaz kolorez
pintatzen gazte ametsak
uniformetasun beltzak
gorriz, berdez, horiz, morez

Betiko txakurrek guau, guaau
eurek hori ta guk hau arauak desarau, haau
egunez eta gau marrua edo miau, miaau
borrokak hor dirau
Bat, bi, hiru, lau… 7 katu baino gehio.

Gora gure borroka
Soka tira, tira soka
Hamarkada galanta
gure etxean, aizu, topa
Txakurrak kanpora
Alde hemendik ospa
7katu, 7 kale
dira gure erronka

Gora, gora, Errondan gora
katuak kalean
beti daude gure alboan,
alternatiba berriak, autogestioa,
Jo ta ke sua, fyah, okupazioa.
Borrokalariak aurrera bagoaz,
gauzak aldatu egin behar, nola ba!

Lau, hiru, bi, bat
etorri gure aldera
hauxe da momentua,
ban barran barra,
gu kaleko indarra
Alde Zaharreko euskaldun garra, farra,
erresistentzia da bide bakarra
erresistentzia da bide bakarra

Gaztetxea, nire etxea, zure etxea, gure etxea, koloreztatzen gabiltza kalea
Gorria, laranja edo morea, bestela berdea
Ez ditugu nahi zuen kolore grisak, kea.
Gazteak elkartu, harremanak sortu,
Bilboko Alde Zaharra berreskuratu eta indartu,
Arauak ez onartu, kaiolak zer? Apurtu
Beste hamar urte hemen gutxienez egin apustu.

Jira ta bira badirudi sokatira,
Presoak herrira, txakurrak nora? Mendira,
Alkateen panpinak sentirazten digu mina,
zuen irrifarrak guretzat ez dira atseginak.

Ikusten da Bilbo osoa, eskilaretatik gora,
Que no te lo cuenten, etorri zu, zeu, froga.

Baga, biga , higa, laga boga,
NOLA?
Gu gara piratak,
Gaztetxea da gure altxorra.

Betiko txakurrek guau, guaau
eurek hori ta guk hau arauak desarau, haau
egunez eta gau marrua edo miau, miaau
borrokak hor dirau
Bat, bi, hiru, lau… 7 katu baino gehio.