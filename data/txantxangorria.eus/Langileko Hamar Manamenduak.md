---
id: tx-907
izenburua: Langileko Hamar Manamenduak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/etpxgvJ9wp4
---

Nagusiak dituzu maitatuko
bai eta
ahal bezain ongi zerbitzatuko.

Lanaren hasteko eta uzteko
orduak
dituzu beti errespetatuko.

Lanean denborarik ez galtzeko
ez zara
inoiz lagunekin solastatuko.

Nagusiak arrazoin duelako
denetan
ez zara haren kontra mintzatuko.

Telefona ez duzu hunkituko
ahalaz
kanpotik inork ez zaitu deituko.

Ongi irabazten duzulako
ez duzu
emendazionerik galdatuko.

Lan presatu baten antzinatzeko
beharrez
igandetan artzea onartuko.

Nagusiak hazten zaituelako
agian
ez diozu ezerrez ebatsiko.

Nagusiak baitira beharrezko
sekulan
ez duzu haien lana trabatuko.

Ta gauean langileek eskainiko ... ("Zer eskainiko?")
pottotta ta ipurdi
nagusiarendako ... (bis)