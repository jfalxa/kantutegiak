---
id: tx-1456
izenburua: Amaigabeko Gaua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q-FJd2BpisA
---

Bart gauean deitu zuen
hitz egin nahi zuela
tabernan gelditu ginen
garagardoa biontzat
amaigabeko gaua

Bihotzak larrutuz 
eskuak trukatuz
biak bat izaten
amaitu genuen
esatekoa zena
esaterik ez zegoen
zauri arteko hitzak 
musu bihurtuta

Orduak igaro ziren
giro atseginean
inguruko giro epelak 
baita musika egokiak
bidea erreztu zuten

Bihotzak larrutuz
.........
......

Uste nuen halakoak pelikuletan
bakarrik gertatzen zirela
uste nuen halakoak abestiak egiteko
asmatzen zirela
azkenean egi bihurtu zen
eta hotza zegoena berotzen hasi zen
bere etxean amaitu genuen

Bihotzak larrutuz
..........
.....