---
id: tx-1463
izenburua: Ai Mari Migel
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wFPaFT5RGu8
---

Ai Mari Migel!
nun don senargei hori?
Ai Mari Migel
noiz don eztegue?

Ai Mari Migel!
nun don senargei hori?
Ai Mari Migel
noiz don eztegue?

Zapi ta zapi esanagaitik
ezin dot kendu katue,
sekula bere eztot ikusi
honelan neure burue.

Zapi ta zapi esanagaitik
ezin dot kendu katue,
sekula bere eztot ikusi
honelan neure burue.

Ai Mari Migel!
non dona alkondarie?
Ai, Mari Migel,
non dona katue?

Ai Mari Migel!
non dona alkondarie?
Ai, Mari Migel,
non dona katue?

Zapi ta zapi esanagaitik
ezin dot kendu katue,
sekula bere ez dot ikusi
honelan neure burue.

Zapi ta zapi esanagaitik
ezin dot kendu katue,
sekula bere ez dot ikusi
honelan neure burue.

Mari trapuzar
iñar-arteko sasiartean jaioa
Mari trapuzar
iñar-arteko sasiartean jaioa
egundo bere ez don ikusi
[h]eure garrian gorua
egundo bere ez don ikusi
[h]eure garrian gorua

Mari trapuzar
iñar-arteko sasiartean jaioa
Mari trapuzar
iñar-arteko sasiartean jaioa
egundo bere ez don ikusi
[h]eure garrian gorua
egundo bere ez don ikusi
[h]eure garrian gorua