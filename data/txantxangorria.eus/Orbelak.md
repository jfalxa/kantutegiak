---
id: tx-2368
izenburua: Orbelak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UhGsnKmP480
---

ORBELAK (Deustu/Sani, Euskal Herria) 2019.

Maketaren aurrerapen abestia, Basauriko Txarraska Gaztetxean grabatu eta ekoiztua Asier Salazarren eskutik. Bideoaren zuzendaritza eta grabaketa Aitor Garcia-k eta Jon Salegi-k egin dute, 2018ko abenduaren 23an Deustualdeko zenbait gunetan.

BIDEOA:
Jon Salegi
Instagram: @_salegi
Ekinanekin
Instagram: @ekinanekin

GRABAKETA:
-Asier Salazar: (618 03 49 77)
Facebook: Asier Sani
salazar.asier@gmail.com

Eskerrak eman nahi dizkiogu maketaren diseinuan aritu diren pertsonei:

-Jon Lorenzo:
Instagram: @jonlo_07
jonl1a1011@gmail.com

-Ibon Montaño

KONTAKTUA:
-Instagram eta twitter: @orbelak_taldea

orbelaktaldea@gmail.com


-------------LETRA--------------

Hau berri ona, hainbeste txarren artean
Adar ximelduan hostoa heldu da
Jausi baino lehen, ozen entzun dezatela gure aldarri garratza
Bulegotatik kea salduz, bizi diren horien aurka
Tamalez, berben prezioa garesti da merkatu hontan

Indarraren akordea jendaurrean ezkutua
Zerutik orbelak jausten diren bitartean
Bata bestearen kontra hosto beltzez jantzita
Zerutik orbelak jausten diren bitartean

Arnasa anabasan, itxurazko bake honetan
Zulo beltzean jaustea errazegia baita
Zoroen talde batek zioen moduan
Ez dakigu nora goazen baina behetik irtengo gara

Hostoa behin erorita udazkena hasiko, ta oihukatu dezagun garai ilunak argitzeko, atzean geratu dira kanta eta gudariak, halere, hau ez da azkena izango ziur nago

Indarraren akordea jendaurrean ezkutua
Zerutik orbelak jausten diren bitartean
Bata bestearen kontra hosto beltzez jantzita
Zerutik orbelak jausten diren bitartean