---
id: tx-1036
izenburua: Erregeak Datoz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kxC9DOl56A8
---

Abesti monarkikoa. Zer egingo dugu? Gure olentzero errepublikanoari konpetentzia egiten dioten monarkek. Hala ere, GORA EUSKAL ERREPUBLIKA!!!

Erregeak datoz presaz Belena
guk bila dezagun, ahal den lehena
Erregeak datoz, erregeak d/toz,
presaz Belena presaz Belena
guk bila dezagun guk bila dezagun
ahal den lehena
ahal den lehena
Ate txokoan oilar bi
batak bestea dirudi,
etxe honetako, etxeko andreak
a/ma bir/ji/na di/ru/di
a/ma bir/ji/na, a/ma bir/ji/na
zu za/ra ze/ru/ko e/rre/gi/na.
E/rre/ge/ak da/toz pre/saz Be/le/na
guk bi/la de/za/gun, a/hal den le/he/na
E/rre/ge/ak da/toz, e/rre/ge/ak da/toz,
pre/saz Be/le/na pre/saz Be/le/na
guk bi/la de/za/gun guk bi/la de/za/gun
a/hal den le/he/na
a/hal den le/he/na
A/te/an e/der e/pe/lez
a/tal bu/ru/a zi/la/rrez,
na/gu/si jau/na e/san be/zai/gu
ha/si/ko ga/ren a/la ez
na/gu/si jau/na,
na/gu/si jau/na,
ga/bon e/ma/nez gatoz gu/ga/na
Tra/ka/tan, Tra/ka/tan, hi/ru e/rre/ge
Tra/ka/tan, Tra/ka/tan, Be/le/ne/ra
Tra/ka/tan, Tra/ka/tan, hi/ru e/rre/ge
Tra/ka/tan, Tra/ka/tan, Be/le/ne/ra
Mel/txo/rrek Tra/ka/tan, zal/di/a bel/tza
Gax/pa/rrek Tra/ka/tan, zal/di go/rri
Tra/ka/tan, Tra/ka/tan Bal/ta/xar za/ha/rrak,
zal/di/a Tra/ka/tran zu/ri zu/ri
Tra/ka/tan, Tra/ka/tan, hi/ru e/rre/ge
Tra/ka/tan, Tra/ka/tan, Be/le/ne/ra
Tra/ka/tan, Tra/ka/tan, hi/ru e/rre/ge
Tra/ka/tan, Tra/ka/tan, Be/le/ne/ra
Be/le/nen ja/io den haur/txo po/li/ta
Tra/ka/tan, Tra/ka/tan, jau/res/te/ra
Tra/ka/tan, Tra/ka/tan, za/pa/ta/txo/an
jos/tai/lu po/li/tak e/ma/te/ra