---
id: tx-1833
izenburua: Baga, Biga, Klik!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hywE_v_piTA
---

Xirristi-mirristi salda
kikili zopa olio plat
gerrena urrup edan edo klik
klikiki mili kili klikik
kliklik
 
Xirristi-mirristi salda
kikili zopa olio plat
gerrena urrup edan edo...
klikiki mili kili klikik kliklik
 
Xirristi-mirristi salda
kikili zopa olio plat
gerrena urrup edan edo...
 
Xirristi-mirristi salda
kikili zopa olio plat
gerrena urrup edan edo...
 
Biga, biga, baga, biga,
higa, laga, boga, sega,
zai, zai, zoi, zai, zai, zoi,
bele, harma, tiro, pun!
 
Biga, biga, baga, biga,
higa, laga, boga, sega,
zai, zai, zoi, zai, zai, zoi,
bele, harma, tiro, pun!
 
(Berriz)
 
Tiro pun!
Segi!
 
Biga, biga, baga, biga,
higa, laga, boga, sega,
zai, zai, zoi, zai, zai, zoi,
bele, harma, tiro, pun!
 
(Berriz 3 aldiz gehiago)
 
Tiro pun!
Pun! Pun! Pun!