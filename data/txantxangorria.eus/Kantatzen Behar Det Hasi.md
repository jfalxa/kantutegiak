---
id: tx-2467
izenburua: Kantatzen Behar Det Hasi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XGrcpYoGfe0
---

I.Kantatzen bear det asi nola bear den ikasi,
len interesa franko banuen, orain joan zaizkit igasi,
andrea egin zaut nagusi saltzea luke merezi,
iñok nai balu erosi.

II.Ni nago morroi serbitzen besteren kargak berdintzen;
andrea ola ametitzeak eztu legeak agintzen;
asiagatik urdintzen gazteak dio begitzen
¿nork daki nola dabiltzen?

III.Etxera natorrenean, beti edo geienean
embusteriaz engañatzen nau dirua asmatzen duenean;
malezia barrenean eta gero azkenean
jartzeko aren gainean."