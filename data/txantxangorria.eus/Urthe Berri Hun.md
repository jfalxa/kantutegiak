---
id: tx-184
izenburua: Urthe Berri Hun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vKjO52xECzA
---

Aize hegoa itzülikaz da iraixten bortün behera, 
Aro gaitzarek nolaxek ez dü xahatü itsas bazterra, Abentiarek nunbait behar dü egotxi bere imur tzarra, Seinalatzeko akabatzera duala urte zaharra. 
Adio urte zaharrari, agur eder bat berriari, Boztario et’osagarri desiratzen batak bestiari ! 
Dolürik gabe adios, arren, denek urte zaharrari, Ezperantza güziak ezariz heltzen zaikün berriari, Ekar dizagün bakiarekin, koraje penan denari, Xantza hun bat gure familietan ‘ta ororer osagarri. 
Umore huna begira dezan beti bezala Xiberun, Liberti gitin, alagerakin, bortü eder horien ondun, Tradizione zahar horiek edüki ditzagün gogun, 
Oro akortin bizi gitian. Güzier urte berri hun! 
Hitzak: ETXAHUN Iruri eta Dominika ETCHART Müsika : Jean ETCHART