---
id: tx-3217
izenburua: Gora Gora Betizu -Betizu Taldea-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jORoRpaUrx0
---

Uuuuu,dantzatu nahi baduzu,uuuuu, aurrera Betizu.Gora, gora, gora, gora hei! hei! hei!Behera, behera, behera, behera hei! hei! hei!Gora, gora, gora, gora Betizu!Behera, behera, behera, behera hei! hei! hei!Gori, gori, gori, haa,gure jaia hasi da,giro, giro, giro, haa,goazen dantzatzera;Uuuuu,dantzatu nahi baduzu,uuuuu, aurrera Betizu.Asko dago esateko,asko dago pentsatzeko,baina kantu hau da soilikelkarrekin dantzatzeko.Gora, gora, gora, gora hei! hei! hei!Behera, behera, behera, behera hei! hei! hei!Gora, gora, gora, gora Betizu!Behera, behera, behera, behera hei! hei! hei!