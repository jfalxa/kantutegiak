---
id: tx-469
izenburua: Gure Astoa Balaan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2TOdoEDOD3Y
---

Gure astoa Balaan, izan zerauku elizan

Asto koxkor bat zela debozionetan
Sartu zen elizako koro barne hetan
Eta ihanka lotu bere otoitzetan
Aldareko buketak ausikiz artetan

Gure astoa Balaan, izan zerauku elizan

Utzirik lo kuluxka ta konfesionala
Jaun ertorak hartu du kurutze makila
Eta hartaz zanpatuz debruetarat zoala
Elizatik haizatu oi gure kabala!

Gure astoa Balaan, izan zerauku elizan

Zergatik ez den haizu elizan astorik
Pentsaketan ari da Balaan geroztik
Orroit baita asto bat beretter izanik
Lehen eguberria gertatu zelarik

Gure astoa Balaan, izan zerauku elizan