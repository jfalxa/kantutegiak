---
id: tx-1077
izenburua: Kurrela Baten Aitormenak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GDNUYhyrSqE
---

Igande gaba berriz
bihar goizean izorrai
ez daukat batere gogorik
beste baten onerako
produzitzen hasteko
ez dago eskubiderik
gauzak honela daude
onartzen dudalako
kontuan harturik
hau ez dela betirako
kontuan harturik
hau ez dela betirako!!!

Automatikoki ari naiz lanean
programaturik nago
nire aurreko hau
edo ni naiz makina?
hori zalantzan dago
erlojuari begira
denbora gelditu da
hau ez da inoiz bukatzen
eta ni behea jota
ez da inoiz bukatzen
eta ni behea jota!!!