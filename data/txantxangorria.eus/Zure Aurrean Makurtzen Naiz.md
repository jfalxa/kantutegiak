---
id: tx-1338
izenburua: Zure Aurrean Makurtzen Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S1eJfwsq-Kk
---

Ikus laguna
goiza heltzean
atzean utzi duguna
ixiltasuna
eta berarekin
gauaren lasaitasuna
zuk irtetzen duzunean
mamu beltz bat agertzen da
batzuentzat beldurgarria
besteentzat zoragarria
agertuko da argia
iluntasuna joatean
hasiko da egun berri bat
zuk gaua nahi duzunean
entzun gaua
eraman zurekin
zure aurrean
makurtzen naiz.