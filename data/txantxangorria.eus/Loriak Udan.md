---
id: tx-2668
izenburua: Loriak Udan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iAcJYkqA_7I
---

1. Loriak udan intza bezala maite det dama gazte bat.
Ari adinbat nai diotenik ezta munduan beste bat.
Iñoiz edo bein pasatzen badet ikusi gabe aste bat,
Biotz guzia banatutzen zait alango gauza triste bat.

2.-Neskatxa gazte, paregabia, apirilako arrosa,
Izarra bezein diztizaria, txori bezein airosa.
Oraintxe bañon gusto geyago nik ezin nezake goza;
Zorionian ikusten zaitut, nere biotzak au poza!

3.-Ez aldidazu aintzik ematen nik zaitudala naiago,
Jai mariñelak gau ilunian izarra baño geiago?
Nere onduan zaitudalakotz pozez zoraturik nago,
Zu ikusiak konsolatu nau, triste neguen lenago.

4.-Nik ainbat iñork nai zutenik arren etzazula uste,
Nere begiek beren aurrian beti desio zaituzte.
Eguzkirikan ikusi gabe txoria egoten da triste,
Ni ez nau ezerk alegratutzen zu ikustiaz ainbertze.

5.-Aurpegi fiña, gorputza, berriz, eztago zer esanikan;
Izketan ere grazi ederra, ezer eztuzu tatxikan.
Mundu guzia miratu ta're zu bezelako damikan,
Agian izan laiteke, bañon ez det sinesten danikan.

6.-Nere betiko pensamentua, nere konsolagarria.
Zu gabetanik ezin bizi naiz, esaten dizut egia.
Zu baziñake arbola, eta ni banintzake txoria,
Zu ziñaden arbola artantxen egingo nuke kabia.

7.-Amodiua nere biotzak zurenganonuntz darama.
Erri guzian zeren daukazu neskatxa fiñaren fama.
Beste fortunik mundu onetan nik deseiratzen dedana,
Aur batek berriz izan gaitzala, ni aita eta zu ama.

8.-Falta duenak logratutzeko itz egitia txit ondo,
Eta nik ere sayatu bear ote egin dazken konpondu.
Gaur nagon bezein atrebitua sekulan ez naiz egondu:
Orgatik Lopez galdetzen dizut nerekin naizun ezkondu. (kolpez)

9.-Ezkondutziak izan bear du preziso gauza txarren bat.
Ala esaten ari zait beti nere konsejatzale bat.
Alaxen ere aren esanak oso utzirik alde bat,
Ongi pozik artu nezake zu bezalako andre bat.

10.-Zegorrek ere ongi dakizu aspaldi ontan nagola
Zuregatikan penak sufritzen bañan ordean au nola.
Alaxen ere nigana ezin biguindu zaitut iñola:
Ni zuretako argizaria, zu neretako marmola.

11.-Nere biotza urtzen dijua eta ezta mixterio.
Penaren kargak estutu eta zumua kendutzen dio.
Begiyak dauzkat gau eta egun eginikan bi errio;
Beti negarra dariokela zu zeralako medio.

12.-Zu zeralako medio juaten ba naiz lur azpira,
Gero damuak eta malkuak alferrik izango dira.
Bein juan ez geroz oyen birtutez berriz ez naiteke jira.
Ori gertatu baño lenago izazu nitaz kupira."