---
id: tx-2646
izenburua: Erdeuskera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ST3p1FMyqoc
---

Kalean beti entzuten dut,
gaztelania euskara kenduz,
denbora osoa nere ondoan,
amorru mordoa sentitzen dut.

Euskera matenduz trinko ta sendo,
horrek bakarrik bereizten du,
guk dugun miña gure ikurriña,
jende guztietatik nor garen gu.

Beti entzuten; 
“niri euskara ez zait gustatzen”.

Hasi naiz nazkatzen, ez dira ohartzen,
horrela hizkuntzak, hasten dira galtzen.

Ez izan zerria mugitu gerria,
errima larria abesti berria,
altxatu gerria zabaldu berria,
denok izagutzeko aberria,
nahi ta nahi ez.

Bestela euskara dago negarrez,
batzuk frantsesez besteak inglesez,
egindako lan guztia errez,
guk baietz, beraiek ezetz, 
berak ez, guk baietz.

Zer esango digute haiek, 
euskara suntsitu nahi dutenek...

Muahahahahahaha...

On the middle of the night, you and I, 
we’re ready to be high, smiling to the life, 
flying to the sky, oh yeah!
(...)

Reggae musikarekin sistema aldatuz,
kantak sortuz rimak sartuz,
reggae ta rap-a uztartuz,
musika protestarako badela onartuz,
kanten bidez gure askatasuna aldarrikatuz.

Reggae musikarekin sistema aldatuz,
kantak sortuz, rimak sartuz,
reggae ta rap-a uztartuz,
musika protestarako badela onartuz,
kanten bidez gure askatasuna aldarrikatuz.
Kanten bidez gure askatasuna aldarrikatuuuz.

Euskara suntsitu nahi dutenek,
Muahahahahahaah...

On the middle of the night, you and I, 
we’re ready to be high, smiling to the life, 
flying to the sky, oh yeah!
(...)