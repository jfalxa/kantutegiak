---
id: tx-2456
izenburua: Iparra Galdu Barik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2qs0GTzBjsw
---

Irailaren amaieran (Huarteko txoznetan) grabatuta, Jon Corres eta Josu Etxarriren partez (corresjon@gmail.com)


Kontaktua:
Beñat (666 09 69 04)
Matxi (606 70 61 59)
Twitter: @herdoil_taldea



Letra:

Bizitasunak! Mugatzen digun! Egunerokotasuna!
Giltzapean! Bahitutako askatasuna!
Isiltasunan! Ahaztutako oihuak!

GAZTE MUGITU ZAITEZ!
LORTZEKO, ALTERNATIBA BERRIAK!
IPARRA GALDU BARIK, BORROKAN!
(TINKO!) GELDIEZINAK!

Gazte aktiboak! Herri boterean!
Beharrezkoak diren sustraiak!
Erori izan arren, beti altxatu izanak!
Ekartasuna da gure arma!

GAZTE MUGITU ZAITEZ!
LORTZEKO, PROIEKTU BERRIAK
IPARRA GALDU BARIK, BORROKAN!
(TINKO!) GELDIEZINAK!

(Gaur egungo, gizarte ustel honen, ezjakintasunari aurre egiteko,ezinbestekoak diren gazte adimenak esnatu eta ametsak errotzeko asanbladak sortu, gurea zena berreskuratzeko egin behar dena egingo dugu!) AMORRUA ANTOLATU BORROKARI EKIN!

UOOHOOHOO! UOOHOOHOO! UOOHOOHOO! X4