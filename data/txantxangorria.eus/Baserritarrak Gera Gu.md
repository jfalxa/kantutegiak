---
id: tx-59
izenburua: Baserritarrak Gera Gu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dlWHSGmhSWk
---

Baserritarrak gera gu
ezin gentzake ukatu.
Baina kaletarrak bezin ongi
soinua jotzen degu.

Le! le, le, re, le, re, le, le, re
Le, le, le, re
Le! le, le, re, le, re, le, le, re
Le, le, le, re
Le! le, le, re, le, re, le, le, re
Le, le, le, re
Le! le, le, le, re, le

Goizean goiz behia jaitsi
gosaria geroko utzi
Goizean goiz behia jaitsi
gosaria geroko utzi
Lana sobrante izan dugu
Lana sobrante izan dugu
eta deskantso g/txi
eta deskantso gutxi