---
id: tx-87
izenburua: Astelehenetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0csouK7coik
---

Musika eta hitzak: Olaia Inziarte
Iker Laurobarekin grabatua.

Perkusioa: Josu Erviti
Kitarra elektrikoa: Ander Zubillaga

HITZAK:

Mendilerro bat zulatua harrobi batekin
Makroproiektu baten amuan gezurra zintzilik
Neskatzat duten mutiko bat lotsatua disforiagatik
Basorik handiena suak hartua eta denak ixilik

Hartu nahi al duzu zerbait nirekin lana eta gero
munduaz eta gutaz solasteko? Beharra badago...
Badakidan arren astelehenetan bizitzeko gogoa izatea iraultzailea dela
ulertuko dut bakarrik egon nahi baduzu

Apustu bat da itsasoa edozein espezierentzat
Lau euro kobratzen du ordua, eta ezin erosi sendagaiak
behar bereziak dituen alabarentzat

Hartu nahi al duzu zerbait nirekin lana eta gero
munduaz eta gutaz solasteko? Beharra badago...
Badakidan arren astelehenetan bizitzeko gogoa izatea iraultzailea dela
ulertuko dut bakarrik egon nahi baduzu,
nik ere hala nahi izaten dut

Lo egin nahi baduzu gauetan ezin duzulako
hurrengo kafean solastuko dugu,
idatzi iezadazu