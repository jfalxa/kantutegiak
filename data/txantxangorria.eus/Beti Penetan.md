---
id: tx-61
izenburua: Beti Penetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Sbv4IF7aiVE
---

Beti penetan, beti penetan
bizi naiz mundu unetan.
Egunaz zerbait alegratzen naiz,
gabaz beti penetan:
Neurenganako amorioa
joan zitzaitzun batetan.