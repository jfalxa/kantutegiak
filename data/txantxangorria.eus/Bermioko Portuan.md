---
id: tx-2062
izenburua: Bermioko Portuan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uMueAzWN930
---

Bermioko portuan
goizeko ordu bietan (bis)
Ez da ez besterik ikusten
Ez da ez besterik ikusten
txo, harrapaixu batela!

Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
porque lo he visto yo.
Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
parce que moi je lai vu.

Arratsateko oskorritik
goizeko oskorriraino
gauean ez da entzunen
ez da entzunen gauean
gauean ez da entzunen
sorgin musika baino.

Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
porque lo he visto yo.

Gezurra dirudi baina
sano ta fresko gaude.
Musika da denboraren
denboraren musika da
musika da denboraren
sekretuaren jabe.

Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
parce que moi je lai vu.

Gazte odola duenak
festa eta algara,
hamabost urtez oraindik
oraindik hamabost urtez
hamabost urtez oraindik
hasi berriak gara.

Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
parce que moi je lai vu.
Egia da, egia da,
neuk ikusi dudalako.
Egia da, egia da,
porque lo he visto yo.