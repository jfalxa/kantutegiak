---
id: tx-1002
izenburua: Ur Berri Urte Berri. Hela! Hela!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C71kDPrpiHY
---

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Damatxo gazte konponitua
Hoiek kolore gorriak
Zure mateilak iduritzentzaz
Klabelinaren orriak.

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Sentitzen zaitut sentitzen
sala berrian altxatzen,
zure oin txiki politen hotsa
hemendixen dut aditzen.

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Hemendixen aditzen ta 
eskaileretan jeitxitzen) 
urdai azpia buruan eta 
lukainka parea eskuen.

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Hor goien goien iturri 
Ura da txorrotik etorri, 
etxe honetako zaldun gazteak 
hamalau urre zintzarri. 

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Hamalau urre zintzarrirekin 
zazpi damaren eguzki, 
hoiek danak hala izanik 
gehiago ditu merezi.

Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?

Heila, Heila! 


Heila, Heila! Nor da nor da?
Ni naiz, urteberri
zer dakartzu berri?


Heila, Heila! 
#saveyourinternet