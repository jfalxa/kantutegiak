---
id: tx-2111
izenburua: Euskalerri Nerea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UqpsCwuYSSM
---

Euskal Herri nerea 
ezin zaitut maite 
baina nun biziko naiz 
zugandik aparte. 
Anaien aurpegian 
begirada hotza, 
bihotz berotasuna 
izaten da motza.

Arratsaldeak beti dakarren tristura, 
neretzat izaten da bakardade hura. 
Argitu nahirik begiak, balio al du gure egiak? 
Bide bat aukeratzean, gelditu nahi nuke nere lurrean.

Euskal Herri nerea 
ezin zaitut maite 
baina nun biziko naiz 
zugandik aparte.

Ezin erabakirik esandako hitzak, 
pauso baldarrak ditu gizonen bizitzak. 
Neurri zuzen baten beharrak lotutzen dizkit nere indarrak. 
Erreka zabal garbia zugan egon bedi gure argia.

Euskal Herri nerea behar zaitut maite, 
nun biziko nintzake zugandik aparte. 
Anaien aurpegian begirada hotza, 
bihotz berotasuna izaten da motza.

Urruti joan ezkero berriz etortzean 
nolako zulo beltza nere bihotzean. 
Egin dezagun aurrera, gerra hontan saiatuko gera. 
Tristea etxerik eza, lurrak emango dit bere babesa.

Euskal Herri nerea behar...