---
id: tx-2401
izenburua: Euskararen Bihozfera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9HesatbCOTI
---

Bideo grabazioak: Zigor Etxebarria, Nerea Andres
Edizio lanak: Hannot Mintegia
Izenburua: Euskararen Bihozfera
Musika: Jon Urriolabeitia
Letra:  Galder Urzaa
Bateria: Gorka Leon
Baxua:  Joseba Urruela
Gitarra: Jon Urriolabeitia
Hamonnda: Mikel Nuñez
Tronboia: Aratz Diez Garmendia
Tronpeta: Gorka Fernandez
Saxoa:  Eguzkiñe Jorrin
Abeslariak: Jon Urriolabeitia, Iñigo Eiguren, Gentza Madariaga, Itziar Muniategi eta Antton Uriarte.
Koruak: Maddi Gerrikaetxebarria, Liloye Richard, Naia Ellis, Olatz Erkoreka, Nerea Ordeñana eta Naiara Sanchez.

Errealizazio eta gidoia: Oier Plaza
Produkzio zuzendaria: Gotzon Bareño
Kamera-lanak: Oier Plaza, Fermin Aio eta Ager Galarza
Errealizadore-laguntzailea: Aitor Ferrer
Produkzioa idazkaria: Eider Plaza
Aire-irudi operadorea: Dani Ojanguren 
Aire-irudien kamera-operadorea: Kerman Goikuria. 
Muntaia:Hannot Mintegia, Oier Plaza eta Fermin Aio  
Itsasontzi protagonista: Urandere 
B itsasontzia: Jon Etxebarria
Protagonista nagusiak: Seber Altubeko LH4. mailako Peru Arteagabeitia, Amets Azula, Beñat Zobaran, Izar Larruzea, Eneko Arrien, Goizalde Eizagirre.





Seber Altubeko leiho-begietatik
Euskal Herriko lau haizetara
ikastola eta euskarari omenaldi bat
jalgi direlako plazara

Urdaibai biosfera
euskararekiko bihozbera
bihotzetik bihotzera
euskararen bihozfera

Bidean gauden seinale Ibilaldia
bostekoa emateko unea
aldapa eta oztopoen erdian
hegaldia berregiteko etena

Urdaibai biosfera
euskararekiko bihozbera
bihotzetik bihotzera
euskararen bihozfera

Oinatzak, ametsak…
Atzokoa, biharkoa…
Ikastolen hatsa,
belaunaldiz belaunaldikoa

Urdaibai biosfera
euskararen bihozfera