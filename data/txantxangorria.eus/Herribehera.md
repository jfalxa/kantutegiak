---
id: tx-691
izenburua: Herribehera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OonCPwyJVvc
---

Herribehera Herribehera
Zure landen zabalera
Ortzi-muga den hartan mugatzen da.
Zure lur emankorretan
Ixurtzen diren asmoak
Gogotsu hartuko ahal ditu lur gozoak.
Zure gaztelu zaharrek
Gorderik duten aintzina
Hats tristetan mintzo da haren mina.
Oohh oohhh ooohhh
Horma zahar arraituetan
Xoriak dira kantatzen
Mendetako lo geldia salatzen.
Nafarroa anaia zaharra
Kondairaren lehen sustarra
Bego higan arbasoen amets hura.