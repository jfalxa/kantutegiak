---
id: tx-178
izenburua: Erdian Ere Nafarroa Oinez 2022
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uRU0miuGRnw
---

Tafallako Ikastolak 2022an bere 4. Nafarroa Oineza ospatuko du, eta bete berri du bere 50+2 urteurrena. Bideoklip honetan goraipatu nahi izan da, azken 50 urte hauetan ikastola komunitateak egindako lana. Erdialdean euskara ereiten, gogoratuz, lanean jarraitu beharra dagoela. 
 Nafarroa Oinez URRIAREN16an, Denak Tafallara!

Entzun ditugu gurasoen zirraraz betetako ipuinak
ta euri berriak ziprintindu du gure gogoa
Oinez edo hegaz egin genituen lehen irriak
Mila haize harrotuen aurka borrokan
Urjauzi  bat basamortuan

Iragan ederrena da zuek egindako guztia
traben lurzoru gogorra laiekin irauliaz
97ko ume koxkor baten begiak
dir dirka ari dira oraingoan
zerbat oroitzapen kolkoan!

Erdian erein, heldu da indarrak berriz batzeko ordua
Erdian erein, gaurkoan belaunaldi berriaren taupada

Hamaikan hamaika ginen ta beste pauso aitzina
Izan nahi dugun guztia izango garelakoan
Ia konturatu gabe pasa zaigu mende erdia 
ezkur batetik datorren enborra
argi baten bila loratuz doa

Erdian erein, heldu da indarrak berriz batzeko ordua
Erdian erein, gaurkoan belaunaldi berriaren taupada
Erdian erein
Erdian erein