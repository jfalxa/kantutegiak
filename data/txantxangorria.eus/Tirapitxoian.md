---
id: tx-2813
izenburua: Tirapitxoian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j8EN2bsk1sw
---

Tirapitxoian elkartu gara
tribu guztietako indioak.
Karabinak hartu eta bagoaz
Carrefour asaltoz hartzera.

Erreserba txikia da asetzeko
gure nahia.
Ghettoetatik atera gara
auzo aberatsak suntsitzera.
Gerra pintura aurpegietan,
trukatutako motorrak.
Kate, aizkora, makila, labanak...
Robin Hood kinkien garaia da!

Erreserba txikia da asetzeko gure nahiak.
Ghettoetatik atera gara
auzo aberatsak suntsitzera.
Gizon txuriaren negarra,
euri-dantzaren ondorioa.

Karabinak hartu eta bagoaz...
Robin Hood kinkien garaia da!
Erreserba txikia da asetzeko gure nahiak.
Ghettoetatik atera gara
auzo aberatsak suntsitzera.