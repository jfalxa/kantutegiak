---
id: tx-2676
izenburua: Segundu Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DJIvo8f1OCg
---

Ahaztezinezko segundu hura,
taupadarik gabe gelditu zinena.
Arimak egin zizun izar txikia.
Argi bizizkourrezko dirdira.
Izar arteko ametsen estalpean,
maitasun ohatze goxo batean.
Zauden tokian besarkatzen
zaitut,
hemen, nire besoen artean.

Segundu batean,
bizitzak bira eman zuen,
zorigaitzaren ilunpean,
nahigabean.
Eta ikusi zintudan
zilarrezko belardietan,
izar dirdira birekin,
begietan.

Zerbait gaizki egin nuen
zigortu izan dut neure burua.
Baina orain badakit lasai zaudela.

Izar txiki bat zara,
dirdira egiten duena.
Gauero kulunkatzen zaitut,
besoetan.