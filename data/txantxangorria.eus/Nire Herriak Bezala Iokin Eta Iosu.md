---
id: tx-3095
izenburua: Nire Herriak Bezala Iokin Eta Iosu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yRt_4j9PJPM
---

Nire herriak bezala
nahia badut
aterik ez;
nire herriak bezala
orro dagit
atsekabez.
Nahi handi hau
zezen bat dut
egarri baten
moskorrez.
Euskal Herriren 
enzierroa
luzatzen doa
kale luzez.