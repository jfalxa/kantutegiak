---
id: tx-1313
izenburua: Afera Ustela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lOLeXRbABng
---

Azkenean heldu zara, aldeko duzu zortea
zerbait aproposa lortzekotan gu gara zure jendea

jatorrak
lagunak
salbatzaileak
eta astunak

men egiten duen oro daramagu gailurrera
hemen kontzientziak bost axola, izenpetu eta aurrera

hau duzu merkatuko materialik onena
zatoz gurekin, bana dezagun fortuna
kontrakulturaren industriaren zurtoina
ikusleen zerbitzura

hemen dena da amarru, dena jukutria
urrutira ez joan
hau da kulturaren fast food-a
hemen dena da amarru

negozioa
arrakasta
etekina
zure arima
eta zure amarena

baina kontuz biharamunean zer jazo den galdetzean
bat-batean zureak egin du
dena galdu duzu jada