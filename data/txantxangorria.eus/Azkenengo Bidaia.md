---
id: tx-2877
izenburua: Azkenengo Bidaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pEhtZKOaKbI
---

Honela heldu gera 
hodeien garaiera 
itsaso lurrundu, oroimen haize 
berriz ere lurra hartu arte 

Altueratik begira 
memoriak utzitako haria 
altueratik begira 
soro zabal horietan txiki ikusten da garia 

Non dira ametsak? 
Non lortutako eztiak 
galdutako aukerak 
eguzkiak erreak, lasta danakin batera 

Zer da ba, zer bada? 
Iraungo gaituena 
han geratzen dena 
datozenen taupaden erritmo ozena 

Azkenengo bidaian goaz 
atzera begira
credits
released 17 March 2015 
MUSIKARIAK 
Harkaitz Bastarrika (ahotsa, baxua, koroak) 
Danel S. Martiarena (kitarra, koroak) 
Amaia Ureta (pianoa) 
Eusebio Soria (baxua) 
Mikel Alberdi (tronpeta) 
Josu Tete (tronboia) 
Xabi Zeberio (biolina, biola) 
Jose Urrejola (flauta) 
Mixel Ducau (saxo tenorea) 
Iñigo Egia (kongak, shakerra, tanborina, hirukia, guiroa, rika, surdoa) 
Ander Zulaika (bateria, paila, koroak) 

HITZAK: Danel S. Martiarena 

GRABAKETA, NAHASKETA, MASTERIZAZIOA: Oh Brother! Studio