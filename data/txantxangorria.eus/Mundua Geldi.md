---
id: tx-517
izenburua: Mundua Geldi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wHK7ds0ubXQ
---

Ideia eta produkzioa: RuralKids
Grabazioa: Dronetikan AirWorks
Musika: Dupla
Grabaketa eta nahasketa: Haritz Harreguy
Aktore nagusia: Irati Saez de Urabain

Follow DUPLA:


LETRA:
Beste egun bat sofan etzanda
barne bakearen eztanda
beste igande bat atsalde pasa
bizi izan dugun bizitzaren ainorantzan

triste nabil ta ez dakit zergatik,
haserre nabil arrazoi barik
gaur ohean geratuko naiz 
ez dut kalera atera nahi
   
ixilik egoten ikasi dut
eskopeta presto hitzak barrun 
norantza jakin gabe ezin lasaitu
plater zikinekin ezin afaldu

kontaktu fisiko falta zoramen aparta
etxean itxita egotearen martxa
ez dao ilusiorik ezta helbururik
mundua gelditu da abisua eman barik 

ez naiz inorekin hitzeiteko gai
baina bakarrik ez dut egon nahi
sarean nabil babes eskean
bihotzantzako pilulen menpean

Zindagizut
saiatu naizela
barnean dudan mamua akatzen
nirekin bueltaka dudan tristura 
betirako kanpora ateratzen 

ixilik egoten ikasi dut
eskopeta presto hitzak barrun 
norantza jakin gabe ezin lasaitu
plater zikinekin ezin afaldu