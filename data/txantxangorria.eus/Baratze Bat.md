---
id: tx-3392
izenburua: Baratze Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kuHBG93JqMo
---

Hau da egün eijerra
mila txoriek kanta
tziauri ene erregina
besoak zuri ditizüt para.

Baratze bat nahi dizüt egin
Amets sekretuenekilan
Liliak egün jauntzi eijerrenetan
Mila kolorez dantzan bihotzetan.

Gaü beltz ta sakonetik
Jeikitzen naiz hoztürik
Bihotza hain tristerik
Usu hitaz berantetsitürik.

Baratze bat...

Karrosa bat hor dugu
Kanpoan gure aiduru
Ez gal aboro denbora
Hiskor beitago amodioa

Baratze bat...

Maitarzun berriari
ilargiak dü argi
emanen deikü aski
guk elgar maita dezagun beti.