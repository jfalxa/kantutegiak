---
id: tx-2621
izenburua: Euskal Herri Tropikala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5zs4yEMS4ws
---

gabon euskal herria  bihar goizean ipar haizeak 
ehun kilometro orduko abiadurarekin tenperaturak
- 20 gradu zentigraduetara jetxiaraziko ditu
arratsaldean ego haizeak tenperaturak igo eta 
ostarteak nagusituko dira zeru guztietan gauean berriz
ekaitz bortitzak espero dira ongi etorriak euskal herri tropikalera

udaberria neguaren hilberria euria egingo duenez eskuan beti aterkia
etzazu erratu erabakia amak esaten dizunean gainean beti berokia
aurten ere ez euki zalantza uda pasatuko dela bainan auskalo norantza
hotza egiten badu ere prestatu erratzak uda ondo hasteko sorgin dantza
atzo elurra ta gaur zaparrada guretzako hau ez da eguraldi txarra
kazkabarrarekin ere moldatzen gara hartu bainujantzia goazen danok hondartzara
goizean xirimiria lasai! disfrutatu kalean ez egon eguzkiaren zai
astelehenetik igandera ongi etorriak euskal herri tropikalera

batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala
batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala

usoak galdurik dabiltz gure inguruan loreak loratzen noiz? abenduan
udaberria erabat esnatu denean berriro negua gustura gaudenean
ekainean elurra gailurretan zoratu egingo gara edozein egunetan
abuztuan hotza normala da aurten eskiatzera joango gara irailean
hemen mikroklimak nagusitu egin dira ordizin eguzkia ta altamiran elurra
ez dela ona esnatzea goizegi behe-lainoak ez du uzten ikusten ormaiztegin
badirudi bihar hobetuko duela aspaldiko partez eguzkiak berotuko gaituela
metereologoak zer esan du? baliteke bakarrik ardiek zerua irakur dezakete



batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala
batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala

negu urdin negu beltz negu gorriak gure neguak ez dira oso eramangarriak
gabonak harrigarriak dira olentzero txankletetan etorri da aurten herrira
eguraldian ez diote hartzen neurria asmatzen dutenean ñoooo…harrigarria
bihar txingorra ez da larria ana urrutiaren ahotik entzunda zoragarria
argi ibili badator udazkena thorren lehengusua da trumoi du abizena
burura harririk erori ez dakigun etxean geratzea izango da hoberena
urtero panorama berbera egun urdinenak ere erortzen dira zerutik behera
ez dutenentzat ulertzen gazteleraz si vestimos de goretex por algo sera 

batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala
batzuetan hotz besteetan bero
eguraldi ezberdina egiten du egunero
hau da hau klima mundiala
gure euskal herri tropikala