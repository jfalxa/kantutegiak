---
id: tx-454
izenburua: Azken Udazken
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/u-96xbtnmC4
---

Neguaren atarian
Pozak izozten direnian
Zure besoetan nahi dut
Sentitzea udaberrian

Besarka nazazu
Berriro matte
Besarka nazazu
Lokartu arte
Berriro

Laino grisak zeruetan
Ez da hostorik zuhaitzetan
Baina udaberriaren
Usaina duzu eskuetan

Besarka nazazu
Berriro matte
Besarka nazazu
Lokartu arte
Berriro