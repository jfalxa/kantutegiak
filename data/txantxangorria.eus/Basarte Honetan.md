---
id: tx-1978
izenburua: Basarte Honetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SZ6PJXQrqTk
---

Zuhaitz hostotsuen
adar gorenetan
haizeak marmar du egiten
hozkiro eta ozen,
Basarte honetan
burrubartean naiz galtzen
eta bakardadean dut pentsatzen.

Honela munduan,
sentitzen dudanarengaindik
haize batek bizitza egiten du,
eta ezerk zentzurik ez du.

Ez eta bakarti pentsatzeko dudan
arimak ere.

Honela munduan,
sentitzen dudanarengaindik
haize batek bizitza egiten du,
eta ezerk zentzurik ez du.