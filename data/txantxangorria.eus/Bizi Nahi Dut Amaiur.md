---
id: tx-3151
izenburua: Bizi Nahi Dut Amaiur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IpWjTjV7hV8
---

Aurrera filmetako gidariak!
Aurrera akademiako agintariak!
Aurrera koilare
des/ber/di/ne/ko txa/ku/rrak!
On/do ne/ur/tu gel/di/tzen zai/zun
bizi txikia!
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada

Ga/iz/ki sen/ti/tzen naiz!
Ga/iz/ki sen/ti/tzen naiz!
Ne/gar e/gin nahi dut e/ta e/zin
O/ihu e/gin nahi dut
e/ta de/be/ka/tu/a
Mai/ta/tu nahi dut
e/ta ez di/da/te uz/ten
Bo/rro/ka ez dut nahi
ba/ina be/ha/rrez/ko/a da
Hiltzea nahi izango nuke
baina une berean

Bi/zi nahi dut a/hal bal/din ba/da
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada

Bi/zi nahi dut a/hal bal/din ba/da
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada
Bizi nahi dut ahal baldin bada