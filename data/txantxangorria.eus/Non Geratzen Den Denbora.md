---
id: tx-683
izenburua: Non Geratzen Den Denbora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IWZ8_Zyp6GQ
---

Gure baitango lurraldeetan
Non geratzen den denbora
Non ez dagoen arnas biderik
Non ez goazen inora
Non oihukatzen hurbiltzen garen
Erraldoiaren ondora
Igo nazazu nire mendira
Zure gailurretik gora.
Egunez zu bizitzera
Arimak zenbatzera
Lotan nabigatzera
Elur gazietan.
Igo nazazu nire mendira
Aurki dezadan bidea
Askatu nadin zure loturaz
Behar dezadan airea
Ez nadin izan jainko lurtiarra
Bai ezinen bidaidea
Igo nazazu nire mendira
Zu ez baitzara nirea.
Egunez zu bizitzera
Azken mugen ertzera
Lainoen agurtzera
Elur gazietan.