---
id: tx-3037
izenburua: Iluna Denerako
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/k0HWDlbbYZE
---

Kantaita bat 
ene ezpainetan, 
Artizarra 
keinuka zeruan, 
zure lehen loak hartu 
zaituenean 
zer ote da 
zure gogoan?... 

Ixuria
izan zara
bizitzaren ibar 
honetara; 
zu zara gure
azala eta
asmo berrien haragia. 

Nigan bada 
zuretzat egina
iluna denerako aterbea; 
zu izango zara 
bihotzean den 
suarentzako erretxina.