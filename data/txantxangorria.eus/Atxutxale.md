---
id: tx-1739
izenburua: Atxutxale
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Vg5MBzIsFdI
---

Entzun al duk notizia?
Ez ba neska, zer jaso da?
Juan Marik egin du hutsa.
Ai! Bai! Zerbait entzun dinat.

Barakaldoko mitinean
kriston hanka sartzea
eduki zuela
gora ezker abertzalea oihukatuta
gora ezkerraldea esan beharrean.
Hori duk buru nahastea
bai ba tipo horrek duena
bere taldeko mitinean
gure alde jotzen duena.