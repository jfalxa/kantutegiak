---
id: tx-1707
izenburua: Malkoak Euri Balira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E5cFaRypyu4
---

Malkoak euri balira udazken traidorean
maitalerik gabe kolpez bazter utzia
emazte adintsua gertatzen denean
ehun hodei ilunen jasa izugarriek
kaleak uholdeka itoko lituzkete,
errotik garbituko ohizko zekenkeriak
hondakinez gainezka dituen bihotzak.
Malkoak euri balira 
kupirarik gabe haurtxo baten gorputza 
gerra izugarriek 
burniz eta metrailaz hausten dutenean,
ama denen oihuek 
espantuzko irrintziz izoztu lezakete 
entzumenik ez duen itsaso zabala. 
Malkoak euri balira
adoleszente baten ezpain oihukariak
harresi baten kontra torturaren espantuz
zauri direnean etxerik altuenak
lotsaz eror litezke gizadi osoa
redimitu nahirik, 
eta lore bakarrek entona lezakete
ehortze letani bat zerraldo gainetan.