---
id: tx-412
izenburua: Baldin Bada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qsU2ipaMy3I
---

BALDIN BADA   (Baldin Bada)
Aurrera filmetako gidari ak. 
Aurrera akademiako agintarak. 
Aurrera koilare desberdineko zakurrak. 
Ondo neurtu gelditzen zaizun bizi txikia.

Bizi nahi dut  ahal baldin bada. 
Bizi nahi dut  ahal baldin bada. 
Bizi nahi dut  ahal baldin bada. 
Bizi nahi dut  ahal baldin bada.

Gaizki sentitzen naiz! 
Gaizki sentitzen naiz! 
Negar egin nahi dut  eta ezin. 
Oihu egin nahi dut eta debekatua. 
Maitatu nahi dut eta ez didate uzten. 
Borroka ez dut nahi baina beharrezkoa da. 
Hiltzea nahiko nuke baina une berean… 
Bizi nahi dut ahal baldin bada…