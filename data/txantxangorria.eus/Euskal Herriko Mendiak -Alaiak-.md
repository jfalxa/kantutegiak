---
id: tx-3322
izenburua: Euskal Herriko Mendiak -Alaiak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h5O6SSfMe6Q
---

Maite dut gure Euskal Herria
Nola hemen bainaiz sortüa
Mendiei hurbil damat bizia
Horien kontra sano airia



Euskal Herriko mendiak
Bide xoragarriak
Horiei begira daude
Heldu diren guziak

Negua murritz izanagatik
Primadera sortzen lore lurretik
Zoin eder iguzkia zerutik
Xoriak kantuz hasten goizetik


Jiten denean udaberria
Ardiez betetzen ikusten mendiak
Goizetan laster hasten argia
Artzaina goiz ibiltzen artaldia

Estribillo.