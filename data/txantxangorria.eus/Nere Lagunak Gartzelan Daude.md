---
id: tx-1229
izenburua: Nere Lagunak Gartzelan Daude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BoRJ1HGpcyc
---

Kotoia eta tinta eskuan
jai batzordeko bilera, kalejira auzoan,
kontzentrara lan baldintza duinegatik
txoznako turnoa ez zait tokatzen oraindik.

Biharko salduak hamar mila rifa,
neska bat bortxatua izan da gero manifa;
beltzak azaldu dira bapatean,
ez al digute utziko sekula bakean!

Nere lagunak gartzelan daude
lana egiteagatik bere herriaren alde
nere lagunak gartzelan daude

Gaurko asanbladara berandu noa,
azkena izango ote naiz agian lehenengoa?
afari herrikoia presoen alde,
gaztetxearen urteurrena, kontzertua debalde

Zenbat aldizkari, manifestu eta txosten,
jada ez dakit ere nere burua ere non den!
han, hemen, batean eta bestean...
eduki nahi zaituztegu gurekin kalean!

Nere lagunak gartzelan daude
lana egiteagatik bere herriaren alde
nere lagunak gartzelan daude
justiziaren izenean bahitu dituzte

Eutsi gogor mutil, eutsi gogor neska,
eutsi gogor gazte, ondo zabiltzate!
injustizia aurrean, duintasuna soinean,
herri baten babesa duzue atzean!

Zuen delitua elkartasuna,
herri batenganako maitasuna
lapurtu dizuete askatasuna,
baina inolaz ere ez duintasuna.

Elkartasunaren txanponak mila aurpegi;
asko kriminalizatuak baina ez etsi!
Euskal Herri libre, justu baten alde
animo neska mutilak zuekin gaudeee!!!

Nere lagunak gartzelan daude
noiz arte?