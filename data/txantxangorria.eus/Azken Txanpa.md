---
id: tx-2875
izenburua: Azken Txanpa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JvITnGhpB4U
---

Arraunlariak, triste
Gaur ez dira, irabazle,
Kaieko urgazitan
Beso biotz neketan

Azken txanpana egin
Baina arraun bat zaie autsi
Urrengoan lagunok
Garaipen, zuekin
Urrengoan lagunok
Garaipen, zuekin

Agur zorionak
Irabazle ari
Itsasoa ez da
Agortuko orain dikan

Agur zorionak
Irabazle ari
Itsasoa ez da
Agortuko orain

Egun luzez prestatu,
Eta orain dena da galdu
Ametsen laburrada
Estroparen, kulunka

Arraunak elkar jotzen
Izerditan dira galtzen
Urrengoan lagunok
Garaipen, zuekin
Urrengoan lagunok
Garaipen, zuekin

Agur zorionak
Irabazle ari
Itsasoa ez da
Agortuko orain dikan

Agur zorionak
Irabazle ari
Itsasoa ez da
Agortuko orain