---
id: tx-1134
izenburua: Behera Behera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-jlaZwxJA2A
---

Behera, behera,
Behera, behera,
Behera, behera,behera, behera,behera, behera,
Behera, behera,
Behera, behera,
Behera, behera,behera, bai!Jai!

Salto, jauzi, salto ta jauzi,
txalo ta txalo ta txalo,
salto, jauzi salto ta jauzi.

Txalo, txalo, txaloka!!!