---
id: tx-2189
izenburua: Sarri Sarri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_2ZY93qAhtM
---

Kortatu taldearen abestia, seguraski talde honetako ezagunena. Hitza Lurdes eta Josu Landarena. Músika Jamaikako Toots and the Maytals taldearen "Chatty Chatty" abestiaren gainean egina dago. Abestian Ruper Ordorikak kolaboratu zuen
Hitza 1985ean idatzi zuten Martutene kartzelatik Piti eta Sarri ihes egin eta gwero.