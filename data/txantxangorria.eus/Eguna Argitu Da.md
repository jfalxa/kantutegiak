---
id: tx-1074
izenburua: Eguna Argitu Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rSkExflGwDQ
---

Eguna argitu da bildu gara kantoietan lo zauden hori esnatu!
Heldu eskutik eta sikatu begietako amorruaren malkoak ta odola.
Heldu da eguna.
Ez egon besteek egingo dutenaren zain, ez egon besteek bidea hasteko zain, heldu da eguna gaur da gaur da unea harriak zorrozteko.
Zatoz hona batu ta goazen kale erdira egin begiekin ameztea kantoi beltzetik belarrira.
Zatoz!
Heldu da eguna.
Ez egon besteek egingo dutenaren zain, ez egon besteek bidea hasteko zain, heldu da eguna gaur da gaur da unea kaleak oldartzeko.
Zatoz hona batu ta lotu indar berriak bide zaharretan ereiteko hazi berriaren bizinahia.
Gaur da gaur da eguna gaur da gaur da unea, kalera!
Gaur da gaur da eguna gaur da gaur da unea ameztu.
Ez egon inor zain, aske izan.
Zure esku dago etorkizuna berria.
Ez babestu kantoietan dena dugu eskuan.
Eguna argitu da bildu gara kantoietan lo zauden hori esnatu!
Heldu eskutik eta sikatu begietako amorruaren malkoak ta odola.