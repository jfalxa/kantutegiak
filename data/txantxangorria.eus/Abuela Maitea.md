---
id: tx-107
izenburua: Abuela Maitea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0UugDCxX2gY
---

Heldu nazazu eskutik, soilik zuk dakizun bezala
Gelditu dezagun denbora, gure begiradan
Orain arte ez dut aurkitu, abagunerik onena
Aitortzeko besteen aurrean niretzat zu izan zarena
Ezkutuko zaindaria, itsasargia bidean
Babestu nauzu behar izan dudanean, inork ez bezela
Zurekin lo hartzen nuen, nire egun ilunetan
Zurekin amets egin dut, gaur gauean
Heldu nazazu eskutik, soilik zuk dakizun bezala
Gelditu dezagun denbora, gure begiradan
Ta esaidazu belarrira, beste behin mesedez amama
Zure bihotzaren zati bat eramango dudala
Zure begi urdinetan, islatu dira nireak
Mundua aldatzen ikusi dugu batera, hainbeste urtetan
Baina orain ulertzen dut, betirako ez garela
Abesti hau zuretzako da, abuela maitea
Heldu nazazu
Heldu nazazu
Heldu nazazu