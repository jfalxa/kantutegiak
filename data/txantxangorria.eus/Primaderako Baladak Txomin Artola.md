---
id: tx-3112
izenburua: Primaderako Baladak Txomin Artola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tbhtQvtPwPI
---

Hodei ilunek eraman du/te
negu beltzeko landua
eguzki printze laztantzen dute
ortu txuriko mandoa
ibar luzea u/rundik dator
kuku gisaren kantua
soi/ne/ko be/rriz ber/de bi/si/ta
ez di/tu/a lur o/so/a
ha/rri tar/te/an le/un/ki da/tor
e/rre/ke/tan ur go/zo/a
xo/ko gor/de/a xo/mo/rro bi/la
az/ta/rri/ka ur xo/xo/a
bide ertzeko haritz ondoen
itzal etzanak berrotan


es/kul/pi/pi/ta sor/tu be/rri/a
e/guz/ki laz/tan le/lo/tan
o/kil ber/de/en da/biltz zu/lo/an
en/bo/rri har/tu gel/do ta


sa/kan lu/ze/a a/ge/ri di/ra
be/lar/di ber/de gar/bi/a
goi/ze/ko i/hin/tza
u/rre/tan dau/ka

e/guz/ki/a/ren ar/gi/a
be/lar fres/ko/tan zu/ri zuri/a
baz/kan da/bil/tza ar/di/a

i/tu/rri/ra/ko sen/da hes/tu/a
e/rren/ka da e/to/rri/a
u/ra e/da/ten a/ri da as/ka
be/ho/rran da
da go/rri/a

u/rra sa/be/la e/ro/ri za/ie
pa/go gai/ne/tik go/rri/a

hai/ze fres/ko/a as/tin/tzen di/tu
ba/sa ma/zus/ten lo/re/a
sa/si on/do/ko la/har hos/to/en
ber/de ar/te/ko mo/re/a
a/ran/tza ar/te/an be/rriz/ten di/ra
tris/tu/ra/ren ko/lo/re/a

E/gun be/rri/a u/rra/tu/a da
hil/tzen a/ri da a/rra/tsa
e/rre/kon/do/ko ur lin/tzu/re/tan
lo/re/tan da/go sa/ha/tsa
a/da/rrez e/ta har/tu na/hi du
u/da/be/rri/a ar/na/sa