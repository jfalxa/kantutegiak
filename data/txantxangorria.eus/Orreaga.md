---
id: tx-1261
izenburua: Orreaga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fyIsBK-F9yM
---

Zuhaitzetan den ederrena da 
oihan beltzian pagoa...
Orreagako mendi gainetan 
badabil euskal-gogoa:
Galernen eta gerlen artetik 
mendetan barna xut doa,
pagadoietan ixilik kontuz 
Europakoen geroa.

Zenbat gerlari, sarraskiz gose, 
odolez eta suz hordi,
Europatarrak zanpatu nahiz 
iragan denik horgaindik!
Ibañetako mendi lepoa 
heietaz mintza baladi,
aipa letzazke lehen-lehenik 
Arrolan eta Karlandi.

Buru-gabetu, bihotzez hustu 
jendalde lehertuekin,
mundu guztia nahiko zuten, 
beren azpiko bategin.
Euskal jendeak, denen gatik xut, 
gogor jeikitzen du jakin
mendi gain hautan, burua gora, 
gizonak gizon dagotzin.

Indarkeriak ezarri mugak 
kraskatzen hasi orduko,
Euskal gogoa noiz da 
jauziren arranoaren pareko?
Azpilkueta bezein kartsuko 
eta jakintza gaitzeko
gizon libroak noiz ote dire 
Orreagatik jaliko?

Zoin den pizgarri mendietarik 
jauzika doan xirripa...
Euskal-semeak, Aiten gogoa 
bihotz guziaz hurrupa!
Altxa burua, denek bat kanta: 
Euskal Gudua gaur aipa:
Entzunik zuen irrintzina, 
xutik jar dadin Europa!