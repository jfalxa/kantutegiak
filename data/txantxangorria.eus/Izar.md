---
id: tx-1367
izenburua: Izar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4-udLPaQaz0
---

Ilunpetan, hitz ederrenak hurbiltzen direnean, gertu sentitzen zaittut
Hor zaude, ta nire itzaletik urrun egon arren, hor zaude berriro.
Aurrean, zuri-beltzean dagoen irudi bat, lehenengo taupada.
Doinu bat, etsipenaren kontrako garraxi bat, patuaren algara, arimaren hitza… 

HEMEN NAIZ, ZUREKIN ESNATZEKO
BAKARRIK, ZURE ITSASOA IKUSTEKO
HEMEN NAIZ, MALKOAK APURTZEKO
ZUGANDIK, IRRIBARREAK ASKATZEKO 

Bide hau, izan dadila izan beharko dena, ez dut nahi begirik.
Ez dut nahi, etorkizunik ez dut nahi barkamenik, ez dut nahi beharrik.
Nahi zaittut, iluntasunaren isladatik urrun, orainaren aldean
Ta nahi nuke, egindako negar bakoitzaren truke, argi bat piztea, gure bidean… 

HEMEN NAIZ, ZUREKIN ESNATZEKO
BAKARRIK, ZURE ITSASOA IKUSTEKO
HEMEN NAIZ, MALKOAK APURTZEKO
ZUGANDIK, IRRIBARREAK ASKATZEKO 

Besarkatu nazazu, hegan egin dezagun
Zaindu dezadan udaberri hau, eta ez da bukatuko, ez dut alde egingo. 

HEMEN NAIZ, ZUREKIN ESNATZEKO
BAKARRIK, ZURE ITSASOA IKUSTEKO
HEMEN NAIZ, MALKOAK APURTZEKO
ZUGANDIK, IRRIBARREAK ASKATZEKO!