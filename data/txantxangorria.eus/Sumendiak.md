---
id: tx-247
izenburua: Sumendiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fqncDsJKrkI
---

“Sumendiak”, ETS-k egin duen Volcans (Buhos) abestiaren euskarazko moldaketa da! Gozatu!

Sasi eta arau guztien gainetik
mundua gaur bukatzen dela badakit.
Zeru beltzean su festan sumendi bi,
lurra lehertzen eta airean dantzari.
Lubakietan, loratu ginen bi hazi,
munduan jabe ginela sinistarazi.
Ta orain munduan kaiolaturik
nondik atera behar dugu? Esan nondik!
Gerra hontan zurekin galdu nahi dut nik.
Sasi eta arau guztien gainetik
mundua gaur bukatzen dela badakit.
Zeru beltzean su festan sumendi bi,
lurra lehertzen eta airean dantzari.
Badator sua, bero geldiezina da.
Lehen muxua, orain sumendia gara.
Negurik hotzenean, zein uda lehor,
lurra lehertzen da eta beti zaude hor.
Taupada bakoitzean leherketa dator.
Sasi eta arau guztien gainetik
mundua gaur bukatzen dela badakit.
Zeru beltzean su festan sumendi bi,
lurra lehertzen eta airean dantzari. (x2)