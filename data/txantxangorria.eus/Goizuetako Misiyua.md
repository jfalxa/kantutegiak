---
id: tx-2838
izenburua: Goizuetako Misiyua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s6Dj9_9GbBI
---

Gaude mila ta bederatzirehun
hamaseigarren urtian
oso ez gera iristen baina
ez gabiltza apartian;
misionistak izandu dira
Goizuetako partian,
hoien bisitaz oroituko naiz
bizirik nagoen artian.

Alkatia ta bikarioak
eman zizkaten eskuak,
haien barrendik irtetzen ziran
arnasa larri estuak;
alde batera uzteko diye
mundu hontako gustuak...
hori entzunda ez zeuden lasai
hemengo maiorezkuak.

Makario zan lehendabiziko
predikatutzen hasia
asko gustatu zitzaidan haren
hitz egiteko grazia;
beldurrik gabe joateko dio
bada ta komenentzia.

Mutil gaztiengatikan ere
zerbait esan du frailiak,
uzteko dio garai txarreko
zurrutak eta bailiak;
nola dakarren ondoren txarra
gazte parranda-zaliak,
ohera joanda errezatzeko
ez bagaude loaliak.

Eju ein zuten guraso zaharrak
atentziuan jartzeko,
bakoitzak bere familiari
ongi erreparatzeko;
misio santu hartako hitzak
ondo goguan hartzeko,
munduan ongi portatu eta
gero zerura sartzeko.

 Emakumiak ere gaixuak
ez daude denak justuak,
eztul batzuek aditu ditut
negarrarekin nahastuak;
malkuak bera deizkiotela
gurutzaturik eskuak...
ez ikaratu, barkatuko du
gure Jaungoiko prestuak.

Ministro hoiek izandu dira
predikadore sotilak
lehendabiziko gizonengatik
izugarrizko batailak;
gero aboan hartu dituzte
neskatxak eta mutilak,
azkenerako astindu ziran
atsuan patar-botilak.

Bertso horiek ez dira izan
goizuetarren kaltian,
lehen aina anima ez dago orain
diabruaren katian;
Aita San Pedrok sartu gaitzala
zeruetako atian
denak jostatzen han ibiltzeko
aingeruekin batian.