---
id: tx-1776
izenburua: Zuei
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7ZKXlV2fWjo
---

Nire miresmen guztia irrifarra galtzen ez duzuenoi.
Nire miresmen guztia egunero borrokatzen  duzuenoi.
Gauza txikietan bada ere, irrifarra galtzen ez duzuenoi.
Gauza txikietan bada ere, irrifarra galtzen ez duzuenoi, zuei.
Ez dira, ez dira pasako.
Herriaren gainetik, ez dira pasako