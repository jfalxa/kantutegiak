---
id: tx-524
izenburua: Iluntasuna Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6MtHc65q5HU
---

Bihotzetik heldu zenidanean
lurraren kontra zanpatzeko.
Uste nuen amaiera heldu zela baina
begiak ireki nituen.
Bide bazterrean galdu nituen
irribarreak, maitasuna.
Bizi nintzen infernuan ez nituen behar
eta orain, ez dakit non dauden.
LAPURTU ZIDATEN IZAN NUEN BIZIA
IZAN NINTZEN UME ZORIONTSUENA
ILUNTASUNEAN IZAN NINTZEN ARGIA
ETA ORAIN BERRIZ, ILUNTASUNA NAIZ
Negar egiteak ez du balio
barkamenak ez dauka zentzurik.
Azken aldiz besarkatuko zaitut astiro
eta ez, ez naiz itzuliko.
Egin dezakedan gauza bakarra
aitzinera begiratzea eta
iragana irribarre batez agurtzea
eta zurekin amets egitea.
LAPURTU ZIDATEN IZAN NUEN BIZIA
IZAN NINTZEN UME ZORIONTSUENA
ILUNTASUNEAN IZAN NINTZEN ARGIA
ORAIN BERRIZ, ILUNTASUNA NAIZ