---
id: tx-509
izenburua: Herria Batua Inoiz Ez Garaitua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ojMumtqv0KA
---

Txileko kantak aipatzen duen “herria” ez da, noski, Euskal Herria; 1973. urtea 2021 ez den bezalaxe. Hartara, kantuak Txileko lurrak aipatzen dituen tokian euskal lurraldeei egin zaie erreferentzia. Eta batean zein bertzean, herriaren batasun gogoa aldarrikatzen du kantak, ikuspegi itxaropentsu, garaipenezko eta, agian, xaloegi batekin. Baina ez da ahazten ahal borrokarako kantu bat dela, iraultzaileak martxan jarri nahi dituena; langileak adorez, maitasunez eta elkartasunez bete nahi dituena. Eta, begirada horrekin, LABek pentsatu du kantu honen euskarazko bertsioak bazuela tokia langile klasearen batasunaren aldeko eta Euskal Herriaren batasunaren aldeko borroken kantutegian, edonork edonon edonoiz erabil dezan. Ikusteko dago kantak bere bidea eginen ote duen, ea benetan loratuko den. Baina, bederen, dagoeneko hemen dugu, borrokan bidaide.

Euskal Herria, 2021eko maiatzaren 1a


Zutik, kanta, garaitzera goaz.
Hemen dira batasun banderak.
Ta zu, zatoz, gurekin batera,
ikusiko kantua ta indarrak loratzen.
Gorri da ekiaren goiza,
igarri du datorren bizitza.

Zutik, altxa, herria aurrera.
Hobea da iritsiko dena,
gure poza konkistatua da
ta mila ahots oldartuko dira,
ozen erran dute askatasun kanta,
laster dator gure garaipena.

Ta oraintxe herriak,
borrokan altxatuak,
garrasi egin du:
“Aurrera, eta batu!”.

Herria, batua, inoiz ez garaitua. [BIS]

Herria ari da batasuna sortzen
nafar baratzetik ipar trenbidera,
sortaldeko borda argitsutik
sartaldeko fabrika ilunera,
lanean gabiltza, herria badabil,
pausoz pauso geroa du ekarri.

Zutik, altxa, herria aurrera.
Borroka da guztion egia.
Harrizkoa, langile multzoa,
justizia da bere animoa.
Adore, maitasun ta elkartasunez
hemen zaude, borrokan bidaide.

Ta oraintxe herriak,
borrokan altxatuak,
garrasi egin du:
“Aurrera, eta batu!”.

Herria, batua, inoiz ez garaitua. [BIS]