---
id: tx-561
izenburua: Azken Muxarraren Testamentua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jJ5KvNgZezQ
---

EZ ETSI The Beatles taldearen LET IT BE abestia da, Joxerra Garzia idazleak euskaratuta. Pianoa: Susana Abelairas-Etxebarria. Ahotsa eta Kitarra: Joxerra Garzia Garmendia. Ahotsak eta koroak: Janire Sanchez Uriarte, Itsaso Arrieta Goiri eta Katalin Zarate Arrieta. Perkusioa: Victor San Juan, Maite Sorazu Gondra, Elena Diaz Ereño. Talde Teknikoa: Joseba Ibarra Bustinza, J.R. "Mon" Facorro Fiuza & Josi Sierra Orrantia. 2020.ko Udaberri konfinatuan grabatuta & editatuta inor EZ (dezan) ETSI!
LETRA @EZABATZEN-ek euskaratuta:
Noraezean baldin banago,  ama goxo datorkit,
 hitz zuhurrez mintzo:  ez etsi! …
 Ta ilun-aldietan  bera alboan daukat beti,
 hitz zuhurrez mintzo:  ez etsi!  …
 Ez etsi, ez etsi, ez etsi, ez etsi! 
 Hitz zuhurrez mintzo: ez etsi! 
 Ta lur jota daudenak ados  jartzen badira aurki, 
 bada irtenbide bat,   ez etsi! …
 Zail dirudien arren  lortu liteke, ta sinetsi:
 bada irtenbide bat,  ez etsi! …
 Ez etsi, ez etsi, ez etsi, ez etsi!
 bada irtenbide bat, ez etsi! …
 Ez etsi, ez etsi, ez etsi, ez etsi!
 Hitz zuhur ixilez: ez etsi! 
 Gau hodeitsuan ere  argi bat nigan da pizturik 
 egun-argirarte ez etsi! …
 Musikak esnatu nau,  eta ama goxo datorkit,
 hitz zuhurrez mintzo: ez etsi!
 Ez etsi, ez etsi,ez etsi, ez etsi! bada irtenbide bat, ez etsi! (BIS)
 Ez etsi, ez etsi, ez etsi, ez etsi! Hitz zuhur ixilez: ez etsi!