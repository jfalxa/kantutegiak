---
id: tx-1442
izenburua: Txuriko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rjVF2sTf7cg
---

Gu gazte giñadela txalupa guztiak
zakurtxo bat ohi zuten ontzi barrenean
begi erne, abilla, ez zen zaunkaria
ihes zihoan arraina harrapatzailea

Seme esango dizut nola gertatzen zen
punttutako arraia, suelto batzuetan
legatza ospa, ihes, txakurra jauzten zen
'ta bet-betan arraia hartzen zuen hortzetan.

Lana horren saria 'txakurraren partia'
deitzen genuen guziok, ongi merezia
maitea-maitea zen txakur ehiztaria
txalupa betetzen zuen anima gabeak.

Unea etorri da zuri kontatzeko
zer nolako txakurra zen gure Txuriko
uhin izugarriak ez zuen izutuko
arraia utzi baino, lehenago itoko.

Goiz itsusi batean ez naiz ez ahaztuko
bere lana beteaz jauzi zen Txuriko
baga haundi artean, ehiza ez utziko
legatz haundi batekin ito zen betiko.