---
id: tx-1705
izenburua: Elearen Lainoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HTxu5g9S-vE
---

Jagoitik basamortu horailak ditut desiratzen,
Itsasertz biluziak,
Karrika hutsak,
Eguzki hobengabeak kixkaili larruak,
Irri partekatuak noiztenka
Eta denbora.

Ezer ez has, ez pentsa, ez erabaki,
Ez mintza amodioaren aitortzeko ez bada..., 

Elearen lainoa plazaren erditik
Kezkaren sosegu agurka
Hegaldatzen denean
Azkorriko zeru ubelegiek biltzen naute
Eta denborak.

Ezer ez has...

Lerratzen diren so auherrak

Ditut maite eta denboraren
Bihozkada hunkituak:

Ezer ez has...

Ez mintza amodioaren aitortzeko ez bada,
Utz bizitzaren hautsak herioaren itsutzera

Eta ibili
Denboran bezala
Kanta baten hatzetarik oinik dukedano.

Ezer ez has...