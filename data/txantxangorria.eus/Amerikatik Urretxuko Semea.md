---
id: tx-1406
izenburua: Amerikatik Urretxuko Semea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WnGLXsGoO_Q
---

1. Villarreal de Urrechu nere erri maitea
seme bat emen dezu
amorioz betea. (bis)
Nai baña ¿nola icusi?
au da lan tristea
zuretzat nai det bici
Urrechu nerea. (bis)

2. Bi milla eta seirégun
Legua badirá
Montevideotican
Euskal errirá.
Naiz esperantzétan
Etorri baguerá,
Aurreratasun gabe
Urtiac juan dirá.

3. Bai nere adisquidiac,
Bearda pentsatú,
Zuretzat Americac
Nola dan mudatú.
Ynorc emen ecin du
Lanican billatú
Orain datorrenari
Bear zayo damutú.

4. Gañera izan dégu
Emen ere guerrá
Gure zori onéan
Paquea egin dá.
Bañan guerrac ondoren
Dacar dictadurá
Don Lorenzo Latorre
Nagúsi degulá.

5. Ez bada, ez etorri
Gaur lur onetará,
Il edo bici obeda
Juatea Habanará.
Au dá gure bandera
Españaren onrá
Churrucaren semeac
Ara juango guerá.

6. Agur, adisquideac
Icúsi artean
Zuen ganatuco naiz
Egunen batean.
Esperantzetan bici
Nai det bitarten
Guero ezurrac utzi
Nere lur maitean."