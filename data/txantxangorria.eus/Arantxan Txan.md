---
id: tx-1820
izenburua: Arantxan Txan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tMgzD5peZes
---

Arantxan txan, arantxan txan
guli guli guli guli guli arantxan txan
Arantxan txan, arantxan txan
guli guli guli guli guli arantxan txan

Arabi, arabi,
guli guli guli guli guli arantxan txan
Arabi, arabi,
guli guli guli guli guli arantxan txan

-Orain, azkarrago egingo dugu
-Trena bezain azkar

Arantxan txan...

-Azkarrago oraindik
-Hegazkinez!

Arantxan txan...

-Eta bukatzeko...
-Suziria bezain azkar

Arantxan txan...