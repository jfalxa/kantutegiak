---
id: tx-1573
izenburua: Astotxoa Gure Astotxoa Oskorri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dTmxGKhNLqs
---

Astotxoa, gure astotxoa,
astotxoa, arrantza/rik ez.
Astotxoa, gure astotxoa,
astotxoa, arrantzarik ez.
Buruan joko zaitut,
buruan joko zaitut,
Buruan! Buruan!
a!!!
Astotxoa, gure astotxoa,
astotxoa, arrantzarik ez.
Astotxoa, gure astotxoa,
astotxoa, arrantzarik ez.
Buruan joko zaitut,
buruan joko zaitut,
Tronkoan  joko zaitut.
Tronkoan  joko zaitut,
Buruan! Buruan!

Tronkoan! Tronkoan
a!!!

Astotxoa, gure astotxoa,
astotxoa, arrantza/rik ez.
Astotxoa, gure astotxoa,
astotxoa, arrantzarik ez.
Bu/ru/an jo/ko zai/tut,
bu/ru/an jo/ko zai/tut,
Tronkoan  joko zaitut.
Tronkoan  joko zaitut.
Ipurdian  joko zaitut.
Ipurdian  joko zaitut,
Buruan! Buruan!
Tronkoan! Tronkoan!
Ipurdian! Ipurdian!
a!!!
a!!! a!!! a!!!