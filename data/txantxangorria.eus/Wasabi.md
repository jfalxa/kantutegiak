---
id: tx-1013
izenburua: Wasabi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sNWJTTrA0o4
---

Skasti taldearen 'Wasabi' kantuaren bideoklip ofiziala, bere azken diskotik ('YE', Baga-Biga, 2018). Bideoaren egilea Mattin Saldias izan da.


Bizi berri ditugun 
argi-ilunetaz harro gaude 
iraganeko minei 
ezintasunei 
kaso askorik egin gabe 

Zorionez, ordea 
txiki zara, maitea 
zinez txiki zara, maitea 
baina azken unean 
zure azken aleak 
aise igaroko du bahea