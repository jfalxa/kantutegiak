---
id: tx-1614
izenburua: Giltz Gordea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/38lYNO1LkDA
---

Hemen dago
heldu da eguna,
pozik izan, heldu da
Gailendu baita
argitasuna,
pozik izan, hemen da.

Goizeko haizeak nahi ditu eraman
lo errearen hausterrak.

Goizak argitara
ematen du
gauaren egia.
Egunak
fatu ilunena du
hurrengo gaua.

Non ote da unean uneko gakoa.,
bizi pozaren giltz-gordea?

Alerik ale doan harea
bizitzaren 
neurria.
Oharkabe jausten den
alea
nora ote doa?

Non dago unean uneko gakoa.
bizi pozaren giltz-gordea?

Geldirik goroldioari begira.
ezkilak entzuten dira.
Geldirik goroldioari begira nago.
ezkilak gero eta urrunago.