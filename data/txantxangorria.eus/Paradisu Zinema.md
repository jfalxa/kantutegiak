---
id: tx-148
izenburua: Paradisu Zinema
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D0qFmNshagQ
---

Txikitan, maitasuna,
erreprimitu egin ziguten.
Pelikularen zati hori
laguna, moztu ziguten.

Paradisu zineman ikusiko gara.
Leku hau "ametsak forjatzen diren
materialarekin egina dago"
Paradisu zineman ikusiko gara,
"Alemeniarrak grisez jantzita eta zu urdinez"

Oraindik ez da berandu
samurtasunari toki bat egiteko.
Zaborrontzira botatako muxuak
berreskuratu behar ditugu, maitia