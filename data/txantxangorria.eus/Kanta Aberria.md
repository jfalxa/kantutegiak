---
id: tx-2007
izenburua: Kanta Aberria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7xIXizxPl2E
---

Kantazak, euskalduna,
kantazak hire Herria!
Kantazak, euskalduna,
hire aberria!

Gure mendi zelaiak bakezko jauntzitan,
Etxe xuri gorriak altzotan!
Gure ikurrin bandera plaza kaskoetan,
herritarrak kantuz ostatutan!
Horra Aberria nola nik utzi dutan
kanpo alde joan egunetan...

Nere ama maiteak nigarra gordetzen,
ttipitik bainuen kexarazten,
haren bihotz zaurtuan nik deus ez ikusten,
haurrak ez baitaki so egiten.
Amaren iduri Euskadi zaut agertzen,
bainan nik kasurik ez emaiten!

Euskadi odoletan dute herrestatu,
ezpata ukalditan puskatu!
Gu Euskadiko semek, hor behar ixildu,
maitatzea omen da bekatu!
Ez, euskalduna, ez!Ez gaiten ba lotsatu!
Ulitxak lehoina aurtiko du!