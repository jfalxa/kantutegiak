---
id: tx-891
izenburua: Pater Noster
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O442RwL_jp8
---

Jarraitu eta entzun SPOTIFY-en!!😱🔥
Síguenos y escúchanos en SPOTIFY!!

Hotzak, behatz horiek nire lepoan
Aste santu eternalena
Ta azkenean
Lepotik bularrera, bularretik hanketara
Ah, ah, ama…
Pater Noster hau luze doa

Aitak (berorrek) nik baino gehiago bizi du
Sei urte bete berri ditut
Ta espiritu santuaren odola
Maindireetatik behera
Ah, ah, ama…
Pater Noster hau luze doa

--------------

Musika eta hitzak:
Pello Reparaz

Zuzendaria eta istorioa:
Loïc Grobéty-Vega

Argazki Zuzendaria:
Laurent Poulain

Grafismoak:
Andrei Warren

Arte zuzendariak:
Iratxe Reparaz

Aktoreak:
Aitzol Araña
Lukas Petriati
Jose Miguel Petriati

Ekoizlea:
DeepSoda

Muntaia:
Loïc Grobéty-Vega

Kolorea:
Fran Cóndor

Musika nahasketa:
Paxkal Etxepare

Produkzio burua:
Joseba Gracenea

Produkzio laguntzailea:
Joseba Razquin

Bigarren produkzio laguntzailea:
Lur Larraza

Kamera laguntzailea (Fokularia):
Ruben Yanes

Bigarren kamera laguntzailea:
Nestor Urbieta

Jantziak:
Alaia Arnaiz Del Escobal

Haur bideratzailea:
Imanol Janices

Eskerrak:
Panda Artist Management
Sergio Martinez
Eneko Sagardoy
Julian Montori
Amaia Montori
Ainara Goienetxe
Miguel Angel Goienetxe
Garikoitz Petriati
Garrapata
Gorka Pastor
Iban Larreburu
Libe Amunarriz
Iker Gozategi
Maria Carmen Escala
Miguel Ángel Reparaz
Juan Carlos Reparaz
Juantxo Reparaz
Artzai Iraurgi
Pau Vargas
Xafan Arratibel
Aitor Karasatorre
Goizeder Beltza
Iosu Agirre
Cristian Chivite
Maribel Lakuntza
Mari Nieves Lakuntza
Jone Etaio
Mikel Senar
Fran Araña
Laida Iraurgi
Maider Iraurgi
Jose Mari Mendinueta
Jokin Razkin
Oihan Razkin
Aberri Barandalla
Javier Irigoien
Xabier Irigoien
Jon Irigoien
Julen Irigoien
Aldabide Elkartea
Isabel Del Moral