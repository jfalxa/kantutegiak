---
id: tx-1135
izenburua: Maitasun Nortasun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E7ZJzTRyuG0
---

Naizena senditu nahi dut
senditzen dutana maite
maite dutana kantatuz
bi aldiz maite nezake
ezin dut deus egin kantu
benetan maitatu gabe
dena dut zinez kantatu
amodioa ere barne.

Maitasunarena, iparra hegoa
maitasunarena, hango hemengoa
nortasunarena ez dut banakoa
soilik Euskal Herrikoa.

Ez naiz hemengo ez hango
barka ezin badut jasan
ene bihotza inongo
mugatzarrak molda dezan
osoa dut maiteago
zenbat hautsiago izan
aginterri ezin deno
behintzat herri bakar gisan

Maitasunarena, iparra hegoa
maitasunarena, hango hemengoa
nortasunarena ez dut banakoa
soilik Euskal Herrikoa.

Kantatu eta maitatu
den dena ere ez noski
tamalez maizegi jardun
izan dut hortz eta guzti
izate geuk hautatua
mozten zaiguno bortizki
behar dut erremin hori
bihur dakidan abesti