---
id: tx-3007
izenburua: Zutaz Mintzatzen Banaiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JhdxoOvzNvM
---

Halako samin osteko zerutxo bat
aurkitu genuen, arratsalde hartan
eta ordudanik zure argitan nago.

Halako arnas berri bat
sumatu nuen ene baitan
zure nahiaz geratu nintzan
zeure txeren argi distiran.

Halako amets estiz bizi zintudan
laztan desiran, bihotz zirraran
bizipoza hurbildu zitzaidan.

Halako xarmat eder bat
zurgatu nuen ezpain musutan
mingotsak aldegin ostean,
nigan zauzkat bihotzean.

Zenbat gogoratzen zaitudan,
zenbat eta zenbat
zenbat zaitudan atsegin
zenbat eta zenbat.

Eta zutaz mintzatzen banaiz
neu naizenaz mintzatzen naiz
zeu zerenaz oroitzen banaiz
gu biotaz ari naiz.