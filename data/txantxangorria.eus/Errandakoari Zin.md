---
id: tx-2549
izenburua: Errandakoari Zin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JhCuoky6UBU
---

Azken Trenaren " Zerotik " estudioko lanaren abestia duzue hau. 2017ko ekainan grabatua Elkar estudioetan, Victor Sanchez teknikariaren eskutik.

Ahotsa eta gitarra: Jokin Pinatxo
Gitarra: Jon Macicior
Baxua: Julen Barandiaran
Bateria: Mattin Arbelaitz



Hitzak (Nahia Fagoaga):

Errandakoari zin
Eta errandakoak min
Errandakoari zin
Eta errandakoak min
 
Ekintzek hitzek zin egindakoa
ixiltzen dutenean
Ahul sentitzen naiz, harrak barrua
jan balit bezala.
 
Mutu jarraitzen dut, burua galduz,
Ikusezin bihurtuz.
Soilik ihes egin nahi dut hemendik,
Guztia ahaztuko dudalakoan.
 
Errandakoari zin
Eta errandakoak min
Errandakoari zin
Eta errandakoak min.
 
Arroxa zena beltz bihurtu zenean
Amore eman nahi nuen.
Pausu bat aurrera emateko gai
Ez nintzen momentu hartan
 
Jabetu naiz bizipen bakoitzak nire
Sustraiak sendotu dituela.
Batzuetan mina izan daiteke
Sendagairik onena.

Errandakoari zin
Eta errandakoak min
Errandakoari zin
Eta errandakoak min.
 
Errandakoari zin
Eta errandakoak min
Errandakoari zin
Eta errandakoak min.