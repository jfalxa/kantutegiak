---
id: tx-2120
izenburua: Eh Sukarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v9fcI9oZlWA
---

Gu eta gure ingurukoak
potroetara arte gaude
nahi ditugun gauza guztiak
eskuan izan arte

Borroka ezazu
gurekin batera
gu garela EH Sukarra
Dantza egin ezazu
goizetik gauera
kriston martxaz
EH Sukarra