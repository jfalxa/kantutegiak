---
id: tx-3026
izenburua: Kideari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wcTXhufkwn8
---

Zer esango adiskide kuttun,
ez naiz aldatu hainbeste.
Hau mundu hau argi ta ilun
onartu ein dut honenbestez.
Eder basoen, berdetasunak
eta hiri-zubien, lisunak,
zoratzen naute oraindik.
Bihoztu arren etorkizuna,
zalantzarako daukat azuna,
hain urrun zaude nigandik.
Ohiko amesten jarraitzen dut,
hortan jartzen dut bizia,
berehala jartzen nauzut
betiereko aurre iritziak.
etxean hildako, emazteak
eskubide duinen ebazteak,
arrazoi bide lausoan
injustizia ez da gaztea,
ez da gaitza hori ebaztea,
behatuz, gure pausoa.

Ezkortasunak gogaitzen nau,
begiratzen dut argira,
zorigaitzak beharrean diren
baina behin g/toz bizira.
Berdin-berdinak dittut akatsak,
zuzenduko dizkit azken hatsak,
marketsa naiz zorionaz.
Ez nau ga/la/ra/zi/ko go/ia/tsak,
aguro abailtzen nau mahatsak,
maitemintzen naiz, pertsonaz.
Zure inteligentzia benaz,
egiten dut gehien mira,
kulturaz bizitza ez dena
problemak deus ez badira.
Egia izaten da bakuna,
errealitatea zurruna,
eta bakana loria.
Lortu nuen denok nahi duguna,
zu bezalako mutil-laguna
ezagutzeko zoria.
zer esango adiskide kuttun,
ez naiz aldatu ha/inbeste,
hau mundu hau, argi ta ilun
onartu ein dut honenbestez.

tra/lo  ra lo la ro la lo
tra/ro la ro la lo la la/ro
la/ro la ro la ro la/ro

tra/ro  la ro la lo ra la/ro
tra/lo la ro la lo la la/ro
la/ro la ro la ro la/ro

tra/ro  la ro la lo ra la/ro
tra/lo la ro la lo la la/ro
tra/ro la ro la la ra lo

tra/ro  la ro la la ra ra la lo
tra/ro la ro la la la/ra la lo
tra/ro la ro la la la/ra lo

tra/ro  la ro la la ra ra la lo
tra/ro la ro la la la/ra la lo
tra/ro la ro la la la/ra lo


tra/ro  la ro la la ra ra la lo
tra/ro la ro la la la/ra la lo
tra/ro la ro la la la/ra lo

tra/ro  la ro la la ra ra la lo
tra/ro la ro la la la/ra la lo
tra/ro la ro la la la/ra lo

tra/ro  la ro la la ra ra la lo
tra/ro la ro la la la/ra la lo
tra/ro la ro la la la/ra lo