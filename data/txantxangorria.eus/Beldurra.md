---
id: tx-366
izenburua: Beldurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zha3iQP1uDQ
---

"Beldurra" kantaren bideoklip ofiziala. 
Kantua Musiiki estudioetan grabatua, Adrián Vallejoren eskutik.
Bideoaren egilea: Alex Sanz. 
Nahiak Nahi sortzen dugun musikariak: Nahia Ojeta, Rubén Matilla, Iñaki Pulido, Iñaki Santos eta Jaitxo Segura.

Instagram: @nahiaknahi
Facebook: Nahiak Nahi

Letra:

Suaren magalean
Arriskua saihesten
Lagun bat, lagun bat
Laguntza eskatzen

Izuaren arrakasta
Etsigarria
Beldurra, beldurra
Dator nigana

Argia urrunean
Baina berririk ez
Egunak, egunak
Sutan ez arren
Herriko karriketan korrikan
Ezkutatzeko ezer
Arian, arian
Urak harria

Nahiz urrunegi ikusi
Azken hatsa bazina bezala egin
Egin irri
Keinu bat eskuan.

Beharrunea iragan ondoan
Ametsa, ametsa asmo hutsa da.
Saiakera berri bat badoa
Zulotik ateratzeko busti behar
Azken unea heldu da
Hor doa, sute erraldoia! 

Nahiz urrunegi ikusi
Azken hatsa bazina bezala egin
Egin irri
Bihotza eskuan

Ordu txikietan
Gogoak handitzen
Atzera kontaketan 
Egunak zenbatzen 

Agertzear zara
Ezer esan gabe
Surik ederrenaz
Kaleak argitzen

Ordu txikietan
Gogoak handitzen
Atzera kontaketan 
Egunak zenbatzen 

 
Agertzear zara
Ezer esan gabe
Surik ederrena
Egiten!