---
id: tx-870
izenburua: Bihotzetatik Zintzilik "5 Urte"
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PDsvq0TTs2E
---

Siroka musika taldeak, 5 urte eta gero, euskal preso eta iheslarien eskubideen aldeko  "Bihotzetatik zintzilik" kanta berritu du eta honako bertsio hau egin du Etxerat elkartearen laguntzarekin.
Lan honetan beste musikari batzuek hartu dute parte: Petti, Gorka Suaia, Antton, Zaloa (Kokein), Antton, Andres Monsalve, Amaia Pavon, Aritz Madariaga, Igor Arzuaga...


Ilusioaren bidetik, ausardia lagun izan zenuen
etorkizuna irabazteko hautua egin zenuen
sentimenduen bidetik errepidea ezagutu genuen
urruntasunean gertu sentitzen gaituzue
Argazki bihurtu zaituzte
ixilarazi nahi zaituzte
itsu, mutu eta gor nahi zaituzte
debekatuta zaude, debekatuta gaude
debekatuta zaude, gaude, zaude, gaude....
Argazkiak hormetatik zintzilik
zuen irudiak lapurtu dituzten arren
gure artean jarraitzen duzue bizirik
gure bihotzetatik zintzilik
Irriak, laztanak, muxuak, besarkadak,
epaiketak, kolpeak, tortura, malkoak, isolamendua
txanponaren bi aldeak, borrokaren bi aldeak
Guk ezta ere ez zaituztegu nahi
argazki batean giltzapeturik
ez ziega batean, ez erbestean
soilik hemen barruan gure bihotzean
ez gara libre izango
libre denok izan arte
askatasunaren bide
bihur daitezela kilometro hauek
Argazkiak hormetatik zintzilik
zuen irudiak lapurtu dituzten arren
gure artean jarraitzen duzue bizirik
gure bihotzetatik zintzilik
Borrokaren bidetik etsipenaren etsai agertu zinen
barroteen arteko duintasuna bihurtu zinen
debekatuta zaude,
debekatuta gaude, zaude, gaude, zaude...
Argazkiak hormetatik zintzilik
zuen irudiak lapurtu dituzten arren
gure artean jarraitzen duzue bizirik
gure bihotzetatik zintzilik