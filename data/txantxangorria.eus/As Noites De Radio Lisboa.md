---
id: tx-1829
izenburua: As Noites De Radio Lisboa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1KYp-Xl9d8w
---

Ezpazioan orteaz ertzik ez... lehiotik at
argiak bustitzen du nere ontzia
grabitate ezean nigan oriomen oro
iragana bihurtzen da gardena

Lurraren azala ximeldu zen
harrizko uhinak, plastikozko mende
infinitora narama, ni naiz Ulises
planetaren hautsak haizeak
zabaltzen ditu orbean

Itzalean logelan flexoaren ondoan irratia
bihotz baten taupadak nahasten ziren
Radio Lisboaren emisioez
gauaren azken mugetan... mugan