---
id: tx-643
izenburua: Oi Pello Pello
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_FQyTWB02vE
---

Oi! Pello Pello logale nauk eta
Jinen nizan ohera?
Irun ezan eta gero gero gero
Irun ezan eta gero gero bai.
Oi! Pello Pello irun diat eta
Jinen nizan ohera?
Astalka zan eta gero gero gero
Astalka zan eta gero gero bai.
Oi! Pello Pello astalkatu diat eta
Jinen nizan ohera?
Harilka zan eta gero gero gero
Harilka zan eta gero gero bai.
Oi! Pello Pello harilkatu diat eta
Jinen nizan ohera?…