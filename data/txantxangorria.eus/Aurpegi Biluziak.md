---
id: tx-38
izenburua: Aurpegi Biluziak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Pp3XMoalSSw
---

AÑUBE taldeak 2021eko iraila eta abendua artean Julen Urzaizen bitartez Iruñako Sound of Sirens estudioan grabatutako diskaren abestia.

-----------------------------------

𝐋𝐄𝐓𝐑𝐀

Nola ulertu mundu honen grazia,
bide zuzenenak ere okertzen direnean
Nola ulertu gizakia, nola ez etsi handikeri hontan,
begiak zabaltzean ezer ez ikusten dela

EZ EGON MENPE, EZ IZAN EPEL,
ZURE ETXEA JAKIN ZAINTZEN
GAINETIK KENDU, OZTOPO ZEIN BELDUR,
IZAN ZARENAREN ISPILU

Zein gogorra den egun bereiztea,
lasterbidea aurkitzea, arriskutsua izan ohi da
Nola ez jausi sistema hontan, zuloraino barneratzean,
zaborraz gaituzte, bete oharkabean

EZ EGON MENPE, EZ IZAN EPEL,
ZURE ETXEA JAKIN ZAINTZEN
GAINETIK KENDU, OZTOPO ZEIN BELDUR,
IZAN ZARENAREN ISPILU