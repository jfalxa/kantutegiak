---
id: tx-1426
izenburua: Oihu Loreak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aHQiMtb7ykg
---

Basalore gorriak oihu, 
gerra urrun batetik oihu. Zatoz, 
su ertzetik oihu. 
Desertuko loreak oihu,
bere azken hatsetik oihu. Zatoz,
munduari oihu.
Sutan daude loreak, egin oihu,
sutan daude loreak, egin oihu.
Egunsentiko
lore zauritu,
zure eskuak
nork erre ditu?
zure oihuak
nork ito ditu?
Sutan daude loreak, egin oihu.
Begi urrun puztuek oihu,
eztarriak urratuz oihu. Zatoz,
ur ertzetik oihu.
Lampedusa loreak oihu,
itsaslami galduei oihu. Zatoz,
munduari oihu.
Ito dira loreak, egin oihu,
ito dira loreak, egin oihu.
Egunsentiko
lore zauritu,
zure eskuak
nork erre ditu?
zure oihuak
nork ito ditu?
Sutan daude loreak, egin oihu…
Sutan daude loreak, egin oihu…
Sutan daude loreak, egin oihu…