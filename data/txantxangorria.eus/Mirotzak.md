---
id: tx-2124
izenburua: Mirotzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CzkcMp_Nxyo
---

Itsas ulu zolia

Benito Lertxundi, ahots nagusia, gitarra
Juantxo Zeberio, pianoa, sintetizadoreak
Fernando Ederra, baxua
Pello Ramirez, eskusoinua, biolontxeloa
Kutxo Otxoa de Eribe, biolina
Joxe Mari Irastorza, flauta, txirulak
Gurutz Bikuña, gitarra elektrikoa
Olatz Zugasti, ahotsa, harpa, sintetizadoreak
Angel Unzu, gitarra akustikoak, bouzouki, perkusioak
Igor Telletxea, bateria
Imanol Urkizu, atabala
Laguntzaileak: 

Mikel Artieda, baxua (10)
Mikel F. Krutzaga, baxua, programazioak (5)
Orioko arraunlariak, ahotsak (10)
Gratxina Lertxundi, ahotsa (10)