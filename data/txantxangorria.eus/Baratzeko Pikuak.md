---
id: tx-56
izenburua: Baratzeko Pikuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ac3TD5zccUI
---

Baratzeko pikuak
hiru txorten ditu (Bis)
neska mutil-zaleak
hankak arin ditu
ai, oi, ei, hankak arin ditu
Baratzeko pikuak
hiru txorten ditu.

Hankak arinak eta
burua arinago (Bis)
dantzan hobeki daki
arto jorran baino.
ai, oi, ei, arto jorran baino
Hankak arinak eta
burua arinago.