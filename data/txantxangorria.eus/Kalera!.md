---
id: tx-1233
izenburua: Kalera!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iuVaEupXWo4
---

BORROKAN DABIL HERRI BAT OSORIK

ZUEN INDARRA GURE ALDE DA

BORROKALARI HERRI BAT OSORIK

ALTXA DA KALERA

Hesi artean ehunka zeldatan

Egunak eta orduak zelatan

Harrapatuta euren lehioetatik begira

Noiz helduko ote diren herrira

Lau horma dira teilatu bat eusten

Beraien kabuz ez direnak jausten

Baina indarrez eroriko den etxe hutsa da

Emaiozu azken bultzada!

BORROKAN DABIL HERRI BAT OSORIK...

Pitzatutako pareta hautsiak

Botako ditu gure erauntsiak

Gau ilun honek ere amaiera du gordeta

Egunsentia dator eta!

BORROKAN DABIL HERRI BAT OSORIK...

Herri berri bat da

Harritik harrira jasoa

Tantaz betetzeko

Askatasun itsasoa

Harri berri bat da

Herrritik herrira jasoa

Kantaz betetzeko

Askatasun itsasoa

BORROKAN DABIL HERRI BAT OSORIK

ZUEN INDARRA GURE ALDE DA

BORROKALARI HERRI BAT OSORIK

KALERA ROCk-era! (bis)