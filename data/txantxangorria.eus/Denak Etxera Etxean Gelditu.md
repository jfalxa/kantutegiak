---
id: tx-2264
izenburua: Denak Etxera Etxean Gelditu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bwr-FBcUiMs
---

Nork ote daki virus honen jatorria? Sistemaren espekulazio hutsa al da? Jendea urduri haien etxetik ateratzean,artalde bat bezala, zaindarien mesedetan! Ze panorama, que yo no quiero fama! A toda la peña yo le digo Caaalma Merkatu zein farmazik dirudite apocalipsia, ongii etorri paper higienikoaren krisira Poliziak ta militarrak atera dira, zoritxarra duten bizilagunak isuntzera, ta bitartean etxean sarturik, ez dugu zer egiteko hoberik DENAK ETXERA ETXEAN GELDITU DENAK ETXERA EZ ZAITEZ ATERA Inposatu egin diguten beldur globala, krisi hau justifikatzeko erabili duten plana Orain atratzen dituzte paradisu fiskalak, birusak tapatu ditzan monarki honen lanak. Alarma egoera honen edabe bakarra, elkartasuna zaintza eta soinu zatarrak. Batu ditzagun letrak, melodiak elkarlanaz, musikak arindu dezan egunotako zama. DENAK ETXERA ETXEAN GELDITU DENAK ETXERA EZ ZAITEZ ATERA Hasiera batean dena bazirudien broma, birus batek jartzea mundua hankaz gora Europara etorri ta itxi zuten erroma Orain guztiak etxean galtzen dugu denbora Egun konturatzen zara zer zen baliotsua, Askotan egon naiz gor askotan itsua Zeintzuk diren ondoan behar ditudanak, Kalera atera ezin eta buruan mila kana. Pare bat egun ta oraindik geratzen direnak, Ez da ezer entzuten ez oihuk ez sirenak Orain txoriek kentzen herriari isiltasuna, Ez diegu inoiz entzun pentsa zein astuna. Last tunak sesioetan egia ziren orduan Esatean ez zitzaidan burutik pasa Tasak jarri ta berriz zinturoiak estuak, Langile klasearentzat neurriak eskasak DENAK ETXERA ETXEAN GELDITU DENAK ETXERA EZ ZAITEZ ATERA


MacLiman