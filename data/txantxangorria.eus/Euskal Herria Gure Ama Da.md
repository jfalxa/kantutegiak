---
id: tx-1837
izenburua: Euskal Herria Gure Ama Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RI5QFAQHGXg
---

Lapurdi eta Bachenabarre,
Zibero eta Nabarra
Gipuzko herri eta Bizkaia
Zazpigarrena Alaba !
Guziek elgar atchik dezagun
Niholere ahal bada
Aiten kostumak segi ditzagun
Ukha ez dezagun Ama.

Eskual-herrian gaizki gabiltza
Zorigaitzez aspaldian;
Hantche kastillaz, hemen frantsesez
Ari gira solasean;
Denen ikhastia gizonak on du, dabilalarik munduan;
Bainan eskuara, ahantzi gabe,
Beira dezagun buruan.

Ala Frantzian nola Españan,
Etsai batzu errabian,
Gure mintzaia galdu beharrez
Ari dirade lanean:
Ama utz-eta amaizuna har,
Gertha litake orduan !
Geldi gaitezen gu Amarekin:
Hoberik ez da munduan !

4.Aita eskuaraz mintzo da eta
Semean arrapostua:
Soy Vascongado, pero ahantzi
ttipitikan ikhasia !
Ez dea hori amarendako
Bihotz-erdira-garria ?
Berak mundurat eman ondoan
Semean arnegatzea