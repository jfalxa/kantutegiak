---
id: tx-2649
izenburua: Ilargia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rlIg1PhK_IU
---

paulineetjulietteduo@gmail.com


esaiozu euriari berriz ez jauzteko,
esan bakardadeari gaur ez etortzeko.

eusten nauen soka zara eta itotzen nauena,
ametsak sortu zizkidana, galtzen dituena.

zuretzat ilargia lapurtuko nuke gauero,
eta zu itsu zaude bere argia ikusteko,
irribarrez, gero minez, eragin didazu negarra,
nire sua itzali da,
ez zara gaueko izar bakarra, ez zara!!

esan sentitzen dudana ez dela egia,
une baten sinesteko ez garen guztia.