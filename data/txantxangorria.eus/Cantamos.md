---
id: tx-3372
izenburua: Cantamos
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0PdaWLnWlls
---

Cantamos nestes tempos escuros
Com raiba a poesía recreadora de conciencia
Cantamos para que o povo deixe
De ter medo e saia do silencio
Abestu txileko estadioan
Egin zuten monduan borreroaz farre eginez
Abestu guztiok espetxetan
Entzun dezaten eraman nahi dutenek
Gazteriaren ahotsa espaniar espetxetan ixteko helburua
duten muntaiak asmatuz
Cantamos com linguas que botam faiscas, 
do lume da inquedança manifesta a tua raiba
Cantamos para que o justo deixe 
De estar definido como algo ilegal
Cantamos como o cantar de Cuba
Que nos trae Carlos Puebla
Ambas maos cortaronlhe 
o amigo Victor Jara pero a sua guitarra 
segue dando ecos de son.
Cantamos com linguas que botam 
faiscas do lume da inquedança 
manifesta a tua raiba.
Por fortuna ainda seguem nacendo 
nos povos tupamaros que luitam pola justiça.
Cantamos com linguas 
que botam faiscas do lume da inquedança, 
manifesta a tua raiba