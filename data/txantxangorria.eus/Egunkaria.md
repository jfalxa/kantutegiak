---
id: tx-1844
izenburua: Egunkaria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cbq_uNcndGQ
---

EGUNKARIA
«JO EZAK!»
IMUNTZO ETA BELOKI
ELKAR, 1991
 
Hor dago egunkaria
behar zen mezularia
hor dago egunkaria
behar zen mezularia
euskal tinta ta paperez
goizero prentsa berria
goizero prentsa berria
hor dago egunkaria
 
Xaho eta Lauaxeta
Xaho eta Lauaxeta
lumak liluraz beteta
herri berriko kronikak
idazteko prest daude ta
idazteko prest daude ta
Xaho eta Lauaxeta
 
Urrutiko intxaurren hotsak
urrutiko intxaurren hotsak
bertako tristura pozak
zuzen kontatuz garaitu
español frantses arrotzak
frantses español arrotzak
urrutiko intxaurren hotsak
 
Batasun bideak urra
hautsi muga ta gezurra
batasun bideak urra
hautsi muga ta gezurra
egunkaria izan bedi
abertzaleon ikurra
abertzaleon ikurra
batasun bideak urra
 
Hitzak: P. Zubiria
Musika: Imuntzo-Epelarre