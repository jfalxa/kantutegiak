---
id: tx-1029
izenburua: Si Ma Ma Ma Ka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Gcsv-NfX31w
---

Musika: Herrikoia (Ghana)
Letra: Herrikoia eta Koldo Celestino


Simamaka, simamaka
ruka, ruka, ruka, simamaka
Simamaka, simamaka
ruka, ruka, ruka, simamaka

Tembea, kimbea, tembea, kimbea,
ruka, ruka, ruka, simamaka
Tembea, kimbea, tembea, kimbea,
ruka, ruka, ruka, simamaka

Algaraka, algaraka,
talka, talka, talka, algaraka
Algaraka, algaraka,
talka, talka, talka, algaraka

Txaloka, saltoka, txaloka, saltoka,
talka, talka, talka, algaraka
Txaloka, saltoka, txaloka, saltoka,
talka, talka, talka, algaraka