---
id: tx-1904
izenburua: Haurtxoa Sehaskan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/am0ctvKvtGQ
---

Haurtxo polita sehaskan dago
zapi zuritan txit bero.
Haurtxo polita sehaskan dago
zapi zuritan txit bero.
Amonak dio, Ene potxolo!
Arren egin ba lo, lo!
Amonak dio, Ene potxolo!
Arren egin ba lo, lo!
Txakur handia etorriko da
Zuk ez badeza egiten lo.
Txakur handia etorriko da
Zuk ez badezu egiten lo.
Horregatik ba Ene potxolo!
Egin aguro lo, lo, lo
Horregatik ba, ene potxolo!
Egin aguro lo, lo, lo