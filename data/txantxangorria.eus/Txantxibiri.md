---
id: tx-2736
izenburua: Txantxibiri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9whGme8Ejz8
---

Txantxibiri, txantxibiri gabiltzanian
Elorrioko kalian,
hamalau atso tronpeta jotzen
zazpi astoren gainian.

Astoak ziren txiki-txikiak,
atsoak, berriz, kristonak!
Nola demontre konpontzen ziran
zazpi astoren gainian (bis).

Saltzen, saltzen,
txakur txiki baten karameluak,
Está muy bien!
esaten du konfiteroak (bis).

Cuando vamos a Otxandiano
karnabal egunian,
me cagüen la mar,
comeremos chocolate
sonbreruaren gainian.

Anteron txamarrotia,
Sinkerren bibotia
Haretxek ei dauko,
ei dauko,
preso Euskal Herria.
Corre, corre maquinista
a toda velocidad
que la máquina del tren
va a llegar….
Y nos vamos hasta Boise
San Inazio egunean