---
id: tx-1075
izenburua: Su Txikien Itsasoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DKxi38tHluQ
---

Askapena eta militante internazionaliston kriminalizazioaren aurrean Sirokak, Bad Sound System eta Skama la Rede (Asturiarrak) taldeen kolaborazioarekin egindako abestia eta nazioarteko lagunen ekarpenekin batera indarrrez beteriko bideoklip hau grabatu genuen. 

Orain, fiskalaren espetxeratze eta ilegazizazio eskaeren aurrean ekimen eder honi argia eman eta ozen aldarrikatu nahi dugu lanean jarraituko dugu eraiki nahi dugun etorkizun horren alde: munduko herrion askatasuna!

Hemendik eskerrak eman nahi dizkiegu abesti eta bideo honen osaketan parte hartu zenuten guztiei. Mila esker!

Andoaingo Garate Estudioetan Kaki Arkarazoren gidaritzapean grabatua
Musika: Siroka taldeak Bad Sound System eta Skama la Rede taldearen kolaborazioarekin
Letra: Ruben Sanchez
Ideia, gidoia eta "cineamanoa": Arturo Lpz "Pio"
Argazki zuzendaria: Irantzu Pastor
Zuzendaria eta edizioa: Uraitz Soubies

LETRA:

Salvadorretik zerura igo zen euskaldun batek mundua ikusi zuen han goitik

ta bueltan zera esan zigun mundua su txikien itsaso bat dela, su txikien itsasoa


Herri bakoitzak ere, bere sua dauka eta batzuen sua ain da bizia

ez dago itzal dezakeen ekaitzik, erautsik, errautzik


Delitu bakarra elkartasuna

herrion arteko xamurtasuna

eraiki nahi dugun etorkizuna

munduko herrion askatasuna


Eh! Eh! Gizakiak bezala, uste dut denok lagundu behar dugula

gerora hobe izateko bide bakarra, eskuz esku hartu lepotik ta segi aurrera

denok norbait edo zerbait galdu dugula, beti gabiltz askatasunerantz begira

BadSound, Siroka eta Skama elkartasuna, beti egongo gara kateen kontra!


Eskubideei indarra, surik ezin hil, zazpiak bat bagara

Askatasuna, amets bakarra. Elkartasuna...Askapena!

Suaren indarraren izaera, su bueltan herri denak biltzen gera

bihotzetatik sekula irten ez dena aberria nola den

Elkartuz, elkartzen, elkarkor, ez autsi segi zutik gogor

Euskal Herria dabil gogoaz, Askatasuna!


Herri bakoitzak ere, bere sua dauka

eta batzuen sua ain da bizia

ez dago itzal dezakeen ekaitzik

erautsi errautzik


Delitu bakarra elkartasuna

herrion arteko xamurtasuna

eraiki nahi dugun etorkizuna

munduko herrion askatasuna


Agora vamos valtar toles rexes d´aquelles prisiones

que ciarren alos qu´esbrexen pola nuestra llibertá.


Vamos dir toos construyendo l´alternativa social

de millones de persones que quieren vivir en paz.


Da igual la llingua que falemos, el camín ye la conciencia,

el destín ye´l derecho a decidir, lóbxectivu: la independencia.


Delitu bakarra elkartasuna

herrion arteko xamurtasuna

eraiki nahi dugun etorkizuna

munduko herrion askatasuna