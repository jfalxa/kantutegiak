---
id: tx-1791
izenburua: Heriotzaren Begiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dUcjfMfrXQ8
---

Etorriko da zure soaz heriotza
gor, logabe, alhadura zaharren gisa,
goiznabarretik gauera alboan dugun
ohidura zentzugabe bat bailitzan.

Zure begiak alferrikako hitza izanik
garraisi mutu, isiltasun oro isil,
goiz bakoitzean aurkitzen dituzu adi
ispilura begiratuz, ilun, hurbil.

Jakinen dugu egun hartan, oh
itxaropen!
bizitza zarela eta ezereza,
guztiontzat du heriotzak soa zorrotz
bakar, mutu, leizera jetsiko gera.

Urratuko da ezpain hertsien keinua,
aurpegi arrotza leihoaren ondoan,
usadioen guneak desitxuratuz
biluztasuna nagusitzen denean.

Zure begiak argi grisaren errainu,
mendi ilunen goizorduko izotza,
esnatzearen dardara eta ikara
kale hutsetik hurbiltzen zarenean.

Jakinen dugu egun hartan, oh
itxaropen!
bizitza zarela eta ezereza,
guztiontzat du heriotzak soa zorrotz
bakar, mutu, leizera jetsiko gera