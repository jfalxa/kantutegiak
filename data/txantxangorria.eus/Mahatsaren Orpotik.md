---
id: tx-1521
izenburua: Mahatsaren Orpotik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DfxHpb5T8_Y
---

Mahatsaren orpotik dator
mama gozoa, mama gozoa.
Edango nukeela beterik
basoa,
klink, beterik basoa.
Nik zuri, zuk neri,
agur eginaz alkarri,
basoa txit garbi
beharko da ipini