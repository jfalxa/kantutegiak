---
id: tx-980
izenburua: Iraultza Alaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/phpv8Ptmedw
---

Hemen nago, atzoko ajien arrastoarekin
idazten dot paper berde huts baten
neure buruari galdezka:
ea noiz arte jarraitu behar dan
burue makurtute hemen
stand by egoeran
anulatute gagoz
piztu dana, zutunik jarri
begirada gora, hey, goazen!

Ozen esateko egune heldu da
gauzak aldatzeko bildurrik ez dagoela,
noiz arte inoren menpe?
Europa zaharreko azken indioak
txikiak gara bihotz handikoak
luzaro iraungo ahal dugu?
edo desagertu?
Ez, ez !
Altxeu gaitez, goazen!

Oh ama lurra, oh gure lurra,
begi gorriek, esku zuriek
herri bat ez da sekula hiltzen
kantetan daben artien,
sinistu egizu posible dala.

Irrifar bategaz dana aldatu leikela,
iraultza alaia martxan dago
eta ez dago gelditzerik
...zabaldu egizu mezue!
Oh ama lurra, betiko lurra
herritxiki bat aurrera begire
bizirik dago gure arima,
bizirik gure iraultza.

Sinistu dagigun posible dala
irrifar bategaz dana aldatu leikela
gure hizkuntza martxan dago eta
zabaldu egizu mezue.