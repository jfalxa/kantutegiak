---
id: tx-33
izenburua: Kilima Aldaketa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zwP3UhRQpxg
---

Uda diskoko lehen singlea. Imanol G.Gurrutxaga eta Nagore Muriel (I.G.G. Ekoizpenak) izan dira bideoaren egileak.

Aktoreak: Fariba Sheikhan, Ane Lauroba, Maria Isabel Uriarte, Eneko Sheikhan, Ianire Usandizaga eta Karmele Ugalde.

Musikariak: Hilario Rodeiro, Olaia Inziarte eta Izaro Andres.

Musika eta hitzak: Iker Lauroba

KILIMA ALDAKETA

Urtzaroa 
bizkortu duzu poloetan,
glaziarretan
epel ta eroso ginenean.

Eten dira 
komunikabideak,
gure artean
Beringiako zubirik ez dago.

Galdu naiz
aurki aurkituko nauzulako,
aztarnak 
irakurtzen ikasi duzulako
eta bihar
topa egiten topatuko nauzu
basamortuan.

Zure eskuen
kilima aldaketak
azalean
inurritu dizkit nahiak.

Idoloak 
arpilatzaileentzat,
urakanek
eraitsi dituzte gurtza lekuak

Galdu naiz…

Kilima aldaketa
izotz aro honetan.

Kilima aldaketak
zure laztanik gabe utziko nau.

Kilima aldaketa
izotz aro honetan.

Kilima aldaketak
zure azalik gabe utziko nau.