---
id: tx-785
izenburua: Xxi Mendean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YmnoHQ2pxlk
---

2ZIO taldearen 'Bideak' diskoko 'XXI.mendean' abestiaren bideoklipa. Pette Etxebarriak egina. Mila esker parte hartu duten guztiei!

Hitzak:

XXI.mendean oraindik atzerakoi,
bidea egin gabe ukitu nahi da gailurra.
Aintzina joateko arrazoi
bakarra da atzean gelditzeko beldurra.

Gizakia hil du presak, estresak,
orain politikoek ematen dituzte mezak.
Inmobilizatu gaitu askatasun ezak
ta pertsona mugitzen du interesak.

Bitxia da sentitzen dena erratea,
normala da kontatzera telebistara joatea,
kamarak denetan izatea.
Polizientzat reality bat da gure intimitatea.

Sexualitatea: oroitzapenen kutxa,
gizonak kudeatzen du emazteen gorputza.
Bankueri laguntza,
aurrerakuntzaren izenean murrizten da hezkuntza.

Gezurra ofizial, egia klandestino.
Futbola eta droga dira sinonimo,
baloi baten atzetik mugitzen da herria
Al Capone berria deitzen da Florentino.

Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratzen gara galtzen gu.
Askatasuna diruz erosten da
ta aske izan nahi duenak kartzelarekin ordaintzen du.

Noiz ta nun galdu ginen? Zein izan zen unea?
Noiz hartu genun atzerakuntzaren bidea?
Gailuen jostailu hondamenaren ertzean,
mugikor baten esklabu hogeita bat garren mendean.

Dirua: sistema hontan jainkoa,
ez zara inor barruan ez baduzu nahikoa,
polita, ederra, eta denen gustokoa,
nazkagarria sortu dugun prototipoa.

Lanean ta lanetik lekora
lepora lotutako hariekin nora?
Orduka kobratzen dugu eta gu gustora,
neurri batean saltzen dugu gure denbora.

Lekurik ez etxean, jakirik ez eltzean,
berdintasuna kaletik dabil noraezean...
Baina pozik ibili zaitezke aurrerantzean
inor zutaz oroitu bada urtebetetzean.

Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratzen gara galtzen gu.
Askatasuna diruz erosten da
ta aske izan nahi duenak kartzelarekin ordaintzen du.

Munduan den biztanleriaren hiru laurden
gosez hiltzen ari da
eta hemen janaria dastatu baino lehen
botatzen da zakarrontzira.

Beti mugimenduan baina beti zai,
edozein bizitza bihurtu da balada,
egunean zernahi eginda ere ohera
kontzientzia lasai joateko gai bagara.

Beti baten menpeko, laneko orenez lepo,
eko gabeko kexa batzuk tarteko.
Egun bakoitzeko hobeto isildu
ta sos pixkat bildu bizirauteko.

Egungo sisteman ezin onik atxeman:
ez duenari kendu ta duenari eman.
Handiuste guziak boteretik joan bitez,
jainko ttiki ainitz daude ta fededunik ez.

Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratz

XXI.mendean oraindik atzerakoi,  bidea egin gabe ukitu nahi da gailurra.
Aintzina joateko arrazoi bakarra da atzean gelditzeko beldurra.

Gizakia hil du presak, estresak, orain politikoek ematen dituzte mezak.
Inmobilizatu gaitu askatasun ezak ta pertsona mugitzen du interesak.

Bitxia da sentitzen dena erratea, normala da kontatzera telebistara joatea,
kamarak denetan izatea.
Polizientzat reality bat da gure intimitatea.

Sexualitatea: oroitzapenen kutxa, gizonak kudeatzen du emazteen gorputza.
Bankueri laguntza, aurrerakuntzaren izenean murrizten da
hezkuntza.

Gezurra ofizial, egia klandestino. Futbola eta droga dira sinonimo,
baloi baten atzetik mugitzen da herria Al Capone berria deitzen da Florentino.
Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratzen gara galtzen gu.
Askatasuna diruz erosten da
ta aske izan nahi duenak kartzelarekin ordaintzen du.

Noiz ta nun galdu ginen? Zein izan zen unea? Noiz hartu genun atzerakuntzaren bidea? Gailuen jostailu hondamenaren ertzean, mugikor baten esklabu hogeita bat garren mendean.
Dirua: sistema hontan jainkoa,
ez zara inor barruan ez baduzu nahikoa, polita, ederra, eta denen gustokoa, nazkagarria sortu dugun prototipoa.
 


Lanean ta lanetik lekora lepora lotutako hariekin nora?
Orduka kobratzen dugu eta gu gustora, neurri batean saltzen dugu gure denbora.

Lekurik ez etxean, jakirik ez eltzean, berdintasuna kaletik dabil noraezean . Baina pozik ibili zaitezke aurrerantzean inor zutaz oroitu bada urtebetetzean.

Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratzen gara galtzen gu.
Askatasuna diruz erosten da
ta aske izan nahi duenak kartzelarekin ordaintzen du.

Munduan den biztanleriaren hiru laurden gosez hiltzen ari da
eta hemen janaria dastatu baino lehen botatzen da zakarrontzira.

Beti mugimenduan baina beti zai, edozein bizitza bihurtu da balada, egunean zernahi eginda ere ohera kontzientzia lasai joateko gai bagara.

Beti baten menpeko, laneko orenez lepo, eko gabeko kexa batzuk tarteko.
Egun bakoitzeko hobeto isildu ta sos pixkat bildu bizirauteko.

Egungo sisteman ezin onik atxeman: ez duenari kendu ta duenari eman. Handiuste guziak boteretik joan bitez,
jainko ttiki ainitz daude ta fededunik ez.

Etekinak materiaz nahi dira zenba,
ni-ak agintzen badu ateratzen gara galtzen gu.
Askatasuna diruz erosten da
ta aske izan nahi duenak kartzelarekin ordaintzen du.