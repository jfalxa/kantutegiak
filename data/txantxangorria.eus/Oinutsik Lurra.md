---
id: tx-1617
izenburua: Oinutsik Lurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i_SIAkh0w0Q
---

Maite ditut zure oinutsak

lurrez zikin jantzita.

 

Gustuko ditut zure oinak.

Gustuko dut haiek magalean edukitzea

katutxo izutuak bailiran.

 

Maite ditut zure oinutsak

lurrez zikin jantzita.

 

Hondartzetako kristal izkutuek

utzitako orbanen orijinaltasuna,

eta zure behatzek 10 izateko duten ohitura.

 

Maite ditut zure oinutsak

lurrez zikin jantzita.