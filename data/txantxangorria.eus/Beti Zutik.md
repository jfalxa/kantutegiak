---
id: tx-3385
izenburua: Beti Zutik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jqq-2SHYXYY
---

Bizitzaren alde latzak
ezagutzen ditugu
ez gara makurtu
eta ez dugu intentziorik

Borreroaren aurpegiaz
jabetu baikara
hamaika mehatxu,
hamaika erantzun
BETI ZUTIK

Bizitzaren alde onak
gozatzen ditugu
ez gara geratu
eta ez dugu intentziorik

Gure amets ta ideiak
borrokatuko ditugu
hamaika mehatxu,
hamaika erantzun
BETI ZUTIK

Esna adi!
zure bila datoz mugitu adi
presta adi!
geurea dugu garaipena

Arrotzek ezpataz hil nahi banaute
ixilduko gaituztelakoan
zutitu eta euskaraz mintzatuko naiz
neure hiltzailearen aurrean