---
id: tx-143
izenburua: Ezinbestia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ybsz85LczHo
---

Ezinbestia 
Liliak dirade bertan tximaltzen, gük ere, ber-gisan, itxüra galtzen, Gaztek hori eztie untsa senditzen, xahartürik baizik ez borogatzen. 
Gazte, odol-bero izanik, tenore goxoak iganik, 
Jauzi handienak eginik, bazterra zuin laster den jinik! 
Orai nun zidie gazte lagünak? Nun dira zirekin igan egünak? 
Aldiz hor dütügü eritarzünak, gützaz dolü gabe horra zaizkünak. 
Oroek hau dügü ezinbestea, mügatürik dela gure bizia, 
Bena bardin azkar bizi-nahia, eder izan bedi beste mündia! 
Hitzak: Roger IDIART eta Jean ETCHART 
Müsika : Jean ETCHART