---
id: tx-2399
izenburua: Zubiak Batuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YFZy9h7up2g
---

Urretxu Zumarraga Ikastolak antolatzen duen Kilometroak 2018 jaiaren abesti ofizialaren bideoklipa. Partehartzaileak hauek dira:

Hitzak: Urretxu Zumarragako Bertso Eskola
Doinua: Secundino Esnaola Musika Eskola

Abeslariak: Sergio Patxuko, Maddi Martin, Uxue Kerejeta eta Secundino Esnaola Musika Eskolako abesbatza
Musikariak: Secundino Esnaola Musika Eskola
Gidoia: Urretxu Zumarraga Ikastola eta Iñigo SBeltza.