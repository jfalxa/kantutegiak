---
id: tx-3073
izenburua: Eia, Eia Aire Ahizpak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T266Jcuim-4
---

Eia, eia, eia, eia
norbait kanpoan ote da?
Langileak ote dira?
Langileak baldin badira 
barnera sar ditezela.

Eia, eia, eia, eia
norbait kanpoan ote da?
Langileak ote dira?
Nagusiak baldin badira
kanpoan egon ditela.