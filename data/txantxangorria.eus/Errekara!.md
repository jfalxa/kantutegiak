---
id: tx-154
izenburua: Errekara!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GbPuOz89zl0
---

ERREKARA!

Txipli ta txapla, txipli ta txapla
errekatik itsasora,
gaur ekainaren hogeita seia
hau ietarron ganora!
kanpotik busti, barrutik busti
zatoze gure ondora!
gaur ietarron eguna da ta
erdu udaren berora!

Errekara, errekara!
IEtarrok errekara!
Errekara, errekara!
IEtarrok errekara! 

Negua joan zan, uda badator
hau bai sasoi emankorra!
gaur errekara salto eginda
plisti-plasta orokorra
bildur barik ba hondatu prakak
soineko eta atorra,
lehorrekoak, etorri hona,
uretan busti mokorra.

Errekara, errekara!
IEtarrok errekara!
Errekara, errekara!
IEtarrok errekara! 

Errekan hasi, besarkatuta,
kantu alaia ahoan,
ate gainean dantza egiten,
aurresku ta fandangoan,
freskatzen gara, bustitzen gara,
bata besteen ondoan,
errekan behera, gure parranda
bainauko da itsasoan.

Errekara, errekara!
IEtarrok errekara!
Errekara, errekara!
IEtarrok errekara! 

************************************************
- Musika originala: Josu Bergara eta Agustín Bergara.
- Letra: Karmelo Landa.
- Musikari eta abeslariak: Josu Bergara, Naroa Gaintza, Iholdi Beristain, Ardoa Barrura Fanfarrea eta IEko kantu poteoko taldea.
- Musika moldaketak: Mikel Nuñez Lauzirika
- Produkzio musikala: David Sanchez Damián. Blue Bayou Estudioa.

- Ardoa Barrura Fanfarrea osatzen dute:
Roberto Alonso, tronpeta; Santi García, saxofoia; Jon Txintxurreta, saxofoia; Igor Crespo, bonboa, saxofoia; Unai Ercilla, tronpeta eta tronboia; Jesús Rioja, tuba; Richard Isasi, tronpeta; Ibon Meabe, kaxa edo danborra; Asier Villan, txindatak; Roberto González, tronboia; Garikoitz Badiola, tronboia; Jaime Esteban, tronpeta; Aitor Menendez, helikoia; Txomin González, bonboa.

- IEKO kantu poteoko taldetik grabaketan  parte hartu dute:
Karmelo Landa Mendibe, Joxe Mari Landa Mendibe, Andoni Landa Mendibe, Irune Benedicto Arbaiza, Begoñe Aspiazu Iribar, Marisol Aguirre García, Sara Egia Agirre, Nerea Rodríguez Uribe-Etxebarria, Amaia Muniozguren Agirre, Garbiñe Muniozguren Agirre, Manu Fernández Saiz, Nerea Muniozguren Agirre, Ernesto Ramirez, Idoye Errazti Olartekoetxea, Amaia Bergara Aretxabaleta, Julen Cáceres Iriondo eta June Bergara Gonzalez.

- Dantzaria: Ibabe Beristain
- Txistulariak: Eduardo Aretxabaleta eta Alexander Iurrebaso.

- Bideogileak: Kostako Vibes

Ean, 2022ko ekainaren 26an.
Eskerrik asko danori!

©Josu Bergara