---
id: tx-1445
izenburua: Koilarak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h9McJDrLgXI
---

Etxe bat bezala balitz nire bizitzaren armairua
Etxe bat bezala balitz nire bizitzaren gordailua
jarriko nuke limoiondo bat gazi-gazia
eta gero eraiki etxeko ataria.

Etxe bat bezala balitz nire bizitzaren armairua
banatuko nuke hiru pisutan ere urteen pisu guztia.
Eta eskailerak ahalik eta aldapatsuen
eta azpian atseden ezkutalekua.

Eta gela guztietan oheak beti bikoitzak,
bikoitzak oheak eta binaka gela bakoitzean,
biren handitasun batek har dezan atseden,
tamainan ta binan.

Eta gela batean
bi ohe bakar pegatuta
har dezaten arnasa 
izaretatik ebatuta.
Eta aurrean, bilutsik eta garbi ikusteko elkar
dutxa zuri bat.

Etxe bat bezala balitz
nire bizitzaren armairua,
lortuko nuke bizi bat etxeko solairu bakoitzean,
eta koilarak beti handiak eta txikiak,
eta paretetako arrakalak argazkiz josiak.