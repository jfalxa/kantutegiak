---
id: tx-2483
izenburua: Prefosta! Herri Urrats 2018
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5tpfztVt2tE
---

Aurtengo klipa Manex Erdozainzi Etxart kolegioan grabatu dugu. Denek, ikasleek, erakasleek, langileek, burasoek parte hartu zuten !


Lehen agian eta orain prefosta!
Gure ametsei jarraika dena posible baita, kosta ahala kosta.
Koska bat gorago dago koska
euskararen lurrera hazi berriak datoz-ta.

Ikasten, hartu, eman hitza
transmititzen den jakintza ez baita nehoiz ahazten.
Hazten ertzetatik, ametsetatik hasten...
Iragana irakurtzen ta etorkizuna idazten.

Atzokoen hitza gaurko irakaspena,
gaurko izerdia biharko lorpena,
jaso duguna da eman zigutena,
ez al da emateko jaso genuena?

Lotuz mihia eta bihotza,
gaindituz beldurra eta lotsa...
Agian amets batzuk txiki geratu zaizkigu
baina amets berriak ere egiten badakigu.

Ibiliz ez ote da egiten bidea?
Erantzuna: prefosta!
Usteak uste, urratsak egin ditugu ta urratsek egin gaituzte.
Hemen gira, prefosta!
Ibiliz ez ote da egiten bidea?
Erantzuna: prefosta!
Urratsak egin ditugu ta urratsek egin gaituzte.
Kosta ahala kosta.
Urteen esperientzian barnekalden zein kostan

Orain prefosta eta lehen agian!
Oinetako zaharrekin zenbat urrats diren egin ibilbide berrian:
Hizkuntza ikuspegian, hasteko,
punturik ez jartzeko esaldien erdian.

Zenbat lan borondatezko, etxeko, kaleko...
urrats berri bat egun bakoitzeko.
Egin den lanak fruituak eman izanak
erakusten du oraindik zenbat baden egiteko.

Ez gabiltza lur antzutan,
lurrak zer dakarren ez dakigun arren, batzutan,
ametsen fabrika diren prefabrikatutan
anitz baloratzen da ez bada anitz ukan.

Izan gaitezen baikor!
Berreskura dezagun beti egon dena hor.
Anitzetan oinarria ez al dago ertzean?
Hizkuntzak ez baitu mugarik guk ezarri ezean.

Ibiliz ez ote da egiten bidea?
Erantzuna: prefosta!
Usteak uste, urratsak egin ditugu ta urratsek egin gaituzte.
Hemen gira prefosta!
Ibiliz ez ote da egiten bidea?
Erantzuna: prefosta!
Urratsak egin ditugu ta urratsek egin gaituzte.