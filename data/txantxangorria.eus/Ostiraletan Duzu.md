---
id: tx-498
izenburua: Ostiraletan Duzu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D36k241dkww
---

Ostiraletan duzu Garruzen merkatu
ene maite pollita han nuen kausitu
pott bat galdegin neron xapela eskian
biga eman zautadan nigarra begian.

Gero galdegin neron " nigarrez zer duzu?"
Hola xangrinatzeko sujetik ez duzu".
Arrapostu'man zautan "Zuhaurrek dakizu,
ene nigar ororen sujeta zzira zu".

Hola denaz geroztik ez egin nigarrik,
ez daite arrosarik elhorri gaberik,
zu hola penaturik urritiki baitut nik,
plazer duzuna duzu eginen nitarik.