---
id: tx-3136
izenburua: Gazte Eguna Urduñan Izargi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rOcpLHfjpzs
---

Lau urteko borroka
gure udaletxean
Lau urteko arazoak
Urduña izoztu nahian
Hala /re bidea
ez dago ilunean
urrunean ikusten da
argia GAZTETXEAN
Ez dago hamairurik
hurrengoa baizik
lau urte kalean
herri osoa zutik
Ez dago hamairurik
hurrengoa baizik
herri osoa zutik
GAZTETXEAaaa!! bizirik

U/da/be/rri guz/ti/e/tan
os/pa/tzen du/gu ja/ia
ka/le/ak be/te/tze/a  lor/tu
mar/txo/an zein a/pi/ri/lan

le/ku/a i/txa/ron du/gu
i/ri/tsi da ga/ra/ia
de/non ar/te/an ur/tze/ko
i/txi/ta/ko za/rra/ila

Ez da/go ha/mai/ru/rik
hu/rren/go/a bai/zik
lau ur/te ka/le/an
he/rri o/so/a zu/tik
Ez da/go ha/mai/ru/rik
hu/rren/go/a bai/zik
he/rri o/so/a zu/tik
GAZ/TE/TXE/Aaaa!! bi/zi/rik

HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!
HE/RRI BAT GAZ/TE/TXE BAT!!!

Sor/gi/ña/ren la/gun/tzaz
e/gin/go du/gu au/rre/ra
e/guz/ki/a/ren be/ro/az
i/zo/tza  de/segi/te/ra
bi/ho/tza/ren tau/pa/daz
i/bai ba/ten an/tze/ra
he/rri ba/ten bul/tza/daz
i/tsa/so/an ba/rre/na

Ez da/go ha/mai/ru/rik
hu/rren/go/a bai/zik
lau ur/te ka/le/an
he/rri o/so/a zu/tik
Ez da/go ha/mai/ru/rik
hu/rren/go/a bai/zik
he/rri o/so/a zu/tik
GAZ/TE/TXE/Aaaa!! bi/zi/rik

BI/ZI/RIK!!

Ez da/go ha/mai/ru/rik
hu/rren/go/a bai/zik

GAZ/TE/TXE/Aaaa!!


GAZ/TE/TXE/Aaaa!!


GAZ/TE/TXE/Aaaa!!

BI/ZI/RIK!!