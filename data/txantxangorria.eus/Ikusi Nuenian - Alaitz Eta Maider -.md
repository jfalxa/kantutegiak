---
id: tx-3343
izenburua: Ikusi Nuenian - Alaitz Eta Maider -
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KxPjNfevA88
---

1.Ikusi nuenian
nik zure kokotza,
iruditu zitzaidan, monona,
gaztainân morkotsa.

2.Ikusi nuenian
nik zure ahoa,
iruditu zitzaidan, monona,
karobi zuloa.

Errepika:
""Sí, morenita, sí,
si es para divertir;
tu llevarás la manta, morena,
yo llevaré el candil.""

3.Ikusi nuenian
nik zure sudurra,
iruditu zitzaidan, monona,
odolki muturra.

4.Ikusi nuenian
nik zure begia,
iruditu zitzaidan, monona,
anpolai gorria.

Errepika:
""Sí, morenita, sí,...""

5.Ikusi nuenian
zure belarria,
iruditu zitzaidan, monona,
azaren orria.

6.Ikusi nuenian
nik zure kopeta,
iruditu zitzaidan, monona,
frontoiân pareta.

Errepika:
""Sí, morenita, sí,..."