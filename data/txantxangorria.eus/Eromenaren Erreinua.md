---
id: tx-2443
izenburua: Eromenaren Erreinua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AMgvsYhB7u0
---

Ahuntzaperen 3.diskako aurrerapen kanta.

AHUNTZAPE - ITZALEN BABESEAN

Eromenaren erreinua

LETRA

Nire buru barruan
Nire zainetan zehar
Amorrua izeneko emozio eroa
Gorputz barnean eta ez dakit nola atera
Odolak garraiatzen ditu
Nire kaosaren globulo gorriak

Zerbait egin beharra daukat
Hau onartezina da
Odolaren lurrina sentitzen dut
Borrokaren gosez beteko naiz

NIRE BARNEKO SUA EROMENAREN ERREINUA
KAOSAREN POZOIAK

Amorruz beteriko lau hitzen artean...
Hitzik gabeko doinu baten atzean
Hemen aurkitzen dut babesa
Zurekin bukatzeko dudan modu bakarrean

Haginak estutzen ditut
hausnarketa egitean
Ohartzen naiz bai naiz
Nire eromenaren kaosak