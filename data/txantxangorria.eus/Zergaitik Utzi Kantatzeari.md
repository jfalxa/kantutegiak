---
id: tx-1790
izenburua: Zergaitik Utzi Kantatzeari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KjuZ8rQUi2A
---

Nere bizitza, munduko arranguren 
gainetik, amaiezinezko 
kantu batean, airos doa
nere bizitza kantu batean.
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?

Urrun bada ere, sorkuntza berri bat
irigartzen duen doinu hori
entzuten dut, harrabots eta 
borroka ororen artean.
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?

Ekaitzak ondoan ziztuka
ari bada, zer axola!
Egia bizirik dela badakit
bizirik dela badakit
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?

Zer axola nere inguruan
iluna hertsitzen bada?
Gauean kantak sortzen ditu, 
Gauena kantak sortzen ditu.
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?

Nere pake kuttunena
itzutuko duen trumoirik ez da
harkaitz horri lotzen natzaion
lotzen natzaion bitartean.
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?

Amodioa zeru-lurren
jauna bada, jauna bada
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?
Zergaitik utzi behar diot, 
zergaitik utzi kantatzeari?