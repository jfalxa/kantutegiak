---
id: tx-825
izenburua: Zure Aurrean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e5ddUwo2e3E
---

Gaur, zure aurrean
biluztu naiz osorik.
Ez dut, ezetzaren
ukoaren beldurrik.

Ni naiz naizena nahi dudana izan nahi dudalako
Nire nahia nahiz ni, naizen moduan maite naiz, zure aurrean berdin.
zure aurrean berdin.
Ezetzaren aurrean, neronek ezabatuko dut
ezaren hezea nire bi begietatik, 
ez dut zure beharrik.

Nire gorputzean
neronek soilik agintzen dut!

Argiak itzaltzen dituztenean
alboratu nahi zaituztenean
Pauso bat eman, beti aurrera!

Zure aurrean, dantzatuko dut suaren gainean.
Ez naiz ixilduko!
Zure aurrean, beti izango naiz ederrena.
Ez naiz kokilduko