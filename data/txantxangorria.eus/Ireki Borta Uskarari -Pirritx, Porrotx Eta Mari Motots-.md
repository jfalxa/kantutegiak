---
id: tx-3340
izenburua: Ireki Borta Uskarari -Pirritx, Porrotx Eta Mari Motots-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fhUr3Dny52k
---

Basotik hartzaren orrua
Ezkan behera xurrumurrua,
elur malutak pausatzean teilatuetan
oihartzunean urrutiko kantuetan,
zabalduz karrika-karrika
garraxi baten aldarrika
Erronkaribarreko uskararen borroka
dago ate joka IREKI BORTA!

Mugi! Altxa! Burgi! Dantza!
USKARAZ MINTZA!
Hezi! Kanta! Hazi! Topa!
IREKI BORTA!




Irekirik bortak urari
gure ametsen aintzirari,
putzuan bare dagoenaren gisara
mugitzen ez den hizkuntza hil eginen da.
Uskara berpizteko giltza
hitza da lehenengo baldintza,
sustraietatikan datorkigun bitxia
Busti mihia, eman bizia!

Mugi! Altxa! Burgi! Dantza!
USKARAZ MINTZA!
Hezi! Kanta! Hazi! Topa!
IREKI BORTA!