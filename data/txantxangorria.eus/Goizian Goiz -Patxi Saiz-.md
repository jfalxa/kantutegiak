---
id: tx-3247
izenburua: Goizian Goiz -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7miAVQfgtQU
---

Goizian goiz jaiki naiz eta
leiho parean eseri,
begira nago zure begiak
ea non diren ageri,
baina hormetan zehar ez diot
antzik ematen ezeri.
Zure begiak hurbil banitu
Haietan nenbilke igeri.

Leihorik gabeko begirada
ezin ikusi eguzkia
amarauna zintzilik
so daukat zelatari lez jarria
nire lurralde libre bakarra
ostatuko lisuria
kartzelako poema hauek dira
ene ondasun guztia

kartzelako poema hauek dira
ene ondasun guztia

Nire begiak ez dira ixuri
gogo malko bat bakarra
harria bezala izan nahi dut
gorputza gogoa iharra

Ura baino mugitzen ez duen
erreka ondoko legarra
ez dut nahi, ez sentitu,
ez pentsatu
ez entzun zure negarra

Ilunpetan bizia ez dugu argi
bizitzen jakingo
jada ez diren ziurtasunak
dira gaur ezin beteko

Haurtzaroko gerizondoak
helduko dira uda oro
ama betiko iraungo duen
erbesterik ere ez dago

ama betiko iraungo duen
erbesterik ere ez dago

Goizian goiz jaiki naiz eta
leiho parean eseri,
begira nago zure begiak
ea non diren ageri,
baina hormetan zehar ez diot
antzik ematen ezeri.
Zu/re be/gi/ak hur/bil ba/ni/tu
Haietan nenbilke igeri.

Zure begiak hurbil banitu
Ha/ie/tan nen/bil/ke i/ge/ri.

be/gi/ra na/go zu/re be/gi/ak
ea non di/ren a/ge/ri,

ni/re lu/rral/de li/bre ba/ka/rra
os/ta/tu/ko li/su/ri/a
kar/tze/la/ko poe/ma ha/uek di/ra
e/ne on/da/sun guz/ti/a

be/gi/ra na/go zu/re be/gi/ak
ea non di/ren a/ge/ri,

a/ma be/ti/ko i/raun/go du/en
er/bes/te/rik e/re ez da/go