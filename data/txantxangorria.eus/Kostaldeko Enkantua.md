---
id: tx-3074
izenburua: Kostaldeko Enkantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hkF6Gh-T1Q4
---

Ze! gaur antxiokan eh??
Ederra dakazue errekaxi!!

Denporali, denporali!!
Denporali zeurire!!

Koaxi!! koaxi??


Ondarrutik etorri gara,
ez gara jente makala,
horra hor gure kantua,
kostaldeko enkantua.

On/da/rru/tik e/to/rri ga/ra,
ez ga/ra jen/te ma/ka/la,
ho/rra hor gu/re kan/tu/a,
kos/tal/de/ko en/kan/tu/a.

Txi/txa/rru/a ta ber/de/la
Jo/xe/mi/ge/len ba/te/la,
Zor/no/tza/ko por/tu al/de/tik
hel/du da Pa/txi geu/re/a.

Ai, Ma/ri/txu no/ra ho/a?,
oi, e/der ga/lant ho/ri!
La Ha/ba/na al/de/ra, txi/ki/to
na/hi ba/duk e/to/rri.

On/da/rru/tik e/to/rri ga/ra,
ez ga/ra jen/te ma/ka/la,
ho/rra hor gu/re kan/tu/a,
kos/tal/de/ko en/kan/tu/a.

On/da/rru/tik e/to/rri ga/ra,
ez ga/ra jen/te ma/ka/la,
ho/rra hor gu/re kan/tu/a,
kos/tal/de/ko en/kan/tu/a.

Bo/ga/-Bo/ga ma/ri/ñe/la
joan gai/te/zen Ku/ba al/de/ra,
mai/te/a bi/si/ta/tze/ra,
mi/la gau/za i/kas/te/ra.

Bos/te/hun ur/te be/te/tze/a
ho/ri bai fe/txa tris/te/a,
zer/bait os/pa/tu be/ha/rre/an
ho/be bar/ka es/ka/tze/a!!
(lau/ro/ge/ta/ha/ma/bi/ra/ko, bro/ther!!)

Son, e/rro/na ta dan/tzo/ia,
be/ne/ta/ko ba/zi/lo/ia,
txan/da/ka ha/si/ko ga/ra-/e/ta,
ba/da/tor a/rin tron/bo/ia!

bes/teen i/dia dau/kan i/bi/li kon/tu/a
a/ka/ba/tze/ra go/az hu/rren/go kon/tu/an
ai-/oi-/e/ne! hu/rren/go pun/tu/an,
bes/te/tik ia ba/da i/bi/li kon/tu/an

On/da/rru/tik e/to/rri ga/ra,
ez ga/ra jen/te ma/ka/la,
ho/rra hor gu/re kan/tu/a,
kos/tal/de/ko en/kan/tu/a.

On/da/rru/tik e/to/rri ga/ra,
ez ga/ra jen/te ma/ka/la,
ho/rra hor gu/re kan/tu/a,
kos/tal/de/ko en/kan/tu/a.