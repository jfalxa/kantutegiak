---
id: tx-2776
izenburua: Kapito Harri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eKfQSPKvO_k
---

Oroitzapen handiak
dauzka izen horrek
mintzatzen balakite
horko haitz gogorrek
xuxen ikas ginio
Guk euskadun haurrek
zer bihotzak zituzten
euskaldun zaharrek.

Hatz lodi batenpean
herriko lehenak
bazagozin buru has
Guzien lagunak
zeruari galdatuz
onart zitzan lanak
lege batzu zituzten
finkatzen zutenak.

Zorrik nehori gabe
zauden langile hotz
batasunaren alde
ziren dena bihotz
euskaldunak euskaldun
nahaian betikotz
bazauzkaten gogoa
ta begiak zorrotz.

Ixil nindagolarik
hitz hekienpean
gogoeta hunek nau
jo nahigabean
Euskal Herriangaindi
oraiko mendean
nor ote da zaharrez
Oroit gutartean?

Aitek erranik badu
aspaldi aspaldi
hazi ona ez daike
beti alfer geldi
gureaz jabetzeko
noizko gure aldi
biltzar heien orena
laster jin baledi...