---
id: tx-968
izenburua: Ez Dok Hamahiru  Korrika 13
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6Ae0HPxaFR4
---

Korrika 13

Arr
Err
Irr, orr, urr
Arr, harri
Err, herri
Irr, irri
Harri herri, irri urri 


Ga
Ge
Gi, go, gu
Ga, gara
Ge, gero
Gu, gure
Gu gara gure gero


Geu gara harri
Geu urre gorri
Geu gure orri
Gure urri, gure irri


Harriz harri gora
Gure gerogura
Irriz gara
Gero, gu herri gara


Ez dok hamairu!
Ez da malefiziorik!
Herri bat
Herri bat osorik
Gero bat
Gero bat gaurdanik (bis)


Ar
Er
Ir, gor, gur
Ar, ari
Er, eri
Ir, hiri
Hara herri hori hiri! 


Gar
Ger 
Gir, gor, gur
Gar gorri
Gorgarri
Gure herri
Geroaren egarri 


Ez dok hamairu!
Ez da malefiziorik!
Herri bat
Herri bat osorik
Gero bat
Gero bat gaurdanik (bis) 


Ez dok hamairu!
Bada egitekorik!
Herri bat
Herri bat osorik
Gero bat
Gero bat gaurdanik