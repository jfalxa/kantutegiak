---
id: tx-1254
izenburua: Hemen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ej2QWRQ_lc4
---

Info gehiago: www.lavendetta.org

Aktoreak:
Inder txikitan: Karla Alaiza.
Inder 9 urterekin: Jaione Elorza.
Inder heldua: Julene Lekunberri.
Inderren aita: Javi Alaiza
Aurkezlea: Xabier Euzkitze.
Pilotariak: Joseba Ezkurdia, Abel Barriola, Juan Mtez de Irujo.
Eguraldiko gizona: Alex Sardui.
Amaierako neskatxa: Eider Murugarren.
Haurrak: Gazte Arbizuarrak.
Publikoa: Orotariko Arbizuarrak
Kolaborazioak: Juan Padilla, Manu Trijueque.

Bideoklipa: Taom (www.taom.eus)

Eskerrak: Beotibar Tolosa, Labrit Iruñea, Aspe Pelota, Juan Padilla Palacio Barria de Fruniz, Eitb.


Indarraren eredu zara
Denon harribitxia
Irrifarre ura piztean
Eta hortzak estutzean

Herri honek batu gaitu bidean
Gaztetako ametsekin borrokan
hitz hauek idatzi ditut zuretzat
zurekin batera dauden bihotz urratuentzat

Hemen gaituzu betiko
Hemen gaituzu betiko
Hemen gaituzu eztarritatik eskutara
Ta hemen gaituzu betiko

Jakin bide luze honetan
bakarrik ez zauzela
danak inderra eskuetan
kemena bixotzien

Herri honek batu gaitu bidean
Gaztetako ametsekin borrokan
hitz hauek idatzi ditut zuretzat
zurekin batera dauden bixotz urratuentzat

Hemen gaituzu betiko
Hemen gaituzu betiko
Hemen gaituzu eztarritatik eskutara
Ta hemen gaituzu betiko
Gogor ekin
Herria hirekin
Gogor ekin
Kaletan ikusi
Begitako dizdira hori
Txokora eta zabalera
Emaiok egurra
Gogor ekin
Gogor ekin
Hemen gaituzu betiko
Hemen gaituzu betiko
Hemen gaituzu eztarritatik eskutara
Ta hemen gaituzu betiko
Hemen gaituzu betiko
Hemen gaituzu betiko
Hemen gaituzu eztarritatik eskutara
Ta hemen gaituzu betiko