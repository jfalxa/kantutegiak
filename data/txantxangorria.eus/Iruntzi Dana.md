---
id: tx-679
izenburua: Iruntzi Dana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JY9Cb-5al64
---

Zabaldu ahoa
Iruntzi dana
Berba gaiztorik esan ez dagizun
Zarratuko dizut bizia
Zarratuko dizut bizia
Zein gozoa den pozoina
Erreinua oraleku duenarentzat
Zein gozoa den pozoina
Erreinuaaaaa
Zabaldu ahoa
Iruntzi dana
Berba okerrik idatzi ez dagizun
Luzatuko dizut biziaaa
Luzatuko dizut biziaaa
Zein gozoa den pozoina
Tronua oraleku duenarentzat
Zein gozoa den pozoina
Tronuaaa
Zabaldu ahoa
Aruntzi dana
Berbarik egin ez dagizun
Ukatuko dizut bizia
Ukatuko dizut biziaaa
Ooooooo
Haserretu zaitez
Egin negar
Irriz duzu heriotza
Haserretu zaitez
Egin negar
Irriz duzu heriotza
Zabaldu ahoa
Iruntzi dana
Zabaldu ahoa
Iruntzi dana
Zabaldu ahoa
Iruntzi dana!!!