---
id: tx-1622
izenburua: Barrezka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wDIf3FAuHp0
---

Hilik aurkitu dituzte txakurrak kalean zin egingo dugu 
hortikan ez genbiltzala, drogatuta geunden 100.000 kilometrotara BARREZKA

Hilik aurkitu dituzte FMIkoak zin egingo dugu hortikan ez genbiltzala, moskortuta geunden taberna uzten duenetan BARREZKA

Hilik aurkitu dituzte europeistak, zin egingo dugu hortikan ez genbiltzala, etxean geunden Monty Python,s Flying Circus ikusten BARREZKA

Hilik aurkitu dituzte espainiar epaileak, zin egingo dugu hortikan ez genbiltzala
bizitza ospatzen ari ginen sagardotegian BARREZKA

Zer mugatik zetorren gizarte modernoa? Harroputzegia zen gu gizajalea gorrotoa suge tartean urrea bada, Nor izango ote da odola azaltzeko trebe? BARREZKA! x2