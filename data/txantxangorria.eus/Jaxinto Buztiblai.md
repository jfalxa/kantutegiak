---
id: tx-3350
izenburua: Jaxinto Buztiblai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FwbDgPbvxYc
---

Lasterketa hasi da 
eta badoa tropela
bizkor, txirrindulariek
nahi dute txapela
bat berandu atera da
ta motel motela
eskuz agur eginez honela.

Jaxinto, Jaxinto, 
Jaxinto Bustiblai
Beti azkena eta beti alai!

Mendatea iritsi da, 
a ze izerdia
mingaina kanpora 
eta altxa ipurdia
batek bizikleta utzi, 
perretxiko bila
nor izango ote da mutila?

Jaxinto Bustiblai
Beti azkena eta beti alai!

Helmugan sprint bizia
hortzak estututa
laguna irrifarrez
sorpresa bat du ta
jendea busti du
ustekabean hartuta.

Jaxinto Bustiblai
Beti azkena eta beti alai!

Sari bakarra, 
Jaxintok izan du nekea
hala ere pozik dago, 
lortu du berea,
helburu politena
badauka betea,
bere lema ongi pasatzea

Jaxinto Bustiblai
Beti azkena eta beti alai!