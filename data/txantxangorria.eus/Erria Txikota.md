---
id: tx-1247
izenburua: Erria Txikota
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e3QFP0sltM0
---

Saria hausiya zekarkiñe
marian sartuko omen ditun
eokieran, bi mila baixkiñe,

beatzik alden palota abitu.
Etzion guk konpondu ezinik
Baño lanai gor ekiñ aurretik
Saria zabaldu ertza danetik
Eztiñau ber joskura okerrik

MUTILLOYAN AMARRATUTA
ZENBAT LEZAKE KONTA…
AITU, MUXU BOTA
ABITZEA GUAZTIK, ERRIA TXIKOTA!!

Arrai tokin aberi zikiña
Hogei brazan tarrata garbiya…

MUTILLOYAN AMARRATUTA
ZENBAT LEZAKE KONTA…
AITU, MUXU BOTA
ABITZEA GUAZTIK, ERRIA TXIKOTA!!