---
id: tx-609
izenburua: Errota Zaharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/daq9-p1q5wY
---

Errota zahar maitea
uraren ertzean,
uraren ertzean da
basati beltzean.
Negar egiten dezu
aleak txetzean.
Ni ere triste nabil
zutaz oroitzean.

Zotin segua dabil
errotarria itzalitako
izar distirak
dira neretzat
zure begiak.

Izar eder bat dago
hor goiko lekuan,
errota zaharra berriz
erreka zokuan.
Berebiziko pena
badaukat barruan,
ezin gintezke bizi
elkarren ondoan.

Bizitzaren legea
nahi det onartu
hala beharra
goraiez hartu
berez doana
zertan behartu.