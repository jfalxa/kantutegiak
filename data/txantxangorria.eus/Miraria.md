---
id: tx-266
izenburua: Miraria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OTpWeW1WnNs
---

Izerdi odoldua,
 arnasa larria
 ta gose bizia,
 ikusi dut,
 beartsuaren kopetan...
 
 Ixi!
 
 Ugarian igariz,
 irri-parrea
 ta urre dizdira,
 ikusi dut,
 ugazabaren jauregian...
 
 Ixi!
 
 Jaun eta jabe,
 ezpata zorrotzez,
 pake zaintzez,
 ikusi dut,
 Cartaginesa zaldian...
 
 Ixi!
 
 Akelar'go dantza
 uriko plazan?
 —lena gutxi bazan—
 ikusi dut,
 ...bedinkatua itzalkipean...
 
 Ixi!
 
 Begiak dira mintzo...
 
 Ixi ta ixi! Ago ixillik!