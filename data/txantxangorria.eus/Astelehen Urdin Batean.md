---
id: tx-1806
izenburua: Astelehen Urdin Batean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kCmN-X_uC4Y
---

Izar betaurrekodunak
izotzaren erreinuan
zerura oihu egiten dute
semeak bilatzen.

Izar betaurrekodunak
izotzaren erreinuan
lurrera ihes egiten dute
erribera baten bila.

Izar betaurrekodunak
begi trebez
itxasoan jartzen dira
astelehen urdin batean.

Izar betaurrekodunak
egiak alaitzen ari direnak.

Izar betaurrekodunak
gezurrak hilkutzan lurperatzen
dituztenak
eta gero ur garbia edanik etxera dijoaz
bibolina ahustera.

Izar betaurrekodunak
zeruan hegoak
lurrera kataiatuz.