---
id: tx-1263
izenburua: Nereari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sVDhodJIfsU
---

Hire ahizpei esaien hik, ttikia,
zergatik ez dagoen etxean ogirik,
esaien ogia erosten dela,
baina ez apirilik
edo hire aitaren ondrarik.

Zetazko boza,
erretenaren ondoko murmurio kantariak,
esan zaien
nola bizi diren
esperantzarik gabeko mila gizon doilor,
dirutan amiltzen nauten hoiek.

Hire ahizpei esaien hik, ttikia…