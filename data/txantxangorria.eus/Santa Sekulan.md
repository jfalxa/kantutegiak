---
id: tx-3
izenburua: Santa Sekulan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fzD8_SwQwsk
---

Santa Sekulan, ez det ikusi
beste horrenbeste kumeri
aita oian eseri eta
ama gañera erori
osasunakin urte askuan
segitzen baie lan horri
aurrera ere izango dute
anka gorria ugari

Diferentziak izaten dira amorraia ta eskailluk
hori ere ezetz igual esango du
zenbait txoro kaskalluk
gizasemien ankatarteko txortena eta koskalluk
hoiek dirala esaten dute
emakumien jostaillu

Jende danari jartzen baiozu, begiratua alaia
iñoiz edo behin etorriko da
guk zuen gustoko galaia
guk ezin aprobetxatu badeu
kertenik gabeko laia
ordun zuek ere alper alperril
txilbor azpiko zelaia

Kasu eder bat gertatu zitzaidan
gazte xamarra nintzala
dama polit bat inguratu zan

txix egiten ari nintzala
nezesidadez kunplitutzeko
hura tokia etzala
esan zidaten zeladorian
alaba zaharrena zala

Inguratuta esan zidaten
belarriaren ertzera
anai txikiyan konformidadez
oso makala etzera

Iñularrian, garai onakin
baldin etortzen bazera
Nik gonak altza, zuk prakak jetxi
biok konponduko gera