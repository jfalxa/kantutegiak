---
id: tx-2984
izenburua: Brodatzen Ari Nintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HBhhLqtcfoo
---

Brodatzen ari nintzen
ene salan jarririk
aire bat entzun nuen
itsasoko aldetik
Itsasoko aldetik
untzian kantaturik.

Brodatzea utzirik
gan nintzen amagana
hean jaliko nintzen
gibeleko leihora
gibeleko leihora
itsasoko aldera.

Bai habil, haurra, habil
erron kapitainari
jin dadin afaitera
hemen deskantsatzera
hemen deskantsatzera
salaren ikustera.

Jaun Kapitaina, amak
igortzen nau zugana
jin zaiten afaitera
hantxet deskantsatzera
hantxet deskantsatzera
salaren ikustera.

Andre gazte xarmanta
hori ezin ditekena
iphar haizea dugu
gan behar dut aintzina
ezin ilkia baitut
hauxe da ene pena.

Andre gazte xarmanta
zu sar zaite untzira
gurekin afaitera
eta deskantsatzera
hortxet deskantsatzera
salaren ikustera.

Andre gazte xarmanta
igaiten da untzira
han emaiten diote
lo belharra papora
eta untzi handian
lo dago gaixo haurra.

Jaun kapitaina, nora
deramazu zuk haurra?
Zaluxko itzulazu
hartu duzun lekura
hartu duzun lekura
aita amen gortera.

Nere mariñel ona
heda zak heda bela
bethi nahi nuena
jina zaitak aldera
ez duk hain usu
jiten zoriona eskura.

Jaun kapitaina, nora
ekarri nauzu huna?
Zalu itzul nezazu
hartu nauzun lekura
hartu nauzun lekura
aita amen gortera.

Andre gazte xarmanta
hori ezin egina
hiru ehun lekhutan
juanak gira aintzina
ene meneko zira
orain duzu orena.

Andre gazte xarmantak
hor hartzen du ezpata
bihotzetik sartzen ta
hila doa lurrera
aldiz haren arima
hegaldaka zerura!

Nere kapitain jauna
hauxe duzu malurra.
Nere mariñel ona
norat aurthiki haurra?
orat aurthiki haurra?
Hortxet itsas zolara.

Hiru ehun lekhutan
dago itsas leihorra.
Oi Ama anderea
so egizu leihora
zur’alaba gaixoa
uhinak derabila.