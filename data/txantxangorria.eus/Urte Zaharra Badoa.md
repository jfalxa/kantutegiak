---
id: tx-393
izenburua: Urte Zaharra Badoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pryHu3hM5Q4
---

Lyricist: Itxaso Azkona
Composer: Itxaso Azkona

Urte Berri on, Urte Berri on
Urte Berri, Urte Berri
Urte Berri, Berri on !!!
Urte Zaharra badoa
eta ospatu behar dugu
pena geratuko zaigu, bai
lotan gure bihotzean.
Urte zaharra joan zaitez
hori dagokizu orain
berriak dakartza opari
kanta eta alaitasuna.
Urte zaharra badoa
ta berarekin tristura
zoriontsuago izatea