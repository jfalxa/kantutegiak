---
id: tx-1932
izenburua: Wafa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yzxwlnl_NVU
---

(Wafa Idris in memoriam)

Oliba mikatza izanda
Nahi zenuke gozoan
Gozotasun horregatik
Maite zaitut osoan

Kufiya lepoan
Horra zein gazte doan
Wafa! Wafa! Wafa! Wafa! 
Sua bihotzondoan

Kufiya lepoan
Horra zein heldu doan
Wafa! Wafa! Wafa! Wafa! 
Sua bihotzondoan

Pikondo errea izanda
Nahi zenuke hostoan
Hostotasun horregatik 
Maite zaitut osoan

Kufiya lepoan 
Horra zein astun doan
Wafa! Wafa! Wafa! Wafa!
Sua bihotzondoan

Kufiya lepoan
Horra zein arin doan
Wafa! Wafa! Wafa! Wafa!
Sua bihotzondoan

Mahasti zapaldua izanda 
Nahi zenuke mulkoan 
Harrotasun horregatik 
Maite zaitut osoan

Kufiya lepoan
Horra zein apal doan
Wafa! Wafa! Wafa! Wafa! 
Sua bihotzondoan

Kufiya lepoan
Horra zein harro doan
Wafa! Wafa! Wafa! Wafa! 
Sua bihotzondoan.

Etxe apurtua izanda
Zutik zaude ukoan 
Osotasun horregatik
Maite zaitut osoan

Kufiya lepoan
Horra zein lerden doan
Palestina! Palestina! 
Sua bihotzondoan.

Kantuz naramala
Hautsia du hegala 
Palestina! Palestina! 
Erdiminez Ramallah.