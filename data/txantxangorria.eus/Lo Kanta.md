---
id: tx-252
izenburua: Lo Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Oyrj_1wd5BQ
---

IDOIAren "Ilun eta abar" diskoko bigarren aurrerapen abestiaren bideoa.

Vídeo de la segunda canción de adelanto del disco "Ilun eta abar" de IDOIA.

Musika eta hitzak/Música y letra: Idoia Asurmendi

Kamera/Cámara: 
Nora Goyalde
Jon Lasa

Muntaia/Montaje: Nora Goyalde

LO KANTA 

Izarrik al dago 
itsasoaren beste aldean? 
Eguna hiltzen bada, 
aurkitu gaitzala bazter ezkutu batean. 

Zeruak arnasten du isilean. 
Kresalak busti ditu zure begiak.
 
Itsasoari lo kanta bat, 
olatuen oihuek baretzeko gaua. 
Itsasoari lo kanta bat,
ilargiak zainduko gaitu, egizu lo lasai.