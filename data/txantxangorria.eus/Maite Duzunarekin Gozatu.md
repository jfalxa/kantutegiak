---
id: tx-1375
izenburua: Maite Duzunarekin Gozatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5RxM_C4BpaI
---

YOGURINHA BOROVAren bideoklip berria, BAI SI YES Diskoaren abestia "Maite duzunarekin gozatu". 
Compra aqui el disco: Erosi hemen diskoa:

SUBTITULOS hechos por: ANA MORALES y LUIS FERNANDEZ

Londres, Sudafrica, Japón, Mexico, Lanzarote, Toledo, Iruña, eta Bilbon grabatutako bideoa
Grabado en Londres, Sudafrica, Japón, Mexico, Lanzarote, Toledo, Iruña, Bilbao

Bideoegileak/Realizador: Alejandro Negueruela
Abestia/Canción: Iker Sadaba y Alberto Domingo

Laguntzaileak/Ayudantes: Alejandro Negueruela, Iñaki Queralt , Belen Toledo, Eduardo Gaviña , Lucia Martinez , Eduardo Alonso, Warren Byars , Cess † Emm, Charada.

Protagonistak/Ayudantes: Amaia Arkotxa, Karmele Larrinaga, Iñaki Maruri, Angelita la Perversa , Tomás Gallego-La Centollo Olé, Gerry Regitschnig , Ylenia Baglietto, Diego Pérez, Lander Otaola, Caprichossi Seoane Alba, Nagore Gore, Jeyxi Spaguetti , Oihana Gil Sanchez, Ainhize Gil Sanchez, Amaia Diaz de Durana, Raul hernandez Bou, Frangorka, Manapan, Pili Cepeda , Dj 2ttor, Tigrida Revuelta, Anakoz Merikaetxebarria Magallon, Aritz García López , Ana Baranda, Santiago Garcia, Oscar Beato, Ionkar Angulo, Julián Fernández, Munduko Emakumeak / Babel, Ana Morales, Alvaro Përez, Luis Quintero, Julio Carrasco, Chichi L´Amoroso, Iker Sadaba, Elisa García eta Naiara Urrutia , Manel Dalgo, Lady gessmar,Alejandro Q. Asencio, Elsa Carazo, Patricia Rodriguez, Victor Santamaría, Masai, Nee y Toñi la Canaria, Alvaro Përez, Luis Quintero, Julio Carrasco, Chichi L´Amoroso, Alberto Perez, Daniel Gonzalez, Pilar Jiménez, Manuela Grasa, Asier A. Bilbao, Perdido Roy, Eduardo Alonso, Warren Byars , Ismael González, Javier Ferreiro, Juan Carlos ( Patricia Mattel ), Peregrin, Ricardo Vicente " Piruli ", eduardo gaviña eta yogurinha borova.

Letra de la cancion:
2) MAITE DUZUNAREKIN GOZATU
Besteen iritzien
Gatibu zinen
Zure bidea azkenik
Hartu zenuen Ateak zabaldu
Egin zenituen
Ortzadarrarekin
Amets eginen
Maite duzunarekin gozatu
Askatasun hegalak zabalduz
Zure nortasuna ez ezkutatu
Barruko gogoa aldarrikatu
Gaua iritsi da
Pozak zoratzen
Zaude zu nirekin
Maita gaitezen
Gorputzak askatuz
Elkar gozatzen
Sentsazio berriak
Biok sentitzen

TRADUCCIÓN:
Estabas pres@ del qué dirán
Pero tomaste tu camino
Abriste las puertas
A un sueño de arcoiris
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO
Llega la noche
te invade la alegría
Quédate conmigo
Amémonos
Cuerpos libres
Disfrutando junt@s
Nuevas sensaciones
nos embargan
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO
Disfruta...
Vuela...
Ama...
¡¡¡REIVINDICA!!!
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO
DISFRUTA CON QUIEN AMAS
VUELA EN LIBERTAD
NO OCULTES TU IDENTIDAD
REIVINDICA TU DESEO

ENLACES: