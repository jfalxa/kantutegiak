---
id: tx-3165
izenburua: Nostalgia -Kauta-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WuMkYFXtxEI
---

Erlojuaren norantza geldi ezin hontan 
gogora ekarri ditut nire ondora 
ustekabez elkar eman zigun 
aterpe hura 
bezalako momentuak, nostalgia 

Iraganeko ume txikia 
itzaliko ez den inozentzia 
behin esan zenidan, arrazoi zenuen 
aro bakoitza ahaztezina dela 

Izango naiz 
une berrien zain 
esperoa 
izango naiz 
aro berria da 
izango naiz 
une berrien zain 
esperoa 
izango naiz 

Bera bezalako erokeria 
uda hartako lehen maitasuna 
amaitu berri den idatzi honetatik 
agertuko al da, nostalgia 

Izango naiz 
une berrien zain 
esperoa 
izango naiz 
aro berria da 
izango naiz 
une berrien zain 
esperoa 
izango naiz 

Ahhhhh........