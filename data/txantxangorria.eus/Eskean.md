---
id: tx-908
izenburua: Eskean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4GIPOTnaQaM
---

Abenduko gau batean
izarrik ez zela
hurbildu nintzen eskean
ohi bezela.
Ez dekozu maitasunik
barruan gordeta
berandu zatoz jadanik
hustu naz eta.
Zer falta jatzu laztana?
Hain zara ederra
guzurrarentzat labana
hotzaren erra.
Xedetzat ez dot gorputza
ez bada arima
zure aitzaki harroputza
zure biktima.
Maite zaitut esan eta
betor bizi-poza
bete gabeko apeta
ezin dot goza.
Galdu dozuna eskura
itzuliko balitz,
hiztegi oso bat zen hura
ez ziren bi hitz.Itxaropenik ez dozu
maitasunarengan
merke datozen bi moxu
badoaz hegan.
Zein izan dozu ebazle
asmo okerrekoa
bat beti dena garaile
ez menpekoa.
handia
itxi ukabila
galdu dezala bizia
doala bila.