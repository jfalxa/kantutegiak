---
id: tx-755
izenburua: Txanton Pipirri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wxBEEe3yJkc
---

Txanton Pipirri hemen dago
limosna on bat balego.
Kantau bihar dot
baldin ahal badot
nik gaur daukadan gosia
errukitzeko jentia.
2)
Zabalak dodaz gibela
estiak eta sabela.
Hartuko leuke
baldin baleuke
ganbela bete okela.
Baina hau noiz izango ete da?
3)
Ez da errotarri hain gogorrik
nire haginak lakorik
bada bakotxak
zorrotz nahi motzak
ogi bat hurunduten dau
hiru bigunak edo lau.
4)
Batuten dodaz kalian
arraina ugari danian
hogei txitxarro
edo gehiago
baina guztiak gabian
sartzen ditut sabelian.
5)
Orduan pozarren nago
gaberdi ingururaino.
Handik goizera
nire sabela
beti dago gur gur gur gur
bartko aparia agur.
6)
Egunsentian apala
daukat tripako azala.
Balego orduan
nire albuan
lagun onen bat nok opa
perol handi bete zopa!
9)
Ni jaio nintzan orduan
aztua etxian eguan.
Zuzen begira
nire arpegira
eta esan eutsan amari:
"Entzun egizuz berba bi".
10)
"Inun aurkitu ezin da
ezpada Bilbon Giralda
andra bakarra
zeinen bularra
asko dan esne bagarik
ume honentzat bakarrik".
11)
Igarlia zan aztua
baina amak eukan modua
Emon titia
eukan guztia
ta gero esnia ugari
auzoko behien erroti.
12)Batuten dodaz mendian
egurrak egunsentian
eruateko
amak jateko
egun guztian ogia
niri emonik erdia.
13)Ez da neuretzat guztia
batuten dodan ogia.
Zaku bete otzur
batuten dot hur
eskian iluntzerako
ama eta neuretzako.