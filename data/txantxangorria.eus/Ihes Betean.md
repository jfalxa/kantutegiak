---
id: tx-2110
izenburua: Ihes Betean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7Uq4em3ohpw
---

Ihes betean haizeak daraman hostoa,
ihes betan baina noraezean.

Ihes betean mila nortasun eta aurpegi,
ihes betean baina izenik gabe.

Sarerik gabe
bakarrik, negu luzean
libre izan nahian.

Ihes betean bogan doan itsauntzia,
ihes betean baina porturik gabe.

Ihes betean kulunka ari den zuhaixka
ihes betean baina sustrairik gabe.

Sarerik gabe
bakarrik, negu luzean
libre izan nahian
eta ezin.

Argazki zahar bat eskuan,
nire izena entzun dut isilean
amaren begietan.

Masailetik argazkira
malko bat erori zait urrutira,
amaren aurpegira.

Bart amets egin dut, ama,
ez naizela, ez naizela sekula
itzuliren etxera.