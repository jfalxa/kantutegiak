---
id: tx-1756
izenburua: Nelson Mandela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A6amux3IHwI
---

Lehengo astean zendu
zen Nelson Mandela
eta mundu osoan
piztu da kandela
gustorago negoke
hemen dena dela
betiko itxi balute
egon zen kartzela