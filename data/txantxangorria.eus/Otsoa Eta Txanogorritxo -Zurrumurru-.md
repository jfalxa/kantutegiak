---
id: tx-3239
izenburua: Otsoa Eta Txanogorritxo -Zurrumurru-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_q3mlnOhxdg
---

Auuuuh!
Auuuuh!
uuuuh! uuuuh!
pon pon pon po
txu txu txu txu txu txu
txu txu txu
pon pon pon po
txu txu txu txu txu txu
txu txu txu
Auuuuh!
Otsoak, uuu/uh!
Mendi gainetan ugari
Xuri-beltzak, urdin, gorri
Trumilka ziren etorri.
Horregatik, Txanogorritxu
Urrundu otsoetatik!
Horregatik, Txanogorritxu
Urrundu otsoetatik!
txu txu txu txu/txu/ru/txu/txu
txu txu txu txu/txu/ru/txu/txu
Auuuuh!
Otsoak, uuuuh!
Goseak ditu ekarri.
Errez baita aurkitzea
Inguru hontan jana/ri.
Horregatik, Txanogorritxu
Mintzatzen da otsoekin.
Horregatik, Txanogorritxu
Mintzatzen da otsoekin.
txu txu txu txu/txu/ru/txu/txu
txu txu txu txu/txu/ru/txu/txu
Au/uu/uh!
O/tso/ak, uuu/uh!
Jan di/tu ha/ma/sei txe/rri.
E/hun o/ilo, ho/gei ar/di:
Go/se/a ken/tzen ba/da/ki!
Ho/rre/ga/tik, Txa/no/go/rri/txu
Bil/dur/tu da o/tso/e/kin!
Ho/rre/ga/tik, Txa/no/go/rri/txu
Bil/dur/tu da o/tso/e/kin!
O/tso/ak, uuu/uh!
I/rri/far e/gi/ten da/ki.
hor/tzak lu/ze, hain e/der/ki:
I/zan de/za/gun e/rru/ki.
Ho/rre/ga/tik, Txa/no/go/rri/txu
Po/zik da/bil o/tso/e/kin.
Ho/rre/ga/tik, Txa/no/go/rri/txu
Po/zik da/bil o/tso/e/kin.
txu txu txu txu/txu/ru/txu/txu
pon pon pon po
txu txu txu txu/txu/ru/txu/txu
Au/uu/uh!
O/tso/ak, uuu/uh!
Be/rek da/ki/ten bi/de/tik,
Lais/ter be/rriz, us/te ga/be
Ar/tal/de/a/ren er/di/tik…
Zo/ri/tza/rrez, Txa/no/go/rri/txu
Be/ti da/bil o/tso/e/kin
Zo/ri/tza/rrez, Txa/no/go/rri/txu
Be/ti da/bil o/tso/e/kin
Beti dabil otsoekin
Beti dabil otsoekin
Beti dabil otsoekin
Beti dabil otsoekin
Beti dabil otsoekin
Beti dabil otsoekin
Be/ti da/bil o/tso/e/kin
Be/ti da/bil o/tso/e/kin
Be/ti da/bil o/tso/e/kin