---
id: tx-1181
izenburua: Gure Nahia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LUV_KpMBoF8
---

Bete ditut birikak
Aire garbi berriarekin
Gogo eta irrikak
Asetzeko elkarrekin
Bihotzetik ezpainetara
Hitzak doaz mara-mara
Gure hizkuntzaren bidean
elkarrekin airean
Libre hegan
Txorien antzera
Besoak astinduz
Gora eta behera
Goazen,
Aurten
Ametsak egi bilakatzen
Euskararen erritmora
Denok eskuak gora (x2)
Euskaraz bizi, gure nahia
Saiatu ezkero ez da zaila
Elkarren ondoan etzan
Maite zaitudala esan
Libre hegan
Txorien antzera
Besoak astinduz
Gora eta behera
Hainbat arraza kultura eta kolore
Denok gara lur bereko sustrai eta lore
Hainbat arraza kultura eta kolore
Denok gara lur bereko sustrai eta lore
Goazen,
Aurten
Ametsak egi bilakatzen
Euskararen erritmora
Denok eskuak gora (x3)