---
id: tx-1718
izenburua: Nafarra Oi Nafarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pCUu-oebvU8
---

Nafarra, oi Nafarra, Euzkadi lehena,
 Ederra zen ametsa zure Erregena.
 
 1
 
 Arrosa bat urhezko zazpi hostozkoa,
 Errege ahur baten neurri betekoa,
 Edozoin baratzetan ezin sortuzkoa,
 Zurean zen loratu Batasun gogoa.
 
 2
 
 Entzuten da kantatzen zure jauregitan
 Uztai barri xaharren erreberietan,
 Zantxo-ren hil hobitik ilun nabarretan
 Bethi jalgitzen diren hasperen trixtetan.
 
 3
 
 Azkarrena zu zinen zazpi anaietan.
 Zu kadeetan preso nork zaitu ba eman?
 Xuti zaite berriz Errege jauntzitan,
 Batasun berriaren Arrosa eskutan.