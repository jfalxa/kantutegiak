---
id: tx-1919
izenburua: Bil, Bil, Bil
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QkbakoFBN_U
---

Bil, Bil, Bil
Bil bil bil, haurrak, bil eta,
Bil bil bil, ez ixil eta
Euskaraz kanta ( bis)

Euskara gabe,
Euskal Herria
Nigargarri.

Euskara gabe,
Ez bailitaike
Euskal Herria
Euskal Herri.

Bil bil bil, haurrak, bil eta,
Bil bil bil, ez ixil eta
Euskaraz kanta ( bis)

Kantua gabe
Euskal Herria nigargarri.

Euskara gabe,
Ez bailitaike
Euskal Herria
Euskal Herri.

Bil bil bil, haurrak, bil eta,
Bil bil bil, ez ixil eta
Euskaraz kanta ( bis)

Bihotza gabe
Euskal Herria
nigargarri.

Bihotza gabe,
Ez bailitaike
Euskal Herria
Euskal Herri.

Bil bil bil, haurrak, bil eta,
Bil bil bil, ez ixil eta
Euskaraz kanta ( bis)