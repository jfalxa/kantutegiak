---
id: tx-2979
izenburua: Batasunaren Indarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H3Ac3GFgm2k
---

BATASUNAREN INDARRA

Euskal Herri langilearen
eskubideen defentsan konpromisua, Sua!
Ondorioa, errepresioa,
sasia, heriotza, espetxea.

Duintasun pertsonal kolektiboa
Askatasun bideak, ate irekiak!

Herriaren sukoi, (a)bangoardia
Ziega zulotik iparra.
Amnistia, burujabetza,
Bakerantz, konponbiderantz, askatasuna!

2015, Oliba Gorriak, 40 minutu rock
Ez dugu izaterik aske haiek gabe,
Batasunaren indarraz, 
barrutik eta kanpotik
norabide berean, garaipeneratz!
Dabilen herriari erdoilik ez!

Elkartu besoak, ukabilak gora
Urrats sendoan garaipenerantz!