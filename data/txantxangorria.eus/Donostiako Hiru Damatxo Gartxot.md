---
id: tx-3191
izenburua: Donostiako Hiru Damatxo Gartxot
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/U5SnqKl6WAg
---

Donostiako hiru damatxo
Errenderian dendari.
Donostiako hiru damatxo
Errenderian dendari.
Josten ere badakite, baina
ardoa edaten hobeki.
Eta kriskitin, kraskitin,
arrosa krabelin,
ardoa edaten hobeki.

Donostiako hiru damatxo
Errenteriko kalean,
Donostiako hiru damatxo
Errenteriko kalean,
egunez oso triste ibili,
baina dantzatu gauean
Eta kriskitin, kraskitin,
arrosa krabelin,
baina dantzatu gauean.

Donostiarrak ekarri dute
Getariatik akerra,
Donostiarrak ekarri dute
Getariatik akerra,
kanpaindorrian ipini dute
Aita Santutzat dutela
Eta kriskitin, kraskitin,
arrosa krabelin,
Aita Santutzat dutela.

Donostiako neskatxatxoak
mandatuen aitzakian
Donostiako neskatxatxoak
mandatuen aitzakian
lagunarekin egoten dira
kalian jolaskarian,
eta kriskitin, kraskitin,
arrosa krabelin,
pozez algara haundian.

Do/nos/ti/a/ko a/rran/tza/li/ak
di/ra xit gi/zon  ba/pu/ak;
Do/nos/ti/a/ko a/rran/tza/li/ak
di/ra xit gi/zon  ba/pu/ak;
Gaz/te/lu/pe/ko sa/gar/du/a/kin
e/gi/ten on/gi tra/gu/ak;
E/ta kris/ki/tin, kras/ki/tin,
a/rro/sa kra/be/lin,
maiz bus/ti/ri/kan a/bu/ak.