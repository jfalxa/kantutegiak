---
id: tx-2292
izenburua: Deportivo Alaves
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ocWOMUuJ-A0
---

Autorea: ALFREDO DONNAY




Gora beti Alaves,
agertzen zarena indarrez, 
lorian izandako
talde eder zuri urdin hura lez;     
Gasteizek txapeldun
ikusten zaitu zu, bihotzez. 
Ez zaizu faltako
Festa-girorik, inola bez. 

Aupa Alaves, kirol talde bikaina, 
Araba osoa duzu esperantzan; 
Oionetik Aramaiora zure
garaipenak ipintzen gaitu martxan. 
Harmonian egingo dugu jaia, 
Zeledonekin babazorrook dantzan. 
Txalo artean agurtuko zaitugu
Andra Mari Zuriaren plazan. 

Gora beti Alaves,
agertzen zarena indarrez...

Aupa Alaves, futbolean aparta, 
aupa neska-mutil jatorrak, Aurrera! 
Gasteiztarrok Mendizorrotza aldera
etortzen gara zu animatzera.
Alaves, gol egiten duzunean
abesten dugu guztiok batera,
irrintzi hau lau haizetara zabalduz
arabar guztien bihotzera.                  (bi)