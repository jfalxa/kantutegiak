---
id: tx-1032
izenburua: Kolorea Kendu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ju8Q7Oetso0
---

Kolorea kendu abestia, Ze Esatek! taldearen "Ibuprofeno" diskaren bigarren singlearen bideoklip ofiziala.

To purchase "Kolorea Kendu" by Ze Esatek! please visit:


Taberna sarreran
tabako makinaren ondoan
lagun batekin hizketan
zu han zinen
hiru kontatu eta
gin tonic bat lagunduta
zugana gerturatu nintzan
Aupa zer moduz?
aizu, nola deitzen zara
ze polita kamiseta
forever single
esaldi antzuak
urduri esan nizkizunak
pentsatzen nuena ordea
Zure ile beltzan
begi berdeetan
irribar goxoan
galdu nahi nuke
denbora gelditu
bildurrak baztertu
eta espain gorri horiei
Kolorea kendu!
Zer
abesti hau
ez nau asko motibatzen
ezetz esaten gustatzen?
baina musika ona da
Txorakeriak
urduri esan nizkizunak
pentsatzen nuena ordea
Zure ile beltzan
begi berdeetan
irribar goxoan
galdu nahi nuke
denbora gelditu
bildurrak baztertu
eta espain gorri horiei
Kolorea kendu
Zure ile beltzan
begi berdeetan
irribar goxoan
galdu nahi nuke
denbora gelditu
bildurrak baztertu
eta espain gorri horiei
Kolorea kendu
Kolorea kendu
Kolorea kendu
Kolorea kendu