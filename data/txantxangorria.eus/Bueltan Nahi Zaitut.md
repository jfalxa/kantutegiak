---
id: tx-950
izenburua: Bueltan Nahi Zaitut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T51fV_NtBXI
---

Gaur urrun zara,
 ta aspaldi ez dut zure berri,  
azken udako argazkiei begira, 
ezin ukatu poza piztu zaidanik

 

Zubia:
Oroitzen zaitut, lanbroetan, 
zuretzat hain arrotza zen itsasoari begira,
baina ba ote da itsasorikan, inoiz ezagutu ez duzun herriaren ortzimugan

Oroitzen zaitut, ilunabarretan, 
dunek ez daukate inolaz zuen mendien itxura
baina han goitikan, irriz gainezka, 
egiten genituen etorkizunerako planak



Bueltaaaan, nahi zaitut, 
edonora bada ere izan zaitezela bueltan, 
munduaren, muga ta ertzetan, 
ez zaitzala inork gelditu, 
edonorako itzuleran

Bueltaaaan, nahi zaitut, 
edonora bada ere izan zaitezela bueltan, 
munduaren, muga ta ertzetan, 
ez zaitzala inork gelditu, edonorako itzuleran