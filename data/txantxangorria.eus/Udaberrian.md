---
id: tx-1117
izenburua: Udaberrian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3qbu5z7VVsU
---

Udaberrian abiatu nintzen 
dama gazte bat bilatzen 
atia kox-kox jo nion baina 
ez ziraden erantzuten. 

Errespuesta eman eta gero 
maitia zer nahi zenduke 
zure gustua horrela bada 
biok lo egingo genduke. 

Zure aita tabernan dago 
ama berriz sukaldian 
hemen ataian egongo gera 
hoiek lotara artian. 

Atai aldian hotza egiten du 
hozkildu egingo gera 
nere mantia zabaltzen badet 
biok tapatuko gera. 

Zuk ere tira nik ere tira 
apurtu egingo genduke 
nere herrian mantak badaude 
berriak diruan truke. 

Atia ere tirrikiti-tarra 
zakurra berriz zaunkaka 
atiarena nik egingo det 
zakurrak zeinek ixildu. 

Zuk txakur hori ixildutzeko 
eper bizia behar du 
eper bizia harrapatzeko 
mutil mantxua zera zu.