---
id: tx-2394
izenburua: Piztu Bilbo Itzali Mtv
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/laZDf4o4t9I
---

Motorrak berotzen joateko hona hemen artista herrikoi talde izugarri batek "Piztu Bilbo"-rentzako egindako abestia; era kolektiboan, berdintsuan eta interes komertzialetatik at sortutakoa! MTVak inposatzen duen kultura eredu kapitalistatik kanpo musika asko, eta oso ona, egiteko aukerak badaudela erakusten digu lan honek. Piztu Bilboren eskerrik beroenak parte-hartzaile guztiei: Suaia, Ziztada, Aiala, Zoroen Kluba, AK, Salda Dago, Gatom, Komando Braga, Raperu, Akerjenbe. Ekoizpena: Thunder Clap, Euridia, Etchecoy.

PIZTU BILBO, ITZALI MTV
Rrrrrrra! Txo, txo,  txo, txo,…!
Dana da drama, Bilbon, dana da drama
Kapi-Kapitala dabil bere kaxa
Gazteon zama, por la lana, onartuz zuen lana
Iraultzaren parte, ez e(d)uki zalantza
Abortatu plana, Aburto, ez dala justo!
Azken irrifarra, iraultzailea ta punto


MTV amatatu! Egin apustu!
Bilbo piztu! Banketxeak hustu!


Laurogeitamarreko hamarkada
Soilik musikari zuzenduta zegoena
Denborak aurrera egin ahala
Ignorantzia piztu duena
Sexu, droga, jaia, estereotipo denak finkatuz
Musika kendu ta iraintzen gaituena


Money, money, make money, money
Money, money PNV gure Cartel de Cali
Money, money, hiria titanioz estali
Money, money, ta gu pikutara bidali


Dirudunen aldeko teoria
asetu ezin musikaren egarria
Herriak egia eskatzen du, agian
Berria nagiak entzuten begiak


Multia bazara, Patxa!
bitartean gazteria gure kaxa
mila tonto bete ahoa zaie gitaz
ez irauli working class-ean


Kulturi da herridxena
ez enpresa handidxena
kapitalistik gudau berantzat
MTVa adibide onena, EAJ fuera!

Pentsamendu ustelak botakuz herbestera
Eskemak hausteko gatoz 
ta, mesedez, benetan hausi daitzezela


Txo,txo,… auzoz auzo piztu Bilbo!
Txo,txo,… erakuslehio hiriari planto
Rrrrraaaa!
Gureaz harro bertsoz bertso
Txo, txo, txo, txo…
Tutuetatik kanpo
Uo o o,  a a a
Uo o o,  a a a
Ez dugu behar,
ez dugu behar, 
MTVa ez dugu behar
Musika herritik ta herriarentzat
MTVa hemendik at!


Begira kalean daude
bere gurdi berrian
berriro behin eta ere 
berrikuntza diru guztia
Begira kalean daude
bere gurdi berrian
guda nahi badute 
horrela  izan daitela


Egiten duzun estiloak
Ez dit axola
Hip-hop, reggae, punk, edo rock and rolla
Itzali telebista
Atera mundura, 
kalea, izan zen gure eskola
reggae musikan sustraiak eta kulturala
ta hori da gu bultzatzen ari garena
bai, gu gara, euskal gazteriaren ahotsa, ahotsa


Lurraldeko mugimendu sozialak zapaltzen 
Musika komertzialak, sexistak ordaintzen
Artistak borrokan errespetua galtzen
Itzali MTV ke ez dugu sartzen
Dena turismoarentzat 
Ezer ez herriarentzat
Nola ikusten da bizitza
MTV antenatik at?


Geure kultura, geure artea, geureak dira,
Nahiz ta aukerak eman eta eman kanpotik datorrenea
Masifikazio, aprobetxategi
Eman iezaiozu nahi duzun izena honi
Ieieeee, nahi duzun izena honi, ieieeei


Txo,txo,… auzoz auzo piztu Bilbo!
Txo,txo,… erakus-lehio hiriari planto
Rrrrraaaa!
Gureaz harro bertsoz bertso
Txo, txo, txx, txo…
Tutuetatik kanpo
Uo o o,  a a a
Uo o o,  a a a