---
id: tx-2473
izenburua: Amildegietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TfynvpgiyoA
---

Erraza zen orbainen itzal 
handietan babestea.
Erraza zen helbide denak
erretzea, gordetzea. 
Erakusleiho atzean 
ezkutatzen ohitu gara.
Zeinen erraza ikusezin 
izanda kexak botatzea.

Ozenak dira nire hitzak amildegian daudenean.

Horren isila izan naiz:
abeslari lez.
Horrenbeste eserita:
eskalatzaile.
Azalpenak ematen,
arau zentzugabeak
zalantzan jarri gabe
atrezzoa osatzen.
Ezarritakoaren 
gainean igeri.
Aurreiritzien aurretik
laster batean.
Gelditu nahi dut eta
urrun begiratu.
Hegan egitera noa
ibiltzen ahaztu arte.
Txateko elkarrizketaren amaiera
Mezu bat idatzi...