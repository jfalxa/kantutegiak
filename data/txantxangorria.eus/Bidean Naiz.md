---
id: tx-2389
izenburua: Bidean Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PQbRis-bg0Y
---

Ibai Marin - Bidean naiz
2018 - Bideoklip ofiziala. Live video. Zuzeneko soinua. Zuzendaritza, soinua eta irudien grabaketa (Tutti community).


BIDEAN NAIZ

Beste etapa bat tokau zait
gogoz hartu beharrekoa
ez nago eroso baina
aurkituko dut nire lekua

Urte asko izan dira
zuekin pasa direnak
baina ez, ez naiz damutzen
nire bideari ekiteaz.

Bide luzea egin izan arren
gehiago nahi dut.
ez bait naiz aspertzen hontaz.

Eta bidean naiz zuentzako
river rock on the road
nire kantuak biluzteko
eta arima erakusteko.

Bide luzea egin izan arren
gehiago nahi dut.
ez bait naiz aspertzen hontaz.