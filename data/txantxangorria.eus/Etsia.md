---
id: tx-3023
izenburua: Etsia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9hiSXorJgnQ
---

Wishlist


00:00 / 02:58



Includes unlimited streaming via the free Bandcamp app, plus high-quality download in MP3, FLAC and more.
 €0.99 EUR


    Buy the Full Digital Album

about
‘Etsia’, bizi dugun aroan txertaturiko kantua da inondik ere, hirukoak Venicen (Kalifornia, AEB) ROSS ROBINSON ekoizlearekin batera grabatua (The Cure, At The Drive-In…). Berri Txarrak-en 20. urteurrena betetzen den honetan, sortze prozesu berezi batean murgilduta dago taldea. Kantu hau 2014 amaieran kaleratuko den lan berriaren parte izango da.

-----------------------


'Etsia’, produced by ROSS ROBINSON (The Cure, At The Drive-In…) was recorded by the Basque power trio in Venice (California) last April. This is Berri Txarrak’s 20th anniversary and the band is involved in a very special creative process, which will come to an end in late 2014 with the release of their much anticipated new album.


------------------------


‘Etsia’, es un tema ligado a su tiempo, grabado por el power trío en Venice (California) bajo la producción de ROSS ROBINSON (The Cure, At The Drive-In…). Esta canción formará parte del nuevo trabajo de Berri Txarrak, quienes se encuentran inmersos en un novedoso proceso creativo dentro de su 20º aniversario y que verá la luz a finales de este 2014.
lyrics
zer esan -nola esan- aro ilun honetaz
kartoiak, nekeak jada esan ez duenik
tunela nondik hasi
goilare kamuts hazi diren kantu hauekin

esana baitago, aldez edo moldez
otsoak dabiltzala artzai
putzutik salbatzeko bota zitzaiela soka
lepora lotu beharrean

nola azalera irten

esana baitago
altuegi doaz putreak
euren uso hegalak astinduaz
jauregiaren hormetan egiten duela talka
dena galdu duenaren oihuak

eta azalera irten
nola azalera irten

zer esan -nola esan- aro eder honetaz
sormenak, kemenak ezin esan dezakeenik
nola zauri etsaia
nola utzi etsia kantu hauekin

‘Etsia’, bizi dugun aroan txertaturiko kantua da inondik ere, hirukoak Venicen (Kalifornia, AEB) ROSS ROBINSON ekoizlearekin batera grabatua (The Cure, At The Drive-In…)