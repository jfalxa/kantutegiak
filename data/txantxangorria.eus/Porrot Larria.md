---
id: tx-190
izenburua: Porrot Larria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CXprkyXpDps
---

Isildu Zaitez taldearen laugarren diskako "Porrot Larria" kantua. 
www.isilduzaitez.com

Porrot larria hegazkin txiki
eta zaharraren motor batean.
Hurbil dagoen aireportua
distantziaz ordubetera.

Bizirauteko aukera
bakarra pisua galtzea da.
Jada funtsezko ez diren
objektu guztiak bota dira.

Zorigaiztoko gertaera hontan,
irtenbide latza bidaiarentzat.
Hemetxe dilema morala,
azkenengo bide-orria.

Bizirauteko aukera
bakarra pisua galtzea da.
Bakoitzak berea, nor da
lehena, erokeria abegi hontan