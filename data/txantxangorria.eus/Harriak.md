---
id: tx-343
izenburua: Harriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6RPV6k3hSNw
---

Nik besoa eskaini
zuk eskua hartu.
Hala ere, beldurrik ez didazula diozu.
Zeru beltzetik elur zuria ari du,
bakoitzak murrua bere erara egiten du.
Harri pila bat dut zain ohean
utzidazu gaur gordetzen zurean.
Badira gau ilunean baino
ikusten ez diren hainbat gauza.
Beldurrak eta arrainak
hil arte hazi eta hazi eta hazi.
Behin zubi oso bat jan nuen
lehenengo harritik azkenera
hainbeste nahi nuenera
ez joateko.

arrain bat oparituko dizut nola hazten den ulertzeko zenbat ur behar duen arnasteko, zenbat ur bitzitzeko, harri pila bat dugu metatua begien atzeko aldean: zubiak, murruak, bideak, etxeak, harriak finean,  gorputzak, galderak, begiak, hitzak, giltzak eta oheak, Diogenesen sindrome emozional batek jota bezala, amets zaharrak ereiten ditugu gorputz berri eta emankorretan, nor garen ahaztu arte opaltzen besteen hankarteetan, besarka nazazu gogor iztarrekin hegan ez ateratzeko, zure barruraino sartu nahi dut benetan nor zaren ikusteko, beldur bat oparituko dizut nola hazten den ulertzeko, zenbat zu behar duen arnasteko, zenbat ni bizitzeko