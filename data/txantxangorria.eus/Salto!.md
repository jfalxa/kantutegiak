---
id: tx-1010
izenburua: Salto!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ov41dPr5pOw
---

Atzamar artien harie doa lez
Bizitzak ez deu atzera egiten
Eta konturatzen garenerako
Egun bet baino ez da geldituko
Lan eta lan, denbora saltzen
Zoriontasune albo baten izten
Alperriko geuzek pilatzen
Jakin, diruek ez dau bizitza ordaintzen

Nahi dogune egin geinke
Galtzen badogu dekogun bildurre
Arriskatu zaitez, salto!
Bizi berri bet itxaroten dago

Nork ez dau maite askatasune
Norberak nahi dabena egitie
Alperrik izengo da gero damutzie
Errautsak aidien doazenien
Geuzek berez ez dire aldatzen
Ez badogu geuk egiten
Uuh hutsera saltoten ez badogu
Leku bardinien jarraituko dogu

Nahi dogune egin geinke
Galtzen badogu dekogun bildurre
Arriskatu zaitez, salto!
Bizi berri bet itxaroten dago

Atxike, mitxike egunero
Hamalau ordu lanerako
Lanetik urten ta denborarik ez
Lagunek astien ikusi bez
Juan nintzen maitasunez
Eskatu neban jai egun bet
Orduek sartzeko esan eustien
Ta nik ezetz!
Pareu nintzen lanien
Pentseu neban orduen:
Banoa, banoa, pim pum ez dot jarraitzen

Nahi dogune egin geinke
Galtzen badogu dekogun bildurre
Arriskatu zaitez, salto!
Bizi berri bet itxaroten dago

Nahi dogune egin geinke
Galtzen badogu dekogun bildurre
Arriskatu zaitez, salto!
Bizi berri bet itxaroten dago