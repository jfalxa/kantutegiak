---
id: tx-2657
izenburua: Jhon Zaharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R9dQEVOc96Q
---

Mikel Markezek eta Arkaitz Minerrek Gorlizen 2017 martxoaren 17an emaniko emanaldian

Etxerako ordua
iritsi zaigu John Zaharra
Kanta zagun azkena
alaitzeko gure bidea

Zenbat kanta, zenbat hitz
Lagunak lagun truke
Biharko ere zerbait
utzi beharko genuke

Etxerako ordua
iritsi zaigu John zahara
kanta zagun azkena
alaitzeko gure bidea

Gure emaztetxuak
lo hartuta leudeke
ai zer gau goxua
ezkonduta bagendeuke

Etxerako ordua 
iritsi zaigu John zaharra
kanta zaigun azkena
alaitzeko gure bidea

Etzaitez hasi orain
maitasunaren eske
ilargiaren lagun
zeruraino joan gintezke

Etxerako ordua
iritsi zaigu John zaharra
kanta zagun azkena
alaitzeko gure bidea