---
id: tx-2012
izenburua: Apur Dezagun Katea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2BGk7pGkkN4
---

Apur dezagun katea
kanta dezagun batea
hau da Fandangoa
Biba Berango!

Munduan beste asko lez
artaburua mozkorrez
atzo aspertu nintzaden
maisuez eta eskolez.

Poeta naizenez gero
ez dut zerurik espero
bederatzi kopla ditut
lau zuhur eta bost ero.

Ilargiaren adarra
handik zintzilik abarrak
gogoan larriak dira
zure bi begi nabarrak.

Zerutik dator harria
nundikan berriz argian
gau ilun honetan dakust
zure aurpegi garbia.

Artzoko oilar gorria
inundik ez etorria
goseak arintzearren
judu batek igorria.

Goizaldean eguzkiak
printzak daduzka bustiak
oso merke saltzen dira
euskaldunaren ustiak.

Gure amonak esan dit
aitak ardoa edan dik
Joan zaitez tabernara
eta ekar zazu handik.

Euskararen asturua
ez da gauza segurua
askozez hobekiago
dabil munduan judua.

Eta hau hola ez bazan
sar dedila kalabazan
ipuin txit barregarriak
kontatu nituen plazan.