---
id: tx-2288
izenburua: Akuamarina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3jz-notd9IE
---

Zure akuamarina begien
ildaskak galtzen naute. 
Ez baita zerurik 
bertan aurki daitekenik. 

Nire asmoaren baitan 
zure hitzak maitatzen. 
Ez baita zerurik
bertan aurki daitekenik.

Eta zure oreinetan, 
aurkituko ditut
ilargiaren izpiak, 
zurekin dantzatuz
aurkituko naute 
izarren pare. 

Mmmmm itxi begiak. 

bertan aurki daitekenik.

Eta zure oreinetan, 
aurkituko ditut
ilargiaren izpiak, 
zurekin dantzatuz
aurkituko naute 
izarren pare.