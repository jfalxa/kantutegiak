---
id: tx-1624
izenburua: Irailaren 14a
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GNmqKJw9At8
---

Noiz arte segituko duzu 
Ahotsak itzali nahirik 
Noiz arte iraungo dugu 
Zauri zaharrak ezin itxiz 

Denon bihotzak hautsita 
Inpotentziaz ezinduta 
Noiz bihurtu zenituzten 
Zuenak mutuak, harrizkoak 

Eta berriz malko gorriak 
Irailaren 14an 
Noiz arte kondenatuta 
Larunbat ilunetara 

Elkarri kateatuta 
Autonomia kalean 
Elkar kolpatuz, zatituz, urrunduz 
Irten ezinda argi gabeko kalean 

Eta berriz malko gorriak 
Irailaren 14an 
Noiz arte kondenatuta 
Larunbat ilunetara 

Eta ezin atzean utzi 
Irailaren 14a 
Noiz arte kondenatuta 
Negu minera, negu luzera 

Goazen aurrera