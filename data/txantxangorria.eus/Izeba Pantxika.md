---
id: tx-2538
izenburua: Izeba Pantxika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gcj_qJTnsaA
---

Pan/txi/ka du i/ze/na, ni/re i/ze/ba gi/ze/nak.
Ka/le/ti/kan do/a gu/re Pan/txi/ka po/tto/la.
Honela egiten du bere bi hanka motzaz.
Honela e/giten du bere bi hanka motzaz.
Pantxika du izena, nire izeba gizenak.
Kaletikan doa gure Pantxika pottola.