---
id: tx-333
izenburua: Ama Kontatu Arren
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ehdOLw5g23Q
---

Ama kontatu arren herri honen historia
Guztia ez delako odola eta hilobia
Dena es zen geratu Gernikan eroria
Harrotasunez bizi nahi nuke memoria

Ama kontatu arren nola ditugun ama
Amalurra guztia ta ilargi erdibana
Nola hegan dabilen gure Anbotoko dama
Umetxo batek haren izena darama

Aita kontatu arren Orreagako barrikada
Karlomagnori bota genion harrikada
Haiek zortzirehun ziren gu hamabost seme-alaba
Inoiz gure txikian garaile izan gara

Aita kontatu arren Ternuako abentura
Nola baleen artetik irten ginen mundura
Gure erreinua ziren olioa eta ura
Ta baleontzi zaharra berriz zutitu da

Ama kontatu arren nuklearraren borroka
Nola egin genuen zentrala handien kontra
Kalera irten ginen aldarriak borborka
Ta zu gehiago ezin izan ziguten bota


Ama kontatu arren Nafarroko erresuma
Nola izan zen horren handia ta ezaguna
Katea artean preso gorde dut maitasuna
Ama izan ahal naiteke nafarra ta euskalduna

Eta Edurne Pasaban eta Elkano bera
Eta legeak lege ikastolen sorrera
Eta menderik mende iraun duen euskera
Harro egon ezgero herri izango gera