---
id: tx-21
izenburua: Trikitilariak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/elsYQoZYhEU
---

Trikitilariak goizeko seiretan
kalez kale doaz festetan
egun berri on bat
doinu alaietan
panderoa ari da kaletan

Ez egon pentsetan
geroztik penetan
atozte gurekin
dantzetan