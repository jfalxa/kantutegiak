---
id: tx-1428
izenburua: Maitale Izan Debalde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/myu5ctM5vAw
---

• Hitzak : Peio Chauvin
• Musika : Xan Errotabehere

Bizkarrez bizkar,
ni dena indar,
hainbat igan niz debalde.
Bihotza gose,
huts frangoz ase,
halere nauzu maitale.
Ni beti suhar,
eskua onhar,
agian nindaike emaile...
...nindukezu ni emaile
Haiza nezazu !
Zampa nezazu !
Penaz nizate zoroago !
Zutarik urrun
nuzuia egun ?
Elurra udan aiseago !
Ezin ahantzi,
nola itzali ?
Nolaz berriz maita niro ?
Banintza gaua,
zu ilargia,
goxa nezake egunero
Banintz’ihintza,
zu iduzkia,
zuretzat nindaike...
zuretzat nindaike hil artio.