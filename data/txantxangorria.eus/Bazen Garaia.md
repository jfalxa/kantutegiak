---
id: tx-1658
izenburua: Bazen Garaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WvDBw1Zt9Ow
---

Kanta berri bat
hastera doa;
ziur hobea liteekela izan
baina
zuei eskeinia,
barnetik irten da
bazen garaia !
Gure kultura
suspertu bada
askoren lanak
badu ordaina;
zuei
eskerrak hemendik, bihotz
bihotzetik
bazen garaia !

Bazen garaia !
Hasi da jaia !

Eskerrak hemendik, bihotz
bihotzetik
bazen garaia !