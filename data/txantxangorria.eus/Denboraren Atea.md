---
id: tx-155
izenburua: Denboraren Atea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5QYLByJ4Wjc
---

Mendeetako ahots bat beste mundutik
Entzun dut ta hemen nago ohean zutik
“Erderaz eztuzu nai ene biotza
Euzkeraz kopla egiten eztut nik lotza”

“Zure urrezko uleak nauko galdurik
Katena orrekin nauko ongi loturik”
“Zure begietan dago ene suertea
Nai baduzu bizia edo eriotzea”

“Bai, bai, bai
Ez, ez, ez
Zugatik ene begiak daude negarrez
Ez, ez, ez
Bai, bai, bai
Ni joan nai zurekin eta zuk eztuzu nai”

Ez zaidazu galdetu benetakoa naizen
Hilda bagaude nola ari gara kantatzen?

“Ezkuak duzu leun, lepoa txuri
Elurrezko bularra zureak dirudi”
“Jazmiña eta karmiña daode nasturik
Zure matel bietan ondo ifinirik”

“Bai, bai, bai
Ez, ez, ez
Zugatik ene begiak daude negarrez
Ez, ez, ez
Bai, bai, bai
Ni joan nai zurekin eta zuk eztuzu nai”

Begiratu bibratzen nire bi mototsak
Arriagatik datoz perreoen hotsak
Jimmy Jazzen, Kutxi eta Zapa
Flowarekin saltoka igo dut aldapa

Infernura joango naiz reggaetoi kantari
Zure bila noa Errementari
Normaltasun berri hau ezin leike jasan
Pantera beltza naiz ama zuriaren plazan

“Bai, bai, bai
Ez, ez, ez
Zugatik ene begiak daude negarrez
Ez, ez, ez
Bai, bai, bai
Ni joan nai zurekin eta zuk eztuzu nai”

“Bai, bai, bai
Ez, ez, ez
Zugatik ene begiak daude negarrez”
Ey Gasteiz
Please don´t cry
Berrehun urtetan hemen zeunden gure zai