---
id: tx-2740
izenburua: Azken Dantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ci-SGbVY20M
---

Azken dantza hau
maitia zurekin
nahi zinduzket
eraman nerekin.
Baina gaurko xedia
ezin daike betia
badakit nik ere
bihar dela joaitia
Bego pena hau
itzuliren naiz
bai berriz Euskal Herrira.
Bego urte hau
etorriko naiz
betikotz zure ondora.

Azken dantza hau
ez da sekulako.
zin egina dut
zin egin betiko
hemen gure lurrean
Bizi behar dudala.
Eta hori ez bada
hil hotz jar nadila.