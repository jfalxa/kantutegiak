---
id: tx-1874
izenburua: Jaun Baruak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ehCG4EQScV0
---

Jaun Baruak aspaldin 
xederak hedatü zütin,
txori eijer bat hatzaman dizü 
Paubeko seroren komentin,
orain harekin bizirn düzü 
aspaldian gogun beitzin.

Xedera baliz halako 
merkatietan saltzeko,
Ziberuako aitunen semek 
eros litzazkie oro,
halako txori eijerto 
zunbaiten hatzamaiteko.

Igaran apirilaren bürian, 
armadaren erdian,
züntüdan bihotzian, 
armak oro eskian;
present espiritian, 
manka besuen artian.

Jaun maite banaizü, 
erraiten düzü bezala,
kita ezazü, kita ezazü 
Erregeren zerbütxüa
eta maita herria, 
üken dezadan plazera.