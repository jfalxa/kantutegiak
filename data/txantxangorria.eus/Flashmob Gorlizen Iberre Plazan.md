---
id: tx-1702
izenburua: Flashmob Gorlizen Iberre Plazan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1-qjSzbLHJg
---

2014ko maiatzaren 23an GORLIZ-Plentziako "Gure esku dago" taldeak antolaturiko Flashmoba Gorlizko Iberre plazan.

ESKUTAN (Pirritx, Porrotx eta MariMotots