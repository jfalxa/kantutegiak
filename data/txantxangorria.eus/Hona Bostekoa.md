---
id: tx-1390
izenburua: Hona Bostekoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5zwOgbt_9UE
---

Hatz potoloa 
saltoka doa 
Pasaia eta Lezon du egun pasa oparoa 
eskura du euskara. 
Hatz erakusle 
milaka puzzle 
badiaren bost puntak non batzen diren ikusle eta 
eskura du euskara. 
Hatz bihotza da 
denen taupada 
kresalarekin haizean hedatzen den algara 
eskura du euskara. 

Hatz alferrena 
lotan zaudena 
zoaz anai-arrebekin bidean barrena 
eta eskura du euskara. 
HONA BOSTEKOA, ONA! 
EUSKARA BABESTEKO 
ETA EUSKARAZ JOLASTEKO 
HONA BOSTEKOA, ONA! 
EUSKARAZ SOLASTEKO 
ETA EUSKARAZ BIZITZEKO 
Hatz ttipiari 
begira adi 
ikastolarekin batera egingo da handi! 
eta eskura du euskara. 
HONA BOSTEKOA, ONA! 
EUSKARA BABESTEKO 
ETA EUSKARAZ JOLASTEKO 
HONA BOSTEKOA, ONA! 
EUSKARAZ SOLASTEKO 
ETA EUSKARAZ BIZITZEKO 
HONA BOSTEKOA, ONA! 
EUSKARA BABESTEKO 
ETA EUSKARAZ JOLASTEKO 
HONA BOSTEKOA, ONA! 
EUSKARAZ SOLASTEKO 
ETA EUSKARAZ BIZITZEKO