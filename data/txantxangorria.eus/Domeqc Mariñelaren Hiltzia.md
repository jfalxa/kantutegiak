---
id: tx-2346
izenburua: Domeqc Mariñelaren Hiltzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z5BkGLia8hw
---

Ez da mariñeletan ez ja Donibandarrik
Amodiorik ez dianik itxaso handiari
Txipitik maithatüz hori ez da estonagtarri
Nahiz hen zorthia ez den bethi inberagarri
Aitak erakutsitrik bidia semiari
Bizi dira Eskualdün hun, fidel itxasuari

Jinkuak egin bagüntü algarren maithatzeko
Ez zatin Krimiñelik jente hunen arteko
Eziz eta mariñel bat planta hortan hiltzeko
Alhargüntsa gazte baten nigarretan uzteko
Bi urthetako haur bat goizik malürratzeko
Aita eta anaie bat Krimuan probatzeko

Itxas portin hartürik arrantzüko untzia
Gaiaz phartitü ziren hiru aita semiak
Zioielarik gaur date arrantzüko egüna
Fortünatzeko beitügü güne izigarria
Xantzak lagüntzen badü gure projet hartia
Zuiñen eder datin darri etxenko batzarria

Ama baten besuan haur txipi bat nigarrez
Bere aita zenaren besarkatü beharrez
O inozent malerusa badakikek hatsarrez
Bethi izan behar dela lotsa itxasuarez 
Handitü hizatinin ohart hadi aitarez
Eta ez itxasua har irus izan beharrez