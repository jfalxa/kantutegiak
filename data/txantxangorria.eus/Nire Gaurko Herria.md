---
id: tx-2479
izenburua: Nire Gaurko Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UxY_PdimYtA
---

Nire gaurko herriak ez dauka
bere garai bateko itxura
lantzean behin bueltatzen naizenean
Eibarko kaleetara
ezezaguna zait begietan
nire herrian arrotz sentitzen naiz

Lantegiak dira desagertu
handiak eta txikiak
orain dendariak agertzen dira
lehen beharginak zeuden tokian
garai bateko Eibar gris hura
galduta kolore artean

Txikitan sarri joan ohi ginen
fabrika zaharretara
aspaldi luzean itxita zeuden
lana beste nonbaitera eroanda
mamuak bereizten genituen bertan
pabiloi ilunetan beharrean

Aitak dio bazela garai bat
non jendea lan bila etorrita
goizean beharra eskatu eta
arratsalderako beharrean

Mendiguren eta Zarraua
Alfa, Sarasketa, Lambretta,
Norma, Orbea, Star, Aurrera
bizirik gure aitaren garaian
niretzat iragan baten lekuko
orain betirako galduta