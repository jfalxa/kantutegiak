---
id: tx-1218
izenburua: Isolatutako Uhartean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mecx9sccS38
---

Isolatutako uhartean
ezereztutako ikustezinak

Desintegratzera ohitu diren
ohitura zaharretan hautsa
milaka metrotik ikusita
ipuinak besterik ez dira
putz eginez putz eginez
haizeak daramatza
izandako guztia
zirriborro bat besterik ez da

Isolatutako uhartean
ezereztutako ikustezinak

Desintegratzera ohitu diren
sustraien artean hautsa
milaka metrotik ikusita
lastoa besterik ez dira
putz eginez putz eginez
haizeak daramatza
hazirik gabeko airean
ezer jaiotzerik ez da