---
id: tx-1368
izenburua: Zarrantzako Penak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zIxQrgpbsTM
---

Hainitzen ahotikan bai diat aditu,
Denborak lehendanik direla ematu,
Mainak aurthen gurekin izatu baitire,
Mintzatu beharko ziteian bertzela ere.
Noiz ere baitzituen hamasei otsailak,
Eman izan gintuan hiritikan belak.
Ilhabete hunek gintuan fagoratu,
Ordainez martxoak xoil kruelki tratatu.
Martxoak hirurean hasi alta hartzen,
Itsasoa zuan haizez osoki armatzen,
Ez huan ordutikan hilaren finaraino,
Izatu pausurik guretzat egundaino.
Zenbat belatxo hartze, zenbat trebes jartze,
Papaioa nausi eta gabiatik galtze,
Hamahiruraino gintian frogatu,
Momentu tristerikan han artean pasatu.
Hamalaurean huen haizea hain handi,
Halaber itsasoa hainbat izigarri,
Non baikinduan jarri biharamun artian,
Su eta khepean hekin burua etsian.
Nôtre Dame de Sarrance (“Zarrantza”) Donibane Lohizuneko badian ainguratu
ohi zen XVII mendeko balea-ontzia zen. Kantua 1690 urtearen ingurukoa da. Patri
Urkizuren ustez, Zarrantzak 1688 eta 1690 urteen artean Ternuarako bidean izan
zituen arazoak azaltzen ditu.