---
id: tx-1638
izenburua: Ez Etsi  Hesian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J3pCtgUS_78
---

Aurkitu nauzu bidean
Amets egiteko beti prest,
Zerua ukitu nahiean
Alferrik baina bizirik,
Eta ez esan, ez pentsatu
Horregatik, ezkutatuko naizenik.

BAINA ZURE BEGIEK ESNATU DUTE
ILUNPETAN ZEGOEN ITXAROPENA
HITZIK ESAN GABE OIHUKATZEN NAUTE
EZ EZ ETSI, EZ ETSI

Irauten jaio nintzen egunetik
Itsumustuka, malko baten
Ibilbidean galdurik
Loturik, orotzaipenean
Mehatxuak, irudiak
Honezkero, burutik kendu ezinik.

BAINA ZURE BEGIEK ESNATU DUTE
ILUNPETAN ZEGOEN ITXAROPENA
HITZIK ESAN GABE OIHUKATZEN NAUTE
EZ EZ ETSI, EZ ETSI.

Nire baitan helmuga bat
Oraindik urrun sentitzen dudana
Ez nuke nahi bide horretan
Nire hitzekin zure begirada 
Itsasoan galtzerik
Ez arren

EZ EZ ETSI, EZ ETSI.
BAINA ZURE BEGIEK ESNATU DUTE