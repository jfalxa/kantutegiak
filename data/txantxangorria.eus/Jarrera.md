---
id: tx-753
izenburua: Jarrera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4k_Mwlr3vGo
---

Gure kale zahar hauek hildak ziruditen
etorkizuna aldarrikatzera goaz oraingoan
Kateak astintzeko unea heldu dela
kaletan garrasia Kolpez kolpe bat eginda


Akordeen eztandarekin 
errealitatera bueltatuz
mezu zuzen baten bidez
doinuetan isladatu
Soinu zaharrak oinarritzat
ahotsak kolpatuaz
zerbait jaiotzera doa
gure auzoetan zehar


BIZIRIK ETA OSTIKOKA KALEETAN
GOGOR ETA TINKO ZUEN AUZOETAN
MEZUA, JARRERA ETA HARROTASUNA