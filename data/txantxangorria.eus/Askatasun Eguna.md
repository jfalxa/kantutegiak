---
id: tx-942
izenburua: Askatasun Eguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N5zQOOuUBVQ
---

Ja badator, jada gertu dago
Guretzat askatasun eguna
Eta batera abestu dezagun
Alkartuko gaituen oihua.

Une hontan
Gauzak naiko oker
Jendeak nahi du bizi lasai.

Ero gara
Lasai bagaude
Zerbait emango dutenaren zai.

Hala ere
Baikor izan behar
Ekin su ta gar
Eta jarrai.

Ja badator, jada gertu dago
Guretzat askatasun eguna
Eta batera abestu dezagun
Alkartuko gaituen oihua.

Une honetan
Inoiz ez bezela
Sakabanatuta anaiak.

Batzuk orain
Helburuak utzita
Bere aulkia gorde nahian.

Hala ere
Baikor izan behar
Ekin su ta gar
Eta jarrai.

Ja badator, jada gertu dago
Guretzat askatasun eguna
Eta batera abestu dezagun
Alkartuko gaituen oihua.