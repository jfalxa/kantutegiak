---
id: tx-687
izenburua: Nirekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aYD9Ysxh32A
---

Emoixtaxux muxutxuek
maitie
hartuixu guruxun
guztixe
eroaixu bixotz
tristie
zure bixotzarekin.

Emostazu porrutxue
sorgine
ahetetxu baten
berdine
begitxu gorrixen
samine
sendatuixu nirekin.
Oinak hotz gabien, berbak irratixen
bero pixka bat gure neuke
berbaixu bajutxu, eskue helduixu
ez utzi laztanik eiteke.

Gero esnatzien lagaixu etzien
zure marrubi usaintxue
ta ez eixu pentsa ez dela erreza
gauza politxek pentsatzie.