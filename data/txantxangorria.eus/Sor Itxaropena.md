---
id: tx-3060
izenburua: Sor Itxaropena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-nQqB1NyqCE
---

Pobreak ez du ogirikan,
ez (e)ta/aberatsak ere/errukirikan,
indarkeria da nonahitikan.
Entzun da negar eta oihu larri:
Eguberri! Eguberri!
Piztu itxaropen, sor maitasun
herriz-herri.

Giza bihotza ez da lehor,
bihur daiteke baratze emankor,
Salbatzailea horretara dator.
Belen aldetik datorkigu argi
Eguberri! Eguberri!
Piztu itxaropen sor maitasun
herriz-herri.

Jai handi da gaur herrientzat,
jai izan bedi bai guztientzat,
elkarturik maiteki senidentzat.
Zaite Zu, Jesus, bihotzen lokarri:
Eguberri! Eguberri!
Piztu itxaropen sor maitasun
herriz-herri.