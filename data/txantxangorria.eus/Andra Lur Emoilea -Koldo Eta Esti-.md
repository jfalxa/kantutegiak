---
id: tx-3209
izenburua: Andra Lur Emoilea -Koldo Eta Esti-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JUNzqSNburg
---

Polit askoa da goitik behera
dakusanak ezin lelo eutsi.
Besarkatzen dauanak maite ta
lur-emoilea barik
ezin lei bizi.

Ule baltza, betule kizkurrak,
aurpegia zuri, orina baltz.
Haren so gozoak, nire sorgin
kontu erabilizu
lur-emoileaz.

Badadukazu,erre egurrean.
Badadukazu, hil traidorea
Banoa lur-emoileagana,
nire bihotzeko kuttunagana.

Burua emon neban, gitxik lez.
Maitasun-sekretuak esan
maite neban, gitxik egin legez.
Andra lur-emoile.
hil nozu penaz.

Bedar txar, hegian zuztartua
egur igar, surik gabekoa,
usteldu ba, betiko lurrean.
Andra lur-emoilea,
nahi ez banozu.