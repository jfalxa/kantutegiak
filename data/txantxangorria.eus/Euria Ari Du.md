---
id: tx-1944
izenburua: Euria Ari Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IQo1sGMRGgk
---

Azula diskoren singela.
Mikel Urdangarin - kitarra
Koldo Uriarte - piano
Angel Celada - bateria

Gauez jaiki naiz / Despierto, es de noche,
sabaian behera / la lluvia cae
euria ari du. / techo abajo
Kafea baltza, begiak lurrun, / el café, fuerte, los ojos vaporosos
euria ari du. / llueve

Nahi nuke amets / Quisiera soñar,
nahi nuke lo / dormir y más tarde
itzartu gero / despertar
ta oroitu ez / y no recordar nada
burutik kendu / librarme de esa
irudia / imagen
sabaian behera / la lluvia que cae
datorren euria / del techo abajo

Mahai gainean / Sobre la mesa,
ordu joanak, / horas perdidas,
ogi birrinak. / migas de pan
Nire barnean / dentro de mi,
gela bat hutsik, / una habitación vacía,
itoginak. / goteras.

Nahi dut aterki / Quisiera refugiarme
bate(a)n bildu / en un paraguas
ta kiribildu, / recogerme como cuando
berriz umeki / era niño
neuk ez baneki / si no supiera
hautsi naizela / que me he roto
tanta bat lurrean / como una gota de agua
bezala / contra el suelo

puskak biltzeko sasoia da / es hora de unir los pedazos
egurastu / de que entre
dena / el aire
ez dut gehiago euririk nahi / no quiero más lluvia
sabaian / techo
behera / abajo

nahi nuke amets / quisiera refugiarme
baten bildu / en un sueño
ta kiribildu / recogerme y
ta oroitu ez / no recordar nada
burutik kendu / olvidar que
hautsi naizela / me he roto
sentitu atertu / sentir que
duela / ha escampado

berriz hasteko sasoia da / Es hora de empezar de nuevo
zutundu ta / de levantarse
aurrera / y avanzar
ez dut gehiago euririk nahi / no quiero más lluvia
sabaian / techo
behera. / abajo.