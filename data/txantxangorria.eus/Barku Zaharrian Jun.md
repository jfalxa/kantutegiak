---
id: tx-1165
izenburua: Barku Zaharrian Jun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ptnkPFVzdak
---

Barku zaharrian jun,
berrian etorri,
nere nobiyuaren
suertia den hori.

Nere nobiyuaren ama,
asto baten jabe dana
hartxek ekarriko digu
etxean behar degun dana.