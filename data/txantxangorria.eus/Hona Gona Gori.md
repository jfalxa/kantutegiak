---
id: tx-160
izenburua: Hona Gona Gori
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wrLDsvgt6eA
---

Eskean Kristö taldearen (Arratia 2012) lehen diskoa "Azote kaldüz" da. 2013ko Banden Lehia irabazi ondorengo emaitza. Eskean Kristö  taldearen rockak gorputza eta sakontasuna du. Garai bateko rockaren zantzuak, zintzur apurtuaren zertzeladak, gitarraren kolore eta oinarrien kolpea, kilikaria, kateatzaile…

Hona gona gori
Hona gona gori, hona gona gori,
nora joan zoazen baneki ta bazeneki,
nora joan zoazen urrutira nigandik.
Ezin bizi nigaz, ezta ni barik,
ezin egon batera, ezta bakarrik.

Hemen nozu, hondakinak beti jateko prest, beti jan guran.
Hemen nozu, zuri begira, borondatezko esklabu.

Hona gona gori, hona gona gori,
eskutik helduta, min emotera, beste bateri.
Bihotza baltz, asmo garratzak, sugegorri gerri dantzak.
Bihotza baltz, ahoa zoli, auzika guran edozeineri.

Hemen nozu, hondakinak beti jateko prest, beti jan guran.
Hemen nozu, zuri begira, borondatezko esklabu.
Hona gona gori, ezin bizi nigaz, ezta ni barik.