---
id: tx-205
izenburua: Herenegun   Bi Mundu Feat Ttaka Eta Amaio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aPX5X2LvPc4
---

Bi Mundu- Herenegun Taldea ft TTaka eta Amaiow.
Muir Estudioan (Donostia) grabatutako Yon Vidaurrengandik (2022) eta Estanis Elorzak (Doctor Master) masterizatutako abestia.
Julen Arregik eta Mikel Zubillagak eginiko bideoklipa.
Herenegun, TTaka, Amaiow. 2022. Antiguo.

LETRA
Aldapeko sagarraren adarraren puntan
Puntaren puntan, puntaren puntan
Txoriak zioen txirorirori
Txoriak egiten dik txiroriro txiroriro

Txoria ikusten det hegan ihesik
Salto bat eginda jun da adar hortatik
Nora doan hegan nahi nuke nik jakin
Eta toki horretan ba ote den zuhaitzik

Hegan jartzen da berriro lainoen gainetik
Horrela sentitzen baita txoria bizirik
Txoria maite duzula esaten duzu beti 
Benetan maite baduzu hegoak ez ebaki

Txoria joan orduko sagarra da erori
Zuhaitza haserre dago kendu duta gaiñetik
Zu adarra ni sagarra eta nik zure beharra
Nik zugan sinistu nahian utzi nauzu zintzilik 
(Goazen!)

Ezin jakin non zauden galdetzen dudan bakoitzean
ilunean itzala bezala zara ezkutatzen
Eguzkiak indarrez kolorez paisaia marrazten
Ta ni berriz hasi naiz ortzimuga bilatzen

Joan zinenetik ez dakit non nagon nor naizen ta berdin du denak
Azaldu edo etorriko zaren itxaropena oraindik ez da bukatzen
Nahizta gauetan zerura begiratzen
Hor egongo naizelako
Esperantza galdu gabeee e

Atzerrira joan nintzen, basoan galduta, aztarna artean zurekin lotuta,
Herritik portura, portutik bustita,
Ta hor sentitu nintzen urtxintxa,
Utikan, 
egin nuen salto eta hortikan bota nituen malkok, ze hor ere ezintudan aurkitu ta berriz ere in nintzen tristetu 
eta triste jarritu nuen, 
ibai batean nindon bidaiatzen, 
Uste nuen hor zinela ta guzti, 
ta hortxe joan nintzen zuzen eguzkira,
Eta bidean ilargia aurkitu nuen,
Hor ere ezinela ikustean, bihurtu nintzen ero,
ohera erori ta hasi ginen be berriro,

Joan zinenetik ez dakit non nagon nor naizen ta berdin du denak
Azaldu edo etorriko zarenaren itxaropena  ez da bukatzen
Nahizta gauetan zerura begiratzen
Hor egongo zeralako 
Esperantza galdu gabeee e

Ezin jakin non zauden galdetzen dudan bakoitzean
ilunean itzala bezala zara ezkutatzen
Eguzkiak indarrez kolorez paisaiak marrazten
Ta ni berriz hasi naiz ortzimuga bilatzen

Lur jota ohean etzan naiz
Buruz behera
Buruan ahots bat hasten
Zait berriz oihuka
Pentsatzen hasten naizen bakoitzean
Zurea aukera
Sortu berri diren 
Gure bi munduen talka

Aldapeko sagarraren adarraren puntan...
Puntaren puntan
Txoriak zihoen, txirorirori
Txoriak egiten dit txirorirori

Txoria joan orduko sagarra da erori
Zuhaitza haserre dago kendu duta gaiñetik
Zu adarra ni sagarra eta nik zure beharra
Nik zugan sinistu nahian utzi nauzu zintzilik

Ezin jakin non zauden galdetzen dudan bakoitzean
ilunean itzala bezala zara ezkutatzen
Eguzkiak indarrez kolorez paisaiak marrazten
Ta ni berriz hasi naiz ortzimuga bilatzen (x2)