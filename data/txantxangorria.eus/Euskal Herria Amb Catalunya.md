---
id: tx-2610
izenburua: Euskal Herria Amb Catalunya
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xgodeb3lMQE
---

[EUS] Euskal Herriko hamaika musikari, bertsolari eta kantari ekimen propioz elkartu dira, hizkuntza biak uztartu eta Lluis Llach-en L'Estaca kantuaren bertsio elebiduna egiteko. Modu honetan, Durangoko Landako gunean grabatu duten bideoklipean, Euskal Herriko esparru kultural honetatik ere herri katalanari babes eta elkartasun keinua egin gura izan zaio. Helburu hori lortzeko, Gure Esku Dago ekimenarekin elkarlanean ari da musikagintzako herritar talde hau, «Euskal Herri osora zabaltzeaz gain, Kataluniara ere gure ahotsak iritsi daitezen».

[CAT] Diversos bersolaris i cantants bascos s'han reunit per iniciativa pròpia per fer-ne una nova versió, en aquest cas bilingüe, en la qual es combinen ambdós idiomes, el català i el basc. Aquest videoclip, enregistrat al recinte de la Fira del llibre i el disc de Durango, és doncs, una mostra de suport i solidaritat amb el poble català des d'aquest àmbit de la cultura basca. Per aconseguir aquest objectiu, estan treballant colze a colze amb Gure Esku Dago, «ja que no només volen que les nostres veus es difonguin a tota Euskal Herria, sinó que també volen que arribin a Catalunya».

[CAS] Por propia iniciativa, varios músic@s, cantantes y bertsolaris de Euskal Herria se han juntado para unir ambos idiomas y hacer una versión bilingüe de la canción L'Estaca de Lluis Llach. De esta manera, grabaron el videoclip en Landako Gunea de Durango, y han querido trasladar desde este espacio tan referente de la cultura vasca, su apoyo y un gesto de solidaridad al pueblo catalán. Para llevar a cabo esta tarea, el grupo de ciudadan@s músic@s ha contado con el apoyo de Gure Esku Dago a fin de que  «además de llegar a toda Euskal Herria, nuestras voces también alcancen Catalunya».

Help us caption & translate this video!





(8 konpas abestu barik)
L'avi Siset em parlava de bon matí al portal
mentre el sol esperàvem i els carros vèiem passar.
Ez al dek, gazte, ikusten gure etxola zein dan?
desegiten ez badugu, bertan galduko gera.
Baina guztiok batera saiatu hura botatzera,
usteltzen hasita dago-ta, laister eroriko da.
Si jo l'estiro fort per aquí i tu l'estires fort per allà,
segur que tomba, tomba, tomba, i ens podrem alliberar.
(4 konpas abestu barik)
Però, Siset, fa molt temps ja, les mans se'm van escorxant,
i quan la força se me'n va ella és més ampla i més gran.
Usteltzen badago ere, karga badu oraindik,
berriz arnasa hartzeko esaigun elkarrekin:
Baina guztiok batera saiatu hura botatzera,
usteltzen hasita dago-ta, laister eroriko da.
Si jo l'estiro fort per aquí i tu l'estires fort per allà,
segur que tomba, tomba, tomba, i ens podrem alliberar.
(4 konpas abestu barik)

L'avi Siset ja no diu res, mal vent que se l'emportà,
ell qui sap cap a quin indret i jo a sota el portal.
Haur batzuk ikusten ditut eta inguraturik,
aitona zaharraren kanta nahi diet erakutsi:
Baina guztiok batera saiatu hura botatzera,
usteltzen hasita dago-ta, laister eroriko da.
Si jo l'estiro fort per aquí i tu l'estires fort per allà,
segur que tomba, tomba, tomba, i ens podrem alliberar.
Baina guztiok batera saiatu hura botatzera,
usteltzen hasita dago-ta, laister eroriko da.
Si jo l'estiro fort per aquí i tu l'estires fort per allà,
segur que tomba, tomba, tomba, i ens podrem alliberar.
Lara lara lara, lara lara lara lara
lara larala ralarala ralara lara lara
Lara lara lara, lara lara lara lara
lara larala ralarala ralara lara lara
Lara lara lara, lara lara lara lara
lara larala ralarala ralara lara lara
Lara lara lara, lara lara lara lara
lara larala ralarala ralara lara lara