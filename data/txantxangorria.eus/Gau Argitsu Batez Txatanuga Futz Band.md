---
id: tx-3077
izenburua: Gau Argitsu Batez Txatanuga Futz Band
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XglGMWAKyqY
---

Gau argitsu batez, lehenengo baten
amets zoragarri bat topatu nuen
egonezinez, ikusi ondo/en
noiz agertuko den, bai, desiratzen.

Gau argitsu batez, bere irifarra
haizeak zekarren goxo niregana
ahaztu ezinez, ikusi ondoren
farre xarmant hori kantari zuen.

Nork e/san/go di/o, ba/tek ba/da/ki
a/rra/zo/ien bi/la, ho/rre/tan na/bil.

Gau ar/gi/tsu ba/tez, mai/te/min/du/a
sen/ti/tzen ba/du/zu be/ne/tan ba/rru/a
be/ra i/kus/te/an ta hu/rre/ra/tze/an
in/gu/ra/tu zai/tez ta kan/ta/tu ho/lan.

Nork e/san/go di/o, ba/tek ba/da/ki
a/rra/zo/ien bi/la, ho/rre/tan na/bil.

Gau ar/gi/tsu ba/tez, mai/te/min/du/a
sen/ti/tzen ba/du/zu be/ne/tan ba/rru/a
be/ra i/kus/te/an ta hu/rre/ra/tze/an
in/gu/ra/tu zai/tez ta kan/ta/tu ho/lan.

Nork e/san/go di/o, ba/tek ba/da/ki
a/rra/zo/ien bi/la, ho/rre/tan na/bil.
Bai ho/rre/tan na/bil.

Gau ar/gi/tsu ba/tez, mai/te/min/du/a
sen/ti/tzen ba/du/zu be/ne/tan ba/rru/a
be/ra i/kus/te/an ta hu/rre/ra/tze/an
in/gu/ra/tu zai/tez ta kan/ta/tu ho/lan.