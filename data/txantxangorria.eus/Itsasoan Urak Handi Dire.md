---
id: tx-1894
izenburua: Itsasoan Urak Handi Dire
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4p_mWylMuRA
---

Zazpi laguneko famili batetan
arotza zen gure aita
gure herriko alkate ez izan arren,
lan egiten zuena.

Itsasoan urak handi dire,
murgildu nahi dutenentzat
gure herriko lanak handi dire,
astun dire, gogor dire,
zatiturik gaudenontzat.

Markatzen ari diren bide nabarra
ez da izango guretzat,
lehengo sokari ezarririk datorren
oztopo bat besterik.

Itsasoan...

Hainbeste mendetako gure morrontzak
baditu mila tankera
eta, zuhur aukeratzen ez badugu,
bertan galduko gera.