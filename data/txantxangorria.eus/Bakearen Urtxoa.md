---
id: tx-468
izenburua: Bakearen Urtxoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L_vj91mrJ0g
---

Bakearen urtxoa,
Oi! urtxo eztia,
hegalez kurri ezazu
lur zabal guztia.
Kontsola gerlapean
dagokeen jendëa,
hilen haur, aitetamak
eta emaztëa,
kolpatua herrestan
dabilan tristëa,
presoner dohakabe,
hilik den bizia.

Errozute deneri:

Jende gaixoak,
ez beha gau beltzari
baina bai izarreri!
Bakea dela zueri!

Bakearen urtxoa,
jarrai bideari,
zure lili eztiak
eskainiz orori.
Mintza zaitez ezinik
dagoen eriari,
bihotza bero ezaiozu
zahar hoztuari,
esperantxa emaiozu
kezkan den amari,
bere latza keniozu
hilen ezkilari.

Errozute deneri:

Jende gaixoak,
ez beha gau beltzari
baina bai izarreri!
Bakea dela zueri!

Bakearen urtxoa,
hemen zaitez geldi,
lur hau da sakratua,
deitzen da Euskadi.
Zorte txar batez joak
girade aspaldi,
denak anaiak eta
bizi bi alderdi.
Gerla da berrikitan
pasa hemen gaindik;
euskalduner urtxoa,
otoi, mintza bedi!

Errozute deneri:

Oi, euskaldunak,
ez beha gau beltzari
baina bai izarreri!
Bakea dela zueri!