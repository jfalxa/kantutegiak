---
id: tx-2719
izenburua: Not Time For Love Mikel Markez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ziX4HSu9QOI
---

Hoien legea guretzat apartheid,
gartzela, ihesa, hauspena ta isiltzea

hoien legea,
zu ta ni heuren gisara eukitzea
gordeak dituzu arma ta izpien
kontrako altzeiru atzean
eta gu negar gasen mende gaudenok
oker omen gaude

Ez dute uzten maitatzen gauean

Tortura sufritzen, zeldatan usteltzen,
gutunak idazten eta bertan hil
eutsi zioten minari baina
bakardadeak irentsi
gorteek justizia egin zieten
matoi txukunen gisan
maiz borrokatuak iraun gogoz
baina maizago hiltzea nahi

Ez dute uzten maitatzen gauean
ez agurrik ez negarrik gauean
ez zergatik ez norarik gabe
sirenotsa garraixi bakarra gauean.

Eraman zituzten Sacco ta Vanzetti
Lorca, Lauaxe/ta eta Puig Antich
etorri ziren Victor Jara
Bobby Sands ta lagunen bila
Bostonen, Kabulen, Ramallahn;
Santiagon, Saharan, Belfasten
edo Elgoibarren

eta beste hainbat lekutan
hau zerrenda amaigabea

Zatozte anaiei borrokan
lagundu nahi duten guztiak
gcrrara ohitu zintezke
baina gerra egon badago
arrainak itsasoa bcehar du ta
herriak zu behar zaitu
gure gogoak iraun artian
ez da beldurra nagusituko.

Ez dute uzten maitatzen gauean
ez agurrik ez negarrik gauean
ez zergatik ez norarik gabe
sirena hotsa garraixi bakarra gauean.
Sirena garraixi bakarra gauean.