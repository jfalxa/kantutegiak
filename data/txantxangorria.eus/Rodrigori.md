---
id: tx-3149
izenburua: Rodrigori
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MbO-wr0zjpM
---

Niko Etxart, Beñat Axeari, Shanti Jimenez, Amaia eta Imanol Larzabalek Pierre Topet Etxahun eta Etxahun Hiruriren omenez 1980an egindako diskan Hirurien gai hau era zoragarriz interpretatu zuten.

Jinkoak ezarririk familiaren bürü
Ohore haizü da ama bezain aita
Haur gisako zonbait harien üngürian
Ho/ri da e/gi/az/ko bi/zi/tze per/fe/i/ta
Bena nahi gabiak beit dirade artian
Hortan da Rodrigo ez jakin zer haüta
Aita afruntütan, nigarretan üztia
Edo pünitzia, maitiaren aita.
Be/na na/hi ga/bi/ak be/it di/ra/te ar/ti/an
Hor/tan da Ro/dri/go ez ja/kin zer haü/ta
Ai/ta a/frun/tü/tan, ni/ga/rre/tan üz/ti/a
E/do pü/ni/tzi/a, mai/ti/a/ren ai/ta.
Es/ku/al/dün o/do/la, da de/nen lei/ha/le/na
Cas/ti/lla/ko/a al/diz, o/ro/ren be/ro/re/na
Han ez/pa/ta pün/tan, jü/ja/tü/rik di/ra/de
Bi/de xü/xe/ne/tik baz/ter/tzen di/re/nak.
Ro/dri/go jo/an ha/di, haü/ta/tü dü/an bi/de/an
O/rai kun/pli/tze/ra ai/ta/ren de/sei/na
Zer na/hi hel da/din gü/zi/ak par/ka/tü/rik
Ü/tzü/li/ren bei/tzaik fi/de/lik Xi/me/na
Ro/dri/go jo/an ha/di, haü/ta/tü dü/an bi/de/an
O/rai kun/pli/tze/ra ai/ta/ren de/sei/na
Zer na/hi hel da/din gü/zi/ak par/ka/tü/rik
Ü/tzü/li/ren bei/tzaik fi/de/lik Xi/me/na
Zer na/hi hel da/din gü/zi/ak par/ka/tü/rik
Ü/tzü/li/ren bei/tzaik fi/de/lik Xi/me/na
Zer na/hi hel da/din gü/zi/ak par/ka/tü/rik
Ü/tzü/li/ren bei/tzaik fi/de/lik Xi/me/na
Xi/me/na
Xi/me/na
Ützüliren beitzaik fidelik Ximena
Ximena
Xi/me ...