---
id: tx-2715
izenburua: Jan, Jan, Jan Eta Jan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_wi5ilGfI-o
---

Jan, jan, jan eta jan
sagarra sartu dut ahoan.
Edan, edan, edan
egarririk zaudenean.
BELARRIEK JAN NAHI DUTE,
GOSEZ DAUDELA DIOTE,
SOINUAK JATEN DITUZTE
NIK BEHINTZAT HALA DUT USTE.