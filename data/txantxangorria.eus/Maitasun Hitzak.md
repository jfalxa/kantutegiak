---
id: tx-1073
izenburua: Maitasun Hitzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TewzdvrcN48
---

Berak maite ninduen
Nik bera ere bai
Oraindik oroitzen naiz
Bere begietaz
Oraindik oroitzen naiz
Zuhaitz lore artean
Berak esaten zizkidan
Maitasun hitz politak

Maitasun hitzak, umil ta xamurrak
Besterik etzekien, hain gaztea zan
Etzuen mundua, gaiztoa ikusten
Oraindik ametsak, bizi zituan
Hiru gauza bakarrik zekizkin esaten
Aintzinako ipuin xarmangarriak
Maitasun istori, poeten abesti
Besterik etzekin, hain gaztea zen

Egunak joan ta gero
Nabaritzen dudanian
Bakardade haundi bat
Neure bihotzean
Oraindik oroitzen naiz
Zuhaitz lore artean
Berak esaten zizkidan
Maitasun hitz politak.