---
id: tx-2233
izenburua: Bortükak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/E9PBzWh5Eb8
---

Sasu ederra jin denean,
Pettara ützi beharrean
Egün artzainak joanik gira,
bortüala bürüz goizaldian
Gure artaldearen ürratsean,
tzintzarradaren sonüan
Karrikak oro gainti ebilten, 
denek algarrekin alegrantzian

Begira detzagün beti, 
gure Eüskal Herriko ohidürak
Herriak bizirik izan ditean, 
"ta kanta ditzen bortüak"

Gero, üdan denbora dügü,
bortü hegietan igaraiten
Kabalen artean egünoroz, 
gure lana baliatzen
Besterik ez nahiagorik, 
gü heben gira gozatzen
Arrenkürak saihetsean ützirik,
gure egiazko bizia da heben.

Larrazkena heltzen delarik,
bazterrak gorriz beztitürik
Olhako bizia ützirik, 
etxaltealat arra-ützülirik
Ordüan ez dügüla jagoiti,
beste mengoa handiagorik
Dagün urtean arrajin ditean,
bortükak bedatsea arrajinik