---
id: tx-2939
izenburua: Parranda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/riNo_XHncYM
---

Ilundu da eguna
gaua argitzen koloretako bonbilak
festetan gabiltza
oraindik ez goaz lotara,
dantza ta dantza ariko gara!

Parranda egin nahi dugu, bai!!
izarpean jolastu
Parranda egin nahi dugu, bai!!
algara, irrintzi, kantu.

Txaranga saltoka,
tiobiboan agur eginez bueltaka,
txurroak ñam-ñam-ñam
bero-bero kukurutxoan,
firin-faran denok parrandan! bai!!

Parranda egin nahi dugu, bai!!
izarpean jolastu
Parranda egin nahi dugu, bai!!
algara, irrintzi, kantu.