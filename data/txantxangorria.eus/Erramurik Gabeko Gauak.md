---
id: tx-860
izenburua: Erramurik Gabeko Gauak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LxvMRK0GWkM
---

Erramu koroen zain 
ez gara inoiz egon, 
baina ez esan guri 
zer den txar eta zer on. 

Estatuak eskale- 
entzat bakarrizketa. 
Hezur eta haragi- 
-zkoa da gure festa. 

Ondo hondoratua, 
txarto ohoratua. 
Ahazturak badaki 
zein den gure hautua. 

Gaua putzua bada, 
ezin naiteke alda. 
Gaua samurra bada, 
samurra nire malda... 

Aire garbi apurra, 
zigarroaren kea, 
atxiki eta arnastu, 
aski eta pakea. 


Egunaren nekeak 
kendu arren barrea, 
imintzio samurra 
bedi gure larrea. 

Eginaren ordaina, 
sobera eske al da? 
Norberari berea, 
leun dezagun malda. 

Hitz samurrez biluzi 
egun ilunen kraka. 
Erantzi eta erantzi: 
gauak arrazoi dauka! 

Erantzi hitzak eta 
erantzi atzaparrak, 
jantzi denak ahantzi 
“erantzi” hitza jantzi. 

Ispilua zu zaitut 
gardena eta zintzo: 
benetako islada 
zure begien mintzo. 

Gaua putzua bada, 
ezin naiteke alda. 
Gaua samurra bada, 
samurra nire malda...