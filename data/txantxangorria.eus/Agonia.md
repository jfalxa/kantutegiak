---
id: tx-408
izenburua: Agonia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I4_KoSOwGN4
---

Morire come le allodole assetate
sul miraggio

O coma la quaglia
passato il mare
nei primi cespugli
perché di volare
non ha piú voglia.

Ma non vivere di lamento
come un cardellino accecato.


Hil, hegats egarrituak bezala
Ispilatze baten gainean.

Edo galeperra bezala
Itsasoa zeharkatu ondoren
Lehen matekin batera
Hegan egiteko gogorik ez duelako.

Baina ez negar hutsez bizi
Kardantxilo itsutu baten antzera.