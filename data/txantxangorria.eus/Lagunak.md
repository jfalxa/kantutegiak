---
id: tx-1565
izenburua: Lagunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rYBUq6jPhCA
---

Zenbat momentu zailetan aurkitu gara? Zenbat ezinezko lortu ditugu? Nola heldu gara beti izarretara? Nola egin dugu ezin bada lortu? Bat gehi bat mila gara, zuk laguntzean, bultzada bat putzu sakona uzteko. Horma garaiak igotzean, heltzen nauen soka tentea, une latzetan irrifar bat lortzeko. Laino artean bidea, galdu baduzu, arazoen aurrean, ez larritu, zure laguna nauzu, etzazu ahaztu, laguna nauzu. Zenbatetan bueltatuko gara izarretara? Zenbat ezinezko lortuko ditugu? Behar duzunean hementxe nago, niretzat anaia zarelako, lagun on bat baino askoz gehiago, gehiago, gehiago...