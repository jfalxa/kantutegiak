---
id: tx-2656
izenburua: Gorbeiako Larran
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s_0NR53aOPo
---

GORBEIAKO LARRAN
AKER BAT DAGO ERDIAN
ATSO ERRENA DABIL
BERE AURREAN DANTZAN

GOAZEN, GOAZEN, GOAZEN
EURARI LAGUNTZEN
MENDI TA ARKAITZ ARTEAN
IUJUJU EGITEN

LARA, LARA, LA,...