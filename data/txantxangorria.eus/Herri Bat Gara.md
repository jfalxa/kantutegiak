---
id: tx-1633
izenburua: Herri Bat Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/T4e79jeRAVs
---

Zure nahia, nire nahia
joanean bat egin dira
pausuz pausu, tantaz tanta
urak egin du bidea.


Abian da, abian da
herri honen aldarria
izan nahiaren egarriz
itsasorantz doa uholdea.

Herri bat gara, bai herri bat gara
kateak haustera doana.

Urak arrakaldu du lurra
ta ondu ditu gure penak
bota daiogun suari egurra
garretan da sentipena

Zirimolan danbadan
urak ireki ditu harresiak
kimu berriak sortu dira
Euskalerriaren loraldian.

Ur tanta bat ur tanta bi
garai berri bat da ageri
zirimolaren danbadan
kimu berriak ganbaran.

Zure sua nire sua
bat egin dira su garretan
iziotu da gure nahia
barrendu zaigu zainetan.

Abian da, abian da
herri honen aldarria
izan nahiaren egarriz
itsasorantz doa uholdea.

Ur tanta bat ur tanta bi
garai berri bat da ageri
zirimolaren danbadan
kimu berriak ganbaran.

Herri bat gara, bai herri bat gara
kateak haustera doana.