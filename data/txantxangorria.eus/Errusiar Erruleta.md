---
id: tx-865
izenburua: Errusiar Erruleta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FWgVVdCYCgY
---

ERRUSIAR ERRULETA (ERTZAK 2019)

Grabaketa/Grabación: Peio Gorrotxategi (Aieka Estudioak)
Nahasketa/Mezcla: Txosse Ruiz (WheelSound Studio)
Master: Txosse Ruiz (WheelSound Studio)
Bideoa: JC Exekutibo

HITZAK:

Ondo hasi dena ez da ondo bukatu
zure mozorroa kentzerakoan
Oihu egin nere izena gorroto banauzu
Oihu egin nere izena gorroto banauzu

Oro jakintsu asko dago inguruan
zer irakatsi dezake ezjakinak
Oihu egin nere izena gorroto banauzu
Oihu egin nere izena gorroto banauzu

Errusiar erruleta
nahigabe hasi zara jolasten gurekin batera
zure txanda iritxi da
errusiar erruleta zure amaiera iritxi da