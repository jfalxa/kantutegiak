---
id: tx-1381
izenburua: Zestoako Harrobia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aQAJLe0RIag
---

Gaur kantatutzera goaz
gure munduaren berri,
inguruan bait ditugu
hainbat eta hainbat zerri.

Gizonikan eskasena
dena agintez puztua,
mundu erdian gosea
eta arnas estua.

Gaur egun nagusi dugu
moda eta estrabagantzia.
Marta Sanchezen globoek
dute orain garrantzia
Ekologiak hartu du
oso bide aldrebesa,
itxasoko ura dugu
orain gazi ordez geza.

Zestoako harrobia
hau dek erokeria,
ama natura apurtu
herriaren hilobia.

Gaur kantatutzera goaz
gure munduaren berri,
inguruan bait ditugu
hainbat eta hainbat zerri.