---
id: tx-2059
izenburua: Zenbat Min
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RRzkKv4bqT4
---

aske naiz isilpean
irria begietan
ilusio lainoetan itsututa
amildegi hertzean
barearen ostean
ekaitza dator bero
aurrez aurre ikustean
barne suak itotzen nau berriro
zalantzak beldurtzen nau
beldurrak babesten nau
zu nun zauden pentsatzeak
saminduta gorrotoz betetzen nau
zenbat negar eta zenbat min
ta zenbat gau lo ein barik
zenbat musu eman gabe
(ta zenbat hitz esan gabe)
zenbat malko... maite zaitudalako
zu gabe... nun zaude-