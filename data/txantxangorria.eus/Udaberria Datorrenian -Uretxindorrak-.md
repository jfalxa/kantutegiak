---
id: tx-3208
izenburua: Udaberria Datorrenian -Uretxindorrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CPFI5EJJhPQ
---

Udaberria datorrenian
mutil gazteak alaitu,
hainbat neskatxa dabiltzansan
mutil zaharren atzean.

Uda erdian elkartu eta
beti loturik ibili,
azkatasuna aspaldi galduta
bere mendean egonik.

Negu gorriaren hotzakin
bazterrik bila ezinka,
txikito lakain bat ezin edanik
etxe barruan sartuta.