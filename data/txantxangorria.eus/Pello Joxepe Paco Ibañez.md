---
id: tx-3113
izenburua: Pello Joxepe Paco Ibañez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gYrYeZcINwc
---

Pello Joxepe tabernan dala
haurra jaio da Larraulen
etxera joan da esan omen du:
-Ez da neria izanen,
bere amak topa dezala
haur horrek aita zein duen.

Pello Joxepe tabernan dala
haurra jaio da Larraulen



Hau pena eta pesadunbria!
Senarrak haurra ukatu
haur horrentzako beste jaberik
ezin lite/ke topatu.
Pello Joxepe bihotz neria
haur horrek aita zu zaitu

haur horrentzako beste jaberik
ezin liteke topatu.

Fortunatua nintzela baino
ni naiz fortuna gabia,
a/gu/re ba/tek e/ra/man di/tu
umea eta andria
Pello Joxepe bihotz neria
haur horrek aita zu zaitu
haur horrentzako beste jaberik
ezin liteke topatu.




haurra berea bazuen ere
andrea nuen neria.