---
id: tx-1342
izenburua: Lau Urteko Haur Baten Hiltzea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5ia2RDrDVR4
---

Sujet eder bat deklaratzen dut gure artean gerta dena.
Aditzeak berak ematen daut nire bihotzean pena
Guztiek hau konpreni dezaten lau urteko haur batena
gisa honetan martir hiltzeko gaixoak non zu(e)n hogena.

Haurraren aita gertatua da gaitz batez atakatua,
konsultatu ere izan baitu bere doktor medikua
ta jende urinez frotatzeko eman dio kontseilua,
zeren bertzela ez da izanen gaitz hor(re)tarik sendatua.

Haur gaixo hori hil eta gero gerrenian zuten jarri,
ama su egiten ari zen eta aita itzultzen ezarri,
mundu guztiak ikusi dezan ez deia pena egingarri,
zenbat sufriarazi dioten aingeruño gaixoari.

Haur gaixo hori ari zutela han gerrenian erretzen
nehork ikusiko zituztela ez baitzitzaien pentsatzen
baina aingeruek abertiturik aitatxi arribatu zen
sartu orduko abiatuta haurra non zuten galdetzen.

Aita tristerik zegoen baina ama koraje handian
Arrapostua ematen dio lo zegoela ohian.
Zerbait iduriturik bilari abiatu zen ordian
haurra errerik kausitu zuen gerrenian ohe azpian.

Buraso krudel batzuendako hira zuten flakezia
aitortzen ziren obligatuak hain beren krimen handia
behaz bezala abisatua zen herriko justizia
lepoak mozteko izan dute azkeneko sententzia.