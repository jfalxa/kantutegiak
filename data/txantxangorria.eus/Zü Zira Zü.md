---
id: tx-2670
izenburua: Zü Zira Zü
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-prWcvDDxqY
---

Zü zira, zü, ekhiaren paria,
        Liliaren floria,
Eta mirail ezinago garbia!
Ikhusirik zure begithartia
Elizateke posible, maitia,
        Düdan pazentzia,
Hanbat zirade glorifikagarria!