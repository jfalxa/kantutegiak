---
id: tx-3317
izenburua: Alarguna -Argoitia Ana-Arrebak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_t8eRrIN5ss
---

Oraindaineko inularretan
maite nuen itsasoa;
arraiz beterik azaldutzean
txalupa muga lepoan.
Arrantzalea nuen senarra,
arrantzalea gaixoa,
itsas gaizto horrek errez iruntzi,
gaur diot nik gorrotoa

Alabatxoa mendi gainetik
aitatxo noiz etorriko
itsasoari begira dago,
eguna argitu dulako.
Nik nola esan itoa dala,
galdu zaigula betiko?
ta alabatxoa zai ta zai dago
eguna argitu duneko.

Bedeinkatzen det Jaungoiko ona.
Bedeinkatzen det zure eskua,
bakar-bakarrik eskatzen dizut
senarrarekin zerua.
Zeru goietan bildu gaitezen
beheko familia osua.
Ongi zaude ta noiz ikusiko
aita zain alabatxoa.