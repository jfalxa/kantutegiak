---
id: tx-296
izenburua: Apala Haiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fi-DSHUfUO8
---

Apala Haiz (Jon Bergaretxe/Joxean Artze) 1978.


Apala haiz, zomorro, jauntxoekin bainan, zein gogorra mendekoekin.
Goitik datorkikena doblatua igortzen diok behekoari.
Zehatz-mehatz txirtxila hazatela goikoek, guk hitaz zipristiñak besterik nabait ez ditzagun.