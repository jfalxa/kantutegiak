---
id: tx-2291
izenburua: Argizagiak Zelütik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/owWJJfppOP8
---

Argizagiak zelütik
Argitzen dizü eijerki;
Ene maite pollita eztüzü ageri
Zelüko Jinko Jaona! zer eginen düt nik
Zer eginen düt nik.

-Fiatik batere eztüzü,
Mündia erriz ari zaizü;
Bathü orotzaz agrada zira zü,
Bat har ezazü, hura aski dükezü,
Horrez segür nüzü.

Ürso-aphalaren malürra
Galdüz geroztik laguna!
Triste da bethi bere bihotzetik,
Zeren ezpeitü maithatü bat baizik
Maithatü bat baizik.

Amodio berriak
Sendotzen tizü eriak;
Zure begiak haiñ dira eztiak
Zeren beitira eniak zuriak,
Zuriak eniak."