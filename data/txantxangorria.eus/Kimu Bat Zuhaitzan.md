---
id: tx-1260
izenburua: Kimu Bat Zuhaitzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QGcprbr5Osg
---

Gogoan dut goiz hura
negua zen
elur giro ta izotz
jende guztia triste ta goibel
aurpegi, gorputz ta bihotz…

Nire gitarrarekin irten nintzen
natura ilun hartara
eta orduan ikusi nuen 
kimu bat zuhaitzan.

Zuhaitzpe hartara
joaten hasi nintzen
neguaren ekaitzan
kantatzera irten zela
kimu bat zuhaitzan.

Kimu bat zuhaitzan
kimu bat zuhaitzan
han sortu baitzan
salba gaitzan
kimu bat zuhaitzan

Negu beltz luze hura
igaro zen 
urtu ziren izotzak
udaberriak askatu zituen
ibaiak eta bihotzak.

Eta nola indarrak hartzen zituen
argia ta ekaitza
eta berdetuz loratu zen
kimua zuhaitzan.

Kimu bat…

Geroztik negurik beltzenetan
dena ilun denean
gogoan dut kimuaren jaiotza
gure lurraren minean.

Eta nola indarrak hartzen zituen
argia ta ekaitza
negu beltzean loratu zen
kimua zuhaitzan

Kimu bat zuhaitzan….