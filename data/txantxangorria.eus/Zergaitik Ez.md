---
id: tx-1814
izenburua: Zergaitik Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dJZAIwyd4mQ
---

zergaitik ez?
zergaitik eeeez ez?
olentzero ez da pasatzen
afrikatik
zergatik ez?
zergatik ez, ez?
hau bai eta hori eeez!!

niretzat zergaitien
misilen eta kanoiak
hartu ta kalera botatzeko
sagarrak eta meloiak
baita negurik hotzenetan
kolorez berriz jantziz gero
luzatzeko prest gaude!

zergaitik ez?
zergaitik ez?
olentzero ez da patzen afrikatik??!
zergaitik ez?bost ta bost hamar ta hamaika ,ez?

zergaitik ez?
zergaitik ez?
euskal presoak ez daude euskal herrian?
zergaitik ez? zergaitik ez, ez ,ez?!
hau bai eta hori ez!

zein ederra izango zan izan
sinistu meseduan
infernutik etortzeko
gure azken kontzertua
zer dakarkizue hor goian
emen beheian begilez
hurrengoan entzun beharko duzue
zergaitik ez?

ez ez ez zergaitik ez ? zergaitik ez , ez ez?
Olentzero ez da pasatzen afrikatik !
zergaitik ez?zergaitik ez?
bost ta bost hamar ta hamaika ez?
Hau bai eta hori ez!