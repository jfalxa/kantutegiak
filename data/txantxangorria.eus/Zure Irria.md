---
id: tx-2545
izenburua: Zure Irria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H7oKlmftHQQ
---

Saiek jaten duten aragia 
Baino txarragoa da gizona 
Ortziren ekaitza jaiki da 
Sugarren gara berpiztu da 
Gaizki badoa gure herria 
Emaidazu ba zure irria 

Alaitasunaren bila goazen 
Jende baliosik ez da hemen 
Gu ere ustelak izan arren 
Zerbait saia gaitezen berriz sortzen 
Hutz dezagun gaur gure herria 
Emaidazu ba zure irria 
Ene begiko argi bakarra