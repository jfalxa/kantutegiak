---
id: tx-78
izenburua: Rude Girl
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fg-z-zhyjng
---

RUDE GIRL

Beti pasatu ditut CD-etan amodiozko abestiak,
beti iruditu zaizkit baladak apur bat gogaikarriak.
Baina pikutara dena nire bertsoak zuretzako dira,
egunak eman nahi ditut zure ahotsa entzuten, zuri begira.

Ametsetako lagun,
bihotzez maite zaitut.
Oztopo guztien aurka borrokatzen
erakutsi didazu.

Maite dut zure herriarekin daukazun konpromezua,
maite dut begirada batekin bidaltzen duzun mezua.
Zu zara gizatasunez jokatzeko nahi dudan eredua.
Zure eskutik hartuta egurra emanez utzi nahi dut mundua.

Ametsetako lagun,
bihotzez maite zaitut.
Oztopo guztien aurka borrokatzen
erakutsi didazu.

Ez dago zure ahoa istaliko duen makillajerik,
ez dago mundu puta honetan zurekin ahalko duenik.
Mila esker baino gehiago emanez abesten dizut zuri,
segi aurrera ta ez aldatu inoiz neska borrokalari.

Ametsetako lagun,
bihotzez maite zaitut.
Oztopo guztien aurka borrokatzen
erakutsi didazu.