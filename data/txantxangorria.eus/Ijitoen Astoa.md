---
id: tx-1432
izenburua: Ijitoen Astoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uS56FOSLJ-s
---

Ijitoen astoa
kantuan hastera doa, Aaaa!
Berri on bat eman nahi du
Belen aldetik jasoa.Aaaa!
Jainkoa jaio da Belengo estalpean,
ez naiz berriketan ari
hantxe bertan ni hantxe nintzen bai.
Idi zahar baten ondoan. Aaaa!
Idi zahar adar motzak
pozirik dauka bihotza. Muu! Muu!
Idi baten harnashotsak
nahi ditu Joxe arotzak. Muu! Muu!
Menditik haize hotza, errekaldean izotza
negarren malko printzak
haren begietan dabiltza,
Jesus haurraren jaiotzan. Muu! Muu!
Dulundak gauerdian
bildotsak eta ardiak, Bee! Bee!
Menditik datoz artzainak
Perutxo eta Maria. Bee! Bee!
Salto ta brinko eginaz Belen aldera doaz
han goian izar artean
izar agi bat dizdiz ari da
estalpe baten gainean. Bee! Bee!