---
id: tx-2531
izenburua: Moonshine
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vOrgfdM9jME
---

Zuzenean grabatuta, Elorrioko (Bizkaia) Argiñeta auzoan. 2017ko abuztuan.

 
Kamera, edizioa eta zuzendaritza: Paul Baños.
Soinua: Unai Abaunza.
Dron irudiak: Igor Tellitu.


"Noraeza, ezereza eta nahasmena
botila bat, berandu baino lehen hurrena
Kristauak eta etxekoandreak kalera
Belaunikaturik aldaketaren zain
Aldaratu zituztenak aldaraztekotan.

Debekua lege bihurtzen denean
Lehortea soluzio garestiena
jendea ero alde batetik bestera
baso aldean ikusi daiteke
sugarraren distira ilargipean. 

MOONSHINE! MOONRAKERS!

Gauaren iluntasunaren lagun, 
lagunaren laguntasuna beti ilun
odola bor-bor bihotz taupadak azkar
Milaka begi kokotean baina
ez dago beldurrik Tommyren konpainian

MOONSHINE! MOONRAKERS!"