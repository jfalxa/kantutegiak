---
id: tx-2226
izenburua: Telesforo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/k1VKes0-iFs
---

Eusko Gudarostea sortu 
Lortuz arma franko
Arma, tiro, pum! 
Eutsi genion tinko 

Monzondar Telesforo, 
Borrokaz herria jaso 
Moztutako sustraietatik 
bietan jarraitzeraino

Denok ukabilak gora 
Ortzira begirada 
Odola bada borbor 
gogor bihotz taupadak

Erbeste eta kartzela 
Etxe berria 
Abestuz zazpiak bat 
Euskararen herria 

Ez ziren miopiak, 
Denboraren distopiak 
Askatasun utopiak 
Lubaki zahar-berriak

Izar gorriak dir dir 
Gudarien bihotzkada 
Itziar, Monzon, Txikiaren 
Seme alabak gara

Denok ukabilak gora 
Zerura begirada 
Odola bada borbor 
gogor bihotz taupada

Izar gorriak dir dir 
Gudarien bihotzkada 
Itziar, Monzon, Txikiaren 
Seme alabak gara