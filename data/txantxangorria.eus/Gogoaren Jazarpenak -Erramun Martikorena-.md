---
id: tx-3202
izenburua: Gogoaren Jazarpenak -Erramun Martikorena-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dYiPi6i5VOc
---

Zure garaiko muxuak
egunsentiko ihintza
zure bi ezpain goriak
bihotz taupaden bizitza
herri honen oinazea
jasangaitza ta bortitza
elgarrengandik urruti
jarri diguten baldintza
 
Askatasunaren muga
etsaien ziba likitsa
nire inguruan lau murru
eta bakardade hitsa
halere sines nezazu
gaur irabazten gabiltza
borrokan tinko izatea
da garaipenaren giltza
 
Udaberriko liliak
zabal eguerdi aldera
iguzkiaren beroa
murgilduz doa barnera
hamentxe ta han sentitzen naiz
lili horien antzera
ardurak jiten zitzaizkit
ondorapenak atzera
 
Ez dezagun inoiz etsi
joan gaitezen aurrera
gau beltz hunek ere noizbait
izanen du bukaera
hortan itzuliko gara
euskaldun denak batera
mundura sortu gintuen
herriaren magalera
 
Besarkada eztiño bat
gurasoentzat aurrera
miresgarria da zinez
beren maitasun gardena
nahiz ta bihotz zolatan
sartu dieten eztena
gogo berrien iturri
bilakatzen dute pena
 
Etsaien zapalketari
gogoaren jazarmena
Zein azkarra den maitea
zuengandik datorrena
horrela nola ez atxik
zutik ta tinko kemena
zuen amodioa da
elikatzen gaituena
 
Zein azkarra den maitea
zuengandik datorrena
horrela nola ez atxik
zutik ta tinko kemena
zuen amodioa da
elikatzen gaituena.