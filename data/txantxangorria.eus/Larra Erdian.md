---
id: tx-2987
izenburua: Larra Erdian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g8ax7BfqAeE
---

Larra erdian duzu bizia landatu,
goiko haizeek dute bereganatu.
Hutsean dabilenez indarrak ahuldu,
mendi zulo bat jo eta burua makurtu.
Zalantzarik gabeko etsipen usteldua,
neguko basoen biluts bakartia.
Arramaskaz jausi den marru larria,
zirrizkatu ezin duen, itokor, eguzki bortia.