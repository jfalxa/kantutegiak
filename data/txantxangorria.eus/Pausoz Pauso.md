---
id: tx-1053
izenburua: Pausoz Pauso
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2tIowO4ZHwU
---

Baldintzarik gogorrenetan
mendekuaren jopuntuan
neurr/i gabeko erasoekin
eskubideak urratuz
senidengandik urrunduz
mila kilometroa
herria zuekin beti egongo da!

Pausoz pauso! elkartasunean
pausoz pauso askatu!
pausoz pauso presoak etxera!
pausoz pauso askatu!

Eus/kal pre/so po/li/ti/ko/ek
e/te/nik ga/be/ko el/kar/ta/su/na
duin/ta/su/na/ren i/zar,
ar/na/sa e/ta bi/ho/tza
de/nok ba/te/ra el/kar/tuz
as/ka/ta/sun bi/de/an
pau/so/ak u/rra/tsak
a/me/tsak be/te/tzen!

Pau/soz pau/so! el/kar/ta/su/ne/an
pau/soz pau/so as/ka/tu!
pau/soz pau/so pre/so/ak e/txe/ra!
pau/soz pau/so as/ka/tu!

Bat, bi, hi/ru, lau,...
Es/ku/bi/de/ak, i/txa/ro/pe/nak,
sen/ti/men/du/ak, el/kar/ta/su/na
bo/rro/ka/re/kin gu/re/kin
e/txe/an i/zan ar/te
eus/kal pre/so de/nak
as/ka/ta/su/ne/an!

Pau/soz pau/so! el/kar/ta/su/ne/an
pau/soz pau/so as/ka/tu!
pau/soz pau/so pre/so/ak e/txe/ra!
pau/soz pau/so as/ka/tu!

Pau/soz pau/so! el/kar/ta/su/ne/an
pau/soz pau/so as/ka/tu!
pau/soz pau/so pre/so/ak e/txe/ra!
pau/soz pau/so as/ka/tu!