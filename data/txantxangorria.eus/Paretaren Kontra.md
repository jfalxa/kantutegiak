---
id: tx-919
izenburua: Paretaren Kontra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/znvGgOITA3o
---

Paretaren kontra
dutxa barnean itota
nire gainean igota
lurrean.
Zapaltzen ninduten
beti irainak entzuten
eta inork ez ninduen
uzten bakean.

Malkotan maiz
bakarrik naiz
lagunduko al nauzue?

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.

Izebak galdetzen zidan
zerbait ba ote zen nigan
ama ta aitak segidan
goizetan;
beti desberdin zen
beldurrez bizitzen nintzen
ez nituen konprenitzen benetan

Nahikoa da
gutxi gara...
lagunduko al nauzue?
Ez naiz gogorrena
baina ez naiz koldarrena
bakean uztea da
nahi nukeena.

Bakean aske,
aske izan arte.

Bakean aske,
aske izan arte.

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.

Paretaren kontra
dutxa barnean itota
nire gainean igota
lurrean.
Zapaltzen ninduten
beti irainak entzuten
eta inork ez ninduen
uzten bakean.

Malkotan maiz
bakarrik naiz
lagunduko al nauzue?

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.

Ni erasoen jomugan
ni gurasoen tristuran
eta ez dut egiz jarriko
berriz dudan
zenbat balio dudan.