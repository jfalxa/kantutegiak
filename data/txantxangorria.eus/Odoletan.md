---
id: tx-233
izenburua: Odoletan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vT5aEnwJSM4
---

Musika eta letra: HUNTZA
Grabaketa: Haritz Harreguy (Haritz Harreguy Studio)
Masterizazioa: Victor Garcia (Ultramarinos)

Bideoa:
Zuzendaritza: Arriguri
Ekoizpen Buruak: Liam McDonnell eta Ane Aldaya
Argazki Zuzendaritza: Nestor Urbieta
Gaffer: Ekhi Alde
Arte Zuzendaritza eta Jantziteria: Claudia Chocarro
Arte eta Jantziteri laguntzailea: Irati Peluaga
Kolorea: Heli Suarez
MUAH+FX: Santi Goicoechea
MUAH+FX laguntzailea: Indrani Barcelo
Soinu diseinua: Asier Renteria


Zauriak zauri, abestu dezagun minez;
Isiltzen dena, beti apurtzen erraz...
Baina lagunak, beti ulertuko zaitu;
Hastera goaz, (e)ta arnasa luze hartu!
Kontuan hartu, ez bazara konturatu:
Iluna dela, ikusten ez den guztia.
Eta bi alde, bi aldeetan gaude gu;
Batzuetan zerua gara eta askotan infernu.
Bidegurutze batean goaz aurrera,
usteak uste, adorez batera.
Bidegurutze batean goaz aurrera,
ustelak ustel, helduko al gera?
Odoletan Daukazuz azala,
Harri artean erori al zara?
Ezin naiz bizi lekzioak entzuteko,
Neure burua zaintzen zuk erakusteko.
Ezin naiz bizi lekzioak entzuteko,
Pikuak biltzen ez al zaudete hobeto?
Odoletan Daukazuz azala,
Harri artean erori al zara?