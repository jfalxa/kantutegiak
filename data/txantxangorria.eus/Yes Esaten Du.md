---
id: tx-2897
izenburua: Yes Esaten Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L_r6nmQHY3M
---

Lagun xelebre eta jatorra da
Liverpoolgo Marilin
Bisitan joaten natzaionean
gogoz beti elkarrekin
Une alai triste edo zailetan
edonondik galdera.
Eta irribartsu ezpainetatik
beti erantzun bera

Yes esaten du
Yes ematen du
Yes laguntzen du
Yes beti Yes very well!

Lagun xelebre eta jatorra da
Parisko Marie Marti.
Bisitan joaten natzaionean
gogoz beti elkarrekin
Une alai triste edo zailetan
edonondik galdera
Eta irribartsu ezpainetatik
Beti erantzun bera

Oui esaten du
Oui ematen du
Oui laguntzen du
Oui beti Oui Oui très bien!

Lagun xelebre eta jatorra da
Sevillako Jesulín
Bisitan joaten natzaionean
gogoz beti elkarrekin
Une alai triste edo zailetan
edonondik galdera
Eta irribartsu ezpainetatik
Beti erantzun bera

Sí esaten du
Sí ematen du
Sí laguntzen du
Sí Sí Sí Sí Sí Sofí!

Sí esaten du
Sí ematen du
Sí laguntzen du
Sí Sí Sí Sí Sí Sofí!

Zer dio Porrotx? Que Sí!