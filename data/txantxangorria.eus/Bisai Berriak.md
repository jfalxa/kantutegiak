---
id: tx-2093
izenburua: Bisai Berriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wHHliRewuuw
---

Mendeetan barrena gordea
Harri bat bihotzean
Lan eginez herri bihurtua
Taupada bakoitzean

Elorrixon zu ta ni 
Bi indar, bi argi
Iraganeko bi zubi
Ta geroaren irudi

Elorrixon zu ta ni
Bi indar ta bi argi
Zeru zati honetan
Izarra bateginik

Bisai berriak ditu orainak
Segitzeko ustean
Iraultza baten zainak
Kolore gorriz
Haizeratzen hastean

Elorrixon zu ta ni
Bi indar, bi argi
Iraganeko bi zubi 
Ta geroaren irudi

Elorrixon zu ta ni
Bi indar ta bi argi
Zeru zati honetan 
Izarra bateginik

Asmo berri berritzaileen
Lema gidatzeko asmoz
Asmo berri berritzaileei 
Berriz ekiteko gogoz

Elorrixon zu ta ni 
Bi indar, bi argi
Iraganeko zubi
Ta geroaren irudi

Elorrixon zu ta ni
Bi indar ta argi
Zeru zati honetan
Izarra bateginik

Zeru zati honetan bateginik
Infernu zati honetan bateginik