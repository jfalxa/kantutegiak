---
id: tx-1071
izenburua: Ezin Ahaztu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/K2uDSVEru70
---

Goizetik arratsera 
zu zaitut gogoan. 
Zuri begira beti 
naiz egonik lotan. 
Euskalerriko mendi 
itsaso bazterrak 
politak izan arren 
zu ederragoa. 
Badet bihotzean 
maite lilugarri. 
Zugandik ezin banaiz 
urrutian bizi. 
Atoz ene maite, 
atoz bai nerekin, 
nigarra begietan 
zu ezin ikusi. 
Agur ama nerea, 
ez zaitut ez aztu 
agur ama nerea 
agur, agur, agur.