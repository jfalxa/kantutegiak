---
id: tx-2677
izenburua: Enuen Uste Hala Izango Zenik -Passenger   Let Her Go-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ceC0b_b8rSY
---

Enuen uste hala izango zenik 
bazela horrelako minik 
Altxako ginela distantziaren kolpetik 
ta somatzen zaitut gau urrunean 
hego berriak bizkarrean 
denborak bizipoza lapurtu dit 
gorde naiz mendi atzean 

Gure argazkiei begira atzeman dut irri segida 
baina bizitzaren hesteetan galdu dira 
Argirik ezin ikusi gertu saiatu naiz nola ulertu 
horrenbeste gauza nahi nizkizuke galdetu 

Zalantzak bildu dira barruan 
tristura azaltzeko orduan 
al naiz berriz gogotsu munduan 
horrenbeste eraiki elkarlanean 
dena baztertu bat-batean 
bidailagun ona izan ote naiz iraun artean 

Gogorra da dena onartzea 
damuz josita arnastea 
izan ez direnen min gordin horri eustea 
malkoz opa dizut onena 
arren ez ahaztu izan ginena 
ilusio berriz hartu beharko datorrena 

Enuen... 

Berriz sentitu nahi dut bizirik 
arren grisa ez den egunik 
gure zena nola ihesi doan 
behatz artetik 
ta marraztu naiz gaur urrunean 
beldur beltzenen gailurrean 
nuen dena eman nizun ta orain gorde naiz 
mendi atzean 

Enuen...