---
id: tx-37
izenburua: Euskal Herritik Aparte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z7yXPUHHzTw
---

EUSKAL HERRITIK APARTE

        Xabier Amuriza , 1970


Euskal Herritik aparte
mila legua bitarte
esku ta hanka loturik preso Zamoran gauzkate.
Pentsaturik zenbat urte
behar dogun hemen bete
pazientzia hartu beharko libre utzi arte.
 
Nahiz 'ta kriminal ez izan
kaiola-txorien gisan
bost gizon handi kolpera preso sartu ginduezan.
Baina zergatik izan zan
nahi nuke poliki esan
inozentzian bizi direnak itzartu daitezan.
 

Maiatzeko egun batez
hogeitamar hain suertez
gose greba bat hasi genduan geure borondatez.
Goitik laguntzarik batez
guztia zan geure kaltez
obispo ta apaiz golko-haundiak lasai bizi beitez. 

Pentsaturiko moduan
Bilboko obispaduan
sotanu baten gela zahartxo bat atondu genduan.
Poliziak inguruan
jarri jakuzan behinguan
gure sabelak ez zun goserik sentitzen orduan. 

Paper bat geunkan egina
laburra baina gordina
bertan azaltzen genduan Euskal Herriko samina.
Zer sermoi 'ta zer dotrina
bazan zerbaiten premina
barriro ere egingo geunke kanpoan bagina. 

Hiru egun 'ta erdira
lotu 'ta komisarira
ardi gaxoak bezala goaz geure hiltegira.
Jotzen hasiko balira
bizkarrik ez erretira
lehenago ere emondakoak danak hartu dira.


Geroztik astebetera
tribunal faltso batera
azaldu ginen gure krimenen kontu emotera.
Mila arrazoi atera
ebezan gure kaltera
karga ederra debalde jaso genduan kolpera. 

Tribunaleko nausia
trumoi artetik jausia
gizona ala piztia zer zan dudan gagoz ia.
Eztula ta aharrausia
haren zientzi guzia
esku onetan dago mutilak gure justizia.

 Ezker eta eskubira
ha zan ezpata dirdira
pentsa zeiteken sartu ginela sugeen habira.
Ikaraz nengon begira
bost juezen aurpegira
akerrak ere haien ondoan pizti finak dira.


Juizio xelebre hartan
poz bat nuen neure baitan
halako pizti saila debalde ikustea bertan.
Baina gauza baten faltan
geratu nintzen penatan
buztanik zeuken begiratzea ahaztu egin jatan.

 Aitortu nahi dot hortako
abogaduak ainako
saio bikainik San Pedrok ere ez zula botako.
Baina guztia zertako
alferrik hitz baterako
hizketan jardun nahiz zaunka egin bardin zan harako.

 Espainia haundia 'ta
librea zelan ez kanta
Gibraltar izan ezean hemen ez da ezer falta.
Askatasunez aparta
handian barriz galanta
honelakoak bat izan behar besterik ez da-ta.

 Agur eginik Burgosi
nahiz 'ta ez asko merezi
bueltan gentozen Criado jauna gendula nagusi.
Bakoitzak zazpi polizi
heuren pistola 'ta guzi
bideak libre zitun ihesi nahi zuenak hasi.

 Heldu ginen Zamorara
ifernu zulo hontara
Jainkorik bada dagon lekutik gaizala anpara.
Urte batzuren bolada
gu hemen izango gara
bien bitarte Euskal Herria askatzen ez bada.

 Pilatos eta Herodes
izan dira gure juez
Anas ta Kaifas ere oraingoz kontsola daitekez
Hala ere gure partez
lasai gagoz eta bakez
askatasuna kendu dauskue baina umorerik ez.