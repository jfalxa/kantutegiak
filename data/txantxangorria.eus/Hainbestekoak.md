---
id: tx-2570
izenburua: Hainbestekoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9nVZnBtw1_U
---

Paititi diskako "Hainbestekoak" abestia akustiko formatuan zuzenean. Bideoa EITB-ko Miramoneko estudioetan grabatua izan da Julen Idigorasen eskutik.

Email: hirigalduak@gmail.com


o/rain da/mu/tze/ra/ko/an!
A/ka/tsak!