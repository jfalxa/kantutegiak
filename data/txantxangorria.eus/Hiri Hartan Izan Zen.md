---
id: tx-2039
izenburua: Hiri Hartan Izan Zen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0jiGzKidRuA
---

Hiri hartan izan zen
ia maiteminez
lokartzen ginenean
esnez, atseginez
gose aseezinez
hordirik jakin-minez
heriotzaren hatsa
antzeman ezinez

Kalean ganibetak
tabernan babesa
ilargi eta argi
errima traketsa
esku baten zerbeza
etxerako pereza
amildegiratzea 
zer gauza erreza

Zure irribarrea
lore basamortu
gudu eremu zabal
izarazko ortu
zure haserrea sortu
ta barkamena lortu
berriz zure azpian
nazazun zigortu

Nik ere maite zaitut
lehenengo aipuan
larru dardarak datoz
ontasun prestuan
atseginen estuan
lamiak apustuan
hankak zabalik eta
bihotza eskuan