---
id: tx-1064
izenburua: Salbatore
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AdVa_Wq8TFs
---

l.- Salbatore gora da Garazi aldian:
Ni ere han nunduzun igaran astian,
Debozone gabe senthoralian,
Ene gazte lagunak han beitziradian.

II.- Bortietan artzain, eta ez jaisten ardirik,
Ountsa jan, edan, eta egin lo zabalik,
Mundian ez ahalda ni bezañ irousik.
Enuke segur nahi bizitze hoberik.

III.- Izarbat jeikiten da goizerri aldeti,
Argi eder batetan, leñhuru bateki,
Erien sendotzeko photeriareki;
hounki jin egin diat nik hari segurki.

IV.- Izar houra jiten da boztarioreki,
Zelialat eroanen naiala bereki;
Hitzaman diriozut nik hari segurki,
haren zerbutchari nizatila bethi.

V.- Amorio zaharra behar hait kitatu,
Hanitch phena dereitak hik eni kaosatu.
Maite berribat zitak ezpiritian sarthu
Hari behar deroat bihotza libratu.

VI.- -Amorio zaharrak zutia jenatzen,
Berribaten jitiak hanitch agradatzen?
Zu ere gaztettorik hasi zinen maithatzen:
Hoberik duzunian orai naizu kitatzen.

VII.- Zounbat aldiz nik eztut egin nigarrez uthurri,
Zu zinadiala kaosa amak eraginik,
Arrazou ere baziala sobera badakit,
Zeren zutzaz benintzan charmaturik bethi.

VIII.-Kitatzeko sujeta othe zer ahalden,
Ahal bezain etsatoki ari nuzu phentsatzen:
Ene buria deusetzaz ere eztit akusatzen
Inozent nuzu; eta joan zite arren."