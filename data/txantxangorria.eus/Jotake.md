---
id: tx-2080
izenburua: Jotake
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3QoaG4NyyUA
---

Nik ez diat inoiz esango 
hire egin beharra zein den nik aspaldi eginela aukera...
JOTAKE IRABAZI ARTE

jaio ginen bai 
guda baten erdian
urteak pasa ta berdinean gaude
ez esan inoiz
hua bukatua dago
ez inoiz esan bidea amaitu da 

nik ez diat inoiz esango
hire egin beharra zein den
entzun jendearen ahotsa
JOTAKE IRABAZI ARTE

SU TA GAR gogoan gelditu barik jotake gogor etsaiari
SU TA GAR hemen gaude gau ta egunez 
jotake gogor etsaiari

jaio nintzen bai
guda baten erdian eta amorrua izan dut gurasotzat 
iritsiko da 
herriaren ordua
egindakoa ordaindu beharko dute

buru barruan .... tanten moduan 
eta amorrua izan dut gurasotzat
ezin konpondu hainbat eta hainbat kale
egindakoa ordaindu beharko dute

nik ez diat inoiz esango
hire egin beharra zein den
nik aspaldi egin nuen aukera
JOTAKE IRABAZI ARTE


SU TA GAR gogoan gelditu barik jotake gogor etsaiari
SU TA GAR hemen gaude gau ta egunez 
jotake gogor etsaiari

JO TA KE 
IRABAZI ARTE 
JO TA KE 
IRABAZI ARTE