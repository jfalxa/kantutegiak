---
id: tx-2363
izenburua: Don Boogie On
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w7JAE-Gz9XU
---

SKABIDEAN, BIDEAK DARRAI 2018

Bideoa/Video: Sergio Elia, www.wefeelm.com
Kontaktua/Contacto: 610887684 Nikolas
www.facebook.com/skabidean
@SkabideanTaldea


LETRA (EUS) :

Don don boogie on,
Mugi boogie gin,
Don don boogie, mugi grin,
Don don boogie on.
(euskara-ingelera hitz-jokoa)

Argi ditugu gure influentziak,
musikan aurrera bultzatu gaituztenak,
ez itxura
ezta moda,
ez arropa
ezta pop-a,
ez patxangeoa.

Irlako kultura ez da ezaguna 
horregatik zabaltzen dugu eta,
kaskamotz ta,
musikari,
rasta, mod ta, 
esatari, 
segi beti dantzan!

Gure jatorria ahaztu gabe,
nahiago dugu 
aditzen zena lehen:
Paragons,
Buster, the Maytals,
Etiopians,
Desmond dekker
edo Laurel Aitken.

Gutxiengoa beti izan garen arren, 
zatoz gurekin, indartu gaitezen, 
gure musika 
sustatuz,
gure kultura 
hedatuz,
berriz piztearren!