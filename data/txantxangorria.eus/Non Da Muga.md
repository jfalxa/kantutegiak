---
id: tx-356
izenburua: Non Da Muga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UjNSRV6Uu7s
---

Non da muga?
Non da muga?
Non da muga?
Non da muga?

Itsas ilunean txalupak
zeru magalean izarrak
eta biak beltz, eta biak beltz,
biak beltz, biak beltz,
zerua eta itsasoa
eta biak beltz, biak beltz

Non da muga?
Non da muga?

Eta biak argi, eta biak argi
txalupak eta izarrak
eta dena bat, eta dena bi

Non da muga?
Non da muga?
Non da muga?
Non da muga?
Non da muga?

Bustia eta lehorraren artekoa
beharra eta nahiaren artekoa
naizena eta zarenaren artekoa

Non da muga?
Non da muga?

Orain ilun dago
eta ezin bereizi
egunak argitu arte

Orain ilun dago 
eta ezin bereizi
egunak argitu arte
eguzkia irten arte, irten arte,
eguzkia irten arte.