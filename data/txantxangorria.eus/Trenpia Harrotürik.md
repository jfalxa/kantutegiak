---
id: tx-95
izenburua: Trenpia Harrotürik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RexkJdHmtDc
---

Trenpia harrotürik 
Heben girade adixkidik, apaidü on bat eginik, Trenpia harro-harrotürik, kantatzen dügü bihotzetik. Ahaide xaharrik goxuenak goratik kanta ditzagün, Bai et’ahaide berrienak, algarrekin, harro ditzagün! 
Ahaide bat ederra kantatüz gora, Denak hebentxe gira, oso alagera!
Oi! Botz honaren ükeitea ez da aski kantatzeko, 
Hitzak direnin ikasirik hobe da behazalentako. 
Kantore haitü egileak izan dira Xiberoan: 
TOPET, LIGUEIX, Pierra ETXAHUN, ATTULI ‘ta besterik bidean. 
Jünta zitaie gurekila, kantatzeko denek gora, 
Eskü-zartaz harro bihotza gozatzeko beroki besta. 
Batek dezagün eman gora, bestek eztiki apala, 
Euskal jentea hola beita, boztarioan algarrekila. 
Hitzak: Dominika ETCHART 
Müsika: Jean ETCHART