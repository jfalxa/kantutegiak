---
id: tx-726
izenburua: Jada Ez Zauden Lekuan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2fD8tRk1uXI
---

Isiltasunak harrapatzen nau taberna-gau haiek gogoratzen ditudanean.
Itxaropena besarkatuta. Gerorik ez balitz bezala. 
Musika gure arima suspertzen.

Herrian, ilunpetan. Non guztia loratzen den. 
Zapore gazi-gozoan. Lagun artean. Jada ez zauden lekuan.

Maitasun ezkutuen beroan. Non haurtzaroak besarkatutako 
adiskidetasunak betikoak baitira.

Herrian, ilunpetan. Non guztia loratzen den. 
Zapore gazi-gozoan. Lagun artean. Jada ez zauden lekuan.

Melodiaren erritmoan oinarrituz. Bizitakoen bideak dirau. 
Belaunaldi berriek leku egiten. 
Zu eta biok orain arrotzak gara.