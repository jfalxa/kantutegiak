---
id: tx-2999
izenburua: Orain
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Xhk2h4TAiXI
---

Itxaropena helmugara 
heldu da ihesaren trena 
Iragan beltza atzean
Kolpeak, ubeldurak, etengabeko irainak.

Erabakia hartua da
Bazoaz askatasunaren bila

Orain ilunpean pizti horren zorigaitza sortzen.
Orain lotutako kateak askatasunez apurtzen.

Lehertuta ibili zara azken urtetan,
Dudan jarriz bizitzaren sentzua
Erakutsi duzu zure nortasuna,
Behingoz lortu zulotik irtetea.

Orain ilunpean pizti horren zorigaitza sortzen.
Orain lotutako kateak askatasunez apurtzen.

Orain ilunpean pizti horren zorigaitza sortzen.
Orain lotutako kateak askatasunez apurtzen.