---
id: tx-1412
izenburua: Hitz Debekatuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kyM8XrP4GPM
---

Hitz debekatuak
ahoaren barruan gelditzen dira
nor zaren esateko
beldurrez.
Ispilu aurrean
biluzten zarenean eta ez duzu
ikusten duzun hori izan nahi.

Bakarra zarela
ez zalantzan jarri inoiz,
lortuko duzula sinistu.

Esaiozu barru horri
bizigai eder bat dela,
besteen pareko,
ez menpeko.
Esaiozu bihotzari
itsasoaren olatu dela,
indartzen handitzen ari dena.