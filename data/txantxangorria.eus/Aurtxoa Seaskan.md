---
id: tx-876
izenburua: Aurtxoa Seaskan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FgGGIMvcJno
---

Aurtxo polita seaskan dago,
zapi zuritan txit bero.
Aurtxo polita seaskan dago,
zapi zuritan txit bero.
 
Amonak dio, ene potxolo,
arren egin ba, lo, lo.
Amonak dio, ene potxolo,
arren egin ba, lo, lo.