---
id: tx-2653
izenburua: Argi Zirrinta Zenian Baiona Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NIV0hn_MRhA
---

Ai, ai, ai neskatxa
Goizeko lanbrotsan
Zure soak zauztan
Goi-izarrak eman
Barnea dirdiran
Bai eta ikaran
Jarri nintzan bertan
Zure ondo hartan.

Argi zirrinta zenian, kalian
Donibaneko hirian
Treina hartzerat nindoan,
goguan
Pena nuela alan
Nexkatxaño bat bidian,
abian
Ikusi nuenian
Haren zama nik hartzian,
eskian
Hundik nuen herian..
. 
Baionarako zirare, ni ere
Baditut hogoi urte...
Uztailez ditut nik bete,
zuk ote
Baduzuia hoinbeste ?„
Zer adin dutan jakiteko
zer dikek
Hiretako egite.
Zuzenago litzakidek,
entzute,
Bai alez naukan maite..

Zure ta ene ixtorio,
maiteño
Izan zen holaxeño...
Ezkondu ta ondoko,
urteko
Baginuen mimiño
Geroztik orai artino,
zazpiño,
Jin zaizkigu oraino,
Bainan dolurik hortako
nehoizno,
Etzaut jin gogoraino