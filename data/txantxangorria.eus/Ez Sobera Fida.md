---
id: tx-2372
izenburua: Ez Sobera Fida
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eN1CCkuHPS4
---

#saveyourinternet #Artículo13

Oi ez sobera fida, jende faltsuari
harek düan ele ezti, erre eijerrari
besoak zabalik egin, batzarri ederrari,
gibel üngüratü ordüko, ostikoz da ari.

Mündüan ikusten dügü, eskü emaile ederrik,
adixkide sinoa, hori da segürik,
bena faltsükerian esküa ükenik,
zonbat aldiz ez gira, hanitx tronpatürik.

Oraiko denboretan, zonbat tristezia,
mündüan ikusten dügün, harrigarri beita,
gerla eta tzarkeria, bai injustizia,
faltsütarzünak beitü, dener pizten herra.

Gure artean ez dadin, izan holakorik,
galtatzen düt ümilki, bihotzan erditik,
leialtarzünaren xenda; oi! Jarraiki errotik
irus bizi gitüan, euskaldünak betik.