---
id: tx-2287
izenburua: Loezina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y-3mH5_cR88
---

Abestia / Tema: Loezina
Albuma / Album: Izaera (2020)
Musika eta letra / Música y letra: Skabidean
Musikaren grabazioa, nahasketa eta masterizazioa / Grabación, mezcla y masterización de la música: Kaki Arkarazo (Garate Studios, Andoain).
Bideoa / Video: XAM, Luzía Vegas eta Cuto 
Eskerrik anitz Julen Camposi (punki aritzeagatik) eta Idoiaren amonari (etxea errazteagatik).

Taldearen kontaktua / Contacto del grupo: 610887684
Posta elektronikoa / Correo electrónico: skabidean.blog@gmail.com


LETRA [EUS]
Margolan makurra bailitzan, sabaian dilindan,
nago beharturik ibiltzera gauero museoan.
Nahiz eta bihurtu alde batera edo bertzera,
ezin egin ihesi, ohartu beharra daukat preso naizela.
Iluntasunean hasperena, pikutara guztia,
ea zer dioen hiriak, har dezagun airea.

Etxeko atariaren danbadaz
haizearen hotzaren ukabilkada,
zirimiri leunaren errezela,
ilberri den gauaren islada...

Gizon kopetiluna, pantailari begira,
txakur zuri txikiak egiten dio tira,
marrazten du arkupean fatxadako farolak
lasai erretzen ari den lagun baten ingerada.
Espaloian jarririk, izkinaren bueltan,
punki gandor gorri baten marmar ulergaitza,
putzu zikin baten gainean dago dantzan,
alkohol usaina darion mozkor erotu bat.
 
Ordu txikitako pentsamendu sarkorrek
loezinean murgiltzen naute,
amesgaiztoen lapurrak dira 
atzarririk daudenen horiek hain zuzen.

Deabruen artean naiz aspalditik,
eta azkenean egiten da ikasi
norbera dela bere suen iturri,
deabrurik gabe ez dela infernurik!

Argi hori margul honek nazkatu nau jada,
auzotik at paratzen naiz itsaso beltzean soa,
ohartzeko hura dela benetan zabala,
nire kezka zirta bat soilik izartegi osoan.
Lasaiturik bueltan noa espaloi artera,
dagoeneko ez zait arrotz etxerako bidea, 
deabruak uxatzeko heldu dira
karriketan sartzen diren suzko argi-izpiak!

Ordu txikitako pentsamendu sarkorrek,
loezinean murgiltzen naute,
amesgaiztoen lapurrak dira
atzarririk daudenen horiek hain zuzen.

Baina lau haizeek dezatela jakin,
nire kezkek jabe bakarra naute ni.
Preso haiek direla dut erabaki,
ni naiz haien mundua, ez alderantziz!