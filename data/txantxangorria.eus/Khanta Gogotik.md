---
id: tx-395
izenburua: Khanta Gogotik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fKv56iZPdmk
---

eglise d'ordiarp .jean etchegoyen, beñat queheille, jean philippe prat, jean etchart, dominique epherre , pierre jaragoyhen, jean louis bergara


Khantore bat benian nik ,
Ikhasi ttipi ttipitik,
Khantatzen nian goratik
Argizagiak zelutik. 
Xiberotarrek üsantx’ederrak
Herrian sano begira
Khanta goratik eman azpitik
Koblak untsa ikhasirik.
Bazter orotan bizi gira, bakotxa ahal bezala,
Bena ororen plazera,
Khantan eskuaraz aitzia
Ikhasi dut nik eskuara,
Aitak eta amak bezala,
Etxen mintzatu behar da,
Eskola hoberik ezpeita.
Denek eskuaraz khanta,
Hartuz usu xarramela.
Xiberotar ta Manexa,
Hortan badugu haitia.