---
id: tx-972
izenburua: Aide Korrika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7p2Z_S1Vfn8
---

Aide Korrika!
Aide Korrika!
Hala zankoa!
Aide korrika!
Arraiopola
Aide korrika!
Dida korrika!
Aide korrika!
Bejondeigula

Ezarle, karrerista, lasai pote geldoa eta
Esprinterrak,
Futinero erdi ero, errlebozaleak eta hanka okerrak,
Euskararen bizitzaren geroaren atzetik
Hau da gure maratoia.
Euskararen bizitzaren geroaren atzetik
Hau da gure arrazoia.
Aide korrika!...

Elorrixon añiketan, Donibanen lasterka,
Oiñatin atxitxika, arrasaten takarrara,
Itsas ertzean berriz, saltaka ta korrika.
Datorrena datorrela, hemen gehiena dago
Gure esku, gure hanka.
Dinbi-danba, plisti-plasta, riki-raka,
Tiki-taka, kinki-kanka, kinki-kanka.

Aide korrika!...
Dinbi-danba, plisti-plasta, riki-raka,
Tiki-taka, kinki-kanka, kinki-kanka.
Datorrena datorrela, hemen gehiena dago
Gure esku, gure hanka.