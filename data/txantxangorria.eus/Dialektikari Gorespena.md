---
id: tx-305
izenburua: Dialektikari Gorespena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d45nxvy4pnU
---

Pauso sendoz, doa munduan injustizia.

Zapaltaileak, beste hamar mila urtezPortada (El pueblo no perdonara)

domeinatzera prestatzen dira.

Heien biolentziak seguratzen du:

“Guztiak berdin jarraituko du”.

Zapaltailean aboza entzuten da soilik,

eta merkatuan esplotazioa ohiuztatzen da:

“Orain da nere hasiera”.

Eta zapalduen artean, asko diote:

“ezta inoiz nahi duguna erdietsiko”.

Oraindik bizirik, dagoenak

ez dezala “sekulan” esan.

SENDOA EZTA SENDO EZ DU GUZTIAK

BERDIN JARRAITUKO

Agintzen dutenak, mintza daitezenean,

domeinatuko mintzatuko gara!

Zein ausart daiteke, “sekula”, esatera?

Zeinen eskutan dago opresioaren segi da?

GUREETAN!

Zeinetean haren amaiera?

GUREETAN ERE!

Altxa dadila makurtua dagoena!

Burruka dadila galdurik dagoena!

Zeinen menperatuko du bere kondizioa ezagutzen duena.

ZEREN GAURKO GARAITUAK, BIHARKO GARAILEAR

BAITIRA

ETA “SEKULAN” “GAUR BERTAN” BAITA.