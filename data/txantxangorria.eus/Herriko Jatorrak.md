---
id: tx-73
izenburua: Herriko Jatorrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cdh3-1CzJx0
---

"Tautalak binan bina
Eskuak patrikan
Txatoak, besamotxak
Kojuak korrikan...
Altxola, Golindo,
Xuaxola, Pantolingo,
Beltxor eta Garro,
Fafai, Kallebarro.

Txiliku, kaskatxuri,
Trumoia, Tximista,
Kukulu, Juli-Juli,
Bilbo eta Erreusta.
Katxina, Ziotza,
Pikatza ta Makatza,
Kiriki, Pikante,
Ttantton eta Tente.

Iñurritzan Ispilla
Sanpardon Kirkilla.
Oillua ta Xagua,
Lagarto, Mustua.
Arrantzan Muxarra,
Txofertzan Joxe Mantzo,
Pelotan Xaporra,
musikan Barrentzo.

Etxebeltz ta Antonbeltz,
Zalbide, Zaldubi,
Paskuelita, Paskueltxo,
Pipas, Astazubi
Amoña, Okontxo
Amaska ta Etxetxo
Komelio, Asentxio,
Salbardin, Primotxo.

Pipekale, Santana
Xiano, Meana
Txabillo ta Txandarra
Kapaxko, Karrama
Baserri, Saberri
Kintxe eta Lanberri
Ai ene! Alleme
Aupa Patxiseme!"