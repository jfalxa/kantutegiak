---
id: tx-446
izenburua: Ez Dakit Zer Ordu Izango Den
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2pnay1OWBgY
---

Ez dakit zer ordu izango zen (G. Aleman - Txomin Artola)
Zubitoa 2. trastean

G  320003
D  xx0232
C  x32010
Em 022000
Am x02210
D7 xx0212



G               D
Ez dakit zer ordu izango zen
C                  G
ez bainaiz gogoratzen;
               D
ez dakit noren eskuek
       C          G
atera ninduten nire
                D
lehenengo kabi berotik,
C               G
esku inpertsonalak,
     D       C    G
trebeak, ohituak.


Em            Am         D7
Ezin esango nolakoa izan zen
            G         Em
botatako lehen garrasia
         Am       C     D    G 
gogorra edota apala, agian.



G D C G (x2)


                 D
Beste denok bezala
              C
jaio nintzen mundura,
 G               D
beste denoi bezala,
 C              G
belarri barnetatik

irristatzen
          D      C
zitzaizkidan hitzak
G            D        C
egituratu ziren garunean
         G        D
ikasi zuten ibiltzen
        C         G
mingainean zehar.


Em              Am
Beste denok bezala
         D7
erori-altxa
              G
erori eta altxa,
Em           Am        D7
erori eta altxa egin nuen 
                G
behin eta berriro 
 D            C           G
zangoak indartu zireneraino.