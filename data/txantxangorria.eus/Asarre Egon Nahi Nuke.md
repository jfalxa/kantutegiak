---
id: tx-1314
izenburua: Asarre Egon Nahi Nuke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eMGT5b4ZagE
---

Asarre egon nahi nuke
bihotza betean 
oraindikan gaztea
naizen bitartean
nik sinisten ditudan
egia trebeak
biribilka ditezen
erdiko kalean. 

Ez dakit gauza askorik
ez nago seguru,
bizitzeko baditut
oraindik bost ordu.
Hiru pasa nituen 
bideak bilatzen 
eta beste hainbeste 
lehengoak urratzen. 

Nundik nora nijoan
ez da gauza argia,
bainan aserre nago,
hori da egia.
Eta egon nahi nuke 
bihotza betean 
oraindikan gaztea 
naizen bitartean.