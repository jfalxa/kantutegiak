---
id: tx-1014
izenburua: Lasai, Lasai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/J7kqrGB6j28
---

Zuzendaritza: Mikel Alberdi 
Edizioa: Julen Idigoras
Kameralariak: Ibai Zangitu, Jon Ander Alberdi, Mikel Serrato eta Mikel Alberdi
Zuzendari fotografikoa: Ekaitz Zilarmendi
Produkzioa: Lara Diez
Aktoreak: Bakene, Maria, Amaia eta Iñigo Antsorregi

Esker bereziak Zubietako Hipodromoari eta herriari, Xabiri eta nola ez, hipodromora inguratu zineten eta hau guztia posible egin duzuen guztioi! Lan hau zuentzat da, eskerrikasko!

LETRA (Euskera)

Noizbait, iratxo bat 
sorbalda gainean eseri zitzaidan ta
belarrira esan zidan:
ez det bi aldiz pentsatzerik nahi!
Bihotzetik bihotzera
esan zidan:
batera aspertuko gera
lasai, lasai...

Ze…

SARRITAN GALDETZEN DIOT 
NERE BURUARI
HEMEN EZ OTE DABILEN 
DENA AZKARREGI.
SARRITAN GALDETZEN DIOT
NERE BURUARI
HEMEN EZ OTE DABILEN…

Presa arerio,
minutuak momentu bilakatu eta
zuhaitzak dantzan ikusi,
dauden alfonbra ederrenetan.
Erlojuak ezkutatuz
zelaietan,
batera aspertuko gera
lasai, lasai...

Ai… Ze…

SARRITAN GALDETZEN DIOT 
NERE BURUARI
HEMEN EZ OTE DABILEN 
DENA AZKARREGI.
SARRITAN GALDETZEN DIOT
NERE BURUARI
HEMEN EZ OTE DABILEN…

SARRITAN GALDETZEN DIOT 
NERE BURUARI
HEMEN EZ OTE DABILEN 
DENA AZKARREGI.
SARRITAN GALDETZEN DIOT
NERE BURUARI
HEMEN EZ OTE DABILEN…

LETRA (CAST)

CALMA, CALMA

En algún momento, un duende
se sentó en mi hombro y
me dijo al oído:
¡No quiero pensar dos veces!
De corazón a corazón
me dijo:
nos aburriremos juntos
con calma, con calma

A MENUDO ME PREGUNTO
SI NO VA TODO DEMASIADO DEPRISA
A MENUDO ME PREGUNTO
SI NO VA TODO DEMASIADO DEPRISA

La prisa es nuestra enemiga,
convertir los minutos en momentos y
ver los árboles bailar,
en las alfombras más bonitas.
Escondiendo los relojes
en los prados,
nos aburriremos juntos
con calma, con calma