---
id: tx-1997
izenburua: Campanades A Morts
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7rcT6oODQOE
---

Campanades a morts 
fan un crit per la guerra 
dels tres fills que han perdut 
les tres campanes negres. 

I el poble es recull 
quan el lament s'acosta, 
ja són tres penes més 
que hem de dur a la memòria. 

Campanades a morts 
per les tres boques closes, 
ai d'aquell trobador 
que oblidés les tres notes! 

Qui ha tallat tot l'alè 
d'aquests cossos tan joves, 
sense cap més tresor 
que la raó dels que ploren? 

Assassins de raons, de vides, 
que mai no tingueu repòs en cap dels vostres dies 
i que en la mort us persegueixin les nostres memòries. 

Campanades a morts 
fan un crit per la guerra 
dels tres fills que han perdut 
les tres campanes negres. 

II 

Obriu-me el ventre 
pel seu repòs, 
dels meus jardins 
porteu les millors flors. 

Per aquests homes 
caveu-me fons, 
i en el meu cos 
hi graveu el seu nom. 

Que cap oratge 
desvetllí el son 
d'aquells que han mort 
sense tenir el cap cot. 


III 

Disset anys només 
i tu tan vell; 
gelós de la llum dels seus ulls, 
has volgut tancar ses parpelles, 
però no podràs, que tots guardem aquesta llum 
i els nostres ulls seran llampecs per als teus vespres. 

Disset anys només 
i tu tan vell; 
envejós de tan jove bellesa, 
has volgut esquinçar els seus membres, 
però no podràs, que del seu cos tenim record 
i cada nit aprendrem a estimar-lo. 

Disset anys només 
i tu tan vell; 
impotent per l'amor que ell tenia, 
li has donat la mort per companya, 
però no podràs, que per allò que ell va estimar, 
el nostres cos sempre estarà en primavera. 

Disset anys només 
i tu tan vell; 
envejós de tan jove bellesa, 
has volgut esquinçar els seus membres, 
però no podràs, que tots guardem aquesta llum 
i els nostres ulls seran llampecs per als teus vespres. 

IV 

La misèria esdevingué poeta 
i escrigué en els camps 
en forma de trinxeres, 
i els homes anaren cap a elles. 
Cadascú fou un mot 
del victoriós poema.