---
id: tx-2042
izenburua: Zaindu Maite Duzun Hori
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9L8sDIuwkyg
---

Iritzi helduak, uste haundikoak,
Gure herriaz. 
Goiz eta arrats berriketan
Gure herriaz.
Nik ere nahi nituzke
Halako segurtasunak eduki.
Baina, gauzak zer diren,
kontuak ez zaizkit ateratzen ongi. 
Hori suertea
Nonnahikoak diren hoiena! 
Sasi guztien gainetikan 
Dabiltza hegan.
Inork ez zidan esan
Euskaldun izatea zein nekeza den,
Hobe nuela hautazea
Munduko hiritar izatea. 

Kantu leunak 
Nahi nituzke jarri. 
Eguzkia
Ainubian denari. 
Zaindu maite duzun hori.
Hala esaten didate:
Zertan zabiltza maite kontuetan?
Elkarbizitza jokuan
Eta zu, berriz, bertso ttikietan.
Kantu leunak
Nahi nituzke jarri.
Eguzkia
Ainubian denari.
Zaindu maite duzun hori.
Zaindu gorroto duzun hori.
Zaindu...