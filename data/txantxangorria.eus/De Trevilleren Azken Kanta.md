---
id: tx-1694
izenburua: De Trevilleren Azken Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uykrp0g6I_c
---

Eginik "De Tréville"-en phastualarentako.
Iruri-ko Jauregian dila urthe zunbait.


Herio latza hüllantzen ari hiz
Dolürik gabe orain nitarat
Othoitzen hait ixtant bat ützi nezak
Ene azken adiuen egitera.

Hori bera da denen ixtoria
Heriuak bardintzen handi txipiak
Zeren ilüsione bat bera da
Lürren gañeko gure pasajia.

Ene jaurregi pare gabekua
Hi altxatüz banian fiertate
Hitan igaran, denbora goxua
Haiñ llabür nükila ez nian uste.

O Basabürü amiagarria
Bortüz eta mendiz üngüxatürik
Bostetan begixtatü zütiet
Ürgullürekin terraza huntarik.

Adios arren Marie-Maidalena
Zük ingana nezazü lotarik
Ene ondotik baratzen direnez
Izan ez nadiala ahatzerik.