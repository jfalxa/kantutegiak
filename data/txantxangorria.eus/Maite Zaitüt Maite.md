---
id: tx-180
izenburua: Maite Zaitüt Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DhWcHDlw_rg
---

Maite zaitüt maitea
Ahaire hau nik asmatü nizün goiz batez kitaraikin
Amodiozko gei bat jarri nizün eskentzen deizüt klarki
Sonü hontan, sonü hontan, hitzez erran nola maite zaitüdan
Sonü hontan, sonü hontan, hitzez erran nola maite zaitüdan.

Enüke nahi hitza baliatü lehenekoek bezala
Amodiozko kantore zaharrak dü berritzeko beharra
Ürzo xuri, edo lili, izar maite, zerbait berri nahi nüke
Hitzak praube, ez düt uste, nola nahi amodioz niz egarri.

Amodioz gose eta egarri, berriz aipatzen deizüt
Sentimentüz bihotzak badaki, nola erranen deizüt
Ahotik hots, bihotz gogoz, ahotik hots, ahoko hitza da hotz
Ahotik hots, bihotz gogoz, ahotik hots, ahoko hitza da hotz.

Zuri hontü ahaire hontan ez dit ez azkenian hitz berririk atzaman
Hitza sortzen da bihotzean izanik ere bero hozten da ahozkatzean.

Ene ürzo lüma gris gaixua, ene izar maitea
Ene batzeko bihotz eztia, bai zü ene maitea
Erraiten deizüt, orai ordü da, erraiten deizüt
Maite zaitüt maitea.