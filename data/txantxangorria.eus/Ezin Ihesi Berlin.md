---
id: tx-1098
izenburua: Ezin Ihesi Berlin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LU2gfV6oBfA
---

Munduaren amaieran
kabareta zen hiria
Déja vu bat Harridura
Beste  behin bizi al nintzen ba?
Ni ta Nick Cave Ta Win Wenders
Colombokin Casolare -n
Noiz denbora Hasten bada,
Espazioa non bukatzen da?
Geroa ez da idatzia
Movimiento Kino zinean
Joe Jack/son I/kus/te/zi/na
Ni/re on/do/an e/se/ri da
Ger/ta/tu zen Be/hin ba/te/an
Ta be/rriz ger/ta/tu/ko da
Bag/dad ke/bab De/se/rri/an
Ber/lin du/gu gu/re bai/tan
Lu/cha Ar/ma/da Ma/iatz len
U/to/pi/a ez da e/ten

E/ZIN I/HE/SI ZU/GAN/DIK
E/ZIN I/HE/SI BER/LIN
E/ZIN A/TZE/AN U/TZI
E/ZIN I/HE/SI BER/LIN

EZIN IHESI ZUGANDIK
EZIN IHESI BERLIN
EZIN ATZEAN UTZI
EZIN IHESI BERLIN