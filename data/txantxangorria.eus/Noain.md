---
id: tx-497
izenburua: Noain
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dOf8ctNdhlA
---

NOAIN

Amets hura amaitzean
azken hatsa geratu zen han.
Armarri ta ezpata, irrintzi dantza,
herri baten esperantza
odol artean galduta.
Gure seme-alabek ez dezaten esan
guregatik orain
galdu dugula Noain.

Jausitako aberrian
jaiki dadin ausardia.
Amaitu da dolua, sentimendua
ez bedi izan zapaldua
altxa behar da burua.
Gure seme-alaben irribarrea
izango da mendekua.
Heldu zaigu ordua!

Ahanzturaren lurrak irauli nahi eta ezin.
Arbasoen senaren legez berriz ekin.
Haiengatik, sortu edo hil.
Deika dugu geroa,
altxatu Nafarroa!
altxatu Nafarroa!