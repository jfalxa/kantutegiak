---
id: tx-1050
izenburua: Fan Hemendik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RNYuKw-j9Gg
---

Goizeko ordu biak dira ta
etxerantza bidean gatoz
aste honetako laugarren
kontrola mekauen dios
pikoletoak
errespetoak
eurentzako
Alde hemendik esan diogu ta
ulertu egin eztigu ba.

Txakurrak, gau eta egun, han eta hemen
Beldurrak ez du balio
Bidali beraiek egin beharko ditugu
Hemendik, hemendik kanpora.

Langileen aldeko manifa
bagoaz pankarta atzean
zazpi furgona hamasei beltz
kolpeka kale ertzean
kaskogorriak
alperzorriak
ze ostia!
Sudurzulotik sartzen duzue
zuen ausardia guztia.

Txakurrak, gau eta egun, han eta hemen
Beldurrak ez du balio
Bidali beraiek egin beharko ditugu
Hemendik, hemendik kanpora.

Hendaian lanean hasi nintzen
Behobian berriz parada
lehiatila jeitxi eta
berriz tipo bera da
Aizu madero
ia egunero
hemen zaude
nahi al dezu boletotxoren bat
presoak etxeratzean alde.

Txakurrak, gau eta egun, han eta hemen
Beldurrak ez du balio
Bidali beraiek egin beharko ditugu
Hemendik, hemendik kanpora.