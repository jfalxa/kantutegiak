---
id: tx-1646
izenburua: Herrimina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YmAZUbBRQRc
---

Zeruan eder ilargia
izarren aintzindari;
haren ondoan artizarra
goizean da nabari.

Haiek zerutik beira daude
nere sorterriari;
ni berriz basabazter hontan
nigarretan naiz ari.

Txoriak ere kantatzen du
baratzeko hesian:
"Oriko txoria laket da
beti Ori-mendian".

Uda ta negu, gau eta egun
han bizi da lorian.
Nik ere bizi ta hil nahi dut
sortu nintzen tokian.

(P. Zarranz / "Ganbara";
Praetoriusen doinu batean oinarriturik)