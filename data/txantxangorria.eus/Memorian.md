---
id: tx-2916
izenburua: Memorian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IV3-FTNCL-Y
---

Haize argi baten boladan zoaz, mugaz bestalde,
izaren gora. Bilatzen zaitut eta ez zaude,
bizitzen zaitut muinaren barne.

Larrosak sortzen diren larrean
ikusten zaitut zeure barrean.

Zure joanak bilustu gaitu,
ta isiltasunak dario egun.
Eta hain gertu ta hain urrun zaitut,
hegaka doan, hostoa zaigu.

Izarrak ari dira argitzen,
ilenti zara, gure oroimen.

Oroitzapenez bete izan dut, bizitza ta itxaropena,
esaidazu zelan doakizun,
goiztiri ederrik zauden hortan ba ote dagoen?

Bilatzen zaitut eta ez zaude,
ta egonezin hau ehutsi ezin dut.
Argia ilun, bizia samin;
eguzki izpiak hain dira urri.

Esaidazu amets egin daitekeen,
zorionik han somatzen ote den.

Lur barruko ke-adarra, errotik sortzen dan arra.
Eguzkiaren sugarra aldendu da, urrunera, urrunera.

Hain bertan, ta hain urrun zaitut.
Hain gaude, zure aiduru. Zure joanak bilustu gaitu.
Ta isiltasunak dario egun.

Ta memorian, zure irria.
Ta memorian, zure begiak.
Ta memorian, zure keinua.
Ta memorian, zure izatea.
Ta memorian, bizi zaitugu.
Ta memorian, zure aiduru. 
Ta memorian, zaitut katigu.
Ta memorian, izango zaitut.