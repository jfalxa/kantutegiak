---
id: tx-3214
izenburua: A Ze Minak! Takolo, Pirritx Eta Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Tlp-JM-2OTE
---

A ze minak, liztorrak eginak
atzetik zizt! zazt! datorkit.
Ziztatu nau buruan
aaaii!
A ze minak,...
Ziztatu nau sorbaldan
buruan, sorbaldan
aaaii!

A ze minak,...
Ziztatu nau eskuan
buruan, sorbaldan, eskuan
aaaii!

A ze minak,...
Ziztatu nau gerrian
buruan, sorbaldan, 
eskuan, gerrian
aaaii!

A ze minak,...
Ziztatu nau oinetan
buruan, sorbaldan, 
eskuan, gerrian, oinetan
aaaii!

A ze minak,...
Ziztatu nau ipurdian
buruan, sorbaldan, 
eskuan, gerrian, 
oinetan, ipurdian
aaaii!