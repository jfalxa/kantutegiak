---
id: tx-3199
izenburua: Gure Trikitilariak -Tapia Eta Leturia-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o3ePtZuuHgI
---

Non ote daude gure 
Aspaldiko lagunak 
Gu baino lehen sortutako 
Trikitilari onak 
Non ote daude gure 
Aspaldiko lagunak 
Kantuz bizi izanda gero 
Kantuz joan zirenak. 

Ez dut ikusten inon 
Haien oroigarririk 
Ez dago haien izena 
Daraman plazarik 
Ez dut ikusten inon 
Haien oroigarririk 
Eskerrak gure bihotzak 
Ez dauzkan ahazturik. 

Gure bihotzak badaki 
Gure eskuak ere bai 
Lehengoak nor ziren eta 
Noraino genitun gai 
Gure bihotzak badaki 
Gure eskuak ere bai 
Zenbat jende jarri zuten 
Dantza egiten alai. 

Kolorezko soinu txiki 
Bizi-bizi kuttuna 
Ez da zuntzun 
Ez da tuntun 
Kolorezko soinu txiki 
Bizi-bizi kuttuna 
Ez zaizu behin ere faltako 
Gu denon maitasuna.