---
id: tx-1693
izenburua: Galdera Hotzen Erantzun Eza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vluV07h8wOk
---

Galdera hotzen erantzun-eza
alkoholetan bustia
ezin irentsiz, nora-ezean
dabil gizon bakartia,
nazka gordina baso-erditan
bortxaz hartzera jaurtia,
dabil gizon bakartia.

Galdera hotzen erantzun-eza
bihotz-sapaian ozkia,
ilunabarrez teilatuetan
odol gorri da ekia,
giza-eremu eritsu honen
madariku ta argazkia,
odol gorri da ekia.

Galdera hotzen erantzun-eza
arrazoien minbizia:
adimenaren begi etzanen
itzal ilun nahasia,
ixiltasunez jakin-gosea
saretzen duen hesia,
itzal ilun nahasia.

Galdera hotzen erantzun-eza
soiltasunean etzanda
ezaren zirbil zimurtuetan
etsipen-marruen galda,
sukar sendakaitz ikara arrotzak
bihotz-muinetan eztanda,
etsipen-marruen galda.

Galdera hotzen erantzun-eza
eztarritik ausikika,
patu beltzaren zaunka zorrotzak
grabatuak intzirika,
bildur guztien mentu zaharrak
garuenan karraxika,
grabatuak intzirika.

Galdera hotzen erantzun-eza
behazunetan mindua,
estutasunen ezpain moretan
sargoriak ozpindua,
arrastakako eskarmentuan
oharkabean bildua,
sargoriak ozpindua.

Galdera hotzen erantzun-eza
pentsamentuen kurpila,
nahi-ezinak besabesaka,
ondoan malkor hurbila,
zorigaitzaren begi ustelen
osin-zuloan ur-bila,
ondoan malkor hurbila.

Galdera hotzen erantzun-eza
alkoholetan bustia
ezin irentsiz, nora-ezean
dabil gizon bakartia,
nazka gordina baso-erditan
bortxaz hartzera jaurtia,
dabil gizon bakartia.