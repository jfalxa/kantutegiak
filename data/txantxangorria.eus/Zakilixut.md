---
id: tx-945
izenburua: Zakilixut
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YWK9USehNs8
---

Adi ezak laguntxo, ze habil gaur lige bat harrapa nahirik?
Anai txikiari lan asko ematen diok aspalditik
ikusiko dek laister hik, geldituko zaik alperrik
hori altxatzeko zementoa beharko dik.
Xutik, xutik, zaitu ezak hori, zaitu ezak hori
bera bat bakarra hire amak eman bait dik
zaitu ezak hori, zaitu ezak hori
hamaika lore eder errezka zimeldu dik.
Bainan orain ezin aze, zulo berrian sarturik
bromuro piska bat ona duk noizbaitean hau jeisteko
janari onak eta ere lo asko
horrela zaitzen baduk anai txiki egingo zaik mozolo.
Adi zak Xabier zergatik sartzen zera nirekin.
Zure bibote handi hori zein lekutan ote den ibili.
Zure anai txiki hori ez da pakean geldituko makina aldiz jolasean da egongo.
Zaitu ezak hori, zaitu ezak hori
bera bat bakarra hire amak eman bait dik
zaitu ezak hori, zaitu ezak hori
hamaika lore eder errezka zimeldu dik.
Bainan orain ezin aze, zulo berrian sarturik
bromuro piska bat ona duk noizbaitean hau jeisteko
janari onak eta ere lo asko
horrela zaitzen baduk anai txiki egingo zaik mozolo.
Zulo bat bakarra probato duk hik.
Askotan ibili nahiago diat nik.
Besteentzat ere utzi batzu gaurtik
Hiretzako nahi dituk nik utzirik.
Arrantzale ona izan nintzen lehenik.
Nik hautsiko dizkiat marka horiek nik.
Globoan igo falta zaidak bakarrik.
Han goian ibiltzen nintzen aspalditik.
Atzetik proposamenak eduki.
Mantekilla ARIAS ibiltzen diat nik.
Adi ezak laguntxo, ze habil gaur lige bat harrapa nahirik?
Anai txikiari lan asko ematen diok aspalditik
ikusiko dek laister hik, geldituko zaik alperrik
hori altxatzeko zementoa beharko dik.
Xutik, xutik, zaitu ezak hori, zaitu ezak hori
bera bat bakarra hire amak eman bait dik
zaitu ezak hori, zaitu ezak hori
hamaika lore eder errezka zimeldu dik.
Bainan orain ezin aze, zulo berrian sarturik
bromuro piska bat ona duk noizbaitean hau jeisteko
janari onak eta ere lo asko
horrela zaitzen baduk anai txiki egingo zaik mozolo.