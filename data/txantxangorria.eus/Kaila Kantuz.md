---
id: tx-814
izenburua: Kaila Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8dgSepq-6xI
---

Kaila kantuz ogipetik 
uztail agorriletan: 
maitea ganik etxerakoan 
aurthen entzun dut bostetan: 
amodioak nindera bila 
haren ate leihoetan

Maitea ganik etxerakoan entzun izan dut bortzetan,
amodioak bainerabia haren ate-lehioetan.

Basterretik basterrera oi munduaren zabala
Ez dakienak erran lezake ni alegera naizela
Hortzetan dizut irria eta bi begietan nigarra.
Ez dakienak erran lezake ni alegera naizela
Hortzetan dizut irria eta bi begietan nigarra.

Gaua ilun, bidea luze, ez dea pena haundia?
Zur(e)_ikustera jiten gitzaizu, oi_izar xarmagarria;
otoi atea idek aguzu, zuk, tendreziaz betea.
Zur(e)_ikustera jiten gitzaizu, oi_izar xarmagarria;
otoi atea idek aguzu, zuk, tendreziaz betea.

Gaua luze izanagatik, argi eskasik ez dugu:
izar eder pozgarri hura lehioan omen diagozu;
gu etxean sartu artino harek argituren dauku.
Izar eder pozgarri hura le/hio/an o/men da/go/zu;
gu e/txe/an sar/tu ar/ti/no ha/rek ar/gi/tu/ren da/u/ku.

A/mo/di/o/a, a/mo/di/o na/hi du/e/nak har di/ro;
nik ba/ten/tzat har/tu dut, e/ta se/ku/la ez u/tzi/ko,
ez se/ku/la, ton/ba/ren bar/nen sar/tu ar/ti/no.

Ku/ku/iak u/me/ak xi/lo tti/pi/an ha/ritz gai/ne/an,
a/ma, ni e/re na/hi niz ez/kon/du a/di/nak di/tu/da/ni/an;
e/ne la/gu/nak e/gi/nak di/re ju/an den as/pal/di han/di/an.

Pri/ma/de/ran zoi/nen e/der bri/o/le/ta/ren lo/ri/a!
As/pal/di/an nik ez/tut i/ku/si neu/re mai/ti/a/ren be/gi/a;
ba/lin/ba gai/xo/ak ez/tu a/han/tze ni/ri e/man fe/di/a.

O/roi/tzen nu/zu, o/roi/tzen, ez za/ta/zu a/hanz/ten;
Ma/da/le/na ba/tek be/zan/bat mun/du/ian dut so/fri/tzen;
ja/ten du/dan o/gi/a e/re ni/ga/rrez dut tren/pa/tzen.

Ga/ua i/lun, bi/di/a lu/ze, ez de/a pe/na han/di/a?
Zu/re i/kus/te/ra ji/ten gi/ra, i/zar xar/ma/ga/rri/a;
bor/ta i/rek a/gu/zu, zuk, ten/dre/zi/az be/ti/a.

Ga/ua lu/ze i/za/na/ga/tik, ar/gi men/tsik ez du/gu:
i/zar xar/ma/ga/rri hu/ra le/io/an o/men da/go/zu;
gu e/txi/an sar/tu ar/ti/no ha/rek ar/gi/tu/ren de/rau/ku.