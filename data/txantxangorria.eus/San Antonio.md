---
id: tx-3198
izenburua: San Antonio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6uwIKLFQfxU
---

Neskatila polita eta liraina,
izan zinen eta zara orain ere
edertasunak agurra dakar baina,
zure 30 urtearen truke.
Jauntxozalea zara, jarraitu horrela
baina printzerik ez da, hemengo plazetan
Mutil asko hurbildu dira zuregana,
bihotz minduta maitasunari esker
eta ezin duzu aukeratu laztana
nor bikain hori itxaroten zaude
Jauntxozalea zara, jarraitu horrela
baina printzerik ez da, hemengo plazetan
Urkiolako gainean san antonio
neska zaharraren gidari kutuna
askorentzako bihotzian zan amodio
zuk erregutu beharko duzuna
Jauntxozalea zara, jarraitu horrela
baina printzerik ez da, hemengo plazetan