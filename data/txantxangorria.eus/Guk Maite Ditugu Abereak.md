---
id: tx-2532
izenburua: Guk Maite Ditugu Abereak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CV5nKfj4DLw
---

Guk maite ditugu abereak.
Guk maite ditugu abereak.
Zaldia ta txakurra,
txoriak, arrainak;
denak maite ditugu,
politak dira eta.
Orain kanta dezagun:
astoak ja, ja, ja;
idiak mu, mu, mu;
ahatetxoak kua, kua, kua;
zaldiak iji, ji, ji;
txakelak kroa, kroa, kroa;
txerrinoak kurrin kurrin;
txepetxak piripi;
kilkerrak kri, kri, kri;
oilarrak kukurruku kuku
kukurru kuku
katuak miau, miau, miau;
txakurrak uau, uau, uau;
arkumeak be, be, be.