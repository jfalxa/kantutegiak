---
id: tx-323
izenburua: Arbasoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2szA1khevrY
---

Ezer ez omen gara izan
"Zerbait" izan direnen hitzetan
Baina badira sustraiak
Aspaldi ereindako hazietan
Hazietan
Ereindako hazietan
Arbasoen istorioetan
Altzoan entzundako abestietan
Betetako ametsetan
Eta betetzeko falta direnetan
Duintasunez jasotako ukabiletan
Beldurrik gabe oratutako eskuetan
Esperantzaz altxatako barrikadetan
Dena eman dutenen itzaletan
Nekazarien esku landuetan
Fabrikako langileen aurpegi ilunetan
Kalean dabiltzan umeen jolasetan
Amaierarik gabeko ilunabar luzeetan
Arbasoen istorioetan
Altzoan entzundako abestietan
Betetako ametsetan
Eta betetzeko falta direnetan
Arbasoen istorioetan!
Betetako ametsetan!