---
id: tx-34
izenburua: Goizetan Jelkitzen Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5VyMVJs5J6I
---

Goizetan jelkitzen da izar bat ederrik, hura dela diote zelian ederrenik,
Lürrean ikusten düt bat ederragorik, zelietan ere ez beitü harek bere parerik.
Aspaldiko denboretan, gaiaz eta beti, ihizen nabilazü txori eijer bati,
Azkenekoz atzaman düt, Oi bena trixteki ! lümarik eijerrena beitzaio erori.
Txori kantazale eijer xarmagarria, aspaldian ez düt entzün zure botz eztia,
Arren kontsola zite, tristeziaz betia, ez zirade izanen gaixki tratatüa !
Eijerki mintzo zira üsatü bezala, tronpatü nahi naizüla badizüt beldürra,
Zü ziradila kausa galdü dit libertatea, ez nezazüla kita fidela bazira !
Hitzak : herrikoak
Müsika : Jean ETCHART