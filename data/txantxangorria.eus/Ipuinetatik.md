---
id: tx-2630
izenburua: Ipuinetatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lkLNv6EZZZs
---

Behin ba omen zen ahate txiki bat 
desberdina denengandik,
ez zituena moko luze bat
ta bi hegalzabal, polit
Zisne talde batek gonbit, 
maitatu zuten gogotik,
haiekin dabil geroztik.
Bere burua onartu zuen,
ta ausardi horrengatik;
barrutik zisne bilakatzeak, 
edertu zuen kanpotik.

Murumendiko Mariren berri
konta beharrean nago
seme-alabak ez zituen nahi
bihurtu kristau esklabo. 
Mari sutu zen zeharo 
guztíek zioten klaro: 
madarikatua dago!

Garbi zuenez, alele egin zuen 
gure mundutik harago. 
Lurrean daude ta infenuan 
baino deabru gehiago. 

Baziren hiru txerritxo ausart
etxe berean heziak,
lasai bizitzen ez zien uzten
otsoaren maleziak
Harrizkoan babes guziak
etsaiaren gabeziak
aldaketan sinetsiak.
Elkarrekiko maitasun harek
egin zituen bereziak.
Badaude eta indarrez bota
ezin diren harresiak.