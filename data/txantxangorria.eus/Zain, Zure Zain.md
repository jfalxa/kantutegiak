---
id: tx-1828
izenburua: Zain, Zure Zain
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VfeTfHfb_a8
---

Denbora badoa berarekin hurruntzen zoaz
ase naizelarik zuganako amodioaz
ez dut oraindikan zer gertatu zaigun ulertu
gauza on guztiak nozbait behar dute bukatu
 
zain zure zain egongo naiz bihotzeko etorki onetan
zain zure zain maitasun hau agortuko ez dan iturria
 
iluntasunean ote naiz ni galduko
zu aurkitu ezean nor laztandu ta maitatuko?
hemen zinelarik zein erreza zan zu aurkitzea
zu gabe ordea hurrun doaz nire ametsak
 
zain zure zain egongo naiz bihotzeko etorki honetan
zain zure zain maitasun hau agortuko ez dan iturria (bis)