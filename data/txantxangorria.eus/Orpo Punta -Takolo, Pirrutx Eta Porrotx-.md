---
id: tx-3213
izenburua: Orpo Punta -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a2ASVCbfrG4
---

Musika: Imanol Urbieta

Orpo punta, orpo punta,
aurrera eta itzulika.
Orpo punta, orpo punta,
aurrera eta itzulika.
Orpo punta, orpo punta,
aurrera eta itzulika.
Orpo punta, orpo punta,
aurrera eta bertan.
Plis, plis, plas, txaloka,
jiraka eta biraka.
Plis, plis, plas, txaloka,
jiraka eta biraka.
Plis, plis, plas, txaloka,
jiraka eta biraka.
Plis, plis, plas, txaloka,
jiraka eta biraka.