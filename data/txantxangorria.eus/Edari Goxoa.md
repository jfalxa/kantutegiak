---
id: tx-1983
izenburua: Edari Goxoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ss4xXuExwTA
---

jaunak, hau bai edari goxoa
arimen iratzargarri
egin dezagun azken tragoa
atea itxi aurretik

haserre joan zaizkigu emazteak
beren izenak madarikatzen
azkena egin dezagun mutilak
abisua jo aurretik
mundura sufritzeko ez ginen jaio
ezta gutarrak jagoteko
azkena egin dezagun agudo
infernua gure zai dago