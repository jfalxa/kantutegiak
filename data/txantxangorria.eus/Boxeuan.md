---
id: tx-2678
izenburua: Boxeuan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S8MtGEaXPUg
---

Gazte arrunt bat esangurarik ez dut, akitua dut murrua. 
Marmar xalo hauen truke, promesen pare. 
Ilun ta beltz, izateak nahi dena entzun du 
ta arrotz zaiona ez, mmm. 

Agurtu nuen sorterria, hain nintzen mutil xume 
ezezagunen mempeko, tren geltokiaren itzalean ihesi- 
Nora jo? Adore txanponen bila, patrikak zenbat zulo. 
Harago, bada zerbait gehiago. 

Lalala... 

Lasaitasunaren gose, lan soil baten egarri. Eskaintzarik ez. 
Ni neu bainaiz nire eginbeharrren iturri. 
Hala hobe, unerik eroenetan da norbera bere fede. 

Lalala... 

Neguko arropak erantziz erbestea dut amets. Bidaia on. 
Hotzak odolustuko ez nauen leku hartara bertara. 
Bidaia on. 

Argilunpetako gerlari, zenbaitetan galtzaile 
ta soinean daramatzat etsipenaren ubeldurak 
onduko dira. Segi! 
Nahiz izan hainbat kalte 
ni enoa eroa ni 
erronkak iraun arte.