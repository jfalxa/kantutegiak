---
id: tx-2128
izenburua: Ze Esatek!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OobZlOdb7zU
---

Badator bueltan Euskal Herri osoa dantzan jarri duen taldea. Bere 50 kontzertuko bira latzaren ostean 2009an bai EH-n baita Països Catalans-etan ere, lokomotora bueltan da inoiz baino egur gehiago emateko asmoz eta bere maitasuna Mirentxu Azkarateri deklaratzeko asmoz.
Diska berri batekin datoz, 13 abestiz osatua. Bertan 2009ko biran eskenatokiak astindu zituzten hit-ez gain, beste sorpresa berriren bat ere aurkituko duzue.