---
id: tx-1666
izenburua: Joseba Elosegi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/loXRwRsocyE
---

Lotuz aire zaharrari
dugun kanta pertsu berri
joseba elosegiri x3
gizon miresgarriari


Ene haur eta ahaide
baita euskaldun haurride
zonbait zaituztedan maite x3
nehoiz nehork ez dakite


Adios mendi ederrak
oihan ta itsas bazterrak
adios Donostiarrak x3
jauntziren ditut su-garrak


Arras triste da bizia
askatasunik gabia
herriarentzat hiltzia x3
nuen nik neure lehia


Donostiko medikiak
erien sendaltzailiak
utz otoi ene zauriak x3
dagoden oroitgarriak


ni hiltzen baniz huntarik
ez otoi egin nigarrik
ez baitut nigar beharrik x3
ez baitut jarraikilerik