---
id: tx-910
izenburua: Hego Haizea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W27EhNO_owY
---

HEGO HAIZEA


Lumak hegan
ta esku bat eman
ta etorri irriparrez gaiñezka
gordeta zeuden
oroitzapenen ikuiluan
haize bero bat sartzen da batzuetan
(Hego haize bero bat sartzen da)


Lumak hegan
ta oain antzeman
zure belarritan orkesta
gaurko txorien
bidai luzenetan abentura
haize bolada batez asten da


dandarabiribiri dan dan dan...


Iparraldeko gauen hotza freskatzen daki
ta arauen kate motza lotzen du naizekebaki
eman zion ahotsa alproja mistiko bati, PATXUKO
sin viento sur tu y yo no estabamos aqui
geata ibiltari, haize boladen menpeko 
ta ez bazaude burutik seko ez zaitut gustoko OSO CRAZY
pero hay mucha verdad en los locos
Patxuko, el viento sur a ti y a mi nos da en el COCO


HEGO HAIZEA I NEED YOU ALONG DO WAY
GOXO GOXO BAZATOZ TA GOXO GOXO BAZOAZ


Hay Juantxito, Que este aire me esta poniendo loquito
en la cima o bajo tierra sin mentiras
lo que pongo en el papel yo me lo quito 
ya esta Patxuko en el garito
Oye Juantxito, que el Jon Mari lia, lia, que te lia
otro fichaje de la mereologia 
un loco solo no puede formar jauria
busca conpañia, burun dabillen...


Musika: Patxuko nice
Hitzak: Juantxo Arakama (Glaukoma)