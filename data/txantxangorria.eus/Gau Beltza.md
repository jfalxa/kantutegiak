---
id: tx-444
izenburua: Gau Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/X6tgh1q--MU
---

BERTSOLARIA(K): XABIER TERREROS GARTZIA "TERRE"
DOINUA: HAMALAU HERIOTZENA I
Euskal Herrian aspaldidanik ospatzen da arima hilen gaua... Gaur "Halloween" izenez ezaguna.


1.
Urriaren hogei ta
hamaika heltzean
sorgin-arrats, arima
hilen gau beltzean.
Kalabazak, arbiak
hustu baratzean
ikusi ezinean
kandelak jartzean
arima erratuak
itzalen atzean.

2.
Herri bakoitzak bere
erritual fede
ta ohiturak garatu
ditu mendez mende.
Nahiz sortzeak hiltzea
duen beti lege
zer dela ta ez gara
ausartzen behin ere
Heriori zeharka
begiratzen ere.

3.
Arratoiaren begi,
apoaren bihotz
saguzarraren buru,
laban, sega, aihotz.
"Ziri edo sari" oihu
aldaba zaharrak joz
terrorezko maskarak
ipinita gatoz
norberaren ikarak
ezkutatu asmoz.

4.
Eskaparate oro
eder dekoratuz
guztia dago salgai
trukoz eta tratuz.
Utik! Goazen kalera
bertsoak kantatuz
beldurrezko kondaira
ahaztuak kontatuz
etxean ditugun lau
trapu zahar hartuz.

5.
Estatu Batuetan
nahiz ezagun egin,
zeltiarrek bezala
hemen ere berdin
kalabazak, maskarak
kandelei eragin
izaera antzeko
ta izen desberdin
aspaldi ospatzen da
gurean Halloween.