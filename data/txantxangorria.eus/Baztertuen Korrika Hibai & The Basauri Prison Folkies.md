---
id: tx-2353
izenburua: Baztertuen Korrika Hibai & The Basauri Prison Folkies
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1ODDjOGYyuE
---

BAZTERTUEN KORRIKA

Batueraz mosu Sekretuak
Nerbioi ibai zikinaren aurrean,
Maite zaitut esan nizun Ainhoako frontoian
ta Ondarrueraz ein genuen larrutan.
Galeretan boluntario zugatik Euskara 
Martin Larralde ren ondun arraun egitera.
Koltxoiaren malgukiak ixiltasunian, Euskal hitzek
itzartu zuten Azkaine herria.
oh, oh ,oh Goazen Korrika 
Altzukuko bertsuen epeltasunerantz.
oh, oh, oh 
Santurtziko kale gogorretan tipi tapa.

Sarri ren pianista erahil zuten tabernan
Beltzaranak jo zuen txistuaz agurra,
Babilonian Korrika Basko Mohikanoak, 
Edinburgon entzuten dira euren pausokadak.
Bidea latza da ametsak bezala,
nere oin deseginen olerki kartsua.
Hogei aldiz etsi gabe Marathon bezala, Gau ta eguez
eternidadera.
oh, oh ,oh Goazen Korrika 
Altzukuko bertsuen epeltasunerantz.
oh, oh, oh 
Santurtziko kale gogorretan tipi tapa.