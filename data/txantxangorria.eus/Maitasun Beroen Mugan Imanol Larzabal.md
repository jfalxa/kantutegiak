---
id: tx-1635
izenburua: Maitasun Beroen Mugan Imanol Larzabal
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Td6L3IirJ5s
---

Maitasun beroen mugan
sabelez argitutako
egunsenti gordinetan
hasi da infinitoa

Maitasun beroen mugan
haragiz kolpatutako
itsaso mugikorrean
galdu da infini/oa.

Mai/ta/sun be/ro/en mu/gan
de/si/oz zar/ta/tu/ta/ko
be/gi/ra/den bus/ti/du/ran
i/to da in/fi/ni/to/a.

Maitasun beroen mugan
bihotzez gozatutako
debeku maiteenetan
piztu da infinitoa.

Mai/ta/sun be/ro/en mu/gan
es/pan/tuz hon/da/tu/ta/ko
gaz/ta/ro go/go/ra/tu/an
bi/zi da in/fi/ni/to/a.


Maitasun beroen mugan
sabelez argitutako
egunsenti gordinetan
hasi da infinitoa