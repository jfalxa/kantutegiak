---
id: tx-3162
izenburua: Zure Doinua -Pitxi Eta Letu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lOsYmsET-ZM
---

Euri asko egin du neska lehenengo laztanetik
amaigabeko gau sutsuak ezin kendu burutik
Alkoholak eraman zituen lotsaren azken tantak
Zein zoriontsu izan garen denbora guzti hontan

Zure irribarre goxo hura non galdu ote da
ez al zara konturatu gurea haustu dela
Zein gogorra egiten zaidan bukaera onartzea
Zu zoriontsu izatia nere azken eskaria

Taupada luzeak izan dira maitia baina orain
Nere kantuak ez du zure doinua

Jakin inor ez bezala maite izan zaitudala
aska ditzagun bihotzak idatziz atal berri bat

taupada luzeak izan dira maitia baina orain
nere kantuak ez du zure doinua