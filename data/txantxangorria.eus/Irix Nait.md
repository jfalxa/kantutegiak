---
id: tx-2151
izenburua: Irix Nait
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/h1Dfgfu3PKc
---

Erasmus programarekin Finlandian dagoen euskaldun batek eskatutako abestia, Irlandan programa berekin dauden lagun batzuei bidaltzeko.