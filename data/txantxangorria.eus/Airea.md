---
id: tx-1148
izenburua: Airea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/imYtmT5vMT8
---

Agertu zara jendetzaren artean 
Berandu egin zaigun azken taberna honetan 
Argia soinean, distiraz jantzita 
Jendetzaren artean zu agertu zara. 

Airea izan nahi dut 
Zure hasperen bakoitzean 
Nik aire izan nahi dut. 

Momentu batez denbora gelditu da 
Gure begiradak gurutzatzean 
Galduta nago, iluntasunean 
Galduta, zure begietan. 

Airea izan nahi dut 
Zure hasperen bakoitzean 
Nik aire izan nahi dut 
Goizero zurekin esnatu 
Eta belarrira esan: 
Nik aire izan nahi dut 

Aire, airea... aire, airea 

Airea izan nahi dut 
Zure hasperen bakoitzean 
Nik aire izan nahi dut 
Goizero zurekin esnatu 
Eta belarrira esan: 
Nik aire izan nahi dut 

Aire, airea... aire, airea.