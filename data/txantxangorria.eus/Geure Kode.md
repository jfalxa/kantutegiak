---
id: tx-868
izenburua: Geure Kode
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mplDrcHVHR8
---

Gatomek bere diska berriaren aurrerapen abestia atera du bideoklip formatuan. Urriaren 29 an aurkeztuko den"Euskalskillz" lan berriaren parte den abestia dugu "Gure kode". Soinu jamaikarrez beteriko lana dugu non reggae  eta dancehall soinuak nagusitzen direlarik
Audio ekoizpena, nahastea eta masterra #Gatom -en eskutik  #tiropunekoizpenak estudioetan
Bideoaren grabazio eta zuzendaritza: Xabier Salazar eta Iñigo Iriondo
Postprodukzioa: Tiropun Ekoizpenak

Hemen egunero borrokan
arbasun modun borrokan
hemen eztu pentsa porrotan
gizarti ondino lotan
x2

Jentik gudau bakarrik parranda
aman eskotik dan dana janda
jentik gudau bakarrik parranda
etxera buelta like oso panda

Aguanta kamarada eztanda
bombo klap heldu da geure txanda
azken txampan eztau champan
harrokeri barik borrokan

Heziketa gaur egungo trampa
idea esplosibukaz eztanda
protestaten kalien etzanda
polizidxek manifetan talka ta

zauritu asko azken urtietan
eztaui esaten hori telebistan
zauritu asko azken urtietan
idealangaitzik borrokan

estrbi x2

Telebista gaur egungo trampa
gatomek rapa like helikopta ezin konta
metralleta, lirican, arround my mind
iraultzarako irrikan

Itsosuen usaine kresala ta arraine
zabaldu oine beso ta burmuine 
umik ona eztakaz zikoinek
histori errealak eztizen ipuinek

Sortuten geure kode gu
sisteman bizirauten hori badakigu
loratzen geure lore look
oxigenu sortuten natura zainduzu
naturalan kolore is good
egunez eta gauez zentzunek bizitzuz, 
itsoko zapore gerlaridxen adore historidxen lorpenak zaintzion ohore