---
id: tx-2941
izenburua: Bihar Igandea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kH3F0cyBsMI
---

Bihar da igande,

izebaren eguna.

Gela garbitzen du

jantzi zuria(re)kin.

Laranja aurkitzen du.

zurituta jaten du.

Oh! sabel handia,

oh! sabel handia.

Zein aterako da plazara?

Atera dadila...

bere lagunarekin.

Dantzan egingo luke 

dantza(tze)n baleki.

Lara, la lara, la,

lara, lara, lara, la,

lara, la lara, la,

lara, lara, lara, la,