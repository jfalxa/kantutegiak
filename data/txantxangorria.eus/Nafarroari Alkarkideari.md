---
id: tx-1269
izenburua: Nafarroari Alkarkideari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7lCnQUcY0cI
---

NABARRAKO
EUZKO BAZKUNARI
Zorioneko batzar onetan
Nabarrak gure anaiak,
Bizi dirade lege onean
Gaur guztiz gizon ernayak
Guregatik on esango badu
Etorkizunak kondairak
Laurak bat beti... maite alkartu
Izan euskaldun leyalak.
Alkar gaitezen txiki ta aundi
Aberats eta pobreak
Maita zaiteste, esaten digu
Jaungoikoaren legeak.
Auziak utsi alde batera
Batu euskaldun guziak
Besoak zabal esaten digu
Gaur Iruña erriak.
Erruki zaite, oh, Jaun maitea!
Lagundu Euskalerria
Bada guztiok alkartu gera
Oraiñ beltz eta zuriya
Anai arteko guda beti da
Guztizko negargarriya
Arren anayak errespetatu
Gizon guztien biziya.

Jose Mari Iparragirre