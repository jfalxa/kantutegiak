---
id: tx-172
izenburua: Suzko Erroberak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0nzHoX5SBX4
---

#BULEGO #Suzkoerroberak​​

Entzun SPOTIFY-en! / Escúchala en SPOTIFY! 🎆


------------------------------------------------------------------------
Gauerdian,
zelai bakarti bat
herriaren mugarrian.
Zurrumurru batek
apurtzen du
haraneko isila.

Aingerua deabruz jantzita
sorbaldatik zirika:
“onena galdu nahi ez badek
ezin gaituk erretira”

Besoak zabalik,
haizea azalean,
adrenalina zainetan.

Jakingo bazenu zure
bihurri aurpegi hori
zenbat gustatzen zaidan.

Besoak zabalik,
haizea azalean,
adrenalina zainetan.

Bihotza gidari dugunok
ez dugu inoiz
iparrorratzik eskutan

Suzko erroberak
Bihurtzen gara
Suzko erroberak
Gauaren babesean

Damuaren mamuak
ozenki esan zuen:
pentsatu zazue ondo
arauak hautsi baino lehen.

Zuzena-oker, okerra-zuzen,
hemen ez da inor libratzen
noizean behin,
noizean behin!

Besoak zabalik,
haizea azalean,
adrenalina zainetan.

Jakingo bazenu zure
bihurri aurpegi hori
zenbat gustatzen zaidan.

Besoak zabalik,
haizea azalean,
adrenalina zainetan.

Bihotza gidari dugunok
ez dugu inoiz
iparrorratzik eskutan.

Suzko erroberak
Bihurtzen gara
Suzko erroberak
Gauaren babesean

eeez ez dago gerorik ez,
egingabekoagatik
baino eginagatik
damutu gaitezen

Suzko erroberak
Bihurtzen gara
Suzko erroberak
Gauaren babesean

Suzko erroberak
Suzko erroberak
Gauaren babesean
------------------------------------------------------------------------

Zuzendaritza: Bárbara Fdez.
Argazki zuzendaritza: David Mendizábal
Kamera Laguntzailea: David Palomo
Steadicam: Edu Mato
Gaffer: Ionan Aisa
Arte Zuzendaritza: Isabel Ortega
Makillajea eta Jantziak: Javier Serrano
Jantziteria: Eneko Aranbarri eta Morrow Fashion
Argazki finkoa: Maite Artajo
Ekoizpen burua: Iker Aranguren
Ekoizpen laguntzailea: Mikel Casado
Grafismoak: Ipnosix Studio
Edizioa: Bárbara Fdez.
Kolorea:  David Mendizábal.

Aktoreak:
Helena Pastor
Rocío Areán
Yasmarlin Báez
Lucía Gracía
Maddi Laiglesia
Leire Lizarraga
Itsaso Etxeberria
Iñaki Zabala
Lankidetza berezia:
Nerea Uribe, Pablo Dávila, Anaïs Del Río, Paula Marín, Adrián Pulido

Eskerrak
Iñaki, Jesús, Itsaso, Ana Estrella, Javier, Kiko, Rodrigo, Gloria, IES Plaza de la Cruz, Eregui jostailu denda, The Dinner jatetxea.

Esker Bereziak:
Panda Artist eta Oso Polita