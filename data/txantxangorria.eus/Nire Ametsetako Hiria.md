---
id: tx-1333
izenburua: Nire Ametsetako Hiria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MfWysyD3tvM
---

ZAMARRA ETA ATERKIA taldearen bigarren bideoklip ofiziala. Segundo bideo-clip oficial de la banda  ZAMARRA ETA ATERKIA.


Nire ametsetan bada hiri bat, 
harresirik gabekoa, 
ez dena inoiz izandu inbaditua. 

Han inorrek ez du ikasi 
inorretaz inolaz babestu beharra duenik. 
Ez mamu eta ez herensuge 
mitologia zaharraren aztarnetan ere, 
umetxoei kontatzen zaizkien ipuinetan ere, 
eta gauez ez du inorrek amesten.... 
ultramundutik hegan etorriko den, 
edo gudariren baten poltsikotik eroriko den 
pakearekin..... 

Nire ametsetan bada hiri bat, 
akaso gatzik gabea, telebistako eztabaiden zaleentzat. 
Xoxo-xamarra izan liteke, 
kafetegi tabernatan, mundu guztia berdetzen dutenentzat. 

Han inorrek ez du ikasi….