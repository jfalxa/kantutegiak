---
id: tx-1360
izenburua: Canço A Catalunya
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/05V5MXuVmxw
---

Anai zaharren lur gazte borrokan ezaguna,                         
ttiki geralako haundi aurkitu nahi duguna.
guregana zugandik datorkigu eguna,
itsasaldeko eguzki bero eta biguna.

Zu zera beste aberri, etxe hurbil maitea,
jakituriak guretzat mugan duen atea.
Hemendik aldegin eta behar balitz joatea
zure altzoan nahi nuke jasoa izatea.

Oi Katalunia, oi Katalunia!

Itsas ilun honetatik zure argi haundira
bihotz begiak zabalik nagokizu begira.
Urteak eta mendeak zugan iraupen dira,
eraman gaitzazu azkar askatasun berrira.

Ez zera zekenen hobi, baizik langile lurra,
Euskadiko haur ttikiak duen anai zuhurra.
Zure itzalak uxatzen dit heriotzaren beldurra,
Inoiz errausten ez dena baita gure egurra.

Oi Katalunia, oi Katalunia!