---
id: tx-1642
izenburua: Etxahun Iruri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AYntLa93s4Y
---

Txülüla joile, pilotari,
Jauzi emaile, koblakari
Artetan ere laborari
Usü ametsan ari
Horra nur izan ziren, Etxahun Iruri:
Gano handiko poeta eta zuiñen maithagarri!

Etxahun Iruri, Xiberuko lilia,
Maidalenako maxelatik
Hagalthatü txori khantaria,
Zük düzü arraphiztü
Jei zaharren leiñhüria
Ezinago maithatürik
Üskal Herria.

Üskaldün hanitx beno lehen
Üskaltzale agertü zinen,
Zure denboran ari beitzen
Xiberuko jeia galtzen.
Ordian zinen hasi bazterren harrotzen
Bekhan dizügü ikhusi, txori bat haiñ gora heltzen.