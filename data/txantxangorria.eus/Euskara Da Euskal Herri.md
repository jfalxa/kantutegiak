---
id: tx-1697
izenburua: Euskara Da Euskal Herri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OQKqIQOttdk
---

'Euskara da Euskal Herri' kanta egin dute auzolanean Altsasuko Iñigo Aritza ikastolak eta Txioka haur ikastolak Auzolanean, horrela osatu dute Euskara da Euskal Herri kanta Altsasuko Iñigo Aritza ikastolak eta Txioka haur ikastolak. Kanta egin, grabatu eta kantaren gainean bideo bat grabatu dute. Hemen dituzue kantaren letra, kanta bera eta bideoklipa.
Euskara da Euskal Herri
Espaloitik jaitsi gabe
denok euskararen alde
bide berean al gaude?
bultzatuaz denok alde
jaitsi gabe.
Nirekin etorri
bidez bide herriz herri
egin dezagun euskaraz
kantatu, lan eta jolas
euskara da Euskal Herri.
Gurekin etorri
bidez bide herriz herri
gurea zaintzen nahiko lan
ikasi dut ikastolan
euskara da Euskal Herri.
Euskal Herrian euskaraz
Kantu, irri eta jolas
Denok batera bagoaz
Nahiz ez den izango erraz
Aizu zatoz, gu bagoaz.
Nirekin etorri
bidez bide herriz herri
egin dezagun euskaraz
kantatu, lan eta jolas
euskara da Euskal Herri.
Gurekin etorri
bidez bide herriz herri
gurea zaintzen nahiko lan
ikasi dut ikastolan
euskara da Euskal Herri.
Nirekin etorri
bidez bide herriz herri
egin dezagun euskaraz
kantatu, lan eta jolas
euskara da Euskal Herri.