---
id: tx-2158
izenburua: Laster
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DbHbW1BbHCI
---

Gontzal Mendibil Zeanurin jaio zen 1956an. Bere lehen diskoa Xeberrirekin batera atera zuen, 1975an, eta garai hartan oso ezaguna egin zen 'Gontzal Mendibil eta Xeberri' bikotea. Politikaz hitz egin eta kantatzeko aukera zabaldu zen garaian zeuden, eta haien kantak oso politikoak ziren hasiera batean.