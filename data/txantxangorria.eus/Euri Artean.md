---
id: tx-851
izenburua: Euri Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MQnRBC96z3Q
---

EURI ARTEAN.
Musika: Xabi Solano
Hitzak: Jon Garmendia «Txuria»

Udazken ilun baten egun euritsua
    Azaroan
  gogoratzen dut
   nola egon nintzen kalean
 zure esperoan
  lehen aldia izango balitz bezala…
   eta hostoak
    jausi ziren
     euri artean.


Maitasun pixka bat baino ez nuen behar
 besarkadak
Ez nengoela bakarrik jakin nahi nuen
zure ondoan
baina jendea aurrera eta atzera...
eta hostoak
jausi ziren
euri artean.
                                Askatasunaren
                           euritan busti nahi nuke
                                zurekin dantzatuz
                              kale erdian bada ere
                                muxukatuz
                                 bizitza  
                          zer den dastatu
                          eta euri artean zuri begiratu.

Ematen du mendeak pasatu direla
Milurtekoak
elurrak hoztu dituela gure hitzak
 gure oheak.
   Itxoiten iragan zaizkigu urtaroak…
     eta hostoak
      jausi ziren
       euri artean.