---
id: tx-1937
izenburua: 1512 2012 Behartuta Ezkondutakoaren Drama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1pp84R7Qm_k
---

Familia deseratuak legez, desiratua?
Ez! Ezkontarazia indarrez ama.
Armez orniturik, garrez horditurik.
Ta ez kunpliturik sedukzioaren programa.
Ez maiteminik,
ez leialtasun zinik ez larru grinik...
Eskopeta atzean jarririk!
Saririk gabe ezkontarazi gintuzten
eta egin ziren gure jabe:
bat, bost, bat, bi
Kezka ekarriz pezeta egarriz.
kez eta odol gorriz
zikindu zuten lur librea zena.
Ezkon katea ezartzea
Prest Nafarroa askea lotzeko katea.
Eta orain denak haien eskutan,
gaude arriskuan.
Txarrak tratuak:
irainak eta mehatxuak. Kolpeak!
Haren buruan gure patuak
aukeratuak ditu
senar dugun Albako Dukeak.
Iluna dugu amaren biharra.
U/mi/li/a/zi/o/a e/ta tra/tu txa/rrak
Aurrerako esperantza bakarra.
Uztea behartutako senarra.
Senide saldua!
Ze koittadua!
Izan zen Nafarroako Estatua
bukatua hemen.
Alaba Parisen apupilo hartzen dute.
Boterea eskuratua meza baten truke.
Eta geroztik ez da egunik senti dugunik
zer den ezkongai aske izatea.
legez loturik ta leloturik, leizean sarturik
Platon bizi du nafar gizarteak.
Baina pizturik iraun du suak;

Prest Berotuz xuabe,
bes/te/e/tan iraultzaren garrak!
Babes etxe beharrik gabe,
inoren mirabe etxetik
botako ditugu senarrak'
Ez adiorik, ez da erremedio/rik,
ez da akordiorik...
Eska ez dibortzio abokaturik!
Baimenik gabe bananduko gara
ta izanen gure jabe!
Bi, zero, bat, bi.
Amaren seme-alaba nafarrak!
Argi izan behar dugu iparra!
Behar da gure ahotsa eta indarra!
Ai/ta/or/de/a e/txe/tik bo/ta be/har da.
Bestela egun batean
aita dutxan denean.
Ama gosaria prestatzen sukaldean.
Hotza hezurretan dauka,
atzoko senarraren zaunkak
seme ala/ben aurka,
entzuten buru barnean.
Bi laranjen zukua
eta begietan malkua.
Mozteko labana luzea bere eskuan.
Gelditzen da
begira altzairuaren dirdira,
pen/tsa/men/du/en er/di/ra da/tor
pi/pi/ak be/za/la ar/gi/ra:
Zer e/gin dut nik hau me/re/zi i/za/te/ko?
Ta pen/tsa/men/du/ak
kol/pez moz/ten di/ra se/ko!
Au/to/ma/ten gi/sa mu/gi/tzen da i/tza/la...
Her/tzai/na/ken hil e/za/zu.
Ai/ta kan/tu/an be/za/la:
"pa/si/llo/an u/rrats ba/tzuk i/rris/ta/tzen".
Ba/ina ai/ta/or/de/a or/de/a ez da o/har/tzen.
U/rrats bat, bi u/rrats, hi/ru, lau, bost, sei...
Ta sar/tzen da ko/mu/ne/an
no/la be/hi/no/la Nor/man Ba/tes.
Kor/ti/na/ren a/tze/tik na/ba/ri da i/tza/la.
A/mai/tze/ra do/a ai/ta/ren bi/zi/tza zi/ta/la.
Hatz kos/ko/ak zu/ri/tuz eu/tsiz sas/ta/ka/ia,
e/ba/ki na/hiz bu/ru/a no/la ga/ri/a i/gi/ta/ia.
Jaus/ten da be/hin, bi/tan, hi/ru/tan...
So/kalt Sta/cca/tto/an ten/tsi/o/a su/tan!
Ta bai/ne/ran ja/us/ten da a/ni/ma/li/a,
kor/ti/na go/rri za/rra/ta/tu/ek
be/ti/ra/ko es/ta/li/a!