---
id: tx-2153
izenburua: Errota Zaharra Lourdes Iriondo-Xabier Lete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TXA8jZnAID4
---

Euskal Kantagintza Berriari lehen taupadak ematen lagundu zion Lourdes Iriondo kantari eta sortzailea hil egin da.


Hogeigarren mendearen 60ko harmarkadako gazte euskaldunen gogo-bihotzetan iltzatuta geratuko dira Lourdes Iriondo Mujikaren kantuak eta kantukera. JoxAnton Artze Ez Dok Amairu taldekidearen hitzetan: Oso sakonetik kantatzen zuen, barnemuinetik erabat. Hustu egiten zen kantu bakoitzean, bihotz eta arima. Erabateko sentimenduarekin kantatzen zuen

Gazte-gaztetatik bihotzeko eritasun batek baldintzatu zion bizitza eta heriotza ere hortik etorri zitzaion atzo, 2005eko abenduaren 27an, 68 urte zituela.

Jaio Urnietan jaio zen 1937ko hirugarren hilabetea azkentzeko lau egun falta zirela (1937-03-27). Umetatik sortu zitzaion musikarako zaletasuna eta zortzi urtez opera ikasten aritu zen Donostian. Baina ikasketak utzi beharrean gertatu zen osasun arazoek behartuta, bihotz handiegia zuela-eta. Bihotza txikiagotzeko ebakuntza Parisen egin zioten 1958an.

1964. urtean gitarra jotzen ikasten hasi zen eta 1965ean lagun batek eskatuta Gipuzkoako Andoaingo ikastolaren aldeko jaialdi batean kantatu zuen estreinakoz jendaurrean. Jaialdi horretan izandako arrakastak beste hamaika emanalditarako ateak zabaldu zizkion. 1965eko maiatzaren 5ean Gipuzkoako Aizarnan egin zen Gazteriaren Egunean kantatu zuen eta, handik, Loiolako Herri Irratitik deitu zion Joxe Mari Iriondo kazetariak eta lau kanta grabatu zizkion: Abesten dizut, Jauna; Aurra seaskan; Arren, jauna; eta Amatxo laztana. Kantu haiek irratiaren uhinetatik zabaldu ahala berebiziko arrakasta izan zuten oso denbora gutxian