---
id: tx-1811
izenburua: Poema Dels Oprimits
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eRXBmbcBS3Y
---

En aquest país ocult,
amb les ombres vaig jugant;
intentant ser jo mateix,
pintant un endemà...
Tot allò que no puc ser,
raja sempre del meu cant;
l'alba és ben a prop,
la sento,em va envoltant...
La història ja se sap,
no tothom l'explica igual;
es difícil d'empassar,
la veritat del del costat...
Ara a prop dels oprimits,
un poema vull cantar,
tota l'amargor,
que volen amagar...
I canto als llits buits,
a la mare, al seu patir;
a tot el temps fugaç,
de que ens van desposseint...
Al ferro, al seu crit
al pare atemorit
a tothom que ho patim...
Escoltem des de petits,
tot allò que hem d'estimar,
tot allò que hem de ser,
a la força imposat...
ara, prop dels oprimits,
cantaré una cançó,
coberta tenen ja,
tota l'amargor...
A la soledat,
als íntims i als amics,
a cada instant del viure,
que de les mans, ha fugit
Als qui van ser ahir,
als qui son ara aquí,
als que encara han de venir...
Estimada vull sentir,
que aviat tot canviarà:
que també seràs amb mi,
el dia de demà...
I que els que vindran després,
ja mai més no cantaran,
aquest cant dels oprimits,
i tampoc no ploraran...
Diga'm per favor,
ara amiga ja,
allò que vull escoltar,
estimada, amor...
En els accidents,
les adversitats
a els que per nosaltres fins la vida han donat..
Als que de camí,
mai no han arribat,
als que lluitem,
i al vent de...llibertat."