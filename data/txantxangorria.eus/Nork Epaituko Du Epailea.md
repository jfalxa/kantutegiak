---
id: tx-547
izenburua: Nork Epaituko Du Epailea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i8hZ1zcUL7I
---

Iluntasunaren mundutik etorri dira
izua, beldurra zabaltzera
gorputzaren barruan sortzen den ar bat bezala.

Izurrite baten antzerako zerbait dira
inguratzen dien guztia kutsatu nahi dute
pozoindutako ideien bitartez
denaren jabe izan nahi dute.

Diktatzaile huraren ikaslerik onenak
agintzen duten herrialde hontan
ezer gutxi espero daiteke
beraz goazen denok hemendik.

Zeren egiaren esanahia usteltzeko gai dira
zure bizitza guztiz, guztiz izorratuz
baina zer nahi dute? Guk ez ditugu nahi
ez diegu ezer zor, alde hemendik!

Nork epaituko du epailea? Herria izango da
eta sinetsi ezin daitezkeen milaka injuztiziek
konbinentzia eskatzen ari diren hoiek
mendeku bila ari dira.