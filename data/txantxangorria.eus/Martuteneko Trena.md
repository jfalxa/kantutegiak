---
id: tx-811
izenburua: Martuteneko Trena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AzNTwSjblrg
---

Triki-traka, triki-traka
martuteneko trena,
triki-traka, triki-traka
zubitik barrena.

Aurretikan dabilena
aurrera dihoala,
azkena datorkigula
galditu dadila.