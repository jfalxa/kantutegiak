---
id: tx-2952
izenburua: Mieltxo Harrijasotzailea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7NjCUn7b-IQ
---

Hau dugu Mieltxo gurea
guztiontzat arras maitea
indartsu eta trebea
harrijasotzailea.

Tori,
zuk ura eta nik airea eman,
eskuak txuri hautsetan!
zuk ura eta nik airea eman,
giharrak prest beroketan!

Sutsuki, heldu harriari
gerrira, ari eta ari
bulartsu bultza pisuari
sorbaldan, eutsi goiari

Tori,...