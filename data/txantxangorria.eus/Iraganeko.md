---
id: tx-1587
izenburua: Iraganeko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9i6Ct0DcU9U
---

Zein ona biok nere gelan 
“You’re wondering now” entzuten 
Argazki bat malkoen artean hondoratu zen 
Ta ilusioak gezur bakoitzeko labankada bat jasotzen du gibelean 

GAUR, ATZOAN, IPUINEAN BIZITZEN, ZER GELDITUKO OTE ZAIZU ATZERA BEGIRATZEN? 
HOSTOAK GALTZEN DITUZTE ERE, BASO ERRALDOIAN DIRAUTEN ZUHAITZEK 

Hain gogorra zinen! 
Tanta bat unibertsoan bakarrik zera… 
Zein magikoa zen orain gogoratzen 
Arrosez margotutako istorioek 
Ez zutela kontatzen nola errespetatu 
Sexua askatasunez 

Galdu da geroa!!