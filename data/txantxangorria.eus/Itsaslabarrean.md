---
id: tx-473
izenburua: Itsaslabarrean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5rpL4lTP2TA
---

Itsaslabarretik itsasora jauzi batez aurrea hartu. 

Neguko gau egun hotzenetan artileaz oinak laztandu. 

Deika ari dira argazkiak gelan, 

oroimenak giltzatu zaitu.     

Itzartu duzu paperezko eszena ta itxaroten zein, zein, zein, 

zein keinuk, eramango zaituen hara, 

zai zara. 

Laino batean irudikatu duzu nire sudurraren orinak. 

Nahikoa izan duzu behatzaren punta helarazteko maitasuna. 

Oh nere maitea galdu ez ote naiz zure ezpainen errepidean? 

Itsaso zara nire begietara 

jausi egin nuen behin, behin. 

Galdezka zirenei esan nien                               

egoteko lasai, 

pozik noa.

Ni banoa.


Uhhh… 

 

Zu, grabitatea, ekarri nauzu zure mundura. 

Zu, irla sekretua. 


Galdezka zirenei esan nien 

egoteko lasai, 

pozik noa. 

Ni banoa, bagoaz