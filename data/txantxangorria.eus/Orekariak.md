---
id: tx-1516
izenburua: Orekariak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1TFSnd1lF5Q
---

Orekariak (ft Juantxo Arakama)
from Orekariak by Skakeitan
Orekariak cover art

    Wishlist 

 
00:00 / 04:22
 
 

Includes unlimited streaming via the free Bandcamp app, plus high-quality download in MP3, FLAC and more.
 €1 EUR  or more

     
    Buy the Full Digital Album

lyrics
Itsasoak erantzunak balitu bezala
Isiluneek urratu dizute azala
Haizeak edonora eraman ditzala
Galdera guztiek sortutako itzalak

Agian zalantzek mantentzen dute oreka
Uraren dantzek indartzen diraute erreka
Nahiz eta bidean harrien kontra jo tarteka
Haizea sor liteke soilik hasperenka

Gau berberean ilun ta argiarekin dantza
Sartu ta azalean bizirik zaudela dioten zauri ta arantzak
Egun ta gauaren arteko balantza
Orekak askotan badauka kolore grisaren antza

Auskalo orain zein ote den erroa
Inoiz kiskali ezinik bilatuz beroa
Betiko soilik orekariak
Sokaren gainean eraiki nahian geroa

Ametsetan korrika nagoela izar bat jarraitzen, ta harrapatzen naute,
mozkor itsuenaren antzera, piloto automatikoa ahaztu det ta sentitzen det
baino behin eta berriz eroriko ez banintz ziur nago ni zalantzen gerlari ezinezkoa nukeela nere muturrean izarra ikustea pausoka aldentzen den bakoitzean
Ametsetan korrika itsasora nihoan zuzen, olatu bakoitzak galdera ikur bat zeuken, zalantza tarten ondoratzen hasi nintzen, baino bela nenbilen hanka ta besok astintzen, ura tragatzen ikasten da igerian, bizitza honek jarri gaitu txuri ta beltzen erdian, sokaren dardaretan aurkitzen dunak saria badaki zer sentitzen dun orekariak.