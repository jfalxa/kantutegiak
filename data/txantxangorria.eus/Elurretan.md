---
id: tx-2270
izenburua: Elurretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YxcncpD4640
---

Dirudienez
Elurrak dena estaltzen duenean
Bideak desagertzen dizunean
Esango nizun
Zergatik izkutatzen zaren
Zergatik hain pozik
Urtarrilako elurraz
Hirea den lehengo argiaz
Abiatzen zera kaletik
Urtarrila hontan
Emeki-emeki urtzen ari da
Zauri gorriak erakusten
Baita ere aldatu ez diren bideak
Urtarrila hontan
Urtarrila hontan
Hurrengo elurtearte
Zulora itzuliko zara
Hurrengo elurtearte
Sekretu guztiak gordetzen
Hurrengo elurtearte
Urtarrila bat oroitzen
Hurrengo elurtearte
Kale ta teilatu txurien
Nanana...
Hurrengo elurtearte
Nanana...
Hurrengo elurtearte
Nanana...
Hurrengo elurtearte
Hurrengo elurtearte
Hurrengo elurtearte
Jarraituko nuke ni hola
Urtarrila hontan
Urtarrila hontan
Urtarrila hontan
Hurrengo elurtearte
Hurrengo elurtearte
Hurrengo elurtearte