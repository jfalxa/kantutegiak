---
id: tx-2095
izenburua: Egin Kontu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZWpDX6ZxzqQ
---

Egin kontu
Bost mila zazpiehun
kultura ezberdin
desagertu dira historian.
Esaten dutenez
aurrerabidea
elkar jan horretan
oinarritu da mendeetan zehar.
Beraz ez larritu
izan jakintsu
orpoa erakutsi
aurrerabideari.
Jarraitu egiozu
asko dakienari
behingoz utziozu
baldar izate horri.
Gabiltza hainbeste
nor gara gu
zer gara gu
euskotarrak gara gu
Ez dela egoki
egunak ematea
zilborrari begira
munduan zentroa bagina
Ez dela zilegi
insolidarioa ei da
traban ibiltzea
ezberdintasun kontuekin
Diru kontua dela
autokonplazentzia
atzerakoia
txokoan bizitzeko gogoa.
Gabitlza hainbeste
nor gara gu
zer gara gu
euskotarrak gara gu
Ez baita ahaztu behar
kultura guzti horiek
zirela traba hutsa
delako progreso horrentzat.
Ez baita ahaztu behar
bost mila kultura
desagertu horiek
ezin dira okerturik egon.