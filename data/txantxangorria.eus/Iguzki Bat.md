---
id: tx-2271
izenburua: Iguzki Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZGRS9pM1Kmw
---

Oi ene maitia
Gaur loreak dantzan
Ihintzak sorta kantuz, belaien erdian

Oi ene maitia
Ene ametsen iturri
Maitasun uhainetan zurekin igeri

Iguzki bat, zara maitia
Iguzki bat, zara maitia

Oi ene maitia
Udaberri betean
Bihotzaren taupadak, kresalaren soinutan

Eta uda gorrian
Larruak berotzean
Arnasaren gezala, haize fresko idurian

Iguzki bat, zara maitia
Iguzki bat, zara maitia

Oi ene maitia
Udazken garaietan
Hego haizea lagun egal egin dezagun

Ta negu ondarrean
Itsas amari begira
Ametsez beztiturik, amodioz berotua

Iguzki bat, zara maitia
Iguzki bat, zara maitia

Oi ene maitia
Nire zuhaitz Itzaltsua
Beti goxo zurekin, zu gabe daukat min

Oi ene maitia
Ezti gozo epela
Berriz xista nezazu, maitasunaren erlea

Iguzki bat, zara maitia
Iguzki bat, zara maitia