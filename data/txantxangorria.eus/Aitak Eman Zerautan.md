---
id: tx-2796
izenburua: Aitak Eman Zerautan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SXFIwFkh9Ak
---

Aitak eman zerautan zerurat juaitian
Enetzat mahastia hartzeko lanean;
Artha nezala,
Aitak bezala,
Haintzur eta jorra,
Laguntzat hor nuela... sotoko ophorra.

Etxe xuri ttipi bat, badut mahastian,
Sagar, udare, phiko, mertxiken artean,
Etx'aitzinean,
Loren erdian,
Baratze alhorra,
Dutxuluaren pean... sotoko ophorra.

Mahastian xoriek goiz dute kantatzen;
Orduan bozkarioz lanari naiz lotzen;
Gero zait hasten
Bihotza hozten,
Agortzen zintzurra,
Laster dut eskuratzen... sotoko ophorra.

Ez dut hiritar gisan gosari ttikia:
Bixkotxatto batekin edari bixia;
Nik dut ogia
Axal gorria,
Bai gasna gogorra,
Eta arno xuritik... sotoko ophorra.

Neguan ez da deusik eder mahastian.
Aldaxketan molkoak agertu artian,
Ikhusiz huntan,
Osto artean
Larritzen pikorra,
Nola ahantz dezaket... sotoko ophorra.

Igandetan debotki meza dut entzuten,
Gero lagun enekin plazetan jostatzen.
Han dut aditzen
Nitaz erraiten:
«Zer muthil gotorra!
Ez ahal dik hastio... sotoko ophorra».

Gorphutza baitut sano, arpegia gorri
Hortako dute nitaz zerbait erran nahi,
Bainan ez naiz ni
Nihoiz ibili
Oraino moxkorra,
Izanagatik maite... sotoko ophorra.

Badut moltsa luza bat urhez hanpatua,
Hal'ere ezkontzeko lotsaz gelditua,
Beldur bainintzen
Gerta zadien
Andrea mokorra, 
Eta hauts ostikoka... sotoko ophorra.

Ene gazte lagunak ezkondu direnak,
Haurrez betheak dire orai gehienak;
Balute ere
Bertze hainbertze,
Ez naiz jeloskorra,
Haur hainitz bezain on dut... sotoko ophorra.

Herriko zahar batzuk, sotorat ethorriz,
Han idukitzen naute lehertua irriz,
Konkorrek kanta,
Mainguek dantza,
Oihuka elkhorra...
Biba! zaharrak eta... sotoko ophorra.

Burlaka hasten baita artzaina menditik,
Zaharrek errepostu mahasti gainetik:
To, edantzak hik,
Xirripakotik
Gibel aldez gora,
Guk husteko denboran... sotoko ophorra.

Ophor hori ez baitzen andrentzat egina,
Hekier dut eskaintzen gathelutto bana;
Hastio naute
Erraiten dute:
Donado zingorra!
Xehaturen ahal zaik... sotoko ophorra.

Bertsu hauk egitean iphar-goxoa zen
Sotoan hari nintzen arnoen aldatzen;
Donado hemen
Zein ongi naizen,
Egin dut aithorra,
Tente xutik aldean... sotoko ophorra.