---
id: tx-2134
izenburua: Heriotz Min
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I6OPKVBqc8A
---

Antxeten hegadak damaio negar haizeari!
Aparrezko zotina dario uhinari,
neguminezko bihotz puskatuan ilun-gogozko etsipenean.

Antxeten hegadak damaio hauspo negarrari!
Lekeitioko kai gainetan kresalezko hozkirria senti dugu,
geure barnerako aizto gisa zorion urragarri herio-itzal.

Itxaropenezko damuz bizitzan bizi zinen bizi,
heriotzean bizi izango zara, Santi.

Atxeten hegadak damaio bizitza zure heriotzari!
Putreen olatuek erain zaituzte herriaren olatu nekaezinek,
beren kolko beroskan Atotsi, gure branka hilenti hilezkor.

Agur eta ohore, kapitain zaharra,
itsasontzia uretan da, marinelak prest dira.