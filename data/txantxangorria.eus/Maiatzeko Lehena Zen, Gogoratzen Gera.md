---
id: tx-1287
izenburua: Maiatzeko Lehena Zen, Gogoratzen Gera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1MjQ98p7gwk
---

(maiatzeko lehena zen, gogoratzen gera:
"carnet de identidad he dicho"
gogoratzen geranez...)

... izenez ez naiz nehor
aiton semeetarikorik ez.
Artoaren berdea daramat jaiotzez.
Aitaren ez ziren soroetan sortu ninduten.
Ene arbasoak bezala,
maizterra naiz sorkundez.
Artoaren nekeak narama.

Ez gehiago galda mesedez,
ez gehiago galda ene izenez.

... abizenez ez naiz ezen
baina izardiaren bustia daramat jaiotzez.
Ene ez ziren soroek landu ninduten.
Ene aita bezala, nekazari naiz nekez.
Izardiak bataiatu nau.

... soro ta mendi ez ziren ene
handi mendietarikoa ez
uztaren zai artajorraketak konfirmatu nau.
Golde, gurdi ta baserri kendu zidaten.
Ene hauzokoa bezala,
kaleko egin naute,
behekotasunarekin batera.

...tximini luzearen sahetsean
ene izardi berdea egosten ari zatzait
garia edo azeituna utzarazi ziotenekin batera.
Kastellanoa bezala,
salgai gaituzte:
IZARDIAREN PREZIO BERDINEKOAK GERA.

... tornu, fresa ta beste lanabes,
langileetarikoa beharrez
gorritzen ari naute beren erabiltzez,
garietatik edo azeitunetatik datorrenekin batera.
Lankidea bezala,
leinu berdinekoa gaituzte.
BATASUNA DA GURE NORTASUN AGIRIA

Ez gehiago galda mesedez,
ez gehiago galda GURE izenez.