---
id: tx-3010
izenburua: Handia Izanen Naizenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ynd8qewAmjQ
---

Handia izanen naizenean, ama
eskailera bat eginen dut
zeruraino bezain gorakoa
izarrak biltzera joateko.

Boltsikoak izarrez eta
kometaz beterik jetsiko naiz
eskolako lagunen artean,
banan banan banatzera

Baina zuri, amatxo
hilargi betea ekarriko dizut
argi indarrik gastatu gabe
etxea argi dezazun.