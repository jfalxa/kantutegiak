---
id: tx-2082
izenburua: Zure Ingurua -
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LxkV0VCqLbE
---

Begira ezazu zure ingurura
eta esaidazu horrelakoa den
nahi duzun mundua
espabilatu garaia duzu eta
eskuratzeko dagoeneko
galdutako guztia
gazte berri, neoliberal
gazte burges, gazte konforme
zure mutur aurrean 
jokatzen ari dira
gazte global, antiradikal
pasiboa, positiboa
hutsaren aurrena
zu zeu zeu zara
ez egin so zure zilborrari
jokoan ez zaude bakarrik
banatu indarra eta hor eutsi
gogor eutsi, gogor eutsi!!
horri atxeki, horri atxeki!!
eralda ezazu zure ingurua
benetan ziur zaude 
horrelakoa den
nahi duzun gizartea
gauzatu jada, ahulen mendekua
behingoz argi uzteko
gazteriarekiko begirunea
oraina jada iragan da
etorkizuna jokoan.