---
id: tx-1923
izenburua: Agortu Dira Hitzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6LErLlqEOfg
---

Zenbat aldiz kalean, bihotza besoetan
begietan sua ta belaunak zailenetan
irteera baten bila, noraezean
zakur haiei hortzak erakutsi nahiean

Zoritxarrez guretzat, zorionez denontzat,
azkeneraino zoaz.
Barruan daukazuna libratzeko unea da,
aupurtzeko unea da.

MAITASUNA, BORROKA
OIHUAK, ERANTZUNA
GARAI ONAK, GOGORRAK
ASKATASUNA ZURE
ARGIA TA BIDEA
ZURE INDARRA GUREA

Beldurraren kateak hautsi dituzu
hutsik dauden gezur denak garaitu dituzu
hemen izango zaitut, hemen izango nauzu
nire miresmena ta babesa badaukazu

Jaioa zara ta orain ez begiratu atzera,
ez itsutu bidean.
Hitzak agortu dira, heldu gogor bizitza
eutsi goian laguna