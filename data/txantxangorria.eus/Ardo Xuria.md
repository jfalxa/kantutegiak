---
id: tx-2616
izenburua: Ardo Xuria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JuUV4QIDdZc
---

Kolpü bat edan, beste bat edan
Gure gizona trenpian.
Kolpü bat edan, beste bat edan
Gure gizona lürrian.
Etxerakuan, ülün beltzian,
Dena trebeska bidian.
Etxeko bortan nurbait bada han,
Erratza beitü eskian.
Senar tristia, mozkorzalia,
Zertako dük emaztia ?
Etxekandere, begi ñabarra,
Ez zitela otoi kexa !
Berriz artino adixkidiak,
Gauden zabalik begiak !
Ezin xüti, ezin ibil, ezin ezagün lagüna
Ardo xuriak ezartzen dizü
Planta txarrian gizona (2)