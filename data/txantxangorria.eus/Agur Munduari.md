---
id: tx-862
izenburua: Agur Munduari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o1NDMfXmwRE
---

Goizeko ihintzari
Iguzkiak agurra
Nik ere munduari
Behako bat samurra

Elizatik airatu da
Argi zeinuaren danga
Etxetan argia piztu
Huna goiz alba ederra
Mendi zelai erreketan
Gau ihiziak zaluxko
Kontrabanda utzia du
Beldur da ukan papako !
Alor landa baztarretan
Bide haunditan bezala
Jendea lanera doa
Zer jan ta edan bertzela ?

Jendea lanera doa
Zer janta edan bertzela ?
Zenbat jendek dute egun
Zoriona jastatuko ?
Nun dira haurrak sorturen
Nun bertzeak itzaliko ?