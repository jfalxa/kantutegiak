---
id: tx-763
izenburua: Non Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wDLBmgLXnug
---

Non gara? (Nerea Elustondo Plazaola)
Eguzkia gorde da
uhin urdinetan …
bizi ote gaitezke
iragan minetan?
Atzo etorkizuna,
bihar iragana,
pozen eta minen zama
dena haizeak darama,
darama, dena haizeak, darama, darama,
dena haizeak darama.
Lainoetan igeri
itsas bete arrain …
bizi ote gaitezke
datorrenaren zain?
Atzo etorkizuna,
bihar iragana,
pozen eta minen zama
dena haizeak darama,
darama, dena haizeak, darama, darama,
dena haizeak darama.
Ortzadarrak zeruan
hartu du atseden …
biziko ote gara
orain eta hemen?
Atzo etorkizuna….