---
id: tx-1495
izenburua: Zortzi Orduko Ekaitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CX1yUEERDto
---

Zeruan hodeiak daude
itsasoan txalupak
zapatak gustiz bustiak
gorputz osoa dardaka

Neguko eguraldia dago
eta uda oraintxe hasi da
trumoiak sumatzen dira
itsasertze honetan

Hemendik ikusten zaitut
etortzen zaren moduan
beldurra ematen didazu
ikusten zaitudanean.

Hondarraz beteta nago
nonbait txukundu beharra
goitik behera bustia
euriaren errua da

Haizeak erakusten digu
bere indar guztia
tximistak marrazkiak dira
ekaitzen bakardadean.

Hemendik ikusten zaitut
etortzen zaren moduan
beldurra ematen didazu
ikusten zaitudanean.