---
id: tx-1953
izenburua: Nora Zoaz Bakarrik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QcQZkpnB_2E
---

nora zoaz bakarrik, 
eder galant puska hori?
- iturrira, nahi baduzu etorri.
- goazen ba elkarrekin...


gerritik heldu, 
eta goazen ikustera 
iturrian zer dagoen...
goazen lehenbailehen, 
heldu eta goazen,
ikustera iturrian zer dagoen!