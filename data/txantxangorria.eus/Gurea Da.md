---
id: tx-2658
izenburua: Gurea Da
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ngenqLkxK48
---

letra eta airea: Manex Pagola
Gurea da

Gurea da
Etxea.
Gurea da
Herria.

I
Guk maite dugu euskal etxea,
Nola xoriek ohantzea,
Guk maite dugu Eskual-Herria,
Gure sor-Herri bezala.

II
Etxean ginen ait'amekilan,
Gu denak bizi goxoki.
Mahain berean, goiz aratsetan,
Han ginen denak bilduki.

III
Gure Herrian herioaren
Itzal beltza da agertzen,
Mutil neska gazteak baitzauzku
Herritik urrun joaten.

IV
Noiz ote zauku Euskal Herrira
Primadera etorriko?
Gaztek ez badute axolarik
Negua zauku sartuko!