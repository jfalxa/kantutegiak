---
id: tx-3197
izenburua: Beti Eskamak Kentzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hzhjk9pP1Zw
---

Beti ezkamak kentzen, kentzen, 
kentzen, kentzen, kentzen, kentzen. 
Beti ezkamak kentzen, kentzen, 
kentzen, kentzen, kentzen.
Zer gara gu?, nor gara gu? 
Euskotarrak gara gu.