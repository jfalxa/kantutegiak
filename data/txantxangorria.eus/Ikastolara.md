---
id: tx-2555
izenburua: Ikastolara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YfBl_QYVAz8
---

Goiz goizetik goaz ikastolara, ikastolara.
Goiz goizetik gagoz gu ikastolan, gu ikastolan.
Lan ta lan, lan ta lan, lan ta lan, lan ta lan.
Lan ta lan, lan ta lan, lan ta lan, lan ta lan.
Neska-mutikoak beti lan ta lan, beti lan ta lan.
Goiz goizetik gagoz gu ikastolan, gu ikastolan.
Lan ta lan, lan ta lan, lan ta lan, lan ta lan.
Lan ta lan, lan ta lan, lan ta lan, lan ta lan.