---
id: tx-2516
izenburua: Boom!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_xqb1WDCNiM
---

Boom! Deustuko Eztanda taldearen lehen abestia, 2017ko udan sortua. Espero dugu gustatzea!! Laister abesti gehiago Instagram: @eztandataldea Posta elektronikoa: eztandadeusto@gmail.com Letra: Aupa, zelan talde? Euskal Herria gure herrialde Gu deustutik gatoz Euskararen alde Egiten dugu zuentzat Rapa debalde Adi txabal! Gu gara eztanda 48014-ko original banda 2017 heldu da gure txanda Entzun gure flow hau baina ez sofan etzanda ENTZUNDA BOOM! Eztandaren ordua hau da zuei eskerrak emateko modua Eskerrik asko Deustuko jende guztia Gogoratu hau dela deustuko herria! (Instrumental) Benga etorri gurekin Rapa abestera Badakigu zer egin Zuekin batera Bizitzari ekin Atera kalera Eztandarekin Juerga botatzera Hementxe gaude Nosotros somos cinco Euskal rapa Mantentzen dugu tinko Gu hasi gara, zero partitura Musika korrontea eta eztanda ura Etorri denak gurekin deustura Batera egoten da oso gustura A ver como te lo digo Que yo quiero estar contigo Come on eztanda bolumena igo! ENTZUNDA BOOM! Eztandaren ordua hau da zuei eskerrak emateko modua Eskerrik asko Deustuko jende guztia Gogoratu hau dela deustuko herria!