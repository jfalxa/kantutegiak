---
id: tx-2242
izenburua: Gorlizko Balkoitik Balkoira Ibiltaria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/edB6zQtCl9s
---

2020ko abuztuaren 7an Gorlizen lehenengo aldiz eginiko "Balkoitik balkoira ibiltaria" ekimena.