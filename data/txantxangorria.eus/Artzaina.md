---
id: tx-102
izenburua: Artzaina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Od_uEfFzCSs
---

Artzaina 
Azkorri eztia da sortia, orai argi elkitzen ekia, 
Berotzez beitü jaixten mendia. 
Gozoki titzü ene sankuak, ürixkitzen bazkagia mardulak, Urhats bakotxak hantzen belharrak. 
Artzaina dügü heben nausea, igaraiten beitü irus bizia, Egiten beitü bere nahia. 
Goxo zait aizearen ballaka, zuinen ezti txarpoillan ürrina, Eta ezpelaren berde ederra. 
Xenda hegian düzü artzaina, bazterrer so errege bezala, Zankapean etzanik txakürra. 
Hürrün, büriak gora, zaldiak; ordokian esnaurtüz behiak, Mendi paitean alhan ardiak. 
Üngüratzez bortüko bazterrak, ezta bekaitx ikusiz besteak, Gogo-bihotzak dütü beteak. 
Hitzak: Junes CASENAVE-HARIGILE 
Müsika: Jean ETCHART