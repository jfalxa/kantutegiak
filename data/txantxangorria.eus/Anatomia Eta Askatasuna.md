---
id: tx-3302
izenburua: Anatomia Eta Askatasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l7mIBYKyEpU
---

Letra: Amets Arzallus
Musika eta konponketak: Haimar Arejita
Ahotsak: Eñaut Elorrieta eta Gari
Gitarrak: Haimar Arejita
Baxua: Igor Arzanegi
Bateria eta perkusioa: Jon Fresko
Saxo baritonoa eta tenorra: Ander Ertzilla
Tronpeta: L. Esteban Valcarcel
Soinu teknikaria eta masteringa: Iñigo Etxebarrieta
Ekoizpena: Haimar Arejita
Grabaketa, nahasketa eta masterizazioa: Muxik on, (Mungia, Bizkaia )
Bideoklipa
Irudia: Markel Urrutia, Hodei Ensunza eta Oier Plaza

Vigon bi begi, 
Fleuryn beste bi; 
giltzurrun bat Ourensen 
bestea Caceresen
erdi usteltzen
Azazkal bat Sorian, 
orpoa Segovian, 
belaun bat Martutenen 
ta birikak Teruelen. 
Hain aparte, 
eta nork daki noiz arte.
Anatomia dispertsatu bat 
esperantzaren mapa bezala, 
toles gaitezen elkarrengana,
 izan gaitezen bat.
Perpinyanen zortzi agin, 
Ocañan bekainak berriz. 
Estamu huts bat eta azken botila, 
gora Sevilla!
Zenbat gorputz zati hautsi dauden Herreran, 
zenbat Fresnesen, 
zenbat Zueran. 
Zatoz nere bihotza, 
batu hezurrak eta goazen hegan.
Soto del Real-Sevilla-Fleury-Alicante-Paris 
zenbat zauri Navalcarnero-Alcala-Puerto; 
zenbat zauri Lyon-Algeciras-Basauri, zenbat zauri.


Izan gaitezen.

Izan gaitezen.

Toles gaitezen.

Toles gaitezen.

Toles gaitezen.