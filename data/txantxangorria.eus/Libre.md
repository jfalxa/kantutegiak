---
id: tx-1822
izenburua: Libre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jGuslbr5ZoI
---

Musika: Roger Daltrey
Hitzak: Xabier Saldias

LIBRE! 
Bizi nahi kate gabe!
LIBRE! 
ASKE BAI!
LOTUTA EZ!

Nik ez dut nahi
zenbat jendek
jarritako
hainbat lege
ez nago ados
lege hauek
ezarriak
beti indarrez.

Nik ez dut nahi
aintzinatik
jarritako
Jaungoikorik
sinesmen bat
da alferrik
ikertzeke
onarturik.

Kate hauen kontra
loturen aurka
nik deihadarra.

Kate hauen kontra
loturen aurka
deihadarrez nik