---
id: tx-2816
izenburua: Ama Euskadi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wZVCwppHQ7w
---

Zelütik jinik dei sühar bat, loetarik erauztera,
   Amari bi koblen huntzeko bortxatü niz sinestera,
   eta ez niz segür herabe Ama zuri kantatzera, 
   hain beitzira andere handi, ümila eta ejerra.

   Bedatseko lehen beroetan, Ori delarik berdatzen,
   harrotürik artzainak dira petik gora abiatzen,
   beha itzazü kantan ari zure lürraren laidatzen,
   zuri düen amodioa ez dizüe ezabatzen.

   Ama egün xiberotar bat, düzü zure zerbütxari,
   nahiz zuri bestimenta bat paregabeko merezi,
   artzain batek eskentzen deizü, Ori bortüan da bizi,  
   egün oroz goguan zütü, oi, gaixo Ama Euskadi.


   (Hitzak: Pierre Bordazarre, Etxahun-Iruri)