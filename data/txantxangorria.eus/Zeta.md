---
id: tx-1942
izenburua: Zeta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V7tRXVLrua8
---

Letra: Alaia Martin
Musika: Hutsa

Begiez, begi-niniez
xuxurlatuz : ni ez
nintzen itsultzen lamiez
bihotza puzteko legamiez
nik beti egiten nuen ihes
-ilbera gauak betez hilez
kresaldutako ilez
itsu-makilez.

Txirlazko sandaliez
ihes, zauriez....

Eta batzutan eguzkia estaltzeko
tapatzen ditugu begiak
barrua erretzen digulako bere aurpegiak.
Eta gordetzen ditugu promesak,egiak
ez daitezen izan benetazkoegiak
benetazko egiak.

Itsaso bat adibidez
ezin askatu algez, sustraiez
nahi ez duelako , nahiz egon nahiez.
Maitemindu dena itsaspeko paisaiez
eta ja berdin dio, bai, ez , bai, ez,.....

Txirlazko sandaliez
ihes, zauriez....

Eta batzutan eguzkia estaltzeko
tapatzen ditugu begiak
barrua erretzen digulako bere aurpegiak.
Eta gordetzen ditugu promesak,egiak
ez daitezen izan benetazkoegiak
benetazko egiak.

Zerua saretuta, zeta
tximistez eta
zu-ren zetaz beteta
laino bat hori baita, zeta
Amaieren poeta
hau da azken letra...

Eta batzutan eguzkia estaltzeko
tapatzen ditugu begiak
barrua erretzen digulako bere aurpegiak.
Eta gordetzen ditugu promesak,egiak
ez daitezen izan benetazkoegiak
benetazko egiak.