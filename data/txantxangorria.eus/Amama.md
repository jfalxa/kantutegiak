---
id: tx-3034
izenburua: Amama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zTP-WxIK5Dc
---

Gerra osteko sasoian gertatu ziranak
Kontau euskuzan baten Sestaoko amamak
Preso luze emon eban bere aita zanak
Istoriak ikaratu gintuezan danak

 Bilboko espetxean bere aitak urte bi
Egunero joakola bisitan aitteri
Hara arte heltzeko bere galdu egun erdi
Baina askatu artean holan behar segi

 Akabun jeneralak antza eban parkau
Eta horretarako gutuna idatzi dau
Baina papel ziztriña belu zan ailegau
Bidean galdu ebazan hiru aste edo lau
ta heldu orduko aita fusilatuta dau

 Enterratzeko bere falta da dirua
Prestamistara joan ta hau bere karua
Halan be ailega jakon hiletan ordua
Samurra ez eban euki hak bizimodua

 Amamak esan eban daula parkatuta
Hori bertute dala ezin jako uka
istoria ez daiten lotu denboran galduta
ez daiguzan gureak itzi hor ahaztuta.

 Akabun jeneralak antza eban parkau
Eta horretarako gutuna idatzi dau
Baina papel ziztriña belu zan ailegau
Bidean galdu ebazan hiru aste edo lau

 Amamak esan eban daula parkatuta
Hori bertute dala ezin jako uka.
Istoria ez daiten lotu denboran galduta
Ez daiguzan gureak itzi hor ahaztuta
Ez daitezela lotu guzurrez ostuta