---
id: tx-610
izenburua: Lekandapean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sOVvtxZn1Oo
---

Lekandapean urduri
Otsoek hainbat ulubi,
Herri bakarra bagara ere
Ikusten dodaz mundu bi.
Eztartana zagarie, itzilespitso argie
Batzuentzako gezurre dana,
Besteentzako egie.
Saguek hona ta hara,
Ustu deuzkue ganbara.
Lapur handiek kalean dabiz,
Ta txikiek kartzelara.
Bele baltzak sosoari
Sarri dabilko kantari
Nozarte egon beharko dogu, tirekail kanpaiari.
Kalean orbela
Erortzen, erortzen
Betazpia bentanarean
Haizeak atean, sirikatsen
Eta orbela jausten, jausten
JAUSTEN.
Munduan kukuak, kuku
Eta oilarrak, kukurru.
Berba ein behar dogun unean
Zegaitik gagoz gu mutu.
Mirue dau pikondoan
Ta katue sutondoan.
Urrin dauena noiz egongo da
Maite dauenan alboan.
Animalien arten, bizi gura dot bakean
Inoren menpe egongo ez dan
ABERRI ASKE batean.
Hodei artean marmarrak
Urrun ioakuz antzarak
Eurek eroan daiezala
Gure pena ta negarra.
Kalean orbela erortzen, erortzen
Betazpia bentanarean
Haizea atean zirikatzen
Eta orbela jausten, jausten
JAUSTEN.