---
id: tx-443
izenburua: Kendu Gorbata
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/P00un1Q5Ryg
---

[EUS] Kendu Gorbata, The Clayton taldearen lehen diskako aurrerapen kanta.

Sare sozialak / Redes sociales

Muntaia / Montaje: Mikel Ullibarriarana eta Maite Ponferrada 
Grabaketa eta masterizazioa / Mezcla y master: Silver Recordings 
Kolaborazioa / Colaboración: Ane eta Maialen (Perlata) 


Hitzak:

Nor ote dabil ate joka goizeko ordu txikienetan, 
ez dut inoren beharrik behar dudan arren barrenean.
Utzi nazazu lurrean!
Erorketaren zapore mingotsa da begiraden iluntasunaren eragile handiena.

Erakutsi hortzak bizitzari hark bizkarra ematen dizunean. 
Argitu zalantzak, izarren hautsik gabeko ametsak posible dira.
Erakutsi hortzak bizitzari! Izaera non da?
Arnasa hartzen hasten ikasteko, kendu gorbata.

Zalantzen itzalak itzultzean egunsentiko eguzkipean,
Ez dut egingo negarrik behar dudan arren barrenean.
Izaren epeltasunean egutegia menpekotasuna da,
begiraden iluntasunaren eragile handiena.

Erakutsi hortzak bizitzari hark bizkarra ematen dizunean. 
Argitu zalantzak, izarren hautsik gabeko ametsak posible dira.
Erakutsi hortzak bizitzari! Izaera non da?
Arnasa hartzen hasten ikasteko!

Erakutsi hortzak bizitzari hark bizkarra ematen dizunean. 
Argitu zalantzak, izarren hautsik gabeko ametsak posible dira.
Erakutsi hortzak bizitzari! Izaera non da?
Arnasa hartzen hasten ikasteko, kendu gorbata.