---
id: tx-1781
izenburua: Juana Bixenta Olabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rrfdw1_AObI
---

Juana Bixenta Olabe
 
          1
    J. Bixenta:
Nagusi jauna, auxen da lana,
amak bigaltzen nau berorrengana.
    N. jauna:
Ai, bigaldubo bazinduke maiz!
Zu ikusita konsolatzen naiz.
            Xinista zazu
oso zoraturikan nakazu!
Orren polita nola zera zu?
 
          2
    J. Bixenta:
Amak esan dit, etxeko errenta
pagatzeko dirurik ez zuben-ta,
ia egingo diyon mesere,
illabetian geienaz ere,
            itxogotia; (sic)
bara zierto dala ematia
pasa baño len illabetia.
 
          3
    N. jauna:
Logratutzia, errex alare,
naiz ori ta beste zernai gauza're,
seguru dala esan amari,
zu baldiñ bazatoz mandatari;
            dudarik gabe,
nere borondatiaren jabe
zu zera, Juana Bixenta Olabe.
 
          4
    J. Bixenta:
Beraz bertatik nua etxera
poz aundi bat amari ematera;
oraiñ ariyo, nagusi jauna,
presaka juan biat amagana.
    N.jauna:
            Ez zuazela;
portatu zaite nik nai bezela,
gaur etxera juan ez zaitezela.
 
          5
Zuben etxia dago irruti,
birian zer gerta izan bildurti;
bara iya pasa da eguna,
arratsa berriz dator illuna;
            oraiñ juatia
txartzat iruki, nere maitia;
gaur emen bertan gera zaitia.
 
          6
Ez juan etxera, bire oietan,
gabaz, bakarrik eta illunbetan;
kontura erori zaitia zu:
gabaz illunbian juan biazu
            peligro aundiz,
eta obe dezu eun aldiz
biyar goizian juan egun argiz.
 
          7
    J. Bixenta:
Ezetz etsiya egon liteke,
ni emen gelditu eziñ niteke;
amak espero nau gaberako,
eta ark espero naubelako,
            juan biar det nik;
alaba ikusi bagetanik
amak izango ez luke onik.
 
          8
    N. jauna:
Emen zuretzat dira parako
jaki txit goxuak afaitarako;
erariya berriz, —Jaungoikua!—,
illak piztutzeko modukua
            ardo onduba,
ontzen urtietan egonduba,
Malaga'tikan da bigalduba.
 
          9
    J. Bixenta:
Jauna, berorrek, bañan alferrik,
badaki armatzen lazo ederrik;
lazua eiz bage gera dediñ,
nere etxian nai ditut egiñ
            afaldu ta lo;
naiago ditut ango bi talo,
ta ez emengo milla erregalo.
 
          10
    N. jauna:
Biyotz nereko dama polita,
asmoz muda zaite, auxen tori-ta
amasei duroko urre fiña,
sendatu nai zazun nere miña,
    J. Bixenta:
            Ez, jauna, ez, ez!
Merezi beziñ kopeta beltzez,
esango diyot ezetz ta ezetz!
 
          11
    N. jauna:
Gañera berriz, Juana Bixenta,
utziko dizutet etxeko errenta
kito zor dirazuten guziya,
kunplitzen banazu kutiziya.
    J. Bixenta:
            Lotsa gogorra!!
Sufritzen dago gaur nere onra,
penaz malkuak darizkit orra!!
 
          12
    N. jauna:
Nik ditut kulpak, ez egiñ negar,
orlakorik ez nizun esan biar;
animan sentitutzen det miña
zuri ofensa ori egiña.
            Maldiziyua!
Ez dakit nun nekan juiziyua,
eskatzen dizut barkaziyua!
 
          13
    J. Bixenta:
Barkaziyua, du-ta eskatu,
nere biyotz onak eziñ ukatu;
erakutsi ditalako, jauna,
gaitz egiñaren damutasuna,
            konforme nago;
ez egiñ-ta ofensa geiago,
gaurkua oso aztutzat dago.
 
          14
    N. jauna:
Berriz nik egiñ zuri ofensa?
Arren orlakorik ez bara pensa!
Zaukazkit neskatxa fiñ batetzat,
eta gusto aundiz emaztetzat
            artuko zaitut;
biyotzetikan esaten dizut,
zuretzat ona baderizkizut.
 
          15
    J. Bixenta:
Ori egiyaz baldiñ badiyo,
neregana egon liteke fiyo;
bañan usatubaz kortesiya,
ama neriari lizentziya
            eska bezaio.
    N. jauna:
Ori gustora egingo zaio,
biyar goizian juango naitzaio.
 
          16
    J. Bixenta:
Banua oraiñ.
    N. jauna:
Atoz onuntza,
birerako artu zazu laguntza.
Adi zazu, morroi Joxe Juakiñ
etxera farolan argiyakiñ
            aingeru oni
gaur zuk laguntzia da komeni,
kezketan egon ez nariyen ni.
 
          17
    Morroiak:
Aingeru ari lagundu eta
orra, jauna, egiñ ostera buelta;
birian, bai, bildurtxo zan bera,
bañan allegatu-ta etxera
            kontentu zegon;
oraiñ kezketan ez du zer egon,
oiera juan ta pasatu gau on.
 
          18
Eldu zanian urrengo goiza,
nagusiyak zuben kunplitu itza
juan zitzaion amari etxera,
andretzat alaba eskatzera;
            amak txit firme
artu ta gero milla informe,
gelditu ziran biyak konforme.
 
          19
Andik zortzi bat egunetara,
edo amar ziran ez dakit bara;
ez, amar eziñ zitezken izan,
andik zortzi egunera elizan
            ezkondu ziran;
ezkondu ta etxera segiran,
jendia franko bazuten jiran.
 
          20
Bost illabete edo sei ontan
ez dirade beti egondu lotan;
eman dute zenbait jirabira
eta gozatutzen bizi dira
            ditxak ausarki;
obeto eziñ litezke arki,
espero dute aurtxo bat aurki.
 
          21
Dama gaztiak, ez egon lotan,
begira zaitezte ispillu ontan
gustatuta birtute ederra,
andretzat artu bere maizterra
            du nagusiyak;
orla irabaztera graziyak
saia zaitezte beste guziyak.