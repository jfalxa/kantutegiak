---
id: tx-1913
izenburua: Apurtu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qvLjWi_EYSE
---

Ordulariaren orratzak
aurrera doaz
egunero daukagu
euskaldun bat bahituta
eusko gudaria ala
intsumitua
presondegi barruan
gaizki tratatuak.
Zure ideietan
jaiotzeagatik
zurea den lurra
babesteagatik
Euskadiren aurka doan
edozer gauzak
izan beharra dauka
guztiz apurtua.

Ideia garbi eduki
euskarak behar du irabazi
nagusitzen denean
lortuko dugu Euskadi.
Gogor eutsi behar dugu
gure nahiak defendatuz
Euskal Herri bahitu bat
behar delako askatua

Kate guztiak apurtu!
Muga guztiak apurtu!

Euskara nagusitzeko
Euskal Herria askatzeko.

Irtenbidea dago
hortaz ziur nago
edozein arazo
beti konpontzeko
interesik ez bada
arazoa konpontzeko
bakerik ez da egongo
etsaiarentzako.
Ulertu baduzu
nire adierazpenak
hobe duzu kontutan
edukitzea,
herri bat gara
independentea
beste asko daude
gu bezala munduan.

Ideia garbi eduki
euskarak behar du irabazi
nagusitzen denean
lortuko dugu Euskadi.
Gogor eutsi behar dugu
gure nahiak defendatuz
Euskal Herri bahitu bat
behar delako askatua

Kate guztiak apurtu!
Muga guztiak apurtu!

Euskara nagusitzeko
Euskal Herria askatzeko.

Bai Euskal Herria askatzeko
loturik dugu eta.

Ez gara espainolak
inondik inora
ez gara frantsesak
ez ahal zaizu axola?
Baina nik ordea
hizkuntza bat
daukat hizketan
euskaldun bat naiz
Euskal Herrian jaiota.
Nire lirika da
euskal presentzia
beste jende bat gara
guztiz ezberdina
kultura batekin
musika batekin
guztiz egiten
gaituena ez berdin

Ideia garbi eduki
euskarak behar du irabazi
nagusitzen denean
lortuko dugu Euskadi.
Gogor eutsi behar dugu
gure nahiak defendatuz
Euskal Herri bahitu bat
behar delako askatua.

Kate guztiak apurtu!
Muga guztiak apurtu!

Euskara nagusitzeko
Euskal Herria askatzeko.