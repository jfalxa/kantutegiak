---
id: tx-3308
izenburua: Nere Herriko Neskatxa Maite
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HDL7qxG84Wk
---

Nere herriko neskatxa maite
ahozko lorez zaitut gaur laztantzen
itsaso garden, lur gozoko landare
kresalaren usain, zeru kolore
nere bihotzaren taupaden hotsez
zure grazia dut kantatzen.

Bihotz minberen egunsentia
herri sufrituaren lamia
ipuin zaharren piper eta eztia
erreka garbien kantu bitxia
udazken lizunez zaude jantzia
izar zerutarren irria.

Lanbro artetik itsas geldira
leunki zoazen txori airosa
amodiozko sentipenaren hatsa
zure ezpainetan loratuz doa
goizeko ihintzetan belardi zera
eguzkitan zilar dizdira.