---
id: tx-869
izenburua: Tximeleta Mendian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HB8FMfBmyO4
---

Imanol Urbietari omenaldi proiektua, CEINPROko 
3D Animazioak, Jokuak eta Multimedia Elkarreragileetako 
ikasleek garatuta.
Proyecto de homenaje a Imanol Urbieta de los alumnos de 1º del 
Ciclo Animaciones 3D, Juegos y Entornos Interactivos de CEINPRO


Tximeleta mendian
lore baten gainean,
hegoak geldi zeuzkan
gaixoturik zegoan.

Tximeleta bakarrik,
tximeleta tristerik.
Nork nahi luke sendatu
tximeletatxo hori?

Tximeleta mendian…

Lorez lore hor dabil
burrun-burrun erlea.
Ikusiko ote du
tximeleta gaixoa?

Tximeleta mendian…

Erleak ikusirik
tximeleta gaixorik,
“nik sendatuko zaitut”
dio tximeletari.

Tximeleta mendian
lore baten gainean.
Poz-pozik esan zion:
“mila esker erlea”.

Erlea badijoa
bere erlategira,
tximeletatxoarentzat
eztia ekartzera.

Tximeleta mendian…

Etorri zen erlea
ezti goxoz betea,
ezti hartatik jaten
tximeleta hasi zen.

Tximeleta mendian…

Bi hegotxoak laster
hasi ziren mugitzen.
Erleak tximeleta
eztiz sendatu zuen.

Tximeleta mendian
lore baten gainean.
Poz-pozik esan zion:
“mila esker erlea”.