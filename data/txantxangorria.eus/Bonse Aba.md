---
id: tx-2480
izenburua: Bonse Aba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eLc-KtJ27k8
---

Musika: Herrikoia (Zambia)
Letra: Koldo Celestino


Bonse aba mu pokelela
Bali pele maka
Akuba bana
Akuba bana
Akuba bana
Akuba bana
Kuba bana
Bakwa Lesa

Beltz, zuriak
marroi ta horiak,
denok gara euskaldunak.
Denok gara euskaldunak

Angolarrak,
ginearrak
eta baita
aslarrak,
herri hontan
bizi gara
ta euskaraz
mintzi gara.

Beltz, zuriak
marroi ta horiak,
denok gara euskaldunak.
Denok gara euskaldunak.

Batzuk bertan
jaioak eta
hainbat orain
etorriak.
Altu, baxu,
lodi, argal,
lur honetan 
batu gara.