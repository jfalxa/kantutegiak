---
id: tx-2267
izenburua: Oihal Puxka Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Bgux223WpxA
---

Oihal puska bat ez da aterpe,
Kanping denda bat ez da etxe,
Ihes-bidea ez da bide.
Guk bagenun bizi modua
Zoro zelaiak, ganadua
Orain guztia da galdua.
Guda txikirik ez omen da
Txiker batekin duen bultza
Oinazean dien zokora.
Inoiz bizitza izan liteke
Ohial pusketa baten pare
Han laburregi, sobera hemen
Urratu edo zulatua
Oihal denda bat ezin da izan
Haurrentzako bizi-lekua
Haurrentzako bizi-lekua
Mmmm
Gora beheratsua bizitza
Erauntsi ostean ekaitza
Inorenak lur eta hitza
Borroka ei da giza lege
Gerrarik eza, ezta bake
Ihes- bidea ez da bide
Oihal puska bat ez da aterpe
Kanping denda bat ez da etxe
Ihes- bidea ez da bide
Inoiz bizitza izan liteke
Oihal pusketa baten pare
Hasieran latz, leun ondoren
Urratu edo zulatua
Oihal denda bat ezin da izan
Haurrentzako bizi lekua
Haurrentzako bizi lekua
Mmmm