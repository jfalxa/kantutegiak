---
id: tx-2797
izenburua: Pilotarien Biltzarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AMeae9UncOM
---

Agur, agur, jente guzik, agur bihotzetik
Zaharrak eta gaztiak bildurik hurruntik
Euskaldun joko jaunari bere lur harrotik
Lili eder bada egun zabaltzen itzaldetik.

Gizon gaztentako beharra
Ta zaharren indarra,
Gora kanta obra ederra
Pilotarien biltzarra.

Gure jinko jaunak deizku ezari mundian
Txorik kantuz oihanian et’ardik mendian
Untzietan mariñelak itsaso erdian
Eta pilota maitea euskal-herri aldian.

Gizon…

Arbolarik ederrena oihanian haitxa
Aize handirenak dio ezterla ez tatxa
Euskaldun pilota horri ez besuak kutxa
Beti nausi izan dadin denak dezagun altxa.