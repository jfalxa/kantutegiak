---
id: tx-1228
izenburua: Ekar Ditzagun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/A6cbHicwtas
---

Gehiegi izan da denbora 
gehiegi sufrimendua 
gehiegi itxi beharreko zauria 
gehiegi dauden orbanak 
Baina azkenik iritsi da 
baina azkenik zabaldu da 
garaipenaren eguna 
ospatzeko irrintzia 

Gugandik urrun zeudenak 
etxean ditugun egun honetan 
ekar ditzagun gogora 
zeruan izar bat ez duzutenak 

Gehiegi izan da denbora 
gehiegi sufrimendua 
gehiegi itxi beharreko zauria 
gehiegi dauden orbanak 
Baina azkenik iritsi da 
baina azkenik zabaldu da 
garaipenaren eguna 
ospatzeko irrintzia 

Gugandik urrun zeudenak 
etxean ditugun egun honetan 
ekar ditzagun gogora 
zeruan izar bat ez duzutenak 
(3 aldiz) 

Preso eta iheslariak etxean 
ditugun egun honetan 
Ekar ditzagun gogora 
zeruan izar bat ez duzutenak 

Presoak Etxera!