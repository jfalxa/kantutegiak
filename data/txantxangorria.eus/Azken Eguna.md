---
id: tx-1651
izenburua: Azken Eguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z-ePEsFoMOk
---

Gu eta zu hemen, orain,
aurkitzen gara berriz,
agian azken aldiz,
ez dago galtzeko arriskurik...
Denborak dena alda dezake,
errukirik izan gabe;
ezin bere indarra gainditu inoiz.
Betirako zela uste baina;
betirako denik ez dago.
Hasten den guztia bukatzen da,
bukatzen dena dena desagertu..
gugan eragina du?
Badirudi...
Zer egin, zer esan
oraindik ezdakigu
ziur ze bide hartu
Hainbeste aldatu ote gara?
Garai hartan bizi genuena
gure barnean mantentzen da
erdi hildako oroitzapenen eran.
Aspaldin piztu genuen sua
bizirik mantzereik al da...
edo denbora den haize hotza
indarrez pasa da gainetik..
errautsa soilik utziz?
Badirudi