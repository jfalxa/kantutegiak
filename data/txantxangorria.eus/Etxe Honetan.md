---
id: tx-2387
izenburua: Etxe Honetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kt5X380JvbM
---

ETXE HONETAN (Hitzak eta musika: Josu Bergara / Konposizioa: Nerea Alberdi)

Barriro be lotuko gara,
sutondoan danok batera, 
- Olentzero noiz dator, ama? -
txikixen betiko galdera.

Gabon gabian alkartuta,
senide, herritar, lagunak,
etxe honetan sartzen dana,
etxekotzat hartuko da. 

Aintzina eitxen zan antzera,
hauxe da geure herriko ohitura,
badator urte amaiera,
min danak botaizuz sutara!

Geuretzat hauxe da handixena,
munduko altxorrik ederrena,
amumak jagon eban euskara,
gaur lobak eingo dau berbera.

**************************************
Trikitixa -   Gaizka Peñafiel 
Txistua  - Eduardo Aretxabaleta 
Ahotsa - Naroa Gaintza 
 Koroa: IEko kantu poteoko taldea: Nerea Muniozguren, Garbiñe Muniozguren, Zorione Benedicto, Itziar Benedicto, Irune Benedicto, Andoni Landa, Jose Mari Landa, Manu Fernandez, Ernesto Ramirez, Imanol Elorriaga, Ramon Lejardi, Esther Uriarte, Nerea Rodríguez, Marisol Agirre, Eduardo Aretxabaleta eta Agustín Bergara.


****************************************** 
Diskoa deskargatu: www.josubergara.eus 
Kontratazioa: info@taupaka.eus (+34) 688 840 713
CD-a erosi: info@josubergara.eus 

************************************ 
Josu Bergara musikariaren 5. estudioko lana "Asunak. Bide-bazterreko melodia sendagarriak" izenburuarekin argitaratu dena 2018ko maiatzaren 30ean, Taupaka Elkartearen zigilupean. 

*********************************** 
Ahotsa, gitarra eta musika: Josu Bergara 
Biolina eta konposizioa: Nerea Alberdi 
Pianoa: Mikel Nuñez 
Ahotsa: Irati Bilbao 
Gitarra: Lüi Young 
Kontrabaxua: Dani Gonzalez 
Bateria: Txema Arana 


******************************** 
Grabaketa: Tio Pete Estudioa - Unai Mimenza teknikaria 
Produkzioa eta edizioa: David Sanchez Damián - Bigorringo estudioa 
Masterizazioa: Ibon Larruzea - Euridia Estudioa