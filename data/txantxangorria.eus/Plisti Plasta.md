---
id: tx-2595
izenburua: Plisti Plasta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n7gtU10vBn0
---

Plisti-plasta, plisti-plaust
mela-mela eta blai-blai.
Plisti-plasta, plisti-plaust
mela-mela eta blai-blai.
Zaparrada, zipristin,
harri koskor eta buztin.
Plisti-plasta, plisti-plaust
mela-mela eta blai-blai.
Bota berri ta guzti
irten naiz etxetik,
gauez egin du eta
euria gogotik.
Putzutan ez sartzeko
nire amak esan dit.
Berak esandakoa
egin behar beti.