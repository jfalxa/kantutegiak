---
id: tx-1236
izenburua: Madrilgo Lagunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gxrQCJ44Y-E
---

www.hotsak.com


MADRILGO LAGUNAK
        Bitoriano Gandiaga , 1977
 
 
Guztitariko lagunak
nituen egin Madrilen;
guztitariko lagunak.
Ongi hartzen gaituzte
euskaldunok Madrilen.
 
                              Eskuineko zenbait lagun
                              egin nuen Madrilen;
                              eskuineko zenbait lagun.
                              Ezkerreko adiskide
                              zenbait ere egin nuen.
 
                              Guztitariko lagunak
                              nituen lagun Madrilen.
                              Guztiak haserretu
                              zaizkida bat baten.
                              Ez dut izan erraza
                              pasarte hau Madrilen.
 
                              Euskaldunok jende gisa
                              atsegin gara Madrilen,
                              baina ez euskaldun gisa;
                              ta hala haserretu ginen
                              euskal arazoz larriki
                              mintza bainintzaien.
                              Harrez gero agurra
                              ukatu zidaten
                              lehenengo eskuinekoek,
                              ezkerrekoek ondoren.
                              Ez dut izan gozoa
                              pasadizo hau Madrilen.
 
Gerora adiskidetu
izan gara noizbaiten,
baina oraindik ez dakit
zer arraiotan ez duten
euskal arazoz mintza nakien
aurpegi hobez onhartzen.