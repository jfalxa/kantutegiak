---
id: tx-211
izenburua: Zurea Eta Nirea Euskararen Bidea Kilometroak 1995
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ocoUwrVKECQ
---

Trikitixa: Izaro Garmendia 
Bateria: Mitxel Longaron 
Kitarra elektrikoa: Gurutz Bikuña.fi; 
Bajoa: Sindo Mellado 
Ahotsak: Herri Ametsa eta Zuniolako haurrak Xabier Saldias 

Argiak ura urrezta. 
Heldu da Ikastolen festa: aurten ere izan dadila 
jendez ta posez gainezka. 
Ez gaitu izutzen geroak. Gure bihotzak, beroak. 
Jaso ditzala Donostian 
euskarak bere hegoak! 
ZUREA ETA NIREA 
EUSKARAREN BIDEA 
Hogei ken bat, hemeretzi. Ez· dezagun inoiz etsi: 
euskarak behar gaitu eta euskarari zintzo eutsi. 
Basoan eder haritza. 
Ez dezagun, ez, gal hitza! Ate orotara moldatu 
gure hizkuntzaren giltza. 
ZUREA ETA NIREA 
EUSKARAREN BJDEA 
Erleak gozo eztia. 
Kito hemen abestia. 
Gora gure Kilometroak eta gora Donostia. 
ZUREA ETA NIREA 
EUSKARAREN BIDEA