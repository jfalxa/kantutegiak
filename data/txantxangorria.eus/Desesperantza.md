---
id: tx-282
izenburua: Desesperantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uFwgbRusOGw
---

Errez ahazten ditugu, bizi izan zituztenak, herritarrak sailkatzen, jarraitzen, ditugula.
Aspaldi izan ginen gu ezezagunenak, bide-gurutze berritan aurkitu ginela.
Askotan galtzen, sufritzen, ertzean geratu zirenak. Esperantza galdu gabe, munduz aldatzen direnak.
BIZI BERRI BAT AMESTEN, GERTUKOAK AGURTZEAREN KEMENEZ. 
AMETS GAIZTOENAK SENTITZEKO, DESESPERANTZAN BABESEAN AURKITZEKO.
Planeta erdi gurutzatu hamaika etxe atzean. Bizipenak, gorrotoa, bihotz onekoei eskerrak.
Ongi etorri errefuxiatu banderak ikustean, beraien seme-alabak nahastu nahi ez dituztela.
Askotan galtzen, sufritzen, ertzean geratu zirenak. Esperantza galdu gabe, munduz aldatzen direnak.
BIZI BERRI BAT AMESTEN, GERTUKOAK AGURTZEAREN KEMENEZ. 
AMETS GAIZTOENAK SENTITZEKO, DESESPERANTZAN BABESEAN AURKITZEKO.