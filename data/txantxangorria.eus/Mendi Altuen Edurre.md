---
id: tx-1664
izenburua: Mendi Altuen Edurre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zoZVDY1C9Go
---

Tapia eta Leturia / Amuriza
Bizkaiako Kopla Zaharrak

Mendi altuen edurre
bota surtera egurre
alaba galanten amak
mutil zantarren bildurre.
 
Aite gureak amari
gona gorrie ekarri
luze dala labur dala
garri garritik ebagi.
 
Gorozikeko etxe baten
zazpi koltxa oge baten
mutil bi han ipini dauz
deskantsetan apur baten.
 
Lastozkue zan atie
buruko orratza trangie
ha edeitteko eztanak
zetarako jok andrie.
 
Kalien nago kalien
harri hotzaren ganien
atie nok edei ez-ta
gabak emon daidenien.
 
Hau da pandero barrie
Durangotik ekarrie
hamar erreal pageute
dana da urre gorrie