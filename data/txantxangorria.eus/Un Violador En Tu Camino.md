---
id: tx-2304
izenburua: Un Violador En Tu Camino
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n-JzuBiY0c4
---

UN VIOLADOR EN TU CAMINO

Patriarkatua juez
Epaitzen gaituna sortzez
Ta gure zigorra
Bortxa, nahiz ikusi nahi ez.

Patriarkatua juez
Epaitzen gaituna sortzez
Ta gure zigorra
Bortxa, ikus liteke errez

Feminizidioa
Zigorgabe ni hiltzen nauena
Desagertzen gaituzte
Ta bortxatzen gaituzte

Ta errua ez zen neria, 
ez non nengoen, ez nola jantzia (4)

Bortxatzailea nor zen? Zu!
Bortxatzailea nor da? Zu!

Txakurrak dira 
Epaileak
Estatua
Presidentea 

Estatu zapaltzailea 
Harro ta bortxatzailea 
Estatu zapaltzailea 
Arra ta bortxatzailea

Bortxatzailea nor zen? Zu!
Bortxatzailea nor da? Zu!

Duerme tranquila niña inocente, 
sin preocuparte del bandolero, 
que por tus sueños dulce y sonriente 
vela tu amante carabinero. 

Bortxatzailea nor da? Zu!
Bortxatzailea nor da? Zu!
Bortxatzailea nor da? Zu!
Bortxatzailea nor da? Zu!