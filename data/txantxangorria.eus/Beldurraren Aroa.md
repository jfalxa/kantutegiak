---
id: tx-3368
izenburua: Beldurraren Aroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SX4e4PKYgdY
---

BELDURRAREN AROA

Beldurra itxi gabeko ate guztiei, 
Baita erlojurik gabeko denborari ere
Bakardadeari,
Jendetzari, izan zenari, denari ere!

Emakumeak indarkeriari, 
Gizonak beldurrik gabeko emakumeari
Estatuek,
Pentsatzen duen pertsona orori!

Beldurraren aroa sortu nahi dute,
Segurtasuna salduz, beldurrez!
Pentsamendu, iritziak, ezabatu nahiean, beldurrez!
Gure armak ideiak direlako!