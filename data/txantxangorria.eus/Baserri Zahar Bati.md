---
id: tx-3130
izenburua: Baserri Zahar Bati
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ocn6wRmpcwY
---

Baserri xahar maitagarri bat, oihan baten sahetsean,
 egun guziez ikusten nuen, etxetik ateratzean;
 orai lurrerat erori da ta pena daukat bihotzean,
 nigar iten dut hor pasatako egun onez orroitzean.
 
 Nik haur-denboran baserri hortan nintuen josta-lagunak,
 Beti gogorat etortzen zaizkit hor pasatako egunak.
 Ai zer andana biltzen ginenak, ahaide ta ezagunak!
 Bertze mintzorik etzen gurekin, oro ginen eskualdunak,

Hori ikusiz askok iten du «Zer gertatzen da?» galdera,
 buraso zarrak hil ta joan munduaz beste aldera;
 gazteak, berriz, lurraren iges, herrira edo kalera,
 horra zertarik etorri zaikun baserriaren galera.

Baserri maite, zonbeit bisita gaztean egin dautzut nik,
 asko kantu're ikasi nuen baserri hortan entzunik;
 ta orai berriz kasik iduri ez dela hauzotasunik,
 hauzo artean ere ez baita lehengo goxotasunik.
 
 Oraiko bizi berriak ere badauka bere itzala:
 bakotxa bere kontu hai gira, guhaur bagine bezala;
 erdi lasterka iduri eta beti presan gabiltzala,
 lagun urkoa geio maitatzen Jainkoak lagun gaitzala!
 
 Azken agur bat egiten dautzut, baserri maitagarria,
 jende guziek bakarrik utziz gaur lurrean eroria;
 zu ikusteak hemen uzten nau malkoz beterik begia,
 hola segituz ez da luzaro biziko Eskual-harria.