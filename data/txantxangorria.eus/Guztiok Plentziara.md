---
id: tx-2693
izenburua: Guztiok Plentziara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2c-_RIzE0ug
---

Jai egunak Plentziako herrian, 
zahar eta gazteak dantzan 
alaitzen dira. 
Arin, arin, txistu ta danbolin; 
lagun guztiok elkarrekin 
kalean ibiliz.
Hemen nauzu poz handiz beterik, 
ditudala masailak gorriturik. 
Gora ta gora dabil kantaria, 
hainbat maite duen geure Herria. 
[Jai egunak, zorionekoak, 
euskaldun guztiok Plentziara
ongi pasatzera.]  [bi]

Gaminizko lur ederrean, 
sortu egin zan Ertaroan
Plentzia herritxoa. 
Hain politak ditu inguruak;
mendi ta itsasadar artean,
itsaso ertzean. 
Arrainetan eta marinelak, 
plentziarrak oso trebeak dira. 
Beren fama guztiz ezaguna da, 
onartuak, bai, mundu zabalean. 
[Goratua da geure Herria, 
guztion ardura hauxe da:
PLENTZIA MAITATZEA.]  [bi]