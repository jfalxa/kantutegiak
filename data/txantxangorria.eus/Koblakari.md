---
id: tx-1665
izenburua: Koblakari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Miq6wSfxMDc
---

Zilar ezpata idüri oihanean goizanko ekia
Bardin gure artean zartzen poeten begia
Bihotzean egiten dizü ezin elkizko zauria
Irustarzün ütürria.

Koblakari Koblakari denek zure bihotzean nahi
Koblakari Koblakari zütaz gütützü egarri.

Ama deitzen den bezala korpitza hozten senditzean
Zure berotarzüneala jin nüzü bizitzean
Bizientako amodioaz zük bihotza senditzean
Sü hartzen deikü güzia.

Ori mendi bera düzü hiru erregeen aitzinean
Lotsa gabe txütik zira Frantzi eta Espainian
Zure lana hegaltatürik betikoz zelü lürretan
Euskal Herri güzian.

Bizi denboran beitzüan aterbüa etxe batetan
Hilik ere poeta bizi hanitxetan
Heriotza lotsagarriak kentzen badü izaitea
Ez dateke zure kasüa