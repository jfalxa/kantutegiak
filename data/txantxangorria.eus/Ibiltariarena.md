---
id: tx-553
izenburua: Ibiltariarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S_V7gA5PXr0
---

Agur ta banoa
ni ez bait naiz hemengoa
ez dut aberririk
libertatea ezik
t´aspaldi luzean
bizi naiz erbestean
inoren etxean
arrotzen artean.

Agur ta banoa
ni ez bait naiz hemengoa
ez dut ekipairik
nere gitarra baizik
ta zure itzala
ipar-orratz bezala
zama arina da
amaigabea bidea.

Agur ta banoa
ni ez bait naiz hemengoa
ez dut helbururik
ibiltzea besterik
nekeen gainetik
ametsaren azpitik
haizeak naroa
ez naiz inon inongoa.