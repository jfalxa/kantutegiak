---
id: tx-1371
izenburua: Aita Floren
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tS6A-fwede4
---

Hippy batzuk daude
Tosuko landetan
Lo egiten dute
kanpaina dendetan
Guk parkinga
egin nahi dugu
lur zikina
eta porruak kentzeko
dena porlanez janzteko
belar, gehiagorik ez da izango
berde, dirua dugu nahiago
Aita Floren, eman ya dirua
Bai bai, aberastuko naiz
Aita Floren, eder da dirua
Bai bai, irabaztean maiz.
Zertarako ortua? 
dirua da kontua
jai-jai, duzue Tosurekin.
Aita Floren, eman ya dirua
Bai bai, aberastuko naiz.
Gizon emakume
katu eta txakur
Tosutik kanpora,
betira arte, agur!
Guk parkinga
egin nahi dugu
Andra Mari
bunker txaletez josteko
ahal dugun dena osteko
baba-lore balore gabeak
gara lurzoru honen jabeak!
Aita Floren, eman ya dirua
Bai bai, aberastuko naiz
Aita Floren, eder da dirua
Bai bai, irabaztean maiz.
Zertarako arbolak? 
hobe Madrilen golak
jai-jai, duzue Tosurekin.
Aita Floren, eman ba dirua
Bai bai, aberastuko naiz.
Aita Floren, eder da dirua
Bai bai, irabaztean maiz.
Aita Floren, eman ba dirua
Bai bai, aberastuko naiz
Aita Floren, eder da dirua
Bai bai, irabaztean maiz.
Zertarako abestu? 
benga behingoz ulertu
jai-jai, duzue Tosurekin.
Aita Floren, eman ba dirua
Bai bai, aberastuko naiz.