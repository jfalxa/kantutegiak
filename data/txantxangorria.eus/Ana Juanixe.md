---
id: tx-1154
izenburua: Ana Juanixe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/szZTnJc8GUE
---

Egun batian ni ba'nenguan,
kostela-kostel gaiñian,
ni ba'nenguan kostel-saltzian
ezpabe gari-saltzian.
Iru damatxu etorri ziran
bata bestien atzian
irugarrenak pregunta eustan
gariak zegan zirian.

Txalopatxu bat, txalopatxu bi
Santa Klara'ren parian...
Neure anaiya antxe datorke
batian edo bestian...
-Neure anaiya: zer barri diraz
Motriku erri onian?
-Barriak onak diradez, baña
jazo da kalte sobria.
-Oh, neure aniya: ez edo-da il
biyoren aita maitia?
-Biyoren aita ez da il, baña
jazo da kalte sobria.
-Oh traidoria! Ez edo-da il
biyoren ama maitia?
-Biyoren ama ez da il, baña
jazo da kalte sobria.
Ana Juanixe ezkontzen zala
iñuen atzo kalian,
Ana Juanixe ezkontzen zala
aita ta amaren lotsian...