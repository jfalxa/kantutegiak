---
id: tx-1947
izenburua: Neguari Abesten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9G3BXHqE7RM
---

Ene Kantak-ek CD berria dakar. Bertan, neguko abestiak daude; gehienak berriki sortuak edota zeudenak, moldatuta. Oraingo honetan, gure filosofiari jarraituz, Nafarroako Bertso Eskolarako dirua lortu nahi dugu lan honekin, "D" ereduko eskoletan eta ikastoletan lanean jarraitu ahal izateko. Zenbait lagun etorri dira gurera abestera, batez ere, bertsolariak, hala nola; Igor Elortza, Uxue Alberdi, Xabi Terreros. Estitxu Fernandez eta Urko Aristi kazetariak, Leihotikan taldeko, Gorka abeslaria, edota Zubieta-Olazabal pilotari bikotea ere, animatu da kantatzera. Aurreko lanean bezala, Kukuxumusuk marrazkiak egin dizkigu eta osotasuna ematen diote diskaren aurkezpenari.

negua da,
abendua,
kalean jolastu gara
elurretan
blai eginda
goazen sutondora, di da

Eskularruak,
katiuskak kendu etxeko atarian
eta ez ahaztu txano bustia
eskutan hartzeaz

negua da,
abendua,
kalean jolastu gara
elurretan
blai eginda
goazen sutondora, di da

Noiz pasako zait?
Hau da, hau mina
hau da, hau dardara
hortzak kla, kla, kla
eskuak more
hotzarekin normala

negua da,
abendua,
kalean jolastu gara
elurretan
blai eginda
goazen sutondora, di da

Txingurritu zait gorputz osoa
ez da sinale txarra
orain jantzia berogailura
biharko lehorra

negua da,
abendua,
kalean jolastu gara
elurretan
blai eginda