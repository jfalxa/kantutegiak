---
id: tx-1994
izenburua: Aintzinako Bihotz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q4IPQ_qTmuo
---

Bihotz, buztinezko Bihotz,
Etxe ttiki bat zara,
Ahula, hauskorra, lau gelatako;

Lau, lau gelatan Bihotz,
Zenbat mamu dauzkazun,
Nola ikaratzen zaren, gauean;

Gau, gau batzutan Bihotz,
Hautsi egiten zara,
Lurrera jausi eltzetxo baten gisan;

Bihotz, jausitako Bihotz,
Ari zra intzirika,
Kexu zara, negar zara, oi Bihotz;

Zer, zer diozu Bihotz,
Ez zaitut konprenitzen
Zure hitza arrotza zait, bitxia;

Zeit, Einsamen Helian,
Abens grauen flammendes,
(Denbora, Helian bakartia,
Iluntzea, grisa, lamatan)
Ez zaitut konprenitzen, oi ene Bihotz;

Der Tod ist ein Meister,
Und du, zur Linken, du,
(Heriotza maisu bat da
Eta zu, siniestroa zu)
Ez zaitut konprenitzen, oi ene Bihotz;

Der, des menschen Sinn,
Von zweifeln voll,
(Gizonen patua
dudaz betea)
Ez zaitut konprenitzen, oi ene Bihotz;

Bihotz, antzinako Bihotz,
Ez al zara zaharregi,
Eta ilun, eta itxu, barregarri;

Ilun, ez da dena ilun,
Begirazazu leihotik,
Ikustazu baso hori,berdatzen;

Berde,zein berde dauden,
Basoko garo zuhaitzak,
Zein ixil eta lasai, arratsean;

Arrats, arrats hontan Baso,
Hartzazu ene Bihotza,
Buztinetik sortua da, zu bezala.