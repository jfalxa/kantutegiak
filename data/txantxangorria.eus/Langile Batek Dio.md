---
id: tx-2696
izenburua: Langile Batek Dio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oClw7OtOMbw
---

Langile bat naiz
lan egiten dut
Egun guziek
hamaika ordu
Baina nausia
beti kexu da
Egin duguna
ez zaio aski
Baina nausia
beti kexu da
Egin duguna
ez zaio aski
Neure lagunak
langile onak
dirade denak
Gazte sakonak
gure kopeta
Izerditan da
beti indarka
ari baitira
Euskal Herrian
Langabezia edo
txipia irabazia
Euskal Herrian
lan/ga/be/zi/a e/do
txi/pi/a i/ra/ba/zi/a
Mi/la_al/diz di/ra
a/dis/ki/di/rei
be/har du/gu/la
gu/du/an ha/si
zuzen_egoten da
langile batek
irabaz dezan
Hoinbeste gutxi
kapitalisten
jokabidea
Guk langileek
ez dugu nahi.
kapitalisten
jokabidea
Abertzaleek
ez dugu nahi.
Langile oro
nahi bagendu
Gure burua
zinez altxatu
Lanegileok
lantegietan
Jar gaiten nausi
guziazpetan
Askatasuna
galdatzen dugu
Lantegietan ta aberrian
Askatasuna
galdatzen dugu
Lantegietan ta aberrian
Geroan ere
lan egingo dut
Bizi guzia
zuen honetan
langile bat naiz
lan egingo dut
Bizi guzian
denen honetan
langile bat naiz
lan egingo dut
Bizi guzian
denen honetan
la/ra la/ra la la/ra la/ra ra ra
la/ra la/ra ra ra
la ra ra ra la/ra

la/ra la/la la/ra la/ra ra ra
la/ra la/ra ra ra
la ra ra ra la/ra

la/ra ra ra
la/ra ra ra la/ra
la/ra ra ra la/ra
la/ra ra la/ra
la/ra la la
la/ra ra ra la/ra
la/ra ra ra la/ra
la ra ra