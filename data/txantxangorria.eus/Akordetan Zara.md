---
id: tx-543
izenburua: Akordetan Zara?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/p-zfXJ3r2Zw
---

MG Bandaren "Ama Sua" (2020) diskoko aurkezpen abestia.

▼▼ Letra ▼▼
Akordetan zara egoan giroaz?
Akordetan zara denbora hartaz?
Buruen ahal dekozu jentean beroa?
Akordetan zara edo ahiztu jatzu?

Eztau lekurik ideientzako posea preparau argazkirako.
Esan beharrak esaten , karaktereak kontatzen.
Begibistako amodioa minutu bateko promo bideoan.

Akordetan zara...

Urre , denboreagaz uger ta Zer izingo garen biher gurego dot ez pentsau.
Biher, biher automata lez, buruen neurona erdigez ta beste erdie..#@&$∆

Akordetan zara...