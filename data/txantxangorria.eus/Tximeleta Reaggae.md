---
id: tx-1772
izenburua: Tximeleta Reaggae
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vpquwg2rDDc
---

Tximeleta haiek hegal egiten
itsasoaren barren aldera
milaka tximeleta beilegi
uhinen gainteik

Kostaldetik berezi ziren
laino ttiki bat bezalaxe
baporeak atzera utziz
galdu egin ziren

Tximeleta haiek hegal egiten
itsasoaren ixiltasunerantz
ez joan, ez joan, itsaso horretan
pausalekurik ez da.

Ez dago irlarik
ez dago harkaitzik
itsasoan ez dago
ur iluna besterik.