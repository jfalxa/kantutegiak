---
id: tx-2103
izenburua: Txinaurria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CJD4o000KCM
---

Primadera hastetik uda-azkenerat
txinaurria txuhurra landa bazterrerat
txori papo gorri bat arbol batetarat
egun oroz jiten zen kantuz aritzerat
txinaurria trufa irri egiterat.

Txinaurria lotzen da egunetik lanari
egun guzietako duen guduari
karga ezin altxatuz nigarrez da ari
txoriak itzalpetik trufa eta irri
txinaurriak derauko zerbait erran nahi.

Hauxe diat txoria, hiri erraiteko
nik ere bi beharri hire aditzeko
kanpuan baduk orain gauz ainitz biltzeko
emak zoko batean neguan jateko
lanari lotzen bahiz ez zauk dolutuko.

Txinaurri lepho mehe itsusi txirtxila
ez nauk heldu hi ganat ni deuseren bila
lanian ez duk ari ihizi abila
utzak, utzak hik ere lan hori, ergela
urte oroz heldu duk berdin uztarila.