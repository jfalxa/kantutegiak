---
id: tx-1593
izenburua: Kanpo Errobi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4ilpSNY-djg
---

Kanpo,
kanpokoa da
gu gobernatzen ari zeraukun jauna (bis)
azerietan abilena ... (bis)

Maltzur,
ta dena gezur
herria izorratzen dakite segur, (bis)
ebatsiz gaztain eta ezkur. (bis)

Uzkur
ta beti beldur,
halere jakin saltzen etxalde ta lur, (bis)
badakizue bai nora bihur ... (bis)

Aski
lan tzarra geldi
gurea dena otoi guretzat utzi (bis)
gure lurrean gu nagusi, (bis)

(Daniel Landart)