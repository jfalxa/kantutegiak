---
id: tx-3048
izenburua: Mundiala!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PWh7d5ULmxU
---

KILOMETRAOK 98 - ¡Mundiala!
TOLOSA

Musika: Joxan Goikoetxea
Hitzak: Jon Sarasua

Joxan Goikoetxea: Sintetizadoreak, programazioak, ambienteak.
Abraham Olano: Ahotsa.
Ainhoa Arteta: Ahotsa.
Angel Mª Peñagarikano: Ahotsa.
Andoni Egaña: Ahotsa
Sebastian Lizaso: Ahotsa.
Alaitz eta Maider: Ahotsa.
Kike Amunarriz: Ahotsa.
Mikel Markez: Ahotsa.
Niko Etxart: Ahotsa.
Alan Griffin: Ahotsa.
Luis Martin: Kitarra.
Txema Garcés: Baxua.
Juan Arriola: Bibolina.

Bideoklipa: Jon Heras
Produkzio artistikoa: Joxan Goikoetxea
Audio nahasketak: Jean Phocas
Koordinjazioa: Kontxi Luluaga
Diseinua: ZUT ! Creativos.


Kilometroak
erlojurik gabe
kronometroak
denboraren alde
joan bagoaz
ta egon ez gaude
etorkizuna
hautatu dugu lurralde.

Erloju gabe
noski, baldinbaite
euskaraenera
gonibidatu zaite
ze erremedio
festa dugu maite
tira, agian
festak berak gaitu maite.

Mundiala da,
Euskara da.

Milaka urtez
segunduz segundu
planeta honek
mintzo bat entzun du
mundura goaz
ta gu gara mundu
gure bidaiak
bere sekretua nundu ?

Mapamundia daukat
goikoz behera
ikasiminez
noa buruz behera
abentura bat
da euskara bera
hoa mundura
ta hator Tolosaldera.
Mundiala da,
Euskera da.