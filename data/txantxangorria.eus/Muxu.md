---
id: tx-138
izenburua: Muxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3N86ZXHiDvY
---

Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Aizu, aizu esaidazu
aizu esaidazu zer nahi duzun
Aizu, aizu nahi baduzu
Aizu nahi baduzu non nahi duzun
Emango nizkizuke, belarri ertzean
ta gustuko baduzu, ezpain bakoitzean
egindazu txoko bat zure bihotzean
belarri ertzean, ezpain bakoitzean
Txoko bat gorde dizut nire bihotzean
aurrean, atzean, kopeta gainean
Txoko bat gorde dizut nire bihotzean
belarri ertzean, ezpain bakoitzean
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Aizu, aizu hor begira
Aizu zuetzat ere badira
Aizu, lotsatzen bazara
Zatoz eta esan belarrira
Emango nizkizuke, belarri ertzean
ta gustuko baduzu, ezpain bakoitzean
egindazu txoko bat zure bihotzean
belarri ertzean, ezpain bakoitzean
Txoko bat gorde dizut nire bihotzean
aurrean, atzean, kopeta gainean
Txoko bat gorde dizut nire bihotzean
belarri ertzean, ezpain bakoitzean
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, non nahi duzu muxu
Muxu muxu, muxu muxu
Muxu muxu, non nahi duzu muxu