---
id: tx-867
izenburua: Baina Berdin Dio
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gcybBcDTfx8
---

Esti Markez eta Mikel Markez "Baina berdio dio" kantatzen "Biribilean" zuzeneko diskoan
Irudiak: Gaizka Peñafiel

Trikia: Julen Alonso
Arrabita: Arkaitz Miner

BAINA BERDIN DIO

Harrizko galtzaretatik 
asfaltozko autobidera 
gizakiaren bidexka 
izan da beti bera.

Maitasun ta gorrotoz 
munduak mila bira 
odolustuen gorpuak 
zagi hutsak baitira. 

Baina berdin dio maite 
zu eta ni bagoaz 
denaren gainetik 
gure bakardadearen 
zoriontasunean. 

Jendea presa handiz doa 
ba al dugu helbururik 
bizitza agudo egin behar da 
ez dago denbora galtzerik. 

Ei. lagun, hago ixilik 
arazotan izango haiz bestela 
ibil denen modura 
izan denak bezala. 

Baina berdin dio maite 
zu eta ni bagoaz 
denaren gainetik 
gure bakardadearen 
zoriontasunean
zoriontasunean. 

Goizero esnatzean 
nire aita joaten zen lanera 
hala egin zuen beti 
txingurrien modura.