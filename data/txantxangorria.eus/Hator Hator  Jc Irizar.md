---
id: tx-3279
izenburua: Hator Hator  Jc Irizar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/z0dGvn-wG_k
---

Hator, hator, mutil, etxera,
gaztaina ximelak jatera,
Gabon gaua, ospatutzeko,
aitaren ta amaren ondoan.
Ikusiko duk aita barrezka,
ama ere poz atseginez. (Bis)

Eragiok mutil,
aurreko danbolin horri,
gaztainak erre artean,
gaztainak erre artean,
txipli, txapla, pun!
Gabon gaua pozik igaro
daigun.(Bis