---
id: tx-897
izenburua: Munduko Bidetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LMfiB4Ttr1Y
---

MUNDUKO BIDETAN

 

            Munduko bidetan elgarri zer erran?

            Munduko bidetan gizonen artean...!

 

 

      I

 

Ni ez naiz ez zuk uste duzun etsaia

Hasi gaiten solastatzen

Elgarren ezagutzen.

 

 

      II

 

Iguzkiak sortaldetik agertzean

Sartaldera du bidea

Hau aspaldikoa.

 

 

      III

 

Lore bat da gure etxean ere lore

Azkatasuna guk ere

Dugu maite!

 

 

      1969-70.ekoa. Kantari abertzalea beste jende bat bezalakoa da, ez besteak baino gaixtoagoa, eta kantatzen duena ura, lurra, iguzkia eta lilia bezain argi eta xinpleak dira... eta beharrezkoak!