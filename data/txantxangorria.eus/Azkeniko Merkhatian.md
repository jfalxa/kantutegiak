---
id: tx-151
izenburua: Azkeniko Merkhatian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7wYIamh7-wc
---

Azkeniko merkhatian 
Azkeniko merkhatian jokü hon bat egin nian, Etxekuek nahi gaberik merkhatia hartü nian, Bai eta ere ützüli hantik bigeren goizean. 
Etxen sartü nintzanean, bertan « bonjour » erran nian, Etxeko horiek ixilik, ez eni arrapostürik, 
Bai’ta ni estonatürik, zer ote günian okerrik ? 
Oi zer ote egin düt okerrik? Oi zer nahi düzie nitarik? Ifernüko debrik oldartürik, hüts egin niala ez düt düdarik ! 
Ama horek alhabari : « zerra ezan borta hori ! 
Eta gero lotakio aita eijer xarmant hori, 
Behar diñagü egotxi eta bilhoak idoki!” 
“Maia hor eztün hoberik, eztiñat arrünkürarik, 
Ostatean nizanean arduz untsa berotürik, 
Etzitadan ez orhitzen badüdanez emazterik?” 

Hitzak: herrikoak 
Müsika: Jean ETCHART