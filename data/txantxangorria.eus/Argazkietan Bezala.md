---
id: tx-2830
izenburua: Argazkietan Bezala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IuT0L1IjgQU
---

Esan mundua hain grisa ez dala,
San Martinak laister izango dirala
Nahiz egia lez, gazia den kresala,
Mesedez Irribarre egin Dezala
Argazki zuri-beltz horretan bezala!
Argazki zuri-beltz horretan bezala!

Hagin zorrotzen begiradatan
Gurutzatzen dan txoria
Arrano bat nola izan
Gulakoa bai ordia
Dagokixenak banatzen ohi dau
Zotzan zenbaki gorria
Guri berdia ezin tokatu
Ze maltzurra dan "zoria"

Ezjakintasunaren menpeko
Gorrotoaren otsoa
Nahiz ta kostatu zuzendu behar
Oker eindako pausoa
Zuhaitz Adarretatik erori
Ta galduz gero hostoa
Galduko da, gezurrez estaliz
Gure egia osoa

Ez dugu sekula maitatuko
Gezur izpien itzala
Gu engainatu asmoz jantzi duten
Otsoen ardi-azala
Duintasuna prezioz merkatu?
Ba konponbide bezala
Irrifarre sarkastikoz, baina
Irrifar egin dezala...
Argazki zuri-beltz horretan bezala!
Erakutsi gutxiago