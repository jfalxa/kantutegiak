---
id: tx-2230
izenburua: Uda Hartatik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fyp1FJQZjdY
---

Abestia Iruñako Sound of Sirens estudioan grabatua eta Masterizatua dago Julen Urzaizen eskutik. Videoclipa Dani SKST lagunak egina.

LETRA:
Uda urrun, eguzkitsu, Beriain aurrean
500 urteak gertu zireneko hartan
gazte asaldatu, inkonformismoa
militantzia akordez hamarkada luze bat hasten da!

hainbat kilometro eginak ta egiteko!

Uda hartatik gorroto balak bezala
arbasoak ispilu, oraina borrokatuz
lagun ta ideologia bihotzaren erdian
uda hartatik 1 5 1 2

“Euskal Herriya plaza” oinen azpian
lehen pausoak Alertarekin batera!
armak instituzio kaleak bezala
ideien guda ere gudaren alde ekarpena da!

hainbat kilometro eginak ta egiteko!

Uda hartatik gorroto balak bezala
arbasoak ispilu, oraina borrokatuz
lagun ta ideologia bihotzaren erdian
uda hartatik 1 5 1 2