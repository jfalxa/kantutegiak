---
id: tx-1695
izenburua: Nire Ibaia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ai9HQfWTr3Y
---

Nire ibaia ez dator itsasora
nire ibaia ez da ibai
inoizko ubidea, kantari ez dakien erretena.
Ur mehea dakar, baina
leporaino hartzen nau.

Nire ibaia ez dator itsasora
nire ibaia ez da ibai
erreka hauspeztua, izar bakarreko ispilua.
Ez nau hezatzen, baina
busti egiten nau.

Nire ibaia ez dator itsasora
nire ibaia ez da ibai
ezin ditu bi bazter, lizarrik gabeko errekaldea,
Ez dakit nora, baina
eraman egiten nau.

Nire ibaia ez dator itsasora
nire ibaia ez da ibai,
egarriaren lotsa, hodei nekatu baten malura.
Ez nau hasetzen, baina
ito egiten nau.

(Koldo Izagirre / Karlos Jimenez)