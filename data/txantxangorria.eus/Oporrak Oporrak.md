---
id: tx-2725
izenburua: Oporrak Oporrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gDnBdhBBqbs
---

Oporrak, oporrak, Gabon oporrak!
Utzi lantxoak dira Gabonak!
Oporrak, oporrak, Gabon oporrak!
Utzi lantxoak dira Gabonak!
Amatxo non dago Jaiotza gordeta
Jesus ta Maria eta estalpea?
Non ote da artzaia?
Eta non arraia?
Aurretik astoa gainean lastoa.
Oporrak oporrak….
Goi-goian izarra eta aingerua,
Erregeak ere behatzen zerua
Meltxor da aurrena, Gaspar bigarrena
Eta gure Baltasar azken-azkena!
Oporrak oporrak….