---
id: tx-2492
izenburua: Sorginkerian Dugu Independentzia!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j29cG2jwtgg
---

GAZTE AKELARREA 2018 MARTXOAK 30 · 31 - APIRILAK 1 | OIARTZUN SORGINKERIAN DUGU INDEPENDENTZIA! 
Doinua: Urrats erromeria. 
Letra: Koldo Gezuraga. 
Kantuaren grabazioa: Zarata estudioa (Berango). 
Eskerrik beroenak: Uribe Kostako gazteak, Erandioko gaztetxea (desalojo arriskuan) eta eskuak zikintzeko beti prest zaudeten guztiei!
 _______________________________ 


SORGINKERIAN DUGU INDEPENDENTZIA 
Guga, biba, higa, laga, suga, hega. 
Heltzean gerora, geroa eltzera. 
Apo odola, orbel esentzia. 
Ze sorginkeria, independentzia! 
Errautsen zirriborroak, gure atzamarren kargu. 
Beldurrari agur esan, eta egurra dakargu. 
Ohar daitezen denon argiaz, arnas bihurtu dugun magiaz. 
Euskal Herriko ilargipeak, borroka zaporea du! 
Guga, biba, higa, laga, suga, hega. 
Heltzean gerora, geroa eltzera. 
Apo odola, orbel esentzia. 
Ze sorginkeria, independentzia! 
Inkisizio ilunari, darion lerdea. 
Gudan hausteko irrifartsu, da sorgin errebeldea. 
Gogoz egiten nola ari den, orbainetatik orainari men. 
Euskal erratzen, garrak lerratzen, prest da gure edabea! 
Guga, biba, higa, laga, suga, hega. 
Heltzean gerora, geroa eltzera. 
Apo odola, orbel esentzia. 
Ze sorginkeria, independentzia!