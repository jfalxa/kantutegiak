---
id: tx-2708
izenburua: Ene Maitia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/60jVdPwEP5M
---

Ene maitia zü gabe bizia
ez date zühur ez irus seküla
ene maitia zü gabe bizia
irudi likezü_egiazki inpernüa.

Zü zira_ene maitia
betiko amodiua
ene bizian argia
izatian arrazua.
Ixten baldin banaüzü
bihotz erdia likedazüt
Ixten baldin banaüzü
bihotz erdia likedazüt

Ene maitia tziaui enegana
bizikotz nahi züntüzket enia
ene maitia izar pare gabea
ez nazazü ütz otoi ez seküla

Zü zira_ene maitia
betiko amodiua
ene bizian argia
izatian arrazua.
Ixten baldin banaüzü
bihotz erdia likedazüt
Ixten baldin banaüzü
bihotz erdia likedazüt

Ixten baldin banaüzü
bihotz erdia likedazüt

Ixten baldin banaüzü
bihotz erdia likedazüt