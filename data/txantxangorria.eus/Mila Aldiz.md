---
id: tx-663
izenburua: Mila Aldiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0049XKT_OWA
---

Nøgen – Mila aldiz

By Ander Perez de Arenaza
@anderperetz
Animazio laguntzailea: Pau Gross
Diseinu grafikoa: Javier Rodriguez
Zuzendaritza eta gidoia: Ander Merino eta David Olariaga

Haritz Harreguyren estudioan grabatua eta nahastua.
Victor Garcíak masterizatua Ultramarinos Mastering-en.

----------------------

Nøgen – Mila aldiz

By Ander Perez de Arenaza
@anderperetz
Ayudante de animación: Pau Gross
Diseño gráfico: Javier Rodriguez
Dirección y guión: Ander Merino eta David Olariaga

Grabado y mezclado en el estudio de Haritz Harreguy.
Masterizado por Victor García en Ultramarinos Mastering.


--------------------



Follow Nøgen




Lyrics.

Lurrean botata ez dakit zein egun den gaur
Zentzua galduta  gertatzeko dagoenaren zain
Eramango nauen zerbait dauka, hiru minutu ta hamar
Inora itzultzeko aitzakiren bat.

Bidaiatzen hasi soinu hori entzutean
Melodia berri edo akorde segidaren bat
Zerbaitek eztanda egiten du harrapatzen zaituenean
Denboran atzera salto egitean

This is our life
This is our time
Ez esan biziraun behar dela

This is our life
This is our time
Mila aldiz sentitzeko aukera

Gauaren alde bakoitzak bere izarra duela zuretzat
Denak hemen, denak orain, argiak ezabatu baino lehen.


This is our life
This is our time
Ez esan biziraun behar dela

This is our life
This is our time
Mila aldiz sentitzeko aukera

This is our time