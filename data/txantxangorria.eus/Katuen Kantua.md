---
id: tx-2948
izenburua: Katuen Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uGQtcqMQgZk
---

Katu bat lasai garaje alboan
Buztana buztana altxatuz miauka
kamioi handi bat heldu denean
burrunbadan balaztaka.
Txoferrak eskatu du errime,
marmarrean eta irainka:
“Gurpil zahar hori puskatu baitzait,
nork daki hemen mekanika?”
Mekanikoa, logale, etzanda;
garagardoen katua:
“Ai zer tristea langilearen
atsedenaldi ukatua.”
Halaxe da katua
nazkatu, konplikatu
eta madarikatua bilakatu.
Elkarri agintzen hasi dira eta
katuak entzun du garbi:
“Kamioia altxatu beharko du eta
horko katu hori ekarri!”
Katua jaiki eta arrapaladan
handik eskapatu nahiz:
“Zuen kamioiak zeuek altxatu,
ni katu bat baizik ez naiz!”
Bi polizia patruilakoek
ozen dute oihukatu:
“Katu hiztunak aspaldi ziren
hiri honetan debekatu!”
Halaxe da katua
nazkatu, konplikatu
eta madarikatu bilakatu.
Pistola bana atera eta
sakatu dute katua.
Tiro artean ihesi doa
lasai zegoen katua.
Katua harrapatu ezinik
geu gaituzte galdekatu.
Hagatik dugu ikusi legez
gertaera musikatu.
Katuak, katu, katuak katu,
Katuak eta katuak…
Katuen gauzak ere bilaka
daitezke konplikatuak.