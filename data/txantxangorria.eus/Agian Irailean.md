---
id: tx-1490
izenburua: Agian Irailean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nsgrL5FIuEo
---

Iraila amaieran hil zituzten bostak
tiroz eten zuten
lagunaren kanta
gerora be, sufritu gaitu herriak.
Hilabete kutun, udazken bezpera
orbelak poliki eztali gaitzala
eternitatea daramagu, deslai.
Lur gorri, mahaztietan
uda lehorrak
uzta berria
musika airean, uzta berria.
Zure eguzkia, nire ilargia
horra bi aberri, lur zati berean
bata zurea da bestea nirea.
Eta bizi libre eroen artean
dragoi hegalariz, lamiez eta eztiz
gobernatuko den erreinu gordian.
Goiztiar, arraztidiar
San Mikeletan
katu ta dantza mozkortu gara
San Mikeletan.
Soinean bi hego egin nahi dut hegaz
hego bakarragaz, ezinezkoa da
hego bakarragaz, nekaturik nago.
Desegin dadila koldarren tropela
inoiz DESKUIDUAN han izan banintzen
aurrerantzean ez, ez naiz han izango.
Itsasen, batak irailan
zulatu ditu
harriki ezinak
porlanezko hormak
itsasen batan.
Badoa iraila, uda biharamun
eguna hoztu du, ez da desagertu
traineru garaituak zekarren bitxa.
AGIAN IRAILAN HURRENGO IRAILAN
AUKERA BERRI BAT ENEGARRENEAN,
IKASIKO DIZUT ZER DEN MAITATZEA
Urrun, banoa urrun
nigandik urrun,
zugandik gertu
lagun eroak,
ta haizea lagun
HEGAZ
HEGAZ
Haizea lagun
Dragoi hegalariz
lamiez eta eztiz
ZUGATIK GERTU!