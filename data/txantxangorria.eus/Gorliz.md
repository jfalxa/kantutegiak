---
id: tx-969
izenburua: Gorliz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/snMvWPsw9qU
---

ZIRKORRIKA abestia

Ahotsak:
Izaro
Oihana Fernandez
Jurgi Ekiza
Miren Narbaiza
Petti
Jarauta 69
Txindatak: Mingüi Salaberri
Tuba: Fredi Larreta
Tronboia: Javier Arrieta
Bonbardinoa: Mintxo Garaikoetxea
Tronpeta: Roberto Ezker
Saxoa: Fernando Lacunza
Oreka TX
Txalapartak, banbuak, sega eta sardea: Harkaitz Mtnez de San
Vicente eta Mikel Ugarte
Trompeta solista: Javi Perez
Akordeoia eta saxoa: Xabier Zabala
Alboka, ttun-ttuna eta bonboa: Harkaitz Mtnez de San Vicente
Grabazioa, nahasketa eta masterizazioa: Kaki Arkarazo
Zuzendaritza: Harkaitz Mtnez de San Vicente
Kanta hau 2016ko azaroan Andoaingo Garate Estudioan grabatu dugu.
Eskerrak: Karlos Piñuela, Rafa Lekunberri, Tonino Karotone, Jon
Celestino, Txalapart, Hiru Damatxo ideia faktoria... eta proiektu honetan era batera edo bestera parte hartu duzuen guztioi!

Hizkuntzaren malabaristak,
akrobatak, trapezistak,
herri honen ilusionistak...
bidera atera gara,
mundura atera gara,
herriz herri igaroko gara!

Bat, izan bat, egin bat,
Bat zuk eta bat nik, bat, denok bat,
bat eta batzuk gara, 
hona-hara,
euskara bat, mundu bat.

Bi urteko ekilibristak,
su-irensle optimistak,
jaiotzetik ilusionistak...
bidera atera gara,
Korrika atera gara,
herriz herri igaroko gara!

Atso-agure kontortsionistak,
pailazoak, eskapistak,
euripean ilusionistak...
bidera atera gara,
mundura atera gara,
herriz herri igaroko gara!

Bat, izan bat, egin bat,
Bat zuk eta bat nik, bat, denok bat,
bat eta batzuk gara, 
hona-hara,
euskara bat, mundu bat.

Ahots kordan funanbulistak,
fakirrak ta alkimistak,
ofizioz ilusionistak...
bidera atera gara,
Korrika atera gara,
herriz herri igaroko gara!

Bizirauten espezialistak,
bi buruko aktibistak,
bihotzaren ilusionistak...
bidera atera gara,
herriz herri ibiliko gara!

Bat, izan bat, egin bat,
Bat zuk eta bat nik, bat, denok bat,
bat eta batzuk gara, 
hona-hara,
euskara bat, mundu bat.