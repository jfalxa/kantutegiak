---
id: tx-2332
izenburua: Sautrela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Wx-gDI8XMYs
---

O heuskara lauda ezak garaziko herria
Zeren hantik ukhen baituk behar duian thornuia
Lehenago hi baitinzan lengoajetan azkena
Orai aldiz izaneniz orotako lehena.

Heuskaldunak mundu orotan preziatu ziraden
Bana haien lengoajiaz bertze oro burlatzen
Zeren ezein eskripturan erideiten ezpaitzen
Orai dute ikasiren nola gauza hona zen.

Heuskaldun den gizon orok alxa beza buruia
Ezi huien lengoajia izanen da floria
Prinze eta iaun handiek orok haren galdia
Skribatus halbalute ikhasteko desira.

Desir hura konplitu du garaziko naturak
Eta haren adiskide orai bordelen denak
Lehen inprimizalia heuskararen hura da
Basko oro obligatu iagoitikoz hargana.

Etai lelori bailelo leloa zarai leloa
Heuskara da kanpora eta goazen oro danzara.