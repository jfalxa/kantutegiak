---
id: tx-1549
izenburua: Maite Zaitut Amatxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rFRPiG5ne0M
---

Konposizioak, moldaerak zein programaketa: XABIER ZABALA

Txantxangorri batek bezala,amatxo, kantatu nahiko nizuke

goxo-goxo eskatu gabe eman dizkidazun muxuen truke.

Betazalak irekitzerako hatzetik laztanduz mantso-mantso;

Goizero etortzen zarenean ta barre egiten didazunean ze guapa egoten zaren, amatxo!

 

Gauez ez badago izarrik ta banago itzarrik

zurekin ez dut izaten inoiz ametsgaitzen beldurrik

Ez da oso garestia, baina udako marrubi gorriak adina

maite dugu, amatxo, zuri darizun usaina.

 

Edonora joango nintzake oinez,hegaz,zaldiz zein bizikletaz

behar ditudan bide guztien mapa baitago zure eskuetan,

Haserretzen zintudanean trumoi aurpegia jartzen didazu

baina lainoa joaten bada berriro barrezka hasten zara eta magia egiten duzu.

 

Kapa gorriaren ordez daukazu amantala

baina nik ikusten zaitut heroi bat bazina bezala.

Muxuz bete-beterik kolkoa ta irribarre bihurtuz malkoa,

nagusitzean izan nahi dut zu bezalakoa.

 

Batzuetan ukitzen dut goitik behera irrikaz zure sabela

ta hunkitzen naiz pentsatzean neu ere bertatik etorri naizela.

Zu zarenez nire ilargi betea eta ni zure iratxo,

sekretutxo bat esan nahi dizut bihotzean gorde dezazun zuk:

 

Zenbat MAITE ZAITUDAN AMATXO!