---
id: tx-2902
izenburua: Tafalla
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O3dvbwcR6QU
---

Zabaldu leihoak
horra_egunsentia
Makotxa, Balgorra
Larrain, Valdetina,
La Peña, Zidakos
laztanduz argia.
Gorria, zuria,
mairua_euskalduna
Tafallan badira.
Langile bipila
ta nekazaria:
bizi da Tafalla
bizi_Euskal Herria.

A, a, a, 
A, a, a, 
A, a, a, 
A, a, a, 

Hegoan Olite,
Baldorba gainian,
Uxue gotorra,
Artaxona hesia.
Lurrari damaio
euskarak bizia:
ezpuendas, gardatxos,
bitxarras, iraskos
oihartzun eztia!
Mahatsa, garia,
lora, hazia:
hori da Tafalla
hori_Euskal Herria.

A, a, a, 
A, a, a, 
A, a, a, 
A, a, a, 

Herri-lur, eskortan,
tafallarrak adi;
batzarrera deika
ezkila da ari.
Radica galapan
lagunekin dabil
sua emateko
Lagunero zital
atzerritarrari.
Apala ta duina,
halere zauriak;
min badu Tafallak
min du Euskal Herriak.

A, a, a, 
A, a, a, 
A, a, a, 
A, a, a, 

Hasi da Colaino
kantuz Pepitari,
Lanedetxe(e)tan ozen
jota_entzuten dabil,
Cuatro_Esquinas bertan
Los Pajes kantari,
betoz txistu-errondak
koadrilan bilduak
lagunduz elkarri.
Zaharra, gaztia,
neria, zuria,
kanta zak Tafalla,
kanta_Euskal Herria!

A, a, a, 
A, a, a, 
A, a, a, 
A, a, a,