---
id: tx-1662
izenburua: Zu Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zEWlyEyT1RQ
---

Ezagutu zintudan 
gabe hortatikan 
nork pentsatuko luke 
nork esango zidan 
dei baten berbetan itoko nintzela 

Irrifar goxua baino ikasia 
begirada inozo bezain lisuna 
Arnasa kentzeko gorputz landua. 

Zu gabe, zu gabe. 
Askoz hobe nago zu gabe. 
(2 aldiz) 

Besteak bezela ni ere jauzi nintzan 
berezi daitezela guztiz sinistuta 
itxu itxu zure zarekadan 
Esan nizkizun hitz goxo guztiak. 
Bidali nizkizun mesu ta imeilak 
zure harrokeria puzteko baino ez ziran. 

Zu gabe, zu gabe. 
Askoz hobe nago zu gabe.