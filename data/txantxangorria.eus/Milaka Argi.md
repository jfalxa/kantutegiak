---
id: tx-1734
izenburua: Milaka Argi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oLYJ2M5ArdM
---

Etorri dira, eraman naute, loturik iluntasunean, 
Beldurra, gaua eta bakardadea.
Euren arma jipoiak eta irainak,
Neurea itxaropena.

Ukitu, kolpatu eta zigortuko naute
Baina, ez al dakite
Gazte garela eta ni eraman ez gero
Hamaikak jarraituko dute.

Ikusiz ikasi beharko dute
Ezingo dutela geurekin.
Izar bat itzaltzean,
Ahots bat isilaraztean,
Ilunperik ez dela.

MILAKA GARA, MILAKA ARGI
AMETSAK EGI BIHURTZEN, EGI BIHURTZEN.

Nire indarra ez da apurtuko
Inoiz ez naiz eroriko
Sortu dugun guztia, geroa 
aldean daramagu