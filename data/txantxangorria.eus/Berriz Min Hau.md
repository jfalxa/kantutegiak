---
id: tx-889
izenburua: Berriz Min Hau
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pzwaVsWR6j8
---

Min hau nuere-nuerea
dudala badakit
baina zauria ez dut
topatzen inundik;
benetan al datorkit
poza erraietatik
ala ez ote nabil
neu bere atzetik?
Honezkero ikasia
nago gerizpean
itzalari azalean
eusteko bestean
aldiz uzkurtu eta
puztu sahiespean
bihotza somatzen dut
eguzki eskean
berriz min hau
luze dirau (bis)
Gudu zelai bihurtzen
denean norbera
irabaztea zer da
zer garela?
Nik ez dut nahi
hemendik onik atera
baizik eta bizirik
sentitzea bera
berri min hau
luze dirau (bis