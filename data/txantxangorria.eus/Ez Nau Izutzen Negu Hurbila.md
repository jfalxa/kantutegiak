---
id: tx-2077
izenburua: Ez Nau Izutzen Negu Hurbila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XBFg1WnW-YA
---

Ez nau izutzen negu hurbilak
Uda beteko beroan
Dakidalako irauten duela
Orainak ere geroan
Izadiaren joan geldian
Gizalditako lerroan
Guzia presente bihurtu arte
Nor izanaren erroan.

Ez nau beldurtzen egunsentian
Arnas zuridun izotzak
Non dirudien bizirik gabe
Natura zabal hilotzak
Eguzki eder joan guztien
Argia baitu bihotzak
Eta zentzuen mila erregai
Iraganaren oroitzak.

Ez nau larritzen azken orduan
Arnasa galdu beharrak
Bide xumea hesituaren
Amildegiaren larrak...
Ardo berriak onduko ditu
Mahastietan aihen zaharrak
Gure oraina arrazoiturik
Beste batzuren biarrak.