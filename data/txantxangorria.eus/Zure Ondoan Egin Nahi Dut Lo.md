---
id: tx-418
izenburua: Zure Ondoan Egin Nahi Dut Lo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KLKc60mYJZk
---

Modus Operandi taldearen "Zure Ondoan Egin Nahi Dut Lo" abestiaren bideoklip ofiziala, bere azken diskotik aterata (Dantza Gaitezen Hil Arte, Baga-Biga, 2020). 

Videoclip oficial "Zure Ondoan Egin Nahi Dut Lo" del grupo Modus Operandi, tema incluido en su último álbum (Dantza Gaitezen Hil Arte, Baga-Biga, 2020).

Lyrics

Gau osoan noraezean, penak itotzen tabernaz taberna
ez naiz batere zorionekoa amodio kontuetan
gau osoan ezerezean, gudako zauriak diraute irekiak
ez dakit egun hurbilen batean sendatuko diren

Zure ondoan egin nahi dut lo, eta gaua pasatu ilargiari so
zure ondoan egin nahi dut lo betiko

Goizargiko eguzki printzek lehortu dituzte
atzoko malko horiek
samina eta atsekabea eraman dituzte
eta bitartean hemen esperoan eta bitartean zure esperoan
zure ondoan egin nahi dut lo, eta gaua pasatu ilargiari so
zure ondoan egin nahi dut lo betiko
agertu zinen zuriz jantzita askatasunaren izenpean
zurekin nahi dut eman bizitza osoa.
libre izan nahi baduzu, aska zaitez,
apurtu kateak eta atzera pausorik ez
libre izan nahi baduzu begira zaitez,
bizitza bakarra duzu, atzera pausorik ez!

Zure ondoan, zure ondoan, a! zure ondoan, zure ondoan,
zure ondoan egin nahi dut lo, eta gaua pasatu ilargiari so
zure ondoan egin nahi dut, lo betiko