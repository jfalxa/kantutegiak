---
id: tx-1934
izenburua: Hau Umorea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3XW-G1h_gSQ
---

Run run run run run
alde batetik bestera
nafarroa gipuzkoa
bizkaia ta araba
lapurdi xuberoa
denok herri bat gara
txoko guztietan sortzen
irribarre ta algara

Beti martxan
herriz herri
ilusioak eskaintzen
betiko lagunari

TUNELA!!

Tunel batetik ba doa
burua gorderik

GORA!!

Gora hau umorea
eta kitto
Gero hor konpon.


GORA ESKUAK DENOK!!
Gora! Gora!

Tope, tope tomatxa!
Tope, tope tomatxa
hau da eskuen martxa
Tope, tope tomatxa
basotik, da bildotsa.

Mugi ipurdia!

Goian dago eguzkia
errekan zubia
kirrinka doa gurdia
txinpun suziria.

Behera! Behera!

Oooo... Laaa... Riii
Olarai liria olarai ku-ku! ku-ku!
Oooo... Laaa... Riii
Olarai liria olarai ku-ku! ku-ku!
Oooo... Laaa... Riii
Olarai liria olarai ku-ku! ku-ku!

Pailazoak hementxe gaude
zuek alaitzeko,
Takolo, Pirrutx ta Porrotx
gara denentzako

Zirkoa martxan dago ta
harrera jar zazu
eskuz krisk-krask eginez
parte hartzen duzu

Pailazoak hementxe gaude
zuek alaitzeko
Takolo, Pirrutx ta Porrotx
gara denentzako

Aurrera!

Txibiripon, txibiripon
txibiri, txibiri, txibiripon

Ixil-ixilik menditik dator
arrano beltza hegan hegan dator,
aurrera txibirin pin pon
albora txibirin pin pon
eta beste albora
txibiri pin pon.