---
id: tx-434
izenburua: Nora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JksBdnxt6nE
---

Inoiz kontatuko badute nire istoria,
zenbat hondar aletan kabituko da nire memoria?
Inoiz kontatuko badute eman dudan guztia, 
zenbat eman dudan kanpora?
zenbat eman dudan barrura?

Jakin nahi nuke ze usain duen itsasoak barrutik. 
Ilargi guztiak agertu arte zurekin dantzan egin. 
Biluzik ispiluari keinu txiki bat egin
Zer ikusten dudan kanpotik? Zer ikusten dudan barrutik?

Ez dezatela esan maite ez nintzenik. 
Ez dezatela esan maite ez nintzenik. 
Ez dezatela esan maite ez nintzenik. 
Ez dezatela esan maite ez nintzenik.

Inoiz kontatuko badizut nire istoria,
zenbat hondar aletan kabituko zait nire memoria? 
Inoiz kontatuko badizut izan naizen guztia.
Jakingo al dut kontatzen zein den egia? 
Jakingo al dut kontatzen zein den egia?