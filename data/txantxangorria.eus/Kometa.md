---
id: tx-1784
izenburua: Kometa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CiKCExMHc5M
---

Hegan hegan hegan hegan
txistuka airean
Zeru urdinean txori artean
Hegan hegan hegan hegan
Eguzkipean
Euskal Herrian ametsetan

Kometa txuri, gorri, berde
mila kolore
Sokak askatuta 
loturarik gabe