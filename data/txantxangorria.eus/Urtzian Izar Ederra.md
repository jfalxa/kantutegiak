---
id: tx-2131
izenburua: Urtzian Izar Ederra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I4hc34kOh4s
---

Nire semeari eskainitako abesti herrikoia:

Urtzian izar ederra 
gabaz agertzen da 
zure irudia neretzat 
ederragoa da. 
Oi maitia. oi laztana 
neure poza ta atsegina 
zu gabe bizitza neretzat 
mingotsa da.

ETXEAN NAHI ZAITUGU