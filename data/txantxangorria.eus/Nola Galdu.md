---
id: tx-335
izenburua: Nola Galdu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LWZFJdNcWdA
---

Nola bizitzaren ur ertzean hondarturiko balea hura
Nola jaten eman eta esku-ahurrez ureztatzen zuena
Bere aldean berarengan itsu eta garbi sinesten zuena
Balea atera zen eta han geratu zinen eskuetan ura
Eta orain zaintzen zaitut urrunetik begira
Ni lehorrak itotzen nau eta zu berriz urak
Dena da horren erraza hasten
Baina horren nekeza hazten
Eta gero berriz ia ezinezkoa ahazten
Eta orain zaintzen zaitut urrunetik begira
Ni lehorrak itotzen nau ta zu berriz urak
Zuk aurkitu zenuelako nik bilatu nahi ez nuena
Baina nola ez duen beti jasotzen ereiten duenak
Zuri ikasi nizun nik dakidan gauzarik baliotsuena