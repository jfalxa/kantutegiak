---
id: tx-2017
izenburua: Amnistiaren Dema
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dr2UlgNUdW0
---

Amnistiaren alde,
zigortuen alde,
gu ere haiekin bat,
zigorpean gaude.
Ez dugu izaterik
aske haiek gabe
Haiekin askatuko
baigara gu ere.

Amnistiaren dema
garaipen eguna
lortzen ari garena
lortuko duguna.

Amnistiaren demak
bakea du izar
eta askatasuna
oinarrizko indar
herri dena abian
hego eta ipar
gaur borroka dugu ta
garaipena bihar.

Amnistiaren dema...

Zaudete ziur haiek
Ez dute etsiko
ez ditugu guk ere
horrela utziko.
Denak noizbait etxean
zutik eta tinko
egon ziur lortuko
dugula betiko.

Amnistiaren dema...