---
id: tx-2347
izenburua: Bi Ama Baditit
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/L5haekL7IaA
---

1. Hanitxen gisa baditit
Bi ""Ama"" amiragarri
Enia eta Euskadi
Biak pare gabeko dira
Afekzionez betherik
Biak hanitx maite ditit
Eta harien besuetarik
Gogo hunez Khantatzen dit

2. Ô ""Amatto"" etxen dena
Zü zira lehen lehena
Nitzaz orhitü zirena
Zunbat aldiz Kausatü deizüt
Hanitx turment hanitx phena
Beitzen zure bihotz mina
Har nezan bide xuxena
Hartakoz zütüt maitena

3. ""Ama"" Euskadi-entako
Zer nüke suetatzeko
Oso irus ikhusteko
Gaztelietan dutien haurrak
Aurthen libra ditin oro
Zer alegrantzia gero
Haren eta guretako
Pau-eko Esualdünentako"