---
id: tx-1482
izenburua: Ez Da Ezetz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5c3gtKroIbE
---

Musika: Xabi Solano
Letra: Jon Garmendia "Txuria"

Gaur da larunbata...
gauari su ematera aterata
titipuntak ere
musikaren erritmora dantzan daude.

Beroa baino ez...
Ez zaitez pegatu nigana mesedez
Nahi dudanean... bai!
Eta utzi bakean ez baldin badut nahi.

Eta...
zurekin goxo-goxo egon nahi dut
nire desio denak bete nahi ditut
Eta...
zurekin pornografia nahi dut
nire desio denak bete nahi ditut

No... es no!
Solo no
Quita bicho
que te he dicho que no.

Ez da... ez!
Da... ezetz!
Uler dezazun:
Ez da ez!

tinc dos cadenes que em nuguen les mans
i una condemna que et tanca la boca
tens la consciència a puntet de plorar
perquè hem mamat la moral del masoca
y una condena que tanca la boca
i vaia, jo vull entrar al teu cavall de Troia
la llen/gu/a mana: figa, cul i polla
Y NO!


Eskuak soinean...
Nik nahi gabe jartzen dizkidazunean...

Tabernako gauak nire legeak ditu, 
nire arauak
Errespetatzea
ez dut uste denik asko eskatzea!
Salto egin nahi dut
Libre naizelako
salto egin nahi dut!