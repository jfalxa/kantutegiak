---
id: tx-580
izenburua: Henry Millerri Eskutitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_IdMZwvGb8k
---

Sexoa gora, sexoa behera
sentitzen detan pereza
garai batean izan genuen
zorionaren promesa
plazera ez da beti haundia
postura aldrebesa
gora begira jartzen denean
zeru-lurren jueza.
Desio dugu desio bera
desio dugu ez ohia?

zoramenak jotzen duen arte
modu baldarrean goia
misterioa desegiten da
askatzean botoia
pasioaren hilobia da
eguneroko ohia

Maite duenak edertasuna. 
maite duenak lilura. 
hurbiletik begira dezala. 
Platonen liburu hura. 
oturuntza ez da asetzeko. 
norberaren ardura
amaitzen ez den miresmena da
ametsaren aingura

Zahartzaroan sentitzen du batek
nereaz konturatuta
zeinen laburra izan den uda
ia neguan sartuta
xurgaketan ibili beharra
burua makurtuta
arrek ez dugu asko irauten
bandera altxatuta.

Petrarcak Laura, Dantek Beatriz,
eta Pellok Madalena
maitasunean egin behar da
bestearekin ahal dena
batek poemak, besteak gerra
ahitu arte kemena
kupirik gabe fornikatzea
horixe da onena

Egon badago, oraindik, “Henry Millerri eskutitza” (1999) bezalako kanturik; gai arina jorratzen du —“ez noblea”, esango genuke modu aristotelikoan—, umoretsua da, eta formalki bertsolaritzaren patroiari obeditzen dio argi eta garbi. Halako abesti bat, neurri handi batean, “Gizon arruntaren koplak” (1976) edo “Teologia, ideologia” (1978) bezalako abesti baten lerroan kokatu behar litzateke. Sortzailearen asmo epatatzaileak, hitzei darien umoreak, irudietan dagoen esajerazioak eta kutsu bufoi orokorrak egunerokotasunari loturiko bertsoen taxukera gogorarazten dute65. Musikak ere, alai eta bizi, eta esateko erak, umore eta epatatze asmoa markatuz, bertsolaritzaren inpronta ikusarazten dute “Henry Millerri eskutitza”-n. “Henry Millerri 
eskutitza” satira gisa irakur daiteke, baina hemengo erotismoa “serioa” da.