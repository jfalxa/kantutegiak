---
id: tx-1331
izenburua: Zerotik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O0Pn-_E0qZg
---

('Hesian', Baga-Biga, 2016).

Jarraitu gaitzazu! / ¡Síguenos! / Follow us!


Nekatu naiz jada, berriz hasi nahi dut
zerotik ahal bada iragana ahaztuz
urtetako zama arindu behar dut

bizipen saminak kanporatuz.

Ez dago atzorik, oraina besterik
bizitza bakarra baino ez dago
gaurkoz soilik bada, ilea askatu,
erotu gaitezen, dantza dezagun!

ASKE SENTITU, ASKE IZATEKO,
MUNDU ZORO HONETAN ZORO EDERRENA NAIZELAKO!

Txoriak buruan, non nahi ametsetan,
bizitzako harian orekaria,
bide zuzenik ez, goazen idaztera,
mila istorio zoriontasunera!

Burua jan beharrean, mundua jan nahi dut,
ni naizena izan, nigan sinistu,
ez dago mugarik, helmugak besterik,
bizipoza derrigor dezagun!

ASKE SENTITU, ASKE IZATEKO, MUNDU ZORO HONETAN, ZORO EDERRENA NAIZELAKO!

Eta noizean behin denborari putz egin
oroitzapenak orainera ekarri,
horrela ikasi nola desikasi, argi ikusi zer nahi dudan,
bizitzera noa!