---
id: tx-462
izenburua: Suge
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/of0T6vA8Vd0
---

Suge Addar taldearen single berria da. 2021eko ekainean grabatua Mario Gutierrez (Panpot) eta Xabier Eguiaren (El tigre estudios) eskutik.
Bideoa: Xabin Zabala & Mikel Marin

Contact: addar.rockband@gmail.com



Ez dira ikusten jainkoen semeak, 
sagar ustel baten truke...suge
erdi biluzten gorde du azala, 
iltze herdoildu bati heldute...suge
bizia izutzen gaur zabortegira 
kondenatu gaituzue...suge
katea estutzen, merezi duzula, 
zeruan hartu lekue...suge