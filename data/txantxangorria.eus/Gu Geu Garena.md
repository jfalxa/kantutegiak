---
id: tx-1
izenburua: Gu Geu Garena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4uOJ1z5R1KA
---

Logeletako sabaiek 
hitz egingo balute… 
Esan ezetz, esan baina
berriz egingo nuke
  
Inoiz egin dudan bidaia luzeena
itsaso gorriko olatuetan
Ibili naiz ibili hego-haizetan
kasko bat buruan eta bi oinetan
 
Erori naiz erori buruhausteetan
erantzunik gabeko barne bidaietan
baina gaur utziko ditut mamu ilunak
Piztu dezagun benetan gu geu garena!

Senaren bideak berpiztu
oraingoz dramak itzali
hortik dela uste baduzu
ez pentsatu eta segi!

Gaur soinean ez daukat pisurik
hel nazazu bitartean eskutik
Gaur bihotzak ez dauka izurik
askatu dut eta…
benetan ni neu naizena
 
Esan ezetz, esan baina
berriz egingo nuke