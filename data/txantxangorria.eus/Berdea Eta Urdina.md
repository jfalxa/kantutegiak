---
id: tx-437
izenburua: Berdea Eta Urdina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dam0ecYX7lc
---

Siniste hutsa ere
nola  zeran nerea?
nola maite zaitudan 
urdina  ala berdea

Bi begi zoragarri  
handiko erregina zelaien berdea eta 
itsasoko urdina.

Hain mundurantza zabal 
hain mundutik ordea 
lore bitxi bat zera
urdina  eta berdea.

Zugan gorpuztu diren 
bi arimaz egina
bietan zein hautatu 
berdea ala urdina?

Aldapan gora doan 
etxerako bidean noala, 
desio det berriro 
besotan hartu nazala, 
Itsasoa eta lurra 
menperatzen dituen sorgina 
zu zera haizearen 
berdea eta urdina.

Aldapan gora doan 
etxerako bidean noala 
desio det berriro 
besotan hartu nazala. 
Itsasoa eta lurra 
menperatzen dituen sorgina 
Igeldo haizearen
berdea  eta urdina