---
id: tx-3314
izenburua: Itxaropena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WPOF9U6fgC0
---

Biziaren hatsa berpizten denean,
arbola loratzeko hastapenean.
Udaberriko txoriekin,
nere buru, nere indar, nere aho eta tripekin,
abesten zaitut itxaropena...
Zu zara egiazko izar eztia,
izpi zizta zorroz ainitzez bestia.
Udaberriko txoriekin,
nere gogo, nere bihotz, nere amets guztiekin,
abesten zaitut itxaropena...
Bazter guztiak jantzi ederrenean,
argia iluna gaindi duenean.
Uda beteko txoriekin, 
nere azal, nere odol, nere hezur eta zainekin, 
idazten zaitut itxaropena...
Zu zara egiazko gotorlekua, 
guda daukalarik ikaran mundua. 
Uda beteko txoriekin,
nere gogo, nere bihotz, nere amets guztiekin,
idazten zaitut itxaropena...
Mendietako haizea oldartzean,
ziluriasko hostoak erortzean,
Larrazkeneko txoriekin, 
nere etsi, nere argi, nere izatearekin, 
margotzen zaitut itxaropena.
Zu zara egiazko hego haizea,
mila koloretan eraldatzen dena. 
Larrazkeneko txoriekin,
nere gogo, nere bihotz, nere amets guztiekin,
margotzen zaitut itxaropena...
Heriotz irudi isiltasunean,
elurrez estali lurraren gainean. 
Negu gorriko txoriekin,
bere beso, nere gorputz, nere esku billduekin, 
babesten zaitut itxaropena..
Zu zara egiazko bake fereka,
geroa eta iraganaren oreka.
Negu gorriko txoriekin,
nere gogo, nere bihotz, nere amets guztiekin,
babesten zaitut itxaropena...

Patxi Saiz jaunaren kantu polit bat. 1998. urtean, "Gaur olerkiak, bihar euskal presoak herrirat!" bilduma barruan kaleratu zuen, Iparraldeko Preso Sustengu Komiteen Koordinaketari laguntzeko.

1965. urtean jaio zen Patxi Saiz, Getaria herri txikian. 1983. urte inguruan hasi zen jendaurrean jotzen, Ibai Rekondo lagunarekin batera. «Kantatzeak asko betetzen ninduen. Etxean ere beti izan dugu abesteko ohitura. Hortik sortu zen Ibai lagunarekin harremana, bere gurasoak bazkari batean ezagutu baikenituen». Era askotako abestiak egiten ditu Patxik, amodiozkoak edota tonu politikoak, beti bere ahots indartsu eta sakonarekin. Bere estiloa folk da, malenkonia eta nostalgiaz bustia. 1987, 1990 eta 1992. urteko Euskal Kantu Txapelketetan sariak irabazi zituen eta 1988 eta 1990. urteen bitartean, Itziar Zamora kantariarekin bikotea osatu zuen. «Hiru alditan hartu nuen parte Euskal Kantu Txapelketan, bi bider bakarlari gisa eta beste batean Itziar Zamorarekin».

1991 eta 1994. urte bitartean, Kazkabarra taldeko abeslaria izan zen Patxi. "Azken saltsak" (1991) diskoa kaleratu zuten banda honekin. «Kazkabarra taldearen azken kondarrak ezagutu nituen nik. Kantari jardun nuen baten faltan ibili baitziren tarte batean». Euskal kantagintza berreskuratzen lan oparoa egin zuen Kazkabarrako kideak. «Egin beharreko lan bat zen eta derrigorrezko eginbehar izaten segitzen du egun».

1994. urtean, Patxi Saiz jaunak bakarlari lanari ekin zion maketa pare bat ekoiztuz, "Ibaiak dira aske dabiltzan bakarrak" eta "Iratzarra" maketak. 2003. urtean, "Galdetzen", bere lehen diskoa aurkeztu zuen eta bi urte geroago, "Haize urrunak", autoekoizpen moduan. «Nik bazterkeria jasan dut diskoetxeen aldetik. Ez dizute ezezkorik ematen. Esaten dizute hurrengo batean, hurrengo batean, beti aitzakiekin», adierazi zuen kantariak. 

2011. urtean, "Itxaropena" diskoa ekoiztu zuen Patxik. Bakarlari gisako hirugarren lan honetan, berriro autoekoizpenaren aldeko apustua egin zuen, bere musika "duintasunez" kaleratzeko asmoz. Juan Luis Aranburu, Xabier Azkarate, Josu Salegi, Nerea Lasarte eta Kemen Lertxundi musikariek hartu zuten parte lan honetan, akordeoia, pianoa, oboea, bibolina eta beste hainbat instrumentuak joz. 'Itxaropena" diskoak, Ernest Arranbide jaunak kartzela barruan idatzitako testu bat zeukan oinarri. Badira ere gizatasunaren eta zorionaren inguruan taxututako aleak, 'Guajira txiki' edota 'Zutaz oroitzean' kantak adibidez. "Elkarri begira" kanta amodiozko abesti bat zen eta "Gernika", Basarri poetak idatzi zuen testua, suntsiketa hartaz hitzeginez.