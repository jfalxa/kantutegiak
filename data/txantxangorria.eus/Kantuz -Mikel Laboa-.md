---
id: tx-3169
izenburua: Kantuz -Mikel Laboa-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0xZ-DFylLOs
---

Kantuz sortu naiz eta kantuz nahi bizi,
kantuz igortzen ditut nik penak ihesi;
kantuz izan dudana zerbait irabazi,
kantuz gostura ditut guziak iretsi;
kantuz ez duta beraz hitzea merezi?

Kantuz eman izan dut zonbeiten berriak,
kantuz atseginekin erranez egiak;
kantuz egin baititut usu afruntuiak,
kantuz aitortzen ditut nere bekatuiak,
kantuz eginen ditut nik penitentziak.

Kantuz ehortz nezate, hiltzen naizenian,
kantuz ene lagunek harturik airian;
kantuz ariko zaizkit lurrean sartzian;
kantuz frago utziko diote munduan,
kantuz har diten behi nitaz orhoitzian.