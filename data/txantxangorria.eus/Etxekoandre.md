---
id: tx-932
izenburua: Etxekoandre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q8QqN1wfLt4
---

Etxeko andre ederra nintzen, ederra eta maitea.
Etxeak janzten nituen eta ni nintzen etxe betea.
Etxeko jauna lan haundietan kanpoan zebilen beti.
Behin neskame bat ekarri zuen ez nedin hain bakar senti.
Neskameari ez zion uzten langabean asper zedin.
Nirekin amets egiten zuen beste guzia harekin.
Behin erran nuen eder naizeno aldatzea komeni zait.
Ederra baina zerbait gehiago izango naizen norabait.
Maita, maita, maita nazazu gutxio
Goza, goza, goza nazazu gehiago.
Etxe batean atea jorik nekez ideki zidaten.
Txakurrak eta haurrak bakarrik ezagutu bait ninduten.
Jauregi handi bat ikusi nuen eusko zeozer izenez.
Arrotz gelara pasa ninduten nongoa zara esanez.
Euskal zeozer zioen batean hartu ninduten zatozka.
Derrigorrezko poza ote zen gelditu zitzaidan kozka.
Eliza edo zirudien bat han gogoz hartu ninduten.
Noan kanpora pentsatu nuen hil usaina dago hemen.
Herriko andre eder izana kalean eskale nago.
Maita nazazu gutxiago ta goza nazazu gehiago.