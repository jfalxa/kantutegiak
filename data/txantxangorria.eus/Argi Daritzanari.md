---
id: tx-3384
izenburua: Argi Daritzanari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OeNWnJqRpfw
---

Argia daritzanari -Arnaut Oihenart
Hire abilidadea nahiko nikek koplak idazterakoan.

Bost silabako lerroak errimatuz, trinko bezain tinko, rap letrak idatzi hituen oharkabean.

1657an.

Hire gaitasunak berdintzen ikasi artean, hire letrak kantura eramango dizkiagu. Oraingoan Ibon (Norte Apache), Endika Lahaine eta Maisha lagun handien ezinbesteko laguntzarekin.

Gauik, egunik

Eztinat hunik,

Hireki ezpaniz Argia:

Hireki ezpaniz

Itsu hutsa niz,

Zeren baihiz en'argia.

 

Hirekil'aldiz,

Bederak'aldiz,

Bazter lekutan baturik,

Nonbait banago,

Nun botzen-ago,

Ezi Errege gertaturik.

 

Lagun-artean,

Hel nadinean,

Elhaketan, erhogoan,

Eznun dostatzen,

Eznun mintzatzen,

Hi beti, beti, aut gogoan.

 

Nik dudan lana,

Ezin errana,

Zer koeinta dudan higati:

En'exil, eta

Maiz pensaketa,

Egonak ziotsan nigati.

 

Kadran-orratzak,

Burdin-aitz latzak

Hunki-eta buztan mehea,

Xuxen, han hara,

Eguerditara

Diadukan punta xehea.

 

Ni, hala hala,

Tiraz bezala,

Hik joz geroz bihotzean,

Hiri gorpitzez,

Beti, ed'orhitzez

Narrain, bait'are lotzean.

 

Bana herabe

Staliri gabe

Dinat, Argia, jitera

Hir'etx'irira,

Leku agerira,

Jenten minz'erazitera:

 

Beraz higanik

Egin, jadanik,

Noiz nakidana dakidan

Jin, eta nura,

Gorde-lekura,

Eta hi han bat akidan.