---
id: tx-521
izenburua: Bizi Gira...Kutxa Bira!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/URg_CLXs3Xs
---

Baionatik Segurara
Deikaztelutik Viedmara
Eskurik esku
Kutxaren baitan herri gara

Egin adina egiteke
Baina ekinez lor daiteke
Ahorik aho
Kutsa daitezen kutxa bete

Orroz jorra urrez urra
Elea erein ta landa
Hitza bada gure lurra
Adar oro erroan da

Aztarna ahaztuen lokarrira
Urrats berri biziz lotu gira
Herririk herri
Laxa dezagun kutxa bira