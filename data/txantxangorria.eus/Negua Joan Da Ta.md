---
id: tx-3067
izenburua: Negua Joan Da Ta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kZCNt9ZY4co
---

Audioa: Antton Jauregi "KARANTTON"

Elurrak joan direnean nire mendien artean
Eguzkia teloian atzekaldean da

Ateratzeko beldur da, beldur eszenikoa
Aspaldi antzeztu ez duen obra honetan

Izpi txiki txikiren bat agertuz doa gaurkoan
Poztasun handi batek besarkatzen nau

Mesedez ozen esan negua joan egin dela
Nire arima hotzak ez du sinizten eta

Laztandu nazazu orain, ur urdinen artean
Orain lainorik ez da, aurpegi biluziak
Ta larrua jotzean garrasirik ozenena,
Gordin amaigabea, negua joan da ta.

Jaio berrien antzera taupaden beharra dut orain
Belarriekin ikusteko nire begiek entzuten ez dutena

Mesedez ozen esan negua joan egin dela
Izara guztiak erre ditut eta

Soinu bakar bakarra zure bularraldean
Negua joan da ta
Borobildu zaizkit ertzak zure ondoan
Izpi lasaigarri bat
Negua joan da ta, negua joan da ta