---
id: tx-347
izenburua: Errurik Gabeko Habia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/R-gbYNnP_ho
---

Olerkia Harkaitz Cano


Errurik gabeko habia
Errua errun duen oiloa zara: 
zure errua, zure pozoia, zure bazka. 
Errua errun duen oiloa zara. 

Maldan behera zoaz, 
erruberari bultzaka; 
erru ezberdina da erru bera. 

Erruak dakar erru-dantza; 
erruak soilik du erruaren antza. 
Zer izan zen lehenago,
errua edo arrautza? 

Eta errurik ez balitz? 
Bizitza balitz habia? 
Dena bere kabuz balebil? 
Kulparik hona ez ekarri, 
nirekin ez konparti…  

Bizi gara larru gorririk; 
larru gorritan ez da errurik,  
ez deabru, ez aingerurik. 

Aingeruak ainguraturik, 
gura hain makurrak eni loturik... 
Erruari erranez berriz:  
“Errua, diat erruki!”

Errua errubera bat da,  
eta doa maldanbehera;  
erru ezberdina da erru bera, 
horregatik diotsut zera: 
“Arbola pean behar zaitut lurpera.
Ez lurpetik atera, ez itzuli etxera!” 

Gizon errugabea, pausajea arin.
Emakume errugabea, aise dabil. 
Erruaren hazitik dator gorantza:  
zer izan zen lehenago, 
arrosa edota arantza?
Erruak dauka matriuskaren antza: 
handiak barruan txikiak dauzka. 
Errua errun duen erruduna 
naizenik ez erran: basta…! 

Errua errun duen erruduna 
naizenik ez erran. 

Manipulazio estrategiak (*Zart, 2020)