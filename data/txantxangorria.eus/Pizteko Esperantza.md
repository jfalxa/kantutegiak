---
id: tx-2659
izenburua: Pizteko Esperantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fMulA_UlulY
---

Herrian batzuk dira oso ondo bizi
patxada ederrean nik ditut ikusi
diruaren bidea baitute ikasi
gizonki jokatzea ez al da lehenbizi?
 
Gazte idealistak burrukan sartuta
ia danak dauzkagu zeharo ahaztuta
jarraituko degu guk berriz zapalduta
herria nola askatu, haik kartzelatuta?
 
Euskal gazte zintzoak zuen omeneko
bertso bero bat daukat zuek aipatzeko
legez debekatu da hola bizitzeko
beste biderik ez da libre izateko.
 
Askatasun bide hau bai dala garratza
lan handia daukagu kementzen bihotza
gure kalbarioa euskaldun gurutza
hilagatik badegu pizteko esperantza.