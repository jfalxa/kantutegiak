---
id: tx-3075
izenburua: Ikusi Eta Ikasi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/P0-sdzsoMUs
---

Ikusi 
Ikusi eta ikasi 
Ez pentsa ezerren beharrik daukazula heltzeko hire helmugara
Entzun 
argi eta garbi entzun
Hire inguruan esaten diren gauza guztiak entzun

Ikusi
Ikusi eta ikasi
Ez esan gero inoiz aukerarik ez duzula izan

Heldu
Ondo heldu
Ez galdu bidean inoiz berriz ez duzun edukiko gauza

Ikusi eta ikasi
Eta ondo entzun
Hobeto heldu
Hobeto heldu

Ikusi eta ikasi
Eta ondo entzun
Hobeto heldu
Hobeto heldu

Ikusi
Ikasi
Entzun
Heldu

Ikusi eta ikasi
Eta ondo entzun
Hobeto heldu

Ikusi eta ikasi