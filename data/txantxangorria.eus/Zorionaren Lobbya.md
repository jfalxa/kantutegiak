---
id: tx-962
izenburua: Zorionaren Lobbya
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m9G49o4eeGw
---

Ez zaitut ahaztu baina nekatu naiz
zu gogoratze hutsaz
nire datuak doan eman ditut zorionaren lobbyan
eta orain…

Ez zaitu ahaztu baina nekatu naiz
zu gogoratzen hutsaz
hosto galkorreko arbolan egin dizudan kabiak
kaiola forma dauka
eta orain…

Nola esan
sakon hunkitzen nauela aldatu nahi duen jendeak
euren ahalegin zintzoak

Sakon hunkitzen nauela
niretzat ez dudan ekimena besteengan ikusteak
tarteka utzi arren
nire akatsak errepikatzen
buruz ikasi arte
zaude lasai: nire datuak asapaldian dauzka
zorionaren lobby nekaezinak