---
id: tx-2534
izenburua: Eguzkitan Din Dan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y-DoDcTzQmg
---

Eguzkitan din-dan,
euritan txiplisplan,
baratzan larrosak
eta krasminak
dantzan ta dantzan.

Din, dan, don.
Din, dan, don.
Gure bara/tzan,
larrosak dantzan.
Din, dan, don.
Din, dan, don.
Gure baratzan,
larrosak dantzan.