---
id: tx-2588
izenburua: Gure Istorioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iIQSrF2K5Do
---

Hor doakizue bertsoa
oraingoan gure istorioa
nahiz pozik ikusi

alaitsuak beti
barrutik doa prozesioa.

Egunak joan eta etorri
guretzako denak dira berdin
batzuetan gogoa
ez zaigu nahikoa
eta horrela gabiltza herriz herri.

Hau da gure bizitza
musika eta hitza
tresnekin honela gabiltza
festara etorri
alaitasuna jarri
eta "topera" momentuak bizi.

Argi dago gure helburua
ez ibili nekatzen burua
hau da gure toki
guretzako aski
irriparrez beterik ingurua.

Hasierako pretentsioak
handiak ziren ilusioak
jendeak batetik
nagiak bestetik
hortik joan zaizkigu afizioak.

Hauxe da...

Hau da gure bizitza
musika eta hitza
tresnekin honela gabiltza
festara etorri
alaitasuna jarri
eta "topera" momentuak bizi.

Hauxe da...

Hau da gure bizitza
musika eta hitza
tresnekin honela gabiltza
festara etorri
alaitasuna jarri
eta "topera" momentuak bizi.