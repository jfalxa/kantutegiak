---
id: tx-698
izenburua: Iñurriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qezhg9xlC3Y
---

Orain inurriak
kontau gura dodaz
lokartu aurretik
olgetan badoaz
goiz itzartuta be
dagit irribarre
lehen ametsa zana
amets ez danetik.

Mundua ez dala
horren arraroa
txikitu dalako
usteak naroa
eskuei begira
handitu egin dira
neure besoetan
hartu nebanetik.

Gaur bizitza dator
ogi samurrean
barretxu ta usain
lehertu beharrean
hau gosea, ama!
asetu ezin dana
hamaika mosutan
jaten dodanetik.

Poz barri batera
jaioten ari naz
inoz galdutako
ilusinoagaz
dana ez da zelai
baina loratsu bai
lapiko txikiak
urtzen nauenetik.

Alde egin dau lehengo
hainbat bildur txikik
ta mamuak datoz
kortinen azpitik
damuak auzoan
gu aro gozoan
trenak agurtzea
jolasa danetik.

Maite dogulako
ilargi betea
eta egunero
barre egitea
taupadaz taupada
aberatsak gara
urrezko laztanen
jabe garenetik.