---
id: tx-445
izenburua: Lehen Floria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yD-sPylh5rI
---

Eijerra zira maitia
erraitzen deizüt egia
nurk eraman ote deizü
trai la la la lai
zure lehen floria?

Ez dizu egin izotzik
ez eta ere karruñik
ene lehen floriari
kalte egin dienik.

Landan eder iratze
behia ederrak aretxe,
zu bezalako pollitetarik
desir nüke bi seme.

Horren maite banauzu
obrak erakats itzatzü:
elizalat eraman eta
han espusa nezazu.

Apezak dira Españian
beretterrak Erruman,
hurak anti jin artino
gitian txosta kanberan.