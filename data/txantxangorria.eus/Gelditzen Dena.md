---
id: tx-1885
izenburua: Gelditzen Dena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AYUVGpngH9M
---

Uretan galtzen den laztana bezela, naiz 
Uretan itotzen den arnasa bezela 
Uretan isiltzen den oihua bezela, naiz 
Uretan iluntzen den argi izpia bezela, naiz 

Uretan hiltzen den olatua bezela, naiz 
Uretan ahazten den zenbakia bezela 
Uretan lehortzen den ahoa bezela, naiz 
Uretan pizten den zulo beltza bezela, naiz 

Gelditzen dena...isiltasuna 

Uretan isurtzen den hodeia bezela, naiz 
Uretan ahitzen den haizea bezela 
Uretan jaio den orbela bezela, naiz 
Uretan anizten den desertua bezela, naiz 

Barne barnean itotzen naiz,
barne barnean murgiltzen naiz,
barne barnean itotzen den haizearen oihuarekin bat,
galtzen den uretan itotzen naiz.