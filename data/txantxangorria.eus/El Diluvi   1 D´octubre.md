---
id: tx-1028
izenburua: El Diluvi   1 D´octubre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M6DQVaSWWT8
---

Udazkenarekin udaberria etorri zen,
erabakitzeko eguna,
gelditu edo alde egin 
aukeratzeko ordua.

Bat eginda gorputzak, anaitasun begiradak
hauteslekuak babesteko lo egin gabeko gauak. 
Egunsentiak euria eta hautesontziak zekartzan,
maitasunezko bozkak eta bihotzik gabeko txakurrak.

Denak bat eginda 
herriaren borondatea adierazi ahal izateko!

Gure oroimenean beti geratuko da kataluniar herriaren duintasuna.
Gure oroimenean beti geratuko da duintasunaren urriak 1a.
Arrazoiaren indarrak errepresioaren indarrari irabazi zionekoa.

Ikastetxeetan jipoiak, piztiek apurtutako ikastokiak
Gazteak eta amonak espainiar demokrazia dagoen lurrean kolpatuak.
Nola azaldu haurrei honelako basakeria?
Nola onartu bozkatze hutsagatik jipoitua izan zitezkeela?
Baina herriak beldurrari aurre egin 
eta hautesontziak betetzeko adorea atera zuen.
Baina herriak beldurrari aurre egin eta alde egitea erabaki zuen!

Errepublikaren ordua da
Askatasunaren ordua
Bi herri batera, eskutik helduta
Askatasunera!