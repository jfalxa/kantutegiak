---
id: tx-3348
izenburua: Gure Herria Gure Erabakia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QcEYllhY40k
---

Martxoaren 7 eta 8an Artikako "El sotano"estudioan Iker Piedrafitaren gidaritzapean grabatua.
Musika:Juanpa Agirre eta Fran Urias (Hesian)
Hitzak:Mikel Mundiñano "Zato" (Tximeleta) eta Fran Urias (Hesian)
Parte hartzeak:
Bateria:Endika Jaka
Baxua:Luisillo Kalandraka (Vendetta)
Gitarrak:Fran Urias (Hesian)
Trikitixa:Mikel Mundiñano "Zato" (Tximeleta)
Ahotsak:
Juanpa Agirre
Fran Urias (Hesian)
Mikel Mundiñano "Zato" (Tximeleta)
Adur Goikoetxea
Nekane Jaka
Maider Ansa (Tximeleta)
Lur Larraza
Amets Larraza
Jonathan Rodriguez "Pox"
Jokin Artieda "Armo"
Izaskun Razkin
Nerea Gonzalo
Maria Goikoetxea
Oihana Razkin
Jon Goikoetxea
Xabier Garziandia
Joxe Garziandia
Angel Mari Urrestarazu
Juana Jaka
Arrate Arizkorreta


GURE HERRIA, GURE ERABAKIA
 
HERRIA, IRITZIA
ETORKIZUNA, ERABAKIA

Zazpitan ehun 
Urteetan herri  
Guztion eskutik 
malko ta irri 

Arbasoena 
eta gu(u) denon aberri 
Izan dadila
hi(i)tza denon aldarri 

Denon esku 
zuek eta eurak 
idei ezberdinak
bide berri bat 

Eraikitzeko
Zubi bat, galdetu herriari 
Amets eitteko
Hitza_hartu, hitz egin 

GURE HERRIA, GUZTION IRITZIA
ETORKIZUNA, GURE ERABAKIA 

Apirilan
Atzean elurrak 
Hamahiruan                   
Ahaztu beldurrak 

Aiherra/gorrotiñe utzi
Ez dugu, bizkarrez bizitzerik 
Hitza emanez                              
Ez dugu, mugarik

GURE HERRIA, GUZTION IRITZIA
ETORKIZUNA, GURE ERABAKIA 

Etxarrik emango du lehen pausoa
Bihurtzeko eskubide bat denon gogoa                
Biharko eguna, guztion ardura
Gure_eskuekin, gure modura