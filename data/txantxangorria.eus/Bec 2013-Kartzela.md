---
id: tx-1754
izenburua: Bec 2013-Kartzela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Lq6mXO7Xiqg
---

Andaluzian sortu nintzen ta
Rodriguez hiru abizen
ikasketetan nola eneukan
aski argi ta aski zen
hogei urte ta han hasi nintzen
Benemerita serbitzen
trafikoari paso emandak
goizetik gau izerditzen
ta azkenean Donostiako
koartelera heldu nintzen
eta ordutik lana egin dut
Intxaurrondoak astintzen
Intxaurrondoak astintzen

Nere izena gorroto duten
ez dakit zenbat familik
ez da izango Euskal Herrian
ikusi ez dudan mutilik
galdetzen nien: Hik ez ote duk
inoiz hil goardia zibilik
eta hil baduk zer sentitu duk
neure lankide bat hilik
galdera hauei entzun arte
hor egongo haiz zintzilik
baina begiak zarratu eta
segitzen zuten ixilik
segitzen zuten ixilik.

Baina heldu da aro berria
nunbait bukatu da gerla
ene zain daude magistratuak
eta auzitegi-gela
bi ordu dira hontaz ta hartaz
galdezka ari direla
eta orain neu nago ixilik
lehen mutila haiek bezela
tokatuko zait itzal luzea
zulo bateko kartzela
baina espero dut presidenteak
indultatuko nauela
indultatuko nauela