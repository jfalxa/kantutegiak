---
id: tx-1730
izenburua: Heldu Nazazu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B1UnH7wmkGs
---

Isiltasuna
milaka aurpegi
iluntasuna
mila begi
ezkutatuta bizi izan zara,
zure arazoa zu zara
Heldu nazazu
Heldu nazazu
alboan nagola
Heldu nazazu
zure alboan nagola.