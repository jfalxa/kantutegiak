---
id: tx-2333
izenburua: Maialen Ta Amaiurren Bals Pirata
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YaOJ_8ubAqM
---

Azaroaren hamaika
malkoen erresuman,
bere erraietatik atera zineten
nigarrez, nigarrez.

Zuekin bakardadean
bakardade gogorra
ispiritua indartzen duena
triste, triste.

Mundu krudel honen aurrean
poztasuna ta kolorea
bide zailena aukeratzea
izanen da aukerarik onena.

Amaiur-go gatazka da gure oroimena
Maialen martiria, gure ontasun mugagabea

Odoleko, Odoleko
Odoleko arrebak
Lotan aingeruak bezala
Maialen ta Amaiur-ren seaska kantan