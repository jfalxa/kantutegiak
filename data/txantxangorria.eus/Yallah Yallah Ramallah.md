---
id: tx-2806
izenburua: Yallah Yallah Ramallah
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Q2zgsi0Kf0Y
---

Haizea dator Jordan ibaitik
Soinua Mediterraneo itsasotik
Mohamed Al Durra umearen graffiti 
Jamal, nire adina duen aitarekin
Posible al da bizitza berreraiki?
Baietz beste murru bat erori
Jerusalemgo bortan jendea adi
Damasko atetik helduko oihuari:
YALAH YALAH RAMALLAH!!
YALAH YALAH RAMALLAH!!
Allenderena oroituz, odola ematean
Zumardi handiak irekiko dira
Hortik pasatzeko gizaki askea
Jasotzeko gizarte hobe bat 
YALAH YALAH RAMALLAH!!
YALAH YALAH RAMALLAH!!
Al Kasaba zineman
Woody Allenen filma
Melinda&Melinda
Eta Paradise, orain da
YALAH YALAH RAMALLAH!!
YALAH YALAH RAMALLAH!!
YALAH YALAH YALAH RAMALLAH…
Palestinan naiz jada
YALAH YALAH YALAH RAMALLAH…
Esan nuen eta itzuli naiz
YALAH YALAH YALAH RAMALLAH…
Apalkuntza amaitu da
YALAH YALAH YALAH RAMALLAH…
olibondoak nonahi 
YALAH YALAH RAMALLAH!! HEY, HEY, HEY
YALAH YALAH RAMALLAH!! HEY, HEY, HEY
YALAH YALAH RAMALLAH!! HEY, HEY, HEY
YALAH YALAH RAMALLAH!