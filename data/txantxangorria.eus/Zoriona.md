---
id: tx-1011
izenburua: Zoriona
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7nM-K_DIqfs
---

Eromenak esan zidan non aurkitu poza, 
egin topa mozkortu daigun zoriona! 
denbora da nabilela aukera bila, 
adore bila tragoka mozkortu zait ausardia. 
nola baina desiratu askatasuna, 
norbere burua preso hartzen duenak? 
eromenak esan zidan poza non bizi dan! 
ezin damutu izango gara biaramun, 
gaur egiten ez badugu! 
errutina gupidarik gabe kolpeka, 
behin da berriz desira oro hautsiaz. 
eskuminak zoriontasunari aurkitzen baduzu, 
arriskatzen ez duenak ezingo du ezer lortu! 
eromenak esan zidan poza non bizi dan! 
ezin damutu izango gara biaramun, 
gaur egiten ez badugu! 
egin dezagun topa mozkortzeko zoriona 
une zailak gainditzen oroitzapenen ajeak 
lagun gaitzan eromenak esan zidan 
poza non bizi dan! 
ezin damutu izango gara biaramun, 
gaur egiten ez badugu!