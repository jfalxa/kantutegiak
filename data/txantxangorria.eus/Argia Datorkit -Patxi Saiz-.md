---
id: tx-3316
izenburua: Argia Datorkit -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VvANTeOGU14
---

Argia datorkit 
Ikasitakotik desikastera noanean
hurkoaren mina barneratzean
urrikaltzen naizenean
bizitza naizela ohartzean.

Musikak goratzen gaitu gure arima ilunaren
amildegi sakon malkarretatik.
Doinu eder baten kolpe zirrara batez jantzirik
bihotz bateaz berpizten gara.

Argia datorkit
haur baten begi garbiak antzematean
zu ikustean bihotza goxoki,
aztoratzen zaidanean.
Bizitza naizela ohartzean.

Musikak goratzen gaitu gure arima ilunaren
amildegi sakon malkarretatik.
Doinu eder baten kolpe zirrara batez jantzirik
bihotz bateaz berpizten gara.

argia datorkit
osotasunaren parte naizela sentitzean
txorien kantuak entzutean
izarrak begiratzean
bizitza naizela ohartzean.

Musikak goratzen gaitu gure arima ilunaren
amildegi sakon malkarretatik.
Doinu eder baten kolpe zirrara batez jantzirik
bihotz bateaz berpizten gara.