---
id: tx-762
izenburua: Behartsu Jaio Hintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/32IpnL8ub3o
---

Behartsu jaio hintzen
zoria ez duk lagun
diruak sartu hinduen
zuloan zerraldo.

Hirirako bidean
aita ere galdu huen
negarrak irtetzen dik
amaren begietatik.

Errauts haizenez gero,
errezatu gau ilunari,
infernuan kiskalduko haiz.