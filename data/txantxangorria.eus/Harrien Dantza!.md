---
id: tx-2814
izenburua: Harrien Dantza!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MsnTuKXMrMc
---

Kaleko graffitietan, matxinada deia!
Matxinada deia!

Greba orokorretan, harrien dantza!
Harrien dantza!

Auzoko tabernetan, zurito pare bat, Zurito pare bat!
(BIS)

Ieeee, ieeee, Harrien dantza!
Kaleko graffitietan, matxinada deia!
Greba orokorretan, harrien dantza!
Auzoko tabernetan, zurito pare bat!

Bizitza aurrera doa, lau tragoren artean,
eta udaletxea, txerrikume berbera!!

Kaleko graffitietan, matxinada deia!
Greba orokorretan, harrien dantza!
Auzoko tabernetan, zurito pare bat

Bizitza aurrera doa, lau tragoren artean,
eta udaletxea, txerrikume berbera!!

Harrien dantza!
Gure helburua!
Harrien dantza!
Gure esperantza!
Harrien dantza!
Gure entifada!

Bizitza aurrera doa, lau tragoren artean,
eta udaletxea, txerrikume berbera!

Harrien dantza!
Gure helburua!
Harrien dantza!
Gure esperantza!
Harrien dantza!
Gure entifada!

Ieeee, ieeeee