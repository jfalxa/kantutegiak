---
id: tx-1144
izenburua: Geroari Begira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DP7b0CpQ7v0
---

Hizkuntza bat ohitura herria
sortzen segi
dezagun gure aberria
gure mezua ahobetez
lau haizetara euskara izan da
beti izan da
herriarena, herriarena!!!
ARABA EUSKARAZEN
ARABA EUSKARAZEN
GEROARI BEGIRA
AGURAINDIK LAUTADARA
HERRIETATIK MUNDURA
GIRA ETA BIRA
Zatoz hona, gure ikastolara
ta ez geratu begira
mingainetik tira eta tira
margotzeko dena kolorez
apaintzeko arantzaz eta lorez
 eraiki dezagun
emanda jaso ordez.
ARABA EUSKARAZEN
ARABA EUSKARAZEN
GEROARI BEGIRA
AGURAINDIK LAUTADARA
HERRIETATIK MUNDURA
GIRA ETA BIRA
Barrutik hitz egiten
kalera irten
zerk egiten zaitu
 boteretsua
eta adoretsua
pentsa ezazu,
Pentsa zazu, zein den gure erronka
pentsa zazu, euskararen borroka
eta egin dezagun topa
euskararengatik.
Agurainek badituela harresiak
euskarari kendu beharreko hesiak
sutara bota ditzagun
kate guztiak!
ARABA EUSKARAZEN
ARABA EUSKARAZEN
GEROARI BEGIRA
AGURAINDIK LAUTADARA
HERRIETATIK MUNDURA
GIRA ETA BIRA
MIHIAN KILI KILI TA EUSKARAZ IBILI
GEROARI BEGIRA
UZTAZ, UZTA
GARENAREN JOSTUN
BARNERATU BARREIA DEZAGUN
JANTZI GOGOZ TA GOAZEN
JANTZI GOGOZ TA GOAZEN
JANTZI GOGOZ TA GOAZEN
IRIBAR BATEKIN
JARRAITU ERABILTZEN