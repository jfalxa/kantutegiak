---
id: tx-1753
izenburua: Mundu Saldua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lAGvEzbrwPs
---

Eros ezazu ilusioa, 
erosi maitasuna, 
eros ezazu etorkizuna 
edo askatasuna. 

Hemen guztia saldu eta erosten da, 
hemen guztiok merkatuan gara. 

Saltzen dira irri eta negarrak, 
zoriontasuna. 
Baita ere ongizatea 
eta duintasuna. 
Hemen denak dauka diru kolorea, 
hemen guztia erosi ahal da. 

Mundu saldua! Mundu itsua! 
Mundu saldua! Mundu galdua! 

MUNDUA SALGAI JARRI DUGU ETA 
MUNDUA SALGAI ETA GEURE BURUA 
APURTU KATEAK, ASKA GAITEZAN. 

Gaitasuna gu geu izateko, askapenerako. 
Ahalmena bidea aukeratzeko. 
Hutsik ez egiteko. 
Hemen buruaz jokatu beharko da. 
Hemen bihotzaz irabaziko da.