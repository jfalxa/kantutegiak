---
id: tx-77
izenburua: Ni Naiz Kapitan Pilotu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FwW-wKEkfSc
---

Ni naiz kapitan pilotu,
niri behar zait obeditu,
obeditu,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.

Kapitana da txit beltza,
dirudiena alkitrana,
alkitrana,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.

Barkua da oso fina,
dirudiena bergantina,
bergantina,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.

Deuseren beldurrik ez du,
inork ez du hau ikaratu,
ikaratu,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.

Pipa badu ezpainetan ,
ez du beldurrik sekuletan,
sekuletan,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.

Bizar luze gorrixkara ,
holandesaren tankerara,
tankerara,
bestela zenbaiten kasketa,
bonbilun, bonbilunera,
buruan jartzen bazait niri,
bonbilun bat eta bonbilun bi;
eragiok Xanti arraun horri.