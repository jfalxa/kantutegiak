---
id: tx-3410
izenburua: Giza Harresia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Mi2SPVGI93U
---

Belarrira esaten dizkidazun
hitz goxo hoiek
laguntzen didate sentitzen
adiskidetasuna.
Izandako amets gaiztotan
gauza asko dut ikusi,
horregaitik zaude zu nigan
eta aegongo beti.
Besarkada bat
eskertzeko gogoa.
Asmo zintzoek
mundua eraldatzen dute.
Giza harresiak zutik dirau kaleetan,
lorpenen giltzak eta esentziak.
Beldurrei aurre egiteko
babesa eskatzen diet indarrei,
bide egokia aurkitzen
asmatu nahiean.
Ona eta txarraren arteko
gatazka hontan
zein da garaile?Zugan naukazun bitartean
norbait izan ahalko naiz.
Besarkada bat...