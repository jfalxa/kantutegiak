---
id: tx-2572
izenburua: Loa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vaPwz4DSMmc
---

Hegazkinak zeruan
Patrol berdeak mugan
Berba arrotzak udan
Behelainoa neguan…

Leiho bako bihotzak
Lizuntzen ditu hotzak
Iluntzean,
Itzartzean… ta noznahi
Iluntzean,
Itzartzean… be bai.

Loa, 
morfinadun suge
Loa,
osatzeko zu be
Loa,
izan naiten zure 
Loa…

Esan bako guzurrak
Ispilutik begira
Zelan lisatzen dira
Arimako izurrak?…

Agor deukoz ezpanak
Ezagutzen ez danak
Norberegan
Nor zelan dan… denboran
Norberegan
Nor zelan dan… nor dan.

Loa, 
morfinadun suge
Loa,
osatzeko zu be
Loa,
izan naiten zure 
loa
Zeruan hegazkinak,
Arratsaldea jausten,
Gaba egunaz jolasten,
Geografia ezberdinak…

Ez dago mundu zaharrik,
Zu ta biok bakarrik
Iluntzean
Itzartzean… ta noznahi…
Iluntzean 
Itzartzean… be bai.

Loa, 
morfinadun suge
Loa,
osatzeko zu be
Loa,
izan naiten zure 
loa…