---
id: tx-2178
izenburua: Mendigoxaliarena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dVBVqhsM6nc
---

Eñaut Elorrietak (KEN7) "Lauaxeta_2005-ehungarrenean_hamaika" diskan musikatu duen Lauaxetaren olerkia.
Esteban Urkiaga, "Lauaxeta" Laukizen jaio zen 1905ean eta Mungian bizi. Durangoko jesulagunen ikastetxean latina eta erretorika ikasi ondoren, abadetza-ikasketak egin zituen Loiolan eta Oñan, baina mezaberria eman baino lehenago utzi egin zuen komentua. Loiolan euskaltzaletu eta literatur zaletu zen, aita Estefania irakasle euskaltzalearen eta Euskal Akademia osatzen zuten lagunekin, Zaitegi eta Ibinagabeitiarekin, besteak beste, Garai hartan latineko klasikoen itzulpenak egin zituen. Espainian Francoren Altxamendua sortu zenean, zeregin guztiak alde batera utzi eta Eusko Gudarostean sartu zen. Komandante-graduarekin, milizien antolaketan eta propaganda-lanetan aritu zen. Gudari aldizkaria eta euskarazko Eguna egunkaria kaleratu zituen.

Eginkizun horietan ari zela, Gernikako bonbardaketaren ondorioez argazkiak egin nahi zituen kazetari frantses bati laguntzera joan zen. Faxistek preso harrapatu eta Gasteizera eraman zuten Lauaxeta. Heriotza-zigorra ezarri eta fusilatu egin zuten. Heriotzaren zain zegoela idazki hunkigarriak idatzi zituen