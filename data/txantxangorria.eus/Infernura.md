---
id: tx-1620
izenburua: Infernura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sidZkh-85SY
---

Lurra zapaldu orduko bere ingurua loratzen zen, 
takoi altuen gainean zein hankutsik horrek ez zuen axola. 
Baina orain badakit ez dudala nahi, 
joan dadila infernura eta egon dadila bertan nire zain. 
Ederra soilik ez eta bazuen zer esana, 
deabruari ematen zion harek, gau eta egunez jana. 
Eta argia izateaz gain, lapurtu zidan barrena, 
sortuz betirako huts horretan atsekabe errena. 
Eta badakit, bere jokoan jausiko naizela, 
gaizkiaren etxean graziaz, eraztun bat jantziko dudala. 
Eta orain, badakit ez dudala nahi, 
zintzo izan direnekin bat, jainkoaren etxean etzan. 
Eta orain, badakit zer egingo dudan, 
zerua zabalduta ere, infernura jaitsiko naiz.