---
id: tx-1178
izenburua: Urxapal
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d0dBmGaQka4
---

Urx'apal bat badügü herrian trixterik,
Nigarrez ari düzü kaloian barnetik,
Bere lagün maitiaz beit' izan ützirik:
Kuntsola ezazie, ziek, adixkidik.

Oi! ene izatia dolorez betherik!
Mündian ez ahal da ni bezañ trixterik,
Ni bezala maitiak traditü dianik.
Habil, amodiua, hürrün ene ganik!

Traditü zütüdala deitaziz erraiten;
Bata bezain bestia, gü biak bat ginen,
Ene bihotza duzü zuri bethi egonen;
Kitatü behar zütüt ützi lotsaz etxekuen.

Oi! ene traidora, zer düzü jaukitzen?
Elhe faltsü erraitez etzireia asetzen?
Ene flakü izanez zira prebalitzen:
Hortarik ageri'zü nunko seme ziren.

Maitia, nahi zütüt segreki mintzatü,
Arrazuak dereitzüt nahi esplikatü;
Bihotz oroz zün tüdan osoki maithatü
kitatu behar zütüt, hiltzera nuazü

Thunba bat nahi dizütt Iürpian ezarri,
Ene khorpitz trixtia gorde mündiari,
Ene ixter-begiek egin dezen erri:
Haiekin zirade, zu ere, hain sarri.