---
id: tx-191
izenburua: Elantxobe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z4mNR0xwjtQ
---

Muxika ta letra Mikel “Txistulito”

        (1ª estrofa)                             
Arrauna geldi amua bota
arrain txikia ta txipiroia
haizea dator ekaitza dirudi
tira portura Joxe Mari.   

        (2ª estrofa)                              
Aldapan gora usain ona    
saltsa zuria ta saltsa beltza
arrain potea kaltzadan
eta marmitxe etxean.        (bir)

        (3ª) estrofa)                            
Arraun egin arraun arin           
extropadak nork irabazi
Nikolas deuna Elizabarrin
ta Madalenak batek daki.  

        (4ª) estrofa                       
Ogoño mendian Lapatza azpian     
Elantxobe erdian herri jatorra
zein ederra da gure paradizua. (bir)

             (Final)                            
Zein ederra da gure paradizua       
Zein ederra da gure paradizua
Zein ederra da gure paradizua.