---
id: tx-954
izenburua: Bedatsiaren Heltzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/G81mmHJ199w
---

Bedatsearen heltzean
    bazterrak dirade bethatzen lorez
    bortietan ere olha saria
    nola beztitzen belharrez,
    artzainak gü goiz'aldi eder batez
    gora bürüz baguatza plazerez
    gure ardi salduk aintzin otsamenez
    üdakoz dener adios erranez.