---
id: tx-222
izenburua: Askatasunaren Besoetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fkl-uwFa7kI
---

“Askatasunaren besoetan” bideo hau Hernanin grabatu zen Balu.Art-en ekimen batean.
Irudiak baliatu ditugu liburu honen aurkezpena egiteko: Xabi Solano Maiza “Trikitixa liburua III”

Xabi Solano Maiza trikitilariaren metodo didaktikoaren 3. partea plazaratu dute 5GORAk eta Elkarrek. Bildumari jarraituz, liburu-cd honek honako hauek biltzen ditu: herrikoi kutsuko 8 doinu eta Xabi Solanoren beraren beste 13 piezen partiturak, bai solfeorako bai sistema zifraturako, gehi argazki bilduma eder bat. 

Gure buruaren kontra 
Egiten dugu sarri
Gure ekintzak bailira
Lotsaren ezaugarri
Guk ez dugu gerrarik eraman
Guri gerra ezarri digute
Askatasunaren besoetan 
Bakean bizi nahi genuke

Mendeak atzera joan
Eta ulertu ondo
Zenbat liskar izan dugu
Gerra, eta gerra ondo
Guk ez dugu gerrarik eraman
Guri gerra ezarri digute
Askatasunaren besoetan 
Bakean bizi nahi genuke

Konplejuz beteta gaude
Beti kanpora beha
Bestearena bailitzan
Gureak baino hobea
Guk ez dugu gerrarik eraman
Guri gerra ezarri digute
Askatasunaren besoetan 
Bakean bizi nahi genuke


Hau den moduan gabiltza
Dena askatu mesedez
Ta izateko eskubideak
Gurea dugu berez
Guk ez dugu gerrarik eraman
Guri gerra ezarri digute
Askatasunaren besoetan 
Bakean bizi nahi genuke