---
id: tx-1501
izenburua: Buruko Mina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VGjPFdrJTu0
---

ai ei ai ei hauxe da hauxe burko mina
ai ei ai ei glada fitexe mediku ona
ai ei ai ei hauxe da hauxe burko mina
ai ei ai ei glada fitexe mediku ona
tira mihia egin eztula
altxa harazkar ote du sukar
foltsua hartu
tripa ikertu
ez dut ikusten gaitza nun duen
ai ei ai ei hauxe da hauxe burko mina
ai ei ai ei glada fitexe mediku ona
ai ei ai ei hauxe da hauxe burko mina
ai ei ai ei glada fitexe mediku ona
zer daukat goxe horren erixe
zen da hadi ta hatorri baita
oi adixkide noiz da igande
eskolaz libre ez dut min batre

ai ei ai ei hauxe zen hauxe burko mina
ai ei ai ei joan zaitezke mediku ona
ai ei ai ei hauxe zen hauxe burko mina
ai ei ai ei joan zaitezke mediku ona