---
id: tx-2321
izenburua: Sagu Txiki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bk4IZIBq0lU
---

Sagu txiki, sagu maite,
zulotxoan gorde zaitez.
Zapik ikusten bazaitu
kris kras jan egingo zaitu.
Ez nau jango!
Jango zaitu"