---
id: tx-2615
izenburua: Eperrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aW6ouKOQVd8
---

Eperrak badituzu bere bi hegalak
Bai eta bürün gainin kokarda ejer bat
Zük ere balinbazünü gaztetarsün ejer bat
Neskatilen gogatzeko bilo holli pollit bat.

Amorosak behar lüke izan lotsa gabe
Gaiaz ebiltia ez üken herabe
Egünaz ebiltia desohore leike
Txoriak ere oro haier soz dirade.

Ebili izan nüzü gaiaz eta beti
Eia! atzamanen nianez lili ejer hori
Azkenian atzaman düt oi! bena tristeki
Lümarik ejerrena beitzaio erori.

Mündian ez da nihur penarik gabe bizi
Nik ere badit aski hoitan segurki
Nik desiratzen nizün ezkuntzia zurekin
Bena ene aitak eztü entzün nahi hori.

Zure aitak zer lüke eni erraiteko
Ala enitzaio aski aren ofizioko
Ala zü soberaxe zitzaio enetako
Printzerik ez ahal da orai zuenganako.