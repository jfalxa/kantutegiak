---
id: tx-548
izenburua: Beste Behin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QXZrs4XUAP4
---

Musika: Iñigo Etxezarreta (E.T.S.)
Letra: Jon Maia
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Ekoizpen exekutiboa: Baga-Biga

Kantu bat ala liburu bat, xuxurla bat edo haur baten oihua.
Bi begiraden arteko talkatik berriro sortu den mundua.
Eskurik esku lore bat, harri bat, mezu bat jaso eta beste bati pasa.
Amaierarik ez duen musu baten arnasa.

Beste behin itsasoa, beste behin kantu berri bat.
Beste behin "Lau teilatu" eta "zu goruntz begira".
Haizea aurpegian, esku bat izarren bila.
Zure arnasa lepoan, bidea hasi dadila!

Zure eskuko azalean irakurritakoan nire historia
urrunetik entzuten den aspaldiko amets baten melodia.
Desio bat, esperantza bat, milaka begi so eta zu euritan korrika.
Eta ilargiaren argitara entzuten da…

Esaidazu berriro gaur belarri ondora
gure amets hura.
Kantu hau gorde dezala eguzkiak hartzen 
dituen ortzi-mugak.
Berriro bat izateko gau luze bat dator.
Sekretu bat daukazu.
Berriro indar bat dator, gure bila dator.
Besarkatuko al nauzu?

#EnTolSarmiento​ #BesteBehin