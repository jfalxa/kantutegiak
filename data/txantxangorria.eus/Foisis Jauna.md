---
id: tx-1840
izenburua: Foisis Jauna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xjhBNp7GA_8
---

Ez
ez dut utziko inori
nere barruan agintzen
Zu inor zara eta halaere
menperatzen diazu
Lasto ustela izango naiz?
Bakarrik dakit
hala naizela
eta...

Zuk bakarrik dakizu
Pilatos zenbat maite dudan
Zuk
zenbat miresten zalantza
zenbat nazkatzen beldurra
Zuk bakarrik ezgutzen
nire barnea
nire kanpoa.

Ni
reenkarnatuko banintz
obeitzeko nintzateke
(eta) lo
naturaren sartadak
pairatzen ditut garunetan
bibolin hotzak bailira
Horregatikan
maite zaitut gaur.

Bai
orori bai esanaz
farrearen mugetan nauzu
Gaur
elurra bobiletan
funditzen den bezala nauzu
Guttikin naiz zoriontsu
ikusi zaitut...
ikusi zaitut...