---
id: tx-1168
izenburua: Xarmengarria Zira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mOTBH4VIXlk
---

Xarmangarria zira eder eta gazte,
ene bihotzak eztu zu baizikan maite.
Bertze zenbait bezala, ote zira libre?
Zurekin ezkontzeaz dudarik ez nuke.
Bertze zenbait bezala, ote zira libre?
Zurekin ezkontzeaz dudarik ez nuke.
Xuri gorria zira, arrosa bezala:
profetak ere dira mintzo hola-hola.
Araberan bazinu gorputza horrela,
iduriko zinuen.. zeruko izarra.
Araberan bazinu gorputza horrela,
iduriko zinuen... zeruko izarra!!
........................................­...............................
Imanol jaunaren kantu bat, bere "Erromantzeak" diskotik. 1984. urtean ekoiztu zuen, IZ estudioetan, Angel Katarain jaunaren laguntza teknikoaz.

Imanol, ahotsa
Karlos Gimenez, pianoa
Pascal Gaigne, gitarra
Michel Nesprias, soinu txikia
Alfredo Rodriguez, oboea
Enrique F. Altuna, biolontxeloa
Amaia Zubiria, ahotsa ("Argi-azkorrian jinik")
Iñaki Eizmendi, ahotsa ("Xarmegarria zira")
Beñat Axiari, ahotsa ("Bazko eta Salbatore")

Imanol Larzabal abeslariak euskal poesia eta musika uztartu zituen sentikortasun handiko kantetan. Bere ibilbide luzean, Imanol kantariak hainbat musika estilo mota jorratu zituen, kantagintza tradizionala, jazz, rock, edota folk musika. Paco Ibañez edota Gwendal artista ezagunekin lan egin zuen 70. hamarkadan. Beraiekin, Frantzian, Belgikan eta Alemania aldean ibili zen kontzertuak eskeintzen.

80. hamarkada nahiko oparoa izan zen Imanolentzat ikuspegi musikaletik. Ia urtero disko bat kaleratu zuen, "Jo ezan" (1981), Etxahun eta Etxahun" (1982), "Iratze okre geldiak" (1983), "Erromantzeak" (1984), "Oroituz" (1985), "Mea kulparik ez" (1986), "Joan-etorrian" (1987), "Muga beroetan" (1989) eta "Amodioaren berri" (1990). Disko guzti hauetan guztietan euskal poeta garaikideen olerki ederrak musikatu zituen, adibidez, Mikel Arregi poetarenak, Koldo Izagirre, Joseba Sarrionandia, Juan Mari Lekuona, edota Xabier Lete jaunaren hitzak.