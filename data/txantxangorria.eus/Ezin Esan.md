---
id: tx-1802
izenburua: Ezin Esan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cfJDtNRG_Fc
---

Ezin esan maite,
ezin esan eder,
ezin esan elkar,
ez zuhaitz, ez lur, ez bihotz.
Ezin esan.

Kolpe bat sentitu dut bihotzean
tiranoak giltzapetu nahi digu hitza.
Maite zaitut diostazun bakoitzean,
baina...

Ezin esan libre
ezin esan berdin,
ezin esan elkar,
ez zuhaitz, ez lur, ez bihotz.
Ezin esan.

Kolpe bat sentitu dut bihotzean
tiranoak giltzapetu nahi digu hitza.
Maite zaitut diostazun bakoitzean,
baina...

Ezin esan maite,
ezin esan eder,
ezin esan elkar,
ezin libre,
ezin esan anai,
ezin maite.