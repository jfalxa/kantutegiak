---
id: tx-1774
izenburua: Itsasontzi Baten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xVt4T2VvH_g
---

Itsasontzi baten 
Euskal Herritik kanpora
naramate eta ez dakit nora. (bis)
Agur nere ama
laztan goxoari
agur nere maite politari
ez egin negar
etorriko naiz
egunen baten
pozez kantari

Itsasontzi baten...

Agur senideak
agur lagunari
agur Euskal Herri osoari.
Ez egin negar
etorriko naiz
egunen baten
pozez kantari

Gora Euskal Herri
Gora Euskal Herri
Gora Euskal Herriari.