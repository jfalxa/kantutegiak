---
id: tx-300
izenburua: Lege Eta Debeku Berriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XOr0q-w18I0
---

2021ko urrian Akelarre Kultur Elkartean grabatutako bideoklipa Ekaitz Iriarteren eskutik.
Abestia 2021ko azaroan Sound Of Sirens estudioetan grabatua izan da Julen Urzaizen eskutik.


LEGE ETA DEBEKU BERRIAK

Zaila da ongi azaltzea
baina kaleak ez dira berdinak
Tentsioa nabaria da
haurrek jolasten zuten parkean
Begirada arrotzak
auzokideen artean
eta inoiz baino estuago
lepo inguruan soka

Muga pauso bakoitzean aurkitu eta
impotentziari aurre egin
ta botereak gure ahoa estali
Gurea zen askatasun puska eraman
kalean baina barroteen artean
Nortasuna urratuta

BIHAR BIZI NAHI GENUENA
ASMO ETA GOGO GUZTIAK
BAHITU EGIN DITUZTE
HUTSUNEZ BETE DUTE GURE BIZITZA
ETA NORA JOAN DIRA AMETSAK
HAIZEAK EZ DITU ERAMAN
LAPURTU DITUZTE
TA IZKUTATU DEBEKUAREN AZPIAN

Askatasun eguarria
indartsu nabari da inguruan
Oroitzapenetaz bizi
baina ilusioa galduta
Gehiegikerien aroan
duintasuna lapurtua
Lege eta debeku berriak
eguneroko ogia

Muga pauso bakoitzean aurkitu eta
impotentziari aurre egin
ta botereak gure ahoa estali
Hanka eta eskuetatik loturik
astinduz kateak baina apurtu ezinik
guztiz indar gabeturik