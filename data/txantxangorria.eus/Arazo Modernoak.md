---
id: tx-2577
izenburua: Arazo Modernoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Az6wLaSXyss
---

bibalataldea@gmail.com


HITZAK:

Interneteko birusak eta
txertorik gabeko haurrak
adar jartzeen hipotekak ta
hipotekaren adarrak
faborito edo retweet
Cola Caoa edo Nesquik
Zarako praka apurtuekin
aitaren atorra zaharrak.

Arazo modernoak,
entzefalograma planoak,
bizitza garantian ote dudan
begiratzera noa.

Hauek ez dira nik egunero
jaten ditudan kiwiak
Lisabö entzunez azken zerbeza
artesanala edan diat
ta ez duk geratzen besterik
joder hireak egin dik!
Bonoa edo egun sueltoa?
Ze klabe dauka wifiak?

Arazo modernoak,
entzefalograma planoak,
bizitza garantian ote dudan
begiratzera noa.