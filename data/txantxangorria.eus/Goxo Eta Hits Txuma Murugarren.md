---
id: tx-3079
izenburua: Goxo Eta Hits Txuma Murugarren
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zQj7oycbGnc
---

Kantua:"Gozo eta hits" Txuma Murugarren,B aldeko kantuak El Monstruito-ren produkzioa Isidro Elgezabal-ekin Zuzendaria eta edizioa:Mentxu Sesar
Zuzendari laguntzailea:Asier Abio
Kamara:Iñaki Lauzirika.
Argazki finkoa:Lorea Argarate

Jantziak:Marta Muñoz Isabel Sesar Leire Goienetxea

Makillajea:Izaskun Makua Iratxe Zapiain
Produkzioa:Janire Cabrera Miri Petralanda Leire Onzain

BIDEOKLIP HAU ONDARROAN ETA MUNGIAN GRABATU ZEN 2011.eko IRAILEAN. Informazio gehiago:elmonstruito@hotmail.es --