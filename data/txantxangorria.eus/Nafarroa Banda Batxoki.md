---
id: tx-3096
izenburua: Nafarroa Banda Batxoki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F5ExU6lWINA
---

Orain dela bost mende
herri libre izan ginen
Nafarroako erresuma
bere izena zen
Eta gure arbasoek
ha/ien o/do/la e/man zu/ten
independentziaren alde
borroka egin zuen.
Eta gaurko gudariek
bide bera darraite
Nafarroako erresuma
bere izena zen!
Bere izena zen!
bere izena!
Orain dela bost mende
herri libre izan ginen
Nafarroako erresuma
bere izena zen
Eta gure arbasoek
haien odola eman zuten
independentziaren alde
borroka egin zuten.
Eta gaurko gudariek
bide bera darraite
Nafarroako erresuma
bere izena zen!
Bere izena zen!
bere izena!

Gaur da eguna
guzti hau kontu hau
ez dela ametsa
arranoaren hegaldia
HASI DA!!
Araba, Bizkaia,
Gipuzkoa, Nafarroa
Lapurdi, Zuberoa
beste aldean.
Guztiok anai-arrebak
guztiok nafarrak!

Guztiok anai-arrebak
guztiok nafarrak!
ak! ak! aak!!