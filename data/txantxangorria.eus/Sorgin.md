---
id: tx-634
izenburua: Sorgin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NWHuN5oaOPw
---

Sorgin, sorgin
Tximeletak joan egin dire gure ondotik.
Eeh sorgin, sorgin
Hasi baño lehen dana amaitu da.
Begiratzen zaitut
Ez dot ikusten,
Zuri beltzien baño ez zaitut sentitzen.
Gorantza egin barik
Berantz ein dogu,
Bat batien negue heldu jaku.
Buelteko dire berriz
Enarak hegaz egiten,
Ta maiatzak hartuko gaitu
Beste leku batien.
Beste beso batzuetan
Egongo garen arren,
Berriz pasatuko dire
Tximeleta bi aidien,
Aidien...
Sorgin, sorgin
Arezko irudi bi gara
Olatuaren zai.
A ze pena, pena
Tripako piercing hori
Berriz ez hainkatzea.
Mile momentu gordeko dodaz,
Eta beste mile ahaztu.
Denboraren labana
Arima zulatzen,
Hosto lehorrak
Haizetan dantzatzen.
Buelteko dire berriz
Enarak hegaz egiten,
Ta maiatzak hartuko gaitu
Beste leku batien.
Beste beso batzuetan
Egongo garen arren,
Berriz pasatuko dire
Tximeleta bi aidien,
Aidien...