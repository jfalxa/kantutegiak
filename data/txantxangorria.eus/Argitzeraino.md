---
id: tx-963
izenburua: Argitzeraino
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7Ndjf-Zqj6w
---

[EUS] Azaroaren 29an kaleratuko dugun 'Ni' diskoaren aurrerapen kanta

[CAS] Single del disco 'Ni' que presentaremos el 29 de noviembre


HITZAK / Letra

Harriak zulatzen ditut nik
kate beltzak apurtu nahirik
korapiloak hausteko
bizitzaren zama arintzeko
zerbeza batzuk hartzera
zatoz nirekin etxera
korapiloak haustera
zure ondora
nirekin gora

Oh! oi...
prest nago gaueko argiak pizteko
oh! oi...
prest zurekin irrifar egiteko
oh! oi...
dantza dezagun, dantzan egin gaua...
oh! oi...
argitzeraino!

Alferrik denbora galtzea...
alferrik dela sufritzea
ikasi genuen txikitan
ta erraz ahaztu dugu haunditan
gaur joan gaitezke festara
zu ere etortzen bazara
korapiloak haustera
nire ondora
zurekin gora

Oh! oi...
prest nago gaueko argiak pizteko
oh! oi...
prest zurekin irrifar egiteko
oh! oi...
dantza dezagun, dantzan egin gaua...
oh! oi...
argitzeraino!

Pa fuera
las penas
pa fuera
condenas.
Yo quiero bailar,
romper las cadenas.
Tocar las estrellas.
Gritar
revolución.
Seremos la llama
que prenda la noche.
Yo quiero bailar
que todos los muros
caeran uno a uno
bailando
hasta el amanecer.
Y cuando sale el sol
ojos brillantes que
rebosan ilusión.
Perdemos el control
se sienten
instantes de revolución.