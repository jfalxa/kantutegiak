---
id: tx-245
izenburua: Galdutako Zubixak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UZX1n1l4zMg
---

GALDUTAKO ZUBIXAK

        Kemen Lertxundi , 2008
 
Galdutako hondartzak

galdutako zure enbatak

hamen daz

galdutako zubixak

irabazitxako eurixak

hamen daz

hamen daz, zure amets

ixan ahalko zin guztixan

kalatxorixan heganaldi bat

Pragako hiritxik etorritxako

notixixak

 

Mileurtekun azkana

ixan leike zure esana

baine ezta

tortura guztin atzin

gorde dizen arrazoikin

ixe ezta

ixe ezta nitzako

ixan bizin guzurrakin

jaxo barrixan negar malko bat

irrati batek anuntxixako dau

zure ixena

 

Inun eztala ixe ez zala

beixak itxitxakun entzungo dana

kanta ixil bat nahi eta ezin bat

lehenengo argiuni nire bixitzan

zeure herixotzan

 

Eskina ilunetan

paradisu urdinetan

entzun zan

sasi guztin gainetik

ixildu zin ahuetatik

entzun zan

Bixarko egunez

argitxuko daben gaba

sinistutako ezkon barri bat

nire etsai ixan nahiko leuken guztin

esperantzak

 

Inun eztala ixe ezala

beixak itxitxakun entzungo dana

kanta ixil bat nahi eta ezin bat

lehenengo argiuni nire bixitzan

zeure herixotzan

 

© Kemen Lertxundi    

anonimoak

 

mendeak
15
16
17
18
19
20
21
 

poemen
aurkibide
alfabetikoa