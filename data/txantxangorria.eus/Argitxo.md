---
id: tx-2865
izenburua: Argitxo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jXr7T3i2EEg
---

Argitxoren kanta

Neri deitzen didate
Argitxo iratxoa
eskuan kriseilua,
buruan txanoa.
Txiki-txikia naiz ni,
mendiko magoa,
leizezuloa utzi
(e)ta hirira noa.

Harri murri zap(at)ero
bero, bero, bero
haurrak egin euskaraz
orain eta gero!

Ni ezkutatzen naiz
edozein txokotan:
motxilan, poltsikoan
edota karnetan.
Egunez ikastolan
nabil gehienetan
(e)ta gauetan sartzen naiz
haurren ametsetan.

Harri murri...

Ezin nauzu ikusi
ez eta ikutu
baina Argitxok beti
zu ikusten zaitu.
Eta ni nahi banauzu
gertutik sentitu
faborez eta masedez
euskaraz mintzatu.

Harri murri...

Gauza harrigarriak
egiten badakit
magia ikasi nuen
txiki-txikitatik
opari asko dauzkat
zuentzat gorderik,
jolasorduak ere
luza ditzaket nik.

Harri murri...