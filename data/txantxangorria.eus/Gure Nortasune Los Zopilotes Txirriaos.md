---
id: tx-3085
izenburua: Gure Nortasune Los Zopilotes Txirriaos
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KYRMe3LpQgI
---

Hau betekaie
Hemen gaude ostatuen,
Ollasko eta ardoz.
Hori bai, fandangue.

Iritxi dek ordue,
Patxaran hizoztuez,
Pa/rri/ek e/ta kan/tu/ek,
Ei/te/ko gu/re ar/ti/en.

Gu/re pa/rran/dan!
To/pa ei/ten!
Gu/re bi/ho/tzak,
guz/tiz bi/zi/ki al/ga/raz..

I/ra/ul/tza o/do/le/an,
a/ge/ri za/gu zai/ne/tan.
Gu/re nor/ta/su/na,
zu/tik e/ta tin/ko, hor nun na/hi.

Ein de/za/gun!
A/bes/tu a/lai!
Ba/rre/nak guz/tiz,
e/txe/ko pa/txa/ra/naz blai.

I/ra/ul/tza o/do/le/an,
a/ge/ri za/gu zai/ne/tan.
Gu/re nor/ta/su/na,
zu/tik e/ta tin/ko, hor nun na/hi.



U/tzi gin/tuz/ten,
zau/ri gor/di/nek,
Sen/da dai/te/tzen
al/den az/ka/rren guz/ti/ek.

I/ra/ul/tza o/do/le/an,
a/ge/ri za/gu zai/ne/tan.
Gu/re nor/ta/su/na,
zu/tik e/ta tin/ko,
hor nun na/hi.

Mun/du/ek ein de/za/la
dan/tza gu/re kon/tu/re.
Zo/ri/txa/rrak lur/pe/ra/tu/te,

As/tin/du de/za/ten
gu/re bi/ho/tzi/ek,
A/tzo/ko ne/gar e/ta mal/ku/ek,


Gu/re pa/rran/dan!
To/pa ei/ten!
Gu/re bi/ho/tzak,
guz/tiz bi/zi/ki al/ga/raz..

I/ra/ul/tza o/do/le/an,
a/ge/ri za/gu zai/ne/tan.
Gu/re nor/ta/su/na,
zu/tik e/ta tin/ko, hor nun na/hi.




Ein de/za/gun.
A/bes/tu a/lai.
Ba/rre/nak guz/tiz,
e/txe/ko pa/txa/ra/naz blai.

I/ra/ul/tza o/do/le/an,
a/ge/ri za/gu zai/ne/tan.
Gu/re nor/ta/su/na,
zu/tik e/ta tin/ko,
hor nun na/hi.