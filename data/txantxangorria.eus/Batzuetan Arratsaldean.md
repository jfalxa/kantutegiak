---
id: tx-2805
izenburua: Batzuetan Arratsaldean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W-ox3iEdn40
---

Batzuetan arratsaldean
banku batean eseritzen naiz
etxeko atarian
baso bat ardo eskuan
denbora nola doan begiratuz.

Nere pentsamenduarekin
pasatu zait eguna eta
ezin naizela kexa
pentsatzen dut azkenean
orduan ardo pixka bat edaten dut.

Halako batean txakurrak
belarriak altxatzen ditu
honela adieraziz
gure laguna heldu dela
eta korrikahara doa bere bila.

Lasaitasunak inguratzen nau
eta egunari amaiera
ematen hasten gara.