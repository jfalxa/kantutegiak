---
id: tx-1292
izenburua: Sexuarekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aaqJvWnq5SI
---

En Tol Sarmiento (ETS) taldearen "Sexuarekin" kantua zuzenean, Pello Reparaz (Vendetta) abeslariaren partaidetzarekin. Bideoa "Ametsetan" disko + DVD-aren parte da (Baga-Biga, 2017). Bideokliparen egileak: Artifikzio.

El grupo En Tol Sarmiento (ETS) toca en directo "Sexuarekin", con la participación de Pello Reparaz (Vendetta). El vídeo está incluido en el CD+DVD "Ametsetan" (Baga-Biga, 2017). Realizadores del videoclip: Artifikzio.

Jada salgai/ Ya a la venta/ Out now:

Jarraitu gaitzazu! /¡Síguenos!/Follow us!


Sexuarekin bat izango gara
zu ta nik gozatzen dugun bakoitzean
ta geure burutik joan daitezela


gizarteko aurreiritzi guztiak.

Ezkutatu zuten itzalpean
erakutsi ziguten beldurra
baina gaur egun esan behar
askatzeko garaia da.

Biluztuta besarkatu
geure gorputzez gozatu
lotsik gabe ezagutu.

Sexuarekin bat izango gara
zu ta nik gozatzen dugun bakoitzean
ta geure burutik joan daitezela
gizarteko aurreiritzi guztiak.

Mendi magalean, tabernetan,
izpiluen aurrean etzanda,
banaka, binaka edo taldeka
plazera da helburua.

Biluztuta besarkatu
geure gorputzez gozatu
lotsik gabe ezagutu.

Sexuarekin bat izango gara
zu ta nik gozatzen dugun bakoitzean
ta geure burutik joan daitezela
gizarteko aurreiritzi guztiak.

Biluztuta besarkatu
geure gorputzez gozatu
lotsik gabe ezagutu.

Sexuarekin, sexuarekin, sexuarekin, sexuarekin
Sexuarekin, sexuarekin, sexuarekin, sexuarekin.

Sexuarekin bat izango gara
zu ta nik gozatzen dugun bakoitzean
ta geure burutik joan daitezela
gizarteko aurreiritzi guztiak.

Sexuarekin bat izango gara
zu ta nik gozatzen dugun bakoitzean
ta geure burutik joan daitezela
gizarteko aurreiritzi guztiak.