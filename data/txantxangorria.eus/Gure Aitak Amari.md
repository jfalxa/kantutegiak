---
id: tx-3196
izenburua: Gure Aitak Amari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UvVTiqhuI4E
---

Gure aitak amari
gona gorria ekarri;
berriz ere maiteko dio
gure amak aitari.
Gure aitak amari
gona gorria ekarri.

Gona gorri gorria
zazpi jostunek josia;
berriz ere maiteko dio
gure amak aitari.
Gure aitak amari
gona gorria ekarri.

Gure amak aitari
fraka berriak ekarri;
berriz ere maiteko dio
gure aitak amari.
Gure amak aitari
fraka berriak ekarri.

Fraka berri berriak
zazpi jostunek josiak;
berriz ere maiteko dio
gure aitak amari.
Gure amak aitari
fraka berriak ekarri.