---
id: tx-3363
izenburua: Ideien Guda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9EjC-tESWTM
---

...esklabutza, zapalkuntza, 
gerra, gose eta heriotza, 
langabezia, pobrezia... 
eguneroko ogia!
... aniztasuna, berdintasuna,
Kontzientzia, justizia, 
elkartasuna, armonía... 
egutegi gorria!
ATZO, GAUR ETA BETI 
GORRIAK!
GAUR ETA BETI 
IDEIEN GUDAN!
...hipokresia, miseria, berekolkeria,
Ignorantzia, konpetentzia... 
eguneroko ogia!
...bakea, lana, askatasuna,
Umiltasuna, maitasuna
Irria eta errebeldia
Ilusio gorria!

ATZO, GAUR ETA BETI 
GORRIAK!
GAUR ETA BETI 
IDEIEN GUDAN!

Partehartzea ala boterea? 
Determinisme ala boterea?
Klase borroka ala sumisioa?
Ganancias per a la burgesia 
els beneficis col lectius?
Sinismena ala pentsamendua?
Socialisme o mort?
Victoriosos derrotados...
Garaipenararte!

ATZO, GAUR ETA BETI 
GORRIAK!
GAUR ETA BETI 
IDEIEN GUDAN!