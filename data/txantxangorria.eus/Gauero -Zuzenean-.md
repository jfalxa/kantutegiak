---
id: tx-3333
izenburua: Gauero -Zuzenean-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mSJl6WOvgr8
---

Itzalpean tristerik
eguna igaro
nahiz ta etsaiek
hori luketen nahiago
bihotza daukat
inoiz baino askeago
sentimenduak
preso hartzerik ez dago

Gauero sartu ohi naiz
ziegara joatean
sarri hotzegi
diren izaren artean
nire ezpainak
zure ezpainean eskean
gertu sentitzen zaitut
segundu batean!

Gauero lotan
jarri eta gero
zurekin oroitzen naiz
amesten dut

Gauero lotan
jarri eta gero
zurekin oroitzen naiz
amesten dut

Askatasuna amets
helburu desira,
barroterikan gabe
elkarri begira
joan gaitezen biok
aske urrutira
ametsak egi
bihurtu ohi diren tokira!

Gauero lotan
jarri eta gero
zurekin oroitzen naiz
amesten dut

Gauero lotan
jarri eta gero
zurekin oroitzen naiz
amesten dut

Itzalpean tristerik
eguna igaro
nahiz ta etsaiek
hori luketen nahiago
bihotza daukat
inoiz baino askeago
sentimenduak
preso hartzerik ez dago