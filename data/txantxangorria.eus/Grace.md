---
id: tx-156
izenburua: Grace
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Nj0WR4Po3mY
---

zer gertatu da?
Atzo arte dena zen eder 
biok noraezean gaur 

Giza harremanak 
orekari baten gisa
bi aldeko dira 
eta ezinegona                               
eranzteko adore apur bat                    
lortu nahian ausardiaz
lehen aldiz
lehenetsi naiz 


Askok tira egin arren 
oreka mantentzea da hemen      
Maitasunean gako
baldintzarik gabe eman
jaso nahi genukenaren eredui zan
Edozer datorrela 

Saiatzen nabil
Eskaintzen naizen hori
Kon di zio barik
Nor zaren jakin
Bi tar te an eraikiz
Une bat elkarrekin

Zaila da oso
 Beldurrez gaude ta denok

Irribarre hau ez dago zai
Bizi nahi du hemen ta orain

Ez banaiz nire
Ez naiz, ez naiz 


Askok tira egin arren 
oreka mantentzea da hemen 
Maitasunean gako
baldintzarik gabe eman 
Beste motxilekin ulerkorra izan 
Zainduz norberarena

Askok tira egin arren 
oreka mantentzea da hemen 
Maitasunean gako
baldintzarik gabe eman 
jaso nahi genukenaren eredu izan
Edozer datorrela