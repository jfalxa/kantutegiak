---
id: tx-3408
izenburua: Askatasunera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M4xPZuVlNek
---

Quale si ? Eskualduna naiz
D'induve si ? Mendiko gizon naiz
U to paese, cumu si chjama ?
Euskadi ene herria da

Nor zara ? Eiu so corsu
Nungoa zara ? Vengu d'un paisolu
Zein da zure / Herriaten izena ?
U mo paese / Ghje CORSICA

Ripigliu : SIMU FRATELLI
ANAIAK GARA
CIRCHEMU A STRADA
ASKATASUNERA

U populu corsu / Campa di stonde amare
D'ii nostr'antenati / A lingua si more
L'usu nustrale / Di campà
Senza rivolta / Smariscerà

Baita / Nere herrian
Bainan euskara / Beti bizi da
Eskualdunak izan / Nahi dugu
Presoak etxean / Nahi ditugu

Ripigliu : SIMU FRATELLI
ANAIAK GARA
CIRCHEMU A STRADA
ASKATASUNERA

Ghje ora ava / Tutt'inseme
Per noi / Di custrui l'avvene
E di lottà / Senza piantà
Sin'à l'alba / Di a LIBERTA

Jeiki jeiki / Denak extean
Orain ibili / Gara bidean
Haurrek gurekin / Herriarentzat
Eskuz esku / Askatasunera