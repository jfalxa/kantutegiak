---
id: tx-3189
izenburua: Bedatsian -Kike Ugarte-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fCQP-TXX0qo
---

Ez gal gure odol beroa 
Bizi bedi gure Xiberoa.

Bedatsian dirade zühainak loratzen 
Udan ekhiarekin bazterrak berotzen 
Larrazkenian aldiz egünak llaburtzen 
Negian haize hotza hezürretan sartzen.

Ez gal gure ...

Aspaldian bizi da gure popülia 
Zazpi probientzietan oso berexia 
Kantore zahar eta jauzi emailia 
Xoriñua bezala txülüla joilia.

Ez gal gure ...

Jendik erraiten die mündian gisala 
Hau bezalako lekü ejerrik ez dela 
Bena gure xokua hüsten ari dela 
Eta segür hiltzeko pündian girela.

Ez gal gure ...

Tristeziaz besterik behar dügü heben 
bizitzeko lotsarik ez dezagün ükhen 
Oi Xibero maitia ez zira galdüren 
zure semek zütie bihar salbatüren.

Ez gal gure ...