---
id: tx-988
izenburua: Heldu Korrika 15
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r0gfE_gwQ20
---

Oilarrak kukurukuz
Ozenki hor küküruküz
Herri hontan zer ote da?

Heldu heldu Korrikari
Heldu heldu Euskarari
Ibiliz goitik behera
Karrantzatik Iruñera!

Mendez mende euskalduna
Izan da Euskara-duna
Aitzina egin beza!

Herria da gorputza
Hizkuntza aldiz bihotza
Xalbadorren mezua
Herri honen oihua!

Hitza pitz ahoz-aho
Erabiliz egunero
Mendi puntatik ibaira
Nortasunaren mintzaira…

Heldu heldu Korrikari
Heldu heldu Euskarari

Erabiliz barra-barra
Karrantzatik Iruñera!!