---
id: tx-2603
izenburua: Gaur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SMvWHMgGAlc
---

Euskadiko Orkestra Sinfonikoa
Zea mays
Fernando Velazquez.
Euskadiko Orkestra Sinfonikoaren egoitzan, Miramonen, grabatuta

Berriz ere erratuko naiz, gaur, gaur, gaur
Urteak 10 dituenean
11 aldiz erori egin naiz

300ekin hezur guztiak hautsita
Suposatzen da ikasiaz
Berriro jausten ez zarela
baina niretzat teoria
Faltsua da

Denbora galtzen trebea naiz
Zerrenda luzeen erregina
Idatzitakoa galtzeko ere aparta
baina egia esan
maite ditut nire akatsak, nire huts guztiak
ta ez naiz ezkutatuko besteen aurrean
suposatzen da ikasiaz berriro jausten ez zarela
baina beste behin egin dut gaur, gaur, gaur, gaur
ta hurrengoan altxatuko naiz
ta berdin zait dena!!!
Berriz ere erratuko naiz, gaur, gaur, gaur
Bikaina naiz arte horretan!
Berriz ere erratuko naiz, gaur, gaur, gaur