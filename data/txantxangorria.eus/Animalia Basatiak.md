---
id: tx-2494
izenburua: Animalia Basatiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/so0Ke_FlFU4
---

Bideo ekoizpena/ Producción del vídeo: Recrum Media & Dando al Rec

 ** Hitzak | Letra: 
Gorbata, jantzi, 
lanpostu bikainak jaso genuen 
heziketa onena gizarte aurreratuan… 
Hori guztia alfer-alferrik da 
barnean doan bidaiari 
iluna askatu egiten bada… 
Azkar! doaz taupadak 
Azkar! esnatu egin da 
Azkar! geldiezina da 
Azkar! amorru bizia 
Itxurak gorde zentzuduna 
izan printzipio batzuk 
jarraitu behar dira 
gizarte aurreratuan…
 Baina tamalez balio guztiak baztertzen dira, 
pikutara doaz piztia pizten denean 
Azkar! doaz taupadak 
Azkar! esnatu egin da 
Azkar! geldiezina da 
Azkar! amorru bizia 
Ez da! kontrolatzerik 
Ez da! gizatasunik 
Ez dago su-etenik 
Animalia basatienak bagina bezala 
Taupadak gora doazela izerdi hotza eskuetan (x2) 
Azkar! doaz taupadak 
Azkar! esnatu egin da 
Azkar! geldiezina da 
Azkar! amorru bizi… 
Animalia basatiak barnean daukagun sena 
Animali basatiak barnean daramagun sugarra