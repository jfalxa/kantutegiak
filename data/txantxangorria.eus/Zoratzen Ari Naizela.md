---
id: tx-1341
izenburua: Zoratzen Ari Naizela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tEoLG5DVaLs
---

"Zoratzen ari naizela, hasi naizela
Dirauts psikoanalistak
zuganako amodioaren berri ematerakoan

zure begi okre horien lama geldoa
ene bihotza kiskaltzen ari delako
zoratzen ari omen naiz
sinestari nahi didate.

Krabelinen eta larrosa gorrien petaloen ordez
psikoanalistaren batoi zuriaren
zurea ez den batoi zuriaren
fragantziaz ase nahi dudalakotz
zoratzen ari nahizela
hala dirauts psikoanalista
zubipeko ur kantarien
ahots zoilaren pareko zitzaidan
zure mintzoa
hiru zurito eta lau txupito edanez
gure ezpainak zubi ondoan
eletu ziren
maite zaitut, maite zaitut
lau txupito eta zure ezpain eleak
zoratzen ari omen naiz
hala dirauts psikoanalistak."