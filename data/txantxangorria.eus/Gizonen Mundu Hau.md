---
id: tx-3049
izenburua: Gizonen Mundu Hau
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N7ECRAfDS-M
---

SDB2013 - Sortuko dira besteak 
Kursaal Auditorium - Donostia Sn.Sn.
2013.XII.22

"Gizonen mundu hau" 
It´s a man´s man´s man´s world (James Brown) 
Euskarazko adapatazioa: Harkaitz Cano

Ahotsa: Estitxu Pinatxo

Musikariak: Xanet Arozena, Txema Garcés, Oriol Flores, Luis Fernandez, Miren Zeberio, Xabier Zeberio.
Zuzendaritza artistikoa: Joxan Goikoetxea

Audio Teknikaria: Josean Ezeiza
Audio nahasketak: Jose Mari San Sebastian - ETB
Argiak, bideo proiekzioa eta dekoratuak: Sendoa Lusarreta

Bideo grabaketa eta errealizazioa: Dani Blanco

Sortuko Dira Besteak’ 2012an sortutako ikuskizun kolektiboa da, hasieran nazioarteko kantautoreen klasikoak egokitzea helburu zuena. Aurreko urtean, Eñaut Elorrieta, Alex Sardui, Mikel Urdangarin, Petti, Arantxa Irazusta eta Pier Paul Berzaitz, 2010ean zendutako Xabier Lete poeta eta kantariaren omenezko kontzertu-sorta baterako elkartu ziren. Sentiberatasun eta musika-estilo desberdinen arteko lehen topaketa hartatik, kontzertu-sorta berri bat antolatzeko asmoa sortu zen, Bernardo Atxaga eta Harkaitz Cano idazleak eta Amets Arzallus bertsolaria tarteko zirela.

Leteren ondarea eta lorratza ondo errotuta geratu dira kantari eta musikari horien sentimenduetan, eta horrexek bultzatu ditu ikuskizunari "Sortuko dira besteak” izena jartzera. "Izarren hautsa" poema ezagunetik ateratako izenburu horrek ezin hobeto islatzen du poeta oiartzuarrak belaunaldi gazteei egindako gonbita, aurreko musikari eta sortzaileen lanari jarraipena eman diezaioten.


Gizonen mundu hau...
gizonen mundu hau...
alfer-alferrik; erabat alferrik, emakume barik.

Hara… gizonen autoek garamatzate,
garraio-trenak sortu dituzte,
argindarraz argitu iluna,
barkuz zeharkatu ura: (hor da,) Noe laguna.

Gizonen mundu hau, gizonen, gizonen mundu…
Alfer-alferrik; erabat alferrik, emakume barik...

Gizonak pozik dauzka alabak / semeak ere bai
hark egindako jostailuekin alai...
Bere onenak emanda, gizonak baina,
sosa dakar beste gizonentzat a(d)ina.

Gizonen mundu hau...
Alfer-alferrik; erabat alferrik, emakume barik...

Galduta/ oihanean…
Galduta/ saminean…
Beti galduta…
Beti saminduta…