---
id: tx-2809
izenburua: Euskal Hitza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n2mtOkP8A-k
---

Han edo hemen berdin da
denok hizketan gabiltza
komunikatzeko giltza
hori da gure hitza

Asko erderaz dabiltza
Eginez euren bizitza
Ez dira konturatuko
Gaztelera da baldintza

Goazen euskeraz kanta zazu ta ozen
goazen erdera laga zazu alde baten

Bizi naiz Euskal Herrian
euskararen aberrian
hizkuntza galtzen omen da
eiten ez den tokian

Zein polita den euskera
hainbat euskalki batera
han da hemen argi degu
gu euskaldunak gera

Goazen euskeraz kanta zazu ta ozen
goazen frantzesa laga zazu alde baten

Ni euskalduna naiz eta
erderaz itet hizketa
euskal sena non duzu ba?
ezkutuan gordeta

Bakarra gaurko galdera
Ba al dakizu euskera?
Gu horretan ari gera
Hau da zure aukera

Goazen euskeraz kanta zazu ta ozen
goazen erdera laga zazu alde baten