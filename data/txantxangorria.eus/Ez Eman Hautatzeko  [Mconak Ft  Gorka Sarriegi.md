---
id: tx-494
izenburua: Ez Eman Hautatzeko  [Mconak Ft  Gorka Sarriegi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RRdsq97hhJE
---

McOnak Taldeak bere hamargarren urteurrena ospatzeko kaleratuko duen diskoaren bigarren aurrerapen abestia. 2020-ko Abenduan, Usurbilen Haritz Harreguy Studio-an grabatua. Abesti honetan Gorka Sarriegik (Sorotan Bele) eta Arkaitz Minerrek (Mandolina) parte hartu dute. Abestiaren letra Kirmen Uriberen "Ez eman hautatzeko" olerkian oinarrituta dago.

Bideoa: Deslaian ekoizpenak 
Grabaketa eta nahasketak: Haritz Harreguy 
Mastering: Victor Garcia (Ultramarinos studio)



LETRA: EZ EMAN HAUTATZEKO (EUS)

Ez eman hautatzeko itsasoa ta lehorraren artian.
Ez eman hautatzeko kresal usain edo zure grazia.
Gustora bizi naiz lerro fin ta gazi gozo honetan.
Gustora bizi naiz ni itsas labarrean.

Ez eman hautatzeko hego haize zoro edo mendebala.
Ez eman hautatzeko olatu berde edo mendi urdina.
Gustora bizi naiz harri gorrien itsasadarrean.
Badakit ari fin bat dela nire bizilekua.

MARRAZTURIKO MAPETATIK IHESI,
ITSASOAN GALTZEN NAIZ SARRI.
BESTEETAN BELDURRAK JOTA ,
AINGURA BEHAR DUT BOTA,
TA SENTITU ZURE BABESA.

ITSASOAREKIN BAKARRIK GALDUKO
NINTZATEKE TA LEHORRAREKIN ITO


Ez eman hautatzeko itsasoa ta lehorraren artian.
Ez eman hautazeko laster bueltatuko naiz.
Gustora bizi naiz ni azken piraten gotorlekuan.
Itsasargia dakusat bagatoz barruko bandan.

MARRAZTURIKO MAPETATIK IHESI,
ITSASOAN GALTZEN NAIZ SARRI.
BESTEETAN BELDURRAK JOTA ,
AINGURA BEHAR DUT BOTA,
TA SENTITU ZURE BABESA.

ITSASOAREKIN BAKARRIK GALDUKO
NINTZATEKE TA LEHORRAREKIN ITO.