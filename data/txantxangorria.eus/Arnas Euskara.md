---
id: tx-2655
izenburua: Arnas Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6PP7OGuhZSc
---

2017ko maiatzaren 27an Tolosaldeko euskalgintzak egin duen Lip Duba.

Hitzak: Oihana Iguaran
Musika: Zigor Sagarna 
Moldaketa: Laket taldea

ARNASTU EUSKARA

Korronte txiki baten 
eraz 
hasiak fin
zurrunbiloan goaz
hegaz
elkarrekin
haize-errota berriz
xarmaz
bete dadin
euskaraz har ezazu
arnas
ta putz egin

Aharrausian hasia
hauspoz
ta kalera
arnasestuan bide
askoz
ta aurrera
hasperen eta zotin
saltoz
biharrera
gaur irrintzi bat da ta
zatoz
ospatzera

Lau haizetara
zabal dezagun arnas gunea
gu haize gara
hizkuntza baten hauspo xumea 
indar ufada
hitzarekiko begirunea
ta euskara da
gure tokia ta unea

Hots hutsa ez da hitza
hitza ez da hots hutsa
hitzetatik hizkuntza
hizkuntzatik ahotsa 
ahotsez eman hatsa
hatsa hauspotuz bultza
herria zen gorputza
hizkuntza da bihotza