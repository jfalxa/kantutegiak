---
id: tx-2721
izenburua: Martin Larralde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/alS7sPHYPEQ
---

Larre berdeak, etxe zuri teila gorriak,
jendarme auto bat
bidean bildots artean pasatzen.
Etxe elizetan otoitzak,
betiko otoitzak neguko keearen antzera
lehun igotzen.
Martin Larralde ez da sekula itzuli,
baina itzuli balitz
(arantzak barrurantza itzuliak lituzkeen sagarroia bezala)...
Itzuli izan balitz, zer? Gaur igandea da,
larre berdeak,
etxe zuri teila gorriak, jendarme autoa bidan
Bayonne 17 panopetik.
Jendea familia erretratuetan bezala
larritasuneraino
perfumaturiko arima erakutsiz doa mezara.
Inork ere ez du
(dena ohitura, dena errua, dena barkamena)
oroimenaren
korapiloa deslotzen. Inork ere ez du
koblakaririk behar...

Martin Larralde honelako egun batez hil zen
galeretan,
begiak zabalik etzanda, agian, zerua,
itsaso ziki bat dela esanez.