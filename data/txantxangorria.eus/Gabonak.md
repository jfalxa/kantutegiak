---
id: tx-2860
izenburua: Gabonak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Fj_fPc_rqzg
---

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
gabonak etorriko baitira

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
gabonak etorriko baitira

Laster izango dira gabonak
denok alaituko gara
Papa Noel, Olentzero eta
erregeen opariak
Onak jasoko du opari bat
gaiztoak berriz ikatza
Ohiturak halaxe dio behintzat
nahiz ta ez bete askotan

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
gabonak etorriko baitira

Gudazale amorratuari
arma zeharo berriak
Kalean lo egiten duenari
lo egiteko konpainia
Atxiloturik daudenei
kate eta eskuburdinak
Nagusikeriaz blai denari
fondo erreserbatuak

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
gabonak etorriko baitira

Arrantzale ero batzuei
sare pelagikoak
Gosez hiltzen ari direnei
arrain arras ustelak
Paradisuaren zai(n) gaudenei
hautsez e(g)indako ametsak
Itsaso, zeru eta ibaiei
kolore eta usai(n) berriak

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
oraindik gabonak ez baitira

Gabonak, gabonak etorriko dira
ta haiekin urte berria
Denok gabiltza alai kantari
oraindik gabonak ez baitira

Oraindik gabonak ez baitira
Oraindik gabonak ez baitira