---
id: tx-2198
izenburua: Musturrek Sartunde
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9bNwBOWKUvk
---

Gatibu talderen abestia.2002an Zoramena diskakoa

Erdi lo hartu nindun zure telefono deiak ez dau inor etxien eta, pasatu zaitez.
Geldi egon ezine sartu eustan gorputzien eta nerbioso pareu segundo bakarrien.

Eta prakak ezin sartu helduko nazela berandu ezin lotu arkondarie eskilarak beherantz banoie.

Eskutik heldu ginen eskuak sutan ziren di-da gure artien sortu zan rollotxue portaleko atien lehenengo kalentoie igogailu barruen fuera lotsa guztiak.

Eta prakak ezin kendu ezin izango dot inoiz ahaztu ezin kendu arkondarea sofaren ganien etzunde.

Beldur barik irrintzika erropak etxien ziher botata eta zu biluzik.

Zoro moduen maitatu ginen eta probatu gendun eztian musturrek sartunde.

Zoro moduen jarri ginen eta gozatzen heldu zan goiza.

Erdi lo hartu nindun zure mutilaren deiak: kotxie aparkatu dot laster gorantza noie.

Eta prakak ezin sartu ezin izango dot inoz ahaztu ezin lotu arkondarea.

Eskilarak berantz banoie.

Agur esan barik banoie, banoie.

Deitu nahi dozunien.

Eta zoro moduen jarriko gara eta probatu gendun eztian musturrek sartunde.

Zoro moduen jarriko gara eta gozatzen goiza helduko da alkarreri lotute.