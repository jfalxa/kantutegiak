---
id: tx-3313
izenburua: Elkartuko Gara -Urtz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CgwQKljvaOg
---

Ederra izan da, ederra,
ederra gure artekoa

Eguzkiari eskatzen diot
elkartu gaitzala berriro!

Uztartu gintuen abestia
ez dezan urratu denborak!

Gauerdi honetako bakardadean
egunsentian dugu itxaropena
Amaigabeko gau hotzetan
zure irria gorde dut bihotzean.

Elkartuko gara larretan
milaka loreen artean!
Elkartuko gara bidean
iluna itzaltzen denean!

Mila esker laguna bihotzetik
emandako guztiagatik!