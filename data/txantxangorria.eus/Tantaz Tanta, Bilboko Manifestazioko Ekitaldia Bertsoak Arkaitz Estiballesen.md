---
id: tx-3111
izenburua: "Tantaz Tanta, Bilboko Manifestazioko Ekitaldia/ Bertsoak: Arkaitz
  Estiballesen"
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6-Q8jBQqKRs
---

Arratsaldeon eta mila esker
hurbiltze arren Bilbora.
Aste honetan ikusi dugu
Españaren metafora:
Elkartasuna atxilotua
Galindo libre kanpora
baina jai dute tantarik tanta
gu bagoaz itsasora.
Gaur sortu dugun marea urdina
geldi ezina izango da
larai, larai, larai, larai
GELDI EZINA IZANGO DA.


Besarkada bat espetxetara
muxu haundi bat sasira
eutsi goiari hemen apurka
zerbait mugitzen ari da
baina ez gaude beste batzuk zer
egingo duten begira
ahal dugun denok apur, apur bat
eman dezagun zikira
eta lehenbailehen ekar ditzagun
falta direnak HERRIRA
larai, larai, larai, larai
FALTA DIRENAK HERRIRA