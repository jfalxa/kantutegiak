---
id: tx-1089
izenburua: Ameriketara Joan Nintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DPDgjhvbafo
---

Ameriketara joan nintzan
zentimorik gabe
handik etorri nintzan maitia
bost miloien jabe.
Txin, txin, txin, txin,
diruaren hotsa,
harexek ematen dit maitia
bihotzian poza. (Bis)