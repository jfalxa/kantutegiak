---
id: tx-1355
izenburua: Germans De Llengua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fCcUihue52Q
---

Àlbum disponible en format CD i digital a:

Propers concerts i més informació a:


han vingut de totes parts, de totes les ciutats
dels pobles més petits
de costa, plana i muntanya.


gent de tot arreu de totes les edats
del més vell al més infant, del més ric fins al més pobre.

i uniràn el sud i el nord, amb el cor i amb un somriure
units per un crit de catalunya lliure

no són soldats ni són guerrers,
homes i dones del carrer, germans de llengua.

des de de lleida amb autocars, cap a la costa
han fet cap aquest matí
amb la guitarra i la senyera.

tarragona i girona finalment
semblava que hi faltava gent,
han arribat a barcelona

i uniràn el sud i el nord, amb el cor i amb un somriure
units per un crit de catalunya lliure

no són soldats ni són guerrers,
homes i dones del carrer, germans de llengua.

i s'agafen de la mà, de l'ebre a l'empordà
des d'un català francés fins a un català valencià...

i s'apreten fort les mans, i el cor els batega el pit
hi ha qui canta, ai! qui plora, ai! qui riu

i uniràn el sud i el nord, amb el cor i amb un somriure
units per un crit de catalunya lliure

no són soldats ni són guerrers,
homes i dones del carrer, germans de llengua

i uniràn el sud i el nord, amb el cor i amb un somriure
units per un crit de catalunya lliure

no són soldats ni són guerrers,
homes i dones del carrer, germans de llengua

germans de llengua...