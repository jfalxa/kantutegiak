---
id: tx-688
izenburua: Ezer Ez Da Betiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c6ERPvzraV4
---

udazken arratsaldetan
iluntzen duenean
hostoak erortzen dira
negar tanten moduan
oroitzen al dun jaietan
mozkortu ginenean
elkar maitatu genuen
izarren azpian
eta parke hutsetan azken besarkada
esan nizun dama ez zela betiko
gure artekoa bukatua dago
jadanik berandu da
agur esatea
ez dela erreza
aitor ezazu neska
musuka nazazu
begiak itxita.