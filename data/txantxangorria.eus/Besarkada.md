---
id: tx-1107
izenburua: Besarkada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n4GrwjRTtMU
---

I Haizea dabil atean, zakurra zaunka katean. 
Egin zaidazu toki txiki bat zure besoen artean.  
II Bi besoekin borobil, bi besoekin oro bil. 
Zure sehaskan nire burua ahaztu nahiean nabil.  
III Dardar naiz, baina bi esku... 
Ingura nazazu estu; nire gorputzak haize bortitzen batere beldurrik ez du.
IV Amapola bat larrean, 
belarria bularrean: bihotza, 
gaur zu entzun nahi zaitut mundua entzun beharrean.  
V Petaloak tximeleta, lilia azal gordeka. 
Biluzik nago, jantzi nazazu hemen hotzak nago eta.  
VI Ibaiak zubia hil du, urtu naiz eta amildu. Askatu eta utzi erortzen, utzi joaten, baina bildu.
VII Lore urdin bat negarrez hasi da ilunabarrez: marea gora dator, maitea, bete gaitzala aparrez.  
VIII Badatoz haize hegoak, zure hatsaren beroak. 
Utzi uzkurtzen eta laztandu itzazu nire hegoak.  
IX Haizea naiz eta ura. Besarkada lurrundu da. 
Orain aske naiz, zuri eskerrak berriz sortu naiz mundura.
X Haizea dabil atean, zakurra zaunka katean. 
Nahi al zenuke toki txiki bat nire besoen artean?