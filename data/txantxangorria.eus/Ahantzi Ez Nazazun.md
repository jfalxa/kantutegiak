---
id: tx-545
izenburua: Ahantzi Ez Nazazun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/51-s7pn4hwo
---

Diskoa /Album: Enarak itzuli dira  (2019) 
Bideoaren egilea: Nestor Urdanpilleta
Grabaketak: Nestor Urdanpilleta eta Jon Andoni Gerrikaetxebarria (Aua)
Irudi batzuk lekeitioko  Sukha  tabernan grabatuak izan dira

serge1975.bandcamp.com
instagram : @sertx_serge

Hitzak (Gari Berasaluze)

Sortaldera,
eguzkiaren bila atera den
 brigada hura gaur itzuli da,
izerdi usanairen bidean, gaur itzuli da
sortaldera

zuk bustitako maindireak banderatzat hartuta
batzuetan lurralde okupatua da desira

kondenatu nazazu,
berriro bekatuetara
zabaldu kometak, gau haizeetara
 eta ez esan ezer, lasai ez esan 
bihar goizean hitzegingo dugu, kafearekin
zuk bustitako maindireak banderatzat hartuta
batzuetan lurralde okupatua da desira
 
orain, abesti zahar baten babesarekin biluztuko zaitut
abesti zahar baten babesarekin ahantzi ez nazazun.