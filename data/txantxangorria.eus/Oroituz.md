---
id: tx-796
izenburua: Oroituz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i-73Co7Y2Fk
---

Oroitzen haiz? 
Oroitzen haiz? 

Iratze tartean 
zaldikume gazteak ginelarik 
mendi xulufraiei usainka 
edo katakurtxintxak pertsegitzen 
haize-euritan oratuta 
xoro-xoro! 
xoro-xoro! 
Beran edo Pantikosan 

Automobil gorrian 
gauaren oihanera nola sartzen ginen 
koadrillan, begiak izar, 
gasnaz mozkor 
ardoz ase 
poz-nigarretan! 
poz-nigarretan! 
Arantzan edo Erratzun 

Oroitzen haiz? 
Oroitzen haiz?