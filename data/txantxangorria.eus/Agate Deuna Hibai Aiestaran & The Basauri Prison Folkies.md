---
id: tx-2229
izenburua: Agate Deuna Hibai Aiestaran & The Basauri Prison Folkies
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b0Whq7duQ9A
---

Aintzaldun daigun Agate Deuna
bihar da ba Deun Ag/te
etxe honetan zorion hutsa
betiko euko al dabe.
Deun Agatena batzeko gatoz
aurten be igazko berberak
igaz lez hartu gagizun eta
zabaldu zuen sake/ak.

Zorion, etxe hontako denoi!
Oles egitera gatoz,
aterik ate ohitura zaharra
aurten berritzeko asmoz.

Aintzaldun daigun Agate Deuna
bihar da ba Deun Agate
etxe honetan zo/rion hutsa
betiko euko al dabe.

Ez gagoz oso aberats diruz,
ezta ere oinetakoz.
Baina eztarriz sano gabiltza,
ta kanta nahi degu gogoz

Santa Ageda bezpera dogu
Euskal Herriko eguna,
etxe guztiak kantuz pozteko
aukeratua doguna.

Aintzaldun daigun Agate Deuna
bihar da ba Deun Agate
etxe honetan zo/rion hutsa
betiko euko al dabe.


Orain bagoaz alde_egitera
agur dautsugu gogotik
Agate Deuna bitarte dala
ez eizue izan kalterik...
EUP!!