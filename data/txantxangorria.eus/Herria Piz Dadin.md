---
id: tx-2406
izenburua: Herria Piz Dadin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2srrdKic4Bk
---

2018ko uztailean Xurxo Nuñez-ek Etxalarren ekoitzia. 
Letra: Unai Mendizabal 
Xurxo Nuñez: Gitarrak, bajoa eta perkusioak 
Itsaso Elizagoien: Trikitixa eta ahotsa 
Irune Elizagoien: Panderoa eta ahots nagusia 

Etxalarko Altxata Kultura eta Kirol Elkartea


Herria piz dadin
Altxata lanari ekin
dudarik ez egin
etor zaitez gurekin

Mapari begira
nahiz ez den ikusten argi
jendeagatik da
herri txiki hau handi

Auzolana sortzen
indarrak batuta estu
gauza asko lortzen
baitira eskuz esku.

Izaerak lotzen
dituen gure kultura
hartu eta goazen
Etxalartik mundura.

Altxata ipurdiak
lanean hamaika lagun
herri txiki bizi bat
sortzen segi dezagun.