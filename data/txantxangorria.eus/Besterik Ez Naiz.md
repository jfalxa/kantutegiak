---
id: tx-2141
izenburua: Besterik Ez Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hDm1klnmc5I
---

Azalez jantzita ditut hezurrak 
Eta kolpez jantzi ditut beldurrak 
Gezurrik ez dut poltsikoan 
Ireki dizkidazu ateak 
Hautsi dizkidazu leihoak 
Dudan onena azaleratu eta 
Hondoratu dizkidazu mamuak 

Eta ni, ni hemen naiz, biluzik zure aurrean, 
Besterik ez naiz, 
Zugan bizi nahian arima guztiak zurgatzeko gai naiz. 

Zureztatu dituzu nire ametsak 
Besarkatu dizkidazu nire hitzak 
Harrotasunez bete dizkidazu birikiak. 

Dena ematearen beldurrik ez dut orain, biluzik zure aurrean, 
Besterik ez naiz 
Zugan iraun nahian errotazioa gelditzeko gai naiz 
Zugan oxigeno izan nahian baso eta oihan izateko gai naiz 
Zugan gelditu nahian denbora hiltzeko gai 
hiltzeko gai 
hiltzeko gai naiz.