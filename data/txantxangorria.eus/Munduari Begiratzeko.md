---
id: tx-2003
izenburua: Munduari Begiratzeko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LXXInSXqf9k
---

Munduari begiratzeko modu bat,
laguntzarrekin kantatzeko modu bat,
horman eguzkia hartzeko modu bat,
itxaroten jakiteko modu bat.

Pagoak ukitzeko modu bat,
azalak irakurtzeko modu bat,
hego-haizea usaintzeko modu bat,
autoan negar egiteko modu bat.

Ez onena agian,
ez ederrena agian,
zaharregia, txikiegia agian.

Begietara begiratzeko modu bat,
galderak egiteko modu bat,
eskutik heltzeko modu bat,
amets egiteko modu bat.

Ez onena agian,
ez ederrena agian,
zaharregia, txikiegia agian,
agian.