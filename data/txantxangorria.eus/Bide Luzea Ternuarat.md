---
id: tx-1540
izenburua: Bide Luzea Ternuarat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/psISthbagW4
---

Kontxatik doa untzia,
      han konpainia guzia,
      Ternuarat badoazi
      zerbait nahiz irabazi.

      Bide luze Ternuarat,
      itsasoa zabal harat,
      ubirik ez pasatzeko,
      marinelen salbatzeko.

      Itsasoaren gainean,
      untzi tzar baten menean,
      marinelak arriskuan
      Herioaren eskuan.

      Haize largoz badoazi,
      zabalerat irabazi
      itsasoan barna urrun,
      begiz ezin ikus Larrun.

      Han jotzen ditu kontristak
      mendebal haize tenpestak
      denbora gaiztoa sartzen,
      gabiko biak erortzen.

      Zeru guzia isuri,
      babazuza eta euri,
      marinelak trenpatuak,
      hotz handiak arrolduak.

      Bela guziak harturik,
      belatxoa anekaturik,
      untzia badoa segiz
      itsasoa handiegiz.

      Gau beltzean ilunbean
      untzia tormentapean,
      aparailua desegin,
      ilunez deus ezin egin.

      Elementak badarontsa,
      itsasoak abarrotsa,
      haize tenpesta uxia da
      itsasoan maskarada.

      Marinelak harrituak
      tronpen jotzen unatuak,
      gabetu indar guziez
      etsitu bere biziez.

      Urakanaren furia
      ifernuko iduria,
      Satan beltzak darabila
      untzien galtzen dabila.

      Uhainek untzia joka,
      gora-behera saltoka,
      eta branka pulunpaka,
      kostadua arrolaka.

      Uhin baten bizkarrean
      bi uhinen hondarrean
      tirabira darabila,
      untziak agertzen gila.

      Bere mastak galdu ditu,
      untzia motz da gelditu,
      itsasoak hautsi lema,
      trebes galtzerat darama.

      Uhin hautsiak gainetik
      iragaten trebesetik,
      marinelak erorika
      untzi askan igerika.

      Gaineko zubia hautsi,
      ura tilaperat jautsi,
      tilapean ura gora,
      untzia doa hondora.

      Marinelak biluziak,
      luzatu nahiz biziak,
      uhinpean igerika
      untzi puskei atxikika.

      Marinelaren bentura
      itsasoan sepultura,
      sekulako bere fina,
      etxerako berriz mina.


   (1798. urtean jasota, egile ezezaguna)