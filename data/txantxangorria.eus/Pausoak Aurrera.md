---
id: tx-2338
izenburua: Pausoak Aurrera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iUAWs8VjsTE
---

Galtzagorriak - Pausoak Aurrera (2019)

Abestia Peio Skalarik grabatua "Estudio Bat"en.
Kaki Arkarazok masterizatua.
Bideokliparen grabaketa, edizioa eta produkzioa:
Asier Leoz

Canción grabada por Peio Skalari en "Estudio Bat".
Masterizada por Kaki Arkarazo.
Grabación, edición y producción:
Asier Leoz

Rudies & Rebels

galtzagorriakt@gmail.com
Letra:

(EUSKARA)

Etorkizun iluna gurea
Borrokatzea ez da inoiz izan merkea
Zaila da gure anaitasuna haustea
Aurrera eginez aurkituko dugu bidea

Ez ditugu onartuko haien legeak
Indarra eginez apurtuko ditugu kateak

Ez zaigu axola zer egiten duen besteak
Gure ideiek argituko dizkigute kaleak

Pausoak aurrera
Non dago irteera?
Azkar bizitzea
Baita gure aukera

Urteak pasa korronte berean
Musika, borroka bada gurea
Etsaiei egurra ematean
Harrotasuna pizten da gure barnean

Gauetan tabernan
Doinu zaharren artean
Gizarte honetaz
Ihes egitean

Pausoak aurrera
Non dago irteera?
Azkar bizitzea
Baita gure aukera

Saldu diren traidoreekin
Egingo dugu sutzarra
Geratzen garenen artean
Pausoak, pausoak...
AURRERA!

Pausoak aurrera
Non dago irteera?
Azkar bizitzea
Baita gure aukera


(CASTELLANO)

Oscuro es nuestro futuro
Luchar nunca ha salido barato
Es difícil romper nuestra amistad
Siguiendo hacia delante encontraremos el camino

No aceptaremos sus leyes
Con fuerza romperemos las cadenas

No nos importa qué haga el otro/la otra
Nuestras ideas nos alumbrarán las calles

Pasos al frente
¿Dónde está la salida?
Vivir deprisa
Es nuestra elección

Pasan los años en la misma corriente
La música, la lucha son nuestras
Al darle caña a lxs enemigxs
Se enciende el orgullo en nuestro interior

Noches en el bar
Entre viejas canciones
Pa´escapar
De esta sociedad

Pasos al frente
¿Dónde está la salida?
Vivir deprisa
Es nuestra elección

Con lxs traidorxs que se han vendido
Haremos una hoguera
Y entre lxs que quedemos
Pasos, pasos...
AL FRENTE!

Pasos al frente
¿Dónde está la salida?
Vivir deprisa
Es nuestra elección