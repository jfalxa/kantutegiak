---
id: tx-2851
izenburua: Euskaraz Bizi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dQWN0kcpv8Y
---

Gure hizkuntza berpiztu dadin
ahaleginak egin behar dira
Euskara kaleratzerakoan
bizirik mantenduko dugu
Hitz egin ausartasun osoz
euskarak eskertuko dizu

Hitz egin, hitz egin
hitz egin zazu
euskararen patua
jokuan baitago