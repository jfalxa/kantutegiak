---
id: tx-2092
izenburua: Goizeko Euri Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TxAB7JKnH84
---

Goizeko euri artean aterkiñarik gabe
eskuak sakelean pena bat bihotzean
etxetik urruti nago kaletan bakarrik
goizeko euri artean kitarra bakarrekin.

Lan bat hartu behar dut gaur zerbait bazkaltzeko
iñork ez nau hartu nahi arrotza naizelako
kaletan abestu dut nere herriko kantak
jendeak zerbait utzi du goizeko euri artean.

Ez horrela ibili neskatxak esaten dit
hobeki hemen bizi gu biok elkarrekin
batzuetan galdetzen diot nere buruari zertarako
naizen hemen zergatik ari naizen betiro kezkatzen.

Bainan aurrera joan behar dut bidea hartu arte
bakarrik izan behar dut hori iritxi arte
eta egun on batean pozik ikusiko dut
iguzkia agertzen goizeko euri artean.