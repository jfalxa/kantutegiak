---
id: tx-3016
izenburua: Independentzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WQeTJRjrZ_E
---

Oskar Estanga berstolari eta musiko naparraren kantu alai bat, "Independentzia" izenekoa.

Askatasunaren izenean, 
zenbat gauza ditzakegun auta?
Aukeratzeak beti berekin 
ondorioren bat dauka,
ustez gure aldekoa dena 
bihur daiteke gure aurka,
askatzen gaituen horrek 
nola mantentzen gaituen lotuta.
Bost eta bi irabazten digu 
zazpi eguneko asteak,
erokeria eroszaleak, 
hola omen garen gazteak,
zorion hutsen bila bagoaz,
eta horiek izateak 
mugatzen gaituzten pertsonak,
eta ez gara nahi askeak...
Jende arteko harremanetan, 
zenbateraino aske geran?
Hala antzeko zehozerretan, 
ze askatasun klase ote da?
Bai, erantzuna nahi dut eman 
ondoren datorren galderan
Ea bizi ote garen benetan 
ongi sentitzen garen eran?
Gugandik hasten bait da, gure independentzia!!
Gu landu dezakegu, gure independentzia!!
Gure mugak landuta, gure independentzia!!
Ez alferrik galduta, gure independentzia!!
Kartzela zabala edo altua, 
nola ez dugu izango zalantza?
Nork esan du garena izanda 
ezin dugula egin dantza?
Umore nahia berezko dugu, 
elkarrengana garamatza,
berreskuratu beharra dugu 
garen horretan konfidantza. 
Jende arteko harremanetan 
zenbateraino aske geran?
Hala antzeko zehozerretan, 
ze askatasun klase ote da?
Bai, erantzuna nahi dut eman, 
ondoren datorren galderan,
Ea bizi ote garen benetan 
ongi sentitzen garen eran?
Gugandik hasten bait da, gure independentzia!!
Gu landu dezakegu, gure independentzia!!
Gure mugak landuta, gure independentzia!!
Ez alferrik galduta, gure independentzia!!