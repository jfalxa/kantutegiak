---
id: tx-2744
izenburua: Txoria Txori
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PRwqvk_IBNs
---

Hegoak ebaki banizkio
nerea izango zen,
ez zuen aldegingo.
Bainan, honela
ez zen gehiago txoria izango
eta nik...
txoria nuen maite.