---
id: tx-2783
izenburua: Txirriren Oroimenez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/M9eVOfCgF4c
---

1 Aita Santua elizan

Erreala Anoetan

usoa hegan egiten

o Pernando Amezketan

Txirri genuen ezaguna

Azpeiti inguru honetan

ez dakit nola adierazi

nor zen Txirri bi hitzetan

2 gizon puska dezentea

Apetitu onekoa

begirada txorrotxa ta

sudurra zuen txatoa

kopeta aldetik soiltzen

hasita pixkat kaskoa

gorputz guztian ugari

zikatriz ta arrastoa

3 edozein ordutan pronto

mutilla parrandarako

lagunak biltzen artista

hemengo izan edo hango

zahar gazte mutil o neska

berdintsu berarentzako

beiñere ez nion ikusi

presarikan etxerako

4 Mexikon Argentinan ta

Vietnamen ere turista

Marruekosen ibilia

zen gure kontrabandista

Korzegan motxileroa

Errusian alpinista

Kolombian inspektore

Kuban berriz masajista

5 bi mila ta amabosteko

abuztuan azkenian

eguzkia illundu zan

arratsearen erdian

itzulerarik gabeko

azken bidai luzian

nork daki gero zer dagon

egongo gera agian.