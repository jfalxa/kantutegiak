---
id: tx-1402
izenburua: Eraso Sexistarik Ez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cWKRWC0wydA
---

Gaur egun ez da erraza
Kanonak apurtzea
Gizartearen aurrean
Norbera bezela
Libre izatea
Trabesti, transexualak
Gay eta lesbianak
Sexu erasoen beldurrez
Herriko jaietan
Egun nahiz gauez
Eraso sexistarik ez
Ez hemen ez inon ez
Libre maita, libre bizi
Zure aukerari eutsi.
Eraso sexistarik ez
Esan ezazu ozen
Bizitzan beldurrik ez.
Beldurrik ez...
Beldurrik ez...
Beldurrik ez...
Bizitzan beldurrik ez
Azken urte hauetan
Beti albisteetan
Emakumeen aurkako
Indarkeri, eraso
Ta bortxaketak
Hau da egoera latza
Gizarte honen gaitza
Deuseztatu behar duguna
Gure eginkizuna
Salatu behar da
Eraso sexistarik ez...
Eraso guztien aurrean
Ez isildu, salatu, mugitu, zuk duzu aukera!
Ez onartu eta deuseztatu
Ez utzi erantzunik gabe horrelako jarrera
Zure eskuetan dago
Hau onartzea edo behin eta berriz salatzea
Ez zaitez egon geldirik
Mugitu, salatu, gelditu eta erantzun!
Eraso sexistarik ez...