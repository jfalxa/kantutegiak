---
id: tx-976
izenburua: Emazteen Fabore
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NgyLaY4y3jE
---

Bernard Etxepare.

Linguae Vasconum Primitiae liburuaren egilea.

1545.

Euskaraz argitaratuko lehen liburua.

Fuerte hasi zen Bernard. Bernard handia. Hitzaurretik azken poemaraino.

Emakumeen alde idatzitako letrak dira honakoak.

Etxepare Rapek kantura eramana, Irati Epelderen ahotsarekin lagundua.

Andren gaiz erraile orok bear luke pensatu
Bera eta bertze oro nontik ginaden sorthu
Ama emazte luien ala ez nahi nuke galdatu
Amagatik andre oro behar luke goratu

Gizonaren probexuko emaztia bethi da
Oro behin haietarik sortzen gira mundura
Sorthu eta hil ginate hark haz ezpaginiza
Haziz gero egun oroz behar haren aiuta.

Emazterik ezten lekuian eztakusat plazerik
Ez gizona ez exia behinere tsahurik
Exian den gauza oro gaizki erreglaturik
Parabizuian nahi enuke emazterik ezpaliz.

Bertuteak behar luke gizonetan handiago
Emaztetan nik dakusat hongiz ere gehiago
Mila gizon gazxtorik da emazte batendako
Gizon baten mila andre bere fedean dago.

Nik eztanzut emaztiak borxaturik gizona
Bana bera zoraturik andriari darraika
Zenbait andre hel baledi oneriztez hargana
Zein gizonek andriari emaiten du ogena.

Emazterik ezten lekuian eztakusat plazerik
Ez gizona ez exia behinere tsahurik
Exian den gauza oro gaizki erreglaturik
Parabizuian nahi enuke emazterik ezpaliz.

Munduian ezta gauzarik hain eder ez plazentik
Nola emaztia gizonaren petik buluzkorririk
Beso biak zabaldurik dago errendaturik
Gizonorrek dagiela hartzaz nahi duienik.

Emazterik ezten lekuian eztakusat plazerik
Ez gizona ez exia behinere tsahurik
Exian den gauza oro gaizki erreglaturik
Parabizuian nahi enuke emazterik ezpaliz.