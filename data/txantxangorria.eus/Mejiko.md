---
id: tx-1353
izenburua: Mejiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oY255qJjN64
---

Lurralde benetan argia
eguzkitsu ta beroa
bertan jatorrizko ilargiak
ta izarren izpiak
pizten du zerua.
Abestiak dira goxoak
sentimenduz ornituak
ahazten ditu arazoak
ta grina eroak
ta altxa buruak.
Zin dagizul
lurralde paregabea
zin dagizul
garranlzitsuena ohorea
Guadalupeko Ama Birjinak
magalean babesten ditu
sinismenak du eragina
nahiz beti nahi ta ezinak
ametsa ohi du gainditu.
Noizpait festa eta parrandaz
baldin bazabiltza gose
nahikoa jan eta edanda
lurrean etzanda
tekilaz ase.
Ez zara inoiz izango arrotza
jendearen artean
utziko duzu bertan lotsa
ta inoiz senti hotza
zuk bihotzean.
Zin dagizut
lurralde paregabea
zin dagizut
garrantzitsuena ohorea
Baina erne ibili zaitez
hau da gertatutakoa
behin bati egin zion kalte
gehiegi zuelako maite
berea ez zen ohean.


Mejiko, Mejiko
neskatxa ta andere
politak baita ere
zelai ea lore
maitatzeko.

Mejiko, Mejiko
argi eta kolore
ederrak baita ere
zelai eta lore 
zuhaitz ta landare
miresteko