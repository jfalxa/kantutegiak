---
id: tx-2673
izenburua: Ezkongaietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e0O8wFJPlq4
---

Ezkongaietan zerbait banintzan
ezkondu eta ezer ez; (Bis)
eder zalea banintzan ere
aspertu nintzan ederrez
nere gustua egin nuen ta
orain bizi naiz dolorez

Nere andrea andre ederra
ezkondu nintzan orduan, (Bis)
mudatuko zen esperantzarik
ere bat ere ez nuan,
surik batere baldin badago
maiz dago haren onduan.

Zokoak zikin, tximak jario,
haurra zintzilik besoan; (Bis)
adabaki desegokiagorik
gona zahar haren zuloan;
hiru txikiko botila handia
dauka bere alboan.