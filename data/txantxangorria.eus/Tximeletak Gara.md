---
id: tx-788
izenburua: Tximeletak Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rt6HaCFBkfM
---

Gauak egiten ditu milagro galantak
deabru bihurtuaz santu eta santak
estimazio gutxi daukanean tantak
atera mugitzeko jotzen dira kantak
gure behatzen puntak eta zuen hankak.

Tximeletak airean hartzen du arnasa
hauspoari emanaz hasten da jolasa
lorez-lore ibiltzen ez dago erraza
idea anitz izaten da zenbait kalabaza
hain zuzen horregatik gabiltz plazaz-plaza

Saltsarako premirik inon baldin bada
nahi bada kalejira, nahi bada balada
nahi duzuenerako Tximeletak gara

Eztarriari tire, gorputzari arre
esentziaren bila goaz larrez-larre
lantokia bihurtu zaigu akelarre
zertarako ibili jaietan hasarre
egin diezaiogun munduari farre

Nafarrotik mundura hegoak mugitu
koloreak eraman bizitza sentitu
erromeria ezin liteke gelditu
gure musikak denak aldarazten ditu
piztiak baretutak barea pizt ditu

Saltsarako premirik inon baldin bada
nahi bada kalejira, nahi bada balada
nahi duzuenerako Tximeletak gara

Saltsarako premirik inon baldin bada
nahi bada kalejira, nahi bada balada
nahi duzuenerako Tximeletak gara

Saltsarako premirik inon baldin bada
nahi bada kalejira, nahi bada balada
nahi duzuenerako Tximeletak gara


Eskatu zidatena zan lan desberdina
egingo ote nien letra bat arina
egingo ez nuen ba nere ahalegina
beti izan dalako oso talde fina
lotan dagona ere esnatzen dakina
lotan dagona ere esnatzen dakina