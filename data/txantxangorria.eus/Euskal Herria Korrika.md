---
id: tx-981
izenburua: Euskal Herria Korrika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7hi05G3KVJI
---

Euskal Herria Korrika!
Bat, bi, hiru, lau, bost, sei,
zortzi, bederatzi, hamar
Euskal Herria Korrika!

Arantzazu izan da
gordeleku gotor
euskal hots eta hitzen
babesleku jator
txori ezin zen eta
izan ginen sator
izkutua uzteko
garaia badator.

Hizkiak izkututik
badatoz argira
K bi hanka luzekin
korrika hasi da
Oñatin pasa da O
jira eta jira
ERRE ere hedatu
zaigu Euskal Herrian.

ERRE hor ibili da
Euskal Herrietan
I tximina dugu
labe garaietan
K azkenik sartu da
Bilboko kaleetan
A ikasten hasi da
non eta AEKn

Senak agindu zigun
zenari eustea
baina izkutatzea
ez al da tristea?
Nahi badugu euskara
igotzen hastea
komeni da korrika
Bilbora jeistea.