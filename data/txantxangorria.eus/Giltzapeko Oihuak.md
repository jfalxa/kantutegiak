---
id: tx-474
izenburua: Giltzapeko Oihuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YFy46ECJXXk
---

✨🎭          Ainhoa Larrañaga. 
🎬               Bernat Alberdi. 
🎥💡          Kepa Aranburu.
📸               Asier Egaña
🎧🔉          Zuzeneko soinua: Imanol Astudillo 
🥼👢👜   Jantziak: @In luuk - Iratxe Gaztelu. 
✂️👩‍🦰         Ile apaintzileak: Artez Ile-apaindegia Maider Erostarbe.
👄👁️💄   @ane Ane Pico, @iraaiia  Iraia Urkia.
🧠               Fx Ander Ugarte (Wero), Andoni Galdos.


Ongi etorri nire buruko 
Oihuen  antzerkira 
Nire katakunbetan 
Mila ahots bizi dira

Irribarre zorrotza 
Begiradetan izotza

Jende "normal" guztia 
Akotatzen justizia 
Ezberdinarekiko 
Epaile eta polizia

Kale  iskinatan 
Jende guztia zelatan

Normaltasunean preso Hilobian hobeto
Ni aldatzen saiatuagatik
Ez da aldatu mundu ero hau

Esan zu zeu zarela 
Ni akats bat naizela
Zuen arauak bete arte 
Naizenak giltzapean dirau

Zuen estigma zikin guztiak 
Bihurtu zaizkit espetxe
Libre bizi naizela uste al duzu?
Pentsa ezazu, ohar zaitez, zu ezta ere!

Albo-kalteak 
Norberak bere kateak

Normaltasunean preso 
Hilobian hobeto
Ni aldatzen saiatuagatik
Ez da aldatu mundu ero hau

Esan zu zeu zarela 
Ni akats bat naizela
Zuen arauak bete arte 
Naizenak giltzapean dirau