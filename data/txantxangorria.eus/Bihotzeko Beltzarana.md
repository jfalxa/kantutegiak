---
id: tx-2561
izenburua: Bihotzeko Beltzarana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oVcjSemrkOE
---

Gizon jator ta leialtzat 
daukat nere burua 
ez zaizkit falta andreak 
maitasun ta dirua. 
Zaldi gainean banoa 
lasai munduan zehar 
nere gidari bakarrak 
dira ilargi eta izar. 
Ay, ay, ay, ay, ay laztana 
nere bihotzeko beltxarana 
Beti nik kitarra joaz 
pasatzen dut eguna 
abestuz mariatxiarekin 
heltzen da biharamuna
Gustoko dut edatea 
goizetan uxuala 
ta arratsaldeetarako 
gaztarekin tekila