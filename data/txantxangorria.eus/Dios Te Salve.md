---
id: tx-3393
izenburua: Dios Te Salve
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CjMQl9VfRkU
---

Haritz ondoan iturri.
Ura pil pil pil erori.
Etxe hontako etxekoandreak
Ama birjina dirudi.
DIOS TE SALVE, JENDE NOBLIA
GABON JAINKOAK DIELA
LEGIAREKIN KUNPLI DEZAGUN
SANTA AGEDA BEZPERAN
Horma txulotik xagua.
Haren atzetik katua.
Etxe hontako limosnarekin
ez da beteko zakua.
DIOS TE SALVE...
Hor goian goian izarra.
Hari begira lizarra.
Etxe hontako nagusi jaunak
urre gorrizko bizarra.
DIOS TE SALVE...