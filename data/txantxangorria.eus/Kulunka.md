---
id: tx-1264
izenburua: Kulunka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tOyBSlVGcDU
---

Kulunka Olatzen sehaska kanta izandako 'Greensleeves' abestitik abiatutako moldaketa bat da. 2017ko abenduaren 1ean Mecca Estudioetan grabatutako abestia, Iñaki Salvador eta Eduardo Salvador musikariekin batera. Arriguri Kooperatibak grabatu eta montatutako bideoa. Grabaketa eta nahasketa: Mikel Eceiza (Mecca Recording Studio). Masterizazioa: Estanis Elorza (Dr. Master). Bideoa: Arriguri Koop. Pianoa: Iñaki Salvador Kontrabaxua: Eduardo Salvador Ahotsa: Olatz Salvador Doinua: Greensleeves Letra: Olatz Salvador


Kulunkaren azken ertzean
Gordetzen zara txoko beltzean
Uluka sartu zara hotzean

Loak emango dizu bakea

Ez da saltzen, ez da merkea
Duintasunaren zaporea
Itxaropenerantz heltzean
Dena dago murruz betea

Ahanzturaren bazterretan
Ezer ez daukanaren izena
Zure eskuak nire eskuetan
Eman didazu berriz dena

Desegin zaizkit, blaitu euriz
Atzo genituen sabaiak, etsiz
Esango dizut noizbait irriz
Ez gaituela bustiko berriz

Zure begiradari eutsiz
Korrika, trenez edo itsasontziz
Ihes egingo genuke aldiz
Bertan etorkizun bat balitz