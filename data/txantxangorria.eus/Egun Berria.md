---
id: tx-2224
izenburua: Egun Berria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TCKsFZZMARA
---

Gaur ere eguna argitu da 
egun berri bat bizitzeko, 
zenbatek ezingo dute bizi
gaurko egun berria. 
Teoriak ta ardurak 
ez naute eramango iñora, 
dena errezagoa da 
nahi ezkero bizitzea.
Gaur ere eguna argitu da
egun berri bat bizitzeko,
zenbatek ezingo dute bizi 
gaurko egun berria.
Une bakoitzari emanda
dagokion denbora ta aukera,
lasaitasuna bizi
hori da garrantzitsuena.