---
id: tx-279
izenburua: Herriari Kantuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jdAlOzVfLT4
---

Herriari kantuz:

Lurrarekiko lotura, herrian barne joskura.
Lilia behin eta berriz loratzen duen (h)ura.

Kantu baten irria, herritarren lokarria.
Doinu baten baitako askatasun hazia.

Mendeetan zehar irauteko eman dira mila pauso.
Izaterik behintzat ez digute ez ukatuko.

Mundua nire hitzez margotuz,
munduari kantatuz.
Hizkuntza arnastea, 
herria dantzatuz maitatzea.

Lurrak, ohiturak, lagunak; lehengoak ta oraingoak.
Ama berdinetik sortutako guziak.

Izan zirenen eta izanen direnentzat, oroimenaren bitsan:
ate ororen giltza.

Mendeetan zehar irauteko eman dira mila pauso.
Norabide bereko kolore, sustrai, arnas ta hauspo.

Mundua nire hitzez margotuz,
munduari kantatuz.
Hizkuntza arnastea, 
herria dantzatuz maitatzea.


Gure kulturarekiko eta naturarekiko dugun lotura oinarritzat hartuz sortutako kantua, inguruko paisaien eta bertako animalien irudiez osatutako omenaldia.
Herritik herriarentzat.

Hitzak eta gitarra: Jon Ollo
Gitarra eta soloak: Jon Arrieta
Ahotsa eta kajoia: Jaione Cordoba
Ahotsa eta trikitixa: Iraia Jaurena
Ahotsa eta baxua: Xabi Ariztia