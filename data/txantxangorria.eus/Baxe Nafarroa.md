---
id: tx-1416
izenburua: Baxe Nafarroa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vxRZ0KqygAg
---

Eki ahula hiri so 

argia du maltso 

aurpegia apaltzen dun 

mila koloredun 

larrazkeneko haizean 

zoin herabe hizan...

Ez haiz neguetaz beldur 

jiten bada elur 

hire lurra dun zainduko 

bertan pausatuko 

hortako behar dun, aitor! 

gehiago oharkor... 

Ager’zan, Baxe Nafarroa 

barnean dunan giroa 

Zu, Xu, Hi, nahi dunan hitza 

hire ahurrean bizitza 

zazpietaik ene aldaxka... 

ene sehaska. 

Hi, nekazarien zorra 

zonbat emankorra 

primaderako loreak 

saldu izerdiak... 

zoin eder dunan natura 

usaintzen din hurra... 

Gure ibilbidetan 

uda denboretan 

han entzunarazten ditun 

iturri bihurdun 

herri ttipiz osaturik 

ez ukan beldurrik.