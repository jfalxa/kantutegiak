---
id: tx-1673
izenburua: Iragana Hil Baino Lehen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tjHcU9U4jLk
---

Tren geltoki zaharrean zai nago
zure irrifarra oroitu nahian
baina euriak bustita, galduz doaz
ene taupada isilak

Gauean murgildu zen tren hotsa
bakarrik utziz nire biotza,
musu bero bat masailean eta lepotik
zapi goxo bat zintzilik

Iragana hil baino lehen
esaidazu arren
nora jo, non maita zaitzaket.

Iragana, hil bada ere
esaidazu arren
nora jo, non aurki zaitzaket.

Ziur nago berandu ez dela
irakurri dut ortzean.
ilargia izkutatuz doa zure begi etsietan.

Hitzez nola azaldu ez dakit
barnean dudan samina;
herbesteratuen artean
halaxe omen da herrimina.