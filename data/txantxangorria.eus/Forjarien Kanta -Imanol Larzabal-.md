---
id: tx-3158
izenburua: Forjarien Kanta -Imanol Larzabal-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qKxK0vVUm-A
---

Gaur forjariak lana utzirik
gatoz guztiok kalera
labeak oso itzaldu eta
protesta bat egitera.
Mailu burdinak bota ditugu
hantxe bertan bazterrera
ez dugu nahi berriro ere
toki hartara lanera.

Dinbili, danba ! gau eta goiza
su eta keen artean
sosegu eta deskantsu gabe
geure buruen kaltean,
horrexegatik lanak utzirik
gatoz guztiok batean.

Gora, bai gora beti, gora forjaria !
langile trebe zintzo da mailukaria
lurpera, bai lurpera beti nagusia !
ez digute egingo nahi duten guztia.
Ez badigu ematen arrazoia guri,
ez badigu ematen behar legez ongi
su emango diogu geure fabrikari
eta nagusiaren etxe guztiari.

Hiru babarrun jateagatik
horrenbeste neke, pena,
erdi ustelik aurkitzen dute
forjariaren barrena,
gure kontura egiten dute
nagusiek nahi dutena
gure lepotik gizentzen dute
bete faltrikara dena.

Oso goizetik lanean hasi
su eta keen artean,
dinbili, danba ! gelditu gabe
guztiz ilundu artean,
arropa denak puskatzen eta
osasunaren kaltean.

Ez badigute guri jornalik haunditzen
berriro gu ez gara lanera bihurtzen
arrazoiz gehiago badugu eskatzen
ez gaituzte inola hoiek ikaratzen.
Ez badigu ematen arrazoia guri,
ez badigu ematen behar legez ongi
su emango diogu geure fabrikari
eta nagusiaren etxe guztiari.
Ez badigu ematen arrazoia guri,
ez badigu ematen behar legez ongi
su emango diogu geure fabrikari
eta nagusiaren etxe guztiari.