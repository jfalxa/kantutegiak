---
id: tx-297
izenburua: Mundu Zahar
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b8DI37fP6xo
---

MUNDU ZAHAR - J Martina 

[Camarón|en "Viejo Mundo" abestian oinarrituta]

Bideoklipa: Tobby Records (@tobbyrecords) 
Nahasketa eta masterra: Oasis Studios

J Martinaren Kontaktua: kontaktua@jmartina.com

LETRA:

Mundu Zahar
Egun eta gauaren
Zaldi zuria eta beltza
Trostan zure zehar

Palazio goibel hori
Non ehun printzipek amestu zuten loriaz
Non ehun erreginek amestu zuten amodioaz
Ta esnatu ziren malkotan

Ogi apur bat
Eta ur garden tanta bat
Arbola itzala
Eta zure begiak
Haiek baino zoriontsuagoak dira
Eskale pobreagorik

Mundua
Beltzean hauts maluta bat
Giza jendearen zientzia, hitza

Herriak
Abereak eta loreak
Zazpi klimak ez dira itzalen itzal baino

Ogi apur bat
Eta ur garden tanta bat
Arbola itzala
Eta zure begiak
Haiek baino zoriontsuagoak dira
Eskale pobreagorik

Maitaleen poz uluei, keinu
Hipokritaren otoitzei, erdeinu