---
id: tx-2602
izenburua: Ez Zaude Bakarrik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4VTq-zqhlX4
---

ikastolara heldu naiz
bultzaka hasi dira 
klasera sartu orduko
daude niri begira 
hobeto litzateke bai 
lagun onak balira
jadanik galduta daukat
lehenik neukan dizdira

ikastolatik kaletik
denek ni begiratu
esaten didate
jeitsi, igo edo makurtu
laguntza eskatu, noski
baina nori eskatu
nire bila daudenenan 
ezin naiz izkutatu

ikastolatik barrena 
galdu zait umorea
zulo beltz batean nago
igual amaigabea
itzelezko mina sortu
ez guztia ordea
zeren minik handiena 
dudan bakardadea





malko bat duzu begian
ezin dena sikatu
pertsonak igarotzean
zuk burua makurtu
oso triste zaude beti
ezin dizut ulertu
nahi baduzu_etorri hona
ta kontatzen saiatu

orain galdetzen didazu
nola ote nagoen
ez duzun nigan pentsatu
nengonean txartoen
orain holan jartzen zara
ah ze faltsua zaren!
zuk soberan zenekien
zer egiten zidaten

txartoi sentitzen naiz orain
barkatu niri aizu
laguntzen saiatuko naiz
nire hitza daukazu
momento txar guztietan
zure alboan naukazu
beste_aukera bat nahi nuke
emango al didazu?





txarto egon naizenean
hau ez zait gertatu maiz
zure_aukera polit hori
heldu zait niri garaiz
basamortua nuena
orain bete zait zelaiz
nire ondoan bazaude
aurrera irtengo naiz

ikastolatik barrena 
berriz dut umorea
zulo beltz baten nengoen
ez zen amaigabea
dena ez dugu konpondu
bai gehiena ordea 
zeren jada nik ez daukat 
neukan bakardadea