---
id: tx-450
izenburua: Haizeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7mIYkZDOmMU
---

HAIZEAK by HUNTZA ft. Doctor Prats (Official Video)




Harrizko kaleak gure sustraietaraino
eustea zaila den ametsetaraino
izarak zintzilik, balkoietan itzalak
haizeak, haizeak, eraman ditzala…
eraman ditzala, eraman ditzala
ditzala…

Aizu, badakizu ez dela bate(re) ta
ondo maitatzeko,
Poliki-poliki itzuliko naiz zugana
Berriz asmatzeko.

Hem caminat entre tempestes, 
però ara tornem seguint el vent.
Des d’una nit de primavera, 
cremarem els mals moments, massa bé.

Som les de sempre amb les de sempre,
som les històries que ens han forjat.
Som tot allò que ens queda per viure,
som cada crit de llibertat.

Betikoak gara lagun, betikoekin, 
ez dezagun inoiz ahaztu;
uztaileko eguzkiak beti bezala
kiskaliko digu azala.