---
id: tx-1855
izenburua: Negu Minean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IIEqGV1iXGE
---

Negu minean 
zuriz estaliren ditun bazterrak 
hire soina,sabela eta izterrak 
Negu minean 
zuriz estaliren ditun herriak 
hire ahoa eta ezpain gorriak 
Negu minean
zuriz estaliren ditun haginen adarrak 
eta busti eginen hire begi nabarrak 
Negu minean 
zuriz estaliren ditun sahatsak 
haizeak orrazturen hire adatsak
 Negu minean 
hormatu eginen ditun inguruko ur latsak 
eta hire ohoretan iraganen ditun arratsak