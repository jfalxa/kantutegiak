---
id: tx-100
izenburua: Ez Gaitzala Loak Harrapatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6_IQkxqbOpU
---

JANUS LESTER
Ez gaitzala loak harrapatu

Jarraitu /  Follow us


Jokin Pinatxo - Garazi Esnaola - Ane Bastida - Julen Suarez -  Andoni Arriola - Mattin Arbelaitz - Javi Echarri

Grabaketa. Gaztain Estudioak
Ekoizpena. Eñaut Gaztañaga
Bideo grabaketa eta edizioa. Enjoy the silence
Koreografia. Joseba Vergara
Dantzariak. Gabriela Vallejo, Alba Bravo, Lucia Ramirez eta Libe Unibaso. 
Argazkilaria. Eneritz Zapiain

Letra :

EZ GAITZALA LOAK HARRAPATU

Gogoratzen al zara 
Denbora gelditu eta 
Ametsak piztu ziren 
Itzali gabeko gauaz

Ateak zabalik eta 
Begiak itxita dantzan 
Besarkada iheslari 
Artean kateatuta

Ez gaitzala loak harrapatu 
Ez gaitzala harrapatu
Ez gaitzala loak harrapatu 
Ez gaitzala harrapatu

Goazen kontzientziak 
Dantzan jarri genituen 
Inoiz norbait izan ginen 
Ezleku hartara

Ate birakariak 
Bueltaka dirau 
Noraezean
Lekutik mugitu gabe 
Banoa eta banator 
Ez noa inora 
Inertziak
Ibilarazten nau

Ez gaitzala loak harrapatu 
Ez gaitzala harrapatu
Ez gaitzala loak harrapatu 
Ez gaitzala harrapatu