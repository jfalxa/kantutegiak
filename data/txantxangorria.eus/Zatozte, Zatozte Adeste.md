---
id: tx-3059
izenburua: Zatozte, Zatozte Adeste
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jjNZS0tmroY
---

Zatozte!, zatozte!
Belengo estalpera,
Jaunaren semea ikustera,
Jainkoa gizon guregatik eginda.
Eta horrela bete dira
igarlearen hitzak
Esandakoa osatuaz,
Gaur gabean.
Zatozte!, zatozte!
Belengo estalpera,
Jaunaren semea ikustera,
Jainkoa gizon guregatik eginda.
Eta horrela bete dira
igarlearen hitzak
Esandakoa osatuaz,
Gaur gabean. (2)