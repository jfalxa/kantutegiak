---
id: tx-98
izenburua: Ez Daukat Ezer
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/efoGtsyHpBU
---

2022an argitaratuko den Jaio Naiz Baina EParen barruan doan abestia.
Sahatsa jauregi (@sahatsajauregi) artistarekin egindako kolaborazioaren parte dira album honetako irudi guztiak.

Artea: Sahatsa Jauregi ( @sahatsajauregi)
Argazkiak: Ander Sagastiberri ( @universidad_carlos_segundo)
Estilismoa: Julene Gregorio ( @julenegr)
Bideoa: Mikel Barberia ( @nork_____)
              Zoe Martikorena ( @zoemartiko)
Muntaia: Javier Jorajuria ( @tobbyrecords)
Produkzioa: J Martina
                      Ibil Bedi
                      Niko Bortolini
Grabazioa eta nahasketa: Tobby Records ( @tobbyrecords)
                                              Javier Jorajuria
                                              Nikolas Bortolini
Master: Eurdia ( @ibonlarruzea)


-----------------------------------

J Martina:
Kontaktua: kontaktua@jmartina.com

Ibil Bedi:
Kontaktua: ibilbedi@gmail.com

-----------------------------------

LETRA

Ezin dut ikusi atzetik datorrena
bizkarretik igotzen zaizkit
laztanak ta eztenak
zapatilek distira
korrikarik ez

Etor daitezela nire bila
inozoaren hitzak (bis)

Ez daukat ezer
ez daukat ezer zuretzat (bis)

Ez dira bereizten gezurra ta egia
galdu dira jada hirian
besarkadarik ez
atzetik dator eta
labankada bat

Etor daitezela nire bila
inozoaren hitzak (bis)

Beso leunak ta ardi bat odoletan
gari soro oso batek su
harea begitan
oskol zatiak gertu
korrikarik ez

Etor daitezela nire bila
inozoaren hitzak (bis)

Ez daukat ezer
ez daukat ezer zuretzat (bis)

Ez daukat ezer (bis)