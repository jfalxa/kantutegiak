---
id: tx-1987
izenburua: Athletic
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xRXo1tkx8uE
---

Athletic gorri eta zuria
denontzat zara zu geuria.
Herritik sortu zinelako
maite zaitu herriak.
Gaztedi gorri zuria
zelai orlegian,Euskal Herriaren erakusgarria.
Zabaldu daigun guztiak
irrintzi alaia:
Athletic, Athleticzu zara nagusia.