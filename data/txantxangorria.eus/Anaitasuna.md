---
id: tx-2705
izenburua: Anaitasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/69_IOi6LpX8
---

Batasunaren ordua.

Euskotar guztien batasuna.

Non jaio garen ez du garrantzirik

maite ba dugu geure Euskalerria.

Benetan daukat bihotz alaia.

La,la,la,la,la,la.

Batasuna, denon batasuna.

Batasunaren ordua.

Bizkaia, Gipuzkoa, Araba,

Benafar, Lapurdi, Zuberoa,

Nafarroako erreinuarekin

egiten dute geure Aberria.

Zazpiak bat da Euskalerria.

La,la,la,la,la,la.

Batasuna, denon batasuna.

Batasunaren ordua.