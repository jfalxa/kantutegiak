---
id: tx-1086
izenburua: Mende Laurden
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fqMVmJgIFnk
---

Eguzkiak argitzen ditu,
Bilboko kale ilunak .
Eta berdez margotzen ditu,
Hain beltzak ziren izkinak.

Etor zaitez lagun kaleak hartzera, aste nagusiko gauetara..

Mende laurden eguzkizale, ta beti iraultzaile.
Mende laurden eguzkizale, amalurraren alde.(bis)

Ez dago bilbon zu baino hobe,
bihotz gorridun izar berde,
ekologismoa , gogor defendatzen.
Askatasunak duen bide,
malkartsu honi aurre eginez,
bilboko herriaren, bozeramaile.

Mende laurden...

Txupinetik agurrera,
txupinera ta marijaiak,
erein dituzte txosnetan,
eguzkilore alaiak.

Etor zaitez lagun kaleak hartzera, aste nagusiko gauetara!!

Mende laurden...

Parte hartzaile aktiboa,
herri baten oihartzuna,
ekologiaren txoko euskalduna.
Konpartsa baten ahalegina,
Bilboko jaien lagunmina,
zuri eguzkizale, esker mila!!!

Mende laurden eguzkizale, ta beti iraultzaile.
Mende laurden eguzkizale, amateurraren alde.(bis)

Beti eguzkizale , beti eguzkizale, beti eguzkizale, mende laurden.(bis).