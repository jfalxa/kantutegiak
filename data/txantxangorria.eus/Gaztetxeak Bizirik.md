---
id: tx-2629
izenburua: Gaztetxeak Bizirik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4ezJBiHYarI
---

Donostian Kortxoenea
Euskal Jai Iruñea
Erori arren jaikiz
berriro eraikiz
guztiona delako gaztetxea!

Guztiona da eta ez da inorena
kendu nahi digutena
baina ematen du
ez direla konturatzen
gazte geranez ez gera nekatzen.

Bota dituzte paretak!
itxi dituzte ateak!
itxi dituzte leihoak!
zabal ditzagun ahoak!

Sortu nahi baldin badegu beste aldea
guretik ta gurea,
parrandaz haratago
zer egin badago
lau esku dira bi baino gehiago!

Zirkua, hitzaldiak edota baratza,
herriaren ardatza.
Jaten degunez eta
ereitez degunez
esnatuko gera biharko egunez!

Herrian sortu berri den
gazte asanbladaren irria
lau haizetara zabalduz
hegan doa bere aldarria
astintzeko, askatzeko, aldatzeko
lotan dagoen herria
txikitik iraultza eginda izan dadin mugarria!