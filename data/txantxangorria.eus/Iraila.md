---
id: tx-3360
izenburua: Iraila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_OWZdsSz-zw
---

Iraila
Iraila izango zen
hitzak urtaro batetik bestera
bidaltzen genituen
izen gabeko txorien antzera...

Zerua ilun
soineko beltz
zu hain eder
Nik zerotik hasi nahi nuen...

Iraila izango zen
munduaren nekea hostoetan
maite izan genituen
gauzek zentzua galdu zuten ia...

Zerua ilun
soineko beltz
zu hain eder
hain eder

Zerua ilun
zorua orbel
zu hain eder...

Nola igarri orduan
iragan hau guztia
betiko izango genuela aurretik...

Aurretik
Iragan hau guztia...