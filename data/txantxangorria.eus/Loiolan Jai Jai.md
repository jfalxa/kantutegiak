---
id: tx-2748
izenburua: Loiolan Jai Jai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QeydMnONp8Q
---

Behin batian, Loiolan,
erromeria zan.
Hantxen ikusi nuen
neskatxa bat plazan
txoria baino ere
ariñago dantzan.
Huraxe bai polita,
han politik ba zan!

Esan nion desio
senti nuen gisan,
harekin hizketa bat
nahi nuela izan.
Erantzun zidan ezik
atsegin har nezan
adituko zidala
zer nahi nion esan.

Aurkitu ginanian
inor gabe jiran,
koloriak gorritu
arazi zizkidan.
Kontatuko dizuet
guztia segidan,
zer esan nion eta
nola erantzun zidan.

-Dama polita zera
polita guztiz, ai!
baiñan halere zaude
oraindik ezkongai.
Ezkon gaitezen biok!
Esan zaidazu bai!
-Ni zurekin ezkondu?
Ni zurekin? ja, ja!

Behin batian, Loiolan,
erromeria zan.
Hantxen ikusi nuen
neskatxa bat plazan
txoria baino ere
ariñago dantzan.
Huraxe bai polita,
han politik ba zan!