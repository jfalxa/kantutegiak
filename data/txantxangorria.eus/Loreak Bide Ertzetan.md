---
id: tx-861
izenburua: Loreak Bide Ertzetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9tRKkjQc6Vw
---

Argazki gehienak: Josu Odriozola


Espaloi gainetik norabiderik gabe
Lore lehorrak eskutan ditudalarik
Ezta ezer baliokorik
Huts hau betetzekorik

Bakardadea

Poza tristura bilakatu da
Segundu batean 
Madarikatua
Nola ahaztu egun beltz hura
Zama ilun honekin 
Bizitzera kondenatua

Lore sortak hilda 
Daude bide ertzetan
Baina zu bizirik 
Nire barnean (bis)

Ez nuen inoiz usteko 
Honela izango zenik 
Hain etorkizun hurbilean
Banatuko ginenik
Tamalez ez nuen izan
Zu agurtzeko aukerarik
Betiko nire bihotzean
Edonon zaudelarik

Zenbat gazte hil dira
Burdin artean odolhustuta

Ez zegokien heriotza (bis)

Ez nuen inoiz usteko
honela izango zenik 
Hain etorkizun hurbilean
banatuko ginenik
Tamalez ez nuen izan
Zu agurtzeko aukerarik
Betiko nire bihotzean
Edonon zaudelarik