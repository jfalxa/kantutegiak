---
id: tx-3042
izenburua: Hegazti Errariak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eStpNeCSpwM
---

Hegazti errariak
pausatu dira
leihoan
argia eta itzala
bereizten diren lekuan
argia eta itzala
leihoan
pausatu dira
hegazti errariak