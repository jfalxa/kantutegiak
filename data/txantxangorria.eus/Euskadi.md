---
id: tx-2056
izenburua: Euskadi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v1UFfyuD26I
---

Ikusi nahi ditugu behin eta betiko
gure hamaika mutil mundu guztizen kontra.

EUSKADI EUSKADI...!!

Arratzaldeko 7rak katedralean
eguna ta momentua iritzi dira
jokalariak zelai orlegian daude
dena listo, dena prest....
Astera doaaaa
Ikusi nahi ditugu behin eta betiko
gure hamaika mutil mundu gustizen kontra.
Etxetik zarata bat entzuten ba dozu
ez izutu, ez beldurtu ezer ez data, 
ez da bomba ezta ekaitza, ez lurrikada,
herria Euskadin gola ospatzen ari da!
Nahiz eta txarto juan
Euskal hintxa zuekin
segi aurrera Euskadi, 
orain eta beti!!!

EUSKADI EUSKADI...!!