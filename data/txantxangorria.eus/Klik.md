---
id: tx-2070
izenburua: Klik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3Bs3NX45pwQ
---

Dena aldatu egin da, dena da berritu,
auzolana zena elkar-lana bihurtu,
denen parte hartzea derrigorra dugu
etorkizunak lotan ez gaitzan aurkitu.

Norbanakoen bulkadaz hasitako hura
orain herriak eman beharko bultzada,
Ikastolen Elkartean dago sustraitua
klik batekin joango gara infinitura.
 
Klik bat eginda aurrera goaz
keinu txiki bat etorkizunerantz,
Urola bailaran ditu Euskal Herriak
hizkuntza zaharraren ikasle berriak

Lau hankan aurrena, pausoz-pauso hurrena,
Euskara datorkigu historian barrena
Barruan izan arren hitzik ederrena
Hizkuntzarik onena dugu hitz egiten dena.
 
Klik bat eginda aurrera goaz...

Pauso bat emanda zatoz Kilometroakera!
Hurrats bat eginda haur-ikastola sortzera!
Keinu bat eginda goaz indarrak biltzera!
Klik batekin indartuz euskal izaera!

Klik bat eginda aurrera goaz...