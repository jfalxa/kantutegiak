---
id: tx-1916
izenburua: Inertzia Zeiharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6g-KjFiVtGI
---

Canción inspirada en el poema "Oblicua Inercia" de el poeta burgalés Juan Carlos García Hoyuelos publicada en el poemario Libro/DVD " Se lo Dije a la Noche".

Composición musical, grabación y mezcla: 
German A. Navarro B.

Arreglos vocales y adaptación al Euskera:
Lore Calypso

Producción y realización de Video:
PixelArtSound - German A. Navarro B.

Ezinegona ta zure keinu bizia
banatzen duen lerroa ezberdintzen jakingo
banu zalantza egingo nuke , 
zalantza, zalantza egingo nuke.
Zeure ezbaien jabe naiz, haien artean
egiten dut lo, haien artean,
Donosti eta Burgosen baitan,
zu eta ni.
Zure betazalek, zurtz hutsi dute zerua, bizitza gabe.
Zure betazalek, gure lehen ilargi biluzia ostentzen dute.
Ilargia lebitatzen du, itsu zu lo egin, lasai,
zurea eta nirea, gorputz berak
beldur ezberdinez.
Irrikaz, pasioz behatzen dut zure ia ahitu atsedena,
naiz zure ezpainen begirale azken ametsaren amaieran.
Gure goizak liztuz hezeak,
lehenago ostendutako pasioz igurtziak,
inertzia, inertzia zeiharraz.
Eta zure azalean nire eskuen aztarnak geratzen dira,
ta zure azalean bi itsasargi eta zigilua bezala
tatuatzen den apar segizioa.
Olatuak haragizko labarretan hautsiz,
bi ozeano haserretu batzen duen olatu aztarna