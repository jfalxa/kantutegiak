---
id: tx-1585
izenburua: Infernuko Hauspotik Irtetzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6eyKBg9Njoc
---

Durangon bazkalduta
Mañarian gora
Durangon bazkalduta
Mañarian gora
Txakurzuloan jausi naiz
ipurdiaz gora.
Txakurzuloan jausi naiz
ipurdiaz gora.

Mañaritik hasita
Urkiolaraino
Mañaritik hasita
Urkiolaraino
ez dago besterikan
aldapia baino.
Ez dago besterikan
aldapia baino.

Aita San Antonio
Urkiolakua
Aita San Antonio
Urkiolakua
hamaika neska mutil
uztartutakua.
hamaika neska mutil
uztartutakua.

A/kordatzen al zara
zer egin genduan
Akordatzen al zara
zer egin genduan
Urkiola gaineko
paguan onduan.
Urkiola gaineko
paguan onduan.


Ekin, ekin, gazteak,
neuk esan artean;
Ekin, ekin, gazteak,
neuk esan artean;
etxera joango gara
gaua iten denean.
etxera joango gara
gaua iten denean.

Hamaika demoniño
baiagok munduan
Hamaika demoniño
baiagok munduan
Josepa lengusinak
engañau ninduan.
Jo/se/pa len/gu/si/nak
en/ga/ñau nin/du/an.

Diman erromerian
maietz pazkoetan
Diman erromerian
maietz pazkoetan
hor/txe da/toz mu/ti/lok
nes/kak kon/bi/de/tan
hortxe datoz mutilok
neskak konbidetan


Neskak konbidetako
Dimako plazea
Neskak konbidetako
Dimako plazea
mutilek galdu dabe
sedazko poltsea.
mutilek galdu dabe
sedazko poltsea.