---
id: tx-2847
izenburua: Gorlizko Olentzeroa 2015
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/110XTzkFmeE
---

KAIXO GUZTIOI AURTEN BERRIRO

GORLIZEN DA OLENTZERO

ASKO POZTEN NAIZ ZUEK EUSKARAZ

MINTZATZEN ENTZUNEZ GERO

ASKO POZTEN NAIZ ZUEK EUSKARAZ

MINTZATZEN ENTZUNEZ GERO

Aitite ta amama

Aita eta ama

Osaba ta izeko

Familia dana

Opariak baditut

Gorlizko haurrentzat

Jatorrak ta zintzoak

Zareten denentzat

Famili on bat dugu

Eguberrietan

Janari ta edari 

Onena denetan

Baina gogoan izan

Ezer ez duena

Errefuxiatu eta 

Txiroen kondena

Maitasuna dastatu,

Ukitu, sentitu

Baietz egun hauetan

Edonon aurkitu

Telebistan ikusi

Dituzue sarri

Eta nahi ditut zuen

Gogora ekarri

Besarkada bat Gorliz

Bihotzez emana

Olentzeron partetik

Musu haundi bana

Gogoan izan haiek

Eguberrietan

Banatzen jakitea

Ona da benetan

KAIXO GUZTIOI…