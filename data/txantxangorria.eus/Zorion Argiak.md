---
id: tx-1825
izenburua: Zorion Argiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Kyd3eOY7YoA
---

Zorion argiak piztu dituzte 
ez dakit ondo zer ospatzeko. 

Baina argi hoiek ezin dute garbitu 
munduko miseriak daukan aurpegi zikina. 

Gauero zerua begiratzean, 
izarrak dira nire zorion argiak. 

GEZUR BATEN ERDIAN 
BIZI GARELAKO 
BEHAR ZAITUT MAITIA 
EGIA IZATEKO. 

Sentimenduak saltzen, opari-paperekin estaltzen 
egunero bizitza ezin den azaldu. 

Urrearen dizdiran begiratzean 
zer ikus daiteke bere barnean. 

Gauero zerua begiratzean 
zu zara nire izarra ta zorion argia!