---
id: tx-852
izenburua: Morts Pour La Patrie
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ztdF90wYIig
---

Kaleko Urdangak (Bardulia, Basque Country) 2019.

(EUS) "Morts pour la patrie" 2019ko azaroan atera den Vis Vires & Kaleko Urdangak EP konpartituko  abestietako bat da. Sound of Sirens Iruñeko estudioetan grabatua Julen Urzaizen gidaritzapean. Bideoklipa Hodei Izarrak ekoiztua eta grabatua, Kemen Atxurra, Maialen Arrinda, Aingeru Bidaurreta eta Inge Abrisketaren laguntzarekin. Eskerrik beroenak Bermeo, Algorta eta Bergarako gudariei, Gorpuzkingz eta Intxorta 1937 kultur elkarteari.


Email: kalekourdangak@gmail.com

MORTS POUR LA PATRIE

Ezkil hotsa udako sargorian
DEI BAT! herrian barrena
Armak hartzera behartu gaituzte
FRANTSES! aberriaren alde
Ta Baionatik burni bidean
Biba ta txalo artean
Adorez goaz orain bi egun
Ume koskor ginenak

Gu gara 49. erreximentua
Zin egin digute garaipen azkar bat
Laster bueltan etxera

Lubaki hezeak nigar ta garrasiak
Haragi ustel usaina
Anaia hila Verdungo lur beltzean
Lagun denak galduak
Infernuko atean Somme ibarrean
Ofizial arrotzen menpe
Vive la France allez! oihukatuta
Engainatu gaituzte

Hemendik entzuten dut erbesteratua
Ama aiten nigarra
Ene azken arnasa, ez naiz itzuliko
Oi ama Eskual Herria!

Euskaraz baizik ez zekiten haiek
Haiena ez zen aberriaren alde