---
id: tx-3068
izenburua: Las Kabras
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/F2fCQ1zf2xg
---

Plentzia-Gorlizko NON OTE taldeak eginiko abestia Plentziako "Las Kabras" koadrilarentzat. 
Koadrila honek 2009ko Antolin Deunaren jaietan urrezko alpargata jabetu zuen jaietan izandako partehartzeagatik.
Letra eta musika: Xabier de la Maza