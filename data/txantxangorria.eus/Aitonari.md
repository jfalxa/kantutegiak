---
id: tx-2641
izenburua: Aitonari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2fxbEieOE3Q
---

Zuhaitz, ari zara emeki ihartzen
zuritzen, biluzten,
nekatzen, lokartzen.

Etxea duzu oraindik berotzen,
argitzen, babesten,
elkartzen, bizitzen.

Amesten duzu zure hazi beretik,
egiaren bidetik,
orbel gorri artetik.

Euri tanta bat sustrai gogorretatik,
bizitzaren errotik,
amaiera atzetik.

Amaier atzetik
orbel gorri artetik.

Eta gu,
euri tanten artean,
itsaso zabalean,
bihotzak estutuz.

Zu,
itxaropen betean,
bizitzaren aurrean,
arnasari helduz.