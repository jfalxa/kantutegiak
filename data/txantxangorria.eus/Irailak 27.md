---
id: tx-457
izenburua: Irailak 27
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cwhNTXpzCSQ
---

Errekaldian lizarra
goiko mendian negarra
gaurko egunez itzali dira
gure anaien biziak.

Errekaldian lizarra
goiko mendian negarra
gaurko egunez itzali dira
gure anaien biziak.

Modu txarrean epaituak
frogarik gabe erahilak
amorruz eta erneguz dira
lehertu bihotzak

Euskalerriak negar batean
jaso notizia
gure zainetan odola_hasi da
irakin beharrean.

Errekaldian lizarra
goiko mendian negarra
gaurko egunez itzali dira
gure anaien biziak.


Burroka ez da bukatu ondiño,
ez da bukatuko
Euskalerria askatu artio
ez dugu, ez, etsiko

Gure herri zaharrak
arnas betean
aurrera joko