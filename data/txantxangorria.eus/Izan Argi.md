---
id: tx-2341
izenburua: Izan Argi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C_2jY9WFCRI
---

Sustrai bizien soinua
Elkartuz bihotzetan
Ahalegin izpiak batuz
Jaio zen ikastola
Ametsak egi bihurtuz
Kolorez jantzi gara
Zu ere bazara argi
Loreztatu euskara
Bidea pausoka eraikiz
Oinez elkar lagunduz
Nafarroa aberastuz
Denok dantza dezagun!
Mugimendua sentitu
Gure erritmoa entzun
Erriberan beti zara
Ongi etorria lagun!
Gure herri
Geroaren argi
Orainaren irri
Eutsi euskarari
Erroen hari
Auzolanen lokarri
Txikien ahotsari
Bozgorailu handi
Ez etsi

Euskararen dizdira
Izan argi
ekarri digu Argiak
Izan Argi
Herriaren indarraren
Izan Argi
Ahots geldiezina
Hegoaldean, zure Nafarroa Oinez
Ebro ibai aldera gurekin etor zaitez
Hegoaldean, zure Nafarroa Oinez
Eguzkialdera gurekin etor zaitez
Hegoaldean, zure Nafarroa Oinez
Erribera zainduz, gure Nafarroa Oinez
Euskararen dizdira
Izan argi
ekarri digu Argiak
Izan Argi
Herriaren indarraren
Izan Argi
Ahots geldiezina
Ebro ibai aldean, zure Nafarroa Oinez
Eguzkialdean, zure Nafarroa Oinez
Hegoaldean, zure Nafarroa Oinez
Erriberan Izan Argi piztu gure Nafarroa
Oinez