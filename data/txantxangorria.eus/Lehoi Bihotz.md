---
id: tx-1570
izenburua: Lehoi Bihotz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2rvU1iR0v08
---

Hitzak: Mikel Bizar
Musika: Mikel Bizar / Jagoba Ormaetxea
Ahotsak: Mikel Bizar - Gexan Etxabe - Amaia Luzarraga
Orkestazioa: Jagoba Ormaetxea
Zuzendaritza: Iker Sanchez
Piano: Julio Vega


Entzun haizea
narama zuzen zure alborantz
arima(re)n doinuan
gordeta ditut zure oihuak

Lehoi gazte batek behar dau sentitu bizitza
izarren hautsean izan arren orroak be badira

Ein dut amets zure barne
dirdiratuz argi gabe
odol hontan itsasoa
zainetatik doa
zure hutsune magalean
lehoi bihotz zutunik dau
harmaila honen taupadetan
nire alboan