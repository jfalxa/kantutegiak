---
id: tx-966
izenburua: Zuk Non Duzu Zauria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JllPqKJyCSc
---

“Mutiko bat da” zioten,
segur eta harro hazi ginen
eta ongi erakutsi ziguten
gordezinak gordetzen
barnea iraultzen…

Zuk non duzu zauria,
malko arrasto lehorra,
urratu ttipi hura?

Bagenekien gizon ginela,
arraildurek zeramatela
oinazearen muinera,
bakarrizketa izanen zela
ihesa.

Zuk non duzu zauria,
malko arrasto lehorra,
urratu ttipi hura?

Bekoz-beko jarrita gaudenean
konfidantza egin hurran
hotzikara etortzen da.

Zuk non duzu zauria
malko arrasto lehorra
urratu ttipi hura?