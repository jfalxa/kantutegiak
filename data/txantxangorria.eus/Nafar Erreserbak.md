---
id: tx-1610
izenburua: Nafar Erreserbak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zIWBeZlluH0
---

Dandor doinuak Ibañetako gainean
Nafar erreserbak, aizkorak eskuan
Arrano Beltza, zelatan zeruan hegan
Harriak, enborrak, etsaiaren kontra

Buruak altxa, azken guda dantza
Igitai eta mailu guztiak
Gorria odola, gorria bihotza
Orreagako bailaran!

Lehen aldiz elkarturik,
atzerriko erasoen aurka
Eneko Aritza gidari,
amaren etxea defendatzeko prest!

Inperio arrotzak,
piztiz beteriko armadak
Itxoiten ez zuten herri txiki baten
kolpea jaso zuten bertan!

Zortzi puntako izarrekin,
kateak ate bihurturik
Zapalduek historia idaztear daukate

Buruak altxa, azken guda dantza
Igitai eta mailu guztiak
Gorria odola, gorria bihotza
Orreagako bailaran!

Arrano beltza, zelatan zerua hegan
Nafar erreserbak, aizkorak eskuan
Buruak altxa, azken guda dantza
Igitai eta mailu guztiak
Gorria odola, gorria bihotza
Orreagako bailaran!

Zortzi puntako izarrekin,
kateak ate bihurturik
Zapaldutako herrien arnasa
historian zehar entzungo dute


Buruak altxa, azken guda dantza
Igitai eta mailu guztiak
Gorria odola, gorria bihotza
Orreagako bailaran!