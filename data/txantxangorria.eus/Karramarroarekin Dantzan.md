---
id: tx-3284
izenburua: Karramarroarekin Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8Zei-4GZKag
---

Musika: Xabier Azurmendi
Musika konponketak: Iñaki Salvador

Itsas arroketan ni jolasean
karramarro bat kolkotik jaitsi zitzaidan
bi printzekin koska-koska zintzilik ipurdian
bertan itsatsita nuela hasi nintzen dantzan.

Atzera, atzeraka
salto eginez bira
atzera, atzeraka
karramarroarekin batera.

Atzera, atzeraka
salto eginez bira
atzera, atzeraka
karramarroarekin batera.