---
id: tx-2190
izenburua: Artzeren Esana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N46phkq2srU
---

Dantzarako musika elektronikoa, eurodance deritzana, euskaraz eta modu arrakastatsuan jorratzen lehena izan zen Hemendik At! taldea. 1995ean sortu zen, Iruñean, eta sintetizadorez egindako doinu dantzagarriz eta hitz aldarrikatzailez beteriko hiru disko grabatu zituen Gor diskoetxearentzat, harik eta 2002an desegin zen arte. Ordura arte euskaraz ez zeuden doinu batzuk eta estetika bat ekarri zituen taldeak. "Goazen", "Orain" eta "Sex-sua" izan ziren gehien dantzatutako kantuetako batzuk. Taldekideetako batzuek Ados sortu zuten ondoren.

20 urte inguruko hiru gaztek sortu zuten Hemendik At!: Mattin (ahotsa), Iker Sadaba (ahotsa eta programazioak) eta Arantza (ahotsa). Eurodance estilora lerratutako kantuak euskaraz egitea zen hirukotearen asmoa. 90eko hamarkadaren hasieran indarra hartu zuen musika joera horrek, besteak beste 2Unlimited, Twenty 4 Seven eta Masterboy taldeen eraginez. Sintetizadorez sorturiko esaldi melodikoak, emakumezkoen ahotsa eta korua, gizonezkoen rap zatiak eta beat edo erritmo sendo bat ziren musika horren osagaiak.

Taldekideen lagunek bakarrik entzun zuten maketa bat grabatu zuten, eta Gor diskoetxera bidali zuten. 1997ko udaren atarian ikusi zuen argia "Goazen" kantuak, Aurtengo GORakada bildumaren barruan. Kantak oihartzun handia lortu zuen herrietako festetan eta tabernetan, eta dantzarako musika elektronikoaren inguruko aurreiritziak gainditu zituen (bakalaoak gogor jotzen zuen garaia zen). Hortaz, luze gabe heldu zen lehen diskoa, Hemendik at! (Gor, 1997), Stella Berzal abeslariarekin grabaturikoa. Hemendik At!-en berrikuntzetako bat, hitzen edukiari dagokionez, dantzarako musikari lotutako irudi hedonista hutsa apurtzea izan zen. Izan ere, Euskal Herriko gatazka politikoaz, euskararen egoeraz, presoez eta sexu indarkeriaz hitz egiten baitzuten kantuetan, ikuspegi abertzale batetik. Horrek eraman zuen beren estilokoak ez ziren hainbat talderekin agertokia partekatzera: Alaitz eta Maider, Kashbad, Koma... "Goazen" kantuaren arrakastaz baliatuz, Wazen izeneko talde bat sortu zen, musika estilo bertsukoa baina Hemendik At!-ekin harremanik ez zuena.

1998an, Mattinek taldea utzi zuen, eta Alberto Domingok ordezkatu zuen, eta 1999an, Edurne Arizuk hartu zuen Arantzaren lekua. Laukote horrek grabatu zuen Orain (Gor, 1999), lehen diskoaren ildo berekoa baina landuagoa. Kantuen artean, Gozategi taldeak Jon Maia bertsolariarekin Nafarroa Oinez-erako egindako Jaso dezagun euskara kantuaren moldaketa zegoen. "Independance", "Espetxeratu zintuztenetik", "Anti nazi" eta "Bake bidean" kantuetan hitzen ildo politiko eta aldarrikatzailea berretsi zuen taldeak.

Etorkizuna hirugarren eta azken diskoan (Gor, 2001), lehen aldiz ingelesez abestutako kantu bat sartu zuten, "No more tears for you", eta "Bandera, untzi eta kalaberaz" kantua single berezi batean argitaratu zuten, Philly Crewk egindako nahasketa berriekin. Behin formularen berritasunak eragindako ustekabea gaindituta, diskotik diskora taldearen oihartzuna apalduz joan zen, eta 2002an banandu egin zen Hemendik At!.

Ondoren, Stella Berzalek eta Edurne Arizuk Ados bikotea sortu zuten, eta izen bereko disko bat grabatu zuten. Ukitu etnikoko hip hop eta dance musika egin zuten, beste hizkuntza (arabiera, portugesa...) eta tresna batzuk (djembe...) hartuta. Iker Sadabak ekoitzi zuen Adosen diskoa. Halaber, Iluna Bar taldean aritu ziren bi kantariak, eta Itxaroten (Kuma) diskoa grabatu zuten.

Iker Sadabak dance-pop musika sortzen jarraitu du.