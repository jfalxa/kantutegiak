---
id: tx-2583
izenburua: Biba Berango
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BgeyZeBVNvg
---

BIBA BERANGO
 
                Hitzak: Gabriel Aresti, 1964
                Musika: herriarena
 
 
            apur dezagun katea
            kanta dezagun batea
            hau da fandango
            biba berango.
 
Munduan beste asko lez,
arta buruan oskolez.
Atzo aspertu nintzaden
maisuez eta eskolez.
 
Poeta naizen ezkero,
eztut zerurik espero.
Bederatzi kopla ditut,
lau zuhur eta bost ero.
 
Ilargiaren adarrak:
handik zintzilik abarrak.
Gogoangarriak dira
zure bi begi nabarrak.
 
Zerutik dator harria.
Nundikan berriz argia?
Gau ilun honetan dakust
zure aurpegi garbia.
 
Martxoko oilar gorria,
inundik ez etorria,
goseak arintzearren
judu batek igorria.
 
Goizaldean eguzkiak
printzak daduzka bustiak.
Oso merke saltzen dira
euskaldunaren ustiak.
 
Gure amonak esan dit:
aitak arnoa edan dik.
Joan zaitez tafernara
eta ekar zazu handik.
 
Euskeraren asturua
ezta gauza segurua:
Askozaz hobekiago
dabil munduan judua.
 
Eta hau hola ezpazan
sar nerila kalabazan.
Ipui txit barregarriak
kontatu nituen plazan.