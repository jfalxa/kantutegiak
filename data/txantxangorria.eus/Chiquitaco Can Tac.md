---
id: tx-965
izenburua: Chiquitaco Can Tac
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6tNY0yGJ5aQ
---

Gure txikitako kantak, kanta ederrenak
Gaur egun entzuten ez direnak
Gure txikitako kantak, kanta ederrenak
Euskeraz ziren gehienak

Bertan herriko tabernetan
Gertatzen ari dena
Tristea da benetan
Erre, fustrazioan ere
Euskeraz ezin entzun
kanta bat bera ere

Tradizioan anbizioa
Euskalduna zenekoa
Tradizioan traizioa
Izan liteke gaurkoa

Gure txikitako kantak, kanta ederrenak
Gaur egun entzuten ez direnak
Gure txikitako kantak, kanta ederrenak
Euskeraz ziren gehienak

Gazte herrigintzan desastre
Zure esku dago ta
oraingoz la cagaste

Tronko! errepika ta foto
Hainbeste reggaeton ta
Hainbeste reggaetonto

Eszena beltza eszena txuri
Bakoitza bere neurrian

Pitufi kanta
Pitufi guaia
Pitufoaren herrian

Rocka saiatu ta borroka
Orain dugu erronka
Etxean ate joka

Ea giroa batpatean
Zenbat aldatu zaigun
Hamarkada batean

Kinko etxean telecinco
Telebista betiko
Ez al da itzaliko

Nike gustora edango nike
Juan Carlos ta Felipe
Kalimotxo jelikpe

Tradizioan anbizioa
Euskalduna zenekoa
Erregerik pe
Erreggaetonik pe
Limoik eta jelik pe

Gure txikitako kantak, kanta ederrenak
Gaur egun entzuten ez direnak
Gure txikitako kantak, kanta ederrenak
Euskeraz ziren gehienak

Gure txikitako kantak, kanta ederrenak
Kalean entzuten ez direnak

Gure txikitako kantak, kanta ia denak
Euskeraz ziren gehienak