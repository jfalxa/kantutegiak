---
id: tx-1227
izenburua: Anabasa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lUwFJHjN3Sg
---

Ikarragarrizko anabasa 
naturaz arduratu gabe 
borondate on guztia 
apurtuz sentzu gabe 
Burdinezko sugetxa da 
dena hautsiaz dakarte 
herriaren hitzak 
baliorik ez daukate 

Mendian barrena doa 
baserriak deuseztatuz 
Ze Esatek! Anabasa Lyrics
herri txikientzako 
kalterik besterik ez du 
Nazio aberatsak 
gehiegi aberasten ditu 
herri txikientzako kalterik besterik ez du 

Trenak suntzitu anabasa gelditu 
(4 aldiz) 

Batzuk hipokreziaz dira beraz zain mintzatzen 
prozesuan izenean 
herritarrei lapurtzen 
gure nortasuna ala ez dute isiltzen 
arazoen aurrean 
indarrez gera altzatzen 

Trenak suntzitu anabasa gelditu 
(4 aldiz)