---
id: tx-2661
izenburua: Esola - L'Estaca
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9bHEo6Q1vzo
---

L'avi Siset em parlava 
de bon matí al portal 
mentre el sol esperàvem 
i els carros vèiem passar. 

Siset, que no veus l'estaca 
on estem tots lligats? 
Si no podem desfer-nos-en 
mai no podrem caminar! 

Si estirem tots, ella caurà 
i molt de temps no pot durar, 
segur que tomba, tomba, tomba 
ben corcada deu ser ja. 

Si jo l'estiro fort per aquí 
i tu l'estires fort per allà, 
segur que tomba, tomba, tomba, 
i ens podrem alliberar. 

Però, Siset, fa molt temps ja, 
les mans se'm van escorxant, 
i quan la força se me'n va 
ella és més ampla i més gran. 

Ben cert sé que està podrida 
però és que, Siset, pesa tant, 
que a cops la força m'oblida. 
Torna'm a dir el teu cant: 

Si estirem tots, ella caurà... 

Si jo l'estiro fort per aquí... 

L'avi Siset ja no diu res, 
mal vent que se l'emportà, 
ell qui sap cap a quin indret 
i jo a sota el portal. 

I mentre passen els nous vailets 
estiro el coll per cantar 
el darrer cant d'en Siset, 
el darrer que em va ensenyar. 

Si estirem tots, ella caurà... 

Si jo l'estiro fort per aquí... 


Agure zahar batek zion 
bere etxe aurrean
goizean goiz lantokira 
irteten nintzanean:
Ez al dek, gazte, ikusten 
gure etxola zein dan?
desegiten ez badugu, 
bertan galduko gera.

Baina guztiok batera 
saiatu hura botatzera,
usteltzen hasita dago-ta, 
laister eroriko da.
Hik bultza gogor hortikan, 
ta bultza nik hemendikan,
ikusiko dek nola-nola 
laister eroriko dan.

Baina denbora badoa, 
nekea zaigu hasi,
eskuak zartatu zaizkit, 
eta indarrak utzi.
Usteltzen badago ere, 
karga badu oraindik,
berriz arnasa hartzeko 
esaigun elkarrekin:

Baina guztiok batera 
saiatu hura botatzera ...

Agure zaharra falta da 
gure etxe ondotik,
haize txar batek hartu ta 
eraman du hemendik.
Haur batzuk ikusten ditut 
eta inguraturik,
aitona zaharraren kanta 
nahi diet erakutsi:

Baina guztiok batera 
saiatu hura botatzera...