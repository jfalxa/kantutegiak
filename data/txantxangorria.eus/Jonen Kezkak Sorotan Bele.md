---
id: tx-2844
izenburua: Jonen Kezkak Sorotan Bele
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N1GTmVsgEMo
---

Jon-ek kezkak baditu
ea lagunekin bat datorren
aste burutan irteten
saiatuz bere penak gordetzen

Beirazko ontziak iraultzeko
ez du zailtasunarik aurkitzen
taldearen magalean
ez da inoiz bakarrik ikusten

Hondarribiko kaleak
bere zapatak urtzen dituzte
jendearen eritziei
jadanik ez die arreta ipintzen

Noiz behinka nekatzen dio
herriko giro alda ezinak
gatzaren beharra dauka
gauerdiari zaporea hartzeko

Ez da geldirik iraungo erori arte
ez da etxera joango lortu gabe

Gogoratuko ditu
umeetako abenturak
neskatxei erakusteko
nor ote zen ausartena

Bere tristurak landatzen
kupela botatzen joan da
merezita duen uzta
hasi da orain jasotzen

Batzuen lilura lortu arren
askok ez du ulertu nolakoa den

Jonek jarraitzen du bere neska maitatzen
berak argi dauka iparra non dagoen
baina buruz-bera dihardu hemen