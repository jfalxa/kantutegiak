---
id: tx-402
izenburua: Bakarrik Nago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rYPXn1vUi3I
---

Ustegabeko ausentziak ustegabeko unetan,
jendea saltoka dantzan dabil zuk musika entzun ezin.

Zeren zuk ez duzu dagoeneko inor ezagutzen. Holako gau ilunetan, bakardadearen eskuetan plastikozko topaketak.

Bakarrik nago ilargia bezala, bakarrik nago ilargia ni naiz.
Mila begiraden azpian, bakarrik nago!!

Azken orduko lagun berriak, non daude betiko lagun zaharrak? Norbait zurekin ari da blagan bainan zuk ez duzu ezer ulertzen.

Jendetzaren erdian, bi zatitan erdibitua.
Hanka bat han hemen bestea, lur gainean ur azpian, plastikozko topaketak.

Bakarrik nago...

Ustegabeko jokuak ustegabeko partaideak.
Gorputz goseti hordoituak piztu-ezina piztu nahiean.

Egunsentia badator bere atzapar luzeekin. Egunerokotasuna esnatuko da zurekin, krudela da bizitza krudela azken piztiekin.