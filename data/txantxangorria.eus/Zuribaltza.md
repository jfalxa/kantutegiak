---
id: tx-53
izenburua: Zuribaltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0BBn3VdN4zA
---

Bideoa Joseba Batiz

Hogeitabost Urtetan, 
Lamina baltza, 
Lamina zuri
Lamina beti Aratustetan. 
Azkenengo urtean, 
eliza barri_allega da gasa 
Zirriak ein bero berotan

Aratustetan
Atorra, Lamina jarri
gitxarra, soinua hartu
euroz bete poltsikua 
Atorrak, Lamiak 
Aratustetan Zuribaltza, 
zuribaltza Aratustetan(2)