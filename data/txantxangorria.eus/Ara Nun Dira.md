---
id: tx-2712
izenburua: Ara Nun Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3zAjn4LT7WU
---

Hara nun diran mendi maiteak!
Hara nun diran zelaiak!
Baserri eder zuri-zuriak,
iturri eta ibaiak.
Hendaian nago zoraturikan,
zabal-zabalik begiak,
nerea, baino lur hoberikan
ez da Europa guztian.

Agur bai, agur Donostiako
nere anaia maitiak,
Bilbaotikan izango dira
aita zaharraren berriak.
Eta gainera hitz neurtuetan
garbi esanez egiak,
Sudamerikan zer pasatzen dan
jakin dezaten herriak.