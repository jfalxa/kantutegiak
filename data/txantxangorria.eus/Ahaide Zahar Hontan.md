---
id: tx-2370
izenburua: Ahaide Zahar Hontan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zdJ5Q4iPNw8
---

Zazpi urta badizü, ene karinoa
zütan ezarri nüela, ene amodioa
geroztik nabilazü, tristerik gogoa
beldürrez ez zidazan, zü gogoa gaixoa

Zazpi urtez zitzaützat, ebili ondotik,
ene tronpatü nahiz, malezian beti
orai eginagatik, aisa eni herri
begiraizü jinkoak, zü ez zitzan püni

Ahaide zahar hontan, bi berset berririk
alegrantziareki, kantatü nahi dütüt;
bihotza libratürik, pena orotarik,
desir nüan maitea, beitüt gogatürik

Pikanki mintzo zira, gizoner maitea,
ez sunja bazeneza, düzün estatüa,
ez balinbanü maite, fidelitatea,
elitzeikezü konbeni, hola mintzatzea

Zaldi xuri bat badüt, zure zerbütxüko
zük plazer düzünean, biak juraiteko.
Etxekoer errezü, dolorez adio
ützüliren ez zira, mentüraz haboro.