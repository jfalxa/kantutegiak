---
id: tx-2066
izenburua: Boga Boga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Shj_coU_PqM
---

Boga, boga
mariñela
mariñela
joan behar degu
urrutira
urrutira
bai Indietara,
bai Indietara.
Ez det,
ez det,
ez det,
nik ikusiko
zure kai ederra
kai ederra
Agur,
agur,
agur,
Ondarroako
itsas
itsaso bazterra,
itsas
itsaso bazterra.
Mariñela,
mariñela,
boga!
mariñela.