---
id: tx-1120
izenburua: Aske Bizi Nahia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/273_0xGCuyM
---

Historia ezkutatu nahi izan dute



Gaztelako inperialista militarren aginduz



konkista inbasioa odolezko hitzarmena



Nafartarren subiranotasuna erasotuz



Inbasioak kateez lotuta gauzka



errepresio gordina demokraziaz margotuta



matxinaden aroak erresistentziaren aroak



gogora ekarriaz bidean eroritakoak



Askatasun egarriak



ez du ezagutzen inoiz errendizioa



errespeta dezatela



euskal herritarron aske bizi nahia



bahituta izana



tiraniaz kolpatua



bizirik dirauen herria



Nafarroa



Laino beltzen artean izpiak zulatuz



kolore aske ta biziek zerua bereganatuz



senide ta lagunak guregana gerturatuz



independentzia nahia barrenetik oihukatuz



Askatasun egarriak...



Pauso nekezenak eman zituztenak gogoan 



beraien irudiak tatuatuz bihotzetan



pauso nekezenak eman zituztenak gogoan



Aurki argia, gure herria, eguna



kolore biziz dator etorkizuna



erori zirenei, merezi dutenei



seme alabei irriak zor dizkiegulako



Askatasun egarriak...