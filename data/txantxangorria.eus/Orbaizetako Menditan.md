---
id: tx-2734
izenburua: Orbaizetako Menditan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c7wS6JUtGNs
---

Orhoit othe zira zu nexkatxa hamabost urte gintila
Zure begien behako hua gau hartan loratu zela
Ederra zinen nexkatxa Bortu-Izarra bezala
Ariman sartu zintzaitan gure hamabost urtetan
Orbaizetako menditan geroztikan banabil ametsetan.

Zure begi beltzen dirdiretan ene gaua iruzkitu
Nahi gabezko estekak oro ahalko nuzkela moztu
Ederra zinen nexkatxa Bortu-Izarra bezala
Hego xuri bat zen piztu libertatia enetu
Orbaizetako menditan geroztikan banabil amentsetan.

Juanen ginela juanen gu biak jantzatzerat elgarrekin
Nigar ihintza begian eta gerrian bero batekin
Ederra zinen nexkatxa Artzain-Izarra bezala
Goizalderaino besotan inguru-inguru jantzan
Orbaizetako menditan geroztikan banabil amentsetan.

Arrastiri goibel hartan egin despedida penarekin
Jenderikan bazen soberakin elhakatzeko zurekin
Ederra zinen nexkatxa Bortu Elurra bezala
Adios erran gabetan oi berritz jantzatzekotan
Orbaizetako menditan geroztikan banabil amentsetan.

Iratitik Orreagaraino bortuak elur xuritan
Iduri ere izaiten zuten zeruak eztei jauntzitan
Ederra zinen nexkatxa Libertatia bezala
Bizi bazira oraino kantu hau da zuretako
Orbaizetako menditan geroztikan banabil amentsetan.