---
id: tx-1826
izenburua: Joxek Andreari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GwKpnbXKQI4
---

Joxek andreari beti egunero
galdera berdina egiten deutso,
Felisa maittie egie gero!
Zenbat gizonekaz ein dozu lo?
Bermeon hasitze Gernikeraino
neska ezaguna izan nintzen gero!
Urkiolara joan ta San Antonio
zeugaz hasi nintzan betirako.
San Fermin utzita San Antonio
kortako zekorran adarrentzako
Felisa maittie gezurre baino
hontxe aukera dozu esateko.
Ai Jesus Maria! Hau entzuteko
nire gizon honek holan esateko!
Josetxu maittie isildu hobeto
zeugaz bakarrik egin dut nik lo.