---
id: tx-1793
izenburua: Jardin Bat Zuretzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZabePD5DgTU
---

Egunak eta gauak, 
uda ta negua, 
etsipen ilunena, 
zorion ordua, 
bizitzaren zauriak, 
helburu galduak, 
laztanik samurrenak, 
bide erratuak, 
elkarren babesean igarotakuak. 
Egunsentiko ihintza, 
goizeko loreak, 
itsasoaren hatsa eta koloreak, 
urruneko hiriak, 
lurralde hobeak, 
musika eder baten azken akordeak, 
maitasunezko hitzak barruan gordeak. 
Eskutikan helduta amilduak gera 
erreka bat bezela menditikan behera, 
oinazea geurea, 
geurea plazera, 
itzal bihurtuko naiz zu galtzen bazera, 
itzalaren itzala ezereza bera. 
Mugarik gogorrena, 
noizbait hil beharra, 
igaro eta gero munduko zeharra, 
menta beltxen usaina zure irrifarra, 
betiko gozatzea nere ametsa da, 
zeruko jardinetan, 
zerurikan bada...