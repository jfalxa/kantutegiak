---
id: tx-1063
izenburua: Oilanda Gazte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x4C8InF4vDw
---

Oilanda gazte polliño bat gure herrian badugu,
Hegal-petik lumaño bat falta ez balimbalu,
Munduan aski ezin liteke oilanda hortaz espantu.

Pello Joxepe tabernan dela
haurra jaio da Larraunen.
Etxera joan ta esan omen du:
"Ez da nirea izanen,
bere amak topa dezala
haur horrek aita zein duen".