---
id: tx-2870
izenburua: Txori Erresiñula
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7JeK0iVncH0
---

Txori erresiñula
üdan da kantari
zeren ordian beitü
kanpuan janari;
negian ezta ageri
balinban ezta eri.
üdan jin baledi,
konsola nainte ni.



Txori erresiñula
ororen gehien,
bestek heno hobeki
hark beitü kantatzen
harek du inganatzen
mündia bai tronpatzen;
bera eztüt ikusten,
bai botza entzüten.

    


Botz aren entzün nahiz,
erratürik nago,
ni hari hüilant, eta
hura hürrünago.
jarraiki ninkiro
bizia gal artino;
aspaldi handian,
desir hori nian.

    
 

Txoria zoñen eijer
kantüz oihanian!
Nihaurek entzün dizüt
igaran gaüan,
eia! guazen, maitia,
bibiak ikustera;
enzüten badüzü,
xarmatüren zütü.

    
 

—Amak utzi nündüzün
bedats azkenian;
geroztik nabilazü
hegalez airian.
Gaiak urtuki nindüzün
sasiño batetara,
han züzün xedera,
oi ene malürra!

    
 

Bortiak txuri dira
elür dienian;
sasiak ere ülhün,
osto dienian.
Ala ni malerusa!
zeren han sartü nintzan?
Juan banintz aintzina,
eskapatzen nintzan.

    
 

—Txoria, zaud'ixilik,
ez egin nigarrik.
Zer profeitü dükezü
hol'aflijiturik?
Nik eramanen zütüt,
txedera laxatürik,
Orhiko bortütik,
ororen gañetik.

    
 

Txoria zaude ixilik,
ez egin kantürik;
txoria zaude ixilik,
ez egin kantürik;
eztuzu profeitürik
ni hola penatürik,
ez eta plazerik
ni tunban sarturik.