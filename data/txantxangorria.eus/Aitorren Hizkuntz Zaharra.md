---
id: tx-2182
izenburua: Aitorren Hizkuntz Zaharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8tZaXL-d3PQ
---

1968an Bergarako Uretxindorrak taldeak lau abestien diskan azaldu zuen kanta hau. Gaur egun, gure hizkuntzaren defentsan bigentzia osoa du. Inkunablea