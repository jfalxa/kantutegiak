---
id: tx-89
izenburua: Nire Errua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yqylfk2NX4g
---

2021eko neguan Zestoako Gaztain Estudiotan grabatua eta nahastua @gaztain_estudioak
Kolaborazio berezia: Ainhoa Arroitajauregi @ainhoaarroita
Gitarra eta ahotsa: Fran Urias @franuriasagirre
Baxua: Ane Bastida @bastitxatxi
Bateria: Iñaki Bastida @bastisaiz
Grabazioa, nahasketak eta ekoizpena: Eñaut Gaztañaga @gaztaingrises

-------------------------------------------

Bideoaren grabazioa, edizioa eta montajea: Kulturaz kooperatiba @kulturaz_koop

Hitzak:

Itsasoaren magaletik
Bidali nuen keinua
Zin egin nuen gibeletik
Eta hitzik ez dut bete ordutik
Hitzik ez dut bete oraindik

NIRE ERRUA IZAN ZEN
PROMESA GUZTIAK HAUTSI NITUEN
NIRE ERRUA IZAN ZEN
ORAIN ETA BETI, BETI BEZALAXE, BETI BEZALAXE

Olatuen barruan
Naufrago baten islada
Hondar artean bilatzen dut
Galdu nuen oihartzuna
Inoiz izan ez dudan hura

NIRE ERRUA IZAN ZEN
PROMESA GUZTIAK HAUTSI NITUEN
NIRE ERRUA IZAN ZEN

-------------------------------------------