---
id: tx-1517
izenburua: Larru Beltzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/12EWPPuLUw0
---

Neguaren bihotzeko begirada zaurituak
gurutzatzen ditugu plaza desolatuetan
gerla garaien mehatxuak aidean doaz
justiziaren larru beltzak sardezkatuz

Denboraren orratzak plegatzen gabiltza
larru beltzak josi asmoz

Ez dago eskubiderik ahurretan hiltzen
denik gose ankerraren lautada hotzean
ogi beharraren premiak sabeletan doaz
justiziaren larru beltzak sardezkatuz

Denboraren orratzak plegatzen gabiltza
larru beltzak josi asmoz

Plaza desolatuetan galdez daude denak
noiz ote diren beren haurrak itzuliko
baina trenak umezurtzez beterik doaz
justizizaren larru beltzak sardezkatuz

Denboraren orratzak plegatzen gabiltza
larru beltzak josi asmoz