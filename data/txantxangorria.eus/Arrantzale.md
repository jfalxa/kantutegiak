---
id: tx-1461
izenburua: Arrantzale
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CymUvZ-stLY
---

Arrantzale maitagarri
Gogoan diat hire irri
Libertatez hain egarri
Hautatu hire aberri
Arrantzale maitagarri
Gogoan diat hire aberri

Abertzale bi gudari
Guretako haiz erori
Sasitar ta presoeri
Laguntza nahiz ekarri

Tirokatu xoriari
Pozik hago ihiztari
Ez uste ken geroari
Esperantzaren iturri.

Gertatua Didieri
Hautetsien nahikari
Lotu gaiten borrokari
Bizi dadin Euskal herri