---
id: tx-1056
izenburua: Gaur, Bihar Eta Etzi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iUAI5qs2O5k
---

Transmisio altxor baliotsua dugu
Aberatsak gara naiz eta ez esplotatu
Ez gara ohartzen herri batek eztu irauten
dakienak ez badu mingaina erabiltzen

Dm Am 
Txakurra eta umeari euskaraz
Dm Am
gustura sentitzen zera
Dm Am
Lagunekin ohituraz beti erdaraz
C Em
madrilen, burgosen, sevillan bezela
Dakigunok ez badugu gure hizkuntza erabiltzen
akabo euskara eta akabo eh

Oraina, geroa, gaur, bihar eta etzi
mantentzeari baino indartzeari eutsi
Zazpi lurraldeak elkartzen gaituen zubi
kale, lantegi, ikastola euskaraz busti

Txakurra eta umeari euskaraz...