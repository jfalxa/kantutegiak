---
id: tx-2826
izenburua: Etorri Bihotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WFWZzTbvxhg
---

Etorri bihotza, naiz betirako,
etorri bihotza, nire bila bazabiltza,
ezkutuko unetxoak ditut zuretzako,
isildutako ipuinak kontatuko dizkizut,
etorri bihotza, nauzu aldatuko,
etorri bihotza, gaur zoriontzeko,
etorri bihotza...
Maitasuna galdu nuen hitz goxoen baladan,
gozatzeko ordu zaharrak datozen zalantzak,
zure itzalak argia du dantzan,
zure egarriz nago ni esperantzan....
Baina, etorri bihotza, naiz betirako, 
berandu egin aurretik atera ilunpetik,
maitea, bihotza eta laztana,
etorri bihotza, naiz betirako!!!