---
id: tx-1785
izenburua: Zuretzako Hitz Ederrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NtNtjo0oLxw
---

Bi milagarrenez, paper zuri-zurialez
hitzen bila ziñez, zoritxarrez
gaurkoan ere beldurrez

Ta bere buruaz beste egiten duen guztiak
zuretzako hitz ederrak utzi ditu idatziak...

Bi milagarrenez, paper zurialez,
hitzen bila ziñez, zorionez,
bihar itzultzeko miñez

Ta bere buruaz beste egiten duen guztiak,
zuretzako hitz ederrak utzi ditu idatziak,
historian irakurriko dizkizute ilargiak,
baina lainoak eztali dituen eguzkiak...

Izen ondo berriak landatu nahi...
aditz ondoak ureztatu nahi...
bainan ospelegia duk herria hau,
eta zu haize bila! 

Ta bere buruaz beste egiten duen guztiak,
zuretzako hitz ederrak utzi ditu idatziak,
historian irakurriko dizkizute ilargiak,
baina lainoak eztali dituen eguzkiak...