---
id: tx-2057
izenburua: Txori Barrote 25 Urte
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DIx1TNxqs00
---

Hogeita bost urtetan
istorio pila.
Pozak, ezustekoak,
une ederrak mila
Festan barneratzen da
gogotsu okila,
jai herrikoien alde,
elkartasun bila.

Jendetzak Areatza betetzen
duenean
kolorez janzten dira
jai-guneko ertzak.
Indartsu azaldu dira
marra zuri-beltzak
egon ezin
dutenen ametsak beteaz.

Okilek  kolpe ta kolpe
lanari ekin diote.
Txioka zera diote:
AURRERA TXORI BARROTE

Urrun daudenen alde
lan isil, apala.
Lagunen argazkia,
lagunen itzala
festan agertuko da
behar den bezala.
Nahi ez duenak ere
ikusi dezala!

Beti aldarrikatzen
eta beti alai
okila izango da
barrotea hausteko gai.
Marijaia zain dago,
Gargantua ere bai.
Txori Barroterekin
egingo dute jai!