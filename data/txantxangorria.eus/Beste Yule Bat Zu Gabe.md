---
id: tx-105
izenburua: Beste Yule Bat Zu Gabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a1Dlf0oDX6o
---

Beste Yule bat zu gabe-

Balder hil zuten mihura hostoez
apaindu dut gure atea.
Ahuntzaren haragia irentsiko dut
Thor jaunaren izenean.
Hoztu du nire bihotza lur honen hezetasunak.
Abendu latza ta deslaia, iragarri dit igarleak.

Beste Yule bat zu gabe,
Zure gerezi zaporeko ezpainak
dastatu gabe.
Noctumbriako ziegetan askeagoa nintzen, gudari garaituen koplak desafinatzen.

Kiskalitako enborraren keak usatzen ditu ispirituak.
Zabarkeriaz entzuten ditut ospakizuen barre ta algarak.
Katu bat zure aldarean,
Freyja bera izango ote da?.
Dena suntsituz ihesi noa nere arimaren belztasunerantz.
Has enviado
Has enviado
Escribe a Hibai Deiedra