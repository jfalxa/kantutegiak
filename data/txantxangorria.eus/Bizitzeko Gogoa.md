---
id: tx-629
izenburua: Bizitzeko Gogoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/y24OfnLUZd4
---

Denbora aurrera doa
Erlojuaren orratzak
Tiki, tak
Ezin da atzera beitu
Ta ezin da kontrolatu geroa
Horregatik lau haizetara
Askatu nahiko neuke dekoten korapiloa
Eten barik ittoten nauen soka hau
Apurtu ein biot
Egin nahi dodazan gauza guztixek egin
Erratzeko aukera bat euki nahi dot
Ta inderra dagoen artien, bidiek zabalduz
Nigan sinestu
Ta begixek itxite
Sentitzen dot barruen
Itxaso bat kolpeka, bai
Zelan dekoten
Askatasun beharra
Bizitzeko gogoa
Bizitzeko gogoa...
Laino guztixen gainetik
Eta sasixen azpitik
Bizi naz,
Eta nago mugaturik
Ezer esan ezinik
Ixilik
Horregatik lau haizetara
Askatu nahiko neuke dekoten korapiloa
Eten barik ittoten nauen soka hau
Apurtu ein biot
Egin nahi dodazan gauza guztixek egin
Erratzeko aukera bat euki nahi dot
Ta inderra dagoen artien, bidiek zabalduz
Nigan sinestu
Ezin dot gehiago
Ta begixek itxite
Sentitzen dot barruen
Itxaso bat kolpeka, bai
Zelan dekoten
Askatasun beharra
Bizitzeko gogoa
Bizitzeko gogoa
Ta begixek itxite
Sentitzen dot barruen
Itxaso bat kolpeka, bai
Zelan dekoten
Askatasun beharra
Bizitzeko gogoa
Bizitzeko gogoa
Ta begixek itxite
Sentitzen dot barruen
Itxaso bat kolpeka, bai
Zelan dekoten
Askatasun beharra
Bizitzeko gogoa
Bizitzeko gogoa!