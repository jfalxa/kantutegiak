---
id: tx-769
izenburua: As Noites Da Radio Lisboa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DRvhlvzM7dM
---

Ezpazioan orteaz ertzik ez... lehiotik at
Argiak bustitzen du nere ontzia
Grabitate ezean nigan oriomen oro
Iragana bihurtzen da gardena
Lurraren azala ximeldu zen
Harrizko uhinak, plastikozko mende
Infinitora narama, ni naiz Ulises
Planetaren hautsak haizeak
Zabaltzen ditu orbean
Itzalean logelan flexoaren ondoan irratia
Bihotz baten taupadak nahasten ziren
Radio Lisboaren emisioez
Gauaren azken mugetan... mugan