---
id: tx-2367
izenburua: Bazen Behin Zaldupe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a_toCD6mfzk
---

BAZEN BEHIN ZALDUPE 
Bazen behin eskola bat Ondarrun, 
pertsonak libre hezitzen, 
ostadarraren mila kolorez. 
Frida Kahlo, Txirrita, Jane Goodall. 
Albert Einstein, Iribar… 
Zuk nahi duzuna izan zaitezke. 
Nikaragua, Maroko, Galizia… 
Mundu guztiko umeak elkarren lagun, ondarrutarrez. 
Uu. denon eskola gara. 
Uu. askotariko jendea. 
Uu. aukera berdinak bultzatzen. 
Izarrak gertu daude Zaldupen. 
Bizitza maite dugu Zaldupen. 
Bazen behin eskola bat Ondarrun, 
pertsonak libre hezitzen, 
ostadarraren mila kolorez. 
Uu. denon eskola gara. 
Uu. askotariko jendea. 
Uu. aukera berdinak bultzatzen. 
Izarrak gertu daude Zaldupen. 
Bizitza maite dugu Zaldupen. 
Mundua bertan dago Zaldupen.
Euskarak bizi gaitu Zaldupen. 
Frida Kahlo, Txirrita, Jane Goodall. 
Albert Einstein, Iribar… 
Zuk nahi duzuna izan zaitezke. 
Nikaragua, Maroko, Galizia… 
Mundu guztiko umeak elkarren lagun, ondarrutarrez. 
Izarrak gertu daude Zaldupen. 
Bizitza maite dugu Zaldupen. 
Mundua bertan dago Zaldupen. 
Euskarak bizi gaitu Zaldupen. 
Ametsak gertu daude Zaldupen. 
Natura maite dugu Zaldupen. 
Mundua bertan dago Zaldupen. 
Euskarak bizi gaitu Zaldupen. 

Hitzak: Kirmen Uribe Doinua: Keu Agirretxea 

Bideoa: Josu Eizagirre Eta baita ere Gorka Txakartegi, Goretti Serrano, Enara Ituarte, Beñat Eizagirre, Aiora Diez, Aiza Gandarias, Txomin Ituarte, Ander Badiola, Ane Martinez de Luco, Nora Agirretxea, Leire Azpiazu, Mikel Martinez de Azagra, Josu Garate, Asier Ituarte, Kepa Arakistain, Ane Agirre, Nora Agirre, Beñat Bidegain, Leire Gomez, Eihar Uribe, Kemen Lertxundi, Markos Unzeta, Ana Elordi, Maria Illarramendi, Maritere Kaltzakorta eta Josu Erviti. 

AME Estudioetan grabatua 2018ko abenduan.