---
id: tx-1613
izenburua: Zerbait Asmatuko Dugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c4LH4rMt8tg
---

Hozkailua marmarka
Ezin lorikan hartu
Biharko egunak ere
Hogeita lau ordu ditu
Dei baten esperoan zaude
Ez da iristen
noiztik da
Zure bizia
Atzera kontu bat bihurtu zela
Abixua buzoian
Hasteko mehatxu
Alabak dio ama
Zerbait asmatuko dugu
Dei baten esperoan zaude
Ez da iristen
noiztik da
Zure bizia
Atzera kontu bat bihurtu zela
Atzera kontu klube bat
Ta inoz geratu daitekenaren
Itxaropen mailegua
Zer da ba bizitza hau
Zer
Zer
Zer