---
id: tx-1277
izenburua: Denbora Da Poligrafo Bakarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/PM7B0ECqnpQ
---

Berri Txarrak's official video of the song 'Poligrafo Bakarra', taken from their triple new album 'Denbora Da Poligrafo Bakarra' (Only In Dreams, 2014). Song produced by Ricky Falkner, video by Joseba Elorza (www.miraruido.com) //

'Poligrafo bakarra' kantaren bideoklip ofiziala, 'Denbora da poligrafo bakarra' (Only In Dreams, 2014) lan hirukoitzetik erauzia. Kantua Ricky Falknerrek ekoitzia da. Bideoaren egilea: Joseba Elorza. 


Ikasteko presa daukat eta, 
Artisauaren marmolezko egon-harria 
Kalera baino lehen, guztion antzera, 
Eguneko maskara aukeratzen 

Distortsioari itsatsia,
Barneko galderei entzungor egiteko
Edo horiek denak behingoz,
Kolpe bakarrez erantzuteko

Denbora da poligrafo bakarra.
Beste dena aieru...
Denbora poligrafo bakarra.

Pozik nire haitzuloan, zuloan, nire ametsen siloan
Han estalaktitak, urten letaginak, 
Hegan joan ziren beldurrak,
Nire kanturik onenak…

Denbora da poligrafo bakarra.
Beste dena aieru...
Denbora poligrafo bakarra.

Ez eska aholkurik .....

Eta pasioa da hemen exijitzea zilegi den gutxieneko hori

Denbora da poligrafo bakarra.
Beste dena aieru...
Denbora poligrafo bakarra.

Beste dena aieru...