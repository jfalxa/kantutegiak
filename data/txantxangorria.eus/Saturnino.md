---
id: tx-3298
izenburua: Saturnino
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jdQsxUNw8NI
---

Iaz egin nuen
lagun berria
Saturno planetatik
etorria
bere plater txiki
hegalaria
nire teilatuan zen
eroria.
Trikitrixa jotzen du
esku batez
txape/la jazten du
kolore berdez
piperrak irensten
egun ta gauez
zenbatzen du aldrebes.
Saturnino! Saturnino!
Nirekin bizitzen gelditu zaitez
beste egun bat
gehiago mesedez!
Iaz egin nuen lagun berria
Saturno planetatik etorria
bere plater txiki hegalaria
nire teilatuan zen eroria.
Trikitrixa jotzen du
esku batez
txapela jazten du
kolore ber/ez
piperrak irensten
egun ta gauez
zenbatzen du aldrebes.
Saturnino! Saturnino!
Ni/re/kin bi/zi/tzen
gel/di/tu zai/tez
bes/te e/gun bat
ge/hia/go me/se/dez!
I/az e/gin nu/en la/gun be/rri/a
Sa/tur/no pla/ne/ta/tik e/to/rri/a
be/re pla/ter txi/ki he/ga/la/ri/a
ne/re te/ila/tu/an zen e/ro/ri/a.
Tri/ki/tri/xa jo/tzen du
es/ku ba/tez
txa/pe/la jaz/ten du
ko/lo/re ber/dez
pi/pe/rrak i/rens/ten
e/gun ta gau/ez
zen/ba/tzen du al/dre/bes.
Sa/tur/ni/no! Sa/tur/ni/no!
Ni/re/kin bi/zi/tzen gel/di/tu zai/tez
bes/te e/gun bat
ge/hia/go me/se/dez!
Sa/tur/ni/no! Sa/tur/ni/no!
Ni/re/kin bi/zi/tzen
gel/di/tu zai/tez
bes/te e/gun bat
ge/hia/go me/se/dez!
bes/te e/gun bat
ge/hia/go me/se/dez!