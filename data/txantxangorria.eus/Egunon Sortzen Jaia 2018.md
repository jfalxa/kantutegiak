---
id: tx-2482
izenburua: Egunon Sortzen Jaia 2018
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WbXnNoQMGc4
---

Kanta: EgunOn
Taldea: Afu

MILA ESKER:

BARAÑAINGO ALAITZ I KASTETXEKO IKASLE ETA GURASOAK

IOSU MARTINEZ | musika edizioa

AFU
BEÑAT ZIGANDA | baxua
IBAI GOÑI | bateria
XABIER GOMEZ | saxoa
MIKEL LAZKOZ | kitarra
ION ERRO | tronpeta
OIHANA FERNANDEZ | ahotsa

SUBSISTAK
MIRIAN UHALTE | koroak
AINHOA ERDOZAIN | koroak`

ANUK EPELDE ARROYO | hasierako ahotsa

GAIZKA AZKETA | bideo edizioa

IGOR ARROYO ETA AIORA EPELDE | hitzak



"Beste planeta batean nago. Dena da kolore berekoa. Pertsona denak berdin-berdinak. Denak mintzo dira hizkuntza berberan. Ze tristea, ze aspergarria den planeta hau...
Ufa! Zelako amets gaiztoa!

Eguna esnatu eta
Egunon! esatean
koloretako izpi bat
sortu zait ezpainetan.

Goazen gosaltzera
Egunon familia!
bai goxoak direla
kolorezko jakiak

Kalera atera naiz
Egunon auzoa!
Etxe sailen artetik
sortu da ortzadarra

Autobusean goaz
lagunak kantuan
helmugara helduta:
Egunon eskola!

Hitza nahiz bizitza
hobe da anitza
Egunon! Sabah El-Jer!
Bonjour! Bon día!

Hitza nahiz bizitza
hobe da anitza
mundua ederra da
kolorez jantzita"