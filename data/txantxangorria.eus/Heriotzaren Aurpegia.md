---
id: tx-2251
izenburua: Heriotzaren Aurpegia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/soou-XAHqb0
---

Heriotzaren aurpegia ikustean
Benetan kontutan hartzen dugu
Daukaguna.
Gupida gabe epaitzen duen Jaun
Ahaltsua gure patuan deituak gaude
Bere ondora.
Zure ordua heldu da,
Senideen malkoak, negarrak!
Arima gora doa, gorpua utzita zerura
Hildakoen erreinuan aingeru bihurtuko da.
Zeru goikoaren deia jasota
Argia dator gure barnera.
Bizi hobe batean guztiak elkartuko gara
Izuak aldentzeko kontatzen diguten kondaira.