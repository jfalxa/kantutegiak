---
id: tx-62
izenburua: Kuttuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/anPYtIfjJuI
---

Une hoietan, deitu izan zaitudanetan
Galdetzen diot nere buruari
Entzun ote nauzun sekula?
Bilatzen bazaitut behar zaitudanetan
Gau hotzetan
Bilatzen bazaitut nirekin pasako al duzu negua?
Kuttuna, ez nazazu aukeratu hurruna
Kuttuna, agur esan eta banoa urruna
Agertzen zara atean gautxoria bezala
Gutxien espero dudanetan
Zure begiak nireetan
Baina bazoaz ihesi eguna argitzen denean
Ta ni atzetik pausua jarrai ezinean.
Ta etxera bueltan, oinak neka neka
Nire masailetan zure usaina neukan
Ezin dut lokartu zure besoetan
Mesedez laztana, askatu gaitezan
Kuttuna, ez nazazu aukeratu hurruna
Kuttuna, agur esan eta banoa urruna