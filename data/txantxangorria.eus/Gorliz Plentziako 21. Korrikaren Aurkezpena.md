---
id: tx-960
izenburua: Gorliz Plentziako 21. Korrikaren Aurkezpena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-nznB3nWjkE
---

KLIKA KORRIKA 2019

Mad Muasel:

Heldu da guztion andereñoa
adi, adi, pasatuko dut zerrenda
je suis Mad Mua Sel ohlala!
Faty faty den soinua zabaltzen dut mundura

Guztiok bat egitea ezinbestekoa
bakoitzaren nortasuna errespetatzea
motorrak arrankatzeko momentuko aukera
gurea, herrikoa, KO RRI KA

Tipi tapa tipi tapa lortzen merezi duguna
martxan jartzen hasteko eginzazu KLIKA
ez dugu presarik helburua argi dago eta.
Pausuz pausu, gure erritmora!

Gure erritmora
nire, zure, gure erritmora
KLIKA KORRIKA KLIKA
GURE ERRITMORA 
KLIKA KORRIKA KLIKA
KORRIKA!

Jai giroa, bero beroa
Orain oraingoa gero gerokoa
Oreka ez galtzeko oinarri sendoa
Hauxe da gure leloa:
PO-PO- PO- SIBLEA DA KLIKA ALDATZEA
elkarren artean konektatzea
Pra- pra- praktikatzea
euskara gure artean bizitzea

Lan ta lan, banaka, taldean
Aukeratu zure lekua bidean
Herriaren arnasa sentitzen da
Korrika datorrenean

Zu gabe, zu gabe, hau ez da posible
orain gaur eta hemen dena da POSIBLE  

Gure erritmora
nire, zure, gure erritmora
KLIKA KORRIKA KLIKA
GURE ERRITMORA 
KLIKA KORRIKA KLIKA
KORRIKA!


Fermin:

Klika, klika! 
Altxa ezkerreko besoa
klika klika!
Orain altxa eskubikoa
Klika, klika!                  
Antenak ditugu piztuta
Klika, klika!                  
Gure sarea doa korrika

Klika, klika!
Euskal planeta ari da dantzan
Klika, klika!
Betikoak ari dira guri sermoika
Klika, klika!
Euskaraz egingo diet zirika
Klika, klika!
Ikimilikiliklik…a!!!

La Furia:

Suaren erritmora, hitzari bide eman 
arbasoen dantzak, kulturaren gorputzean
Kantauriko brisa, Barde(a)tako ziertzoa
Gure Borrokan tinko jarraitzeko hauspoa
Pista argituko duen metxa piztu dadila
Herri feminista eta askearen alde Klika

Lekukoa neskatxari emango dioten,
Amamek ahotsa altxatu dezatela ozen,
Aterpea sentitzeko, 
komunikatzen den herri bat ez da inoiz zatituko.

Erriberatik la Furia banator korrika
Batu gaitezen guztiok arma tiro Klika!


Gure erritmora
Nire, zure, gure erritmora
KLIKA KORRIKA KLIKA
GURE ERRITMORA 
KLIKA KORRIKA KLIKA
KORRIKA!