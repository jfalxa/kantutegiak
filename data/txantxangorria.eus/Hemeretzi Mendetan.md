---
id: tx-1208
izenburua: Hemeretzi Mendetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Xsp2AqfTG0g
---

Hemeretzi mende ta erdi du jadanik,

Jesukristo lurretik Zerurat iganik.

Bertsu berriak aurten kantatzen ditut nik

Euskal Herri maiteak gogorat emanik.

Aspaldian, gogoan, banuen desira:

Larrun mendi gainean aurten izan gira!

Xoraturik han nago, handikan begira,

Euskal Herrian toki ederrak baitira.

Ageri da Naparra eta Gipuzkoa,

Handik landa Lapurdi paregabekoa;

gero Baxe-Naparra eta Xiberoa,

urrunago deitzen dut gaineratekoa.

Itsasoan, txalupak dabiltza arrantzan.

Ainitz gizon lurrean ari laborantzan.

Mendietan ere ba, gau egun artzaintzan

Horra, Euskal-Herrian, zer gisaz gabiltzan.

Itsaso distiranta, eder bezen haundi,

ageri da hemendik urdin (eder) eta argi,

arbasoek (aitattoek) utzia guretzat aspaldi,

bozkariatu gabe ez litake geldi.

Grazia eske nago Ama Birjinari:

Jar zaite, Zerutikan, ororen gidari, 

emaguzu bakea mundu guziari,

zabal zazu Fedea Euskal Herriari.

Bertsu hauk eman ditut Lapurdi maitean,

Larrun mendi gainean den otelño batean.

Berritz etorriko naiz, lagunen artean,

Jainkoak plazer badu, heldu den urtean.