---
id: tx-6
izenburua: Astodunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BHQZlgtZF-Q
---

Arre, potxo, arre!
Arre, asto, arre!
Arre, maletxoria!
Arre, bada, arre!
Isso, isso, isso!
Hortik zek harua?
Beti baina okerretik
asto berenua!

Ni naz emakume bat
arrain saltzailia
marinelak itxasotik
dakarren guztia.
Nahi besigu, nahi lebatz,
atun, sardinia,
nahi txirla, nahi mielgia,
baita angilia.

Hamarrak jo artian,
izotz handi baten,
oinak bilosik nago
arrainak garbitzen.
Zesto artian gero
ondo plegatzia
astuan kargetako
daukat nik penia.

Gabaz ta egunaz nabil
neure astuagaz
kargia jausirikan
guztia galduaz.
Hargatik izaten ditut
fortunarik asko
portuan errial bat
aforan sei lauko.

Jagi zaite jentia
ia ordua dozu
zortzi legua eginda
atetan naukazu.
Hor dozu lebatz fresko
bart urtenikua
hemen nago hotzak hilik
bai ikustekua!