---
id: tx-533
izenburua: Eta Ni Kalean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xM5GxNHgtfg
---

Abesti hau "The Lio" diskaren parte da.
2021ko urtarrilean Ekaitz Iriarteren eskutik grabatu eta editatutako bideoklipa.
Abestia Iruñako Sound of Sirens estudioan grabatu eta masterizatua izan da Julen Urzaizen eskutik.




ETA NI KALEAN

Euriak nire aurpegiaren aurka talka egitean
Kaleak hutsik daudela
ikusten dut begirada altxatzean
bakardadea bidelagun dudala noa aurrera
ta ezin dut entzun egunero jasaten dudan
trafiko ta obra guztien zarata

Argiak itzaltzen doaz
Nire pausoekin batera
Ez dago jenderik eta ni kalean

Auzotik buelta norabidea arrats galduta
ta kaikuaren irribarrea
marraztuta nire aurpegi bustian
arrazoirik gabe jarraitzen dut aurrera kaleetan zehar
ez ditut ikusi ezta txakur puten argi urdinen disdirak

Itzali dira argiak
Eta itxi leiho guztiak
Ez dago jenderik eta ni kalean

Argiak itzali dira
Nire pausoekin batera
Ez dago jenderik eta ni kalean