---
id: tx-2645
izenburua: Zu Eta Ni
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cgTA_5YCNys
---

Ekialdetik haizea
zerua argituko duen
itxaropen betea.
Itsas-txoriak airean kantari.
Eta gu,
amildegi ertzean begira, kezkati.

Herri libre batean amestu genuen.
Ez hesirik;ez eta mugarririk;
ez debekurik.
Egunak aste, eta asteak urte,
bata bestearen babesean,
itsaso, olatu artean.

Euri hodeiak datoz mendebaldetik,
eta gu, ekaitzaren aldean,
nora ezean.
Mugaren hesi arantzadunen azpitik,
gerra eta goseetatik
ihesean.

Zu eta ni,
bizirauteko bidean bakarrik
Zu eta ni,
esperantzaren harian zintzilik.
Eta zuek gu,
amildegiko ertzera itzuliz.