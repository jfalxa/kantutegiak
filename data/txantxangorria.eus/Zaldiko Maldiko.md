---
id: tx-2949
izenburua: Zaldiko Maldiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QAET9w5gUqs
---

Zaldiko maldiko 
behera eta gora 
jirafa gainean
jira eta bira.

Pi, pi, pi! Pi, pi, pi! 
Auto txikian
Po, po, po! Po, po, po!
Kamioi handian.