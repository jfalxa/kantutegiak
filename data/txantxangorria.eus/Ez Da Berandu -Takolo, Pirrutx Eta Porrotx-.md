---
id: tx-3218
izenburua: Ez Da Berandu -Takolo, Pirrutx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7YEOszxv7H8
---

Ez, ez, ez,
ez da berandu
Olatuak
txalupa jo du
Haizeak lagunduko digu
iristea dugu helburu

Eutsi lemak
astindu eta
ura sentitu gero
Itsas lanbro
haize euritsu
Arrantzaleok
beti aurrera

Ez, ez, ez da berandu
Olatuak
txalupa jo du
Haizeak lagunduko digu
iristea dugu helburu

Sarea bota uretara
Itxaron eta jaso
sardinak dantzan dabiltza
arrantzaleok
beti aurrera

Ez, ez, ez da berandu
Olatuak
txalupa jo du
Haizeak lagunduko digu
iristea dugu helburu

Ez, ez, ez da berandu
Olatuak
txalupa jo du
Haizeak lagunduko digu
iristea dugu helburu
iristea dugu helburu
iristea dugu helburu