---
id: tx-3210
izenburua: Barcina Go Home
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i_ytBsnRe3Q
---

Zapaldutako gizaki
eskubideak bi zati
bihurtu nahi gaituzue
txanponen zenbaki
Ez da erraza agintzen
zuek beti ahalegintzen
lan gehien egin duzue
gure dirua zaintzen..
Hizkuntza bat harribitxi
hura hil nahia ze bitxi
hitza kendu diguzue
ta ahoa itxi

Bukaeran da bidaia
urteetako muntaia
herri honi heldu zaio
aldaketa garaia!
Nafarroak ez du bizi nahi
Eskuinaren hankapean
Herri libre izan nahi dugu
Utzi pakean !
Etxera !, go home !