---
id: tx-1242
izenburua: Miseriaren Inperioa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OV812jdrMiQ
---

Aitzakiaren aitzakiaz, egindako sarraskiak. 
Bortxaren monopolioaz, 
egi bakarraren eraikuntza. Indarraren indarraz, bakearen zaindariak, beste herri bat bonbardatu dute 
demokraziaren izenean. 

Miseriaren inperio, askatasunaren arerio 
Odolaren kolorea, kolorezko iraultzetan. 
Boterearen adorea, zuen moral bikoitzetan. 
Karitatez mozorrotutako elkartasun artifiziala aberatsen hegemonia babesteko tirokatzen diren balak. 
Lehen ere gutxi dutenen bizibideen konkista, 
zilegitzen dabil beren gezurraren telebista, 
LaSexta zein ETBren diskurtso supremazistan, 
demokrazi Washingtonen miseri inperialista. 
Baina bai, hala da, hori da normalena, terrorismoaz ari ta finantzatzearena, Sirian agertzen diren Europako armena, postureo hipokrita Je suis France-rena. 
Bereizten ote ditugu justua eta normala, 
zapaltzaile eta biktimen juzkua eta morala, 
munduaren kontakizun neokoloniala, 
egia denak berdinak balira bezala.