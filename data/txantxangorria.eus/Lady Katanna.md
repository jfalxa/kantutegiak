---
id: tx-261
izenburua: Lady Katanna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kzziTzt5eao
---

Sistema apurtzen da
Mugimendua deika
Martxan jarri gogor
Piztu da frekuentzia
Uhaina ixten da
Hirien gainean
Katana bi ditu
Mundua berriz hausteko

Mundua…..hausteko
Mundua…..hausteko
Mundua…..hausteko

Inperioa erortzen da
Matxinada deika
Dantzan jarri gogor
Jauzi da frekuentzia
Uhaina ixten da
Hirien gainean
Katana bi ditu
Mundua berriz hausteko.

Mundua…..hausteko
Mundua…..hausteko
Mundua…..hausteko
Izan zu killer 

Eman gogor beleei
zure dantza gustatu zait
Ez dadila sua itzali! 
Ez duzu bere zerua nahi.

Denbora agortzen ari zait
baina bidaia ez da hil.
orain ez dut gelditu nahi
beldurrak erori zaizkit

Gerra dantzalekuetan
Gerra bizitzarengatik
Bakea teilatuetan
Gerra neofaxismoari

Segi beti errepidean
eman suabe lagunei
gardeniak su artean 
gure bidaia ez da hil

Bidairik ez, askapenik ez duzu