---
id: tx-1455
izenburua: Lagun Abertzaleak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Pl0iO59SMnQ
---

Batzuk Jainkorik gabe, besteak fededun,
Denak alor berean uztarriko lagun,
Abertzale guztiek lan egin dezagun,
Euskal-Herria egin dezagun euskaldun.

Abetzaleak gara, ez dugu gordeko,
Zazpiak-Bat baitugu Herri bakartako.
Giderrak noiz dituen eskutan hartuko,
Horren alde gara gu beti zutituko.

Berma abertzaleak, Euskadiren alde:
Zuen borroka ez utz bozetan debalde!
Bozka, abertzaleak, Euskadiren alde:
Euskal-Herria ez utz laguntzarik gabe!

Euskal-Herria berriz egin behar dugu,
Haundikiak baitira hartaz baliatu.
Gure mintzaira dute beti mespretxatu,
Aitamen lurra laster arrotzari saldu.

Zer eskatzen ote du gaur Euskal-Herriak
Eta partikulazki euskal gazteriak?
Herrian bizitzeko lantegi berriak!
Nork ditu eraikiko, ez bada Herriak?

Berma abertzaleak, Euskadiren…

Lana Herrian eta Euskara plazara,
Erroietarik libre laborari-lurra,
Herriko gobernua Herriko-Etxera
Eta Euskadikoa guztien menera.

Langile, laborari, populu guztia,
Sozial asmu berri bat da gure nahia.
Hori buruz baturik gaitezen abia,
Tresnatako harturik gaur autonomia.

Berma abertzaleak, Euskadiren…