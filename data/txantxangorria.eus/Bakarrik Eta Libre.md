---
id: tx-522
izenburua: Bakarrik Eta Libre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SCq-JbrNFmo
---

Bakarrik eta libre · Antxon Sarasua · Miren Agur Meabe · Maite Mutuberria
Maitte-Maitte


Bakarrik eta libre badoa,
bakarrik eta libre gaztea,
poltsikoan eskuak, oinak arrastaka …
Luzea da bidea!!.

Bakarrik eta libre gauean.
Gauez argitzen zaio bidea;
argitzen zelaiak, zuhaitzak, mendiak …
Gauez badoa gaztea!

Bidean zergatik, berak ba ote daki?
Zertarako libre ta bakarrik?
Adi zak/n gaztea apurtu kateak!!
Jakingo duk/n bai bai zer ona zaigun bakea …

Kanta kantari badoa, bide guztien ildoan