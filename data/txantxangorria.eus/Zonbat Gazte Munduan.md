---
id: tx-882
izenburua: Zonbat Gazte Munduan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BCIWidp3GWg
---

ZONBAT GAZTE MUNDUAN

 

Zonbat gazte munduan

Beren biziko primaderan,

Estekatuak,

Preso daudenak

Basaki zanpatuak...

 

Oi. Oi. Oi!

Ta denak gizon onak!

 

Zuzentasuna gatik

Abiatu dira lehenik

Hoien kantua

Ez da goxoa

Ta guk naihago loa!

 

Oi... Oi... Oi...

 

................................

 

Hoien kantua

Ez da goxoa

Bainan guk naiago loa!