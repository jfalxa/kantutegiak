---
id: tx-1430
izenburua: Bother The Police
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QrZsBAwg6-g
---

BOTHER BOTHER BOTHER BOTHER BOTHER THE POLICE
Herri honetan ze hari zara
bueltako bideak ahaztu ala

eta molestatzen, molestatzen goaz jarraitzera
eta zu galdutako bidea elkarrekin aurkitzera
que haces chabal de uniforme con tu edad
o es que no queda ilusión para más
ten en cuenta no estamos aquí
para un rosario rezar
BOTHER BOTHER BOTHER BOTHER BOTHER THE POLICE
estableciendo la orden
antes de tu mente ordenar
y ya nadie sabe nadie sabe dónde acabarán los recuerdos de aquel tiempo en el que sabías cantar
ze adi mutil gorroto begiekin kalea gustora zapaldu ezin,
geuretan zabiltza nonetik ez duzu zereginik.
BOTHER BOTHER BOTHER BOTHER BOTHER THE POLICE