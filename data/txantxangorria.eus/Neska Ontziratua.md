---
id: tx-1567
izenburua: Neska Ontziratua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uNwgteZGpGg
---

Brodatzen ari nintzen,
ene salan jarririk;
Aire bat entzun nuen
itsasoko aldetik;
Itsasoko aldetik,
untzian kantaturik.

Jaun kapitaina, amak
igortzen nau zugana
Jin zaiten afaitera
eta (hantxe) deskantsatzera,
hantxe deskantsatzera,
salaren ikustera.



Andre gazte xarmanta
hoy ezin ditekeena
iphar haizea dugu
gau behar dut aintzina
ezin ilkia baitut
hauxe da ene pena.

Jaun kapitaina,
nora ekarri nauzu huna?
Zalu itzul nezazu
hartu nauzun lekura.
hartu nauzun lekura,
aita-amen gortera.

Andre gazte xarmanta
hoy ezin ditekeena
iphar haizea dugu
gau behar dut aintzina
ezin ilkia baitut
hauxe da ene pena.

Andre gazte xarmantak
hor hartzen du ezpata
bihotzetik sartzen ta
hila doa lurrera!...
Aldiz haren arima
hegaldaka zerura!