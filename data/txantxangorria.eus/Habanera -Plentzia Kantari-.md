---
id: tx-3238
izenburua: Habanera -Plentzia Kantari-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dKai-xEwjOo
---

Abestu nahi genioke
ozenki Antolintxuari,
erdaraz kutsaturikan
bizi den euskaldunari,
bilbainadak jakin arren
euskaraz ari denari,
Euskal Herria egiten
ari den zahar-gaztea/ri.
Ez pentsa gero dugunik
hau entzutea mirari
auzoak arnas berritzen
ari direla nabari
Euskal Kultura kalera
ateratzen aintzindari
horren lekuko garbia:
hemen da PLENTZIA KANTARI