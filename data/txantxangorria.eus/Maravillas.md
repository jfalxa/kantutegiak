---
id: tx-2974
izenburua: Maravillas
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sdxeczE9TWc
---

Ostikoz bota digute atea
zein dira deitu gabe datozenak?
Zer nahi dute aita, zuzenean zuri begira?
Zein dira oihuka mintzo direnak?

Norbaitek oroituko ditu etorkizunean
ahanzturaren zingira honetan gertatuak

Ikara; bortxaren bigarren izena
zein dira deitu gabe datozenak?
Gogoan dut orain aspaldiko aholkua:
ez fida herraren mirabe denaz

Norbaitek oroituko ditu etorkizunean
ahanzturaren zingira honetan ito dituztenak
abesti berriren batean
zuhaitz zaharren azaletan
Argaren zilar uretan
Iluntzeetan

Ostikoz bota digute atea
zein dira deitu gabe datozenak?
Agur ahizpak eta senitarteko guztiak
agur lagunak eta jolasten ginen tokia
adio ene Larraga maitea

Norbaitek oroituko ditu etorkizunean
ahanzturaren zingira honetan ito dituztenak
abesti berriren batean
zuhaitz zaharren azaletan
Erriberako izarretan
Nafarroan…

Maravillas Lamberto eta 1936ko gerran ahaztutako guztiei eskainia.