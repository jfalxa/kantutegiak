---
id: tx-3253
izenburua: Elurra Teilatuan - Gonzalez Bastida, José María
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oABC3hBOOyk
---

Elurra teilatuan
zakua lepoan
ibili beharko degu
aurtengo neguan.

Riau, riau, riau
riaukataplau...

hau dek umoria
utzi alde batera
euskaldun jendia.

Riau, riau, riau
riaukataplau...

hau dek umoria
utzi alde batera
euskaldun jendia.