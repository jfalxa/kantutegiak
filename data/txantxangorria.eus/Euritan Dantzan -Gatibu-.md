---
id: tx-1629
izenburua: Euritan Dantzan -Gatibu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vX-q6Q1GZ0Q
---

Euritan dantzan (2014) [Audio].
"Euritan Dantzan" diskotik ateratako abestia.

Bizirik nau
ta holan sentitzen naz
topera nau
geur inork ez nau geldittuko.
Egun goibel hau
egun argi bihurtu
nahi dot geurkoan.
Gogoa dekot eta
kale erdixen
ez dot inoren baimenik biher
dantzan hasten naz
ie...! Dantzan kalien
danak begire
dantzan kalien.

Eurixe da ah, ah, ah!
Eurixe da ah, ah, ah!

Zaparrada goitik behera
ta saltoka potxingoan
aske sentitzen naz ni
busti-bustitte
ta ur tantak goitik behera
Ustaritzen zein Gasteizen
Santurtzi zein Iruñean
bardin-bardin jauzten dire
kalien...
Euritan dantzan
kalien!

Udazkena
eta erropa zaharrak
zer gehixau bide
zoriontsu izeteko.
Joan etorri bet da gure bizixe,

ta une honek
hostixek dire, beraz
denbora txarrari
aurpegi ona jartzen badakit,
holan nabil ni
ie, ie!

Dantzan kalien
danak begire
dantzan kalien.

Eurixe da ah, ah!
Eurixe da ah, ah!

Ta saltoka...