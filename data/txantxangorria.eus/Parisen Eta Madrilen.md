---
id: tx-301
izenburua: Parisen Eta Madrilen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lANjyN9cHas
---

Parisen eta Madrilen direla bi gobernio
Elkualdun abertzalea biek hastio
Egina zuten tratua abertzalen lekutzera
Eskual-Herritik kanporat urrun botatzea

Zuzen kontrako tratua baitzuten biek finkatu
Eskual seme hoberenak zaizkote oldartu
Ez dute hartu puñalik ez eta ere bonbarik
Ez zuten harma beharrik goseaz besterik

Aserik dagon munduan hautatu dute gosea
Anaientzat ukaiteko libertadea
Euzkadiko elizetan justizia eskaleak
Anaientzat amodioz daude goseak

Gau luze luzearena orai badator azkena
Eskual anaien artean da maitasuna
Gaztek egiten badute elgarrekin batasuna
Orduan laster datorke azkatasuna.