---
id: tx-149
izenburua: Zer Da 32?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZyRNVOdijbM
---

90 beltzean,

ia-ia ilbeltzean,

etxera uda ekarri nuen

mundura sortzean.

Katuka zutik jartzean

lehen urtebetetzean

Parotek ere urte bat bete

zuen espetxean.

 

Joan nintzen ikastolara

lehenik ta behin Hendaiara,

bertan ze pozik jolasten nintzen

esaten zaila da,

ta amaitu zenean jada

nere haurtzaro bolada

Ionek kartzelan kunplitu zuen

lehen hamarkada.

 

Ikastola bat txiki da

joan naiz handitik handira

Gasteizko letren fakultatean

emanez segida,

alde egin dut urrutira

San Petersburgo hirira,

ta Ion kartzelan sartu zutela

hogei urte dira.

 

Zer da 32,

bizitza bat, bizitza bi

azkenerako krisketa kendu

zaio ateari,

Nahiz ta dena ez den argi,

ta zalantzaz naizen ari,

ea etxea senti dezaken

ilunpeko kabi.