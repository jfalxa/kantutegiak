---
id: tx-249
izenburua: Zeruarekin Amesten Dugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g_5mqcTKYpo
---

Modus Operandi taldearen "Zeruarekin Amestu Dugu" abesti berriaren bideoklip ofiziala.
Videoclip oficial de "Zeruarekin Amestu Dugu", la nueva canción de Modus Operandi.

Hitzak:

Behin baten, zoriontsuak izan ginen
Gure ametsak beteko zirela bazirudien
Eta bapatean, lapurtu ziguten melodia
Ilusioa, ilunabarra eta egunsentia

Zeruarekin amestu dugu, zerua berriz ikutu dugu
Itxaropenaren esklabu, gara bilakatu

Nahi ditugu, mila besarkada milaka muxu
Mila gau ero, milaka irribarre aurpegi biluzietan
Munduak berriro, har dezala arnasa astiro
Iragana gogoratuz etorkizunari begira

Kantuz kantu goaz aurrera, zuekin batera geroa idaztera goaz
Zeruarekin amestu dugu, zerua berriz ikutu dugu
Itxaropenaren esklabu, gara bilakatu