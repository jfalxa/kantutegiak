---
id: tx-1644
izenburua: Ezkilaren Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r84jYX8nPdk
---

Gure eliz'dorreko ezkila zaharrak
badauzka hedaturik berrien bakarrak.

Dilin, dalan, dalan
Din-dan boleran!

Ene aitatxi zuten noizbait bataiatu,
ezkil zaharra baitzen alegrantziatu.

Dilin, dalan, dalan
Din-dan boleran!

Sortuz geroz gizonak zor du maitatzea,
bai kartsuki jo zuen haren ezkontzea.

Dilin, dalan, dalan
Din-dan boleran!

Bainan ezkil zaharra zertako brandaka
etsai gaixtoa dator, lot, gizon armaka.

Dilin, dalan, dalan
Din-dan boleran!

Arrats hotz latzgarria, ezkil hetsitua,
aitatxi gorputz da-ta gerla da galdua.

Dilin, dalan, dalan
Din-dan boleran!

Bainan ezkila mintzo, xo dezagun adi,
haur ttipi bat sortu da, azkar bizi bedi!

Dilin, dalan, dalan
Din-dan boleran!

Kanta beza ezkilak menderen mendetan,
kanta beza bizia Eskual herrietan!

Dilin, dalan, dalan
Din-dan boleran!