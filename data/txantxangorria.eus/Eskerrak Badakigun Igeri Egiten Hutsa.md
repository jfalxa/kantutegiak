---
id: tx-3101
izenburua: Eskerrak Badakigun Igeri Egiten Hutsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pGrej7af1ZI
---

Ateak asmatu ziren
paretak zeharkatzeko
giltzak ateak ixteko
eta itsasoak,

A/te/ak as/ma/tu zi/ren
paretak zeharkatzeko
giltzak ateak ixteko
eta itsasoak,
giltzak haraxe botatzeko.

Es/ke/rrak ba/da/ki/gu i/ge/ri e/gi/ten.
Eskerrak badakigu igeri egiten.

Eskerrak badakigu igeri egiten.

Giltzak urpetik berreskuratzeko
ateak zabaldu
eta paretak zeharkatzeko
atzera begiratzen dugunean
itsasoaz baino ez gogoratzeko.

Eskerrak badakigu igeri egiten.

Eskerrak badakigu igeri egiten.

Eskerrak badakigu igeri egiten.

Eskerrak badakigu igeri egiten.

Eskerrak badaki/gu igeri egiten.

Eskerrak badakigu igeri egiten.

Eskerrak badakigu igeri egiten.