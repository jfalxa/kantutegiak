---
id: tx-2209
izenburua: Anek Idatzi Dit Zutaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ghaKE2JqEXM
---

Anek idatzi dit zutaz
Diño ondo zaudela
Oslon bizi zarela orain
Hara ezkondu zinela
San Antoniotan,
Polpolera bidean
Laino artean, bi amoradu
Maldan gora...
Jakin izan dut berandu
Ama izan zinen Maiatzan
Jon jarri diozu izena
Jende onaren izena
San Juan gaba zan, erre zenituen bekainak
Ezpan moreak, azken Waltz-a eta ardo
Ona...
Anek idatzi dit zutaz
Diño ez zarela aldatu
Beti lez oinak aidean
Kantatu eta dantzatu
Eta ez nau batere…