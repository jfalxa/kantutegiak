---
id: tx-2052
izenburua: Oi Gu Hemen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d7GdlVOhx7Q
---

Oi gu hemen bidean galduak (3fois) 

Heriotz minez.



Gizonen itzala luzea

Negarrez, haizea gauean 

Arbolak, erroak airean 

Heriotza da.



Refrain 



Pobreak kalean bilutsik 

Barnea esperantzik gabe

Doinua, basoko tristura 

Ilunabarrez.



Refrain 



Nundikan ezinaren mina 

Zergatik errekan iluna

Egurra, kolpeka landua 

Bide hustuak.



Refrain 



La la la la