---
id: tx-2752
izenburua: Zenbat Gera?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qwdotjoFODE
---

Zenbat gera?, lau, bat,
hiru, bost, zazpi.
Zer egin degu, ezerrez,
zer egiten degu, alkar jo
zer egingo degu, alkar hil.

Gure asmoak, esperantzak,
herria, askatasuna,
justizia, pakea,
egia, maitasuna,
mitoak, hitz hutsak.

Zenbat gera...

Hori ez, hori ez, hori ez,
Hori ez, hori ez, hori ez...