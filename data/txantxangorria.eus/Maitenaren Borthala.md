---
id: tx-129
izenburua: Maitenaren Borthala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y1ADJ_gS7RI
---

Maitenaren borthala 
Heltü ginenian maitenaren bortala, horak hasi zeizkün xanpaz berhala, 
Laster egin günian bertan gordatzera, erresiñula igan zühain batetara. 
Xori erresinüla, hots, emak eneki, maitenaren bortala biak alkarreki, 
Botz ezti batez izok deklara segreki, haren adixkide bat badela hireki. 
Nur da edo zer da? Maitenak leihoti, adixkideak gira, ziaude beheiti, 
Eta borta ideki emeki emeki, mintza ahal zitzadan ahalaz segreki. 
Nur da edo zer da? Nunko zirade zü? Etxe ondorik eztit, parka izadazü, Egarri handi batek hartürik niagozü, ütürri hun bat nun den, otoi, erradazü ! 
Zure egarriaz ez da miraküllü, egünko egünean bero egiten dü, 
Ütürri hun bat hortxe batüren beitüzü, zük galtatzen düzüna gure behar dügü! 
Hitzak: herrikoak 
Müsika: Jean ETCHART