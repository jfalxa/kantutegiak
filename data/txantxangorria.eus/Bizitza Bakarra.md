---
id: tx-3062
izenburua: Bizitza Bakarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pfKLMvu1zUI
---

Beldurrak gainditu nahi izateko
Bizitza bakarra
Itxaropenari eusteko
Itxaropen zakarra

Ametsetan sinisteko
Bizitza bakarra
oztopoz beteriko bidean
aurrera jarraitzearen beharra

Azken eguna heldu da
Burua garbitu barik
Zure adorea ta gogoak
Utzi zintuztenetik
Lotu gabe dituzun
hari-mutur guzti horiek
ilundu egin dute
zure kontzientzia

Azken segunduan
momenturik txarrenak
Dituzu irudikatzen.
Azken segunduan
momenturik txarrenak
Dituzu irudikatzen.

Patuari ezin zaio
aurka egin (x4)

Lotu gabe dituzun
hari-mutur guzti horiek
ilundu egin dute
zure kontzientzia.