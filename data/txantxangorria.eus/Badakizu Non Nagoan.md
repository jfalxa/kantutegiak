---
id: tx-80
izenburua: Badakizu Non Nagoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FgppFpUrfGY
---

Eskribatzen  badaustazu
ba dakizu non nagoan,
infernu labanagoan,
deabruaren ahoan.

Madarikatu gintuen
manera ezagunean
jaio ginen egunean
ta kantatzen dugunean.

Egarrie kenduteko
daudedaz mila edurte,
baso bete metal ur te
hogetabost pake urte.

Bohotzez maitatu biot
ene amatxo laztana,
titia emo ustana,
isiltzeko diostana.

Egia latzik kkantatzen
hemen ez naiz atrebitzen,
hortako ez dut zerbitzen
eta ditut esbribitzen.

Basurtun izan ziraden
eroriak alemanak
heriotzea emanak
hilerrira eramanak.

Bilbaon ba dagoz bandera
gorri eta beilegiak,
halan dinosku legiak,
garala epelegiak.

Bilbaon ibiltzeagatik
ez nuen galdu bemikan,
hil nintzlako Gernikan.

Eskribitzen badaustazu
ba dakizu non nagoan,
infernu labanagoan,
deabruaren ahoan.