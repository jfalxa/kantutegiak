---
id: tx-3170
izenburua: Nahiak Dastatuz -Sokaide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZKXP8L6FkrM
---

Jorratuz, sortuz, bilduz,
eraikiz, sinetsiz, haziz


Irudietan bilatu
Akatsak baztertuz
Ez jausiz altxatu.
Begirada goratu
Urrunean pausatu
Margoztuz, hausnartuz.
Suntsituz, akatuz
Oinetakoak erantsiz
Saltoz ur gainean
Bustiz sakonean
Zapuztuz, itoko zaituena.

eta ARNASTU BIZI USAINA
ETA DASTATU ZURE NAHIA
EZ PENTSA DENIK HORREN ZAILA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

ARNASTU BIZI USAINA
ETA DASTATU ZURE NAHIA
EZ PENTSA DENIK HORREN ZAILA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

Hobetuz, moldatuz,
gauzatuz, hartuz.

Ja/soz, e/gi/a bi/la/ka/tuz
Hustu barrutik hegaz aske biziz
Indartsu oihukatu
Oroitu baina aurrera begiratu.

e/ta AR/NAS/TU BI/ZI U/SAI/NA
ETA DASTATU ZURE NAHIA
EZ PENTSA DENIK HORREN ZAILA
SENTITZEA MOMENTUZ BIDAIA

AR/NAS/TU BI/ZI U/SAI/NA
E/TA DAS/TA/TU ZU/RE NA/HIA
EZ PEN/TSA DE/NIK HO/RREN ZA/I/LA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

AR/NAS/TU BI/ZI U/SAI/NA
E/TA DAS/TA/TU ZU/RE NA/HIA
EZ PEN/TSA DE/NIK HO/RREN ZA/I/LA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

AR/NAS/TU BI/ZI U/SAI/NA
E/TA DAS/TA/TU ZU/RE NA/HIA
EZ PEN/TSA DE/NIK HO/RREN ZA/I/LA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

AR/NAS/TU BI/ZI U/SAI/NA
E/TA DAS/TA/TU ZU/RE NA/HIA
EZ PEN/TSA DE/NIK HO/RREN ZA/I/LA
SEN/TI/TZE/A MO/MEN/TUZ BI/DA/IA

Jo/rra/tuz, sor/tuz, bil/duz,
e/ra/i/kiz, si/ne/tsiz, ha/ziz


SEN/TI/TU