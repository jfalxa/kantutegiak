---
id: tx-886
izenburua: Memoriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kuLH0IuykLQ
---

Olerkiaren hariak txiratu
ahal bezain bat moldatu
hitz bereziak ta notak elkartu
aire berri bat sortu

Gure arbasoen aztarnak segituz
haien hitzak ahozkatuz
noten zinezko kolorea onartuz
haien guduak aipatuz
Horrela iraun dezake 
gure herriko memoriak

Esaldien erritmoa kontatu
hitzen erranahiak ulertu
pensamolde ezberdinak aztertu
ideia berri bat sortu

Gure arbasoen aztarnak segituz
haien hitzak ahozkatuz
noten zinezko kolorea onartuz
haien guduak aipatuz
Horrela iraun dezake 
gure herriko memoriak

Gure arbasoen aztarnak segituz
haien hitzak gogoratuz
gizaldi honen atea zabaldu
mezulari bilakatuz

Horrela iraun dezake 
gure herriko nortasunak