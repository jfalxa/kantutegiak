---
id: tx-709
izenburua: Atarratzeko Gazteluko Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bqVCqoYrP1A
---

Ozaze Jaurgainian bi zitroin doratu
Atarratzeko Jaunak bata du galdatu;
Uken du arrapostu eztirela huntu,
Hunturik direnian batto ukenen du.
Portaliala juan zite, ahizpa maitia,
Ingoiti horra duzu Atarratzeko Jauna,
Otoi erran izozu ni eri nizala,
Zazpi egun hoietan oihan nizala.
Klara, zuaza orai salako leihora,
Ipar-ala egua denez jakitera,
Iparra baldin bada goraintzi Salari,
Ene korpitzaren txerka jin dadila sarri.
Ama, juanen gira oro elkarrekin,
Etxerat jinen zira xangri handireki,
Bihotza kargaturik, begiak bustirik,
Eta zure alaba tunban ehortzirik.
Ama, saldu nauzu bige bat bezala,
Bai eta desterratu, oi Españiala,
Aita bizi uken banu, ama zu bezala
Enundunun ezkunduren Atarratzeko salala.
Atharratze jauregian bi zitroin doratü
Ongriako Erregek batto dü galdatü
Arrapostü üken dü eztirela ontü
Ontü direnean batto ükhenen du.
Atharratzeko hiria, hiri ordoki
Hur handi bat badizü alde bateti
Erregeren bidea erdi-erditik
Maria Maidalena beste aldeti.
Aita saldü naizü idi bat bezala
Ama bizi üken banü, aita zu bezala
Enündüzün ez jaunen Ongrian behera
Bena bai ezkuntüren Atharratze Salala.
Ahizpa juan zite portaliala
Ingoiti horra düzü Ongriako erregia
Hari erranizozü ni eri nizala
Zazpi urte hontan ohian nizala.
Ahizpa, enükezü ez sinhetsia
Zazpi urte hontan ohian zirela
Zazpi urte hontan ohian zirela
Bera nahi dükezü jin zü zien leküla.
Ahizpa jaunts ezazü arruba xuria
Nik ere jauntsiaren dit ene berria
Ingoiti horra düzü Ongriako erregia
Botzik kita ezazü zure sor etxea.
Aita zü izan zira ene saltzaile
Anaie gehiena dihariren harzale
Anaie arteko zamariz igaraile
Anaie ttipiena ene kontsolazale.
Aita, juanen gira oro alkareki
Etxerat jinen zira xangri handireki
Bihotza kargaturik begiak bustirik
Eta zure alhaba tunban ehortzirik.
Ahizpa zuaza orai Salako leihora
Iparra ala hegoa denez jakitera
Iparra balin bada garaintzi Salari
Ene gorpitzaren txerka jin dadila sarri.
Atharratzeko zenüak berak arrapikatzen
Hanko jente gaztea beltzez da beztitzen
Andere Santa Klara hantik partitzen
Haren peko zamaria ürthez da zelatzen.