---
id: tx-1857
izenburua: Ikusi Mendizaleak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/x-fGWrWeC5c
---

Ikusi mendizaleak
baso eta zelaiak,
mendi tontor gainera
igon behar dogu.
Ez nekeak, ezta bide txarrak
gora, gora Euskalerria.
Gu euskaldunak gara
Euskalerrikoak.

Hemen mendi tontorrean
euskal lurren artean
begiak zabaldurik
bihotza erreta.
Hain ederra, hain polita da ta,
gora, gora Euskalerria.
Gu euskaldunak gara
Euskalerrikoak.