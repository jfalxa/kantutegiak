---
id: tx-1589
izenburua: Katalintxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/G3e1f_hDGAs
---

Katalintxu lora ederra,
zeu krabelina fina,
antzina egin jatan
zeuganako mina.
Zeuganako min hori
pentsatu ezkero
ezin dot lorik egin
ametsetan baino.

Katalintxu lora ederra,
zeu krabelina fina,
antzina egin jatan
zeuganako mina.

Amuak bota dituz
ura barrenean
arrakaiak oisteko
eria danian.
Arrakaia juan da
Bilboko hurira
German geratu da
zerura begira.

Katalintxu lora ederra,
zeu krabelina fina,
antzina egin jatan
zeuganako mina.

Katalintxu lora ederra,
zeu krabelina fina,
antzina egin jatan
zeuganako mina.

Zeuganako min hori
pentsatu ezkero
ezin dot lorik egin
ametsetan baino.

Katalintxu lora ederra,
zeu krabelina fina,
antzina egin jatan
zeuganako mina.