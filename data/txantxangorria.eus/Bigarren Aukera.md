---
id: tx-3065
izenburua: Bigarren Aukera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eOOkvgJVw34
---

Hain gaztea izanik
Hain inozoa izanik
Azal ilunagoa, egunero esklabu gisa lan egiten duzu
Gainera zor dituzula eskerrak

Fideltasunean, zintzotasunean, ez daukazu parekorik

Hala ere zuk ez duzu amorrurik gorde
Lagunik minena daukazu jabe

Gogora ezazu hegaldatu zenuen kometa hori
Kabuleko zeru urdina estali zuena
Erronka nagusienak askatzen zaituztela diote
Bizitzak bigarren aukera bat ematen duela

Hala ere zuk ez duzu amorrurik gorde
Lagunik minena daukazu jabe

Gogora ezazu hegaldatu zenuen kometa hori
Kabuleko zeru urdina estali zuena
Gogora ezazu zugatik mila aldiz egin nuela
Ta beste mila aldiz egingo nukeela
Bizitzak bigarren aukera bat 
Ematen duela diote kalean.