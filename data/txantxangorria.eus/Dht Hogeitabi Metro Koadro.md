---
id: tx-1592
izenburua: Dht Hogeitabi Metro Koadro
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D5C4S38uwgY
---

Kalean pilatuta zeuden harrietatik
lagun artean amets berri bat eraikiz
Deustuko ezkerreko espaloian
bai, goiko aldean.

Gure eskuekin
zue eskuetara
ateak irekiz
berriz gara

Izan ginenetik
orain garenera
auzotik auzora
zugana!

Hogeitabost urtez. 
Hamaika amets
elkartasuna adieraziz
pauso handiz eta txikiz
Hogeitabost amets
Hamaika urtez
atzokoa landuz
etorkizuna eraikiz.

Bizitari makurrak,
argazki lapurrak
solasaldi amaigabeak
futbolin mundialak
sugearen begiek
ikusi dute dena

Ongi etorriak, herri bazkariak
momentu onenak izan dira

Eta aurreko patioan
milaka aldarrikapen
idatzi dira margotu dira

Hogeitabost urte. 
Hamaika amets.

Eta izan zarenak
dena eman zutenak
deserriko mina
bagoaz aurrera
urruneko hitzak
gure paretetan
elkarrekin gaude
borroka honetan
gurekin izan arte.

Amaiur taldean urtetan aritu ondoren, 22M2 (Hobeita Bi Metro Koadro) izeneko hau Naroa Gaintzaren proiektu berria dugu. 2014ko negua eta udaberria bitartean Asier Erzilla
teknikariarekin grabatu da Bilbon, eta, besteak beste, David Nanclares musikaria, David Gorospe bateria jolea, Mandoilek taldeko Igon Olaguenagak ahotsean, eta Eunate Vilchesek biolinean, izan
ditu lagun. Bakarkako disko bat izan arren, rock and roll banda baten kontzeptuari heldu dio. Elementu gutxirekin bada ere, rock talde on batek asko eman dezakeela erakusten digu. Bere