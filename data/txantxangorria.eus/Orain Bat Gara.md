---
id: tx-472
izenburua: Orain Bat Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dShhXs6jYHk
---

Behatzak hotz, leihoa bero. 
Gaur inoiz ez da gero. 
Bi gara: Zutabe bat, bi erro. 
Lau begi, lau esku, bi lepo. 

Bihar kantatu ezin, 
gaur kantuan nauzu: zara, naiz, gara. 
Gainetik soilik izarak. 
Gaur kantuan nauzu: orain bat gara. 

Giltzapean bildu duzu ahotsa. 
Inork ez gaitu ezagutzen. 
Negua da eta ez du hotzik egiten. 
Leiho ireki bat zalantza guztiak argitzen. 



Musika: Ibil bedi
Hitzak: Ibai Osinaga
Grabaketa eta nahasketak: Ibai Osinaga 
Masterizazioa: Josu Erviti (DrumGrooveStudio)
Diseinua: Mikel Tristan

ibil bedi BELTXARGA BELTZA, 2021

ibilbedi@gmail.com