---
id: tx-1757
izenburua: Txapeldunaren Agurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wfTUsFbCGn4
---

Zerutik begira daude
Joxe eta Manuel Mari,
ziega zulotik entzuten 
beste euskaldun ugari.
Muxu bat iparraldeko
bertso eskolako haurrari,
lagundu nauten guztiak
Olatz eta arreba bi.
Baina ni ez bainaiz hemen
neuk sortu hutsean ari
aitarena da txapel hau
eskeintzen diot amari
haurra nintzela euskaraz
erakutsi zidanari.