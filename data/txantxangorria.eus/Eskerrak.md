---
id: tx-1996
izenburua: Eskerrak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3D1-FVaU0W8
---

Izarrei, gas eta hautsei,
atmosferari, atomoei
hodeiei, euriari, natura amari,
jaiotzari.

Es/ke/rrak:
idatzi gabe/ko historiari,
idatzitakoari, garenari,
ekartzen gaituen iraganari,
tak/tu/a ez bai/ta soi/lik,
gu/re lo/ka/rri.

Lurrari, zelulei,
eboluzioari, garunari,
uhinei, bihotzei, aniztasunari,
sentimenduei.

Es/ke/rrak:
i/da/tzi ga/be/ko his/to/ri/a/ri,
i/da/tzi/ta/ko/a/ri, ga/re/na/ri,
e/kar/tzen gai/tu/en i/ra/ga/na/ri,
tak/tu/a ez bai/ta soi/lik,
gu/re lo/ka/rri.

E/ta e/san, zer da gu/re/a?
Zer/ta/ra hel/du ga/ren
i/zen/pe/tu/a, mun/du/a.
E/za/gu/tza: haus/nar/tze/rik ba/da den
bar/ne/ko/a den, gor/pu/tza.