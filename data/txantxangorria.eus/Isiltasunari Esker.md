---
id: tx-144
izenburua: Isiltasunari Esker
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5RFkMKfERpU
---

ISILTASUNARI ESKER 
(ISILTASUNAREN SOINUA)
(The Sound of Silence-Simon & Garkunkel)-Aire Ahizpak

1. Agur lagun iluna
jina nizu mintzatzera
Amets bat jin zautat gogora haziak utzi ditu ene loan amets hori ene baitan egonen da betiko
isiltasunagatik.
2. Ametsetan ibili naiz
bide argiztatuetan
argi baten azpian geldituz umeltasun ta hotza sendituz dirdira batek begiak gogorki zauztan ilunpean
isiltasuna hunki.
3. Argiak ikusi nituen
hamar mil jenden bederen alegia eleketan nabil
kasu egin gabe erranez
hitzak sekulan ere kantatu gabe hortako
isiltasun harengatik.
4. Xangrea lotu bezala 
isiltasuna hedatzen da
entzun otoi denek ene hitzak 
besoak altxatuz zier buruz 
baina hitzak urtzen dira segidan ixuriz
isiltasunaren baitan.
5. Otoitz ta belaunikatuz 
Jinkoa galde iten zuten 
distira baten eran (a)gertu zen 
izkiriatu hitzak erakutsiz 
Hor ziotela profetaren 
erranak apalik hedatuak
ia denetan
isiltasunari esker, 
isiltasunari esker.