---
id: tx-3271
izenburua: Gazte Danbada -Iheskide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o8v4WhyAiPs
---

Ahots nagusia eta kitarra: Xagu (Iheskide)
Bigarren ahotsa: Nerea (Keike)
Baxua eta koroak: Tronpi (Iheskide)
Kitarra eta koroak: Kata (Iheskide)
Bateria: Petra (Iheskide)
 
Gudarako irrintziak bezala haize berriekin zabalduz,
menditik datorren adar hotsak gazteok biltzera deitzen gaitu.
Iraganetik ikasia, oraina bizi eta lantzen dugu.
Pausoz pauso irabazten gabiltzan biharko garaipena helburu!
Norbanakotik hasita, gazte mugimendu anitza,
norberaren baitan aurkitzen baita benetako iraultzaren giltza.
Elkarrekin landu, elkarrekin sortu, elkarrekin amestu,
zurrunbilo bilakatu!
Izan gazte, izan aske, izan zaitez burujabe.
Ez zaitez jauzi putzuan, garun lokartuen munduan.
Taupadaren ordua heldu da.
Badator jada, gazte danbada
Ukabil itxiaren indarra, gazte boterearen islada
Gazte danbada, badator jada
Kateak hautsi, etorkizuna irauli
Hamarkadetan zehar kaleak astintzen,
iluntasuna argituz kolpatuak izan arren.
Askatasunerantz gabiltza, lan, ekin eta bultza
Gazteok daukagu hitza, borroka eta antolakuntza!
Ekiozu etengabe, izan zaitez errebelde.
Alternatibak eraikiz, haika neska eta mutil.
Taupadaren ordua heldu da.
Badator jada, gazte danbada...