---
id: tx-3281
izenburua: Goizuetan -Mikel Laboa-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vrPOANqYA8w
---

Goizuetan bada gizon bat
deitzen zaio "trabuko"
itzak ederrak bihotza paltso
sekula etzaio faltako
egin dituen dilijentziak
berari zaizko damuko.
Ondo oroitu adi
zer egin uan Elaman.
difuntu horrek izandu balu
aidekorikan Lesakan
orain baino len egongo itzen
ni orain nagoen atakan.
Nere andreak ekarri zuen
Aranaztikan dotea,
obe zukean ikusi ez balu
Berdabioko atea,
orain etzuen hark edukiko
dadukan pesadunbrea.
Nere buruaz ez naiz oroitzen
zeren ez naizen bakarra
azitzekoak or uzten ditut
bi seme ta iru alaba
Jaun zerukoak adi dezala
oien amaren negarra.