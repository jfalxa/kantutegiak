---
id: tx-1696
izenburua: Nevada -Kherau-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7_r_wnGf08M
---

Larreko enbata zuriak
Bakardadean euriak
Gozo naroa zeugana Nevadako ilargiak

 Ardi galdu etorkina
bertokoentzako zikina
burua galduko neuke, akorduan ez bazina

 AI OI ENE, besotan zintudan alaba maite
AI OI ENE, Aitaren alaba, neguko lore

 Hiru metroko edurra
Ta urratuta zintzurra
Askok iruntsi genduan bizi barrian guzurra

 Aiztokadak bihotzean
Ta makalen azalean
Helduko ez jatzun gutuna lotu zuztarren artean

 6 urtez behar eskasa
artaldearen anabasa
etxea iztea honen truke e'jatan egin erraza

 zeu munduratu orduko
sehaska ondoari uko
barriro alkar ikustean ez zaitut ezagutuko