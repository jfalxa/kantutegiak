---
id: tx-2993
izenburua: Borrokalari Kalera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OUs8LOVDso4
---

Altxa begiak, zabaldu orok 
deiadarra lagunari 
berriro izanen gaituk libre 
indar emanaz iraultzari. 

Kalera, kalera, borrokalari kalera, 
kalera, kalera, borrokalari kalera, 
hire indarraren beharra diagu 
gure indarrarekin batera 
hire indarraren beharra diagu 
gure indarrarekin batera. 

Zai dago ama, zai aita 
zai andre ta lagunak 
hator, hator Euskadira, 
hator, hator etxera. 

Zai dago ama, zai aita 
zai andre ta lagunak 
hator, hator Euskadira, 
hator, hator etxera. 

Bultza ta bultza, euskal langile, 
Euskal Herri sufritua 
burni kateak geldi labetan 
danba danba, lurrera gartzelak. 

Kalera, kalera, borrokalari kalera, 
kalera, kalera, borrokalari kalera, 
hire indarraren beharra diagu 
gure indarrarekin batera 
hire indarraren beharra diagu 
gure indarrarekin batera. 

Zai dago ama, zai aita 
zai andre ta lagunak 
hator, hator Euskadira, 
hator, hator etxera. 

Zai dago ama, zai aita 
zai andre ta lagunak 
hator, hator Euskadira, 
hator, hator etxera.