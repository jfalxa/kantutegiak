---
id: tx-226
izenburua: Bakarrik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i6ku_mzIwZc
---

Zuzendaritza eta argazki zuzendaritza: @jonnarvaezz
Acting: @ametslarrea
Ekoizlea: @mon.dvy
Nahasketa eta mastering: @01erebo
Zuzendari laguntzailea eta ekoizpen burua: @dwightt_garrett
Ekoizpen laguntzailea: @varobre
Gaffer: @borjaherran
Kamera laguntzailea: @internshotz
Bestelako argazkiak: @eriksuso

-------------

Bakarrik a,a
bakarrik e,e
hemendikan bakarrik nozelako
(x2)

Goizeko zortziyak ta ni hemen
portalin bakarrik zure deia itxoiten
beste egun bat nozela zu gabe
egun argi edo grixei bakarrik abesten
(x2)

Bakarrik a,a
bakarrik e,e
hemendikan bakarrik nozelako
(x2)

Esan ezetz, norbaitekin,
nozenian ta ez bakarrik
ta esan zeba
hausten den dena segundu baten
sayak zeba e

Ta biyek, san genula ezetz
hau ez dela betirako bidia (x2)

Goizeko zortziyak ta ni hemen
portalin bakarrik zure deia itxoiten
beste egun bat nozela zugabe
egun argi edo grixei bakarrik abesten

Bakarrik a,a
bakarrik e,e
hemendikan bakarrik nozelako
(x2)

Ta zure deia mobilan
ta ni hartu gabe
zalantzak burun
ta zuregatik dena beltza
milaka zalantza ta ni hemen zu gabe

Bakarrik a,a
bakarrik e,e
hemendikan bakarrik nozelako
(x2)