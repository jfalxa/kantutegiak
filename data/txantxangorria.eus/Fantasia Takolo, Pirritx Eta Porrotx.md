---
id: tx-3228
izenburua: Fantasia Takolo, Pirritx Eta Porrotx
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dI9uHcNkxfE
---

Iepa! festa orain hasi da
istorio askoren habia.
Ireki ba galaxia
planeta handia da fantasia.

Basoan nengoen arazo askorekin
Robin Hood nintzen nire arkuarekin
galtzak urrundu
pobreei lagundu
mundu justu bat ahalegindu.

Arratoiak ziren nagusi kalean
hantxe ni Hamelingo txirularia
doinu alaia
dantza garaia
mendira hurbiltzen animaliak