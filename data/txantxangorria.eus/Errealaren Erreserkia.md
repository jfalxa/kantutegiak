---
id: tx-1859
izenburua: Errealaren Erreserkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_kIwNNafWn4
---

Txuri-urdin
Txuri-urdin maitea
Txuri-urdin
Txuri-urdin Aurrera
Beti, Beti maite
maite, maitea
Donostia, donostiarra
(bis)
Aurrera mutilak!
Aurrera Gipuzkoa!
Aurrera txuri-urdinak!
(bis)
Gazte, gazte, gazte,
gazte, gaztedi,
Gaztedi aupa!
Aupa mutilak!
Gazte, gazte, gazte,
gazte, gaztedi,
Gaztedi txuri-urdinak!
(bis)