---
id: tx-2275
izenburua: Eintzak Karaokie Honekin Ga2
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7ohd5y-YENc
---

Lan gogorra hirea
ordenagailuan
argazki eta musikaz

Influencer euskaldun
eta jubilatua
guk jarraitzen diagu
Txantxangorria da.

Xabier Lete edo
Kaleko Urdangak
Anje Duhalde eta
Kupela Taldea.

Gabi de la Maza
ez haiz aspertzen ala?
Gabi de la Maza, bai!

Gabi de la Maza
ez haiz aspertzen ala?
Gabi de la Maza,
Hiri berdin zaik dana!

Bideo batekin hasi
eta gero bestera
Txantxangorriarekin
egin ezak gaupasa.