---
id: tx-1598
izenburua: Indar Bat Dabil Hor Mikel Markez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VSj1AwTtl5I
---

Bizi-legeak dio

argi bezain idor

gaztea hilkor dela

zaharra derrigor. 

Herri batez, ordea,

zergatik ez aitor

hain zahar dela zeren

sortu zen ez-hilkor?

Ba, hala izan dadin 

indar bat dabil hor.

Talde, xede, mugida

han tira, hemen bultz!

Zenbaitek uzten duen

Arima ta gorputz!

Su hori dena ezin

bihur liteke hauts

begiratzen badugu

heburu bereruntz

gailurra bertan dago

ezin da egin huts.

Gesalak haitzen kontra

haustean hartzen min

aparrak eman dezan

oinetan atsegin.

Gure erronka ez da 

hil bat piztu dedin

baizik bizi den zerbait

biziago egin

sentituz geure eta 

ondorengoen duin.

Gure aditz gutarrak:

gara, ginen, gaude.

Itsasoa bizi da

lorik egin gabe.

Gauden zu ta ni, denak,

olatuen alde!

Argia islatzen da

malkoetan ere

garaipen bat ospatuz

bada hainbat hobe!