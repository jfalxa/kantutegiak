---
id: tx-2937
izenburua: Xanti Eta Maribi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NKSuiRnZW9k
---

Bat eta bi, Xanti eta Maribi,
korrika eta korrika, katuaren ihesi,
korrika dihoazela zakur beltza ikusi,
putzura erori eta ipurdia busti,
amona garrasika etxeko lehiotik.
Bat, bi, hiru eta lau.