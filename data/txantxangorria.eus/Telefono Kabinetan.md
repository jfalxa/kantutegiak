---
id: tx-2627
izenburua: Telefono Kabinetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/02ZXAN9c0fQ
---

Urteak pasatu dira denbora ez horrenbeste,
hitz haiek galdu al ziren, edo nunbait aurkitu daitezke,
ez uste, berriz entzun nahi nituzke,
hain txoro, hain hauskor, eta hain aske.
 
Delitugile txikiak lez hitz sekretuak ezpainetan,
bihotza txingurrituta, bizia zijoala bertan,
genbiltzan nerabetan,
telefono kabinetan,
delitugile txikiak lez gaueko ordu txikietan.
 
Telefono kabinarik jada, ez dago karrika ertz hartan
ta hango hitzen ikara, askotan botatzen dut faltan,
esanez, betiko ta sekula santan
mundua erori arte lau puskatan.
 
Delitugile txikiak lez hitz sekretuak ezpainetan,
bihotza txingurrituta, bizia zijoala bertan,
genbiltzan nerabetan,
telefono kabinetan,
delitugile txikiak lez gaueko ordu txikietan.
 
Delitugile txikiak lez hitz sekretuak ezpainetan,
bihotza txingurrituta, bizia zijoala bertan,
genbiltzan nerabetan,
telefono kabinetan,
delitugile txikiak lez gaueko ordu txikietan.