---
id: tx-2782
izenburua: Bale Ihizi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TQkmaTCiRL0
---

Ni itsas xoria banitz
Iparreko izotz lurretarat
Joan nintaike hegaldaka
Bale itsu gizenen ondotik

Itsasoa harro dago
Haizeak oro tzarrean
Eta gu ezin kontsola
Etxe goxoa gogoan harturik

Zoin asto izan behar den
Hala guziz miserian bizi
Untzirat iragaiteko
Bale bakartiaren hilzaile

Sos poxika baten gatik
Zonbat arriskurik ez den
Ur untzia uzkeilirik
Buztan hasarretuak leherturik

Lana gustokoa zaio
Arrantzale gai den gazteari
Bainan ikaretan dago
Bale basa hurbiltzen denean

Lot lanari, erna gaiten
Dio kapitaina jaunak
Marinela eri dago
Gorputz animak errotik daldaran

Borroka hasten denean
Uhain zoroak jauzian gora
Aberea zaurituta
Odol basa zurrutan dario

Oligo hexur paflea
Sos guttirekin sanitzat
Moltsa arin itzultzen da
Gaztea Ziburuko tabernarat.