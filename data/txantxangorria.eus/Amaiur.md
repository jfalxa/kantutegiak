---
id: tx-2217
izenburua: Amaiur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GS9bYl3zpwg
---

Amaiur,
askatasunaren ikur
gauden ziur
gazte eta zaharrak
ezjakin eta zuhur.

Amaiur,
ezkutaturik gelditua da harri zaharren tartean,
mendean zehar txit gorderikan goroldioen azpian.
Bainan izanen da denbora bat izango ahal da agian,
guztiok zutik lortuko dugu agertu oihu bizian:
Euskal Herriko askatasuna Amaiurko gazteluan!

Nafarroako independentzia Baztango herri aldean
jokatu zuen azken borroka hamaseigarren mendean
borroka harek badu segida jarraipena gure artean
Xabierko zaldun eta lagunek idatzi zuten ekinean:
Euskal Herriko askatasuna Amaiurko gazteluan!
Euskal Herriko askatasuna Amaiurko gazteluan!