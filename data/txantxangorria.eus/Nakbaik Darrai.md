---
id: tx-1747
izenburua: Nakbaik Darrai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D1HbwojnZMQ
---

Letra: Pruluns
Musika: Eneko eta Iñaki


Hiltzea denborapasa
Tiranoaren jolasa

Norberak sufritu duen hura
Eiteko kapaza.
Misil eta bonba jasa
Suaren kezko arnasa
Eguzkia itzaldu denean
Argitu da Gaza.

Zapalkuntzaren "gurua"
Zapalkuntza den gudua
Estatu Batuen babesa ta
Europa mutua.
Zurituz bere burua
Hamas omen helburua
Tiropitxoira bonbak botata
Sari segurua.

Okupazio grabea
Arazo honen klabea
Nazien eta hauen artean
Non dago aldea.
Intifadaren idea
Askapenaren bidea
Harri koxkorrak tankeen aurka
Ezin du ordea.

O.N.U.-ren ahalegina
"Hoja de ruta"" zikina
kendu ordez kiratsa estali
Eiten dun lurrina
Ekialdeko samina
Herri zapalduaren mina
Ez dagokien
hura lortzeko
Sionisten grina.

Genoziodioa dohai
Ankerkeriarako gai
Guraso, seme, alaba, anai
Denak ote etsai.
Agintari Israeldarrai
Amorruak gora darrai
Hiltzen zareten gauean behintzat
Eingo det lo lasai