---
id: tx-2709
izenburua: Ari Naizela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iji9v4jEkl8
---

Pello Errota:

Ari naizela, ari naizela,
hor ikusten det Txirrita.
Eta zein ez da harrituko gaur
gizon hori ikusita?
Dudarik gabe egina dio
andregaiari bisita.
Oso dotore etorri zaigu
bi alkandora jantzita.

Txirrita:

Hauxe da lotsa eman didana
gizon artera sartuta!
Edozer gauza esaten degu
edanarekin poztuta.
Bi alkandora ekarri ditut,
bat eranztea ahaztuta...
Pellok bi nola jantziko ditu
bat besterikan ez du ta?