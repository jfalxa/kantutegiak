---
id: tx-2955
izenburua: "@Txik Ikastol@"
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9_clvaaSVU0
---

Senpereko aintziran, aitzindariak aspaldian,
Sarea ehuten hasi ziren ametsen artean.
Urratsez urrats aintzinatuz, hitz berri ainitzi egokituz,
Ta begiradak apurka eztituz… , gauzatzen doaz…
@txik ikastol@ (x4)

HERRI URRATS // primantza bat baino gehio
HERRI URRATS // euskara prest gerorako
HERRI URRATS // gaur egungoa delako
@txik ikastol@

Makilak guritzen dira baina, legeak harri bihurtzen.
Maltzurkeria ere ari da, arauz modernizatzen…
Nahi gaituzte atzokoari lotu, eta iluntasunean gakotu,
Nahi digute argindarra moztu, gure erantzuna :
@xik ikastol@

HERRI URRATS// primantza bat baino gehio
HERRI URRATS// euskara prest gerorako
HERRI URRATS// gaur egungoa delako
@xik ikastol@

HERRI URRATS// konektatu da sarea
HERRI URRATS// euskara baita haria
HERRI URRATS// gure esku da geroa
@xik ikastol@