---
id: tx-1459
izenburua: Pianistaren Heriotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0LAJhPNRzSo
---

Bat-batean, auzoa
isildu egin da,
hotzikara bizkarretik gora.
Gaueko oin hotsak
izoztu egin dira.
Aurrera egin nahi 
eta ez dakite nora.

Izan ere,
dantza lekuan,
tiro hots batek
gelditu du dena.
Pianista
lurrean etzanda,
baldosak gorritzen eta...

Dantza automaten antzera,
gutako azkenak aurkitu arte azkena;
dantza azkena balitz bezala,
azkenik ez duten automaten antzera.

Tipo batek eta barmanak
hartu dute pianista
eta eraman dute kalera.
Odolean zerrautsa,
trompeta ahora,
pistara jendea eta...

Dantza automaten antzera,
gutako azkenak aurkitu arte azkena;
dantza azkena balitz bezala,
azkenik ez duten automaten antzera.

Gaueko oin hotsak
berpiztu dira.
Aurrera egin dute, badakite nora,
soilik bidean zehar,
lurreko gorpua,
zapatak ez zikintzearren,
saiestu dutela.

Dantza automaten antzera,
gutako azkenak aurkitu arte azkena;
dantza azkena balitz bezala,
azkenik ez duten automaten antzera.