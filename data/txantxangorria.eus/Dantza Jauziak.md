---
id: tx-2397
izenburua: Dantza Jauziak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OJbm2467D-Y
---

Bethi danik Altzükün, dantzari hunik sortzen
Oraikuk aldiz egin ahala egiten
Phüntü liak aisa saihetsilat uzten
Frijatak muldegaizten godaletak irailtzen!!!

Altzükütarrak, Altzükütarrak
Dantzari eta Khantari
Altzükütarrak, Altzükütarrak
Besta egile eta txoxtakari.

Khantatzen ahal da hein bat edanik
Dantzan aldiz hobe da barurik egonik
Altzukar gaztik bestan usaturik
Botzak saldun dute largatzen gogotik.

Botza largatzen bada phuntia barreatzen
Ondokun kuntrariua bardin agitzen
Hein hun bat egiteko gu biak junazten
Aitzina ariko gir ez bazide debeatzen.