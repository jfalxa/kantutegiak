---
id: tx-2947
izenburua: Nire Mundua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y6cdIVo1DgA
---

Ospa egiteko milaka arrazoi
bidai galdu bat behin eta berriro
esaidazu zein abesti aukeratu zenuen.

Eta abestuko dizut nahi duzun aldiro.

Zer pasatzen da zurekin?
Zer daukazu esateko?
Ez duzu ikusten zarela
“Sally Cinnamon” niretzako.

Hitzik gabe ikusiko nauzu
idei askorekin baina zearo itsu
ohiuka pasako naiz
kaletik galdezka.

Zer pasatzen da zurekin?
zer daukazu esateko?
ez duzu ikusten zarela
“Sally Cinnamon” niretzako.