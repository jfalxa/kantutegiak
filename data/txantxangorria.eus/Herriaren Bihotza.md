---
id: tx-2802
izenburua: Herriaren Bihotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JvE3oSuA7kc
---

Euskal Herria da gure herria
euskara da gure hizkuntza
herria da gorputza
eta hizkuntza berriz bihotza.

Kalean eta etxean
lagunekin eta familian
egin behar duguna beti 
da erabili etengabean.

Gure hizkuntza zaharra
galdu egiten bada
euskaldunon borroka
bukatua dago jada.

Bizi bizi euskaraz
esan esan euskaraz
Euskal Herrian eta munduan
bultza dezagun gure hizkuntza.

Hizkuntza da herriek daukaten nortasuna
Euskara da ikasi behar dugun hizkuntza.
Euskara da defenda behar dugun hizkuntza
Euskara da euskaldun egiten gaituena.

Erasoak pairatzen
Euskara ari da galtzen
akabo gure herria
ez bagara mintzatzen.

Ametsez betetako herria
herri txiki ta zaharra
ametsak egi bihurtzeko
daukagu bide bakarra

Benetan nahi badugu
Euskara izatea ofiziala
guztiok hitz egitea da
benetako egin beharra