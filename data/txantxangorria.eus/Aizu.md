---
id: tx-2607
izenburua: Aizu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e8B5GL0Lh-U
---

Hirurogei ta hirurogeitalau urteen artean jaio ginen biok,
heroinaren sarreraren kolpe zuzena
jaso genuenetakoak adituen ustez.

Hirurogeitaemeretzigarren urtean
agindu zuten krisialdi ekonomikoak,
punk-roka musika entzuten,
gu biok ere harrapatu gintuen.
Era berean, "arrisku" taldean...

Aizu, oinazera kondenatua
Lagun, luze hitz egin bizitzaz
Aizu, sakontzea estimatzen dut
Lagun, emaiguzu osasuna

Gaua ez zen inoiz hain luzea izan,
ezta egunsentia hain urrun egon.
Giza egiturak jotzen gaituenean,
negar egiteari utzi behar diogu.
Ez aukeratu auto bazterketa,
ez bilatu elbarritasuna,
gure gaitasuna gutxiestea baita,
bizkarrezurra okerturik izan arren.
Duintasuna soberan baduzu

Aizu, begietara so egin
Lagun, erlantza duzu oraindik
Aizu, gelditu hemen gurekin
Lagun, gure semeak entzun nahi dizu

Aizu, oinazera kondenatua
Lagun, erlantza duzu oraindik
Aizu, luze hitz egin bizitzaz
Lagun, gure semeak entzun nahi dizu