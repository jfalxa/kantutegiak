---
id: tx-91
izenburua: Plegu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/19T175cYYU0
---

Grabaketa: Vivak, Esanezin eta Bihar. Nahasketa eta masterizazioa: Aimar Ibarzabal (Ilun produkzioak) Audio de la canción Plegu grabada en 2022. Plegu abestiaren audioa 2022ean grabatua. 

PLEGU

Plegu abestia hiru artisten egindako kolaboraziotik sortzen den abesti bat da. Abesti
honek Folk, Pop eta elektronika puntuak jorratzen dituela esan genezake, bakoitzaren
estiloa argi utziz. Hurrengo lerroetan artista bakoitzaren deskripzio txiki bat jarriko dugu:

VIVAK
2021an sortutako proiektu instrumental akustikoa da. 2022ean beraien lehen EP-a
argitaratu zuten lau abestiz osatuta. 2022. Urtean zehar hainbat kontzertu eman dute,
beti ere, haien estilo akustiko instrumentala errespetatuz.

BIHAR
Konposatzaile eta interpretea ezkutuan mantentzearen arrazoia anonimatua
kontserbatu nahia da. Ez dago famatu izateko nahirik, ezta asmo komertzialik ere.
Sortzailea albo batera utzi eta abestiei garrantzia ematea besterik ez. Etxean konposatu
eta etxean ekoitzi dira baina kalea da euren lekua.

ESANEZIN
Esanezin Ianire Aranzabek (Altzagarate, 1999) 2019an Oriol Floresen (Bartzelona,
1979) laguntzarekin sortu zuen proiektua da. Bi single kaleratu eta horri jarraipena
emanez 2021ean kaleratu zuen“Aireratzen” bere lehen EPa. Poparen bueltan jorratzen
ditu bere doinuak nagusiki, bere gitarra akustikoa eta ahotsa oinarri direlarik, baina
soinu eta mundu berriei ateak itxi gabe.
2021 urtean zehar bakarka diska aurkezten ibili ostean banda osatu eta 2022an banda
formatuan aritu dira diskoa aurkezten, Arri Iraeta, Yeray Gascon eta Iñigo Asensio
bidelagun dituela.


Hau ote da behingoz eskatu dezuna?
Hau ote da behingoz espero dezuna?
Hau ote da behingoz merezi dezuna?
Hau ote da behingoz esan nahi dezuna?
Esan nahi dezuna…

Inork ez daki nolakoak garen
Hitzen artean loturak al daude?
Biok labar aurrean, hobi bat hontzian

Oroitzen ditut suak
Zoru bustiko oihuak
Oroitzen ditut ilunak
Oraindik hemen diren mamuak  

Oroitzen ditut suak
Zoru bustiko oihuak
Oroitzen ditut ilunak
Oraindik etortzear diren egunak

Biharamu zelai bat zuriz jantzita,
Eskuak eranztean, biak elurrez gorriak. Nork esan du orain…

Inoiz ez garela leku berean amilduko ez garela berriz elkartuko
Bihar hau bakarrik balego
Arnasa hartuta itotzeko

Inoiz ez zarela galdu gabe aurkituko
Inoiz zauriak zeuden lekuan orain
gutun hau bakarrik balego
Inoiz etzaitula iraganak eutsiko