---
id: tx-1462
izenburua: Xurxuri Baten Gainean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sN5Fzh703Dg
---

Hitzak: Manez Pagola

Xurxuri baten gainean 
Xoriak kantatzen du 
Kantatzen du biziari…

Xoriek ez dakite mundu huntan 
Zer gertatzen den : 
Hamar jendetarik sei 
Nola gosearekein dauden 
Bainan guk badakigu 
Ta hori aski zauku.

Xurxuri baten gainean…

Xoriek ez dakite, gizon onak 
Preso badirela 
Eta Euskadin ere 
Euskaldunak hortan garela 
Bainan guk badakigu 
Ta hori aski zauku…

Xurxuri baten gainean…