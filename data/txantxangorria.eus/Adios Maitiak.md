---
id: tx-1681
izenburua: Adios Maitiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3UwTqScXqMo
---

Adios maitiak,
adios lagunak!
Etorri da urrun joaitia
baina ere agian
Berriz itzultzea (bis)

Pietunak ekarri zuenean
armadarako galdea
ez nakien nola gorde
bihotzeko ilun betea
hor utzi behar bainituen
aita-ama haurriden xokoa
ta horiekin batean zu neure maitia.

Adios maitiak, ...

Bihar zuetarik urrundu ta,
trenez joanen naizenean
badakit oraindanik
zer duketan nik gogoan
Asteko lan dorpea utzirik,
gazte lagun guziak plazetan
ta ni, berriz, hantxetan,kaserna barnian!

Adios maitiak, ...