---
id: tx-2695
izenburua: Txalaparta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eZs4fjVl9os
---

Tiriki, trauki 
hiru ta lau dira zazpi, 
xurruxumurru 
bat batean dira urrun, 
tiriki, trauki 
ezker eskubi 
bien artean dago zubi, 
hots bare-bare 
hor leiho sare 
hiru ta lau dira bi... 
hiru ta lau dira... 
bat, bat, bat, bat! 
Tralararai, tralararai..