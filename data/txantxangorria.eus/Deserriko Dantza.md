---
id: tx-1394
izenburua: Deserriko Dantza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VnCzMP2-WvQ
---

Ilargi beteko gau baten
isil-isilik gindoazen
mendizerran barrena

lagun nuela maitea

Ilargia betea bazen
beteago zen gure pena
ilargi beteko gau batez
mendizerran barrena

Barkatu dezatela gerrak
dena odoltzen duen gerrak
muga zeharkatu baino lehen
ilargi beteko gau baten
makurtu nintzen lurra bera
atsekabez musukatzera
barkatu nazala gerrak

Ene sorterrian utzi nuen
bizi erdia lokartua
beste erdia hartu nuen
ez jausteko abaildua

Orain Frantziako lurretan
bihar agian urrunago
ni ez naiz herriminez hilko
herriminez bizi iraun baino

Ene sorlekuko lurretan
hiru muino mendizerran
ta lau pinu baso itxi bat
bost lur sail lur gehiegizkoa
oi ene herriko lurretan
oh Valles!
ene sorlekua!

Itxaropen bat birrindua
oroimina infinitua
ta herri bat hain txikia ezen
amets bakarrean sartzen den
ilargi beteko gau baten

Itxaropen bat birrindua
oroimina infinitua
ta herri bat hain txikia ezen
amets bakarrean sartzen den
ilargi beteko gau baten