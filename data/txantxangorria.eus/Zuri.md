---
id: tx-1003
izenburua: Zuri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-To6ccojdlY
---

Itsulapikoa hustu zutenek bahitu medioetan
larri dabiltzanei behar dutena esanez:
horra errudunak!

Esan zenbat garezur kabitzen den bi kontinenteren
artean zerbaitek eztanda egin gabe
ez al gara akordatzen? Garai batean
gure herria etorkina izan zen

Hala zabaltzen da izurri berri hau: irribarre batez
kanpokoa mespretxatzen aurrerakoi trajea kendu
gabe

Garezurrak pilatzen bi kontinenteren artean
zerbaitek eztanda egin gabe ez al gara akordatzen?
Garai batean gure herria etorkina izan zen

Etorkina zen eta orain ez daki nora doan,
lozorroan jendea jendetzat hartzen ez duen jendez
beteta bazterrak

Zein azkar ikasi dugun arrazakeria zuritzen
domaia da
#saveyourinternet