---
id: tx-3097
izenburua: Hau Da Gaua Uga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xky7zx3GE5M
---

Gauero leihoan zai gaude
begira gure unean,
geldirik ezer egin ahal izan gabe.
Logelan zigorturik 
ezin hitzegin zu itxoiten,
amets hau bait da geure buruen jabe.

-leloa-

Zu ta ni, motxila bat harturik
Zu ta ni, lo daudenean 
Zu ta ni, ihes egingo dugu 
Zu ta ni, egunsentian... 

Baina hau da gaua 
pausu batzutan lortuko dugu, 
geurea izango da etorkizuna. 
Hamabost metro eta jada 
burutu da bidaia,
etorri eta aske izango gara.

-leloa-

Zu ta ni, motxila bat harturik
Zu ta ni, lo daudenean
Zu ta ni, ihes egingo dugu
Zu ta ni, egunsentian...