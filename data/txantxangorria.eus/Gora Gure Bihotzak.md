---
id: tx-973
izenburua: Gora Gure Bihotzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eWRNZYWI-Fw
---

Gora gure bihotzak, malkorik ez bidean
gora gure bihotzak, 
atzera pausorik ez bidean.

Pertsona ugariri munduan hitza kendu digute
baina gure oihua erraz entzun daiteke
askapenaren bidean gizaki libre ezberdinek
metatu ditzagun ahotsak
behingoz entzun diezaguten.

Aurrera egingo dugu, ekaitzen aurrean aurrera.
Gora gure bihotzak, malkorik ez bidean
gora gure bihotzak, 
atzera pausorik ez bidean.

Herri baten sufrimendua eta borroka galduak
etsipenaren aurrean gu gaude tinko geurean
izateko jaio ginen eta izan nahi dugu
gaur izaterik ez badugu, bihar lortuko dugu.

Milaka bihotz ari dira taupaka,
milaka ahots oihuka eta goraka.
Milak ideiek egin dute
talka, mila, mila, mila, mila.
Milak txori ari dira hegaka.
Milaka kimu ari dira banaka.
Milaka otso dira uluka, mila, mila, mila, mila.