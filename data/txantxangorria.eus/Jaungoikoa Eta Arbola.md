---
id: tx-1878
izenburua: Jaungoikoa Eta Arbola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rnZMmNFjTWI
---

Sol                     Do       Sol
  Fueristak gera eta izango
                                       Re
mundua mundu dan artean
     Sol                 Do      mim lam     Re7   Sol
sentimendu hau bizirik dago betiko euskalerrian
        Re            Sol
naiz eri izan gure arima
      Re                     Sol
gaude soseguz bakean
                      Do     mim  lam     Re7     Sol
ikusiko da gure arbola zutik egunen batean.

Sol                    Do       Sol
  Mendietako raza noblea
                                   Re
gaur buru makur begira
   Sol               Do      mim lam        Re7  Sol
beti izan da fueroen legea euskaldunen anima
    Re              Sol
argitasunik ez da ageri
     Re              Sol
zeruan dago iluna
                 Do        mim   lam      Re7     Sol
libertadea esan kantari il arte maita dugula.

Sol                           Do        Sol
  Zoaz Don Karlos errege jauna
                               Re
urrun bai gure lurretik
    Sol            Do       mim lam      Re7     Sol
ez dezu utzi guretzat pena eta tristura besterik
   Re                  Sol
lutoz negarrez ama gaisoak
      Re                   Sol
Ai! Ezin konsolaturik
                         Do         mim 
ez degu nahi gehiago ikusi
   lam       Re7       Sol 
zori gaiztoko gerrarik