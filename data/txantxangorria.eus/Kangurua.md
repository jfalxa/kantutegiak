---
id: tx-2953
izenburua: Kangurua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w-USp6sK0tk
---

Australiatik etorri 
kangurua dunba dunba, 
luzeak dauzka zangoak 
salto salto dunba dunba

Egunsentian esnatu orduko,
saltoka dabil iluntzera arte
ez da erbia, ez igela,
ez matxinsaltoa
bere jauzi ikusgarriz
zaku lasterketan lehena beti!

Australiatik etorri 
kangurua dunba dunba, 
luzeak dauzka zangoak 
salto salto dunba dunba

Aurreko poltsan kumea darama,
pozik umea eta pozik ama
poltsatik at dabil orain
amaren atzetik pinpirin eta lirain
brinkoz-brinko hanka hutsik
zerua, hodeiak ukitu ezinik.

Australiatik etorri 
kangurua dunba dunba, 
luzeak dauzka zangoak 
salto salto dunba dunba.