---
id: tx-2054
izenburua: Bandera, Untzi Eta Kalaberaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S3ZOKbMBIiQ
---

Ezpata hartu ta bagoaz
txalupa honekin itsas hondora.
kizki, Mizkin eta ni, Krispin
hiru pirata maltzur eta ziztrin.
Bandera, untzi ta kalaberaz
aurrera
pirata denak! (bis)
Hiru piratak hurbiltzen dira
irla sekretu handira.
Beste untzi bat dute ikusi
kanoiak prestatu Mizkin!!!
Su ta hautsa zaio dena erori
belauntzitik, ergel hori!
Txalupa pusketan leherturik...

Hantxe ikusten dugu irla,
desertu bat bezala dago hilda
itsaslapurren buruzagi
altxorra zaintzen dago, Bizargorri.
Bandera, untzi eta kalaberaz
aurrera
pirata denak! (bis)
Bandera, untzi ta kalaberaz
aurrera! (bis)
Erasora!