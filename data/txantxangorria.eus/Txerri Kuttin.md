---
id: tx-3289
izenburua: Txerri Kuttin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Nr0lNSInw84
---

Nora zoaz txerri kuttin 
Kurrin kurrin!
Nora zoaz txerri kuttin
Kurrin kurrin!
Isatsa borobil eta fin
ipurdia berriz pottolin
Kurrin kurrin!

Belarri okerrak, mutur zikin
Kurrin kurrin!
Belarri okerrak, mutur zikin
Kurrin kurrin!
Jatea beretzat da atsegin
dena irensten du arin
Kurrin kurrin!

Nora zoaz txerri kuttin...

Hegan egin nahi baina ezin
Kurrin kurrin!
Hegan egin nahi baina ezin
Kurrin kurrin!
Korrika ta gero salto egin
lurrean zaude poxpolin
Kurrin kurrin!

Nora zoaz txerri kuttin...