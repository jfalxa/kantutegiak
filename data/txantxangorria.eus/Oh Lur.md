---
id: tx-1674
izenburua: Oh Lur
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s8ctb67trdc
---

Geurea dena lapurtzen uzteagatik,
geure izaerari uko egiteagatik. indartsuenak tinko eusten du
makila
anai txiki baten etsipenaren bila.

Oh, lur irents nazazu!

Isilik datorren heriotza;
gaisotasun baten izerdi hotza.
Mutu geratzen den herri zaharra,
haurrak ezin eutsi negarra.
Lotsarik gabeko herri xumeak
kanpotar ahotsen menpean
Itxaropen galdu baten, semeak
aita mendekatu nahiean

Oh, lur irents nazazu!

Aitonaren hitza dut gogoan
geure ama hizkuntza
mintzerakoan:
"ni banoa, seme, har ezak hitza,
emaiok euskarari hire bizitza !"
Lotsarik gabeko herri xumeak
kanpotar ahotsen menpean.
Barkeminik gabeko errugabeak aita mendekatu nahiean.

Oh, lur irents nazazu!