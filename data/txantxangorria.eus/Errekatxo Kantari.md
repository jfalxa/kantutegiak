---
id: tx-2540
izenburua: Errekatxo Kantari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wHwfK8sX9tE
---

Errekatxo kantari,
gulu gulu gulu;
haurtxoak inguruan
jolastu ohi dugu.

Haurtxoek lore bana
dute eskuetan;
bilduei inguruko
zelai e/derretan.

Gulu gulu gulu gu,
gulu gulu gulu,
errekatxoak han du
bere zurru-murru.