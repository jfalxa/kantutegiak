---
id: tx-3305
izenburua: Ardiak Lerro Lerro
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/av9WFUWMs4w
---

Ardiak lerro-lerro mendi maldan doatzi
   haize firfira batek deituz gorago beti.
   Ibai zola erreketan denak lotan sartzen dira.
   Zoin amultsua zautan gaua jasten mendira!

   Egunaz ixil bazen, gauaz pizten mendia.
   Belar punta bakotxak nasai dakar bizia.
   Gaztain ondotan kukuak joiten dauku soinua
   eta izarrekilan pestan dugu zerua.

   Oroituko naiz beti Arradoiko bazterrez,
   horrarat so gelditu bainaiz maiz bi begiez.
   Bainan orain ezin dut zangorik hor finkatu,
   bizi bihurri hunek bainau hortik urrundu.