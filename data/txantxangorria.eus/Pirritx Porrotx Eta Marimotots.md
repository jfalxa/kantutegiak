---
id: tx-1119
izenburua: Pirritx Porrotx Eta Marimotots
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_cV9lYWqYq8
---

Hankaluze, saltari eta haundia
bibote kilikagarria
astakirten samarra baina
Porrotx beti maitagarria.

Ki/ki/rri/ki, pin/pi/rin e/ta ar/gi/a
pinpilinpauxa dantzaria
oso mari-matraka baina
Pirritx beti maitagarria.

Pipi pipi pipi pipi
Pi/rritx
po/po po/po po/po
popo popo Porrotx
Pipi pipi pipi pipi
Pirritx
Pirritx eta Porrotx.

Ilargiaren magala dut bizileku,
hamaika zuhaitz gordeleku,
lagun txakurrarekin
jolasean kuku,
ez naiz inoiz ere i/zu.

Belaunak urratu ditut
hormetan igoz,
tximeletekin hegan gogoz,
putzuei begia
ateratzen bi saltoz,
betez dena usain gozoz.
Usain gozoz!

Mari Mo, Mari Mo,
Mari Motots naiz ni
parkean bizi naiz alai kantari
Mari Mo, Mari Mo,
Mari Motots naiz ni
ume bihurrien laguna naiz ni
ume bihurrien laguna naiz ni

Behin Pirritx e/to/rri zi/tzai/gun,
a/ma/txo i/zan na/hi zu/e/la.
Go/go/tsu nen/go/en,
e/ta ha/ra he/men
bi u/me/txo ba/te/ra.
Go/xo har/tu du/te ti/ti/a.
O/zen bo/ta puz/ke/rra.
Zuk, al/da/ie/zu par/de/la,
gar/bi/tu ka/ka-/pas/te/la.
Su/du/rra be/zain go/rri/a
ez jar/tze/ko i/pur/di/a.

HAU LORE ETA HAU PUPU
PAILAZO TXIKIAK JAIO ZAIZKIGU
HAU PUPU ETA HAU LORE,
FAMILIA DUGU MILAPILA KOLORE!


HAU PUPU ETA HAU LORE,
AUPA! O/E, O/E, O/E,
SALTXITXA TA PURE!