---
id: tx-2702
izenburua: Santiago Eta Santa Ana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/P19nS0wBwog
---

Santiago eta Santa Ana (Uribeko kopla zaharrak)
Santiau paseu de eta Santa Ana gaubian  (bi) 
bi mutil egon zazen, morena, bentana batian, oi, ei 
bi mutil egon zazen, morena, bentana batian.

Mutilok egon zazen guzurre kontetan  (bi) 
aitxitxe koitxedue, morena, taluak erretan, oi, ei 
aitxitxe koitxedue, morena, taluak erretan.

Amama koitxedie kortatik negarrez  (bi) 
katu demoniñoe, morena, taluak dandarrez, oi, ei 
katu demoniñoe, morena, taluak dandarrez.

Masuste koloreko begitxo baltzokaz  (bi) 
ondo engaña nona, morena, berba labanokaz, oi, ei 
ondo engaña nona, morena, berba labanokaz.

Harako zeuk gure eta neuk gure nendunian  (bi) 
berba labanik asko, morena, zendula miñian, oi, ei 
berba labanik asko, morena, zendula miñian.

Neu bere Josepa ta zeu bere Joseba  (bi) 
ondo kontau biarko, morena, hamalau pezetan, oi, ei 
ondo kontau biarko, morena, hamalau pezetan.