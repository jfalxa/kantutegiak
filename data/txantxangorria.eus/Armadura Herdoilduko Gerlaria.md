---
id: tx-433
izenburua: Armadura Herdoilduko Gerlaria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8jshmzL2fIQ
---

Robert Fisher-en liburua, bertsotan jarria. Bere burua babestu nahian oskola bat eraiki duten pertsona guztiei eskainia. 

LETRA

Beldurra hartu zienez
pare bat gerlari latzi
ezpata erosi zuen
eta armadura jantzi 
(azaletik burdinera
bi zentimetro distantzi).
Hari zegokion oro
handitasun ta garrantzi.
Pare bat izerdi saio
ta pare bat negar antsi.
Armadura erdoildu da
ta azalera itsasi.
Orain ezin du laztandu
ta poemarik idatzi
gizarajoak ezin du
bere azala erantzi.

Ezpata zorrotza bere
beso laburren luzapen;
besteak zauritzen ditu
bera zauri ez dezaten,
nahiz eta erasoetan
minik ez duen jasaten
zenbat denbora musurik
ez diotela ematen?
Beldurra ari da bere 
armadurapea jaten. 
Lakuko dortoka bat behin
ahalegindu zen esaten:
“Nahiz eta kartzela askoz
hitz egin ahalko diaten
zaila da azala baino 
estuagorik izaten”.

Armadura haren erruz
ez zitekeenez mugi
biluztea erabaki du
eta jarri da urduri:
pieza bat kendu du, lehenik;
ta gero, kendu ditu bi...
askatasunak beldurra
ematen digu batzuri.
Usaimenak seinalatu
ditu sei larrosa zuri
eta berak atzaparra
luzatu die hiruri
hatzean zast egin du ta
odol tanta bat isuri
baina egin duen zauriak
irribarre bat dirudi