---
id: tx-2716
izenburua: Txantxangorriak Basoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GIn_vvLyqK4
---

Txantxangorriak basoan
galdu du bere mokoa. (bis)
Galdezka gaude,
mokorik gabe
nola kantatu lezake?
Txantxangorriak basoan
galdu du bere mokoa.
Txantxangorriak basoan
aurkitu baitu mokoa. (bis)
Txorro-txioka,
txiru-liruka
adarrez adar saltoka.
Txantxangorriak basoan
aurkitu baitu mokoa.