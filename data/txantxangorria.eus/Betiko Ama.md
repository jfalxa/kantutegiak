---
id: tx-2282
izenburua: Betiko Ama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mENq9ud81Jw
---

Laztanaz mintzoka nituen egunak, 
beharrez ditut orain goxoak gogoan, 
humilez joan zen 30 Irailan, 
nahi dut ni atzera hamairu, etabar... 
Ikasiz neronen denboran esanaz, 
espero nuen ta, gerta zedin txarra, 
izan zen gertuan bihotzerreana, 
kartetan, arratsalde euritsu batean. 
Burrukan neronen mundu kaxkarrean, 
gertuka balitzan haren ahotsa, 
noiz joko hark soinua nere bihotzean, 
azkenik izan zen halako batean. 
Laguna bailitzan niri lapurretan, 
gaueko ilunak azkartuz benetan, 
orain dut nik eza nire bihotzean, 
guztiaz malkotan, nire ametsgaitza. 
Hortxe dut gogoan nire ama zena, 
maiterik betiko ikustea alaia, 
ez da ba, norbere amaren maitea, 
gauzarik handiaz bizi izatea!!
........................................................................
Zigor Gazkez jaunaren kantu bat, bere lehendabiziko diskotik. 1996. urtean kaleratu zuen zigilupean, Mikel Azpiroz maisuaren Hammond organoa lagundurik.

Zigor Gazkez, armonika eta gitarra 
Mikel Azpiroz, Hammond organoa

Bitxia da Zigor Gazkez gaztearen kasua, arrakasta haundiz agertu zen euskal eszenan, disko eder bat ekoiztu zuen eta bapatean desagertu zen. Bere kantak entzuterakoan, Bob Dylan jaunaren oroimena belarrira erraz etortzen zaizu. Hala ere, Zigorrek badauka zeozer berezia, bere ahotsa edota gitarra jotzeko modua agian, xarmangarria suertatzen bait da Dylan gehiegi gogoko ez diotenei ere. ?Onartzen dut antzekotasunik izan dezakeela Dylanekin... Jendeak nahi duena esan dezake, aske da. Ni ere bai, nahi dudana egiteko?. Zigorrek ahosoinua zortzi urterekin hasi zen jotzen, oso gazte, eta 14 betetzean hartu zuen lehen gitarra. 1995.urteko abenduan, Azkoitia herrian, John Martyn jaunarekin jo zuen. "Martyn gazte asko ikusi zuen eta, nonbait, bera entzuteko geldituko ez ziren beldurrez, taulara igo eta "deja tocar mi primero" esan zidan. Eseak eginez zetorren eta utzi egin behar", jajaja...

Azkoitiko kontzertua amaituta Iñigo Clemente jauna gerturatu zitzaion Zigorreri. IZ zigiluko telefonoa eman eta bost hilabetera diskorako proposamen bat eskeini zioten. ?Urrezko ametsa zen. Aise iritsia, gehiegi borrokatu izan gabe?. "Zigor Gazkez" diskoak, ahosoinua eta gitarraren babesean, rock doinuko 14 kantu biltzen ditu. Aurretik ingelesez abesten bazuen ere, Zigorrek euskaraz grabatzen hasi zen bere abestiak, eta gustora geratu zen lehen lanaren emaitzaz.