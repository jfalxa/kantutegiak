---
id: tx-1425
izenburua: Avirons Bayonnais
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Dw9qkBXxVxA
---

Mende Bat ari zaukula azkar eta erne
Aviron Bayonnais

PUBLICIDAD
inRead invented by Teads
Errugbian premiatsu da elkartasuna
Kontrakoen errespetua ta lagunena
Horrela baitugu lortzen garaipena
Dena xuri urdinez beztitzen da Baïona
bihotz guzietan sendi da alaitasuna
Guzietan nagusi da gure peña

Agur agur
Aviron Bayonnais-ko ekipari
Gure herriko xuri urdineri
Biltzen gaituen "Jean Dauger" zelaiari
Txalo Txalo
Gure hamabost jokolarieri
Gogotik ari diren mutileri
Baiona ohoratzen duteneri

Mende Bat ari zaukula azkar eta erne
Aviron Bayonnais

Gure uztatzea denetan da famatua
Hegaletan ardura markatzen entsegua
Euskal Herri guzitik sustengatua
Gaztetxoen formakuntzan guziz aritua
Publikoak beti danik suhar jarraitua
Goratuz aspaldiko izpiritua

Agur agur
Aviron Bayonnais-ko ekipari
Gure herriko xuri urdineri
Biltzen gaituen "Jean Dauger" zelaiari
Txalo Txalo
Gure hamabost jokolarieri
Gogotik ari diren mutileri
Baiona ohoratzen duteneri

Txalo Txalo
Aviron Bayonnais-ko ekipari
Gure herriko xuri urdineri
Biltzen gaituen "Jean Dauger" zelaiari

Mende Bat ari zaukula azkar eta erne
Aviron Bayonnais