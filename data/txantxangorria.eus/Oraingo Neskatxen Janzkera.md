---
id: tx-2845
izenburua: Oraingo Neskatxen Janzkera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/5YWtjhORCnY
---

Ara neskatx gaztiak, zuentzat bertsuak,
bixtan daukazkitenei petxu ta besuak,
eztiran ola bizi zuen gurasuak ...
esperantza ederra daukate gaxuak
zartzera pasatzeko egun erosuak.

Gona laburra jantzi, illia moztuta
nor azalduko zaien dabiltza poztuta,
amaren arrazoiak balio ez du ta
berenak aundiago kopeta beztuta,
gorputza adornatzen arimaz aztuta.

Dantzatik geldituta illunabarrian
bakoitzak mutil bana badute urrian,
fallorik ote daukan atzian ta aurrian
elkar neurtuko dute luze-laburrian,
erretiratzen dira garai ederrian.

Lenguan neskazar bi, orien jazkera!
Gona motxak jantzita pasiatutzera
otsuak soildutako ardien antzera,
besuak agiriyan, kolkuan petxera,
amarrak alde arte ez datoz etxera.

Ni, askotan bezela, tabernan nenguan,
neskazar oiei esan nieten lenguan:
jantzi ori ona da denbora beruan
baiña ez ilbeltzian eta abenduan ...
gaixuak, otzak ilko zaituzte neguan.

Ustez ez nien egin albertitu baizik,
bat oso zapuztu zan esan gabe itzik,
bestiak errespuesta eman zidan pozik:
"ez dauka jantzi onek iñorako lotsik,
sasoi daukanak ez du sentitutzen otzik".

Beraz edade ortan alako sasoia,
ogei ta bi urtetan etzeunden lasaia
adituko zenduen mutillen usaia ...
polita zera baiña ajatzen asiya,
gona motz oin azpian daukazu etsaia.

Garai batez majuak bazituen sobran,
orain zeiñek nai duen emen dabil proban,
afanosa bat dela ezagun du obran ...
iñork jakin nai badu neskatx ori nor dan
Ote-Motxen jaiua, Ximelaren bordan.