---
id: tx-1882
izenburua: Ume Eder Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B-5eEnUjxk8
---

Ume eder bat ikusi nuen
Donostiako kalean,
hitz erditxo bat hari esan gabe,
nola pasatu parean?
Gorputza zuen liraina eta
oinak zebiltzan aidean
politagorik ez det ikusi
nere begien aurrean

Aingeru zuri, paregabea
Euskal Herriko alaba,
usterik gabe zugana beti
nere bihotzak narama.
Ikusi nahian beti hor nabil,
nere maitia, hau lana...!
Zoraturikan hemen naukazu
beti pentsatzen zugana

Galai gazteak galdetzen dute:
aingeru hori nun dago?
Nere maitia nola deitzen dan
ez du inortxok jakingo;
ez berak ere, ez luke nahiko,
konfiantza horretan nago.
Amoriodun bihotz hoberik
Euskal Herrian ez dago