---
id: tx-2662
izenburua: Esola -Agure Zaharra-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N0wK92CF_po
---

Agure zahar batek zion 
bere etxe aurrean
goizean goiz lantokira 
irteten nintzanean:
Ez al dek, gazte, ikusten 
gure etxola zein dan?
desegiten ez badugu, 
bertan galduko gera.

Baina guztiok batera 
saiatu hura botatzera,
usteltzen hasita dago-ta, 
laister eroriko da.
Hik bultza gogor hortikan, 
ta bultza nik hemendikan,
ikusiko dek nola-nola 
laister eroriko dan.

Baina denbora badoa, 
nekea zaigu hasi,
eskuak zartatu zaizkit, 
eta indarrak utzi.
Usteltzen badago ere, 
karga badu oraindik,
berriz arnasa hartzeko 
esaigun elkarrekin:

Baina guztiok batera 
saiatu hura botatzera ...

Agure zaharra falta da 
gure etxe ondotik,
haize txar batek hartu ta 
eraman du hemendik.
Haur batzuk ikusten ditut 
eta inguraturik,
aitona zaharraren kanta 
nahi diet erakutsi:

Baina guztiok batera 
saiatu hura botatzera...