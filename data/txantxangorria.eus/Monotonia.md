---
id: tx-1226
izenburua: Monotonia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZLs7_r_x89k
---

Ostirala seiak lanetik kanpora 
hemendik ni banoa ta eztakit nora 
trago batzuk hartuta zulotik zulora 
betiko moduan betiko ritmora. (x2) 

Uoooooooo! 
Hauxe da gure egia 
Uoooooooo! 
Dana zahar ta berria 
Uoooooooo! 
Zesatekek jarria 
Uoooooooo! 
Gure (eta zuen) monotonia 

Astearen erdia lanean sartuta 
asteburu osoa seko mozkortuta 
oso daukagu asimilatuta 
oso gauzkate gu anulatuta. 

BIZI ZAITEZ ALAI ESKEMAK APURTUZ 
ZURE UTOPIAK GOGOR BORROKATUZ 
BIZIA ESTUTUZ ONDO ESPRIMITUZ 
GUZTIA ALDATUZ DANA KUESTIONATUZ