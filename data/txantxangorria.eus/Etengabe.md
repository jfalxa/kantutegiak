---
id: tx-2350
izenburua: Etengabe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dRg1gsdkzLE
---

Lagundu nizun behar zenun guztian. Ez zu ez zara lagun, ez, nitzat jada ezezagun.
Kaletik zoaz nitaz berba eginez. Zu lotsabakoa, zu, esker txarrekoa zara.

Etengabean bizkarretik erasotzen, etengabean faltsukeriak sortzen. Gero ez etorri barkamen eskean, zure bidea jarraitu ta utzi bakean.

Ez dut ulertzen zergatik zaren horrela. Nik zer egin dizut? Zu gezurretan zagozela.
Baina bost axola, jarraituko dut aurrera, ni nire bidetik, zuk jakin zer egin.

Jende asko dago faltsukeriak sortzen, gaiñea gehiengoak ez digute ezagutzen. Bidean goaz lagun eta familian esker, eskerrik asko zuek gabe gu ez gara ezer. 
Goazen aurrera, lana eginez guztiok batera. Goazen aurrera, bakarrik bazoaz alferrik dena. Ezin atzera, bidean lagunak daudela, hori da inporta duena, beti zaindu gaituztenak.
Beti daudelako gure ondoan, beti irrifar batez ahoan. Segi egurra ematen auzoan, akabu zuen jokoa.

Etengabe jarraituko dut aurrera, ez zaitut nire ondoan ezertarako nahi. Etengabe jarraituko dut aurrera, etengabe bai, etengabe!