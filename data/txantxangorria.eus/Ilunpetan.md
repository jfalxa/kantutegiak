---
id: tx-745
izenburua: Ilunpetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/V4k8gvryxBs
---

Egiek eta zure begiek esan ziguten alde egiteko,
Irriak eta larunbat gauak hemen uzteko,
Gauez ihesi eta egunez ikusi,
Ez dela gauza bera izango,
Gauez ihesi eta egunez ikusi,
Aurretik duguna egiteko.

Ongi etorriak beti zuenera,
Ilunpetan agorutak,
Ongi etorriak beti zuenera,
Elkarritanako lotuak.

Berriek 'ta gaueko sirenek oihukatu basora joateko,
Muxuak eta gaueko izarrak hemen uzteko,
Gauez ihesi eta egunez ikusi,
Ez dela gauza bera izango,
Gauez ihesi eta egunez ikusi,
Aurretik duguna egiteko.

Ongi etorriak beti zuenera,
Ilunpetan agortuak,
Ongi etorriak beti zuenera,
Elkarritanako lotuak.

Ta bizitako guztiak
Ta abestutako olerkiak,
Berpizteko berandu al da?

Ta sortutako bideak,
Margotutako paretak,
Argitzeko berandu al da?

Berandu al da?,
Berandu al da?,
Berandu al da?.
Berandu al da?

Ongi etorriak beti zuenera,
Ilunpetan agortuak,
Ongi etorriak beti zuenera,
Elkarritanako lotuak.