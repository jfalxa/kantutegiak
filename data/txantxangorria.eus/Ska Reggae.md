---
id: tx-2876
izenburua: Ska Reggae
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tJOS_xf17bI
---

Denok  saltoka denok dantza ska ta reggae
Denok  saltoka denok dantza ska ta reggae
hankak mugituaz, eskuak altxatuz
gorputza jorratu ta beti argi

Elkarri begira, keinu bat tira
gaua etorriko da eta ez berehala.

Festa bukatu da, orain dut duda
zertan geratuko dan, ajolik ez.