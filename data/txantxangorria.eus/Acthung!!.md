---
id: tx-2972
izenburua: Acthung!!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/glsGF27C8A8
---

ACHTUNG!!

Promesak kontuz
Txaloak kontuz
Aholkuak kontuz
Kasu eman

Ametsak kontuz
Jainkoak kontuz
Sexua kontuz
Kasu eman

Dena da egia, gezur erdia
Dena da gezurra, erdi egia

Galderak kontuz
Erantzunak kontuz
Jakiak kontuz
Kasu eman

Leloak kontuz
Azkarrak kontuz
Kamerak kontuz
Kasu eman

Dena da egia, gezur erdia
Dena da gezurra, erdi egia

Internet kontuz
Antena 3 kontuz
Lau teilatu kontuz

Eta ukitu gabe ubeltzen diren gorputzatal guztiak

Dena da egia, gezur erdia

Ta hitz asko ikusi dut zentzua galtzen
Ta hitz asko ikusi dut itxuraldatzen
Ziurtasuna harrokeriaren itzala da
Ta hitz asko ikusi dut esanahia ahazten