---
id: tx-3321
izenburua: Ilargira Noa -Alaitz Eta Maider-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-L1ibqVacNQ
---

Agur agur agur
Ilargira noa (bis)
 
Nerekin damazkit pentsakisunak
neronekin ezin ikusiak
neronekin gose ta gorrotoak
neronekin inbide guztiak
 
Agur agur agur...
 
Hemen uzten ditut nahirik onenak
hemen pakea ta osasuna
Compartilhar
hemen gizonaren ezkubideak
hemen danontzat askatasuna
 
Agur agur agur...
 
Amets ola bart gabean egin dut
amets jarri nau poztuta
bainan Almeriako bomba horrek
utzi dit ametsa zapuztuta
utzi dit ametsa zapuztuta
eta orain ezin ilargira joan (bis)