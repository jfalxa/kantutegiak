---
id: tx-3093
izenburua: Niri Euskaraz Mesedez Kalera Lanera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/G2izSB4pXkU
---

Ume guztiak elkartu gara
euskararen inguruan,
jolas kantuan arituz gero
geurea da seguruan.
Gauza hoberik dudarik gabe
ez da bilatzen munduan.
 
 Bizi euskaraz denok esanez:
"niri, euskaraz mesedez " (bis)
 
Gazte helduak zaharrak ere
elkar goratuz euskara,
Gure ikastolan asmo horrekin
lanean ari bai gara.
Aurrera jator euskal langintzan
euskara biziko bada.
 
Bizi euskaraz denok esanez:
"niri, euskaraz mesedez " (bis)