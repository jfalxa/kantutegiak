---
id: tx-3187
izenburua: Anttoni Eta Antton -Mattin Eta Katrin-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Oe1l2AQWJm8
---

Antoni! Antoni!
Zure atean nago ni.
Ai, Anton! Ai, Anton!
Ate ondoan, hor konpon.

Hormatxoriak negu gorrian
ez du atsegin elurra:
zerua goibel, kabia hotza
eta janari gabe lurra.
Ai, maite, nire bihotzak duen
zure hotzaren beldurra!
Biontzat kabi bat berotzeko
bilduko nuke egurra.

Antoni! Antoni!...

Enarak ez du behin egindako
kabirik inoiz aldatzen:
urtero beti kabi aretan
umeak ditu bazkatzen.
Baina zu, Anton, enara txarra
zaitut niretzat bilatzen;
jai bakoitzean nexka berria
ikusten zaitut maitatzen.

Antoni! Antoni!...

Enarak ere bere kabia
galduta badu ikusten,
biderik gabe, haruntz eta honuntz
kabitik du aldegiten.
Egarri dagoen nire bihotza,
zuk eman ezik edaten,
berriro ere ibiliko da
gogorik gabe nonbaiten.

Antoni! Antoni!...

Zure bihotza egarri dela
esan didazu bertsotan;
neronek ere ikasia dut
arrazoi duzula hortan.
Zu beti zabiltz edari bila,
ikusi zaitut askotan,
baina neurriz gain egarri hori
itotzen duzu ardotan.

Antoni! Antoni!
kalabaza zale ez naiz ni.
Ai, Anton! Ai, Anton!
Haize freskoari, gabon!