---
id: tx-1625
izenburua: Lepoan Hartu Rip
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Yb-BpvEo_Qk
---

Trailara, trailara
la-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!

Gazte bat lurrean aurkitu dugu
lore gorriz beterik kolkoa...
burdinen artetik ihesi dator
euskal gazteriaren oihua!
Mutilak, eskuak elkar gurutza!
ekin ta bultza denok batera!
Bidean anaia erortzen bazaik,
lepoan hartu ta segi aurrera!

Trailara, trailara
la-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!

Ez dugu beldurrik, ez dugu lotsarik
nor geran, zer geran aitortzeko!
Ez gaituk lapurrak, ez eta zakurrak
kataiaz loturik ibiltzeko!
Gizonak bagera, jo zagun aurrera,
gure herriaren jabe egin arte!
Askatasunaren hegal azpian
kabia egiten ohituak gare!
Ibiltzen aspaldi ikasia dugu,
otsoak eskutik hartu gabe!

Trailara, trailara
la-la-ra la-la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!

Arrotzek ezpataz hil nahi ba naute
izituko gaituztelakoan,
zutitu ta euskeraz mintzatuko naiz
nere hiltzailearen aurrean!
Mutilak, ez gero nigarrik egin
erortzen banaiz gau ilunean,
izar berri bat piztutzera noa
Euskal Herriko zeru gainean...
Euskal Herriko zeru gainean...
Euskal Herriko zeru gainean...

La-la-ra
La-la-ra la-la la-la-ra
Lepoan hartu ta segi aurrera!
Lepoan hartu ta segi aurrera!