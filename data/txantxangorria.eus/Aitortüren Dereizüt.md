---
id: tx-195
izenburua: Aitortüren Dereizüt
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vjNP1xEkvy8
---

Aitortüren dereizut adiskidea,
ene bihotzeko pena handia
Galtzen ari zaikü, , bizipidea, 
aiten aitek ützirik, lür eder maitagarria
lür eder maitagarria
Galtzen ari zaikü, , bizipidea, 
aiten aitek ützirik, lür eder maitagarria
lür eder maitagarria

Zauritik odola zalhe beitoa
batere dügüia sendotzekoa
Norat ari zaikü,oi Xiberoa,
lürrak saldü eta, zer date gure geroa?
zer date gure geroa?
Norat ari zaikü,oi Xiberoa,
lürrak saldü eta, zer date gure geroa?
zer date gure geroa?

Euskal popülüa ez da galdüko
behar ere beita iratzarriko
Gük ez dütügü ez lürrak saldüko
ontsa bermatürük, azkar girade biziko!
zkar girade biziko!
Gük ez dütügü ez lürrak saldüko
ontsa bermatürük, azkar girade biziko!
zkar girade biziko!