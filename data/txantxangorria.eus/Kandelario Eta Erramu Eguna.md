---
id: tx-994
izenburua: Kandelario Eta Erramu Eguna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tv0d6Ww5y5Y
---

Otsaillaren bigarrengoa
izan oi da Kandelero,
negu erdian nola datorren
zailla da izaten bero,
baiña guraso zarren antzera
ziñez sinistu ezkero
fedean dago berotasuna
lenago, orain ta gero.

Kandelario goiz batean bein
aurtxo nintzela amari
galdetu nion: “Zer delako gaur
ataria orren txuri?”
Erantzun zidan: “Zeruko Aitak
bart jarri du zapi ori
apaintasun bat ematearren
kandelero egunari”.

Garai artako gauza geienak
oraindik dauzkat goguan:
kandela batzuk artuta ama
eliz aldera zijuan,
gero kandelak bedeinkatu ta
andikan etxerakuan
aren fedeak berotasunez
elurra urtutzen zuan.

Garizumaren azken aldera
degu Erramu-eguna
Jerusalenen Jesus sartzea
adierazten diguna,
guk erramuak bedeinkatzera
eramaten dituguna:
fedea duten guztientzako
badauka laguntasuna.

Suari bota oi geniogun
ekaitza zetorrenean,
gurutzak ere egin oi ziren
Santa Kurutz egunean...
Kandela-bedeinkatu tantoak
botatzen ziren gaiñean,
kredoak esan ta jarri gero
leioetan ta atean.

Baso ta soro, belardietan
egiten genuen berdin
gurutze banaz zutik jarriaz
bakoitza kredo banakin;
arri kazkabar eta tximistak
kalte asko oi du egin,
fede aundian jarri oi ziren
ori gertatu etzedin.