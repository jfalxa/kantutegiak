---
id: tx-339
izenburua: Baga, Biga, Boga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mppYx2LoFbk
---

Hitzak, koposizioa eta musika: XIBEROOTS
Partehartzea: Andoni Basterretxea Urreisti DELIRIUM TREMENS

Grabaketa, nahasketa eta masterizazioa: ELKAR ESTUDIOA
Teknikaria: Victor Sanchez

Bideo Klipa: KESTU produkzioko Gabriel Courty Villanua eta Julien Rider

Eskerrak: LAT Lapurdiko Arraun Taldea eta Sarako Olhain ikastola






Zia boga hau iragan eta, goazen hamar kolpe emaitera

Ikasontzea den ikastola, ühain gainetik lau haizetara

Seaskaren ametsen indarra, argian egon dadin euskara

Herri urratsen urt’oroz batea, biziazak hire probintzea

 

Bizizak hire probintzea, bizizak hire Euskera

Bakoitzak berekoa da, Eta denak geureak dira

Erabil zure Euskera, Euskera gurea da

 

Üthürritik bai itsasoala, Euskara beti boga doala

Zühaitzetan ere gisala, enborretarik ostoetara

Gelditzen ezin ahala, bakotxak hortan dü bere rola

Segi dezagün orai hola hola, biziko beita ikastola

(berriz)

 

Baxe Nafarroa, Lapurdi Araba

Bizkaia, Nafarroa, Gipuzkoa ta Xiberoa

Senpereko aintzira, Herri Urrats, denak hor gira !

...Indarrak batea, Herri Urrats, Ikastolaren besta

 

Boga egin dezagun denok, denok batera

Araunak indar aztindu eta, aurrera egitera

Haizeak ere badaki bidea, goazen berarekin batera

Eramango baitu begiak itxita Herri Urratsera

 

Boga Boga, mariñela!

Joan behar degu, Senperera!

 

Baga Biga Boga …(x8)

 

Baga Biga Boga, Baga biga

Zuka, Xuka Toka Noka Baga biga!