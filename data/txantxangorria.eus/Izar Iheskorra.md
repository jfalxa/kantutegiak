---
id: tx-2883
izenburua: Izar Iheskorra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eNvFGc0FjEg
---

Gauaren babesean 
gaur goizean
lo goxoen ametsetan 
nabil hizketan
nigan daukat laztana
zure xarma
sentipen polit guztiak
malkoz bustiak.

"Urrun zaude eta gertu"
sentitzen zaitut nik
lagun gaur gauak esana dit, zergaitik,
irrifar batekin, ez zaude nirekin
lasai maitia ulertu:
zergaitik nik ere 
ezin dudan bizi zu gabe, xuabe,
zurekin baino zu gabe!!(zu gabe!)

Zure izar iheskorra
da hilezkorra
edozer dut desira
zuri begira
ni zugan ta zu nigan
nahiz dakidan
zarela hain besarkaezina
hain ahaztezina.