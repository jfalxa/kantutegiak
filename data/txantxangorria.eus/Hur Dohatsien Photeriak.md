---
id: tx-127
izenburua: Hur Dohatsien Photeriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S4edgRe7gH8
---

Hur dohatsien photeriak 
Gerezitziak lilitan, erreka berde urdinak, 
Zohart ihitza dirdiran, bortüko izar arhinak.
Hur dohatsien photeriak, gure lamiñen ageriak, 
Aro txarretan atheriak. Agur herriko anderiak !
Üdako egün ahazkorrak, ülhün nabartze ahalkorrak, 
Gaiazko amets emankorrak, ürzo bidajant iheskorrak. 
Gerezitziak lilitan, erreka berde urdinak, 
Argizagia distiran eta gizonen bardinak. 
Hitzak : JnMixel BEDAXAGAR 
Müsika : Jean ETCHART