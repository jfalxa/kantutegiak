---
id: tx-2533
izenburua: Esnatu Nintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tTlrbXYq77c
---

Amets erneek bahitu zidaten gaua eta
begirik biltzerik ere ez nuen izan, mendetan.
Hiena, hezurtsu baten antzera
iragana saldu nuen gorpu baten truke, gose nintzen eta.
Ustel usaina zuten nire hitzek orduan
ez nintzen gehiago inor laztantzeko gai.
Bizitzea zer zen ere ahaztuta
izarari eusten nion gogor, lema bailitzan.
Arraunik ez zenidan hurbildu
eta oihalak zenizkidan hautsi,
oharkabeko hilketa izango zen
erretzaile pasibo ni.
Hemen, ez da errudunik
fikzioa errealitate bihurtzen sekula ez nuen jakin.

Baina, lo hartu nuen inoiz, nekeaz
harretatik irten berri ziren tximeletekin bazka egiteko.
Esnatu nintzen hilkor
zurbil izateari utzi eta bizitzari eusteko.
Denbora lapurtu diezadakezu
ez zait hain baliotsu arnasa besteko,
urregorrizko musuek ez didate jaten inoiz emango.
Baina, lo hartu nuen inoiz, nekeaz
ilargi beteak jarraitzen nau etxera, orain, gauero.
Esnatu nintzen hilkor
egiak eta gezurrak katilu berean nahasteko.
Azukrerik ez dut behar.
Nire kafea blues beltz bat da, orain eta betiko.
Orain eta betiko, larrua eranzten dudaneraino
orain eta betiko mundua xahutu nahi dut oinen azpian
beren hegalen azpian beleek itotzen nauten arte.

Esnatu nintzen hilkor, hegalen azpian.
Itotzen, bizitzak, musuek, hiena bat nintzen,
beleek itotzen nauten arte.
Baina, lo hartu nuen inoiz.



Traducciones: es.eneritzfuryak.com/letras
Translations: en.eneritzfuryak.com/lyrics