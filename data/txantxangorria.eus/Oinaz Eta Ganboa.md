---
id: tx-1286
izenburua: Oinaz Eta Ganboa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qxi6kBQLwic
---

Oinaz eta Ganboaren
errierta handia, 
ikusirik lur zelaia
jota dago mendia.
Su gabe dago zuzia,
su gabe ilendia.
Seinale gertuagorik
esazue non dia?

Mondragoe hartu dute 
oinaztarrek traizioz
gero salduo Bizkaia
urrearen prezioz
ezagutuko baitira
Judasen ofizioz.
Euskalerria beteko
tatxaz eta bizioz.

Oraindik hementxe gaude
Oinaz eta Ganboa
Oinaz zaldunaren pean
Ganboa naiz mandoa.
Gure gorroto bizia
ez baita aurtengoa
gure arrazoi guztia 
hasarrez hemen doa.

Bostehun urtean egon 
gara justizi-eske
baina sua eta ura
ezin nahas ditezke,
irakin edo amata
ez dago ezer beste,
inork ez dezala esan:
“adiskide zaitezte”.