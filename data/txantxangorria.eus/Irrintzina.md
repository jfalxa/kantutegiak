---
id: tx-903
izenburua: Irrintzina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IVSwZ_ynAvU
---

Erradak irrintzina
nondik heldu hizan,
hire oihua samina
non hartua dukan
mendi kasko-kaskotan
ala erreketan.
Erradak irrintzina
non sortea hizan
Tralala

Hik dituk lehenago
bildu eskualdunak,
harpetan, zelaietan,
xutik zagotzinak
hi zintzur bularretan,
bortuko artzainak
baderamatzak bere
gau eta egunak
Tralala

Zer ote daukun bada
Gaur hemen edango
Hire ehortzeguna
Ez dela biharko.
Zimeldu den arbola
baita berpiztuko
Xori guztiek vetan
Berriz kantatzeko
Tralala

Emak ba irrintzina
Beti hireari
Idekiz sail berri bat
Gogo guzieri
Erakuts bide ona
Bihotz kartsueri
Geroa segurtatuz
Eskual Herriari.