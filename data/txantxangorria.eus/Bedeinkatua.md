---
id: tx-1712
izenburua: Bedeinkatua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bXsrYVhZd9I
---

Bedeinkatua.
Santifikatua.
Hirugarren ordenako buruzagi ospetsua.
Herriaren zaintzailea.
Birtute guzien irudi eta Zeruko Erreinuen espilua.
Zu, mantxarik gabea.
Espirituz bezain gorputzez lehorra.
Zuhurtasunez josia.
Hitz ederren eta fortuna dezentearen jabea.
Zu, nunbaitetik etorria.
Profeten ahotsa.
Auzokoen kezka.
Zu, 35 urte luzez emaztea
Igande arratsaldetan etxeko lehiotik begira eduki zenduna.
Emazte gaixoa.
Emazte iluna.
Emazte bizarduna.
Emakume leial batek merezi ta desio bezain gartsuki,
o doilorkeriaren haundia,
sekulan izorratua eta zure gorputzez gozatua izan ez zena.
Bere erraietako indar desesperatuen
birtutez santutasunetik aterea.
Aitaren hustelkeriaren kulpaz seme tonto batzuen ama izandua.
Ixil-ixilik urte luzetan bizitua
eta arratsalde hotz eta euritsu batean
ia inor konturatu gabe
hila eta hobiratua izan zena.
Zu, Maltako kaballeroa.
Prozesiotako zingurria.
Ohitura onen defentzailea.
Dantza zikinen etsai amorratua.
Neska-zaharren poz zimurra.
Agure okerren segurantzia.
Ilunpetako zomorroa.
Nagusi guztien zerbitzari parerik gabea.
X. mendeko itzala.
12 etxeren jabea.
Zimaurretako zizare koloregabea.
Santua.
Santua.
Guztiz santua.
Ahuzi konpontzaile trebea.
Zizeron berpiztua.
Grekoaren koadroetatik ateratako gizon magroa.
Sabiduriz eta zerrez beterik
munduko pekatuen aurrean oihu egiten duena.
Lapur galanta.
Astazakil paregabea.
Zure heriotzako egunean kanposantuko
harrek bazkatuko dute zimaur gozoa.

NERE HITZA EZ DEZALA
HAIZEAK ERAMAN

Amen.