---
id: tx-1484
izenburua: Ardiherria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EqM6mNLrfKA
---

Beste ardi guztietatik bera zen desberdinena
ez zen txuria ez zen beltza ez handiena ez txikiena
baina bazeukan zerbait beste guztiek ez zeukatena
artzainak bera zuen maiteena.
Ardi txakurrei eta makilei ez zien obeditzen
sortze beretik ikasi baitzuen larrean libre bizitzen
itsaso eta mendien arteko lurretan bizitzen
askatasunari irri bat eskeintzen.