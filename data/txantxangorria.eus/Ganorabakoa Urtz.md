---
id: tx-1653
izenburua: Ganorabakoa Urtz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2VZG1LJvgQk
---

Ni al naiz zure bidean
sarritan gurutzatu den ganorabako hori;
ganorabako hori al naiz ni?
Ni al naiz zure aholkuak
sarritan entzun ez dituen ganorabako hori;
ganorabako hori al naiz ni?
Ni al naiz zure ardura
sarritan eskertu ez duen ganorabako hori;
ganorabako hori al naiz ni?

Ni al naiz, ziur al zaude
negar eginarazten dizun
ganorabako hori?
Burua ez badut galdu
nola, nola ninteke izan
horren leloa?

Ni al naiz, ziur al zaude
negar eginarazten dizun
ganorabako hori?
Neure ametsa bazara
nola, nola ninteke izan
horrn leloa?

Zatoz nirekin...

Ni al naiz, ziur al zaude
negar eginarazten dizun
ganorabako hori?
Ni banaiz, hitz egidazu
hiltzen, hiltzen etzazu utzi
istorio eder hau.

Zatoz nirekin...