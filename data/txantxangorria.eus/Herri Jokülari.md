---
id: tx-181
izenburua: Herri Jokülari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AoIKfbahPbg
---

Herri hanitxetan da orai jokülari, euskal plazetan gainti ikusten ausarki, Gizaldien indarrak hor dira ageri, aupaka gazteriak herri aizoeki. 
Güdükalekü eder horietan jokülariak zainetan, 
Zer jentetzea, arrabotsetan, egargari oihü handitan. 
Batak dir’aizkolari, besteak segarri, zakülarik trostaka intzirikan ari, 
Unküdi, harri, lasto, katau-en altxari, gizon pezü azkarrak sokaren tiari. 
Denak gitian junta plaza jokietara, gano dianak lagünt gazte indardüna, 
Jente güziak goatzan horien süstatzera, borrokarier soz beita laket euskaldüna. 
Hitzak: Dominika ETCHART 
Müsika: Jean ETCHART