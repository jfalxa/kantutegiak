---
id: tx-186
izenburua: Amodiua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gJ0OH95V9c0
---

Hau dügü hau bizitzea, mündü huntako izatea, 
Han hor gerla’ta trixtezia, falta delakoz amodiua. 
Oiei aiei aieia ! Amodiua, amodiua, 
Oiei aiei aieia   a   a !  Otoi hel zite gü gana ! 
Oiei aiei aieia   a   a !  Otoi hel zite gü gana ! 
Zer familiak ürratürik, aitak et’amak behexirik, 
Haurrak hen artin tirakatürik estelakoz amodiorik. 
Gaztiak gazterik etxerik junik, mündian hanitx herratürik, Zühür bideak bazter ützirik, dir’amodioz gabetürik. 
Xaharrak etxen trixtetürik, haurrak bere alde juranik, Haidürü dira, penatürik, amodio leinhü bat hetarik. 
Hitzak: Dominika ETCHART 
Müsika: Jean ETCHART