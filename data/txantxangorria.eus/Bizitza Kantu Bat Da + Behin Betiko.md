---
id: tx-1084
izenburua: Bizitza Kantu Bat Da + Behin Betiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/86hgf25XCYA
---

Bizitza kantu bat da, guretzat hain justu
neska mutilak hori dago gure esku.
Kantari eskolara, kantari jolastu,
kantari lotaratu, kantari amestu,
kantatzen ez duenak zorionik ez du.

Behin betiko, behin betiko
sinple bezain tinko,
ez dugu etsiko!
Behin betiko, behin betiko
sinple bezain tinko,
EUSKARAZ ETA KITTO!

Etxean eta kalean
berdin-berdin jolasean,
eskolan, lanean,
borrokan eta pakean. (tristuran eta pozean)
Dakitenek erabiliz,
ez dakitenek ikasiz,
herri bat osaturik
euskaraz nahi dugu bizi.

Hasi etxetik eta kalera,
hasi kaletik eta etxera,
martxan jarri da lege berria:
Euskaraz Euskal Herrian.