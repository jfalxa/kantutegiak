---
id: tx-3249
izenburua: Goiko Mendian Elurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/DUfZtxHiiX0
---

Igurtzirik bi eskuak.

Igurtzirik belarriak.

Bero-bero-bero jarri naiz

jolastera noa orain.

Goiko mendian elurra

zuritzen ari da lurra

zuri-zuriak gailaurrak

gorri-gorriak sudurrak.