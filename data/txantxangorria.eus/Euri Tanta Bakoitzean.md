---
id: tx-2060
izenburua: Euri Tanta Bakoitzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Et4vF7oROD4
---

Maite ditut udaberrian 
agertzen diren lehen loreak,
zeruak urdinak bilakatzean.
Udako gauetan lotaratzea
izarrak itzaltzen direnean,
eta argia somatzen hasten denean,
urrunean.
Hostoak erortzen begiratzea,
igande euritsuak udazkenean,
leihoan jarrita gela barruan.
Gogoko ditut elurrak neguan
berarekin dakarren lasaitasuna
denbora gelditu balitz bezala.

Eta zu, beti pentsatzen zaitut
hostoen erortzean, loretan,denetan,
izarren itzaltean, euri tanta bakoitzean,
denetan.

Orduak pasatu gora begira
hodeien itxurak ulertu nahian
guztiek daukate zeresanen bat
bilatzen bada.
Begiak itxita non-nahi etzanda 
egunak emango nituzke hala
haizeak kantatzen duen artean.

Eta zu, beti pentsatzen zaitut
hostoen erortzean, loretan,denetan,
izarren itzaltean, euri tanta bakoitzean,
denetan, benetan.

Elur-luma zuritan, hodeirik zailenean
loretan, denetan
hostoen erortzean, euri tanta bakoitzean, denetan.