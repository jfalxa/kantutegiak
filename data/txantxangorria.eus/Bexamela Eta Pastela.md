---
id: tx-2048
izenburua: Bexamela Eta Pastela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vDoAO00J7wM
---

Maite dut zuk laztantzea / dantzatzea
zure ezpainak xurgatzea / hortzen esmaltea
baita ere zure garagar / zaporea
ahal denean lepotik / kosk egitea
Ai bexamela / eta pastela,
ai bexamela...
Maite dut zu lotsatzea / dir-dir ikustea
nata mihiztea / bustitzea
zeruan barrena / hegal egitea
eta bapatean / lur jotzea
Ai Bexamela...
Maite dut oihukatzea / muxu ematea
zure malko gorrien / zuritzea
emozionatzea / zu ferekatzea
eta maite zaitut / esatea
Ai Bexamela...