---
id: tx-1315
izenburua: Zeru Tolesgarria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TRSeTGQm9KI
---

Zeru Tolesgarria izeneko abestiaren bideoklipa. 2016ko urrian grabatua Tindoufen, errefuxiatu Saharauien kanpamenduetan. Fishara zine jaialdi solidarioa ospatzen zen bitartean nik hartutako irudiak dira. Iñigo Asensio arduratu da ondorengo muntaiaz. Abestia, izen bereko diskoan aurkitzen da. ZABALDU DEZAGUN ZERUA SAHARA LIBRE BATEAN!



 Letra (harkaitz Cano): 
Patrikan, 
bi sosez gain 
komeni 
gauzen artean 
gordeta larrialdi zerutxo bat tolestuta, 
lauzpabost doblatan edo, 
zer gerta ere. 
Zerua tolesgarri, 
urdin huts grisaren kontra dosi bat, 
garbi, garden, dosi doi bat... 
barre egiteko. 
Arbela zikinegia dagoenean 
berriz hasteko. 
Zeru pusketa bat, 
zeruaren argazki ez dena. 
Soberan hodei oro, imitaziorik ez! 
Ez onartu zeru apurtua baizik eta, 
zeru portatila. 
Foto-karnet itxurako zeru txiki bat, 
tolesgarria.