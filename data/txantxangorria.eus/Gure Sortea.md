---
id: tx-590
izenburua: Gure Sortea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/obO3NwVKV1M
---

Euskal herriko neska pollitak norapeit dira airatu
Seme sanoak ezkondu gabe bakar bakarrik gelditu
Mendi gaineko elur xuria etsaiek dute zikindu
Hori ikusi ama Euskadi nigar ahuenez da lotu
nigar ahuenez da lotu.

Zazpi probintzi zazpiak anai bat basteari so zauden
baina goiz baten haundi mundiek muga bat duten ezartzen
eta geroztik asko ta asko seme alabek ahantzi
ama bakarrak betidanikan ukan dugula Euskadi
ukan dugula Euskadi.

Erdar haizeak gogo bohotzak usu baititu nahasi
egia gordez sakelak betez ezkerrak karguduneri
baina halere ama Euskadi ez du etsitzen batere
baitaki semeek egiazkoek dutela zin zinez maite
dutela zin zinez maite.

Abertzalea ikaran dago mintzaira ez nahiz galdu
ez daki nola hamendik goiti beharko duen jokatu
Euskal arima arrotzek dute hiltzeari kondenatu
amaren maitez harmen bidea behar ote dugu hartu
behar ote dugu hartu.

Pasionea iragan ordu etorriko da eguna
gizon emazteek haur ala gazteek igurikatzen duguna
Euskal Herria ez da ez hilen baina Euskadi sortuko
gero sekulan nehork ez deusek ez baitu hiltzen ahalko
ez baitu hiltzen ahalko.