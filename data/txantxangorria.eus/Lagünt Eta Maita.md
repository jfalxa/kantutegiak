---
id: tx-198
izenburua: Lagünt Eta Maita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FjC1-kW6pFQ
---

Oi XIBEROA nigarrez ari zira, haurrak zütarik hürrüntzen beitira, 
Bena zure besuetat ar’ützüliko dira, zure bular nasaia pare gabe beita. 
Lagünt eta maita gure XIBEROA, 
Lagünt eta maita gure sor-lekia, 
Lagünt eta maita dantzari’ta kantariak, 
Lagünt eta maita gure talde ttipia ! 
Oi XIBEROA etzitila lotsa, zure batzea orano liliz beterik da, 
Ben’egün ezpadüzü eiten azia, desertü bat bezala, bihar, izanen zira. 
Nahi balinbadüzü ama XIBEROA, gure talde ttipiaren badüzü lagüntza, Herriko kantari’ta dantzari güziak prest dira, zure uhuratzeko oi gure maitea!
 
Hitzak: Dominika eta Jean ETCHART 
Müsika: Jean ETCHART