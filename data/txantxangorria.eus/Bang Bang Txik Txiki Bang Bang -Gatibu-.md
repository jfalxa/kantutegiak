---
id: tx-1387
izenburua: Bang Bang Txik Txiki Bang Bang -Gatibu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8FiemrAOXDE
---

Gehiago nahi dot eta
igo bolumena
gorputzek eskatzen deust
martxa, martxa

Bang-bang txik-txiki bang-bang
buelta ta bueltak eman
bang-bang akabu bako
dantzan, dantzan

Segi, segi, dantzan
ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Zelan begiratzen dozun
ta zelan mugitzen zaren
gauaren erregina
martxa, martxa

Bang-bang txik-txiki bang-bang
erritmoz beteta dago
bang-bang akabu bako
dantza, dantza

Segi, segi, dantzan
ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua

Bang-bang txik-txiki bang-bang
 txik-txiki bang-bang
 txik-txiki bang-bang...



Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua


Ta barre zoroak
 jaja, jaja
ta zure posturak
 sua, sua