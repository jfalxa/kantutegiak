---
id: tx-1052
izenburua: Esna Hadi
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xuSYHMQve5E
---

Mende askotan zehar etxeko atea irekita izan nuen beti
ez nion inori ere sarrera ukatu eta nuen apurra eskaintzen nuen
1948an hasi zen amesgaiztoa, uztak ostu, lurrak kendu
eta nire biztanleak etxetatik bota zituzten, bota zituzten

Mundua esna hadi, entzun zan nire garraxia
mundua esna hadi, sala zak nire egoera

Gazak eztu lorik egiten zeruak heriotza jaurtitzen dueako gauetan
Zisjordania ere kaiola bihurtu eta harresiz inguratu dute
Zatitu naute, okupatu naute, puskatu naute baina hala ere itxaropenez bizi naiz
eta itzuleraren giltza oraindik soinean, soinean daramat

Mundua esna hadi...


Holokaustoa ez da amaitu, oraindik jarraitzen du,
biktimak borreroaren papera hartu baitu
protagonista zarrak, SS berriak,
mendebaldeko herrien babesaren jabeak

Mundua esna hadi, entzun zan nire garraxia
mundua mugi hadi, aska zak nire lurraldea