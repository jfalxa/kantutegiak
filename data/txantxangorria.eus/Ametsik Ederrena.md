---
id: tx-1503
izenburua: Ametsik Ederrena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1DWQVIBDVns
---

Badiñotsut,
ez daigun pentsa
igarotako
egunetan.
Behingo minak
ta zoixonak.
Pasataku
pasata ra.

Bixitxi beti ru aurrea.

Ametsik ederrena,
egunik luziena,
musorik goxuenak
oindik ailateko raz.

Badiñue
burotso batzuk
matxasune
hiltxen dala.
Niri ezgata
hori pasaten.
Ixan be zeu za
neure launik onena.

Bixitxi beti ru aurrea.

Ametsik ederrena,
egunik luzeena,
musorik goxuenak
oindik ailateko raz.