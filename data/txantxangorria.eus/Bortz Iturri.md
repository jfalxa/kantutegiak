---
id: tx-467
izenburua: Bortz Iturri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ukzgq57d_8w
---

Bortz iturri baziren neure mendietan

bortz iturri kantari alai arroketan.

Edaten maite nuen heien ur hotzetan

hetarik baten kanta agortu da betan.

 

Bortz gorostiz egina zen neure oihana

nongo aire gaixtoak bota du gehiena?

Hil da neure zeruko izar eztiena

ta itzalak geroztik bete daut barrena.

 

Hil da uhain arina plaia bazterrean

hil da uso gaztea iratze gorrian

hil da neure umea bidegurutzean

bere primaderako lehen iguzkian.

 

Mundarraingo kaskoa arrokak inguru

mendirik ederrena dirudi gaztelu.

Bortz semen erdian nik banuen urgulu

zeruko Jainko Jauna, bortizki jo nauzu.