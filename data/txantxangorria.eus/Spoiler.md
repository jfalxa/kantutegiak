---
id: tx-1322
izenburua: Spoiler
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JrJj_1LqODw
---

*DIGITAL PRE-ORDER:

*Kantua erosi - Buy this song - Compra esta canción: 

************

Berri Txarrak-en 9. estudio lanaren bigarren aurrerapen kantua duzue 'Spoiler!'. 'INFRASOINUAK' diskoa Bill Stevenson eta Jason Livermorek ekoitzi dute eta 2017ko azaroaren 24an argitaratuko da, taldearen beraren zigilupean (Only In Dreams).


SPOILER! hitzak:

Spoiler! Spoiler!
Lapurtutakoa ez dute sekula itzuliko
ondoriorik ez duela jakin badakitelako

Spoiler! Spoiler!
Eszeptizismoa indartzen duen umorea baino ez da onartuko
eta orduan bai, irri egin denok

Gure alde ez dutela ezer egingo

Spoiler! Spoiler!
Aldiro zailagoa izango da sare honetatik askatzea
pozik armiarmak

Gu eguraldiaz nola 
halaxe mintzatuko da
neurri berean 
eguraldia gutaz

Gure alde ez dutela ezer egingo

Eta plurala egitean 
bakoitzak zer gu, zer haiek
ulertzen duen argitzea 
zaila izanen da ni-aren diktadurapean

Isolamendua izango da boladan jarriko duten izurritea
eta hori bera estaltzen asmatzea zure zeregin nagusiena
horretarako zure eskura milaka iragazki batera
zein epetan eta nola erabakitzeari deituko zaio askatasuna
eta behingoz orduan bai libre izango gara

Argi izan
plurala egitean 
bakoitzak zer gu, zer haiek 
ulertzen duen argitzea 
zaila izanen da ni-aren diktaduran

Spoiler! Spoiler!
Ez dugu itxarongo
ez dugu gehiago itxarongo

********
SPOILER! (Traducción letra castellano):

¡Spoiler! ¡Spoiler!
jamás devolverán lo robado
porque saben que no tiene consecuencias
¡Spoiler! ¡Spoiler!
solo se permitirá el humor que fortalezca el escepticismo
y entonces sí, nos reiremos todos

no harán nada por nosotros

¡Spoiler! ¡Spoiler!
cada vez será más difícil soltarse de esta red
(y las arañas tan felices)
la meteorología hablará de nosotros en la misma medida
que nosotros hablamos de ella

no harán nada por nosotros

y al utilizar el plural
será complicado dilucidar 
qué ‘nosotros’ y qué ‘ellos’ entiende cada uno
en esta dictadura del ‘yo’

la nueva plaga de moda será el aislamiento
y el más importante de tus quehaceres 
será conseguir que no se note
para eso tendrás miles de filtros a mano
se le llamará libertad a poder decidir en qué plazos y de qué manera 
y entonces sí, de una vez por todas seremos 
libres

¡Spoiler! ¡Spoiler!
no esperaremos
no esperaremos más