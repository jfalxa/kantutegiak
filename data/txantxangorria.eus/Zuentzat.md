---
id: tx-2965
izenburua: Zuentzat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MglEJqfnOdI
---

Zuentzat dira abesti honen lerroak, kantu honen leloa.
Zuentzat ere gure begirunea, besarkada beroena.
Zuentzat kantu bat osatzeko
ahalegin hau eta gure mirespen guztia.
Zuentzat ez da filmerik eginen,
telebistarik izanen Isiltasuna,
itsukeria eta egurra, besterik ez.
Dena den, denborak bakoitza bere lekuan ipintzen omen du.
Zuentzat da gure kantua, zuentzat da gure oihua, zuentzat.