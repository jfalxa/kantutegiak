---
id: tx-1841
izenburua: Haur Kanta Ii Iii
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AZIE9Xcf_ME
---

Zu bezelako haurrek
lo egin dezaten,
ilargia hasi da
zerua pintatzen.

Txoriak lotan daude
hagitz da berandu,
ilargiak erreka
erdia edan du. 


Txoriak lotan daude
hagitz da berandu,
ilargiak erreka
erdia edan du.

Izarrak hasi dira
zeruan musuka,
ilargia zelatan
atzetik kukuka.

Izar dago zerua
honuntza begira:
sehaska kulunkatzen
noiz hasiko dira?


Izar dago zerua
honuntza begira:
sehaska kulunkatzen
noiz hasiko dira?

Etzaittu eramango
mamuak aidean,
sehaskan lo egiten
duzun bitartean.

Goiko lepo gainean
ilargia duzu,
zuregana heldu da
ematera musu.

Goiko lepo gainean
ilargia duzu,
zuregana heldu da
ematera musu.

Zaude lo goxo-goxo
atorri artio,
zuretzat kollare bat 
ahaztu baitzaio.

Kollarea jantzita
eramango zaitu,
bere lagun guztiak
urruti baitittu.

Kollarea jantzita
eramango zaitu,
bere lagun guztiak
urruti baitittu.

Ilargitik zintzilik
ibiliko zera,
kolunpio batean
izarren antzera.

Zeruak ikustean
pentsatuko duzu:
"Holako lagunekin
nor ez zoriontsu?"

Zeruak ikustean
pentsatuko duzu:
"Holako lagunekin
nor ez zoriontsu?"

Eta lagun onena
ekarriko dizu:
"Tori izar bat zuretzat
gustatzen bazaizu"

Jostatuko zerate
txirrista batean,
eguzkiak argia
esnatu artean.

Jostatuko zerate
txirrista batean,
eguzkiak argia
esnatu artean.

(Mikel Arregi - Imanol)