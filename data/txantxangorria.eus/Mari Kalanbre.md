---
id: tx-1554
izenburua: Mari Kalanbre
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e4U52_5JQVI
---

Konposizioak, moldaerak zein programaketa: XABIER ZABALA

Aurpegian laurehun muskulu lanean,
Malkoak isuriz, begiak garbituz.
Inmunoglobulina listuan,
infeziorik ez ahoan.
Bibra, bibra, bibra
Biba bibrazioak!
Askatu bizkarreko tentsioak!
Diafragma dantzan,
poza digestioan.
12 aire litro birikietan,
arnasa firin-faran.
Serotonina, dopamina, adrenalina,
Tristra sendatzeko aspirina 
zientizak dio hori,
egin irri!

Kable ta alanbre,
mari Kalanbre,
Zirti ta zarta
neska aparta!

Amaren sabel barruan ere
banuen hainbat dohain ta boere,
amagandik jaso nituen;
hark erakutsi zidan erabiltzen.
Txinparta-pindara ibiltzen naiz
gau ilunenak argituz,
tximista, trumoi eta burrunba,
ametsak esnatuz...

Kable ta alanbre,
mari Kalanbre,
Zirti ta zarta
neska aparta!

Kable ta alanbre,