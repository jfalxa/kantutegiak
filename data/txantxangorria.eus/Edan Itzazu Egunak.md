---
id: tx-3011
izenburua: Edan Itzazu Egunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-azhX6EWvGE
---

Edan itzazu egunak
horditu zaitez bizitzaz
jabe zaitez zeu zarenaz.

Ikusten ez baduzu ere
onura etorriko zaizu
izan zaite esperantza.

Bizitzaren joanean
dena kentzen bazaituzte
jabe zaitez zeu zarenaz.

Daukazuna hutsa bada
zeuk egin daikezunaz
iritsiko zaizu garaipena.
Garaipena!

Gezur kiratsak barreiatuz
zeure izena lurperatuz
denek abandona zaitzakete.

Bakardadera kondenatuz
inoren babesik gabe
zeure kontra jar daitezke.

Bizitzaren ertzeraino
halabeharrez modu txarrez
eraman zaitzakete.

Edan itzazu egunak
erabakia zeugan duzu
zentzu berrien jardueran.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
si/nez ezazu zeugan.


Edan itzazu egunak
zaren horri_eutsi eiozu
egiak libratuko zaitu.

Ekiozu gogo biziz,
egingo duzun ahaleginez
emariak izango dituzu.

Oztendurik bada ere
zaude lasai, egon ziur
aukera etorriko zaizu.

Edan itzazu egunak
orain hutsik bazaude ere
geroa zeure esku duzu.

zeure esku duzu.


Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.

Ez ezazu bildurrik izan
sinez ezazu zeugan.