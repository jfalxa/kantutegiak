---
id: tx-2644
izenburua: Batzuetan Ixilik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/q2b_AQrkP3I
---

Batzuetan ixilik
ondo entzuten da
guzurrik handiena
egi bihurtzen da

Mundua hankaz gora
dagoen honetan
haizea zoro dabil
txorien menean

Etxe guztietan dago 
beti hutsunen bat
ohe ixila eta
leiho zarratua

Esaiozu aitari
sua isiotzeko
laster noala eta 
trankil egoteko

Batzuetan ixilik
egoten naz ondo
guzurrik handiena
ez da betirako

Esaiozu aitari
sua isiotzeko
laster noala eta 
trankil egoteko