---
id: tx-119
izenburua: Ohidürak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/o967J0rC2Ec
---

Ogia eihean ehoazirik, ina molinetan behexirik, 
Ohatzia lan handia, altxatüazia nahasirik, 
Labia gorri bada, bertan date ogia errazirik, 
Mestüa ez ahatziz, hun beita esnian sopatürik. 
Begira dezagün bethi goguan gaiza hun horien gozua ! 
Goiz-arrats eznia koxila jeixten, kobrezko bertzin beroazten, Kallatatzen zen berhala axuri gatzagia ezartez, 
Gazna biltzen eta gero gaztanaxalian tinkatzen, 
Artzainak arrakasta handia senbeki zelarik iraixten. 
Lagüngua hanitx miñabesetan, xaxkitaka mahatsa biltzen, Ezkatzin, aita aldiz, züganila xehekatzen, 
Egün zunbait heakazten eta ardu huna elkitzen, 
Graspa brentsin tinkatzen, hurarekin minata gozatzen. 
Hitzak : Beñat QUEHEILLE eta Osaba Batista 
Müsika : Jean ETCHART