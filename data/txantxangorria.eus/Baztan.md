---
id: tx-411
izenburua: Baztan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Km6jVOTVC2Q
---

Baztan (Delirium Tremens) Mikel Laboa

Ez dakit garbi noiz
bainan, egun sentiarekin
izango zen
Baztango bazterrak
isilka
nere logelan hasi ziren
hasi ziren
sartzen eta sartzen
lekurik ezak, nunbait
esnatu ninduen
segituan jautsi nintzen
nun nengoen
leihoa ireki nuen
eta emeki emeki
Baztango lurrean
Baztan baitan
Baztan izan nintzen