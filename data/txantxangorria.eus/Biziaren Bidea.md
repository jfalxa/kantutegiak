---
id: tx-586
izenburua: Biziaren Bidea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2-O2PrEsmLM
---

Bizia kurritzen da bide bat bezala
Batean xüxenean, bestean bühürrian
Ez dü beti segitzen abialde nahia
Bena, nontik nahi den, aitzina badoa

Har dezagün biziaren bidea
Soegin gabe denbora iragana
Bakoitxak jarraik bere bidea
Aitzina aitzina bagoatza

Nahi badügü jakin norat giren joaiten
ikasi behar dügü nontik horra giren
Lehenagoko aitek xendak egin zütüen
Sail horietan gira orai ebiliren

Bide hortan paseiatzez maleruski agitüko
Zonbaiten bidajea bazterrean baratüko
Indar begira dezagün aitzina joaiteko
Biharamen zorihontsü badateke franko

Segürki betidanik badügü entzünik
Lür hontan ez dügüla izate bat baizik
Ordüan etxek dezagün bizia azkarrik
Opari ederrena da jinkoarenganik