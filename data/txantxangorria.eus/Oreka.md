---
id: tx-2978
izenburua: Oreka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/d4AWj7QVvHI
---

Esnatu berria naiz eta ez dut ireki nahi

libratuko omen nauen ate sorta

leihoekin aski zait

ez dago inor gure zain

jendez gainezka dagoen kale horretan.

Argia profitatuz amets bat idatzi dut

herri nekatu honen azalean

ea irauten duen,

badakizue hemen memoria ezabatzen digutela

Tristea da dena ospatu behar hau

oraindikan dena lortzeko badago

orekak ez du balio lurrean zaudenean

orekak ez du balio aspaldi jausi zarenean

Gure gezur propioak sinetsi ditugula

besteen gezurrekin aspertuta

lider bat behar dugula dio

liderra izan nahi duenak

Argia profitatuz amets bat idatzi dut

herri nekatu honen azalean

Lider bat behar dugula dio

liderra izan nahi duenak

Tristea da dena ospatu behar hau

oraindikan dena lortzeko badago

orekak ez du balio lurrean zaudenean

orekak ez du balio aspaldi jausi zarenean