---
id: tx-2911
izenburua: Iparragirre Abila Dela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/1tnHKBTjtnI
---

Iparragirre abila dela
askori diot aditzen,
eskola ona eta musika
hori hoiekin serbitzen.
Ni ez nazu ibiltzen
kantuz dirua biltzen,
komeriante moduan.
Debalde festa preparatzen det
gogua dedan orduan.


Eskola ona eta musika
bertsolaria gainera,
gu ere zerbait izango gera
horla hornitzen bagera.
Atoz gure kalera
baserritar legera
musika hoiek utzita
Errenterian bizi naiz eta
egin zaidazu bisita.