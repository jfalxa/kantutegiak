---
id: tx-2564
izenburua: Zen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FJrgPjFbR6c
---

Eneko Gil dantzariaren inprobisazioetan oinarritutako bideoklipa da honakoa. ZEN izena du abestiak eta Beira taldearen hirugarren diskoko lehen abestia da. Braillean izena du diskoak.



Herri zahar hura bizirauteko
zitekeenik bortitzen zen
mapa munditan, kolore gabe,
baztertuta gelditzen zen
Goizero sortu, gauero lehertu,
ta goizero berritzen zen 
herri zahar hura mapamundiko
tolestura sentitzen zen.

Herri zahar hartan askatasuna
borrokatzat ulertzen zen
eta bakea, porrota baitzen,
berehala baztertzen zen.
Gudu zelaitan bonba bat eta
gero beste bat lehertzen zen,
baina tarteka lokatz artean
arrosa bat agertzen zen. 

Herri zahar hura kristal gainean
oinutsirik ibiltzen zen.
Trumoi soinua eta ondoren
nazio bat isiltzen zen.
Gero damuak, eta mamuak...
Bere baitara biltzen zen.
Herri zahar hartan hiltzen zuena
hiltzen zuena hiltzen zen.

Izango zenaz liluratuta
bihurtu zenaz ikaratzen zen.

Bere itsasoa husten zen eta
bere basoa soiltzen zen
ta lurra malkoz busti ondoren
begi-azpia lohitzen zen.
Gizarajoa bere negarrek
eraginik erdoiltzen zen
Herri zahar hura galdu zituen
pertsonekin oroitzen zen.

Errenteriako mural batean grabatutakoak dira dantzako irudi guztiak. Milesker bertako Udalari.