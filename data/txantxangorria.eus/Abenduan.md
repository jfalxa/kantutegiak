---
id: tx-399
izenburua: Abenduan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Bo-7YXDw6p0
---

Isfahaneko basamortu elurtuan
ilunabar garden hura.

Laku guzien urak isla dakar
mugiezi irla bakar.

Udazken margula zena
kantatu nizun zuri,
zuhaitz zaharkitu honek
fruiturik ez du isuri.
Hotza sentitzen du han,
abenduan.

Abenduan...

Karabenserai lasai
galdu batean
haizeak darama zeta.

Espezia usainak hartua da
Teherango bazar itzela.

Udazken margula zen
kantatu nizun zuri,
zuhaitz zaharkitu honek
fruiturik ez du isuri.
Hotza sentitzen du han, 
abenduan.

Abenduan...