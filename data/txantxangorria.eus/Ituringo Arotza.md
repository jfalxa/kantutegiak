---
id: tx-611
izenburua: Ituringo Arotza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oRG3cW37xPg
---

Ituringo arotza, Erramun Joakin,
asarre omen zaude zeren degun jakin.
Santurik ez laiteke fiatu zurekin:
San Kristobal urtuta joalik egin.

Arotzak erran dio bere andreari:
Hurtu behar dinagu; ekarran Santu ori.
Gizona, zertat zoaz? Bekatu da ori.
Etzionagu erranen sekulan ni hori.

Ituringo garaile Ramuntxo Joakin
asarre omen zira zeren dudan jakin.
Konfesa zaite ongi erretorarekin:
ez dute zer fidatu Santuek zurekin.

Kobrezko Santurikan inon bazarete,
egoten al zarete emendik aparte;
baldin arotz oriek jakiten badute,
garien egiteko urtuko zaituzte.