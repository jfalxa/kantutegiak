---
id: tx-928
izenburua: Sentipenak Askatu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EbBkP4LNyEw
---

YOGURINHA BOROVA Sentipenak Askatu (GOR 2019)

DIMAKO ESKOLA
Amaiur Fernandez, Garazi Agirrezabala, Irkus García, Pello Agirre,
Maialen Agirre, Julen Artabe, Izaro Basterra, Arri Pujana, Kattalin Pujana,
Ilargi Uriarte, Alluitz Aierdi, Paule Azurmendi, Keila Pinedo, Aimar Pinedo,
Aiora Gauna, Nora Gauna, Amaiur Gorrotxategi,
Ezaia Torrontegi, Malen Azurmendi, Eñaut Atxaga, Aiur Gallastegi,
Enara Gallastegi, Ane Gorospe, Martin Latatu, Andere Latatu,
Lier Agirrezabala, Izar Azurmendi, Ilargi Azurmendi, Nile Baraietxaburu,
Eñaut Baraietxaburu, Ane Belasko, Jone Beloki, Ainize Eguzkiza,
Iradi Iza, Jare Iza, Ainara Sebastian, Enaitz Gorospe, Sugoi Gorospe,
Udane Ubeda, Arrene Leonardo, Maddi Etxenausia,
Magali Lekue, Etxaun Olazabalaga, Teo Etxezarraga.

#Yogurinha_Borova  #Sentipenak_Askatu #Gor_Discos 

WILD DANCE PROJECT
Josu Collabo, Xabier Gomez,
Ariane Menendez, Leire Del Pozo, 
Oier González, Jon García,
Jonay Alonso, Jon Muñoz,
Mara Irastorza, Haritz Castro.
Jonathan Gómez eta Itxaso Maestre.

Miribillako parkeko grabaketa
Ilargi Argoitia, Nora Muswally,
Lua Ventola, Maren Itza, Kimetz Santamaría,
Victor Manuel Santamaría, Bea Majuelo.

Abesti originala : Bihotz Gorospe eta Mikel Inun.
Grabaketa eta Nahasketak: Rubén G. Mateos.
Jantziak: Miguel Biurrun, Idoia Gaviña, Kahlorias.
Betaurrekoak: Atelier Maldeojo / Bilbo.
Diskaren Diseinua : Higi Vandis.
Argazkigintza : Sergio Marcos.

Bideo Grabaketa: Jose María Bobi eta Alex Argoitia.
Edizioa eta Posprodukzioa: Alex Argoitia.

ESKERRAK
Saioa González
Dimako Eskola
Dimako Udala
Dimako Eskolako Guraso Elkartea
GOR Diskak
Yogurinha Borovaren Familia
Diane Sidney Bakuraira




SENTIPENAK ASKATU
 
BARTOLOK EZ DU MARI
BERE AMETS GOZOETAN
BAI ORDEA JOSE MARI
HAU MAITE DU BENETAN
ZER DIREN ESAN EZINIK
DABILTZ EZKUTAKETAN
ETA TRISTURAZ BETERIK
BIZI DIRA PENETAN
 
 
JUNEK ANE MAITE DU
BEGIETAN ZAIO IGARRI
EGUNERO ESATEN DIZKIOTE
GAUZA POLITAK ELKARRI
BIDEAN AURKITU ARREN
HAMAIKA TRABA ETA HARRI
EUREN MAITASUNA EUSTEN DUTE
GOGOR, BURU-BELARRI
 
 
MAITATU BIHOTZEZ
INOIZ EZ EZKUTATU
SENTIPENAK ASKATU
ZARENA KANPORATU
 
Bartolo no tiene a Mari en sus dulces sueños
él sueña con Jose Mari,
le ama.
Pero no lo pueden decir,
se esconden,
y viven tristes y desolados.
 
June ama a Ane,
se nota en su mirada.
A diario se dicen cosas bonitas
 y aunque encuentren piedras y obstáculos en su camino
alimentan su amor
con tenacidad y determinación.
 
Ama de corazón
nunca te escondas
libera tus sentimientos
muéstrate tal y como eres.
 
ESTRIBILLO en INGLES :
 
Love with your heart
And never hide
Release your feelings
Come out as you are