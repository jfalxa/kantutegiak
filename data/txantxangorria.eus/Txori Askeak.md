---
id: tx-2316
izenburua: Txori Askeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gGhDal0Op44
---

Grabado, mezclado y masterizado en el estudio Sound of Sirens de Iruña a manos de Julen Urzaiz en Noviembre del 2017.


LETRA:

Inguruko kaleetan ez dago aukerarik
Gehiengoak gogoko duenari esateko ezetzik
Ordainetan biziko zara kritiken ihesi
Hemen ez dago zuretzako tokirik

Itxurakeria ostegun ta larunbatero
Jendearen nortasuna non ote dago?
Arropa garestiak haien hutsuneak estaltzeko
Prest zurrumurru faltsuak zabaltzeko

Prestatu gabe ez atera kalera
Begiratuko zaituzte mesprezuz goitik behera
Beti besten seme alabak epaitzen
Euren bizi tristetan usteltzen 

EZ DUTE ULERTU NAHI EZ DELA ADIN KONTUA
EZ DUGULA BIZI NAHI HAIEN MODURA
KAIOLAN SARTU ARREN GURE AMETSAK
BETI IZANGO GARELA TXORI ASKEAK