---
id: tx-3229
izenburua: Perez Sagutxoa -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/otVsznoa9ww
---

Hortz bat erori zait
askaria hartzen
amatxorengana joan naiz
lehenbailen
Erakutsi diot,
esan dit eman
gauez gordetzeko 
burukopean

Perez sagutxo isil-isila
hanka puntetan datorkit bila
Perez sagutxo xarmangarria
hortz baten truke utzi oparia

Pijama jantzi dut
hortza gordeta
ohean sartu naiz
ilusioz beteta
Argia itzaltzera
ez naiz ausartu
urduri nago
eta ezin lokartu

Perez sagutxo isil-isila
hanka puntetan datorkit bila
Perez sagutxo xarmangarria
hortz baten truke utzi oparia

Esnatzerakoan
ezustekoa
ohetik jaikita
altxa burukoa
Azpian panpina
zoragarria
esker mila lagun
maitagarria!

Perez sagutxo isil-isila
hanka puntetan datorkit bila
Perez sagutxo xarmangarria
hortz baten truke utzi oparia

Perez sagutxo isil-isila
hanka puntetan datorkit bila
Perez sagutxo xarmangarria
hortz baten truke utzi oparia