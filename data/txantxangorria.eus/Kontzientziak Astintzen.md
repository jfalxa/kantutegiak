---
id: tx-1046
izenburua: Kontzientziak Astintzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nmDRVt-pJiE
---

KONTZIENTZIAK ASTINTZEN
Etxetik irten orduko antzematen da gauza denak hankaz gora daudela
Jende dena kalean barrena aurrera Itsututa zilborrari begira
Pentsatzea delitu den errealitate hau elikatzen dut egunero
Prentsa arrosan agertzen den pailazorik handienaDaukat nik ídolo

NI ONDO BIZI BANAIZ BESTEAK BOST AXOLA
GEHIAGO IZAN AHAL BADUT LAGUNA IZORRA DADILA!

Guztia kritikatzen sofan eserita gustora pasatzen ditut nik orduak
baina atxiloketak, tortura edo dispertsioa ez dira nire kontuak
Indiferentzia honetan murgilduta Lasai gabiltzaz guztiok kalian
Senideak milaka kilometrotara usteltzen ari diren bitartian

NI ONDO BIZI BANAIZ BESTEAK BOST AXOLA
GEHIAGO IZAN AHAL BADUT LAGUNA IZORRA DADILA!
KONTZIENTZIAK ASTINTZEN ,ENE BURUARI  GERRA DEKLARATZEN

Txikitatik erakutsi digute ixilik egotiaren baliyua
Belaunikatu zaitez, musu iperdian eta zeuretzako duzu mundua
Ez dut ikusi nahi lapur handienek korbata darmatela gaur egunian
Makina bat sarraski eginak dituzte haiek garapenaren izenian

NI ONDO BIZI BANAIZ BESTEAK BOST AXOLA
GEHIAGO IZAN AHAL BADUT LAGUNA IZORRA DADILA!
KONTZIENTZIAK,  ASTINTZEN ENE BURUARI  GERRA DEKLARATZEN