---
id: tx-1890
izenburua: Oriko Xoria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gaDUsqtlbmw
---

Jende gaztea dabila 
etxetik urrun lanila... 
ez erran galdurik dela, 
itzul ditaike berehala.

Jin bedi gure artela 
Oriko xoria Orila! (bis)

Xorittoaren hegala 
arin aidea bezala... 
Gazteek begira dezatela 
azkar euskaldun odola!

Jin bedi gure...

Xoriak jiten zaizkigula, 
huts egin gabe sekula... 
Lahenik bat, gero mila, 
txortak egiten ixtila.

Jin bedi gure...

Xoria izan dadila 
euskaldun gazteen modela! 
Mutiko eta neskatila, 
ziauste denak sorlekula!

Jin bedi gure... (lau aldiz)