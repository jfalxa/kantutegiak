---
id: tx-1782
izenburua: Kaixo, Kaixo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2sXwiz5hoPU
---

Kaixo kaixo,
nor da bestaldean
Bai, esan, erantzun
telefonoari
Zu al zara
bihotzeko kuttun
Zurea nirea
elkartu ditzagun
Hitz egin

Kaixo, kaixo, ni naiz aizu
Zer moduz?
Ongi, ongi, bai eta zu?
Primeran
Abestu nahi al duzu?
Zurekin?
Zain naukazu!
Uouououo

Kilimak belarrietan
telefonoak jotzean
Kilimak belarrietan
zure ahotsa entzutean
Berritsuak, bihurriak
lagun dibertigarriak
Telefono jostari
ameslariak

Kaixo kaixo,
nor da bestaldean...

Kaixo, kaixo, ni naiz aizu...

Blibliblabla aritzeko
kontutxoak kontatzeko
Blibliblabla aritzeko
maite zaitut esateko
Berritsuak, bihurriak
lagun dibertigarriak
Telefono jostari
ameslariak

Kaixo kaixo,
nor da bestaldean...

Kaixo, kaixo, ni naiz aizu...

Kaixo, kaixo, ni naiz aizu...

Kaixo, kaixo, ni naiz aizu...