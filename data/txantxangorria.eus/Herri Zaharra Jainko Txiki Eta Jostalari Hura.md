---
id: tx-1765
izenburua: Herri Zaharra Jainko Txiki Eta Jostalari Hura
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OMByMCPchRw
---

Mikel Urdangarin
Kirmen Uribe
Rafa Rueda
Bingen Mendizabal
Mikel Valverde

Lurra eta itsasoa batera doaz
horrela zioen marinel zaharrak
arrantzarik ez baratzan suan
kimuak ernetzean orduantxe
gerturatzen arrainak kostara
bietan behera ei da aroa
eta herri zaharra da gurea
gure berbe badituzte milaka urte
eta herri zaharra da gurea
baina badakigu elkar ondo hartzen.

Luzatu da uda lehen hotza
kostatu bazterrak epeltzea
fruituak lotsaz itsasoloetan
hodeiak azkar doaz zeruan
lauso umelak badoaz lehortzen
hodeiak azkar doaz zeruan
eta badakigu elkar ondo hartzen.

Nagoen lekuan geratuko naiz
lehorra behar du itsasoak
hala norberak lagun urkoa
eta herri zaharra da gurea
gure berbe badituzte milaka urte
oinazea ere  bada gurea
baina badakigu elkar ondo hartzen
hodeiak azkar doaz zeruan
lauso umelak badoaz lehortzen
eguzkibegia da zeruan 
eta badakigu elkar ondo hartzen.