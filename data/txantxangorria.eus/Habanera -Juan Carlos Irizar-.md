---
id: tx-3235
izenburua: Habanera -Juan Carlos Irizar-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/k6tglWVKc9A
---

Berriro itzuliko balitz
iragan denbora arrotza
berdin kontsumi nezake
banilla gozo artean,
itsaso urrun batetan
irudimena galdurik
udaberriko euritan
larrosak pizten ikusiz.

Osaba komertzianterik
ez nuen izan Habanan,
pianorik ez zegoen
bizi nintzen etxe hartan,
neskatxen puntilla fiûak
udako arratsaldetan,
errosario santua
neguko gela hotzean.

Ezpainek gordetzen dute
ezpain bustien gustoa
desiozko hotzikaran
etsipenaren tamalez,
gaua zelatan dakusat
kontzientzia bilutsik
badoaz orduz geldiak
gogorapenen hegalez.

Jaio giûen, bizi gera
ez dugu ezer eskatzen
itsasontzia astiro
kaiatik ari da urruntzen.
Antillak zintzilik daude
argazkien paretetan
karta bat idatziko dut
norbaitek erantzun dezan.

Tabako, ron ta kanelaz
girotutako arratsetan
algarak entzuten ziren
Habanako putetxetan,
abanikodun mulatak
gauari haize egiten
musiken aide nagiek
odola erretzen zuten.

Jaio giûen, bizi gera
ez dugu ezer eskatzen
itsasontzia astiro
kaiatik ari da urruntzen.
Antillak zintzilik daude
argazkien paretetan
karta bat idatziko dut
norbaitek erantzun dezan.