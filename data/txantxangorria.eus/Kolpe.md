---
id: tx-587
izenburua: Kolpe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7KCc0s9F5DU
---

Hau da jokoa
Honarte nahikoa
Kalea zaitut badakit
Nahi dudana jantzi gaur lagunok hautsi behar dugu gaua erditik
Aizu gaur ez dut nahi negarrik
Ez lerdo bat ohean sartuta
Perreoa nahi dut hasi eta buka
Nire ahizpekin bakarrik
Kolpe eta kolpe bakoitzak tope
Gasteizen izarrek diote
Erre ezazu zerua
Kolpe eta kolpe bakoitzak tope
Gasteizen izarrek diote
Erre ezazu zerua
Orain geu gara dantzan
Gozatuz gure martxan
Jarraitzen dugu dantzan
Oeee
Ez ez ez ez gaur ez goaz hemendik hieska
Ez ez ez ez hau barkatu ez da zure festa
Ez nago jasateko prest
Ardiz jantzitako otso bat zara eta ez dizut sinisten
Ohean zarela nik ez dakit zer
Zurekin ez banoa ni ez naizela ezer
Hor dauzkat lagunak pizti kuttunak
Eta es dut behar inork esatea
Badakit maltzurra naizela
Goxoa krudela zuk esan gabe
Aizu gaur ez dut nahi negarrik
Ez lerdo bat ohean sartuta
Perreoa nahi dut hasi eta buka
Nire ahizpekin bakarrik
Kolpe eta kolpe bakoitzak tope
Gasteizen izarrek diote
Erre ezazu zerua
Kolpe eta kolpe bakoitzak tope
Gasteizen izarrek diote
Erre ezazu zerua
Orain geu gara dantzan
Gozatuz gure martxan
Jarraitzen dugu dantzan
Oeee