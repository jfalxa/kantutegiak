---
id: tx-20
izenburua: Ze Polittak Diran, Loriak Zelayian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mV2eqe-JYFg
---

Ai que bonitas son, las flores en el campo
Ai que bonitos son, los cerdos en la jaula
Ai que bonitas son, las chicas de mi pueblo
Zahartzen diranian, esaten dute
Tutilis mangilis tutilis yes.
Lara la la ...

Ze polittak diran, loriak zelayian
Ze polittak dira, txerriak kayolian
Ze polittak diran, hemengo neskatilak
Zahartzen diranian, esaten dute
Tutilis mangilis tutilis yes.
Lara la la ...

O que sont trés jolies, les fleurs dans la canpagne
O que sont trés jolies, les cochons dans la cage
O que sont trés jolies, les filles de ma ville
Zahartzen diranian, esaten dute
Tutilis mangilis tutilis yes.
Lara la la ...