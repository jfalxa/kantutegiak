---
id: tx-961
izenburua: Larrosa Beltza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tyj-bZsbA0w
---

LARROSA BELTZA (Hitzak)

Urteak hamairu ilargi
Eta bizitzak hamabi
hamaika zuretzako eta
hamabigarrena nik, 
eta gelditzen den hori
biontzat erdiz erdi, biontzat erdiz erdi

Har ezazu Larrosa beltz bat
Ametsen lorategitik
Gauaren amildegi ertza
Hasten den muturretik
Har zazu Larrosa eta
Ez galdetu zergatik

Gau hori iristen ez bada
Zertarako ilargia
Iluntasuna da gaua
Ta zu nere argia

Zer ekarri dezu niretzat
Gauaren beste aldetik
Nora joaten dira ametsak,
gure beso artetik?
Noiz dira zuri larrosak, 
beltzak izan aurretik

Larrosa beltz bat betirako
Larrosa beltz bat zuri
Larrosa beltza delako
Eta ilargia zuri

Gau hori iristen ez bada, 
zertarako ilargia
Iluntasuna da gaua, 
ta zu nere argia, ta zu nere argia...