---
id: tx-3292
izenburua: Eskarabileraren Kanta -Honat-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/eGXlc01Yv4w
---

Zabaldu ahoa ta eman ozen berria airean

zabaldu bihotza ta eman ozen berria airean

haize berriak datoz bultzaka herriko kaleetan

gogo berriak herritar guztion bihotzetan.  

Zapi ta porroiak atera

Basauriko herria kalera

ohartu barik urtebete pasa dela

jaia beste behian hasiko da.  



Eskarabilera burua ez galdu

auskalo non topako dugun

itsasoan galduta edo atzerrian

basauriko kaletan botata.



Zurrakapote tantak soinean

txosnarik txosna goazen parrandan

eguzkia irtenda banoa ohera

bihar berriz egongo gara.  

Kaleko ikatz zatiak biltzen, txiki direnak batera handitzen

bidea eginez, kalerik kale, herriko festak indar berritzen

anaitasuna da zuen ardura, euskera hortzean egun eta gauean

banatu irria, izan bizia, adi begira!