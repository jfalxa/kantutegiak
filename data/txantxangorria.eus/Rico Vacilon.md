---
id: tx-906
izenburua: Rico Vacilon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qJokZ6-4Vgo
---

Vacilón, que rico vacilón
Vacilón
Txa, txa, txa
Que rico txa, txa, txa.

Beltzarana behar da maitatu
txinarra bada besarkatu
ta ile horia muxukatu
baina denek nahi dute probatu.

Larruzko beroki bat eskatu
ilargia bai desiratu
gaztelu eder bat enkargatu
baina denek nahi dute probatu.

Batel aurpegi polita badu
besteak xagu-sudurra du
hauek naute beti probokatu
baina denek nahi dute probatu.