---
id: tx-1816
izenburua: Esaiok
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kzvAIXYsSfA
---

Zenbat garagar errekak pasa behar
oraindik erabaki arte
ehiztari herabetiak hire tiroen
beldurrez nora ezean.

Garaia duk, motel, esaiok,
ez pentsa gehio, esaiok
amodioaren aurrean egia lotsagabe
esaten ikasi behar duk.

Ta ni gau guztia nabil, zuen atzetik
lagundu nahirik, zuen atzetik
mozkortu egin nauzue eta berdin zait
non lo egin edo norekin. (bis)

Neska horrek seguraski nahi dik
hire onduan gau hontan egotea
neska horrek seguraski nahi dik
hire ahotikan hori entzutea
neska horrek seguraski nahi dik
bildurrik gabe hik hori esatea
neska horrek seguraski ere desio
berdinak bete nahian zebilek
ia ba, esaiok ...

Ez pentsa gehio
amodioaren aurrean egia lotsagabe
esaten ikasi behar duk.

Ta ni gau guztia nabil, zuen atzetik
lagundu nahirik, zuen atzetik
mozkortu egin nauzue eta berdin zait
non lo egin edo norekin. (bis)

(J. Zabala)