---
id: tx-1973
izenburua: Itsasoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VCyzP7w5fXE
---

Itsasoa, hain urruti... 
sentimenak horren hurbil... 

Denbora da ez dugula hartzen kafetxorik, 
atzoko azken tabernetan ez zintudan ikusi. 
Zure lagunez oroitzen al zara gutxi bada ere? 
besarkada bat eta jarrai bizirik.