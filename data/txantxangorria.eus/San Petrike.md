---
id: tx-835
izenburua: San Petrike
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/s6wvdgNmWOE
---

Oin punta batez
besteno batez
zutik eta ingura

oin punta
erino batez
besteno batez.
Zutik eta ingura.

Oin punta...
erino...
belauno batez,
besteno batez.
Zutik eta ingura.

Oin punta
erino
belauno
ukondo batez,
besteno batez.
Zutik eta ingura.

Oin punta
erino
belauno
ukondo,

ipurdi batez
besteno batez.
Zutik eta ingura

Oin punta
erino
belauno
ukondo,
ipurdi
soinburu batez
besteno batez.
eta itzulipurdi.