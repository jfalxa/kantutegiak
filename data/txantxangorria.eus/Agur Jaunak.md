---
id: tx-2997
izenburua: Agur Jaunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cKTlctRCcac
---

Agur Jaunak
Jaunak agur,
agur t'erdi
Danak Jainkoak
eiñak gire
zuek eta
bai gu ere.

Agur Jaunak,
agur,
agur t'erdi,
hemen gera,
Agur Jaunak.