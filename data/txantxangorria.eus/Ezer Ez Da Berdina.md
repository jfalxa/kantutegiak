---
id: tx-375
izenburua: Ezer Ez Da Berdina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Cw4AU7Pjps0
---

------------------------------------------------------------------------
Hainbeste urte zu gabe
Eta nork esango zuen
Irailak hamabi batean
Aurkituko ginela

Eta gauzak nola diren
Ea ahaztu nuen
Nola baretzen nauten
Zure begi berdeak

Zenbat oroitzen dudan
Durangoko festetan
Goizeko argia
Zure ezpainen uretan

Zenbat aldatu diren
Gauzak urte gutxitan
Urrundu ginen eta

Ezer ez da berdina
Gure kantu gordinak
Hutsik daude
Hutsik daude
Zu gabe

Bai
Nork esango zuen bada
Piztu genuen sutan
Erreko ginela
Eta gauzak nola diren
Ia ahaztu nuen
Nola baretzen nauten
Zure begi berdeak
Zenbat oroitzen dudan
Durango festetan
Goizeko argia
Zure ezpainen uretan

Zenbat aldatu diren
Gauzak urte gutxitan
Urrundu ginen eta

Ezer ez da berdina
Gure kantu gordinak
Hutsik daude
Hutsik daude
Ezer ez da berdina
Gure kantu gordinak
Hutsik daude
Hutsik daude zu gabe

Saiatu naizen arren
Saiatu naizen arren
Ez dizut idatzi nahi beste
Saiatu naizen arren
Saiatu naizen arren
Ez da egunik zu faltan bota gabe

Ezer ez da berdina
Gure kantu gordinak
Hutsik daude
Hutsik daude
Ezer ez da berdina
Gure kantu gordinak
Hutsik daude
Hutsik daude zu gabe

------------------------------------------------------------------------
Zuzendaria: Iratxe Reparaz

Argazki zuzendaria: KORATGE
Kamara laguntzailea: Nestor Costa
Kamara Auxiliarra: Carlos Barroso

Muntaia: Joan Soler
Kolorea: Jaime Venegas

Produkzio zuzendaria: Joseba Razquin

Jantziak: Eneko Aranbarri
Makillajea: Laura Lesark

Grafismoa: Gorka Larcan

Musika eta hitzak: BULEGO
Musika ekoizpena: Eñaut Gaztañaga eta Tomas Lizarazu
Musika grabazioa eta masterizazioa: Gaztain Estudioak

Esker Bereziak:
Panda Artist, Oso Polita.