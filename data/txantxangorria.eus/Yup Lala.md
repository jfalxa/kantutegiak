---
id: tx-1917
izenburua: Yup Lala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zTK7p3a7OKY
---

Jende lasterkatzaile krudela 
bera die igorri... 
Yup! la-la. 
Ainguriak otoituz berehala 
hola hola izorra zadila. 
Jende lasterkatzaile krudela 
bera die igorri... 
Yup! la-la. 
Ainguriak otoituz berehala 
hola hola izorra zadila. 
Eta ite misa est.

In nomine Patri et Filii.
Gora jauntzi ta duzu erori.
Zerurat hainitz baizuen igorri
laguntza ginuen zor Carrerori.
In nomine Patri et filii.
Gora jauntzi ta duzu erori.
Zerurat hainitz baizuen igorri
laguntza ginuen zor Carrerori.

Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Eta ite misa est.

Madrilek eta munduak ere
bi gauzaren ikastia on dute
eskualdun ta ttipien zanpatze
ezin da iraun eternitate.
Madrilek eta Parisek ere
bi gauzaren ikastia on dute
eskualdun ta ttipien zanpatze
ezin da iraun eternitate.

Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Eta ite misa est.

Goresten haigu, goresten eta
populiaren makila haiz ta
hire indarra handia baita
herria hitaz bozkariatzen da.
Goresten haigu, goresten eta
populiaren makila haiz ta
hire indarra handia baita
herria hitaz bozkariatzen da.

Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Jende lasterkatzaile krudela
bera die igorri...
Yup! la-la.
Ainguriak otoituz berehala
hola hola izorra zadila.
Eta ite misa est.
Deo gratias.