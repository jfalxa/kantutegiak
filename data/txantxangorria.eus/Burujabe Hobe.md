---
id: tx-2507
izenburua: Burujabe Hobe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0t0eCj6Ic0Y
---

Zazpi Euskal Herriek
bat egin dezagun,
guztiok beti-beti
gauden gu euskaldun. (bis)

Agur eta ohore
euskal herriari
Lapurdi, Baxe Nafar,
Zibero gainari,
Bizkai, Nafar,
Gipuzko eta Arabari,
zazpiak bat besarka
lot beitez elkarri


Gure ekonomia
ta gure kultura,
hizkuntza, historia,
lurra ta ohitura.
herri legez zabaldu
gaitezen mundura
pausoa emateko
ordua heldu da.


Euskal Herriak ditu
hamaika kolore,
beilegi, berde, gorri,
urdin, gris zein more.
nortasuna luzaro
gura bada gorde,
zapalduta ezin da,
burujabe hobe.