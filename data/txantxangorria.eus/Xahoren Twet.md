---
id: tx-2720
izenburua: Xahoren Twet
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/IRjkZQkAjag
---

Xahoren, Kanpionen, 
Xalbadorren herri hau
esnatzen denean
Agirreren, Monzonen,
Matalasen herria
ekartzen denean
inperialismoa
intransigentzia
has daitezela dardarka.

Xahoren, Kanpionen, 
Xalbadorren herri hau
esnatzen denean
Agirreren, Monzonen,
Matalasen herria
ekartzen denean
inperialismoa
intransigentzia
has daitezela dardarka.