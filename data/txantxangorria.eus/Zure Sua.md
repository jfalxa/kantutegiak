---
id: tx-1602
izenburua: Zure Sua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NadNk8uYZdA
---

Nire buruak sarri galdetzen dit 
non ezkutatu zaren.
Pauso bat atzera 
txanpa hartzeko 
eta bila abia gaitezen!

Egunak hogeita lau ordu ditu, 
baina zu ez zaude hauetan,
goizetik iluntzera 
zure usainaren arrastoei segika.

Sua, sua, sua, sua zuk berotzen
nauzu eta badakizu.
Sua, sua, sua, sua guk txinparta
eta sugarra sor dezagun.

Batzuetan hala dela sentitu arren,
bestetan zalantzaz josirik, 
nonbait ote zauden
jakin nahian ametsetako gidari.

Urtu egiten naiz 
zure sura gerturatzean
zein zaren jakin gabe
begiraden artean,
aurkituko zaitut negua datorrenean,
haize hotzen kilimez 
epeltzen zarenean
kantuaz nire ahotsa entzutean
igo taula gainera 
agudo musukatzera,
zure suaz erreko nauzu bat-batean,
epeldu nazazu 
eta joan gaitezen batera