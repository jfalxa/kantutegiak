---
id: tx-1443
izenburua: Enbata
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I7aEexIQOG8
---

Enbata dator ustekabean
itsaso barea aldatu egin da
Itxi begiak, arin joango da
itxi begiak, bukatuko da
Enbata dator ta lotu egin naiz
haize zakarrak eramango ez nau
gorputz osoaz saiatuko naiz
ez nau ekaitzak itoko oraingoan
Itxi begiak, arin joango da
itxi begiak, bukatuko da
itxi begiak, ezin gaitu eraman
itxi begiak, beldurrik ez izan
Enbata dator ta ni prest naiz
orain badakit nola egin kontra
goizetik gauera oinak lurrean
inork ez dizkit erahilgo ametsak
Itxi begiak, arin joango da
itxi begiak, bukatuko da
itxi begiak, ezin gaitu eraman
itxi begiak, beldurrik ez izan