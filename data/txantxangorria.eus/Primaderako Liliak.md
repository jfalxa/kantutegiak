---
id: tx-1854
izenburua: Primaderako Liliak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nOE4jjSzXV0
---

Primaderako liliak
Goizik ziren loratuak,
Urthe hartan gertatu zen
Pazko erramu baino lehen.

Gaituzte ezkontarazi
Maite ninduzun hain gutti
Eskontarazi gaituzte
Ez zintudalarik maite.

Ginuelarik elgarri
Eman biziko uztarri.
Ezkilak zeuden ixilik
Nik ez baitakit zergatik.

Gaituzte ezkontarazi...

Ez zuten egin boleran
menturaz ziren Erroman;
Ez ziren gure lekuko
Ez izanen bataioko.

Gaituzte eskontarazi...

Nonbait balinbada zeru
Gure haurra da aingeru
Aingeru zeruan bainan
umerik ez ohantzean.

Gaituzte ezkontarazi...

Bihotza hotzez kaskaran
nago beroaren menturan.
Etor bedi primabera
kanpoak diten berriz lora.

Gaituzte eskontarazi
elgarregin behar bizi;
Ezkontarazi gaituzten
Behar dugu elgar maite.