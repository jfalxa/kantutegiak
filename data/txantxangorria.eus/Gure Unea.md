---
id: tx-350
izenburua: Gure Unea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4skaK0VqvVw
---

Abestia / Tema: Gure unea
Albuma / Album: Hiri zahar zikina (2022)
Musika: Skabidean
Musikaren grabazioa, nahasketa eta masterizazioa / Grabación, mezcla y masterización de la música: Kaki
Arkarazo (Garate Studios, Andoain).
Bideoaren grabazioa eta muntaketa: Skabidean
Bideoaren edizioa: Jon Corres
Bideoa taldekideek grabatu zuten, Andoaingo Garate Studiosen, 2021eko abenduan, diskaren grabazio egunetan.
Mila esker grabazioa gurekin partekatzera hurreratu zineten guztioi! Bideoa, halaber, taldekideek muntatu zuten.
Mila esker Jon Corresi, bideoaren edizioagatik.
---------------------------------------------------------------------------------------------------------------------------
Kontratazioa / Contratación: Bidean Produkzioak (info@bideanprodukzioak.com)
Taldearen zuzeneko kontaktua / Contacto directo del grupo: skabidean.blog@gmail.com edo 610887684
---------------------------------------------------------------------------------------------------------------------------
LETRA
Sortu da eguna,
gurean egotekoa,
berandu goaz egiteko sua,
ordua eta ez gaude guztiak.
Behin harturik lekua izanen da astia
hariztian etzateko elkartasun osoan.

Beroa galdaran, algarak giroan,
azkenik elkarrekin gara.
Gure unea da, bat izatekoa,
barre artean dagigun topa:
pausoak egin ditzagun beti guztiok batera,
hel gaitezen elkarrekin gure helmugara.

Zin egiten diot bihotzez neure buruari
urteen joanarekin aldatuko naizela,
baina beste gauza batzuk, aldiz, argi gera dadin,
beste batzuk, betirako berdin, izanen direla!

Biniloak martxan, segundoak dantzan,
hegan doakigu arratsa,
eta eskerrak emateko, ez da hitzik behar,
guztiok zainduz gero elkar.
Gure etorkizunean izanen garen horientzat
izan bitez bide osoan sorturiko oroitzak.