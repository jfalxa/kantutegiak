---
id: tx-1539
izenburua: Welcome
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RRcZG_PelYo
---

NAROA GAINTZA Ahotsa, doinuak, hitzak 
DAVID NANCLARES Baxua, gitarrak, musika moldatzailea, ekoizpen musikala.
DAVID GOROSPE Bateria.
ASIER ERCILLA Teknikaria, teklatuak, eskusina.

Zure bila noa!
Iritsi da eguna 
egia ote da.

Egunsentia da
dotore jantzita
sabela dantzan

Eta auzoan festa
herrian, hirian
denok kalean.

Zure bila goaz
lapurtutako urteak
hemen dira

Eta iristean
dardara betea
zegoen muga ertzean
Besarkadak 
begirada, oihu, irri,
malko eta algarak.

Hartu nire eskua
bakardade izpirik
ez aurreratzean.

Bidai luze honetan
kontatuko dizut
zer gertatu den.

Zure lekua da
ez izan beldurrik
ez da aldatu lar

Apurka-apurka
bidea pauso txikiz
egiten da.

Eta iristean 
suziriak, dantzak,
loreak herriko plazan
lagun zaharrak
belaunaldi berri, senide
beteranoak.

Hotzikarak, egonezina
asea... zure letrak
sentimendu, kontrajarriak
Bakardadea emaidazu eskua
zain dugu maitasuna.

Eta iristean
zapore gazi-gozoa
zure burmuinean.

Ez daudenak
gogora ekarriz
arinduz gure taupadak.

Eta iristean
begiak betetzen zaizkit
gurekin zaudela.

Hemen zara 
laster besteak
egongo dira.