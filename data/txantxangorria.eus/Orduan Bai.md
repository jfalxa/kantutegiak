---
id: tx-451
izenburua: Orduan Bai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4NN1QDnSA20
---

Orduan Bai |  GURE HIZKERAN (2019)

Recorded, produced and mixed by: James Morgan and Asier Renteria in MagicBox Musika (Mungia).


ORDUAN BAI:

ETXEKALTE:


Hitzak:
  
Eta orduan bai nire moduan bai honela
hasiko dira lorak hazten 
Udazkenean orain sustraietatik eman lorea 
eta orduan bai nire moduan bai... 

Itzali dena, eta erabili gaua, itzali denok isiltasunean 
Eta orduan bai nire moduan bai honela 
hasiko dira lorak hazten 
Udazken anitzak sasoietatik erauziko dira 
eta orduan bai nire moduan bai...
 
Gauan bada argia ilunak ziren aldi haiek koloretan daude 
Egun hoberen zain egon nintzen 
errua besarkatzen nuen 
80 urtetan hainbat ordu pasa ziren eta orain atzera kontu baten, baten 

Zer, elaiak? Orduan iritsi da negua
Bazoazte hegoekaz hurrutiko zerura
oraindik ez dena ezagutzen...
Eraman beroa begi urdinetara 
eraman arnasa itsas azpietara
egunsenti zabaletan kanpoko belarretan 
zapaldu berriak izanez 
Bermioko mendietaz oroituko al dira?