---
id: tx-1711
izenburua: Mündüan Malerusik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZF32ugkJcd4
---

Mündian malerusik 
hanitx begirade
Bena ni bezañ denik 
ezta ihur ere.
Nihauren funtsetan 
nahiz izan jabe,
Hamar urteren galerak 
enetzat dirade. 

Desertüko ihiziak, 
jenten beldürrez
Abisatzen dirade 
egoitera gordez.
Ni ere, hurak bezala, 
gaxua nigarrez, 
Ene bizi trixtia 
kuntserbi beharrez. 

Hogei eta bi urtik 
bete egünin,
Emazte bat hartü nin 
ene zorigaitzin;
Erraiten ahal beitüt 
zereki zeitan jin,
Ene ürka büllhürra 
gorderik altzopin.