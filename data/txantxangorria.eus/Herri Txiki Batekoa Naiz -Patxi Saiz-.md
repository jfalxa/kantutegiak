---
id: tx-3246
izenburua: Herri Txiki Batekoa Naiz -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_3XXMxsrKRE
---

Herri txiki batekoa naiz
bere izatean gogoz
bizi nahi duen asmoz
ukatzen zaio oro

Herri txiki batekoa naiz
bere izatean tinko
oldartzen da une oro
borrokatzen da gogor.

Ez, ez du arima galdu
gaurko Euskal Herriak,
Euskal Herri borrokalari
pentsalariak
ez du arima saldu nahi,
baizik atzeman.
Ez zuten beren arima saldu,
atzo Elgetan, Kalamuan,
Solluben, Artxandan,
Euskal Herriko mendiak
beren odolaz
gorritu zituzten gudariak.
Ez dute beren arima saldu,
gaur heien aurre ezpata
gizonei buruz buru begiratuz,
eskumuturrak loturik
baina bihotzak libro
euskaraz oihuka,
heriotzaren aurrean kantuz,
beren abertzaletasun
eta zuzentasun egarria,
deiadar-kantu duten
gure anai gazteok,
ez eta borroka honetan
bere urterik hoberenak,
nahiz espetxean,
nahiz etxetik kanpo,
daramatzaten abertzaleak.
Izan ala ez izan,
bizi ala hil,
horra zertan den
gaur gure herria,
herri gisa zapaldu nahi gaituzte,
herri gisa eman behar dugu borroka.
Abertzale guztiok elkarturik,
agintaritza bakar baten menpe.
Eta borroka hori luzea
eta latza izanen baita,
bidean anaia erortzen bazaik,
Lepoan hartu eta segi aurrera.

Non dugu duintasuna?
Gure herriaren nahia,
non-nahi eta noiznahi,
zapaltzen ikustean,
gure herriaren izatea,
non-nahi eta noiznahi,
zanpatzen ikustean.

Herri txiki batekoa naiz
bere izatean tinko
oldartzen da une oro
borrokatzen da gogor.