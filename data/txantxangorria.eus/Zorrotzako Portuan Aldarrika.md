---
id: tx-2386
izenburua: Zorrotzako Portuan Aldarrika
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vMLiCfs2zGQ
---

ZORROTZAKO PORTUAN ALDARRIKA

        Gabriel Aresti , 1963

 

 

Aleman barkua atrakatu da Zorrotzan.

Zimentua dakar, ehun kiloko sakoetan.

Bien bitartean

Anton eta Gilen zeuden

zerrarekin

tronko hura erdibitzen.

Sokarekin...

Eztago kablerik...

Bestela...

Tira eta tira,

orain Anton,

gero Gilen,

eznaiz hilen,

Gilen.

Hemen euskeraz

ta han erderaz.

Birao egiten zuten.

Okerbideak ezpaitaki mintzaerarik,

berdin tratatzen baitu

erdalduna

eta

euskalduna.

Arbolaren neurriak hartu nituen.

Antiojuak bustitzen zitzaizkidan.

(Amak gauean pentsatu zuen errekara

erori nintzela). Eta esan nuen:

Beti paratuko naiz

gizonaren alde.

Gilen.

Anton.