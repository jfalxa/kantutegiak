---
id: tx-3207
izenburua: Maritxu Nora Zoaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aYEAy0CF7uA
---

Maritxu nora zoaz eder galant hori?
Iturrira Bartolo nahi badezu etorri
Iturrian zer dago?
Ardotxo txuria
Biak edango degu nahi degun guztia