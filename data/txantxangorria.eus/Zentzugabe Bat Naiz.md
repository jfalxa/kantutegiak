---
id: tx-2957
izenburua: Zentzugabe Bat Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TWR7dPYUo9c
---

Hilko banintz edozein ordutan,
hilko banintz edozein lekutan,
hilko banintz lagunen albotan,
behin betiko bareko barnea...
Hiltzen itxoiten, 
hiltzen ezin larritu!!
Hilko banintz edozein ordutan,
hilko banintz edozein lekutan,
hilko banintz maitalen albotan,
behin betiko aseko barnea....
Zentzugabe, zentzugabe!!