---
id: tx-115
izenburua: Entera Daitezela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/crmoH9dcZ4s
---

------------------------------------------------------------------------
Ze kolorerekin
Ze jarreratan
Ze jostailurekin
Ze generotan
Sartu
Esan ziguten umeak ginenean

Ze itxurarekin
Ze kuadrillatan
Ze gorputzekin
Ze kaiolatan
Sartu behar garen
Esaten  diguten denak

Entera daitezela
Hemen dena ez dela
Eskemek dioten bezala
Entera daitezela
Gu askeak izateko jaio garela

Beti zutik beti aurrera
Nortasuna eta jarrera

Nahi dugun bezala bizi dugu bizitza
Urdin, arrosa, zuri bat ere ez edo guztiak
Nor bere eran
Nor bere martxan
Nor bere gustukoarekin narrutan
Eta zer da zer da
Hazkura ematen dietena

Entera daitezela
hemen dena ez dela
eskemek dioten bezala
entera daitezela
gu askeak izateko jaio garela

Beti zutik beti aurrera
Nortasuna eta jarrera

Barruan dagoena da
Egiten gaituena
Ez galdu denbora
Ez begiratu ikusten denera
Barruan dagoena da
Egiten gaituena
Ez galdu denbora
Ez begiratu ikusten denera

Beti zutik beti aurrera
Nortasuna eta jarrera
Beti zutik beti aurrera
Hau da gure izaera