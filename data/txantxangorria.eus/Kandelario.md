---
id: tx-993
izenburua: Kandelario
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YI6Ehmzh7AE
---

Kandelarioz euria,
laster da udabarria.
Kandelarioz ateri, negua dator atzeti.

Kandelario lario,
hatxari ura dario,
errotari uruna.
Hauxe da guk behar doguna.

Kandelarioz eguzki,
negua dago aurreti.
Kandelarioz edurra,
joan da neguaren beldurra.

Kandelarixo bero,
negua dauko gero.
Kandelarixo hotz,
negua joan da motz.

Kandelario lario,
hatxari ura dario,
makatzari madari.
Eutsi, Peru, hankeari.
#saveyourinternet