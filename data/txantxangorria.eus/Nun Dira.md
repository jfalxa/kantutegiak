---
id: tx-881
izenburua: Nun Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7tDcomOPw-I
---

ANTTON ARANBURU. #Kaos#Kosmos

NUN DIRA
Iraganeko garaiak hobeak zirela esango al zenuke? Zeintzuk ziren zure gaztaroko ametsak? Errealitate bihurtu al dituzu? Orain zarena al da izan nahi zenuen hura? Zer galdu duzu zeure bizitzaren bidaian? Irrati aparatutik Hertzainen musika entzun daiteke: Nola aldatzen diren gauzak, kamarada!  Egia borobila! Nire baitan ere antzematen ditut halako paradoxa nabariak, eta nire militantzia zirkulu hurbilenetan ere bai. Adibidez, gaztetan iraultzaile izan nahi zuen lagunaren kasua, eta funts putre batetako zuzendari moduan ari dena gaur egun. Edota, kantuak zioen bezala, diru asko irabazteko torero izan nahi zuen ikaskidea, eta orain M15 mugimenduko zezenketen kontrako zirkuluko moderatzaile lanetan dabilena.  Edo heavy peto-peto hura, zurekin uda oso batez lanean ibili zena, bere txima luzeak neskalaguna baino gehiago maite zituena, eta egun alopeziak jota halabeharrez ilea zerora (edota gehienez batera edo bira) moztu behar duena. Nolabaiteko justizia poetikoak eragindako zehar-kalteak. Orain musika-erreproduzitzailean Kortatu entzuten da ozenki. Negua eztulka ari da kanpoaldean. Ya estoy harto, no quiero salir más. Iluntasuna eta hotza. Siempre lo mismo. Nostalgia, oroitzapenak eta negar malkoren bat edo beste. ¡Mierda de ciudad!
Ixiltasuna horman eskegita
Iluntzeko ordu malkartsutan
Nora joan dira ilusio zaharrak
Leihotik begira negua estulka

Gogoratzen haiz nolakoak ginan
Gangarrak marrubizko irlan
Esan behar nian ta ez nintzan gai izan
Beti ibili nauk erdi txoratuta

Nun dira? Nun dira?

Nonbait galduta neure saudadean
Talaia romantikoenean
Orekaren bila armoniaren zain
Ez diot irri egiteari utzi nahi

Nun dira? Nun dira?