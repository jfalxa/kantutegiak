---
id: tx-2775
izenburua: Ihesi Zilegi Balitz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ZysC49hwUA4
---

Ihes betea zilegi balitz
nunbait balego bakea
ni ez nintzake etxe ertzeko
loredien maitalea.

Ni ez nintzake oinazearen
menpeko mixerablea
oihu zekenen destinatzaile
etsipenaren semea.

Ni ez nintzake iñorentzako
eskandaluzko kaltea
lur hotz batetan aldatutako
landare sustrai gabea.

Ihes ederra zilegi balitz
urra ahal baledi katea
ni ez nintzake ontzi gabeko
itsasgizon ahalgea.