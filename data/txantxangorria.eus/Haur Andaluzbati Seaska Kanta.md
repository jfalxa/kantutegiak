---
id: tx-576
izenburua: Haur Andaluzbati Seaska Kanta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NGRJvtTDrzc
---

Sehaskan zaude, haurra, kabi beroan lo,
gau luzearen begiak hor daude zuri so
ekarri zinduten lur arrotz hontara,
ekarri zinduten, gaueko haurra.

Zure gurasoek lan egin nahi zuten,
lan egiñaz bizi, ez morroien gisara
zu zera haien fruitu, arrazoi bakarra,
zu zera nere anaia, itxaropen eta beldurra.

Bota zintuztenek irakatsiko dizute
hartu zintuen lur hau lur madarikatu dela,
bultza nahiko zaituzte herri baten desegitera,
hain maltzur da gorrotoa, hain luze ta ilun gaua.

Lotan zaude, haurra, esnatuko zera,
gauaren begiak argituko dira.
Lankide nahi zinduzket, bide-lagun agian,
gaur kabi duzun lur hontan finka zaitezen bihar.
Lore bat da nere hitza, dardarrez dakarkizut,
zure lo hortatik esnatzean ikurrin gisan jaso dezazun.

Sehaskan zaude, haurra, kabi beroan lo,
Gau luzearen begiak hor daude zuri so.