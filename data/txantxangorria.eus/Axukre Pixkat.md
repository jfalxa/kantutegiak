---
id: tx-358
izenburua: Axukre Pixkat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fkkoSrP8A1I
---

GOXOKIAK diskoan aurkituko duzuen abestietariko bat.

Jarraitu sareetan:


HIBAI - GOXOKIAK

HIBAI feat. IRE - Axukre pixkat (Akustikoan)

Musika eta Hitzak Hibai Etxebarria

Gernikako "Amuman Etxie" estudioan grabatua 
Nahasketa eta Masterizazioa: Hibai Etxebarria

Musikariak:
Irene Eiguren -  Ahotsa, Gitarra, Biolontxeloa eta Perkusioa
Hibai Etxebarria - Ahotsa, Gitarra, Teklatuak, Kontrabaxua eta Bateria

Bideoa:
Dekorazioa - Sara Oar
Kamara - Kiara Mendoza
Zuzendaria - Hibai Etxebarria


Hitzak:
Gure irribarrea
Zapi baten azpian
Baina itxaropenak
Arnasten du atzean

Auzoz-auzo
Herririk herri
Pausoz-pauso
Aurrerantz beti
Gazi-geza
Baita bizitza
Kolore anitza

Dastatu bizitza
Mila zapore
Zure zai daude
Zabaldu bihotza
Ta mingotsa badago
Botaiozu axukre pixkat

Bota ta bota! 
Bota ta bota axukrea!

Perrexila da saltsan
Bertsotan eta dantzan
Bakailoa pil-pilean
Jaixetako txosnetan

Goxo-goxo
Laztan nazazu
Muxux-muxu
Jan eingo zaitut
Masta-masta
Gure altxorrak
Nire ahoan

Dastatu bizitza
Mila zapore
Zure zai daude
Zabaldu bihotza
Ta mingotsa badago
Botaiozu axukre pixkat

Bota ta bota! 
Bota ta bota axukrea!

Atera kalera
Mundua jatera

Dastatu bizitza
Mila zapore
Zure zai daude
Zabaldu bihotza
Etorri nerekin
Zeren zai zaude

Dastatu bizitza
Mila zapore
Zure zai daude
Zabaldu bihotza
Ta mingotsa badago
Botaiozu axukre pixkat

Bota ta bota! 
Bota ta bota axukrea!

(C) Hibai Etxebarria