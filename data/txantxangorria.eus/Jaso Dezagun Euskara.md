---
id: tx-748
izenburua: Jaso Dezagun Euskara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NTHqhq8bh3Y
---

Iruñea hiria
Kale ta harresi
Iruñea berria
Historia bizi
Semaforoak berde
Aurrera bi gezi,
2000rantz abian
bimilaka suzi
espaloietatik at
bidera goazi.

Hiriaren mintzoa
Mintzoaren hiri
Altxorraren altzoa
Ahozko agiri
Mende, mendi eta men
Guztietaz gaindi
Altxorraren altzoa
Ahozko mirari
Euskara jaso haugu
Orain jalgi hadi.

Jaso dezagun euskara
Euskara eguneroko
Zapi gorria da
Jaso dezagun euskara
Orain izan behar da
Izanen bagara.

San Fermin bera ere
Heldu da bestara
Euskara eguneroko
Zapi gorria da
Orain izan behar da
Izanen bagara
Batak bertzera dagi
Plazatik zetara
Euskarak Iruñea
Iruñeak euskara.