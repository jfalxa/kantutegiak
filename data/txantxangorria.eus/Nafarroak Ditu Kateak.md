---
id: tx-2901
izenburua: Nafarroak Ditu Kateak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2I99ch3qqtA
---

NAFARROAK DITU

KATEAK

Hitzak eta musika: Fermin Balentzia/Euskaratua:Jokin de Pedro

Santxo Azkarrak, 

Santxo Azkarrak

biziko balitz gurean

kate horiek hautsita

batuko gintuzke denak.

Jada ez dira mairuak

mugen aurka datozenak;

orain kristau txisteradun

dira hortan dihardutenak.

Nafarroak ditu kateak

kanpoan eta barnean;

armarrikoak geureak,

bortxaz jarriak besteak,

bortxaz jarriak besteak

armarrikoak geureak;

kanpoan eta barnean

Nafarroak ditu kateak.

Mila bostehun (e)ta hamabian

Albako Duke arrotzak

sartu armada Iruñean,

Nafarroan gerra-hotsak.

Harrezkero gaztelarra 

Nafarroan erregetzat,

barre egin zuten monarkek

ta negar nafar bihotzak.

Nafarroak ditu kateak...

Historia Herriak egin,

kontrabandistek idatzi;

nahieran kontatzen dute,

gezur biribilez jantzi.

Obanosko infantzoiek

lelo hau ziguten utzi:

“Izan libre aberri askean,

Nafarroak ez du ahantzi”.

Nafarroak ditu kateak.