---
id: tx-2798
izenburua: Gulliverren Bidaiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TEbwNCb2Ttg
---

oi, lai-lairi,
ipuin kontu kontari.
oi, lai-lairi,
marmaraz marmari.

liliputen, lili,
gizon txikien-en aberri,
gulliver putzura erori,
eta liliputen ageri.

oi, lai-lairi,
ipuin kontu kontari.
oi, lai-lairi,
marmaraz marmari.

txikien zaindari,
liliputarren buruzagi,
ezetz esan zion gerrari,
ongi etorri bakeari.

oi, lai-lairi,
ipuin kontu kontari.
oi, lai-lairi,
marmaraz marmari.

halako batean,
bakea lortu zuenean,
agur esan eta gulliverr,
triste joan zen arraunean.

oi, lai-lairi,
ipuin kontu kontari.
oi, lai-lairi,
marmaraz marmari.