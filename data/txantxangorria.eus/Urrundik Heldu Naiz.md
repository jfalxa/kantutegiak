---
id: tx-482
izenburua: Urrundik Heldu Naiz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9a7R7kW7NQc
---

Urrundik heldu naiz
Luze ibiliz
Bidez bide eta bideetako
Hautsak garia sortarazi du
Ene oinetako behatz artetik
Ene burua untzaz ari da
Estaltzen, eskuetako
Behatzetan madariondoaren
Hosto xamurrak loratzen
Bizkarrean
Hortzian barna
Magali mikatzen kimuak
Txertatzen ari zaizkidalarik, zaizkidalarik
San Migueletan, itsasoa
Nabarra ludia ttipitzen duen
Itsasoa
Ene baitan urriaren
Hego haizearekin batera,
Hazi busti eta emankorra
Ari du, ari du
Zilegi bekit adiskide,
Pentsatzea, esatea,
Harek ez du goroldio
Baizik sortuko
Gure harrizko buruetan