---
id: tx-65
izenburua: Mari Manu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ycfOGzKsK-0
---

Bideoa: Joseba Batiz


Tragola, tragola, tragola, soixia, itxasorako otzaria. 
Aquí venimos los “barbis”, 
los “barbis” de nuestro txoko, 
que queremos poco a poco, 
a los padres imitar. 
Un día tan señalado, 
cual este de carnaval, 
en el que aun el más “chaval”, 
siempre estaba disfrazado. 
Urra, José Babil, José Babil, José Babil, Urra, José Babil, José Babil, José Babil, Urra , Katxutxa, capela frente, 
aren gainian iru komediante. 
Mari Manu, eta Txinelak galdu 
oyen azpiko bayoneta zarra. 
Oyen azpian bayonetia, 
lexiba otzara gainian. 
Sinkerek badauko bibotia 
Mari manuk de zurpian. 
Otzaratxu bat, otzaratxu bi, 
arraña dator, portura beti, 
besegu lebatz, sardina ta atun, 
eta beste asko gainetik. 
Por favor, por favor, dame un beso y verás, ¿Qué veré?, ¿qué veré?, ¿te diré? 
Putxerito jan dozu?, Niretzat itxi dozu? Niretzat itxi ez badozu zeuk dana jan dozu.