---
id: tx-2891
izenburua: Akelarrean Dantzan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FA-c1PG17QE
---

Gu sorginak gara
festa ta algara
suaren inguruan
akelarrean dantzan
Gu sorginak gara 
festa ta algara
suaren inguruan
akelarrean dantzan

Zirrizti mirrizti
gerrena plat
olio zopa
Ikili salda urrup! 
Edan edo klik
Ikimilikilikli!