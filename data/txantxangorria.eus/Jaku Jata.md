---
id: tx-2914
izenburua: Jaku Jata
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OTVru-M0JWU
---

2015eko Ekainaren 7an Elgoibarren ospatuko den Euskal Eskola publikoaren festarako eginiko bideoklipa.
musika: Iban Urizar
hitzak: Mikel Olaizola
klipa: Lander Garro
abeslariak: Uxue Alberdi, Porrotx, Zuhaitz Gurrutxaga, Aiora Renteria, Mendaroko Ameikutz abesbatza.
musikariak: Oreka Tx, Patxi Zabaleta, Joanes Ederra, Naia Membrillera, Aitor Nova, Iban Ginea, Andoni Riaño, Balzola anaiak.

JAKU JATA:
Gustatzen jata gure
Eskola bizixa
Etorkizun izango
Dan udaberrixa
Erlien gisan gabiz
Dantzan lorez lore
Mundu txiki bat jantziz
Hamaika kolorez

Aldapatik begira
Bailara estura
Geranaren segida
Zabalik mundura
Mingaina irri bati
Daukagu lotuta
Euskeraz bizitzia
Gustatzen jaku eta

Aniztasunak Dakar
Aberastasuna
Horixe da eskolan
Gustatzen Jakuna
Danok desberdin baina
Eskutik helduta
Ekipo berdinian
Jokatzen dogu eta

Elkarlanak emaitza
Gozuak berekin
Erlien ahaleginak
Eztixa etekin 
Danona, irikixa
Eta herrikua
Gustatzen jata euskal 
Eskola publikua