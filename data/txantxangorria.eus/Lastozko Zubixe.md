---
id: tx-2086
izenburua: Lastozko Zubixe
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Afb6UqG_d_8
---

Jontxuk egin ei eban
lastozko zubia,
handixek pasateko
Loretxu txikia.

Hasi zan pasaten da
jausi zan zubia,
Jontxuk hartu ei eban
sentimentu handia.
Lorek egin ei eban.......
Adolfo txikia.