---
id: tx-1915
izenburua: Gauden Gu Euskaldun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QZVrsXAJBaY
---

Zazpi Euskal Herriek
bat egin dezagun,
guztiok beti-beti
gauden gu euskaldun.

Agur eta ohore
Euskal Herriari
Lapurdi, Baxenabar,
Zubero gainari;
Bizkai, Nafar, Gipuzko
eta Arabari.
Zazpiak bat besarka
lot beitez elgarri.

Zazpi Euskal Herriek
bat egin dezagun,
guztiok beti-beti
gauden gu euskaldun.

Haritz eder bat da
gure mendietan,
zazpi adarrez dena
zabaltzen airetan.
Frantzian, Espainian,
bi alderdietan:
hemen hiru eta han lau,
bat da zazpiretan.

Zazpi Euskal Herriek
bat egin dezagun,
guztiok beti-beti
gauden gu euskaldun.

Hi haiz, Euskal Herria
haritz hori bera,
arrotza nausiturik
moztua sobera.
Oi gure arbasoak
ez, otoi ez beira,
zein goratik garen gu
jautsiak behera!

Zazpi Euskal Herriek
bat egin dezagun,
guztiok beti-beti
gauden gu euskaldun.