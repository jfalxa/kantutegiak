---
id: tx-295
izenburua: Ikasi Ta Lan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ape8l2OtqI8
---

Kontratazioak/ Contrataciones: info@taupaka.eus

“Ikasi ta lan” abestiak, gazte prekarietatea bermatzen duen hainbat aspekturi buruz hitz egiten du. Bizitzan zehar pasa behar ditugun egoera eta baldintza kaskarrez mintzatzen da, “esklabutza” papera denuntziatuz eta kapitalismoaren baitan gazteon duintasunaren zapalkuntza azpimarratuz. Bideoklipean haserrea transmititzen da, paisai apurtuak zein esplotazioa bermatzen duten hainbat komertzio azalduz. Iluna eta mezua barneratzeko irudiak agertzen dira.


Ahotsa/ Letra: Ziztada
Produkzio/ Nahasketa: Rlantz
Master: Antxon Sagardui
Bideoklipa: Tiropunekoizpenak

Ikasi Ta Lan (Letra)
Ikasi ta lan, sartu orduak zure logelan, 
ez izan denborarik zure gauzetan pentsatzeko ta egiteko plan bat,
zenbatetan kapitalismoaren mesedetan, esklabu izatearen kondenan
bizi osoa hipotekaren menpekotasunean
Ta zurekin beste milaka daudeee…
Inor ez da bizitza baten epaile, esklabutzaren bermatzaile direnei
Hau da gure protesta zuentzako festa, sistemaren interesa eza, gezurretan ibiltzeko babesa, dena explota gaitezan.
Gazteak migrazioan isilik geratu lana bila joan, gurasoen urruntasunetik etorkizuna dugu jokoan
Apustu jokoak, drogaren basoak, aktibotasuna zapaldu nahian kendu nahi dizkiguten hegoak
emakume eroa, ez izan gaixtoa, kasu egin ta ez haserretu jarraitu zure estiloa.
Gizartea esnatuko bagenu prekarietatea ez litzateke tabu, ez litzateke tabu, ez litzateke…
Ikasi ta lan, sartu orduak zure logelan, 
ez izan denborarik zure gauzetan pentsatzeko ta egiteko plan bat,
zenbatetan kapitalismoaren mesedetan, esklabu izatearen kondenan
bizi osoa hipotekaren menpekotasunean.
PNV, Iberdrola, El corte ingles, Ibex35 haien guztien politikek egiten gaituzte esklabo
haientzat diru gehiago zapalduak berriz beherago, bizi proiektu bat dugu obeditzeaz harago
Ta zurekin beste milaka daudeee…
Inor ez da bizitza baten epaile, esklabutzaren bermatzaile direnei.