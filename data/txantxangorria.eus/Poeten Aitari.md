---
id: tx-2836
izenburua: Poeten Aitari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BK0BNO-VdVU
---

Iparreko mendietan maitasunaren artzain
Bago, lore, ur, basoliar, orkatzen arduradun
Etxolako sutondoan igaile denen lagun
Edertasuna kantatuz amodioa bezain.

Izar, Itsaso, Pirene, kosmos oroaren dantzan
Ardi galduaren bila ortziekin aitzetan
Maitearentzat gudukan lamiñekin leizetan
Bizia eta herioa elgarrekilan baltsan.

Ari dira zure liran, hiru noten jauzitan
Ezin batuzko ametsen biezko dialektikan
dena rimatzen duela Jainko baten txirulak.

Labe gorrien tximixtan, kearen ordeietan
Laborarien borroketan, langileen uhinetan
Gitarra xume batekin imita zaitzagula.

Betiko bizitzagatik eman duzu odola
Eta horretan errotzen da poeten arbola
Erreduran, urraduran ernalduz eta erdiz.