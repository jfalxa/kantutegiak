---
id: tx-3383
izenburua: Ihauteriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FAoTEV9joVs
---

Inauteriak dira bai
iuju iuju jai!
Festan sortu da nahaspila
mozorroen bila

Inauteriak dira bai
iuju iuju jai!
Festan sortu da nahaspila
mozorroen bila

Inudeak, artzainak,
ziripot, zanpantzarrak
Ikaratu nahi ditut
orain den-denak
mamua naiz ni!

Iratxoak .lamiak,
miolotxin,sorginak
Dardarka jarri dira
orain den-denak
mamua naiz ni
Uuuuu!