---
id: tx-1879
izenburua: Zibilak Esan Naute
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9cfRGA2beJw
---

Zibilek esan naute,
biziro, egoki,
Tolosan behar dala
gauza erabaki;
giltzapian sartu naute,
poliki-poliki:
negar egingo luke
nere amak baleki.

Jesus tribunalian
zutenian sartu,
etzion Pilatosek
kulparik bilatu;
nigan ere arkitu ez arren,
ez naute barkatu;
zergatik ez dituzte
eskuak garbitu?

Kartzelatik atera
fiskalen etxera,
abisatu zidaten
joateko berehala.
Ez etortzeko gehiago
probintzi honetara;
orduan hartu nuen 
Santander aldera.