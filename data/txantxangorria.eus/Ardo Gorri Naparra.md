---
id: tx-2139
izenburua: Ardo Gorri Naparra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3vFgxyNOibQ
---

Red red wine 
ardo gorri naparra
nahi nuke orain edan
oraintxe bertan
red red wine 
ardo gorri naparra
bestela sagardoa
oraintxe bertan.
Donostia bihurtu zaigu tropikala 
banana palmera laster kokoteroak.
Ardoa sagardoa eta txakolina 
aurrerantz edan behar
mangoa eta pina.