---
id: tx-540
izenburua: Zeinen Ederra Izango Den
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HFxxOhAyX3c
---

Zeinen ederra izango den
Eguzki izpiak zure itzala belarretan marrazten
Muxu bat larrunen
Garagardo bat zerura eroritakoen omenez
Herri txikietako festetan
Esnatuko bagina bihar
Dantzatuko genuke plazetan
Sekula santan ez bezala
Eta kantu bakoitzean
Utziko genuke eztarria
Azkenengoa bailitzan
Edota denetan lehenengoa
Gu
Zorionekoak
Gordetako ametsen kutxa irekiak
Zeinen ederra izango den
Eguzki izpiak zure itzala belarretan marrazten
Muxu bat larrunen
Garagardo bat zerura eroritakoen omenez
Hilobak amona ezagutzean
Haritz bat jaioko da Urbasan
Urteekin oartuko gara
Betirako biziko zela
Lagun minekin ilunabarra
Eternala bihurtuko da
Xuxurlatuko digu ilargiak
Oraindik denok ez gaudela
Gu
Zorionekoak
Gordetako ametsen kutxa irekiak
Zeinen ederra izango den
Eguzki izpiak zure itzala belarretan marrazten
Muxu bat Larrunen
Garagardo bat zerura eroritakoen omenez
Ze ederra izango den
Eguzki izpiak zure itzala belarretan marrazten
Muxu bat larrunen
Garagardo bat zerura eroritakoen omenez