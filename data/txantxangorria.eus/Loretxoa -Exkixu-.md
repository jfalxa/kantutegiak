---
id: tx-3299
izenburua: Loretxoa -Exkixu-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Nw2QtwVYSVs
---

Mendian larrartean
aurkitzen da loretxo bat
aurrean umetxo bat
loretxoari begira.

Loreak esan nahi dio
umetxo aska nazazu
jaio naiz libre izateko
ta ez loturik egoteko.

Umetxoak ikusirik
lorea ezin bizirik
arantzak kendu nahi dizkio
bizi berri bat eman.

Orduan izango baitu
indarra eta kemena
orduan emango baitu ugari bere fruitua.