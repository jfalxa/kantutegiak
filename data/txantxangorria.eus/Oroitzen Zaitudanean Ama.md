---
id: tx-773
izenburua: Oroitzen Zaitudanean Ama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S_N256O88Y8
---

Orointzen zaitudanean,
Ama,
Sukaldean egoten zara
Mahaia bostentzat atondu,
Aulkian eseri eta leihotik,
Kristala lausotzen duen lurrina ezabatu gabe,
Neguari begira,
Eta ni
-Badakit-
Zeure begien ondoan nagoela.
Patioan solasean eta lagun batek:
"Itolarria sentitzen diat...".
Zer den amaren begian egotea!