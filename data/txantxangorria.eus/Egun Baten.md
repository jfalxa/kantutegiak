---
id: tx-3244
izenburua: Egun Baten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HE8DQunTHkw
---

Egun baten aditu nuenean
Gure herri xumearen negarra
Argi ikusi nuen,
Asaba zaharren gudu hots oihua
Herri txikian ohiko kantua.

Borroka luzea eta amaigabeak
Gogaitzen ditu gure itxaropenak
Ahalegin oro,
Zapuzten ikustean, galtzailearen arantzak
Minatzen gure bihotzak.

Arantzaren zauri irekitan
Ernatuko da arrosa gorria
Izena eta izana,
Sustraitzen dituen, aro berri baten
ametsetako lurraldean.

Gure izana sustraitzen den ala
Gure izena loratuko dela
Euskal Herria,
Euskaraz margotzen, duen herriaren
Zuri gorri eta berdez.