---
id: tx-2584
izenburua: Ez Geldi Orain
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bat_EAHgOWA
---

Jaiak iritsi dira, aze poza
ta amaitu da lan txarra
betiko biharra

apain jantzi ta banoa
ez geldi orain
zeren gorputzan daukat
herdoil apur bat.

Ahaztu nahi det aste osoan
jasan dudan
naguasiarekiko grinak
erokeriak, kapritxoak
sinistu ezinak,
festa dut gogokoena bizian.

Trago batzuk hartu
ta lagunekin egon elkarturik solasean
bakoitzak penak kontatuz
murgildurik alkoholaren noraezean.

El geldi orain
oraintxe nago eta
kristoren martxan,
ez geldi orain
oraintxe nago eta
punttu-punttuan,
ez geldi orain
nago kristoren martxan
nago punttu-punttuan
ez gelditu orain.

Ez gelditu orain
nahi det jarraitu
gau osoa behintzat
eguna zabaltzen den arteraino
gero egun osoa daukat lo egiteko
orain jai degu
festa dut gogokoen oraino.

Trago batzuk hartu
ta lagunekin egon elkarturik solasean
bakoitzak penak kontatuz
murgildurik alkoholaren noraezean.

Trago batzuk hartu
ta lagunekin egon elkarturik solasean
bakoitzak penak kontatuz
murgildurik alkoholaren noraezean.

El geldi orain
oraintxe nago eta
kristoren martxan,
ez geldi orain
oraintxe nago eta
punttu-punttuan,
ez geldi orain
nago kristoren martxan
nago punttu-punttuan
ez gelditu orain.