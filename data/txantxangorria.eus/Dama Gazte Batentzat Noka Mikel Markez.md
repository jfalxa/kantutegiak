---
id: tx-2724
izenburua: Dama Gazte Batentzat Noka Mikel Markez
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/K7RbT3wrdzU
---

Bertso berri batzuek
Dudarikan gabe
Dama gatze batentzat
Komeni dirade
Irun’en serbitzaile
Zeguan mirabe
Bana Naparrua’ko
Alaba zerade

Come On Baby, spend the night with me

Paketa bat bazuen
Nagusiayagandik
Gurasoen etxera
Eraman du andik
Amak esaten diyo:
“Alde idan emendik “
Bestela jipoien bat
Emango dinat nik”

Come On Baby, spend the night with me

Paketa ori zuben
Irun’en artua
Berez naparra bana
Orren arbolatikan
Datorren frutua
Apaiz egin liteken
Ez da ziertua

Lenbizi Donosti’ra
Gero Donijuana
Barbero ta mediku
Adituengana
Ikusi zutenian:
“Zer daukazu, dama?”
“Operaziyo on bat “
Egin biar dit, jauna”

Au da mediku jaunak
Esandako itza:
“Erremediyo billa
Zertako zabiltza?
Barrengua galtzeko
Etsaiak dabiltza
Laister sendatuko da
Zuk daukazun gaitza