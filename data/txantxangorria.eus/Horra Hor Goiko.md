---
id: tx-101
izenburua: Horra Hor Goiko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8ez4ZoukcLM
---

1.Horra hor goiko hariztitxo baten
kukuak umeak egin dozak aurten,
kukuak egin, amilotxak jan ,
hauxe bere kukuaren zoritxarra zan.
Lalaralala, lala...

2.Amilotxari kukuak dirautso:
""Ez dauat itxiko gaur hazur bat oso.""
Ume jaleak erantzunentzat
dino: ""Hire ardurarik ez zaukat"".
Lalaralala, lala..."