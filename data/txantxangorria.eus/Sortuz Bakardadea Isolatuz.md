---
id: tx-710
izenburua: Sortuz Bakardadea Isolatuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WYSqk8VjI0c
---

"Sortuz Bakardadea Isolatuz" Usurbilen,  2020.urtean Koronabirusaren zabalkundeak eragindako  berrogeialdi garaian sorturiko ekimena izan da. Helburu bikoitzarekin, herritarron bizimodua xamurtzea batetik, eta kulturgileok gizartean dugun zentralitatea aldarrikatzea bestetik. Zortzi astetan zehar 40 zuzeneko ekitaldi inguru burutu ziren ekimen honen babesean eta beste hainbatek parte hartu zuten ikus-entzunezko ezberdinak herrian zabalduaz.  Bejondeizuela parte hartzaile guztiei, ahaztezina izan da! Abesti hau, ekimen honen helburuen eta garapenaren   laburpen bat izateko helburuarekin sortu zen.

Hitzak:

Gaur begiratu leihotik, eta kontaidazu zein den zure ametsa.
Gaur, ikusezinetatik,
abiatuko dugu mundu berria.

Sortzen elkarrengana hurbiltzen
Sortzen garena aldarrikatzen
Sortzen geroa
zugan marrazten
Sortzen bakardadea
Isolatzen

Gaur, kontzientziak astindu.
Barruan senti ditzagun sendo kateak.
Gaur, zentzuak aktibatu,
ziega barruan baitago,
askatasunaren giltza.

Goazen:
Garai zailak sortzaileentzat.
Kulturaren alderdi guztientzat
Iluntzen ari bada gure ortzimuga,
guk argituko dugu ez izan duda.
Usurbilgo oinordekotza,
Artze zumeta eta Laboa.

Haien isla gure iparrotzat, gure ardatz, haien bazterretan har dezagun arnas.

Parte jartua:

Hausnarketa egiteko egunak pasata.
Zu ere kulturatik elikatu al zara?
Bakardadearen aurkako
Txertorik onena.
Kultura ez da aisi, kultura herria da.

Gaur, grisa bada nagusi,
koloreztatu dezagun egunsentia.
Gaur, geroa besarkatzen,
marraztuko dugu dantzan itxaropena!


-----
Bideo mantajea: Fernando Lozano (Galiza)
Nahasketa: Kaki Arkarazo
Instrumentalizazioa: Ixak Arruti
Letra: Luken Arkarazo