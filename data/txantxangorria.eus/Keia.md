---
id: tx-2115
izenburua: Keia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FKli1Xckiks
---

310 kilometrotan
Banatzen dira gure oin bustiak
Lokarri bako zapata beltzak
Zelatan ate ertzean

310 kilometrotan
Banatzen dira gure oin bustiak
Lokarri bako zapata beltzak
Zelatan ate ertzean

Iritsiko da amestutako eguna
ta bitartean egon lasai maitea

Sentipenak, kezko hitzak
Haizeak ekartzen
Zure hitzak, ta bihotzak, 
barne minez gogoratzen

uuu...gogoratzen...uuu...gogoratzen...

310 kilometrotan
Banatzen dira gure oin bustiak
Lokarri bako zapata beltzak
Zelatan ate ertzean

310 kilometrotan
Banatzen dira gure oin bustiak
Lokarri bako zapata beltzak
Zelatan ate ertzean

Zentsurak, gezurrak,
Ez gara isiltzen
Eskuak, berunak harrapatu zizun arren

Sentipenak, kezko hitzak
Haizeak ekartzen
Zure hitzak, ta bihotzak, 
barne minez gogoratzen

Sentipenak, kezko hitzak
Haizeak ekartzen
Zure hitzak, ta bihotzak, 
barne minez gogoratzen