---
id: tx-2513
izenburua: Ama Alabak Gaur Greban
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9xv_XJsR3tk
---

Aita semeak eskolan daude
Ama-alabak gaur greban
Aita semeak lanean daude
Ama-alabak borrokan

Berriz ikusi beharko dugu
Mamu sexista auzoan,
Berriro ere ez da faltako
Gure erantzun feminista

Aita semeak eskolan daude
Ama-alabak gaur greban
Aita semeak lanean daude
Ama-alabak borrokan

Eta patroiek ohostu dute
Gure soldatan zati bat
Eta gu gaude altu oihukatzen
Beti inoren menpean

Aita semeak eskolan daude
Ama-alabak gaur greban
Aita semeak lanean daude
Ama-alabak borrokan

Geurea dugu karga guztia,
Geurea dugu osoan,
Has dadila ere gaurtik gizona
Senideen zaintza lanetan

Aita semeak eskolan daude
Ama-alabak gaur greban
Aita semeak lanean daude
Ama-alabak borrokan

Baina gaztea naiz eta daukat,
Etorkizuna eskuan
Ez gaitu hilko patriarkatuak
Ni bizi naizen artean