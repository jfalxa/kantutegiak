---
id: tx-1771
izenburua: Ezekielen Prophezia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7BAuVfLkBlk
---

eremuko dunen atzetik dabil
zulo urdin guztiak miatu ezinik
eta euri zitalari esker bizi da
ikusi zituen gurpil sutsuak
arranotu zaizkio, amoratu
eta bazter batean aurkitzen da

eremuko dunen atzetik dabil
zulo urdin guztiak miatu ezinik
eta euri zitalari esker bizi da
ikusi zituen gurpil sutsuak
arranotu zaizkio, amoratu
eta bazter batean aurkitzen da

desafinatzeke nahasirik gabe
jadanik agure itxura eutsirik
gauaren pareta "sprai"atu duzu

hamazazpi urteak
hamazazpi zuri dira ezekiel
mingotzaren merkromina ezekiel
eter labur bat zara
hari single bat zara
betiko

eremuko dunen atzetik dabil
zulo urdin guztiak miatu ezinik
eta euri zitalari esker bizi da