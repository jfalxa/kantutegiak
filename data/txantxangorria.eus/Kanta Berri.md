---
id: tx-592
izenburua: Kanta Berri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XL3eR8afJno
---

Oi kanta berri zurekin nago
orain duela aspaldi,
zuretzat nuen kar bero hura
suntsitu ez da dirudi.
Beste lanetan iragan ditut
egunak eta gau-aldi,
bainan oraino ez dut ukatu
Euskadi nere aberri.

Oi kanta berri bihotzetikan
ezpainetarat jalaia,
Euskal airean bialtzen zaitut,
zu neure mezularia.
Jende artean izatu banaiz
beresle xamurgarria,
izan zaite zu ene partetik
bakezale den xoria.

Oi kanta berri izan zaite zu
anai arteko zubia
batasunetik libertatera
gidatzen duen bidea.
Kartzelan den abertzaleari
emozute doi argia
etsiturikan den bihotzari
esperantzaun bizia.

Oi kanta berri gure herriak
behar du bide eta zubi,
rtxetan ere baitezpadako
argiarekin, ur garbi.
Horiek denak ez dire aski
bizi badien Euskadi,
hainbat oraino beharko ditut
kanta bat eta kanta bi.

Horiek denak ez dire aski
bizi badien Euskadi,
hainbat oraino beharko ditut
kanta bat eta kanta bi.