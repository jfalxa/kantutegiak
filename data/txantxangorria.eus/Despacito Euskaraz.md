---
id: tx-1147
izenburua: Despacito "Euskaraz"
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jIUE2hMAkPY
---

Jon Urbietak egindako Luis Fonsiren abestiaren bertsioa

Honi buruz hitz egin dute:
Gaztea Boxen
Urola Kostako Hitzan
Baleiken

Jarraitu nire sare sozialetan:


Ay 
Patxi! 
Oh ez, ouu eeez 
Pipi piripi 
Bai, badakizu zuri begira egoteak 
 denbora asko daramala 
 KENDU AI

Bai,zuk gainean daukazun guzti guztia
Jateko modukoa da. 

Nik zugan pentsatzen pasatzet denbora
Behe behetik hasi eta gora gora
Zure altxor magikoaren bila nabil

Hi, maitian albuan ez hai exeri
aldendu hai uaintxe nire tokitik
Hau dena bion kontua da bakarrik

Ayyy Pin txito
Hasteko urdaiazpikoaren bi pintxito
Ta jarraitzeko jagerran 2 txupito
Horrela jartzen gea bai agustito.

Ayyy pintxito, 
Jarraitzeko oain jarrixkiak 2 txikito
Neu bakarrik eangoitut ta agustito
Inork ez dezela esan que yo invito!

GORA GORA

Bapo (x5) jartzen ari gaituk
Hainbeste pintxo ta ardo
Hemen ur basoik ez al do?
Ez do (x5)
Jarrixkiak ordun bi baso handi handi handi sagardo
(ta) Platertxo bat anakardo
 Y por favor ve cobrando
Ta pin txi to