---
id: tx-2781
izenburua: Ez Egin Ihes
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ScT6rnaHFgs
---

Oooooh... 
Oooooh... 

Udazkenaren hosto galtze antzera 
Pauso grisez idatzirik ibilaldi berbera 
Baina ezin da jarraitu begiratuz atzera 
Udaberriko lorerik ederrena baitzera 

Oooooh... 
Oooooh... 

Oztopo batek izkutatzen bazaitu 
Arnas sakona hartuta aurrera begiratu 
Lurrera eroriz gero altxatu ta jarraitu 
Begirada bakar batek elkar batuko gaitu 

Nahiz eta bizi momenturik zailenak 
Beti argitzen du egunik ilunenak 

Ez egin ihes 
Begiratu zure albora 
Egin garrasi 
Joango gara zure ondora 
Begiak itxi 
Mamua desagertuko da 
Egun politenak 
Gaur hastera doaz 

Oooooh... 
Oooooh...
credits
released March 30, 2016 
Irati Odriozola (ahotsa) 
Miriam Quinn (biolina) 
Bihozkada 

Mikel Zelaiak Bonbereneako estudioetan grabatu, editatu eta masterizatua