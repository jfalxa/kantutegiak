---
id: tx-3069
izenburua: Gorlizko Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2b_Zn2SeUR0
---

Koplak: Iñaki Laparra
Harmonka: Goio Garaialde


ZUEN ARTEAN BERRIRO,
GORLIZ  HERRIA PREST DAGO
ZAPIA JANTZI, TRIKI ATERA
ETA EZ AHAZTU PANDERO!!

Kaletik gora eta behera,
neska-mutilak kalera,
gau-pasa ederra egingo dugu
ta lo egiteko hondartzara!

ZUEN ARTEAN BERRIRO. ..

Hondartza ederra benetan,
hainbat denbora lanetan,
baina "ahaztu zaie" jaso harriak
eta eraman kamioietan!

ZUEN ARTEAN BERRIRO. ..

Hamaiketan kantuz kantu,
nahiz eta txarto abestu
kantari ona, kantari txarra,
zuk gurekin parte hartu!

ZUEN ARTEAN BERRIRO... 

Makailu, musika, txoznak,
azoka, bertso ta dantzak.
zuk ezin duzu gehiago eskatu
hamaika dira aukerak!


ZUEN ARTEAN BERRIRO. ..

Zuen lagunen artean,
tabeman edo kalean,
egin euskaraz, tente ta ozen,
ez utzi hizkuntza etxean!

ZUEN ARTEAN BERRIRO...

Umore ona Gorlizen
baina zu hor ta ni hemen,
ez da esan behar, behin eta berriz
aukeratu bide zuzen!

ZUEN ARTEAN BERRIRO. ..
Ospatu jai herrikoiak,
parekide ta euskaldunak,
gora Gureak, gora Gutarrok
eta Gorlizko Gazteak!!

ZUEN ARTEAN BERRIRO. ..

Eskerrik asko zuei,
gazte,nerabe ta umeei,
ta muxu handia jaso dezaten
Herrian ez daudenei!!

ZUEN ARTEAN BERRIRO. ..