---
id: tx-764
izenburua: Esperantza Ezinbesteko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RTrbPzBg67o
---

Zerbait berri zetorrela inork ez zekien 
Mundua aldatuko zela uste ez baitgenuen 
Batpatean ezustean etorri zen dena
harri bat dut bihotzean itotzen nauena.

Begibistan da okerrago gaudena Begibistan da gure ezintasuna.

Sentipen agortezinez guztiok arduran 
halakorik inoiz ez da sortu ta munduan. 
Gizakiok lotan ginen eta heldu zen hau dena 
nolatan izan ote, oraingo hondamena

Begibistan da okerrago gaudena 
Begibistan da honen zailtasuna.

Hedatu da, hondatu da gure ikusmena 
tupustean ilundu da guztion ahalmena. 
gaixo zeuden ta joan diren ezin agurtuan 
oroipenez, doluminez zaudete goguan

Ta esperantza dugu ezinbesteko 
Esperantza gaur, geroa argitzeko

Abailduran diren horiek une hunkigarrian 
samin osoz ta atsekabez gure memorian.
Zenbat gozagaitz dugun eta, zenbat ezbeharreko 
Zoriona zorigaitza biok bat-bateko.

Ta esperantza da ilundu zaiguna  
Esperantza, orain behar duguna. 
Esperantza dugu ezinbesteko
Esperantza gaur, geroa argitzeko