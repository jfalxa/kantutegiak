---
id: tx-459
izenburua: My Mother Told Me Vikings
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bG2536mIyag
---

Amak esan zidan
Erosiko nituela
Arraundun galerak
Lur berriak aurkitzeko

Brankan zutik nago
Ontzi honen jabe
Ortzemugaraino
Etsaien lepoak moztuz.