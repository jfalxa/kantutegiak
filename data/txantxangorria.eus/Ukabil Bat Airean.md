---
id: tx-2567
izenburua: Ukabil Bat Airean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oEilOdUHL8c
---

Letrak: OIhana Iguaran
Musika: Isiltasun Ituna abestiarren melodia. Kherau taldea

Remiren omenaldi egunean abestia. 2017/07/31


Gaur zortzi urte betetzen dira
bihotz bat isildu zela
gaur zortzi urte haren taupadak
herriarenak direla
gaur zortzi urte oroitzen dugu
atzo balitza bezela
ta zortzi urtez sentitzen dugu
oraindik hemen zaudela.

Ni ere hala nindoan mundu
bidezko baten idean
Josune eta zure eskutik
utopiara bidean
baina bidetik harago joan
zinen hegaldi librean
utzi gintuzun, hankak lurrean.

Eta geroztik itxi gabeko
zauriak dauzkagu nonah
ez badigute gure aitortza
eraikitzen uzten lasai
goitik agintzen nahiz erraza den
amorrurik ez dela nahi
nire gosean nik agintzen dut
nire tristuran ere bai.

Libre hautatu nahi dugulako
non eta nola negar egin
gure egia ta memoria
ez tratatzeko desberdin
herri bezela segiko dugu
mina bihurtzen egi-min
isildu diren bihotz guztien
justizia egin dedin.

Gaur ere hala darraigu mundu
bidezko baten idean
zuen ametsei pausoak gehituz
utopiara bidean
iparrorratz bat ikusten dugu
zuen hegaldi librean
segiko dugu, hankak lurrean
ta ukabil bat airean.