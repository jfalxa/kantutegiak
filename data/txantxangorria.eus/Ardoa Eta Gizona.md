---
id: tx-1713
izenburua: Ardoa Eta Gizona
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tW3Cinqpd0c
---

Ardoa eta gizona
dijoaz kantatzera
han dan andere gizena
entzule dutela.
Gizonak galdetzen dio:
Ona ote zera?
Bonbilean nago eta
hasi zaitez edatera.

Ardoak erantzuten dio
pozak zoraturik
ez da nigana etorri
zu bezelakorik.
Baitutan izan naute
kortxoz estalirik
zuk utzi tantorik.