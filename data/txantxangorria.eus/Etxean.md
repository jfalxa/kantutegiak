---
id: tx-2722
izenburua: Etxean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O_130XNnnTc
---

Kaleak ixildu dira, berriz neguaren hotza
Aurpegietan nekea, eskuan rafiazko poltsa.
Asteroko errutina azkeneko urteetan
zenbat elkartasun keinu kabitzen den furgonetan

Betiko kilometroak, betiko gasolinerak,
betiko ilusioak eta betiko minberak.
Ixilpeka doazenak, zurrunka ari direnak,
istripuz joan zirenak, gogoan dauzkagu denak

NOIZ ARTE, NOIZ ARTE? IRAUNGO DU MIN HONEK BIHOTZEAN
GUREKIN, GUREKIN! NAHI ZAITUGU BERRIZ ETXEAN

Espetxera iristean hatz markak eta karneta
katxeoa ez denean, arkua edo raketa
lokutoriora bidean dena horma dena hesi
agertzen zarenerarte dena gris dena gabezi

Ondo nago ez larritu, ez dugu hain gaizki jaten
baina begiradak inoiz ez du gezurrik esaten
maitasuna mugaturik 40 minututara
eutsi goiari laztana, laster ikusiko gara!

Beste bisita bat beste marra bat egutegian,
hurrengoan etorriko zara gurekin agian,
furgonetan bueltan datoz, animozko hitz xamurrak
makillajeak estali ezin dituen zimurrak.