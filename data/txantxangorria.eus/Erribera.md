---
id: tx-1483
izenburua: Erribera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Msv6tn9shM4
---

Erribera, Erribera,
zure landen zabalera
ortzi muga den hartan
mugatzen da.

Zure lur emankorretan
isurtzen diren asmoak
gogotsu hartuko al ditu
lur gozoak.

Zure gaztelu zaharrek
gorderik duten antzina
hats tristetan mintzo da
haren mina.

Horma zahar arraituetan
txoriak dira kantatzen
mendetako lo geldia
salatzen.

Nafarroa anai zaharra
kondairaren lehen sustarra
bego higan arbasoen
amets hura.