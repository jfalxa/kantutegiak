---
id: tx-2894
izenburua: Bortietako Elhürra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2B51vqfohgo
---

Bortietako elhürra
osoki ari beita hurtzen,
Primaderako txoriak
hasiz geroztik han khantatzen.

Denbora ederrer bürüz
artzainak gira phartitzen,
Nur gure ardi saldueki
belhar phüntaren untsa txerkatzen.

Bortüko bidia beita,
bai segür! aski phenagarri,
Batx batxa juraiten gira,
oi denak, korajez betherik.

Zunbat ixtoria khuntatüz,
erriz batak bestiari.
Xahako bürü gorritik,
drago zunbait üsü thiratürik.