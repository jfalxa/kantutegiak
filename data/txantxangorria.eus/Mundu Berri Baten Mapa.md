---
id: tx-1827
izenburua: Mundu Berri Baten Mapa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kG0kC83qfR8
---

from Ahots Gabekoen Ahotsa by Skakeitan

Behe-lainozko egun batez atera ginen portutik 
Mastan mila koloredun bandera jarrita zutik 
Enbatek eta arrokek jarraitu arren gertutik 
Donostia abordatzen aritu gara ordutik 

Izan gaitezen bihurri, izan gaitezen pirata 
Partxe bat begian jarriz egunsentiari kanta 
Belak zabalduta eta auzolanari tiraka 
Bihotzean daramagu mundu berri baten mapa 

Izan gaitezen pirata! 

Belak, zabaldu ditugu 
Batu ditugu indarrak 
Lotu, guregana eta 
Aska ditzagun amarrak 

Hamar! Batu ditugu indarrak 
Lortu! Mundu berri baten mapa 

Zatoz zabaldu gaitezen itsaso berrietara 
Zatoz itsatsi ezazu zure bandera mastara 
Zatoz txalupa euskaldun, parekide honetara 
Hamar urteren ondoren gero eta gehiago gara 

Izan gaitezen bihurri, izan gaitezen pirata 
Partxe bat begian jarriz egunsentiari kanta 
Belak zabalduta eta auzolanari tiraka 
Bihotzean daramagu mundu berri baten mapa 

Izan gaitezen pirata! 

Belak... 

Belak..