---
id: tx-3021
izenburua: Helduleku Guztiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-he6mzzQIVQ
---

HELDULEKU GUZTIAK

batzutan gogoratzen naiz ahazteko beharraz
entzuten uzten ez digutenen ardailaz
sarri gogoratzen naiz amaia egañaz
bakr anaien baloi zulatuaz
sasikume aldraz

hemen zegoen zinema ttipi hartaz
zure hitzen ausentziaz
buruz ikasi genuen espetxeen izenen zerrenda luzeaz
sarri akordatzen naiz disko urdin hartaz
akorde sinple baina hunkigarriez

grabitatea baino ez dago hor kanpoan
dena dago orain erortzeko zorian

batzutan gogoratzen naiz eta bestetan ez
nahiak eta domaiak tematiak direnez
sarri gogoratzen naiz etorkizunaz:
gaur + gaur + gaur + gaur + gaur + atzotik tanta bat

grabitatea hor kanpoan
dena dago erortzear
maitea urtu ziren helduleku guztiak