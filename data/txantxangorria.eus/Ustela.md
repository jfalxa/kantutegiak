---
id: tx-1850
izenburua: Ustela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tgi6RuL54YE
---

Nire lagun xelebrena
nire mailu urdina
farretan irabazle
usteltzen ari da da da
inpotentzia aurrean
lege onena izandakoa...
ez dut sinisten legeaz
lege-gizon mailu gabe naiz ni

Usteldu bahaiz zer egingo didate
mailu behar zaitut... ez usteldu

Ez pentsa izarrak edo metaforak
kolpatzea nahi dudana
ezta ere poemak egin
gaizki joatenzaidanean
mahiak, kristalak... apurtzea baizik
defentsa onena zara
legeen gorilen aurrean.

Usteldu bahaiz zer egingo didate
mailu behar zaitut... ez usteldu