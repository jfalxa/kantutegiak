---
id: tx-275
izenburua: Su Ñimiño Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bRYnRt3tU_8
---

Su ñimiño bat Mikel Urdangarinek 2021eko udazkenean sortutako abestia da. 
25 urte betetzen dira Urdangarinek Haitzetan lana argitaratu zuenetik. 2021eko azaroaren 26an Haitzetanen birrargitaralpena egingo du eta Su ñimiño bat abestia diskoan agertuko da.

"Su nimiño bat" kantan Mikelek bere taldea du lagun: Koldo Uriarte, Rafa Rueda, Nika Bitchiasvili, Jon Cañaveras, Ander Hurtado de Saratxo eta Alison Keable. Gainera, gonbidatu berezi bat ere du oraingoan akordeoian: Josu Zabala, Hertzainak taldeko kide ohia. 

Zirriborro ekoiztetxeak eta Oier Aranzabalek (*Zart)  egindako bideoklipa, urrian grabatu zuten Bilboko Kafe Antzokian. 
Abestiaren  grabaketa eta nahasketak, aldiz, Koba Estudioko Xapnek egin ditu.

::::::
Su ñimiño bat

Aurki beteko dira 25 urte haitzetan argitaratu nuenetik. Oharkabean joan den ia mende laurdena. Oinez hasi orduko etengabeko bidegurutzetara ekarri nauen pausua. Gaur urteurren bereziaren ateetan jarri nauen pausua. Asko idatz nezake denbora guzti honetan bizitakoaz, gehiegi ziurrenik. Beraz, ideia bakarrari helduko diot pentsamendu hau azal dezadan. Inoiz hala sentitu bada ere sekula bakarrik egon ez den bakarlaria naiz. 

Denetarik bizi izan dut, ospakizun handiak, suziriak, uholdeak, desertua, baso berdea...kontraste handiko iruditegia. Eta, inoiz ere, bakarrik sentitu naiz oso musikaren bidaia intentso honetan. Baina azken hau sentipena baino ez da, ez du zertan egia izan. Izan ere, banitateak engainura garamatza sarri, tentagarri egiteraino, ez garen hori sinisteraino, ez den hura bizitzeraino. Eta  ez da egia. Egoera zailenetan ere, gutxienez nire buruarekin egon izan naiz beti. Eta norbere baitan norbera baino jende eta mundu gehiago bizi dira. Ez gaude bakarrik. Nire ahotsa, nire kemena eta indarra ez dira neureak bakarrik, ezta neure beldurrak ere. Nigan bizi diren arbaso eta lagunenak ere badira. Ezagutzen ez ditudan baina nolabait ere ondoan bizi diren mundu eta jende batenak ere badira.

Horiek guztiak dira oraindik nigan bizirik dirauen su ñimiñoa.


▶ SU ÑIMIÑO BAT - Abestiaren letra
"Etxetik partitzear baletor zalantza
mamu zaharrak zirika, hotzikara latza
betor kemena, betor arbasoen kemena

oinez hasi orduko baletor dardara
pausu bati hurrena balitzaio falta
hautsi dezala, hautsi buruak tranpa
hautsi dezala hautsi buruak tranpa

badatoz gerrariak, badatoz
zaldi erraldoien gainean, milaka datoz
herio danborrek hots
beldurrik ez eta zain
egon

deskuidoz noraezak hartzen bazaitu
ipar haizerik ez eta belak huts
bihur dadila izua olatu
etsia lema, nekea erramu

gaba beltzean itsu gelditzen bazara
ez zeru eta ez lur gelditzen bazara
argitxo bat piztuko dizut lehio ondoan
su nimiño bat bihotz ondoan

badatoz otsoak, badatoz
ezpatak airean zuzen zugana datoz
herio danborrek hots
ez ikaratu ta zain egon
egon

Hortxe aurkituko nauzu bidean
bidezidorrean, ibar luzean
baso berdean, porlanezko oihanean
su nimiño bat bihotz ondoan"