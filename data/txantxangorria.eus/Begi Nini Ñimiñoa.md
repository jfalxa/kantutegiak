---
id: tx-528
izenburua: Begi Nini Ñimiñoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/shezBet5Eh0
---

Hitzak: J. Sarasua
Musika: J. Tapia

«DULTZEMENEOA»
TAPIA ETA LETURIA


Zulo txiki ñimiño bat

hori da begi ninia

Euskal Herria barrutik ikusteko ipinia.

 

Herri txiki ñimiño bat

hori da Euskal Herria

mundua bere barrutik

konprenitzeko jarria.

 

Globo txiki ñimiño bat

hori da mundu urdina

unibertsoa barrutik

entenitzeko egina.

 

Herri txikien mundua

mundu txikien multzoa.

Begi nini ñimiño bat

hori da unibertsoa.