---
id: tx-1939
izenburua: Bixotza Suten
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/swRdsvD8RXc
---

Ez dekogu denbora askorik
norbera izateko be ez dago astirik.
Beti presaka, ganora barik
buelta ta buelta heldu ezinik.

Begixek beti lainoetan
boltsikoak haizez beteta.
Buruan neurona bakarra
bat baina nahiko makarra
aurpegixen tximiste gorri bet
eskuek hotz, bixotza suten.
Suten,
su/ten, bixotza suten!

Momentu bakotzien
ta pausu bakotzien
nahi dodan moduen bizi naz ni.
Zuretzat galazota dauen guztixe
neuretzako da,
neuretzako (neuretzako).

Zazpi goaz cuatrolatastxien
zure bularrak nire aho partien.
Martxarako kantu aproposa!
Nun gogoa, han zangoa!
Eskuek hotz, bixotza suten.
Suten,
suten, suten bixotza suten!

Momentu bakotzien ta pausu bakotzien
askatasunien bizi naz ni


Zuretzat galazota dauen guztixe
neu/re/tza/ko da, neu/re/tza/ko

Momentu bakotzien ta pausu bakotzien
nahi dodan moduen bizi naz ni.
Zuretzat galazota dauen guztixe
neuretzako da,
neuretzako (neuretzako).