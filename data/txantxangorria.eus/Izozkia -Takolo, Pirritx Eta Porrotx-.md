---
id: tx-3296
izenburua: Izozkia -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S67YW6uPo5k
---

Iritsi da uda bero-beroa
gori-gori dago termometroa
Ireki ahoa, mugi mingaina,
izozkia behar dut bai alajaina
izozkia behar dut bai alajaina

Bola bat, bola bi,
txokolate marrubi
Hau tori, nik hori
askarian ez dadin erori