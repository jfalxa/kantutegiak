---
id: tx-3128
izenburua: Hor Baititut Lagunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UZaRg6PWwRk
---

"Hor baititut lagunak" (With a little help of my friends)
Musika eta hitzak: John Lennon - P.MCartney - Joe Cocker
Euskarazko adaptazioa: Juan Garzia

Donostia - Sn.Sn. 2013.12.22
Kursaal Auditorium

Ahotsak: Petti - Olatz Prat - Estitxu Pinatxo
Musikariak: Xanet Arozena, Txema Garcés, Oriol Flores, Mixel Ducau, Luis Fernandez, Miren Zeberio, Xabier Zeberio, Iosune Marín.
Zuzendaritza artistikoa: Joxan Goikoetxea

Audio Teknikaria: Josean Ezeiza
Audio nahasketak: Jose Mari San Sebastian - ETB
Argiak, bideo proiekzioa eta dekoratuak: Sendoa Lusarreta

Bideo grabaketa eta errealizazioa: Dani Blanco

AM Kultur Promotora

****************************************­************************************

Kantuz hasten banaiz tonutik at,
egingo al didazu alde?
Zugana doa, horra, doinu bat:
saia nadin huts egin gabe.

[Baietz lortu: noski] hor baititut lagunak.
[Goia joko: noski] hor baititut lagunak.
[Gogor eutsiko: noski] hor baititut lagunak.

Zertan naiz ni maitearen faltan?
(Jo al zaitu bakardadeak?)
Zer sentitzen dut arrastirian?
(Tristura al dakar isilak?)

[Kezkarik ez: noski]
hor baititut lagunak.
[Goia joko: noski]
hor baititut lagunak.
[Gogor eutsiko: noski]
hor baititut lagunak.

(Inoren beharrik bai?)
Maitatzeko nor edo nor.
(Berdin al dizu nornahi?)
Maitagarri den edonor.
(Ziplo maitemin al liteke bat?)
Aldiro da hori gertatzen.
(Irudirik pizten iluntzeak?)
Esangaitzak, hain nire arren.

[Baietz lortu: noski]
hor baititut lagunak.
[Goia joko: noski]
hor baititut lagunak.
[Gogor eutsiko: noski]
hor baititut lagunak.