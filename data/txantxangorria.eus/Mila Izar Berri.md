---
id: tx-1800
izenburua: Mila Izar Berri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jYXGdknlp9U
---

Historia idazteko unea dela esaten dutean,
egia azaltzeko momentua saltzen dituztenean,
garaile, galtzaile, biktima edo hiltzaile,
izango garela behin ta berriz errepikatzen dute.

Dena ikusi nahi ez duen begirada bakarraz,
ezin da margotu harri zahar baten historia zaharra,
bakarmena, oroimena ez dira inorenak,
denak joan baziren gogoratu ditzagun denak.

Eta maitasuna, ezinda zapaldu.

Ez ahaztu milaka izar berri piztu izan direla,
Euskal Herriko zeru gaianean.

Egon ziur guztiak eramango ditugula,
betirako gure bihotzean.

Aurrera egiteko behar dugun arnasa hartzerakoan,
gogoratu haien izarrek ekarri dutela eguzkia,
ezin du amestu ahazten duen herriak,
irabaztera goaz, lanera eta zabaldu irriak.

Eta maitasuna, ezinda zapaldu.

Ez ahaztu milaka izar berri piztu izan direla,
Euskal Herriko zeru gaianean.

Egon ziur guztiak eramango ditugula,
betirako gure bihotzean.


Ez ahaztu milaka izar berri piztu izan direla,
Euskal Herriko zeru gaianean.

Egon ziur guztiak eramango ditugula,
betirako gure bihotzean.

Ez ahaztu milaka izar berri piztu izan direla,
Euskal Herriko zeru gaianean.

Egon ziur guztiak eramango ditugula,
betirako gure bihotzean.

Ez ahaztu...

Ez ahaztu...

Ez ahaztu...

Ez ahaztu...