---
id: tx-1907
izenburua: Adela
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MsiApn3EYZs
---

Adela,
gure
arabar lorea,
giputz arroka,
hi,
Bizkaiko burdina.

Orain,
batere zaratarik gabe
joan hatzaigu,
eskoletan ere,
euskal eskoletan ere,
klaseen arteko burruka
gogorkien
lehertzen zenean,
zeinean
gizonak
gizonaren kontra,
eta euskaldunek
euskaldunen kontra
dihardukaten.

Azken agurra
erran
deraunagunean,
gogorrenari ere
pausatu ziaion
elur maluta bat
begiaren ertzean
eta
gure amorruagatik
gure gorrotoagatik
egin dinagu
zin finko bat:
azken kolpea eman arte
iraungo dugula
gure burruka honetan
hire memorian
hire memorian
hire memorian!


Hain sinple sinpleki
joan hatzaigu hi,
Adela geurea,
Adela herria,
Adela berria,
gure
Adela gorria.

Hil haiz
lanik eta soldatarik gabe,
esplotadoreek
zerua goian eta lurra behean
utzia;
hil haiz
esplotadoreek
azpian lur hotza gainean harria
jarria,
baina hire erraiek
esperantza hobeko fruituak
sortuko zitinate,
hire bularretako
esne biziaz
helduko 
direnak.

(Gabriel Aresti)