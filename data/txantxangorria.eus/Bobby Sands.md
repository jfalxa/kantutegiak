---
id: tx-2073
izenburua: Bobby Sands
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/43SMb3dGMEw
---

Begira mendira, kantua entzuten da
Askatasunaren oihartzuna, 
kartzeletatik guregana.
Umetako ametsak,
inoiz lortu ez direnak
Baina bidean gelditu diren denak
Benetan merezi dutenak.
Bobby Sandsek zioen moduan
helduko da azken eguna
Gure haurren irribarrea,
izango da mendekua.

Jaso da mezua
iritsi da gure eguna
Gudari lagun eta senideak
badatoz irrintzi artean.

Bobby Sandsek zioen moduan
helduko da azken eguna
Gure haurren irribarrea,
izango da mendekua.