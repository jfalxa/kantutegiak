---
id: tx-1317
izenburua: Maitatzea Nahiago
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-gt5oxnb52Q
---

Eneko Larrañaga
(+34) 677 21 00 78
enekora82@gmail.com
www.enekora.com


Galdu naiteke, baina gaur ere maitatzea nahiago dut.  
Harresiak eraiki ahal ditut,  
zubiak ere besoak bait ditut, 
suaren garretan harri bihurtu, 
baina gaur ere maitatzea nahiago dut. 
Ez dizut zuri gezurrik esan nahi, 
batzutan galdu egiten naiz. 
Holakotan nahiko nuke arima arindu, 
kantatu-dantzatuaz baretzen naiz. 
Galdu naiteke, baina gaur ere maitatzea nahiago dut. 
Pozik-pozik eta aske – bizitzeko jaioak gara 
Sintoniza dezagun elkar – bizitzeko jaioak 
Zainetatik poroetara – bizitzeko jaioak gara 
Bihotzetik azalera 
Aldatzeko behar etengabe honetan. 
Galdu naiteke, baina gaur ere maitatzea nahiago dut. 
Pozik-pozik eta aske – bizitzeko jaioak gara 
Sintoniza dezagun elkar – bizitzeko jaioak 
Zainetatik poroetara – bizitzeko jaioak gara 
Bihotzetik azalera 
Aldatzeko behar etengabe honetan. 
Aldatzeko behar etengabe honetan.
Aldatzeko behar 
Barnean ditudan beldurren altzoan, 
norabiderik gabe noa itsasoan. 
Gaur urari begiratzean, 
ohartu naiz behar dudan hori ni naizela. 
Izar handi hori, ederra ni naiz. 
Uwon djele, djele-djele, uworedje djeledje