---
id: tx-2562
izenburua: Elurra Teilatuan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9qTJRTRtGmI
---

Elurra teilatuan
zakua lepoan
ibili beharko dugu
aurtengo neguan.
Riau, riau, riau
rakataplau...
Hau duk umorea
utzi alde batera
euskaldun jendea.
Riau, riau, riau
rakataplau...
Hau duk umorea
utzi alde batera
euskaldun jendea