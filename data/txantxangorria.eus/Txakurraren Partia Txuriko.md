---
id: tx-701
izenburua: Txakurraren Partia Txuriko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7_X3gA7yXKQ
---

Gu gazte giñadela txalupa guztiak
Zakurtxo bat ohi zuten ontzi barrenean
Begi erne, abilla, ez zen zaunkaria
Ihes zihoan arraina harrapatzailea
Seme esango dizut nola gertatzen zen
Punttutako arraia, suelto batzuetan
Legatza ospa, ihes, txakurra jauzten zen
'Ta bet-betan arraia hartzen zuen hortzetan.
Lana horren saria 'txakurraren partia'
Deitzen genuen guziok, ongi merezia
Maitea-maitea zen txakur ehiztaria
Txalupa betetzen zuen anima gabeak.
Unea etorri da zuri kontatzeko
Zer nolako txakurra zen gure Txuriko
Uhin izugarriak ez zuen izutuko
Arraia utzi baino, lehenago itoko.
Goiz itsusi batean ez naiz ez ahaztuko
Bere lana beteaz jauzi zen Txuriko
Baga haundi artean, ehiza ez utziko
Legatz haundi batekin ito zen betiko.