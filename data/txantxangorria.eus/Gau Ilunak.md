---
id: tx-2995
izenburua: Gau Ilunak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/fzEWoleT6xc
---

Gorliz-Plentziako NON OTE  taldeak eginiko abestia Euskal Herriko gazteria  bizi den egoera azaltzeko, Espainiar inJUSTIZIAk injustizia injustiziaz borrokatuz giza-eskubideak ez ditu errespetatzen.