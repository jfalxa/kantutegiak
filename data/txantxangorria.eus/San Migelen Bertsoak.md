---
id: tx-2253
izenburua: San Migelen Bertsoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jpfq40cAijI
---

Audioaren ekoizpena eta ahotsa: Josetxo Artajo Zaragueta





San Mikelen  bertsuak
Noa kantatzera
Aditu nai duanik
Iñor baldin bada.
Berri txarrak  dabiltza
Mundutik barrena.
Notiziosko naski.
Geyenok al gera.
Aingeruek lapurrak
Eramana dela
Maiatzeko illaren
hamaikagarrena
Lapurrek San Migelen
sartu ziradena.
Baita lapurtu ere
berak nai zutena
Azkenik erlekie.
San Migelarena
Orrek ematen digu.
biotzian pena

San Migelen sartzeko
ladronen pregunte
ogi eta ardoa
gald,in omen zuten.
Orduko (y)alkarreki
junta ina zute
Goazen Frantziarat
arrobatu zute
ebek utzi bear tian
oinekoak lotute.
Bat iruñeko seme
ez ura obea
ark ere Frantziarat.
San Migelen gurea.
ikusi zutenean
Frantziako legea
erre nai zituzten
zilar eta urrea
baina ezta onel
zerurat bidea...