---
id: tx-790
izenburua: Logela Honetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/D2slLpjznvc
---

Musika eta letra: Iñigo Etxezarreta (E.T.S.)& Ander Valverde (Green Valley)
Ekoizpena eta konponketak: Pello Reparaz (DeepSoda) 
Grabaketa eta nahasketa: Paxkal Etxepare (DeepSoda)
Masterizazioa: Simon Capony (DeepSoda)
Arte zuzendaritza: Joseba Razquin (DeepSoda)
Argazkia: Ibai Acevedo
Zuzenketak: Zuriñe Gil
Ekoizpen exekutiboa: Baga-Biga

Zarata honen gainetik oihuka
arreta eskatzen duen ahotsa,
azkarregi doan bizimoduan
izateko leku lasaia.

Bakardadeari besarkatuta, 
itzalaren lagun onena.
Ilunpean berreskuratuko dut 
gero aterako dudan argia.

Logela honetan dago nire aterpea.
Logela honetan entzuten ditut taupadak.
Logela honetan malkotik irribarrera.
Logela honetan…

Etengabeko mezuen soinua.
Uneoro guztion eskuetan.
Arnasaren oroimena galduta,
sentitzen naiz batzuetan hauskorra.

Lasaitasunari besarkatuta,
hemen eta orain egoteko aukera.
Isilean berreskuratuko dut
gero aterako dudan ahotsa.

Dame conciencia y dame vida.
Dame una razón, dame paz que me ilumina.
Dame una batalla que yo limpio mis heridas,
desde la habitación del corazón de mis pupilas.

Cada mañana  me pregunto hacia dónde estaremos yendo,
si hemos perdido el rumbo natural de nuestros cuerpos.
Con la necesidad de vivir de prisa y corriendo
vendemos nuestras almas a cambio de sufrimiento.