---
id: tx-3257
izenburua: Badator Marijaia -Kepa Junkera-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/N_JAUeU6VkQ
---

Musika: Kepa Junkera
Letra: Edorta Jimenez

Aste nagusia
bakarra dago
hamar gauekoa
munduan

Abuztuan Bilbon
denok kalera
katuak eurak be
jaietan

Mari, Mari,
Marijaia dator
Mari, Mari
Marijaia dator

Uger, uger
Bilboko uretan
Mari, Mari
Marijaia dator

Abuztuan danok
zahar eta gazte
gizon eta andre
jaietan

Zapia lepoan
alkar hartuta
kolore guztiak
dantzetan

Goxa eta erlojua
ez ei doaz batera
bata edo bestea
zoratu egin da

Hara hara hara
hara nor datorren
hara hara hara
gure Marijaia

Ene ene ene
oi ai ene bada
aste hau pasata
barriro joango da

Marijaia bera
gure Marijaia
Bilbora etorri da
Aste Nagusira

Aste Nagusi
bakarra munduan
bakarra munduan
Marijaia