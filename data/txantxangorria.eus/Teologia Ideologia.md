---
id: tx-577
izenburua: Teologia Ideologia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I7wLmygIhFk
---

Zeru altuan zegoen jaunak
egina zuen mundua,
sei egunetan izerdi patsez
moldatu eta ondua.
Zazpigarrenez deskantsatzia
atera zuen kontua,
izadiaren igeltserua
etzen batere tontua.

Perfekziozko obra haretan
laister sortu zen nahaspila,
Adan maltzurra abiatzen da
sagar errearen bila.
Lehendabiziko askatu zuen
gerrikoaren hebilla,
Eba ikaraz zegoen baina
bekaturako abila.

Sagarra kozkaz irentsi orduko
hor entzuten da kondena:
“hartu nahi hukek baratza hontako
fruta gozo ta onena;
hoa kanpora, lotsagabea,
eta paga zak ogena,
eskulanaren beharra zegok,
ematen diat ordena”.

Adan badoa triste ta mutu
Paradisua galduta,
Bilbo aldera aillegatzen da
gaueko trena hartuta.
Haurrak negarrez ikusten ditu,
andrea haserratuta,
Altos Hornos-en peoi sartzen da
kontratua firmatuta.

Gertatutako deskalabruak
mingosten diozka gozuak,
eta gainera pagatu behar
piso txar baten plazuak.
Komodidade handirik ez du
proletario auzuak,
eta bizkiak erditzen ditu
bere emazte gaixuak.

“Hori bakarra behar genian”
dio amorru bizitan,
baina aguro agertzen zaio
adiskide bat bisitan…
hiru bat orduz mintzatzen dira
leihoak erdi itxitan,
karneta ere eskeintzen dio
biharamuneko zitan.

Handik aurrera Adan jartzen da
su ta gar kausaren alde,
folleto batzuk eman diozkate
hutsaren truke, debalde,
Marx eta Lenin irakur ditzan
gehiegi nekatu gabe,
hiru astean egiten zaigu
fede berri baten jabe.

Eta horrela bihurtzen dira
teologiak programa,
nolabaitean arindu nahirik
gizonak daraman zama.
Ez baldin badugu guztiz galdu nahi
nahiko eskas duen fama
Mosku aldera itzul liteke
Erromako elizama.