---
id: tx-2601
izenburua: Jakintasuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/g6WQGSeNuZU
---

Jakintsu batek erran omen du duela berrehun urte: 
Etsai gaiztoek Euskal-Herria hau xirxifrikatuko dute 
gure lur onak josteta leku bilakatuko dituzte. 
hori pentsatu zuen gizonak bazuen zonbait bertute.

Txapel handien menian dira flakoak edo pobreak; 
neronek lenbixi urrikari ditüt langile miserabliak, 
ta horiek laguntzaren partez badituzte kontrarioak; 
eritasun txar horren sukarrak, ondorio izigarriak.

Kapitalistak pobrearentzat axola batere ez du ; 
partikularzki laborariak kontrario sobera du ;
aberats puxant errege haundi, dutenak hoinbeste gradu 
denek goseak hil behar dute lurrak ematen ez badu.

Orai zer egin ikus dezagun konparazio batera, 
belar gaixtorik etortzen bada landare onen artera, 
hura behar da kupido gabe ongi zainetik atera, 
eta bota gehiago trabarik eginen ez duen tartera.