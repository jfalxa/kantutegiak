---
id: tx-797
izenburua: Munduaren Esker Ona
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GaiFpSyh1xA
---

Haize otzeko lurrera
biluzirik heldu zinaten
ezer gabe, dardarez,
andreak zapi bat ematen.

Premiazkoak izan gabe
zuentzat karrozarik ez ziraden,
mundura arroztu zinaten
gogoko izan zuenean batek.

Haize hotzeko lurrera
biluzirik heldu zinaten
ezer gabe, dardarez,
andreak zapi bat ematen.

Munduak ez du zorrik zuekin
joateko libre zerate;
ezer ikusterik ez askorekin,
baiñan haurrak beti herabe.

Haize otzeko lurrera
biluzirik heldu zinaten
ezer gabe, dardarez,
andreak zapi bat ematen.

Haize otzeko lurretik
zoldaz beterik ba zoazte,
denek mundua maitaturik
lur puska bat izan baduzue.

Haize hotzeko lurrera
biluzirik heldu zinaten
ezer gabe, dardarez,
andreak zapi bat ematen.