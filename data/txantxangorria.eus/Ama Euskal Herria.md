---
id: tx-3166
izenburua: Ama Euskal Herria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mhoxq2pMpQI
---

Delikatua baitut egungo sujeta, 
hasi aintzin egin dat zonbait gogoeta, 
gure arteko asko ele entzun eta, 
euskaldun agertzea ez baita jasteta. 

Ama Euskal-Herriaz naiz naigabetua, 
uzterat doalakotz betikotz mundua, 
bere seme alabez abandonatua; 
nork barka dezaguke gure bekatua? 

Badakir askoz ere nukela hobea 
nehori ez agertu ene dolarea; 
odolaren mintzoa eduki gordea 
nahiko nuke, bainan ezin dat ordea. 
Ama Euskal-Herriaz... 

Bata dela eskuina, bertzea ezkerra, 
euskaldunen artean dugu herra, 
ahantziz, Ama, zure maitasun ederra; 
horra zure seme hok dautzugun eskerra. 
Ama Euskal-Herriaz... 

Baztertu dugulakotz zure maitasuna, 
desegin da betiko gure batasuna; 
gu baitan aditzen da gudu oihartzuna, 
hau da zuri bizia hekendu dautzuna. 
Ama Euskal-Herriaz... 

Lehen sartu zitzauzun ezpata zorrotza 
zure etxe nagusi jartzean arrotza; 
halere etzitzauzun gelditu bihotza; 
etxekoen eskutik duzu heriotza. 
Ama Euskal-Herriaz... 

Hil hobi huntan noizbat norbait badabila, 
zure izaitearen herrestoen bila, 
lurpetik entzunen du deiadar bipila: 
hernen dago ama bat bere haurrek hila! 
Ama Euskal-Herriaz..