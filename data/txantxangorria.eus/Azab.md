---
id: tx-353
izenburua: Azab
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LQu30NfXWO4
---

Abestiaren grabaketa: Nebula Studios.
Nahasketa: Jean Phocas.
Irudiak: Sonia Aguayo eta Unai Martin.
Muntaia: Unai Martin.
Eskerrak: Eneko eta Hekthor.


Letra:

Txikia zinenetik egundoko arazoak
orain handitu zara ta zu zara arazorik handiena.
Behin ta berriz saiatzen zara,
baina mundua ez dago eginda zure neurrira.

Inork etzaitu maite dio zure buruak
drogekin ixiltzen duzu bera ixiltzen ez bada,
konponbideen bila eman zenitun urteak
baina gizarteak etzintuen lagundu eta…

Zipaio bilakatu zinen,
orain danek dakite ondo nork agintzen duen hemen.
Harro zabiltzan arren,
barruko amorrua ateratzerakoan ezin duzu eten.
Pertsona ona zara baina hala ere
moralak ez du lekurik polizian barne
eta zure nagusiak agintzen duenean 
zure pentsamoldea aldatzen da hala behar bada.

Hasieran errez egiten zenuen lan
berriaren motibazioa zela medio
baina komisarian ikusi medioak
eta hortik aurrera jarri zinen serio:
zalantza bakoitzeko porra atera,
haurtzaroko gabeziak gaitasun egitera,
jabeen izenean pausu bat ez atzera,
edonork ezin du izan zipaio jarrera….


Gestionatu gabeko traumak behar dira,
ideologia falta are gutxiago gorria,
bira eman ideiei, arazoei distira,
jarrera arbitrarioa sartzeko harira.
Baina tira, opoak gainditu behar dira:
galderetan galtzen dira izangaien erdia
eta beira, saiatzen diren guztiak
amankomunean dute gainezka duten hira.

Zure pose traketsak sortzen du gure algara
etorri gure atzetik nonahi, hemendik hara.
Antzaz lortu duzu, nahi zenuena zara
baina halere ispiluak ematen dizu ikara
eta amuak, helduta dauka zure ahoa
uniformeak ez du bilakatzen astoa.
Kanpotik haserrea barnetik malkoak
zu zara arrapatu nahi zenuen gaiztoa.


Ig: @suukasuan
E-posta: sukasuantaldea@gmail.com