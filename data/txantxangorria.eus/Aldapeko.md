---
id: tx-1977
izenburua: Aldapeko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bXgJteAgcRA
---

Aldapeko sagarraren
adarraren puntan
puntaren puntan
txoria zegoen kantari.

Xiru-liruli, xiru-liruri,
nork dantzatuko ote du
soinutxo hori?

Zubiburu zelaieko
oihanaren zolan,
zolaren zolan,
lili bat bada beilari.
Xiru-liruli, xiru-liruri,
nork bilduko ote du
lili xarmant hori?

Mende huntan jasan dudan
bihotzeko pena,
penaren pena,
nola behar dut ekarri?
Xiru-liruli, xiru-liruri,
zuk maitea hartzazu ene pena hori!