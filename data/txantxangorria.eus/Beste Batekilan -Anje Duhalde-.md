---
id: tx-3137
izenburua: Beste Batekilan -Anje Duhalde-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BA9MuUyO0pc
---

Amodio ezti eta garbi batez
neure bihotza da zinez berotua
baina zorigaitzez ari naiz, ari naiz negarrez
zu ez zarelako izanen enia.

Eni erran zautan maite ninduela,
nik ere sinetsi enetako zela
baina airatu da, airatu da beste batekilan
eta nik gelditu bakarrik herrian.

Aitama gaixoak supazter xokoan
nekatuak dira ta otoitzez ari,
gure etxaldea errekara, errekarakoan
beharko ginuen bizirik atxiki.

Baina joan zira beste batekilan,
hiri ederrean egonen zarete.
Ni kanpaina zolan, zolan ta kabalekilan,
biziko naiz ilun, uzkur eta herabe.
Ez dakit zer egin ta nola kontzola
arrunt hoztua dut barneko odola
ez bainuen uste joanen, joanen zinela
ni engainaturik etsai bat bezala...