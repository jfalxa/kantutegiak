---
id: tx-2089
izenburua: Balearen Bertsoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SblTNCEnhIw
---

Mila bederatziehun
da lehenengo urtean
maiatzaren hamalau
garren egunian
Orioko herriko
barraren aurrian,
balia agertu zan
beatzik aldian.

Handia ba zan ere
azkarra ibilian
Bueltaka han zebilen
juan da etorrian
ondarra harrotuaz
murgil igarian,
zorriak zeuzkan eta
haiek bota nahian.

Ikusi zutenian
hala zebilela
beriala jun ziran
treñeruen bila;
arpoi ta dinamita
eta soka pila,
aguro ekartzeko
etzan jende hila.

Bost treñero juan ziran
patroi banarekin,
mutil bizkor bikainak
guztiz onarekin:
Manuel Olaizola
eta Loidirekin.
Uranga, Atxaga ta
Manterolarekin.

Baliak egindako
salto ta marruak
ziran izugarri
ta ikaratzekuak;
atzera egin gabe
hango arriskuak,
arpoiakin hil zuten,
han ziran hangoak.

Bost txalupa jiran da
erdian balia,
gizonak egin zuten
bai nahiko pelia;
ikusi zutenian
hil edo itoa,
legorretikan ba zan
biba ta txaloa.

Hamabi metro luze,
gerria hamar lodi,
buztan pala lau zabal
albuetan pala bi,
ezpainetan bizarrak
beste hilera bi
orrazian bezala
hain zeuzkan ederki.

Gauza ikusgarritzat
egun batzuetan,
herriaren ondoan
arranplan egon zan,
urrutitikan ere
jendea etorri zan,
mila pezetaraino
dirua bildu zan.

Gorputzez zan mila ta
berrehun arrua.
Beste berrehun mingain
ta tripa barruak,
gutxi janez etzegon
batere galdua,
tiñako sei pezetan
izan zan saldua.

Ehun ta hogei duroz
balean bizarrak,
agudo saldu ziren
bere egaltzarrak,
garbitu eta gordeak
dauzke hezurtzarrak
kontu handiekin
lodi ta medarrak.

Gertatua jarri det
egiaren alde,
hau horrela ez bada
jendiari galde;
bihotzez posturikan
atsegintsu gaude,
gora oriotarrak
esan bildur gabe.