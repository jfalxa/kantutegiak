---
id: tx-1112
izenburua: Zaramagako Bozka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dqWDYFEfgXE
---

Denbora ari da pasatzen
ta denok hura ahazten
Halere ere egonez gero
entzuten da oraindik
Gasteizen izan zen hotsa.
Entzun zak, nahi baduk
entzun zak
euren anaien ahotsak
Galde zan, nahi badun,
galde zan
Zaramagako bozaz
Ga/teizen izan zen hotsa.
Odolak eskutatzen du
larrosa-arantza zorrotza
Gasteizko martxoan hirua
eguzki argi gardena
Gezur eta omenaldiak
oker lezake egia
EGIA
E/tzun zak, nahi baduk
entzun zak
euren anaien ahotsak
Galde zan, nahi badun,
galde zan
Zaramagako bozaz
Gasteizen izan zen hotsa.
Odolak eskutatzen du
larrosa-arantza zorrotza
Gasteizko martxoan hirua
eguzki argi gardena
Gezur eta omenaldiak
oker lezake egia
EGIA

En/tzun zak, na/hi ba/duk
en/tzun zak
e/u/ren a/na/ien a/ho/tsak
Gal/de zan, na/hi ba/dun,
gal/de zan
Za/ra/ma/ga/ko bo/zaz
Gas/te/i/zen i/zan zen ho/tsa.
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A
En/tzun zak, na/hi ba/duk
en/tzun zak
e/u/ren a/na/ien a/ho/tsak
Gal/de zan, na/hi ba/dun,
gal/de zan
Za/ra/ma/ga/ko bo/zaz
Gas/te/i/zen i/zan zen ho/tsa.
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A
En/tzun zak, na/hi ba/duk
en/tzun zak
e/u/ren a/na/ien a/ho/tsak
Gal/de zan, na/hi ba/dun,
gal/de zan
Za/ra/ma/ga/ko bo/zaz
Gas/te/i/zen i/zan zen ho/tsa.
O/do/lak es/ku/ta/tzen du
la/rro/sa-/a/ran/tza zo/rro/tza
Gas/te/iz/ko mar/txo/an hi/ru/a
e/guz/ki ar/gi gar/de/na
Ge/zur e/ta o/me/nal/di/ak
o/ker le/za/ke e/gi/a
E/GI/A