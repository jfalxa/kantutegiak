---
id: tx-1464
izenburua: Hego Beltz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0QdZUDKx6Ck
---

Horma zaharra erori da, 
agertu da hego beltz,
ez ikutu, ez probatu, 
zaldi zuri hegalari... 
Eh, gazte, ahal duzu eta!!
Lilurazko ametsak 
zapaltzen iraultza,
berriz du zure arnasa 
arantzari egin pasa.
Horma zaharra erori da, 
agertu da hego beltz,
ez ikutu, ez probatu, 
zaldi zuri hegalari... 
Eh, gazte, ahal duzu eta!!
Kanpotik ekarri 
asmo beltz hoien eskutik,
gure herria gogotik 
nahi dute zintzilik.
Horma zaharra erori da, 
agertu da hego beltz,
ez ikutu, ez probatu, 
zaldi zuri hegalari...