---
id: tx-1865
izenburua: Abadiñoko Karmentxu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KutMtzadxQs
---

Abadiñoko Karmentxu 
neska politte zara zu 
neska politte zariela, ai, 
urrindik agiri dozu. 

Agustuen lenengue 
San Pedro Mendexakue 
etxatela, ai!, aztuko, ai! 
orduko iiluntzekue. 

Santa Eufemiko zelaijen 
laprast eindde jeusi nintzen, 
hazurrik eneuen hausi, ai!, 
mamiñe mailletu baizen. 

Neure nobiaren ama, 
asto baten jabe dana, 
astoak ekarriko'tso, ai!, 
etxean bear dauana. 

Goian da bean hurie, 
erdien Errenterie, 
zeuk dirudizu, maitea, ai!, 
larrose jaio barrie. 

Begire nago begire 
goiko kamiño barrire, 
noz etorriko ete dan, ai!, 
neure laztana herrire.