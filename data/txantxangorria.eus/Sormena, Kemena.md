---
id: tx-1295
izenburua: Sormena, Kemena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TUZ2pw2w5Xg
---

2017ko uztailaren 14an Lemoako igerilekuko terrazan grabatutako irudiak, Hogota Produkzioak-ek ekoiztua.


Dena eskura eman zigutenek                             
kontzientzia garbi izango dute                             
baina gu itolarrian.                                                           
Zerua eskeini zigutenek trukean memoria kendu ziguten, 
mesfidati gure ahalmenaz          
Galdutako belaunaldi baten biktima garela esan arren, 
entzungor egiteko beste adore badugu.                                                                  ALDAMENEKO TRENAK IHES EGIN BADU MALKORIK EZ ISURI KONDENA EZEZAGUN SORMENA ETA KEMENA UZTARTZERAKOAN DESIRA HANDIENAK GURE ESKUETAN                                                    Galdutako azken arnasetik arima siku atera ziguten, 
lorpenaren oroigarri                              
Heriotza jaio bezain laster, 
gorpuak pilatzen hasi ziren, 
hilerriak gainezka egingo du  

Burua galtzeko prest, 
arima biluztean, 
ebidentziak azal.
Zentzua galtzeko prest, 
bihotza eskura eman, 
aitormen itun batez            
Sormena, kemena
Sormena, kemena     
Sormena, kemena     
Sormena, kemena                                                     
Burua galtzeko prest, 
zentzua galtzeko prest, 
guztia galtzeko prest