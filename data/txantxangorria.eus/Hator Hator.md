---
id: tx-2849
izenburua: Hator Hator
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/CcGF-bs9RFI
---

Hator, neska-mutil etxera
gaztaina ximelak jatera,
Gabon gaua ospatutzeko
aitaren eta amaren ondoan.
Ikusiko duk aita barrezka
amaren poz ta atseginez.

Eragiok, mutil, aurreko danbolin horri.
Gaztainak erre artean,
gaztainak erre artean,
txipli txapla, pum!

Gabon gaua pozik igaro daigun.