---
id: tx-3161
izenburua: Gaur Ez Zaitut Entzungo -Olaia Aretxabaleta-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WhvKIrUO9n4
---

Ez nituen jarri tartan
Ondiño hamar kandela
Eta gurasoek esan zidaten
egun batean:

Entzun!pena merezi
duen emakumea
Sofistikatua da,
galanta, dotorea

Eta lasaia,
neurtu zure hitzak, Olaia!
Eta lasaia,
ezagutzen zaitugula, Olaia!

Baina nik oztopo guzti horiek
saltatzen ditut
Marra banatzaile horiek
Oraindik ez naute mantsotzen

Ze niretzat
Ez da poz beteko bizitza
Agenda beti beteta izatea
Eta gero afaria jartzea

Gero ulertu dut benetan esan
nahi zutena:
Hobeto ixilik eta manso ibili
Etsi beharko duzu azkenean...

Ama! Gaur ez zaitut entzungo
Egingo dut nahi dudan hori
Beldurra ematen diona denori
Baina horrela eta guzti,
nire bidea jarraituko dut

Aita! Hori ez da nire Mundua
Ez nahiz ez pitxerra ez loreontzia izango
zure baremoak saltatuko ditut,
Ez dakit planta egiten, beraz,
nire bidea jarraituko dut...

Eta horrela, nire bidea
jarraitzen nindoala
Gizón batekin topatu nintzen
Bakarrik begiratzean elektrikarak
sentiarazten ninduena
Eta xuxurla esan zidan:

Zu eta nire artean
Airea baino askoz gehiago dagoela
Puruegia da
Azaltzeko

Baina zu eta nire artean
Ez daude kateak
Ez daude dibisioiak
Lehenengoa edo bigarrena

Zu eta nire artean
Ispilurik ez dagoela
Ez dut ikusi nahi zu zarela
Nire soil isladapena

Zu eta nire artean
Biluzik nagoela
Ez nahiz harrizkoa
Eta ez dut estaltzen

Eta esan nion...

Eta ni nahi zaitut nire alboan
ze zure besoak
ematen didate niri bero
baina ez naiz inoiz sentitu preso