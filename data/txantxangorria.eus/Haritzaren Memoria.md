---
id: tx-1027
izenburua: Haritzaren Memoria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GASHaM_CTDg
---

Oiartzunen atxiloketak izan ziren Aritxulegiko gudarien baratzaren harira. Gertaera hark inspiratutako kantua da honakoa.


Bilauak edo heroiak
esango du historiak.
Epaitegitatik kanpo
behar luke hilobiak.
Espetxeetan, nahiz segadetan,
ustekabeko tiroketetan
eraitsiak, eroriak.
Minak ez al du min ematerik?
Nori egiten dio kalterik
haritzaren memoriak,

Kate motzak, kale hotzak
edo ta kare goriak…
Nork erabakitzen ditu
gorpuen kategoriak?
Nekez galdetzen du hariztiak:
Zer ote du onetik gaizkiak?
Ta zer txarretik ongiak?
Minak ez al du min ematerik?
Nori egiten dio kalterik
haritzaren memoriak?

Mendira eraman zaitu
negar zoriak
gaurkoan ez du kantatu
behar txoriak.
Ardaxka berri bati begira
lur biluzira erori dira
bere negar malko biak.
Nahiz zainak lurretik erauzirik,
jarraituko du beti bizirik
haritzaren memoriak.

Berdez jantzita zetozen
gorri-horiak,
odola pozoindu zien
‘vascofobia’k.
Gorrotoak dauzka itsuegi
isilik dago Aritxulegi
ez dauka gatzik ogiak.
Nahiz zainak lurretik erauzirik,
jarraituko du beti bizirik
haritzaren memoriak.