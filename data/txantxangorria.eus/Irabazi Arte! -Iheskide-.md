---
id: tx-3332
izenburua: Irabazi Arte! -Iheskide-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dPW7L3KMI5o
---

Oztopo denen gainetik
hemen gara berriz
IHESKIDE,
gudarako gogo biziz!

Barearen osteko
korronte harroa
zuen amets gaiztoena
bihurtzera goaz

Kiskaltzen balbulak,
fusilak eskutan,
bozgorailuetatik
borrokarako deiaren oihartzuna.

Etenik ez duen frente musikala,
zapalkuntzaren kontrako
oihuen armada,
rock matxinoaren
gerrilariak gara
isilarazien garrasiaren islada.

Lauhortza gertu
baina asko falta dira
itzalpeko laguna
harro gaude zutaz!

Punkrock neurotoxina
zainetan borborka
sugearen hozkadaz
akabo zuen nerbio sistema,
ustela!

E/te/nik ez du/en fren/te mu/si/ka/la,
za/pal/kun/tza/ren kon/tra/ko
o/ihu/en ar/ma/da,
rock ma/txi/no/a/ren
ge/rri/la/ri/ak  ga/ra
i/si/la/ra/zi/en ga/rra/si/a/ren is/la/da.

Etenik ez duen frente musikala,
zapalkuntzaren kontrako
oihuen armada,
rock matxinoaren
gerrilariak gara
isilarazien garrasiaren islada.

Etenik ez duen frente musikala,
zapalkuntzaren kontrako
oihuen armada,
rock matxinoaren
gerrilariak gara


izango gara
IRABAZI ARTE!