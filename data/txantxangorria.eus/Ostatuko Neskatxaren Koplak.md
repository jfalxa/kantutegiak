---
id: tx-1974
izenburua: Ostatuko Neskatxaren Koplak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BKybyHZecxQ
---

Isildu zaigu eguna
larre ondoan asuna
zure gomuta saminagoa
bihotz nereko kutuna.

Eperra garitzan kanta
ehizeko zakurra zaunka
teilatu gorriari dario
nigar eta odol tanta.

Belar gainean bildotsa
haizeak leun du hotsa
etsai-urratsen zarata hitsa
gorrotoaren ostotsa.

Oilarrak harro kalparra
zezena ez da koldarra
Espaina aldean preso daukate
nire senargai zangarra.

Eder basoan haritza
pagoa lerden bortitza
bazter hauetan zenbait jaunskilok
errez jan ohi du bere hitza.

Ibaia dator uholde
haizea uraren golde
euskal andra gizonen odola
ez da ixuriko debalde.

Sagarrak daude ustelak
mikaztu dira upelak
lehengo aldean orain gara gu
abertzale txit epelak.

Udako sasoiak uzta
larrean eder irusta
nire bularra zik laztan ezik
ximelduriko masusta.

Artoa zaigu zoritu
sagarrondoak gorritu
zu noiz itzuli itxaropena
ez zait gogotik akitu.

Euriak mardul dario
elurrak dirudi liho
nire bihotzak ez dezaizuke
sekula esan adio.

Usoak dira igaro
basoak dirau oparo
nire magal gozoa dukezu
zatozkenean abaro.

Zelaiak badu hesia
ereina dugu hazia
nire erraietan ernatuko da
askazi ezin-hezia.