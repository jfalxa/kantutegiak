---
id: tx-3245
izenburua: Gernika -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/k3pebwr4ZkY
---

Mila ta bederatzirehun eta
hogei ta hamazazpia
euskotarrentzat urte beltz, ilun
triste ta negargarria.
Hauts bihurturik utzi zutena
Gernika zoragarria
euskal erliki maitatuenak
gordetzen zituen herria.

Apirileko ilarekin zen,
hogei eta seigarrenean,
hain juxtu ere peri eguna,
astelehen arratsaldean;
kale-baserri, milaka anai,
gizatasun ederrean,
plaza inguruan aurkitzen ziran
lehengo ohitura zaharrean.

Kanpandorreko ezkilak hasi
ziren dinbili-danbala,
goiko piztien motor burrunba
entzuna zen berehala;
emakume ta ume gaixoak
kurrixka zeriotela
heriotzari ihesi nahirik
nora jo ez zekitela.

Ehunka izan ziren etxe jausien
zakarpetan geldituak.
Nork daki zenbat kale gorrian
zeharo erdibituak?
Infernu honetan uzten gaituzte
laguntza gabe galduak,
o, euskaldunen min oinazerik,
ez du sentitzen munduak.

Puska dituzte gure herri polit,
apain eta maitekorrak,
puska dituzte baserri txuri,
gure kabo on, xamurrak.
Eta zirtzildurikkaletan utzi.
Euskaldunen hezurrak,
Alemaniatik gu txikitzera
etorritako maltzurrak.

Ez da, ez, alferrikakoa izan
hor isuri den odola,
pentsa oraindik emaitza ederrak
ematekotan dagola.
Euskal gogoa armez ta indarrez
ezin hil liteke inola,
Gernika erre ziguten baina
zutiñik dago arbola.