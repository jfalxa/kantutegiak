---
id: tx-70
izenburua: Binbili Bonbolo
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8LWJK8ylWq8
---

1.Binbili, bonbolo, sendal lo
Akerra Frantzian balego
Astoak soindua, idiak dantza,
Auntzak damboria jo.

2.Dambori berri-berria
Donostiako ekarria
Bazterrak ditu perla churiak
Erdian urre gorria"