---
id: tx-3183
izenburua: Kili Kili
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7oTL0L-HnNA
---

Kili-Kili bat, Kili-Kili bi,
millaka ume dantzan,
alfabetatzen, alfabetatzen,
tella bako ikastolan.

Txan, txan, txoritxoa dantzan.
Pil, pil perejilla saltzan.
Bakoitzak bere eraz,
Euskaldunak garelako;
egin daigun, egin daigun
euskeraz.

Ez, ez, zigor eraztunez,
bai, bai, herri maitasunez.
Jo daiugun aurrera,
kate luze bat eginez;
gaur daukagu, gaur daukagu
aukera.

Din, dan, kale ta ikastolan.
Txin, txaun, baita jolasetan.
Euskera indartu,
geure geurea delako.
Lau lurretan, lau lurretan,
zabaldu.