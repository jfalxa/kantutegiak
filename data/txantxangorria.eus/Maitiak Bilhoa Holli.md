---
id: tx-807
izenburua: Maitiak Bilhoa Holli
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/c8PiQdmEW9M
---

Maitiak bilhoa holli
eta kolorea gorri,
esküko larria churi, zilhar fina üdüri,
eta bera charmagarri best' ororen gaineti

“Etchetto bat badizüt nik
jauregi baten parerik;
hartan barnen egonen zira zilhar kaideran jarririk;
ihurk ezpeitizü erranen nahi eztüzün elherik.

Nik badütüt mila ardi
bortian artzañeki;
Kataluñan ehün mando bere zilhar kargeki:
hurak oro badütüketzü, jiten bazira eneki.”

“Badüzia mila ardi
bortian artzañeki?
Kataluñan ehün mando zilhar diharüreki?
Hurak oro ükhenik ere, eniz jinen zureki.”

“Maitenaren etchekoak,
khechü ümen ziradeie:
alhaba ene emaztetako sobera ümen zaizie.
Ez emazte, bai amore: sofritü behar düke.

Senhartako emozie
Frantziako errege;
Frantzian ezpada ere, Españakoa bedere:
bada errege, enperadore, phüntsela ja ez tüke.”