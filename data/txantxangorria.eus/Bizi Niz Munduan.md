---
id: tx-1648
izenburua: Bizi Niz Munduan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/I5Af-mcNrCA
---

Bizi niz mundüan
Aspaldi handian
Beti tristezian
Pena bihotzean
Amodiotan nintzan
Izar batekilan
Hura ezin ikus
Plazer dudanean
ez beitüt etxean
Ez eta herrian.

Aida baninandi
Ainhera bezala
Ardüra joan naite
Maitearen gana
Ene pena doloren
Hari erraitera
Bai eta xagrinen
Hari salatzera
Aitaren ixilik
Solas egitera.

Izan niz Barkoxen
Kusi baten etxen
Ahüntz jaki jaten
Segür hona beitzen
Kostaletak errerik
Biriak egosirik
Ogia muflerik
Ardoa gozorik
Han etzen eskasik
Maitea zü baizik.