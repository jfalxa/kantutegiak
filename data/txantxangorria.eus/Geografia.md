---
id: tx-1889
izenburua: Geografia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XjG4qqKJ2Z4
---

Batzu sorlekutik hurrin hiltzen dira
                          derrigorrez.
Ataut batzu sehasketatik hurruti
                            behar dira jarri.
 
Gu ez gara Igorretan hilgo, hareatzan eroriko gara
                    gerlan jaurtiriko gezia lez.
Selban hobiratuko gaituzte, lehengusu eta lagunen
                                gutunez estalirik.
 
Haizeak gure errautsak eramanen ditu, beste hizkuntzaz
                    mintzo diren ezpainetaraino.
Gure aitaren etxeak zutik iraunen du
                                      gu gabe.
 
 
© Joseba Sarrionandia