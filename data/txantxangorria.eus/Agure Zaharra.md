---
id: tx-1542
izenburua: Agure Zaharra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b2ZArJdiE-Q
---

Agure zahar batek zion bere etxe aurrean
goizean goiz lantokira irteten nintzanean:
Ez al dek, gazte, ikusten gure etxola zein dan?
desegiten ez badugu, bertan galduko gera.

Baina guztiok batera
saiatu hura botatzera,
usteltzen hasita dago-ta,
laister eroriko da.
Hik bultza gogor hortikan,
ta bultza nik hemendikan,
ikusiko dek nola-nola
laister eroriko dan.

Usteltzen ari dela bai
badakigu arraio!
baina Xabier erortzeko
asko falta al zaio
besteen exolak amets 
ez dit bat inportik
geure bizinahiarenak
iraun dezala zutik

Bai, baina gerta liteke
hau bai buru haustea
erortzen ari den horrek
zutitzez ez uztea

Indarra dute arrazoi
bagenekien aurrez
xehetu baietz indarran
arrazoiaren ordez

Bat batera bultzatu eta
bil dezagun indarra
hori da gu ez usteltzeko
dugun bide bakarra.

Arrazoia, indar eta
bizinahia helburu
elkarri eskua emanda
irabaziko dugu.

Baina guztiok batera
saiatu hura botatzera,
usteltzen hasita dago-ta,
laister eroriko da.
Hik bultza gogor hortikan,
ta bultza nik hemendikan,
ikusiko dek nola-nola
laister eroriko dan.