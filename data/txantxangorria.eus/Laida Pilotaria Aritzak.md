---
id: tx-2799
izenburua: Laida Pilotaria Aritzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JVjQf4_OUy4
---

Zilhar finaren pare da pilotaria
Korpitza sano eta zohardi begia
Txerkatzen dü leialki plazan jokatzera
Bere Eskual Herria gora laidatzia
 
                Errepika
Zelüko jinko jauna
Gützaz orhit zite
Pilotarien Biltzarraren
Zü jar zite jabe
Gazte horiek ondotik
Har dezaien pharte
Zer lizateke Eskual Herria
Pilotari gabe
 
                II
Eijer pilotaria agertzen delarik
Aingürü baten gisa xuriz beztitürik
Gogua arin eta korajez betherik
Harek ez dü ordian nihuren beldürrik
 
                III
Perkaiñen denboratik bai eta orai da
Plazetan zunbat ürhats eginik izan da
Zunbat txapeldün jelki, heben hanitx bada,
Nihuntik ez leiteke, Pilota galtzera
 
                        Lehentze khantatürik Baigorri-ko lehen «Pilotarien Biltzarra»-n.