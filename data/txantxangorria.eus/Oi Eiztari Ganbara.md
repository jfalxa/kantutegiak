---
id: tx-3088
izenburua: Oi Eiztari Ganbara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0bPu9zDI4m0
---

OI, EIZTARI!

Zugatzean,
goi-goi, txindorra kantuz;
bean, adi,
eiztari bat iskulluz.

Aizez kea
Txindorra da lurrera;
luma gorri, 
intziriz doa iltera.

Zugatz-puntan
ixil, ezta kanturik.
Txori leuna
zelaian dago il-otzik.

Eiztari txar:
or muna egalean
nir maitea
kanta-ots samurrean.

Ez, otoi, ez
beraz geziz eratsi;
laztana dot
neure erraiaren antsi.

Argi-miña,
maiteprintzaz jantzia.
Zeru-ago
dauan izate erdia.

Iskilluz
ez su-egin, eiztari!
Bide ertzean
dot maitea kantari.