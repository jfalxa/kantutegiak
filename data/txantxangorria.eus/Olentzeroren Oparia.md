---
id: tx-2299
izenburua: Olentzeroren Oparia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ED0W9v3Ku1Y
---

Astoa gaixo dago.
Olentzero tristerik.
Txabola ikatzez beterik,
pilan-pilan udazkenetik.
Ez dauka txabola uzterik,
ez etxez etxe ibiltzerik.

Horra, Olentzero
pipa hortzetan.
Gauez ibiltzen da
negu hotzetan.

Larri daude auzoan.
Ikatzik ez neguan.
Han egongo da Olentzero,
suaren ondoan txit bero.
"Noiz jaitsiko da txabolatik?
"
galdetzen du Josepantonik.

Horra, Olentzero
pipa hortzetan.
Gauez ibiltzen da
negu hotzetan.

Bi lagun joan dira
elurretan mendira.
"Aizu, Olentzero,
zergatik
ez duzu ekartzen ikatzik?"
"Ezin dut ikatzik banatu,
mesedez, astoa sendatu".

Horra, Olentzero
pipa hortzetan.
Gauez ibiltzen da
negu hotzetan.
Arrantzaka astoa
sendatu da gaixoa.
Eskerrik asko amonari
eta bere kataplasmari.
Geroztik gure Olentzero
herrira jaisten da urtero.

Horra, Olentzero
pipa hortzetan.
Gauez ibiltzen da
negu hotzetan.