---
id: tx-72
izenburua: Goiko Mendian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/msf4PK1uQes
---

Goiko mendian elurra dago
errekaldian izotza (bis)
zugandik aske nago ta
pozik daukat bihotza
uda hastean eguzkitara
urtzen denean elurra
zuk ere sendiko duzu
urrikiaren uztara