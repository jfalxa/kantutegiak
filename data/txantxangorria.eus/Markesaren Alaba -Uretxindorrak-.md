---
id: tx-3203
izenburua: Markesaren Alaba -Uretxindorrak-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LJf_u186_LI
---

Zeruak eta lurrak
egin zituanak
memoria argitzen 
atoz niregana.
Esperantza badaukat
zerorrek emana,
kunplituko dedala
desio dedana.

Markesaren alaba 
interesadue,
marineruarekin
enamoradue.
Deskubritu bagerik
beren sekretue,
amodiua zeukan
barrena sartue.

Ala disponiturik
jarri ziran biak,
kartaz entenitzeko
alkarren berriak,
formalidadiakin
jarteko egiak,
baina etziran lo egon
amaren begiak.

Amak esaten deutso:
-Juanita neuria,
galdu da dinuenez
Antonio Maria.
Nik bilatuko deutsut
beste bat obia,
maiorazko interes
askoren jaubia.

Au da lendabiziko
esan genduena
zer da musikarekin
onratzen duena?
Markesaren alaba
kalian barrena,
esposario zala
a biar zuena.

Penarikan lertute
Antonio il zan,
akonpainatu zuen
Juanitak elizan.
Maitasuna bazion
esandako gisan,
etzuan geroztikan
osasunik izan.