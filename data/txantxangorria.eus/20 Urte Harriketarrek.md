---
id: tx-2326
izenburua: 20 Urte Harriketarrek
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wZ5zcPIdpkE
---

Lemoako KARABIE Gaztetxeak 20 urte betetzen ditu.

ZEINKE taldeak jarri dio doinua urteurrenari: 

"20 urte Harriketarrek" kantua 2019ko Irailean grabatu eta nahastu da Igorren (Bizkaia, EH), Endika Parraren gidaritzapean. Masterizazioa "The Room" (Bartzelona, Herrialde Katalanak) estudioan egin da.

HITZAK:

Bizitzako instante onenak 
inbertitu ditugu hemen
utopikotzat geneukan hori 
irauli eta berpizteko, hemen gara!

Karabi zaharreko pasioa 
belaunaldiz belaunaldi
transmititzeko lanari esker
heldu gara izatera gaur garena

20 URTE BORROKAN, 20 URTE IKASTEN
HAMARKADA BI KOLPEAK IRRIBARREZ JASOTZEN
UNEAK ERAIKITZEN, BIZIMOLDEAK ALDATZEN
DANTZA GABEKO IRAULTZETAN EZ DUGU PARTE HARTZEN

20 urte Harriketarrek, 20 urte Harriketarrek

XX. mendearen mugan
hasitako bidaia honen
bizipenak distantziaz begiratu
bihotzak buelta eman arte!

20 URTE BORROKAN, 20 URTE IKASTEN
HAMARKADA BI KOLPEAK IRRIBARREZ JASOTZEN
UNEAK ERAIKITZEN, BIZIMOLDEAK ALDATZEN
DANTZA GABEKO IRAULTZETAN EZ DUGULA SINESTEN

Babestuak sentitu gara hemen,
maitatuak sentitzen gara hemen.