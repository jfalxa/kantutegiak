---
id: tx-2460
izenburua: Laboraria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/v80s9VLccyY
---

Laboraria alferretan haiz nekatzen
maiz herioa duk betbetan zuganatzen
hazitan egiteko lanak tuk hiretzat
eta fruitu heldu direnak premuentzat.

Bat ez zaio herioari itzuriko
zorrozki zaio bakoitzari jazarriko
badaramatza erregek tronatarik
hain errexki nola pobreak zokotarik