---
id: tx-1576
izenburua: Askatasun Egarriz Lin Ton Taun
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/LW2m09idFEw
---

Herria, jarri hadi zutik
etsaia, dabil eta hortik
arrotza, daukagu gainetik
geure etxean.

Dena onartutzen baduk
beraien aldera hoa,
heure kezkak zabalduz
ireki zak ahoa.

Herria, jarri hadi zutik
etsaia, dabil eta hortik
arrotza, daukagu gainetik
geure etxean.

Euskerarik gabeko euskadi,
baina zer da hori?
gau eta egun etengabean
euskeraz bizi!!

Muga guztiak apurtu
geure lurra geure eskuetan hartu
aberri aske bat sortu
euskaldun guztien helburu. (3 aldiz)