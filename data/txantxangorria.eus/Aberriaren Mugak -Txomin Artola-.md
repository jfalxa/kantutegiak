---
id: tx-3276
izenburua: Aberriaren Mugak -Txomin Artola-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sP34ia3ENW0
---

Nire aberria lurra bezain zaharra da 
baina ez da lurra bakarrik 
haizeak bezala inguratzen nau eta 
lotzen, askatzen nauelarik.

Nire aberria suaren antzekoa da 
beti bera, berria beti 
epeltzen nau kanpoan hotz dagoenean 
ta erretzen ere badaki.

Aberriaren mugak, euskararenak 
gure baitan daude gehienak 
euskara zabalduaz lau haizeetara 
zabalago egiten gara.

Nire aberria harrizko herria da 
hitzezkoa eta hitzekoa 
altzairuzko txakolin, ardo zurezkoa 
eta hondar anonimoa.

Nire aberria burdin bihurria da 
oroimenean arindua 
itsaslaminen baten herdoilezko kantu 
etorkizunez fosildua.