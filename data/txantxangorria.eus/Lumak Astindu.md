---
id: tx-2998
izenburua: Lumak Astindu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OqXMTtZTpck
---

Lumak astindu eta 

kantari ta bizkor, 

aspaldiko txoria 

berriz hegan dator. 

Betiko ikastolan 

neska ta mutiko 

euskaldunak gara gu

orain ta betiko. 

Ai oi ai, orain ta 

betiko orain ta betiko 

ai oi ai, euskaldunak 

gara gu orain ta 

betiko!

Euskal Herria dadin 

herri euskalduna 

ikastolan oraina 

dugu etorkizuna 

Euskal nortasuna ta 

kultura helburu 

ezinak guk eginak 

bihurtzen ditugu.

Ai oi ai 

bihurtzen ditugu, 

bihurtzen ditugu 

ai oi ai, ezinak guk 

eginak bihurtzen 

ditugu.

Euskaraz kanta,

jolas, 

hazi ta ikasi

guztiok gura dugu

beti euskaraz bizi.

Ai oi ai,

 beti euskaraz, 

beti euskaraz bizi

ai oi ai,

guztiok gura dugu 

beti euskaraz bizi 

ai oi ai,

bihurtzen ditugu, 

bihurtzen ditugu 

ai oi ai,

ezinak guk eginak 

bihurtzen ditugu

ai oi ai,

 orain ta betiko 

orain ta betiko

ai oi ai, 

euskaldunak gara 

gu 

orain ta betiko

Leioako 

Betiko Ikastolak 

50 urte!