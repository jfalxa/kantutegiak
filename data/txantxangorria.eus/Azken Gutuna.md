---
id: tx-613
izenburua: Azken Gutuna
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4o_ZFSzc1eI
---

Bidai luzea egina dugu
Itzulerarik ez duena
Hitzak zeldetan hegaka doaz
Mundua bihotz den mugara
Mugara
Zugana
Zugana
Egun berri bat luzatu digu
Itxaropenaren ateak
Giltza buruan
Bihotza patrikan
Laster haizearen altzoan
Altzoan
Mugara
Zugana
Zugana
Ez da hormarik graffitirik barik
Barrote estuen alboan
Marraztitzazu irri gozoak
Ongi etorri hoien ondoan
Altzoan
Mugara
Mugara