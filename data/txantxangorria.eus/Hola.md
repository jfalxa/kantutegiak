---
id: tx-223
izenburua: Hola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iztCxOaZ1rU
---

Hola tio, aspaldiko!

Ondo?

Andrea zelan?

Eta lanean?

Galdetzen badidazu, erantzungo dizut.

Nondik hasi ez dakit

benetan  diotsut

ni nago izorratuta

bizkar hezurra jota

ikusmena galtzerik eta prostata abisatzen

Eta emaztea?

Hobeto, ematen du

bada urtebete beste batekin joan zela

atzo ikusi nuen guztiz apainduta

Hemen paseatzen, langabezian nago eta

Bestela ondo…

hasta el puto fondo
Por cierto
Zeu, itxura ona daukazu
On/do, ez?
Zu zeuk esan dezu, dena itxura da
kanpotik badirudi, barrutik ez hala
familia ondo dijoa, ez da arazoa
baina zerbait falta zait, hori da arazoa
enpresa aurrera doa, ta ezin naiz kejatu
bestea, buruko minak soinetikan kendu
baina zerbait falta zait, hori da gakoa
kolmata, hartu ta erre. Han da soluzioa

Mesedez! Utzi txiskeroa, probatzera noa
zuk_aldatzen badidazu, erantzungo dizut
nondik hasi ez dakit, benetan diozut
zuk zer esan dezu, dena itxura da
kanpotik badirudi, barrutik ez hala

Galde/tu!
Eta erantzungo di/zut

ia ona, ia ona da

Ondo, ez?
Nahiko nuke
beti dago zerbait
edo zenbait konta da