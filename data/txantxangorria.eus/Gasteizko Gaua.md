---
id: tx-1469
izenburua: Gasteizko Gaua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3emd-Il5Fdg
---

Iluntasunaren oihuak
Kalean dira orain odola
Ta gauaren erdian
Su madarikatuak
Dirdiratzen dira
Denen aurka
Ta gauaren erdian
Ezin ixildu erazi
Herri ahotsak

Gasteizko gaua
Gau ilun hontan
Etorkizuna
Heltzen ari da

Gau ondoren heldu zen goiza
Kaleak gorriz tindatuak
Ta betiko gezurrak
Esaten dituztenek
Hitz egiten zuten justiziaz
Ta betiko gezurrak
Ezin ixildu erazi
Herri ahotsak