---
id: tx-3070
izenburua: Internazionala Ii Kloratita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dfIZRE1vt1Q
---

Zutik lurrean kondenatu
zaren langile tristea,
nekez ginen elkarganatu
indazu albiristea.

Gertatuak ez du ardura,
jende esklabua jeiki,
aldaketak datoz mundura,
nor den herriak badaki.

Oro gudura ala! Bihar izan dadin
Internazionala pertsonaren* adin.
Oro gudura ala! Bihar izan dadin
Internazionala pertsonaren adin