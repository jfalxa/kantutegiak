---
id: tx-3318
izenburua: Kanta Dezagun -Eltzegor-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/_9yLAePNfOo
---

Eltzegor taldearen kantu bat, beraien lehen diskotik, "Ebihotz". 1981. urtean ekoiztu zuten, IZ estudioetan, Anje Katarain teknikoaren laguntzaz.

Juantxo Garzia, ahotsa
Joxemari Ostolaza "Xalbardin", ahotsa
Fernando Unsain, ahotsa
Marco Jossie, ahotsa
Jakes Etxeberria, musika
Benito Zubeldia, musika
Juan Mari Beltran, dultzainak ta klarinetea
J. Markos Juaristi, baxua
Iñaki Peña, akordeoia
Nelly Agirre, biolontxeloa
J.M. Txilibeia, klarinetea

ELTZEGOR musika taldea 1976. urtean sortu zen, Joxemari Ostolaza "Xalbardin" jauna eta ALEGERA taldean aritzen ziren hiru kantarien eskutik (Juantxo Garzia, Jakes Etxeberria eta Marco Jossie). Geroago, Fernando Unsain eta Benito Zubeldia batu zitzaizkien. Euskal Herriko egoera politiko sozialari lotutako abestiak sortu zituzten abertzale ikuspegi batetik. Diskoak grabatzea ez zen taldearen lehentasun nagusia, adiskidetasunak batzen bait zituen taldekideak, elkarrekin kantatzeko plazerraren bila. 1984. urtean, "Ebihotz" lehen diskoa grabatu zuten. Joseba Sarrionandia, Joxemi Zumalabe, Xabardin edota Balere Bakaikoa jaunaren hitzekin. Disko hura harrera ona izan zuen zaletuen artean, baina arazoak ere ekarri zizkion kantuen hitzak zirela ta, zenbait debeku ezagutuz.

Geroago, Jakes Etxeberriak eta Benito Zubeldiak taldea utzi zuten, eta Pantxoa Albizur jauna sartu. 1987. urtean "Sasitik sasira" disko ekoiztu zuten. Kantata moduko bat zen, Fernando Unsainek sorturiko musikaz eta Xalbardin hitzez. «Fernandok kantataren eskema bat grabatu zuen, errefuxiatu bat bere etxea utzita beste leku batera doa, Euskadi den baina beretzako hain Euskadi ez den leku batera. Han aurkitzen da sistema ezberdin batekin, historia eta balore ezberdinekin». 1992. urtean, "Herrialde berdean" diskoa kaleratu zuten, Iharra eta Xenki burkideek, 1971. urtean idatzitako ipuin batean oinarriturik. Atzerrian kantatzera maiz atera ziren Eltzegor taldekoak. Birritan aritu zen Antwerpen hirian (Flandria), baita Herbehereetan, Frantzian, Suitzan, Espainian, Katalunian... Zuzenekoetan, Mikel Artiedak edota Angel Unzuk lagundu zuten Eltzegor taldea.


Denok batera kanta nahiean,
denok kanta nahi batera...
Kanta dezagun libertatea,
kanta libertatea!!