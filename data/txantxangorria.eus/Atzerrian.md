---
id: tx-539
izenburua: Atzerrian
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UC0ukhthhUI
---

Aste pare bat pasata herritik aldatu behar
Guztiak atzerrian
Bidaia dohainik ezta
Non zaude? Mendian
Gutziak Atzerrian

Nire ahoa ta lagunen belarriak
Guztiak atzerrian
Atzerritarra ni izango beharko naiz
Guztiak atzerrian

Motxila bat ta maleta terdi
Beti gabiltza berdin
Nahiko tabako eta mukizapitan
Kopetako izerdi

Ez banago adi arazoak izango dira argi
Ez da gauza berririk
Nola hitz egingo dut zurekin zure hizkuntza ez badakit?
Hobe nago ixilik

No se ke sera y no se ke dira el que dira
Se ke van a mirar pero no pa cubrirme las espaldas
El medico no me da el alta pero sigo andando con mi Altus
Aberria sorbaldan

Mala relacion entre la droga y mi cuerpo
Entre lo que siento y lo ke me hace falta
El dia de la marmota asi me voy acostumbrando a lo ke va venir
Sin ponerme una bolsa en la cara

Aste pare bat pasata herritik aldatu behar
Guztiak atzerrian
Bidaia dohainik ezta
Non zaude? Mendian
Gutziak Atzerrian

Guztiak atzerrian
Guztiak atzerrian

Han zen sortu
Han handitu
Han ari zen maitatzen

Han bere umeen artean
Goxoki du kantatzen