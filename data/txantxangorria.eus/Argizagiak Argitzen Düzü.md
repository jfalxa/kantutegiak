---
id: tx-953
izenburua: Argizagiak Argitzen Düzü
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/l4aDwoKvurU
---

Argizagiak argitzen dizü
Argizagiak argitzen dizü gaüaz zohardi denean
Ene maitea ikusi nizün dantzan plazaren erdian
Izar batek bezala argitzen zizün beste ororen artean.

Norat nahi joan nadin ene bihotza zurekin
Deskantsürik batere ez düt ene gazte lagünekin
Non ez zütüdan aitzinean zure begi eijerrekin.

Ilea xuri begiak beltx nik zü maite zük ni ez
Nik zü maite zük ni ez ene maitea zeren ez
Kanbia zite deseinez eta ni maita bihotzez.