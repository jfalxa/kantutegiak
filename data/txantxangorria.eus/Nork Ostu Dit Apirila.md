---
id: tx-759
izenburua: Nork Ostu Dit Apirila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rFAKyvjxDYI
---

Imanol G.Gurrutxagak egin du bideoaren muntaia (IGG Ekoizpenak)

Nork ostu dit apirila? proiektuari izena ematen dion kanta duzue hau. Sortzaile talde batek, "elkartu" eta itxialdian oinarritutako kantak sortzeari ekin diogu.

Lehena, bertsio bat izango da; baina hortik aurrera letra eta musikak propio sortuak izango dira.

Oraingo hau, Joaquín Sabina - Sabineros- ren kanta mitiko baten bertsioa da. Apirilan eReggae sentituko zaretelakoan...


Giltzarrapo bat dindilizka
paretak hizketan baleki...
Teilatu gorri bakoiza 
liburu ireki.

Festa txikiak balkoietan
ta poliziak leihotan,
kanpora beha beti,
barrura begiratzekotan, ahotan

Nork ostu dit apirila?
Nor izan da lapur ixila?
Nork ostu dit apirila?
Non da denbora hila?
Nora joan haren bila?

Ekonomia babesteko
bihurtu gintuzten zenbaki,
zeinek, nola eta zergatik
bakoitzak daki.

Aspaldiko lagunei deitu,
auzokoari egin on.
Kaiolan txori bati
atea zabaldu genion. Zer dion...


Nork ostu dit apirila?
Nor izan da lapur ixila?
Nork ostu dit apirila?
Non da denbora hila?
Nora joan haren bila?

Nork ostu dit apirila?
Jada kantari hasi ote da kukua

Nork ostu dit apirila?
Ixildua, ez mutua