---
id: tx-2284
izenburua: Emakume Langile, Saretzeari Ekin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MPUgOy_ew14
---

Gasteizko Unibertsitateko Emakume Taldea





Bertsio originala: Ojalá - Silvio Rodriguez
Gasteizko kanpusetik deiadar bat entzun da urrunean
Emakume zapalduon herra dugu barnean
Indiako fabriketan, Mediterraneoko pateretan
Etxeko sukaldetan, eskola, putetxetan
Denak loturik gaude kate berdinetan

[Estribilloa]
Zapalduon kateak zailak dira arintzen
Ez badira errotik aztertu ta suntsitzen
Has gaitezen mundu bat guretzat eraikitzen
Eredu burgesean
Eraiki den bizitza
Hautsi nahian gabiltza
Askatasun gosean
Emakume langile
Saretzeari ekin
Ez bada elgarrekin
Ez delako posible

Sistema honen barne irautea ote da bizitzea?
Bizirautea ez al da pixkanaka hiltzea?
Bortitz eraikitako munduan mirarik ez da gertatuko
Gure etsaiak ez gaitu sekula lagunduko
Borrokatuz ez bada, ez da ezer lortuko

[Azken Estribilloa]

Zapalduon kateak zailak dira arintzen
Ez badira errotik aztertu ta suntsitzen
Has gaitezen mundu bat guretzat eraikitzen
Eredu burgesean
Eraiki den bizitza
Hautsi nahian gabiltza
Askatasun gosean
Emakume langile
Saretzeari ekin
Ez bada elkarrekin
Ez delako posible
Horregatik ondoan nahi zaitugu burkide
Has gaitezen mundu bat guretzat eraikitzen
Eredu burgesean
Eraiki den bizitza
Hautsi nahian gabiltza
Askatasun gosean
Emakume langile
Saretzeari ekin
Ez bada elkarrekin
Ez delako posible