---
id: tx-2536
izenburua: Xalbadorren Heriotzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rXsTOISMrT8
---

Adiskide bat bazen
orotan bihotz-bera,
poesiaren hegoek


sentimentuzko bertsoek
antzaldatzen zutena.

Plazetako kantari
bakardadez josia,
hitzen lihoa iruten
Bere barnean irauten
oinazez ikasia...
ikasia.

Nun hago, zer larretan
Urepeleko artzaina,
Mendi hegaletan gora
oroitzapen den gerora
ihesetan joan hintzana.

Hesia urraturik
libratu huen kanta,
lotura guztietatik
gorputzaren mugetatik
aske senditu nahirik.

Azken hatsa huela
bertsorik sakonena,
inoiz esan ezin diren
estalitako egien
oihurik bortitzena...
bortitzena.

Nun hago, zer larretan
Urepeleko artzaina,
Mendi hegaletan gora
oroitzapen den gerora
ihesetan joan hintzana.