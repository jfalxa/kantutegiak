---
id: tx-2869
izenburua: Baigorriri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/SpeSqGJmgAI
---

Baigorriko herriak hegalak urrun'tu
 Denak ere malkarra, erroitz eta bortu
 Bainan zer inporta du? Ni han nintzan sortu
 Eta hantxet nahi dut engoitik zihartu.
 
 2
 
 Germieta, Belexik, ikusirik Larla
 Diote: «Guk Oilandoi dugu gure perla»
 Arrano begiz dago denen zain Iparla
 Hunen gerizan ditut borda ta etxola.
 
 3
 
 Batzu asetzert dira diru atseginez
 Besteak kontsolatzen, udan hunat jinez
 Nik aldiz hoberikan badut hemen zinez
 Mendi hauk maite ditut amodio-minez.
 
 4
 
 Erratzu, Aldudetik, Banka, Baigorrira
 Urepel, Arrosera, Ortzaiz, Bidarraira
 Batzu mila dudetan egoiten baitira
 Nik dautzut, Euskal Nafar lur bizian zira.
 
 5
 
 Etxea, nun ditutzu zure heur guziak?
 Ohantzetik ereman ainitzak biziak
 Itzulia egiten baitu iguzkiak
 Bihar hel ditzotela gure goraintziak.
 
 6
 
 Token geldituak etzazket ahantzi
 Bihotzera doala otoitz hau ainitzi
 Gure mendi maiteak hiltzerat ez utzi
 Pobre begins ere: berme eta bizi
 
 7
 
 Herria, zutaz daukat pentsamendu ainitz
 Laster erranak dira, aski da zonbait hitz
 Jarraik arbasoeri leial eta bortitz
 Zer laiteke Baigorri... Euskaldun ez balitz!