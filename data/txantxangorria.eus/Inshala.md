---
id: tx-2107
izenburua: Inshala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2fLvysAxetk
---

Egunez itzalak
lotan daude
gauez haizearekin
dantza egiten dute
beraien itzaletan
murgiltzen nintzen,
barruan neramana isladatuz

Haizearen astinduetan askatuz
gorputz libre bat bilakatuz.

Kriseilu baten argi itzalpean
gorputzen itzalekin jolasean
gorputz libreen dantza leunetan
mugarik gabeko mugimenduetan.

danbor soinuek
eraman ninduten
hondar goxo haietan galtzera
haizearen leunkadak sentitzera,
barneratzean aldatzera.

Haizearen astinduetan askatuz
gorputz libre bat bilakatuz.