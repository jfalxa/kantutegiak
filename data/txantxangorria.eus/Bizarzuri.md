---
id: tx-2762
izenburua: Bizarzuri
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NCCQX_LzuOE
---

Gabonak, Gabonak, hau zoriona,
bertsotan ari zaigu gaur aitona,
ja, ja, ja barrezka dago amona.

Bizar zuri, bizar zuri,
mendi gainak, zuri, zuri,
zorionak, zorionak,
bertan ditugu Gabonak.

Tximini zulotik norbait sartu da
mutur beltz, begi gorri, aibalio,
Olentzero dela, halaxe dio.

Bizar zuri ...

Laster urte berri, gaur Olentzero,
alferrontzi, mozkor eta tripero,
hor dago sutondoan bero, bero.