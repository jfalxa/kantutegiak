---
id: tx-2400
izenburua: Emon Bostekoa!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QNRoOPdaZFg
---

Herriak sortu / Herriari
emoteko / Lur bat bere berbari
Teilatu bat izarrei / Izena
emoteko ta / Amesteko
itzarrik (bis)
Beharrak batu / munduari
emoteko / bagarenaren barri
Izango garenarentzat / Ate bat
margotu eta / Beti euki zabalik
/ Ibilaldian / Munduko
de etxekoa / Ibilaldian /
Emon bostekoa (bis)
Ondo etorri! / Eraikitzen
garbizen etxera, / Gure leihotik
/ Geroa ikusten da
Geroa sortu / Atzokotik
ikasteko / Bihar, barriz, zar
egin Ohe bat ilargian /
Urrinetik ikusteko /
Ez gagozela bakarrik
Oraina batu /
Danengandik ikasteko /
Zergatik
garen herri
Salda urdina gosaldu /
Eta goraintziak emon /
Anbotoko Damari…
Ibilaldian /
Emon bostekoa
Ondo etorri! / Eraikitzen
garbizen etxera, /
Gure leihotik /
Geroa ikusten da (bis)
Ibilaldian / Mundukoa
da etxekoa / Ibilaldian /
Emon bostekoa
Ondo etorri! / Eraikitzen
gabizen etxera, /
Gure ikastolatik /
Geroa ikusten da (bis)