---
id: tx-3335
izenburua: Sugarra Zugan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UnJzSBSXcS0
---

Zein, zein, zein 
Zein da zure helburua? 

Ei zu! oihuka nazazu 
zarata ta kolorea. 
aizu! Bidean ez galdu. 
Ez zaitez konformatu. 
Iragana etorkizunaren aurretik doa 
Zeurea da! Geurea da! 
Herrien deiadarra! 

Zein, zein, zein 
Zein da zure helburua? 
Sugarra zugan 
Zein, zein 
Zein da zure helburua? 

Ei zu! fereka nazazu 
osasuna ta sexua 
Gazte! ukabila altsatu 
alternatiba indartu 
Beldurra ta aitzakia segurtasuna bila 
Zeure indarra, 
geure indarra! 
herrien deiadarra! 

Zein, zein, zein 
Zein da zure helburua? 
Sugarra zugan 
Zein, zein 
Zein da zure helburua? 

Gaur ametsa zeurea egin dezakezu 
Oihua malkoen ondoren isaldatuuu (x4) 

Zein, zein, zein 
Zein da zure helburua? 
Sugarra zugan 
Zein, zein 
Zein da zure helburua? (x4) 

Zein, zein, zein 
Zein da zure helburua?