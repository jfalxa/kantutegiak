---
id: tx-2248
izenburua: Udatzen Laztan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7lo6l9sq1s4
---

Musika: Imanol Urkizu
Hitzak: Patziku Perurena




Udatzen laztan liluragarri

eder zara ta dotore

ezpain gorrizta, begiak urdin

bekain berde edo more

eskuak leun, gorputza lirain

larrua gaztain kolore

haizearen eskola zaharrean

tornuz egindako lore.

 

Uda sargori, negua ilun

arrotz izanik hauzoak

haize gainean zoaz oinutsik

hostoak dira pausoak

poza lumatzen jostatzen dira

zure animan usoak

amodioa hegan zurtzituz

izuak eta lausoak.

 

Sagar gorriak aho eztian

mingain berdean mahatsa

umo usaien zinta horizta

gerri estuan ardatza

larru gorritan gorputza eta

haize zuritan adatsa

izar ixilek zu laztantzeko

erazten dute arratsa.

Gau beranduan amoranteak 
liluraz zuri begira 
bihotza dena odolustuaz 
ilargiaren erdira 
sentimenduak sutan dijoaz 
desioaren mendira 
zentzu galduak infinitoan 
zoramen uholde dira

 

Musika: Imanol Urkizu

Hitzak: Patziku Perurena