---
id: tx-1452
izenburua: Kopla Bikiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4W-OIwldj-E
---

Kopla bikiak 
                  aurten jarriak,
                  pontu txikian, 
                  aire luzian, 
atso-agure buruzuriak, 
iduri esposatu berriak, 
                  farragarriak! 
Deia sepulturan hanka biak, 
halere nahasten bazter guziak! 
 
                  Atso markexa, 
                  zeloz bete-ta, 
                  senar gaizoari 
                  beti erresta. 
Batak flakotua du errementa, 
bestiak idortua buketa, 
                  Andre Bixenta, 
sartuz geroz gutxitzen da fuerza, 
itz gutxirekin konpreni beza. 
 
                  Fuerte atsoak 
                  arrazoia du,
                  modu txarretan 
                  sonatzen badu. 
Etxeko esnea kanporat saldu, 
fruituren bat edo bertze galdu, 
                  zer egin biar du? 
Batek sobera besteak bear du.
Aspaldixean bai malpeka du.
 
                  Oi pikarua! 
                  Inbusterua! 
Inganatu nahi andre gaxua! 
Bilatua duk ire portua, 
amarratzeko ire barkua, 
                  zer pilotua! 
Luziferrekin aiz eskolatua, 
bizi aizela kondenatua. 
 
                  Ago pixka bat, 
                  jaiki nadian,
                  zer nahi den ola 
                  inbusterian? 
Jakingo dire Andalucian, 
Aragoan eta Catalonian, 
                  Bai-ta Frantzian, 
paperak bota mundu guzian, 
hi nola habilan portal-azpian.
 
                        (Jose Mari Ezkerra, XIX. m.)