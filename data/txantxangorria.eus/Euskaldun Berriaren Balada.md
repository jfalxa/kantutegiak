---
id: tx-2005
izenburua: Euskaldun Berriaren Balada
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/C3vRC7nu8MI
---

Barakaldokoa naiz eta
daukat paro obrero
horregatik euskaltegira
noa ni egunero.
Euskaraz bazekien nire
bisabueloak edo
baina ni roillo hontaz
orain arte zero,
goizero, goizero
AEKan bezero,
batzuetan ero,
besteetan bero
baina ez dut milagro
handirik espero.

Goizean goiz hori behar dur
motibazio bila.
Euskara ez da ingelesa
baina ez dago hila,
egun askotan pensatzen dut
hau dela inutila,
Nik ez daukat kulparik
ez banaiz abila,
hau da hau makila,
aditz klase pila,
hitzak beste mila;
animo mutila
esaten dute baina
ez da hain fazila.

Ideia hau nire buruan
ez dakit noiz hasi zen
baina batzutan ez naiz hemen
gehiegi dibertitzen,
lau gauzak izen bat dute eta
gauza batek lau izen,
kokoteraino nago
klaseak aditzen
pegatak ipintzen
ta dirua biltzen ...
nola sartu nintzen
ez dut konprenitzen
lau on bat bilatzeko
ez badu serbitzen.