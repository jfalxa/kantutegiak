---
id: tx-69
izenburua: Jo Ta Ke
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/dCC6C27RjHI
---

Marrau jantzi ezkero, atorra zuriazezin leike 
besterik bana beti euskaraz 
ikuak hartu eta beharbada lustriakaz 
ospatu deigun ondo gure oiturakaz. 

Egilior Jose Mari, Ander ta Benjamin 
Juanito Sacristaua, J ose Gondrarekin 
jo ta ke ibiltzen ziran euskera, euskerarekin, 
goazen danok eurakin eta tostadakin. 

Katukale ta baita gero islakale 
Txantxa Pozu aldetik Artzakosolope 
Askarratxe ta bere landare politxe 
Berastegi, Demiku, Goitiz eta Agarre.