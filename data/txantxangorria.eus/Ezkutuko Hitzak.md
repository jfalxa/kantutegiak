---
id: tx-355
izenburua: Ezkutuko Hitzak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3yEPaTJs1OY
---

Hainbat gauza prometitu 
Ezer ez eskaintzekotan 
Kontu gutxiago eta ekintza gehiago 
Nahi ditugu guk 

Lainoen artean 
Ezkutatzen dituzue 
Zuek esandako 
Gezur guztiak 
Pertsona faltsuez 
Nazkatu gara jada 
Non ote daude 
Besteak?

Beti irribarretsu 
Jendearen aurrean 
Zer ezkutatzen duzue 
Barre faltsu horren atzean
Kontu gutxiago eta ekintza gehiago 
Nahi ditugu guk 

Lainoen artean 
Ezkutatzen dituzue 
Zuek esandako 
Gezur guztiak 
Pertsona faltsuez 
Nazkatu gara jada 
Non ote daude 
Besteak?

Zuen hitzak haizeak eman ditzala 
Gezurrez betetako hitzak 
Erre daitezela zuek sortutako 
Sute horretan 



Ahotsa, pianoa eta gitarra: Deñe 
Produkzioa: Eñaut Gaztañaga