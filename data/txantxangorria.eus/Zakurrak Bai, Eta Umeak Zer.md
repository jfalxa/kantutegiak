---
id: tx-2261
izenburua: Zakurrak Bai, Eta Umeak Zer?
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Gebw-oDkiQI
---

Zakurra ikusi dugu
parkera joaten;
jabea ikusi dugu
zakurra eramaten.

Jabeak, zakurra; zakurrak parkean; ta gu hemen!
Zakurrak bai, ta umeak zer?! Zer?! Zer?! Zer?! Zer?!

Gizona ikusi dugu
ogi bila joaten;
Ogia ikusi dugu
kale-buelta ematen.

Ogiak gizona; gizonak kalean; ta gu hemen!
helduak bai, ta umeak zer?! Zer?! Zer?! Zer?! Zer?!

Andrea ikusi dugu
lanera joaten;
Urkulu ikusi dugu
joateko esaten.

Urkuluk Andrea; andreak lanera; ta gu hemen!
Lanera bai, ta umeak_zer?! Zer?! Zer?! Zer?! Zer?!

Kalera irten nahi dugu
umeok lehenbailehen.
Kalera irten nahi dugu
umeok lehenbailehen.

Kaleak, arnasa, arnasak bizitza, bizitzak poza, pozak osasuna… lehenbailehen!
Haurrak kalera lehenbailehen, le-hen-bai-lehen!