---
id: tx-1111
izenburua: Otaloreen Antzera
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uOrJVVltAZQ
---

Zazpi gara
senide guztiok
Aintzinan
guztiz bat ginenok
Elkar gaitezen
indar egiteko.

Lantegien
kearen altzora
Langileok
Sindikatura
Elkar gaitezen
indar egiteko.

Otaloreen antzera
gure elkarbideak
arantza ederrez betea
agertuko dira