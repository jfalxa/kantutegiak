---
id: tx-1471
izenburua: Gogoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FURCflwBCe4
---

Agur esan zenidan gau ilun horretan
itzalak nire aurka jarri ziren.
Herri zikin hontako bazterretan zehar
beldurrek besarkatu ninduten.

Bakardadearen sustraiak erraietan
etengabe joan ziren zabaltzen.
Lotsa galdu nuen taberna lohi hoietan
oroimenak saihesten.

Beste behin ohiko jarrera etsikorrak
zapuztu ditu nire nahiak.

Geroztik gogoan zaitudan bakoitzean
gogor astintzen ditut kaleak, 
burua galtzen du egunsentia heltzean.


Erruaren bidean ezagutu nuen
bizitzaren aurpegi zitala.
Iraganaren kontrako borroka honetan
damutuentzat errukirik ez da. 

Beste behin iraganaren mamuak
bueltatu dira nire ondora.

Geroztik gogoan zaitudan bakoitzean
gogor astintzen ditut kaleak, 
burua galtzen du egunsentia heltzean.
Koldartzen ez nauen labarraren ertzean
ta nire kanturik ederrenetan
ukatzen dudan arren zutaz oroitzen naiz.