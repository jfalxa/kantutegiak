---
id: tx-2685
izenburua: Nere Ongile Maiteari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cK2K--Jf5Z8
---

Ez dakit nola eskerrak eman.
Ah, nere ongile maiteak!
Nere lurrera behar nau eraman
gaur zure borondateak.
Egin dezute zer obra ona
aberats eta pobreak,
horra kunplitu agintzen duana
Jesukristoren legeak.

Bihotz oneko jaunak ba dira
lista duena edertzen,
zorioneko nere zahartzea
oi! Nola duten ondratzen
Munduan bada zorionikan
andre onak du egiten,
gizonak izar hoietan
zerua degu ikusten.