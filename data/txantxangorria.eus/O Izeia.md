---
id: tx-2552
izenburua: O Izeia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/B8aaKCi51Ic
---

Alemaniako gabon-kanta/Jokin De Pedrok euskaratua


O izeia, o izeia
orri berde kutuna (bi)
Udan jantzi berde alai baina neguan ere bai
O izeia, o izeia (bat)
Urtero Gabon izeiak ekarri une alaiak
O izeia, o izeia... (bat)
Kandelatxoek argiak erakutsi jostailuak
O izeia, o izeia    orri berde kutuna (bi)