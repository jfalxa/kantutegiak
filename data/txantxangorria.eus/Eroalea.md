---
id: tx-263
izenburua: Eroalea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/W9RvrA-0-A4
---

Euskaltzaleen Topaguneak antolatzen duen 
"Udagoieneko Ostera" ekimenaren barnean, "Eroalea" abestiaren moldaketa akustikoa.

2017/11/10  Lasarte aretoa (Igorre)

Bateria: Markel Atutxa
Pianoa: Ane Goienetxe
Gitarra elektrikoa: Zoro
Baxua eta ahotsak: Iñigo Santos
Gitarra akustikoa eta ahotsa: Unai Santos