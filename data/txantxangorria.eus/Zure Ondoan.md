---
id: tx-2319
izenburua: Zure Ondoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JHvvRzmJPiQ
---

Zuen Ondoan abestia "Lurpetik" diskatik aterata
Grabaketa eta edizioa: Beñat Goia eta Unai Ruiz de Infante 
Produkzioa: Naiara Olite
Soinua: Estudios K, Alberto Porresen eskutik 



Asko dira elkarrekin pasa ditugunak
Une batzuk alaiak, besteak ilunak
Baina gorde nahi ditiut momentu kuttunak 
Gure oroitzapenik onenen bildumak
Altxorrik handiena baitira lagunak

Bizitza hegaz doa
Denbora geldituz
Gure une gogokoenak
Ahotsaz laztanduz

Herriz herri
Ahoz aho
Egindakoaz
Harro nago

Zuen ondoan, zorion bila, minutuak altxor dira

Ta inork ezin digu esan
Zein den gure historia
Altxorraren uharte honetan

Borrokak bidean
Batu gintuenetik
Beti taldean sentitu
Nahiz eta bakarrik

Aurrera goaz
Atzera ez begirik
Zer datorren nork daki
Beldurrik ez zuekin

Ta inork ezin digu esan
Zein den gure historia