---
id: tx-1590
izenburua: Laztana Latzen Bideoklipa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ej-Y8EcSz60
---

eztarriko korapil sendo hau 
nahi nuke askatzea 
kantu hau bukatzean 
ezagutu zenuen sumendia etengabe su jauzian 
itzaltzen doa orain... 
aitortzen dut, hasieran jolas soil bat zen, haragiaren gosea, mila mozkorren azkena 
barkaidazu baina oraingo hontan ni naiz babes eske datorrena, fideltasunaren ainguran, aldatu naiz 
zurekin pentsatzeak odola irakiten dit, ezintasuna dardakit 
zu gabe 
Inoiz ez dut sinistu amodioan, 
sexuaren indarrak, estali dizkit begiak... azkenik neri ere iritsi zait sentimendu garaia 
sentitzeko garaia... 
aitortzen dut, hasieran jolas soil bat zen, haragiaren gosea, mila mozkorren azkena 
barkaidazu baina oraingo hontan ni naiz babes eske datorrena, fideltasunaren ainguran, aldatu naiz 
zurekin pentsatzean odola irakiten dit, ezintasuna dardakit 
zu gabe 
burua galtzen hasi naiz, inor ez bezela maite zaitudalako, benetan, 
burua galtzen ari naiz, inor ez bezela maite zaitudalako benetan 
Tomado de AlbumCancionYLetra.com 
burua galtzen hasi naiz, inor ez bezela maite zaitudalako, benetan, 
burua galtzen ari naiz, inor ez bezela maite zaitudalako benetan 
benetako gauza da maite zaitut laztana, lotsagabe ohiuka maite zaitut laztana!!