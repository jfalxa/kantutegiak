---
id: tx-431
izenburua: Askatasunaren Semeei
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YCKRuJJbII4
---

MIm SIm SOL RE
Herri neketsu hontan polborazko lohitara
MIm RE SOL RE
euskal semeak boteaz, trintxeretako nekeaz
MIm SIm SOL RE
gure barne muinak hezituaz, ukatzen zaigun izatearen
 MIm SIm LAm SOL RE
funtsa lurrean errotuaz mendez mende bagoaz.
Gure begi legorretan askatasun egarriz
nahigabea haunditzen, etsipenaren aurreko
itsumenean, izar gidaririk ez.
Ez dator gugana jaikosa Mariren
urrezko ileen dizdirarik, ez eta erreka garbietan
lamiek kantatutako doinu liluragarririk.
Ez oten inoiz beren urrezko orrazeaz
sasi nahasi hau orraztera jetsiko?
Ez oten gaitu Sugaar zerutarraren sumindurak
bere seme Jaun Zuriaren ikuituaz txertatuko?
Hor dire nunbait maite, gure errege-alaben
setazko jantzi zuriak hor nunbair, izan ere
oraindikan usaindu gabeko lore freskoak,
askatasunaren semeen parabisuan.
Amodiozko kontuak zilegi izango zaizkigunean,
Euskal Herriko haurrak berriro ere euskal ipuin xarmantez lotaratuko direnean,
gure arteko begirada lanaikorrak
egingo direnean, gure izatearen ardatza sendoaren bueltan
garaipenezko kantuak entzungo direnean...
Orduan bai maite, gure lurreko belar gozoa
zure eta nere soinean lizun bilakatuaz, amodiozko mozkorrean murgilduta,
zorabiozko jirabiretan, haragi dardartien atsedenean,
askatasunaren semeei dei eginaz... gure munduaren hastapena
hantxen osatuko dut zure sabelean.