---
id: tx-3369
izenburua: Salgai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2b2ShVisYLQ
---

Lana bizitzeko dugula
Barkatu bizitzeko lana
Etxea bizitzeko dago
Barkatu ordaintzeko zama

Jaio lana hiltzea dugula
Bizitzeko den bizitza
Ez mugitu ta isil zaitez
Horrela baituzu hitza

Biziraupena erosten dugu
Bizia saltzen digutenean
Ez al da iritsi behingoz
Eguna guzti hau aldatzeko?

Beldurraren preso dira
Baita ere beharraren preso
Ez al da iritsi behingoz
Eguna guzti hau aldatzeko?