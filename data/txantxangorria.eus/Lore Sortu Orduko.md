---
id: tx-2027
izenburua: Lore Sortu Orduko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GfribUY2xHs
---

Hogeigarren urthera hari naiz hurbiltzen...
Eta nere oinetan tomba dut ikusten
Ni zertarako mundura ethorria nintzen?
Ondikoz ez sortzea neretzat hobe zen.

Sekulan zoriona den jakin gabe,
Hainitz dut nik lurrean izan atsegabe
Lore sorthu orduko histu baten pare,
Biziaren uztera guti naiz herabe.

Mendian ur txirripa, harrien artean,
Auhen eta nigarrez jausten da bidean,
Izan nahi baihuke zelhai ederrean...
Hala nere egunak badoaz lurrean.