---
id: tx-2758
izenburua: Matalaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/TTr8pxRxkEs
---

Dolü gabe, dolü gabe 
hiltzen niz. 
bizia Xiberuarentako emaiten 
baitüt.

Agian, agian, egün batez 
jeikiko dira egiazko Xiberutarrak, 
egiazko eskualdünak, 
tirano arrotzen hiltzeko. (bis)