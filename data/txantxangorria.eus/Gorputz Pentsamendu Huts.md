---
id: tx-2035
izenburua: Gorputz Pentsamendu Huts
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cyrcDQf46s4
---

Kobrezko arteriak
Hezur hustuak
Metalezko zilindroa
Hodi desenkarnatua
Nor nintzen?, zer naizen?
Hodiak bezela...
Gorputz pentsamendu huts
Hodiak hodi, gorputzak gorputz
Hala izan behar du
Gorputz pentsamendu huts