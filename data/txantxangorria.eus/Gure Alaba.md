---
id: tx-337
izenburua: Gure Alaba
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OpbawrpZ9F8
---

Gure Alaba (Jon Bergaretxe/Joxe Azurmendi) 1978.


Etxean argi gutxi dugulako
dabil gure alaba
izarretan
kontaduria eramaten,
senarrik izan gabe
mila gizonen andre.
 
(Gure sukaldeko kea
tximini ahora orduko
galdu da.
Fabriketakoa ez).
 
Bainan goizero
itzaltzen dira izarrak,
eta gure alabak
irabazirik ezin aurreratu.
 
(Fabriketako adarra
urrun entzuten da.
Pobreen negarra ez).
Gure etxe etxabolan
argi gehiegi da
alabaren pekatua
ikusteko.
 
Ta argi gutxi.
Nik bakarrik ikusten dut
alabaren negarra.