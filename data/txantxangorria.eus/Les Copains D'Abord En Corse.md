---
id: tx-2591
izenburua: Les Copains D'Abord En Corse
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yGL_X9tc9AI
---

Fideghja lu to fratellu
Listinghje una catena
A listessa di a toia
A noi tutti ci mena

S'e noi tiremu tutt'inseme
Force ch'un ghjornu sciappera
Da fannu u frombu frombu frombu
Ci ribombi da mare in la

S'e noi tiremu tutt'inseme
Force ch'un ghjornu sciappera
Da fannu u frombu frombu frombu
Cum' un cantu di libertà

Discorre cù u to fratellu
Ci vol'apprunta l'avvene
Sola un idea cumuna
Hè la forza chi ci tene

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Ci ribombi da mare in la

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Cum' un cantu di libertà

In terra di Cursichella
S'ha d'addunisce la ghjente
Per un abbracciu cumunu
Chi sera nova sumente

In terra di Cursichella
S'ha d'addunisce la ghjente
Per un abbracciu cumunu
Chi sera nova sumente

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Ci ribombi da mare in la

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Cum' un cantu di libertà

S'e noi vuleme da veru
Un ci sia pi'u pastoghje
Nè per l'unu ne per l'altra
E cantu sottu à e loghje

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Ci ribombi da mare in la

S'e noi tiremu tutt'inseme
Force ch'un ghjotnu sciappera
Da fannu u frombu frombu frombu
Cum' un cantu di libertà

Da fannu u frombu frombu frombu
Cum' un cantu di libertà