---
id: tx-1852
izenburua: Ezekiel Esnatzea Ii
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pVQbNsFgD7M
---

Zuhaitz habainduen hostoetatik
sortzen den murrailen semaforotik
debekapenen argi gorritik
hasten den kondairaren
besorik bulartsuenetik ere
urreetatik
bastoetatik,
ezpatetatik,

II

Hodeietan ni
hodei sabaian, topaketa ezin luzaturik nabil
arrats hontako globoarekin
hutsaltasun tirano honen betiko besarkadarekin
irlatxo bat naiz giltzarenpean
hondartzapean;

III

Hiri loka hontan ohitutzeraino
zuen ohituretan gaur murgiltzen banaiz
konexioak hausteagatik
izar koloni usaia
kapikuatzeagatik baizik ez da
konturatzen naiz: zuen inbentoan
jaio berri naiz.

IV

Beti gelditzen zaizkit hariak
zeintzuetan kokatu itaun berriak
ez dezadala hori
hura ezabata beti gelditzen dira
zubiak erretzeko prest eta zain
bizkar atzean, udaberrian
udaberrian;

V

Suposatzen dut nire haizeak
zuenak bezainbat ozono duela
suposatzen dut zuenak besala
ene txinaurriak antenez muzukatzen direla
baina ezin uka; nire kredoa atzetik doa

VI

Zorion apurrak azkenerako
hari tartetik ebatsiko ditut
desodoratu beharko ditut
inguru hontako desamorezko
leizen ohiartzunak
igandetako bihotz
hauster hau iraul dezadan.