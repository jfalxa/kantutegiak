---
id: tx-1354
izenburua: Poutpourri Bals
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ba30oOoTPgw
---

Limako arratsak bai eder
eguzkiaren sarreran
enaratxoak hega eta hegaka

lino gorriak ametsetan.

Limako arratsak bai eder
zeru, eguzki eta itsaso
denak abesti bihurtzen bait dira
eta bihotzak gorantz jaso.

Neska-mutilen iskanbilaz
alaitzen dira kaleak
zuhaitzetan txio eta txio
haruntz honuntz txoriak
kalez kale maite minez
amesti neskatil gazteak
kalez kale maite minez
amesti neskatil gazteak.

Limako arratsak bai eder
politagorikan ez da ezer
sorginkeriz alaitutzen dute
ta maite nahia sortu laister.

Limako arratsak bai eder
urrunean izkutatuz
dena eguzkia eta itsaso urdin
inoiz ezin nitzake ahaztu.

Prima eijerra
oi zutaz fidaturik
anitz bagira
oro tronpaturik
enia zinenez
erraizu bai ala ez
bestela banoa
desertila
nigarrez uztera.