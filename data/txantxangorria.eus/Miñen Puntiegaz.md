---
id: tx-2614
izenburua: Miñen Puntiegaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/WnEl73MHhss
---

Zenbat bider kruze gara
ta eguno ikusi bez,
gaurkuen emon dost
barruek ziabogie.
Erdu! Hurretu! Esan zozer belarrire!
Dxusturi perfekto baten
erantziku lotsie.
Miñen puntiegaz
mizkeku hitz osue (itsosue),
kalerik kale ibilko gara dandarrez.
Dxo sirenie… entera daidxela herri osue…
Musturrek jangotsutez
bermiotarrez!! (birritan)
Berbak labandukuz gehidxau ezin artien,
berbak loitxukuz berandurartien.
Nireñe hurretukeran okiñ akorduen,
gau ta egun ingotsut astiro…euskerie.
Musturrek jangotsutez
bermiotarrez !!
Bermiotarrez dxangotsutez musturrek.
Okin argi: ez, da ez!
Miñe barroraino hizkuntzi ikutu arte.
Bermiotarrez dxangotsutez musturrek.
Okin argi: ez, beti da ez!
Neska zein mutil, nagusi zein gazte,
eskuek batuz euskerien alde!
Dxo sirenie… entera daidxela herri osue…
Musturrek dxangotsutez
Bermiotarrez!! (birritan)
Bermiotarrez dxangotsutez, bai musturrek!
Musturrek dxangotsutez
Bermiotarrez!! (askotan)