---
id: tx-471
izenburua: Puzzlea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m89fUwVH8VA
---

Puzzle baten moduan
Osatzen dizut zure bizia
Ez dago osorik
Pieza guztiak bere lekuan izan arte.

Puzzle baten moduan
Osatzen dizut zure bizia
Murgildurikan zaude
Metodiko pieza guztiak axatu arte

Jarraikidetxo zubia
Marraz urdainoa
Atzekoak zerga
Eta ez duzu nahikoa jarraitzen
Duzu hutsunea betetzen.

Zure behatz leunek
Apurka osatzen doaz puzzlea
Eta ez duzu asmorik
Paisaiaz gozatzeko guztia amaitu arte

Sortu dituzu gailurrak
Marrazo ta begira
Izarrak eskuetan dituzu
Eta ez duzu nahikoa jarraitzen
Duzu hutsunea betetzen

Bizi ospatuko dugu
Azkeneko ospagina bezala
Zeharkatu ez gaitezen damuzko olatuen gainean, ez!

Egun eta gauak
Joan urtaroak jatetxera
Amaitu duzu puzzlea
Ispiluan begia zure behatz nekatuek
Puzzlea ukitzean, ez

Ez, ez duzu ezer berezirik sentitzen
Zerbait gehiago zor
Zerbait gehiago esperoan zenuen
Nola ez gurutzatu, eraiki nuen zubia
Itsasoratu izan banintz, ilargiratzeko unean
Jada, ez dago ezer sortu
Puzzlea amaitu da
Bizi ospatuko dugu
Azkeneko ospagina bezala
Zeharkatu ez gaitezen, damuzko olatuen aurrean
Paperezko paterean, hego haize arres hura
Uniberso barrena, jarraituko dugu
Hutsunea betetzen

Bizi eta goiburu
Iturriko azken tanta bailintzan
Bizi ospatuko dugu
Jarraituko dugu hutsunea betetzen.