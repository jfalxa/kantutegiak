---
id: tx-1569
izenburua: Alperrik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8Ij54fsKn2s
---

Beti presaka nabil, 
beti gauza pilo bat,
agenda ez dit balio, 
hartzea ere ahaztu egiten zait, 
hori ere bai.
Denborarik ez dauzkat, hau egin behar dut, 
hori egin nahi dut, dena egin nahi...
Alperrik, alper alperrik!!
Den dena ezin leike, 
galdutako denbora berriz eskuratu nahi.
Sarritan erlojuen orratzak zapaltzen, 
neri harrapatu nahian,
egunak egutegiko orriak amiltzen, 
haiek ez dira gelditzen,
batzuk katetik dijoaz jausten...
Beti bueltaka nabil, beti gauza pilo bat, 
aspaldi batean ez dudan 
gauzak guztiak egin nahian,
hau bukatu behar da,
horma jo baino lehenago, 
oraindik astia dago bizitza dagoen bitartean.
Alperrik, alper alperrik!!
Den dena ezin leike, 
galdutako denboraren bila ibiltzea, 
denbora galtzea da!!
Aldatu, bizitza gozatu, uneak, 
une txikienak ezezik, ospa dezakegu, 
rock and roll kanta bat jotzen bitartean!!
Eta horrela hobe...
Ordua, egunak, asteak, 
hilak, urteak, mendeak, 
denak une txiki askoren pilo aroak dira...
Eta hemen dauzkagu, 
gure artean goza dezagun, 
uzten diguten bitartean behintzat!!