---
id: tx-1114
izenburua: Lokarriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w_P00lbLcko
---

Debekuekin sortu
debekuekin hazi
jakinik ezin dela
beti zigorpean bizi

Mehatxuak batetik
mendekuak bestetik
balorezizutako
gizartearen aldetik

Ez dut nahi zu
ta ez dut nahi zuek
nire modura bizitzea.

Izatea
Zuek izatea
Badatorren gizartea

Libera tu mente
piensa diferente
bu(s)ca tu verda(d)
no de(jes) que te la cuente
La e(s)cuela te enseña
(en)la calle se aprende
Que la realidad
no es co/mo ellos te la venden

Estudia lo máximo
pasa de lo minimo
trabaja con ánimo
por un sueldo infimo

Pero por favor.
Ay! no dejes de ser tu mismo
De ser tu mis/mo!
Bete zizkiguten belarriak
Hitz ederrekin, hitz faltsuekin
Kendu zizkiguten lokarriak
Lotzeko kate batzuekin

Bete zizkiguten belarriak
Hitz ederrekin, hitz faltsuekin
Kendu zizkiguten lokarriak
Lotzeko kate batzuekin


Entra mucha gamba
entre la confusión
todo lo que cuenta
tiene doble intención.

Dicen que es la era
de la información
y es solo_otro invento
de la gran Babilon

Ez dut nahi zu
ta ez dut na/hi zuek
nire modura bizitzea.

Izatea
Zuek izatea
Beti izatea

Be/te ziz/ki/gu/ten be/la/rri/ak
Hitz e/de/rre/kin, hitz fal/tsu/e/kin
Ken/du ziz/ki/gu/ten lo/ka/rri/ak
Lo/tze/ko ka/te ba/tzu/e/kin

Be/te ziz/ki/gu/ten be/la/rri/ak
Hitz e/de/rre/kin, hitz fal/tsu/e/kin
Ken/du ziz/ki/gu/ten lo/ka/rri/ak
Lo/tze/ko ka/te ba/tzu/e/kin

Te cuen/tan que pa/sa
a Ki/lo/me/tros de ca/sa
y en/fren/te de tu puer/ta
no tie/nen luz ni a/gu/a

y vier/ten lec/cio/nes
so/bre e/co/no/mi/a
pe/ro que se ha/ce
si fal/ta la co/mi/da
el pue/blo se can/sa
de tan/ta hi/po/cre/si/a
de tan/ta hi/po/cre/si/a
hi/po/cre/si/a

Bete zizkiguten belarriak
Hitz ederrekin, hitz faltsuekin
Kendu zizkiguten lokarriak
Lotzeko kate batzuekin


Bete zizkiguten belarriak
Hitz ederrekin, hitz faltsuekin
Kendu zizkiguten lokarriak
Lotzeko kate batzuekin