---
id: tx-563
izenburua: Egingo Dugu Eztanda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jrA-xQuAFGM
---

Nos vemos en la vuelta? /// Itzuleran ikusiko al gara?

TOUR 2021
Otsailak 5 de Febrero - Andoain (Bastero)
Otsailak 13 de Febrero - Azpeitia (Soreasu)
Otsailak 19 de Febrero - Getxo (Muxikebarri)
Martxoak 5 de Marzo - Bilbo (Campos Elíseos)
Martxoak 12 de Marzo - Gasteiz (Principal)
Apirilak 10 de Abril - Donosti (Kursaal)
Maiatzak 8 de Mayo - Iruña (Baluarte)


Geldiezinak izan ginen,
izar iheskorra zeruan.
Maite genuena egiten
biluztu ginen gau bakoitzean.

Argiak itzali izan arren, 
sua mantentzen ari gara
eta laster gure azken gaua balitz bezala,
egingo dugu eztanda.

Geldiezinak izan ginen,
iraganeko gazteluan.
Batera geroa margotzen
hunkitu ginen taula gainean.

Ekaitz azpian izan arren
sua berpizten ari gara
eta laster gure azken gaua balitz bezala, 
egingo dugu eztanda. 

Mila aldiz berreskuratuko ditugu
erortzean galdutako indarrak.
Berriz berotuko ditugu ahotsak
elkartzeko kantu berri  batean.

Orain ez dezala denborak itzali,
aspaldi piztu genuen sugarra.
Urrun egon ginen baina itzuli gara
ta azkenengo gaua balitz bezala,
egingo dugu eztanda.


Egingo Dugu Eztanda (Explotaremos)

Fuimos imparables,
una estrella fugaz en el cielo.
Haciendo lo que amábamos
nos desnudamos cada noche.

A pesar de apagarse las luces,
seguimos manteniendo el fuego
y pronto como si fuera la última noche, 
explotaremos.

Fuimos imparables,
en un castillo del pasado.
Dibujando juntos el futuro
nos emocionamos sobre las tablas.

A pesar de ser debajo de la tormenta
estamos reavivando el fuego
y pronto como si fuera la última noche, 
explotaremos.

Mil veces recuperaremos
las fuerzas perdidas al caer.
De nuevo calentaremos nuestras voces
para juntarlas en una nueva canción.

Que el tiempo no apague ahora
la llama que encendimos hace tiempo.
Estuvimos lejos pero hemos vuelto
y como si fuera la última noche,
explotaremos.