---
id: tx-2868
izenburua: Tiriki Trauki
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/pp27nDMzZDc
---

Tiriki-trauki
hiru ta lau dira zazpi,
xurruxumurru
bat batean dira urrun,
tiriki-trauki
ezker eskubi
bien artean dago zubi,
hots bare-bare
hor leiho sare
hiru ta lau dira bi…

Tiriki-trauki
hiru ta lau dira zazpi,
xurruxumurru
bat batean dira urrun,
tiriki-trauki
ezker eskubi
bien artean dago zubi,
hots bare-bare
hor leiho sare
hiru ta lau dira bi…

Tiriki-trauki
hiru ta lau dira zazpi,
xurruxumurru
bat batean dira urrun,
tiriki-trauki
ezker eskubi
bien artean dago zubi,
hots bare-bare
hor leiho sare
hiru ta lau dira bi…

Tiriki-trauki
hiru ta lau dira zazpi,
xurruxumurru
bat batean dira urrun,
tiriki-trauki
ezker eskubi
bien artean dago zubi,
hots bare-bare
hor leiho sare
hiru ta lau dira
bat, bat, bat, bat
la, la, la, la