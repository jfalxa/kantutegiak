---
id: tx-2857
izenburua: Ene Haserrea Imanol Larzabal
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qLcumYwu_N8
---

Eta ene haserrea falta bazaizu
alferrik dira leunak zure begiak
eta ene muxua falta bazaizu
haizeak ezpainak hostogabetzen dizkizu.
Eta ene hitza falta bazaizu
ezin duzu inolaz ere kantatu
eta ene ametsa falta bazaizu
nondik sortuko duzu esperantza.
Eta ene oroitzapena falta bazaizu
zertarako bakarrik paseatu
eta ene gorputza falta bazaizu,
bilutsik zoaz.
Imanol jaunak "Ene haserrea", Koldo Izagirreren poema bat kantatuz. "Iratze okre geldiak" diskoa 1982. urtean ekoiztu zuen, IZ estudioetan, Angel Katarain jaunaren laguntza teknikoaz.

Imanol, ahotsa
Michel Nesprias, akordeoia
Karlos Gimenez, pianoa
Pascal Gaigne, gitarrak
Jean-Louis Hargous, saxofoiak
Dominique Benete, kontrabaxua
Jean Louis Chadufau, perkusioak
Alfredo Rodriguez, oboea

Imanol Larzabal abeslariak poesia eta musika uztartu zituen sentikortasun handiko kantetan. Bere ibilbidean hainbat musika mota jorratu zituen: kantagintza, jazza, rocka, edota musika tradizionala. Paco Ibañez edota Gwendal artistekin lan egin zuen ere.

80. hamarkada oparoa izan zen Imanolentzat ikuspegi musikaletik, baina oso nahasia egoera pertsonalari dagokionez. Ia urtero disko bat kaleratu zuen, "Iratze okre geldiak" (IZ, 1983), "Erromantzeak" (IZ, 1984), "Oroituz" (Elkar, 1985), "Mea kulparik ez" (Elkar, 1986), "Joan-etorrian" (Elkar, 1987), "Muga beroetan" (Elkar, 1989) eta "Amodioaren berri" (Elkar, 1990). Disko hauetan guztietan euskal poeta garaikideen olerkiak musikatu zituen, adibidez, Mikel Arregi, Koldo Izagirre, Joseba Sarrionandia, Juan Mari Lekuona, edota Xabier Lete.