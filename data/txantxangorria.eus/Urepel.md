---
id: tx-1967
izenburua: Urepel
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tuCiXpJKBk4
---

Txoko txiki bat zara
edur artien
lotan zagoz, lasaitasunien.
Zuriz mendi partie
zuriz kaliek
etxe bittartietan, ipar haixie.
Ixilik dago dana
ixilik plaza
ixilik urrunien
kanpandorrie.
Kanpoan hotz egiten deu
sutondoetan sartunde
kantu zaharren artien
joatzu bizixe.
Haixe hotza zara zu
ur hotz, hotza zara zu
ur hotzetan Urepel
ur hotzetan Urepel.
Arratsalde gorrixek
tximinixetan
arratsalde gorrixek
Behenaparroan.
Beheko suek bistute
leixoetan edurre
ezkatzetan egurre
hau zure negue.
Haixe hotza zara zu
ur hotz, hotza zara zu.
Ur hotzetan Urepel
eta zagozen lekuen
ur hotzetan Urepel.
Udaberrixe
etorriko jatzu
eguzki azpixen
mile kolore
eguzki azpixen
bi mile kolore.
Berdez, mendixek
zuriz, kaliek
gorriz bixotzak
Urepel.
Txoko txiki bat zara
benetan maite zaitut.
Haixe hotza