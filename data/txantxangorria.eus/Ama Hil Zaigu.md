---
id: tx-2033
izenburua: Ama Hil Zaigu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/oHJSADSNdec
---

Nera Ama
hil zaigu
lanak ithota;
lanaren amoreakgaik
hil zaigu
zertarako den bizitza jakitera
iritsi gabe
zahartzaroaren beldurrez
gaztaroa ezagutu gabe
izeba ere hil da;
era berdinean
hil da izeba...