---
id: tx-2912
izenburua: Ipuinak Egi Bihurtuz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/r9MBm7bn9Uw
---

Hizkuntzaren itsasoan

kantuz doaz batelerak
horrelako gidariak
behar ditu-ta euskerak.
Maialen ta Xanti biak gogotsu
hortxe hasi dira dantzan
gure hizkuntza noizbait erraldoi
izango den esperantzan. 
Euskaraz bizi, bai!Gu garena azaldu nahi!

Euskaraz bizi, bai!Denok pozik eta alai-alai!

Umeak heldu eta helduak umetu
jolastu nahi dunak aitzakirik eztu
amalurrak ama-hizkuntza du bere esku
biek beti izango gaituzte gu gertu.
Trapujale, tripazale
dela denok badakigu
baina euskera maite duenez
bizi-bizirik utzi du.
Intxixuak eta sorginak
sua estimatzen dutelako
euskeraren garra ez itzaltzeko
hitz egiten dute asko.

Euskaraz bizi, bai!Etxean, kalean non-nahi

Euskaraz bizi, bai! Etorkizuna dugu gure zai!

Umeak heldu eta helduak umetu
jolastu nahi dunak aitzakirik eztu
amalurrak ama-hizkuntza du bere esku
biek beti izango gaituzte gu gertu.