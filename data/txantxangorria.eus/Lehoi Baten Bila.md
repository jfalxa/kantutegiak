---
id: tx-2433
izenburua: Lehoi Baten Bila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yBbZNCyqDOg
---

Lehoi baten bila noa

handiena hartuko dut

ez dut beldurrik

begira zenbat lore

aja, aja!

Zuhaitz bat

zuhaitz luze bat

zuhaitz luze luze bat!

zuhaitz luze luze luze bat!

Igo naiz

begiratu dut

jaitsi naiz

Lehoi baten bila noa

handiena hartuko dut

ez dut beldurrik

begira zenbat lore

aja, aja!

Itsasoa

itsaso zabala

itsaso zabal zabala

itsaso zabal zabal zabala

Murgildu naiz

igeri egin dut

atera naiz

Lehoi baten bila noa

…

Kobazulo bat

kobazulo ilun bat

kobazulo ilun ilun bat

kobazulo ilun ilun iluna

Sartu naiz

begiratu dut

ikutu dut buru bat!

bi belarri!

bi begi!

biboteak!

muturra!

ahoa!

hortzak!

lehoi bat!!

Buelta eman dut

korrika egin dut

kobazulotik atera naiz

itsasoan sartu naiz

igeri egin dut

itsasotik atera naiz

zuhaitzera igo naiz

zuhaitzetik jaitsi naiz

etxera joan naiz

ohe azpian sartu naiz

Lehoi baten bila nindoan

ez nuen handiena hartu

beldurra izan nuen

begira zenbat lore

aja, aja!