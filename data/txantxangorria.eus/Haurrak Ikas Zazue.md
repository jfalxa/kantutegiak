---
id: tx-2728
izenburua: Haurrak Ikas Zazue
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4G6UQJfBilw
---

Haurrak ikas zazue euskaraz mintzatzen, 
ongi pilotan eta oneski dantzatzen.

Ai-re tun txi-ki-tun, ai-re tun lai-re, (lau aldiz)

Gure kantu zaharrak kontserba ditzagun; 
aire pollitagorik ez da sortu inun.

Ai-re tun ...

Ez ahantz berriz ere sorterri ederra, 
haren mendiak eta itsaso bazterra.

Ai-re tun ...

Bihotz leiala ere atxik aitameri
eta nonbait dagoen gazte maiteari.

Ai-re tun txi-ki-tun, ai-re tun lai-re, (lau aldiz)