---
id: tx-2437
izenburua: Hare Ale
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4jiFKmcLrN4
---

AZALERA Lorea Zulaika Arriaga (Ahotsa) Beñat Bidegain Lekue ( Gitarra) Jon Lasarte Mujika (Baxua) Urki Gojenola Ibarloza ( Bateria) IRUDIAK: Nerea Iglesias Lili (Aktorea eta ekoizpen laguntzailea) Asier Rivera Arantzamendi (Zuzendaritza, ekoizpena, errealizazioa, kamera eta muntaia) Eneko Elezgarai Urkiza (Ekoizpen laguntzailea, kamera eta argazki zuzendaria) ILUSTRAZIOAK Beñat Etxaburu Uribe (Diskaren azala eta diseinu grafikoa) ESKERRIK BEROENAK mikro zein kameraz haratago ekarpena egin duzuen guztioi: Xabi Arbonies, Portuko Ranpi eta Larraskanda baserria. Abestia Musikorta estudioetan grabatua izan da Xavi Navarroren "Jota" gidaritzapean eta Asier Badiolak nahastu eta masterizatu du.
-------------------------------------------------------------------------- HARE ALE Erre usaina du etorkizunak kixkalitako itsasuak pareta ikusten dut begi aurrian ezin ikusi urrunago, ezin ikusi... urrunago Esnatu nau harezko erlojuak ta non daude? zure bi itsasuak... Hondartza bat bainitzan olatuen eskale ezpain talken zale eta ez dakit nolatan, baina bihurtu zait jada gorputza hare ale, hare ale,harezko erlojuan barne. Norberak sortutako paretak dira zailenak botatzen burdinezko paretak hare erreak urtuko ditu eta egingo dugu bidea Hondartza bat bainitzan olatuen eskale ezpain talken zale eta ez dakit nolatan, baina bihurtu zait jada gorputza hare ale, hare ale Harezko erlojuan jabe.