---
id: tx-1523
izenburua: Bizipen Margotuak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cItc9LwM1E8
---

TXINGUDI, BIZIPEN MARGOTUAK
Joan etorrian dirauten urekin
besarkatuz joan ziren haiek
kresal haizearen lurrinak dakarren
sentimenduan bizi dire
paduren bihotzean, oroitzaren oihalean
bizipen margotuak,
egunsentietan lanbroaren azpian
itsasorantz lekutuak.