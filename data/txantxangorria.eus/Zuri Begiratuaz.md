---
id: tx-2632
izenburua: Zuri Begiratuaz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/wWVzgJUpKE4
---

Zuri begiratuaz nago ertzeko bidean
Gerturatzen hasia nago arantzazko bidetan
Askotan naute zapaldu zure begi hoiek
Zer esan nahi didaten oraindik ez jakin gabe
Zer esan nahi didaten oraindik ez jakin gabe

Bakar bat izatea dut nik nere bizitze
Nahiz zure aire xarman bat berriz ere behar det
Zeruko izarretan dagoen ederrena da Ekia
Lurreko anderetan, oi ene maitia 
Lurreko anderetan, oi ene maitia 

itsasoa dut aurrean neure arnasetan
berari abesten diot neure ametsetan
itsasoa dut aurrean neure arnasetan
berari abesten diot neure ametsetan
berari abesten diot neure ametsetan

orlegiaren argia geure mendietan
argitasunaren bidean zurekin naiz ametsetan
orlegiaren argia geure mendietan
argitasunaren bidean zurekin naiz ametsetan
argitasunaren bidean zurekin naiz ametsetan