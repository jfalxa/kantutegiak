---
id: tx-1596
izenburua: Gauza Xumeetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/f1IL8cBsqJQ
---

Gauza xumeetan da edertasuna

gauza singleetan da baretasuna

kafe bat, ardo opa bat, 

zure begi aratzak bart,

itsas danbadaren kea

bai maitea.

Gordetzenb den oro galdetzen da,

usteltzen da, itzaltzen da,

ematen den oro hazten da,

berdetzen da, itzultzen da.

Gure barnean dago behar duguna

gure barnean dago handitasuna

arima isilaren soan

bihotz azpiko txokoan

alda arazteko kemena

hor da dena.

Gordetzen den oro galtzen da,

ustelten da, itzaltzen da,

ematen den oro hazten da, 

berdetzen da, itzultzen da