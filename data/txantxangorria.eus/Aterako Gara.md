---
id: tx-2241
izenburua: Aterako Gara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qNDiW5EPvQE
---

Julen Cobos, Gorka Benitez, Ricardo Boya eta Cesar Cisneros. 
Bartzelonan 2020 udaran grabatuta.

Bideoa, Julen Cobos, Julio Bravoren "El tren" (1940) dokumentaleko irudiekin.

Aterako gara eta Tarantela Sanferminerak, "7 asteko ilunean" lana osotzen dute. Bi kantu, bi bideo, bi begirada konfinamenduari.


Aterako gara, zulo hontatik guztiok batera
aterako gara, zulo hontatik eskutik helduta
Senide eta lagunak gaur etxian sartuta
eutsi gogor, zaindu maite duzuna
Balkoian barrikada, trintxerak sukaldean
eutsi gogor, zaindu maite duzuna
zaindu maite duzuna
Aterako gara, agian ez da izango berdina
Aterako gara, akaso zertxobait ikasita
Senide eta lagunak, gaur berriro kalean
ez dezagun inor utzi bidean
zazpi asteko ilunean argi izpi bat kantuan
eutsi gogor zaindu maite duzuna
zaindu maite duzuna
Aterako gara, zulo hontatik guztiok batera
Aterako gara, zulo hontatik eskutik helduta