---
id: tx-1196
izenburua: Aupa Epifanio!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aElXk54gaiU
---

Musika: Xabier Zabala
Hitzak: Amaia Agirre



Zertan ari da aitona Epifanio
Afrikako haurrengan irriparrak sortzen.
Irriparrak sortzen? Nola?
Bizikleta zaharrak konpontzen!

Eman haizea gurpilari
ta arnasa etorkizunari
Irri sortzaile bolondresa
milaka umeren ametsa.

Aupa Epi haurren bidelagun
aupa Epi txapeldun
azokarako bidea hegan
egin dezagun
Aupa Epi haurren bidelagun
aupa Epi txapeldun
zugan zenbat elkartasun
eta zenbat maitasun

Aupa Epi haurren bidelagun
aupa Epi txapeldun
azokarako bidea hegan
egin dezagun
Aupa Epi haurren bidelagun
aupa Epi txapeldun
zugan zenbat elkartasun
eta zenbat maitasun

Ring-ring-ring-ring
pedalei gogor eragin

Gurpilen gainean
muxu truk lanean
irria sortu asmotan
umeen ahotan
itsasoaren bestaldean
umeak zure galdean
ezer ez duenari
bizikleta opari

Aupa Epi haurren bidelagun
aupa Epi txapeldun
azokarako bidea hegan
egin dezagun