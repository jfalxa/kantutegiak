---
id: tx-921
izenburua: Ametsak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S7Hdqx7INqY
---

Zein ederra zagozen esanda
hitzak ere zuretzat ditut
handik aurrera gorputzak berak
ezin daust oreka mantendu,
aurkitu, sentitu, lasaitu, zoratu, ezin kendu
gero, buruan garatzen doa
azkenean bihotzean dut.
Koloreen artean beti iluna,
abestiak mila, zure isiltasuna,
zure begietako ispilu aurrean
neure bihotza biluzten da.
Ametsak zurekin, konplitu ein barik
gauetako izarren artean nago ni.
Maitasun uretan, itota nintzenean
zeure basoan aurkitzen zen fruitu heldua.
Beti okerra aukeratzeak
kalteak ere baditu,
errealitatea ezin bizi
ta ametsak ezin konplitu,
azaldu, jarraitu, geratu, bukatu, esanak ditu
mundua ta denbora nire aurrean
orain gelditu ahal banitu.
Erakutsi ez dabena
bizitza honetan,
mila bider jo dut
hormaren kontra,
izan nintzen negarrarekin hizketan,
zure helbidea eman zidan.
Ametsak zurekin, konplitu ein barik
gauetako izarren artean nago ni.
Maitasun uretan, itota nintzenean
zeure basoan aurkitzen zen fruitu heldua.
Ametsak zurekin, konplitu ein barik
gauetako izarren artean nago ni.
Maitasun uretan, itota nintzenean
zeure basoan aurkitzen zen fruitu heldua.
Ametsak zurekin, konplitu ein barik
gauetako izarren artean nago ni.
Maitasun uretan, itota nintzenean
zeure basoan aurkitzen zen fruitu heldua.
Ametsak zurekin, konplitu ein barik
gauetako izarren artean nago ni.
Maitasun uretan, itota nintzenean
zeure basoan aurkitzen zen fruitu heldua.