---
id: tx-2556
izenburua: Errregeak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/lcsBI6MKmaY
---

Errege bat Meltxor
Bizar zuri eta pottolon.
Tximini zulotik sartu …
Opariak utzi eta…
Ssshhhhh…GABON!
Atzetik errege Gaspar
Bizar gorri eta Zapiron.
Umeen gelara igo…
Opariak utzi ta …
Ssshhhhh…GABON!
Azkenean gure Baltasar
Beltza eta ikatz-koskor.
Zalditik balkoira igo…KATAKLON!!
Musu bat eman eta…
Ssshhhhh…GABON!