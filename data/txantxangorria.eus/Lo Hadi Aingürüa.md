---
id: tx-475
izenburua: Lo Hadi Aingürüa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/QIUVPxOlvIk
---

Lo hadi aingürüa amaren altzoan
hire herri eijerra botz ezpain xokoan
Axolbean gütük bena nik tristüra gogoan
aro gaitza kanpoan ta_aita itsasoan.

2
Beha zak haize beltza mehatxukaz ari
Zer gaü aldi lüzea, zoinen etsigarri!
Harritürik ikaran niz lotü kürütxeari
Otoitzez Jinkuari egin dezan argi.

3
Lo hadi, lo, maitea, ez egin nigarrik,
zeren nihaur aski nük haben dolügarri.
Egik zerüko aingürüer herri batez batzarri,
pentsa dezan aitari jin dakigün sarri.