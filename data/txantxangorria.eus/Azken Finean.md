---
id: tx-3365
izenburua: Azken Finean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/b5Fu5KHOUmk
---

AZKEN FINEAN
Azken finean 
gauza gutxi dira 
ezinbestekoak
abisatu gabe 
datorren euria bezala
espero ez duzun ur 
zikin hori
zure memoriaren 
zuloetatik sartzen.
Gauza gutxi direlako 
ezinbestekoak 
azken finean
ametsak ere 
aitzakia bat 
besterik ez
inoiz izango ez garen 
hori izango ez garela
onartzeko.
Ezinbestekoak 
gauza gutxi direlako 
azken finean
mina bezala bizirik 
gaudela oroitarazteko
ez direlako gauza bera 
mina eta samina
nahiz eta biek min eman.
Gauza gutxi dira 
azken finean 
ezinbestekoak
azalaren lehen mintzean 
gordetako beldurra bezala
begiak zabalik edukitzea
edo salbatuko garela pentsatzea.