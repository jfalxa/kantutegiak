---
id: tx-929
izenburua: Dastatu Mungia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6fS72k5a_VE
---

Letra: Mungiako Taket bertso eskolak  
musika: Taberna Ibiltaria taldeak. 
Bertan parte hartu dabe:
Ibon Altuna ( Itziarren Semeak ), 
Mikel Bizar ( Idi Bihotz eta Auritz  )  
Zamudioko Bertoko soinu elkarteko 3 neskato 
eta Iñigito ( Txapelpunk ) . 


Zahartu ahala umore aldetik
ez dana oso sendoa
kolore aldatzen hasten da eta
ozpintzea da hurrengoa
berrigei urte pasa ta Urrutia
edariak da lehengoa
urteak joan baina ardao onak
legez hau hobetzen doa

Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna
Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna

In vino veritas esan zeuen
Plinio erromatarrak
"Ardao barruan egia dago"
Moduko kontu edarrak.
Ardoari esker garala egun
barriz esaera zaharrak
egarri barik edaten dogun
animalia bakarrak.

Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna
Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna

Gu ze ete garan ardaoa lagun
azaldu guran gabiz ta
botila bati begiratuta
hona hemen hurrengo pista
ardao baltz, zuri, txakolin edo
Nafarroako gorrixka
gustoko dauzen pertsona behintzat
ezin da izan arrazista

Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna
Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna

Ardaoan egi gaitasunari
hainbeste deutsegunez zor
Karl Marx handiak esandakoa
aipatu behar derrigor.
Behin mitin baten bota ei zeuen
oholtza ganetik gogor
ardao edaten ez dakienaz
ez daitela fida inor

Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna
Ardao azokaren eginkizuna
gu alkartzea ta alkartasuna

Une txarretan dagoenari
Alaitzeko aurpegia
ahalegin txiker bat egin zeinke
bota barik izerdia.
Bilatu ardoak zure barruan
gordeta dekon egia.
Erdu azoka solidariora
eta dastatu Mungia.