---
id: tx-697
izenburua: Hotziturik
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VpbKkUfL91Q
---

Zerua lainoz bete
udazken arratsalde
batean nago ni

Barroteen atzean
udazken kolorea negua dirudi

Kanpoan, nahiz barruan
hotziturik gaude
kanpoan, nahiz barruan
hotziturik gaude

Gau iluna heltzean
izarren itzalak
tristura ematen dit

Hormen iluntasunean
argi izpiak
itxaropena sortzen dit

Kanpoan, nahiz barruan
hotziturik gaude

Kanpoan, nahiz barruan
hotziturik gaude

Kanpoan, nahiz barruan
hotziturik gaude

Kanpoan, nahiz barruan
hotziturik gaude