---
id: tx-2921
izenburua: Buruhandi Eta Erraldoiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JQzBP1o2G9o
---

Jo eta jo ari dira
txistu eta danbolinak,
jo eta jo kalejiran
buruhandi eta erraldoiak.

Airean maskuria
kili-kili gustura,
airean bihurria
ipurdian hazkurra.

Dinbi-danba kolpeka
iji-aja barrezka
haurrak zirikan, xaxaka,
kalean behera iheska!