---
id: tx-2907
izenburua: Amorez Eria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t5ZyTLEiq4I
---

Amorez eri nago ,
aspaldi onetan,
zuregatik, maitea,
gau ta egun penetan;
arki nezake poza,
badakit nik zertan:
sendatuko nitzake
zure besoetan. 

Biotz baten lekuan
milla banituke,
guztiyak zuretzako
izango lirake;
baña millaren lekuan
bat besterik ez det,
artu zazu, maitea,
bat au milla bider. 

Gustoz artuko det nik
zure faborea,
izanagatik ere
amore lorea;
eduki bear nazu
barrenen gordea,
begira ez zaitezen
mudatu ordea.