---
id: tx-287
izenburua: Gatza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/a2nDZbiYJ-E
---

Kilimak taldearen "Gatza" abesti berriaren bideoklip ofiziala.


Hitzak:

Gatza, gatza
zure bizian bota gatza ta egin
dantza, dantza
zure gorputza mugitzean datza
Etzazu esan ez dakizula dantza egiten
jarraitu musikari aldakak mugitzen
lotsarik gabe izango zara zure jabe
zure beldur handiena besteak pentsatzen dutena
Ta entzun!
altxa besoak gora
etzazu galdu denbora
hator gauaz gozatzera
oraintxe daukazu aukera
ta ipurdia bera
mugitu hankekin batera
hator gauaz gozatzera
bestela joan zaitez ohera
Gatza, gatza
zure bizian bota gatza ta egin
dantza, dantza
zure gorputza mugitzean datza
gatza, gatza
etzazu esan ez dakizula, ez dakizula dantza egiten
dantza, dantza
jarraitu musikari ipurdia eta hankak mugitzen
gatza, gatza
lotsarik gabe izango zara zure buruaren jabe
dantza, dantza
zure beldur handiena besteak pentsatzen dutena!!

Saiatu zaitez gaurko mugak
lerelerele lerelerele
izaten biharko helmugak
lerelerele lerelelaii
xehatu zure beldurrak
bihurtuz ogi papurrak
xehatu zure beldurrak

Ta entzun!

altxa besoak gora
etzazu galdu denbora
hator gauaz gozatzera
oraintxe daukazu aukera
ta ipurdia bera
mugitu hankekin batera
hator gauaz gozatzera
bestela joan zaitez ohera
bestela joan zaitez ohera
bestela joan zaitez ohera
gatza, gatza!

-----------------------------------------------
Jarraitu gaitzazu! / ¡Síguenos! / Follow us!