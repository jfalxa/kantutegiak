---
id: tx-96
izenburua: Non Geratu Dira
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FZqEgidDP7M
---

Zelanda taldearen "Non geratu dira" abestiaren bideoklip ofiziala.


-------------------------------------------

Hitzak:

Egiten genuenean
Berba bizi goseaz
estali ezinean
irribarre goxoenak

Besarkatzen ginenean
errudun sentitu gabe
Oraina, umezurtza da baina
Nork itzuliko digu bizinahia

NON GERATU DIRA
NORA DARAMA
NON GALDU GARA
ILUNTZEN HASI DA
NOLATAN, NOLATAN IZAN DA

Gauetan, hurbiltzen garenean
Ez dut hotzik nahi nire biriketan
Su horren arnasak, bizirautea nahi dut
Banatzen gaituzten (n)horma guztiekin amaitu

NON GERATU DIRA
NORA DARAMA
NON GALDU GARA
ILUNTZEN HASI DA
NOLATAN, NOLATAN IZAN DA

-------------------------------------------

Musika:
2021eko udaberria Zestoako Gaztain Estudiotan grabatua eta nahastua
@gaztain_estudioak
Kolaborazio berezia: Olatz Salvador @olatz.salvador / www.olatzsalvador.com
Gitarra eta ahotsa: Fran Urias @franuriasagirre

Baxua: Ane Bastida @bastitxatxi
Bateria: Iñaki Bastida @bastisaiz
Teklatua: Lander Anton
Grabazioa, nahasketak eta ekoizpena: Eñaut Gaztañaga @gaztaingrises

-------------------------------------------

Bideoa:
Zuzendaria: Iker Villa
DOP: Julen de la Serna
Ekoizpena: Gaua
Arte zuzendaritza: Maria Redondo, Iker Aginaga eta Iker Villa
Muntaia eta kolorea: Iker Villa
Aktoreak: Alazne Etxeberria eta Fran Urias
Zelanda taldea: Ane Bastida, Iñaki Bastida, Lander Anton eta Fran Urias
Taldearen irudiak Azpeitiko Soreasu antzokian grabatu dira.

-------------------------------------------

Esker bereziak: Kulturaz kooperatiba, Iraitz Olaizola, Santi Redondo, Bego Mugica