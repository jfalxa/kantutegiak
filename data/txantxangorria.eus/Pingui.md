---
id: tx-3389
izenburua: Pingui
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/rhlpLbKBb_w
---

Pingui pinguino txiki-txikia
Plist!plast! Plist! Plast!
sabela zuria. (bi aldiz)

Txano Borobila bufanda gorria
Plist!plast! Plist! Plast!
dotore jantzia. (bi aldiz)

Ur hotzetan eskuin eta ezker
Plist!plast! Plist! Plast!
Moko luze oker. (bi aldiz)

Negua heltzean lodi-lodia
Plist!plast! Plist! Plast!
Mugitu ipurdia. (bi aldiz)