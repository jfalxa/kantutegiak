---
id: tx-449
izenburua: Amodiozko Poema
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yMTja6FTK5M
---

Gu, dantzari zuberotarrak ginen Estuerdoen kortean
eta ez ginen haien izkuntzaz mintzatzen.
Horregatik, egun guztiz, oihuz genuen dantzatzen.
Haien ondoan inguratutako ega-luze saldo bat ginen
eta zuk autobideko neskatxa maitea
egunsenti-arte kantatuz laztandu ninduzun,
urriko gau epel batean arrapatutako einara nerea.
Egun hartara itzuli nahi nuke
emeki-emeki
itsasoari begira, begi itxiz
jiratutako itzulian barrena.
Neure behatzez gure herriko belarra billatzen,
aizean gure herriaren erortea,
Zarautzko zineetan begirada trixteko
eta buruan aluminiozko xederadun neskatxa billatzen,
gure herriko plazeetan Otsagiko «boboa»
Pelikula mutuetako ereduak
Japoiko portabioietako lumez aztandu ditugu,
tiburoien ortz eta aginen barrena ibilli behar izan dugu,
geure seme-alabak itsas-ertzean eduki ditugu
aingira eta itsas-trikuen eskumenean.
Odolezko ohetan lo dugu egiten
eta
atzerriratzen gaituzten trenetako
komunetan negar egiten
Bainan ez izu, maite,
gure etxera inor ez dator orain,
Gorbeian urtzen ari den elurra da '
ibaiak arrotuak jausten dira,
itoko ahal dituzte?