---
id: tx-1446
izenburua: Euskararen Txantxangorria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-5qW4BT_Wr0
---

Sortaldeko argi berriari
ari zaio aspaldi
arratseko azken izpiari
egun eta gizaldi

Bular gorri, hegakada arin
Lur hauetan da emari Erreka eta ibai zabalaren, Itsasoaren egarri

Lau haizetara hegan
Bera dabil airean
Ahotan, belarritan
Su epelean
Kalearen bizia
Herri honen irria
Euskararen txantxangorria

Haizearen magal leunean Hedatu da oihua Bihotzaren taupadetan, hitzak Sortu du doinua

Olatuek beren solasean
Aukeratutako harria
Denboran zehar dute jolasean
Eten ez dadin haria

Lau haizetara hegan
Bera dabil airean
Ahotan, belarritan
Su epelean
Kalearen bizia
Herri honen irria
Euskararen txantxangorria

Lau haizetara hegan
Bera dabil airean
Ahotan, belarritan
Su epelean
Kalearen bizia
Herri honen irria
Euskararen txantxangorria