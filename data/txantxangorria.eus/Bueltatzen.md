---
id: tx-2068
izenburua: Bueltatzen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/f5kzIZBUiIw
---

Mundua ikusi nahi dut
baina zure ohetik
Mundua ikusi nahi dut

Ahazten ez duzun kantu
zure ezpainetan
kantua izan nahi dut

Ta non ezkutatzen zinen orain arte
urte luzeegi hauetan agertu gabe
Helduidazu eskutik estu arren
ilun dago ta ez dakit bueltatzen

Itsaso izan nahi dut
zure ekaitzetan
Itsasi izan nahi dut

Etxerako bidea
soilik zure oinetan
Bidean izan nahi dut

Ta non ezkutatzen zinen orain arte...

Mundua ikusi nahi dut
baina zure ohetik
Mundua ikusi nahi dut
zure hitzen talaiatik

Ahazten ez duzun kantu
zure ezpainetan
kantua izan nahi dut