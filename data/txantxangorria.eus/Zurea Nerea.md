---
id: tx-2442
izenburua: Zurea Nerea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-Zc_d4VNdkA
---

Gure lehen lana, TRIPTICS I-II diskoa, salgai duzue: 
Jarraitu gaitzazu sareetan:
TripTic II: Gernikaldean HIBAI - Zurea-Nerea (Solo live) 
Musika eta Hitzak: Hibai Etxebarria

 Esaiztazuz 
Zure kontutxuek 
Ezagutu 
Zure sekretuek 
Emaidazu 
Hortako aukerie 
Zu ezagutzeko bidie 
Eiztazu Irribarretsu bet 
Ehizatu 
Gabeko mamuek 
Sendaiztazuz 
Bihotzan zaurixek 
Zu nire aingeru gidarixe 
Zurea zara ez zara nerea 
Inor ez baita inoren jabea 
Zertarako egin amets 
Esna zaudenean 
Hobe elkar… maitatzea 
Debekaturik bait dago 
Gure arteko amodioa 
Baina ezin dut ukatu 
Zurea izan nahi dut 
Eta zuk… Nerea? 
Nigan bada zuretzat lekue 
Ekaitzean babes aterpie 
Ilunetan gidari argixe 
Kantu ederrenen alosie 
Zurea zara ez zara nerea... 
Ez zara nerea… 
Nerea…