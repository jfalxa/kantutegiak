---
id: tx-1541
izenburua: Mintza
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Z8h7VF-1hHY
---

Zer nahi duk, zer nahi dun
zer gaittuk, zer gaittun.
Eta zer daukagun?

Inor izan dena izan garena,
izan gaitekena.

Tu que vols, qué volem,
nosaltres qué som, i, qué ets
i el qué tenim?

qué ha set, el que hem estat,
el que podem ser

Aiaiaiai berrehun mila galdera.
Gurea, zurea, hirea
zabaldu itzazue ateak
oihua, musua eta lerdea

Ez al da gurea
hautatze eskubidea?
Herriak jaio berria du haizean
erabakitzeko eskumena duen umea

Eta zer, eta nork, inoiz
eta inon, egingo dugu.

Gure bidea, izango garena, aukeratzea

I ara qué, i ara qui, ho farem
quand i on? aixó segur.

El nostre camí poder elegir
el que serem.

Manugaitz: Hitza da giltza,
herria mintza
Utzi alboan aditz bortitzak.
Ekintza beharrezkoa, legezkoa,
lege bidegabeen gainetik: gure eskumena
guztion erabakimena.

Zure adimena eta nirea,
milakakoena bihotz berean.
Egarri larri kontenezin eta mina
Gure esku dago itsasoa, uhina!

Ilunbetik Bergarara
emititzen Bergaratik ahal gara, 
Palestina libre! Catalunya lliure!
Eskozia ere, Sahara libre!
Baita ere herri guztiak!
Erabakitzeko eskumena da

Bada ordua! Gurea ere,
Euskal Herria! Euskal Herria!
Libre!
Euskal Herria libre!
Erabakia libre!
bakoitzak edozein bezalaren
bidea behar du libre