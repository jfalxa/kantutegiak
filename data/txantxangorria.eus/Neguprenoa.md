---
id: tx-1026
izenburua: Neguprenoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0f_9SjqyYX8
---

Itsasoak olatuak periodoetan dakartzan bezala
dakarz bihotzak taupadak.
Udazkena heltzeko beldurrari buruz


ADVERTISING

hitz egin nizun, labar haretan.
Hotzak atea jotzearen beldurrari buruz,
ohea hutsik.

Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.

Ta laster zaparradek mugituko dute leihoa
ta ni aterpetik begira.
Eta zu, ordea, zure irribarre epel horrekin,
neoprenatzen negua.

Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.
Azken uda egun hartatik itzuli nintzen udazkenean.