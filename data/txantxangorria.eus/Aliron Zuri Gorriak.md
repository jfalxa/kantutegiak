---
id: tx-514
izenburua: Aliron Zuri Gorriak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/-8wTkHI1ZRs
---

Taberna Ibiltariak Athletic-en omenez sortutako abestia. 

Eskerrik asko bihotzez Athletic, Filmak TV, Bilboko Erraldoien Konpartsa, Bilboko Udala, eta Museo de la Minería del Pais Vasco, eskainitako laguntzagatik. 

Melodia: Jesús Prieto "Pitti"

Bideoa: magic.visuals2021@gmail.com 

ALIRON ZURI-GORRIAK (Juan Luis Goikoetxea/Taberna Ibiltaria/ Pitti)

Meategi batean sortuta
sustraiak burdinazkoak dira, 
Ipar haizeak ekarri euskun
baloiaren jira eta bira.

Osterantzeko bidea hartuz
idatzi bako erabakian,
jokalari-iturri bakarra
euskal gazteen gol-harrobia.

Baloi-munduko lore bitxia,
sinestezineko miraria.
Ibaizabaletik gora dator
gabarra, irabazle-saria.

Lehengoak izan zirelako
ondorengoak izango dira
Iribar, Pitxitxi, eta Zarra
(e)ta etorkizuneko gazteria

Bihotzean haroat Athletic,
maite haut guztiaren gainetik,
heurea, zinez, harrotasunez,
atzo, gaur, bihar (e)ta beti

Lehoien orroa: “beste bat”,
gure San Mames zuri-gorrian,
zaleen bihotz gori-goria:
ALIRON, EUSKAL HERRIA

La lara, lara, lara..... ALIRON, ZURI-GORRIAK

La lara, lara, lara..... ALIRON, ZURI-GORRIAK