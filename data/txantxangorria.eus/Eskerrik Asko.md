---
id: tx-1129
izenburua: Eskerrik Asko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/n2OwRiVq_dQ
---

Pupua etorri zaidanean 
nirekin bat izan zarelako
zuen marrazki (e)ta eSaMeSak 
bitamina goxoak direlako
eskolako lanetan txukun-txukun 
laguntzen didazuelako
pirata-zapia buruan, 
ezpata eskuan, 
altxorra zain daukagulago.

Gau ta egun beti ondoan lagun
hor zaituztedalako
senide, lagun, irakasle, mediku
mila-pila eskerrik asko!
Eskutik helduta, aupa, goazen
jira-biraka, salto
malkoak irri bihurtuko ditugu
mila-pila eskerrik asko!

Bisitan atea zabalduta
bihotza zabaltzen diguzulako
bizi-pozak duen sekretua
guztiokin banatzen duzulako
gauza txikiak handiak
direla erakusten diguzulako
eta ausarten kapitaina bilakatuta
asko maite gaituzulako.

Altxorrik handiena zurekin
aurkitu dugulako
zoriontasuna gure baitan dugu
mila-pila eskerrik asko!
Eskutik helduta, aupa, goazen
jira-biraka, salto
malkoak irri bihurtuko ditugu
mila-pila eskerrik asko!
sendatu naiz!