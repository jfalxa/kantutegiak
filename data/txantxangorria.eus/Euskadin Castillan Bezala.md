---
id: tx-1685
izenburua: Euskadin Castillan Bezala
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/bLtHaCm3_SM
---

Ene Segurako aitonak
ogia behar zuenean
Castillara joaten zen
jornalaritzara,
morrointzara.

Bere igitaia trebenetakoa omen zen
bai, hori bai…
Hamar haur zituen gosez etxean
bai!, hori bai!

Castillako jornalariek
ogia nahi dutenean
Goierrira joaten dira
jornalaritzara,
fabriketara.

Kanpokoak pizkorrak omen dira peontzan
bai, hori bai…
Hamaika haur badute gosez etxean
bai!, hori bai!

Castillako soroez eta Goierriko tximiniez
ene aitona eta kastellanoak
jabe bazeneza,
heuren haurren haseak
ez lukete ihes gehiagorik ikusiko!
ez Castillan, ez Euskadin.