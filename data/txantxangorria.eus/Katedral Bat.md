---
id: tx-1259
izenburua: Katedral Bat
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mGykje1xMsk
---

Hona hemen BERRI TXARRAK-en bideoklip berria, 2018ko martxoan Bilbao Exhibition Center - BEC!-eko kontzertu historikoan zuzenean grabatua. Birari Asia eta Ozeanian ekin ostean, 10.000 zaletik gora bildu zituen taldeak, ‘INFRASOINUAK’ (Only In Dreams, 2017) Bill Stevenson eta Jason Livermorek ekoitzitako disko berriaren 
aurkezpen saio bakan eta berezian. 

Buy the CD/LP album here: 
www.berritxarrak.net/berridenda



Gitarra astindu
eta ikusi kanturik baduen hor nonbait ezkutuan
behatzak bihurritu
ea norbaitek ahaztua duen akorde minorren bat

Eta min eman arte zure falta izan
ez da musa hoberik

Katedral bat egin hegalik gabe sortu ziren
abesti pusketekin
bertako beiratetik sartzen diren eguzki izpien
energiaz berrekin

Eta min eman arte zure falta izan
ez da musa hoberik
ikasitakoa desikasten ikasi
ahaztutakoa ahaztu eta aurrera egin

“Irakurtzea da bestela idaztea”
paperak aitortzen dit
“kantatu besterik ez nuen egin nahi”
erantzuten diot nik

Neure ahotsa bilatu, beti besteena entzun
loa kentzen didanari ekintzekin erantzun
lagunarteko uneak luzarazi
maitasuna egin maite dugunekin
carpe noctem!

Eta min eman arte zure falta izan
ez da musa hoberik
bihotzen artean parkour egitean erori
baina beti zutik erori

Gitarra laztandu barruan kanturik baduen…