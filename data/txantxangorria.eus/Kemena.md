---
id: tx-417
izenburua: Kemena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Y69VA_DKmjI
---

KEMENA- ZEA MAYS 2021
Cast: Aiora Renteria, Claudia Chocarro, Zaldi baltza.

Ekoizpen arduraduna: Xalba Ramirez.
Argazki zuzendaria: Iyán Altube.
Kamerak: Iyán Altube, Arriguri.
Gafer: Asier Marleza.
Arte zuzendaritza: Claudia Chocarro.
Jantziteria: Claudia Chocarro.
Makilaje eta ileapaindegia: Laura Umaran.
Edizioa: Sara Monedero
Kolorea: Helí Suárez

Eskerrak: Alegría abereak, Busturiko Udala, Adrian Nogales "Ninja", Badator, Nestor Urbieta.

Asier Basabe- drums.
Ruben Gonzalez- bass.
Iñaki Imaz “Piti”- electric guitar, acoustic guitar.
Aiora Renteria- vocals, backing vocals, synthesizers.

Santi García-  Synthesizers.

Music and lyrics- ZEA MAYS

Produced by SANTI GARCÍA
Sound engineer - Mixed by SANTI GRACÍA
Recorded @ULTRAMARINOS, Sant Feliú de Guíxols
Mastered by VICTOR GARCÍA

KEMENA

NIRE ITSASORA ITZULI NAIZ
BIDAIAN JOATEKO AURRERA
TA EZKUTATU DUDAN KEMENA
BERRESKURATZERA

KORAPILATUTAKO ESKUAK
MINAK ZURI BELTZEAN
UR TANTAK NIRE GORPUTZEAN
EURIRIK EZ DENEAN

GURUTZATUTAKO BELDURRAK
ZUBIAK BIHOTZEAN
UR GEZA BEGIETAN
NEGARRA ERREKA DA

NIRE ITSASORA ITZULI NAIZ
BIDAIAN JOATEKO AURRERA
TA EZKUTATU DUDAN KEMENA
BERRESKURATZERA

BEIRA APURTUEN HOTSAK
NIRE EZTARRIAN
ZIN DAGIT, HASIKO NAIZ
EZ DAITELA BERANDU IZAN

NIRE ITSASORA ITZULI NAIZ
BIDAIAN JOATEKO AURRERA
TA EZKUTATU DUDAN KEMENA
BERRESKURATZERA