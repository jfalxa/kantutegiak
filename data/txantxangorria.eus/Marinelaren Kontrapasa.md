---
id: tx-2735
izenburua: Marinelaren Kontrapasa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/HlK670TvpJw
---

Marinela,
Zuk beti ezin egona!
Ohetako,
Itsaso gazi urdina!
Teilatutzat,
Zeruko hedoi arina!
Begietan
Izarren argi gordina!
Kulunkari,
Beladun untzi sorgina!

Hargatik, hargatik,
Ez gal gogotik
Zure goait, zure goait
Dagola norbait
Itsas-hegitik!

Mariñela
Zuk laket aize bizia!
Ez astio
Uhainen jantza-jauzia!
Bainan nunbait
Lore bat, dena grazia,
Begi urdin,
Sudur motx, ilhe hartzia,
Zure beha
Badago urthe guzia...

Geroztik, geroztik,
Ez gal gogotik
Zure goait, zure goait
Dagola norbait
Itsas-hegitik.

Mariñela
Lanjerra duzu ikasi:
Zonbat neska
Hor gaindi ere ikusi,
Bakar batzu
Ez baizitzaizun itsusi:
Ba ahal zira
Hetarik eman ihesi
Ez baitautzu
Bihotza nehork ebatsi!

Nehundik, nehundik
Ez gal gogotik
Zure goait, zure goait
Dagola norbait
Itsas-hegitik.