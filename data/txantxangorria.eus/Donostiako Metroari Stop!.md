---
id: tx-2441
izenburua: Donostiako Metroari Stop!
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w4qNL74TPvY
---

Behin baten herri polit baten,
makina handi bat sartu zen,
eta herritarrak galdetu gabe
hasi zen kaleak zulatzen.

Leherketak goiz ta atsalden
hasi zitzaizkigun bapaten
ta jendea aluzinatzen
inork ez zuen holakorik eskatzen

Ta satorrak negarrez...
Galdetzen ea zer gertatzen dan
gure mundu ero honetan

Baino lasaitu sator...
zen satorralaia hemen dator
ta ekartzen digu soluzioa, herri mobilizazioa

Estaltzeko euren zuloa

Metroa gelditzen, metroa gelditzen
herria ere hasia delako antolatzen
Eta metroa gelditzeko gaudela hemen
jende jatorran arten herria ere hasia delako eraikitzen, protestatzen

Ta metroa gelditzen...

Donostiako metroa
bizilagunen terremotoa
zulatu nahi digute
gure lurra eta poltsikoa

Ta bizikletan gabiltz gustora
autobusa garesti baino ondo doa
ez gastatu diru publikoa
beharrezkoa ez den kapritxoan

Sekulako gastoa...
200 miloi eurotakoa 
ta nola dauden pentsioak

ta lan prekarioak
badira benetako arazoak 
baino daukagu soluzioa, herri mobilizazioa

estaltzeko euren zuloa

Metroa gelditzen, metroa gelditzen
herria ere hasia delako antolatzen
Eta metroa gelditzeko gaudela hemen
jende jatorran arten herria ere hasia delako antolatzen

Eta ez al zerate, konprenditzeko gai ez degula lur azpitik ibili nahi?

Hiri eredu hau nahi!