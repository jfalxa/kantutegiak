---
id: tx-1149
izenburua: Gure Ereserkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/j9hcK04CjME
---

Eskerrak eman nahi dizkizuet modu honetan 
Zuen omenez idatzitako kantua baita 
Nire alboan egon zarete momentu latzenetan 
Beti aldamenean! 

Gaur irri baten inguruan elkartuko gara 
Barreak bota auzoko tabernetan 
Trago bat orain eta gero beste trago bat 
Honek ez du amaitu behar! 

Betidanik izan ez bada 
Betirako izan dadila. 

Denok batera kanta 
Hau da gure ereserkia 
Egun ilunak koloreztatzeko 
Hemen zaudete lagunak 

Ohartu ez bazarete ere hemen gaude batera 
Familia polita osatu dugu jada 
Etorkizunari eskatzen diot gauza soil bat 
Hautsezina bihurtu dezala! 

Berrogei urteren buruan konturatzea 
Oraindik ere elkarrekin gaudela 
Denborak bere aztarnak utzi ditu baina 
Izandakoak gara! 

Betidanik izan ez bada 
Betirako izan dadila 

Denok batera kanta 
Hau da gure ereserkia 
Egun ilunak koloreztatzeko 
Hemen zaudete lagunak 

Betidanik izan ez bada 
Betirako izan dadila 

Denok batera kanta 
Hau da gure ereserkia 
Egun ilunak koloreztatzeko 
Hemen zaudete lagunak 

Denok batera kanta 
Hau da gure ereserkia 
Egun ilunak koloreztatzeko 
Hemen zaudete lagunak