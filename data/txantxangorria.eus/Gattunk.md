---
id: tx-2899
izenburua: Gattunk
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/aQonEOl2mdQ
---

Gattunk, Gattunk

lotzen gattun harira

Gattunk, Gattunk

lotzen gattunk, lotzen gattunk



Hariketan ari

Harik eta behar den artean ari

artean arin



Gattunk, Gattunk... (berriz)

lokarri listari sokari hari

Gattunk, Gattunk... (berriz)



Hari artean, hari tia hari tira

Hari artean, ahalari tiraka

Hari artean, hariari tiraka ari

Hari artean, hari ariketa

Hari artean, hari ariketa

Hari artean, hari tira hari tira

Ahalari tiraka, hariketa hariketa



Hari gattunk, ari gattunk ttunk

Hari gattunk bagattunk ari

Hari gattunk ari gattunk

Bagattunk hari

hari gattunk gattunk



Lotu josi heldu

Bai bagattunk

ba ote goaz goaztean



Zitxatoztilakoan, genitxatoztinan,

gatoztin bai bagattunk

ba ote goaz

Zitxatoztilakoan, genitxatoztinan,

goaztean