---
id: tx-3195
izenburua: Nire Aitak Amari
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/4O-E1ws8KQ4
---

Nire aitak amari
gona gorria ekarri;
berriz ere maiteko dio
Nire amak aitari.
Gure aitak amari
gona gorria ekarri.

Gona gorri gorria
zazpi jostunek josia;
berriz ere maiteko dio
gure amak aitari.
Nire aitak amari
gona gorria ekarri.

Gure amak aitari
fraka berriak ekarri;
berriz ere maiteko dio
Nire aitak amari.
Nire  amak aitari
fraka berriak ekarri.

Fraka berri berriak
zazpi jostunek josiak;
berriz ere maiteko dio
gure aitak amari.
Nire amak aitari
fraka berriak ekarri.