---
id: tx-1795
izenburua: Argiaren Hiria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/i99_Zi3-bnY
---

Larogeitamaikaren martxoa huen
niretzako hila bat miragarri
nik neuk bertan ezagutu bait nuen
bihotzaren argi betegarri.

Bost egun bakarrik izan ziren
argiaren hiria ikusteko
nahiko eta soberan izan nituen
halako gauzez maitemintzeko.

Bainan eskuak orain lotuak ditut
arimaren nahia asetzeko,
ezinezkoa den altxorra nahi dut
berandu da atzera bueltatzeko.

Gogoratzen hasten naizen guztietan
dorre izugarri bat ikusten dut
begien malkoz dago bustia
aire ziztu batekin lehortuz.

Bidai luze bat dut orain buruan
gehiegi ez ote da niretzako
beranduegi bai izan daiteke
amets zahar bat orain berpizteko.