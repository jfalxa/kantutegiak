---
id: tx-3234
izenburua: Nazkaturik -Takolo, Pirritx Eta Porrotx-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XSjHifCTm4M
---

Nazkaturik nago
mila lanekin
gelatik irten da segi
ingelesa, tronpeta gero baleta
niretzat gehiegi da hori.

Jolastu! gure aukera.
Jolastu! Denok batera.
Jolastu! Goazen batera.

Ikas makinarik
ez dut izan nahi ez! ez!
Haurra izan nahi dut.

Haurra
Haurra izan nahi dut
izan behar dut
Beharra dut.