---
id: tx-2837
izenburua: Sarako Iheslariaren Kantua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Vz-IT-JnOi8
---

SARAKO IHESLIARREN KANTUA
1/ «Nora zoazte hor gaindi, ardi nahasiak?»
«Espainiara goazi, doi-doia biziak.
launa, ahantz bekitzu guk egin gaizkiak,
gosez hil ez gaitezen mendian guziak.»
2/ Emazteak ikaran sen arra bilatzen,
gizonak emaztea orori galdatzen.
«Zer egin zare, ama? Non da ene aita?»
Horra urragarri den haur gaixoen pleinta.
3/ Etxelarren heldu-ta, oi, gure lastima!
Lurrean sartu zuken nork be re arima.
Nihork ez du leihorrik, ez norat arrima,
zenbat baikira bizi, hainbertze bitima!
4/ Koko beltzak ez gaitu segur urrikari,
xixpaz eskaintzen dio frantses gizonari.
Eskualdunak bakarrik demagu janari,
ongi eginaz, launa, zuk emozu sari.
5/ Gure galgarri haizen, Martin Dulunia,
bizirik jan darokuk bular-haragia!
Ator eta ikusak gure agonia,
hire baitara launak sar dezan argia.

6/ Zeruetako Jauna, barka, barka guri,
eta guri bezala egizu orori.
Barka diozute zure etsaieri,
halaber nahi dugu guk egin gureri.
7/ Bainan bihur gaitzatzu gure elizara,
ikus dezagun orok, ikus gure Sara;
begiz ikus detzagun batzuk haurrideak,
zenbaitek burasoak et'orok etxeak.
8/ Ez bazaitzu komeni mirakulu hori,
indar emozu, otoi, gure bihotzari,
zure nahi saindura dadien sumeti,
zure manamenduak iduk detzan beti.