---
id: tx-1104
izenburua: Oihal Laranjak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/FTfh-EbxxzE
---

Udazkeneko arratsalde hotz honetan
Orbel laranjaz estali da kalea
Zu ezagutzeko aukera ez izan arren
Barruak behartzen nau harria izatera

Arima, hezurra ta azala horma hontan
Begirada bat guzti hau ulertzeko ulertzeko
Harresi hau eraikia izan dela
Beste harresi guztiak betiko botatzeko

BADOA ONTZIA OIHAL LARANJAK HAIZEZ BETETA 
BIRIKA GUZTI HAUEK HAUSPO DIRELA
ETORKIZUN OPARO BATEN ADE GAUR BORROKATZEN EZ DUENAK
BIHAR MALKORIK ISURI EZ DEZALA

Desobedientzia du izena ontziak
Erosotasunaren kaitik da irtengo
Itsaso zabala gure aurrean izanda
Ez gara putzu txiki honekin konformatuko

Soldatapeko marrazoak sartzen dira
Aurpegia estalita indarrean
Izua ardatz duen sistema hondoratu
Antolatutako arrain txikiek handia jan