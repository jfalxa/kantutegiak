---
id: tx-2761
izenburua: Gure Zortea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/mqy4EMbvxQQ
---

Hori ikusiz ama Euska/di
nigar hauen ez da lotu
nigar hauen ez da lotu.
Zazpi probintzi zazpiak anai bat
besteari sosalde
baina goiz batez haundi
haundiek muga bat duten ezartzen
eta geroztik asko ta asko
seme alabek ahantziz
ama bakarrak beti dabila
ukan dugula Euskadi
ukan dugula Euskadi.
Erdal haizeak gogo bihotzak
usu nahi dituna hasi
egia gordez sakelak betez
ezkerrak karguduneri
bainan halere ama Euskadi
ez du etsitzen bat ere
badaki seme egiazkoek
dutela zin zinez maite
dutela zin zinez maite.
Abertzalea ikaran dago
mintzaira ez haiz galdu
ez daki nola hemendik goitik
beharko duen jokatu
Euskal arima arrotzek dute
hiltzeari kondenatu
amaren maitez armen bidea
behar ote dugu hartu
behar ote dugu hartu.
Pasionean iragan ordu
etorriko da eguna...
Gizon-emaztek, haur ala gazteek
igurikatzen duguna;
Euskal Herria ez da ez hilen
bainan Euskadi sortuko,
gero sekulan nehork ez deus ez
ez baitu hiltzen ahalko,
ez baitu hiltzen ahalko.
gero sekulan nehork ez deus ez
ez baitu hil/tzen a/hal/ko,
ez baitu hiltzen ahalko.