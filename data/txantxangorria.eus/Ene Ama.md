---
id: tx-284
izenburua: Ene Ama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jwOzo_uvBuM
---

"Ene ama", Rigoberta Bandiniren "Ay mamá" abestiaren cover euskaratua, KRUSAK talde bilbotarraren eskutik eta Xabier Iriarteren laguntzarekin. #gazteacover

KRUSAK: Irama Sagardi y Amets Rodrigo
Producción, mezcla y edición de vídeo: Xabier Iriarte

Odol hustutu zara hainbeste hilabetetan
barkaidazu hasi aurretik harroa naizela,
badakizu?
egunero salda daukazu sukaldean
zugatik ez legoke gerrarik izango
entzuidazu
Ama! Ama! ama!
geldizazu hiria
bularra airera ateraz
Delacroix estilora
Ama! Ama! Ama!
Guztion ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma
Guztion ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma

Zure gorputza nire inguruan harresi
negar egiteko zorian baina indartsu
Entzuidazu

Ama! Ama! ama!
geldizazu hiria
bularra airera ateraz
Delacroix estilora
Ama! Ama! Ama!
Guztion ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma
Guztion ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma
ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma
Guztion ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma-ma

Zergatik zarete gure titien beldur
munduaren edertasuna zor diegu
Entzuidazu
Lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo
Lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo-lo
Entzuidazu
Ama! Ama! Ama!