---
id: tx-2840
izenburua: Matalaz Hiltzia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VgYbIWc_t9Q
---

Argi lenhuria hasi zaikü agertzen
Lextarreko plazan khürütxe xürian
Erregeren ordrez Matalaz dua
Trixterik gaixua guardien artian

Bi eskik khatiraz nulaxe tinkatürik
kriminelen gisa thian bizkarrian
Axko handiari büz hori zer lazkeria
Izigarrikeria ez denin ogendant

Ene Mitikile oi zer lekhü maitia
Bainian deseña lüzaz bizitzeko
Zure boztariuan zure bake handian
Zure besuetan untsa gozatzeko
Jaun kunte txar batek ürgüllü ekhintia
Sobera zeitadan popüliarentzako
Orai hil behar dit bena nahiago düt
Hola eziz biz Jaun horren esklabo.

Adios Xibero zazpi probintzietako
En'uste orotan xokhorik hobena
Orai heltürik da düdarik gabetarik
Jüjatü bezala en'azken orena
Dener parkatzen düt Jinkuarek bezala
Begira dezen ene orhitzapena
Burreuak zier orai gomendatzen niz
Egin ezazie orai zien lana.