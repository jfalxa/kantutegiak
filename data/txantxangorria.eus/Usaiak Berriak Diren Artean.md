---
id: tx-2283
izenburua: Usaiak Berriak Diren Artean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UxxvPsccjCE
---

LETRA

Udaberria ekartzen nau, 
ezeren ulua buruan dirau, 
eguzkia izpia jotzen duenean, 
usai berriak diren artean... 
Euri tantak lori, txori kantauak, 
erreka biderak antolaktua 
bailaran gora zeru bidean, 
usai berriak diren artean... 
Alboko herriko ile betz hura 
buruan hegoak behar zituenak, 
ez dakit nolatan, ezagutzean, 
usai berriak diren artean... 
Berriro bueltu nintzen oraindik, 
mendiak apaintzen hasi nintzan ni, 
uste ezean, berriz kaltean, 
usai berriak diren artean... 
Handik ametsiko, aldez geroztik, 
haren berririk jakin izanik, 
udaberri kasa maiteenean, 
usai berriak diren artean!!
.......................................................................................
Zigor Gazkez jaunaren kantu bat, bere lehendabiziko diskotik. 1996. urtean kaleratu zuen zigilupean, Mikel Azpiroz maisuaren Hammond organoa lagundurik.

Zigor Gazkez, armonika eta gitarra 
Mikel Azpiroz, Hammond organoa

Bitxia da Zigor Gazkez gaztearen kasua, arrakasta haundiz agertu zen euskal eszenan, disko eder bat ekoiztu zuen eta bapatean desagertu zen. Bere kantak entzuterakoan, Bob Dylan jaunaren oroimena belarrira erraz etortzen zaizu. Hala ere, Zigorrek badauka zeozer berezia, bere ahotsa edota gitarra jotzeko modua agian, xarmangarria suertatzen bait da Dylan gehiegi gogoko ez diotenei ere. ?Onartzen dut antzekotasunik izan dezakeela Dylanekin... Jendeak nahi duena esan dezake, aske da. Ni ere bai, nahi dudana egiteko?. Zigorrek ahosoinua zortzi urterekin hasi zen jotzen, oso gazte, eta 14 betetzean hartu zuen lehen gitarra. 1995.urteko abenduan, Azkoitia herrian, John Martyn jaunarekin jo zuen. "Martyn gazte asko ikusi zuen eta, nonbait, bera entzuteko geldituko ez ziren beldurrez, taulara igo eta "deja tocar mi primero" esan zidan. Eseak eginez zetorren eta utzi egin behar", jajaja...

Azkoitiko kontzertua amaituta Iñigo Clemente jauna gerturatu zitzaion Zigorreri. IZ zigiluko telefonoa eman eta bost hilabetera diskorako proposamen bat eskeini zioten. ?Urrezko ametsa zen. Aise iritsia, gehiegi borrokatu izan gabe?. "Zigor Gazkez" diskoak, ahosoinua eta gitarraren babesean, rock doinuko 14 kantu biltzen ditu. Aurretik ingelesez abesten bazuen ere, Zigorrek euskaraz grabatzen hasi zen bere abestiak, eta gustora geratu zen lehen lanaren emaitzaz.