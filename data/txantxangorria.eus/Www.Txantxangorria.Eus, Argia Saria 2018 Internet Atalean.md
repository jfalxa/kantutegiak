---
id: tx-2527
izenburua: Www.Txantxangorria.Eus, Argia Saria 2018 Internet Atalean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cTBydw_hf9Q
---

2018ko euskarazko komunikazio proiektu onenak saritu nahi izan ditugu, urtero legez. 
Irratiko saria Jokin Aldazabalek gidatzen duen Euskadi Irratiko Ekosfera saioarentzat izan da.
Ikus-entzunezko saria Uxue Alberdi eta Aitzol Barandiaranentzat izan da, Euskal Herriko Bertsolari Txalpelketa Nagusiari ETBn egindako jarraipenagatik.
Prentsa idatzizko saria, Harmaila aldizkariarentzat eta haren egile diren Eñaut Barandiaran, Aitor Manterola eta Iñaki Berastegirentzat izan da.
Interneteko saria Txantxangorria.eus webgunearen arduradun Gabi de La Mazarentzat izan da. 
Sustapen ekintza saria Iruñeko Udalari eman diogu. Mugimendu feministek urteak daramatzate eraso sexistak salatzen, gizartea eta erakunde publikoak interpelatzen.
Merezimenduzko saria Pirritx, Porrotx eta Marimotots pailazoei eman diegu, 30. urteurrena bete berri dutela-eta, egindako ibilbidea aitortzeko. 
Bigarren merezimenduzko sari bat ere eman dugu: Bakearen Artisauei egindako lana eskertu nahi izan diegu.

Aipatutako bertsoa: