---
id: tx-257
izenburua: Violetaren Martxa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/XIpxW6f887g
---

HAUXE DA DESPEDIDIA - 
Dibi lada,
debele dede,
dabala dada,
hauxe da despedidia.
Atzo goizean goiz jeiki nintzen
erlojuaren kolpetik.
Atzo goizean goiz jaiki nintzen
erdi-loaren trotetik.
Hau ez da martxa ederra baina
zer atera kolkopetik?
Hau ez da martxa ederra baina
ez dut hautsiko kopetik.
Atzo haizeak leun zebiltzan
eguzkia ere murritz.
Atzo haizeak leun zebiltzan
ta gaur ere hala dabiltz.
Auskalo orain zertan nengoken
kanpoan ekaitza balitz.
Auskalo orain zertan nengoken
alfer haundi bat ez banintz.
Erreka doa bidean baina
gu hain azkar ez goaz ta.
Erreka doa bidean baina
alboan daukagu aska.
Ur geldiari trago egiok
eta egarria aska.
Ur geldiari trago egiok
laster esango duk basta.
Bart berandutxo oheratu naiz
orduak kandelan errez.
Bart berandutxo oheratu naiz
egitekorik ezer ez.
Goizean jaiki ezinik eta
lo egiten dugu errez.
Goizean jaiki ezinik eta
sartzeko ere alferrez.