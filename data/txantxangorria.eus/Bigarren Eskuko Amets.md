---
id: tx-440
izenburua: Bigarren Eskuko Amets
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/KfLOQjbYwSk
---

Antzeko gauzen atzetik denok gauean
Sute historiko bat, larrialdi irteera
Etxera eramango gaituen norbait
Galdera askorik egin gabe ahal dela
Hartzazu nire poesia
Bigarren eskuko amets
Aitortu: buruarekin txortan
Zakilarekin pentsatzen
Irudikatu duzula jendea
Zure ezpainak irakurtzen nenbilen
Hartzazu nire poesia
Lerro okerretan nire gabeziak
Bi mila izen eta zure begiak
Ihintzaren ezizen
Dena hormatzen
Dena argitzen izotz beltz moduko batez
Bakardadeari izkin egiten
Non bizi garen ez baleki legez
Hartzazu nire poesia
Lerro okerretan nire gabeziak
Bi mila izen ta zure begiak
Bigarren eskuko amets