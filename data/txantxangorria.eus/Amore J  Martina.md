---
id: tx-161
izenburua: Amore J  Martina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/uArd51v5Xfc
---

2022an argitaratuko den Jaio Naiz Baina EParen barruan doan abestia.
Sahatsa jauregi (@sahatsajauregi) artistarekin egindako kolaborazioaren parte dira album honetako irudi guztiak.

Artea: Sahatsa Jauregi ( @sahatsajauregi)
Argazkiak: Ander Sagastiberri ( @universidad_carlos_segundo)
Estilismoa: Julene Gregorio ( @julenegr)
Bideoa: Mikel Barberia ( @nork_____)
              Zoe Martikorena ( @zoemartiko)
Muntaia: Javier Jorajuria ( @tobbyrecords)
Produkzioa: J Martina
                      Niko Bortolini
Grabazioa eta nahasketa: Tobby Records ( @tobbyrecords)
                                              Javier Jorajuria
                                              Nikolas Bortolini
Master: Eurdia ( @ibonlarruzea)

-----------------------------------

Kontaktua: kontaktua@jmartina.com

-----------------------------------

LETRA

Jada ez dukat gogoan nola
hasi zen guzti hau nola
entzuten diren oraindik
tiro hotsak mendietan
itsasaldeko ekaitza
desegindako ametsak
zer geratzen da orain
zer geratzen da orain

Esaidazu berriz
zerbait izango garela
hautu bat hartu aurretik
guztia utzi aurretik
guztia eman aurretik
amore eman aurretik
bizia eman zugatik

Garai baten azkenengo
hondarretan jaio gara
mendekuaren usaina
menperatu du etsaiak
kalean ikusi zaitut
bizitza baten eskean
loak hartu nahiko nuke
berriz zure besoetan

Esaidazu berriz
zerbait izango garela
hautu bat hartu aurretik
guztia utzi aurretik
guztia eman aurretik
amore eman aurretik
bizia eman zugatik