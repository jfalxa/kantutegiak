---
id: tx-2176
izenburua: Udazken Koloretan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8M42pJv5mH0
---

Benito Lertxundik Martin Irizarri eskeinitako abestia. Martin Irizar Benitoren taldeko musikari bat zen, txirrinduz zijoala auto batek aurretik eraman zuena.﻿

Udazken koloretan, 
landen lurrinak 
zeharkatuz, 
hitaz oroit 
eta higan nauk. 

Zuhaitz biluziaren 
gerizpean orpondoa, 
orbelaren hilobi, 
horiz eta gorriz 
dena lokarturik, 
dena lokarturik. 

Eskuratu hostoa 
xume bezain eder, 
hain soila bere heriotzean, 
zuhaitz guztiaren 
bizkortasunez hain betea, 
non erortze honen duintasunak 
hiri kantatzera naraman. 

Berriro zuhaitzari 
so natzaiok, kezkaturik ote?... 
joate askearen oparotasunean, 
betikotasunaren irria zirudik. 
irria zirudik. 

Gatibu naukan denborak, 
bere baitan egositako 
ametsetaz isekaz 
bailegoan. 

Udazken koloretan, 
landen lurrinak zeharkatuz, 
hitaz oroit eta higan nauk, 
hain soil hire heriotzean, 
hain xume, adiorik gabeko 
partitzean.