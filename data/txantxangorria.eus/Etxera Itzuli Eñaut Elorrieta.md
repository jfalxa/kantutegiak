---
id: tx-3174
izenburua: Etxera Itzuli Eñaut Elorrieta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/X9pqkpeI-AA
---

ETXERA ITZULI
 
altxorren mapak besapean
etxea utzi eta ondinen
abestien xerka abiatu ninduzun
izuen gordelekuetan barrena
 
sukarri urdinarre tipiak
eta oihan beltzetako xokotan
usteltzen diren xoxo habiak soilik
nizkizun bidaian eriden
 
denborak bidea ahorturik
etxera itzuli ninduzunean
berria zizun ateko zura
eta sarraila ere
 
 
© Joseba Sarrionandia
 
  
Joseba Sarrionandia / Izuen gordelekuetan barrena"