---
id: tx-1886
izenburua: Sekulabelardiko Kantaria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/7GX1zmNlYlI
---

Egun da abenduaren lehena,
bi mila eta zortzikoa,
gauza ttikien erresumatik
partitu da maisua.
Ume ixilen hondartzan edo
paradisuko ostatuan,
non entzungo ote da orain
ahotsa zilarrezkoa?
Euskaldun jendea negarrez dago
Eskiulatik Berangora,
sekulabelardiko kantari*,
zure ahotsa entzun guran...
Abestiaren etxe goienan
kandelak iziotu dira,
Atahualpak eta Etxahunek
zabaldu dute berria:
Haiekin dela, kantuan ari,
jakin dadila Donostian,
Lekeition eta Baztanen ere
Euskal Herri guztian.
* Joseba Sarrionandiak esan bezala