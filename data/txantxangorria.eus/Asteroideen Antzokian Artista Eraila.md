---
id: tx-1267
izenburua: Asteroideen Antzokian Artista Eraila
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8Oc4RroaRO8
---

Kerobia taldearen bideoklipa. Deskargatu bideoklipa eta abestiak bere web orrian. 
www.kerobia.com



Ongi iritsia naufragoei
asteroideen antzokira,
irteerak badakizue non diren:
(norberaren barne) espaziontzietan.

Eta asanbladan aztertu zuten;
jakintsuenek ebatzi ez zutena
azal barrurako unibertsoa
kuantifika zitekeen (ala ez).

Egun hura amaitu aurretik
erabakia hartu beharra izan zuten:
ihes egin ala bertan itxoin
errugabeak zirela antzezteko.

Eta ingurura begiratu nuen
ikusteko neonezko argiak
nola egunsentiratzen gintuen
Off-en esanez:
"azkenean gizaki orok
aurre egin behar dio bakarrik
azkeneko instantean
azkeneko hatsari."

Egun hura iritsi aurretik
erabakia hartu beharra izan zuten;
ihes egin ala bertan itxoin
errugabeak zirela antzezteko.

Eta larritzen hasi zen
amets eraileen asanblada hartan:
"bi aukera soilik ditut
bi aukera soilik ditut."

Erabaki bakarra hartu nuen
eszenatokira igoko nintzela
gure azken funtzioa eskeintzeko,
hil bedi artista betirako.

Erabaki bakarra hartu nuen
eszenatokitik jeitsiko nintzela
mundura zure bila ateratzeko,
hil bedi artista betirako.