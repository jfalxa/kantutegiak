---
id: tx-2910
izenburua: Ia Guriak Egin Du
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8UFtxpR3GzI
---

Ia guriak egin du,
badegu zeinek agindu. (Bis)
Ez oraindik umildu,
alkarrengana bildu.
Gerra nahi duen guzia
berari kendu bizia.

Ez naiz ni gerraren zale
baizik pakearen alde. (Bis)
Zeinek nahi duen galde,
berari tira, dale.
Bala bat sartu buruan,
aspertuko da orduan.

Gu gera hiru probintzi
lehengo legerik ez hautsi. (Bis)
Hoieri firme eutsi,
nahiz hanka bana hautsi;
jaioko dira berriak,
gu gera Euskal Herriak.

Xenpelar