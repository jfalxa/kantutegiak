---
id: tx-3367
izenburua: Txoriburuak Sokan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nuOfWpUOG1o
---

Ah! maite banizundu nik zuk bezela
baina txoriburu gara dantzan ta dantzan,
maitasunaren sokan
irri kantuz beti nabil ni
jolas dantzan oihuz
zure ingurun ni biziko naiz
beti zoriontsu
noiz esango zuk bai!
ta biok beti alai

Ah! maite banizundu nik zuk bezela
baina txoriburu gara dantzan ta dantzan
maitasunaren sokan
nahigabetuz ta bizi naiz ni
noiz maiteko na(u)zu?
zoritxarrez apurtuko da
gure zoriona
noiz esango zuk bai!
ta biok beti alai

Ah! maite banizundu nik zuk bezela
baina txoriburu gara dantzan ta dantzan,
maitasunaren sokan
sokan dantzan nabil... Ni!