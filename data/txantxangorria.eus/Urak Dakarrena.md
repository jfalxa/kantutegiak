---
id: tx-2726
izenburua: Urak Dakarrena
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Jeul-hDOCaE
---

Urak dakarrena
urak daroa
lurrak emandakoa
lurrean gelditzen da.

Eta gu hemen
beti gu hemen.

Zuhaitzak bezala lurrean
kantuak bezala haizean
muiñak bezala erroetan
ura bezala itsasoan.

Ekaitzak bota-arazten
ipar eta hego aldetik
gizonak izateko hasi behar da
gizon izaten hemen.