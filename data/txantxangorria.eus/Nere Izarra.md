---
id: tx-2683
izenburua: Nere Izarra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/udOMilAtl2w
---

Zu zera nere izarra
Zu nere eguzkia
Aingeruen artetik
Zerutik jatxia.
 
Barren kalera nua
Bertan nai det bizi,
Antxen ikusiko det
Sarritan Maiñasi.
 
Nere biotza dago
Oso penaturik,
Joan bear detalako
Laister zure ondotik.
 
Zure ondoren nabil
erdi txoraturik
nere niotz gaisoa
ezin sendaturik.
 
Zure amorio gabe,
ai nere Mañazi!
ez det mundu onetan
geiago nai bizi.
 
Goizetikan gabera
emen nago itxoten
nere izar txuria ez
da oraindik agertzen.
 
Alfer alferrik dabiltz
zozuak kantari,
ai! nere Mañazitxok
ez du nai etorri.