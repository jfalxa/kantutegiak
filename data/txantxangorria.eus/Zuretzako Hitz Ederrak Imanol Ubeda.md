---
id: tx-2986
izenburua: Zuretzako Hitz Ederrak Imanol Ubeda
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iPJQu3UZGn4
---

Bi milagarrenez
paper zuri-zurialez
hitzen bila zinez
zoritxarrez
gaurkoan ere beldurrez

Ta bere buruaz beste
egiten du eguzkiak
zuretzako hitz ederrak
utzi ditu idatziak

Bi milagarrenez
paper zurialez
hitzen bila zinez
zorionez
bihar itzultzeko minez

Ta bere buruaz beste
egiten du eguzkiak
zuretzako hitz ederrak
utzi ditu idatziak
gustora irakurriko
dizkizuke ilargiak,
baina lainoak estali
ditu ele guzia

Izen ondo berriak landatu nahi
aditzondoak ureztatu nahi
bainan ospelegia duk herria hau
eta zu haize bila

Ta bere buruaz beste
egiten du eguzkiak
zuretzako hitz ederrak
utzi ditu idatziak
historian irakurriko
dizkizuke ilargiak
baina lainoak estali
ditu ele guzia