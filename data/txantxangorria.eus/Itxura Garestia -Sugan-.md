---
id: tx-3240
izenburua: Itxura Garestia -Sugan-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Lw2n3XQsSpU
---

Noren irudia jarraitzen duzu?
Nor da eredua?
Zerk bultzatzen zaitu?
ZEin da egungo baloreen funtsa?

Dirua maixu den, esklabu garen
mailaketa dago.
Non gauden badakigu,
non egon gintezkeen jakin nahi dugu.

itxurak agintzen duen
mundu anitza
azalaren kanpoko figurek
dute hitza

kolore, jazkera
herria, lurraldea
poltsiko beteekin 
ganorazko bidea

Ikasgai ugari, ser ikasien
beharra edonork du
bekoizkeria ororen
zentzugabekeriaz ez al zara ohartu?

Moralaren sasioa
kementsu izatea, nire saiakera
guztiaren gainetik
ederrestekoa baita geure izaera