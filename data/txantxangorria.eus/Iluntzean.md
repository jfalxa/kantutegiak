---
id: tx-2200
izenburua: Iluntzean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/tdTg9nOT3Xc
---

Maixa eta Ixiarren abesti mitikoa.

Ilargia uretan murgildu da
itsas arrain txikien ondora
uraren hozkirriaren zirrara
hondar epelaren alboan.

Iluntzean uraren ertzean 
errainu baten argia,
ene maite biok jolasean
olatu zipriztin artean.

Haizeak zimurtu du itsasoa
kresalak lehortu malkoa,
maite zure zain hementxe naukazu
izar goiztiarrei begira.

Iluntzean uraren ertzean...