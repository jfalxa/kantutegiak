---
id: tx-2369
izenburua: Egin Zaidazu Bisita
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/hFjMJ6_eDbQ
---

Errenteriako Udalak “Egin zaidazu bisita” kantua sortu du, gure herriaren aniztasuna irudikatzea helburu duena. “Elkarbizitzarako herri aliantza” prozesuaren oin garrantzitsua da kantua, herri kohesioan eta ezberdinen arteko zubien eraikuntzan lagungarri izan nahi duena.

Bernardo Atxaga idazleak egin zion lehen ekarpena prozesuari, begirada eta kolore anitzak oinarri zituen manifestua sortuz. Herriko musikari eta ahotsek egin diote bigarren ekarpena prozesuari, Bernardoren hitzetan oinarrituta eta Xenpelar bertsolari zenaren bertsoak gogoan, aniztasunean elkarbizitza sustatu nahi dugu. Izan ere, bertsoaren hitzek adierazten duten gisara, “Errenterian bizi naiz eta, egin zaidazu bisita” mezuaren bidez, ongi etorria eta harrera egin nahi diogu mundu osoari erritmo biziz eta alaitasunez 9 hizkuntzatan (euskara, gaztelera, errumaniera, arabiera, italiera, ingelesa, frantsesa, galiziera eta alemanera) sortu den kantuaren bidez.

Kantua Creative Commons lizentziapean dago; hau da, libreki kopiatzeko eta hedatzeko aukera dago.

Abestiaren letra
Podemos compartir
algo más que portal
together we can live
et parler de la vie
podemos compartir
el dolor por partir
ou pouvons-nous sourire
irria dugu herri

Chaque personne est une boîte aux lettres
Jede Person hat eine Adresse
دع الحروف تطير في الشوارع
queremos olhar e nos ver
ni zuri eta zu beltz
en el tablero de ajedrez
nobody more or less

Errenterian bizi naiz eta
da la iluzia nu la frică
questo è la mia città
egin zaidazu bisita (bis)

Begirada bat nahi dut parean
eta ez ertzean lau so
ideiak argi nahi ditut baina,
ahal bada argian lauso
dardara laztan bilakatzen dut
eta bertigoa pauso
kaleari kale egin beharrean
egin kaleari auzo

Errenterian bizi naiz eta
da la iluzia nu la frică
questo è la mia città
egin zaidazu bisita (bis)

Kanta nola abestu
Podemos compartir
algo más que portal
tugeder ui kan liif
e t par ‘ler ðe la ‘βje
podemos compartir
el dolor por partir
ow pow β ons ‘no ws sow ‘ri re
irria dugu herri

xak person et un boet au let
yed person ua en adgres
de alhuruf tatir fi alshawarie
kerémos olhar i nos ver
ni zuri eta zu beltz
en el tablero de ajedrez
noubodi mogr o less

Errenterian bizi naiz eta
da la iluzia niu la a frica
kesto e la mia sita
egin zaidazu bisita (bis)

Begirada bat nahi dut parean
eta ez ertzean lau so
ideiak argi nahi ditut baina,
ahal bada argian lauso
dardara laztan bilakatzen dut
eta bertigoa pauso
kaleari kale egin beharrean
egin kaleari auzo

Errenterian bizi naiz eta
da la iluzia nu la frică
kesto e la mia sita
egin zaidazu bisita (bis)

Itzulpena
Partekatu dezakegu
portal bat baino gehiago
elkarrekin bizi gaitezke
eta bizitzaz hitz egin
Partekatu dezakegu
joan izanaren mina
edota irri egin diezaiokegu elkarri
irria dugu herri

Pertsona bakoitza buzoi bat da
Bakoitzak helbide bat du
hegan egin dezatela gutunek kalean
elkar ikusi nahi dugu eta begiratu nahi diogu
ni zuri eta zu beltz
xake-taulan
inor ez gehiago ez gutxiago

Errenterian bizi naiz eta
bai ilusioari eta ez beldurrari
hau nire herria da-eta
egin zaidazu bisita (bis)

Begirada bat nahi dut parean
eta ez ertzean lau so
ideiak argi nahi ditut baina,
ahal bada argian lauso
dardara laztan bilakatzen dut
eta bertigoa pauso
kaleari kale egin beharrean
egin kaleari auzo

Abeslariak:
Leire Martinez – La Oreja de Van Gogh (Sony Music España etxearen kortesiaz)
Jonatan Camacho & David Escudero
Mikel Markez
Lynda Chinedu Phillips
Asmae Barrouz

Abesbatzak:
Landarbaso 
Andara Mari
Zaria Koru eskola

Kantaren sortzaileak:
Jon Martin – Bertsolaria
Alaia Martin – Bertsolaria
Igor Arruabarrena – Konpositorea – Compositor