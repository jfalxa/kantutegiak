---
id: tx-1192
izenburua: Parabara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/H74S9u4t4D0
---

parabara papa paraba!

parabara pa para pa!

parabara pa pa paraba pa!

parabarabarabarabara pa!

perebere pepe perebe!

perebe pe pere pe!

perebere pe pe perebe pe!

pereberebereberebere pe!

piribiri pi pi piribi!

piribiri pi piri pi!

piribiri pi pi piribi pi!

piribiribiribiribiri pi!

poroboro po po porobo!

poroboro po poro po!

poroboro po po porobo po!

poroboroboroboroboro po!

puruburu pu pu purubu!

puruburu pu puru pu!

puruburu pu pu purubupu!

puruburuburuburuburu pu!