---
id: tx-2954
izenburua: Txaranga
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/OdI6m7dA9wo
---

Hankak mugituz, ta-pa-ta-ta!
denok txaloka, plast-plast-plast-plast-plast!
eta iepa! Altxa umorea,
zatoz gurekin jaiak ospatzera, txin pun!
Ilusioz bizi gaitezen biharamona alaia da eta!