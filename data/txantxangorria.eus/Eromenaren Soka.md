---
id: tx-1334
izenburua: Eromenaren Soka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/MJojVm1_FEM
---

Eromenaren soka gainean dantzan,
dardara eta izerdi hotza gorputzean.
Oroitzapen galduak, sentimendu antzuak,
esperantza beharra errealitatean.

Polbora usain eta berun artean,
zein da lagun, zein da etsai?
Zein da lagun, zein da etsai momentua iristean?

Odolaren lurrinak
uneoro besarkatzen zaitu.
Eguna, gaua berdin du,
mina eremuak argia beti du.

Noizbait iritsiko da azken geltokira
non bertan idatzi bat.
Zure kanoiari azken laztana…
Bala batez, dena amaitu da!

Polbora usain eta berun artean...

Noizbait iritsiko da azken geltokira...