---
id: tx-1486
izenburua: Euskaldun Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/w-FeRxi8mNU
---

A, a, euskalduna al zera?
Euskalduna bazera erabili euskera
a, a, a, erabili euskera.

E, e, Euskal Herrian zaude
euskera zabaltzeko mintzatu bildur gabe
e, e, e, mintzatu bildur gabe.

I, i, zehorrek erabaki
nongo euskalduna dan euskeraz ez badaki
i, i, i, euskeraz ez badaki.

O, o, esango det gehiago
euskerarikan gabe Euskadirik ez dago
o, o, o, Euskadirik ez dago.

U, u, danok ondo dakigu
euskera indartzeko lan egin behar dugu
u, u, u, lan egin behar dugu.

A, a, konturatzen al zera?
Mintzatzen ez baduzu galduko da euskera 
a, a, a, asto galanta zara.

E, e, ze iso ta ze arre
nungo euskalduna zu herri hizkuntzarik gabe,
e, e, e, bai astoa zu ere.

I, i, ez zabiltza egoki
zure errua bada nik ez zaitut erruki
i, i, i, astoak e(re) badaki.

O, o, esan beharra dago
ni mintzatuagatik enbido ta ordago
o, o, o, astoa lasai dago.

U, u, gure errua dugu
Aitorren hizkuntz zaharra baldin galtzen badugu
u, u, u, lau hanka baditugu.