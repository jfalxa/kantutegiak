---
id: tx-1509
izenburua: Legoiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/e9cHom-HoLU
---

Hitzak: Edorta Jimenez, Aiora Renteria
Musika: Joseba K. Iglesias, Aiora Renteria
Abotsak: Airara Renteria (Zea Mays), Naroa Gaintza (22 m2)
Bateria: Asier Basabe (Zea Mays)
Baxua: David Gonzalez (Berri txarrak)
Kitarrak: Piti Imaz (Zea Mays), Joseba K. Iglesias (OST)
Teklatuak: Jagoba Ormaetxea




Bete da ametsa
neska legoiak dabilz
orroka Lezaman
ta San Mames berrian
neskak zein mutilak
zurigorriak
Athletic gurea
gu gara.

Bihotz zurigorria
taupaka, kolpeka, beti oh oh oh
harmailetan orro
Athletic, neskena, denona.

Jagi da legoia
legoi emea da
aitu haren orroa
Aupa!!!
hamaika zelaian
ta hamaika esperoan
aitu haren orroa
Aupa!!!

Jokoak legea
ahal eta ezina
barrea, negarra
neskok, gola eta hutsa!
garaitzak ospatu
behar den legean
neskok be oso harro
gabarrean!

Bihotz zurigorria
taupaka, kolpeka, beti oh oh oh
harmailetan orro
Athletic, neskena, denona.

Jagi da legoia
aitu haren orroa: GORA!!!
jagi da legoia
legoi emea da
aitu haren orroa
jagi da legoia
legoi emea da
aitu haren orroa: AUPA!!!
Hamaika zelaian
ta hamaika esperoan
aitu haren orroa...