---
id: tx-3167
izenburua: Olentzero Eitb
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/vhtgJ-5y5YY
---

Olentzero joan zaigu
mendira lanera
intentzioarekin
ikatz egitera.
Aditu duenean
Jesus jaio dela
lasterka etorri da
berri ona ematera.
Horra! Horra!
Gure Olentzero!
Pipa hortzetan duela
eserita dago
kapoiak ere baditu
arraultzatxoekin
bihar meriendatzeko
botila ardoakin.
Olentzero buruhandia
entendimentuz jantzia
bart arratsian edan omen du
bost arruako sagia
ai urde tripahaundia