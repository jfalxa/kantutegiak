---
id: tx-174
izenburua: Porrusalda Buelta Bikoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/8MzMbE3ZNks
---

Urkiola ganeko pagoaren ondoan

Urkiola ganeko pagoaren ondoan

Aite San Antonio dago inguruan

Ai! Ai! dago inguruan

Urkiola ganeko pagoaren ondoan

 

 

Despedidea da ta ibili kontuen

Despedidea da ta ibili kontuen

Mutil bardingotseak dagoz inguruen

Ai! Ai! dagoz inguruen

Despedidea da ta ibili kontuen