---
id: tx-986
izenburua: Zure Ezpainen Itsasoan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/InerzNGK1oA
---

Ura ez da gardena, beteta dago.
Kresala ez da gazia, ez da itsasten.
Kalatxoririk ez da, kalak bakarrik
zure ezpainen itsasoan.
Zure ezpainen itsasoan kulunkan dantzan
lo hartuko nuke zure eskuen balantzan.
Ta zure paparrean, bihotza entzutean,
taupada bakoitzean moteltzen dituzu segundoak.
Denbora ez da gelditzen, hala ere, baina ez zait axola.
Ez diot ta beldurrik zure alboan zahartzeari.
Ura ez da gardena, beteta dago.
Kresala ez da gazia, ez da itsasten.
Kalatxoririk ez da, kalak bakarrik
zure ezpainen itsasoan.
Zure hortzen artean urtzen da nire harea,
eta nire labarrak zure bizkarrean.
Zure besoak dira itsasontzien eztenak
ziztadaka zure gorputzetik galtzen diren azkenak,
galdu eztenak.
Ura ez da gardena, beteta dago.
Kresala ez da gazia, ez da itsasten.
Ta izan nahi nuke irla txiki bat bertan,
atseden hartzeko mundu honetaz.
Ez naute nekatzen mareek, ezta korronteek.
Ez du inoiz ulertuko galdu eztenak zure ezpainen itsasoan.
Zure ezpainen itsasoan.
Zure ezpainen itsasoan.
Zure ezpainen itsasoan.
Zure ezpainen itsasoan.