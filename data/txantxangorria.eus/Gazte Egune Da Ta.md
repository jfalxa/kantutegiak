---
id: tx-2451
izenburua: Gazte Egune Da Ta
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/to2dV7WgjOQ
---

“Orain da gure garaia” zioen euskal herriko bakalauaren sustatzaileak... eta orain GORA ETORRI gara, benetan! BADATOR GURE LEHEN SINGELA!

Mozkorraldi bateko azken guito epelaren barrutik atera zen idea txar hau, serioegi hartu eta gazte eguneko abestia egiten amaitu dugu. Badira “euskal herriko gazte egunik handijena geuria da txo” diotenak... baina kostaldeko kresalak 14, bertara joan ta 4! Eta zuk eta guk, badakigu zein den egia absolutua 😏 Durango 1-Lekeitio 0 😎

Urriak 14an ez duzula galdu nahi Euskal Herriko gazte egunik handiena? Ba pi-pa tiketak erostera bukatu baino lehen Intxaurre, Mesoi Berria eta Kale Kantoira!

#DontBeALoserMyFriend
#EgiaAbsolutuaTxo

#GoraEtorriGara
#GlutenikGabekoMusika
#HasiDaJolgorixue
#EguneroDaGazteEgune