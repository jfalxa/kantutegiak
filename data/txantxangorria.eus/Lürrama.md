---
id: tx-2295
izenburua: Lürrama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gc0XOMioWKs
---

Hürrün aranatzen düan oihü hau, nolaz ez dügü ahal entzüten
Lürrün hau oihal bat bezala, ez da ere deüserik ahal ikusten

Eramaiten dügün bizitzeak bestena aisa beitü llabürtzen
Bizia zor dügün oparia, ondokoer da zalhe elkitzen

Itsaso bazter, bortüetan, denetan herexak dütügü üzten
Oihan eta mendian, ühaitzetan hontarzünak oro dira hüsten

Haurrer üzten dügün mündüa, ez da batere orai berdatzen
Partida ürrentü denean, ez da seküla arrapartitzen

Errepika:
Lürrama, haur ororen bularra, guregatik ari da agortzen
Lürrama, gaur hor den indarra, gureganik haria da mozten