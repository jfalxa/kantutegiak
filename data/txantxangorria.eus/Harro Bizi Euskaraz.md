---
id: tx-3373
izenburua: Harro Bizi Euskaraz
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ENURukaJWc4
---

Hona hemen, Harro Bizi Euskeraz abestia. 
Lemoako Euskera Planarekin elkarlanean egin dugun kanta honetan herriko elkarte eta taldeetako pertsonak izan dira protagonista eta abeslari. "Harrobi izin, geroa izin!" 

2013ko azaroan Grabasonic estudioan (Berango) grabatua eta nahastua. 
Musika: Larregi + Iban Agirregoikoa (pianoa). 
Hitzak: Larregi eta Lemoako Euskera Plana.
lyrics
"Hau", "hori" edo "hura" 
aldapaz beteta dago bidea 
baina ezina ekinez lortzen omen da 

Alkarren lekuko gara 
alkarri emanez bizi gara 
Berak egiten gaitu eta guk bera 

Harro bizi euskeraz 
harrobi izin, geroa izin 
egunero errotzen dan hitza maitatu, hitza arnastu 
Harro bizi euskeraz, 
harrobi izin, geroa izin 
Lemoan ereiten gabiltza 

Danok dogu zeresana 
danon esku dago zeregina 
Berbak ematen dozku hizkuntzaren giltza 

Zure ahotik atera dan doinu eder hori entzun gura dot 
Lotsa barik egizu, zainduz, maitatuz 

Harro bizi euskeraz 
harrobi izin, geroa izin 
egunero errotzen dan hitza maitatu, hitza arnastu 
Harro bizi euskeraz, 
harrobi izin, geroa izin 
Lemoan ereiten gabiltza 

Geuk dogu giltza!