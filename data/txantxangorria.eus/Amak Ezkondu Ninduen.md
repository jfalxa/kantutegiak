---
id: tx-1876
izenburua: Amak Ezkondu Ninduen
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/81pgBl0xZzI
---

Amak ezkondu ninduen 
hamabost urtekin, hamabost urtekin. 
Senarrak bazituen laurogei berekin 
eta ni neskatila gaztea 
agure zaharrakin, agure zaharrakin.

Ezkondu ta lehenengo gauean 
joan ginan ohera, joan ginan ohera. 
Senarra jarri zitzaidan atzekoz aurrera 
eta ni neskatila gaztea 
nola pasa gaua, nola pasa gaua!

Gau pasa ta hurrengo egunean 
joan ginan etxera, joan ginan etxera: 
Egun on Jainkoak deizuela aita eta ama, 
senarra eman didazute bainan 
ezertarako ez dana, ezertarako ez dana.

Neska! Hago isilikan 
aberatsa den hori, aberatsa den hori! 
Pazientziz hartu itzan urte bat edo bi, 
hori laster hilko den eta 
biziko haiz ongi, biziko haiz ongi.

Deabruak eraman dezala