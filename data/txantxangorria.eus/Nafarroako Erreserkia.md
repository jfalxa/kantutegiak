---
id: tx-3134
izenburua: Nafarroako Erreserkia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iea_WR5RGJk
---

Nafarroa, lur haundi ta azkar, beti leial, zure ospea da antzinako lege zaharra Nafarroa, gizon askatuen sorlekua, zuri nahi dizugu gaur kanta. Gaiten denok bat, denok gogo bat behin betiko iritsi dezagun aintza, bake eta maitasuna