---
id: tx-1572
izenburua: Langile Baten Seme
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/iXWREFsk9pw
---

Langile baten semea naiz ni, aita bezela langile,
eguneroko ahaleginetan soldata baten egile;
esku hutsik jaio eta esku hutsikan nabile,
puntakoetan inor etzaigu gertatu ongile.
Aita ta amak nahiko ninduten fabrika batian,
bizitzarekin burrukatzeko altzairu tartian;
bidetxiurretik noa, agian nere kaltian,
baina ez det uste nabilenikan horren apartian.
Izerdi asko ez det ixurtzen larreko pendizan,
paper tartean murgildutako zomorroa izan,
limosnatik bizi diren umerzurtz batzuen gisan
periodikuak ez dire gutaz mintzatzen Parisan.
Horretan badet bizi geraden herriaren antza,
ezintasuna gailendu eta bakardade latza;
hezurreraino sartua galtzailearen arantza,
eromen kolpez berpiztu nahirik gure esperantza.
Gitarra txar bat hartu nuen ta hemen nabil deman
euskaldun zaharrok aspalditikan
beti dugun teman; ardo mindua edanaz
Historiaren tabernan,
nahiz muturreko galanki jaso bueltan ezin eman.
Hamaika orru ta bertso arrunt nauzue botia,
ez da erreza trebe xehetzen euskaldun otia;
norbaitek esango balu hobe nukela joatia,
neri berdin zait zuek hemen ta ni hor egotia.