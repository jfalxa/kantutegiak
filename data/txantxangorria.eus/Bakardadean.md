---
id: tx-2280
izenburua: Bakardadean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/siDEkVnOUlc
---

Bakardadean,
zu ia hilotz. 
Minduta, 
gorputza ta arima.

Haien plazerra zure inpernu. 
Egia ote den galdetuz zure buruari. 

Existitzen den ere
zalantzan jartzen duzun hori. 

Zer ote zaren, besteen, jostailu. 

Eskuko bost hatzek, bat hartu durela dirudi. 

Bakardadean, 
Zu ia hilotz. 
Zure nortasuna ere 
Nabariz arrotz.

Baina, nik, sinisten dizut. 
Eta, sinistuko dizut beti.