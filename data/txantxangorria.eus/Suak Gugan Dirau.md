---
id: tx-309
izenburua: Suak Gugan Dirau
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nyDZZxIYTfs
---

(EUS) "Suak gugan dirau" laster argitaratuko den "ERRAUTSETATIK" disko berriko aurrerapen abestia da. Iruñako Sound of Sirens Studioetan Julen Urzaizen eskutik grabatua, nahastua eta masterizatua. Kaset Ekoizpenak eta Tough Ain't Enough​ Recordsek banandua. Bideoa Hodei Torres eta Oskar Gutierrezen gidaritzapean grabatua.

Beldurrak bezala, uzkurtuz azala
Somatzen dut barrenean 
hemen etzaudela
Sortu zinen enbor beretik sortuko dira besteak 
Borroka hontan iraungo duten zuhaitz ardaxka gazteak

Argi! konpromesuz eraiki!
Atzo ta gaur! zuen suak gugan dirau
Agur! Ohore!
Eskerrak! Herria Egiteagatik

Herri oso bat elkartuaz 
kate berdinai lotua
Mailaz-maila
milaka bulkada 
Izan zinetenaren isla
izango direnaren ardatz
Belaunaldiz belaunaldi ekinez goaz aurrera