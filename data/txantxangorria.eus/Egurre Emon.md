---
id: tx-560
izenburua: Egurre Emon
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/RggMg42OBD0
---

#MusikakSalbatukoGaitu #gatibu #EgurreEmon


" Egurre emon kantak MUSIKAK SALBATUKO GAITU diskoaren mezuetako bat islatzen du. Egoerarik makurrenetan, egoerarik aldrebesenetan ere, ez etsitzeko mezua da, bizitzarako jarreratzat har daitekeena, eta batez ere, musikagintzan, euskarazko kantagintzan, rock and rollean ari diren ofizikideei egindako proposamena da, segi dezaten kantuan, sortzen."

Letra:
ATIEK ZABALDU EIN DIRE
EGUZKI BERRI BET KALIEN
GU BARIK HEMEN EZ DAU EZER
JARRI GAITTEZ MARTXAN
HASI ZAITTEZ DANTZAN

ETA EMON, BIZIXERI EGURRE EMON

BIZITZA GOGORRA DAN ARREN
GU GOGORRAGOAK GARA
HAU EZ DA LARROSA BIDE BAT
AGINEK HEMEN ESTUTU EIN BIHER DIRE

ETA EMON, BIZITZERI EGURRE EMON
EURRERA BEITTU ETA EMON
BIZIXERI EGURRE EMON

EZ DEKOGU GALTZEKO EZER
BELDUR TA LOTSAK BAKARRIK
ROCK AND ROLL HAU ENTZUTEN BADOZU
BIZIRIK ZAGOZ ZU
TA BIZI EIN BIHOZU

ETA EMON, BIZIXERI EGURRE EMON
EURRERA BEITTU ETA EMON
BIZIXERI EGURRE EMON
ETA EMON BEHIN DA BERRIZ EGURRE EMON
EURRERA BEITTU ETA EMON
AHAZTU BARIK EGURRE EMON