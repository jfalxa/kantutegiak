---
id: tx-3081
izenburua: I'M Irish In Euskalherria Barry Manley
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/YYZcNDMOtAk
---

I'm Irish in Euskalherria
Donostira etorri nintzen, gazte eta inozente
Urteak pasa dira, daukat maila dexente
Kanpotik ikusita,ez da batere erreza
Baina  zailatasuna haundiena
Da bakoitzak duena
I'm Irish in Euskalherria (x2)
Ez naiz hemengoa, ez naiz hemengoa
Baina ikasteko prest

Garagardo bat eskatu, ez dit ondo ulertzen
Háblame en cristiano ez zait asko gustatzen
Halako jarrera halako zensura
Edo agian ikusten dit
Apaisaren itxura

Ruperen textua maialenaren bertsoa
Sarrionaundia Benito eta Mikel Laboa
Hizkuntza da altxorra
Gure ondasun guztia
Zaindu maitatu eta sentitu
Da gure betebeharra
I'm Irish in Euskalherria
Ez naiz hemengoa, ez naiz hemengoa
Baina ikasteko prest