---
id: tx-2244
izenburua: Esperantzara Kondenatua
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/m7x-KP_bbCE
---

Garaipenik eta galtzekorik gabe
esperantzara kondenatua
noiztik da negarrik egiten ez dudala
Iparra erakusten det behatzaz
halako ergelkeria ispiluen museoa
edo bestelako desesperazioekin
ze, bost inporta zeru infernuak
dana da zernahi gosea det orain
garaipenik eta galtzekorik gabe
esperantzara kondenatua
esku eta begiz kanpoko bakardadea
existitzen ez den herio bateko morroi
txakur bat bezala arnasestuka
gustura nengoen hantxe eta eder
…eta ez det sinisten…