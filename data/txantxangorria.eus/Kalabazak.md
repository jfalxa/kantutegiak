---
id: tx-2619
izenburua: Kalabazak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/xzuNQN6o3ho
---

Nere laranja erdia denez 
ezin det burutik kendu errex 
bihotzeko zu, maite nazazu 
mesedez, mesedez!

Esperantza sartuta zainetan 
etorriko da azken finean 
bularra bistan, nere konkistan 
zaldian gainean. 

Ta hala bazan, edo ez bazan
sartu dadila betiko kalabazan!
Ta hala bazan, edo ez bazan
kalabazan!

Ez dago printze urdin ederrik,
muxuan zain, egon naiz alperrik 
ta nazkatu naiz, nagonez garaiz 
emango dízut nik!
 
Deldarazio eszenan horra, 
ezezkoa, izan da gogorra 
malkotan urtuz, gazi bihurtuz 
azukre koxkorra.

Ta hala bazan, edo ez bazan
sartu dadila betiko kalabazan!
Ta hala bazan, edo ez bazan
kalabazan!