---
id: tx-2335
izenburua: Nire Panpin Panpoxa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3BwPU6H8FWE
---

Nire panpin panpoxa
ezpainak larrosa
maitia, laztana,
bihotza, kuttuna
esaten didana.

Nire panpin panpoxa
maitasun oskotsa,
begi eder hoiek
begiratzen naute
zorabioaz.

Nire panpin panpoxa
guztiz preziosa
irrifar ederrez
tristurak arinduz
diharduena.

Nire panpin panpoxa
ezpainak larrosa
maitia, laztana,
bihotza, kuttuna
esaten didana.