---
id: tx-1786
izenburua: Berreterretxe Kanthoria
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/9QKrx361z8c
---

Haltzak ez dü bihotzik,
Ez gaztanberak hezürrik.
Ez nian uste erraiten ziela aitunen semek gezurrik.
Andozeko ibarra,
Ala zer ibar lüzia!
Hiruretan ebaki zaitan armarik gabe bihotza.
Bereterretxek oheti
Neskatuari eztiki:
«Abil, eta so egin ezan gizonik denez ageri».
Neskatuak ber'hala,
Ikusi zian bezala
Hiru dozena bazabiltzala borta batetik bestila.
Bereterretxek leihoti
Jaun Kuntiari goraintzi:
Ehun behi bazereitzola, beren zezena ondoti.
Jaun Kuntiak ber'hala,
Traidore batek bezala:
«Bereterrex, haigü bortala: ützüliren hiz berhala».
«Ama, indazüt atorra
Mentüraz sekulakoa!
Bizi denak oroit ükenen dü Bazko gai-erdi ondua!
Heltü nintzan Ligira,
Buneta erori lürrera,
Buneta erori lürrera eta eskurik ezin behera.
Heltü nintzan Ezpeldoira,
Han haritx bati esteki,
Han haritx bati esteki eta bizia zeitan idoki.
Marisantzen lasterra
Bost mendietan behera!
Bi belainez herrestan sartü da Lakarri-Büstanobila.
«Büstanobi gaztia,
Ene anaie maitia,
Hitzaz onik ez balinbada, ene semea joan da».
«Arreba, hago ixilik!
Ez otoi egin nigarrik
Hire semea bizi bada, Mauliala dün joanik.
Marisantzen lasterra
Jaun Kuntiaren bortala!
«Ai, ei, eta, jauna, nun düzie ene seme galanta?»
Hik bahiena semerik
Bereterretxez besterik?
Ezpeldoi altian dün hilik; habil, eraikan bizirik...»
Ezpeldoiko jenteak,
Ala sendimentü gabeak!
Hila hain hüllan üken-eta deüsere ez zakienak!
Ezpeldoiko alaba
Margarita deitzen da:
Bereterretxen odoletik ahürka biltzen ari da.
Ezpeldoiko bukata,
Ala bukata ederra!
Bereterretxen atorretarik hirur dozena ümen da