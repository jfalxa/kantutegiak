---
id: tx-276
izenburua: Burdin Rockanrolla
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/6aj9ZlojWuc
---

Abuztuaren 30 eta 31 egunetan Pottoko Studio-n grabatutako "Burdin Rockanrolla" abestia. 

Eskerrik asko, Fredi Pelaez (Pottoko Studio), Danel grabaketa lanetan ibiltzeagatik, Patxi, "Arraio", "Browstons",  gertuko lagunei, familiari eta zuri proiektu hau jarraitzeagatik!

LETRA:

Ezpata baten zarata
Mailuaren kolpeak, burrunba dakar
Izerdipean, berotasuna

Txinpartak aurpegian
Jantzi luze batez, estalia
burnia gori-gori degu jarrita 

Errementariaren etxean, 
Zotza burruntzi
Ta zurean zer?

Mendi tontorretik jeisten ari Ezpata baten zarata
Mailuaren kolpeak, burrunba dakar
Izerdipean, berotasuna

Txinpartak aurpegian
Jantzi luze batez, estalia
burnia gori-gori degu jarrita 

Errementariaren etxean, 
Zotza burruntzi
Ta zurean zer?

Mendi tontorretik jeisten ari da
Uraren bidea errotan biraka
Burdinaren Haranean

Jadanik herdoilduta
Txondorraren keak beldurtu du piztia
Txingarrak prest dira!

Errementariaren etxean, 
Zotza burruntzi
Ta zurean zer?

Errementariaren etxean,
SUA, SUA
Ta zurean zer?