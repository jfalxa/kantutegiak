---
id: tx-538
izenburua: Zer Geratuko Da Azkenean
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/GAzR1E7Z-8w
---

Kafesne hotza
Aparraren argazkia egiten hoztua, hoztua
Ta zure irrifarra
Pantailari begira betirako galdua, ahaztuta

Zer geratuko da azkenean?
Zenbat hotz kabitzen zaigu neguberrian
Mezu puta bat hitz arros artean dantzan
Zer geratuko da azkenean?

Oroitzapenak
Denak iruditan gordetzeko erregaldiaren jabetara
Zure begien kolorearekin oroitzeko
Beheraka begiratu behar

Zer geratuko da azkenean?
Zenbat hotz kabitzen zaigu neguberrian
Mezu puta bat hitz arros artean dantzan
Zer geratuko da azkenean?

Zer geratuko da azkenean?
Zenbat hotz kabitzen zaigu neguberrian
Mezu puta bat hitz arros artean dantzan
Zer geratuko da azkenean?

Zer geratuko da azkenean?
Zenbat hotz kabitzen zaigu neguberrian
Mezu puta bat hitz arros artean dantzan
Zer geratuko da azkenean?