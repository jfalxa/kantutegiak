---
id: tx-321
izenburua: Bedatsian Guk
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/AsJpuCYkF7g
---

Bedatsian dirade zühainak loratzen
Udan ekhiarekin bazterrak berotzen
Larrazkenian aldiz egünak laburtzen
Negian haize hotza hezürretan sartzen.

Ez gal gure odol beroa
Bizi bedi gure Xiberoa.

Aspaldian bizi da gure popülia
Zazpi probintzietan oso berexia
Kantore zahar eta jauzi emailia
Xoriñua bezala txülüla joilia.

Jendik erraiten die mündian gisala
Hau bezalako lekü ejerrik ez dela
Bena gure xokua listen ari dela
Eta segur hiltzeko pündian girela.

Tristeziaz besterik behar dügü heben
Bizitzeko lotsarik ez dezagün ükhen
Oi Xibero maitia ez zira galdüren
Zure sernek zütie bihar salbatüren.