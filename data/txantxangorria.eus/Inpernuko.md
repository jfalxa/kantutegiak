---
id: tx-2887
izenburua: Inpernuko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qzQM9tn9xGs
---

INPERNUKO HAUSPO HORI
ZERUTAKO ZORION
GERNIKAKO SUTEARI 
IHES EGIN ZENION
USTEAK DIRA USTEL
OROITZAPENAK ERE
OROITZAPENAK ERE
LAPURTZEN BADITTUZTE

BEHATZAK ZURE GAINEAN 
PAUSATUA ZENEKI
SENTITZEN NUEN ZIRRARA
ZER DEN BAZENEKI
KONTRAKOA EMATEN DU
BAINA HEZKUNTZA BATEK
BAINA HEZKUNTZA BATEK
BEREIZTU GINTUEN GU

MALDAN BEHERA MALDAN GORA
DENBORAREN BIRA
NON ZINEN ETA NON ZAUDEN
GALDERA BERAK DIRA
BADAKIZU HORRELA 
NIRE BIHOTZA DIDA
NIRE BIHOTZA DIDA
ERAMAN ZENUELA

GAUARENTZAKO AMETSAK
EGIN DIREN BEZALA
JAKIZU EGUNEZ ERE
AMESTEN ZATTUDALA
ORAIN NIGARREZ NAGO
TA MALKO BAKOITZEKO
TA MALKO BAKOITZEKO
MAITE ZAITTUT GEHIAGO