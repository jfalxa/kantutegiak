---
id: tx-2863
izenburua: Arrantzalearen Emaztea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/qkgqyqDoafQ
---

Oihan puntako ertzean amak itsasora
Ditu begiak zorrotz: senarra badoa.
Altzoan du haurtxoo bat hotzez suminduta,
Bestea alboan zutik gonari helduta.

Emazte et’ haurrak utzi behar
Etxetik urrun itsas ohantzean
Neke du bainan pozik doaa
Handik baitaakar haurren haztekoa.

Aita arrantzale zuen aitona ere bai;
lehiorretik urruti ez da bizi lasai.
Itsasoz dabil beti arrisku artean
Bainan gogoa nun du? Bereen etxean.

Kezkaz bizi da emaztea,
Noiz itzuliko ai senar maitea
Itzuli dira gehienak baai
Bainan betiiko galdu dira zenbait..