---
id: tx-2688
izenburua: Gasolina
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kehDs0lJOBo
---

Taldea: Marianitoz Blai
Sortze-lehorte luze baten ostean, bonbilak pizten hasi dira. Hemen aletxo bat: "Gasolina" abesti berria. 2017ko urtarrilaren 15ean Alakrania Estudioetan grabatuta, Arkaitz Mirandaren eskutik. Irudiak 2016ko abenduaren

Zama horrek baditu milaka harri
Baina ez etsi nekeari
Bideak zabaltzen dira ezerezetik
Beste batzuk, ordez, itxi
Normaltasunaren pozoia irentsi
Eta gero botaka egin
Gasolina gehio jaurti suari
Nire lagun maite hori

Puskatuko duzu ametsen hautsontzi zikin hori
Ta aurpegi horrek ez du izango preziorik
Galduko zara ongi ezagutzen dituzun kaletatik
Begirada altxatuz, behingoz beti!