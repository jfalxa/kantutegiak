---
id: tx-822
izenburua: Agur Bretainia
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/P6ZPztG5ALE
---

AGUR BRETAINIA

 
Agur Bretainia,
Erreinu zeharra,
Aberri haurride,
Gu bezala preso!
Agur Bretainia,
Zure zorigaitza,
Lohi beltzarekin
Zaitzu saminduko!

         Zure nortasuna Bretoin izaitea
        Dirudun haundiek badakite zerbait...
        Bainan hoien joko zikina
        Lehertzen da nunbait!...

                Pozoindatu behar dute dena!
                Bihar zer izanen da gizona?

         Itsasotik biziz, itsasoa heziz
        Gorputz arimetan zaituzte zafratu
        Bainan hoien joko zikina
        Lehertzen da noizbait!...

       1978 ekoa. Telebixtan ikusi nuen Bretainiako uretan Amoco Cadiz itsas-ontzi famatuaren lehertzea, itsas guzia bere petrolaz hondatzen zuela. Hango arrantzale Bretainiarreri pentsatu nuen eta Bretoin doñu ederreri hurbilduz bezala, aire edo doñu minorrean, prestatu Kanta Bi ikuskarrirako.