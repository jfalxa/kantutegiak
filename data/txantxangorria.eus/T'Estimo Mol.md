---
id: tx-1195
izenburua: T'Estimo Mol
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zzopaT-s4D0
---

De colors són els meus somnis
tot i que ja m'he llevat
i la son no ve a veure'm
quan la Lluna s'ha aixecat
salto i salto sobre els nuvols
sembla que pugui volar
el meu cor fa pampallugues
i no deixa de ballar
Quan posem els peus enlaire
quan posem el cap per avall
quan et crido a la muntanya
i l'eco em va contestan
quan et faig amb margarides
la polsera de colors
vull mirar-te als ulls i dir-te
que el que sento és això
T'estimo molt, jo a tu t'estimo molt
si estas amb mi desapereix tot el que és trist
un dia gris el pintes de colors
fins a la Lluna i tornar t'estimo jo
Els meus ulls són com bombetes
els meus llavis riuen fort
no puc amagar el que sento
quan em mires tan d'aprop
ara ja no se que em passa
el cos m'esta tremolant
el meu cor fa pampallugues
i no deixa de ballar
Quan s'uneixen els colors
per fer l'arc de sant martí
quan escrius amb el teu dit
el meu nom en el mirall
li dono canya a la radio
i escolto aquesta canço
vull mirar-te als ulls i dir-te
que el que sento és això
T'estimo molt, jo a tu t'etimo molt
si estas amb mi desapereix tot el que és trist
un dia gris el pintes de colors
fins a la Lluna i tornar t'estimo jo
Maite Zaitut, Maite Maite Zaitut
si estas amb mi desapereix tot el que és trist
un dia gris el pintes de colors
fins a la Lluna i tornar t'estimo jo14/11/13 letra deT'estimo molt deLax'N'Busto - MUSICA.COM

T'estimo molt, jo a tu t'etimo molt
si estas amb mi desapereix tot el que és trist
un dia gris el pintes de colors
fins a la Lluna i tornar t'estimo jo
fins a la Lluna i tornar t'estimo jo
fins a la Lluna i tornar t'estimo jo
Maite Zaitut