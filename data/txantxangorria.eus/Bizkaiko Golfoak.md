---
id: tx-3404
izenburua: Bizkaiko Golfoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/gOUt72TyLWQ
---

Bizkaiko golfoak, Bizkaiko golfoak
Bizkaiko golfoak, Bizkaiko golfoak menperatuezinak

Kresalak hezi gintuen
Trobikako Matxinen ikasle
Ron-ez bustitako hondartzetan
Bob Marley Armintzan brankadaka
galerna Portu Zaharrean
Bermioko talaieruari
gustetan jako San Eneperi, larregi

Bizkaiko golfoak, Bizkaiko golfoak
Bizkaiko golfoak...

Mundakan jaun zuriari
Bila joan jakoz lamin bi
Elantxoben plaza da dantzari
Txo! Ziaboga autobusari
Zuluan irlako akar baltzak
Antzar bezperan testigantzak
Itsas zakarra dauen garaian
Jai Braian

Bizkaiko golfoak...menperatuezinak!