---
id: tx-2990
izenburua: Bihotzean Zaitugu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/t2kh4nnq3S0
---

Hemen ez gaude guztiok
Jendea falta zaigu
Horregatik haientzako
Abesti hau egin dugu
Ezberdin pentsatzeagatik
Kartzelara joan zinen
Baina arazo guztiei
Aurre egin zenien
Hemendik hurrun zauden arren
Bihotzean zaitugu
Zu askatzeagatik
Borroka egingo dugu
Pentsa kanpoan guztiak
Zain dauzkazula
Ta kalea gutxi barru
Zapalduko duzula

PRESOAK!!
KALERA!!

GUZTIAK!!
ETXERA!!

Lasai egon zaitez
eta ez makurtu burua
ukabila itxita
altxa zazu eskua
Pentsa kanpoan guztiak
Zain dauzkazula
Ta kalea gutxi barru
Zapalduko duzula

PRESOAK!!
KALERA!!

GUZTIAK!!
ETXERA!!