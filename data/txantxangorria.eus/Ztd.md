---
id: tx-2586
izenburua: Ztd
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/kxj5CmDdVQ0
---

Hau duzu Zazkel Tribu Dantza 
herri aspertu honentzako gure errebantxa 
Hau duzu Zazkel Tribu Dantza 
herri aspertu honentzako gure errebantxa 

Entzun duzu Le Freak irratian 
eztanda bat izan da hirian 
afrobeat ta disko musikaz 
jendea 
dantzan da irrikaz 

Trikitronpetak behingoz suntsitu 
fusil ta aizkorak horman ezkutatu 
ei! zu! hainbeste plastikozko ska 
erritmo hori jasaterik ez da 

Hiriko auzo ilunetan 
iraultza bat hasi da benetan 
afrobeat daukate helburu 
musika bonba detonatu 
4, 3, 2, 1 

Trikitronpetak behingoz suntsitu 
fusil ta aizkorak horman ezkutatu 
ei! zu! hainbeste plastikozko ska 
erritmo hori jasaterik ez da 

Badator hemen da 
zirrara urruna 
gero eta ozenago 
desio iluna 
izerditan labain 
hau dantza lizuna 
xamur besarkatu 
umel eta biguna 
narrasti hurbilduz 
sugeen antzera 
emeki-emeki 
gerrian behera 

Badator hemen da 
desio iluna 
ezin ihes egin 
min gozo hau zer da 
Zazkel Tribu Dantza