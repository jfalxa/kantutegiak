---
id: tx-2421
izenburua: Ekar Ditzagun Gogora
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/zw7yJEO7Au8
---

Gehiegi izan da denbora
gehiegi sufrimendua
gehiegi itxi beharreko zauria
gehiegi dauden orbanak
Baina azkenik iritsi da
baina azkenik zabaldu da
garaipenaren eguna
ospatzeko irrintzia

Gugandik urrun zeudenak
etxean ditugun egun honetan
ekar ditzagun gogora
zeruan izar bat piztu zutenak

Gehiegi izan da denbora
gehiegi sufrimendua
gehiegi itxi beharreko zauria
gehiegi dauden orbanak
Baina azkenik iritsi da
baina azkenik zabaldu da
garaipenaren eguna
ospatzeko irrintzia

Gugandik urrun zeudenak
etxean ditugun egun honetan
ekar ditzagun gogora
zeruan izar bat piztu zutenak
(3 aldiz)

Preso eta iheslariak etxean
ditugun egun honetan
Ekar ditzagun gogora
zeruan izar bat piztu zutenak

Presoak Etxera!