---
id: tx-2162
izenburua: Arbola
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0phYr8XGL3I
---

Guk taldea Iparraldean sortu zen, 1968 inguru hartan, Joanes Borda eta Beñat Sarasolaren eskutik. Talde beteranoa da, klasikoa Euskal Herrian. Ahotsari eta gitarrei eman diote garrantzia, eta haien kantakera indartsua da, letra zorrotzak erabiltzen dituztelarik. Garai hartan Euskal Herriak bizi zuen egoera politikoa salatzea zuten helburu, erreibindikazioak zabaltzea, eta kantua horretarako bitartekaria zen, arte edo estetika zehatz bat lortzeko apustua baino. 

Handik gutxira Panpi Lakarrieu sartu zen taldean, eta gero Pier Paul Berzaiz eta Beñat Davant. Aurkezpen ofiziala 1974ean egin zuten ikuskizun batekin, 'Aita, seme, laborari', eta aitatutakoez aparte Erramun Martikorenak ere hartu zuen parte, eta baita Peio Etxegoien edo Joanes Bordak ere. Bost laguneko formazioa nagusitu zen eta horrekin 1980ra arte iraun zuten. Antzerki estiloko emanaldiak antolatzen zituzten kantuen arteko loturak istorioen bidez gauzatuz. 

Eneko Labeguerie, Michel patriarkaren semea taldean sartu zenerako bazituzten bi disko txiki argitaratuak. Gero LP bikoitza egin zuten, 'Laborari langile eta nagusi'. Ondoren beste lau edo bost disko atera dituzte, 'Arrozten' deitua, esaterako, 1999an. Azken urteotan Guk taldea lau lagunek osatzen dute.

Arbola gainean xoria da kantatzen,
kantatzen da eta bazterrak inarrosten,
inarrosi eta hostoa da erortzen,
euskal bihotza da bilaiz egiten.
Bilaizi ondoan han dira errabian,
xiloka beltzian handik elki nahian,
handik elki eta kantatzerat abian,
lagun zonbait bidian ikusiko agian.
Laaaaaa, lara, lara, lara,....
Goiz argi eder bat beti jaikitzen nunbait,
nunbait jeiki eta zorionean zonbait,
bizi hobeago denek dukete gogo,
bainan bakarrik ezin egin asmo.
Haur baten sortzeko sofritze beharrezko,
bizi hobetzeko legeen bihurtzeko,
nor da abiatzen lagun zonbaiten biltzen
herriaren salbatzen herioari kentzen.
Laaaaaa, lara, lara, lara,....
Gaur Euskal Herrian zuhaina da landatzen,
landatzen da eta gero fruitu ekartzen,
fruitu ekar eta auzoa da kexatzen,
iraultzailaria madarikatzen.
Jar giten ildoan igeri izerdian,
izerdian eta lepo-zinak hegian,
besoez ta botzez, hortzez eta haginez,
lagun bat zorionez ikusiko dugunez.