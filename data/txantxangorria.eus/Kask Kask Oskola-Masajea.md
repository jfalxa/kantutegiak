---
id: tx-1545
izenburua: Kask Kask Oskola-Masajea
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/sGWM0f7OuQs
---

Kask-kask oskola, kraskatu arrautza oskola;
behera badoa haren gorringoa.
Inurritxoak gora, gerritik lepora;
behera behera inurriak, beti bihurriak.
Aupa elefanteak, zuek bai mastodonteak.
Mesedez jaitsi, ez nazazue hautsi.
Txitoak goraka, txio-txio-txioka;
txitoak beheraka, aita kukurruka.
Igo sagu saguzarrak, igo sagu saguzarrak,
odola zurrupatu, odola zurrupatu;
jaitsi sagu saguzarrak, jaitsi sagu saguzarrak,
odola zurrupatu, odola zurrupatu.
Laranjen igoera, zukua atera, zukua atera.
Laranjen jaitsiera, zukua atera, zukua atera.
Hotzak akabatzen nago. Fu, fu!
Hotzak akabatzen nago. Fu, fu!
Hotzikara eta agur.