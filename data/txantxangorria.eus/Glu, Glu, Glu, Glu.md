---
id: tx-531
izenburua: Glu, Glu, Glu, Glu
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Ja37Isy8saM
---

1. Viva Rioja
Viva Naparra
Arkume onaren iztarra,
Emen guztiok anayak gera
Ustu dezagun picharra
Glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu

2. Ardo fiña ta jateko ona
Jartzen badute gaur ugari,
Gure barrenak berdindurikan
Jarriko dira guri guri.
Glu glu glu...

3. Gure sabela bete bear da
Albada gauza onarekiñ,
Dagon lekutik ¡eragiñ bapo!
¡Aupa mutillak! gogoz ekiñ.
Glu glu glu...

4. Ez ikaratu dagon artean
Jan eta edan gaur gogotik,
Ustutzen bada ekarko degu
Berriro lengoko tokitik
Glu glu glu...

5. Umoria da gauzik onena
Nai gabeak ditu astutzen,
Uju ta aja asi gaitean
Euskal doñuak kantatutzen.
Glu glu glu..."