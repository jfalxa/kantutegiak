---
id: tx-1308
izenburua: Aitxitxe Mukurrunduko
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/NU5Gqen3wfs
---

Aitxitxe mukurrunduko pipetxu zalie da baina, aitxitxe mukurrunduko pipetxu zalie da baina, pipetxuk erre dotso, sudur haundiaren puntia, amomak putz ein dotso, sudur haundiaren puntian. Aitxitxe mukurrunduko pipetxu zalie da baina, aitxitxe mukurrunduko pipetxu zalie da baina, pipetxuk erre dotso, alkondarie barria, amomak ekar deutso, alkondarie barria. Aitxitxe mukurrunduko pipetxu zalie da baina, aitxitxe mukurrunduko pipetxu zalie da baina, pipetxuk erre dotsoz, bibotaren puntiak, amomak ebei dotsoz, bibotaren puntiak. Aitxitxe mukurrunduko pipetxu zalie da baina, aitxitxe mukurrunduko pipetxu zalie da baina, pipetxu galdu ein jako, lastegiko belarretan, amomak gorde ein dotsoz, lastegiko belarretan. Aitxitxe mukurrunduko pipetxu zalie da baina, aitxitxe mukurrunduko pipetxu zalie da baina, pipetxuk erre dotsoz, lastegin dauden bedarrak, amomak amata ein dauz, lastegin dauden bedarrak, gehiao ez dotso gordeko, aitxitxari pipetxua.