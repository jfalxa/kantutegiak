---
id: tx-715
izenburua: Mariñelaren Zai
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/UFTOmBjfsa4
---

Arotzak zuen alaba bat
Zan herriko xarmangarriena
Bi gizonek nahi zuten jabe izan
Haren bihotzaz nola esan.
"Zer dun? Zer dun? Ene alaba?"
"Mariñela maite dut aita"
"Ez al dun ulertzen ezinezkoa da. Jauntxoa da hiretzat maitia."
Orduan egarriak denak ziren bat
Maitasun ukatuarentzat
Jauntxoa dator hire esku bila
Mariñela irlandara doa.
Hala izan zan ezkontz behartua,
Aberatsik ez zirenak
Ta guztiak dantzan, guztiak alai
Mail ezberdinak ahaztuaz.
"Zer dun? Zer dun? Ene alaba?"
"Mariñela maite dut aita"
"Ez al dun ulertzen ezinezkoa da, jauntxoaren emaztea zara."
Gaur egun oraindik ikus dezakegu, alaba hura leihoan.
Mariñelaren zai-zai, itsasoan du itxaropena.