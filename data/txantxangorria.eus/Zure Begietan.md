---
id: tx-2044
izenburua: Zure Begietan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/Qsg0BeJr-IE
---

Zure begietan ikusten ditut nireak
zure begietan ikusten ditut bekainak
eta nire ahoa izan daitekeena

Somatzen dut nola ari zaidan barruetan
somatzen dut handitzen putzu bat
bihotzaren parean handitzen putzu bat

Zuri begiratuta dakit nolakoa naizen.
Zuri begiratuta egiten dut nire erretratua.

Somatzen dut bidea amaitzen aurrerago
somatzen dut bidegurutzea bertan
banaduko gaituen... bidegurutzea.

Zuri begiratuta dakit nolakoa naizen.
Zuri begiratuta egiten dut nire erretratua.

Nituen ametsetatik gerturago nagoen galdetzen dut
ez naizen bidean galdu
ez dudan ilusiorik erre
ez nauen izan beharrak ito.