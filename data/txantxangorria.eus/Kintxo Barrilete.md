---
id: tx-3380
izenburua: Kintxo Barrilete
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ftILMfs1yQU
---

Musika eta hitzak: Carlos Mejia Godoy
Hitzen moldaerak: Takolo, Pirritx eta Porrotx
Musika moldaera: Xabier Zabala

Ta gora Kintxo, Kintxo Barrilete
gure adiskide aparta
ta aupa herriko neska-mutiko denak
hau da alaitasunaren zalaparta!

Ta gora Kintxo, Kintxo Barrilete
ez zaitugu, ez ahaztuko
kalean, plazan eta txoko guztietan
ozen dugu kantatuko.

Izena jarri zioten Joaquin Carmelo
baina inork ez du hala ezagutzen
maitasunez deitzen diogu:
Barrilete!
kometarekin delako jolasten.

Hamar urte besterik ez, arduratsua
beti amari ari zaio laguntzen
zapatak garbitzen ditu kalerik kale
anai-arrebak eskola izan dezaten.

Denborak arin-arin egin du aurrera
orain Kintxok ez du galtza motzik jazten
kometa kintxo txiki bati oparituta
mutil txikia ari zaigu handitzen.

Elkartasun bidean aurrera badoa
herriz herri anaitasuna zabaltzen
hamaika anai-arrebekin bat eginik
etorkizuna askatasunez sortzen.

Ta gora Kintxo, Kintxo Barrilete
gure adiskide aparta
ta aupa herriko neska-mutiko denak
hau da alaitasunaren zalaparta!

Ta gora Kintxo, Kintxo Barrilete
ez zaitugu, ez ahaztuko
kalean, plazan eta txoko guztietan
ozen dugu kantatuko.