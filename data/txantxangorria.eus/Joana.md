---
id: tx-1656
izenburua: Joana
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/BAuALPKTcXY
---

Zure arnasa bakardadean
oihu mutu baten keinu hotza;
ordurik gabeko erlojuan
bukaezina da zure mina;
orain... da... orain.

Baina negu gorriko elur malutek
zure irria izoztu arren
gure bihotzen oroimenean
amodioak bizirik dirau...

...Ta joana joan bada,
geroa etorriko da.
...Ta joana joan bada,
geroa ehundu beharko duzu
orainaren hariaz.