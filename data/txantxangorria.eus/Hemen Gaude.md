---
id: tx-2117
izenburua: Hemen Gaude
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/ol5U8MflOPo
---

agian hasieran bertan
ekibokatu ginen
munduraaa euskaldun sortzean
eta gero gero ez genuen
iraultzaren borrokatik
apartatzen apartatzen jakin
esna kantu bat abestu genion sehazka utzari
eta goiz batez El puerto-ko gartzelan esnatu ginen

Maite genituen guzengatik
erori ginen preso
baina gure maitasuna horaindik ez dago preso

Zorionez edo zorigaitzez haunditzek ekibokatu
ekarri ekarri gintuzten
beste herriko azken ipurdi hontan
bizi edo hiltzera
eta bizi estularri mirariz
gain bizi gara
eta bizitza ez da guretzat
egundo izango
lehen zen bezalakoa
arres gero

maite geniteuen gaiuzengatik erori ginen preso
baina gure maitasuna horaindik ez dago preso
maite genitueen gauzen gaitik erori ginen preso
baina gure maitasuna horaindik ez dago preso ez dago preso...

Joseba Sarrionandia