---
id: tx-3269
izenburua: Ai Zer Plazera -Patxi Saiz-
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3SD4uEIR9WQ
---

Ai zer plazera 
bost urthen bürian 
sor-lekhilat ützültzia 
ene herria, etsitü nian, 
zure berriz ikhustia 
aski sofritüz nik hanitxetan 
desir nükian hiltzia 
ezbazüntüdan hain maite ükhen, 
oi Ama Eskual-Herria. 

Hanitxen gisa 
phartitü nintzan 
etsaian güdükatzera 
gogua bero, bihotza laxü, 
eta kasik alagera 
ez bainakian, orano ordian, 
zer zen amaren beharra 
hari phentsa eta biga bostetan, 
aiher zitzaizün nigarra. 

Erresiñola 
khantari eijer 
libertatian denian 
bena trixtüran higatzez dua 
kaloia baten barnian 
gü ere ama hala guntüzün, 
tirano haien artian 
zure ametsa kontsolagarri, 
tristüra handirenian. 

Orhiko txoria 
da den bezala 
fidel Orhi bortiari 
üskaldün guziak gütüzün, 
ama zure zerbütxari 
alageraki lehen bezala, 
egin izagüzü erri 
ikhusten gütüzü othoitzetan 
zure arbola saintiari. 

Sorthü berriak, 
bere khuñatik, 
lehen elhia du "ama" 
bere ilüsione ederrian, 
ez dezazüla abandona 
zure amodiuaz egar itzozü 
bizitze huntako phenak 
begira dezan azken hatsetan, 
azken hitza zure "Ama".