---
id: tx-2505
izenburua: Maule Muffin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/0PFcdb8m9pY
---

errepika batetan lagünen artean
erritmo eringarri bat atzaman günian 
sekülan entzünik xibero xokuan 
raggamuffin estyle eko izenean

errepika..

hamar bat garagardo edanik güntian 
ontsa sartzeko gure soinüan
zinkako mürrüak hasi ziren dantzan 
estilo berri honen entzütean

 errepika..

alhorretan pusatzen den belharrarekilan
 dereiziegü jarriko bürüa aidean
kanuto bat esküan katxia bestean
müsika behatzen ari mendien artean

errepika..

xiberrotar kantore hau gure moldean 
dügü sarraziko gazte oroen bürüan 
ahotik beharriala jenteak mintzatzean
gütüe ezagüziko eüskal herri osoan