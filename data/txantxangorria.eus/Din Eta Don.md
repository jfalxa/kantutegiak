---
id: tx-394
izenburua: Din Eta Don
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/cTk9tF45TLg
---

Din Eta Don · Kurkuluxetan
Neguko Haur Kantak
Composer: Itxaso Azkona
Lyricist: Koldo Zelestino



“Bat, bi, hiru, gora behera…
Eguberri on!, Eguberri on!             
Bat, bi, hiru, gora behera…
Eguberri on!, Gora eta behera.            2 aldiz dana
Din eta Don kanta dezagun denok,
Don eta Din neguko jaietan       2 aldiz dena
Bat, bi, hiru, gora behera…
Eguberri on!, Eguberri on!
Bat, bi, hiru, gora behera…
Eguberri on! . Gora eta behera.       2 aldiz dana
Din eta Don Olentzero badator,
Don eta Din ta Mari Domingi.”
 
Bat, bi, hiru, gora behera…
Eguberri on!, Eguberri on!
Bat, bi, hiru, gora behera…
Eguberri on! . Gora eta behera.       2 aldiz dana