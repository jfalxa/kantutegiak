---
id: tx-1750
izenburua: Bec 2013 Puntuka
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/VjdCAo-XfgI
---

Gabon adiskidea
nolatan zu hemen?
ikusi beharko da 
adiskide garen
nik bai ez pentsatu det
ukatu baino lehen
berba egingo dugu 
amaitu ondoren
zergatik zatoz horren 
itxura beltzeko
ezin da txuriz joan
ikatza lortzeko
gogoa al daukazu
haurrak izutzeko
ta zu berriz guztiak
parrezka jartzeko
zeinen ridikuloa
uniforme hori
ba koka-kolak dio
nagoela ongi
falta dira zintzilik
bola bat edo bi
zure kapoitxo hoiek
ekartzea tori
kontuz nirekin daude
CIA eta NASA
eta halere sartzeko
etxera kapaza
ez dira zu ipintzen 
ez bide erraza
lagun haundiak baina
laguntza eskasa
ze tximini hestuak 
gaurko etxeetan
neu ere sartu ezinik
nabil bai benetan
gainera zu bajorik
ez da kaleetan
amaitu behar dugu
gaurbiok benetan
sartzeko beste bide 
bat ba ote dago
nik mepamsaren ordez
atean nahiago
zoaz zeu aurretikan
gero ni errazago
lan zikinak egiten
ni ohituta nago
baina ta zer umeak
esnatzen badira
baten bat ikusi dut
leihotik begira
ez gaitzala ikusi
azkar erretira
nik ipuina kontatu
ta zeuk jarri tira
noski bion artean
hobe izango da ta
ia ba sartzen geran 
elkarri tiraka
azkenik egin dugu
langintza aparta
eskerrak hi errege
eguna bada ta