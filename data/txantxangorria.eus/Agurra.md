---
id: tx-2801
izenburua: Agurra
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/jYes9sGzJ94
---

Maitia, pozez agurtzen det zure etorrera,
nere bihotza pil pil aurkitzen da zu ikustean,
zoriontsu eta maiteki,
dantzatuko det agurrik goxo eta ederrena. (bis)

Murrixkak eta lauarinak, dira bihotzez zuretzat eginak,
nere oinak nahi lukete, dantzik ederrena ongi bete,
zuretzat nere maite polit paregabea,
sutagar, maitasunez natorkizu eskeintzera. (bis)