---
id: tx-2674
izenburua: Isilaren Hotsa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/2GsMCikxUtw
---

Gabon gau hotza zuri, nola bizia eskeini 
zuk landatutako hazia gaztetasunaren loria 
hazi da, hazia den argia, zauria 
isila den hotsa 

Ez du inork arrazoirik 
burua argi dut nik 
ez da hala izan betidanik 
denbora beste irakaslerik 
ez dakit, egiaren neurririk 
badut nik, isilaren hotsa 

Isiltasunaren haur 
bihurtuko zaitut gaur 
garaia baita jabetzeko 
azalekoaz eranzteko 
kanta bat sortuaz barrenerako 
ikasteko 
isilezko hotsak 

Lagunok elkar zaindu 
aurreiritziak zendu 
gauak gaituenez aldatu 
garunak elkar joka aritu 
aizu! Isila bihurtu baino lehen pentsatu 
isila den hotzaz