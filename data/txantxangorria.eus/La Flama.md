---
id: tx-2116
izenburua: La Flama
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/drkJx0Cmbno
---

Kataluniaren eta Països Catalans herrialde osoaren independentziaren alde egindako lipdub-a, 2010eko urriaren 24an grabatuta, Vic hirian. Bideo honek lipdub jendetsuenaren errekorra dauka, 5771 pertsona, World Records Academy-ak ziurtatuta. Iniziatiba hau herri kataluniarreko jendearengan sortu da. Mundu guztiari jakinarazi nahi zaio Katalunia nazio bat dela, eta horregatik independetzia behar duela, biziraupena eta etorkizuna ziurtatzeko. Aukeratutako abestia La Flama da, Obrint Pas taldearena.