---
id: tx-1082
izenburua: Anakleto
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/nCuzYqYBSJE
---

ANAKLETO!
ANAKLETO
A! iji-aja beti aitona ijitoa
na! astoen apaintzaileetan trebeena
kle! saldu eta erosi dena merke-merke
ta! zugana gatoz, Anakleto!

A! bizarren igurtziz piztu pospoloa
na! txipli-txapla pun! sutan errez gaztainak
kle! gitarra astinduz, ai! lerele, lerele!
to! zure parekorik ez dago.

Karabana bitxia, tramankuluz josia
Anakleto zaharraren altxor txikia
korbata (e)ta bi lore
zain aitona dotore
kanturako elkartu gara gaur ere.

Gora, behera, aurrera, egin dezagun topa
amets guztiak bete daitezen opa
ahantz ditzagun penak, atera madalenak
lagunak gora (e)ta hori da dena!