---
id: tx-2976
izenburua: Etorkizuneko Aurrekari Guztiak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/S7E9Qt3H0t4
---

Hizkuntza ezberdin baten
mintzatzen zara
bai baina

Zaude zu ez zaituztela
ulertu nahi
eta ez dela besterik

Ez da besterik

*Usoek ez dakitela
esku ahurretik jaten besterik
lekuko huts izatera
behartu egin zintuztenetik

Larria da, lasai
aspaldi ohitu zinen sirena hotsekin
etorkizuneko aurrekari guztiak
dauzkazu zurekin

Kafka jaunak ez lukeela hobe idatziko
pentsatzen duzu
burdinen soinu herdoilduek esnatzean

Larria da, lasai
aspaldi ohitu zinen sirena hotsekin
etorkizuneko aurrekari guztiak
dauzkazu zurekin