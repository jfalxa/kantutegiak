---
id: tx-1741
izenburua: Odolaren Mintzoa
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/3FWHj_45HQ8
---

Guztia emateko prest!
Guztia galdu arren

Amaituko ez zen istorio batean,
dirudienez murgildu nintzenean
Zoriontasunaren erreinuan galdurik
bihurtu nizun nik nire munduaren euskarri!

Noizbait maite bazenidan, esaidazu behingoz egia!
Noiz bilakatu zen amodioa mirabe?

Egiez, mintzatzen, hasi gaitezen!

Guztia emateko prest!
Guztia galdu nuen!

Lotura sendoenak apurtzerakoan
etortzen dira ondorio larrienak
Gauero nere buruari galdetuz: zertan ari zinen, 
maite zenidala telefonoz esaten zenidan bakoitzean?

 3 x (Noizbait maite bazenidan, esaidazu behingoz egia!
        Noiz bilakatu zen amodioa mirabe?)

Noizbait maite baninduzu, bada garai begira nazazu!
Noiz izan zen moztua, gure arteko lotura?