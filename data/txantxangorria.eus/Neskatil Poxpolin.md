---
id: tx-2945
izenburua: Neskatil Poxpolin
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/O-ha3UOhJBQ
---

Oi! Neskatil pospolin, pirrin, 
ezpain gorri, begi urdin, 
polita ta krabelin, 
zu ikusi ta bihotza, 
batzutan tirrin, 
bestetan pirrin 
tirripititin.