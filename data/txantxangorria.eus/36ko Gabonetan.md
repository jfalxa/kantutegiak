---
id: tx-1760
izenburua: 36ko Gabonetan
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/EmvPHTyN1y4
---

Musika: Xabi Aburruzaga
Letra Kirmen Uribe
Ahotsa: Zigor Sagarna
Kitarra akustikoa :Aitor Antruejo
Kontrabaxua: Juyma I. Stevez
Arrabita: Xabier Zeberio
Whistle eta perkusioa: Jose Urrojola
Trikitrixa: Xabi Aburruzaga

Mila bederatzirehun eta
hogeita hamaseian
su-etena sinatu zen
Kalamuako frontean.

Eguberri bazkarian
mahai berean eseri
ziren ordura artean
elkarren aurka zeudenak,
elkarren aurka zeudenak.

Inork ezin zuen jakin
biharamonean
San Estebantxe egunez
gerra berpiztuko zela.

Haietariko ehundaka
lubakietan hilko ziren
ezustean hasitako
erasoaldi bortitzean,
erasoaldi bortitzean.

Zeri buruz egingo zuten hitz?
Euskaraz egingo zuten
Nafarroa eta Bizkaia aldeko gazteek
zertan egingo zuten haiek bat?
zertan egingo zuten bat?

Hitzok behar ditugu orain
isilaldi osteko
lehenengo hitz horiek
hitz ahulak, lotsatiak.

Jakin badakigun arren
ez zutela izan indarrik
ez zirela izan gai
gerra hura gelditzeko,
gerra hura gelditzeko.

Zeri buruz egingo zuten hitz?
Euskaraz egingo zuten
Nafarroa eta Bizkaia aldeko gazteek
zertan egingo zuten haiek bat?
zertan egingo zuten bat?