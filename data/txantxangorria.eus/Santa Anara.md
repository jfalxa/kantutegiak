---
id: tx-341
izenburua: Santa Anara
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/JoevSbjX6R8
---

RUPER ORDORIKA - SANTA ANARA
--------------------------------------------------------
Ruper Ordorika musikariaren “Amour eta toujours” disko berria irekitzen duen abestia duzue: Santa Anara
Album berezi honek osatuko du dagoeneko 20 diskatik gora heldu den bere ibilbidea. Ruper maila gorenean dabil euskal musikan.
Ezin da Ruper Ordorika laburrean definitu...

Santa Anara noala Santa Anara goiz goizik
egun ona kai aldera noa ni
Argia jalgitzen da itsasoko aldetik
Santa Anara noala Santa Anara
Habanatik menturazko bidean
zure ondora noa ni
Kresalaren usaina sekretuz bete heldu da
Hemen ametsetan bezala
mundua pausan da zein ona den sentitzea goraldika bizitza
Kresalaren usainak zure aldera naroa
Santa Anara noala Santa Anara goiz-goizik
egun ona atera da kai aldera noa ni
Santa Anara larala lara lara lala
bidean habanako ole oli ole ola
hiritik menturazko larala lara lara lala
aroan zuregana ole oli ole ola
poz-pozik Santa Anara larala lara lara lala
bidean itsasoko ole oli ole ola
aldetik krabelina larala lara lara lala
eskuan ta kolkoa ole oli ole ola
bete diru lalala lalala lala