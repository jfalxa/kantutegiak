---
id: tx-2518
izenburua: Urdiñarbek Muskildiko Maskaradari Errezibitzeko Bertsoak
kantutegia: Txantxangorria.Eus
partitura: null
midi: null
youtube: https://youtu.be/yyUeCXrw_Y8
---

Hur xendatik zide gaur
Munartino heltzen
zuen maskad dugu
egun errezebitzen
jenteek dioie dela
zer nahi agertzen
ene auzoak ez dakit
zer dioen pentsatzen.

Zamalzaina urruxa
kantinier mutiko
Khestuek zer dukie
guri urthukitzeko?
Aphez baten sartzia
emazte ezkontzeko
Jinkoak ohar duke 
nur den behesteko.

Phediku phasta zerbait
eman sukaldariak
zabaltuko dutugu
gure beharriak
plazerrekin emanen
edari bazkarriak
phedikien arabera
undar batzarriak

Haleikere nahi nuke
erran gaztearri
lothurik egon zitzaie
zien herriari
Muskildi bizirik bere
kabaret gauarri
eman zien fedea
herri auzoarri.