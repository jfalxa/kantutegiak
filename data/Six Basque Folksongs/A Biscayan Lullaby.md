---
id: ab-4641
izenburua: A Biscayan Lullaby
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004641.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004641.MID
youtube: null
---

Lua lua zaintchu laztan ori
Lua lua masusta
Aita gurea Gazteitzen da
Ama mandoan artuta
Lua Lua.

Aita gurea Gazteitzen da
Ama mandoan artuta
Aita gureak diru asko du
Ama bidian salduta
Lua Lua.