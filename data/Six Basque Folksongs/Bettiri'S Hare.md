---
id: ab-4643
izenburua: Bettiri'S Hare
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004643.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004643.MID
youtube: null
---

Bettiri Uhartekoa,
ah qu'il était bon chasseur.
Erbi bat sasi tchokoan
atcheman zuen par bonheur:
Hamabortz egun hil zela
Ah qu'il avait bon odeur
Hartarik jatekotan
Il faut avoir un bon coeur.