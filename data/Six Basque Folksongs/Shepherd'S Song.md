---
id: ab-4644
izenburua: Shepherd'S Song
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004644.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004644.MID
youtube: null
---

Goizean argi ezkorrian
jinen ardiekila
Ñoizten ñoizten entzünen düdan
maitia zure botza
Ardiak nun ützi tüzün
erradazüt egia
Nigarretan ikusten dizüt
zure begi eijerrak.