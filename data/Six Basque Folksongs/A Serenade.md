---
id: ab-4645
izenburua: A Serenade
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004645.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004645.MID
youtube: null
---

Anderea ideka dazu salako leihoa
Zurekilan har dezadan agi e'iteko loa
Koloria churi gorri zira charmegarria
Zure bularren arteko loaren gosua.

Izar arte denian goizetan izotza
Ene maite polita et zaitela lotsa
Berantcheño ginikan egin dizut utsa
Berriz ere ginen zu ezaitela lotsa.

Izar ihitzak ditu bazterrak freskatzen
Gabaz dabilan gaichoak oi nahi ihiztotzen
Ni maitiarenganik nekez naiz partitzen
Nahi bezen ardura ezbaitut ikhusten.