---
id: ab-4646
izenburua: The Sermon
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004646.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004646.MID
youtube: null
---

Konbeni diren bersu berriak
esplikatu al banintza
Aditutzia deseo duen bati
eman diot itza
Munduko baile soinu ederrek
enganaturik gabiltza
Gure arimen kaltean dator
gure munduko bizitza.

Abenduaren lendabiziko
goizango zazpietan zen
Templo santuan egondua naiz
sermon eder bat aditzen
Jesusen plaga preziotchuaz
astera noaije berritzen
Zeruko atyak lagun naizela
entendimenduz argitzen.

Kristau fidelak bidean dator
azken juezoko eguna
Bakotchak gauren begien bistan
ikusi bearko duguna
Kochapetako zelai santuan
trompeta soinu iluna
Galduak pena eramango du
justuarentzat fortuna.

Kochapetako zelai santuan askok dugu ikusiko
Jesus nola den sentenzio bat guri ematen asiko
Galduak pena eramango du onak eztu mereziko
Justuarentzat zeruan silla eternidade guziko.