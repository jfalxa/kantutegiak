---
id: ab-4642
izenburua: A Love Song
kantutegia: Six Basque Folksongs
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004642.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004642.MID
youtube: null
---

Mündian den ederrenik zelian ekia
Lürreko anderetan oi ene maitia
Begia düzü ñabarra churi gorri larrea
Ene begietako charmagarria.

Errak amodioa niganik noiz hua
Akaba erazi gabetarik otoi habilua
Eztizadala sesiorano gogua
Churit eztakidan gazterik bilhoa.

Aspaldi badizü aspaldi banabindala
Ürzo baten onduan oihaniala
Orai etchitzen dizüt ezin diro dela
Ezin ikhusten beitüt baten hegala.

Ihizlari fidela balinbazira
Nulaz erraiten düzü ezin naikezüla
Jinen düzü dembora ez ühken antsia
Bentüraz izanen gira biak algarrekila.

San Katalina ondoko igantin
Orhoitzen zira dia zer hitz artü günin
Gai hotz terrible huntan hürrüntik nizü jin
Borta idokedazüt barnera sar nadin.

Aita dizüt ohin ama sükaldin
Haien bien artetik bortala ezin jin
Fidelitatiaz egiten badüzü zin
Leihua zabaltzen deizüt plazer badüzü jin.

Goratto düzü leihua hora jiteko
Zübürik eztüt ikusten horat ezarteko
Abis hun balükezü bizkarraren hausteko
Enüzü ez hain asto Kattalin adio.

In nomine Patri eta Efili
Ala ni goradanik aphal erori
Saihets ezürretarik hautse dit hamabi
Lephoko zainetarik hogei eta bi.