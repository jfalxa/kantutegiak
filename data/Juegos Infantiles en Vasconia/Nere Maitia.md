---
id: ab-4512
izenburua: Nere Maitia
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004512.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004512.MID
youtube: null
---

Nere maitia, zein dezu maite?
Nik dedanian dedana;
nik dedanian dedana eta
zu zera nere laztana.