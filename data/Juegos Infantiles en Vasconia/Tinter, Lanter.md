---
id: ab-4589
izenburua: Tinter, Lanter
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004589.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004589.MID
youtube: null
---

Tinter, lanter.
Ai! Ekarrak adarra
einen daiat txirula.
Zertaz?
Gaztena laida politaz.

Txirula, mirula kantari.
Balinbaiz izerdi,
kris, kras, atera adi.