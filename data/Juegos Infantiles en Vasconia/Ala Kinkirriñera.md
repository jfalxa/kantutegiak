---
id: ab-4608
izenburua: Ala Kinkirriñera
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004608.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004608.MID
youtube: null
---

Ala kinkirriñera
ala samurrera,
Josepa plaza berriko,
jira adi beste aldera.