---
id: ab-4504
izenburua: Dingilin-Dango
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004504.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004504.MID
youtube: null
---

Dingilin-dango eragidazu,
laster aziko nayatzu;
baitta bere, ta lastertxuago,
bezuan ba-narabiltzu.