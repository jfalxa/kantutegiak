---
id: ab-4498
izenburua: Txalito Txalo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004498.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004498.MID
youtube: null
---

Txalito txalo
txalopin txalo,
txoritxua mispillua
gañian dago;
badago, bego,
zapatatxo berrixeri
begira dago.