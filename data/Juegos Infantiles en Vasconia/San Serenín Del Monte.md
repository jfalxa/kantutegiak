---
id: ab-4605
izenburua: San Serenín Del Monte
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004605.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004605.MID
youtube: null
---

San Serenín del monte
San Serenín del rey,
yo como soy cristiana
yo me arrodillaré.

San Serenín del monte
San Serenín cortés,
yo como soy cristiana
yo me sentaré.

San Serenín del monte
San Serenín cortés,
yo como buena cristiana
yo me tumbaré.

San Serenín del monte
San Serenín cortés,
yo como buena cristiana
yo me levantaré.