---
id: ab-4485
izenburua: Lenengotxu Ori
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004485.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004485.MID
youtube: null
---

Lenengotxu ori
punta-beatz ori,
beste guztien aldean
trokotza dok ori.

Bigarrentxu ori
punta-beatz ori,
beste guztien aldean
bizkorra dok ori.

Irugarrentxu ori
punta-beatz ori,
beste guztien aldean
luzea dok ori.

Laugarrentxu ori
punta-beatz ori,
beste gustien aldean
lerdoa dok ori.

Bostkarrentxu ori
punta-beatz ori,
beste guztien aldean
txatxarra dok ori.