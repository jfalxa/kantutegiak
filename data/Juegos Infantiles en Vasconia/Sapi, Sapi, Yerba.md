---
id: ab-4525
izenburua: Sapi, Sapi, Yerba
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004525.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004525.MID
youtube: null
---

Sapi, sapi, yerba
que canta la culebra,
sapi, sapirón
que canta el culebrón.