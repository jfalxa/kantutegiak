---
id: ab-4492
izenburua: Mixi, Marrau
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004492.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004492.MID
youtube: null
---

Mixi, marrau,
katuak yan nau
aitak il nau
amak yan nau
arreba ponpoxak
piztu nau.