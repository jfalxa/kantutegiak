---
id: ab-4543
izenburua: Patiná, Patiná
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004543.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004543.MID
youtube: null
---

Patiná, patiná,
patinaba una niña en París.
Resbaló, resbaló
y en la acera de enfrente cayó.
Y de pre, y de pre
y de premio le iban a dar.
Un vestí, un vestí,
un vestido para el Carnaval.