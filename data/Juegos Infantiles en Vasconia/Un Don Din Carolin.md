---
id: ab-4531
izenburua: Un Don Din Carolin
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004531.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004531.MID
youtube: null
---

Un don din carolin carolete,
un don din carolin carolan,
mango puede, mango puede,
con la tijera y el dedal.