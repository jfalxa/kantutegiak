---
id: ab-4510
izenburua: Arre Burriquito
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004510.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004510.MID
youtube: null
---

Arre burrikito,
ixo likanda,
Biturien dagoz
iru ollanda:
bata da koxoa,
bestea bere alangoa,
irugarrena bere
kasta malditoa.