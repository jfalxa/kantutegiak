---
id: ab-4513
izenburua: Itsasoetan
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004513.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004513.MID
youtube: null
---

Itsasoetan laño dago,
Baionako barraraño.
Txoriak bere umiak baño,
nik zu zaitut maitiago.