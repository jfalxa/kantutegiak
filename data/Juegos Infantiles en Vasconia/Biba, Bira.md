---
id: ab-4610
izenburua: Biba, Bira
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004610.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004610.MID
youtube: null
---

Bira, bira, monte, (sic) zozo balkate
Ibarrangeluan alkate,
Bilbon errejidore.
Gure auzoko Petratxu bére
atzera bira leiteke.