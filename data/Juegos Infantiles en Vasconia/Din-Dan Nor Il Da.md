---
id: ab-4503
izenburua: Din-Dan Nor Il Da?
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004503.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004503.MID
youtube: null
---

Din-dan
nor il da?
Peru zapatarija.
Zer egin dau pekatu?
Auzoko txakurra urkatu,
tella gañian sikatu
aren emazte kottadiak
amar maradi pagatu.