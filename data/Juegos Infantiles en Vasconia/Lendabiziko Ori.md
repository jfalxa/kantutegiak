---
id: ab-4486
izenburua: Lendabiziko Ori
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004486.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004486.MID
youtube: null
---

Lendabiziko ori
txoporren ori,
beste guziek baño
txoporroago dek ori.
Txingilingi txongi...

Bigarren ori
subien ori,
beste guziek baño
subiago dek ori.
Txingili txongi...

Irugarren ori
luxien ori,
beste guziek baño
luxiago dek ori.
Txingilingi txongi...

Laugarren ori
mantxoen ori,
beste guziek baño
mantxoago dek ori.
Txingilingi txongi...

Bostgarren ori
txikien ori,
beste guziek baño
txikiagoa dek ori.
Txingilingi txongi...