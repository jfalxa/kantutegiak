---
id: ab-4516
izenburua: Haurra, Eizu Lo, Lo, Lo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004516.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004516.MID
youtube: null
---

Haurra, eizu lo, lo, lo;
emanen dautzut bi goxo:
Orain bat eta gero bertzia.
Bai, bai, bai, ...
Ez, ez, ez, ...
Ikusi nuenian oi zure begiak
iduritu zitzaiztan zeruko izarrak.