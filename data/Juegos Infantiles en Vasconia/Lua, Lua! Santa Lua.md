---
id: ab-4520
izenburua: Lua, Lua! Santa Lua
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004520.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004520.MID
youtube: null
---

Lua, lua! Santa Lua!
Zeruetako Jainkua:
Aurtxo oni emanakiyozu
ordutxo beteko lua.