---
id: ab-4496
izenburua: Txalo Pintxalo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004496.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004496.MID
youtube: null
---

Txalopin txalo
txalo ta txalo...
Katuxue mispilla
ganian dago.
Ba-dago bego
ba-dago bego
zapatatxu barrijen
begira dago.
Gastelarako
ardaua ekarteko
geure umetxuak
brindo egitteko.