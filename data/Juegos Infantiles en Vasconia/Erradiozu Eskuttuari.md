---
id: ab-4491
izenburua: Erradiozu Eskuttuari
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004491.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004491.MID
youtube: null
---

Erradiozu eskuttuari,
eskuttu txiki poliñuari,
lehenik bati,
gero bertziari;
erradiozu poliñuari,
panpalakio poliñuari.