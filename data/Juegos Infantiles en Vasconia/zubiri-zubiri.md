---
id: ab-4575
izenburua: Zubiri-Zubiri
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004575.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004575.MID
youtube: null
---

- Zubiri-zubiri
nunguari-nunguari,
nungo alkate zerade?
- Prantzia'ko erregearen
seme-alabak gerade.

Onen urrena zubi ontatik
pasatzen dana
emen geldituko dala
emen geldituko dala.