---
id: ab-4482
izenburua: Bera Dago
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004482.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004482.MID
youtube: null
---

Bera dago,
bakarrrik dago;
bera dago,
bakar-bakarrik dago.