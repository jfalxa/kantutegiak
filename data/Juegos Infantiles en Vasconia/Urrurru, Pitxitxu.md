---
id: ab-4609
izenburua: Urrurru, Pitxitxu
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004609.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004609.MID
youtube: null
---

Urrurru, pitxitxu,
altza gloria, gloriatxu.
A la kinkinera,
a la samurrera,
Maria Etxebarria
jira bestaldera.