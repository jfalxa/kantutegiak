---
id: ab-4545
izenburua: Naranjas De La China
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004545.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004545.MID
youtube: null
---

Naranjas de la China, na,
China, na, China, na,
naranjas de la China, na
me dan para merendar.

Con una varita China, na
China, na, China, na,
con una varita China, na
me voy a merendar.