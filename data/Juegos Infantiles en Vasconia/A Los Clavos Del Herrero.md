---
id: ab-4569
izenburua: A Los Clavos Del Herrero
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004569.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004569.MID
youtube: null
---

A los clavos del herrero
jugaba San Millán
San Millán como era herrero
con el chiquili, chiquili, chan,
jugaba San Millán.