---
id: ab-4564
izenburua: Yop, Yop
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004564.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004564.MID
youtube: null
---

Yop, yop, yop,
yo, yo, yogur,
un yogur para beber
correr, saltar,
agacharse, levantarse y respirar.