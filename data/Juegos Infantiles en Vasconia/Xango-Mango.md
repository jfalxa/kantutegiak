---
id: ab-4480
izenburua: Xango-Mango
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004480.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004480.MID
youtube: null
---

- Xango-mango,
aurra nongo?
- Ona bada etxerako;
gaixtua ba-da kanporako.(17)

(17) Variante "Gaixtua ba-da putzurako" (Si es malo, para el
pozo).