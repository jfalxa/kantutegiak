---
id: ab-4505
izenburua: Brin-Bran Boleran
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004505.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004505.MID
youtube: null
---

Brin-bran boleran
elizako kanberan
sei edo zazpi gizon
eta denak koleran.