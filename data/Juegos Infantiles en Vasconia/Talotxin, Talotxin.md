---
id: ab-4500
izenburua: Talotxin, Talotxin
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004500.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004500.MID
youtube: null
---

Talo, talo, talotxin
gure aurrak bortz otsin.
Atoz etxera Martín
opil aundi batekin.