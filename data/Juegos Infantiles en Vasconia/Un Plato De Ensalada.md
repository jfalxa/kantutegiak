---
id: ab-4527
izenburua: Un Plato De Ensalada
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004527.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004527.MID
youtube: null
---

Un plato de ensalada comimos una vez
jugamos a las cartas, sota, caballo y rey.
Anita chinita, nay, nay, nay
sota de bastos, nay, nay, nay
sota de bastos lairón, lairón, lairón.