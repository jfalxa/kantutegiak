---
id: ab-4514
izenburua: Aingerutxo Polita
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004514.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004514.MID
youtube: null
---

- Aingerutxo polita
zeruan ze berri?
- Zeruan berri onak
orain eta beti.