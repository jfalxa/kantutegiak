---
id: ab-4606
izenburua: Cu-Cú, Cantaba La Rana
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004606.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004606.MID
youtube: null
---

Cu-cú, cantaba la rana,
cu-cú, cucú, debajo del agua,
cu-cú, cu-cú, se echó a revolcar.