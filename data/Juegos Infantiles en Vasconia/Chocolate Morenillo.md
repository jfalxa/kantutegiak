---
id: ab-4542
izenburua: Chocolate Morenillo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004542.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004542.MID
youtube: null
---

Chocolate morenillo
corre corre que te pillo
por las puertas del castillo.