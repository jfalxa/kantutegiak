---
id: ab-4563
izenburua: Trinaranjus
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004563.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004563.MID
youtube: null
---

Trina, trina, trinaranjus
sin bur, sin bur, sin burbujas
refres, refres, refrescantes.