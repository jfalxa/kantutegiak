---
id: ab-4571
izenburua: En La Calle 24
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004571.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004571.MID
youtube: null
---

En la callle, lle
veinticuatro, tro
habido
un asesinato, to
Una vieja, ja
mata un gato, to
con la punta, ta
del zapato, to.
Pobre vieja, ja
pobre gato, to
pobre punta, ta
del zapato, to.