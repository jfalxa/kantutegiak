---
id: ab-4590
izenburua: Txulubite-Malubite
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004590.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004590.MID
youtube: null
---

Txulubite-malubite
Prantziaren sartu,
Españaren at'ra (atera).
Nere txulubite gaixoa,
oxorik, bixirik atera;
oxorik, bixirik atera.

Ttittu bien; ttittu bien;
urik eztau iturrien.
Ttittu bien; ttittu bien;
urik eztau iturrien.