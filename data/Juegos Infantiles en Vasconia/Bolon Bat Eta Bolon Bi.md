---
id: ab-4517
izenburua: Bolon Bat Eta Bolon Bi
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004517.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004517.MID
youtube: null
---

Bolon bat eta bolon bi,
bolon putzura erori.
Erori bazen, erori,
ezta gerostik egarri (ageri).
Bolon bolon bolona,
gure aurraren aur ona
bolon bolon bolona.