---
id: ab-4497
izenburua: Txalopin Txalo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004497.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004497.MID
youtube: null
---

Txalopin txalo
xoxua an dago;
xoxua mizpiru
gañian dago.
Ta nai badu, bego,
ta nai badu, bego,
zapatatxo berrien
esperoan dago.