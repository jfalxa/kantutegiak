---
id: ab-4522
izenburua: Kilibi-Kalaba
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004522.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004522.MID
youtube: null
---

Kilibi-kalaba
arrosa krabelin:
attuna, amuna,
amuna, attuna.