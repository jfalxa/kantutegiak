---
id: ab-4587
izenburua: Tres-Tres, Mariandres
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004587.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004587.MID
youtube: null
---

Tres-tres, Mariandres,
atorria zarra, gonarik ez;
iru opil ostuta
niri bapez emon ez,
ez, ez ...