---
id: ab-4489
izenburua: Atxia-Motxia
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004489.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004489.MID
youtube: null
---

Atxia motxia perolipan
nire semea errotan.
Errotara ninoiala
topa neban asto bat
kendu neutsan bustan bat
ipini neutsan beste bat
errota txikinak klin klan.

Atxia motxia perolipan,
nire semea errekan.
Errekara ninoiala
topa neban erbi bat
kendu neutsan begi bat
ipini neutsan barri bat
erreka txikinak plist plas.