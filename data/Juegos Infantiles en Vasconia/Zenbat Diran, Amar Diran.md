---
id: ab-4487
izenburua: Zenbat Diran, Amar Diran
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004487.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004487.MID
youtube: null
---

Zenbat diran amar diran
xenbat diran ikusi.
Olluak kakaritzen dago,
ollarrak ari ikasten.
Txakurtxua josten eta
arratoitxua lauretan.
Txindurria barriketan
kakalardua ametsetan.
Eperra nabo,
idia dantzan.
Astua danboliña joten
onen erdi-erdian
zorria barrez itotzen.