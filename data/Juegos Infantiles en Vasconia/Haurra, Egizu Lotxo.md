---
id: ab-4515
izenburua: Haurra, Egizu Lotxo
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004515.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004515.MID
youtube: null
---

Haurra, egizu lotxo ta lotxo
nik emango dizut bi kokotxo,
bat orain eta bestea gero,
danbolintxua kalian dago.