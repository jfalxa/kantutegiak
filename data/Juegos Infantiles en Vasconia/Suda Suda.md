---
id: ab-4588
izenburua: Suda Suda
kantutegia: Juegos Infantiles En Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004588.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004588.MID
youtube: null
---

Suda suda cáscara ruda
tira coces una mula
sal, sal, sal,
para Pascua yo silbar.