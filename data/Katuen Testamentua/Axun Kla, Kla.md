---
id: ab-4852
izenburua: Axun Kla, Kla
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004852.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004852.MID
youtube: null
---

Axun, kla, kla,
axun, kla, kla,
Josepa Antoni bruji
leatza ostu dabe
Leon Pepenetik.

Leatza ostu dabe
Leon Pepenetik,
Mari mutrikuarra
begira dabil.

Ortuan perejilla
saleruan gatza,
atera platerera
jan daigun leatza.