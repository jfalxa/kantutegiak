---
id: ab-4873
izenburua: Aitite Mukuerretako
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004873.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004873.MID
youtube: null
---

Aitite mukuerretako
pipazalie,
pipeak erre dotsola
surren puntie.

Amoma koitadea
kortatik negarrez,
katuk eron dotsola
taloa dandarrez.