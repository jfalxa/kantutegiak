---
id: ab-4870
izenburua: Ponpijjola
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004870.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004870.MID
youtube: null
---

Ponpijjola ta ponpijjolla
txepetxak antzarra jan dabela,
txepetxak antzarra jan badeu
tripe zitela bete deu.
Tan parran tai...tan.

Ponpijjola ta ponpijjola
antxoa palea jan dabela,
antxoa palea jan badeu
tripe zitela bete deu.
Tan parran tai...tan.

Ponpijjola ta ponpijjola
eulijjak txepetxa jan dabela,
eulijjak txepetxa jan badeu
tripe zitela bete deu.
Tan parran tai...tan.