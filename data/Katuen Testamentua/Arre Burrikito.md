---
id: ab-4884
izenburua: Arre Burrikito
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004884.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004884.MID
youtube: null
---

Arre burrikito
asto berenoa,
okerreko bidetik
zerk ete daroa.