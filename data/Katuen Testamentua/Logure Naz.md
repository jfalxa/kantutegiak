---
id: ab-4859
izenburua: Logure Naz
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004859.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004859.MID
youtube: null
---

Logure naz ta lokalde
gure ollorrak jo dabe,
gure ollarrak jo dabe baña
ni nau ondiño logure.