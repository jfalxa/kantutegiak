---
id: ab-4893
izenburua: Aiñ Urrin Etxe Poxpola
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004893.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004893.MID
youtube: null
---

Aiñ urrin etxe poxpola
olan, olan,
zuri-gorri umea
jaiobarria,
tximinitik gora
kea dariola
olan, olan.
Gantxoan arbola
zelai-erdian,
erreka poxpola
etxe-atarian,
sutan tanboliña
gaztaña erretzen,
lapikoa gal-gal
babak egosten,
tran, la, la,...
dana txiki-txikia dona.