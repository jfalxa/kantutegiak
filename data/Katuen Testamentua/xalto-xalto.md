---
id: ab-4890
izenburua: Xalto-Xalto
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004890.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004890.MID
youtube: null
---

Xalto-xalto, mikili xalto,
iltzen dana zerureko,
ezpabere mundureko,
aitxek eta amak zoratzeko.