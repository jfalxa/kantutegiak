---
id: ab-4858
izenburua: A, E, I, O, U
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004858.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004858.MID
youtube: null
---

A, e, i, o, u,
ama merienda beogu,
arrautze bi ta kopeitsu,
aite etorri orduko dana jango gu.

A, e, i, o, u,
ama merienda beogu,
txokolate ta opiltxu
bestela eskolan ikasiko ez togu.

A, e, i, o, u,
ama merienda beogu,
baso esne ta pasteltxu
a janda be ondiño geio gurogu.