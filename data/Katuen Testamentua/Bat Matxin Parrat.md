---
id: ab-4867
izenburua: Bat Matxin Parrat
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004867.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004867.MID
youtube: null
---

Bat: Matxin Parrat.
Bi: Mitxel Eperdi.
Hiru: kolko bete diru.
Lau: iko melau.
Bost: koxkotean koxt.
Sei: korta bete bei.
Zazpi: buru bete bazpi.
Zortzi: katillu bete zorri.
Bederatzi: katun narrue eratzi.
Hamar: mar-mar.