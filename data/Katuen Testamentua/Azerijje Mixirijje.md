---
id: ab-4881
izenburua: Azerijje Mixirijje
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004881.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004881.MID
youtube: null
---

Azerijje mixirijje kumunikan
zure semie errotan,
errotatxurik nabillela
topeu naben erbi bet,
jeurti notsen arribet,
atara notsen begi bet,
ipiñi notsen barri bet,
zelemiñe kanpo,
zelemiñe kanpo,
gari-buru, gari-buru,
txirrist.