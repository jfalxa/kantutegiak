---
id: ab-4878
izenburua: Txiri-Txiri
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004878.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004878.MID
youtube: null
---

Txiri-txiri
oñe-oñe,
ama legez,
aite legez,
amama legez,
aitite legez,
txiri-txiri,
oñe-oñe.