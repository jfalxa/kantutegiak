---
id: ab-4875
izenburua: Kuku Biku
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004875.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004875.MID
youtube: null
---

Kuku biku
anean iku,
txorijjak umeak
basoan iku.
Txantxi perra
kastellano,
bidean bidean
kanta soño
erla perla
tuku taka pist.