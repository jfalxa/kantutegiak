---
id: ab-4885
izenburua: Ez Poztu Neskatillok
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004885.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004885.MID
youtube: null
---

Ez poztu neskatillok
txapel okerrari
astean zazpi surre
zapatuetan bi.
Domekan okelea
euzuan ikusi,
domekan okelea
euzuan ikusi.

Kortan daukaguz txal bi
azurre ta zala,
Aita San Antoniok
loditu daizala.
Tellatuen edurre
abarra basuan,
taluak erretako
urune auzuan.