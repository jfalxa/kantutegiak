---
id: ab-4880
izenburua: Antonio Polonio
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004880.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004880.MID
youtube: null
---

Antonio Polonio
pasa por aquí,
errotako txakurtxuek
jantzan ei daki.
- Nork erakutsi.
- Eindde ikesi.