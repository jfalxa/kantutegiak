---
id: ab-4855
izenburua: Kuku-Miku
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004855.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004855.MID
youtube: null
---

Kuku-miku kañola
kañaberia larrosa,
Pitxoke ta Pixote
jaunak nora zuazte
kalian gora
kalian bera,
biribiltxua alkate.
Eskuan daukat azixe
poltsan urregorrixe
amumatxuak balekixa
ateako leistake begixa.