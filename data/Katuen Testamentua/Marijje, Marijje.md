---
id: ab-4886
izenburua: Marijje, Marijje
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004886.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004886.MID
youtube: null
---

Marijje, Marijje
untxe diñotsut egijje:
topau egizu, topau egizu
ollo pintaren abijje.