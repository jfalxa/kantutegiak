---
id: ab-4857
izenburua: Zenbat Diran Amar Diran
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004857.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004857.MID
youtube: null
---

Zenbat diran amar diran
zenbat diran ikusi,
ollua kakarixetan
ollarrari ikasten,
txakurtxua josten eta
arratoitxua labretan,
txindurrixa garixetan,
kakarlardua laixetan,
eperra nabotzan,
idixa dantzan,
astua tanbolin jotzen
onen erdi erdixan dago
zorrixa barrez ittotzen.