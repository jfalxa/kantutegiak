---
id: ab-4871
izenburua: Dingilin Dango Eta
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004871.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004871.MID
youtube: null
---

Dingilin dango eta
ez naz ni zoroa
Santa Katalineko
dendari koxoa.
Sakuen sartu daure
sakuen zuloa
andixik agiri dau
kadera koxua.

Olloa faltzau jako
Marimondrongori,
señale onak ditu
agertu baledi:
buztena mardo.mardo
paparra bellegi,
aren arrautzatxuak
bolea dirudi.