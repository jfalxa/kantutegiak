---
id: ab-4869
izenburua: Andrakilla Mardo
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004869.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004869.MID
youtube: null
---

Andrakilla mardo
txorta luzia,
begi ederrakin
ondo jantzixa.
Ibiliko degu
dringilin-dranguan
aitak eta amak ni
ibiltzen nauen moduan.