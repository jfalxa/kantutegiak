---
id: ab-4892
izenburua: Garia Saltzen Nenguanian
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004892.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004892.MID
youtube: null
---

Garia saltzen nenguanian
Donostiako kalean,
dama gazte bat etorri jaten
garia zegan neukuan.
Batzuentzako amazortzian,
zeuretzat amabostian
amabostian nai ezpadezu,
laztantxu baten trukian.