---
id: ab-4896
izenburua: Aponetan
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004896.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004896.MID
youtube: null
---

A pone a bateta,
a pone a bi eta,
iru a pone,
lau a pone,
bost a pone,
sei a pone.

A sake a bat eta,
a sake a bi eta,...

A kulo a bat eta,
a kulo a bi eta,...

A karne a bat eta,
a karne a bi eta,...