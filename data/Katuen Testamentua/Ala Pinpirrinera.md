---
id: ab-4895
izenburua: Ala Pinpirrinera
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004895.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004895.MID
youtube: null
---

Ala pinpirrinera,
ala sanburrinera,
Maritxu Plazaberriko
buelta best'aldera.