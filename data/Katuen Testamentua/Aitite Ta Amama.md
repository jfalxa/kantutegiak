---
id: ab-4861
izenburua: Aitite Ta Amama
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004861.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004861.MID
youtube: null
---

Aitite ta amama
egurretan dire,
azkorea galdute
asarratu dire.
Aititek amamari
gonetatik tire
amamak aititeri
praketatik tire.
Tiretu ta tiretu
prakazarrak apurtu.
Tire ta tire
alkarrenak dire
tire ta tire
konponduko dire.

Aitite ta amama
egurretan dire
zkorea galdute
asarratu dire.
Aititek amamari
kopetatik tire
amamak aititeri
belarritik tire.
Tiretu ta tiretu
belarria gorritu.
Tire ta tire
alkarrenak dire
tire ta tire
konponduko dire.