---
id: ab-4863
izenburua: Kanuto
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004863.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004863.MID
youtube: null
---

Egun baten Kanuto
kalera joan zan,
karruak zapalduta
etxera joan zan.

Amak esan ei otsan
lotara joateko,
atzera kontestau eutsan
"Ez dot nai amatxo".

Kanuto Zizilindro,
Zilindro, Zilindro,
Kanuto Zizilindro...
pux! pux! pux.