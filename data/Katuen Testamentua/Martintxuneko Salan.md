---
id: ab-4868
izenburua: Martintxuneko Salan
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004868.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004868.MID
youtube: null
---

Martintxuneko salan
brotxian labana!,
txirrist einda jausi da,
txirrist einda jausi da
Martintxu laztana.

Martintxu dauanian
bularko miñagaz
amatxu juango jako,
amatxu juango jako
txokolatiagaz.

Txokolate fin orrek
erdian ondarra,
aretxek sendauko dau,
aretxek sendauko dau
Martintxun bularra.