---
id: ab-4864
izenburua: Din, Dan, Nor Il Da?
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004864.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004864.MID
youtube: null
---

- Din dan, nor il da?
- Peru zapataria.
- Zer egin dau? -Pekatu.
Auzoko txakurra urkitu,
txisilupean beratu,
tellatu gañian sikatu.
- Ori baiño ezpadau egin
biarko jako parkatu.

- Din dan, nor il da?
- Peru zapataria,
daroiela eleixara
kakarraldoak billurtu.
Aren emazte koittaduak
amar maradi pagatu.
Ori baño ezpadau egin
biarko jako parkatu.