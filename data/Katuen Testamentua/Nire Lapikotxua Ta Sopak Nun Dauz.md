---
id: ab-4879
izenburua: Nire Lapikotxua Ta Sopak Nun Dauz?
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004879.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004879.MID
youtube: null
---

- Nire lapikotxua ta sopak nun dauz?
- Txakurrek eta katuek jan.
- Txakurre ta katue nun dauz?
- Eskillarak gora.
- Eskillarak gora nun dauz?
- Suek erre.
- Sue nun dau?
- Urek amantau.
- Ure nun dau?
- Kortako bei gorriak edan.
- Kortako bei gorria nun dau?
- Diruagaitik saldu.
- Dirua nun dau?
- Okelatan gasten.
- Okelea nun dau?
- Txakurrek eta katuek jan.
- Txakurrek eta katuek nun dauz?
- Telletu ganian narrua siketzen.