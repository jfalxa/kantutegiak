---
id: ab-4856
izenburua: Txinkirrikitxin
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004856.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004856.MID
youtube: null
---

- Txinkirrikitxin-
atzo nintzen Bilbora
- txinkirrikitxin-
tomate berdetan.
- Txinkirrikitxin-
Bilbora noa
- txinkirrikitxin-
zer eiten?
- Txinkirrikitxin-
amantaltxo bat
- txinkirrikitxin-
erosten.

- Txikirrikitxin-
atzo nintzen Bilbora,
- txinkirrikitxin-
tomate berdetan.
- Txinkirrikitxin-
Bilbora noa
- txinkirrikitxin-
zer eiten?
- Txinkirrikitxin-
makallautxoa
- txinkirrikitxin-
ekarten.