---
id: ab-4866
izenburua: Matxalen Kardu
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004866.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004866.MID
youtube: null
---

Matxalen Karduk asto gañian
Bilbora bosteun arrautze
daruazala, Erletxetako
mutil koskorrak dirautse:

- Matxalen Kardu, Matxalen Kardu,
zer daruazu astoan?
- Zera jaroat... asto korotzak
zuek joteko lepuen.

"Añaniñoen atso sorgiña"
esanda, danak arrike,
artzen deutsoe asto zar ori
aurretik arin arinke.

Alako baten zapalduten dau
kurebixoen etxie.
Onek asarrez lau aldetarik
astuari eltzen deutsie.

Ene mutillak! Zer zoan ango
putz, arrantza ta dantzie!
Agur Matxalen, zure astua...ta
arrautzak Bilbon saltzie.