---
id: ab-4894
izenburua: Iru Te Iru Sei
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004894.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004894.MID
youtube: null
---

Iru te iru sei,
iru bederatzi,
amar hemeretzi,
bat hogei,
kaskallu ta miskillu
irurogei.