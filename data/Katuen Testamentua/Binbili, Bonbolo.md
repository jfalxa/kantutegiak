---
id: ab-4883
izenburua: Binbili, Bonbolo
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004883.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004883.MID
youtube: null
---

Binbili, bonbolo
sen da lo,
akerra Frantzian
balego,
akerra kantan,
idia jantzan
auntzak tanbolina jo.