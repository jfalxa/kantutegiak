---
id: ab-4860
izenburua: Mari Andresan Korotza
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004860.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004860.MID
youtube: null
---

Mari Andresan
Mari Andresan
korotza,
zazpi libra ta
zazpi libra ta
bost ontza.
Arek eiten deu
arek eiten deu
demasa:
epel-epel,
lodi-lodi,
punta metza,
epel-epel,
lodi-lodi,
punta metza.