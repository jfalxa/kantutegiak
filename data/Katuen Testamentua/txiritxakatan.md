---
id: ab-4889
izenburua: Txiritxakatan
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004889.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004889.MID
youtube: null
---

Txiritxakatan akerra dantzan
astoa bibolin-joten,
azagarie sueri puzke,
erbie urun-eralten.