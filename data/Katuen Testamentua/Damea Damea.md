---
id: ab-4874
izenburua: Damea Damea
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004874.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004874.MID
youtube: null
---

Damea damea bularretan
zeiten on zara orduatan?
Zeruko ixerrak kontetan
Zenbat dira ixerrok?
Bat eta bi te iru te lau,
bost eta sei te zazpi te lau-

Sasiolako komentopetik
hamalau frailek urten dau,
Fraile manga-tximurre
atxine ixin neuntsen bildurre.