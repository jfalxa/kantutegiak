---
id: ab-4862
izenburua: Ardiaren Kanta
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004862.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004862.MID
youtube: null
---

Entzun gure badozu
ardijjaren kantarik,-jai, jai!-
erdueze ona ta
esango deutsuk nik.

Dauka barriz piku bat
ardi pikaruak -jai, jai!-
jostorraz miñen miñena
baño okerragoa.

Nai dabenian sartzeko
nai daben lekutik -jai, jai!-
distingidu bagarik
pobre aberatsik.

Ardijje arrapetako
ezkerreko aldijjan-jai, jai!-
eskumia sartzen da
gona manerian.

Arrapatzen dauanian
entero dago poz,-poz, poz!-
iltzeko mai gañian
atzamarrakin kraus.