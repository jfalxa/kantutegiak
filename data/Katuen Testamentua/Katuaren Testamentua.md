---
id: ab-4851
izenburua: Katuaren Testamentua
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004851.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004851.MID
youtube: null
---

Il da, il da
gure katue,
ein dau, ein dau
testamentue:
amumantzako, amumantzako
ainkak eta burue,
aittittentzako, aittittentzako
abarkak eiteko narrue.
Mantxonentzako, Mantxonentzako
tripa barrue,
orixe dala, orixe dala
katuen testamentue.