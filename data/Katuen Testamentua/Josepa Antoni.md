---
id: ab-4865
izenburua: Josepa Antoni
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004865.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004865.MID
youtube: null
---

Josepa Antoni, koloregorri
kapitan baten alaba,
aitak doterik emon ez arren
billatuko dau senarra.

Astelenian ezkondu eta,
martitzenian basora,
eguaztenian orbela batu,
eguenian etxera.
Barixekuan bendeja prestau
zapatuan be plazara,
domekan bere apaindu eta
amarretako mezara.