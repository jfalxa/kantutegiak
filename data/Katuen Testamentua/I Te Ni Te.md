---
id: ab-4876
izenburua: I Te Ni Te
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004876.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004876.MID
youtube: null
---

I te ni te
perekin txarakin
polborin katxemarin.
Anton Anton
droge droge
Artolo Gomez
Artetxe Kandil.