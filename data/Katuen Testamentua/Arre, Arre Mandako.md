---
id: ab-4877
izenburua: Arre, Arre Mandako
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004877.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004877.MID
youtube: null
---

Arre, arre mandako
bijjer Gernikerako
etzi Mugijjerako,
d'andik zer ekarriko?
- Zapata ta garriko.