---
id: ab-4891
izenburua: Bat Eta Bi, Iru Te Lau
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004891.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004891.MID
youtube: null
---

Bat eta bi, iru te lau,
Mokotik andrea banatu dau,
bost eta sei, zazpi ta zortzi,
bizkaitarren kuadrilleri barriketa gitxi,
ezpabere belarrijjek eusi.