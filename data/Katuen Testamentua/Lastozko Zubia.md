---
id: ab-4854
izenburua: Lastozko Zubia
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004854.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004854.MID
youtube: null
---

Jontxuk egin ei xeban
lastozko zubixe,
andixek pasateko
Loretxu txikixe.
Adolfo txikixe.
Begotxu txikixe.

Asi zan pasaten da
jausi zan zubixe,
Jontxuk artu ei xeban
Loretxuk
Adolfok
sentimentu andixe.