---
id: ab-4853
izenburua: Maritxu Telletuko
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004853.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004853.MID
youtube: null
---

Maritxu Telletuko
gona gorridune
eutsi agin zarra ta
ekarri barrixe.