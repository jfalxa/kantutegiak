---
id: ab-4882
izenburua: Joan Nintzan Merkatura
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004882.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004882.MID
youtube: null
---

Joan nintzan merkatura
erosi nauan txakurkumatxu bat.
Txakurkumatxuak txau-txau-txau,
txarrikumatxuak kurri-kurri-kurrin,
tanbolintxuak prau-prau-prau,
txilinetxuak pio-pio-pin,
Ai ze gaua pase nabe nik.