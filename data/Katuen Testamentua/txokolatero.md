---
id: ab-4897
izenburua: Txokolatero
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004897.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004897.MID
youtube: null
---

Txokolatero,
molinero,
asto gañian
kaballero.

Zenbat atzamar dago zure lepoan.