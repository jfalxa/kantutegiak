---
id: ab-4872
izenburua: Bela Bela Jaune
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004872.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004872.MID
youtube: null
---

- Bela bela jaune
aite non da jaune?
- Aite soloetan
- Soloetan z'eiten?
- Artoak ereiten.
- Artoa zetako?
- Olloarentzako.
- Olloa z'eiteko?
- Arrautzea eiteko.
- Arrautzea zatako?
- Abadearentzako.
- Abadea zetako?
- Mezea emoteko.
- Mezea zetako?
- Aingerutxu biren erdien
zerure joateko.