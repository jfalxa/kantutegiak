---
id: ab-4887
izenburua: Drin-Dron Zeledron
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004887.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004887.MID
youtube: null
---

Drin-dron Zeledron,
Isabela, Manuela,
Katalina buelta,
buelta bonita,
ze letra-letra?
Pepita mon
Jaun Antxeko trentza
edoxian esperantza,
kalian tardantza,
astotxuek eitten dau
mendien arrantza.