---
id: ab-4888
izenburua: Txalo Pin Txalo
kantutegia: Katuen Testamentua
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004888.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004888.MID
youtube: null
---

Txalo pin txalo,
txalo pin txalo,
katutxua mizpira-
- gañian dago.
Bego badago,
badago bego,
katutxua mizpira-
- gañian dago
begira dago.