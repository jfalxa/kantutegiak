---
id: ab-1371
izenburua: Dohainetan Ederrena
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001371.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001371.MID
youtube: null
---

Dohainetan ederrena,
Hain baliosa dena,
Egun nahi dut aiphatu,
Errepikaz kantatu:
O garbitasun maitea!
Atsaginez bethea,
Ochala! Zu laudatzean,
bazintut bihotzean!
Berthute preziatua,
Ez aski maithatua;
Lore guzien lorea,
Arrantzerik gabea,
Arrantzerik gabea.