---
id: ab-1337
izenburua: Errege Handienaren
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001337.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001337.MID
youtube: null
---

Errege handienaren
Banderak du distiatzen;
Yesus, Yainko Gizonaren
Kurutzeak du argitzen,
Kurutzeak du argitzen.
Gizonen Kreatzailea
Gizonek han eman zuten,
Gizonek han eman zuten.
Yustuen Buruzagia
Hoben dunentzat han hilzen,
Hoben dunentzat han hilzen.