---
id: ab-1414
izenburua: Kristo Guziek Dezagun
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001414.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001414.MID
youtube: null
---

Kristo guziek dezagun egun adora,
Kristo guziek dezagun egun adora.
Hauche da izarra agertzen dena!
Berri bat dakhake den handiena,
Jesus maitearen sortzearena.