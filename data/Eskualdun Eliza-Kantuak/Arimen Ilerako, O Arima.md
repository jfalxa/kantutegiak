---
id: ab-1468
izenburua: Arimen Ilerako, O Arima
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001468.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001468.MID
youtube: null
---

O arima erostariak!
Zek zaukez alarauka
Zuen ardurea Jaunak
Orain bere ezaldauka.