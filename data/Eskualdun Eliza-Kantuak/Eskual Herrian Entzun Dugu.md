---
id: ab-1445
izenburua: Eskual Herrian Entzun Dugu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001445.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001445.MID
youtube: null
---

Eskual herrian entzun dugu
Mirakulu baten fama
Lourdesen ikusi omen du.
Haur batek Jesusen Ama
Goazen, goazen galtatzen gutu
Ama Birjinak Lourdera,
Dugun ikus noun den agertu
Goazen Lourdeko harpera.