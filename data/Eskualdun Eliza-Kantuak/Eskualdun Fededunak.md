---
id: ab-1435
izenburua: Eskualdun Fededunak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001435.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001435.MID
youtube: null
---

Eskualdun fededunak,
Zahar eta gazte,
Amaren laudatzerat lehia,
Lehia zaitezte.
Eskualdunak, bai, zatozte,
Zahar eta gazte,
Amaren laudatzerat
lehia zaitezte.
Goazen airoski denak
Axulaz aldera;
Hara, mendi gainean,
Amaren kapera;
Guretzat othiotzean
maiz han dago bera,
Goazen, goazen harekin
othoitz egitera.