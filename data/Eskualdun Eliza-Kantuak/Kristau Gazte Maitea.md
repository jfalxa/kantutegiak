---
id: ab-1465
izenburua: Kristau Gazte Maitea
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001465.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001465.MID
youtube: null
---

Kristau gazte maitea,
zuretzat naiz mintzo,
Adizkitzu gogotik
zembait solas gozo.
Zerurako bidea
Zaitzu irakhasten,
Eman zaite gogotik,
zu, haren ikhasten.