---
id: ab-1428
izenburua: Ahaide Ederrenetan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001428.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001428.MID
youtube: null
---

Ahaide ederretan
Dugun orok betan
Ahaide ederrenetan
Dugun orok betan
khanta Jesusen sortzia,
Haren zelutik yausia;
Jina duzu gure gana,
Oi! Zer zoriona!
Jina duzu gure gana,
Oi! Zer zoriona.