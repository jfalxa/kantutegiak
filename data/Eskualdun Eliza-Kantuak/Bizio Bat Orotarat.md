---
id: ab-1301
izenburua: Bizio Bat Orotarat
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001301.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001301.MID
youtube: null
---

Bizio bat orotarat ondikotz hedatua,
Hemen baltsan behar dugu lehiaz gudukatu;
Kristauen bihotzetarik bethikotz atheratu.
Hemen baltsan behar dugu lehiaz gudukatu;
Kristauen bihotzetarik bethikotz atheratu.