---
id: ab-1284
izenburua: Ala Ni Bainaiz Dohakabea
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001284.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001284.MID
youtube: null
---

Ala ni bainaiz dohakabea
Jainkoaren laidostatzeaz,
Satan gidari hartzeaz,
Ene zorthearen tristea!
Satan gidari hartzeaz,
Gisa hortan zerua galtzeaz.