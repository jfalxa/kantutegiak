---
id: ab-1475
izenburua: Ama Maite, Maria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001475.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001475.MID
youtube: null
---

Ama maite, Maria,
Eguiguzu lagun,
Zure bitartez Jesus
ikusi dezagun.
Aingeruzko lirijo estimagarrija
Usai gorazko telak
Zerutik yeichia,
Gorde nazazu,
Ama Guztiz maitatua,
Ez dezadan zikindu
Birtute garbia.