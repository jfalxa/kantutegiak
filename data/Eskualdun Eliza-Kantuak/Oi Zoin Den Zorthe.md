---
id: ab-1394
izenburua: Oi Zoin Den Zorthe
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001394.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001394.MID
youtube: null
---

Oi zoin den zorthe trichtea!
Bekatorea Oi! Zoin den zorthe trichtea!
Phentsazak maiz
Bekatuan bizitzea,
Salbatu nahi bahaiz
Salbatu nahi bahaiz.