---
id: ab-1395
izenburua: Mundu Zoro
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001395.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001395.MID
youtube: null
---

Mundu zoro enganioz bethea
Iduki nauk ondikotz gathetan;
Lausengari, enmganakor trichtea,
Atzeman nauk eta zembatetan!
Lorepean arrantzeak gorderik,
Sararazi nauk hire saretan;
Hire baitan sobera fidaturik,
Erori nauk asko bekatutan,
Erori nauk asko bekatutan.