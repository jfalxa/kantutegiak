---
id: ab-1357
izenburua: Hau Da Gure
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001357.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001357.MID
youtube: null
---

Hau da gure itsumendua,
zerk gaitu bada zoratzen?
Ahantzirik salbamendua,
Mundua dugu maithatzen.