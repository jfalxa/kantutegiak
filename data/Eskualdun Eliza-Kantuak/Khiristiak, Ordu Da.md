---
id: ab-1455
izenburua: Khiristiak, Ordu Da
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001455.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001455.MID
youtube: null
---

Khiristioak, ordu da
Dugun sogin gora:
Jesus hantik jeitchi da
Goure salbatzera.
O Jesus! Egun zira
Hasi odol ichourten;
Noula jinko beitzira,
Dereikuzu ikhasten
Noul'egin behar dugun
zelian har gitzatzun.