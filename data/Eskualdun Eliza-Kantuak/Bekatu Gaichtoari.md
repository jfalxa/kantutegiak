---
id: ab-1319
izenburua: Bekatu Gaichtoari
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001319.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001319.MID
youtube: null
---

Bekatu gaichtoari ihes egiteko,
Gogoan har ditzagun, gure azken finak.
Hala yoanen zaizkigu gaizkirako minak.