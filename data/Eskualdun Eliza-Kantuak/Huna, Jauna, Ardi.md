---
id: ab-1396
izenburua: Huna, Jauna, Ardi
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001396.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001396.MID
youtube: null
---

Huna Yauna ardi bat galtzen zena,
Ez bazindu izan urrikari;
Bainan orai zure eztitasuna
Agertzen da nere arimari,
Bainan orai zure eztitasuna
Agertzen da nere arimari.