---
id: ab-1432
izenburua: Bihotzterrireki Haustez
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001432.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001432.MID
youtube: null
---

Bihotzterri haustez
Gure beithan sar gitian,
Eztakigu dagun ourthez
bizirik giratekian.
Hautsak burutik behera
Zer giren gutu salhatzen,
Lurretik jinak lurrera
Berriz girela utzulzen.