---
id: ab-1320
izenburua: Sinhesten Dut Elizak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001320.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001320.MID
youtube: null
---

Sinhesten dut elizak dadazkon egiak,
Sinhesten dut hala direla guziak.
Neronek ikusirik baino,
Hark erranik segurkiago,
Sinhesten dut hala direla guziak.