---
id: ab-1477
izenburua: Hel Gaiten
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001477.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001477.MID
youtube: null
---

Hel gaiten guziak,
hel Bethleemerat
Yesus maitearen
adoratzerat;
Hel gaiten guziak,
hel Bethleemerat
Yesus maitearen
adoratzerat.
Aingeruek dute
han dela erran,
Kristauak, Zatozte,
han da mañateran.