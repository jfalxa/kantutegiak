---
id: ab-1429
izenburua: Jinkoaren Jitia Gatik
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001429.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001429.MID
youtube: null
---

Jainkoaren jitia gatik
Dugun orok Noël kanta.
Zeren gure amorekatik
gizonturik sorthu beita.
Adamek zien ekharri
gizon kentia galtzera,
Jinko Semia da ethorri
Ororen eraikitera.