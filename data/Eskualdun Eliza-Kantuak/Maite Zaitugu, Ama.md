---
id: ab-1473
izenburua: Maite Zaitugu, Ama
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001473.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001473.MID
youtube: null
---

Maite zaitugu, ama,
Birjina Maria,
Izantzagu gurekin
beti erruquia,
Zureni gure
penen kontsolagarria,
Zera gure biotzen
Izar pozgarria
Zera gure biotzen
Izar pozgarria
Kanta dezagun alai
biotz biotzetik
Mariaren izena
gloriaz beterik,
Ez dala guretzako
naigabe ta minik,
Lagundutzen badigik
Birjina zerutik.