---
id: ab-1392
izenburua: Zer Dire Gure Egunak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001392.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001392.MID
youtube: null
---

Zer dire gure egunak!
Zer da gure bizia?
Itzalaren pare dire
Non dugu zuhurtzia?
Atsegin iragankorrei
Bihotza largatzeaz?
Akabo da laster gutaz,
orhoit gaiten hiltzeaz.