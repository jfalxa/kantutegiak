---
id: ab-1382
izenburua: Ala Ni Bainaiz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001382.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001382.MID
youtube: null
---

Ala ni bainaiz dohakabea
Jainkoaren laidostatzeaz,
Satan gidari hartzeaz,
Ene zorthearen tristea!
Satan gidari hartzeaz,
Gisa hortan zerua galtzeaz.