---
id: ab-1340
izenburua: Zerutik Yautsi Zare
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001340.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001340.MID
youtube: null
---

Zerutik yautsi zare,
bitima garbia,
zu zare sakramendu,
Zu zare ostia.
Arimen yaunharia.
Sakrifizioa;
Bata nola bertzea,
Yesus dibinoa,
Yesus dibinoa.