---
id: ab-1418
izenburua: Oi Bethleem
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001418.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001418.MID
youtube: null
---

Oi Bethleem!
Ala egun zure loriak,
oi! Bethleem!
Ongi baitu distiratzen;
Zuganik heldu den argiak
Bethetzen tu bazter guztia,
oi! Bethleem!
oi! Bethleem.