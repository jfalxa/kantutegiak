---
id: ab-1422
izenburua: Phiztu Da Lorios
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001422.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001422.MID
youtube: null
---

Phiztu da lorios Jesus hilen artetik,
Igan bitorios zerura guregatik.
Eta ni munduan
Hila bekatuan
O heriotze trichtea,
Bekhatutan bizitzea.