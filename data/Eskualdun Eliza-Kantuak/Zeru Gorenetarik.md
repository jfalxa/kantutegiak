---
id: ab-1359
izenburua: Zeru Gorenetarik
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001359.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001359.MID
youtube: null
---

Zeru gorenetarik,
Mirakulu handia!
Aitaren eskunetik
Hemen da yautsia,
Yesus, Yesus.