---
id: ab-1442
izenburua: Adoratzen Zaitut, Jesus
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001442.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001442.MID
youtube: null
---

Adoratzen zaitut,
Jesus maitea,
Sakramendu hortan
bethi gordea;
Hemen zutan dut
ene sinhestea
In dazu zeruan
zure ikustea.
Adoratzen zaitut,
Jesus maitea,
Sakramendu hortan
bethi gordea.
Jesusi ez zaio aski orai behin
Gure salbatzeko sakrifika dadin
Gizona nahi du berekin bat egin,
Bethi egoiten da nolazpeit gurekin
Bethi egoiten de nolazpeit gurekin.