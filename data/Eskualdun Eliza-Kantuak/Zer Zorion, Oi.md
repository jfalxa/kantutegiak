---
id: ab-1373
izenburua: Zer Zorion, Oi
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001373.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001373.MID
youtube: null
---

Zer zorion! Oi! Hau da ohorea!
Ikusten dut Yesus maitea,
Zer zorion!
Zer zorion.