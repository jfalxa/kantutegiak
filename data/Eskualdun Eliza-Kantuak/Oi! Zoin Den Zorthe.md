---
id: ab-1401
izenburua: Oi! Zoin Den Zorthe
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001401.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001401.MID
youtube: null
---

Oi zoin den zorthe trichtea!
Oi! Zoin den zorthe trichtea!
Bekhatorea,
Oi! Zoin den zorthe trichtea!
Phentsazak maiz
Bekhatuan bizitzea,
Salbatu nahi bahaiz
Salbatu nahi bahaiz.