---
id: ab-1294
izenburua: Nigar Marrasketan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001294.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001294.MID
youtube: null
---

Nigar marrasketan,
arima daunatuak,
Leiz eternletan,
Bethikotz hondatuak
Daude; aditzagun
Heien boz sarkorrak,
Ongi, har detzagun
Heien arrankurak.