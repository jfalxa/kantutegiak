---
id: ab-1433
izenburua: Jainko Adoragarria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001433.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001433.MID
youtube: null
---

Jainko adoragarria,
Aitarik hoberena,
Entzun zazu zure Seme
guregatik hil dena.
Grazia eske dagotzu
guretzat kurutzean,
Othoi! Sokorri gaitzatzu
gure heriotzean.
Ondikotz merezi tugu
gaztigu garratzenak,
Othoi! Ahantz bekizkitzu,
Jauna, gure hobenak.
Hel zaizkigu, Birjina,
gure behar orduetan
guziz hiltzeko orenean.