---
id: ab-1406
izenburua: Maria Guziz Saindua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001406.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001406.MID
youtube: null
---

Maria guziz saindua,
Berthutez berregindua.
Graziaz aberastua,
Zureganik Sallbatzailea
Sorthu zauku, Ama maitea.
Maria gure ama da,
Dugun maitha, dugun lauda;
Maria gure Ama da,
Maria, Maria
Dugun maitha, dugun lauda
Maria, Maria
Dugun maitha, dugun lauda.