---
id: ab-1420
izenburua: Egun Estrainatu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001420.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001420.MID
youtube: null
---

Egun astrainatu gaitu
Haurrak odolaz.
Jesus haurtcho,
larru delikatua,
zortzi garreneko odolostua.