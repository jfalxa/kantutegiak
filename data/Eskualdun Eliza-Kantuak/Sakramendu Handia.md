---
id: ab-1307
izenburua: Sakramendu Handia
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001307.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001307.MID
youtube: null
---

Sakramentu handia,
Ah, gure yanharia!
Jesus Jauna hor dugu,
Berak erran derauku.