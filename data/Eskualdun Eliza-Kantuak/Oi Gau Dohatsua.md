---
id: ab-1409
izenburua: Oi Gau Dohatsua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001409.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001409.MID
youtube: null
---

Oi gau dohatsua,
Yainkoaz hautatua,
Zaukuna hurbiltzen,
Zaukuna hurbiltzen,
Mesias maitea, graizaz bethea,
Da munduratzen.