---
id: ab-1333
izenburua: Gizona, Nun Duk
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001333.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001333.MID
youtube: null
---

Gizona, nun duk zuhurtzia?
Zer ez dakik,
Ez dela deus ere bizia
Khe bat baizik?
Higuintzak bihotzez
mundua orai danik;
Herioa duk hurbildua
Hire ganik.