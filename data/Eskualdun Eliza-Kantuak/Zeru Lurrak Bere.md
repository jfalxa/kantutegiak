---
id: ab-1345
izenburua: Zeru Lurrak Bere
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001345.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001345.MID
youtube: null
---

Zeru lurrak bere hitzaz
Egin dituen Yainkoa.
Egin da bere emaitzaz
Gure Aita zerukoa.
Hori da beraz, yauna,
Hartu duzun izena
Gu denen onetan.
Heda dezagula.