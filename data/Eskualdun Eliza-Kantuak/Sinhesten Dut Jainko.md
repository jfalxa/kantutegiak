---
id: ab-1367
izenburua: Sinhesten Dut Jainko
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001367.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001367.MID
youtube: null
---

Sinhesten dut jainko
Aita guziz botheretsua,
Guzien egilea da
zeruan bai lurrean
Sinheste huntan fernuki
Bizi nahi naiz bethi
Fede hau galtzen lukena
Ez baititake salbe,
Ez baititake salba.