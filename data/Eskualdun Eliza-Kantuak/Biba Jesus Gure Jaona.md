---
id: ab-1463
izenburua: Biba Jesus Gure Jaona
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001463.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001463.MID
youtube: null
---

Biba Jesus gure jaona,
Gaurko gaian sortzen dena
Bib'andere Dona Maria,
Biba Josef Aita houna.