---
id: ab-1313
izenburua: Heda, Yauna, Heda
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001313.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001313.MID
youtube: null
---

Heda, Yauna, Heda zure argiak,
Zuk argituz har ditzagun egiak.
Gizon lagun eman daukuzunak
Nola argi gaitzazke gu gizonak.