---
id: ab-1385
izenburua: Zato, Izpiritua, Zato
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001385.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001385.MID
youtube: null
---

Zato, izpiritua,
zato laguntzera,
Gure bortiztera,
Zato Izpiritua.
Gure berotzera,
su amultsua.
Azkar zatzu arimak,
Bethe zatzu bihotzak
Zure dohainik sainduenez.