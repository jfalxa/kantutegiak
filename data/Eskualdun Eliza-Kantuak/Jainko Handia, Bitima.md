---
id: ab-1374
izenburua: Jainko Handia, Bitima
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001374.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001374.MID
youtube: null
---

Jainko handia,
bitima salbagarria!
O aita ona,
Enetzat eman zarena!
Aldarearen gainetik
Beha zaguzu,
Ostia saindu hortatik
Adi gaitzatzu,
Yaun ona!
Kanta zagun baltsan
Yainkoaren ontasuna,
kantza zagun baltsan
Yesusen bihotz ona.