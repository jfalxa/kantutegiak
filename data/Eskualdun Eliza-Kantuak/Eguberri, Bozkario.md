---
id: ab-1426
izenburua: Eguberri, Bozkario
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001426.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001426.MID
youtube: null
---

Eguberri, bozkario,
bozkario lurrian.
Dugun laida misterio,
Asmatia zelian.
Gizonaren erosteko
jinko gizon egina
Goure zorren phakatzeko
Bera zordun emana,
Goure zorren phakatzeko
Bera zordun emana.