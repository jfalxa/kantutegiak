---
id: ab-1403
izenburua: Oi! Zoin Diren Dohatsuak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001403.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001403.MID
youtube: null
---

Oi! Zoin diren dohatsuak
Bekatutik urrunduak,
Berthutez berreginduak
Bizi direnak!
Salbatzeagatik,
Jesus bihotzetik,
Guzien gainetik,
Maite dutenak.