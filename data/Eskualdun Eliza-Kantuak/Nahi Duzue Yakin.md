---
id: ab-1282
izenburua: Nahi Duzue Yakin
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001282.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001282.MID
youtube: null
---

Nahi duzue yakin
nere gutizia?
Jainko onarentzat da
ene min guzia.
Ene baïthan nago gogoetan,
Noiz ikusiko dudan ene Jainkoa;
Ezin bertzela biziz,
ah! Hiltzerat noha.