---
id: ab-1378
izenburua: Othe Da Deus Nesesario
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001378.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001378.MID
youtube: null
---

Othe da deus nesesario denik
Mundu huntan baizen salbatzea?
Ez ahal da halako zorionik,
Nola zeruetara heltzea,
Ez ahal da halako zorionik,
Nola zeruetara heltzea.