---
id: ab-1457
izenburua: Zeruko Erregina
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001457.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001457.MID
youtube: null
---

Zeruko erregina,
Haurrak, lauda zagun
Ametan eztiena
Lauda gaut'egun;
Aingeruek zeruan
Dutena laudatzen,
Nolaz ez dut lurrean
Bethi maitaturen.