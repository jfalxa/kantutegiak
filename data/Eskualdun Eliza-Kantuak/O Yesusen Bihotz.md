---
id: ab-1321
izenburua: O Yesusen Bihotz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001321.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001321.MID
youtube: null
---

O Yesusen bihotz samur, enetzat idekia,
Othoi! Emazu neurean zuganako lehia.
Ikusiz zure burua arrantzeez bethea,
Ni holare ez dut nahi lore pean neurea.