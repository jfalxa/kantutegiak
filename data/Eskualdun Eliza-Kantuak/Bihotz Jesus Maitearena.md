---
id: ab-1304
izenburua: Bihotz Jesus Maitearena
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001304.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001304.MID
youtube: null
---

Bihotz Jesus maitearena,
Graziaren ithurria,
Hemen naiz,
Bihotz samurrena,
zureganat ethorria.