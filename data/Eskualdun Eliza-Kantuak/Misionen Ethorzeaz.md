---
id: ab-1377
izenburua: Misionen Ethorzeaz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001377.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001377.MID
youtube: null
---

Misionen ethortzeaz
Yauna dela laudatu
Helas! Hauen eskasiaz
hanitz dire daunatu.
Kristau leialak, zatozte
laster misionerat,
Yainkoak deitzen zaituzte
harekin baketzerat.