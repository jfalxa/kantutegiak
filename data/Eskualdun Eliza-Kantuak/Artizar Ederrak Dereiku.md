---
id: ab-1448
izenburua: Artizar Ederrak Dereiku
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001448.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001448.MID
youtube: null
---

Artizar ederrak dereiku erraiten
Eguna zaikula bullantzen
Haren argi maithagarriak
Alegeratzen tu lurrian guziak
Haren ondotik da Ekhia agertzen,
Harek ullhumpiak ohiltzen.