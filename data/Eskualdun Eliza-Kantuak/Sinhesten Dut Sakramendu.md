---
id: ab-1324
izenburua: Sinhesten Dut Sakramendu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001324.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001324.MID
youtube: null
---

Sinhesten dut Sakramendu sainduan,
Yesus ona, zu zarela ostian,
Sinhesten dut, sinhesten dut.
Yesus ona! Oi! Ene Nausia,
Amodioak zaitu hola ezestatu.
Ni ere, ordainez, Zurea naiz guzia,
Nahi zaitut bethiere maithatu.