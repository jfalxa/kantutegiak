---
id: ab-1342
izenburua: Bihotz Yesus Maitearena
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001342.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001342.MID
youtube: null
---

Bihotz Yesus maitearena,
Graziaren ithurria.
Hemen naiz,
Bihotz samurrena,
Zure ganat ethorria.
Oezin aski laudatuzko,
Bihotz garbi sakratua!
Noiz zaitut bada maithatuko?
Zerk nauka sorhaiotua?
Bihotz enetzat sustatua,
Berant zaitut ezagutu;
Bihotz ezti preziatua
Helas, ez zaitut maitatu.