---
id: ab-1375
izenburua: Oi Arima Jainkoaz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001375.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001375.MID
youtube: null
---

Oi arima, Yainkoaz onhetsia,
Orhoit hadi badela leku bat.
Oi! Arima yainkoaz onhetsia,
Orhoitt hadi badela leku bat,
Hemen arras Zeruko yustizia
eztitua ez duenarentzat.
Hemen arras Zeruko yustizia
eztitua ez duenarentzat.
Othoi! Yauna,
Othoi! Urrikalmendu,
Othoi! Purgatorioko arimentzat.
Darotzugu galdatzen barkhamendu
Heien mundu huntako hutsentzat.