---
id: ab-1471
izenburua: Jesusen Biotzari-Alkar Bat Egiñaz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001471.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001471.MID
youtube: null
---

Alkar bat egiñaz
O Biotz lastana,
gatos damuturik
eskean zugana.