---
id: ab-1322
izenburua: Ala Baita Dohatsua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001322.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001322.MID
youtube: null
---

Ala baita dohatsua
Inozentzian dena!
Nihoiz bere deskantsua
Ezin gal dezakena!
Munduak bere gozoak
alferrik laudatzen,
Barneko bake hark berak
Guziak tu garhaitzen,
Barneko bake hark berak
Guziak tu garhaitzen.