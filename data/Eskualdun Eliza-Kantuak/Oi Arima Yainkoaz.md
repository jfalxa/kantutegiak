---
id: ab-1334
izenburua: Oi Arima Yainkoaz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001334.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001334.MID
youtube: null
---

Oi! arima, yainkoaz onhetsia,
Orahoit hadi badela lekhu bat,
Hemen arras zeruko yustizia
eztitua ez duenarentzat.