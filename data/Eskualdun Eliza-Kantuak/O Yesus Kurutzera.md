---
id: ab-1306
izenburua: O Yesus Kurutzera
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001306.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001306.MID
youtube: null
---

O Yesus! Kurutzera
Nigatik igan zarena!
Ene gaztigatzera
maiz behartzen zaitudana!
Orai, zure aldera,
Bihurtzen naiz yainko ona!
Ah! Orhoitzaite odola
Enetzat eman duzula.