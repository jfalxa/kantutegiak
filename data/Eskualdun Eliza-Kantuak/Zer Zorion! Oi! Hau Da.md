---
id: ab-1318
izenburua: Zer Zorion! Oi! Hau Da
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001318.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001318.MID
youtube: null
---

Zer zorion! Oi! Hau da ohorea!
Ikusten dut Yesus maitea,
Zer zorion!
Zer zorion!
Horra zerutik yautsia neregatik,
Zer zorion! Yautsia neregatik,
zer zorion! Yautsia neregatik
zer zorion.