---
id: ab-1309
izenburua: Bizirik Naiz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001309.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001309.MID
youtube: null
---

Bizirik naiz, baina ez,
Yainkoan dut bizia.
Orai hark hazirikan,
hartan natza guzia;
Hura zeruan beharrez,
Hiltzen naiz ezin hilez,
hiltzen naiz ezin hilez.