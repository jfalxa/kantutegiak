---
id: ab-1381
izenburua: Oi Birjina Ama
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001381.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001381.MID
youtube: null
---

Oi! Birjina! Ama ona!
Entzun zatzu zure haurrak;
Senda zatzu,
Garbizatzu
Bekhatorosen bihotzak,
Bekhatorosen bihotzak.