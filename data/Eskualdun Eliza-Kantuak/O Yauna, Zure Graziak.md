---
id: ab-1347
izenburua: O Yauna, Zure Graziak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001347.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001347.MID
youtube: null
---

O Yauna, zure graziak
Ez zaizkit baliatu,
Niri ongi egitetik
Ez, ez zare baratu;
Oradainez ene phartetik,
Nola zaitut maithatu.