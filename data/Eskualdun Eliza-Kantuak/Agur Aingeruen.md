---
id: ab-1296
izenburua: Agur Aingeruen
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001296.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001296.MID
youtube: null
---

Agur aingeruen
Eta zeru lurren
Erregina puchanta!
Ederra zare guzia,
Hiten duzu iguzkia,
Zein zaren distiranta.