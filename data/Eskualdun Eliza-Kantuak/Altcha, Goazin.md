---
id: ab-1354
izenburua: Altcha, Goazin
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001354.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001354.MID
youtube: null
---

Altcha, goazin
Bathaioko
Ithurri sakraturat.
Kristau egin gineneko.
Botuen berritzerat.
Arnegatzen dut
Bihotzez dut,
Satani behin bethikotz ukhatzen,
Zerua lurra lekhuko hartzen.