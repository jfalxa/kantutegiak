---
id: ab-1349
izenburua: Aingeru Batek Mariari
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001349.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001349.MID
youtube: null
---

Aingeru batek Mariari
Dio: graziaz bethea,
Yaungoikoaren Semeari
Eman diozu sortzea.