---
id: ab-1452
izenburua: O Jesusen Bihotz Gartsua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001452.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001452.MID
youtube: null
---

O Jesusen bihotz gartsua,
Zu zira ene bizia,
Zure alderat sustatia
Ematen dezut en'izaite guzia.
Aingurien amori ounak
Erre gitzala guziak,
Jesusen zinez maithatzeko,
laidatzeko et'ohoratzeko,
Jesusen zinez maithatzeko
laidatzeko et'ohoratzeko.