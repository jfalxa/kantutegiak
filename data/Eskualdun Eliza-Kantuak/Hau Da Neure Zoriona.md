---
id: ab-1438
izenburua: Hau Da Neure Zoriona
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001438.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001438.MID
youtube: null
---

Hau da neure zoriona!
Zembat ontasun betan
Salbatzaile guziz ona,
Zer! Zu zare ni baithan!
Jesus ona! Jesus ona!
Othoi! Ez ni abandona.
Egon zaite nere baithan
egon menderen mendetan.
Oi zer ogi dut ikusten
Gure aldaretara,
Zerutik yausten,
Gu bazkatzera.