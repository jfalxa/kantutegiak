---
id: ab-1295
izenburua: Zuen Heriotzeaz Funtski
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001295.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001295.MID
youtube: null
---

Zuen heriotzean
funtski orhoitzerat,
Lehia zintezkete
Ongi bizitzerat;
muga latzgarri hura
datza hurbildua,
Halarik ez duzue
higuintzen mundua.