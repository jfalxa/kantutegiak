---
id: ab-1410
izenburua: Jaiki Gaiten Kristauak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001410.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001410.MID
youtube: null
---

Yaiki gaiten kristauak,
Guziak ohetarik
Hel gaiten Elizarat,
Boz eta alegerarik.
Garuen kantuek
Gaituzte Gombidatzen,
Artzainen manjuretek
lotharik iratzartzen.