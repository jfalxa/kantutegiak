---
id: ab-1461
izenburua: Oi! Jesusen Bihotz Samur
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001461.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001461.MID
youtube: null
---

O Jesusen bihotz samur,
Enetzat idekia,
Othoi! Emazu neurean
Zu ganako lehia,
Zu ganako lehia.