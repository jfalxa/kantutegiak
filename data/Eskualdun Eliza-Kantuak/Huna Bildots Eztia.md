---
id: ab-1467
izenburua: Huna Bildots Eztia
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001467.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001467.MID
youtube: null
---

Huna bildots eztia
Egiazko ogia
Zerutik da yautsia,
Dugun odora.
Da gur'Artzain ona,
Kreatzailea, Aita hoberena,
Salbatzailea, Aita hoberena,
Salbatzailea.