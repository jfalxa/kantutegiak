---
id: ab-1459
izenburua: Joan Handiak, Ikhusazie
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001459.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001459.MID
youtube: null
---

Joan handiak, ikhusazie
Jinko bat egun nigarrez;
Eta iziturik horrez
Ah! Ikhara ziteie
Ah! Ikhara ziteie.