---
id: ab-1446
izenburua: Mundu Faltsu Tromperiaz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001446.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001446.MID
youtube: null
---

Mundu faltsu tromperiaz bethia,
Hitz ederrez inganatzalia,
Jin bedi, jin ene illusitzera,
Eniz beldur, Jesus eneki da.