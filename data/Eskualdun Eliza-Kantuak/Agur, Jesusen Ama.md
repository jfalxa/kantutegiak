---
id: ab-1476
izenburua: Agur, Jesusen Ama
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001476.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001476.MID
youtube: null
---

Agur, Jesusen ama,
Birjina maitea,
Agur Ichaso izar
distiazailea,
Agur zeruko eguzki
pozkidan bethia,
Agur pekatorien
Kayola estapia.