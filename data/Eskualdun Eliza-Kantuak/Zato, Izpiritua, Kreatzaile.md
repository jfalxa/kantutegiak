---
id: ab-1298
izenburua: Zato, Izpiritua, Kreatzaile
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001298.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001298.MID
youtube: null
---

Zato Izpiritua,
Kreatzaile Saindua,
Bisitza zatzu hotzak,
othoi! Gure bihotzak.