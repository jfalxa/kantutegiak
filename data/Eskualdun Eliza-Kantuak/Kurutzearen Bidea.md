---
id: ab-1352
izenburua: Kurutzearen Bidea
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001352.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001352.MID
youtube: null
---

Kurutzearen bidea
Dugun egin,
Kristau maiteak,
Salbamenduko bidea
Ideki dauku Kurutzeak,
Ideki dauku Kurutzeak.
Kurutzea, Kurutzea,
beraz bethi
Dezagun lauda,
lauda goraki.