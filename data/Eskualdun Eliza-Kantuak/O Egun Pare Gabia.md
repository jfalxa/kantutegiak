---
id: ab-1444
izenburua: O Egun Pare Gabia
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001444.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001444.MID
youtube: null
---

O egun pare gabia,
Jinkoak eman ditana,
Grazia guziz handia,
Egun ukhen dudana!
Eta halere tristia,
Beldurrez niz ikhara,
Galdurik berthutia,
Herra nadin harzara.
O Jesus! Hel zakizkigu
Indarrez bethe gitzatzu;
Gureki egon zite
Orai eta bethiere.