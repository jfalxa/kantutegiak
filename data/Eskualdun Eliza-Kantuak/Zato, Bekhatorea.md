---
id: ab-1360
izenburua: Zato, Bekhatorea
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001360.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001360.MID
youtube: null
---

Zato, bekhatorea, zato
Elizarat, Yainko yaunaren hitza
funtski aditzerat, funtski aditzerat.
Bekhatuen neurria bethe ez dezazun,
Adi zazu yainkoak zer erraiten dautzun:
Ez egon bekhatuan, ez egon luzaro,
Damu izan dezazun alferretan gero.