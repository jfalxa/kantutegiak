---
id: ab-1469
izenburua: Arimen Ilerako-O Kristiñauak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001469.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001469.MID
youtube: null
---

O Kristiñauak, entzun gagizuz
Gure oiñaze andietan,
Gaut'egun gagoz atsegin barik
izigarrizko penetan.