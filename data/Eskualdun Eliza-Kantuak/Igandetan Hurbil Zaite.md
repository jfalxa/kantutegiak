---
id: ab-1348
izenburua: Igandetan Hurbil Zaite
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001348.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001348.MID
youtube: null
---

Igandetan hurbil zaite
Mezaren entzutera,
Sainduen phestak
orobat tutzu begiratuko,
Heien etsemplu ederrei
arthoski jarraikiko.
Sakramendu Handi hartan
Jesus adoratzera.