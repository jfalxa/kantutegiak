---
id: ab-1434
izenburua: Ala Beita Ikharagarri
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001434.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001434.MID
youtube: null
---

Ala baita ikharagarri
Hobendun baten hiltzia!
Adio deri mundiari,
Erraiten, esteiaria!
Joaiteko daunatieki
sekulakotz erratzera.