---
id: ab-1336
izenburua: Yesu Kristok Dauku
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001336.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001336.MID
youtube: null
---

Yesu kristok dauku manatzen
Estaiari barkatzea,
Eta non ez duzun barkatzen,
Segur duzu damnatzea.
Haren etsai zen guziari
barkatzen hasi da bera.
Obedi zogun legeari,
maithatuz gure etsaia,
maithatuz gure etsaia.