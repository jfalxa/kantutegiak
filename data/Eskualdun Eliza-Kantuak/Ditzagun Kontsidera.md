---
id: ab-1451
izenburua: Ditzagun Kontsidera
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001451.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001451.MID
youtube: null
---

Ditzagun kontsidera
Ifernietako phenak.
Ekharten nai hiltzera
Haiez orhitziak berak,
Bekhatien uzteko
Hek dira phentsatzeko.