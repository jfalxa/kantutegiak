---
id: ab-1456
izenburua: Hirur Erregereki
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001456.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001456.MID
youtube: null
---

Hirur erregereki maiteki,
laidatzen zutugu,
Eta Herodeseki
gerla nahi dugu.