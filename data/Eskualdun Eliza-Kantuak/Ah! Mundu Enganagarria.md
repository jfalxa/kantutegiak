---
id: ab-1358
izenburua: Ah! Mundu Enganagarria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001358.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001358.MID
youtube: null
---

Ah! Mundu eganagarria,
Yakin banerauk berria:
Hire agintza faltsoak,
Zein diren ondikozkoak.
Ah! Orai diat ikusten,
Ikusteaz nauk hiratzen,
Ikusteaz nauk hiratzen.