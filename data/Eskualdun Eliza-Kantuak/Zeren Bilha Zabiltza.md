---
id: ab-1436
izenburua: Zeren Bilha Zabiltza
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001436.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001436.MID
youtube: null
---

Zeren bilha zabiltza,
Andre Madalena?
Zerk hola zaramatza
Desertuan barna?
yauna galduz geroztik,
ez dut gozorikan;
Gauza guziak ditut
Higuin mundu huntan.