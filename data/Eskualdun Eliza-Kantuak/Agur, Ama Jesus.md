---
id: ab-1350
izenburua: Agur, Ama Jesus
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001350.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001350.MID
youtube: null
---

Agur, ama Jesus maitearena,
Emaztetan guzien garbiena,
Bethi dela benedikatua
Zureganik, zure ganik,
zure ganik sorthua,
zure ganik sorthua.