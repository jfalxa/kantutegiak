---
id: ab-1408
izenburua: Oi Eguberri Gaua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001408.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001408.MID
youtube: null
---

Oi eguberri gaua,
Bozkarioako gaua,
Alegeratzen duzu
Bihotzean kristaua.
Mundu guzia duzu
zorionez bethetzen,
Zeren zuk baidiozu
Mesias dela sortzen.