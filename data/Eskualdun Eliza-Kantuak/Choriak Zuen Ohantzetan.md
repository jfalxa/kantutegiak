---
id: ab-1292
izenburua: Choriak Zuen Ohantzetan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001292.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001292.MID
youtube: null
---

Choriak zuen ohentzetan
Lauda ezazu Jauna;
Kanta haren ontasuna,
Inguru guzietan.