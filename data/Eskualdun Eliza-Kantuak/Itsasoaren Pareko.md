---
id: ab-1380
izenburua: Itsasoaren Pareko
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001380.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001380.MID
youtube: null
---

Itsasoaren pareko
mundu hau salbamenduko.
Chume, handiak,
Garen guztiak,
Etsaiarekin guduka
Ari, borthizki borroka.