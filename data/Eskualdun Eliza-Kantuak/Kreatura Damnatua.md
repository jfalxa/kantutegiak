---
id: ab-1407
izenburua: Kreatura Damnatua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001407.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001407.MID
youtube: null
---

Kreatura damnatua,
Zerk hauen tormentatzen,
Zer den hire Ifernua,
Hire penak zer diren,
Erraguk, erraguk,
Argitu nahi gaituk.
Ezin erran dirot hitzez
Zembat dudan sofritzen,
Ene dolore borthitzez
Hasten banintz mintzatzen,
Flakoak, flakoak litezke hitz guziak.