---
id: ab-1356
izenburua: Huna Jesus, Bildots
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001356.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001356.MID
youtube: null
---

Huna, Jesus, bildots amultsua,
Guregatik sakrifikatu dena:
bekatuan bazare, Kristaua
Goardia ongi! Ez hurbil harenganik Yauna,
Zure errezebitzeko
Behinere ez naiz gigne izatu;
Bainan ene ongi sendatzeko,
zure hitz bat askiko da, errazu.