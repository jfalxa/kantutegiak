---
id: ab-1332
izenburua: Ez Dakit Zergatik
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001332.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001332.MID
youtube: null
---

Ez dakit zergatik,
Beldurrak harturik,
zerk nauen hola herstu.
Ez dakit zergatik,
Beldurrak harturik,
zerk nauen hola herstu.
Jauna dut nerekin bere graziekin,
eta nago, eta nago gogor eta muthu.