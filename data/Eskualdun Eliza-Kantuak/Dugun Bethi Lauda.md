---
id: ab-1368
izenburua: Dugun Bethi Lauda
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001368.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001368.MID
youtube: null
---

Dugun bethi lauda yaunaren ontasuna,
Dugun bethi lauda yaunaren ontasuna.
Ohore, Aingeruak, zuen yainkoari,
Esker handienak.
Nausi guzizkoari.
Dugun nethi lauda yaunaren ontasuna,
Dugun bethi lauda yaunaren ontasuna.