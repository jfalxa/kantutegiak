---
id: ab-1364
izenburua: Ohore, Amodio
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001364.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001364.MID
youtube: null
---

Ohore, amodio,Sakramendu sainduan,
Yesus Yaunari,
Ohore, amodio, Orai eta bethi!
Yesus handia, hor goibeldua,
Fedeaz zaitut hemen ikusten,
Maithatzen zaitut eta adoratzen
Aldare hortan yauna amultsua.