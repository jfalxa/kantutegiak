---
id: ab-1326
izenburua: Yauna, Egin Zare Gizon
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001326.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001326.MID
youtube: null
---

Yauna, egin zare gizon,
Amodioak garhaiturik
Eta guk merezi gaberik,
Bethi zare guretzat on.
Ene bihotza da bozten,
Zeren ongi gaitutzu
Maithatu lehenik
Zerutik lurrerat yautsirik,
orai nik ordainez
zinez zaitut maithatzen,
orai nik, ordainez
zinez zaitut maithatzen.