---
id: ab-1323
izenburua: Kristau, Gazte Maitea
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001323.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001323.MID
youtube: null
---

Kristau gazte maitea,
zuretzat naiz mintzo,
Adizkitzu gogotik
zembait solas gozo.
Zerurako bidea
Zaitzu irakasten,
Eman zaite gogotik,
zu, haren ikasten.