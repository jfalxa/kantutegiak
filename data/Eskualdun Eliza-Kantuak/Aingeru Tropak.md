---
id: ab-1366
izenburua: Aingeru Tropak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001366.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001366.MID
youtube: null
---

Aingeru tropak
zituen kreatu
Yaungoiko onak
Eta ere deithu.
Gure gidatzeko
Eta beiratzeko, beiratzeko
Zeruko bidean,
Etsaien artean,
Aingeru tropak.