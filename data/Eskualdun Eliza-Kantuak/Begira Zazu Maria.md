---
id: ab-1431
izenburua: Begira Zazu Maria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001431.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001431.MID
youtube: null
---

Begira zazu Maria,
Baiona, zure herria,
Begira zazu Maria,
Baiona, zure herria.
Zu zare, zu, o Maria!
Sokorri miragarria
Girichtinoen argia,
oi! Izar maithagarria
Bai, zuri gure begia
Beha dago atzarria.