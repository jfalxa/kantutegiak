---
id: ab-1411
izenburua: Zeru Lurrak, Has
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001411.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001411.MID
youtube: null
---

Zeru lurrak, has zaitezte
Bozkarioz kantatzen,
zeren orai gaua duen
Iguzkia argitzen.
Duela lau mila urthe,
izar igurikiak, Izar igurikiak
Ditu lurretik urruntzen
Itzal, hedoi guziak,
Itzal, hedoi guziak.