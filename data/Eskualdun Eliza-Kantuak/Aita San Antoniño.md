---
id: ab-1470
izenburua: Aita San Antoniño
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001470.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001470.MID
youtube: null
---

Aita San Antoniño
Urkiolakua
askoren biotzeko
santu debotua
Askok egiten deutso
San Antoniñori
Egun batian iuan
Bestian etorr.