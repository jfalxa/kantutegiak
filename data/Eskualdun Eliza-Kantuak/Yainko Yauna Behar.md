---
id: ab-1327
izenburua: Yainko Yauna Behar
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001327.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001327.MID
youtube: null
---

Yainko yauna behar duzu
bakharrik adoratu
Hura da gure yabea,
gure kreatzailea,
Salbatzeko behar diren
grazien emailea, grazien emailea.