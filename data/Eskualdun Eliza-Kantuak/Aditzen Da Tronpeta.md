---
id: ab-1317
izenburua: Aditzen Da Tronpeta
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001317.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001317.MID
youtube: null
---

Aditzen da trompeta latzagarria,
Ethorri da Yaunaren eguna:
Egun trichte, egun lotsagarria
Bai egun guzien handiena.
Aditzen da trompeta latzagarria,
Ethorri da Yaunaren egun.