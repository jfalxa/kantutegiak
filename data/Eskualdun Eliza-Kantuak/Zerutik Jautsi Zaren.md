---
id: ab-1400
izenburua: Zerutik Jautsi Zaren
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001400.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001400.MID
youtube: null
---

Zerutik yautsi zaren,
bitima garbia,
zu zare sakramendu,
Zu zare ostia;
Arimen yanharia.
Sakrifizioa;
Bata nola bertzea,
Jesus dibinoa.