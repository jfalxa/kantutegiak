---
id: ab-1365
izenburua: Oi Mirakulu Guziz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001365.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001365.MID
youtube: null
---

Oi! Mirakulu guziz
Espantagarria!
Ogiaren iduriz Jesus estalia!
Hura dut adoratzen
Aldare gainean,
Hura bera dut yaten
Komunionean.