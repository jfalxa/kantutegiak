---
id: ab-1416
izenburua: Inozenten Ama Onak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001416.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001416.MID
youtube: null
---

Inozenten Ama onak
Guziz ziren harritu,
Soldadoak zirenean
Bethleemerat hedatu.