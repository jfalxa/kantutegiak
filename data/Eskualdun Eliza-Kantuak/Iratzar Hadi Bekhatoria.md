---
id: ab-1450
izenburua: Iratzar Hadi Bekhatoria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001450.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001450.MID
youtube: null
---

Iratzar hadi, bekhatoria,
Orena diguk helthia;
Phitzezak gogona,
kountre duk jinkona;
Ordu diano egitzak
Harekilako bakiak.