---
id: ab-1388
izenburua: Kanta Zagun Guziek
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001388.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001388.MID
youtube: null
---

Kanta zagun guziek
ahalik gorena,
Lauda zagun Maria
Andre handiena.
Yakin zuen zerutik
Zer zen gerthaturen:
Birjina gelditurik
Zela amaturen,
Ohora dezagun zeruko Erregina,
yunta tzagun botzak,
Errepikaz kanta Mariaren izena
Kanta zagun: Biba Yainkoaren Ama!
Kanta zagun: Biba yainkoaren Ama.