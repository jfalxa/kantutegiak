---
id: ab-1472
izenburua: Andra Mariari-Erdu Goazan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001472.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001472.MID
youtube: null
---

Erdu goazan guztiok lorakaz
aldra baten Ama,
Ama doguta
opaten lorak Mariari.