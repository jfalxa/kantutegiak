---
id: ab-1383
izenburua: Dugun Alegranzietan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001383.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001383.MID
youtube: null
---

Dugun alegranzietan
kanta yesusen sortzea,
Gure ahal guzietan
Ohora yainko semea;
O Yesus! Zure graziak
Kambia gaitzan guziak.