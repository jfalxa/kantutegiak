---
id: ab-1398
izenburua: Zer Zorion! Oi Hau
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001398.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001398.MID
youtube: null
---

Zer zorion! Oi! Hau da ohorea
Horra, horra
Zeruko yainkoa.
Zato zato, ene Jesus maitea,
zu zare, zu, ene ontasuna.
Oi! Zer dohain ezin prezatuzkoa!
Huna Jesus, ene zoriona,
Jesus, ene zorion.