---
id: ab-1288
izenburua: Zato, Izpiritua, Zato.
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001288.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001288.MID
youtube: null
---

Zato, izpiritua,
zato gu laguntzera,
Othoi! Zato, yauts zaite
gure bihotzetara,
Othoi! Zato, yauts zaite
gure bihotzetara.