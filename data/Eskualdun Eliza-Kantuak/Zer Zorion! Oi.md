---
id: ab-1405
izenburua: Zer Zorion! Oi
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001405.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001405.MID
youtube: null
---

Zer zorion! Oi! Hau da fagorea!
Ikusten dut Yesus maitea,
Zer zorion!
Horra zerutik yautsia, neregatik,
Zer zorion.