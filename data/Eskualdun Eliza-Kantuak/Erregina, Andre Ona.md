---
id: ab-1283
izenburua: Erregina, Andre Ona
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001283.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001283.MID
youtube: null
---

Erregina, Andre ona,
gure laguntza,
gure esperantza zutan dugu.
Entzun zazu, ama maitea,
Othoi! Gure galdea.