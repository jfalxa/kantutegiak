---
id: ab-1289
izenburua: Bekatu Gaichtoari Ihes
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001289.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001289.MID
youtube: null
---

Bekhatu gaichtoari
ihes egiteko,
Bide zehiar guzien
bethikotz uzteko,
Gogoan har ditzagun
gure azken finak;
Hola yoanen zaizkigu
gaizkirako minak.
Gogoan har ditzagun
gure azken finak;
Hola yoanen zaizkigu
gaizkirako minak.