---
id: ab-1314
izenburua: Yesus Onaren Bihotz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001314.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001314.MID
youtube: null
---

Yesus onaren
Bihotz maitea,
zuzare neure guzia;
Bethi da zure ganat izanen
Bakharrik ene lehia.
Bethi da zure ganat izanen
Bakharrik ene lehia.
Nork du erranen ez dakiela
Bihotz hortarat bidia?
Soldaduaren lantzak daroku
ideki hango athia.