---
id: ab-1355
izenburua: Egorri Kalitza
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001355.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001355.MID
youtube: null
---

Egorri kalitza, Jesus, duzu hartzen,
Eroria zaude manu dorpheari
Ni ere natzaio bihotz guziz yartzen,
Ner'atsegabetan zure nahiari.