---
id: ab-1393
izenburua: Josep Fagora Gaitzatzu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001393.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001393.MID
youtube: null
---

Yosep, fagora gaitzatzu,
Zeruan gora zare, zu;
Yosep, fagora gaitzatzu,
Zeruan gora zare, zu.
Egungo phesta huntan,
Zure laudatzeko,
Ditugu, Josep, betan
Bozak yuntatuko.