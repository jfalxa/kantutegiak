---
id: ab-6043
izenburua: Jainko Jauna Behar
kantutegia: Eskualdun Eliza-Kantuak
partitura: null
midi: null
youtube: null
---

Jainko jauna behar duzu
bakharrik adoratu.
Gauza guzien gainetik
hura bera maithatu.
Hura da gure yabea,
gure Kreatzailea,
Salbatzeko behar diren
grazien emailea.