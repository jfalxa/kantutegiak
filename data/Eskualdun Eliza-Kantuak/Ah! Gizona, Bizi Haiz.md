---
id: ab-1285
izenburua: Ah! Gizona, Bizi Haiz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001285.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001285.MID
youtube: null
---

Ah! Gizona, bizi haiz itsutua,
Urruntzen haiz Jaunaren legetik.
Azken finak, utzekotz bekhatua,
Galdu behar ez dituk gogotik.