---
id: ab-1454
izenburua: Beude Bazter Egun
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001454.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001454.MID
youtube: null
---

Beude bazter egun
Goure tristurak;
Jesusez ditzagun
Betha gogoak.
Jesus kristo,
Hanbat houn zirade,
Gouretako Sorthu beitzerade;
Kreaturegatik Soffritu duzu
Eta orogatik zu hil zerade.