---
id: ab-1343
izenburua: Yainkoaren Ama
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001343.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001343.MID
youtube: null
---

Yainkoaren ama,
Ama guztiz ona,
Zaitzagun maitha bethi, bethi;
Zaitzagun maitha bethi, bethi;
Zaitzagun maitha bethi, bethi;
Gu ganik omaiak
Onhets ditzatzu;
Gu ganik lanyerak
Urrun ditzatzu:
Jesusek bethi
Zu entzuten zaitu,
Bethi, bethi.