---
id: ab-1376
izenburua: Hau Da Gure Itsumendua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001376.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001376.MID
youtube: null
---

Hau da gure itsumendua,
zerk gaitu bada zoratzen?
Ahantzirik salbamendua,
Mundua dugu maithatzen.
Lurreko kargu ohoreak
Dire ephe gutitako,
Hango izaite, ohoreak
zaizkit khearen pareko.
Hango izaite, ohoreak
zaizkit khearen pareko,
Khearen pareko,
Khearen pareko.