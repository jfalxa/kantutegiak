---
id: ab-1458
izenburua: Erregeak Zaudezela
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001458.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001458.MID
youtube: null
---

Erregeak zaudezela
Haurraren adoratzen.
ArrApostuaren beha
Herodes zen unhatzen.