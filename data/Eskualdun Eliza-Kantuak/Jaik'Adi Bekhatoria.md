---
id: ab-1441
izenburua: Jaik'Adi Bekhatoria
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001441.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001441.MID
youtube: null
---

Jaik'adi bekhatoria,
Heldu beitzeik tenoria;
Begireik, gaichoa,
Kontra duk Jinkoa,
Ordu deno eginitzak
Harekilanko bakiak.