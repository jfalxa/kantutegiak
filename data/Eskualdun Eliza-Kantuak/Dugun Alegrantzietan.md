---
id: ab-1430
izenburua: Dugun Alegrantzietan
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001430.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001430.MID
youtube: null
---

Dugun alegranzietan
kanta Jesusen sortzea,
Gure ahal guzietan
Ohora Jainko Semea;
O Jesus! Zure graziak
kambia gaitzan guziak.
O Jesus! Zure graziak
kambia gaitzan guzia.