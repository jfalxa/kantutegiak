---
id: ab-1387
izenburua: Jesus Ona, Noizbeit
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001387.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001387.MID
youtube: null
---

Yesus ona,
Noizbeit zure oinetan,
Gure hutsez
gaude ahalketan.
O zorigaitz handiena!
Arbuiatu zaitugu,
Yesus ona.