---
id: ab-1453
izenburua: Birjina Gaztetto Bat Zegoen
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001453.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001453.MID
youtube: null
---

Birjina gaztetto bat zegoen
Kreazale Jaonaren othoitzen,
Nouiz et'ainguru bat lehiatuki
Beitzen zelutik jaitchi Mintzatzera haren.