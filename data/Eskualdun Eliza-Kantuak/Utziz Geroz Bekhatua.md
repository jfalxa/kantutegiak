---
id: ab-1390
izenburua: Utziz Geroz Bekhatua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001390.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001390.MID
youtube: null
---

Utziz geroz bekatua,
Bertze bat naiz aurkitzen,
Ene barnea boztua
Berbetan dut senditzen.
Oi! Hau da alegrantzia!
Bakerik gozoenak
Bethe daut bihotz guzia,
Kendurik hango penak.