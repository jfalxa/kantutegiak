---
id: ab-1330
izenburua: Azkenekotz Zure Ganat
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001330.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001330.MID
youtube: null
---

Azkenekotz zureganat,
Yesus, ene bizia,
Amodioak dakarke
ene bihotz guzia.
Ez dut nehun sosegurik
zu gizatu beharrez;
ene baitharik banoha,
Zu maithatzeko kharrez,
zu maithatzeko kharrez.
Othoi! Bada, ez hiltzeko,
Jesus, ene bizia,
Zu bethi maitha zaitzadan
egidazu grazia.
Othoi! Bada, ez hiltzeko,
Jesus, ene bizia,
Zu bethi maitha zaitzadan
egidazu grazia.