---
id: ab-1402
izenburua: Zure Kontra Naiz Altchatu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001402.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001402.MID
youtube: null
---

Zure kontra naiz altchatu,
Zeru lurren Nausia,
Zure kontra naiz harmatu
Hau da atrebentzia!
Oi! Nola, oi! nola
mundu huntan kontsola.