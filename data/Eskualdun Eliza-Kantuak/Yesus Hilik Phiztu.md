---
id: ab-1310
izenburua: Yesus Hilik Phiztu
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001310.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001310.MID
youtube: null
---

Jesus hilik phiztu,
Oi! Zer bozkarioa!
Ni ere berekin
phiztu nau grazian.
O zer ezagutza,
O zer amodio
Bihurturen diot
bizitze berrian.