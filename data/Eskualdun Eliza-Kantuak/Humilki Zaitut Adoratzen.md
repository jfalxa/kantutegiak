---
id: ab-1443
izenburua: Humilki Zaitut Adoratzen
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001443.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001443.MID
youtube: null
---

Humilki zaitut adoratzen,
ahuspez zure oinetan.
Jauna zuk nauzu, ni laguntzen
Behar ordu guzietan.