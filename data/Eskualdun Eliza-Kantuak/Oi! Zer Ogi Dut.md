---
id: ab-1316
izenburua: Oi! Zer Ogi Dut
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001316.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001316.MID
youtube: null
---

Oi! Zer ogi dut ikusten,
Gure aldaretara,
Zeru gorenetik yausten,
arimen bazkatzera!
Bihotz khartsua!
Amultsua!
Ene yanhari zare bilhakatzen,
Oradainez nola zaitut maithatzen.