---
id: ab-1286
izenburua: Kanta Arin Mundutarrak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001286.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001286.MID
youtube: null
---

Kanta arin mundutarrak
Suntsi beitez bethetan.
Sendimendu kaltekorrak
hil beitez bihotzetan.
Abia gaiten laudatzen
Erregeen Erregea,
Baltsan lehiaz kantatzen
Eman duen legea (bis).