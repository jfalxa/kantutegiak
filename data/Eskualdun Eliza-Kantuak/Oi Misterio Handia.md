---
id: ab-1421
izenburua: Oi Misterio Handia
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001421.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001421.MID
youtube: null
---

Oi misterio handia!
Yaungoikoa estalia!
Aldarearen gainean,
Ostia chimple batean.