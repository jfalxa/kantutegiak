---
id: ab-1302
izenburua: Dezagun Kanta Gogotik
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001302.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001302.MID
youtube: null
---

Dezagun kanta gogotik,
Gaiten guziak bozkaria;
Gure salbatzeagatik,
Yesus eman duzu bizia,
Yesus! Eman duzu bizia.
Dezagun kanta gogotik,
dezagun kanta gogotik.