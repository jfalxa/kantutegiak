---
id: ab-1338
izenburua: Zure Ganat, Yauna
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001338.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001338.MID
youtube: null
---

Zure ganat, Yauna,
Ene yabe ona,
Itzultzen naiz bihotzez
Zu gabe lurrean,
Bethi gaizkipean,
Ibili naiz, ibili naiz
Ibili naiz trebes.