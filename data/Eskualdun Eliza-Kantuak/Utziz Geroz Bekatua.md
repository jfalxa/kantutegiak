---
id: ab-1344
izenburua: Utziz Geroz Bekatua
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001344.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001344.MID
youtube: null
---

Utziz geroz bekatua,
Bertze bat naiz aurkitzen,
Ene barnea boztua
Berbetan dut senditzen.
Oi! Hau da alegrantzia!
Bakerik gozoenak
Bethe daut bihotz guzia,
Khendurik hango penak.