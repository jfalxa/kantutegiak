---
id: ab-1325
izenburua: Uholde Baten Pare
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001325.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001325.MID
youtube: null
---

Uholde baten pare, orai bekatuak,
Gaindiz bazter guziak ditu hondatuak.
Non dire fededunak?
Non dire yustuak?
Azken eguna hurbil
Othe du munduak?
Othoi! Ama maitea,
Urkikal zaizkigu,
Yaunaren haserrea,
othoi! Ezti zazu.