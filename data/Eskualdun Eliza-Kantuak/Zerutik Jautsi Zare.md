---
id: ab-1462
izenburua: Zerutik Jautsi Zare
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001462.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001462.MID
youtube: null
---

Zerutik yautsi zare,
bitima garbia,
zu zare sakramendu,
Zu zare ostia;
Arimen janharia.
Sakrifizioa;
Bata nola bertzea,
Jesus dibinoa.