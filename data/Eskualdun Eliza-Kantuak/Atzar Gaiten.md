---
id: ab-1419
izenburua: Atzar Gaiten
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001419.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001419.MID
youtube: null
---

Atzar gaiten, atzar lotarik,
Gau huntan da Jesus sortzen,
Amodioak garhaiturik
Guregati de ethortzen
Gu zerurat nahiz altchatu
Jesus Jausten da lurrerat,
Heldu da gu nahiz salbatu,
Gorphutz hilkor bat hartzerat.