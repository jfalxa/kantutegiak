---
id: ab-1391
izenburua: Jesusen Bihotz Maite
kantutegia: Eskualdun Eliza-Kantuak
partitura: null
midi: null
youtube: null
---

Yesusen bihotz maite, adoragarria,
Gure bihotzak baite
Zu ganat lehia:
Zu gabe ezin date
Dohatsu bizia;
Gu maitez, sutan zauden
bihotz sakratua
Bethi zare izanen
Gutaz maithatua.