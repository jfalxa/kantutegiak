---
id: ab-1447
izenburua: O Josep Photeretsia
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001447.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001447.MID
youtube: null
---

O Josep photeretsia,
Iguz'ontsa bizitzia
eta saintuki hiltzia.
Dohainetan handiena
Gizonak ukhan diana,
Izan da San Josepena.