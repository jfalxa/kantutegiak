---
id: ab-1464
izenburua: Arropa Churi Ederra
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001464.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001464.MID
youtube: null
---

Arropa churi, ederra,
Yaun onaren ohoretan,
yende onak,
yauntz dezagun,
Bozkarioz, Bazkoetan.