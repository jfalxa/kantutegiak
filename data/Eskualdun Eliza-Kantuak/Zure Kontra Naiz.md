---
id: ab-1335
izenburua: Zure Kontra Naiz
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001335.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001335.MID
youtube: null
---

Zure kontra naiz altchatu,
Zeru lurren Nausia,
Zure kontra naiz harmatu
Hau da atrebentzia!
Oi! Nola, oi! nola
mundu huntan kontsola.