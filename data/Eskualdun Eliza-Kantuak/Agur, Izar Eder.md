---
id: ab-1474
izenburua: Agur, Izar Eder
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001474.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001474.MID
youtube: null
---

Agur, izar eder,
Birjina maitea,
Agur, Ama gozo,
Zeruko atea,
Agur, Ama gozo,
Zeruko atea.