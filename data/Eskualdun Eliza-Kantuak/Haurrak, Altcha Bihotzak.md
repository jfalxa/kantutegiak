---
id: ab-1370
izenburua: Haurrak, Altcha Bihotzak
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001370.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001370.MID
youtube: null
---

Haurrak, altcha bihotzak zuen yainkoari
Humilia zaitezte Jesus maiteari.