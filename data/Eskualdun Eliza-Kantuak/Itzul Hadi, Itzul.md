---
id: ab-1290
izenburua: Itzul Hadi, Itzul
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001290.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001290.MID
youtube: null
---

Itzul hadi, itzul, bekhatorea,
Jainko jaunak deitzen hau arraiki;
Estimazak hitaz duen galdea,
Eta emok bihotza osoki,
Eta emok bihotza osoki.