---
id: ab-1308
izenburua: Adoratzen Zaitut, Berbo
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001308.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001308.MID
youtube: null
---

Adoratzen zaitut,
Berbo dibinoa,
Zeren egin zaren enegatik.
Eta zuri dautzut ohore osoa,
Amatu zarena
Birjina egonik,
Birjina egonik.