---
id: ab-1404
izenburua: Jesus Maitea Zen
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001404.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001404.MID
youtube: null
---

Jesus maitea zen azken afarian,
Bera eman nahiz lehia handian.
Guretzat zelarik hiltzerat abian,
Amodioz zuen bihotza gaindian.
Adora dezagun Jesus yainko yauna,
Sakramendu huntan gurekin dagona.