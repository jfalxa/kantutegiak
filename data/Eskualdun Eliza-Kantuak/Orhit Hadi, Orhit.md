---
id: ab-1439
izenburua: Orhit Hadi, Orhit
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001439.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001439.MID
youtube: null
---

Orhit hadi, horit hiltziaz
Sarri hizate lurpian.
Egun bizkorhiz mundian
Eta bihar hilik benturaz
Helas! Egun ederrena
Hanitzetan duk azkena.