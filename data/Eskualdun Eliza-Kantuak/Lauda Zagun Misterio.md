---
id: ab-1346
izenburua: Lauda Zagun Misterio
kantutegia: Eskualdun Eliza-Kantuak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001346.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001346.MID
youtube: null
---

Lauda zagun misterio handia,
Yaiko Yauna ostian estalia.
Zuek, aingeruak, Zeruko gorthea,
Adora zazue gure Erregea,
gure Erregea.