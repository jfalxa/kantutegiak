---
id: ab-698
izenburua: Kobesioniaz
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000698.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000698.MID
youtube: null
---

Persouna batek, beste orduz,
Egin zian bekhatu ichilian;
Elas! haren ez kobesatuz
Egun dago, egun dago,
ifernu beltzian.