---
id: ab-692
izenburua: Beneditzioneko
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000692.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000692.MID
youtube: null
---

Goure Jaona ostia saintu hontan
Gorderik gizon guzien hounetan,
Bihotz umil batez dezagun adera,
Eta egiazki maitha houra bera,
maitha houra bera.