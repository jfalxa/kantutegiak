---
id: ab-697
izenburua: Arima Konbertitu Sendimentiak Khurutchiaren Aitzinian
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000697.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000697.MID
youtube: null
---

l.- Ene arrerosteko
Kruzifikatu, zirena.
Ingrat niz zouretako,
Bena, othoi, pharka Jaona;
ah! orhit zite odola
or ichouri duzula.

- Ene gastigatzeko
Sobera sujet duzuna.