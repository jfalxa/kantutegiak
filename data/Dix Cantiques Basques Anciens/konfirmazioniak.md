---
id: ab-693
izenburua: Konfirmazioniak
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000693.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000693.MID
youtube: null
---

Ezpiritu Santia, Jaits zite goure gana,
Ezar goure bihotzak amorioz bethiak.