---
id: ab-691
izenburua: Ama Birjinaren Bestez
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000691.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000691.MID
youtube: null
---

Salutatzen zutut Maria,
grazia oroz bethia,
Jinkoaren Ama maitia,
Eta Birjinen gloria.