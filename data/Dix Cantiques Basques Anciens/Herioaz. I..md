---
id: ab-699
izenburua: Herioaz. I.
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000699.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000699.MID
youtube: null
---

l.- Gizona noun duk zuhurtzia?
Zer! eztakik
Hugunt ezak mundu erhoa
Orai danik;
ah! eztuk hurrun herioa
Hire ganik.

- Eztela deus ere bizia
Khebat baizik.