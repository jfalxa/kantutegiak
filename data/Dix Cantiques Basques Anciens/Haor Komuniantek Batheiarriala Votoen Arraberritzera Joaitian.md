---
id: ab-694
izenburua: Haor Komuniantek Batheiarriala Votoen Arraberritzera Joaitian
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000694.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000694.MID
youtube: null
---

l.- Bagoatzu batheiuko uthurri Saintiala
Arnegatzen dut,
Bihotzez dut Satani, haren solaz erhouk eizten,
Zelia lurra, enetzat hartzen.

- Khiristitu gineneko votoen berritzera.