---
id: ab-700
izenburua: Herioaz. Ii.
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000700.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000700.MID
youtube: null
---

Ala beita ikhara garri
Kriminel baten hiltzia!
Adio dero mundia ri
Erraiten, esteiaria!
Joaiteko daunatieki
Sekulakoz erratzera.