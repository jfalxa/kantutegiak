---
id: ab-695
izenburua: Eguberriz
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000695.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000695.MID
youtube: null
---

l.- Khanta beza bitoria
Boztarioz mundiak.
Gaitza zen haren aidia,
Bena goithu Mesiak;
Ikharazi ifernia
Salbazale sorthiak.

- Phorrokatu du buria
Heren suge handiak.