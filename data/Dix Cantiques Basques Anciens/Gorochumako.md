---
id: ab-696
izenburua: Gorochumako
kantutegia: Dix Cantiques Basques Anciens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000696.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000696.MID
youtube: null
---

Iratzar hadi bekhatoria,
Orai duk, orai ordia.
Gogomak, gaichoa,
kontre duk Jinkoa;
ordu diano egin itzak
Harekilako bakiak.