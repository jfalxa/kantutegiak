---
id: ab-3486
izenburua: Godalet - Dantza
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003486.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003486.MID
youtube: null
---

Godalet danza. }
Godalet danza. } (berriz)
Besta hunen acabatzeko }
Emanen dugu phikos. } (berriz)
Godalet hunen ungurian danza famatu ederrena. } (berriz.