---
id: ab-3478
izenburua: Guernikako Arbola
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003478.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003478.MID
youtube: null
---

Guernikako arbola }
Da bedeinkatuba }
Eskualdunen artean }
Guztiz maitatuba: } (berriz)
Eman ta zabal zazu }
Munduban frutuba; } (berriz)
Adoratzen zaitugu, }
Arbola santuba. } (berriz)

Mila urthe inguru da }
Ezaten dutela }
Jainkoak jarri zubela }
Guernikan arbola: } (berriz)
Zaude bada zutikan }
Orain da dembora. } (berriz)
Eroitzen bazera }
Arras galdu gera. } (berriz)

Arbolak erantzun du }
Kuntuz bizitzeko }
Eta bihotzetikan }
Jaunari eskatzeko: } (berriz)
Gerrarik nai ez dugu }
Pakea bethiko; } (berriz)
Gure lege zuzenak }
Emen maitatzeko. } (berriz.