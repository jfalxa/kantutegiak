---
id: ab-3482
izenburua: Glu Glu
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003482.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003482.MID
youtube: null
---

Biba Rioja, Biba Napara
haren me onaren itztarra.
Emen guztiok anayak gera,
ustu desagun Pitcharra:
Glu glu glu glu
Glu glu glu glu
Glu glu glu glu glu glu glu glu glu glu
Glu glu glu glu
Glu glu glu glu
Glu glu glu glu glu glu glu glu glu glu.