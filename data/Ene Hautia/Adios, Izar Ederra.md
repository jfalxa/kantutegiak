---
id: ab-3464
izenburua: Adios, Izar Ederra
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003464.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003464.MID
youtube: null
---

Adios, izar ederra, adios izarra!
Zu zare aingerua munduan bakharra!
Aingeruekin, (berriz) zaitut komparatzen. >
Zembat maite zaitudan ez duzu phensatzen! > (berriz)

Izan naiz Araguan etz Kastilloan,
Hitz batez erraiteko España guzian;
Ez dut ikhusi (berriz) zu bezelakorik, >
Nafarroa guzian zaude famaturik. > (berriz)

Jarrikitzen ninduzun izar eder hari,
Nola mariñel ona bere orratzari.
Jende onak atentzione ene harrazoi huni: >
Etzieztela fida amodioari! > (berriz)

Amodioa duzu arrosaren pare
Usaina badu eta ondoan arhantze;
Maitia, ni enainte egon zu gana jin gabe >
Hil behar banu ere hirur egun gabe! > (berriz.