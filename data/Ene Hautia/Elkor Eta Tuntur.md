---
id: ab-3481
izenburua: Elkor Eta Tuntur
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003481.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003481.MID
youtube: null
---

Chaz hil zerautan senhara
Nik nahi nuen bezala.
Itzutu zen, maingutu zen,
Elkortu zen, kunkurtu zen.
Zahara zen hil bedi:
Etzuen balio lauziri.

Trilili eta tralala
Kantu guzien ama da.
Nik ogi eta chingarra,
Zuk idi baten adarra.
Trala, trala, tralala.
Tralala, tralala, tralala.