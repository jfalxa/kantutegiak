---
id: ab-3484
izenburua: Kinkiri - Kunkuru
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003484.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003484.MID
youtube: null
---

Eran dut eranen et' erango,
ez naiz ichilik egongo.
Plaza untako dama gaztiak,
ez dira monja sartuko.
A la Kinkiri a la Kunkuru }
A la kinkiri, Kunkuru kanta,}
A la Kinkiri kunkuru kanta, }
Gubezala kuk dira, hola. } (berriz.