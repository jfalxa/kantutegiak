---
id: ab-3485
izenburua: Adios, Ene Maitia
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003485.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003485.MID
youtube: null
---

Adios, ene maitia, adios sekulako!
Nik eztit beste phenarik, maitia , zuretako, }
Zeren uzten zutudan haiñ libro bestentako! } (berriz)

Zertako erraiten duzu adio sekulako.
Uste duzia nik eztudala amodio zuretako. }
Zuk nahi balin banaizu enukezu bestentako.} (berriz.