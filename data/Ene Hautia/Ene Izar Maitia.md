---
id: ab-3465
izenburua: Ene Izar Maitia
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003465.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003465.MID
youtube: null
---

Ene izar maitia
Ene charmagarria,
Ichilik zur' ikhustera
jiten nitzauzu leihora.
Koblatzen dudalarik >
Zaude lokharturik:
Gauazk' ametsa bezala
Ene kantua zauzula! > (berriz)

Zuk ez nuzu esagutzen,
Hori ere zaut gaitzitzen;
Ez duzu ene beharrik
Ez eta acholarik.
Hil edo bizi nadin >
Zuretako berdin!
Zu aldiz, maite Maria,
Zu zare ene bizia! > (berriz)

Amodiozko phena zer zen
Oraino ez nakien!
Orai ez nuzu biziko
Baizik zu maithatzeko.
Norat den ichurkia >
Hara juaiten da hura:
Orobat ni, maitenena,
Jiten niz zure gana! > (berriz.