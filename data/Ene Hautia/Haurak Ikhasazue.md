---
id: ab-3467
izenburua: Haurak Ikhasazue
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003467.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003467.MID
youtube: null
---

Haurak ikhasazue eskuaraz mintzatzen, >
Ongi pilotan eta oneski dantzatzen. > (berriz)
la, la, la,...Olle (cri)

Eztut ez ikhusiko nere play' ederra; >
Adios Donibaneko itxaso bazterra. > (berriz)
la, la, la,...

Adios ene maitea bainan ez bethikotz, >
Laster ethorriko naiz zurekin bizitzeko. > (berriz)
la, la, la, ...