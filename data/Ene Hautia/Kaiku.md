---
id: ab-3473
izenburua: Kaiku
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003473.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003473.MID
youtube: null
---

Nere andrea andre ona da,
gobernu ona du hauzuan;
Hartzen duelarik bere alaba
Marikattalin altzuan:
(Errepika): Aizazu! Aizazu. Zer nah'uzu!
Gerore horrela munduan ere
biak biziko geraregu,
Baldin bazara kontentu,
Baldin bazara kontentu.

Nik bethi freskotchorik tabernakua
Sototchuan deraukat ardo gozua.
Ai! zer kontentu! ai! zer alegre!
Ezkaintzen dauzut, nere maitia,
Arraultz etchuak eta kaiku esnia,
arraultz etchuak eta kaiku esnia.
Kaiku, kaiku, kaiku, kaiku, kaiku, kaiku esnia,
Arraultz etchuak eta kaiku esnia.

San Blas' alderat ninduelarik, makhiltcho baten gainean (berriz)
Arhantze beltz bat sarthu zitzautan, uspela nuien oinean!
(Errepika): Aizazu.