---
id: ab-3480
izenburua: Garruzen Merkhatu
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003480.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003480.MID
youtube: null
---

Ostiraletan duzu Garruzen merkhatu;
Ene maite polita han errekontratu;
Pot bat gald'egin neron chapela eskian }
Biga eman zautadan nigarra begian. } (berriz)

Gero gald'egin neron. "Nigarrez zer duzu?
"Hola changrinatzeko sujetik ez duzu."
Arrapostu eman zautan: Zuhaurrek badakizu; }
"Ene nigar ororen sujeta zira zu." } (berriz.