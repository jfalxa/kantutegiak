---
id: ab-3470
izenburua: Mutchurdinak
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003470.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003470.MID
youtube: null
---

Herriko besta bigarranean,
Berek dakiten chokho batean.
Lau andre, hiru mutchurdin, >
Bat alaguna, jarriak itzalean, >
Harri chabal bat belhaunen gainean >
Hari dire, hari dire trukean. >(berriz)

Nahiz zaro kan azken eskua
Mar, Martinek zuen hasi gudua,
Truk! Jozan Mariak chango >
Katichak chaldun, Marichumek hirua,>
Mari Martin zoratu zain burua, >
Laurarekin hiruaren kheinua. >(berriz)

Ago Maria ichil ichilik
Hik eztun ikhusi neure kheinurik,
Tinta bat edanez geroz, begiak nir nir>
Zer! ez dun ahalgerik! >
Edan nezaken, azkarren hortarik, >
Gathilua, gathilua betherik! >(berriz.