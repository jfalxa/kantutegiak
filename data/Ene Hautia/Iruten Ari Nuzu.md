---
id: ab-3462
izenburua: Iruten Ari Nuzu
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003462.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003462.MID
youtube: null
---

Iruten ari nuzu, kilua gerrian, (berriz)
Ardura dudalarik, (berriz), nigarra begian.(berriz).

Jendek erraiten dute, ezkondu ezkondu: (berriz)
Nik ezdut ezkon minik (berriz) gezurra diote.(berriz)

Ezkon minak dutenak seinale dirade: (berriz)
Mathel hezurrak seko (berriz) koloriak berde. (berriz)

Jendek erraiten dute hal' estena franko, (berriz)
Ene maite pollita (berriz) zur' et' enetako.(berriz.