---
id: ab-3474
izenburua: Lili Bat Ikhusi Dut
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003474.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003474.MID
youtube: null
---

Lili bat ikhusi dut baratze batean
Desiratzen bainuke nere sahetsean.
Lorea ez du galtzen udan, ez neguan }
Haren parerik ez da bertze bat munduan. } (berriz)

Deliberatu nuen gau batez joaitera,
Lili arraro haren eskura hartzera,
Ez bainuen pentzatzen beiratzen zutela }
Gau hartan uste nuen han galtzen nintzela. } (berriz)

Etsemplu bat nahi dut eman munduari
Eta partikulazki jende gazteari:
Gabaz ibiltzen dena ez da zuhurregi, }
Ni begiraturen naiz, eskerrak Jainkoari. } (berriz.