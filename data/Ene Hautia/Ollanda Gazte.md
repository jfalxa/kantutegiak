---
id: ab-3483
izenburua: Ollanda Gazte
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003483.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003483.MID
youtube: null
---

Ollanda gazte pollitño bat gure herrian badugu. > (berriz)
Hegal petik lumaño bat falta ezpalimbalu, >
Munduan aski elizateke ollanda hartaz espantu.> (berriz.