---
id: ab-3477
izenburua: Nere Maitiarentzat
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003477.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003477.MID
youtube: null
---

Ume eder bat ikhusinuben
Donibaneko (Donostiako) karrikan.
Hitz erdisko bat hari erran gabe,
Nola pasatu parean?
Gorputza zuan liraña }
Eta oinak zebiltzan airean }
Politagorik ez dut ikhusi }
Nere begien aurrean. } (berriz)

Gazte guziek galdetzendute
Aingeru hori nundago?
Nere maitea nola deitzenden
Ez du nihukek jakingo.
Ez berak ere ezluke naiko,
Konfiantza hortan nago,
Amorio dun bihotz hoberik
Eskual herrian ez dago.