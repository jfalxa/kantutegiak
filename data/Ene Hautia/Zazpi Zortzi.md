---
id: ab-3466
izenburua: Zazpi Zortzi
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003466.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003466.MID
youtube: null
---

Zazpi zortzi bersu berri >
Nahi nitutzke ezarri. > (berriz)
Gauzak diren bezala >
Esplikatuz orori.
Phena changrin handietan
Orai sartzera nua ni. > (berriz)

Aitak dio semeari >
Utzak utzak neska ori, > (berriz)
Zendako duk etchian hori >
Zeren baita bethi eri.
Utzak utzak neska ori,
Gostarik ere zernahi > (berriz)

Semeak arrapostua >
Ai bainian ezti eztia: > (berriz)
Balinbada eria >
Eztu ez bere nahia,
Nik han dut neure fedia
Inposible zaut uztia. > (berriz.