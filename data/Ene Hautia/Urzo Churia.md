---
id: ab-3468
izenburua: Urzo Churia
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003468.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003468.MID
youtube: null
---

Urzo churia, errazu, norat joaiten ziren zu;
Espainiako mendiak elhurrez betheak dituzu;>
Gaurko zure ostatu gure etchean baduzu. >(berriz)

Ez nu izitzten elhurrak ez eta gauak ilhunak,
Maitea gatik pasa nintzazke gauak eta egunak;>
Gauak eta egunak, desertu eta oihanak. >(berriz)

Urzo churia airean ederrago da mahainean,
Zure parerik ez du arkitzen Espania guzian, >
Ez eta ere Frantzian iruskiaren azpian. >(berriz.