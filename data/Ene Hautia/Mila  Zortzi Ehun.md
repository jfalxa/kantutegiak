---
id: ab-3476
izenburua: Mila  Zortzi Ehun
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003476.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003476.MID
youtube: null
---

Mila zortzi ehun hemeretzian,
Urriaren hillaren bederatzian;
Umore ona nuen horien khantatzian,}
Gazte et alegera, trankil bihotzian; }
Ontasuna frango badut intresian, }
Deusik ez etchean: }
Orai bezain aberats nintzan sortzian. } (berriz)

Nork naki zer nahi erranikan ere,
Bidarraitarra nuzu bai nahi ere.
Etcheko seme ona segurki halere, }
Nahiz baden herrian hobechagorik ere; }
Baditut bortz haurhide, enekin sei dire, }
Oro adizkide.
Dotiaren gainetik samurturen ez gire. } (berriz)

Aita ezkondu zen gure amarekin,
Ama aitarekin, biak elgarrekin,
Orduian gaste ziren bertze zerbaitekin; }
Eta orai zahartu miseriarekin; }
Ontasun igorri bertze haurhidekin, }
Phartebat enekin, }
Enekin baino haboro bertze bortzekin. } (berriz)

Adinen beha gaude, egia erraiteko,
Gure ontasun oreren phartitzeko.
Diru idorra bere bada bizikichko }
Jainkoak daki zembat den bakhotcharen dako; }
Izaiten badugu ez dugu utziko }
Auzokuendako, }
Bainan eztaikugu sakelarik erreko. } (berriz.