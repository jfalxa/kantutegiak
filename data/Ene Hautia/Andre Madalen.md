---
id: ab-3469
izenburua: Andre Madalen
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003469.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003469.MID
youtube: null
---

Egun batean, ni ari nintzen,
andrea ezin ikusirik;
Ezan zidaten: Edena dago
ez egin hari kasurik!
(Errepika)
Andre Madalen! Andre Madalen!
Laurden erdi bat olio!
Andreak zorrak in eta gero,
Jaunak pagatuko dio!

Ene andreak badaki josten
Eta lijatzen taulain gainean
Arno churia saldan emanik,
Maite du guzien gainetik.
(Errepika): Andre Madalen!

Bere gizona utzik etchean
Dagoa bera patar batean,
Indarrak galduak moskorrarekin,
Lurrerat joaiten da behin.
(Errepika): Andre Madalen.