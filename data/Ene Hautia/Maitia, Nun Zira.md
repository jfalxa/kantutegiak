---
id: ab-3463
izenburua: Maitia, Nun Zira?
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003463.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003463.MID
youtube: null
---

Maitia nun zira?
Nik etzutut ikhusten,
Ez berririk jakiten,
Nurat galdu zira? (berriz)
Ala khambiatu da zure deseiña?>
Hitz eman zenereitan,
Ez behin, bai berritan, (berriz)
Enia zinela. (berriz) >

Ohikua nuzu;
Enuzu khambiatu,
Bihotzian beinin hartu,
Eta zu maitatu. (berriz)
Aita jeloskor batek dizu kansatu. >
Zure ikhustetik,
Gehiago mintzatzetik (berriz)
Hak' nizu pribatu. >

Zamarik igañik,
Jin zazkit ikhustera,
Ene konsolatzera,
Aitaren ichilik; (berriz)
Hogoi eta lau urthe bazitit betherik: >
Urthe baten burian,
Nik eztiket ordian (berriz)
Aitaren acholik. .