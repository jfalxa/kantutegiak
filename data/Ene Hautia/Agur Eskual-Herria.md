---
id: ab-3479
izenburua: Agur Eskual-Herria
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003479.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003479.MID
youtube: null
---

Guitara zarcho bat da }
Neretzat laguna }
Onela ibiltzen da }
Artista Eskualduna. } (berriz)
Egun batean pobre
Beste batez Jauna,
Kantiri pasatzen dut
Nik bethi eguna.

Naiz dala Italia }
Oro bat Franzia, }
Bitan billatu dut }
Anitz malicia... } (berriz)
Ikusten badut ere
Nik mundu guzia,
Bethi maitatuko dut
Eskual-Herria.

Jaunak ematen badit }
Neri osasuna, }
Izango det oraindit }
Andregai bat ona; } (berriz)
Emen badut francesa
Interesa duna...
Baña nik naiago dut
Utzik Eskualduna.

Agur Eskual-Herria }
Baña ez bethiko, }
Bost edo sei urtean }
Ez dut ikusiko... } (berriz)
Jaunari eskatzen dut
Gratia emateko,
Nere lur maite ontan
Bizia uzteko.