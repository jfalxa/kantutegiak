---
id: ab-3471
izenburua: Ai, Ai, Muthila
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003471.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003471.MID
youtube: null
---

Don Carlosek omen du ezpata zorrotza, (berriz)
Liberal handi horier mozteko bihotza! (berriz)
Ai, ai, ai,! muthila mozteko bihotza! (berriz)
(Errepika):
Ai, ai, ai, ai, chapela goria (berriz)

Arnegin egin dute lastozko zubia, (berriz)
Hantikan pasatzeko Don Carlos handia.(berriz)
Ai, ai, ai, muthila Don Carlos handia. (berriz)
(Errepika): Ai, ai, ai, ai, chapela goria (berriz)

Biba chapel gorriak, burlia ferdiak, (berriz)
Zaldi batean dathor Don Carlos gurea, (berriz)
Don Carlos maitea, gure erregea! (berriz)
(Errepika): Ai, ai, ai, ai, chapela goria.(berriz)

Azpeitiko neskatchak, gona gorriekin, (berriz)
Ez dute dantzatu nai chapel churiekin. (berriz)
Ai, ai, ai, muthila, chapel churiekin. (berriz)
(Errepika): Ai, ai, ai, ai, chapela goria. (berriz.