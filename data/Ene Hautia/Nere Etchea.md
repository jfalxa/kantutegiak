---
id: ab-3472
izenburua: Nere Etchea
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003472.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003472.MID
youtube: null
---

Ikusten duzu goizean,
Argia hasten denean
Menditto baten gainean,
Etche ttipito, aintzin churi bat >
Lau haiz ondoren erdian, >
Chakhur churi bat athean, >
Ithuriño bat aldean, >
Han bizi naiz ni bakean. >(berriz)

Nahiz ez den gaztelua;
Maite dut nik sor-lekhua
Aiten aitek hautatua.
Etchetik kampo zait iduritzen >
Nombeit naizela galdua; >
Nola han bainaiz sorthua, >
Han utziko dut mundua, >
Galtzen ez badut sensua. >(berriz)

Ez da munduan gizonik
Erregerik ez printzerik,
Ni bezein urusa denik;
Badut andrea, badut semea, >
Badut alaba era nik; >
Osasun ona batetik, >
Ontasun aski bertzetik, >
Zer behar dut gehiago nik? >(berriz)

Ene alaba Kattalin,
Bere hameka urthekin
Ongi doha amarekin;
Begiak ditu amak bezala >
Zeru zola bezin urdin; >
Uste dut demborarekin, >
Oraiko itchurarekin, >
Andre on bat dio egin. >(berriz)

Ene andrea Maria
Ez da andre bat handia
Bainan emazte garbia;
Irri batentzat badut etchean >
Nik behar dudan guzia; >
Galdegiten dut grazia >
Dudan bezala hacia >
Akhabatzeko bizia. >(berriz.