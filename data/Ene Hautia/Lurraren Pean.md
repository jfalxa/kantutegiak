---
id: ab-3475
izenburua: Lurraren Pean
kantutegia: Ene Hautia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003475.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003475.MID
youtube: null
---

Lurraren pean sar nindaiteke, maitia, zure ahalgez!
Bost phentsaketa eginik nago zurekin ezkondu beharrez;
Bortha barnetik zerratu eta bethi khamberan nigarrez, }
Sendimenduak airian eta bihotzetikan dolorez }
Ene changrinez hil erazteko sorthua zinen arauez ! }
(berriz)

Oren hunian sortuia zinen izar ororen izarra!
Zure parerik etzaut jiten neure begien bistara.
Espos laguntzat gald'egin zintudan erran nerauzun bezala }
Bainan zuri ez iruditu zuretzat aski nintzala;
}
Ni baino hobebatekila Jainkoak gertha zitzala! }
(berriz)

Mariñelak juaiten dira itsasorat untziko:
Zure ganako amodioa sekulan ezdut utziko.
Charmagarria, nahi ez giren elgarrekilan biziko, }
Behin maite izan zaitut eta etzaitut hastiatuko: }
Bihotzian sarthu zitzautzat eternitate guziko! }
(berriz.