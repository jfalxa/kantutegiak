---
id: ab-4848
izenburua: Donostiako Iru Damatcho
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004848.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004848.MID
youtube: null
---

Donostiako iru damatcho,
Errenterian dendari,
Josten ere badakite baña
Ardo edaten obeki!
Ta krisketin! krosketin!
Larrosa klabelin!
Ardo edaten obeki.