---
id: ab-4832
izenburua: Aitarik Eztut
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004832.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004832.MID
youtube: null
---

Aitarik eztut eta ama ere zahartu,
Emazte baten beharra etchean badugu;
Zuk hala plazer bazindu nahi zinduzketzu:
Ene desira zer den orai badakizu.