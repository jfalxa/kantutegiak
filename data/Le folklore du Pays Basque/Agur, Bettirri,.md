---
id: ab-4844
izenburua: Agur, Bettirri,
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004844.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004844.MID
youtube: null
---

Agur, Bettirri,
Ongi ethorri!
Bizi ziradeya oraino?
- Bai, bizi naiz eta bizi gogo,
Hartzekoak bil arteraino.