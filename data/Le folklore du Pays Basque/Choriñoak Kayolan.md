---
id: ab-4830
izenburua: Choriñoak Kayolan
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004830.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004830.MID
youtube: null
---

Choriñoak kayolan,
Tristerik du kantatzen;
Duelarikan zer yan, zer edan,
Kampoa du desiratzen:
Zeren... zeren...zeren
Libertatea zoin eder den.