---
id: ab-4846
izenburua: Egun Batean
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004846.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004846.MID
youtube: null
---

Egun batean, ni ari nintzan,
Andrea ezin ikusirik;
Esan zidaten: edana dago!
Ez egin hari kasurik!
Total egiña zagoen, baña
Ezurrak zauzkan osorik;
Galdatu zidan eia etzagon
Tafernan ardo gozorik.
REFRAIN:
Andra Madalen! andre Madalen!
Laurden erdi bat olio!
Andreak zorrak in eta gero.
Jaunak pagatuko dio.