---
id: ab-4835
izenburua: Intchauspeko Alaba
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004835.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004835.MID
youtube: null
---

Intchauspeko alaba, dendaria,
Goizian goiz yostera yoalia;
Nigarretan pasatzen du bidia;
Aprendiza kontsolatzaillia.