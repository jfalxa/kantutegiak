---
id: ab-6060
izenburua: Aurtcho Tchikia (Variante)
kantutegia: Le Folklore Du Pays Basque
partitura: null
midi: null
youtube: null
---

Aur gaichua, lo eta lo!
Logiro on bat dago;
Zuk orain eta nik gero,
Biyok egingo degu lo.