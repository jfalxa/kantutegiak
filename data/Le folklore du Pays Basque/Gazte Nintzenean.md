---
id: ab-4843
izenburua: Gazte Nintzenean
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004843.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004843.MID
youtube: null
---

Gazte nintzenean,
Hogoi urthetan,
Ardura nindabilan
Neskatiletan...
Eta orai aldiz ostatuetan,
Diru guti molsan
Behar orduetan;
Nihork ez naute nahi kompainietan.