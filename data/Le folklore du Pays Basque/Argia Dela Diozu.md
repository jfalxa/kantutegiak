---
id: ab-4837
izenburua: Argia Dela Diozu
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004837.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004837.MID
youtube: null
---

Argia dela diozu:
Gaberdi oraino eztuzu!
Enekilako dembora
Luze iduritzen zaitzu:
Amodiorik eztuzu,
Orai zaitut ezagutu!

Ofizialetan deia
Zure sinheste guzia?
Aitak eta amak ere hala dute gutizia;
Lehen bat'et'orai bertzea: oi! hau phenaren tristia!

Othea lili denean,
Choria haren gainean;
Hura juaiten airean berak plazer duenian:
Zur'eta ner'amodioa hala dabila munduian.

Phartitu nintzan herritik,
Bihotza alegerarik;
Arribatu nintzan herrian, nigarra nuen begian:
Har nezazu sahetsian, bizi naizeno munduian.