---
id: ab-4841
izenburua: Adios, Ene Maitea
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004841.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004841.MID
youtube: null
---

Adios, ene maitea,
Adios sekulako;
Nik eztut bertze penarik,
Maitea, zerutako,
Zeren utzi zintudan
Hain libro bertzendako.