---
id: ab-4836
izenburua: Mendian Zoinen Eder
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004836.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004836.MID
youtube: null
---

Mendian zoinen eder
Epher zango gorri!
Ene maiteak ere
Bertzeak iduri:
Niri hitz eman eta
Gibelaz itzuli.