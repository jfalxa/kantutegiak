---
id: ab-4845
izenburua: Hau Da Ikhazketako
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004845.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004845.MID
youtube: null
---

Hau da ikhazketako mandoaren traza:
Burua phisu eta illea latza,
Itchura gaitza;
Bastapetik dohako zorma eta balsa:
Hautche da salza!
Christorik eztaite aldetik pasa.