---
id: ab-4850
izenburua: Aurtcho Tchikia
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004850.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004850.MID
youtube: null
---

Aurtcho tchikia negarrez dago:
Ama, emazu titia!
Aita gaiztoa tabernan dago,
Pikaro jokalaria.