---
id: ab-4839
izenburua: Lurraren Pean
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004839.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004839.MID
youtube: null
---

Lurraren pean sar nindaiteke, maitea, zure ahalgez;
Bost pentsaketa eginik nago zurekin izan beharrez:
Bortha barnetik zerratu eta bethi gamberan nigarrez,
Sentimenduak airean eta bihotzetikan dolorez
Ene changrinez hilarazteko sorthua zinen arabez.