---
id: ab-4831
izenburua: Ene Izar Maitia
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004831.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004831.MID
youtube: null
---

Ene izar maitia,
Ene charmagarria,
Ichilik zure ikhustera
Yiten nitzaitzu leihora;
Koblatzen dudalarik,
Zaude lokharturik:
Gabazko ametsa bezala,
Ene kantua zaitzula.