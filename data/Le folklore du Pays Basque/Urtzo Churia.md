---
id: ab-4833
izenburua: Urtzo Churia
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004833.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004833.MID
youtube: null
---

Urtzo churia, errazu,
Nora yoaiten zera zu?
Espainiako borthuak oro
Elhurrez betheak ditutzu:
Gaurko zure ostatu
Gure etchean baduzu.