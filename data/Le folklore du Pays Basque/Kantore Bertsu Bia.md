---
id: ab-4838
izenburua: Kantore Bertsu Bia
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004838.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004838.MID
youtube: null
---

Kantore bertsu bia nahi tut kantatu,
Suyet aski tristea baitzaku gerthatu:
Urtzo kolomaño bat tristeki baratu,
Luma bat hegalpetik baitzayo faltatu.