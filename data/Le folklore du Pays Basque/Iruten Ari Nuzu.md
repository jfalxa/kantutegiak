---
id: ab-4842
izenburua: Iruten Ari Nuzu
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004842.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004842.MID
youtube: null
---

Iruten ari nuzu, khilOa gerrian,
Ardura dudalarik nigarra begian.