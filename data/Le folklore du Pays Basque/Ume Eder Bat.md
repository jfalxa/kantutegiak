---
id: ab-4834
izenburua: Ume Eder Bat
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004834.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004834.MID
youtube: null
---

Ume eder bat ikusi nuben
Donostiako kalean;
Itz erditcho bat hari esan gabe
Nola pasatu parean?
Gorputza zuben liraña eta
Oñak zebiltzan airean:
Politagorik eztet ikusi
Nere begien aurrean.