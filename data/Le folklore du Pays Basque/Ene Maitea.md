---
id: ab-4840
izenburua: Ene Maitea
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004840.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004840.MID
youtube: null
---

Ene maitea, barda non zinen,
Nik borthañoa yoitean?
Buruan ere min nuen, eta
Dudarik gabe ohean.