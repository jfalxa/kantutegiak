---
id: ab-4849
izenburua: Buba Niña
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004849.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004849.MID
youtube: null
---

Buba niña,
Bubatto, ñiñatto,
Lo zite bertantto,
Bertantto, fitechko.