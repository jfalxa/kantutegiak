---
id: ab-4828
izenburua: Gernikako Arbola
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004828.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004828.MID
youtube: null
---

Gernikako arbola da bendikatuba,
Eskaldunen artean guziz maitatuba;
Eman da zabalzazu munduan frutuba,
Adoratzen zaitugu, arbola santuba.