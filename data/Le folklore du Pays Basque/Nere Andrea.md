---
id: ab-4847
izenburua: Nere Andrea
kantutegia: Le Folklore Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004847.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004847.MID
youtube: null
---

Nere andrea andre ona da:
Gobernu ona du... auzoan,
Hartzen duelarik bere alaba
Mari-Kattalin altzoan!
Aizazu! zer na'uzu?
Gero're horrela mundu ere
Biak biziko gerare gu,
Baldin bazare kontentu.
Nik bethi freskotchorik tabernakoa
Sototchoan deraukat arno gozoa:
Ai! zer kontetu! ai! zer alegre!
Eskaintzen dautzut, nere maitea,
Arroltzetchuak eta kaiku esnea.