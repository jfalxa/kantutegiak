---
id: ab-80
izenburua: Amodijho Andijha
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000080.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000080.MID
youtube: null
---

Bijotz bacarbat
ta milla banituz
millac neure maitia
emongo neuquizuz:
Bijotz bacarbat dancat
ta milla banituz
millac neure maitia
emongo neuzquizuz.
Entsi milla langoa
bat maite neuria
eta neurian ordez
ecazu ceuria.
Eutsi milla langoa
bat maite neuria
eta neurian ordez
ecazu ceuria ecazu ceuria.