---
id: ab-60
izenburua: San Blas Aldera
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000060.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000060.MID
youtube: null
---

San Blas aldera
nen niñoiala
maquilla batenganian
Arantza belz bat
sartu citzaiden
ospela nuen oñian

Aizazu cernaizu
beti zu beti zu
munduban munduban
munduban ere bijoc
bicico guerade guero
baldin bazera contentu
baldin bazera contentu.

Zu beti fraisco
chomin tabernacua
zu icustera
nator desiatuala.