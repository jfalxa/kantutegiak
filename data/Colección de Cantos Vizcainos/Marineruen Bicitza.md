---
id: ab-77
izenburua: Marineruen Bicitza
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000077.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000077.MID
youtube: null
---

Orain nua cantatzen
neure bicitzia
Mariñeru banaiz da
daquidan guztijha.
Cayatic alturara
ichas zabalian
nola nabillen á remo
chalupen ganian.
(Estrivillo) Ychas andiric
Ychas andiric
izaten bot alturan
nago ni galduric.