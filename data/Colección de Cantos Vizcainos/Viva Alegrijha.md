---
id: ab-53
izenburua: Viva Alegrijha
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000053.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000053.MID
youtube: null
---

Secreto andijha da ta
maribillosua
""Causino""ren liburuban
aguerturicua.
Secreto andijha da ta
maribillosua
""Causino""ren liburuban
aguerturicua.
Ez diamante ez perla
ez urre gorriric
""Alegrencijha"" baño
ez dago oberic.
Ez diamante ez perla
ez urre gorriric
""Alegrencijha"" baño
ez dago oberic.

COPLAS CANTADAS POR LOS CELEBRES PLATEROS DE DURANGO
CAUSINO
1ª
Guasa andia da fa
Marabillosua
Causinoen liburuban
Aguerturicua:
Es diamante es perla
Es urre gorriric
Alegrencia baño
Es dago oberic: (Estribillo)
2ª
Causinoc esatendau
Lenengo tonuban
Alegria dala onena
Bicileco munduban:
Es diamante es perla.etc.
3ª
Escolia ederra
Daucagu munduban
Sensañac alegria
Aurrari canletan:
Es diamante es perla.etc.
4ª
Munduco trilessia
Melanconiña da
Au gueiso modubau
Onan quenduco da:
Es diamante es perla.etc.
5ª
Tristesial mundutic
Lasterqui narua
Bedeinca alegria
Jaunac Emonicua:
Es diamante es perla.etc.
6ª
Gure jaquituria
Isanda ta ysangoda
Esinori gachic eguin
Jaunaren bildurra:
Es diamante es perla.etc.
7ª
Espiritu alegria
Destierru onetan
Balioco dau asco
Consola gailesan:
Es diamante es perla.etc.
8ª
Artu alegria ta
Tristesia ychi
Causinuen liburuba
Beti presente euqui:
Es diamante es perla.etc.
DE LAS MOSCAS
1ª
Orainche veseala
Canlatzera nua
Yru eli generuen
Persecusinoa
Nola dan christau bensat
Udaco plagia
Alemanienzacó
Compara Vaguia
2º
Guesoric daguana
Ondo erruqui dot
Endemas vadago oyan
Euliaquin ynos
Ychico esteutzelaco
Daguan lecuban
Nos edo nos sartubaric
Eulijac aguan
3ª
Gesuac daucan layuna
Aviazen jaco
Erruqui andiagas
Euliac quentzeco
Debalde ibilico as
Usatuten veti
Amaica juago jacos
Amabi etorri
4ª
Espata eulijac dira
Beste generoac
Guisonen pantorrilara
obeto sartuco esdau
Afisionadubac
Biardan toquian
5ª
Alemaninac guezuac
Onec galzen daben
Sergaitic gorpusgustia
Sulatuten deuzen
Egusquien urtetic
Ylunci artian
Ybilten dirialaco
Euracas aldian
6ª
Saman ifiniarren
Arbolen orrijac
¿Ustedosu orregaitic
Dala defenzia?
Orida erruquitasunbak
Javiac orturic
Orregaitic euliari
Ecteuso arduraric
7ª
Mando Euliadogun
Beste generua
Zabala ta biribila
Zapua langua
Veti veti jabilcoda
Guilica tripan
Mandua osticaca
Yfini astian
8ª
Onec diras euliac
Munduan jaijoac
Genero gustijac
Cacas tutecuac
Aimbeste dira munduban
Nun principalmente
Arrainta ta fruta lecubetan
Faltarican bague
9ª
Ormaco miarmia
Veti da Veguira
Aurrian Sase Andibat
Verac yfinita
Pensaurican euliac
Juango jacosala
Vay baleuco onduan
Azucar tontorra
10ª
Egurc daucas euliac
Jaunac ifinita
Orregaitic miarmiaren
Estauco bildurric
Euliac estabe gura
Ormaco Sulua
Espada egusquitan
Ecti lapicua
11ª
Africati etortendira
Chori genero bi
Vata da oputia balza
Vestia azul zuri
Datos Euliac jaten
Gelditu bagaric
Yto escaisan euliac
Amorrus beteri.