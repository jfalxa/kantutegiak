---
id: ab-72
izenburua: Sagar Ederra
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000072.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000072.MID
youtube: null
---

Sagar azal ederra
barruba ustelduric
seimbat dagos munduban
lastimas beteric.

Guero batec jaten dau
sospecha bagaric
pensauric ez daucala
barruban dañuric.