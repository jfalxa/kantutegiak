---
id: ab-89
izenburua: Gaboneco Afari On Bat
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000089.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000089.MID
youtube: null
---

Gabon eguna da egun laburra
erlojuetan bacarric
ez dot euquiten illuntzeraco
belaun bijetan indarric.

Eta bat, eta bi, eta iru eta lau
ardoac moscor... moskortuten dan
eta bat, eta bi, eta iru eta lan
ardoac, ardoac, ardoac
moscortuten dan
bai, bai, moskortuten dan.