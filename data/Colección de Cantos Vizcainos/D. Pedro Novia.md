---
id: ab-70
izenburua: D. Pedro Novia
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000070.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000070.MID
youtube: null
---

Zamacola izan bazan
Diman chito andijha
beste bat aurquitzen dot
Bilbon ez chiquijha.

Au da D.Pedro Novia
Bizcaitar cintzua
bai eta ecandubetan
tuit jaungoi coscua.