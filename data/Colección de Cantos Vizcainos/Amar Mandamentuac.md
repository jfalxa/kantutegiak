---
id: ab-64
izenburua: Amar Mandamentuac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000064.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000064.MID
youtube: null
---

Amar dirá
Jaunaren legueco aguinduac
ceintzuc bihar dituzan
gorde cristiñauac.

Lenengo irurac
dira Jaunen honraraco
eta beste zazpirac
danen oneraco.