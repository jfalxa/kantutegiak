---
id: ab-86
izenburua: Kubaren Defenditzalliai
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000086.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000086.MID
youtube: null
---

Kuba ta Españatarrac
Ondo alkarturik
Ezta beu izten Kuban
nok Bakia kaldurik.
Dua agure zarra
Fusilla arturic
kontuban artu baga
Ez otz ez beroric
bay! Garaitziac
darua Ereuntzakoroetzat
Libertadia dacar
¡viva! gudazari guretzat.

Kubatarren onduban
Katalan jentia

Agurakaz batian

Euskaldun noblia:
Duaz egiten pozik
Al daben guztia

Nekien bildur baga
Zara ta gaztia.

.
CORO

Bera alaba Kuba
Gordeko dau Españak
Odola daben arte
Bere semien zanak

Erdu adiskiak
Gaur ziñ egitera
Leyal defenditzeko
Españan bandera:
Jaunkoikuak bialtzen
Zaitue echera
Pozes jan edan eta
Bakez bicitzera
CORO
Bizi bedi España
Ta ez betor gerrarik
Kuban daguan arte
Biotz on bat bacarrit.