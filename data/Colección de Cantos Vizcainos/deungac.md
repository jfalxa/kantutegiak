---
id: ab-82
izenburua: Deungac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000082.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000082.MID
youtube: null
---

Deungac dabillst gaur munduan
eurac nai daben moduan
Onai guztijai
ill ta bicijai
Onei aldabena ostuten
da arei famia quenduten
Onei aldabena ostuten
ta arei famia quenduten
Onei aldabena ostuten
ta arei famia quenduten.