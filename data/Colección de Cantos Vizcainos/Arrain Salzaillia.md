---
id: ab-58
izenburua: Arrain Salzaillia
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000058.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000058.MID
youtube: null
---

Ni nais emacume bat
arrain salzaillia
Marinelac ichasotic
Dacarren gustia
Ni nais emacume bat
arrain salzaillia
Marinelac ichasotic
Dacarren gustia
nai besigu nai legatz
atun sardinia
nai chirla nai miesga
bacart auguillia.