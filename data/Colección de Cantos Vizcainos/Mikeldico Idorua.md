---
id: ab-61
izenburua: Mikeldico Idorua
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000061.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000061.MID
youtube: null
---

Durangoco Mikeldin
Dago arri andi bat
Ascoc uste dabena
Ze aguia dala
Durangoco Mikeldin
dago arri andi bat
Ascoc uste dabela
Ze aguia dala

Ustu nau kenduteco
Onan güenburutic da
gauza chit obiarra
Esatia zerbait.