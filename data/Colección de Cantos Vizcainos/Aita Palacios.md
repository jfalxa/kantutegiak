---
id: ab-75
izenburua: Aita Palacios
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000075.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000075.MID
youtube: null
---

Enaz orain artian
guelditu baquian
berso olgagarriac
ipini artian
Orain paretzera mia
guztientzat guztoz
alabatzen dedala
Aita Palacios.