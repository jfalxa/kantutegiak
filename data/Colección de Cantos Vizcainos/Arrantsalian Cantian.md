---
id: ab-84
izenburua: Arrantsalian Cantian
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000084.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000084.MID
youtube: null
---

Arrantsaliac lango biciric
iñoc ez dauco munduan
dempora onian arraiñac illten
eta charrian Portuan.
Estrivillo. Maletachuan barruan dua
arrain erria ogui ta artoa
jausten baijaco beruna leguez
ondora artoac daroa
jausten baijaco berunac leguez
ondova artoac daroa.