---
id: ab-54
izenburua: Baserrico Cereguiñac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000054.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000054.MID
youtube: null
---

Urteilletic Abendura
ipintera nua
bersuac nezcazalien
alabanciñora
Alan mereci dabe
edocein lecutan
Eurac ez palitzatez
cer dago munduan.
Estrivillo
Oy es alancheda
oy es alancheda,
bay, alncheda,
bay, bay, alncheda.