---
id: ab-85
izenburua: Peccatari Bat Jaunaren Aurrian
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000085.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000085.MID
youtube: null
---

Ceinbat esker daukadan
Jaunari cer emon
ta ez bera garaistu ta
negoalez egon
Alper beti izan naz ni
ezquer gaistocoa
Ona izan da neuretzat
betiJaungoicoa.
Eztaukat iñun pozik
ta nator zugana
Bada dakit artua izango nazana:
Niri lengo baquia
emon leique dana
Ezta besterik iñor
ezpada zeu Ama.