---
id: ab-71
izenburua: Euskaldun Bat
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000071.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000071.MID
youtube: null
---

Emen nago ni tristia
galduta libertadia (nere baquia)
carsela baten
penac esaten
daucadazanac ugari
nere pentzamentuari.