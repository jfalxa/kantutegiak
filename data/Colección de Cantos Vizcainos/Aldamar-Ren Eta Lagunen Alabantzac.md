---
id: ab-78
izenburua: Aldamar-Ren Eta Lagunen Alabantzac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000078.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000078.MID
youtube: null
---

Orain nic gura neuque
Bertso batsuc asmau.
Aldamar lagunacaz
enretan gomutan.
bada guizon prestuboc
chit dabe mereci:
aleguiña eguin gabe
ezin dot nic echi
Bada guizon prestuboc
chit dabe mereci:
aleguiña eguin gabe
ezin dot nic echi.