---
id: ab-62
izenburua: Dueña. Criada. Sensañac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000062.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000062.MID
youtube: null
---

Dueña, criada sensañac
cantatzera guaz
beti, beti serbietan
nola gauden penas

emon ezinic gusturic
duñiac planchias
oguijhas criadiac
sensañac umias.

Au da penia
ay! ene au da penia.