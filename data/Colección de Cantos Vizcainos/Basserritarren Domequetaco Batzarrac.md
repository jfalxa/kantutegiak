---
id: ab-76
izenburua: Basserritarren Domequetaco Batzarrac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000076.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000076.MID
youtube: null
---

Peru ta Chomin Anton
ausoco lagunac
eta Jhoane eta Fraiscu
guizon alargunac.
Laurac egoten dira
Domeca illuntzian
eta jhai egunetan
tabernan musian
Laurac egoten dira
Domeca illuntzian
eta jhai egunetan
tabernan musian.