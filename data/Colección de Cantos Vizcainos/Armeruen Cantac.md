---
id: ab-73
izenburua: Armeruen Cantac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000073.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000073.MID
youtube: null
---

Amabi berso onetan
entzun bihar dogu
Erreguen armerubac
nor zubeditugun.

Onec espalitzatez
beti biarrian
cer eguingo guenduque
garian artian.

Onec espalitzatez
beti biarrian
cer eguingo guenduque
garian artian.

Au onan izanic
entzun issillic
au onan izanic
entzun issillic.