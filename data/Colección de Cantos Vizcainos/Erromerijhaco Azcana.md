---
id: ab-59
izenburua: Erromerijhaco Azcana
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000059.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000059.MID
youtube: null
---

Danzia acabau da
nesca mutilloc
suase arin echera
Bestela bijhar
loric ein baric
jhuan biarco do sube lanera.