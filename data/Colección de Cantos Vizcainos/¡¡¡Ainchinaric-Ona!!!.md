---
id: ab-67
izenburua: ¡¡¡Ainchinaric-Ona!!!
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000067.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000067.MID
youtube: null
---

Bistuco baliztete
orain Jaun Zuria
esagutuco ez leuque
guizaldi gueuria:

Orduric ona eguin dau
mundu zarrac gira,
len goijhan egon zana
etorri da azpira.