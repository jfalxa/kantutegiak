---
id: ab-81
izenburua: Gona Arrua
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000081.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000081.MID
youtube: null
---

Gure demporetaco
dama nescatillac
gure leuque irudi
guztiac sotillac.
Euqui arren bequi chiqui
eta zur chapala
baita onezas ganeti
Aua chit Zabala.