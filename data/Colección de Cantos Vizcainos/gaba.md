---
id: ab-68
izenburua: Gaba
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000068.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000068.MID
youtube: null
---

Betor illuna laster
ama loarena
ceruco bates amua
pena guztijhena

datorrela beragaz
gueure artera loa
cerututen mundu bat
erruqui bacoa.