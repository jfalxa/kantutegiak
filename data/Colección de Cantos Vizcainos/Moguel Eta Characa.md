---
id: ab-88
izenburua: Moguel Eta Characa
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000088.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000088.MID
youtube: null
---

Marquina Echevarrico
senar emazte bi
alaba eder bat euquen
ceruac emonic
alaba eder bat euquen
ceruac emonic
eta amar milla ducat
lurpian gueldiric
icusten ez evela
egunen arguiric
icusten ez evela
egunen arguiric.