---
id: ab-69
izenburua: Dn. Simon Zamacola
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000069.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000069.MID
youtube: null
---

Orain daucat goguan
Dimaco errijha
cerren anche jaijho izan
guizon chito andijha.

D.Simon Zamacola
Jaquitun prestua
eta gueure leguetan
batez bere aituba.