---
id: ab-65
izenburua: Urtia
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000065.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000065.MID
youtube: null
---

Gauza bat dago munduan
aurten igasco moduan
beti baccarra
gazte ta zarra
ill arren bistuten dana
biurtuteco gugana.