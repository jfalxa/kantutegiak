---
id: ab-74
izenburua: Musquijhen Gozitzac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000074.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000074.MID
youtube: null
---

Nicolas Zabalari
Adiuntza onetan
gura neusquijho ipini
berba neurtubetan.

Nicolas Zabalari
Adiuntza onetan
gura neusquijho ipini
berba neurtubetan.

Bere gozitza andijhen
eguiquera batsuc
Icustearren al badait
cerchibait jostatu.