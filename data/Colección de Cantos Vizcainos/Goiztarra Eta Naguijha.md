---
id: ab-79
izenburua: Goiztarra Eta Naguijha
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000079.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000079.MID
youtube: null
---

Jai eguneco arrats aldian
mutil bi duas celaira:
An dagozala etorten jhaque
chori artzeco asmuba
Prestuta sunas guertuteu dabe
bacochac bere zaria
Eta echera duaz biurtzeco
beste goicean goicetic.