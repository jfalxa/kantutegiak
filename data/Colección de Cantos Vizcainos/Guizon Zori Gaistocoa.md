---
id: ab-66
izenburua: Guizon Zori Gaistocoa
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000066.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000066.MID
youtube: null
---

Neuri bici guztija
dancat ni gogoan
da neurritu bihar
dot daquiden moduan
mundura sartu nintzan
mutilla fortunaz
eta, fortuna baric
munduan bici naiz.

Bota nau ez biarrac
ta ez lecu onera;
Ezpada naiz baquezco
Ychaz zaconera:
Amodio seudo bat
beste aukera
Kendurit banarue
soldatu izatera.