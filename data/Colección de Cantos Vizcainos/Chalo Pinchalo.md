---
id: ab-63
izenburua: Chalo Pinchalo
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000063.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000063.MID
youtube: null
---

Chalo pinchalo
chalo pinchalo
catuchua mispiren
ganian dago
badago, bego
badago bego
zapatachu barrijhen
beguira dago.