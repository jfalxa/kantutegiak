---
id: ab-87
izenburua: Zortzico
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000087.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000087.MID
youtube: null
---

Cada vez que te veo
calian pasatzen
con el ojo derecho
maitia deutzut beguiratzen.

Pero si no te veo
dempora lucian
grandes penas padezco
maitia nere biotzian

Cada vez que te veo
calian pasatzen
con el ojo derecho
maitia deutzut beguiratzen.