---
id: ab-55
izenburua: Gaboneco Kantac
kantutegia: Colección De Cantos Vizcainos
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000055.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000055.MID
youtube: null
---

Ardizain devoto bi
amodijho andijhas
Jesusegana duas
jhanari gustijhas
batac darua esnia
bestiac estijha
aurra consolatzeco
biardan guztijha.