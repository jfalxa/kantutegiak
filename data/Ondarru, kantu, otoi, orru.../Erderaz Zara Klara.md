---
id: ab-4473
izenburua: Erderaz Zara Klara
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004473.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004473.MID
youtube: null
---

Erderaz zara Clara,
euskeraz Argia,
zeu zara zeruetan
izar bat garbia.

Augaitik mariñelak
dauke guraria
gaur emon dakizula
zor yatzun gloria.

Zeure umetasuneko
denpora guztia
izan zan erreguzko
ejenplu bizia.

Alkargana baturik
arri koskortxuak,
zenbatu oi zenduzan
errosariuak.

Saritu izan zaitu
Birgiña Mariak
sendatu dituala
zure erabagiak.

Palma eder bategaz
Erramu goizian
jarri izan ziñan
Asisko kalian.

Amazortzi urtegaz
agur munduari
egin da izan ziñan
zelda maitelari.

Jesus zalako zure
guztiz maitagarri,
Biotza emon zeuntsan
osorik berari.

Jesusgaitik zenduan
bizitza guztian,
jardun nekiak artzen
zeure gorputzian.

Baru, zilizio ta
lurraren gañian
lo egiñaz uda eta
negu gogorrian.

Frantsiskoren alaben
errelegiñua,
jarteko izan ziñan
lenengo autua.

Ta millaka askoren
orain gurasua
zarialako zara
santa indartsua.

Zu ikusita duaz
moruak igesi,
zugaz sakramentua
ebelako ikusi.

Berrogeta amar monja
ditu janaritu,
ogi bakar bategaz
ta erdia gelditu.

Andiak izan dira
zure mirariak,
orregaitik zaukaguz
gure gidaritzat.

Zeruan amatzat askok
zaitube alabetan,
lurrian gurasotzat
ez gitxik ondretan.

Elexiak Jesusen
laguntzat kontetan,
bitarteko dogula
guk Klara kantetan.

Zaindu izan gaituzu
zure mantupian,
eskerrak gure Ama
ta urrengo artian.