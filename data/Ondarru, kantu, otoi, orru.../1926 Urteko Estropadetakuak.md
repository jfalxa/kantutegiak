---
id: ab-4449
izenburua: 1926 Urteko Estropadetakuak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004449.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004449.MID
youtube: null
---

Jose Brontxe.k jarriak

- Bertso berriak jartzera nua
gusto duenak aditu,
nere barrena betia dago
geiago ezin kabitu,
pentsamentuan jarri orduko
biotza gata mugitu,
naiago neuke pasa bezela
zuzen kontauko ba nitu.

- Motiborikan asko ba degu
zerbait esaten asteko,
danontzat igual egon da aukera
estropara jokatzeko.
Gure auzuak modu onetan
gu gorrotuan artzeko,
arrazoirikan ez det billatzen
tamaña ortan jartzeko.

- Ogei ta zortzi urte dirade
Donostiara juan ziran,
iru treñeru illaren lauan
ederki probatu zian,
illan zortzian Donostiara
ura bentajaz atzian,
premiua ta bandera artuta
etxera etor zirian.

- Andik lau urte pasata gero
Bilbora ziraden juan,
iru premio egondu eta
danok ziran Ondarruan.
Bizkai danetik erri bakarra
falta izan zan orduan, .
beti ikasten baldin ba dabiz
ez jarri aren onduan,

- Arrezkeroztik aditu degu
makiña bat desprezio,
lenago izanarren zerbait
gaur eztubela balio,
iru nagusi or daudetela
Pasai, Donosti ta Orio,
oyek guztiak aditu arren
egondu gera serio.

- Club Deportibo ondarrutarra
juan dan urtian zan jarri,
Portugaleten dagon kopia
bear zuela ekarri,
da bost treiñeru alkarren kontra
kostauan ziran ezarri,
kopak guretzat ziraden baiña
bai gorrotua ugari.

- Estropada au irabazita
jendia asi zan esaten,
Bilboko kaldereteruari
errez dala irabazten,
orren ablllak izan da zertan
Donostira etzera joaten,
Gipuzkoako mekanikuak
probatu ditugu aurten.

- Gipuzkoako profesoriak
pentsauta zeukan jokuan
Donostiarra lendabiziko
bigarren Pasayakua,
irugarren Fuenterrabia,
laugarren Ondarruakua,
baiña Beitian.kuadrilla ezta
orrela gelditzekua.

- Lekeitio ortan emen zeuzkaten
fiestak preparatuta,
ondarrutarrak baldin ba datoz
Donostiatik galduta,
iru korrida eiteko asmuan
dirua ere batuta,
zezen jaubia bialdu dute
bere zezenak artuta.

- Bizkaia aldian euki nai bogu,
gure onra eta fama,
bi portu orri erregalatu
lezake treñeru bana,
da euren billa datozanian
gañera jan eta edana,
Errege jaunan kopatik berriz
eran dezaten txanpana

- Portu bi orrek nortzuk diraden
Lekeitio ta Bermeo,
egia klaru esatekotan
ajolarikan eztio,
motibo gabe emon dizue
makiña bat desprezio,
orain ixilik egon zeintekez
ezpadezue balio.

- Mundu onetan ikusten degu
persona askoren griña,
norbera baño abillagorik
iñork ikusi eziña;
guk estropara irabaztiak
eman al diote miña?
akaso igual zuek nagusi
juan da probatu ba ziña.

- Bizkaia eta gipuzkoarrak
konbenzitu al zerate oin,
ondarrutarrak baldin ba datoz
berriz ere listo egon.
Orregatikan musikak ere
gaur joten digute aliron,
nai eztuenak aditu bear
ondarrutarrak kanpeon.

- Sei estropada bost treñerutan
eta seiretan lenbizi,
gure denporan olakorikan
iñork ez degu ikusi,
ondarrutarrak arraun lanian
egiñagaitik itxusi,.
obe zenduke eurak bezela
kostautik juaten ikasi.