---
id: ab-4461
izenburua: Aratuste Egunekuak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004461.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004461.MID
youtube: null
---

Armosua pagatzen giñan instantian
lagun batek badio nire aldamenian:
Zer egingo ote dogu gaur arratsaldian?
Mamo jantzita ibili plazan da kalian?
Ez! Oba dugu jan da edan, leku on batian. (bis)

Seiren pentsamentuz zan, merienda jarri
eta parte emoteko beste lagunari,
gurekin nai al zuten broma onetan jarri,
kasurik egin gabe mamo zantarrari
azken ona emoteko karnabalak gabari. (bis)

Esan bezala jarri ditu tabernariak
arrautza, okela ta lebatz saltsa biak,
gañian prijituta tajada aundia,
ondo pozik jan ditu batu dan jendiak,
amaika laguneko kaudrilla gosiak. (bis)

Jaun bat zutik zebillen nire aldamenian,
edozer gauza esaten bere ao-pian,
batian euzkera ta erdera bestian,
bera zala nagusi txalupa egillian,
guri lebatza jan da iges azkenian. (bis)

Karabiñeru traje, bere ros da guzi
antxe mamo polit bat genduan ikusi,
ara gaizki egin naian beste bat zan asi,
nunbait bere arpegia nai zion ikusi,
gizon bat zapaldu ta juan ziran igesi. (bis.