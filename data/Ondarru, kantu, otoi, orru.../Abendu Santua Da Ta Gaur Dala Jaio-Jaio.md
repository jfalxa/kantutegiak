---
id: ab-4452
izenburua: Abendu Santua Da Ta Gaur Dala Jaio-Jaio
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004452.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004452.MID
youtube: null
---

Abendu santua da ta
ia da denporia,
orain kantatu daigun
Jesusen jaiotzia.

Gaur dala jaio jaio,
gaur dala jaiotzia,
Semia jaio ezkero
ama da dontzellia.

Ama Birjiñia,
nun dozu semia?
- Orra nun daukazun
Belengo portalian.

Josepe ze ordu da?
- Maria, gaberdia.
Zeruko izar ederra
laster dogu parian.

Errepika:
Maria Jesus,
Jesus eta Maria,
Maria Jesus.