---
id: ab-4472
izenburua: Berriotxoarenak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004472.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004472.MID
youtube: null
---

Milla zortzireun irurogei ta
au zazpigarren urtia,
Kristo zerutik jetxi zanetik
oneraño bitartia;
iru obispok pasatu duten
martiriuaren fuertia!
penagarriya izan da oien
munduko errematia.

Baten bizitza kanta dezadan
au da bigarren bertsoa,
Elorrio'ko naturala da
Balentin Berriotxoa;
aita ta ama Bizkaia'n daude,
semia an il da gaizoa,
salbajietan kunplitu zaio
eriotzako plazoa.

Amairu urtetik amabostera
estudiyo bat ikasi
gero aitaren ofizioa
erakusten zuten asi;
gogoa zeukan apaiz eginda
naiago zuela bizi,
tormentu gutxi mundu onetan
eztu beregan ikusi.

Berrogei urte orain zituen
gertatu balitz bizirik,
ogei ta zazpi lendabiziko
estudiatzen asirik;
seminario batera joan zan
karpintero ikasirik;
negarrez nabil aren pausoak
ezin adierazirik.

Seminario eder batian
Logroño'n apaiz egiña,
beste guzian baña lenago
ikasirik esamiña,
buru abilla izanik eta
fede santurako fiña,
negarrez urtu leiteke artaz
akordatuko ba giña.

Ogei eta lau urte zituen,
etzeukan memori illa
kolejiyoko maisuak bezin
estudiante abilla;
obispo jaunak ezaguturik
justua eta umilla,
agindu zuen meza emanta
erretiratu zerilla.

Goizeko labak jotzian meza
eman ta konfesiyora,
barau egiñez obra onetan
pasatzen zuen denbora;
pobrerik iñoiz etortzen ba zan
eskian atal ondora,
bere ogia ari emanta
deboziyua gogora.

Logroño'n meza eman zuen ta
Elorrio'ra etorri,
predikatzen da konfesiyuan
bere erriyan zan jarri;
Azpeitia'ko San Iñaziyo'n
bisita egin du sarri,
azkenerako arren pausoak
ziraden lastimagarri.

Suertatu da gure Balentin
Zan Iñaziyo'n sartzia,
pensatu zuen ango Padria
adbertzenzian jartzia,
nola nai zuen Santo Domingok
zeukan jantziya artzia,
ia fabore egingo ziyon
al bazuen ekartzia.

Ango Padriak maite izanik
ipiñi zuten kontuan,
agindu ziyon sartu zerilla,
Ocaña'ko komentuan;
bire guziyan eskian joan zan,
gaubak askotan kanpuan,
bere gorputza azotatuaz
allegatzen zan lekuan.

Allegatu zan Ocaña'ra ta
eman zioten jantziya,
berak nai zuen gorputzarentzat
alako penitentziya;
munduko gauza guztiyak beti
alde batera utziya,
neke trabaju naigabietan
artu du pazientziya.

Gure Balentin Berriotxoa'k
egin zituen pausoak,
konsideratzen jarri ezkero
dirade lastimosoak;
beti negarrez Jesusengana
zabaldurikan besoak,
aren biotza konsolatzen du
zeruko usai gozoak.

Pasiatzera etzan joaten
komentuko lagunakin,
etxian zeukan tertulia ona
errosario birekin;
bere gorputza martirizatzen
adore diziplinakin,
orazioak balio ba du
zeruan dago Balentin.

Bakardadian oneraño zan
Balentiñen istoriya,
Ocaña'tikan Manila ziran
zortzi ministro abiya,
barkuan ere sufritu dute
makiña bat komeriya,
biotza penaz urtu laiteke
ez baldin bada arriya.

Ocaña'tikan Manilara'ño
barkuan bost illabete,
jakiña dago izango zala
asko leguaz aparte;
zortzi ministro oien artian
au zan sobresaliente,
gure Balentin ango maisuak
obispo nombratu dute.

Ango Padriak agindu ziyon
eskubidia artzeko
predikatutzen asi zerilla
kristiandadian jartzeko,
konfesiyo ta barau egunak
animaz akordatzeko,
pekatuaren mantxa Judasen
kalabozuan sartzeko.

Artua jan ta ura erari
arri gañian lo egin,
beti Jesusi erregututzen
gutaz akordatu dedin;
kristandadia nai zuenari
konfesiyoak eragin,
jakiña dago salbajiakin
etzeguela atsegin.

Erreinu artan gobernadore
salbajien nagusiya,
Jaungoikuaren bildur gabia
estranjerian aziya;
religiyoko fede santuan
jarri nai duen guziya,
agindu zuen plazan kentzeko
ezpatarekin biziya.

Garagarraren lendabiziko
goizian ziraden asi,
arratzerako bost milletatik
etzan granorikan bizi;
fede santuko kristabak ilten
zituenian ikusi,
Balentin bere lagunetara
abiatu zan igesi.

Pedro Almato eta bestia
Jeronimo Hermosilla,
gero Balentin Berriotxoa
saltatu zan oben billa;
iru obispo juntatu ziran,
zeukaten biotz umilla,
aien bizitza laster izan da
martirazatuta illa.

Arrapatuta kate gogorrez
lotuak zeuden alkarri,
iltzeko eman sententziya ta
kartzela banatan jarri;
soldadu tropa baten erdiyan
gero plazara ekarri,
ezpatarekin lepuak mostu
dizkate lastimagarri.

Orretarako eskatu zuten
ordu beteko lekua,
Jaungoikoari egin artian
beren ofrezimentua;
gobernadore batek eman du
pena kastidarekua,
iñor oietaz damutzen ba da
bertan kentzeko lepua.

Iru obispok oien korputzak
ogei eta lau orduan
iduki eta enterratuak
urrikaltzeko moduan;
Oriental'ko probintziyan da
Jeu-dat deitzen dan lekuan;
erregutzen det aien animak
gerta ditezen zeruan.

Orra ogei ta lau bertso berriyak
ejenplo eder batian,
oroipengarri kanta ditzaten
pekatarien artian;
ai, gure Jesus, eskatzen dizut
grazi au errematian;
komeni da sartu gaitzazu
zeruetako atian.