---
id: ab-4456
izenburua: Edurra
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004456.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004456.MID
youtube: null
---

Milla zortzireun laroi ta zortzi
ditu aurtengo urtiak
eta bertsoak paratutzeko
daude motibo andiak.
Edurtza andiak arrapau ditu
mendi altuko jentiak,
pozez da penaz ilgo orte dira
jaiota dauden umeak.

Probintziatik Madri aldera
bota dituzte partiak,
guztiak dira desgraziazko
barri txit negargarriak.
Danak diote nola edurrez
beterik dauden mendiak,
ardi txabola baserri etxe
kalerañoko guztiak.

Nola egongo ote dirade
an bizi diran tristeak,
penatutzian bakar bakarrik
arritzen gara guztiak,
dudarik gabe pasau biardute
otz andi eta gosiak
istorian ba kontauko dabe
andik datorren biziak.

Elur andien pixuarekin
tellatu danak jausiak,
aixe ipar otzak pasatzen
orma zarraren artiak,
erdi itxirik dauden ate ta
bentana zulo guztiak,
etxe guztian korritutzen da
beneno bildurgarria.

Famili danak kuxkurturikan
dago bastertzo batian,
eskastxo duen leku epel ta
su apurraren aurrian.
Entzuten dira otso zaratak
bildurgarriak ateian,
etxe danaren jira dabiltza
zerbait iruntzi artian.

Ganadu danak gosez il bear
bei txal eta ardiak,
bedar preskorik ez da agiri
tapau dirade guztiak,
gaztañak eskas artua gitxi
nagusiantzat garia,
Agustuan eramanak dira
jan eztaiean txoriak.

Azken eman du edur tokiak
ta bost kanan edurra,
kristau bizirik ezin pasatu
txara edo egur lekura,
dispentsa utsa plumarik gabe
au pena eta tristura,
mendi altuko labradoria
aurtengo urtian galdu da.

Eguna juanda bildur da penaz
arratza dakar illuna,
biotza triste begiak negar
iturri bien modura,
Jesus onari erregututzen
Famili danak eztura,
arren libra gaizuz Jauna
bestela eraman zerura.

Konsideratu dezagun nola
egiten degun pekatu,
eta batera bildurrik gabe
Jesus maitea ukatu,
orregaitikan erakusten du
gu nola eskarmentatu,
zar eta gazte, pobre eta aberatz
danok kastigatzen gaitu.

Nere biotza urtzen dijua,
negarra dator begira,
illundur,
negarra dator begira,
begira,
illundurikan daukadaz biak
moteltzen asi da miña,
sentidu danak gelditu naian
osola jira ta bira,
zerbait geiago kantau naiarren
ezin emanik begira.