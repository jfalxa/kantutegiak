---
id: ab-4451
izenburua: Goiko Mendiatatik Alegratu Gaitezen
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004451.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004451.MID
youtube: null
---

Goiko mendietatik
pastoriak datoz,
esanik aingeruak
jaio dala Jesus,
Espiritu Santuak
emonik grazia
aren kontzeziñoia
beti da garbia.

Belengo portalian
estalpe batian
jaio da Jesus ona
ganadu artian,
astua ta idia
daukaz aldamendian
arnasaz berotzeko
oztutzen danian.

Ogeta laugarrena
dogu Abendua
desio genduena
logratu genduan,
gabeko amabiak
jo daben orduak
gure salbadoria
jaio da munduan.

ERREPIKA:
Alegratu gaitezen
gaurko gabarekin
gure Jesus onaren
etorrerarekin.