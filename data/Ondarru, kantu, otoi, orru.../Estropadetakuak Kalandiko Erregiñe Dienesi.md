---
id: ab-4450
izenburua: Estropadetakuak Kalandiko Erregiñe Dienesi
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004450.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004450.MID
youtube: null
---

- Bertso berriak jartzera nua
gusto duenak erosi,
fiestak nola pasa ditugun
nai nuke adierazi,
musika eta tanborrarekin
gañera orfeoi ta guzi,
Donostiako etorreria
esan bear det lenbizi.

- Motor ta bapor etorri giñan
Donostiatik batian,
eta Debara sartu giñaden
ia ondarrera artian,
txupiñazo ta txistua joaz
danok umore onian,
da Mutrikuko agintariak
an ziran gure tartian.

3,- Andik urtenda Mutrikun ere
igual egin genduan,
mendietatik guri begira
jendia franko zeguan,
mutrikuar ta elantxobetar
danok alkarren onduan,
anayak giñan bezela igual
etorrera egin genduan.

- Erregearen «Facun»en ziran
gure mutillak atzetik,
motor ta bapor musikarekin
atera ziran etxetik,
leor guztia betia jentez
"viva" dearrez bestetik,
gaur estakigu zenbait persona
etorri zan erbestetik.

- Gero gabian etorri zakun
Mutrikutik tanborrada,
aditutzian gure biotzak
asi ziraden dardara,
guk ain gustora entzun genduen
orren tanborren arrara,
gure atzetik etorri zakun
musikarekin Debarra.

- Mutrikuarrak eta Debarrak
gurekin portatu zera,
zeregaitikan gu alabatzen
gugana etorri zera,
gu emen gaude, gure premiñan
iñoiz arkitzen ba zera,
anaitasun andienarekin
bizi nai degu aurrera.

- Andikan laister aditu nuen
belarrietan pregoya,
gu alabatzen nola datorren
Durangotik orfeoia,
leku batetik adituko zan
orren kantuen orroya,
kristau oyek merezi dute
sonbreru ordez koroia.

- Beko fiestak akabatuta
kale andira giñan jaso,
ezin emanik egondu
giñan ordu betien lau paso,
erregiñatzat Dienesi ta
tanboliñakin Damaso,
obiagorik egongo ote da
mundu onetan akaso?

- Biba Mutriku eta Debarrak,
da Biba Durangokuak,
guztien partez milloi bana esker
Brontxe ondarrutarrak,
agradezitzen ondo dakigu
geuri ondo egindakuak,
zeruetara jasoko al zaitu
danok gure jaungoikuak.

- Ondo dakigu kale anditarrak
zuek zerana nagusi,
lenago ere zuen kalia
obetuen zan ikusi,
kalia orrek orlan ipintzen
nun ote dute ikasi?
Danon erregin or daukazute
Saguneko Dienesi.

- Mai andi baten gañian zeuan
kale andiko erregiña,
jendiak bera salutatutzen
eiten zuen alegiña,
ondo ederki ematen dezu
bada zerbaiten premiña,
obetuago emango zuan
pixkat pintatu ba ziña.

- Gure mutillak nobliak dira
da biotzetik zabalak,
orregatikan orren buruak
arturik daukatez alak,
erri guztia disfrazatuta
ustez ziran karnabalak,
orrelangorik etzan egingo
izan ba ziran makalak.

- Fiestarikan asko ein degu
estropaden izenian,
jendia ere aspertua da
geiago ezin egiñian,
da pelikulak atera ditu
bialtzeko zuzenian,
emen ikusi ez dituenak
ikusiko-itu zinian.

- Koziñeriak ere ez dezute
aurten eduki ain txarrak
zeuei danori jaten ematen
atera tuzte aparrak,
berez umilla eta garbiak
jenioz barriz azkarrak,
orregatikan agertu dira
gure mutillen indarrak.

- Arnabostagerren bertsuarekin
banua despeditzera,
piska bateko erretirua
orain ba guaz artzera,
guregatikan gaizki esaten
barriro asten ba zera,
gaizki esanka gugana aurten
zertan etorri etzera.