---
id: ab-4459
izenburua: Amar Mariñal Ito Dirade
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004459.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004459.MID
youtube: null
---

Amar mariñel ito dirade
Kantabriako uretan,
negar garraxiz beteta dago
Ondarru bere etxetan,
emeretzitik amar ittoak,
bederatzi salbatuak,
Goiko Jaun ona zer dira baiña
zure desio eskutuak?

Mutriku ortan ba zan dei larri
eta gizonen ez biar,
olatuetan trabes zanian
Nuestra Señora de Itziar,
erri guztia begira zeuan
argi gabeko illuntzian,
potiñandia tira-biratan
igarri zuen trantzian.

Isidoro zan patroi abilla,
lagunentzako noblia,
itxasoetan asko ibilia ta
gizon onaren ondria,
parte faltsuik euki ezarren
izan du azken tristia,
alegin sendo egiñagaitik
betiko galdu lemia.

Amar gizon batera ito eta
ainbesteik ez bizik artu,
salbatu ziran bederatzik e
larri ziraden azaldu,
promesa eta ofrezimendu,
erruki eta errezu,
olakoetan gertatzen danez
danak ziraden an keixu.

Ba da negarra Ondarru ontan,
tristura eta lutua,
alargun eta umezurtzetan
inkesa eta ulua,
kanpotik bialduarren
errukiarren dirua,
diruz sekula ez da onduko
bear genduken tratua.

Anai bezela jokatua da
Mutriku guri laguntzen,
alkar arteko aixa txikiak
ez dira orain kontutzen,
jarrai dezagun danon artean
gure zereiñak ondutzen,
itxasoaren tentaziuak
sekula ez dira ta aztutzen.