---
id: ab-4465
izenburua: Txomin Agirre
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004465.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004465.MID
youtube: null
---

Goguan zaitugu Txomin Agirre,
Ondarruko seme argi ta errime,
Kresala ta Garoaren egille,
gure euskera zarraren egille.

Gureak ziran Tramana eta Brix,
gurea benetan zure Kitolis;
gure oiturak
gure ardurak
gure gizonak,
gure esanak,
maitatu ta ezagutu zenduzan ondo,
ta ala jaso, txukun, Arranondo.

Erri ta gizon goratuz Ondarru
arro zabaldu zendun munduz mundu.
Gure kaliak,
gure kantoiak,
gure txokuak,
gure lekuak,
guk bizi ta ikusi bezin argi
Kresala'n agertu zenduzan egoki.

Begiko zendun Euskalerri dana,
sartu ziñan baita mendi barruna;
gure basoak,
gure soloak,
gure artzainak,
gure itzaiak,
aupatu zenduzan samur Garoa'n
eztia darion izkuntz gozoan.

Emen gaukazuz eskerrik onenez
zure mesedeari erantzunez;
gure txaluak,
gure oiuak,
gure biotzak,
gure gogoak,
eskintzen dautsuguz barru eta min:
Ondarru dana arro da zugaz, Txomin.