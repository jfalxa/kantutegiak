---
id: ab-4455
izenburua: Baliarenak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004455.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004455.MID
youtube: null
---

Milla zortzireun da
cincuenta y cuatruan
gure donostiarrak
baletara juan,
orrelango konturik
zeñek du gogoan
etzitzaigun aztuko
denbora galduan.

Donostiako jende
prestu ta noblia,
Gipuzkoa guzian
parerik gabia,
zeuek getariarrok
gauz onen zaliak
bisigua zala uste
al zenduten balia?

Luis gaiztoak emanik
lenengo arponada
balia arrapatzeko
txit gizon ona da
estimatzekoa zan
alako arponada
bederatzi milla errial
ba zuen jornala.

Jose de Karamelok
bigarren golpian
ederki portatu da
bere suertian,
baliari odola
bizkarra betian
etzitzayon gelditu
ito zan artian.

Ondora juan zitzaigun
balia il zanian,
etxera etorri giñan
agiri etzanian.
Getariarrak artu
azaldu zanian
ez genioten kendu
lenengo esanian.

Zuek getariarrok
besigua ugari
utzi ezaiozute
arren baliari,
ama igesi juan da
umia da ori,
segi ezaiozute
nai ba dezute ari.

Besteren gauzarekin
gizonen sasoia
ortikan ere bada
bertso baten gaia,
petxutikan sartuko
ziola arpoia,
kulpa gehiago ez da
au da arrazoia.

Jose Manuel Serenok
indarrak ba ditu,
oien zartadakua
Getarian aditu,
Jende guzia orduan
omen zan arritu,
izugarrizko famak
ark ekarri ditu.

Orixe da modua
gizona galtzeko,
petxua zabalduta
Luisek sartzeko,
paraje sagraduan
kolera jartzeko
injenio txit ederra
zeruan sartzeko.

Getariar oriek
etziran izutzen
Antiguora juan da
balia ikusten,
paga biar zuenik
etzuen pentsatzen,
sartu baño lenago
bi txanpona zuzen.