---
id: ab-4470
izenburua: Markesaren Alaba
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004470.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004470.MID
youtube: null
---

Zeruak eta lurrak
egin zituana,
memoria argitzera
atoz neregana.
Esperantza ba daukat
zerorrek emana,
kunplituko dedala
desio dedana.

Amoriozko penak
bertso larrietan
publika al ba nitza
bertso berrietan;
enteratu naiz ongi
enkargu oietan,
San Jose arratsaldeko
iru t'erdietan.

Markes baten alaba
interesatua,
mariñeruarekin
enamoratua;
deskubritu gaberik
bere sekretua,
amoriua zeukan
barrena sartua.

Egun siñalia zan
goguan artzeko,
esan ziola aren
etxera joateko;
- Deseo dedan itz au
manifestatzeko,
zurekin Antonio
nago izateko.

Zer esaten didazu
Juanita itz ori?
Tentazen ai zerala
trazak ba dirudi.
Etzait zure gradurik
tokatutzen neri,
ez burlarikan egin
mariñeruari.

Eduki dezakezu
ongi sinistua,
aspaldian nenguela
zugaz gustatuta.
Etzaitut nik utziko
desanparatua,
ala egiten dizut
gaur juramentua.

Konformatu ziraden
alkarren artian
ezkonduko zirala
urrengo urtian.
Eskola ikasteko
bien bitartian
beraren erritikan
guztiz apartian.

Ola disponiturik
jarri ziran biak
kartaz entenditzeko
alkarren berriak.
Formalidadiakin
jartzeko egiak,
baiñan etziran lo egon
amaren begiak.

Alperrik izango dira
aserre gurian,
ez naute mudatuko
eternidadian.
Esposatu nai nuke
kariño onian,
Antonio Maria
etortzen danian.

Ezin egon zan ama
itz ori sufritzen,
bereala asi zen
kartak detenitzen;
intentzio aundiagoz
ezkontza galdutzen...
Juanitak alakorik
etzuen pentsatzen.

Amaren malezia!
Korreora juan ta
Antonio il zala
egin zuen karta.
Juanitaren tristura
berri ura jakinda!
engainatua bestek
gezur bat esanda.

Amak esaten dio:
Juanita neria,
galdu da diotenez
Antonio Maria...
Nik billatuko dizut
beste bat obia,
maiorazgo interes
askoren jabia.

Ama, ez neri esan
orrelakorikan;
ez det nik bestegana
amoriorikan.
Ezin alegra leike
nere barrenikan
komenientzi ona
egonagatikan.

Utzi alde batera
orrelako lanak,
ez ditut nik ikusten
zu bezela damak.
Nai ba dituzu artu
onra eta famak,
giatuko zaituzte
aitak eta amak.

Denpora kunpliturik
galaia abian,
zer pasatu ote zan
aren memorian?
Kartarik artu gabe
juan da aspaldian,
inozente sartu zan
jaio zan errian.

Au da lendabiziko
ark esan zuena:
- Zer da musikarekin
onratzen dutena?
- Markesaren alaba
kalian barrena
esposario zala
ark biar zuena.

Desmaiaturik egin
zuen ordu bete,
gero nobia eske
itz bi egin arte;
inguratu zitzaion
makiña bat jende,
bigarren ordurako
il zan derrepente.

Gaua pasau eta
urrengo goizian
entierrua zuen
bigarren klasian...
markesaren alaba
guztien atzian.
Zer pena izango zan
aren biotzian!

Penarekin lerturik
Antonio il zan,
akonpañatu zuen
Juanitak elizan.
Maitasuna ba zion
esan dedan gisan,
geroztikan etzuen
osasunik izan.

Erremedia ba lei
sentimentu ori
bitartzeko bat jarri
Jesus maiteari,
orazio egiñaz
Birgiña Amari,
zeruan gerta dedin
Antonio Mari.

Alkarren konpañia
guk ere nai ba degu
Birgiña, egiozu
Jaunari erregu,
kristau guziogatik
baldin al bazendu,
Iturrinok orrela
desaiatzen du.