---
id: ab-4448
izenburua: Portugalete'Ko Estropadak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004448.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004448.MID
youtube: null
---

- Milla bederatzireun
ogeta bost urte,
Irallaren ogeia
señalatzen dute,
estropada ederrak
izanak dirate,
jokatu zan lekua
da Portugalete,
allegatu da ara
makiña bat jente.

- Club Deportibo Aurrera
Ondarroatarrak
erakutsi dituzte
txit mutil azkarrak,
arraunlariak ere
ez dirade txarrak,
asko bildurtu ditu
oien afarrak,
erri onetan oraindik
ba dira indarrak.

.- Bost traiñeru zirala
jokatutzekuak,
aditzen cinan zuen
ango juraduak,
iru zlran aurretik
ateratakuak,
Ondarru Portugalete
ta Santoñakuak,
markarik andienak
egin gurekuak.

- Zierbana ta Ondarru
ogeta zazpian
jokatzeko zirala
ondo ba zekian
guregana etortzen
bildurtutzen ziran,
orregaitik jarriak
dira atxakiak,
bestelanikan pozik
etorriko ziran.

- Gurekin jokatzera
animatzen etzan,
begira zeguenak
ala dute esan,
atxekia ugari,
orregatik ba zan,
Zierbanatar ori
deskalifika zan
ala esan izango
sendo egon ba zan.

- Zierbanak ukatu,
Sestao zan jarri,
bigarren premiua
eman zion ari,
asko gustatu zaie
Ondarrutarrari,
gogoz eman zioten
eskuak alkarri,
orregatik gelditu
ez da lotsagarri.

- Sestao, Zierbana
alkarren urrenak,
oiek ziran atzetik
jokatu zutenak
estropada onetan
ba ziraden lanak,
bigarren premiua,
dauka Zlerbanak,
baña Ondarrutarrak
jaso dituz famak.

- Gure kontra ba ziran
burla ta oiuak,
ez da esan bearrik
auzo errikuak,
ensaiuan asi ta
erretiratuak,
gu ez gera oraindik
ori eindakuak,
garialako mutil
petxu onekuak.

- Azkenengo estropada
emen egin zala
ogeitairu urte da
jokatua zala,
asaba zarretatik
datorren indarra
dantzatu da ederki
arraunaren pala,
jakin daizu oraindik
lengoak gerala.

- Estropada aurretik,
jokatu onduan,
desafiorik asko
aditu genduan,
premiuak atera
dituzte kasuan,
Errege jaunan kopa
orra nun daguan,
Club Deportivo Aurrera
Ondarroakoan.

- Kopa gurekin dago,
aurtengo urtian,
ta ondo gordetzea
saiatu gaitian
okerrik ez ba dogu
bien bitartian,
allegatuko gara
alako batian,
agur adiskidea
urrengo artian.

- Au sendo ba dakie
ingurutakuak,
txapeldunak gerala
Ondarruakuak,
orra amabi bertso
estropadakuak,
aurten Portugaleten
jokatutakuak,
oiek dira Burgoak
ataratakuak.