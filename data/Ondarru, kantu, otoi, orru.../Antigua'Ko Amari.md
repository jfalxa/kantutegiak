---
id: ab-4478
izenburua: Antigua'Ko Amari
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004478.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004478.MID
youtube: null
---

Milla bederatzireun
gañera lau urte
pelegrinazio bat
disponitu dute,
Agostuan ogeta
bat señalamente,
Ondarruara etorri da
makiña bat jente,
Ama Antiguakua
bisitatu dute.

Antiguako eleizatik
plazara ekarri,
bere guardian ba zan
jendia ugari.
Musika ta aingeru
bandera ta guzi,
aurretikan apaizak
guztien nagusi,
alabantzak ematen
Birgiña Amari.

Plazan paratu zuten
beretzat lekua,
loraz apaindurikan
altara santua,
Fray Bartel jaunarentzat
berriz kurpitua,
erriko prinzipalak
zan disponitua,
Biba Ondarruako
Aiuntamentua.

Bandera ta lujuak
balkoi bentanetan
arku ederrak berriz
kale kantoietan,
arbola ostruakin
arraunak e bertan,
lora txuri-txuri
ia tapaurikan,
zenbat kristau pasada
oien azpitikan.

Fray Bartel jaunarekin
gaude arrituak,
negar eragin digu
orren predikuak,
Amari erreguka
altzaurik eskuak,
klaro esplikatu ditu
fedeko kontuak,
zeiñi ez dio irten
begitik malkuak.

Sermoi au egin zuan
Ondarruko plazan
zortzi milla arima
gutxienez ba zan,
Fray Bartel Jaun orretxek
kontu asko esan,
Jesukristoren dotriña
irakurria zan,
kanta dezagun orain
bere alabantza.

Probintziatik jente
asko etorrita,
Bizkaiatik geiago
ala komenita,
Birgiña Amarengana
biotzak poztuta,
ainbeste milla kristau
danak auzpeztuta,
Imagiña adoratzen
belaunikatuta.

Bizkaian ba zeguan
Jaun onen bearra
Jesusen dotriñari
emateko indarra,
Bizkaiko jauna ere
ez degu ain txarra,
edadian aurrera
bastante da zarra,
sermoya predikatzen
txit gizona aizkarra.

Engañatuta gabiltz
munduko guztiak,
erlejiua galtzen
persona gaiztuak,
Jesusek gugatikan
emandako pausuak,
manifestuan orra
eleizan santuak,
bizi geran artian
atera kontuak.

Jesusen pausuetan
akordatzen gera
guregatik zer moduz
ibili zan bera,
ikusi nai ba degu
begira kurutzera,
ultziakin josirik
esku oñetara,
etsaiak jarri zuten
modu orretara.

Arantzazko koroia
etsaiak sartuta,
buru guztia dauka
dana zulatuta.
Lepotik dariola
odola tantua,
ain zitalak izanik
gure pekatuak
Gurutzan or dago
Jesus penatua.

Nai ba deguz arimak
zeruraño jaso,
sekulako desterra
pekatua preso.
Luziferren galdera
berua da oso,
infernuko etsaiari
zertan egin kaso,
Jesukriston banderan
egon allimoso.

Ama Antiguakua
biotzen graziak
arrigarriak dira
orren mirariak,
zuzen biar gaitu
zure mesediak,
Aita san Pedro iriki
zeruko atiak,
loria gozatzeko
arima tristiak.

Agoniako trantza
etorri ezkero
luzaro bizitzarik
ez degu espero.
Izerdi tantuakin
gorputzetik gero,
azkenengo arnasak
iges egin gero,
Jesusen sententzia
etorriko zaio.

Ama Antiguakua
orain erremata,
agonian urrena
eriotza da ta,
gure defensoria
imagiña da ta
laguntzaliak ere
makiñabat santa,
Zeruan sartutzeko
alabantzak emanda.