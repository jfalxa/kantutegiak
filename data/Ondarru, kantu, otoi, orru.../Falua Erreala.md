---
id: ab-4458
izenburua: Falua Erreala
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004458.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004458.MID
youtube: null
---

Fiestak egiteko
komisionatuak
firmatuak zeuzkaten
Diputaziuak,
Bilbo orretan ba dira
gizon jakintsuak,
guarda dezala beti
gure Jaungoikuak.

Erregiña ta errege,
alteza errealak,
Donostian egondu
dirade aurten danak,
baña enpeñaturik
gure bilbotarrak
baporetan ekarri
zituzten orra danak.

Bilbotik urten eta
zuzen zuzenian
allegatu ziraden
Ondarruko errian,
txalupa ta gizonen
gauden gu premiñan,
erregiña erabiltzeko
gure barrenian.

Ondarrutik urten eta
Bilbora allegatu,
laster esan ziguten
nola preparatu.
Falua erreala
pintau ta doratu,
Espaiñiako grandezak
zuek bear zaitu.

Bilbora abia giñan
Portugaletetik,
gu aurretikan eta
bestiak atzetik,
tringadorak guardian
alde bietatik,
ez zuan apartatzen
beñere gugandik.

Ezker eskubi zeuden
bapore andiak,
extranjero ingeles
Bilbora etorriak,
ederrak zeduzkaten
bandera ta argiak,
saludatzen zituen
gure Erregiñak.

Txupinazo kañoi ta
kantera tiruak,
sarri gelditzen giñan
geienak gortuak,
ixildu gabe kantari
inguratutakuak
musika tanbolin da
korneta soiñuak.

Ia ondarrutarrak
egin alegiña,
eta kontuz eraman
Bilbora Erregiña,
erria luzia da,
bai ongi egiña,
timonela dezute
platiku jakiña.

Gizonak zirudien
aingeru tropia,
Falua erreala
Madriko kortia,
arek barrena zeukan
sedaz estalia,
terziopelozkua
zeukan silleria.

Te Deuna kantauta
bisitan Begoña,
asko gustatu zaio
ango imagiña.
Egun orretan zebillen
Bilbon Erregiña
eleizak bisitatzen
egin du alegiña.

Bilbotik abisata
Tiburtziok jarri,
orra gauza guztiak
paperian garbi,
ofensarikan gabe
batere iñori,
Biba ondarrutarrak,
biba beti beti.

ERREPIKA
Ondarruatik Bilbora juanda
Faluan ibilliak gera,
Erregiña ta Alfonso Trece
ziran gurekin batera,
diploma eder onorekuak
danok erretratau gañera,
Bizkaiko Diputazio jaunak
bialdu diguz etxera.