---
id: ab-4476
izenburua: Salbea
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004476.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004476.MID
youtube: null
---

Jaungoikuak salbau zaitzala
Ama Erregiña Maria,
miserikordiaren Ama
gustiz da miragarria,
konfiantza osuarekin
nago zeugana jarria.

Jaungoikoak salbau zaitzala
berriro dizut esaten,
zure graziaren premiñan
gu beti gera gertatzen,
zu bezelako beste amarik
ez degu iñun billatzen.

Sispuruz eta sentimentuz
beti negarrez gaude gu,
negarrezko balle triste onek
zenbat dolore bizi du!
Zure begi errukitsu oiek
gugana itzuli egizuz,

Balle triste onetatikan
irtetzen geradenian
erakutsi egiguzu Jesus
gure begien aurrian,
zure sabeleko frutua
guztion zorionian.

O klementisima deitzen dizugu
Ama gustizko garbia,
O piadosa zein andia dan
zure miserikordia,
penen artetik zugana gatoz
dultze Birgiña Maria.

Anima galdu ez dezakigun
eskatzen dizut gogotik,
apartatutzen lagunduteko
pekaturako bidetik.
Jaungoikuaren Ama santua
erregu ezagu gugaitik.

Pekatariok egon gaitezen
guztiok prebenituak
izan gaitezen alkantzatzeko
guztiok mereziduak,
gure Jaun eta Jesukristoren
promesamendu guztiak.

Begira ezazu Ama maitea
gu nola bizi geraden,
peligro eta tentaziua
besterik ez dago emen.
Zure anparuan artu gaitzazu
betiko glorian.Amen.