---
id: ab-6059
izenburua: Kañabera Orrekin (2.Aldaera)
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: null
midi: null
youtube: null
---

Albako gultzurruna
aitz baten gaiñian,
gizon bat arrantzuan
itxas bazterrian,
kañabera orrekin
zabiltzan gizona,
Jaungoikoak dizula
zuri egun ona.
Alaitu ta poztu gaitezen
gaurko gabarekin,
gure Jesus onaren
etorrerarekin.