---
id: ab-4475
izenburua: Jaungoikoak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004475.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004475.MID
youtube: null
---

Jaungoikoak salba zaizala
Ama Erregiña Maria,
miserikordiazko Ama
gustiz da miragarria,
konfiantza osoarekin
nago zugana jarria.