---
id: ab-4447
izenburua: 1898'Ko Estropadetakua
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004447.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004447.MID
youtube: null
---

- Bertso berriak paratutzeko
artu det pentsamentua,
esan bear det nola izan dan
estropadako tratua,
laguntasuna eskatzen dizut
Ama Antiguakua,
zuzen da garbi esplikatzeko
ortxen pasatutakua.

- Gure txalupa azkarra dala
esaten omen dirazi,
pruebak onera ematen bada
gaztetasunian ikusi,
kostrutore bat kaia gañian
esanaz omen zan asi,
treneru ori ez da izango
Kontxa onetan nagusi.

- Lendabiziko illaren lauan
giñan jokatutzen asi,
estropadarik jokatutzen guk
or ezkenduan ikasi,
esaten zuten kostautik laster
juango zirala igesi,
baña Kontxa ortan ikusi degu
zein izandu dan nagusi.

- Esaten zuen illaren lauan
ateratutzen duanak,
berriz zortzian irabaztea
izango ditula lanak,
milla peseta eta bandera
arrapatzen dituanak,
merezi ditu probintziako
eta Bizkaiako famak.
- Urte batian Donosti ortan
paratu dute bandera,
orregatikan pentsatu degu
etorritzia onera,
onorko letra eder batzuk
jarrita dauska erdera,
kofradiko benta etxian
gordeta daukagu bera.

- Gure mutillak artzen zioten
arraunai ondo tamaña,
zeregatikan gertatu zaigun
oso kuadrilla biguna,
arraunlariak bizkorrak eta,
patroia berriz txit fiña,
bestela nola egingo genduan
guk egin degun azaña.

- Gure patroiak arraunketa ona
zigunian guri ikusi,
korajez laister ujuka guri
atzetik zaigun asi,
Kontxa onetan gaurkuan zuek
izango zera nagusi,
orla bogatzen egun gutxitan
nola dezute ikasi?

- Santa Clarara allegatuta
deitu zigun espasio,
mutil maitiak zuekin beti
izaten nitzan fio,
txapela eskuan sartuta berak
koraje batekin dio,
auxen naikua dezute eta
ez batere egin geio.

- Banderan buelta emanda gero
brankaz giñanian jarri,
larrez atzetik asi zitzaigun
begiratuz mutillari,
esaten zigun, arraunketa oiek
artuagatikan sarri,
zuena da ta nasai boato
ez kasorik ein inori.

- Lenbizikuan zazpireun eta
gero bandera ta milla,
animaturik etorri giñan
ara peseta oien billa,
kobardituta etziran egon
Manuel eta Cuadrilla,
esaten dute bota dirala
salaran amaika milla.

- Ogei eta bat minutu eta
sei segundo errekorrian,
alaxen oiek markatu zuten
juzgauan eta errian,
tranparik gabe egin degu guk
egun argiren erdian,
alare sano gelditu gera
miñik ez degu gerrian.

- Irabazita genduztenian
banderakin ikusi,
Donostiako andre batzuek
burlaka ziraden asi,
ze uste zuten juango giñala
kanpora andik igesi,
amak semiak ba dituena
orain dezute ikasi.