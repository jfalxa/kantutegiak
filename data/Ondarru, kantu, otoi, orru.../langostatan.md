---
id: ab-4467
izenburua: Langostatan
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004467.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004467.MID
youtube: null
---

Pittu zarran semiak
Ea'n dabiz iru,
langostatan onenak
gaztenak urten du.

Yentiak esanarren
au edo bestia,
Joakin'ek arrapatu
beti langostia.

Gaubian berandu ta
goizian goizetik
Joakin Pittua doia
bestien atzetik

Txalupatik urten da
andik "Maite" nera,
urrengo estasiñoia
"Soziedadera".

ERREPIKA:
Itxaso ta legorrez,
egunez ta gauez
beti gabiltz poz pozez.
Osasunez,
maitasunez,
alaitasunez,
gure erria goratzen.