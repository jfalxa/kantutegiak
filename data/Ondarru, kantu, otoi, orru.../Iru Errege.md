---
id: ab-4454
izenburua: Iru Errege
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004454.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004454.MID
youtube: null
---

- Zeruko erregea
jaio zan orduan
munduan
Herodes erregea
enbidiaz betea
bildurtuten asi zan.

- Herodes erregea
zek bildurtzen zaitu?
Esaizu.
Jaio dan infantea
zeurorrek il eitea
lograduko eztozu.

- Iru errege datoz
Orientetikan Belenera,
aurretik dakarrela
izar argi eder bat
bidea erakustera.

- Eldu zirenean
Belen erriko
portalera,
Jauna adoratzera
auspaz jausi zirean
belauniko lurrera.

- Ofrezietan dautsiez
euren presenteak
nobleak ,
Batak dakar urrea,
besteak mirrea
ta intsentsua besteak.

- Beste bide batetik
biurtuten dira
errira.
Herodesen asmuak
eta pentsamentuak
orain burlatu dira.

- Herodes burlaturik
gelditu zanean,
berenean.
agindu eban ilteko
iñozente guztiak
Belengo errian.

Ai ze negar andiak
orduan Belenen
eukezen!
Amaren bularretik
errukia bagarik
daroez umeak ilten.