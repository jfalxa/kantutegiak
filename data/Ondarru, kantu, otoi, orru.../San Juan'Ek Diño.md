---
id: ab-4463
izenburua: San Juan'Ek Diño
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004463.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004463.MID
youtube: null
---

San Juan'ek diño bere egunian
zelebratzeko fiesta,
kastigatua izango dala
lana egiten duana.

San Juan San Juan, San Juan, San Juan Bautista
Zelebratu daigun gaur zeure fiesta.

Igaz San Juan, San Juan zan eta
aurten San Juan Bautista,
Jesukristoren lengusua zan
Aita San Juan Bautista.

Or dago Txurruka besuak zabalik
txurru berrien aurrean,
txurru berrien aurrean eta
Mutriku eleiz aurrean.

Urak guztiak agortu dira
arrain gaiztoak agiri,
aurtengo urtean mutil txikiak
nun egingo dute igari.