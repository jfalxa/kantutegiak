---
id: ab-4471
izenburua: Ana Juanixe
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004471.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004471.MID
youtube: null
---

Egun batian ni ba'nenguan,
kostela-kostel gaiñian,
ni ba'nenguan kostel-saltzian
ezpabe gari-saltzian.
Iru damatxu etorri ziran
bata bestien atzian
irugarrenak pregunta eustan
gariak zegan zirian.
- Bestientzako dirutan eta
zuretzat laztan-trukian.
- Ez nagizula lotsatu, arren
onelan plaza betian;
onelan plaza betian eta
ainbeste jente artian.
Orrela bere esan bazeustan
leku sekretu batian,
gariak eta zeu ta neu bere
an konponduko giñian.
Ama neuria labarako da
goiz edo arraltsaldian;
paseito bat emoidakezu
ama laban dan artian.
Ama labatik etorri ta
mutil-usaiña etxian...
- Ana Juanixe, Ana Juanixe;
zein da dauana etxian?
- Katalinatxo auzokua da
beroi laban dan artian.
- Eztona ori Katalinatxu;
bizarra dau okotzian...
- Itxagon daizu, mutil, itxagon,
argia piztu artian
argia piztu artian eta
ikusi zeu nor zarian.
- Ama berorrek ez dauela nai
neure osasunik etxian;
biyotz-illunak egiten yataz
argiya datorrenian...
- Urten daik, mutil; urten daik, mutil,
alako bentanarian,
ama neuriak urten dagiyan
kanpora sospetxerian...

Egun batian ni ba'nenguan,
Londres'ko Ziudadian,
maitiarentzat joyak erosten,
Tableru baten gaiñian,
dama bateri galdetu neutsan
joya ok zegan zirian.
- Bestientzako dirutan, eta
zeuretzat buru-trukian.
Eskerrik asko, dama galanta,
Ez dot armarik aldian:
neure arma onak itxi nituan
Motriko erri onian,
Motriko erri onian, eta
Ana Juanixe'n etxian.

Txalopatxu bat, txalopatxu bi
Santa Klara'ren parian...
Neure anaiya antxe datorke
batian edo bestian...
- Neure anaiya: zer barri diraz
Motriku erri onian?
- Barriak onak diradez, baña
jazo da kalte sobria.
- Oh, neure aniya: ez edo-da il
biyoren aita maitia?
- Biyoren aita ez da il, baña
jazo da kalte sobria.
- Oh traidoria! Ez edo-da il
biyoren ama maitia?
- Biyoren ama ez da il, baña
jazo da kalte sobria.
Ana Juanixe ezkontzen zala
iñuen atzo kalian,
Ana Juanixe ezkontzen zala
aita ta amaren lotsian...
- Ezin izan lei ori egiya,
neuk dot armia aldian;
armia eta Gurutza bere
neuk ditudaz bularrian;
ta neure armak tiretuko dau
bizi diraden artian.

Kapitan jauna: arria bela
txalopa nabegantian,
ainbat ariñen sartu gaitezan
Mutriku erri onian,
Mutriku erri onian, eta
Ana Juanixe'n etxian.

Ogetamairu dama lindatxu
Motriku'n molla-gañian,
Ana Juanixe antxe dalarik
arexen danen artian...
Batak "agur" ta bestiak "adios"
Jaunixe negar batian...
- Ana Juanixe: zer pasatu da
Motriku erri onian?
- Zaldun gaztia: neuk bere ez dakit
zeu nundikua zarian.
- Neuk ba'dakit, ba, Ana Juanixe,
zeu nundikua zarian, eta
noren alaba zarian.
Damatxu baten arma-gurutzak
daruadaz bularrian;
Ana Juanixe, ondo dakizu
biok norenak dirian...

Ana Juanixe; ez eixu esan
berbarik banidadian;
despedidako gabian bere
neure besuan ziñian;
zeuk niri laztan, neuk zuri laztan,
an despedidu giñian...
Joya asko bere ba'nakartzuzan
zuretzat izan ustian,
Beste damatxuk gozako dituz
zeuk ezin al dozunian.