---
id: ab-4477
izenburua: Pasinoia
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004477.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004477.MID
youtube: null
---

Jesukristori kendu ezkero
pekatuagaz bizitza,
baldin ez ba dot negar egiten
arrizkoa dot biotza.
Guztiok lagun
kantatu daigun
bere penazko eriotza.

Mundua baiño askoz lenago
señalauriko eguna,
eldu izan zan obretarako
gizonaren osasuna.
Argia bera,
alde batera
bestetik egun illuna.

Eguen santu afal ondoan,
bere lagunak zitula
Jesusek iru diszipuluak
berba egin eutsen onela:
triste nago ta
lo ori bota
egizue emen gaubela.

Getsemaniko Ortu santuan
auspez lurrean jarririk,
esketan eutsan librau eiala
Aitari Kaliza atarik;
Jesus penatzen,
odolez urtzen,
eta ez dot nik negarrik?

Izerdi otza arpegian da
biotza penen artean,
triste arimea gorputza larri
agoliñako trantzean,
baiñan alan bere
egoan firme
Aitaren borondatean.

Juduak Judas lagun arturik
etorri ziran itantzen,
Nazarenuan deritxon Jesus
emen ete da aurkitzen?
Judas orduan,
bake moduan,
dua musua emoten.

Zeinbat direan Judasen legez
Kristoren adiskideak,
Kanpoti laztan barruti saldu
Ausirik bere legeak?
Kristiñau faltso
Arima gexto
Pekatari traidoreak!

Erantzun eban Jesus maiteak
Zeiñen billa zatoze ona?
Ara emen dago ezta besterik
Gura dozuen gizona;
Joateko eztot nik
Soka bearrik
Ez eldu lagunok gana.

Eskuetatik lotu izan eben
Guztion libertadea.
Zegaiti au zan geure onean
Jaunaren borondatea,
Urratuteko
Ainbat urteko
Pekatuaren katea.

Diszipuluak euren Maisua
Ebenean ala ikusi
Bakar bakarrik bera itxita
Joan zirean igesi:
Pedro etorren
Bere ondoren,
Baña au bere urruti.

Daroe bada Jerusalengo
Sazerdoteen aurrera,
Diszipuluen ganean eta
Dotriñan kontu artzera:
Kaifas gextoa
Aurrerengua
Asten jako itantzera.

Eztot nik diño Redentoreak
Predikatu ezkutuan,
agirian zan nire dotriña
Sinagoga ta Tenpluan:
Ez niri itandu
Asko dituzu
Entzun nabenak munduan.

Erantzuna aitu eban ezkero
Soldadu batek bertati
Gora daroa bere besoa
Eta dirautso Jaunari:
Ikasi daizun
Zelan erantzun
Barriz Pontifizeari.

Esan ta egin emoten deutso
Bere arpegi ederrean
Belarriondoko andi bat, zeñek
Ezarri eban lurrean:
Eta Zeruak
Dagoz lotuak
Au ikusten dabenean?

Nozko dirade, Aita Eternua,
Tximistak eta arraioak,
Ez erretako an berbertati
Esku ain sakrilegoak?
Aingeru ostea
Eldu zaitea
Non dituzuez egoak?

Baldin deungaro berba egin ba dot
Errazoia emon egizu
Baiña ezpadot, diño Jesusek,
Ezetan bere erratu,
Zegaiti arren,
Berba egin arren
Onela tratetan nozu?

Indazu Jesus biotzekoa
Zeure sufrimentu ori,
Jakin dagidan zelan penetan
Emongo dodan arpegi
Parkatuagaz
Biotz osoaz
Etsairik andienari.

Damu dot Jauna biotz guztitik
Zeu ofenditu izana,
Ez ez geiago pekaturikan
Neure Jaungoiko laztana,
Lar da kulparik,
Betoz agaitik
Pena orreik niregana.

Sua egoan eskatzean ta
Pedro su atan berotzen,
Zerren biotzak amodioa ta
Otz asko eutsan sentitzen
Su orren keak
Begi bereak
Negarrez bete zituen.

Zerren ollarrak ez eban emon
Birritan bere soñua
Non iru bider ukatu eban
San Pedrok bere Maisua:
Ezta besterik
Okasiñorik
Ateraten dan pagua.

Kaifasek barriz itantzen deutso
Egiñik juramentua
Esan eidazu, Jesus bazara
Mesias prometidua
Alan ezpada
Zetako izan da
Engañatea mundua?

Ondo ekian Jesusek nora
Joean konfesiñoia
Topetan eban kondenetako
Kaifasek okasiñoia
Eta arekin
Gura eutsan egin
Ilteko akusasiñoia.

Alan be baietz erantzuten dau
Aitari begiraturik
Baita Kaifasek bere deadar
Soñekoak urraturik
Blasfemuena
Da onen pena
Ia dago kausa egiñik.

Jagiten dira konziliotik
Ta artuten dabe bidea
Amargurara, non Pilatosek
Edukan bere etxea
Zan Erromako
Denpora atako
Juduen Presidentea.

Juezak diño emongo badot
Gizon au eriotzara
Ekarri egizuz kausa justuak;
Eta ezpadira, zetara
Narabilzue,
Eroaizue
Or Herodesen aurrera.

Aita Eternuan jakituria
Dauko munduak zorotzat,
Agaiti diño Errege orri
Jantzi tunika zuri bat:
Ezpabe nola
Zeruko estola
Erosiko zan guretzat.

Esan eutsien Pilatoseri
Juduen Sazerdoteak,
Uste al dozu ez doguzala
Guk bere geure legeak?
Dau blasfemadu,
Merezi ditu
Eriotza ta azoteak.

Onek esan dau, izanik arotz
Pobretxo baten Semea,
Dala Jainkoaren Semea eta
Judeako Erregea:
Ilten ezpozu
Ezara ez zu
Zesarren adiskidea.

Gura eukean juezak Jesus
Eriotzatik libradu,
Zegaiti bere inozentzia
Ondo eban ezagutu
Baiña juduak
Dagoz itsuak,
Ez eutsien permitidu.

Eriotza bat eginda, diño
Dago Barrabas kartzelan,
Nor nai dozue, Nazarenua
Edo a librau dagidan?
Da oiturea
Bat libretea
Zuen Pasko egunean.

Osta aotik atera eban
Asi zirean deadarrez:
Librau egizu Barrabas; baiña
Jesus Nazarenue ez:
Orrek bear dau
Biziro penau
Abe batean dolorez.

Ostera itantzen dau Pilatosek:
Baiña zer kausa emon dau?
Israeleko Errege bada,
Esatea da pekatu?
Areik deadar
Errege Zesar,
Ori kurutzean iminzu.

Justiziako lege guztiak
Itxirik alde batera
Salbadorea kondenetan dau
Gogor azoteetara:
Zeru altuak
Zer dekretuak
Datoz ortikan lurrera.

Jainkoa bera, zeñek dadukan
Mundua loraz jantzirik,
Kortesi baga bere erropak
Tunikaraiño erantzirik
Narru gorrian
Egun argian
Itxi eban lotsaturik.

Nondik zarie gaba ta odeiak.
Illuntasuna non zara,
Zelan ezatoz Birjiñearen
Seme eder au estaltzera?
Ola eguzkia
Zeure argia
Bialdu egizu atzera.

Bost milla azote eta geiago
Loturik kolumna baten
Azi zirean iru borreru
Bere espaldetan emoten:
Ibai bat zelan
Odola alan
Asi ziran zan jausiten.

Arriak eta Salbadoreak
Biak alkar zirudien
Zegaiti esan onen aoti
Sentimenturik entzuten:
Marmolak eurak
Golpe kruelak
Dituenean artuten.

Pillaretikan biziro Jauna
Soldaduak askaturik,
Joakoz arin adoretara
Burlaz Errege egiñik:
Bere koroa
Arantzazkoa
Buruan estu josirik.

Mantu santarra soñean eta
Kañabera bat eskuan:
Onelan Jesus tratetan eben
Pekatariak munduan,
Mundua bera
Redimietara
Jauna etorren orduan.

Jesus onelan ikusi eta
Pilatos errukiturik,
Diño barriz: nik eztot aurkitzen
Gizon onegan errurik;
Il egizue
Nai badozue
Erroman ezta legerik.

Areik deadar: Jauna dalako,
Pilatos, bildur bazara,
Datorrela arin orren odola
Geure arimen ganera:
Eztau ardura
Geure kontura
Kondena egizu krutzera.

Juez gextoak Sazerdoteen
Esana egiteagaitik,
Publiketan dau sententzia bat,
Ez iñok okerragorik;
Eta gura dau
Burua librau
Jainkoaren odoletik.

Alper alperrik Presidentea
Garbitzen dozak eskuak,
Juez justotzat eduki azan
Hipokrita ori munduak:
Loituta ago;
Baiña geiago
I baño bere juduak.

Krutze baten mundu guztiko
Pekatuak zituzala
Abiatu zan Kalbariora
Jesus lapur bat bezela:
Artu krutzea
Au da bidea,
Galduko gara bestela.

Salbadorea pausu bat bere
Ezin emonik aurrera
Krutze astunak azpiraturik
Benzidu eban lurrera.
O pekatua
zure pixua
Karga guztia zu zara.

Ezin bakarrik eroan eban
Zalako guztiz astuna,
Eta orregaiti emon eutsien
Zirineoa laguna:
Ditxazko gizon
Zara su Simon
Nork leukan zure eguna!

Bide onetan joeanean
Kalbarioko mendira,
bere Semea kontenpletako
Ama egoan begira:
An dira penak
An tormentuak
Martirioak an dira.

Ama Birjiña afligidea
Itxi egizuz begiak,
Estaldu zaite, ain baga bere
Asko daroa Semiak
Tormenturikan;
Bat bere itzikan
Ezteutsu itxiko negarrak.

Bialtzen dituz Amak begiak
Semearen ikustera;
Baiña biotzak sufridu ezinda
Biurtzen dira atzera:
Datoz agaiti
Begietati
Biotzak berba egitera.

Semearenak esaten deutso
Badakizu Ama maitea
Asko sentitzen badozu bere
Onelan ni ikustea:
Bestelan ezin
Zeruarekin
Egingo dala bakea.

Neure Aitaren borondatean
Zeurea beti daukazu,
Aren esanak naroa: Agur,
Bendiziñoia indazu;
Neure ordeaz
Pekatariaz
kontua zeuk edukazu.

Amak diñotso: zoaz neure Jesus
Iltera ordu onean,
Baiñan zelan ni itxi nai nozu
Bakartade ain tristean?
O, zelanbait nik,
Eta ondo pozik
Jarraituteko (?) neukean.

Salbadoreak onelan penaz
Egiñik bide erdia,
Veronikaren karidadeak
Kendu eutsan izerdia:
Obra onen pagu
Jaunak isten du
Pinturan bere arpegia.

Joazan bada bera laguntzen
Jerusalengo Alabak,
Penaren andiz begietati
Urturik euren biotzak;
Eta eurakana
Biurturik Jauna,
Dio: Ay emakumeak!

Arbola eze au erre badau
Pekatuaren su garrak,
Zelan eztituz abrasaduko
Su orrek egur igarrak?
Ez niri negar,
Eztot nik bear,
Pekatuari negarrak.

Eldu zirean Kalbariora,
Egozan prest borreruak
Mundu guztiko Egilleari
Josteko oinak ta eskuak,
Ikusi eian
Bigarren Adan
Arbola atan munduak.

Nok ditu esku ta oin orreitan
Burdiñeagaz idigi
Gorroto edo amodioak
Odolezko lau iturri?
Bularretikan
Ate egiñikan
Amodioa da agiri.

Baltzez jantzirik ezkutetan dau
Zeruak bere arpegia,
Lotseagaiti gelditzen dala
Gorriturik irargia,
Gau illunean
Egoanean
Altuena eguzkia.

Austen dirade arriak, zelan
dagoz biotzak osorik,
Krutze batean dolorez ilten
Geure bizitza ikusirik?
Pekatariak
Gora begiak
Negar oturri egiñik.

Gogoti negar egingo deutsut
Jarririk zeure oiñetan,
Neure Jaun ona zaitudalako
Imiñi krutze orretan:
Daukat damua
Zu ofendidua
Gauza guztien ganean.

Izanagaiti, Jesus maitea,
Neure pekatua andia,
Preziosua da zeure odola,
Au dakidala balia:
Piedadea
Neure Jabea
Jauna miserikordia.

ERREPIKA:
Damu dot Jauna Biotz guztitik
zu ofendidu izana, ez ez geiago
pekaturikan neure Jaungoiko laztana
ezer kulparik betor zeuganik,
pena oriek niregana.