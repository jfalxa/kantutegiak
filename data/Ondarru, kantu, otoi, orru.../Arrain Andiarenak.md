---
id: ab-4457
izenburua: Arrain Andiarenak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004457.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004457.MID
youtube: null
---

Milla zortzireun larogei ta amar
garren urtiak sarturik,
April illaren ogeigarrena
jarri da señalaturik,
Españan eta Estranjerian
guziak aotan arturik
bota dituzte paperak danak
oso gloriaz beterik,
"Ondarrutarrak aiek mutillak"
ez da entzuten besterik.

Egun orretan zer egin zuten
emengo arrantzaliak,
mundu guztiak jakin dezaten
jarriko du istoriak,
begira danok papera oni
zabal zabalik begiak,
gezurrik gabe jarriko ditut
pasatutako egiak,
baldin laguntzen ba dit onetan
gure Jaunaren graziak.

Berrogei eta bat oñak luze
dauzkan treñeru batian,
amar gizon ta iru mutillak
itxasora atera ziran,
geienez ere legorretikan
izango ziran iru millan,
bildurgarrizko arrain aundi bat
azaldu zan oien jiran,
ta bereala persegitzeko
gogoz preparatu ziran.

Isidoro Elu oien patroiak
esan zien laguneri,
nai ba dezute arrimatzia
gustoria egingo det ori,
boga aurrera ara nun dagon
errez joko degu ori,
gero kontuan egon guziok
miñik egin gabe iñori,
tiburoi kasta gaizto itxurako
arrai itzala da ori.

Allegatzian bere aurrera
tira ondo arponada,
ia mutillak, leku onian
sartu iñola al bada,
txikotak listo arriatzeko
arrain oso aundia da,
galduko gaitu guzi guziok
ondo trinkatzen ez ba da,
artu labanak, ebaki sokak
asko moduz asten ba da.

Esponiturik momentu artan
guztiz peligro andian
azkar Liborio Bedialauneta
jarri zan txalupa aurrian
eta arturik arpoi ta soka
bere besoan betian,
garboz tiratu eta sarturik
utzi zion aragian,
oso egoki enkajatu zan
lantza tripa barrenian.

Ondo erituta jarri zanian
orduantxe bai kontuak,
Norestian barruna asi zan
zabaldurikan eguak,
buztan palakin singatzen zuben
baporen aundien moduan,
momentu artan bildurtuko etzan
gizonik ez da munduan,
bere arpoia libratu nairik
errebenta zan orduan.

Luzaro beian ibiliarren
etorri zan azalera,
iru lau aldiz eragin zigun
makiña bat gorabera,
bañon arturik era on baten
txikotakin gogor bera,
aldatu giñion lengo lekutik
arpoia buru partera,
sokakin fuerte sagan arturik
abia giñan etxera.

Goizeko amaika orduak ziran
lenengo jo zutenian,
gauba eginda zortzirak ia
legorrera zutenian,
zeinbat trabaju igaro zuten
oiek bien bitartian,
ezin lezake plumakin jarri
klaro begien aurrean,
ondar sekutan altxata utzi
zuten bizi zan artian.

Ogei ta amabi pie zan luze,
pisuz zazpireun arrua,
ondoko arraya pustoki gabe
diote dala eskaulua,
ez du iñortxok ezagututzen
ai au piztiya modua,
Club Nautikuen baporak azkar
Bilbo aldera dijua,
ondarrutarrak sozio oidi
orra zer erregalua.

Allegatuta paratu zuten
Bilboko Ripa partian,
bear bezela esponiturik
almazen aundi batian,
soziedade klase guztiko
jakintsuak batu zian,
eta esaten zuten "Orlako arrairik
ez daukagu istorian,
merezi luke konserbatzia
sekulako Españian..

Gure bizkaino Martin Zabala
senadoria Madriden,
arrañak peska diskurtsu baten
klaro esplikatu zuben,
Ondarruatar balientiak
zelan arrapatu zuben,
eta Bilboko Diputau jaunak
nola konserbau nai zuben,
operazio ori egiten
iya utziko al zuben.

Arraña berriz ekarri dute
Bilbaotik Ondarruara,
gobernatu ta eskeletua
eramateko bertara,
mediku jaun ta destripadore
asko etorri da ortara,
len istorian ez dagon gauza
berri bat orra bistara,
gure Bilbaoko Institutuan
ikusiko da gustora.

Au ikustia errial biña
kendu diote danari,
Ondarrutarrak kobratu dute
orrekin diru ugari,
milla peseta garbi premio
arraya jo zutenari
orain milla esker guzion partez
Club Nautiko ko jaunari,
Biba Bilbao, biba Ondarroa
lagundu ola alkarr.