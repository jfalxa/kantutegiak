---
id: ab-4468
izenburua: Aztanen Portalean
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004468.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004468.MID
youtube: null
---

Aztan'en portalean
arbola eder bi,
bata da laranjea,
bestea madari.
Larrosatxuak bost orri daukaz,
klabeliñiak amabi,
gure umea gura dabenak
eskau bekio Amari.
Txikitxua naizelako
nago neu astuna,
ez al nazu botako
karkaban barruna.