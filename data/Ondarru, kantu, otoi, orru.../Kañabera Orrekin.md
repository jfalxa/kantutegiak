---
id: ab-4453
izenburua: Kañabera Orrekin
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004453.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004453.MID
youtube: null
---

- Albako untzurrunian
aitz baten ganian
gizon bat arrantzuan
itxas basterrian.
itandu bear diot
ete daben entzun
Belenen gaberdian
zer gertatu zaigun.

- Kañabera orrekin
zagozan gizona,
Jaungoikuak dizula
zuri egun ona;
bart sentidu al dezu
izeren zaratarik,
edo entzun zeruko
aingeru kantarik?

- Gizon gazte eder bat
egoz jantzia zan,
bart gaberdian legez
emen igaro zan.
Aren kanta soiñuak
pozez aditurik
gau guztia eman det
arrain bat gaberik.

- Gizon gaztea etzan
gu lez gorpuzduna,
bart emen igaro dan
gazte egaduna,
ari zan Espiritu
abisoa doiana,
ipar sueztian
erregiakana.

- Neu ere joango nitzan
gaur a ikustera,
barrubiña batzuek
banijua esatera,
baña erropa zarrak,
eskuak utzak,
nola sartu niteke
Jaun baten etxian?

- Neuk ere ardi narruak
neukazan soñian,
baña ementxe egondu naz
bart-ko gau guztian.
Abe Maria eginda
esan egiozu
arrantzale pobre bat
emen naiatortzu.

Arrantzale maitia
neuk biotza nai det,
urria ez zidarra
ezerentzat ez det,
orain aurrerantzian
fede bizi bategaz
bota tretza guztiak
Jesusen izenian.