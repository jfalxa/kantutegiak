---
id: ab-4466
izenburua: Gure Ondarru
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004466.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004466.MID
youtube: null
---

Guztiok Ondarru
gorau daigun,
izan daiten arru
lior eta itxasun

Gabiltzan beti gu
maitez ta alaitsu,
kaletan oiu,
uretan orru,
beti goratzen Ondarru.

Aixe berdietan
edo ipar baltzetan
begira Antigua'ri
gure izarrari.

Errira etortean
kale ta kantoietan
zoratzen pozez,
umore onez,
kresal usainez.

Guztiok Ondarru...

Urertzean sortu
ta urertzean il,
ta bitartean
kantuz,
dantzan,
orruz,
txanpan,
Ondarrun maitale,
Ondarrun kutun:
ala beti
ekin daigun.

Itxasuan
nai liorrian
gure amesak
beti Ondarrun,
berau lakorik
ez dogu guk iñun,
ezin iñun.
Itxasuan
nai liorrian
ezin aztu
gure Ondarru.
Katauriko pitxi arru
gure Ondarru.