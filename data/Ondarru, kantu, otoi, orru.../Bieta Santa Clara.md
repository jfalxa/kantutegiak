---
id: ab-4474
izenburua: Bieta Santa Clara
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: null
midi: null
youtube: null
---

Bieta Santa Clara
euskeraz argia
zu zera zeruetan
dontzella garbia.

Amaren sabeletik
zenduan grazia
munduan izateko
ejenplu bizia.

Alkargana baturik
arri koskortxuak
erreza oi zenduzan
errosariuak.

Amazazpi urtegaz
monja sartu ziñan,
monja sartuta ere
Clara izan ziñan.

Aita San Prantziskua
padrino arturik
esposa izan ziñan
aitamen ixilik.

Eskuetan dan dauka
gauzaren andia,
Kustorio batean
Jesusa bizia.

Aita san Frantziskua
organua joten,
amabi apostolu
zeruan kantaten.

Asisko konbentuan
zinduten ikusi
ikusi eta bertan
moruak igesi.

Berrogetamar monja
ditu janaritu
ogi bakar batekin
ta erdia gelditu.

Uso txuri ederra,
zeruan zer barri?
Zeruan barri onak
orain eta beti.

Monjen erregaluak
intxaurrak oi dira,
ai arek ere guztiak
bai piñak balira.