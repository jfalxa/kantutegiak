---
id: ab-4464
izenburua: Besigu Usaiña
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004464.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004464.MID
youtube: null
---

Besigu usaiña artu orduko
treotzaria buruan,
antxoa txikiz betetuteko
eun ta milla amuak,
atzapar puntak otzez beterik,
begi erriak malkoz bustirik,
lan au maite duan gizonik
oraindik ez da sorturik

Tretza bakotxa txoriandi bat
or gure sari kaskarra,
negu baltz onek ba dauka gutzat
gose, negar eta zorra.
Belarrionduak ospelez gori,
espan mamiñak indarge ori,
lan au maite duan gizonik
oraindik ez da sorturik.