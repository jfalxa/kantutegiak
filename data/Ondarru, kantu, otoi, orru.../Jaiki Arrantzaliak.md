---
id: ab-4469
izenburua: Jaiki Arrantzaliak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004469.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004469.MID
youtube: null
---

Jaiki arrantzaliak
potiña zuen zai beti
Errikuak ez daki
itxastarraren berririk,
lurra agurtu ta itxasoan
eguzki eta illargi.

Ez dizut irikiko
gau illunean aterik,
etxekoak ez daki
kalekoaren berririk,
atoz eguna argitzen danean
egongo gera alkarrekin.