---
id: ab-4460
izenburua: Aratustetakoak Karnabal Kantak
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004460.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004460.MID
youtube: null
---

Alkate jaunan baimenarekin
bakian gera atera,
kalerik kale kantuan gatoz
karnabal zelebratzera.
Urten guztiok balkoietara
mariñelak ikustera,
gauza ontxo bat emon egizu
limosneria ba zera.

Asko daukanak kapoi eder bat,
edo korda bat txorixo,
solomo zati lodi lodi bat
pozik estimau leikixo.
Beste guztiak txakur txiki bat
ori be ezta preziso,
zerren senarrak tabernarako
lenago kendu leikixo.

Lorentzon kantak, Bartolon dantzak
Antonio Marin barriak,
ondo pozikan entzuten ditu
emen dabillan jentiak.
Urrufiñoren zortziko eta
Soteroren komediak,
Ez deutsa iñori gatxik egiten
olako meriendiak.

Kaleko jira eginda gero
daukagu meriendia,
ara juateko pozikan dabil
gure kuadrilla guztia,
usain gozozko tajadak janda
gero gañian kafia,
ez dakit zeiñek pasako daben
karnabal azken obia.

ERREPIKA (Estribillo)
Juan dan urtietako
gure kostumbria
komeni da aurrera
ala segitzia.
Afari ona jan da
gero kantatzia
lengo usario zarrak
betiko ez galtzia.