---
id: ab-4462
izenburua: Kuadrillan Batzen Gera
kantutegia: Ondarru, Kantu, Otoi, Orru...
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004462.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004462.MID
youtube: null
---

Bertso berri batzuek
bera ditut jarri,
aurtengo neguaren
pasadizuari,
tximista eta trumoi
txingor eta euri,
ederto benga dezu
aurten pobreari.

Ez da ederra izan
aurtengo negua,
pobrientzat ez da izan
entero berua,
mendebal, norteko aixe,
ipar pikarua,
aretxek eman digu
sarri endredua.

Ezer entzun al dezu
lagun albokua,
ba badala diñue
eta tartiua,
makallau, sardiña zarra
edo potxerrua,
gustora edango degu
gero sagardua.

Kuadrillan batzen gera
Zubizar azpira,
guazen itxas bistara
Kamiñu barrira,
begira jartzen gera
norteko alderdira,
ez da itxura, guazen,
sagardotegira.

Neska bat sagardaua
ematen ari da,
danok juaten gera
bere alderdira,
geuri begira dago
beti arpegira,
pagatu gabe edo
juango-ote geran.

Andikan nunbaitera
etxera abia,
lenengo errietan
asten da andria
nun ibili zera
alper tunantia,
laster gastatu dezu
zeure pesetia.

Andria esango dizut
nik zuri egia,
beti esango dizut
nik zuri egia,
bada ez det gastatu
baizikan erdia,
egingo dizkiot nik
oneiri guardia.

Ordu batak jo eta
beian inguruan
señeru ate joka
orra nun dijuan,
otarran ogi gutxi,
jakia auzuan,
otz egonarren gizonak
bearko du juan.

Besigu asko ba da
gaur bai Ondarruan,
berrogeitxuren bat arru
zeuek bai orduan,
neretzat ba al da ezer
bada lapikuan,
trontxo murtzillua
or dezu barruan.

Beste gauza bat ere
ba daukat goguan,
ezer ete dakarren
nago esperuan,
lau besigu andi andi
or dezuz barruan,
txikiña kaurtillerdi
ekarri orduan.