---
id: ab-2
izenburua: San Sebastian
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000002.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000002.MID
youtube: null
---

Ardoac para gaitu
cantari bi eta dantzari
ez gaituc mutilloc egarri
ez ori segurqui:

Gure condicioac beti
tabernaraco
esquean gure finac
berdin bearco:

Ecin faltatu
condicioac guc
onac ditugu,
goicean Joaten guera
tabernara
eta arratsean
beranduan echera
gueroc joaten güera:

Bañan alere betor ardoa
edan dezagun
eta gauden aleguere:

Atzo eta egun
ta eranegun ere bai
izandu diagu
gure Errian Jai:

Tabernan beti
sarturic gaude
edanaz ardo gorriti
orditu gabe zuti:

Arropea zar
eta eztarria garbi
zorra franco bai
baña artceco guchi:

Bart gure echean aserre
batzuetan bai neur ere
ez orain bere
gauden aleguerementean
egongo gueraden artean
lecu batean pichar aldean
berdiñ gure fiñac
bearco dic
ospitalean:

Au da san Sebastian
orain dantza gaitean
ia goaz erdian aurrera:

Dantzan dabillenac
arin cetic oñac,
baldiñ ezpaditu oñac ariñac
ez du lucitcen,
bañan orrec ederqui dic dantzatcen:

Ardoaren bentajac
ain dirade aundiac guretzat
gure biotzac,
ardorican edan gabez
ceauden otzac.

Atzo goicean tabernan
ongui neurria
edan nuen
Chopiñ erdia.

Esan bear det eguia
Arrechec ill ciran
neuri egarria.

Zorionean ninzan
mundura ni jaio
cergatic deran naiago
ardo gorri nafarreti
andrea baño.

Ardoa ta cartac
ditut adisquide
ez det nic
beste maiteric:

ez det bearreraco
aficioric aleric
ardoac naduca
enamoraturic:

Ardoric ez bada
neronen biotza
guelditcen da motel
eta ill otza.

Luzatuco liquet
biciac
nerezat balira
ardo guciac,
luzatuco liquet
bicia
nerezat balira
ardo guciac.