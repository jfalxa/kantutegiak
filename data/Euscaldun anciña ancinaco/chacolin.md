---
id: ab-21
izenburua: Chacolin
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000021.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000021.MID
youtube: null
---

Chacolin, Chacolin,
Chacoliñac on eguin,
Maricho arincho da Martincho:
Ase naiz nafarrez
churi gorri ta belzez
jarri naute miñez,
gabe ere onic ez.