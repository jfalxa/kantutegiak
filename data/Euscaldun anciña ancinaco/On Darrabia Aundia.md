---
id: ab-14
izenburua: On Darrabia Aundia
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000014.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000014.MID
youtube: null
---

On darrabian daude
dantzari bi ederrac,
ezagutcen ditu Bernardac,
dantzara ez padute
eramaten berac,
gajo onec eguiten ditu
benazgo negarrac

Bata da Martin
eta bestea Pachico
au ecusi gabetanic
ez luque etsico.
Soñu zaretan ere
dantzariac dira,
zoraturic dago beti,
Bernarda oiei beguira.