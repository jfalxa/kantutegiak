---
id: ab-43
izenburua: Ezcon Berriac Lotarat-Ceco Sonua
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000043.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000043.MID
youtube: null
---

Ezconberriac
pozquidaz daude
pozquidaz daude
eguin diralaco gaur
alcarren jabe
ilizan
gauza ederragoric
ecin izan,
hai orain banengo
ni zuen dichan.