---
id: ab-17
izenburua: Ormachulo
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000017.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000017.MID
youtube: null
---

Ormachulo batetic
gau batean iguesi,
arratoi beltz andi bat
nuen nic icusi,
cijoan pizcorqui:

noranai saltatceco
etcegoen gaizqui,
bañan atzaparra cion
catu batec ezarri:
humildu zan sarri.