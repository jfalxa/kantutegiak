---
id: ab-12
izenburua: Punta Motz
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000012.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000012.MID
youtube: null
---

Gure Aquerrac ditu
punta biac motzac,
cerren galdu ciozcan
neguco itzozac:

Aquer galanta da ta
guziz egoquia
damuric icusten det
puntac ebaquia
alferric ematen diozcat
jana ta jaquia.