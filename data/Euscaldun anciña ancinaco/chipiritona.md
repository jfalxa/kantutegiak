---
id: ab-19
izenburua: Chipiritona
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000019.MID
youtube: null
---

Plazaren erdira irtenic
Andrea ta guizona,
dantzatu bear dute ongui
biac chipiritona,
anciñaco usario au
chit da ona.