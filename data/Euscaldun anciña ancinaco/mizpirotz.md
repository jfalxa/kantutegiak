---
id: ab-22
izenburua: Mizpirotz
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000022.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000022.MID
youtube: null
---

Zure aitac esan dit
nere maite polita,
mispirac dauzcatzula
ustelcen jarrita:

janari gozoa
da guziz mispira
otz ustela
plazan salducen zaudela
beguira erosleai
benaz eta ernai
galdua cera bestela:

Salmenta charra dute
mizpira cimurrac,
bigundu arteraño
mami ta ezurrac:

Salmenta onecoac
badira mizpirac,
erosi gabetanic
guelditcen ez dira

gozoa eta piña
eguteran eguina,
ez da izango miña:

icaratzen baditu
aguin eta ortzac,
jaten asi orduco
mizpira ustel otzac,

gozoa dalarican
ditu bere tachac,
aranzaquin osto chit lachac,
cimurtu ezqueroztic
elcor ta mincachac,
badaquit dirana gaitzac:

Cuidado andia bear du,
cerren eltcen da chit
berandu,
sosoia eguiñ arte
eldueguia ere
ez bart da on beñere,
arretaz contu eguin
bear zaio mizpirari
lanic asco izango baita ala ere,
esaten dizut
zure oneraco,
etzaizu bada
seculan damutuco.