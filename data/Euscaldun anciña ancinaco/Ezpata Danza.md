---
id: ab-31
izenburua: Ezpata Danza
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000031.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000031.MID
youtube: null
---

Gaur ezpata dantzari
aurren azquendari
Corpus da san Juan,
dituztela gogoan,
Zaldibiatarrac
gazte eta zarrac
oi dabilza danzan
beren Errico Plazan:
Gozo edo miña
Martin damboliña
arturic berequin,
Erricardorequin,
dantzari ariña
omen da capaguiña
juan martin,
nic ori ongui jaquin:

Ha! nolacoa Olacoa
danzatcen ezpata, bi
icusiric nago ni,
aren anca, eta planta,
ezta ichusia,
plaza asco icusia.

Gueldi gueldi gueldi
Berdillariri
erabilli compasa,
ez daquion pasa,
Martiñec badaqui,
nola erabaqui,
aren zortcicoa
ez du lendabicicoa:
Diego belcha
asco oi da alcha
arturic ezpata
danzari polit bat da:
Yru chiqui eta
chiqui-chiqui
gueienean aurrean,
biac alcarren parian:

Zubieta,
badu beta
danzatcen zortcicoa,
guziz da pijoa,
Ermentaria,
cer danzaria,
eta Longinos,
icusi ditut inoiz.

Echeric eche
an ta emenche
guizonac eta mutillac;

Salto ta brinco
eguiñic pijo
ustuceituzte botillac.

ceñec achi
Olaco pachi
erabilten ezpata:
Zancoac Arin
Orain baldin
Arras galdu ez bada.