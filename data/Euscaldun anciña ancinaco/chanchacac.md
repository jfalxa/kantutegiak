---
id: ab-4
izenburua: Chanchacac
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000004.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000004.MID
youtube: null
---

Chachancho gazte polit bat
maitatcen det aspaldian
bera bezelacorican
ez dago Errian:

Gorpuz galanta du egoquia
illea guziz lodia
begui belch alaiac guerri lirain
ezpaña chit gorria.

Galai gazte aniz
damacho onen ondoren dabiltx
gau ta egun
beste bat alacoric
ez baitago iñum
ederra da segurqui
berac ere badaqui
bicia galdu lezatque
edo ceñec argati.

engañaturic gaduzca
beste asco eta ni.
Gaztetanic du Aingueru onec
guztiz asco icasi
aldamenean beti nai ditu
aberatsac icusi:

Aingueru au eta
beste lau galai gazte
arrats batean
politquicho eta gozocho
ceuden alcarren aldean.

itz eguiten cioten ederqui
bañan ez daquit
maite ote duten biotceti.

zure ondorendic
dabiltzan galai gazteac
baldin badirade
eche oneco semeac
beren idecoac
naico dituzte emazteac.
Zu utziric artuco dituzte besteac.

Maite zaituztela
esanagatican zuri
juango zaizquitzu
itzuli.

Ezconceco firme
eman arren itza
galai aberatsaquin
contuz zabiltza:

izan cenzaque miña
orain ez bada ere bai guero
adisquide oriec eguiña.

Nai dizutela asco
esango dizute beti
bañan ez nic becela
biotz biotcetic.

Galai maitagarria
iru urte onetan
badaquizu bai
nere barrengo berria.

Nai dizut asco
biotz biotcetic
ez det iñor
zu beste maiteric:

gau eta egun beti
zauzcat gogoan
zu beste maiteric
ez det munduan:

Galai aberats guci
guciac ditut utzi
cergatic nai dedan
zurequin bici.

Oroitcen naiz ongui
nola esaten nizun
arrats batez geundela
bacarric
itc eguiten guelan
zu ta ni
beste guciak baño
ascoz maiteago
izango cinduztala
beti beti.