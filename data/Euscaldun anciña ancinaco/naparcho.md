---
id: ab-16
izenburua: Naparcho
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000016.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000016.MID
youtube: null
---

Naparroatic etorten da
Ardo gorri gozoa,
archec consolatcen dit
niri biotz gaxoa:

Bicicai ona baita guztiz,
erosteco dirua baliz,
edango nugue asco aldiz,
naiz echera eramal zaldiz:

ez det abererican
dirurican ain gutxi,
bañan adisquide
tabernako Luci,
ez nezaque bada nic
edan gabe utzi,
cergatic berac nauen bici:

Ardoric edan gabe
nagoen eguna,
eduritcen zait bada niri
dala guciz illuna:
Picharra ongui bete bedi,
eta sarritan eman
edatera niri,
biotza aurquitcen zat
motel eta eri.