---
id: ab-24
izenburua: Belaun Chingoa
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000024.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000024.MID
youtube: null
---

Euscal Errietaco
soñu ezti,
izan dana beti,
gucien gañetic,
billatu det lurpean sartua,
bai eta aztua,
ceren dan zartuba:

ateratu bear nuque plazara,
nerequin danzara,
oraindic gauza da,
ecusico da
ceiñ atseguiña,
bizarra urdiña,
baña chit arina.