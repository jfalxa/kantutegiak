---
id: ab-7
izenburua: Betronio Aundia
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000007.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000007.MID
youtube: null
---

Betronioc
Pepachori
dio
biotcetic amorio:

Zu cera Betronio aundiaren
chachancho piña,
bestec eramatia
izango muque nic miña:

aurra maitea,
cerorrec daquizuna da
gauzarican oberana
dala guizona,
indarrac berac dituana
badaquizu bada
nai izateco dana,
argatic dizut nic
aditcera ematen
bici bear degu
alcar amatcen.