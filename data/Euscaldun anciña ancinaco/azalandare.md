---
id: ab-8
izenburua: Azalandare
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000008.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000008.MID
youtube: null
---

Azalandare saltcera
maitea
ciñan etorcen
beguiac argui eta
colorea gorri:

naitasun aundia ceñidala,
sinistu nuan eguia zala,
bañan gueroztican jaquindu baitet nic,
bestegandican ixillic
ez dirazula naico
ezpadet diruric
banequique segurqui
ori dala eguia
penaz ill naizala ni
cenduque berria.