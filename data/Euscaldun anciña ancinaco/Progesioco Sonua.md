---
id: ab-40
izenburua: Progesioco Sonua
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000040.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000040.MID
youtube: null
---

GureJaungoico maitatia
izanic onguile aundia
cerutic etorri zan
bai lurrera.

Adanen becatu gaiztoa
sendatutcera
bestela, guinjoatcen galtcera,
Guztion jaun ta jabe zu cera.