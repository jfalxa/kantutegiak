---
id: ab-6
izenburua: Betronio Chiquia
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000006.MID
youtube: null
---

Betronio
zu diruac aituagatic
maite zaitut bada
biotz biotcetic
baldin zuc ni ala
nai banazu izan
jaincoaren legueac
aguintcen duen guisan
zure emazte izateco
jaio ninzan.