---
id: ab-10
izenburua: Eun Dabicoa
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000010.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000010.MID
youtube: null
---

Soñu onen puntubac
dira eun ta bi
adisquide bati
beste ainbeste ducat
emanic nago ni,
nere ustez aurquitcen da
pagaturican ongui,
gueiago bear baldin badu
esaten diot orri,
escatu nai badio
baduela nori,
langai orretatic libre
aurquitcen naiz ni:

naitasunic baldin badio
badu biderican asqui,
ematen badizu nic ainbat
ezta izango chit gaizqui:

nai dezunean atoz
art,cecoen esque,
nic badet hainbeste,
bidezcoa dezula
baldin badezu uste,
zorrac gueroco utzi gabe,
pagatu nai nituzque
lengoac eman nizquizula
orainchen da bi urte,
echean baneuzca gaur
naiago nuque
ala ere beti
izango zaitut maite.