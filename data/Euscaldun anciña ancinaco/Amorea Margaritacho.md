---
id: ab-11
izenburua: Amorea Margaritacho
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000011.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000011.MID
youtube: null
---

Amorea Margaritacho
nola zagoz tristeric
zagoz aleguereric:

zu horrela icustea
ez det nic atseguin,
nai badezu Jaquin.

Atsecaberic asco
zu ikustean nic,
oi daramat maitea
biotcean ixillic:

bañ an alferric.
Jaquinean aurquitutcen naiz
et zaudela nerezat
bañan ecin aztuzat.