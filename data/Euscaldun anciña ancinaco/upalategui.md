---
id: ab-18
izenburua: Upalategui
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000018.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000018.MID
youtube: null
---

Edan ezac goicean
churitic onetic,
arratsaldian
gorritic obetic;

ala eguiten oi diat nic,
eta ic eguic, al badaguic,
on eguingo dic.