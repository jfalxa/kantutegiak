---
id: ab-39
izenburua: Alcate Sonua
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000039.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000039.MID
youtube: null
---

Alcate Jauna bedori
ezagutcen degu aguintari,
Jostaldiatu nai guenduque
bedorrec atseguin baluque
naierara ta garbi:

Ynory ez gaitcic eguin
izan ez nezaquean min,
atsecaberic ez eman niri
jostatu zaitezte emequi
Erria pozquida dedin.