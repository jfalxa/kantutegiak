---
id: ab-20
izenburua: Erreverencia
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000020.MID
youtube: null
---

Erreberencia
Gorputz egunean
elizan sarturic
jaunaren aurrean,
ezpata birequin
laudatuaz gure jauna
badaquigu dantzatcen dana:

Onestasun guciz andiarequin,
lendabici jaunari agur eguin
ala aguintcen baitigu legueac,
ceren gaituen guztioc bereac.