---
id: ab-9
izenburua: Erreguela Zarra
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000009.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000009.MID
youtube: null
---

Atsecabe gaizto bat
daducat biotcean
eritcen nau maite
zuzaz oroitcean:

egunaz eta gabaz
ordu gucietan,
nai nuque quendu baña
guelditcen zait bertan:

Ar gaizto au portitza da
zulatutcen nau lazqui,
urtebete onetan
narabil chit gaizqui:

eciñ jan eciñ edan
eciñ loric eguin
nere barrengo corde
gucietan det miñ:

gaitz au quentceco cera
maitea sendaquin.

Aingueruchoa badaquizu
miñ au nondic datorren
galquitatuago ez dedin,
senda eguidazu arren.

Len, baño len
guztia espada
erdia bereren:

ill bear det bestela
egun guchi barru,
aurquitutcen naiz ia
ez mami ta ez larru,
ainbeste biguntasun
nere maiteac al du,
muga aldia on au
galdu naico ez du,
beretzat izateco
usterican badu:

Zuri itc eguin eta laster
esan nioten Aita Amai
zu bear cinduztadala
baldin bazuten nai:

bata bezala minzatu
citzaidan bestea,
zu izango ziñala
nire emaztea,

aien borondatea zala,
zuc nai batcenduen ala:

Adieratzo nizunean
nic aien borondatea
naitasuna aguertu ceñidan
nere maite maitea:

Guerozti izan dezu
aldaira aundia,
nai badezu esan eguia
orduan alaiago cenidan
ifinten beguia,
besteri nai diozula
artu det berria.

Utsic aurqui gueldi cintezque
ainguerucho gaztea,
ni seguru naucazularic
nai badezu bestea.

Esan bear dirazu bertan eguiazqui,
baldin nai badirazu niri,
eman bear diozu
etsimena besteari,
ecin izan cinceque
inolaz ere bi.

Galai gaztea engañaturic
aurquitutcen cerade,
ez det maite iñor besteric,
zu cerade nere jabe.

Yduripenac dizquitzu
eman naitasunac,
ez det maiteric iñon
baicic ezagunak,
iñoiz eguiñagatik
oequin jardunac,
badaquizu dirana
zure gazte lagunac

izan zaitez guizon
zu hainbat maite deranican
ez det bada nic inon.

Uste baldin badezu
besterena naizala ,
engañaturic zaude
ez naiz bada ala:

ytza eman nizun
ta nai nuque ornitu
jostaquetara ez naiz oitu,
ain guchi bestec itza
zuc baicic cogitu.