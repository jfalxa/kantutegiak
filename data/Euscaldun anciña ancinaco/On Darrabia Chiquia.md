---
id: ab-15
izenburua: On Darrabia Chiquia
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000015.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000015.MID
youtube: null
---

Felipe bostgarrena
zanean etorri
Españiara aguintari:
Yrunen dantzatu zan
Pachico chiquia
arritu zuen jende guztia,

arren dantzatceco
jaquinduriac,
pozquidatu zituen
Españiaco guizon aundiac,
Erregue maite du
On darrabiac.