---
id: ab-5
izenburua: Eun Ducatecoa
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000005.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000005.MID
youtube: null
---

Eun ducatcho
banituen bada nic
orain bi urte
nere Aitac emanic:

Erdiac maiteari
niozcan eman,
eta beste erdiac
edan eta jan,

ez det orain batere
orrengatican maiteagana
Joan ninzan bart ere
Joango ere bai
nere maite politac
baldin badu nai.

nola naico ez det bada nic
maitea etorcea zu
baldin ceuretzaco
izan bear banazu.