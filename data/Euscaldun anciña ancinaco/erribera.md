---
id: ab-13
izenburua: Erribera
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000013.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000013.MID
youtube: null
---

Erriberaren alaba
colore gorria
ederqui dantzatcen dezula
zabaldu da berria
plazan icusi nai cinduzquet
chachan maitagarria.

deituco zaitut bada
urrengo Jaiean
serbitu nai banazu
bear dan gaiean
nere pozqui da aurquitcen da
dama zure naiean.