---
id: ab-23
izenburua: Graciana
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000023.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000023.MID
youtube: null
---

Amorioac nauca
biotcetic eri,
esan gabetanican
ecin nengoque ni:

Pena bat oi daucat biotzcean gorderic,
Graciana icusi nuen ezqueroztic,

Ai au pena ta naigabea
maitecho graciana,
zureganaco miña
sartu zat nigana:

badaquite Ceruac
maite zaitudala,
nere poza litzaque
baninduzuala:

zuregan dago
nere ona eta gaitza,
eman biar dirazu itza,
bestela ill bear det
nere maite biotza.

Maite izango zaitut beti,
zuc ala biotcetic
baldin banazu ni.