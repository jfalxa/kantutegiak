---
id: ab-3
izenburua: Galantac
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000003.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000003.MID
youtube: null
---

Dama gazte galant bat
ikusi det bart arabaquetan
engañatutcen nauela
bere ustetan
ez naiz fiataco berriz
arren itcetan.

Maite nauela aniz
esan izan dit asco aldiz.

Dama gazte galant oni
nai izan diot ongui oraindañocoan
gau eta egun iduquitu det gogoan.

Maite ninduzula aniz
biotz biotcetic
esan didazu ascotan
cerorrec Jaquingo
bait dezu cergatic,
ringueruchoa nai dituzu
besteac bat becela zuc bi.

Nai badezu eguin
maite biren Jabe
guelditu cinteque
bada bat ere gabe.

Galaia alaia nic naia
cerade zu beti
iduripen gaiztoac
quendu bear dira burutic
munduan diraden guizonac
utcico ditut nic zugatic
baldin nai banazu ni.

Galai gaztea
zu penaturiccan icustea
badaquizu dala
neretzaco illcea
eguizu gauza oec
burutic uztea

atoz
nere aldamenera zaitea
biotzetic et zaitut
utzico nere maitea

Dama gazte begui arguia
esaten banauzu eguia
zurea izango naiz beti
itza ematen banazu
biotcetic
beguira ordean
niri besterequin ibilli Jostatcen
alacoric ez zait gustatcen.