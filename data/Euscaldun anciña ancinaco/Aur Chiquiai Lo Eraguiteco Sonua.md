---
id: ab-44
izenburua: Aur Chiquiai Lo Eraguiteco Sonua
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000044.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000044.MID
youtube: null
---

Aurcho chiquia negarrez dago
Ama emazu titia
Aita gaiztoa tabernan dago
picaro jocularia.