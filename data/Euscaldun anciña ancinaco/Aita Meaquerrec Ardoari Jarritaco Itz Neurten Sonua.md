---
id: ab-50
izenburua: Aita Meaquerrec Ardoari Jarritaco Itz Neurten Sonua
kantutegia: Euscaldun Anciña Ancinaco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000050.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000050.MID
youtube: null
---

Ni naiz chit gauza gozoa
eta pozquida osoa
belza naiz ta zuria
illuna ta arguia
indarra naiz ta garboa
eta icena det ardoa.