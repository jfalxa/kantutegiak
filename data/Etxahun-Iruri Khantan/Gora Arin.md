---
id: ab-3720
izenburua: Gora Arin
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003720.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003720.MID
youtube: null
---

Larhüne gañetik jauzi iduzkia
Donibane-ri eskainiz lehen leñhuria
Jeiki ene aurhidiak
Denak guatzan zahar gaztiak
Gortik laidatzera
Untsa uhuratzera
Gure Arin-aren berrogei urthiak

Errepika
Gora ARIN
Gora Donibandarrak
Bethi lorius iduzkian bardin
Argi zazü gure Eskual Izarra
Altxa gure indarrak
Bizi dadin
ARIN

Arin-erat jin nintzan aitaren ondotik
Eta irus bizi niz han sarthü geroztik
Lagün hunak nik han bazitit
Aintzindari zuiñen Xarmantik
Egian erraitera
Nulaz neinte debera
Ez niz hürrüntzera nindeüseren gatik

Arin güziek egün uhura zitzagün
Haiñ bestetan zütügü ikhusi txapedün
Zaude bethi fidel Eskualdün
Orai bezaiñ maitha zitzagün
Segitüz bide berra
Altxa gure bandera
Ziren zerbütxari bethikoz ETXAHU.