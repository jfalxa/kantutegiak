---
id: ab-3722
izenburua: Ligiko Khantoria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003722.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003722.MID
youtube: null
---

Ezinago da eder üda bethin bortia
Eta partikülarzki zohardi batekilan
Goizian ezagütü dit harat bürüz juraitian
Herri xuri goxo bat Xiberuko lilia

Herri xarmant hori da bortüz üngürtürik
Ligi deizen da eta orotan famatürik
Izana gatik heltzeko bidia den bühürrik
Han irus bizi dira ez hor ükhen beldürrik

Ligiko ühaitzian arraña hanitx fina
Bena eskietatzeko ezinago maliña
Horen gatik Ligiarrek badie aski konbina
Truieta franko jaten die herri aizuek txardina

Larrazkenin ürzuak goizik dira phausatzen
Ligiko hitxtoietan laket dira bazkatzen
Ihizlariek atzamanak ostatietan gozatzen
Truieta frando jaten die herri aizuek txardina

Ligin laket dirade zer nahi jokietan
Hor üsü igante da asteko egünetan
Hur Kiroletan igexka uheitz erremulietan
Bai edo süzko orgetan zunpakan botxietan

Egün jei ederrak gütü hunat ekharri
Beganakian bazela herri huntan batzarri
Ligiar eta Ligiarser ez düt erran beharrik
Desiratzen degüla bake eta osagarr.