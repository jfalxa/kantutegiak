---
id: ab-3682
izenburua: Goizian Argi Hastian
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003682.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003682.MID
youtube: null
---

Goizian argi hastian, ene leiho hegina,
Txori bat pausatzen da eta goratik hasten khantan.
Txori eijerra, haiñ alegera, entzüten haidanian,
Ene bihotzeko trixtüra laster dua aidian.

Ene txoririk maitena, zeren jin hiz ni gana,
Iratzarazi naik nialarik ametsik eijerrena.
Jinik hail goizik uste hian hik baniala hanitx phena;
Ez ez, abil kontsolatzera malerusago dena.

Txorittua juan düzü, ta ez arra agertü
Dolütan niz othe zaionez nik errana gaitzitü,
Goiz güzietan, ene khanberan, trixterik niz hidürü,
Nahiz haren aire xarmant bat berriz ere behatü.