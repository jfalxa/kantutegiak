---
id: ab-3697
izenburua: Gaindañeri
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003697.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003697.MID
youtube: null
---

Khantatzerat banua ikharra bateki
Haiñ beste beita jente haiñ beste argi
Khantazale lagünak Kuntent niz zireki
Deskantzatüren girela goxoki
Bi berset horieki nahinüke josi
Beztimenta eijer bat gure Gaindañeri
Gure Gaindañeri

Gaindañen bada mulde irus bizitzeko
Jentia aplikanta zühür et' agüdo
Herritarrak dira ofizialik oro
Bera erretora da manex-eko
Ez beita Xiberun aphezgeik habo
Lotsa sarthürik tresnak deitzela muztüko
Deitzela muztüko

Huntan üztent zütiet gaizo Gaindaiñtarrak
Aldika Khatarien untsa jorratzera
Ez baginande ere gustüko izarrak
Ez zitaiela Khexa sobera
Bihar jinen beniz xahal erostera
Bardin Kostilla baten zirekin jatera
Zirekin jater.