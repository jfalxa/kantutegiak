---
id: ab-3671
izenburua: Gora Eskualdünen Biltzarra
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003671.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003671.MID
youtube: null
---

Gora Eskualdünen Biltzarra
Gora Eskualdün Biltzarra
Hots, anaiak, junta gitia
Bordelesak Barkoxtarrak
Zelietarik Aiten Aitak
Soz ari zaizkü ikharan
Bizi gireno gük ez dügü ez ütziko
Hek egin lana galtzera

Bordeleko aurhide hunak
Agur bihotzan erditik
Egün hunat jinik girade
B er denboran igaraitera
Ixtant goxo bat zirekin
Gure fideltarzünaren erakastera
Ama Eskual-Herriari

Etxahun, batarzün berria
Deiziegü presentatzen
Beitakigü Koblakari zena
Denek düziela aiphatzen
Harek egin Khantore ederrak
Nihurk ez dütü bardintzen
Eskual-Herri güzian orano bizi da
Partikularzki BARKOXEN

Ama Eskual- Herri maitia
Denbora zalhe beitua
Nahi günüke berriz bizi
Egünko jünta goxua
Algarrekin berriz Khantatü
Zuri dügün amodiua
Zeren eta zure haur fidelak bikira
Zabal itzazü besua.