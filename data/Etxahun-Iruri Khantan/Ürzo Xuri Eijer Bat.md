---
id: ab-3734
izenburua: Ürzo Xuri Eijer Bat
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003734.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003734.MID
youtube: null
---

Ürzo xuri eijer bat jin zaikü goiz huntan
Bost-Menditan behera afera handitan
Ihautian ginela señalatzekotan
Denak erauzi gütü Ibar-Esküñ hortan

Ene ürzo pollita zertako jin zira
Gure xokho trankilan hola harrotzera
Uste nin jun zinela lekhü beruetara
Bedastila artino irus bizitzera

Urthe oroz delarik heltzen larrazkena
Saldo handian gira jiten zien gana
Izanik er' AFRIKA lekhü beruena
Ni hunat ützüli niz hau beitüt maitena

Sasurik ederrena beita ihautia
Harek manhatzen deikü libertimentia
Zer othe balio dü gure bizitziak
Ez badütügü hartzen gure deskantziak

ALTZAI, ALTZABEHETI eta ZÜNHARRETA
LAKHARRI-Kin ARHANE eta XARRIKOTA
Plantatü zidelakoz aurthen maskadaka
Xiberun dükezie zerbait arrakasta

Hortan ützen zütiegü ALTZAIAR gaixuak
Ohart etxekiz zien Karesü goxua
Esker mila oroer eginik adiuak
Biba Ibar-Esküña gor Xiberu.