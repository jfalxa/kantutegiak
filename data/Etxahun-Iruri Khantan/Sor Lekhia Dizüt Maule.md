---
id: ab-3696
izenburua: Sor Lekhia Dizüt Maule
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003696.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003696.MID
youtube: null
---

Sor lekhia dizüt Maule
Eta irus bizi niz han
Afekzionerik hobenin
Espusa eta haurrekilan
En' aita et' ama orano bixkor
Beren adin ederrian
Posible deia izatia
Irus ago mündü huntan

Ene ait' et' ama maitik
Adinak zütie markatü
Nahiz eta ziren semer
Ogia untsa segürtatü
Phenatü zidie heñbat pribatü
Ben' ez sekülan etsitü
Ordaritzat nahi deiziegü
Zahar denbora eztitü

Ene espusa zü zira
Etxen behar nin izarra
Ümila eta nulaxek
Eraikitzen gure haurrak
Zer zorthe hunak gidatü nündin
Zure hunat bilatzera
Errekunpentsa nitan baduzü
Merexi düzün senarra

Haur goxuk handi zitaie
Ait.et.amen itzalian
Hek erakutsi bezala
Unest eta mudest izan
Zorthiak ezar bazentzaie
Etxetik hürrün lanian
Ziren haindürü girateke
Bethi Maulen sor leki.