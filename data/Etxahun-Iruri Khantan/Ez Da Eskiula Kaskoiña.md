---
id: ab-3724
izenburua: Ez Da Eskiula Kaskoiña
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003724.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003724.MID
youtube: null
---

Nurk erran othe zian:
"Gaskoiñ da ESKIULA
Gaztek ukhatü diela
Zaharren odola ?"
Ez ! Eskuara bizi da
Han bethi bezala
Eta orok laidatzen
Eskualdün arbola
Botzik girade ESKIULA
Egon beitzira fidela
Eman bethi hola hola

Errepika
Gure tradizionetan
Pastoal eta dantzetan
Ikhusi zitzegün
Bethi gure lagün
Xiberuko bügetan
Bandera eskietan
Bihar nula egün
Bethikoz Eskualdün

Xiberuko plazetan
Seküla geroztik
Aktü hunik izan da
Jinik Eskiulatik
Nurk ez dütü aiphatzen
Legi et' Oskabi
Uthürritto anaiak
Barrake haiekin
Gaztik badira ondotik
Dabiltzanak ber bidetik
Bota aitzina gogotik

Kunpañari ororo
Zahar eta gazter
Eskiulak derizie
Gogotik mila esker
Egin dereiküzie
Egün hanitx plaser
Gurekilan juntatüz
Usaintxa zaharra
MATALAZ zena bezala
Maitha bethi xiberua
Et' Eskual herri güzi.