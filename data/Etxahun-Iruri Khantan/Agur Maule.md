---
id: ab-3714
izenburua: Agur Maule
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003714.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003714.MID
youtube: null
---

Eijer da goiz aldian Maule iratzartzen
Lehen ekhi leñhüria Gaztelü gañen phausatzen
Lankhietarat bürüz mündü bat zotükatzen
Plaza gañetik bandera denen agurtzen :AGUR !
Plaza gañetik bandera denen agurtzen

Errepika
Agur Maule eta Mauletarrak
Zier soz ari da eskual izarra
Agur Maule eta Mauletarrak
Begira ditzatzien üsantxa zaharrak

Agradabliz zira ó Maule maitia
Gaztelü onduan bibil zuiñen gaihera erria
Zützaz jeloz dütüzü gure bi Eskualdiak
Haiñ beita amiragarri zure ixtoria Maule
Haiñ beita amiragarri zure ixtoria

Damürik egiazki ez beniz Mauleko
Zure irustarzünaren algarreki gozatzeko
Zure amodiuaren goxoki Khantatzeko
Ene azken orenian zük lo-erazteko Maule
Ene azken orenian zük lo-eraztek.