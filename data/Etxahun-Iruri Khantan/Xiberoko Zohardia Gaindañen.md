---
id: ab-3723
izenburua: Xiberoko Zohardia Gaindañen
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003723.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003723.MID
youtube: null
---

Ez gal dure odol berua
Bizi dadin gure xiberua

Bihotzaren ersitik Agur Gaindañeri
Baï eta ber denboran mündü güziari
Egün jüntatü gira, khantari, dantzari,
Uhure egin nahiz Eskual Herriari

Plaza ederra dügü, berriz estrenatzen
Egiazki goxoki heben seskantsatzen
Aurhide girera denak, eta elgar maithatzen
Gaimdañek badakizü, gaizen untsa egiten

Sorthü berri delakoz, gure batarzüna
Xibero zohardia, horrek dü izena
Ükhen beza ausarki xantza eta fortüna
Xibero zohardia dezagün denek altx.