---
id: ab-3680
izenburua: Blankaren Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003680.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003680.MID
youtube: null
---

Ulhün ñabarra ixil ixila jin da
Bere thenorian, zelütik behera
Jeloskeriaz Nabarrako gaiz.ederren
Beste mündiari ahalaz gordatzera
Zoihartzeko txapela zaharrin
Sarthü da Erregiña Blanka;
Hor da bellhariko, begiak bustirik,
Jinik Birjinaren behartzera,
Othoi lagünt dezan Kastillanuetarik
Libratzen bethikoz Nabarra.

Aspaldi danik Nabarra eta Kastilla
Aizo dira, bena hozküra bateki;
Nabarr.aberats Kastill.aldiz praubia;
Hartakoz bekhatx da Nabarroaz handizki.
Zunbat aldiz ez zaikü izan
Gaxki giten armadareki!
Birjina Maria begira gitzatzü
Kastillako erregen eskü petik,
Gure erresuma hola egon badin
Eüskaldün orai eta bethi.