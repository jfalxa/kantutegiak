---
id: ab-3692
izenburua: Xiberuko Miralla
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003692.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003692.MID
youtube: null
---

Lehen argi leñhuria agertü da Kanpun
Ollarra Kükürrûkaz ari da barriun
Agur adixkidiak Jinkuak egün hun
Othoi erradaziet zer berri Xiberun

Berri hun eta gaxto balizate franko
Aski Kuriüs denari heben Khuntatzeko
Bena orai badügü eginik hortako
Agerkari berri bat berhez Xiberuko

Denek egin dizogün gogotik batzarri
Amodioz Xiberuko delako mirallari
Eman ditzagun dener sor lekhüko berrik
Eta partikülarzki hürrüneko horik

Gure aurhide hunak bazter guzietan
Herrokatürik dienak beren aferetan
Holaz badakizie untsa zer dabilan
Xiberuko lürretan bere sor lekhian

"Xiberoko miralla" orai sorthü berri
Egiozü agur bat Orhiko txoriari
Maria Maidalena eta Sant-Antoni
Kuntenterez air zaitzu othoitez Jaunari.