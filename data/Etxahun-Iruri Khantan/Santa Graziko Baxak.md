---
id: ab-3721
izenburua: Santa Graziko Baxak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003721.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003721.MID
youtube: null
---

Bortü onduan hanitx gora da hedatürik Santa-Grazi
Haren hobeki ezagützeko egin dire bide berri
Alde batetik botxü latza eta baxa alde petik
Halerik ere dena beitügü esker Jaun Erratorari

Bide hori igaran eta Jinkua zer hasperena
Ez beleiteke arra jeiki alde pialat juiten dena
Merkhatian ahatze dianak goizeko urhats xüxena
Kapable da ülhün onduan Kausatzeko hanitx phena

Igaran merkhatü gai batez herria zaikü lotsatü
Jakinik han fatür gazte bat emen zaiküla larrütü
phentsa so egilia zenez memento hartan phentaü
Txerkazalik zutianian prekoizionez behartü

Nihurk ez lïke xühürrian indar bat Kasü hortako
A ! Eta ülhün zen gai-aldia bai ta denbora gaxto
Bena phekaü txarrak dirade harien Korajian hozteko
Berheziki juranez geroz gizun baten salbatzeko

Harien mereximentia hola gaiaren minian
Desgune hetarat furaitia haren salbatü nahian
Eskerniatürik ari ziren orano jaik berantian
Ikhuslia emen zelarik plaser hartzen axolbian

Ikhertürik larre handia eta barraje bazterra
Bortxatü izan zirnian ofiziuan üztera
Bihotza trixte juriten dira süjet gaizuan etxera
Beren mezü dolorusaren etxekuer egitera

Aita xaharra jeiki zaie aski oihegin onduan
Iratzarrazi direlakoz malezia bat goguan
Erraiten den Hau gaitz horrekin hulaz zidie Kanpuan
Barnerat sar litian eta bero suthondo xokhuan

Estranje zaizü thenore huntan gure heben ikhustia
Denentako hobe zükezün hortarik esküsatzia
Zunbat nahi Krüdel zaidezün jakin beharzü berria
Merkhatütik horra zelarik meskabi zaizü semai

Süerte hori jakin ordüko txerkhatzera juran girade
Eta bazterrak oro ikhertü deüsere hexatü gabe
Nunbait bizirik izan baliz arauz ezagün lizate
Jauzkian hil ez bada unheitzien itho date

Berri horien oron jakitez aita aisa da harrtü
Erraiten de hau gaitz horrekin nula behar zen hexatü
Lehenik zenthü denez othe behar dügü segürtatü
Khanberan jakin eta bertan ziren artian nükezü

Txerkharaiak hots emazie hürrün uheitz bazterretik
Ez beita gure mothikua orano gaixki Heltürik
Ezagün da espusarekin intzirez ohe xokhotik
Arraiñtto-bten ezina elkhiz phentsu duzien hurretik

Mendi gañeko basa ollarrak Khantan hasi trüfatzeko
Zioielarik jinko bat bada gaiko arrantzalentako
Zunbat nahi intziri dian idorrilat jelkhitzeko
Hola igexkan ari deno ez da segürrik ithoko

Khantore horen süjeta aferaren jujazale
Nahigo da zenthürik beno lüzaz egon arrantzale
Txerkazalik hola izanez nula nahi phartizale
üsü nurbait ükhenen dire solaz berri phentsazal.