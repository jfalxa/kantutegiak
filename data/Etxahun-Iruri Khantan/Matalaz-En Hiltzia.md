---
id: ab-3678
izenburua: Matalaz-En Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003678.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003678.MID
youtube: null
---

Argi leñhuria hasi zaikü agertzen
Lextarreko plazan khürütxe xürian
Erregeren ordez harat Matalaz dua
Trixterik gaixua guardien artian
Bi eskik khatiraz nulaxe tinkatürik
Kriminelen gisa thian bizkarrian
Axko handiari büz hori zer lazkeria
Izigarrikeria ez denin ogendant

Ene Mitikile oi zer lekhü maitia
Bainan deseña lüzaz bizitzeko
Zure boztariuan zure bake hadian
Zure besuetan untsa gozatzeko
Jaun kunte txar batek ürgüllü ekhintia
Sobera zeitadan popüliarentko
Orain hil behar dit bainan nahiago düt
Hola eziz bizi Jaun horren esklabo

Adios Xibero zazpi probintzietako
En'uste orotan xokhorik hoberena
Orain heltürik da düdarik gabetarik
Jüjatü bazala en' azken orena
Dener pharkatzen düt Jinkuarek bezaka
Begira dezen ene orhitzapena
Burreuak zier orai goendatzen niz
Egin ezazie orai zien lan.