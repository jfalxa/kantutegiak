---
id: ab-3731
izenburua: Donostia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003731.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003731.MID
youtube: null
---

Bortüz heñbat satisfatürik itxasoz menguatia
Xibero lo etan ützirik bürütü dit juraliltia
Hendaia eta Iruñen gaintik ikhustera Donostia
Ta ber demboran besarkatzera hanko anaie maitiak
Ilüsitürik hantik horra niz haiñ beita xarmagarria

Larrazkeneko egün eder bat bazter güzik Koloretan
Itxasua bake handian bere zilharezko arropan
Barka xuri eijer aldian bat hurrin goxoki jünpakan
Ni haier soz amets batetan gogua phentsamentükan
Posible deia badiren martirak lekhümaithagarri hortan

Basitza ezinago ederrez handitzez dua herria
Aize gaixtuer bühürtzeko fierrik baditü mendiak
Baten Kaskuan amiragarri Birjina Dona Marian
Haren onduan leihora eder bat mundian pare gabia
Zer balio dü edertarzünak ez bada libertatia

Arrüetan mündü ozte bat nahas zahar eta gazte
Poliza gizun ulhün saldo bat Eskualdün garbien arte
Kafeterietan edari franko bena ez Khantorik bate
Bazterrrak irus üdüri ziren bena zü hor bardin trixte
Zure egiazko istarzüna O Donostia nuiz date?

Urthiak zalhe baduatza, laster jinen da berria
Zuiñek ekharriko beteikü arauz Donostiar maitiak
Bakian esperantxarekin osagarrri, pazentzia
Aski berthute begiratzeko argi Eskualdün fedia
Anaie gira eta izanen berriz arte Donosti.