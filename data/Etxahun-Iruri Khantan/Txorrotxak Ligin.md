---
id: ab-3703
izenburua: Txorrotxak Ligin
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003703.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003703.MID
youtube: null
---

Urthe berri jin zaikü ihautiarekilan
Aurthen lüzexko beita untsa goza gitin
Deliberatü dügü akortian ligin
Gaztek behar güntiala han maskadak egin

Ligi bethiz gerotzik beita alagera
Bai eta hanitx maite üsantxa zaharak
Ikhusirik bestetan duatzala galtzera
Nahi izan dügü eman etsenplü ederra

Arren erranen dügü egün hun ororer
Ligiko jente huner bai eta arrotzer
Maskaden ikhustiak dener beitü plaser
Bihotzaren erditik güzier mila esker

Egün hun berhezibat Jaun Mera berria
Eskual- Herri güan zuiñen maithatia
Agrada zaikü zure kuntentamentia
Ikhusiz zure ondotik dela gazteria

Jente hunak zütiegü bakian ütziren
Phentsü dügü zidela gützaz hanitx Kuntent
Gü hasirik beikira azkarki gosetzen
Kauseraz asetzera gira ezkapatze.