---
id: ab-3729
izenburua: Phastuala Barkoxen
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003729.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003729.MID
youtube: null
---

Zazpi probintzieako irusena
Hanitx jente ümen da Xiberua
Dügülatko bethitik bake handiena
Eta bortüko aide sanua
Uda bethin itzalik goxuena
Negian aldiz aize hegua
Gure ganat bijitaz jiten dena
Izaten dü batzarri berua
Zü beitzire lilirik eijerrena
Agur Agur Barkoxe gaixua

Egiaz barreat da gure herria
Eztügü oroek algar ezagützen
Bena denak besta handi bat denian
Kharrikazake gira jüntatzen
Eta ikhus zintzakie ordian
Bazter güziak nula harrotzen
Gure dantzari eta pelotariak
Ezinago ederki jokhatzen
Mündü handi bat jiten da ordian
Arrotza laket beita Barkoxen

Orai bortxatü girade aithortzera
Bazela egün mündü bat Barkoxen
Zahar gazte oro bardin alagera
Denak dütügü erremestiatzen
Orok batin egin dügü lan ederra
Izar handi bat da arraphizten
Eztezagüla ez ützi itzaltzera
Hortaz gazter gira gomendatzen
Ützül gitian üsü Barkoxera
Berriz arte deiziegü erraite.