---
id: ab-3689
izenburua: Maskaradak Sorhütan
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003689.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003689.MID
youtube: null
---

Urthe berria jin da ihautiarekin
Mintzatü da Jaun Phantzart goizan Hoki hegin
Xiberua güninez hiltzera üzten ordin
Oihüka Sohütarrak othoi jeiki gintin.

Behatürik Jaun hori hanko gazteriak
Salduan jünta eta hartü dü xedia
Untsa uhuratzeko aurthen ihautia
Boztarioz Maskaden orai plantatzia

Sohüta bethi danik izan da sall hortan
Uhurez bü agertü jokü güzietan
Pastualetan Koblakan bai eta barrakan
Eta partikülarzki ere maskaradan.

Xiberua othe deia gaztez gabetüko
Saldoka baduatza hirietat oro
Hirilat juan eta zeren egiteko
Sor Lekhiaz hobeki hantik orhitzeko

Esker mila oroer popülü gaxua
Lagüntza ükhen dügü ziren amodiua
Igaran dezagüla ihauti harrua
Biba Sohuta eta gora Xiberua.