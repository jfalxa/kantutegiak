---
id: ab-3673
izenburua: Zantxo-Azkarra-Ren Azken Khantoria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003673.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003673.MID
youtube: null
---

Zelü urdin zohardi eder batetan
Argi dago argi eskualdün izarra
Belhariko hari soz ari dago
Dolümenetan hor Zantxo-Azkarra

Errepika :
Zertako beha ürgüllia
Bai eta anbizionia
Bethi geroztik egin dire
mudian nahas Keria
Irustarzünik ederrena
Erresumetan bakia

Andalusia lür maradikatia
Ene erhokerian hik naik inganatü
Maite bati Khausitü beharrian
Nabarrak holaz untsa dü sofritü

Jinko Jauna zük pharka izadazüt
Orain artino egin bühürrukeriak
Mundian ihes phausian ütz nezazü
Orai gozatzera Eskual-Herri.