---
id: ab-3667
izenburua: Oihanian
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003667.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003667.MID
youtube: null
---

Oihanian zuiñen eijer
Txorriñobat Khantari
Eskualdün arbolari
Zazpi probintzier

Amodio di gazter
Eskuarari bethi exker
Jarraiki aiten ürhazter
Ez zekülan bazter
Ez deia Xarmaggari
Nuri ez dü plaser
Txoribat Khantari ari
Eskualdün aurhider

O Txoria Hanitxetan
Zurekilan batian
Boztarioz Khantatzia
Nian desiretan

Sarthü gira plaseretan
Egün plaza eder huntan
Nahiz boztario ezari
Eskual bihotzetan
Hots anaie maitiak
Bandera eskietan
Eskualdünak beikira
Nagusi güzieta.