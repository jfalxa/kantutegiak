---
id: ab-3735
izenburua: Barkoxe Haniari
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003735.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003735.MID
youtube: null
---

Negü gogorrak itzaman zeikü eztixiago bedatsia
Bena despitez begiratzen dü orhiko ber beltzuria
Bortilat bürûz juran artzaiñer lehentü deie erria
Nulaz Barkoxe zük dereiküzü bardin eder batzarria

Eskual- herriko herrietarik orotako eijerrena
Mendi horien artin gorderik nula perlatrik finena
Zure begirazale fidel da hantxe zure Marie-Maidalena
Gaixua othoitzez Barkoxtarentzat ez dezazien abandona

Arballa eijer bat, haren erditan eginik bide berria
Berantü gabe ükhen dütügü bidajanten favoriak
Herri handi heta bezala batügü inportentik merkhatiak
Tratülariek preferatürik Barkoxeko Kabalia

Mereximentüz laidatüren lehen lehenik Jaun Mera
Haiñ beita afable eman lioke bere suñeko athorra
Eskualdün xedetan dizü ezarri Barkoxe hau zuiñen gora
Ez dü lotsarik dagün bozketan juran dadin zankhaz-gora

Exker berhezak nahi nütüke ziren Jaun Erretorari
Xiberotarra den bezala etxeki da Kartielari
Pare gabeko beztimenta bat eman deio elizari
Katedraletan ez da izaten ofizio ederragorik

Azken exkerrak güzientako herritar ta estranjerak
Best' aldetik saldo handi bat jinik dira egün Barkoxerat
Plaza eder hunek eskentzen deikü pilota partida ederra
Üzten zütiet ziren gustiala izar horien laidatzer.