---
id: ab-3705
izenburua: Mauleko Zaharren Khantoria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003705.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003705.MID
youtube: null
---

Agur Jaun anderiak
On Vous dit de bon coeur
Ziren ganat jin gira
Deix braves rémouleurs
Herri huntan sartzian
Nous avions un peu peur
Que le lion de Soule
Fasse quelque malheur

Maoule herri eijerra
On nous l' avait bien dit
Jentiak alagera
Nous étions avertis
Sarthu gira jatera
Chez althabegoity
Tripot eta lükhainka
De fort bon appétit

Tripot eta lukhainka
Le tout bien arrosé
Bazkarian ürhentzko
Kafe pousse - kafe
Sabel onduan bibil
Nous étions satisfaits
Ahal bezain goratik
On s' est mis á chanter

Des gens du troisiéme áge
Maulen hanitx bada
Hardis pleins de courage
Zuiñe alegera
Ils ont pris le virage
Ehün urthetarat
Tellement ils sont sages
Gure mauletarrak

Buriak xuri bena
Le regard malicieux
Kükürüküz ari dira
Nos braves petits vieux
Andere xahar horiek
Ce soir ouvrez les yeux
Senharrak harro dira
Ils seront ambitieux

Gaixo xahar maitiak
Nous allons vous quitter
Juran beharrik gira
Vers d' autressociétés
Untsa suaña zitaie
Vivez bons retraités
Suchetatzen deiziegü
A tous bonne sant.