---
id: ab-3736
izenburua: Laida Pilotaria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003736.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003736.MID
youtube: null
---

Zilhar finaren pare da pilotaria
Korpitza sano eta zohardi begia
Txerkatzen dü leialki plazan jokatzera
Bere Eskual Herria gora laidatzia

Errepika
Zelüko jinko jauna
Gützaz orhit zite
Zü jar zite jabe
Gazte horiek ondotik
Har dezaien pharte
Zer lizateke Eskual Herria
Pilotari gabe

Eijer pilotaria agertzen delarik
Aingürü baten gisa xuriz beztitürik
Gogua arin eta Korajez betherik
Harek ez dü ordian nihuren beldürrik

Perkaiñen denboratik bai eta orai da
Plazetan zunbat ürhats eginik izan da
Zünbat txapeldün jelki, geben hanitx bada,
Nihuntik ez leiteke, Pilota galtzer.