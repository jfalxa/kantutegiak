---
id: ab-3717
izenburua: Lagünt Eta Maitha
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003717.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003717.MID
youtube: null
---

Biarnon Eskualdünak bizi ginen
Beste mündü hanitx bezala
Bekhan elgar oljisoz bai eta
Ez elgarganatzen seküla
Amak txoriño bat igorri dü

Bi hegalak zabal zabala
Erregen jauregin phausatü da
Beha zazie xaramela

Errepika
Jeiki jeik' Pau-eko Eskualdünak
Zohardik ederrena beita
Batarzün berri bat badüzie
Ziren lagüntzaren goraita
Oro hari arrima zitaie
Haren amodio perfeita
Zirentako baita
Eskualdünek haita
Bethikoz : "Lagünt eta maitha.

Dei sühar xarmant hori entzünik
Salduan gira bibilkatü
Eta elgar ezatügüz geroz
Aldika gük errepikatü
Gidari unesta badütügü
Denak lanari gogoz lothü
Alagrantzian bizi girade
Dantzarik Khantarik batügü

Sor lekhia Kasik ageri da
Hortxek dügü bi ürhatsetan
Hura orhitürik hemen gira
Eskualdün aurhidik herrokan
"Ama" abandonatürik beita
Bere orhidüra zaharretan
Gük süstengatüren dügü bethi
Gure egin ahal orota.