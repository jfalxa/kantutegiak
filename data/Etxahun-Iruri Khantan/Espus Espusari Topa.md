---
id: ab-3685
izenburua: Espus Espusari Topa
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003685.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003685.MID
youtube: null
---

Auiñen eijer den bedatsian lehen lilia
Baratze xokhuan irus ageri
Zuiñ eijer den mahaiñ saintiala bildürik
Bi espus eskik emanik elgarri
Egünko egünak hortara ekharri gütü
Uhure espus eta espusari
Ziren biziko desiratzen dereiziegü
Bier Xantza hun eta osagarri

Espus maitiak orai jakin behar düzie
Ziren hunetan zer datin gerua
Batian plaser bestian aldiz arrünküra
Uste gabeko nahi gabekuak
Bena orai danik biga baziratekie
Aisa egarteko ziren eskerniuak
Bai eta phekü guzien kontsolagarri
Egün jüntatü zutien amodiua

Mündü huntan den bekhaxderiarik handiena
Bethi danik da aberatstarzüna
Ene ustez aldiz huntarzün baliusen da
Osagarri unin gaztetarzüna
Bizitze berri bati bürüz bagutzalarik
Hartürik espusetako erhaztüna
Amodio xühar batek inganatürik
Hori da egiazko fortüna

Zelü gañian ekhirik ederrena beita
Arratsaldiari bürüz itzaltzen
Espus maitiak nik hortan dit zirek bezala
Aita eta ama gaizo horier phentsatzen
Arratsaldi ori bürüz juranik beitira
Ikhas ezazie horen eztitzen
Bakian horiek lagüntüren beitzütie
Ziren haur maiten untsa eraikitze.