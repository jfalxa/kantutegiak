---
id: ab-3740
izenburua: Hauztar Baten Ehün Ürthik
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Bazko altian eijer dirade bazterrak libiz beztitzen
Hauze gañeko baguak aldiz arra ostoz arropatzen
Karrikarteko borthalazian gizon xahar bat ari zen
"Plaser düt aurthen orano beitüt Kükia Khantan entzüten.

Ehün urthiak egün eginik zü gaixo Kadet maitia
Erretzü mendin entzün zuñían lehentze Khantan Kükia
Ordütik hunat berritü düzü hanitxetan bedatsia
Zü bethi lerden osagarritan Hauzen eginik habla
Zunbat gazteri Khuntatü düzü zure izatia
Tunisin Suerte izigarri batez lagün hanitx galdü zünin
Geroztik heben guzien Khide Jinko Jaunak nahi beitzin
Ororen gida eta etsenplü gure artin egon zintin

Zure deseiña bethi izan da bizitzia akortian
Herrian eta hala nula artzaiñ zineno bortian
Jeloskeria franko ikhusirik olha aizuen artian
Txülüia aide batekin oro amodeatzen zuntian

Uhure zuri xahar eijerra gizun hun eta leiala
Dohaiñ ederren baliatzera jin zinen Hauze herrila
Untsaz besterik eztü zureki nihurk ikhasi seküia
Gure artian Zelüko Aitak lüzaz begira zitzala

Boztarioz ikhusten tüzü zure herritar güziak
Alegrantzian zure onduan laidatzen ehün urthiak
Zuretzat galthoz Jinko Jaunari lüza dezazün bizia
Birjina Amaren lagüntzareki ardiesteko zelia

Mündü huntarik bestialako deia düzünin entzünen
Juranen zira bihotza laxü beldür gabe arranküren
Zure bizia izan delakoz bethi ezin-ago xüxen
Jesüs beitzaizü angürieki bidila hartzera jine.