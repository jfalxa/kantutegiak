---
id: ab-3693
izenburua: Xiberoko Neskatila Eijerrak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003693.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003693.MID
youtube: null
---

New -yorkerik jin zaizkü bizpa-lau mezajer
Gure Eskualherrilat ikhustera bazter
Jakitera bazenez zunbait nexka eijer
Kapable egiteko aurthen "Miss Univers.

Ikhertürik Laphurdi ta Baxe-Nabarra
Ez zaie agradatü hankorik nihure
Azkenekotz Xiberun bat hitzartü
Artzaintsa gaztetto bat izarrarern pare

Galdegin ükhen deiet ahal bezaiñ bertan
Zer gatik hartü diren hore etxauetan
Bazakitela franko segür Kharrikesetarik
Emazteki propia Kasik etx'orotan

Gustatü baginande, Kharrikesetarik
Ez günükin segürki hunat jin beharrik
Zeren ta gima harro eta ezpaiñ gorri
Ameriketan badügü, phalatakaz hori

Zaragolla lüzekin dütük fanfaruxka
Zer nahi thenoretan Kanpuan Kuhinxka
Gibela lüxatürik ürhatsa xehexka
A eta ez beldürrez "arrautziak Küxka.

Eijer bai bena prenda debriaz egina
Nuiz nahi dükezü ezkuntzeko adina
Ez dezagüla egin hola moxko fina
Kartielian beitügü aski mutxurdin.