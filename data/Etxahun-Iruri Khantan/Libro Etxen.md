---
id: ab-3670
izenburua: Libro Etxen
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003670.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003670.MID
youtube: null
---

Abrekeria zer trxtezia horen gatik
Behar izan düt nik laster egin sor lekhütik
Adin florian ogenik gabe inozentik
Maite beitzüntüdan ô Ama Eskuadi oron gañetik (berria)

Errepika :
Entzün nezazü "Ama" zure deitzen
Entzün nezazü zure nigarstatzen
Eskualdün lürretik naie ohiltzen
noiz othe niz sarthüren ni libro etxen (berriz)?

Bai maradikatzen haigü Hitler Krimiñela
Lehen lehen kausa behiz izan gitin hola
Errekaz ixurrazi dereikük gure odola
Hire airekuekin nolaxek desegin gure arbola (berriz)

Ez adioreik ô Donostia ta Gernika
Nausi zaharra hasirik beita intzirrika
Ez dakigüla Eskualdün horier haiñ mendeka
Uste beno lehen ez zakion ützül Erreplüblika (berriz.