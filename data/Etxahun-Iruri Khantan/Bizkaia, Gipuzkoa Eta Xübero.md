---
id: ab-3666
izenburua: Bizkaia, Gipuzkoa Eta Xübero
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003666.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003666.MID
youtube: null
---

Goxo da bizitzia Xibero gañian
Mendi horien artian trankilitatian
Hargatik aithortzen dit mügaz bestaldian
Badirela xokho hunak, itxas bazterrian
Khantatzera banua huk phentsamentian
Errepika:
Ondarabia ta Donostia ere bai Bilbao
Eskual Herrian ederrez zidie haiñ pare gabeko
Ene bihotza hartü düzie osorik bethiko
Ziek egin batzarri ederra ez dit ahazteko

Xokhota batetanda fierrik Ondarabi
Hurez gerrikatürik erriz Hendaiari
Bere gaztelü zahar misteriosareki
Lehenago güdüetan nula kolpatürik
Santa Maria eliza huntarzünez fundi.

Monte Igualdetik zit agur Donostia
Denen artian zira arrosa lilia
Kontxaren üngürian bibil da herria
Bere mila Kolorekin espantagarria
Ta herrotik Eskualdün da hanko jentia.

Herri handienetarik bat dügü Bilbao
Itxas port famatia da Atlantikako
Indüstriatik bizi da popülia oro
Denak zühür ta agüdo eta bihotz bero
Muxi bat begiratüz bethi guretako

Kunserba zitzaiela zirüko Jinkuak
Bizkai ta Gipuzkoako probintzia goxuak
Ingana zitzaiela luzaz itxasuak
Guretako begiratüz bethi ver amodiua
Zirekilan beikira oso Xiberu.