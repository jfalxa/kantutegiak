---
id: ab-3674
izenburua: Andalusia
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Ibarrak mendiak oihan eta zelaiak
Oro plaseretan dirade
Nabarroko errege delakoz heltzen
Zaharrak gaztiak aberats ta praubiak
Boztarioz ari dirade
Zantxian Karroxan liliz beztitzen
Grenada oihukaz jentez tethatzen
Errege Zantxo-ren errezebitzen

Errepika :
Agur Andalusi Agur Nabarrari
Uhure güziak Zantxo azkarrari
Zeren jinik beita gure lürretarat
Printzeza Zantxiaren espusatzera (bi aldiz)

Korajez afablez Kabalier ederrez
Ez da oranokoz izan
Zantxoren parerik nihure mündin
Leial da phestü da, atrebitü sentho da
Eta etsariari bürüz
Güdükari da lehuen bardin
Afrikan Morruak abisa ditin
Zeren Zantxo beitükegü gureki.