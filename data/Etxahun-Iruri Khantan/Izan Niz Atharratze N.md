---
id: ab-3725
izenburua: Izan Niz Atharratze N
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003725.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003725.MID
youtube: null
---

Jeiki jeiki etxekuak argia da ingoitik
Ekhia gora ari da Maidalena ondotik
Atzo tun tun egün tun tun arren harruan bethi
Besteak dira herrian ez da estonagarri
Güziak salduan gitian liberti

Errepika
Izan niz Atharratzen
Bero baten thiratzen
Ez harrit ni bezala
Besterik hanitx bazen
Herriko bestak ziren
Mündü handi bat bazen
Zahar gazte zer trenpian
Nulaxek libertitzen
Nihun ez da gozatzen
Nula eta Atharratzen

Bortietako artzaiñak
Arthaldiak utzirik
Pettarrian sarthü dira
Irrintzinez beherik

Etzi goizan ützültzeko
Nulaxek pürgatürik
Irus ez badütie
Ardiak antzütürik
Hargatik artzaiñak
Untsa Kargatüri.