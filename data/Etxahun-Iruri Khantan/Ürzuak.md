---
id: ab-3741
izenburua: Ürzuak
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Üda zirzill baten ondotik da larrazkena eder heltü
Artho alhorrak hollitü dirá eta mendiak gorritü
Xibero gaña eta pettarra egün harruan dütügü
Eskual-HerriaIat lehen ürzuat egün agertü betzaizkü

Bethiz geroztik urthe güziez ürzuak sasoin berrian
Phausatzen dira gure lürretan beren bidaje luzian
Zer boztariua, Ihiziariak, beren armak bizkarrian
Harien goraita juranik dira, eiderra ahun, argitzian

Ez usté ükhen solaza déla ürzo horien ihizteka
Zunbat hasperen, zunbat espanto ta zunbat Jinko
Karraska
Hüts egin dianak ez dü ogenik Kartuxak zütin malphuxka
Konsolatzeko pipa bat egin ta xahakua marrüxka

Ihiziariak egün Ligin niz Kabana haitü batetan
Eskapilako mahaiñ batian ürzo erre bat furxetan
"Zamaltzaiñ"-etik bi drago edanik orai hasten niz
Koblakan
Zier aholküz dezazien ürzo ausarki müseta.