---
id: ab-3709
izenburua: Etxahun Zenari
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003709.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003709.MID
youtube: null
---

Gure zazpi probintzietako
Eskual herrik' ederrena
Bihotzak dio zira Xibero
Arauz beitzütüt maitena
Zure mendi ibar ederrak
Zure Mari-maidalena
Eta horien ororen gaiñetik
Barkoxen Etxahun zena

Uhure arren Barkoxerat
Egün jin den mündiari
Jinik delakoz orhitzapenez
Gure koblari zenari
Haren pareko orano nihur
Nihune ez da ageri
Esker mila erranen deiziegü
Eta lüzaz osagarr.