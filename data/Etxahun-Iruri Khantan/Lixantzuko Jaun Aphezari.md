---
id: ab-3710
izenburua: Lixantzuko Jaun Aphezari
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003710.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003710.MID
youtube: null
---

Badü hogei ta hamalaur urthe Jaun Erretora jin zinen
Erretorgia hunen zerbütxari zure destinia beitzen
Zure botz sano argitiari beha hanitx laket ginen
Posible deia gure artetik orai zirela phartitzen

Zure deseiña bethi izan da Parropianten artian
Bake hun baten ahokatzia bizi gintin akortian
Zure argia ere izan da xede hun bat gurekilan
Batheiüko lehen ürhatsetan eta hiltzeko orenian

Bena denborak ez dü pharkatzen adinarek phekü franko
Güzier ekharten beitereikü aisa dügü sinhetsiko
Düdarik gabe juraiten zira ixtant batez phausatzeko
Ez adiorik berriz artino ez zütügü ahatzek.