---
id: ab-3700
izenburua: Lo Hadi Aingüria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003700.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003700.MID
youtube: null
---

Lo hadi aingüria amaren altzuan
Hire erri eijerra botz ezpaiñ xokhuan
Axolbian gütük bainan ni trixtüra goguan
Hau gaitza dük kanpuan ta aita itxasuan

Beha zak aize beltza mehatxükaz ari
Zer gai haldi lüzia, zuñen etsigarri
Harritürik ikhara niz lothü Khürütxiari
Othoitzez jinkuari egin dezan argi

Lo hadi lo maitia ez egin nigarrik
Zeren nihaur aski nük heben dolügarri
Egik zelüko aingürier erri batez batzarri
Phentsa dezen aitari jin dakigün sarr.