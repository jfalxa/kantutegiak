---
id: ab-3672
izenburua: Bi Ama Baditit
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003672.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003672.MID
youtube: null
---

Hanitxen gisa baditit
Bi "Ama" amiragarri
Enia eta Euskadi
Biak pare gabeko dira
Afekzionez betherik
Biak hanitx maite ditit
Eta harien besuetarik
Gogo hunez Khantatzen dit

Ô "Amatto" etxen dena
Zü zira lehen lehena
Nitzaz orhitü zirena
Zunbat aldiz Kausatü deizüt
Hanitx turment hanitx phena
Beitzen zure bihotz mina
Har nezan bide xuxena
Hartakoz zütüt maitena

"Ama" Euskadi-entako
Zer nüke suetatzeko
Oso irus ikhusteko
Gaztelietan dutien haurrak
Aurthen libra ditin oro
Zer alegrantzia gero
Haren eta guretako
Pau-eko Esualdünentak.