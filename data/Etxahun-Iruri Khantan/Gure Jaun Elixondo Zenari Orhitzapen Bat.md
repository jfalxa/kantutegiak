---
id: ab-3712
izenburua: Gure Jaun Elixondo Zenari Orhitzapen Bat
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Astearte arratsaldian orano oro osagarri
"Gai hun eta bihar artino" erran zeneikün orori
Astizken goiza jeikizen uduri oro batzarri
Txoriak Khantan ari ziren bena zü ez iratzarri

Heriotze traidore Krüdela iragan zen arte hortan
Zützaz jelos zen zinelakoz hanitx gora uhuretan
Orai familia hortxek da zuri dolüz nigarretan
Eta haiñbeste adixkide harriturik doloretan

Urthatsez zük egin suhetak ez dütügü ahatzeko
Esperantxaz betherik ziren denentzat urthe guziko
Posible deia ! Zü haiñ afable ! Zü haiñ hardit ! Zü haiñ sentho
Gütarik phartitü zirela agurrak egin ordüko

Zure bizi xüxena izan da etsenplü bat denentako
Leiak zinen, bihotzez largo eta atrebitü laneko
Azken bi gerletan eginik balentia izigarriko
Kolonel gradian züntügün eta modest ezinago

Batarzün berri bat sor ledin behti aintzinin züntügün hun
Ezpiritüz haiñ zinen brillant orok ezagüt dezagün
Zunbat adixkide zünian ikhus zinioke egün
Zurekin lagün bat juran da eta nolako Eskualdün

Ah! Ehortzeta dolorusa bena izigarrikua
Haiñbeste pheredikü eder - zuretzat azken adiuak
Haiñbeste banderan artian gure Eskual-Herrikua
Dolian dira zazpi pribintziak berheiziki Xiberua

Haiñbeste lan egin onduan orai badüzü phausia
Zure orhitzapen goxua denek dezagün begira
Satisfet juriten ahal zira eginik obra handia
Oraikuan zure aldi zen nuiz dateke guria ?

Zü galdürik gure "Biltzarra" Kolpatürik da bürütik
Krüdel zaikü bena zer egin: "aitzina" guatzan hargatik
"Aintzina" zük erran bezala zük erakutsi bidetik
Eta nihur duda ez dadin lagünt gitzatzü zelüti.