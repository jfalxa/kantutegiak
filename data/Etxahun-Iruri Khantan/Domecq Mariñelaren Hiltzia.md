---
id: ab-3701
izenburua: Domecq Mariñelaren Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003701.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003701.MID
youtube: null
---

Ez da mariñeletan ez ja Donibandarrik
Amodiorik ez dianik itxaso handiari
Txipitik maithatüz hori ez da estonagtarri
Nahiz hen zorthia ez den bethi inberagarri
Aitak erakutsitrik bidia semiari
Bizi dira Eskualdün hun, fidel itxasuari

Jinkuak egin bagüntü algarren maithatzeko
Ez zatin Krimiñelik jente hunen arteko
Eziz eta mariñel bat planta hortan hiltzeko
Alhargüntsa gazte baten nigarretan uzteko
Bi urthetako haur bat goizik malürratzeko
Aita eta anaie bat Krimuan probatzeko

Itxas portin hartürik arrantzüko untzia
Gaiaz phartitü ziren hiru aita semiak
Zioielarik gaur date arrantzüko egüna
Fortünatzeko beitügü güne izigarria
Xantzak lagüntzen badü gure projet hartia
Zuiñen eder datin darri etxenko batzarria

Itxasua anple zen ez zen batere aizeriak
Handaiako leihora hürünian ageri
Guratzan hat ez beitate beste arrantzalerik
Untzian barnen hiruak dritxokaz eta erriz
Itxasuak ederrez ez zialakoz parerik
Nihur ez zian gai hartan esperantxa trixterik

Hel eta hasi ziren beren ofiziuari
Sariak hedatürik arraiñ bühürriari
Gorderik hiru mariñel beste Aldekuetarik
Tirokan hasten dirade xüxenka untziari
Seme batek eskü bat bihotzian ezari
Oihü batez: "Hunkirik niz" diolarik aitari

Tiro batek eginik baten herriotzia
Donibaterat bürüz ützültzen da untzia
Haiñ alagera juranik planta hortan jitia
Phentsa zer izan behar zin etxeko batzarria
Hogei eta bi urthetan holaxek finitzia
Ez deia hori oithian phena izigarria

Ama baten besuan haur txipi bat nigarrez
Bere aita zenaren besarkatü beharrez
O inozent malerusa badakikek hatsarrez
Bethi izan behar dela lotsa itxasuarez
Handitü hizatinin ohart hadi aitarez
Eta ez itxasua har irus izan beharrez

Hürrüntik ezagün da orruaz itxasua
Phausa hadi bakian o mariñel gaixua
Bere traidore obretan ez dik lehen Krimua
Behar dik nunbait egotxi bere errai gaxtua
Hitarik nahi zian azken sadrifiziua
Krimiñelen eskietarik inozent hiltzeku.