---
id: ab-3664
izenburua: Ama
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003664.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003664.MID
youtube: null
---

Ai ! Zer plaserra, bost urthen bürian sor lekhila ützültzia
Ene herria etsitü nian, zure berriz ikhustia
Aski sofritüz nik hanitxetan, desir nükian hiltzia
Ez bazüntüdan haiñ maite ükehn, o Ama Euskal-Herria

Hanitxen gisa phartitü nintzan, etsariak güdükatzera
Gogua bero, bihotza laxu, eta kasik alaagera
Ez bainakian, orano ordian, zer zen amaren beharra
Hari phentsa eta biga bostetan, aiher zitazün nigarra.

Erresiñola khantari eijer libertatian denian
Bena trixtüran higatzez dua kaloia baten barnian
Gü ere ama hala guntüzün, tirano haien artian
Zure ametsa kontsolagarri, trixtüra handirenian

Orhiko txoria da den bezala fidel Orhi bortiari
Üskaldün guziak gütüzü, ama zure zerbütxari
Alageraki lehen bezala, egin izagüzü erri
Ikhusten gütüzü othoitzetan zure arbola saintiari.

Sorthü berriak, bere khuñatik, lehen elhia du "ama"
Bere ilüsione ederrian, ez dezazüla abandona
Zure amodioaz egar itzozü bizitze huntako phenak
Begira dezan azken hatsetan, azken hitza zure "Ama".