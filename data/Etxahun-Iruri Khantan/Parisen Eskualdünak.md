---
id: ab-3719
izenburua: Parisen Eskualdünak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003719.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003719.MID
youtube: null
---

Pariseko Eskualdünak
Oro ditit ezagünak
Jente modest ezinago
Xüxen eta bihotz hunak
Izana gatik sos dünak
Ez zaitze berotü hünak
Oro dira fededünak
Ta herrotik Eskualdünak

Errepika
Osagarri hun eta ere mila esker
Pariseko Eskualdüner

Neskenegün arratsetan
Gaztiak dabiltza dantzan
A ! Zer solazak, zer errik
Orai sala berri hortan
Aldiz igante arratsetan
Zaharraguak herrokan
Eskual-etxean Kartakan
Dritxokan eta ere Khantan

Ama Eskual-Herriaren
Berset hori düt berhezten
ikhus dezala Parisen
Gük ez dügüla ahazten
Zunbat nahi untsa giren
Ardüra gira ohartzen
Xokho eijer bat diala
Guretako begiratze.