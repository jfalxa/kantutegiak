---
id: ab-3744
izenburua: Urthe Berrl Hun Alger-Eko Esküaldüner
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Nathabitatek lekhü eginik heltü zaikü urthe berri
Bortiak xuriz bezti dira zilhar fina bezaiñ argi
Aize hegua imur goxo bat mañü ephel bat udüri
Afrikatik jin zaikü agurrez Ama Eskual-Herriari

Ama nolaxek ez da gozatzen Karesü hun horri phara
Lo-etarik erriz baduago plaseran-handiz ikhara
Amets goxo batetan dua-Afrika bero hortarat
Karesü horrekin orhitürik han dütian bere haurrak

Gaixo amatto usté züniana hariek züntiela ahazten
Aize goxo hortan dereizie hasperettobat igorten
Zük lehen erakutsi ürhatsak bethi dütie segitzen
Beitakie ez déla Eskualdün Ama dianak ükhatzen

ALGER-eko portü ederrian nurk phentsü zükin badela
Eskualdün batarzün berri bat jüntatü nahiz fidelak
Balekie Khantatzen déla han "Gernikako arbola"
Belhariko jar liztaketzü Donianeko mariñelak

Oro batzarri, oro erri da gure haur sorthü berria
Ama ! zure izeneko da oi gure "Eskual-Herria"
Jünta dezagün handit dakigün, nik badüt ustekeria
Denbora lüze gabe datila potro bat izigarria

Ikhusten zütüt ALGER maitia gure haurraren lo-eratzen
Zure itxaso blü ederrian egün oroz mirallstatzen
Zure ekhi beruaren pian othoitze deizüt egiten
Lekhü eijer bat berhez dezagün gur'Eskualdun banderaren

VIl. Ene aurhidiak üzten zütiet eskik emanik elgarri
Alagrantzian egükitzera berriz daigün "Urthe berri"
Üsü üsia hasperenttobat orhitzapenez Amari
Ene suheta : "Berriz artino xantza hun ta osagarri".