---
id: ab-3691
izenburua: Bedatsiaren Heltzian
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003691.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003691.MID
youtube: null
---

Bedatsiaren heltzian bazterrak dirade
bethatzen lorrez
Bortientan ere olha saria nola
beztitzen belharrez
Artzaiñak gü goiz'aldi eder batez
Gora bürüz baguatza plaserrez
Gure ardi salduk aitzin otsamanez
Üdakoz dener adio errane.