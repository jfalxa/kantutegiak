---
id: ab-3707
izenburua: Marxe Eyharxet Zenari
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003707.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003707.MID
youtube: null
---

Bizia llabür hiltzia segür hortzaz ez gira düdatzen
Denek beitakigü heriuak ziur ez diala pharkahtzen
Urthe bat badük Marxe hintzala gure artetik itzaltzen
Denbora baduak hargatik heben ez haigü ahazten

Errepika:
Beha gitzagük Zelietarik Khantan O gaixo Marx.zena
Fidelki etxekitzen diagü gük hire orhitzapena
Gük hire orhitzapena

Behar izan dik zorthe trixte bat gaitz izigarri batekin
Hire gütarik beherzteko hire adinik hobenin
Haiñ hintzan irus et.alagera hire familiarekin
Hire Barkoxen, hire Xiberun, haiñ beste lagünen artin

Hire gogua gazte gaztetik izan dük pare gabeko
Gure eüskualdün jei zaharren alhal oroz altxatzeko
Pastualkari Khantari huna izar bat hintzan dantzako
Eta Kunpaña agradablia mündü ororen arteko

Arbolarik den baliusenak denbora bat baizik ez dü
Hari arimü lanthare gazte lantha izateko frütü
Ez düt düdarik Barkoxtarrek hitan hartürik etsenplü
Aitzina hik hasi sall hori nahi dükien segitü

Adios arren Marx.üzten haigü hizan bake ederrian
Jauank haitatü din lekhian arauz zellüko glorian
Hantik othoitze egin dizagük bethikoz izan gitian
Eskualdün xühar hintzan bezala haiñ maite hin sor
lekhia.