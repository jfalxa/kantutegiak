---
id: ab-3728
izenburua: Orhito-Txoria Kahantan
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003728.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003728.MID
youtube: null
---

Eretzü gibeletik jelki da ekhia
Erroimendiri eskenez lehen leñhuria
Adio mila gogotik gaiko beltzuria
Khantan hasi beitzaikü Orhiko Txoria

Txori xarmagarria argi azkorrian
Nulaz Khantan hasi hiz holako trenpian
Ez düt hik ükhen behar phen bihotzian
Khantezak Khanta gora goza ahal gitian

Khantatzen diat goratik xibero alderat
Larraiñtarrak beikira bethi alagera
Pharka ez badütügü aski botz ederrak
Ez fida ez botzari bihotzari fid.