---
id: ab-3743
izenburua: Urthe Berri Hun
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Aize hegua itzülikaz da iraxten bortün behera
Harro gaitz harek nolaxek ez dü xehatü itxas bazterra
Abentiarek nunbait behar dü egotxi bere imur tzarra
Señalatzeko akabatzera duela urthe zaharra

Dolürik gabe, adios arren denek urthe zaharrari
Esperantxa güziak ezariz heltzen zaikün berriari
Ekhar dizagun bakiarekin Koraje phenan denari
Xantza hun bat gure familietan ta osagarri

Ümure huna begira dezan bethi bezala Xiberun
Liberti gitin alagerakin bortu eder horien ondun
Tradizione zahar horik edüki ditzagün gogun
Oro akortin bizi gitian güzier urthe berri hu.