---
id: ab-3713
izenburua: Parisen Eskualdünak Fede Hun
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Parisen Eskualdünak fede hun bateki
Usantxa zahar hoier girade etxeki
Egün jokhatzen dügü trajedia hori
Ama Eskuak-Herriaz untsa orhitürik
Heben bakhotxak badaki
Bizi behar da goxoki
Jüntatüz oro algarreki

Errepika
Algarren ikhustera
Hobeki maithaitzera
Ixtant goxo baten gozatzia gatik
Eta süstengatzera gora gure bandera
Egon da txütik orai eta bethi

Gure "Eskual - Etxia" gure axolbia
Ikharagarri zaikü aithor düt egia
Herratürik ginateke euskaldün guziak
Ez bagünü aartetan zure batzarria
Pariserat jin gaztiak
Beha zure Khümitia
Txerka zure arrimi.