---
id: ab-3706
izenburua: Soldado Denboran
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003706.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003706.MID
youtube: null
---

Zunbait bertsü berrien hasten niz Khantatzen
Düdarik gabe ez beita geürrik esanen
Amorekatik eta denek jakin dezen
Soldaduek nulaz dügün denbora baliatzen

Kasernako bizia zer izate ederra
Lan gütien egiteko hanixko afera
Manhatzeko badügü gradatüz leherra
Beldürrez ez dezagün egin lan sobera

Goiz huntan nintzalarik ohian phausüki
Amets eijer batetan Eskual - Herriari
Ordre bat ükhen dizüt behar niala jeiki
Kartiereko Korbealat dejaik berantegi

Bethi berrak ginenez hasi murmuzikan
Ez date sarriegi Klasa jin denian
Brigadierrak diote ez othoi sar phenan
Denak Eskualdün gira egünko ekipan

Hitz horik entzünik gira süsmetitzen
Eta batx batxa hasten tresnen bibilkatzen
Bi erhatz zankho mutzak besterik ez beitzen
Manera hunez dügü lan baliusa abiatzen

Karriiola bat badügü laur errotetako
Bekhatüxe bezala ofizio hortako
Bi zaldi ez beitira ez amabalatüko
Hen adina so eginik beita pharkatzeko

Itzüli bat eginez Karriola Kargatü
Jaunak beharko dügü beigen artian hütsü
Dürrüti nahi bahiz lanaz esküsatü
Jin ordüko behar dük pinta gomendatü

Goxoki plantatürik Karriola gañian
Batx batxa sarthü gira Bégles-ko Kharrikan
Nexka gazte saldo bat irrikaz onduan
Arauz ina behar gira planta ederrian

Barnetxe gaizua dago eni Kheñüz ari
Behazak " Xübero" nexka pollit horik
Trüka nezake gaurko sala polizari
Gaur baten pasatzia horietarik bateki

Debrü gizun zirzilla zer leite hitarik
Zehe bat zorro eta gehiago iphurdi
Nahi dena fidatü hire itxurrari
Ez liok sinhets zuiñen hizan andrekari

Lana akaba eta nun dago Dürrüti
Kantinala furanik denbriak hartürik
Adjudant balü atek auher atzamanik
Bost sei egün Kuntsiña batüzke segürik

EtxebestKosinera erri bat hortzetan
Gure gana heltü da bi lapin Kholkuan
Zer debrü ari zizte hor phentsamentükan
Afaltü behar gira oro algarrekilan

Arno pinta bederaz lapina arrosatü
Jaunak beharko dügü besterik phentsatü
Bigik-iga mus-ian eta beharko dü
Bi pinta arno xuri galtzalik phakatü

Zunbat phartida eginez zunbat pinta edan
Denak hasten girade Khantan ea dantzan
Mutxikuk eman dütügü xiberoko mudan
O ! zer phüntü goxuak moxkortü onduan

Trunpeta hasi zaikü apelan sonatzen
Khuntia phaka eta laster dügü egiten
Irritzin eder batez Kartiera anatzen
Gai hun bat dügülarik dener suhetatzen

Jinkuak lagünt beza soldado eskualdüna
Ahal oroz Kuntserbiz gure lan egina
Etxebestek ekharriz üsüxko lapina
Laster heltüren zaikü Klasako egün.