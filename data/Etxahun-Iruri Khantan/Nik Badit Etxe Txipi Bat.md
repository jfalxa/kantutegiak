---
id: ab-3687
izenburua: Nik Badit Etxe Txipi Bat
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003687.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003687.MID
youtube: null
---

Nik badit etxe txipi bat Gaidañeko lurretan
Eniz aberatsas har gatik alagera bizi nizan
Zunbat nahi untsa diren beste herri aizuetan
Düda düt nik izan ditin gü beziñ plaser etan

Errepika
Akortian bizi gira herritarren artian
Ahal oroz llibertitüz xaharrak gaztekilan
Igarailek ikhus gitzela boztarioz oro Khantan
Gaindaiñtarrak Gandaintarsak Gaindañeko Kharrikan

Gaindaiñtarrak jente hunak güzietan famatürik
Arrotzentako badie bethi geroztik batzarri
Xibero orotan ezta haiñ gazteria ederrik
Mothikuak libertisant neskatxak xarmegarri

Gaindaiñtarek emaiten deikieno ausarki mahats ardua
Etxekiren dügü argi bethi bezala gogua
Eta bihotzetik maite bakhotxak bere xokhua
Bethi beraiñ alagera Gaindañeko goxu.