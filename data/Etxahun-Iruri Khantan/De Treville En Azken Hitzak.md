---
id: ab-3677
izenburua: De Treville En Azken Hitzak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003677.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003677.MID
youtube: null
---

Herio latza hüllantzen ari hiz
Dolürik gabe orain nitarat
Othoitzen hait ixtant bat ützi nezak
Ene azken adiuen egitera

Errepika
Hori bera da denen ixtoria
Heriuak bardintzen handi txipiak
Zeren ilüsione bat bera da

Ene jaurregi pare gabekua
Hi altxatüz banian fiertate
Hitan igaran, denbora goxua
Haiñ llabür nükila ez nian uste

O Basabürü amiragarria
Bortüz eta mendiz ürgüratürik
Bostetan begixtatü zütiet
Ürgüllürekin terraza huntatik

Adios arren Marie-Maidalena
Zük ingana nezazü lotarik
Ene ondotik baratzen direnez
Izan ez nadiala ahatzeri.