---
id: ab-3727
izenburua: Mendi Gibeletik
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003727.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003727.MID
youtube: null
---

Mendi gibeletik da agertü ekhia
Bortü gañari eskeniz lehen leñhuria
Adio mila gogotik gaiko beltzuria
Khantan hasiz geroztik Orhiko Txoria

Txori xarmagarria argi azkorrian
Nulaz Khantanhasi hiz holako trenpian
Ez dük hik ükhen behar phena bihotzian
Khantezak Khanta gora satisfa gitian

Khantatzen diat goratik Xibero alderat
Zelietarik manhatürik Ander Santa - Klerak
Oro nahi güntüke bere sor lekhürat
Egünko jei ederraren hor uhuratzera

Txoria trankil ago jarraikiren haigü
Gük ere nahi ditiagü plaserak gozatü
Gure aitek hor baziean heñttobat ürgüllü
Hen orhitzapenetan jarraikiren haigü

Düdarik gabetarik adixkidik triauzte
Saldo eder batetan zahar eta gazte
Orok algarrekilan date plaser hartze
Bihotzaren erditik Biba Atharratz.