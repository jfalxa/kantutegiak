---
id: ab-3726
izenburua: Atharratzen Besta
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Goizik jeiki Atharraztarrak
Besta beitügü egün
Herritarrak Kanpotiarrak
Khide eta ezagün
Güziak saldo berrian
Oro algarren lagün
Egünko jei horik
Untsa goza ditzagün
Trenpütan gireno
Profeita dezagün

Errepika
Izan nüzü Atharratzen
Ixtant bat inganatzen
Ez harrit ni bezala
Besterrik hanitx beitzen
Herriko bestak ziren
Mündü handi bat bazen
Zahar gazte Khantan dantzan
Nulaxek libertitzen
Nihun ez da gozatzen
Nula eta Barkoxen

Hürrünetik jinik bada
Gazteria handia
Somülariek harientzat
Zer alagerantzia
Pilota partidatto bat
Kartieleko lilia
Horien so ez beita
Aisa debeiatzia
Nula ez Barkoxe
Herri famiti.