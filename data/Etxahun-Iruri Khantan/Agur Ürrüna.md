---
id: ab-3716
izenburua: Agur Ürrüna
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003716.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003716.MID
youtube: null
---

Baionatik ta Donibanera egiaz harro dira bazterrak
Arrakasta izigarria badü gure itxaso bazterrak
Donibanerik ta Handaiera bürütürik lehen petigora
Heltzen gira Ürrüñera hori da denen izarra

Errepika
Agur Urrüña maitia
Herri xarmagarria
Enetako da plaser handia
Zure ikhustia,
Pilotariak ta dantzariak,
Laphurdiko liliak

Heltü eta jarri niz goxoki Dongaitzenin Leon xaharraiki
Haren saihetsian bi pilotari, Garmendia ta Zügazti
Herriko plazan txistuareki, bere dantzetan fierrik "Airoski"
Mündü handi bat haieki, eskü zartakan azkarki

Hola "Airoski" emazü bethi, zure salleri bardin azkarki
Uhure egiten düzü Ürrüñeri, Eüskak Herri güziari
Gure bandera balius hori heda ezazü mündia gainti
Xiberotarrak segürki osoki gira zurek.