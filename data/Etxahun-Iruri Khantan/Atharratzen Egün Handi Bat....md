---
id: ab-3698
izenburua: Atharratzen Egün Handi Bat...
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003698.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003698.MID
youtube: null
---

Salütatzen zütiet gure Kunpañia
Hürrüntik jinik diren jaun aitzindariak
Eta Basabürüko Jaun Mera guziak
Botzik phakatzen tügü ziren bazkariak

Bazkari huntto batek bihotza dü phizten
Jente seriusenak bai alageratzen
Lan eder bat eginik denak gira Kuntent
Atharratze jorrel dü nulaxek edertzen

Aski phena izan da ta intziri frando
Egiaz lan horien hola bürützeko
Bena orai eginik irus gira oro
Basabüria die ara baliatüko

Arauz fini beitia hor estaküriak
Laida dezagün denek merkhatü berria
Gormantegi ez izan hor tratüriak
Kasü ez date hürrün oi jandarmeri.