---
id: ab-3742
izenburua: Iruri
kantutegia: Etxahun-Iruri Khantan
partitura: null
midi: null
youtube: null
---

Agradable dirade Larrañe, Ligi eta Sante-Grazi
Aiphatürik dira Atharratze, Alzai, Aloze, Lakharri
Zalgize ere aski plasent da, Ozazek badü batzarri
Bena ororen maithagarriena othe ez deia Iruri

Iruritarrak bethiz geroztik alegerarik duatza
Bethi untsa hartürik ez deia harat jiten den arrtoza
Ükhenik ere zunbait ordütan aski handi ardu hatsa
Hori pharkatü behar ziezü han beita prunki mahats.