---
id: ab-3688
izenburua: Anaia Etxen Da Ezkuntü
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003688.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003688.MID
youtube: null
---

Anaia etxen da ezkuntü
Bortxatü niz ori juraitera
Ene ophilaren egitera
Paris-erat banuazü
Errepika
O ama Eskual-Herri goxua
Zütatik hürrün ni trixterik banua
Asios ene etx.ondua
Adios Xiberua

Paris-eko bizitzia
Lan Khostüz baguatzü
Hargatik berantzen zitadazüt
Zure berriz ikhusia

Bena denbora da llaburxko
Othoi ama entzün nezazü
Xokho bat haita izadazüt
Azken egün horientak.