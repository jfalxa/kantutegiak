---
id: ab-3704
izenburua: Adixkidiak Egün Hun
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003704.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003704.MID
youtube: null
---

Adixkidiak egün hun zer berri dügü Xiberun
Han ez dügü lan eskasik hargatik gira imur hun
Khantatüren dereiziegü gogo hunez bazkal ondun

Xiberutaren Katera bethi geroz alagera
Sorthaphenez Khantari da dantzan frisata goihera
Bena Khexatzen delarik ützi dehar da berber

Xiberutarra agüdo eta phestü ezinago
Ez da indarrez ariko abillegi da hortako
Nausi da sos geñhatzeko gero batx batxa jatko

Ez düt ametsik hiltzeko osagarri hau düdano
Gazna zaharra izanez ogi muflakin jateko
Ardu burrat hun bat gero aisa dügü Khantatük.