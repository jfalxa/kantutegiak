---
id: ab-3669
izenburua: Jeik' Hadi Eüskadi
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003669.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003669.MID
youtube: null
---

Eskual herriko plaza hanitx hütsik dira
Nurat hurtü dira hanko pilotarik
Ostatietan bazter güzik ixil dira
Nurat galdü dira leheneko khantarik
Ez da herrin zahar zunbait baizik
Gaztiak oro azkapirik

Errepika :
Jeik' hadi Eüskadi eta mintza hadi
Ozenki gure gazteriari
behardiela bethi fidelki kunserbi
Gure usantxa zahar horik

Herriko bestak hasirik dira bekhantzen
eta dantza jauzik osoki nülatzen
Gazteria ez da kurri baizik satizfatzen
Kurritzen da bena gaiza güti ikhasten
Arte hortan aldiz zahar horik
Berak dira trixterik etxen

Zuri othoiz gure Mari Midalena
Aholka gitzatzü mendi gaiñ hortarik
Usantxa zahar horik diela ederrenak
Eskualdün khantarik, dantzarik, pilotarik
Dütügün huntarzün baliusena
Ez direia jei eder horik.