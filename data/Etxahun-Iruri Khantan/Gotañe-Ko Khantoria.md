---
id: ab-3715
izenburua: Gotañe-Ko Khantoria
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003715.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003715.MID
youtube: null
---

Nurk erran dü Gotañe zela
Dena Frantximant jarria
Herrotik Eskualdünak gira
Ta maite libertitzia
Iabarne-Kilan egiten dügü
Xokho bat maithagarria
Hanitxek diote ederrez dela
Xibero huntan lilia

Gotañe-ko ibarra gaintik
Izotza goiz da phausatzen
Ebi denin ere funtsetan
Hurra da aisa lokatzen
Ez lotsa izan hor Gotaiñtarrik
Sekülan ez da thatxatzen
Zeibela franko etxen badire
Hurrik ez die txestatzen

Gotañe-Ko ürrüpean dü
Arthuk zügünki phusatzen
Hartakoz da Kabale ederrik
Eta Poalla hanitx hazten
Mauleko jaunek aisa dütie
Tipulak gorri Kharreatzen
Gotañe-Ko ollasko jan eta
Gazterik dire ollartze.