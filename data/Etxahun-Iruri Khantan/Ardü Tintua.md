---
id: ab-3702
izenburua: Ardü Tintua
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003702.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003702.MID
youtube: null
---

Bethi eskualdünak maite arno huna
Ez beita gizona hori hügü diana
Gitin arrasoina : zertako personak
Txerkhatzen dü lana ützik jan - esana

Errepika
Ari ari ari pottoko hik sarri
Beharko dük jarri eneki tafernan
Ari ari ari ez gitük egarri
Behar diat ezari hur ene arduan

Emazte beruak dirade erhuak
Hun zaie pernoa ez arno tintua
Gero mothikuak, gero ondoriuan
Loditzen txorrua, neskatxa ttonttuak

Azken berset hori Noë gaizuari
Harek zuen ekharri aihen balius hori
Horrek emaiten dik indar gaztiari
Ümur zaharrari mihi andrear.