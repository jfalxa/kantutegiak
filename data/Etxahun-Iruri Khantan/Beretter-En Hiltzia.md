---
id: ab-3679
izenburua: Beretter-En Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003679.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003679.MID
youtube: null
---

Llabür da bizitzia
Lüze da phenatzia
Oithin balio dia
Batere sortzia?
Hola ürhentzekotan bizia
Egiaz ez deia trixtezia?

Errepika
Hortan akabatzen da
Gure trajedia
Eta Eüskaldün handi
Baten ixtoria
Ohart etxek zazie
Aurhide maitiak
Beretter-ek egin dutin balentria
Ikharagarriak
Bazter hanitxetan aipha-erazi dü
Eüskual-Herria

Fernando traidoria
Kastillako erregia
Laster hire aldia
Jinen dük segürki
Nabarra tratatü dük abreki
Behin ez hadiala eror gaixki

Guatzan alde barnera
Hil horien ehortzera
Zer etsenplü ederra
Emaiten deikien
Etsai-etan, algar maithatzen!
Halako zunbat othe da heben.