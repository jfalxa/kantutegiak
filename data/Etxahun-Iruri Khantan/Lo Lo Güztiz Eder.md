---
id: ab-3675
izenburua: Lo Lo Güztiz Eder
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003675.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003675.MID
youtube: null
---

Sorthü güziek zorretan dügü goiz edo berant hiltzia
Aberats prauben lege berra da egiazko jüstizia
Hargatik adin florian Maritxu zer trixtezia

Errepika
Lo lo Güztiz eder
Zeren pharadüsian
Lo lo Güztiz eder
Zirateke bakian

Heriotzia gaiza thorpia ta nurta nahi hedatzen
Jaunak berak haitatzen gütü hari nihure gordatzen
Phena zaikü bena egün Maritxu zure aldiz zen

Oihanetako baguak dira primaderaz beztitüko
Jaizgibel-eko üthürriak dü egün oroz Khantatüko
Bena Maritxu ez zaie haboro iratzarrik.