---
id: ab-3686
izenburua: Mutxurdinak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003686.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003686.MID
youtube: null
---

Eskunt geia niz eta etxakin haitatzen
Emazte geia beitüt propi desiratzen
Orai debeiatürik hasten niz etsitzen

Estonatzen nük nulaz izan eskuntzeko
Ata Xiberun badüt mutxurdina franko
Hasperenez beitira beitira haiñ bero
Besterik ezin izanez birjina tük oro

Ez nezaiala gida mutxurdinetarat
Hüllantzeko beitira soberaxe basa
Urgülllütan zütian zireno arrakatsa
Egün den zirzillenaz litakek kuntenta

Alo gizon gaztia ez hadila lotsa
Mutxurdina bateki izanen hiz untsa
Haita ezak errejentsa edo comersanta
Bena hoberena hire dükek laboraisa

Egünko laboraisak oro harro dira
Españak gorri eta xuri aztaparrak
Saltzen dütie zalhe jateko beharrak
Dantelaz garnitzeko suñeko mantharrak

Egünko laboraisa bestitü denian
Zigarreta ahun gizonen artian
Permananta baat harro sartaz garkotxian
Etzaitzaka agrada arropilla hortan

Delikatiegi ziztak beren maneretan
Igantiaren prestatzen asteko egünetan
Gero agertzen dira besta bat denian
Asteko zankha beltzak zetazko galzetan

Ezpalinbadük nahi mutxurdinetarik
Haita ezak gaztetto bat Maulen eraikirik
Badük segürki franko ülhünpian gaintik
Jauziz ebilten beitira monthikuen ondoti.