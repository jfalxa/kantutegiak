---
id: ab-3733
izenburua: Paris-Eko Maskaden Uhuretan
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003733.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003733.MID
youtube: null
---

Paris-en bagira egün
Plaza huntan zumait ehün
Askazi eta ezagün (arra har)
Eskual-Herriko bander' eijerra denek uhura dezagün
Hura dükegüno lagün
Izanen gira fededün
Eta bethikoz Eskualdün

Bortxatin niz aithortzera :
Behar da jin Paris-era
Solaz eder ikhustera
Falta izana gatik bortiak eta txaso bazterra
Ez gitin plañit sobera
Jüntatüz algarretara
Bizi beikir' alagera

Paris-en Eskual - Etxia
Badügü pare gabia
Eskualduñen axolbia
Irus denarek han Kausitzen dü plaser din boztariua
Ta phena dion gaixuak
Merexi Kontsoliua
Biek batzarri goxu.