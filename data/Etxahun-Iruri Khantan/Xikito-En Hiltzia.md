---
id: ab-3737
izenburua: Xikito-En Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003737.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003737.MID
youtube: null
---

O Xikito nun othe hiz
Ehiz haboro agertzen
Azken bi egün horietan
Ez hitzaikü hunat jiten
Itxasua erreüs diagü
Ez haialakoz ikhusten
Eri bahiz sendo hadi
Hi gabe gütük debeiatzen

O Xikito oraikotik
Juran hitzaikü bethikoz
Plazetako erregia
Ez dük haboro gaiztüko
Eskual herrin hitzaz gütük
Mente batez orhitüko
Phausa hadi zelietan
Adios gaizo Xikit.