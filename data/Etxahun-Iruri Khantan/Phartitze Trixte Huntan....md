---
id: ab-3681
izenburua: Phartitze Trixte Huntan...
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003681.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003681.MID
youtube: null
---

Phartitze trixte huntan lagünak adio
Denek bardin etxeki bethi amodio
Ukhenik ere egün hanitx boztario
Zer sogritü ez dügü gük orai artio

Gerlaren lazkeria ezin sinhetsgarri
Atzolo asixkidik egünko etsari
Arrasu gabetarik tirokan algarri
Profeitü thirazale zunbat aitzindari

Nunbait laur urthe badü hunat heldü ginen
Ilüsionez betherik iñorat algarri
Orain bizi ginenen hasten niz Khuntatzen
Aphürraren aphürrak benai ikharasten

O martir sakratiak hortxek ehortzirik
Etxerat juranen gira ziek lo ützirik
Othoitze egizie arren zelietarik
Gure haurrek ez dezen ikhus halakori.