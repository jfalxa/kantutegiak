---
id: ab-3676
izenburua: Agur Agaramuntesak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003676.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003676.MID
youtube: null
---

Errepika
Agur Agaramuntesak
Agur-ere Beaumontesak
Agur Sant-Antoni

Aidia dano da Sant-Antoni gañian
Xapela berriak xuri hegia
Jinik girenaz geroz bake egitera
Dezagün egüntto bat igan alagera

Ditzagün armak phausa bai eta jar gustian
Orok algarrekin jan eta edan
Egün danik aitzina libro girateke
Trankilin jokatzeko nurk gure afereta.