---
id: ab-3668
izenburua: Haur Maitia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003668.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003668.MID
youtube: null
---

Gai ülhünetik landa jin da
Goizeko argi azkorrian
Hiretzat orano argiago
Ene haur maitia
So egizak erriz mendiak
Harruan dira arbola güziak
Liliz bestitürik xuri xuria
So egizak untsa
Gaiza eder horik oro dira
Eskual Herria

Haurra gaztetik ikhas ezak
Gaiza horien maithatzen
Jinkuak eman huntarzünen
Untsa baliatzen
Xedetan hartürik onestatia
Txerka ezak untsa libertitzia
Maite eskualdün dantzak, khantatzia
Pilotan altxatzen
Aiten aitek zelü gañetarik
Deie so egiten.