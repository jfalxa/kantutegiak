---
id: ab-3718
izenburua: Pariseko Eskualdüna
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003718.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003718.MID
youtube: null
---

Eskual-Herri maitian eraikirik txipin
Egon nintzatekian mila plaserekin
Bena arauz benintzan etxen soberakin
Hirilat juraitia deliberatü nin
Parisera heltzin
Sartzian uste nin
Osoki Kaskoiñen artin

Errepika
"Eskualdünian" "Eskualdüna"
Zurekin batian Parisen bizi gira
Eskualdünak oro bibil
Gure Eskual-Herrian bezaiñ alagera
Fidelki etxeki dreiküzü esküara
"Amaren" Khantore, dantza Xaharrak
"Eskualdüna" Eskualdunek

Igande guzietan, Eskualdün guziak
Gure Eskualdünari arrimatzen gira
Ez laidatürik ere Orhiko txoriak
Kontsolatzen gira ziren ez dütügia
Gure Khantariak
Gure dantzariak
Eskual - Herriko liliak

Parisen nahi dena bere bürüz ari
Üdüri dü bildotxa axetan galdürik
Amodio dianak gure arbolari
Arrima dadila gure "Eskualdünari"
Oro algarreki
Jüntatüz geroztik
Nuren dükegü beldürri.