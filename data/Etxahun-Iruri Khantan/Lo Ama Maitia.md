---
id: ab-3665
izenburua: Lo Ama Maitia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003665.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003665.MID
youtube: null
---

Mendi gañetik ekhia zaikü agertü goiz azkorrian
Eta bere bidia jarraikiz gorde itxaso hegian.
Ulhün zeñiak ezagün dira aholküz phausa gitian
Zü era ama Eskual- Herria, arren bihar artinokan.

Errepika : Lo ! Lo ! Ama maitia
Zure aurrak bulharrian
Lo ! Lo ! Ama maitia
Ametsik eijerrenian

Bortu gañia zohardi da-te, lanhageri itxasoa
Aize heguak, ororer deikü, bere karesü goxua
Bazter güziak ixiltü dira, ama othoitzerat nua
Bizi nizaño begira zazü, aitek haitatü xokhua.