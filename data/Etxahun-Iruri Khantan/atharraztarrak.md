---
id: ab-3738
izenburua: Atharraztarrak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003738.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003738.MID
youtube: null
---

A zer alagrantzia Atharratzen egun
Zeren herri huntako hiru gazte lagiin
Pilotan jelkhi dira Frantziako txapeldun
Bihotzaren erditik uhura ditzagun
Zeren bekhan beitira holako plaserak
Kostumaturik ginen erresiñatzera
Oldar gaitzian dira "Atharraztarrak.

Diala berrogei urthe segürki Atharratzen
Pilota jokü ederra haur eskasttobat zen
Nurat nahi juranik guntien zaflatzen
Eskual-herri guzian trixteki berhezten
Bena ez da zeditzen, ez Xiberotarra
Aintzindare zunbaitek phizturik sükharra
Ordüntik sorthürik da : "Atharraztarrak.

Haur ttipitto hori da geroztik handitu
Bide handia gaiñtik trostakaz duazü
Zuiñ orduz balinbada iza thebükatü
Phatarrik gastuenak igaranik dütü
Indarretan dirade gazt' eta zaharrak
Jokülari saldo bat egiazki izarrak
Osagarritan dügü "Atharraztarrak.

Uhure Atharratze herri xarmantari
Zuretzat egina düt azken berset hori
Segi dezagün bethi gazteria eder hori
Aitzina izan dadin hanitx pilotari
Lagünt itzatzu haurrak gogotik plazarrat
Edüki dezen gora lehoinen bandera
Ene phartetik diot : "Biba Atharraztarrak".