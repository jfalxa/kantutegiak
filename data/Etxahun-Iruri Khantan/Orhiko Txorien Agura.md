---
id: ab-3732
izenburua: Orhiko Txorien Agura
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003732.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003732.MID
youtube: null
---

Sen-Jüsefen ermitaik
Goraintzi bat Üskal - Herria
Zure zerbütxari fidel
Girade Orhiko- txoriak

Larraiñtarrak alageraz
Bethie danie gütie famatzen
Lehenagoko erran hori
Gük ere dügü baliatzen

Plaser bagütützie orai
Orhiko -txoriak behatü
Galdegiten dügü dener
Hüts eginen pharkament.