---
id: ab-3684
izenburua: Berterretxen Hiltzia
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003684.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003684.MID
youtube: null
---

O zerüko Jinko Jauna
Zuri hersatzen nüzü
Azken oren izigarri huntan
Argi egin izadazüt

Amodiuaren Krüdela
Zertako naik inganatü
Ustez maitebat baniala eta
Haren gatik hil behar düt

Margarita Margarita
Ez naizia entzüten
Zure etxonduaren aitzinian
Orain naie ehaiten

Ene ametsa bethi züzün
Zurekin espusatzia
Horen phartez nik orai dizüt
Haiñ trixteki hiltzia

Ene Khorpitzaren aitzinian
Argitü datekialrik
Nahi dit belharika zitian
Bi begiak bustiri.