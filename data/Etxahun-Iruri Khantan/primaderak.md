---
id: ab-3683
izenburua: Primaderak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003683.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003683.MID
youtube: null
---

Primadeak goxo dirade Nabarra-ko bortietan
Artzainak sartzen direlarik ardiekilan saietan
Eijer dira bazterrak berde arbolak oro lorretan
Üthürriño baten aldean bi bihotz amoditan

Ürzo xuri xarmangarria zuiñen deitadazün plaser
Erregerik irusenak niz egün heben zuri exker
Adios erran behar deizut zure artzaiñ adixkider
Nahi zütüt espusatako ene maite Güztiz Eder

Amodiua gaiza ederra zunbat ez dü ilüsitzen
Hargatik Zantxo ni ez nüzü zure pormeser fidatzen
Ützi nezazü juraitera aita nik ez diket Kuntent
Jakiten badü zurekilan hola nizala abüsatzen

Ez da mündian orano izan erregerik ez besterik
Amodiuaren botzari behatü izan ez denik
Adam et.Eva egin ziren gütarik lehen lehenik
Bestelan ez beitzatekian düdarik gabe mündüri.