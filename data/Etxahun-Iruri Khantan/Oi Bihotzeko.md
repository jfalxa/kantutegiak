---
id: ab-3711
izenburua: Oi Bihotzeko
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003711.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003711.MID
youtube: null
---

Oi biotzeko bate nin maitia
Phena doloretan itzali zira
Hargatik holaz badüzü bakia
Aita baten zuk bethikotz bühürrikeriak
Desegin zin gure bien bizia
Nik miseria eta zük Khürütxia
Phakütako
Bidez bide banabila orai herratürik
Zü Kitatüz geroztik nik ez dü haboro bakerik
Aments zurekilan banintz hortxek ehortzirik
Bizitze miserable huntarik ni hürrüntürik

Zeren jin zira iratzarraztera
Dolümenetan phena Khuntatzera
Ni irus niz badit bake ederra
Ta zü aldiz
Bazuaza othoitze egitera
Gero ützül orduko Barkoxera
Izanenzira arnegat ber-berra
Nik bazakit
Zuaza zuaza Jakan egon lüzaz othoitzetan
Pelegri bekhatos baten gisa Kunbertitzekotan
Zuaza eta ütz nezazü phausü eder huntan
Bardin bazakit nizatekila ahatzerik bertan

Hori zer den gure destiniua
Untsa phenatü onduan heriuak
Khentzen dütü phena boztariuak
Sekülakoz
Eta hariekin Maria gaixua
Gure bien arteko amodiua
Hartakoz orai aitzina banua
Hürrütxago
Bidez bide banabila orai herratürik
Zü ütziz geroztik nik ez dizüt haboro phausürik
Aments zü bezala banintz hortxek ehortzirik
Ene bizitze miserabletik ni libratüri.