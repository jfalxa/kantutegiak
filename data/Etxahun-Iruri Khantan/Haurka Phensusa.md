---
id: ab-3730
izenburua: Haurka Phensusa
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003730.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003730.MID
youtube: null
---

Agorriko egün bero bat Agrika beltzin bezala
"Orhico-Txoriak" erreus dirade dizka hu ezin antola
Batek die flakatürik bihotza bestek aldiz xaramela
Bortxatü gira aithortzera atzo ezteilar ginela

Orhiko-Txorien familian biga zaizkü espusatü
Eta beren Khantari lagünak ezteila Khümitatü
Jan eta edan dügü ausarki dantzatü eta Khantatü
Khorpitzak phorrokixe dütügü bena nula libertitü

Gaizo Jean-Pierra atzo hi hintzan Khantari ororen izarra
Egün aldiz hortzek müt.türik Kasik begian nigarra
Ahatze hina gagüniala atsülütoki beharra
Hobe zükian galdü ükhen bahü botzaren phartzez bizarra

Behazaliak ez da ez aisa bethi perfeit izatia
Xiberotarren paperetan da bai txori bürükeria
Hartakoz galdegiten deiziegü giren bezala hun hartzia
Aitzinaxago bizi ditian gure "Orhiko-Txoriak".