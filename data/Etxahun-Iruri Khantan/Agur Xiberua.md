---
id: ab-3663
izenburua: Agur Xiberua
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003663.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003663.MID
youtube: null
---

Sor lekhia ützirik gazte nintzalarik
Parisen sarthü nintzan korajez betherik
Plaseres gose eta bürian hartürik
Behar niala alagera bizi
Bostetan geroztik
Nigar egiten dit
Xiberua zuri

Errepika :
Agur Xiberua
Baztek güzietako xokhorik eijerrana
Agur sor lekhia
Zuri ditit ene ametsik goxuenak
Bihotzan ersitik
Bostetan elki deitadazüt hasperena
Zü ützi geroztik
Bizi niz trixterik
Abandonatürik
Ez beita herririk
Parisez besterik
Zü bezalakorik

Palazio ederretan gira alojatzen
Eta segür goratik aide freska hartzen
Gaiñ behera so-ginik betzait üdüritzen
Orhigañen nizala agitzen
Bena ez dira heben
Bazterrak berdatzen
Txoriak khantatzen!

Ametsa, lagün nezak ni Atharratzerat
Ene azken egünen han igaraitea
Orhiko txoriaren khantüz behatzera
Pharka ditzan nik egin nigarrak
Hots, Xiberotarrak
Aintzinian gora
Eskualdün bander.