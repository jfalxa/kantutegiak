---
id: ab-3690
izenburua: Orhiko-Txoriak Eta Pettarreko Artzaiñak
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003690.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003690.MID
youtube: null
---

Bedatsiarekilan elhürra da hurtü
Orhiko-Txoriak gira ordin hegaltatü
Larrañen bullta hartü hebentxe phausatü
Nehiz hebenko artzaiñak goiti Khümitatü

Goiz huntan azkar günin Larrañen hegua
Bena Orhiko-txoriek sano zien gogua
Orhitik miratürik zien pettar xokhua
Bagenakin bazela han gur'inganiua

Üda berrin delarik bortia berdatzen
Urthe oroz bezala artzaiñak Larrañen
Amodioz dirade ixtant bat phausatzen
Eta zuñen goxoki gurekin dringatzen

Ez da mündü bat baizik ta bizi bat baizik
Behar da baliatü gaixo artzaiñ gaztik
Algarren maithatzeko ahalaz fidelki
Adixkide hunekin bihotzan erditik

Ez adiorik zier zeren Larraiñtarrak
Badaki bai zidiela hetüren gütarat
Ziren ardi salduekin gure lürretarat
Ziren haidürü gira oi gaizo pettara.