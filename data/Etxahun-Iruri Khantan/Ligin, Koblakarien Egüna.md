---
id: ab-3694
izenburua: Ligin, Koblakarien Egüna
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003694.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003694.MID
youtube: null
---

Orhi-Kotxoriek dener deikie hortan bihotza altxatzen
Ageri da zibero ez dela segürki orano galtzen
Nahiz eta artzin fidel bat dügün gük galdürik ükhen
Grégoire Eppherre, gure büria, orailbeitügü othoizten

Larrazkeneko azken ürzuak juan dira erranez adio
Bena ordari atzaman dügü heben lau bilhagarro
Gure saretan sarthü dirade et' eztira libratüko
Koblakan nausiak beitira horik Eüskal-Herriko

O Koblakari ene lagünak goxoki deiziet erraiten
Ziberun güziek deiziela ber bihotza exkentzen
Be r denboran besuan artian anaie bezala hartzen
Hiru probintziak beikirade ber Amaren laidatzen

Orai zien aldi Koblakariak eta zütiet ütziko
Ziberotarrer esplikatzera zien amodiua oro
Bazakit Kapable zidela sala hunen altxatzeko
Ützen zütiet arren ordari Ama zien gidatak.