---
id: ab-3695
izenburua: Gazte Girade Gazte
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003695.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003695.MID
youtube: null
---

Errepika:
Gazte girade gazte hogei urthen altian
Eta phentsamentia oro libertimentian
Sentho girade sentho trenpü izigarrian
Indar baten egiteko eskualdün plazetan

Irus girade adin hortan
Sükhar handi bat bihotzetan
Elgarretara jinik gaztiak herrokan
Irustarzün eder batetan
Zuiñ hobe ari gira Khantan
Jauzkan edo pilotan
Herriko plazetan

Bena irustarzün hori da
Barreatüxerik ixtant bat
Errejimentialat juran bejar beita
Nahiz ez dügün lan sobera
Han entsarratürik gira
Besten manhaspenian
Ümilik gaixuak

Heñ bat denbora ifaran da
Azkenekoz libratzen gira
Biba libertatia gaiza ederra da
Ützirik gure bestimenta
Kasernatik hürrün guratza
Gibel soik egin gabe
Nur gure etxetara.