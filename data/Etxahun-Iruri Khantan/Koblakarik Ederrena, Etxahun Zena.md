---
id: ab-3708
izenburua: Koblakarik Ederrena, Etxahun Zena
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003708.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003708.MID
youtube: null
---

Gora ! Agur ! Agur Barkoxeri !
Barkoxtarrak Barkoxtarsak
Alegrantziarekin
Uhure egin dezagün
Gur.Eskual Herriari
Gure Xiberuari
Barkoxeri

Gaztalondotik Larrajatik
Maltaik eta Bürgütik
Zer jendetzia duen plazarrat
Erriz ta Khantaz goratik
Zeren egün Barkoxen beita
Jei bat zuñen diferentik
Denak guratzan ikhustera
Jende hunak algarreki

Othoitzerik den goxuena
Eginik zur' ilherrian
Nahi zütügü Etxahun zena
Bethikoz izar herrian
Zeren zure orhitzapena
Argi da Eskual - Herrian
Adios Koblarik ederrena
Zaude zelüko loria.