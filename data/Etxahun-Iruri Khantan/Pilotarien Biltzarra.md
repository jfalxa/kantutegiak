---
id: ab-3739
izenburua: Pilotarien Biltzarra
kantutegia: Etxahun-Iruri Khantan
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003739.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003739.MID
youtube: null
---

Boztariozko egün huntan
Uhure Ama Eskual-Herriari!
Gure izar gidari huna
Egün zirade Baigorri!
Zure mendiak, zure mahastik
Guretzat dira xorrogarri
Zure plaza handi maithagarrik
Ederrez ez dü parerik
Agur, agur izar pilotarik !
agur gogotik deneri!

Egün Baigorrirat jin dira
Eskual-Herriko haurrik baliusenak !
Gure hiru probintzietako
Pilotari famatienak :
Badira gaztik eta hariekin
Adinak baztertü dütianak
Denek batian egin beharrez
Familiarik goxuena
Ahal oroz lagünt zazü
Ama Horiek egün hasi lana

Nun zütiegü Perkaiñ, Othare
Larralde, Xilhar, Leonis, Xikito ?
Ta zirekin zunbat bertzerik
Itzali dienak bethiko !
Gure plazetan jokatzeko
Hariek ützi etsenplü huna
Ez dügü ez ahatzeko
Izanez denak gogo bereko
Ez da pilota galdüko

Batarzün beri bat, gaztiak
Zientzat egina heben bada egün
Hortarat jin tziztaie denak
Heben girateke lagün !
Elgarrekilan jokü zaharra
Gorenetik altxa dezagün
Ta kaskoiñ gützaz jelos direnak
Etxeki ditzagün hürrün...
Atzo, bihar, hala nola egün
Bego pilota Eskualdü.