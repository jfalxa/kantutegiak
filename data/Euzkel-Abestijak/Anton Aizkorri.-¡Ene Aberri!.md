---
id: ab-3547
izenburua: Anton Aizkorri.-¡Ene Aberri!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003547.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003547.MID
youtube: null
---

(Bizkayeraz)

Anton Aizkorri,
Anton Aizkorri,
Aizkorri'ra juan da
damututa etorri.

¡Ene Aberri!
¡Ene Aberri!
Matte oni gustija,
berarentzat naz ni.