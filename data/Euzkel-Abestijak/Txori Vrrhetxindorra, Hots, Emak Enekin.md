---
id: ab-3510
izenburua: Txori Vrrhetxindorra, Hots, Emak Enekin
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003510.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003510.MID
youtube: null
---

(Zubereraz)

Txori vrrhetxindorra, hots, emak enekin
Maitiaren atela biak alkharrekin;
Erakutsi izok gero ahots ezti batekin
Haren adizkide bat bai-dela hirekin.

Heltv ginenian maitiaren atela,
Horak hasi zeizkvn txanphaz berhala,
Lasterr egin gvnian bertan eskvtatzera;
Vrrketxindorra igain zvhain batetara.

(Bizkayeraz)

¡Oi, txori politta! ¿Zer abil orr abesten?
¿Aberrijan azkena daustak iragarrten?
Ezeistak olakorik attatu be, arren;
Gurau-neukek ilddia neu-Ama baño len.