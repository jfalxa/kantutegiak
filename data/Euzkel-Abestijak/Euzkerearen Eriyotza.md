---
id: ab-3532
izenburua: Euzkerearen Eriyotza
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003532.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003532.MID
youtube: null
---

(Nabarrerazkua)

Nik matte nuban kutuna,
nere atsegin-zoruna,
ibilli bai ta, bai egon,
ez dut arrkitzen iñiñon.
Billaka beti bai nabil,
baña da agiyan nunbait il.

Ibai-ertzetan galdetzen
dut iya ote-dan emen.
Batzutan dagit oyuba,
bestetan, berriz, uluba.
"Mattia, diyot, ¿nun abil?,
baña da noski nunbait il.

LILLI EDERR BAT
(Benabarrerazkva)

Lilli eder bat bai-dut nik
aspaldi begiztaturik;
bañan ez nainte ausarrtzen
haren eskura harrtutzen,
ba'naki zer den galtzia
jun ninddatteke aldera.

Lilli ederra, so-idazu,
matte nauzunez errazu.
Zure begiyak bihotza
barrnarik deraut zaurittu;
bai-dizu zauri huntarik
ene utzitzeko il-illik.