---
id: ab-3544
izenburua: Uso Zurijoi
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003544.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003544.MID
youtube: null
---

(Bizkayeraz)

Uso zurijoi, ¿nora ua
ortze zabalaz egazka?
Mendi-aranak zuri yagozak,
lats-itturrijak lettu dozak.
Uso zurijoi ¿nora ua?
¿Aberri-ataz eyotan?

- Aizkidetxuboi ¿zegattik
ittun dagistak ittaun oi?
¿Mendijok dozak geure mendijak?
¿Errijau-ete asabena?
Arren, itxeistak juaten
ez-ikusteko Ama iltzen.