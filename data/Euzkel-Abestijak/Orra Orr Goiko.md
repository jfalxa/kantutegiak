---
id: ab-3511
izenburua: Orra Orr Goiko
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003511.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003511.MID
youtube: null
---

(Bizkayeraz)

Orra orr goiko
Areiztitxu baten
Kukubak umiak
Egin yozak aurten.
Kukubak egin,
Amilotxak jan,
¡Axe bere kukubaren
Zoritxarra zan!

¡Aupa, mutillak,
Gogorr jantza-egin!
Mai onen gañian
Izteai eragin.
Bijotz alaya
Beti geuria,
Zirkin eta zirkin
Jardun atseden-baga.