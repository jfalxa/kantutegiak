---
id: ab-3530
izenburua: Urrundik
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003530.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003530.MID
youtube: null
---

(Nabarrerazkua)

Urrundik ikusten dut
ikusten mendiya;
beraren gibelian
dago nire erriya.
Yadanik dut aditzen
zoriyon aundiya:
yuale mattiaren
asperen eztiya.

(Bizkayerazkua)

Gastetxu nintzanian
erritik urtenda
negarrez atzerrira
jun nintzan lan-billa.
Oin pozik aberrira
nentorran atzera...
¡Gaxuoi! Ondiño az
arrotzen jopuba.