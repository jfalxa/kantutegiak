---
id: ab-3524
izenburua: Itxarrtu, Euzko-Semia
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003524.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003524.MID
youtube: null
---

(Bizkayerazkua)

Itxarrtu adi, euzko-semia,
ken egixak lo txarrok.
Geure Aberri Ama Euzkadi
dola jakin biarr dok.
Laran-la ¡gora Jaun-goikua!
Laran-la ¡gora Lagi-Zarra!

Uri, baserri, zelai, mendita
geure lagi ederrak
gal-yozak arrotz-zalekerijak
amorde...ta...errderak.
Laran-la, ¡gora...

Jauna'k emonik Aberri ederr
ain garbi ta bikaña
au bazterrturik ¿nok matteko yok
amordeko zistriña?
Laran-la, ¡gora...

Atorr, euzkotarr arrotzalia,
atorr geure arrtera,
Aberri-aldez lan-egiteko
gogoz geuikin batera.
Laran-la, ¡gora...

Gorrotau egik zezenketea
birau, dantza zikiñak.
Matte bijotzez euzkotarr kirol,
jolas garbi bikañak.
Laran-la, ¡gora...

Jai gustijetan mendirik-mendi
azkarr juan gadixan,
uri-zuluan geratu-barik
sagu-zarren antzera.
Laran-la, ¡gora...

Aberri matte au bixi ba-da
zor yautsaguk Sabin'i,
urkamendittik kendu ta onek
yualako ipiñi.
Laran-la, ¡gora...

Alperrik arrotz ta arrotzaliak
esangoek bestera.
Sabin dola-ta gaizkatukozak
Euzkadi ta Euzkera.
Laran-la, ¡gora...

Aupa, mutillak, aurrera beti
au jaritxi-arrtian,
Euzkadi'gaz geuk arrtzekogero
Donokiko zoruna.
Laran-la, ¡gora...