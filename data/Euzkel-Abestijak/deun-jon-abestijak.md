---
id: ab-3503
izenburua: Deun-Jon-Abestijak
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003503.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003503.MID
youtube: null
---

(Bizkayeraz)
Deun-Jon'az zorgin, eltxo ta zorri
Gustijak dira erretan,
Soloetako landara onak
Garbi geratu dattezan.

Gure Aberri-ekandu onak
Gustiz garbirik izteko,
Itz dongea ta jantza zikiña
Erre daiguzan betiko.

¡Gora, gora, gora garbitasuna!
¡Aupa, bizkorr egin subari bira!
¡Ujuju!
¡Deun-Jon'agana guaz gu!

(I)
Alkarrekin eskutik elduta,
Ikotika ta barreka,
Geure bijotz-goguaren poza
Erakutsi bilddurrtzeka.

¡Gora, gora neskatil euzkotarrak!
¡Aupa, bizkorr egin bira ta bira!
¡Ujuju!
¡Aberrtzaliak gara gu!

(II)
Orrtik ziarr dabiltz txoritxubak
Samurr-samurrez pioka;
Aberri-txoritxubak geu gara:
Asi gadixan santsoka.

¡Gora...

(III)
Euzkadi da geure Aberrija,
Bijotzaren mattetsuba:
Opaldu dagijogun berari,
Loraz einda, burestuna.

¡Gora...