---
id: ab-3494
izenburua: Maitia, ¿Nun Zira?
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003494.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003494.MID
youtube: null
---

(Zubereraz)

Maitia, ¿nun zira?
Nik etzvtvt ikhusten,
Ez berririk jakiten,
¿Nurat galdv zira?
¿Ala althatv othe-da zure asmia?
Hitz eman zenereitan,
Ez behin, bai berritan,
Enia zinela.

Alhaba dienei
Erranen dit orori:
So-eidaziet eni,
Beha ene erraneri;
Gastetto direlarik untsa zvxendv:
Handitv direnian,
Berant date agian,
Nik bai-takit untsa.