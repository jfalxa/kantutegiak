---
id: ab-3514
izenburua: ¡Jantzara Muttilak!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003514.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003514.MID
youtube: null
---

(Bizkayerazkua)

Aurten iñungo jairik
baserri polit onetan.
Hamaika jantza biogu egin
arnasarik arrtuteka.
Geure bijotza alattuta,
poz-pozik egindda ¡ujuju!,
¡jantzara, mutillak, erdu!

Euzkadi-ekandubak
matte-matterik bai-doguz.
Eztogu gura jantza zikiñik,
garbixaliak gara gu.
Geure bijotza alattuta,
poz-pozik egindda ¡ujuju!,
jantzara, mutillak, erdu.