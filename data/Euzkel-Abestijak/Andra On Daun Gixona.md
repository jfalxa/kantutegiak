---
id: ab-3529
izenburua: Andra On Daun Gixona
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003529.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003529.MID
youtube: null
---

(Bizkayerazkua)

Neure auzo mattiak, Josepe ta Manu
pipa arrtuko neuke tabakua ba'neu;
pipaz gañera batta nok emon ba'lego
garbittuko neukela botilla bat ardau.
Andra on daun gixona nik eztot erruki,
Jaun-goikua'k emonik, nik alan eduki.