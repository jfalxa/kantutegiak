---
id: ab-3555
izenburua: Agate Deunaren Abestija
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003555.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003555.MID
youtube: null
---

(Bizkayeraz)

Aintzaldu daigun Agate deuna,
Bijarr da, ba, Deun-Agate;
Etxe onetan zorijon utza
Betiko euko al-dabe.

Euzkadi'ren ekandu ederrak
Geuk berrbixi gura doguz,
Orregattik zubekana gatoz:
Agurrik samurrena arrtu.

Geu gara Aberrijaren semiak,
Euzkadi da geure Ama;
Semiak Ama matte-daben lez
Matte dogu Aberrija.

Deun-Agate'na batzeko gatos
Bertoko mutil gastiak:
Bijotzez arrtu gagixubez ta
Zabaldu zuben sakelak.

Etxe onetako euzkotarrai
Opa-dautseguz zoruna,
Jaun-Goikua'ren eskarr berua
Eta azkatasun-argija.

Orain bai-guaz alde-egittera:
Agurr-daitzubegu pozik;
Agate deuna bittarrte dala
Ezexube ixan kalterik.