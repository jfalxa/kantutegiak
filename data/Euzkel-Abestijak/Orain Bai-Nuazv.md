---
id: ab-3540
izenburua: Orain Bai-Nuazv
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003540.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003540.MID
youtube: null
---

(Zuberoraz)

Orain bai-nuazv herrittik:
ardvra dizvt negarra begittik.
Bat maithatv nian gogotik,
bihotzaren erdi-erdittik;
alde-egin beharr dizvt ittunki
¡Ai! ¡ei! ¿nula biziko niz ni?

Ene maitioi, orain nik
jakin nahi nvke zvganik
zerk zadvkan horren ittunik,
so eztiak oro galdvrik;
ala dvzvnentz beldvrrkvnterik
maithatzen dvdan zvtaz besterik.