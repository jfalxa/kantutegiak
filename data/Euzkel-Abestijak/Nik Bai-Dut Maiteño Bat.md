---
id: ab-3499
izenburua: Nik Bai-Dut Maiteño Bat
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003499.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003499.MID
youtube: null
---

(Benabarreraz)
Nik bai-dut maiteño bat, ¡oi!, ¡zoin hura lako!
Ez da tipi ez handi, bai bien arrteko;
Begia du ederra, urdin ezti oso,
Bihotzian sarrthu zaut ez baitzaut jelgiko.

¿Oztian zenbat izarr, maitia, ahal-da?
Zure barrdinik ene begietan ez da;
Neke zaut juaitia, maitia, zuganik;
Agurr erraiten dautzut bihotz-bihotzetik.

(Bizkayeraz)
¡Oi, Aberri laztana, nik matte-zaudana!
Ikusi gura-zenduket azke ta garbija.
Neure bijotz-jabia zeu bakarrik zara;
Pozik, zebarren, emongo neuke neure bixitza.