---
id: ab-3491
izenburua: Itxasua
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003491.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003491.MID
youtube: null
---

(Gipuzkeraz)

Itxasua laño dago
Bayona'ko alderaño
Nik zu zaitut maitiago
Txoriyak beren umiak baño.

Gure oroiz aita dago
Laño-pian gaberaño.
Nik zu zaitut maitiago
Arraitxuak ura baño.