---
id: ab-3502
izenburua: Zelvko Izarren Bidia
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003502.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003502.MID
youtube: null
---

(Zubereraz)
Zelvko izarren bidia
Nik ba'neki,
Han niro ene maite gastia
Zvxen khausi;
¡Bena gaurr jagoiti nik hura
Ez ikhusi!

Haritz gaste bat nik aihotzaz
Ebakirik
Vdvri zait ene bihotza
Zauritvrik
¡Erruak eroriko zaitzola
Eihartvrik!

Zeren beitzen lili ororik
Eyerrena
Bai eta ene bihotzeko
Maitenena,
¡Haren izanen da ene azken
Hasperena.