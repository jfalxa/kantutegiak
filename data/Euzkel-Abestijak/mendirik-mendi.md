---
id: ab-3516
izenburua: Mendirik-Mendi
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003516.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003516.MID
youtube: null
---

(Bizkayerazkua)

Guazan mendirik-mendi,
euzkotarr gastiak;
guazan aldatzak gora,
mendigoxaliak;
axe osasuntsubaz
bixitza inddarrtzera
aberri gustijari
agurr egittera.

Tontorrera elduta
abestijai ekin,
santso eta irrintsi,
uyuyu inddarrtsu ein.
¡Gora Jaun-goikua ta
gora Lagi-Zarra!
¡Gora, gora betiko
Aberri laztana.