---
id: ab-3519
izenburua: Basatxoritxu
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003519.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003519.MID
youtube: null
---

(Bizkayerazkua)

Basa-txoritxu musturr-luziak,
ez jun Donoki'ra billa;
aingerutxurik biarrezkero
orra orr nire mutilla.
Bere musuba ederra da ta
suba bijotz ta ezpanak;
berton urturik bixiko dira
nire bularra ta zanak.

¡Ai, nok leukezan urrezko orratza,
urre-miesa ta arija,
urrez asi eta amattuteko
seintxu ontzako janzkija!
Larrosatxubak bost orri daukoz,
klabeliñeak amabi;
gure umetxuba gura dabenak
eskatu bere amari.