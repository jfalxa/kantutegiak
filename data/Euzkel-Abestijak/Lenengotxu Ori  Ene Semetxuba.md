---
id: ab-3541
izenburua: Lenengotxu Ori / Ene Semetxuba
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003541.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003541.MID
youtube: null
---

(Bizkayeraz)

La lara la la, la la lara la lara,
la lara la la, la la lara la la.

Lenengotxu ori,
punta-biatz ori,
beste gustijen arrtian
txikarra dok ori.

Bigarrentxu ori, punta... nagija dok ori
Irugarren ori... luzia dok ori
Laugarrentxu ori... sendua dok ori
Boskarrentxu ori... lodija dok ori.

ENE SEMETXUBA
(Bizkayeraz)

Ene semetxuba,
matte kutuntxuba,
mosu ta laztandu
egik eure amatxuba.

Amaren pozkarri
eu az ben-benetan:
lo egik, ene kutuna,
neure besuetan.