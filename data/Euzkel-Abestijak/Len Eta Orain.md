---
id: ab-3536
izenburua: Len Eta Orain
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003536.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003536.MID
youtube: null
---

(Bizkayerazkua)

Antziñako
asaba zarrak
izkilluz ebesan Aberritik
jaurti errbestarrak.
Orain barriz dirudun danari
zabaltzen yakozak zoliki
beso euzkotarrak!

Aldi baten
euzko-semiak
txiruak zuazan ta landerrak,
baña baitta azkiak.
Orain ostera arrotzik baltzenei
saldu yaroatsoeguzak
odol ta legiak!

GOXIAN ON
(Nabarrerazkua)

Goxian on,
arratsian on,
matsaren zumua beti duk on,
eta bet iduk on.
Etxera abiya ta juan ezin
erori eta bertan etzin,
eta bertan etzin.