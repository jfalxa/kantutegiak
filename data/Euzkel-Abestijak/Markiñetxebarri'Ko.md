---
id: ab-3518
izenburua: Markiñetxebarri'Ko
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003518.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003518.MID
youtube: null
---

(Bizkayerazkua)

Marrkiñetxebarri'ko senarr-emazte bik
alaba ederra euken Jaungua'k emonik
eta amarr milla dukat lurrpian jabonik
ikusten ezebela egunen argirik.

Bete euzanian neskak amazorrtzi urtiak
zorgin bat aurrki-eban etxetik urrian,
eta asi yakon itz-ein erazo gurarik
ia eukan ezkontzeko emonda berbarik.

Barrez erantzun eutson bere bixarretan
ezebala, ez, berak igarri kartetan.
Alperr zorgin au zan igesten ariña;
eldu yakon zankora txakurren agiña.