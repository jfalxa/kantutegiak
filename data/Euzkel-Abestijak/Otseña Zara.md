---
id: ab-3523
izenburua: Otseña Zara
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003523.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003523.MID
youtube: null
---

(Bizkayerazkua)

Zorijona
zeure laguna zan
zeure lagijen
aldijan.
Noz-nai
zeure buruba
ziñan;
gatxa zer dan
etzekijan.
Itsurik
zeuk arrotza
onetsi, baña,
ta emon zeutson
bijotza.
Ordutik ona
otseña zara,
jabia dozu
España.

Ara zelan,
Euzkeldun errija,
jausi zarean
lexara.
Lotsa...
Lotsarik eztau
orretara
dagonak
azpiratuta.
Begirik
ori ikusteko
ba-daukozu
noz-edo-noz,
Ama, itxarrtu.
Ta orrban ori
kendu odolaz,
arren, negarrez
ez-kendu.