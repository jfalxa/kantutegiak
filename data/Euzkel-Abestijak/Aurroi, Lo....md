---
id: ab-3539
izenburua: Aurroi, Lo...
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003539.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003539.MID
youtube: null
---

(Nabarreraz)

Aurrtxuoi, egik lotxo bat,
emango diyat goxua;
attak bat eta amak bi,
ta Goiko Jauna'k amabi.
¡Lo!...

(Bizkayeraz)

Lo, lo, gotzonen arrtian,
ludi txatxarroz aiztuta;
zorijona Goyan yagok,
bian eztok miña baño.
¡Lo!...