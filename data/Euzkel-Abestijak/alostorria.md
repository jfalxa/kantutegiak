---
id: ab-3534
izenburua: Alostorria
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003534.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003534.MID
youtube: null
---

(Gipuzkerazkua)

Alostorria, bai, Alostorria
Alostorre'ko zurubi luzia.
Alostorria'n nenguanian
gorubetan, bela beltza
kua, kua, kua, kua leyoetan.

(Bizkayerazkua)

Mendijen ganetik agiri dira
bela arrotz zittalak geuri begira.
Gosez geugara bein etorritta
¡geu gaxuok! zati-birrindduko
gabez euzkotarrok.

Euzkadi gaxua, ¿nun jausi zara?
il-zorijan duzu zeure buruba.
Motza sarrtuta, azkatasun-barik,
jopu einda...Jagi, arren, jagi
lasterr edo il, Ama.