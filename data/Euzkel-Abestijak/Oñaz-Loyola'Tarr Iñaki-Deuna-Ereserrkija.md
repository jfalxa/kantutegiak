---
id: ab-3496
izenburua: Oñaz-Loyola'Tarr Iñaki-Deuna-Ereserrkija
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003496.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003496.MID
youtube: null
---

(Bizkayeraz)

Iñaki,
Jaungua'k bidalduba
Ludijan ixatera
Josu'ren
Gudarijen Buruba;
Batzarrak
Autu zinddun
Bizkattarren
Zain Deuntzat.
Zeu zara
Zaindari ta
Jaun onena
Geuretzat.
Iñaki,
Entzun, arren, eiguzu
Ta geure opea arrtu.
Bijotza
Bizkattarrak
Jayotzetik
Damotzu;
Zeuria da
Beria...

Baso, mendi, ibai, arru...
Zeru-gottik,
Deun-Iñaki,
Jabon, ba,
Bizkaya.
Jabo-ixu
Lagi-Zarra;
Areyo-aurrian
Zeuk aginddu
Bizkattarrak...
Gudean sarrturik,
Ezpatea arrtuta,
Bizkattarren aldez,
Donge ta areyua
Indarr zerutarraz
Zatittu osoro ta...
Zorijontsuba
Bizkaya beti ixango da
Zeuk, Iñaki,
Zainduta.