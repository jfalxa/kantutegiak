---
id: ab-3505
izenburua: Gaurik Baltzena
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003505.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003505.MID
youtube: null
---

(Bizkayeraz)
¡Dan...dan...dan...!
Eguzkijak iges-einda,
Dana illundan.

Euzkadi estal
- Bei gau-pian,
Lagi-Zarra gal-eban ta
Baltzittu zan.