---
id: ab-3500
izenburua: Ni Mendexa'Ra
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003500.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003500.MID
youtube: null
---

(Bizkayeraz)
Ni Mendexa'ra Deun-Kepa-aldijan
Bai-nua, bai, jayetara.
¡Arek, bai, neskatxak
Ederrak, lirañak
Lekettiarren aldian
Mendexa baserritxuban!
Ta Jon-Deunan, ta Jon-Deunan
Beti zaukodaz goguan.

Igaztik ona urtebetia da,
¡Aldi orreren luzia!
Ordutik ona
Gau ta egunetan
Aldatu leike gastia,
Ta Jon-Deunan, ta Jon-Deunan
Beti zaukodaz goguan.