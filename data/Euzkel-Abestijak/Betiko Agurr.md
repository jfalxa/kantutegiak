---
id: ab-3557
izenburua: Betiko Agurr
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003557.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003557.MID
youtube: null
---

(Bizkayeraz)

Betiko agurr-egitten natorrkin, ene matte;
ittunak neure bijotzau oñazez yon lerrtuten:
lasterr ¡oi! Neugaz aiztu ta beste bat doken matte.

¿Zegattik egitten daustak betiko agurr neuri?
¿Uste dok ene bijotzon eztola mattasunik?
Matte bat bakarrik yuat: matte oi az erori.