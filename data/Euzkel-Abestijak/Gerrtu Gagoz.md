---
id: ab-3527
izenburua: Gerrtu Gagoz
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003527.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003527.MID
youtube: null
---

(Bizkayerazkua)

Euzkadi polit-ederra
erdi-illik eguan;
abenda gaxuagorik
eztago ludijan.
¡Gaxua!
¡Gaxua!
Euzko batek didar einda
zutik jarri giñan;
gerrtu gagoz bere aldez
asteko lanian.