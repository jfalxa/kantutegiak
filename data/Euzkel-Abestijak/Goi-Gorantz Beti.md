---
id: ab-3513
izenburua: Goi-Gorantz Beti
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003513.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003513.MID
youtube: null
---

(Bizkayerazkua)

Gorago beti
zure gogo ori
jaso-ixu, euzkotarra, bai sendoki.

Aberrijak dei-gaittu,
urrgazi-eske dogu:
¿nok emongo eztautso,
iradu?

Goi-gorantz beti,
gogo zinddo ori;
Aberri ama biarr dogu eutsi.