---
id: ab-3542
izenburua: Txoritxubak Kayolan
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003542.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003542.MID
youtube: null
---

(Bizkayeraz)

Txoritxubak kayolan
ittunik dau abesten,
daularik bertan zer jan,
zer edan,
basua gura-itxaten:
azke, azke
bixi-ixanaz
bai-da gomutaten.

Urrezko espetxian
bixi da oin txorija;
joranez, baña, basora
begira
bixitza yako itxungitzen.
Azke, azke
bixi-eziñaz
gaxua da iltzen.