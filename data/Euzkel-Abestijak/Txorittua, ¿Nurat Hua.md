---
id: ab-3489
izenburua: Txorittua, ¿Nurat Hua?
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003489.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003489.MID
youtube: null
---

(Zubereraz)

Txorittua ¿nurat hua bi hegalez aizian?
Nabarra'lat juaiteko elhvrra dvk bortian:
Junen gvtvt alkharrekin hura hurtv denian.

Hasperena, habilua maitiaren atela:
Habil, eta erran izok nik igorrten haidala;
Bihotzian sarr-hakio hura eni bezala.