---
id: ab-3504
izenburua: Iruten Ari Nuzu
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003504.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003504.MID
youtube: null
---

(Benabarreraz)
Iruten ari nuzu, khilua gerrian,
Ardura dudalarik nigarra begian.

Horrek erraiten dute: ezkondu, ezkondu;
Nik ez dut ezkon-minik, gezurra diote.

(Bizkayeraz)
Benetan da politta geure Aberrija;
Bijotzez matte-daigun, berak matte-gaxan;
Gustirik gaxoena da-ta geure Aberrija.