---
id: ab-3487
izenburua: Euzko-Abendea'Ren Ereserrkija
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003487.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003487.MID
youtube: null
---

(Bizkayeraz)

¡Gora ta gora Euzkadi!
¡Aintza ta aintza
Bere Goiko Jaun onari!

Areitz bat Bizkaya'n da
Zarr, sendo,
Zindo
Bera ta
Bere Lagija lakua;
Areitz-ganian dogu
Gurutza
Deuna
Beti geure goi-buru.
¡Abestu gora Euzkadi!
¡Aintza ta aintza
Bere Goiko Jaun onari.