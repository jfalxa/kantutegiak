---
id: ab-3560
izenburua: Mañari'Ttik Asi Ta...
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003560.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003560.MID
youtube: null
---

(Bizkayeraz)

Mañari'ttik asi ta
Urrkiola'raño
ezta aurrkitzen besterik
aldapea baño.
Durango'n bazkalduta
Mañaria'n gora
Txakurrtzulo'n jausi naz
ipurrdijaz gora.