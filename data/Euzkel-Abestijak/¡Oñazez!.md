---
id: ab-3522
izenburua: ¡Oñazez!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003522.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003522.MID
youtube: null
---

(Bizkayerazkua)

Aldi baten gure ama,
Aberri maitagarrija,
semiok genkusan edurr
barrija baxen garbija:
Orain ostera,
itxasi yako
arrotzen dongakerija!
Au tamal mingotsa!
au oñaze latza!
Orrbandu dala Aberrija!

Aldi baten, lilli jayo
ederrtasunez jantzija,
aldi baten, baratz bikain,
dongaentzako ertsija:
Oraintxu barriz,
baltzez jositta,
kutsatu yako irudija...
Ez negarrik, ama!
zeu garbitziarren
emongo dogu bixija.