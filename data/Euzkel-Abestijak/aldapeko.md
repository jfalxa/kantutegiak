---
id: ab-3546
izenburua: Aldapeko
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003546.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003546.MID
youtube: null
---

(Gipuzkeraz)

Aldapeko sagarraren
adarraren puntan,
puntaren puntan,
txoriya dago
abeslari.
Txiru-liru-li
txiru-liru-li,
¿nork dantzatuko ote-du
eresitxu ori.