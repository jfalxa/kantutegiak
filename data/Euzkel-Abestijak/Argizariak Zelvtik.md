---
id: ab-3508
izenburua: Argizariak Zelvtik
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003508.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003508.MID
youtube: null
---

(Zubereraz)

Argizariak zelvtik
Argitzen dizv eyerrki;
Ene maite polita eztvzv ageri
¡Zelvko Jinko Jauna! zer eginen dvt nik,
Zer eginen dvt nik.

- Zvri nurk zinhetsi eztvzv,
Gvziak erriz ari zaitzv;
Bathv orotzaz atsegin zira zv,
Bat harr-ezazv, hura aski dvkezv,
Horrez baitin nvzv.

- Vrzo-aphalaren zoria
Galdvzgeroztik lagvna!
Alege da bethi bere bihotzetik,
Zeren ezpaitv maithatv bat baizik,
Maithatv bat baizik.

Maitetarrzvn berriak
Sendotzen tizv eriak;
Zure begiak dira ezti-argiak
Zeren baitira eniak zuriak,
Zuriak eniak.