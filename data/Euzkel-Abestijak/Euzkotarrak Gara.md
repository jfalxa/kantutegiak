---
id: ab-3550
izenburua: Euzkotarrak Gara
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003550.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003550.MID
youtube: null
---

(Bizkayeraz)

¡Euzkotarrak, euzkotarrak gara!
¡Gora Jaun-Goikua eta Lagi-Zarra!

Ezkara españarrak,
Ez arrotzaliak,
Ez maketuen edo
Motzen aizkidiak.
Geure Ama danaren
Semiak geu gara:
Bera Euzkadi dala
¡Euzkotarrak...

Geure Euzkadi mattia
Motzak menbetuta,
Nai biarr dogu danok
Olan dakuskula,
Bere azkatasuna,
Azkatasun utsa,
Ezpa-gagoz nastuta,
Motzak ezpa-gara.
¡Euzkotarrak...

Antziñeko enda zarr,
Garbi ta bakana,
Orain maketuenak
Betuta daukona,
Biztuko da ta betik
Jagiko da gora,
Eta euzkotarrena
Euzkadi ixango da.
¡Euzkotarrak...