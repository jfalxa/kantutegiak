---
id: ab-3549
izenburua: Lurraren Pian
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003549.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003549.MID
youtube: null
---

(Bizkayeraz)

¡Lurraren pian sarr-nindaiteke,
maitioi, zure ahalgez!
Bost oldozketa egiñik nago
zurekin ezkondu-beharrez.
Atia ertsi barrniaz eta
beti etxian nigarrez,
gogua arantzaz josita, bai ta
bihotza miñez lerrtuta...
¡Ene oñazez hil-erazteko
sorrtua ziñan arauez!

¡Zori hunian sorrtua ziñen
izarr ororen izarra!
Zure berrdiñik etzaut jiten
neure begien aurrera.
Ezkon-laguntzat galde-ein zintudan
erran nerauzu bezela:
Bañan zuri ez iruditu
zuretzat aski nintzala.
¡Ni baño hobe batekila
Jainkoa'k gertha zitzala.