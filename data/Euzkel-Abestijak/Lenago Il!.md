---
id: ab-3528
izenburua: Lenago Il!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003528.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003528.MID
youtube: null
---

(Bizkayerazkua)

¿Ze ikusten dabe
neure begijok?
Au danau dakust
galduta:
berezi, baso,
muru ta mendi,
uri, baserri
ta dana...
Motzak sarrtuta,
saloberiok
ara birrindu
dabela.
¡Il nayago dot
ikusi baño
Aberrijaren
amaya!

Esaidazube,
Euzkotarrak, ba,
nun diran zuben
errijak.
Eztira agerrtzen
iñun-iñun bez
zuben asaben
lagijak.
Motzak sarrtuta
zuben etxion,
dira amen jabe
nausijak.
¡Ez esan iñoz
mendi onek, ba,
dirala zuben
mendijak!

¿Noz euzkotarrak
jaun-goikua'ren
ixen deuna aiztu
len eben?
Orain motzak lez
lottuten dabe
loidun errderaz
egitten.
Ekandu onak
itxitta, baltzak
arrtu ditube
gaurr emen.
¡Goiko Jaun ona:
erruki zadi
aiztu zabenoi
bai, arren!

¡Erri gaxua!
¿Jayo nintzan ni
zeure il-orduban
eltzeko?
¿Zegatik, Ama,
zeure semiok
eztira itxarrtzen
ondiño?
¿Ama, ilgo zara
motzen azpijan?
¿Ilgo zara zeu
betiko?
¡Itxaron, Ama!
¡jarijon biot
neure odol au
lenago.