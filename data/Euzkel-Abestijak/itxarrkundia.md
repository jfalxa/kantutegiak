---
id: ab-3495
izenburua: Itxarrkundia
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003495.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003495.MID
youtube: null
---

(Bizkayeraz)

Azkatasun-eguzkija
Basotik urrten da,
Bere argija edonun
Arin zabaltzen da.

¡Itxarrtu zaiz, euzkotarrak!
¡Aupa, euzkeldun gustijak!
¡Gora, gora antziñeko lagijak!

Ixilik ba-zagoz danok
¿Zer dozube entzuten?
Ara, gaurr guda-santsuak
Gattuzan dettuten.

¡Itxarrtu...

Arrpegija gorrittuta
Darakus euzkijak;
Gorrijagotuko dira
Bertoko mendijak.

¡Itxarrtu...