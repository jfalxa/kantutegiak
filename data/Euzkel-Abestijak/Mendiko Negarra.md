---
id: ab-3501
izenburua: Mendiko Negarra
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003501.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003501.MID
youtube: null
---

(Bizkayeraz)
Ene Aberri laztana
jausi zara errbestepian...
¡Obia erijotza da!

Zeure basotik
Atzo igoten zan
Eskari ona
Zerura.
Gaur errdeldunak
Sarrtuta,
Axia emen
Birauz bete da,
¡Zelan ondiño il etzara!

Ekandu onak
Asabeari
Zoruna emon
Len eutsen,
Ara motzenak
Gaurr emen
Zeure bijotz
Garbija usteltzen
¡Ta zeu, Ama, etzra iltzen!

Gurutz zurija
Baltzittu da ta
Areitz orrleya
Orittu.
Geratu ezer
Etxatzu:
Seme onik,
Ama, eztozu.
¡Il, Aberrija, zakigu!

Ez-egin negarr:
Etzaut, Ama, nai
Negarr-egiñik
Zu ikusi:
Zeure negarrok
Motzari
Eragitten
Dautso barre-irri.
¡Il, Ama, lasterr! ¡Il zadi.