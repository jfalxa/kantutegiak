---
id: ab-3559
izenburua: Illazkittan / Argizagi Ederra
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003559.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003559.MID
youtube: null
---

(Bizkayeraz)

Ortze ixillan argi-dagijan illazki ederroi:
¿Geure zori au lakorik noz ete-dok ikusi?
Len azke ixanok oin yoguk arrotza nagusi...
Bide latzau argittu itxaroz, gentza-geznarijoi.

Ala-arantzak bijotz gaxuok zulatzen yoskuzak.
¿Zegatik, ba, barre-irri dagik geure zori baltzaz?
¿Ala ortze-gotik dakusk egubantz barrija?
Itxaro gozo au lasterr ¡oi! bete egi geuretzat.

Illun-arrtez yagirk argija malkuok legorrtzen.
Eure bidez ua, illazki, bestetzuk argitzen.
Zorun-euzkija odoltsu baso-ostez jagi dok:
Bere izpijak euzko-semiok yuez azkatuten.

ARGIZAGI EDERRA
(Laburderaz)

Argizagi ederra, argi-egidazu;
Oraño bide luzian jun beharra nuzu.
Gau huntan nahi nuke maitia kausitu;
Haren ateraño argi-egidazu.