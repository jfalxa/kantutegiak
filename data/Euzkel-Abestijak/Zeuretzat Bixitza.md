---
id: ab-3551
izenburua: Zeuretzat Bixitza
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003551.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003551.MID
youtube: null
---

(Bizkayeraz)

Negarrez ikusten zaut,
ene Aberrija;
zeure umiak zattube
ondatu gustija.
¡Ai ene Ama!
Ona nun dakarrtzudan
zeuretzat bixitza.

Mosu otza ein dautzu
erijotza ankerrak;
ta zeure bijotz onan
lettu da odola.
¡Ai, ene Ama!
Arren ¡oi! Arrtu neuri
zeuretzat bixitza.