---
id: ab-3538
izenburua: Leyotxutik
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003538.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003538.MID
youtube: null
---

(Nabarreraz)

Leyotxutik leyotxura
axia otza,
¡oi! axia otza.
Sarrtu bedi barrenera;
an ¿zertan dago?
an, bai ¿zertan dago?

(Bizkayeraz)

Urrunetik aberrira
bidia latza,
¡oi! bidia latza.
Txori ba'nintz niñuake
arantza egaz,
bai, arantza egaz.