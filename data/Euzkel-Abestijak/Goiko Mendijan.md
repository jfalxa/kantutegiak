---
id: ab-3488
izenburua: Goiko Mendijan
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003488.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003488.MID
youtube: null
---

(Bizkayeraz)

Goiko mendijan
Edurra dago,
Erreka-aldian ixotza.
Neu zeugandik
Azke nago ta
Pozik daukot
Bijotza.

¡Ene Aberri
Polit-ederra!
¿Zeu lakorik nun dago, ba?
Zeugaz beti
Bixi gura-dot
Eldu-arrte
Erijotza.