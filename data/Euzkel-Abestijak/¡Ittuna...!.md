---
id: ab-3533
izenburua: ¡Ittuna...!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003533.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003533.MID
youtube: null
---

(Bizkayerazkua)

- ¡Oi, Edurrnetxu mattia,
len bijotz-alaya!
¿Zer donala-ta eldu yan
oin gogora ittuna?
Goratu bijotza,
aurrpegija alattu
pozik ibilli ta
ikusiko don
¡bixitza donan on-ederra!

¡Goiko Jauna'ren Lagija
aiztu-ezetsitta!
¡Geure Aberri gaxua
ilddeko zorijan!
Txorijak ixilddu,
eguskija illundu,
lorak zimeldu,
¡au zoritxarra!
¡Betiko yuadan alaya.