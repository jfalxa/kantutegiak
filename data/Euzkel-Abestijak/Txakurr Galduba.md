---
id: ab-3498
izenburua: Txakurr Galduba
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003498.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003498.MID
youtube: null
---

(Bizkayeraz)

Txakurr gorri-polit bat
Galdu yaku guri;
Ezaugarija bauko,
Argittu ba'ledi.

Belarri baltzak eta
Bustana bellegi,
Iruntsiko leukela
Ogittan libra bi.

Nik matte-dot bijotzez
Euzkadi ederra,
Euzkadi bai-dalako
Euzkotarren Ama.

Ene Ama kuttuna,
Matte-laztantxuba:
Eutsi neure bijotza
Ta neure gogua.