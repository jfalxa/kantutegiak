---
id: ab-3493
izenburua: Urrkiola'Rako Bidian
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003493.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003493.MID
youtube: null
---

(Bizkayeraz)

(I) AUZOKUAK

- ¿Ain arin
Nora yuan
Uso zurijena?

- Mendira,
Andoni'ri
Egittera arrena.

- ¿Zein arren
Urrkijola'n
Deunari etteko?

- España'n
Neba ona
Ez-gaiztotuteko.

(II) EZKONAK ETA LENENGO UMIA

- Jon Ander,
¿Ze eskiñiko
Yeutsagu Deunari?

- Ume au
Emongeutsan
Bijotzez Bera'ri.

- ¿Zelan, ba,
Geuretxu au
Ixangok beria?

- Ba-dona
Jaun-Goiko ta
Lagi-Zarr-zalia.

(III) ATTITTEA TA BILLOBA GEXUA

Attitta,
¿Deuntxubari
Zein eingeutsat eskatu?

- Laztana
Lasterrtxuben
Agigun osatu.

- Ta ¿gero
Zelan eskarr
Emongo deutsat nik?

- Seme on
Ta bizkattar
Zintzoena ixanik.