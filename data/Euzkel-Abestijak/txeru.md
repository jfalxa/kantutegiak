---
id: ab-3535
izenburua: Txeru
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003535.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003535.MID
youtube: null
---

(Bizkayerazkua)

Txeru kartzelan dago,
damia kanpuan;
Txeru'k gura leukela
ba'leuko albuan.
¡Txeru!
Artakamara motxoliñua,
domingillua lairai-lon.

¡AMA!
(Bizkayerazkua)

Ama, zeu-barik larri,
nago neu benetan;
erdu ta poztu naxu
zeure besoetan.
¡Ama!
Triskan egixu neskatotxuba,
mutikotxuba, lairai-lon.