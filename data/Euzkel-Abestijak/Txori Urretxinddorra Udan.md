---
id: ab-3515
izenburua: Txori Urretxinddorra Udan
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003515.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003515.MID
youtube: null
---

(Bizkayerazkua)

Txori urretxinddorra
udan da abeslari,
orduban bai-daulako
basuan janari.
Neguban ezta agiri,
¡Ez-al-dago, ba, eri!
Udan ba-da agiriko
naz ni gotasauko.

Txori urretxinddorra
euria abots zolija,
eure marguak ederrak,
eugan beti poza.
Bijotzez mattetan aut,
ene txori politta.
Poztuteko Euzkadi,
erdu ona beti.

Geure aberri-basuak,
geure mendi-zelayak
lagunik mattentzat auke,
udabarri-txorija.
Samurr-samurr abestu,
alai geure gogua.
Poztuteko Euzkadi,
erdu ona beti.