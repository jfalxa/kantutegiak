---
id: ab-3490
izenburua: ¡Ene Ama!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003490.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003490.MID
youtube: null
---

(Bizkayeraz)

Bixi ezta
Ludi gustijan
Ene Ama
Baxen ederra, baña ez
Ain gaxua.

Gustiz ederra
Bera dalako,
Errbeste-pian
Jausi da...

¡Ara batta,
Sorrtu dittuzan
Semiok, ba,
Berari bixitxa kentzen
Dautsoela.