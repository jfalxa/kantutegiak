---
id: ab-3525
izenburua: Egunto Batez
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003525.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003525.MID
youtube: null
---

(Zubererazkua)

Egunto batez ninddagualarik
mattenarekin leihuan,
erran vkhen niriozvn
hura niala goguan,
ene oinhaze mingatzaz
urrikal harrtv lezan.