---
id: ab-3507
izenburua: Matzaren Orrpotik
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003507.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003507.MID
youtube: null
---

(Bizkayeraz)

Matzaren orrpotik datorr
Mama gozua,
Mama gozua,
Edango neukela
Beterik basua:
¡Klink!
Beterik basua.

Nik zuri,
Zuk niri
Agurr-egiñaz alkarri,
Basua txit garbi
Biarrko da ipiñi.