---
id: ab-3561
izenburua: Txakolin
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003561.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003561.MID
youtube: null
---

(Bizkayeraz)

Txakolin, txakolin, txakoliñak on-egi:
Gustijok edaigun alkarrekin.

Bijotza pozez betetan da txurrut bat arrturik;
Eztago iñun au lako edakirik.