---
id: ab-3545
izenburua: Goizian Goizik
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003545.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003545.MID
youtube: null
---

(Zuberoraz)

Goizian goizik jeiki nvndvzvn
ezkondv nintzan goizian;
bai eta ere bitxiz apaindv
ekhia jelkhi zenian;
etxekandere zabal nvndvzvn
egverdi erdittan,
bai eta ere alhargvntza gaste
ekhia sarrthv zenian.

Nik bai-nizun maitetto bat
notin ororen ixillik,
notin ororen ixillik eta
Jinko Jauna'ri ageririk;
txorta bat igorri dittadazvt
lilli bakhanaz egiñik,
lilli bakhanaz egiñik eta
erdia phozuatvrik.