---
id: ab-3553
izenburua: ¡Ken!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003553.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003553.MID
youtube: null
---

(Bizkayeraz)

¡Kendu, kendu
maketuok eta
euzkotarr maketozaliok!
¡Bota, bota
azurrbaltzok eta
euren lagun gustijok!
Kendu, kendu,
bota, bota,
geuria galtzen dagozanok.
¡Kendu, kendu,
atara bota, bai,
zapal nai gabezanok.