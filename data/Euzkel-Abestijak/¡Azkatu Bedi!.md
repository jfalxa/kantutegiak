---
id: ab-3531
izenburua: ¡Azkatu Bedi!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003531.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003531.MID
youtube: null
---

(Bizkayerazkua)

Emen lagun-arrtian,
azkatasun osoz,
abestu biarr dogu
Aberrija gogoz.
Sarri areyo-arrtian
ixil biarr:
¡Nasaittu bijotzak!
¡Entzun beiz abotsak!
Goraldu orain ama,
Aberri laztana,
¡Azkatu bedi!
Jarei ta azke beti
ikustiarren, ba,
odol au ixuri.
¡Barriz!
Aberrija azkatziarren,
¡egiz!
bixitzea emon pozik.