---
id: ab-3548
izenburua: Bonbolontena
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003548.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003548.MID
youtube: null
---

(Bizkayeraz)

Bonbolontena,
ene laztana,
ez egin lorik basuan.
Azeritxubak
eruango au
errbikuma azalakuan.
¡Lo!...

Ozti zabalan
ixarr-arrtian
argi-yagik illazkijak.
Beren izpijak
illuntzen dozak
begitxu oyen aldian.
¡Lo!...