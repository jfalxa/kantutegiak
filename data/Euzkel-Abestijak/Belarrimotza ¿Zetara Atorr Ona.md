---
id: ab-3520
izenburua: Belarrimotza ¿Zetara Atorr Ona?
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003520.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003520.MID
youtube: null
---

(Bizkayerazkua)

Belarrimotza, ¿zetara atorr ona?
atzera ua eure aberrira.

Ango lurr ederrak gogoz landu egixak,
eta jangok ondo;
ta alan i ta ni adizkide beti.

Eure aberrijan ogasunak itxitta
¿zetara etorri?
¡Ai, belarrimotz! eztaukok bururik.

Emen eztok ezer gose-biarra baño.
Ua, belarrimotz,
ua emetik eu-aberriraño.

Belarrimotza, ¿zetara...