---
id: ab-3521
izenburua: ¡Mendiyan Bai-Da Ederr!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003521.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003521.MID
youtube: null
---

(Benabarrerazkua)

¡Mendiyan bai-da ederr epherr txango-gorri!
Ene mattiak ere berrtziak iduri;
eni hitzeman eta gibelaz itzuli.

Ene bihotza duzu zuganat erori,
eta zuria aldiz harriya iduri:
ene begi gaxuak nigarrez ithurri.

(Zubererazkua)

Oreña lasterr dua horen aitziñian,
huriyan sarrtzen dvzv ahal dianian;
ez bere nahijaz bena bai bere beharrez:
zv ere hala-hala zabiltza arauez.