---
id: ab-3552
izenburua: Ortzeko Ixarr Ederrok
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003552.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003552.MID
youtube: null
---

(Bizkayeraz)

Ortzeko ixarr ederrok
¿nun zeben argija?
Neure mattetxubaren
begi zolijetan.
Eu, matte, baño
polittagorik
ezin neuk ikusi.
Ixarrik ederrena
neutzako eu beti.