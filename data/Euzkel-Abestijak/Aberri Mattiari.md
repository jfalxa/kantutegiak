---
id: ab-3512
izenburua: Aberri Mattiari
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003512.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003512.MID
youtube: null
---

(Bizkayerazkua)

Lora politta ontzijan
begiratzen dodanian,
orduban zaukot, ene mattia,
neure goguan.

Txori politta kayolan
barruratzen dabenian,
¡ai! ordubantxe zaukat, mattia,
neure goguan.

Mutikotxuba ikastolan
il-illik sarrtzen danian
zeure gomuta bixi-bixija
daukot goguan.