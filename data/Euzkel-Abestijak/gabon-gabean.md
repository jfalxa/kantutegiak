---
id: ab-3537
izenburua: Gabon-Gabean
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003537.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003537.MID
youtube: null
---

(Bizkayeraz)

Gabon-gabean ottuten dogu
gustijok apari ona:
besigu, lebatz, makallau-saltza,
bakotxak berak al-dauna.
Gero txurrun plin-plan,
ardau ta pattarra,
katubak arrtuta
zabu-zabuka.