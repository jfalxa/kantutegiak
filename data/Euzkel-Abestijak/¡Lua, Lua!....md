---
id: ab-3556
izenburua: ¡Lua, Lua!...
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003556.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003556.MID
youtube: null
---

(Biakayeraz)

Lua, lua,
seintxu laztanoi;
lua, lua
bilddurrka;
goi-goyan daukak
eure Amatxuba,
kutunoi,
eure begira.
¡Lua, lua!...

Eure lua-zain
yagon gotzonak
lasterr, bai,
eruango au;
Donokijan Ama
- onduan baño
iñun-ez
- iñun obeto.
¡Lua, lua!...