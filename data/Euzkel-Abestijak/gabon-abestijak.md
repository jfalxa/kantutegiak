---
id: ab-3509
izenburua: Gabon-Abestijak
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003509.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003509.MID
youtube: null
---

(Bizkayeraz)

¡Atorr, atorr, mutil etxera!
Gastaña zimelak jatera;
Gabon-gaba ospatuteko
Attaren ta Amaren onduan.
Ikusiko dok atta barreka,
Ama, be, poz-atsegiñez.

Eragijok, mutil,
Aurreko danbolin orri;
Gastañak erre-arrtian
Txipli, txapla, pun,
Gabon-gaba pozik
Igaro daigun.