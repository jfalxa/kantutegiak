---
id: ab-3558
izenburua: ¡Ai, Kepandon!
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003558.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003558.MID
youtube: null
---

(Bizkayeraz)

Esan nora, Kepandon,
ire alabea
otseña ixateko
bidaldu duan, ba.
¡Ai, Kepandon, Kepandon!
Eu diru-zalia;
ildda be ibilliko az
beti diru-billa.

Nora beya jatera
bakik eruaten:
umia bidaltzeko
nora begittu bez.
¡Ai, Kepandon...

Eztakijala diñok
eta euri zer ta
diruba arrtu-ezkero
poz-pozik agola.
¡Ai, Kepandon...

Diruba irabazteko
umia galdu bok
diru-barik entzungok
bein betiko orr konpon.
¡Ai, Kepandon...