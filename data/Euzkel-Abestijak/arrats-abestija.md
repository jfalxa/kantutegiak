---
id: ab-3526
izenburua: Arrats-Abestija
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003526.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003526.MID
youtube: null
---

(Bizkayerazkua)

Azkatasun-eguzkija jun yaku Euzkadi'ttik,
bere bixiko gaitz miñak gabarekin eldurik.
Illargirik ez ortzian, ezta be ixarr-izpirik,
illunperan aberrijau illunduba danetik.

(Gipuzkerazkua)

Arramaka dabiltz oro, arrtzak etz otsuak,
idukitzen ixututa Euzkadi'ko euzkuak.
Abestu ezin ba, pozki euzko gaxo-gaxuak,
orlako estutasunen naigabe, min ta itxubak.

Negarr egin negarr beti, jun danetik euzkiya,
ene gogo, biyotz estarri ta begiya.
Purrukatu errai oro; lerrtu, barren gustiya;
nekatuta dagolako, ondatuba Aberriya.

AGOTA
(Zubererazkua)

Argi-azkorrijan jiñik ene arresekilla,
beti beha, entzvn nahiz nunbattik zv-abotsa.
¿Ardijak nun vtzi tvzv? ¿zerentako errada
nigarrez ikhusten deizvt zure begi ederra.