---
id: ab-3543
izenburua: Saratarra Naizela
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003543.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003543.MID
youtube: null
---

(Laburderaz)

Saratarra naizela
orok bai-dakitte;
Senpere'n bai-dudala
txoriño bat maite.
Ura bezain pollitta
berrtze bat daite
baña nik ezpai ' nuye
ura berrtzeik maite.

(BIZKAYERAZ)

Euzkotarra nazala
orok bai-dakije,
ludijan bai-dodala
aberri bat matte.
Berau baxen politta
besteik ixan leike,
baña au ta be nik eztot
bera baxen matte.