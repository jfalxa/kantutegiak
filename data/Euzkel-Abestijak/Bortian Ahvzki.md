---
id: ab-3506
izenburua: Bortian Ahvzki
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003506.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003506.MID
youtube: null
---

(Zubereraz)
Bortian Ahvzki, hur hunak osoki;
Neskatila eyerrak han dira ageri;
Hirur bai-dirade, ¡oi! bena lerrdenik,
Beko Nabarr orotan ez dvte berrdinik

Neskatila eyerra, ¡oi begi-nabarra!,
¿Nuntik jin izan zira bortv-gain huntara?
Garazi'ko aldetik nahi-nian bezala,
Ahvzki-vthvrrilla hur hotzen harrtzera?

Goizetan ederr dizv ekhiak leinhvrv,
Lvdia argitzen dizv vngvrv-vngvrv;
Ni ere zv-unduan hala nabilazv:
Eia maite-naizvnez, ¡othoi, erradazv!

(Bizkayeraz)
Mendijan axia garbi ta ederra,
Bertan inddarrtuten da sendoro bularra.
Antxe dozu, gaste, osasun betia:
Ixan zatte ba beti mendigoxalia.