---
id: ab-3554
izenburua: Barrda Ametsa
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003554.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003554.MID
youtube: null
---

(Laburderaz)

Barrda ametsa egin dut
Ikhusirik maitia.
¡Ikhusten ezin mintza!
¿Ezteya, bada, oñaz handiya?
¡Eta ezin berrtzia!
¡Oi! nahi dut nik hiltzia.