---
id: ab-3517
izenburua: Jagi, Jagi, Euzkotarrak
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003517.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003517.MID
youtube: null
---

(Bizkayerazkua)

¡Jagi, jagi, euzkotarrak,
lasterr datorr eguna!
Sorrkaldetik agiri da
argi gozo-biguna.
Bere aurrian bilddurrtuta
igesi dua illuna.

¡Jagi, jagi, euzkotarrak,
lasterr datorr eguna!
Sorrkaldetik agiri da
JEL-eguzki ederra.
Bere argijak berotuko
dausku geure bijotza.

¡Jagi, jagi, euzkotarrak,
eta batzau gattezan!
Arrtu danok izkillubak
eta aurrera goyazan.
Geure Aberri-areyuak
gustijak il daiguzan.