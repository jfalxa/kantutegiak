---
id: ab-3492
izenburua: Attonaren Esana
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003492.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003492.MID
youtube: null
---

(Bizkayeraz)

Neure semian seme,
Neure illoba laztana,
Ezeik aiztu attonak
Irakasten dauskana.

Lenen matte-egik beti
Geure Jaungoikua,
Eta onen urrengo
Euzkadi gure Ama.

Gu euzkotarrak gozak,
Utz-utzez euzkotarrak,
Odolez garbi eta
Gogoz sendo-zinduak.

Geuk Euzkadi yuagu
Aberri bakarra;
Bera goraldutia
Dok geure biarrkuna.