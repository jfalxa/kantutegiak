---
id: ab-3497
izenburua: Gastetasunak Bai-Nerabila
kantutegia: Euzkel-Abestijak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003497.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003497.MID
youtube: null
---

(Benabarreraz)

Gastetasunak bai-nerabila aizian ainhara bezala,
Gabak igazten zaitzat ardura egunak ba'lira bezala,
¡Oi! ¡Ardura nabila maitiagana!

- Maite-nauzula zuk erraitiaz ni ez naiz alaituten;
Baizikan ere nere bihotza arras duzu alegetzen,
¡Oi! ¡Zeren zaren hain zoragarri!

- ¿Maitetasunik bai-dudala ez zerauzuya bada iduri?
Itsasua igaran niro zure gatik igeri.
¡Oi! Nire ganik zaren hain zoragarri!

- Zoragarriya ba-naiz ere ez nainteke izan zure:
Nitaz atsegin direnik berrtzerik ludiyan bai-tire;
¡Oi! Nire ganik urrun-zaite.