---
id: ab-3905
izenburua: Muxu Beltzenean
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Muxu Beltzenean,
ezteiak ziren arratsean,
emezortzi txingar puska
nere tupiaz aurrean.
Urra labiru labiru lena
nere tupiaz aurrean.