---
id: ab-3933
izenburua: Atsoa ¿Zer Diñoe?
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Atsoa ¿zer diñoe iturriok gaitik?
- Eztala geldituko uri au ur barik.
Mari Peliz ¿zelan zabiltz?
Ondo ibili, gitxi gastadu.
Doala astotxoa ta ongi algadu.

Erromerira noa, eztaukat dirurik,
balentzianatxo zarra saldu ezpadagit.
Mari Peliz...e.a.

Saldu egiten badot balentzianea
¿zer erabiliko dot euritzan, maitea?
Mari Peliz...e.a.