---
id: ab-4017
izenburua: Zaldibiko Plazan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Zaldibiko plazan
Iru atso dantzan,
Irurak ezeukela
Ardit bana poltsan.