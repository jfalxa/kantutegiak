---
id: ab-4020
izenburua: Zipitin Zipitin
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Zipitin zipitin bandera,
gure errege Franztian dagoena:
naiz betor, naiz bego,
ni aren begira eznago.
Akerrak eta idiak dantza,
astoak danbolina yo,
atariko intxaurraren azpian
kukua yotzen oi dago:
zipitin zipitin zipitin tena
zipitin zipitin zipititon.