---
id: ab-3973
izenburua: Gure Aitak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Gure aitak amari (bis)
goan gorria ekarri
Berriz ere maiteko dio (bis)
gure amak aitari.