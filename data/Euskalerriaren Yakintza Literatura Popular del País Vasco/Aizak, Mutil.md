---
id: ab-3922
izenburua: Aizak, Mutil
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aizak, mutil, yeiki adi.
Argia denez begira adi.
- Bai, nagusia; argia da;
gure oilarra yeiki da.

Aizak, mutil, yeiki adi.
Urik badenez begira adi.
- Bai, nausia: ura bada
Gure pegarra bustia da.

Aizak, mutil, yeiki adi.
Surik badenez begira adi.
- Bai, nausia: sua bada.
Gure katua beroa da.

Aizak, mutil, yeiki adi.
Uria denez begira adi.
- Bai, nausia: uria da.
Gure zakurra busti da.