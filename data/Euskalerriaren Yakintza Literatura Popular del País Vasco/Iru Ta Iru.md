---
id: ab-3980
izenburua: Iru Ta Iru
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Iru ta iru sei,
iru bederatzi,
amar emeretzi,
bat kendu ta bi egotzi,
bat ogei,
bi berrogei,
iru irurogei,
lau laurogei,
bosteun kuku.