---
id: ab-3944
izenburua: Apalazio
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Apalazio zaldunä,
iru Erregen egunä,
iru Erregeak bialdu gaitue
limosnatxo baten bila.

Zotzak eta paluäk,
Martin Antonen kontuäk;
Martin Antoni eroan dautsoz
iru oi anada katuäk.

Aren azurräk batu ezinik
¡ak erabilzan saltuäk!
Saltu ta brinko
Saltu ta brinko,
Jose Mariän aurrean
gabon gabeko intxaur-salsäa
guztiz birrindu artean.