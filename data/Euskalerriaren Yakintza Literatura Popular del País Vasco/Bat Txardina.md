---
id: ab-3953
izenburua: Bat Txardina
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Bat txardina, bi txardina,
gorexema Bazkua xina,
urzo txuriak elizara,
urzo beltxak eburneara:
xera xera xera, la Cuaresma fuera,
entre la carnal, sale la abadexal.