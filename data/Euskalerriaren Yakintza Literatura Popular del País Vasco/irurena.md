---
id: ab-3979
izenburua: Irurena
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Irurena, barurena,
gatuak xan du
gure orantza.
Izan balitz opil berantsa,
etzuen xanen gure orantza.