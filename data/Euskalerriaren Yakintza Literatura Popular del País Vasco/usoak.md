---
id: ab-4016
izenburua: Usoak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Usoak ponporron ponporronilla
elorri baten ondoan:
geure alaba ¿gura etedona
arotz-mutil bat senartzat?
- Ez, aita: ez, ama;
ori ezta nik gura dodana.

Usoak ponporron ponporronilla
elorri baten ondoan:
geure alaba ¿gura etedona
argin mutil bat senartzat?
- Ez, aita: ez, ama;
ori ezta nik gura dodana.

Usoak ponporron ponporronilla
elorri baten ondoan:
geure alaba ¿gura etedona
konfiteru bat senartzat?
- Ez, aita: ez, ama;
ori ezta nik gura dodana.

Usoak ponporron ponporronilla
elorri baten ondoan:
geure alaba ¿gura etedona
soldau mutil bat senartzat?
- Bai, aita: bai, ama;
orixe da nik gura dodana.