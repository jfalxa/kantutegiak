---
id: ab-3969
izenburua: Frantziako Bezperak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aretxetik aretxera salto,
bost txorikuma sinco,
aitearekin amearekin siete,
abidxearekin ocho (1).
Lan liraun lan liraun
lan liraun lan lan,
lenleren linlirin lan lin laun.

Mutil, ¿nun dok asto?
Artzak eta otsoak egin dsoek puskarik asko.
Nire astoa egin badsoek puskarik asko,
ik eta nik eta Maria gureak
bearko dsoagu urte barri-aurre gaizto.
Lan liraun...eta, abar.

Ene astotxu laztan laztan,
Bitoriako kakatzetan datzana.
Lan liraun...e.a.

Ene astoak egin eban ostiko,
apurtu zituan eun
da irurogeta hamabost lapiko.
Lan liraun...e.a.

Lapikoginak ekarri dau alkate,
ari bére ausi deutso buruxe.
Lan liraun...e.a.

Alkateak ekarri dau dxueza,
ari bére ausi deutso puxeza.
Lan liraun...e.a.

Asto bat ildu dsatan
Markolako etxean,
barretxu bat egin eustan
"txiko" esan neutsanean.
Lan liraun...e.a.

Asto zuri, asto gorri, asto baltz,
arratsaldean mendian topetan dira gatx.
Lan liraun lan liraun,
lan liraun lan lan lan liraun,
lan liraun, lan liraun lan lan
lenleren linlirin lin lan lon.