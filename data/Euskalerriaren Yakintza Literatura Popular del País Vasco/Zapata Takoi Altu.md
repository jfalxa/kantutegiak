---
id: ab-4018
izenburua: Zapata Takoi Altu
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Zapata takoi-altu txarolezko fiñak,
galtzeta sedazkoak dauden gareztiñak;
hamaika gona dauzka bolante ta fiñak,
ederki plantxatuta izeba Joakiñak.