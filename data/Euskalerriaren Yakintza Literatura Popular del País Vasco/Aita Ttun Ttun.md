---
id: ab-3939
izenburua: Aita Ttun Ttun
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aita ttun ttun,
ama ttun ttun,
alaba ori ere ttunttuna;
guziak ttunttun izatekotan
senarra bear luke ttunttuna.
Ez tturruputtuttun,
bai tturruputtuttun,
Allin agotak ezkontzen ttun.