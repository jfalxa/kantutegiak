---
id: ab-3984
izenburua: Joan Nintzan Errotara
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Yoan nintzan errotara,
urun piskaten eske,
txakurtzar batek
zaunka egin osten,
yaurti notsan bartzuna,
txikolaña txirikillo
morokillo laña, txikolaña.

Dran dran atea yota
itandu egin osten
dirurik baneroian,
ezetz erantzun notsen,
txakur zar batek
zaunka egin osten,
yaurti notsen bartzuna.
txikolaña txirikillo
morokillo laña, txikolaña.