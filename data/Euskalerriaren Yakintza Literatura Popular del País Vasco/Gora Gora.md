---
id: ab-3972
izenburua: Gora Gora
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Gora gora lumera San Pedro
¿Zuek, zuek nongo jendeak zerate?
- Errege Don Felipen
Seme-alabak gerade.
- Onako zubi txar ontan
pasatzen dena azken legoke.