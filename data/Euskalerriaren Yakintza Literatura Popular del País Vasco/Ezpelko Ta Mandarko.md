---
id: ab-3968
izenburua: Ezpelko Ta Mandarko
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Ezpelko ta mandarko
labe-xokoa xautzeko.
Ire gaurko lagun emana
bertzerik ezten orduko.

Ekio ta mekio (1).
¿Bizkar-ezurra auts balekio!
¿ni ikustera fan artio
senda ezpalekio!

Mandreak eder plegatrik,
ederrago barreatrik.
Ire gaurko lagun emana
urdandegian fedatrik.

Kotxu zuri pintatu
ezne-zopaz kalkatu.
Ire gaurko lagun emana
emale gaizto famatu.

Futu futu ¿zendako?
Hura ezta deustako.
Graitzupean sartu eta
kukusoen eiztatzeko.

Ilurdoian ilurri
pikatu eta erori.
Ire gaurko lagun emana
Urdan kakala erori.

Urak drama aguna,
errota onak irina.
Ire gaurko lagun emanak
pausu guzietan zirina.