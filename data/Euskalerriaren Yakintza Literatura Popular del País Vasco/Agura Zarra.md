---
id: ab-3921
izenburua: Agura Zarra
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Agura zarra baldin baneuko senartzat,
Botako neuke bentanarean kostaltzat;
inok gura ezpadau, bego neuretzat.

Emongo dautsadala apari galant bat:
sardiña buru bi ta garagar opil bat.
Axek yan gura ezpadauz, atara bagi bat.

Agura zirkin-zarkin begiok logura,
ikusten banok bere, enok ire gura.