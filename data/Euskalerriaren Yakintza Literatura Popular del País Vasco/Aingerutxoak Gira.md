---
id: ab-3938
izenburua: Aingerutxoak Gira
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aingerutxoak gira,
zerutik eldu gira,
boltsa badegu,
dirurik eztegu,
altzina yoaiteko,
urtatsa bear degu.