---
id: ab-3917
izenburua: Alostorrea
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Alostorrea bai, Alostorrea,
¡Alostorreko eskalera luzea!
Alostorrean nengoanean goruetan,
Bela beltza kua kua kua kua leioetan.