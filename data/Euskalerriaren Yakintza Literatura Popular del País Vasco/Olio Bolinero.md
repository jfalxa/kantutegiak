---
id: ab-4000
izenburua: Olio Bolinero
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Olio bolinero,
Kattalin sardinero,
zuk mermio napo,
ark ein tzuen yaka,
iru ezkutari,
mutiko dantxari,
dantzari xapuxar,
laku ta mendi xar.
Antxen bolantxen,
Mermio, Napola,
una limosnita
por el amor de Dios.