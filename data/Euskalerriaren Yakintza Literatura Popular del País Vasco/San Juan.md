---
id: ab-4003
izenburua: San Juan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

San Juan, San Juan-goizean
atsoa gindaren gainean,
agurea azpian negarrez
il eztaitean bildurrez.