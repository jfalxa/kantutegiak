---
id: ab-4010
izenburua: Txiribirika
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Txiribirika atean,
basamortu batean,
yarri belauniko,
adora Jesukristo.

¿Gure ortuan zein dabil,
azaburuak ostutzen?
Aitatxo, amatxo,
urra urra pitxitxo.