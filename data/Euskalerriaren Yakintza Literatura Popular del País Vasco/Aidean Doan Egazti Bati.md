---
id: ab-3928
izenburua: Aidean Doan Egazti Bati
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aidean doan egazti bati
begira egondu naiz egun:
ongi begira ni egonarren,
etzuen ezer ezagun;
gabiraiak oilaskoak yanta
auzoak paga dezagun.

Gabiraiak bezala badira
azeri-basakatuak,
ikusi dituztenak badira
biak lepotik lotuak;
kukurruku bat etzuen yotzen
oilar madarikatuak.

Kontzientziaz egin bear da
deklarazioa zuzen.
Ori orrelaxe baldin bada
beste (y) aldera goazen,
gabirai ori yateko ere
antzarra (y) adinbat bazen.