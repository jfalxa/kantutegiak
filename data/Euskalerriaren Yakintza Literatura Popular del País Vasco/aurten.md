---
id: ab-3924
izenburua: Aurten
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aurten gaztaña-urte da Jaunari eskerrak
aditu bearko dira kañonazo ederrak ¡rau!
Rau, rau, rau, rau eta plau ¡au dek kurutzea!
¡jaten degun guztia aize biurtzea!

Polbora orrek orain du bero en garaia,
kerik eztu botatzen baiñan bai usaia ¡rau!
... e.a.

Arriskurik eztute aurtengo ezurrak,
zeren zauritzen diran bakarrik sudurrak ¡rau!
...e.a.

Gabaz egonagatik ilun ta logiro,
begirik ezin bada tiro eta tiro ¡rau!
...e.a.

Prakak zulaturikan, atzeak librean,
disparatu diteke nai dan kalibrean ¡rau!
...e.a.

Naiz jarrita gaudela, naiz dala librean,
ederki artzen degu alkar konpasean ¡rau!
...e.a.

Danok ezpadegu ere kalibre berdina,
bakoitzak egiten du ondo alegina ¡rau!
...e.a.

Gure etxean diran arratoi-saguak,
beldurturik igesi dijoaz gaixuak ¡rau!
...e.a.

Pirrit, porrot, purrut, tirrit eta tarrat.
Kalibre desberdina baina jenero bat ¡rau!
...e.a.