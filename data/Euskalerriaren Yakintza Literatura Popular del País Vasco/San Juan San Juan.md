---
id: ab-4004
izenburua: San Juan San Juan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

San Juan, San Juan eldu da,
sorgin begia galdu da
galdua bada, galdu bedi:
sekulan agertu ezpaledi.