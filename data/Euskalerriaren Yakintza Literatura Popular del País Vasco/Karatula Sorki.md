---
id: ab-3986
izenburua: Karatula Sorki
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Karatula sorki,
ken akit orti,
ik okelea yanda
katuari zapi.
Karatula begi-gorri,
marinerua,
auntzak yan dautsala
bere burua.
Ez karatula,
bai karatula,
txarri arrandula.