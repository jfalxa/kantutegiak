---
id: ab-4013
izenburua: Urra Labiru
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Urra lábiru lábiru lábiru lábira lena,
Urra lábiru lábiru lábiru lábiru lon;
Urra lábiru lena lena, urra lábiru lena lena,
Urra lábiru lábiru lábiru lábiru lon.