---
id: ab-3989
izenburua: Keia Keia Mendira
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Keia keia mendira,
oilotxoa tokira,
mutikotxoa eskolara,
neskatotxoa dotrinara,
amandrea elizara,
aita yauna tabernara.
Euriak dakar menditi,
Nafarroaren gaineti,
amak opilla labati,
aitak ardaua tabernati.
Artu kapela zartxo bat eta
banoa erri onetati.