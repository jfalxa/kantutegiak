---
id: ab-3931
izenburua: Aizazu
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aizazu, txiki; aizazu, maite:
¿nundik bera dago Portugalete?
Portugalete vino clarete
edango neuke baso bat bete.

Baso txikia baiño aundia naiago,
egunean baiño gabean gurago.
Edango neuke, baiña eztok nik edango,
taberneriarekin asarrez nago.