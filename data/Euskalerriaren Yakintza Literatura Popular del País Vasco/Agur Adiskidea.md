---
id: ab-3920
izenburua: Agur Adiskidea
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Agur adiskidea, Yeinkoak egun on.
Zer berri de errazut zuk ¡othoi! Zuberon.
Ezagutzen duzuya Barkoxen Etxehun?
Halako koblakaririk ez omen da nehun.
Bisistaz joan nindaite ezpalitz hain urrun.

Bai, jauna, ezagutzen dut Etxehun Barkoxen,
Egunoroz nitzeyo hurrantik pasatzen.
Bethi gazte ezin izan, ari da zahartzen,
Lauhogei urte dütü munduan iragaiten.
Eta orai bertsuez ezta okupatzen.

Jauna, behar dautazu gauza bat onhetsi,
Bainan etzaitzu behar hargatik ahantzi,
Hüntaz egiten daizut gomendio aski,
Konprenituren duzu hitz laburrez naski,
Errozu ene partez milaka goraintzi.

Zure komisionea nahi düzüt egin
Konbenitzen zaitazu mila plazerekin.
¿Egon nahia zira arras Yaun harekin?
Huna Etxehun bera present da zurekin.
¿Nik eztiot bada zu nor ziren jakin?

Jauna, erranen daitzut egia halere,
zeren egin daitazun errespetuz galde.
Nor nizen erraiteko eznuzu herabe,
Aditzea badukezu, lehenago ere.
Lapurdin deitzen naute Jean Batist Otxalde.

Bihotzaren erditik egin dizit irri,
Zureganik entzunik parabola hori.
Damutzen ezpazayo zeluko Jinkoari,
Eman behar dütüzü zonbait bertsu berri,
Bai eta ver demboran eskuak algarri.

Amodioz deraitzut eskuak edatzen,
Zonbat atsegin dudan eztuzu pentsazten.
Gizon batzuek naute aski atakatzen.
Eztudala ikasi bertsuen emaiten:
Zuk laguntzen banauzu, eznaite izitzen.

Etxehun Ziberoan, Otsalde Lapurdin,
Buruzagi girade kantuak egiten
Eta enüzü beldur nehundik jin dedin:
Nik erranen diotet Eskualerrin
Ari nahi direnetz kanthore egin.

Ezkira behar sartu hortan urguluan
etxekominatu gabe gure izpirituan.
Irakurtu izan dut ehen liburuan
Jaunen jaunak direla jalgitzen munduan,
Erbia lo dagoela uste ezten lekuan.

Lehen zaharretik entzun dizit hori
Beti norbait badela denen bürüzagi.
Arren nahi badira gure kontra ari
oneski emaiteko zonbait bertsu berri,
Plazer dutenean denen zerbitzari.