---
id: ab-3950
izenburua: Bat Bi Iru Lau
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Bat bi iru lau amar hamalau,
aitak saldu nau,
amak erosi nau,
Bilboko alkate zarrak
preso sartu nau.