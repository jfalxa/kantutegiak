---
id: ab-3948
izenburua: Baratzean Aza
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Baratzean aza
- ¡Fera! Fruto, zoaza.
Baratzean porru.
Yan-ondoan dolu
- Baratzean perrezila
- Zaude isil, zaude isil,
zaude isil-isilla.