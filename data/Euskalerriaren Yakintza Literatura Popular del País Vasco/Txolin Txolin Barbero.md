---
id: ab-4011
izenburua: Txolin Txolin Barbero
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Txolin txolin barbero,
Billabonako semea txintxintxin.
Andratxo batek txintxintxin
antxe zeduzka rau rau rau rau
iru alaba pollitak txin txin txin.

Eskatu nion txintxintxin
bat bear nuela neronek rau raurau.
Errespuesta txintxintxin
eman ziraden raurraurraurrau
txaketa zarra neukala txintxintxin.