---
id: ab-3965
izenburua: Euria Dakar
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Euria dakar menditik,
Naparroaren gainetik,
aitonak babak eltzetik,
amonak opila kolkotik,
Elizakoaren tontorra:
ustetxoa, purretxoa.

Azken bi itzok alkar yoten esaten zituzten mutilak eta kantatu bage.
Beste guztiak berdin kantatzen zituzten.