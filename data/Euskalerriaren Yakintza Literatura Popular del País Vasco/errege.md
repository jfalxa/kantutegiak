---
id: ab-3964
izenburua: Errege
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Errege Don Felipen zaldunak gerade.
- Onetxek, gaztetxuok ¿zer gura etedaue?
¿Bidea libre libre gura etedaue?
Atzekoa bego ta aurrekoa betorke.

Libreak libre dira, zu ta ni katigu,
orrek eragiten deust sarritan zizpuru.
Zizpurua da neure gabeango loa,
negarra egunezko mantenimentua.

Egunez negarrez da gabean logura,