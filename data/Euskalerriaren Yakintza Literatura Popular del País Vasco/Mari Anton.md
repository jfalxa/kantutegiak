---
id: ab-3996
izenburua: Mari Anton
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Mari Anton katxalena,
txakurrak yan abe
erdia zoro izanda
ondo egin dabe.
- Ez ni, bai ni, aste guzti
guztiko, atrulon liru lon
liru liru lena.

Mari Anton katxalena
¿Bagoaz tabenara
makallau zati bategaz
zerabi edatera?
- Ez ni, bai ni..., e.a.