---
id: ab-3945
izenburua: Arratoia Arilletan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Arratoia arilletan,
sagua tanbolin yoten;
astoa ta mandoa dantzan,
zorria barrez itoten.
Txantxillon eta txantxillon,
marabe-(??)
eun dukatean andrea:
auxe da eroste merkea.