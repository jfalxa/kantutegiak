---
id: ab-3936
izenburua: Agur Agur Agur
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Agur agur agur San Andreskoak,
dantzari onak dira Musekolakoak.
Rau rau rau Sardiñak eta makallau.

Bekoki ederdunak Garagartzakoak,
astaputz batzaillak Udalakoak.
Rau rau rau Sardiñak eta makallau.

Urrezko amantaldunak Santa Agedakoak,
Belarriko ederdunak Galartzakoak,
Rau rau rau Sardinak eta makallau.