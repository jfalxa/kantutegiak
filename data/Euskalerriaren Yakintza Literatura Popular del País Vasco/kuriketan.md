---
id: ab-3990
izenburua: Kuriketan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Kuriketan txopetan
kaiku zar,
potelen zeli,
zeliko zeli,
nik eztakit zer gizon
taxu dan ori;
zankoa meia
ta lepoa lodi
¿Bazerate edo etzerate?
Kuk bad.