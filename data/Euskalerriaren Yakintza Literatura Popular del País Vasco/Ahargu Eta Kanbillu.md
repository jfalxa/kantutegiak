---
id: ab-3918
izenburua: Ahargu Eta Kanbillu
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Ahargu eta Kanbillu,
ardüra da hetan lanhu.
¿Aspaldian ebili iza
dendariaren onduan?
gantza loditzen ari ziok
¡gaixoa! Sabel onduan.

Barkoxe bürgü berriak
aizo dü Larragorria.
Amoros batek salatü dizü
kurrunkaz bere bürüa,
zeren etzen leihotik yausten
asto bürü-handia.

Amorosak behar lüke
ordü yakinez egin lo.
Bestelan harrek ükhenen dizü
bere kuntea oso:
Hemeretzü kunka olho.

Izan nüzü Oloren
olho erosten mandoen.
Hüllantxeago urrüntzëago
olhöa franko Barkoxen.
Olhöa franko Barkoxen,
hi bezalako astöen.