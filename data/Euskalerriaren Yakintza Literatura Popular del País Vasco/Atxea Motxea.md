---
id: ab-3947
izenburua: Atxea Motxea
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Atxea motxea, besteren kotxea,
zirrin zarran txurrumplet,
olea bita konkollo,
atzera begira dagonentzat
ezer bere eztago,
zidarra kanpan,
zidarra kanpan gari bat.