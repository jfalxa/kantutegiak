---
id: ab-3942
izenburua: Ala Kinkirriñera
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Ala kinkirriñera,
ala samurrera,
Joxepa plaza berriko:
jira adi beste aldera

Var. 1.
Ala kinkirriñera,
ala samurrera,
Maritxu plaza barriko,
bira adi beste aldera.

Var. 2.
Ala pinpirinera,
ala samurriñera;
Kasimira beste erriko,
bira adi beste aldera.