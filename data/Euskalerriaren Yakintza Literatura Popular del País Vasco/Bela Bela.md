---
id: ab-3954
izenburua: Bela Bela
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Bela bela, Yauna
¿Non dago aita yauna?
- Soloetan, yauna
- ¿Soloetan zer eiten?
- Artoak ereiten.
- ¿Artoak zetako?
- Oiloarentzako.
- ¿Oiloa zetako?
- Arauntza egiteko.
- ¿Arrautza zetako?
- Abadearentzako.
- ¿Abadea zetako?
- Mezea emoteko.
- ¿Mezea zetako?
- Purgatoriako arimentzako (1)

(1) Zeanurin aingerutxo biren erdian zerura yoateko esaten dabe.