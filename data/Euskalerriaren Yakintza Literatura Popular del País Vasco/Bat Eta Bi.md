---
id: ab-3952
izenburua: Bat Eta Bi
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Bat eta bi eta iru ta lau,
Txominek andrea falta dau,
Txominek dauko dendea
sagar ustelez betea,
sagar ustelak saldu daizanean
topauko dau andrea.