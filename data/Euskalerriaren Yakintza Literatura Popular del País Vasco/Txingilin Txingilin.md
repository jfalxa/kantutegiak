---
id: ab-4008
izenburua: Txingilin Txingilin
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Txingilin txingilin maña,
bihar Santa Maiña,
etzi Domun Santu,
umeak zuloan sartu,
ama negarrez,
aita dolorez,
abareak kantetan
errealen pozez.