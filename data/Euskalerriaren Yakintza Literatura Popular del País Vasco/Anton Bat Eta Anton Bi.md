---
id: ab-3943
izenburua: Anton Bat Eta Anton Bi
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Anton bat eta Anton bi,
Anton putzura erori,
edan zituen pitxar bi,
ezta geroztik egarri.
Anton kapela rron
lau gizon ta bele bat.

Anton Anton trakillua,
¿nun dek bartko liburua?
Zizilu baten gainean
damatxo baten aldean.
Anton kapela rron,
lau gizon ta bele bat.

Damatxo ¿zer dezu bularretan?
- Idiak artu nau adarretan.
- ¿Zer ordutan ta zer garaitan?
- Gabeko amabi santuetan.
- ¿Zer ibili zinan ordu artan?
Zeruko izarrak kontetan.
Anton, kapela rron,
lau gizon ta bele bat.