---
id: ab-3961
izenburua: Egun Dela
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Egun dela zanpantzart
bihar dela Maria galeta,
atso zarrak gora gora salto,
agureak ere bai;
bota bota gaztañak,
ibiltzeko ezpaiñak:
urrak eta intxaurrak.