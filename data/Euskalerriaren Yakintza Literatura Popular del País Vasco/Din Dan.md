---
id: ab-3957
izenburua: Din Dan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Din dan belaun.
¿Nor hil da?
Peru zapataria.
¿Zer egin dau? Pekatu,
auzoko txakurra urkatu.
.Ori baiño egin ezpadau,
bearko dzako parkatu.