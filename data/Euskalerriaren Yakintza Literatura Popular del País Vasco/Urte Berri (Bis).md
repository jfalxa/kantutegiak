---
id: ab-4015
izenburua: Urte Berri (Bis)
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Urte berri berri
- ¿Zer dakarrazu berri?
- Uraren gaina,
bakea ta osasuna
¡Urtats!

Eguberri xarra,
nik atorra zarra,
maukarik ez,
nik axolarik ez.
¡Urtats!

Aingiruak gara,
zerutik eldu gara,
boltsa badugu,
dirurik eztugu,
urtatsak bear tugu.
¡Urtats.