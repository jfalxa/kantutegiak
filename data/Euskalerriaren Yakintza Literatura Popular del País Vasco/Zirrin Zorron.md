---
id: ab-4021
izenburua: Zirrin Zorron
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Zirrin zorron pitxarrean
¡pontxearen otza!
¡Endayako abadearen
edateko poza.