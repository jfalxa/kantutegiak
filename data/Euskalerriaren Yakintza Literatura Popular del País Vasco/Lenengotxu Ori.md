---
id: ab-3935
izenburua: Lenengotxu Ori
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

La lara la la lalarala lalala,
laralala la lararalala.
Lenengotxu ori,
punta beatz ori,
beste guztien artean
lodia dok ori.

Bigarrentxu ori,
punta beatz ori,
beste guztien artean
bizkorra dok ori.

Irugarren ori,
punta beatz ori,
beste guztien artean
luzea dok ori.

Laugarrentzu ori,
punta beatz ori,
beste guztien artean
alperra dok ori.

Boskarrentzu ori,
punta beatz ori,
beste guztien artean
txikarra dok ori.