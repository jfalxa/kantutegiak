---
id: ab-3941
izenburua: Akherra Hor Heldu Da
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Akherra hor heldu da
arthoaren yatera.
Ekherra; ken ken ken;
akherra gure arthoan zen.

Idia hor heldu da
uraren edatera:
idiak ura, urak sua,
suak makhila, makhilak zakurra,
zakurrak otsoa,
otsoak akherra, akherrak arthoa.
Akherra ken ken ken:
akherra gure arthoan zen.