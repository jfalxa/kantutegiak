---
id: ab-3932
izenburua: Altuan
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Altuan Arantzazu, bajuan Goñati,
Neskatx eder galanten bila nabil beti.

Ederra zera, baina etzera arrosa,
kolore zuri-gorri neskatxa amorosa.