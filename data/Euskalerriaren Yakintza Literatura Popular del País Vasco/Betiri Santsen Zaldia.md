---
id: ab-3925
izenburua: Betiri Santsen Zaldia
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Betiri Santsen zaldia
bidean yoale handia,
lekayotzat duelarik miseria.
Egun guziuez gure etxerat ehakidala abia,
badakik ongi bidea,
yoan nahi dautak undar (?) bagia.

Irakurtu ditut legeak,
gehienak egi egiak,
miseriaren berriak
ta pobrezian sartuiak dira herri huntako yendeak
naiz eta ardura erriak,
bisitatuz goseak, gaiso miserableak.

Yoan den negu beltzean
zernai egin dut etxean,
ahí txorta zonbeit eiten ginuen pertzean,
aza utsak elztean,
ontzekoa (1) marxantean,
Betiri dantzan sukaldean.

(1) Urian, gantza, anabaka.