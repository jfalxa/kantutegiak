---
id: ab-3970
izenburua: Gabon Gabonzeta
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Gabon gabonzeta
gabon erreseta,
gure Yauna yaio da,
yo daigun krisketa.

Txilintxoak kalba
gure monjak kalba,
zelebratu dezagun
gaurko gabon gaba:
oilarrak eta kapoiak
pun pun pun.