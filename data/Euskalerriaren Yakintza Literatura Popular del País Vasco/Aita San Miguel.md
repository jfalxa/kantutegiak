---
id: ab-3930
izenburua: Aita San Miguel
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

l.-Aita San Miguel I-urretako
zeru altuko lorea.
Agura zarrak dantzan ikusi ta
ak egiten dok barrea.

- Garaiko plazan otea lore,
Ermuan asentzioa.
Dantzan ikasi gura dabena
Baruetara beioa.

Aita Juaniko, seme Periko
¿zer dogu aparitako?
- Orio-aza gozo gozoa
arto beroaz yateko.

Kazuelatxoan intxaur-saltsea,
txarra morkoan txunpletin,
eragin eta beti eragin,
atsoak bauke zeregin.