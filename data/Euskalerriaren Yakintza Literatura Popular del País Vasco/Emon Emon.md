---
id: ab-3962
izenburua: Emon Emon
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Emon emon Pepetxuri
txakoliña edaten.
Aurten karu dago, baiña
eztau importaduten.
Txakolin, txakolin,
txakolinak on egin.
Txako ta txako,
txako ta txako,
txakolin Antzorakoa
ezta besteak langoa;
ura dalakoan, ura dalakoan
burura igon daroa.
Txakolin, txakolin,
txakolinak on egin.