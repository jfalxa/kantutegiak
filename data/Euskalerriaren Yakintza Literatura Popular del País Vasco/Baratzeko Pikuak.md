---
id: ab-3949
izenburua: Baratzeko Pikuak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Baratzeko pikuak
iru txorten ditu,
iru txorten aietan
iru mila piku;
neska mutil-zaleak
ankak arin ditu.

Ankak arinak eta
burua arinago,
dantzan obeto daki
artajorran baiño (bis).

Artajorrara noa luerri berrira
belarra yorratuta artoa sasira (bis).