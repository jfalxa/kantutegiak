---
id: ab-3993
izenburua: Lekunberriko Ezkilak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Lapurrik andiena non dago, non dago?
Lekunberriko Antonin, an dago, an dago.
Nai badu, bego; nai badu, bego.
Aundi nai ta ezin, ezin ezin txerriik merizin.