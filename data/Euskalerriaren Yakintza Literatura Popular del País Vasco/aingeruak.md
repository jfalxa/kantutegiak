---
id: ab-3937
izenburua: Aingeruak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Aingeruak gera,
zerutik aldu gera,
orratxaren billa,
molimentarendako,
eztuenak eman nai
atari guztiak autsi ba.