---
id: ab-3940
izenburua: Akerra Burdu Burdu
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Akerra burdu burdu,
errapean llollo,
adarretan osto:
pin pin larrera.