---
id: ab-3978
izenburua: Irurean
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Irurean lantza,
laurean lantza,
gatiuak xan du
gore orantza;
izan balitz opil nero,
ezion xanen gore orantza.
Rara rara rara
rara rara rara
rara ra.