---
id: ab-3976
izenburua: Iru Andereak
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Iru andereak erretilluan eper bat.
Iru andereak ¡zer aparia zuten bart!
Iru andereak iru oillo, bi tortoillo, erretailluan eper bat.
Iru andereak ¡zer aparia zuten bart!


Iru andereak amabi karga orio,
hamaika karga txardin berri,
amar anega ogi xuri,
bederatzi karga ardo,
zortzi idi,
zazpi zezen,
sei sikiro,
bost txirrio,
lau antzara,
iru oillo,
bi tortoillo,
erretilluan eper bat.
Iru andereak
¡zer aparia zuten bart.