---
id: ab-4001
izenburua: Onek Eta Onek
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Onek eta onek
eta onek eta onek
bart eztaiak zituzten.
- ¿Onek oni zer esan?
- Konbida zitzala au ta au:
konbidatzaileak au ta au:
obetxek dira hogei ta lau.