---
id: ab-4007
izenburua: Txilin Txilin Bonba
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Txilin txilin bonba
¿serorea non da?
Kanpaiak yotzen.
Aita dolorez,
ama negarrez,
umeak txuloan
da abadeak kantuan.