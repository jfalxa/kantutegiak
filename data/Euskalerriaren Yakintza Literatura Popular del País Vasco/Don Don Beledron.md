---
id: ab-3959
izenburua: Don Don Beledron
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Don don beledron
Beledrongo elizan,
iru atso dantzan,
irurak ezdute
ardit bana poltsan.