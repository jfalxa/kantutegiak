---
id: ab-3971
izenburua: Giri Giri Giri
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Giri giri giri,
lau karga gari,
maian ogi txuri,
plazan ardo gorri,
gaur egun on,
bihar Pazkuak,
etzi ezkontzen dela
Madalen atsoa.