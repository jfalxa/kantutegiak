---
id: ab-3923
izenburua: Apezen Aurka
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Apezen aurka ginen alditxo batean,
eztutela ematen artorik zorrean.
Errenkura aundiak alkarren artean,
ezkaudela jarririk lenengo klasean.

Artoak aitu eta dirua Mejikon,
ez jakin tripatza'ok nork beteko ittigun.
Apaiz bat edo bestek len emaiten zigun.
Eta orain or konpon Dominus vobiscum.

Dominus vobiscunak esaten du klaru
gure Jaungoiko Jauna zurekin or dago.
Bego gurekin beti, gero ta naiago;
itxera utsik joanta an ezer eztago.

Erriko laborea kanpora bear du;
berdin zan guretzat arria egin balu.
¿Jornaleru gaisoak zer egin bear dugu?
Bazkari txarra eta maipetik apaldu.