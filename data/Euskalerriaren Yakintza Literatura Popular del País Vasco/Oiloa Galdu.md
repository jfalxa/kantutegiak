---
id: ab-3998
izenburua: Oiloa Galdu
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Oiloa galdu zaio
Mari Mondongori,
señaleak baditu,
bila al baledi:
buztana du nabarra
mokoa beilegi
aren arrautxatxoak
arila dirudi.