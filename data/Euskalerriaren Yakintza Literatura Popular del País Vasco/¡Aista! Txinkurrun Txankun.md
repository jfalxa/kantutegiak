---
id: ab-3929
izenburua: ¡Aista! Txinkurrun Txankun
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

¡Aista! txinkurrun txankun eznaiz eroa
sardin burua baino obe da oiloa.
La la ra la lara la
la la ra la lara la
la lara la larala
la la la la
la la la la ra la
la ra la ra la la la
lara lara la la ra
la la la ra la la ra la la
la ra la la ra la la.

Senartzat, naizelako neskatxa zurra,
gaztea nai dut eta ez zar makurra.
La la rala... Etc.

¡Aista! Txinkurrun txankun mendian elurra,
motzailearen andrea bizkar makurra.
La la rala... Etc.