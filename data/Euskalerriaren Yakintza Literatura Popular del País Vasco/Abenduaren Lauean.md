---
id: ab-3919
izenburua: Abenduaren Lauean
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Abenduaren lauean
Elokadiko mandian;
Abenduaren lauean
Elokadiko mendian
neure ardien guardian
rrai larala rai rai rai
neure ardien guardian
freskoak artu nitian.

Maria eta Katalin,
gure etxean oro berdin,
eztute nai ardiekin
rrai larala rairairai
eztute nai ardiekin
gauik pasatu aiekin.

Etxolan baitut bi manta
bat bertzea bezain xarmanta,
dena zilo ta tarrata
rrai larala rairairai
dena zilo ta tarrata


Etxean eder da beia
anka-mail adar-andia,
beterikan establea
rrai larala rairairai
beterikan establea
¡zein den eder ikustea!

Neure lagun Laureano
ezta kexatzen oraino;
orlakorik egundaino
rrai larala rairairai
orlakorik egundaino
eztet ikusi oraino.

Etxol-aitzinean xakurra,
tripa-azpian mutturra;
bainan etzaude gustura:
ataixura burura.