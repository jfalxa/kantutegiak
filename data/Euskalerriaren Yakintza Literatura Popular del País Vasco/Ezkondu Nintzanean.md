---
id: ab-3926
izenburua: Ezkondu Nintzanean
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Ezkondu nintzanean bost gona nituen
bi baituran yarrita bi saldu nituen,
soinean daukadana txito daukat zarra,
barriak egiteko esperantza txarra.

Bizi oi nintzanean ugazabanean
Txokolatea neukan gorderik koprean;
txokolatea eta alan beste asko,
gorderik oi nituen bear nebaneko.

Azak eta borrajak orioz beterik
ezin yan oi nituen nintzala bakarrik,
Ezkondu eta gero urtebete bage
porrusaldea neukan oriorik gabe.

Au da bizimodua dodana neuretzat
ezin dot oriorik porrusaldarentzat.

Seintxu bat sabelean, beste bat besoan,
gizona tabernan da artoa auzoan,
Iru ilinti ketsu suaren ondoan,
arik datorrenean irurak lepoan.

Orrako gizon ori dot neure senarra,
sarritan ekarten deust begira nagarra.
Yazo al balitzate yazo al eztana,
laster salduko neuke nik neure senarra.

Ezkondu nintzala ia dira urte bi,
eztiot kontzejurik emoten iñori.
Gazteak, entzun dozu neure kontsejua:
laster garbatu yatan niri ezkondua.