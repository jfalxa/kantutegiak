---
id: ab-4012
izenburua: Uda Barria
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Uda barria datorrenean,
neuk yakingo dot zer egin:
artu atxurra eta palea
Naparroara tellagin.
Euria dakar menditik,
Naparroako aldetik:
artu atxurra eta palèa,
banoa erri onetatik.