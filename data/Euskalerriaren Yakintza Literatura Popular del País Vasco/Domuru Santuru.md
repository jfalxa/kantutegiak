---
id: ab-3958
izenburua: Domuru Santuru
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Domuru Santuru. Santuru,
txarri andi bat il dogu,
buztana ez bestea yan dogu,
bera lapikoan daukagu.