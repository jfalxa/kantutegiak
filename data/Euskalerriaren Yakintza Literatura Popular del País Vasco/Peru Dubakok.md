---
id: ab-3927
izenburua: Peru Dubakok
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Peru dubakok uste eban
ze etzala diru-bearrik,
makallau ona ekarriteko
Bilboko Barrenkaletik.

Guztiz dotore makilla bategaz
sartu zan Barrenkalean,
txakur gorri bat oin zuriakaz
ebala aldamenean.

Sartu zanean Barrenkalean,
makallau asko ikusirik,
eskatu eban emon eiuela
lepoa bete dubarik.

O gizon tonto ¿zer esaten dok
Asto salbaje andia?
Euk bere ezeuke duban emongo
eure kortako idia.

Makallau orrek hilda dagoz
da nire idia bizirik.
Usteldu eta botau eztaizun
gura nituke dubarik.

Ene gizona: gura badozu
zeure lepoko kargea,
hamalau lauko kostako dsatzu
makallau onen librea.

Toto pintotxo, goazen etxera
ainbat lasterren emendik.
Ezkadsaukazak beingo beingoan
Barrenkaleak arturik.