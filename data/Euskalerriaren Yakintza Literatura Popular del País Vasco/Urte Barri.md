---
id: ab-4014
izenburua: Urte Barri
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Urte barri barri,
txarri-belarri,
daukanak eztaukanari,
intxaurtxu bi,
iru gaztaiña,
Mari montaña,
atsoak dauko
uzkar-usaiña.