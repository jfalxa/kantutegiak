---
id: ab-3985
izenburua: Kandelera (An), Kandelerio (B)
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Kandelera lera lera,
bost ilabete larrainera,
ogirik eztuenarendako
sei ere badirela.

Kandelera lera lera,
bortz ilabete larrainera,
sei ere bai Aezkoakora.

Kandelerio lerio lerio,
atxari ura dario,
makatzari madari,
eutsi, Peru, ankeari.