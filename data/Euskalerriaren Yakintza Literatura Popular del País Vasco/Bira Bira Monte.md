---
id: ab-3956
izenburua: Bira Bira Monte
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Bira bira monte (sic),
zozo balkate,
Ibarrangeluan alkate.
Bilbon errejidore.
geure auzoko Petratxu bére,
atzera bira leiteke.