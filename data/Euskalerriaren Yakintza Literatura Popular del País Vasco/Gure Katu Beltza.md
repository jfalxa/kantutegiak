---
id: ab-3974
izenburua: Gure Katu Beltza
kantutegia: Euskalerriaren Yakintza Literatura Popular Del País Vasco
partitura: null
midi: null
youtube: null
---

Gure katu beltza
gauaz ezta lotsa,
gauaz ibili eta
egunaz lo datza.

Aitak alabari
"aurra, yaiki adi,
lukainka saeskia
katuak yanen din".

Sartzen da leiotik,
ateratzen da atetik;
aita zarrak oetik
katu beltza atera dik.