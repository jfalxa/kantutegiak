---
id: ab-4821
izenburua: Iru Erregue Orienteko
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004821.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004821.MID
youtube: null
---

Iru erregue Orienteko
Gazpar Melchor ta Baltazar
Ayek irurak
Omen cekiten
Trinidadea nola zan
Trinidadea Trinidadea
Birgiña amaren semea
Urasen da gucion erregua.

Iru erregue Orientetik
Jesus Onaren bidera
Izar eder bat aguerturikan
Bidea erakustera
erakustera erakustera
Izarrarekin ziran guiatu
zuten artean
Jesus billatu.

Mundu guziko Egille ona
Biurturik aur umilla
Ikusi nairik Errege ayek
Joan ziraden bere billa
Zer atzegiña zuten naibaitu,
Zutenian Jaun ona arkitu.

Belengo estalpe aurrian ziran
Makurturikan geratu,
Biyotzez anchen umilki zuten
Jesus aurra adoratu.
Zer zori-ona beren barrenen
Ykusirikan Jesus Belenen.

Estalpe artan ikusirikan
Jaun zerukoa jarria,
Bertan eskeñi zizkaten pozez
Intsentsu, mirra ta urria.
Jaun zerukoa esagutzera,
Joan ziran belaunikatutzera.

Ama Birjiña eta San Jose
Soraturikan an daude
Begira nola dan an ageri
Errege denen Errege.
Mundu guziyen Jaun ta jabia
Pekatariyen salbatzallia.