---
id: ab-4729
izenburua: Maitagarriari
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004729.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004729.MID
youtube: null
---

Ditut itz bacar batzuec
chit maitededan batentzat
noa cantatzen abiatzera
salatugabe norentzat.
Ecin entendilitzaque bestec
nayacgatican beretzat
cergatic aren igualic iñun
danic iruritzen etzat.

Ditu oiñ chiqui pollitac eta
Guztiz ariña pausua,
Lurrian eztu utzitzen bere
Oñaren marca osua;
Meriñaquia eramaten du
Biatz birequin jasua,
Bide batian dijuanian
Diruri dala usua.

Guerriya mia gorpus liraña
Alturan ona tamaña,
Jantzitzen berriz beste senbaitec
Cañan obia du maña;
Danic arropa merquienaquiñ
Ibillitzenda apaña,
Eztute sedas jasten diranac
Ere lusitzen are aña.

Arpeguiya du soragarriya
Jaunac milagros eguiña,
Gorris arrosa dirudiyena
Ta suriz berriz iriña;
Buruban ille trensatu beltza
Ugariya ta chit fiña,
Itzican arqui ecin liteque
Fama emateco diña.

Arren onduan ederrac oso
Ematen dute itzusi
Nere aurrian alacorican
Ez det oraindic icusi;
Eman al fama bañan gueyago
Luque ascosaz meresi
Saltzeco gauza balitz ez luque
Faltaco ceñec erosi.

Ecertan eztu fantesirican
Eta chit da cariñosa,
Arren itz batec sartutzen diyo
Triste dagonari posa;
Cantuban berriz gustagarriyac
Ditu graciya ta bosa,
Iñoiz aitzen det eta orduan
Pil pil jartzen sait biotza.

Eciñ contatu litequen ainbat
Neri gustatzen sait ura,
Nayago nuque arrequin bici
Ez joan bacarric cerura;
Bera andican egan jachiya
Dala diruri mundura,
Nigandic arren pentsamenturic
Seculan ecin quenduda.

Cuidadoaren falta liteque
Alaco gauza bat aja,
Diamantiac bañan gueyago
Baliyo duben alhaja;
Arc bearluque gordeta egon
Dedin cristalesco caja,
Iñor ibilli eta ez dezan
Ichura galduta laja.