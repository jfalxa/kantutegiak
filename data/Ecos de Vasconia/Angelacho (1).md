---
id: ab-4772
izenburua: Angelacho (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004772.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004772.MID
youtube: null
---

Aiñ asko eta ondo
zutzaz det aditzen
zeñaz ikusi gabe
zaitut esagutzen.
Ama gandik zera
birtutez azaltzen
Aita gandik sentsuak
dituzu ornitzen.

Nik badakit zerala
ona ta polita
oso chinchua eta
echeko pozkida
eskondutzen bazera
ta aingerurik bada
erakutzi kantatzen
Gernikako arbola.

Mundu ontan pakiak
Poza du berekin
Zure echian poza
Ala da zurekin
Zure ontasun eta
Chinchotasunakin
Bizi liteke ando
Gozotasunakin.

Angela nola zeran
Aingerua izan!
Zori onez izena
Jarriya elizan
Gañera izanikan
Aingeruen gizan
Argatik arki zera
Orrelako dichan.

Izan zaite luzaro
Zu mundu onetan
Birtute ta Fedea
Dezula zañetan
Au esaten dizut nik
Biyotzez benetan
Pozezko borondate
Ta asmo on denetan.

Izar guztiz argiya
Echeko pozkida,
Zure aldia oso
Ondo ageri da,
Aingerua echera
Goitik etorri da
Beti orla egotia
Guztiz komeni da.