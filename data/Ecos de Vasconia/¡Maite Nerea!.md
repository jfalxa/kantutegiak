---
id: ab-4810
izenburua: ¡Maite Nerea!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004810.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004810.MID
youtube: null
---

Amoriyozko izar ederra
Maite zu zera,
Chimparta sartu dirazu gaur
¡Bay! Nere biyotzera;
¡Arren saya zaite gar au
Askar itzaltzera!

Aingerucho soragarriya
Parerik gabia,
Zure amoriyua daukat
Nik barrenen gordia;
Zu zera nere biyotz
Gashuaren jabia.

Zure begiyen dizdizarak
Choratzen naute ni,
Izar oyek biyotz nerean
Egiten dute argi;
Maitatzen banazu beti
Izango naiz ongi.

Arratz ta egun zurekin beti
Nago pentsatzen,
Zu ikusi utsarekin naiz
Osoro choratutzen;
Egun bat aukeratu ta
Ezkondu gaitezen.