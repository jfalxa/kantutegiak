---
id: ab-4733
izenburua: Cantari Euskalduna
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004733.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004733.MID
youtube: null
---

Guitarra zarcho bat da
Nerezat laguna
Onela ibilcenda
Artista Euskalduna.
Egun batean pobre
Beste batez Jauna
Cantari pasatcendet
Nic beti eguna.

Naiz dala Italia
Oro bat Francia
Bita billatu det
Anitz malicia
Icusten badet ere
Nic mundu gucia
Beti maitatuco det
Euscal-Erria!

Jaunac ematen badit
Neri osasuna
Izango det oraindit
Andregai bat ona;
Emen badet francesa
Interesa duna
Baña nic naiago det
Utsic Euskalduna.

Agur Euskal-Erria
Baña ez betico
Bost edo sei urtean
Ez det icusico...
Jaunari escatcen det
Gracia emateco
Nere lur maite ontan
Bizia uzteco.