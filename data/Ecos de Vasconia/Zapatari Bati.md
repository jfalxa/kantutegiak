---
id: ab-4786
izenburua: Zapatari Bati
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004786.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004786.MID
youtube: null
---

Bertso berri batzubek
Anton Iskíñentzat,
chit komeni dirala
iruritutzen zat.
Berak badaki jartzen
beste iñorentzat
ez da milagro bestek
jartzía beretzat. (bis)

Bere ofiziyua
zapatagillia
lan fiñian badauka
abillidadia,
bañan lanerako da
gogorik gabia;
pena artzeko da orren
enfermedadia.

Lana egiñ bañan len
kobratu lenbizi,
diru oyek artu ta
tabernara igasi;
ayek gastatu arte
zeñek echerazi,
bizkarra puzkatzia
ez aldu merezi.

Gozaltzen bear ditu
sei o zazpi baso,
oyek eran artian
zeñek echeraso;
orrenbeste eran ta
tripík eziñ jaso,
orla segitzen badu
galduko da oso.

Sei baso atera ta
sei tragotan ustu,
beste sei erateko
iñor bear eztu;
eran corriente bañan
pagatzia aztu
orla tabernariya
nola aberastu.

Bete ogeigarrena
tabernariyari,
ez dizut nik emango
sagardorik zuri;
ez banazu pagatzen
zor nazuna neri
parte emango diyot
zeladoriari.

Etzayozula eman
parterik iñori
astian pagatuko det
pichar bat edo bi;
ez bazera konforme
ajolik ez neri,
gogua badet eta
naikua da ori.

Ai! Ajolik gabeko
lotzik ezdezuna,
zuregatik galdu det
iya osasuna;
ez banazu pagatzen
azkar zor nazuna
boina kenduko dizut
buruan dezuna.

Zazpi karga sagardo
zor nazkitzu iya;
Barkatu bear nazu
ez da ori egiya;
zedorrek bezin ona
daukat memoriya
geyenaz iru karga
dizut zor utziya.

Berriz orlakorikan
esaten banazu,
arditik neregandik
artuko ez dezu;
neronek aña kulpa
zedorrek badezu
ni zeiñ naizen jakin ta
zertan eman nazu.

Orren bizi modua
Donostiko erriyan,
beti zurrut egiten
sagardotegiyan;
broman eta kantuan
zelebrekeriyan
andikan ateratzen
ezda egun guziyan.

Ez da apuratuko
Iskiña ezertan,
besteren bizkarretik
dabill geyenetan;
tabernariyarekiñ
beti erriyetan
egunen batian or
ilko dute bertan.