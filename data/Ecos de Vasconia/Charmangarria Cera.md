---
id: ab-4768
izenburua: Charmangarria Cera
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004768.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004768.MID
youtube: null
---

Charmangarria cera
eder eta garze
Nere biotzak esduzu
besterik maite.
Beste cembait becela
Banegoque libre
Zurekin ezcontzea
Dudarik ez nuke.

Maitea parti parti
Plazer dezunean
Iñorc icusi gabe
Iluna barrean
Lagun bat emango dizut
Joateko bidean
Arec jarriko zaitu
Tranquil biotzean.

Maitea churi zera
Elurra bezela
Bai eta mintzo zera
Profeta antzera
Orain dainocoan ere
Badira zobera
Baldin placer badezu
Atozquit aldera.

Jaunac zer mintzo zera
Guiza ortan neri,
Zeren zure erropari
Etzayo comeni;
Banidade ta penac
Ikututzen nau ni
Agur esan zezake
Nere erriyari.

Munduco gora berac
Dirala meriyo,
Biotzean zortzen da
Zenbat amoriyo;
Ez liteke gueyegi
Itz onetaz fiyo,
Len pozez zana jarri
Liteke seriyo.