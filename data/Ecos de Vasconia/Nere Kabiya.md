---
id: ab-4820
izenburua: Nere Kabiya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004820.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004820.MID
youtube: null
---

¿Nere erri polita
Nola ez aitatu,
Chikitandik ainbeste
Detana maitatu?
Eztu choriyak ere
Detanez aditu,
Leku goshuagorik
Oraindik arkitu.

Zuk badakizu erri
Chukun ta leyala,
Zure chokuaz aztu
Ez nitekiala;
Eskatzen diyot Jesus
Maitiari ala,
Jayo nintzan kabiyan
Ill nai nukiala.

Joaten dan bezelashe
Choriya garira,
Joan nai nuke nik nere
Kabicho chikira;
Ill baña lenchiago
Kupitzen badira
Eraman nazatela
Nere Oñatira.