---
id: ab-4796
izenburua: Orma-Chulo Batetik
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004796.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004796.MID
youtube: null
---

Ormachulo batetik
gau batean iguesi,
arratoi beltz aundi bat
nuben nik ikusi.
Zijoan pizkorki.
Nora nai saltatzeko
etzegoen gaizki,
bañan atzaparra zion
katu batek ezarri.
Umildu zan zarri.

Katuak eltzen ziyon
atzaparrarekin,
azkazala zorrotzak
zituben berekiñ;
ematen ziyon miñ;
Arratoyak naiko lan
bazeukan berekiñ
karrashika ari zan
oñaziarekiñ,
ta ezin igas egiñ.

Katuak nola zeukan
naikua indarra,
etzuben askatu nai
bere atzaparra;
¡naiz egiñ negarra!
Arratoyak alperrik
zuben deadarra,
ikusten zuben bere
sententzi gogorra,
anchen ill bearra.

¡Gau erdiyan goshoro
bat dabillenian,
zepuan erortzen da
nai ez dubenian,
geyen geyenian;
Bera ganatzen badu
garai ez danian
atzaparrak an ditu
beraren gañian,
azken azkenian.