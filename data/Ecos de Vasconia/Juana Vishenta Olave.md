---
id: ab-4736
izenburua: Juana Vishenta Olave
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004736.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004736.MID
youtube: null
---

Juana, Juana Vishenta Vishenta
Juan Vishenta Olave.

Juana Vishenta:
Nagusi jauna, aushen da lana
amak bigaltzen nau berorrengana:
Nagusi jauna:
Ay bigalduko bacinduque maiz,
zu ikusi ta konsolatzen naiz,
chinista zazu;
¡oso zoraturikan nakazu!!
orren polita nola zera zu?

J.Vish:
Amak esan dit, echeko errenta,
pagatzeko dirurik ez zuben ta:
ia eguingo diyon mesere,
illabetian gueyenaz ere,
ichogotia;
bara cierto da la ematia,
pasa baño len illabetia.

N.jauna:
Logratutzia, erresh alare,
naiz ori ta beste cer nai gauzare:
seguro da la esan amari,
zu baldiñ bazatoz mandatari;
dudarik gabe;
nere borondatiaren jabe
zu cera Juana Vishenta Olave.

J.Vish:
Beraz bertatik, nua echera,
poz aundi bat amari ematera:
oraiñ ariyo nagusi jauna,
presaka juan biat amagana.
N.jauna:
Ez zuacela;
portatu zaite nik nai becela,
gaur echera juan ez zaitecela.

N.jauna:
Zuben echia, dago irruti,
birian cer guerta izan bildurti:
bara iya pasa da eguna,
arratsa berriz dator illuna,
oraiñ juatia;
chartzat iruqui nere maitia,
gaur emen bertan guera zaitia.

Ez juan echera, bire oyetan,
gabaz, bakarrik eta illumbetan;
kontura erori zaitia zu,
gabaz illumbian juan biazu,
peligro aundiz
eta obe dezu eun aldiz,
biyar goician juan egun arguiz.

J. Vish:
Ecetz etsiya, egon liteque,
ni emen guelditu eciñ niteque:
amak espero nau gaberako,
eta ark espero naubelako,
juan biar det nik;
alaba ikusi baguetanik,
amak izango ez luque onik.

N.jauna:
Emen zuretzat, dira parako,
jaqui chit goshuak afaitarako:
erariya berriz jaungoikoa,
illak piztutzeko modukua,
ardo onduba;
ontzen urtietan egonduba,
Malagatikan da bigalduba.

J.Vish:
Jauna berorrek, bañan alferrik,
badaqui armatzen lazo ederrik;
lazua eiz bague guera dediñ,
nere echian nai ditut eguiñ,
afaldu ta lo;
nayago ditut ango bi talo,
ta ez emengo milla erregalo.

N.jauna:
Biyotz nereko, dama polita,
asmoz munda zaite aushen tori ta:
amasei duroko urre fiña,
sendatu nai zazun nere miña.
J.Vish:
Ez jauna, ez, ez;
mereci beciñ kopeta beltzez,
esango diyot ecetz ta ecetz.

N.jauna:
Gañera berriz, Juana Vishenta,
utziko dizutet echeko errenta...
quito zor dirazuten guciya,
kumplitzen banazu kuticiya.
J.Vish:
¡Lotsa gogorra!!
sufritzen dago gaur nere honra,
¡penaz malkuak darizquit orra!!

N.jauna:
Nik ditut kulpak, ez eguiñ negar,
orlakorik ez nizun esan biar:
animan sentitutzen det miña
zuri ofensa ori eguiña,
maldiziyua;
ez daquit nun nekan juiziyua,
eskatzen dizut barkaziyua.

J.Vish:
Barkaziyua, du ta eskatu,
nere biyotz onak eciñ ukatu:
erakutzi ditalako jauna,
gaitz eguiñaren damutasuna,
konforme nago;
ez eguiñ ta ofensa gueyago,
gaurkua oso astutzat dago.

N.jauna:
¿Berriz nik eguiñ zuri ofensa?
¡arren orlakorik ez bara pensa!!
Zaukazquit neskacha fiñ batetzat,
eta gusto aundiz emaztetzat,
artuko zaitut;
biyotzetikan esaten dizut,
zuretzat ona baderizquizut.

J.Vish:
Ori eguiyaz, baldiñ badiyo,
neregana egon liteque fiyo:
bañan usatubaz kortesiya,
ama neriari licenciya,
eska bezayo.
N.jauna:
Ori gustora eguingo zayo,
biyar goician juango naitzayo.

J.Vish:
Banua oraiñ.
N.J:
Atoz onuntza,
birerako artu zazu laguntza:
adi zazu, morroy Joshé Juaquiñ,
echera farolan arguiyaquiñ,
aingueru oni,
gaur zu laguntzia da komeni,
quezquetan egon ez nariyen ni.

Morroya:
Aingueru ari, lagundu eta,
orra jauna eguiñ ostera buelta:
birian bai bildurcho zan bera,
bañan allegatu ta echera,
kontentu cegon;
oraiñ quezquetan ez du cer egon,
oyera juan ta pasatu gau on.

Eldu zanian, urrengo goiza,
nagusiyak zuben cumplitu itza:
juan citzayon amari echera,
andretzat alaba eskatzera,
amak chit firme:
artuta guero milla informe,
guelditu ciran biyak konforme.

Andik zortzi bat, egunetara,
edo amar ciran ez daquit bara:
ez amar eciñ kitezquen izan,
andik zortzi egunera elizan,
ezkondu ciran;
ezkondu ta echera seguiran,
gendia franko bazuten giran.

Bost illabete, edo sei ontan,
ez dirade beti egondu lotan:
eman dute cembait girabira,
eta gozatutzen bici dira,
dichak ausarqui;
obeto eciñ litezque arqui,
espero dute aurcho bat aurqui.

Dama gaztiak, ez egon lotan,
beguira zaitezte ispillu ontan:
gustatu ta virtute ederra,
andretzat artu bere maisterra,
du nagusiyak;
orla irabastera graziyak,
saya zaiteste beste guziyak.