---
id: ab-4808
izenburua: Arantza Zorrotza
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004808.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004808.MID
youtube: null
---

Amodioa bai da
Arantza zorrotza.
Ceñec zulatzen duen
Portizqui biotza,
Eritu baguetanic
Ez liteque goza
Beragandic sarritan
Jayo oidan poza.

Naitasun gueyeguia
Artzea iñori
Cein gauza gaiztoa dan
Progatzen det ongui,
Atzecabez beteric
Gau ta egun beti
Ezur utz biurturic
Arquitutzen naiz ni.

Nere maite polita
Zutzaz oroitzean
Odola pill pill dabill
Nere biotzean,
Campora irten nairic
Aleguin gucian
Min char au bearco zait
Sendatu illtzean.

Itzuca nabil beti
Consuello billa
Arquitzen zait dalaco
Biotza chit ílla,
Alperric izango da
Nere aleguiña
Sendatu ez det uste
Daducaran miña.

Icusiric galaya
Zure etzimena
Nere biotz gaishoac
Artutzen du pena,
Zu sendatzeco nerau
Nintzaque aurrena
Banequique seguru
Menetan zaudela.

Biotzean jayota
Jarriric beguian
Nere amodioa
Dago aguidian,
Egunaz bezalaishen
Gauaren erdian
Neure maite maitea
Zauzcat memorian.

Galay gaztea guztiz
Leunqui mintzocera
Bañan bide anitz da
Mitic biotzera,
Sinistmenic ain erraz
Ez noa artzera,
Lore aurreratuac
Oi datoz galtzera.

Goiz datorren lorea
Berez bada piña
Irme ichiquitzeco
Izango du griña,
Chertu onetic gogoz
Badator eguiña
Aren alea ez da
Izango samiña.

Ale gozoac ere
Eldu baguetanic
Gogoz jan eta gaitz eguin
Icusten ditut nic.
Dala erre egosi
Orobat gordiñíc
Alaco janariac
Ez dezaque onic.

Ez nuque iduquico
Biotza ain triste
Naitasun eltzaquea
Baldin banizuque,
Maitatzen zaiturala
Badira lau urte,
Zeurea eznazula
Ez dezu cer uste.

Galai gaztea ez da
Cer esan gueyago,
Damu det ez jaquiña
Gauza au lenago,
Atzecaberic asco
Dezula igaro
Nere biotz gaishoa
Penaturic dago.

Dama zuregan dago
Nai dezuna eguin
Nere gogoco berri
Badaquizu berdin,
Ni penatuaz artzen
Badezu atzeguin
Lembait len ta beingoan
Obeco nazu ill.

Ez det nai zu illtzeric
Ain guchi penatu
Gogoratzea ere
Litzaque becatu,
Galaya ez dezu cer
Gueyago necatu
Zurea nazu betico
Nai banazu artu.