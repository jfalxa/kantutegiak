---
id: ab-4806
izenburua: Glu Glu Glu
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004806.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004806.MID
youtube: null
---

Viva Rioja
Viva Naparra
Arkume onaren iztarra,
Emen guztiok anayak gera
Ustu dezagun picharra
Glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu

Ardo fiña ta jateko ona
Jartzen badute gaur ugari,
Gure barrenak berdindurikan
Jarriko dira guri guri.
Glu glu glu...

Gure sabela bete bear da
Albada gauza onarekiñ,
Dagon lekutik ¡eragiñ bapo!
¡Aupa mutillak! gogoz ekiñ.
Glu glu glu...

Ez ikaratu dagon artean
Jan eta edan gaur gogotik,
Ustutzen bada ekarko degu
Berriro lengoko tokitik
Glu glu glu...

Umoria da gauzik onena
Nai gabeak ditu astutzen,
Uju ta aja asi gaitean
Euskal doñuak kantatutzen.
Glu glu glu...