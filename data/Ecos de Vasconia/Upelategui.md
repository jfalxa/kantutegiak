---
id: ab-4794
izenburua: Upelategui
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004794.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004794.MID
youtube: null
---

Edan ezak goizian
churitik, onetik
arratzaldean gorritik, obetik.
Ala eguiten diat nik,
eta ik eguik,
albadaguik on eguingodik.

Nai diat neretzako
beti erariya,
goiz aldean zuriya,
argiya;
gerora berriz gorriya;
oso garbiya;
neretzako on dek guziya.

Erari onaz niok
ni beti pentsatzen,
penak zaizkidak astutzen,
eraten;
beti umorez nak jartzen,
eta kantatzen,
askotan ere bai dantzatzen.

Pozez nebillek beti
erariyarekiñ,
chinparta sagarduakiñ,
onakin;
oraiñchen banu nerekin,
jakiyarekin,
erango nikek gogoarekin.