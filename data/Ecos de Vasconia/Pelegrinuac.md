---
id: ab-4763
izenburua: Pelegrinuac
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004763.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004763.MID
youtube: null
---

Pelegrinuac datoz
Santiagotican
atea iriqui besa
icustiagatican.
Chomin jozac trompeta
Pachi nondec conqueta
berdiña baldin baciagoc
ecarri beteta.

Miseriz iya illac gu
Irichi gerade,
Beon laguntzen bear
Atietan gaude;
Chomin jozac... Etc.

Amalau illabete
Birian oñ utzic
Ez dacargu gurequin
Miseriya baicic
Chomin jozac... etc.

Erruquituco alditu
Gaur gure antziyac
Campo ta barren gatoz
Biciro autziyac
Chomin jozac... Etc.

Zori oneco eche
Billatu deguna
Jaincoac eman deyela
Denai osasuna
Chomin jozac... etc.

Agur echeco denac
Eta ondo bizi
Aurrera guaz geren
Traste zar ta guzi
Chomin jozac... etc.