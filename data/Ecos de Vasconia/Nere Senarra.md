---
id: ab-4756
izenburua: Nere Senarra
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004756.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004756.MID
youtube: null
---

Esaten dute bada
ezcondu ezcondu
Etzait neri seculan
Gogoric berotu :
Ah! cer zoriorduco
ezcondu bearra
Gueroztic eguitendet
Sarritan negarra.

Bigar da urte bete
Ezcondu nitzala,
Egun bat guichiago
Zamutu zaidala;
Nere bodaco eguna
Pasatu ezquero
Egun bat cerdan ona
Ez daquit arrezquero.

Plazan, eder ta galan
Ezcondu artean,
Ni ere ala nitzan
Dembora batean;
Cembait icusten ditut
Ezcondu ta guero
Esanaz dabiltzala
Ay! libre banengo.

Ay! au bici modua
Sortu da neretzat,
Ez daucat olioric
Porru saldarentzat;
Azac eta borrajac
Nitzanean bacarric
Beti jaten nituen
Olioz beteric.

Araco guizon ori
Da nere senarra,
Ascotan ecartzen dit
Beguira negarra;
Posible baldin balitz
Imposible dana
Pozic utzico nuque
Nic nere senarra.

Nere senarrac ez du
Beste equintzaric
Tabernara joan eta
Orditu besteric;
Echera etortzeco
Ay! nere bildurra
Noiz icusico detan
Bizcarrean egurra.

Ezcondu ezquerostic
Musua garbitzen
Iturrico urican
Ez det nic gastatzen;
Beguietatic bera
Datorren negarra
Musua garbitzeco
Bastante da bera.

Ezcondu nitzanean
Bost gona nituen,
Bi prenda jarri eta
Bi saldu nituen
Soñean dauca dana
Guciz daucat zarra,
Berriac eguiteco
Esperantza charra.