---
id: ab-4787
izenburua: Mando Baten Gañian Bestia
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004787.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004787.MID
youtube: null
---

Mando baten gañian
Domingo Campaña
etzijuak utzirik
manduaren gaña.
Azpiyan dijuana
mandua dek baña
gañekua ere badek
azpikua aña
mando baten gañian
bestia alajaña.

Indalecio Indalecio
Indalecio moko
uste aldek uste aldek
ezautela joko.

Berriz esaten banak
Indalecio moko
ezurrak autziarte
ez diyat lajako.