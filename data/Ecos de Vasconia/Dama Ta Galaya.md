---
id: ab-4739
izenburua: Dama Ta Galaya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004739.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004739.MID
youtube: null
---

Galayac:
Dezu gorputz egoquiya
polita chit arpeguiya,
eta ederra beguiya:
zure beguiya,
aguitz arguiya,
da guztiyetan aurrena;
bestiac dira urrena,
ori da ta ederrena,
diruri eperrarena.

Ez det dama begui eder,
desiatzen beste eser,
bañan bay...esango det ser:
leguezco birez,
faltziyaquiñ ez,
zu neretzaco logratu;
nic orla naya obratu,
chancha dala ez pentsatu,
zutzaz naiz enamoratu.

Conformatutzen bacera,
laster ezconduco guera,
ez galdu orlaco era:
biyoc munduban,
bici moduban,
artutzen badegu parte;
asnasiac iraun arte,
jaquiñian guera zaite,
nic izango zaitut maite.

Millaca ditut ardiyac,
laroguey ta lau ariyac,
mardulac eta guriyac:
cecen eta bey,
berréun ta oguey,
sey éun eta bost iriyac;
baitare soro aundiyac,
belar ugariz jantziyac,
mantendutzeco guciyac.

Nic ori guztiya daucat
ez balitz bezela deus bat,
badet eta beste gauz bat:
guilzaz aspiyan,
lecu ichiyan,
dacat montuan billbilla;
urrezco cembait curpilla,
urashen bai dala pilla,
ontzaco utsac lau milla.

Dauzcatanaren guiltzaz be,
zu eguiñgo zaitut jabe:
baceudeque duda gabe:
bara beguira,
izango dira,
orra eman noticiyac;
zure contura utsiyac,
dituzu ta mereciyac,
echeco guiltza guciyac.

Ser ongui bicico ceran,
nay dezuna jan ta eran,
guztiya zure auqueran:
jazteco ere,
zuc ez batere,
gastubari beguiratu;
dendic onen-etan sartu
gayic ederrenac artu,
eta soñac encargatu.

Alaja zale bacera,
juango guera esatera,
alajategui batera:
eracusteco,
zuc icusteco,
dacaten alajeriya;
naiz badare gariztiya,
izango da erosiya,
gustatzen zaizun guciya.

Zu servitzeco nic ortan,
gauz oyec erriya ontan,
ez daude denda iñontan:
ta erostera,
erri bestera
eguiñgo degu bay gira;
nay badezu Donostira,
edo bestela Francira,
ango zuc naizun errira.

Esaten dizquitzutanac,
dira ta ciñez esanac,
cumplituco ditut danac:
fiyatu zaite
nic zaitut maite,
eta jaquiñ zazu ecic;
zu gatic ez dala casic,
eguiñgo ez nuquen gaucic,
gusto aundira ta pocic.

Zutzaz enamoratuba,
nago cacican artuba,
illaren erretratuba:
miñic charrena,
dacat barrena,
animaraño sartuba;
au legoque sendatuba,
gauza bat desiatuba
nic baneca logratuba.

Sendaco litzaiquet miña,
bestela senda eciña,
zu neretzaco baciña:
azquen ortara
ni nua bara,
guztiz intentziyo onez;
¡ecin liteque ta obez,
arren esazu favorez,
neretsat ceran edo ez!!

Damac:
Dirazu naitazun fiña,
gozua ta atseguiña;
obiagoric eciña:
jaquiñ naizuna
da erantzuna
esatiaquiñ onela;
alashen banitz becela,
cierto egon zaitecela,
zuria izango naicela.

Au nic diyot:
Ara gauzac nola diran,
egun artatic seguiran,
biyac alcarrenac ciran:
ordu ezquero,
au zan espero,
cumplitu ta berac naya;
ongui daudela da baya,
pasiaz bici alaya,
gure dama ta galaya.

Eguiya nic esateco,
ez cebilltzan ezcontzeco,
gueldi gueldi egoteco:
galayac obra,
seguru sobra,
aurreratuba darama;
eguiñ dirala da fama,
bat aita, bestia ama,
gure galaya ta dama.