---
id: ab-4728
izenburua: Egun Batian Loyolan
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004728.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004728.MID
youtube: null
---

Bein batian Loyolan
Erromeriya zan
Anchen icusi nuben
Nescacha bat plazan
Choriya bañan ere
Ariñago dantzan
Urashen bai polita
An politic bazan. (bis)

Esan niyon desio
Senti nuben guizan,
Arequin izqueta bat
Nai nubela izan:
Erantzun ciran ecic
Atzeguin arnezan
Adituco cirala
Cer nai niyon esan.

Arquitu giñanian
Iñor gabe giran
Coloriac gorritu
Arasi cizquiran
Contatuco dizutet
Guztiya seguiran
Cer esan niyon eta
Nola erantzun ciran.

Dama polita cera
Polita guztiz ¡ay!
Bañan alare saude
Oraindic escongay
¿Escon gaitecen biyoc?
¿Esan zadazu bay?
- ¿Ni zurequiñ ezcondú?
¿Ni zurequiñ? ¡Ja-Jay.