---
id: ab-4809
izenburua: Nere Onguille Maiteari
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004809.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004809.MID
youtube: null
---

Ez daquit nola esquerrac eman
¡ah! nere onguille maiteac.
Nere lurrera bear nau eraman
Gaur zuen borondateac.
Eguin dezute cer obra ona
Aberatz eta pobreac
Orra cumplitu aguintzen duana
Jesucristoren legueac. (bis)

Nere oneraco, erriac dira
Buenos Airesen alcartu,
Bide emanic, Correo Españolac
Noblezaz zayo seguitu;
Arritzeco da, bai, gaur erriac,
Nola diraden arguitu,
Mundu gustian liberal onac
Adisquideac baditu.

Biotz oneco daunac badira,
Lista dutenac edertzen;
Zori oneco nere zartzera
Ah! nola duten honratzen:
Munduan bada zorionican,
Andre onac du eguiten,
Guizonac izar oyetan
Cerua degu icusten.

Oh! zu Romero Gimenes jauna,
Nere adisquide maitea,
Zure biotz onac consolatu du
Nere anima tristea;
Eguiac dira nere ametsac,
Mendiatera juatea:
Agur Romero, auda neretzat
Gozamentu eta paquea.

Agur, zuere Olaso jauna
Vizcaitar, prestu noblea,
Beti betico zure icena;
Biotzean det gordea;
Gaztetanic zait gusta biziqui
Mendiataco aicea,
Agur, banoa, naidet icusi
Euscal-erri maitea.