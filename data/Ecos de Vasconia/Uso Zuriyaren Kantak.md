---
id: ab-4760
izenburua: Uso Zuriyaren Kantak
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004760.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004760.MID
youtube: null
---

Uso zuriya aidean,
ederrago da mayean.
Zure parerik,
ez du arkitzen,
Ezpañia guziyan,
Ez eta ere Franciyan
Eguzkiyaren azpiyan
Eguzkiyaren azpiyan.

Uso zuriya erra zu,
Nora juaten zera zu,
Ezpañiako portuya oro
Elurrez betiak, dituzu
Gaurko zure ostatu,
Gure echian badezu
Gure echian badezu.

Uso zuri bat agertuzaigu,
Frantziyako mugatik,
Luma-cho-ren bat, falta omen du
Egocho biren erdi-tik.
Luma urashen falta ez balu,
Ez da munduban parerik
Ez da munduban parerik.

Ez nau itzuzen elurrak,
Ez eta gau baren illunak,
Maitia gatik pasa nezake
Gau bak eta egunak,
Gau bak eta egunak,
Dezer-tuetan oldenak
Dezer-tuetan oldenak.