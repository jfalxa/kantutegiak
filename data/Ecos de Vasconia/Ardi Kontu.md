---
id: ab-4799
izenburua: Ardi Kontu
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004799.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004799.MID
youtube: null
---

Ardi kontuan dago
Joshe Mari Mocha
besoetan arturik
jayo dan bildocha;
eta ikusirikan
ain guri ta belcha
pozez betetzen zayo
berari biyotza.

Bildochen ama dauka
beraren oñian,
umeari begira
egon eziñian;
noizik bein miliskatzen
du bizkar gañian,
ezagüeraz maitatu
lezaken añian.

Joshe pozez betia
dago penik gabe,
dalako bildoch guri
politaren jabe;
soñu onena bañan
arentzat da obe
bildochan abotikan
goshoro diyon... ¡Bee!

Mendiyan gora bera
dabilla Joshe Mari,
askotan chistu joaz
bestela kantari;
euskeraz deitzen diyo
bere ardiyari,
esanaz: Athos, athos,
chiki chikiyari.

Bere ondora biltzen
dira ardi denak,
bere eskutik jaten
dakite geyenak;
berdiñ maitatzen ditu
medar ta gizenak,
ta buruz badakizki
guztiyen izenak.

Bere bizimoduaz
dago chit kontentu,
kabriyola ederrik
ark egiten ditu;
ondo bizi dan, iñoiz
bazayo galdetu,
erantzun oidu: -Pozez
nabill... Ardi Kontu.