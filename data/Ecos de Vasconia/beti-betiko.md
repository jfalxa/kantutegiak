---
id: ab-4754
izenburua: Beti-Betiko
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004754.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004754.MID
youtube: null
---

Galayak:
Zure aita ta amari
(¡Trantze estua!)
Izandu naiz eskatzen
Zedorren eskua;
Egiña zendukazula
amarekikua,
Zuk esan eta nedukan
sinistua,
Aita berriz mintzatu zait
ain prestua,
Bayetz agiñdu dit eta
au da gustua!
Nere biyotz gashua dago
birpiztua.

Damak:
Nola dezun zuk barren bat
Danik onena,
Nik izan dizut beti
Zuri sinismena;
Baña falta zitzaigun
Lanikan zallena:
Nere aita prestuaren
aitormena:
Aspalditik banekiyen
amarena,
Ta oraiñ badegulako
Biyen baimena,
Bada kizu ni zuretzako
naguena.

Galayak:
Nola mundu onetan ez
Dezun berdiñik
Eziñ arki nezake
Nik zure ordañik;
Neretzako zeradela
Sinistu eziñik
Aspaldiyan igaro det
Gau samiñik;
Bañan ez da mudakorrik
biyotz fiñik;
Oraiñ lengo traba denak
Alderagiñik.
Biziko gerade gu biyok
bat egiñik.

Galayak:
Ai, nik zenbat nai dizudan!

Damak:
¿Eta nik zuri?

Galayak:
Mingañez eziñ esan
Genezake ori.
Biyotzak ala alkartu
Zaizkigu biyori
Nola bildu etzayozkan
len iñori
Pechu batian daudela
badiruri.
Ori egiya izanik...
Mushu bat neri
Ez alnazu bada emango?

Damak: Tori, tori.