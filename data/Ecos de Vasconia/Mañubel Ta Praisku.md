---
id: ab-4767
izenburua: Mañubel Ta Praisku
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004767.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004767.MID
youtube: null
---

Mañubel:
Praisku galtza zarreta
Lagun zar neria
Sayo bat egiteko
Zeukat umoria
Ik ere izanikan
Abillidadia
Asi gabe bildurraz
Alago gordia.

Praisku:
Bildurrikan ezdiat
Nik kantu lanian
Batez ere sagardo
Ederra danian
Pozikan jarduntzen nak
Umore oniak
I bezelakuakin
Biltzen naizenian.

M:
Oso, ederki asi
Aiz bertso botatzen
Eta estarri onez
Sasoyan kantatzen
Bañan ire sarrerak
Dizkiat esautzen
Akulluaren giza
Ari aiz zulatzen.

P:
Zulatzeko i ere
Ez ago motela
Ez itzake asiko
Orren fin bestela
Uste diat gaur geicho
Ik eran dekela,
Mingaña ere jarri
Zaik erdi totela.

M:
Ik ere badek nere
Antza gauz orretan
Zuzen bañan geyago
Abill okerretan
Ire berri zekiten
Ondo bazterretan
Zeñale abillela
Beti gezurretan.

P:
Gezurrikan ez diat
Nik iñon azaltzen
I ari aiz barrengo
Zikiñak zabaltzen
Ez ditut ziri charrak
Iñori botatzen
Pasaitarrena egiten
Ez alaiz lotzatzen.

M:
Abe jondarikela
Amigo neria
Ori dek gezurrezko
Abillidadia
Isturiatzen pasa zaik
Iri edadia
Non arkitu beste bat
I bañan obia.

P:
Manubel ari nazak
Oraiñ albistia
Baten gisakua dek
Gaur emen bestia
Ara zer artu detan
Nik orain ustia
Baso bana sagardo
Biyak eratia.

M:
Ederki esan dirak
Oraingo puntua
Ez aiz mutill makala
Ori dek kontua
Eran da egokigo
Esango kantua
Guazen eta chochetik
Egin zagun tragua.

P:
Nik ondo esan eta
Ik ondo aditu
Biyak ere makalak
Ez gaituk elkartu
Elkar tentatzen ondo
Gaituk gu aritu
Asko diagu eta
Char denak barkatu.