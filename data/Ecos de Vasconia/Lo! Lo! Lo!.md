---
id: ab-4748
izenburua: Lo! Lo! Lo!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004748.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004748.MID
youtube: null
---

Aurcho chiquia
negarrez dago
Ama emazu titia
Aita gaiztoa
tabernan dago
Picaro jocalaria.

Nere maitea lo ta lo
Eguin ezazu gozoro,
Siescachoa erabilli ta
Lo orain eta lo güero.

Aurcho chiquia zuretzat
Opilla zurtan daraucat,
Erdi erdia emango dizut
Beste erdia neretzat.

Aichoa jan ta sabela bero,
Lochoa eguin ta calera guero
Dambolinchoa calean dabil,
Nere maitea danzatzeco.

Nere maitea lo ta lo
Loguiro galan bat dago;
Zuc orain eta nic guero
Eguingo degu gozoro.