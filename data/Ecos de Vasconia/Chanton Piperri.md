---
id: ab-4816
izenburua: Chanton Piperri
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004816.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004816.MID
youtube: null
---

Chanton Piperri emen dago
Limosna on bat balego,
Canta beardot
Baldin albadot
Neure sabelen gosia
Erruqui dedin gentia.

Iru lagunec Ispasterren
Apostuba eguin eben
Jango ebela
Goiztic gabera
Amar erraldeco chalá
Neau laugarren nintzala.

Pozaz neguan zoraturic
Jateco laurden bat osoric,
Nere contuac
Beti alanguac
Joantzan poza putzura
Ametza oidiran modura.

Lequeitioco calian
Bishigu dembora danian
Oguei chicharro
Eta gueyago
Eta guciyac gabian
Sartzen dodaz sabelian.

Orduban contentu nago
Gaberdi osteradaño,
Andic goicera
Neure zabela
Beti dago gur gur gur gur
Bartco afariya agur.

Guztiz zabala dot sabela
Bardin bardin dot guibela
Este bacochac
Cabiucoluque
Gambela bete oquela
Bañan noiz izango oteda.

Ez da errotan arriric
Neure aguiñac langoric,
Neure aguiñac
Zatitucoluque
Ogui andi bat osoric
Balego bigun bigunic.