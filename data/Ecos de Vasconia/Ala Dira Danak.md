---
id: ab-4818
izenburua: Ala Dira Danak
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004818.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004818.MID
youtube: null
---

Chori chiki bat igasi joan zan
Nik kayolan len neukana.
Eta damacho maite dubena
Ill zitzaidan gasho ona.
Guziyak orla dira munduban
Orrela gertatzen da dana
Batzubek ill ta bestiak igas
Guizonak diyo ¡Au da lana!

Amoriyozko begiratua
Dama batek ziran egiñ,
Nere biyotzan berotasuna
Senti nuben berarekiñ;
Neretzat zala pentsaturikan
Soratu nitzan ni arrekin,
¡Zer pena gero ikusirikan
Beste galaicho jaun batekiñ!

¡Geroztik oso penaz bizi naiz
Pozik ez det nik billatzen,
Bakartasuna besterik etzait
Non naitikan neri sortzen;
Nai gabiakin ari naiz oso.
Ezur-utzera biurtutzen,
Nai nuke jakin iñor ote dan
Neregana gaur kupitzen.