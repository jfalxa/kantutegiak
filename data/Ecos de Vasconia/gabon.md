---
id: ab-4775
izenburua: Gabon
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004775.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004775.MID
youtube: null
---

Gaur Gabon arratza da
ez da lo egiteko
Birgiñak amabiyeetan
aurra du egingo
Melchorrek diyo
Jo soñu onak
poztu mundua
Jesus da jayo
Jesus da jayo.

Belengo Portalian
Jesus ona dago
San Jose ta Mariyak
Kontu egiñik ondo;
Melchorrek diyo
Jo soñu... etc.

Astua ta Iriya
Daude laguntzeko
Asnase berenakin
Jesus berotzeko;
Melchorrek diyo
Jo soñu... etc.

Artzayak Jesus gana
Presaka guztiro
Erregaluarekin
Badatoz pozkiro;
Melchorrek diyo
Jo soñu... etc.

Aingeruak diyote
Gloria in excelsis Deo
Miragarrizko itza
Zabaltzeko gero;
Melchorrek diyo.
Jo soñu... etc.

Mundu denen jabia
Umilki or dago
Zerutik etorriya
Adoratutzeko;
Melchorrek diyo
Jo soñu... etc.