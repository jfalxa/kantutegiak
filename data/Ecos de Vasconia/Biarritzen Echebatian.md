---
id: ab-4822
izenburua: Biarritzen Echebatian
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004822.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004822.MID
youtube: null
---

Biarritzen echebatian
Joan dan arratz batian.
Neskatilla dantzatu da
Sei mutillen tartian
Tran laran laran laran
Sei mutillen tartian.

Soñua entzutian plazara,
Joaten dira dantzara;
Elkarrekin jostatuaz
Dute naiko algara,
Tran laran laran laran
Dute naiko algara.

Biarritzen beti mutillak,
Chit maite neskatillak;
Borondatez bai ematen
Dizkate erroskillak,
Tran laran laran laran
Dizkate erroskillak.

Illunabarra etortzian,
Pozkida biyotzan,
Neskatillai echeraño
Laguntzeko pozian.
Tran laran laran laran
Laguntzeko pozian.

Neskatillacho legunak
Dira gure lagunak,
Oso maitatiyak gera
Gu Frantzes-Euskaldunak
Tran laran laran laran
Gu Frantzes-Euskaldunak.