---
id: ab-4802
izenburua: Mariya Nora Zuaz
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004802.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004802.MID
youtube: null
---

Mariya nora zuaz
eder galant ori,
Iturrira Bartolo
nai badezu etorri.
Iturriyan zer dago?
ardocho churiya,
Biyok erango degu
nai dezun guztiya.

Mariya zuregana
biltzen naizenia,
poza sentitutzen det
nere barrenian.
Bartolo nik ere det
atzegiñ artutzen,
ur billa nuanian
banazu laguntzen.

Mariya lagunduko
dizut gaur mendira,
ur presko eder billa
ango iturrira.
Iturri eder ortan
dagon ur garbiyak,
Bartolo alaituko
dizkitzu begiyak.

Mariya nere aurrez
ez jarri seriyo,
zuri dizutalako
zenbait amoriyo.
Far irri gozozkoa
egiten badezu
biyotza atzegiñez
betetzen dirazu.

Pauso ariñan oso
zu baldin bazuaz
ushua dirurizu
eguak jasuaz.
eta ageri bada
gonacho gorriya
Bartolori alaitzen
zayo arpegiya.

Gaztañ arbol onduan
egoten bazera,
pozez inguratutzen
naiz aldamenera.
Ikusten detanian
arpegi gorriya
biyotzeraño sartzen
nazu egarriya.

Joaten zeran bakoitzan
chit goiz belardira
azper eziñik jartzen
naiz zuri begira,
azaltzen dezunian
espaiñ gorrichua,
diruri mayatzeko
goiz arrosachua.

Mariya pentsatutzen
badezu eskontzia,
lendabiziko nitzaz
oroitu zaitia.
Zure mende jartzen naiz
denbora guziko
Bartolorekin gaizki
etzera biziko.