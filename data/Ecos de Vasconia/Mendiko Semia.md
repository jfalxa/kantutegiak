---
id: ab-4781
izenburua: Mendiko Semia
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004781.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004781.MID
youtube: null
---

Zorionez bizi da
jayoa mendiyan,
pake santuan eta
Jaunaren graziyan;
pozturik arkitzen da
norbere tokiyan,
an lan eguiñaz bere
denbora guziyan.
Choriyak maite duben
bezela Kabiya
dalako bertan jayo ta
egan ikasiya;
menditarrak ala du
maitatzen mendiya,
anchen bertan dalako
jayo ta aziya.
Iñoiz etortzen bada
goitikan errira
nai eza zortzen zayo
biyotzen erdira
bañan echol aldera
ematian jira
ariñ ariñ joaten da
Kantari mendira.