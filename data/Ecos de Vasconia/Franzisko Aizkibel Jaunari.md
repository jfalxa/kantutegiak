---
id: ab-4801
izenburua: Franzisko Aizkibel Jaunari
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004801.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004801.MID
youtube: null
---

Españian da gizon bat
bear deguna maita,
Franzisko Aizkibel jauna
euskaldunen aita;
chit da gizon prestuba
eta jakintzuba,
errespeta dezagun guk
gure maisuba.

Ogeita geyo urtian
bizi da Toledon,
Yzarraizko semia
ez da beti lo egon;
liburuen gañian
beti gau eta egun,
gure euskera maitia
galdu ez dezagun.

Euskaldunak gerala
ez bada ukatu,
eta mendiyetara
biegu partitu;
euskera egiten bada;
baserri denetan,
ondo biziko gera
euskal erriyetan.