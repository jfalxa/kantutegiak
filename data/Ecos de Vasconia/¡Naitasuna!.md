---
id: ab-4784
izenburua: ¡Naitasuna!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004784.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004784.MID
youtube: null
---

Ama bat orchen dago
betia zotiñez,
asnase estuakiñ
sosega eziñez;
beguitikan malkoa
dariyola ziñez
zirazkachoan aurra
daukalako miñez.

Biotza daukalarik
naigabez betia,
aurcho ari esaten
baitio ¡maitia!
¿zer nai dezu amachok
zuri ematia?
aurrak erantzuten du
¡Amacho titia!

Amak entzunik bere
aurraren itz otsa,
pizkortzen zayo barren
len zeukana otza
orduan azkaturik
chit pozez gorontza,
bularren ordez dio
ematen ¡Biotza.