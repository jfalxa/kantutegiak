---
id: ab-4741
izenburua: ¡Zuri!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004741.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004741.MID
youtube: null
---

Gaur uste gabe, maite gozoa
icusi zaitutanian
eguiyetaco zorotasun bat
zortu zait gorpuz danian. (bis)
Zuc jarrinazu biyotz gashua
orainchen zorionian
eta beguiyac pozez malcotan...
Aurrian zauzcatenian.

Maite zaitutan iñorc galdetzen
Baldin badizu beiñere,
Esan zayozu zuregatican
Nagola aspaldi miñbere;
Ain borondatez eguingo nuque
Zugatic cernai gauzare
¡Jabetziatic zure izatez!
¡Zu eguitiatic nere!...

Amorez maite zaitutalaco
Nabill orren triste beti,
Orra nolaco estuasunac
Sortzen zaizcan biyotz bati;
Baltsamo embat escatu bear
Codiyot laister norbaiti,
Baldin ta ascar cupiturican
Maitatzen ez banazu ni.

Ez det baltsamo bearricanen
Nere min larri onentzat,
Beste asmo bat bururatu ta
Orain artzen det obetzat;
Chit borondatez esqueitzen dizut
Nere biyotza zuretzat.
¿Nai aldirazu eman zuria
Beti betico neretzat.