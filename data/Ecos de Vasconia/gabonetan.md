---
id: ab-4789
izenburua: Gabonetan
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004789.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004789.MID
youtube: null
---

Usariyo ederrak
ziran antziñetan
denak elkartutzeko
pozez Gabonetan;
jauregi, kale, mendi
ta baserriyetan,
echola, choko, eta
baztar guziyetan.

Biyotz denak arkitzen
dirade pozturik,
elkartasun gozoaz
horrela baturik;
Aitón-Amonak daude
pozkidaz beterik,
an eztago algara
ta phosa besterik.

Gaztañak zirti zarta
erre bitartian,
kontu zarrak kontatzen
denak zukaldian,
gurasoak soratzen
gaztien tartian,
parerikan gabeko
zorion pakian.

Batzubek soñua jó
bestiak kantatu,
kriskitiñak jorikan
gaztiak dantzatu;
ingurukuak berriz
par gauzak kontatu,
denak ageri dira
biziro kontentu.

Alaitasun gozoaz
dira denak jartzen,
far irriya besterik
ezta an agertzen;
Aiton zarra asten da
bertsuak botatzen,
eta Amonachua
panderua jotzen.

Atzeginez beterik
baitira arkitzen,
pozezko katietan
dirala elkartzen;

Guziyak Egillea
dute bedeinkatzen
Zerutik diyelako
Berak lagundutzen.