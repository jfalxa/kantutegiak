---
id: ab-4732
izenburua: Nere Errico Gasteari
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004732.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004732.MID
youtube: null
---

Villarreal de Urrechu nere erri maitea
seme bat emen dezu
amorioz betea. (bis)
Nai baña ¿nola icusi?
au da lan tristea
zuretzat nai det bici
Urrechu nerea. (bis)

Bi milla eta seirégun
Legua badirá
Montevideotican
Euskal errirá.
Naiz esperantzétan
Etorri baguerá,
Aurreratasun gabe
Urtiac juan dirá.

Bai nere adisquidiac,
Bearda pentsatú,
Zuretzat Americac
Nola dan mudatú.
Ynorc emen ecin du
Lanican billatú
Orain datorrenari
Bear zayo damutú.

Gañera izan dégu
Emen ere guerrá
Gure zori onéan
Paquea egin dá.
Bañan guerrac ondoren
Dacar dictadurá
Don Lorenzo Latorre
Nagúsi degulá.

Ez bada, ez etorri
Gaur lur onetará,
Il edo bici obeda
Juatea Habanará.
Au dá gure bandera
Españaren onrá
Churrucaren semeac
Ara juango guerá.

Agur, adisquideac
Icúsi artean
Zuen ganatuco naiz
Egunen batean.
Esperantzetan bici
Nai det bitarten
Guero ezurrac utzi
Nere lur maitean.