---
id: ab-4765
izenburua: Beltzerana
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004765.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004765.MID
youtube: null
---

Beltzerana naizela
calean diote
ez naiz zuri ederra
arrazoya dute. (bis)
Eder zuri galantac
pausoan amabi
beltzera graciosac
milla etatic bi. (bis)

Belcharana naicela
Zuc neri esateco
Lenago zan denbora
Erreparatzeco
Belchac eta zuriyac
Mendiyan ardiyac
Zuc ere ez dituzu
Bentaja guziyac.

Belcharan graciosa
Parerie gabia
Mundu guztiyac diyo
Cerala neria
Munduac jaquin eta
Zuc ez jaquitia
Ondo eguiten dezu
Disimulatzia.

Sarritan amoriyoz
Dizut beguiratzen
En el ojo derecho
Erreparatutzen
Beguiyac belchac belchac
Itz ori donosho
Ez dic mutillac bear
Orren amoroso.

Beltzerana naizela
Esana gatican
Parra egiten det nik
Iñoren gatican
Zuri tristea bañan
Obe da belcharan
Denen aurrez ibilli
Niteke pacharan.

Calian nuanian
Pauso ederrian
Asko jarritzen zaizquit
Begira aurrian
Orduan jiraturik
Ni beste alderdira,
Penaz bezela denak
Gelditutzen dira.