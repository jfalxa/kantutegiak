---
id: ab-4825
izenburua: ¿Oroitzen?
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004825.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004825.MID
youtube: null
---

Oroitzen alzera
Charmangarriya
Noiz eman zenian
Neri feriya.
Ni utzirik
Bazterturik
Bertzetara
Joaten
Omen zera
Yostatzera.

Neskatilla eder
Soragarriya,
Nere biyotzian
Zaude jarriya;
Zu gabe nik
Ez det onik
Sinistatu,
Barrendik nai zaitut
Chit maitatu.

Yzarcho ederra
Dizdizariya,
Zu zera barrengo
Nere argiya;
Gau ta egun
Legun legun
Dizdizara,
Banatzen dirazu
Biyotzera.

Aingeru liraña
Pare gabia,
Zuk alaitzen dezu
Barren neria;
Nere jabe
Duda gabe
Gaur zera zu
Betiko zuretzat
Ar-nazazu.