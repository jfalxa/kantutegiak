---
id: ab-4823
izenburua: Sacristau Zar Baten Diabruqueria
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004823.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004823.MID
youtube: null
---

¿Cer deabruqueria
Zayo burutara?
¿Batista adiñ orretan
Dua escontzera? (bis)

Aur jayo,
gizon bici,
Aurcho biurtzea;
Orra icusten danez
gure suertea.

Ya laroguei urte
Dituen guizonac,
Ez du andreric bear
Baicican salda ona:
Gaztetaco chingarrac
Sentitzen baditu,
Ur frescoa edanaz
Itzali bear ditu.

Lamparac itzaltzera
Dijoan orduan,
Llamarac bota oir ditu
Arguira moduan;
Baña dira llamara
Acabaeracoac,
Ez, olio gueyeguic
Emanicacoac.

Davidec zartu eta
Sunamit artzeco,
Esan zuen zuela
Oñac berotzeco.
Batista adin orretan
Dec ascoz obea,
Palenciaco manta,
Ez andre gaztea.

Ez badic gauza orrec
Erremedioric,
Ez dezac behintzat eguin
Albadec semeric
Esnatzen aicenean
Goicean lotatic,
Osticoca botazac
Andrea aldetic.

Ez adilla fiyatu
Llamara oyetan,
Ondo conserva itzac
Ere errayetan:
Campora ateratzen
Empeñatzen ba-aiz
Otzac illic chit laster
Lurperatuco aiz.