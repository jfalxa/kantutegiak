---
id: ab-4737
izenburua: Juramentuba
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004737.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004737.MID
youtube: null
---

Begui urdiñac dituzu eta,
arpegui zurigorriya,
nere biyotzac maitezaituben
aingueru zoragarriya;
Zumia beciñ biguña eta
mia dá zure guerriya,
estutu bague artuliteque
bi escubaquin neurriya.

Lengo batian, izár berriya,
ongui fortuna nerian,
maite ninduzun galdetu eta
bayetz eranzun ceniran;
pozaquin iya zoraturican
aditu eta seguiran,
beso escuya estutu nuben
zure guerriyaren giran.

Neri beguira guelditu ciñan
buruba triste etzanaz,
ni ere zuri beguira negon,
"¡maite nería!" esanáz;
luzaro ala egondu guiñan
suspiriyuac emanaz,
biyoc elcarren beguiyetatic
amoriyua eranaz.

Ondo penaquin ascaturican
zure guerritic besua,
ez zait aztuco nola ezan nizun:
"¡nere maitecho gozua!
"zorionaren ecartzallia,
"izar amoriosua,
"zu adoratzen igaroco det
"nere bicitza osua!.

"Maite nazula len esan dezu,
"¡berriz esan zazu arren,!
"ez naiz entzutez gogobeteco
"milla bider esan arren:
"nun ta cerubac erabaquiya
"beste moduz ez dacarren,
"dembora guchi barrun gu biyoc
"izango guera elcarren..

Imagiña bat cillarrezcua
cordoi batequin lotuba,
beguiz aurrian jarri ceniran
colcotic ateratuba,
ceñetan cegon gurutzeturic
Jaunaren seme santuba:
"onen aurrian,-esan ceniran-
"eguizu juramentuba..

Faltziyarican ez cegüela
nere biyotz shamurrian,
proga emango nizun eguna
arquitzen zala urrian,
beguiratuba imagiñan ta
belaun escuya lurrian,
juramentuba esqueñi nizun
Jaungoicoaren aurrian.

Fintasunaquiñ cumplituco det
ez da cer egon salantzez:
demborarican igaro gabe
gogo charrezco balantzez:
eracusitzen baldin badizut
falsiyarican mudantzez,
nere biyotza bete dezala
damutazunac arantzez.