---
id: ab-4814
izenburua: ¡Zugana Goizeko Izarra!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004814.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004814.MID
youtube: null
---

Zure bozaren imbiriya du
Baso beltzeko sosuak,
Pollitasunez ez dizu berriz
irabazitzen usuak.
Beiñ edo beste nik entzutian
zure itz shamur goshuak
estitasun bat sentitutzen du
¡Ay! nere biyotz gashuak.

Zugana zugana
Nere goizeko izarra
Zugana zugana
Nere aingeru laztana.

Chori chikiyen ama leyalak
Egatutzen du kabitik,
Ogi apur bat edo gariyen
Ale bat billagatziagatik
Baita nik ere laztan neria
Egingo nukela dakit
Nola choriyak egiten duben
Bere ume chuagatik.

Zugana, Zugana
Nere aingeru laztana. (bis)

Nere aingeru pare gabia
Maita nazazu meserez,
Kupi zaitia ni ikusita
Zure oñetan negarrez;
Mundu onetan zu añako bat
Arkitzen ez da chit errez
Zure onduan konparatu ta
¿Ni zer naiz bada...ezer ez?

Zugana, Zugana
Nere aingeru laztana. (bis)

Mirabe gazte obiagorik
Ez det sekulan ikusi,
Nere biyotzak zure ganuntza
Joan naidu beti igasi;
Zu gabetanik mundu onetan
Ezin niteke ni bizi,
Ez nazazula penak sufritzen
Luzaro eruki-azi.

Zugana, Zugana
Nere aingeru laztana. (bis)

Zenbat naigabe ditut pasatzen
Azpaldiyan zuregatik,
Amaika bider malko tantoak
Ishuri zaizkit begitik;
Maitatutzia erreguz orain
Eskatzen dizut bertatik
Elkarren jabe gu egitia
Dezio det Biyotzetik

Zugana, Zugana
Nere aingeru laztana. (bis.