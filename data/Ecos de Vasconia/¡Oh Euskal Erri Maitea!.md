---
id: ab-4758
izenburua: ¡Oh Euskal Erri Maitea!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004758.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004758.MID
youtube: null
---

¡Oh Euskal erri maite maite maitea
Zuri begira nago
bañan ¡nola! ¡nola!
lengo antzikan eziñ
emanik iñola.
Atzo ziñan zorion
guzien krisola
¿eta gaur? ¿eta gaur?
gaur bestien
gisan Española.
¿Non dira zure famak,
zure historiyak,
ain eder apaindutzen
zinduzten gloriyak?
Lenengo biyotza ta
urrenak begiyak
oroitzean negarrez
jartzen zaizkit biyak
jartzen biyak jartzen biyak
negarrez negarrez.

¿Non dira ondorengo
Inbiriosoak,
Zabaltzen zituztenak
Urrezko lazoak?
Il ziran, alperrikan
Emanik pausoak,
Engañuz bete gabe
Gure gurasoak.

¿Non dira Erromatar
Gudari abillak,
Godoak, kartagoak
Eta moro pillak?
Zu ezin ustar petuz
Millak eta millak,
Euskaldunak gandikan
Izan ziran illak.

Zu zera; ¡O! zu zera
Euskal-erri ama,
Iñoiz iñoren mende
Egon etziñana;
Eta gero zedorren
Naieraz ziñana,
Bat-da Españiako
Koroiarengana.

Geroz ere libreak
Zeuzkatzun erriyak,
Eta pozez janziyak
Zelai ta mendiyak;
Zinduzen zorionak
Ain ziran aundiyak
¡Zu ziñan amerikak!
¡Zu ziñan indiyak!

Egun ayek joan ziran
¡Bai! ezin ukatu,
Zer pasa zan kontatzen
Ez det nai nekatu;
Egin bazan ezpazan
Utz edo pekatu,
Zuretzat ziran lengo
Kontubak trukatu.

Denborarekin gauzak
Emanikan jira,
Beste kristal batetik
Gaude gaur begira;
Baztar guziyak triste
Ikusten baitira,
Zure lengo graziyak
Ezkutatuk dira.

Egun batian gaizki
Jarri ziñan oso,
Ezin zere bururik
Zendubela jaso;
Min charra biotzean
Gelditurik preso,
Geroztik bizi zera
Triste ta penoso.

Aspaldiyan etzaizu
Gaitzikan arindu,
Ezpada, lendandikan
Ere zaizu gaindu;
Zure biotz eriyak
Iltzeko ain mindu;
Adios ama ona
Zureak egindu.

Len zenduben arbol bat
Min sendagarriya,
Autziñako asaba
Zarrak ezarriya;
Bañan nola bera
Igartu dan iya,
Galdu da zuretzako
Bere alderdiya.

Arbolak zeukan arte
Bere ontasuna,
Zureak ziran posa
Eta osasuna;
Orain azaldurikan
Aren ondasuna,
Ezkutatu da zure
Zoriontazuna.

Aritz arbol santuba
Ikusi da galtzen,
Bere grazi ederrak
Lur azpira sartzen;
Berririkan ezpada
Oin orde esartzen,
Ama maitea zera
Biziko penatzen.

Zuri lengo disdiraz
Zeuden kondaryak
Ostiko tu ta dira
Gain duko berriyak;
¿Zer esango da gero
Ikustean biyak?
Nano jira zirala
Jigante aundiyak.

Aundiyak dira zure
Biotzeko penak,
Bañan berriyak eta
Senda litezkenak;
Aitor-en seme onak,
Izatera denak,
Biur zitzaizkizuke
Poz, zori on lenak.

Poz au biurtzen bada
Zure erraietan,
Ongilleak nor diran
Jakingo da bertan;
Izenak ezarriko
Dirare letretan,
Urrez liburutan da
Bronzez paretetan.