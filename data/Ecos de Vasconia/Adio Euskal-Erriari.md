---
id: ab-4740
izenburua: Adio Euskal-Erriari
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004740.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004740.MID
youtube: null
---

Gazte gaztetanican
Erritic campora
Estrangeri aldean
Pasa det dembora
Eguia alde gustietan
Toqui onac badira
Baña biotzac dio
Zuaz Euskal Errira. (bis)

Lur maitea emen uztea
Da negargarria
Emen gelditcen dira
Ama eta Erria.
Urez noa icustera
Bai, mundu berria
Oranche bai naizela
Erruquigarria.

Agur nere biotzceco
Amacho maitea
Laister etorriconaiz
Consola zaitea.
Jaungoicoac bada naidu
Ni urez juatea
Ama certaraco da
Negar eguitea.