---
id: ab-4783
izenburua: Ama Eta Alaba
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004783.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004783.MID
youtube: null
---

Amak ezcondu ninduben
amabost urtetan
amabost urtetan.
Senarrak geishiago
Larogei non bait an
Eta ni neskatilla gazte
Nola nindeque izan
Nola nindeque izan.

Amak:
Neska ishilikan ago
aberatza den ori,
aberatza den ori;
pasientziyan pasaitzan
urte bat edo bi
laizter ilko den zarra eta
biziko aiz ongui
biziko aiz ongui.

Alabak:
Ama alperrikan ari da
izketa moduan,
izketa moduan;
egun bat pasatzeko
bizitzen munduan,
ez det nik agure zarrikan
nai nere onduan
nai nere onduan.

Amak:
Neska eguintzan amaren
esana berela,
esana berela;
izango aiz ederki
marquesa bezela,
ez ariñela izan bada
orlako ustela
orlako ustela.

Alabak:
Ama nola naidu ori
gogoz eguitia,
gogoz eguitia;
beste gauzacho bat det
nik borondatia
diru gabea nayago det
senarra gaztia
senarra gaztia.

Amak:
Neska et zanala artu
dirurik gabia,
dirurik gabia;
bestela izango den
naiko lanbidia
chorakeri denak utzita
artzan aguria
artzan aguria.