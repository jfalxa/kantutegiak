---
id: ab-4800
izenburua: Uda Berriya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004800.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004800.MID
youtube: null
---

Uda Berriyak,
baztar guztiyak;
Ditu ederki
lorez jasten;
egun argiyak,
zoragarriyak,
Era onetan dira asten.

Zelai aundiyak,
intzak bustiyak,
Ederrak dirade agertzen;
or marrubiyak,
guri guriyak,
Lurretik dira azaldutzen.

An, gereziyak,
gorri gorriyak,
Arbolak ditu ematen;
emen choriyak,
aundi chikiyak,
Ari dirade kantatzen.

Nekasariyak,
guzti guztiyak,
Pozez dirade arkitzen;
gauza egokiyak,
uda berriyak,
Ditu mundura ekartzen.