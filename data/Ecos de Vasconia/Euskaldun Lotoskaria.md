---
id: ab-4777
izenburua: Euskaldun Lotoskaria
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004777.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004777.MID
youtube: null
---

Agur jaunak
jaunak agur
agur terdi
Agur jaunak
jaunak agur
agur terdi
denak jinkoak
inak guire
ni ere bai
zuek ere
Agur jaunak
agur terdi
emen guire.