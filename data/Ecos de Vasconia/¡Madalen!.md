---
id: ab-4790
izenburua: ¡Madalen!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004790.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004790.MID
youtube: null
---

Madalen Buzturingo
gabian gabian
errondian dabiltza
zure portalian. (bis)
Ay! Madalen Madalen
Ay! Madalen
zure billa nebillen
oraiñ baño len.

Asi goiko kaletik
eta bekoraño
ez da ederragorik
Madalencho baño
Ay Madalen Madalen
Madalen gashua,
Madalenek eraten du
erari goshua.

Zeiñ ote da berorri
orren famosia,
Madalen deritzona
neskacha airozia,
Ay Madalen Madalen
Madalen neria
Zure ondoren dabill
bueltaka jendia.

Kalian gora bera
ishil ta seríyo,
illiaren puntian
zintia dariyo;
Ay Madalen Madalen
Madalen gashua
seigarren batalloyian
daukazu majua.

Elizara joan eta
belauniko jarri,
lenbiziko errezua
bere majuari;
chikichua da bañon
bai konponduchua
kazadorietako
kabo aurrengua.

Chikichua naizelako
ez nazazula utzí,
Kapitan iríchiko naiz
bigar edo etzi;
Ay Madalen Madalen
Madalen maitia
zu nai zinduzket izan
nere emaztia.

Asi goiko konbetutik
eta kaleraño
ezkonduko lirakenak
makiña bat dago
Ay Madalen Madalen
Ay Madalen,
zure billa nebillen
oraiñ baño len.

Ikuste bakoitzian
Madalen Kalian
poza sentitzen det
nere biyotzian
Ay Madalen Madalen
Madalen loria
nayago nuke baziña
betiko neria.