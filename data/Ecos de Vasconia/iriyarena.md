---
id: ab-4764
izenburua: Iriyarena
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004764.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004764.MID
youtube: null
---

Damboliñaren jirac calean
daramatzi jendeac airean
chistuaren ta atabalaren
soñu alayean.
Errico biztanle guciac
plazara dirade etorriac
arratzeco ceceneco
suac ecarriac.
¡Oh Donostico cecensuzcoa
izan zaitez zorionecoa!

Irtena da sheshena arcupetic
Chingarra dariola atzealdetic
Dambadaca
Ta jiraca
Jendeen tartetic.
Zalapart artan cembat nazpill
Cembat carrashi, cer iscambill
Itzumuca
Trumbulluca
Jende dena dabill
¡Oh, zu, cecen onen autorea
Izan zaitez gloriz betea!

Damacho gazte ta galai fiñac
Elcarri adi bat ecin eguiñac
Arcupetan
Quiriquetan
Ay cer aleguiñac!
Cecensuzcoa bada aurrean
Igues eguin nairic asmoan
Ceren larri
Diran jarri
Elcarren ondoan
¡Oh cecen pishcor chingar jariyo
Cembat contu zure mediyo!

Su curpillac dijoaz astera
Cecensuzcoa igo dute oltzera,
Argui pillac
Su bombillac
Dijoaz airera
Ceñen argui politac dira
Maite degunari beguira
Bat batetan
Beguiyetan
Icusten badira
¡Oh, bai, dira, bai chit chingartiac
Biyotz suzco baten arguiac!

Bejon daizula, cecensuzcoa
Donostiarren invencioa
Gasteentzat
Ta zarrentzat
Fest gain gañecoa.
Ez bada, jendeac naguithu
Plazara joan gabe ez gueldithu
Far asco eguiñ
Dantzatu ariñ
Ta asco divertithu.
¡Cecensuzco eder on deguizula
Gaur ta beti bejondaizula.