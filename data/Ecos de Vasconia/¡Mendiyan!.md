---
id: ab-4779
izenburua: ¡Mendiyan!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004779.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004779.MID
youtube: null
---

Bakar bakarrik nago
mendi muturrian.
Buru utzik jarriya
tontor tontorrian.
¡O zembait pentsamentu
nere barrenian
Ikusirik ainbeste
gaur eder aurrian.
Zelai soro belardi
baso ta mendiyac
zubek zerate nere
leku maitatiyak
pozturik jartzen zairkit
biyotz ta beguiyak
ikusirikan zubek
osoro egokiyak.
Bakar bakarrik nago
mendi muturrian.
Buru utzik jarriya
tontor tontorrian.