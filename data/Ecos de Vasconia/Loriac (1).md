---
id: ab-4746
izenburua: Loriac (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004746.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004746.MID
youtube: null
---

Beti nic damari
loriac ugari!
endemas pishca bat
maite naubenari;
damac berriz neri
¡guezurra diruri!
O siñes pagatu
loresco zorori.
Oso nago mendian
Jaungoico ichubaren
ama Venusengandic
jayotacoaren;
neroni ez naiz jabe
nere biotzaren
gashua preso daucat
amoriyuaren.

Beti nic damari,
¡loriac ugari!
neroni ez zaiz jabe
nere biotzaren
gashua preso daucat
amoriyuaren. (bis)

Sartutzendan becela
balcoi batetican,
eguzquiya guerala
cristaletatican:
modu ortarashe bein
igaro citzaidan,
biyotzera dama bat
begiyetatican.

Biyotz nerian preso
guelditu bire zan,
gueratzen dan becela
chingarra iyescan;
obligatuba dago
egotera bertan
nunta biyotz ta guzti
ateratzen ez dan.

Ditu ille urresco
albiñu luciac
alacoric apain-tzen
ez du orraciac
francotan imbiriya
eman dit aiciac
aren trentza tartetic
jostatzen paciac.

Ondo comparatzia
da gauza eciña
arren beguiyetaco
urdintasun fiña
ez da irrutirare
aitatzeco diña,
itsasoco bagaren
colore urdiña.

Ez da cer aitaturic
ceru zabalare,
bolaras urdin urdiñ
jarritzen dalare;
aingueru neriaren
beguiyac ala-ere,
urdiñ purubagoco
cerubac dirare.

Izarra dirurite
aiñ ditu arguiyac,
ainguerubac cerutic
jechita jarriyac;
gloriyan eguintaco
ispillu garbiyac
Jaungoicoa badala
gogoragarriyac.

Cupidoc eritzen-nau
flechaquin berriro
neregana jiratzen
dituben aldiro
biyotzera zartzen zait
eztiro-eztiro
amoriyotazun bat
gozua guztiro.

Aren espaiñ gorriyac
nere atzeguintzat
ondo mereci dute
famatzia fintzat
cerbaites apartetic
icusita beintzat
mundu guztiyac artzen
ditu crabelintzat.

Farric eguiten badu
cerutar virgiñac
erascuten dituben
ortza chiqui fiñac
churis elurrarequiñ
dirare berdiñac
perla iruri dute
cillarres eguiñac.

Gozuaren virtutez
aren asnaciac
loarasitzen ditu
nere oñaciac
usai oberic ezdu
banatzen aiciac
loriac maitatubas
baratzan paciac.

Lepo biribilla ta
gorputza liraña
chiquiz ez det esaten
oñaren tamaña
ibiltzen aizetzuba
jantziyan apaiña
aren graciyac dira
ecin esan aña.