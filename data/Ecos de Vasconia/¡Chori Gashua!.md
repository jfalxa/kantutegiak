---
id: ab-4780
izenburua: ¡Chori Gashua!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004780.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004780.MID
youtube: null
---

Kayolacho batian
choriya kantari,
pipirripi eguiñaz
triste baita ari! (bis)
Beti begira dago
kanpo ederrari,
libertade onaren
beso zabalari. (bis)

Diyo; beti nago ni
erreja zuluan,
preso bat beselashe
kayola zokuan
¡zein ongi bizi diran
daudenak kanpuan
egatuaz gustora
naiduten lekuan!

¡O, libertade gozo
eder ta maitia,
nere dezioa da
zu-gana joatia!
¡chori gasho onetzaz
kupitu zaitia!
¡¡arren iriki zazu
kayolen atia!!

¿Noiz arkituko ote naiz
ni libertadian,
nai badet lurrian ta
nai badet aidian?
esperantza badaukat
chit indar betian
libratuko naizela
egunen batian.