---
id: ab-4824
izenburua: Mari Domingui
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004824.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004824.MID
youtube: null
---

Orra Mari Domingui
beguira orri
Gurekin nai dubela
Belena etorri
Gurekin nai badezu
Belena etorri
Atera bearko dezu
gona zar ori
Atoz atoz
zure billa
nebillen ni. (bis)
Atoz goacen
Adora dezagun
Belenen jaio dan
Aur eder ori. (bis)

Nondikan ateraren
Gona zar ori,
Berririk baldin baden
Gorde zan ori;
Arriturik ziagon
Belengo erria
Aterakin ortik
Farragarria.
Atoz Atoz etc.

Jodezan panderoa
Ik Antonika
Nondikan etorri aiz
Orren korrika
Ezin esplikaturik
Misterioa
Belenen jayo dala
Aur divinoa
Atoz Atoz etc.

Ojua eta dantza
Denak batian
Egin zagun guztiyak
Oso pakian;
Jesusen izenian
Dantza dezagun
Bere laguntza gero
Izan dezagun.
Atoz Atoz etc.

Irrintzi eta poza
Algararekin
Ori bear diñagu
Guk elkarrekiñ
Pozez adora zagun
Jesus aur ona,
Berak eman dezaigun
Gaur zori-ona
Atoz Atoz etc.