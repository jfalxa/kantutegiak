---
id: ab-4812
izenburua: Bi Maitetasun
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004812.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004812.MID
youtube: null
---

Bi maitetasun dauzkat
biyotz barrenian
zañak dauden bezela
sartubak lurrian
biyak gau eta egunaz
naukate mendian
utzi biarko ditut
iltze naizenian.

Nexka polit polit bat
daukat nik aututa
nere biyotz gaxua
oso xamurtuta
eder ta ona dalako
nauka txoratuta
zer poza nere onduan
uraxen artuta.

Beste maitetasun bat
umetik asiya
da beti maitatzia
nere sort erriya da
beti maitatzia
nere sort erriya.

Bi maitetasun dauzkat
biyotz barrenian
zañak dauden bezela
sartubak lurrian
biyak gau eta egunaz
naukate mendian
utzi biarko ditut
iltze naizenian.
Sort erri ta maitia
maitetasun aundiya
maitia gatik biziya
Euskal erriya gatik guciya.