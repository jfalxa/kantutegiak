---
id: ab-4770
izenburua: Manuelacho
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004770.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004770.MID
youtube: null
---

Manuelacho eskasayozu
Barkaziyua Andresi.
Beti orrela ibilli gabe
Majo sharraren iguesi
Napoleonen pauso gaiztoak
ondo dituzu ikasi.

Ez det Andresi barkaziyua
Nik eskatu bearrikan
Ez detalako sekulan egin
Iñon bear eztanikan
Ez det Andresi barkaziya
Nik eskatu bearrikan.

Amoriyuak ega luzeak
Beti biyotzaren billa!
Ni nai naubena nere aurrera
Belaunka jarri dedilla
Amoriyuak ega luzeak
Beti biyotzaren billa.

Manubelacho aritu zazu
Andresen borondatia
Zure biyotza eta neria
Nai luke bat egitia
Manubelacho aritu zazu
Andresen borondatia.

Nigan pentzatzen baldin badago
Eztago leku charrian
Ez det chinizten esan artean
Guziya nere aurrian
Nigan pentzatzen baldin badago
Eztago leku charrian.

Manubelacho biyotzekua
Pozez nator zuregana
Egi egiya esaten dizut
Biyotzetikan nik dana
Manubelacho biyotzekua
Pozez nator zuregana.

Belaukaturik zure aurrean
Nago, naizenduen gizan
Elkarren jabe zuk nai badezu
Egin gaitian elizan
Belaukaturik zure aurrean
Nago naizenduen gizan.

Andres maitea neria zera
Ezin det bada ukatu
Amoriyuen indarrak gaitu
Biyak betiko elkartu
Andres maitea neria zera
Ezin det bada ukatu.