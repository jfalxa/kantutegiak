---
id: ab-4753
izenburua: Pello Joshepe (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004753.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004753.MID
youtube: null
---

Pello Joshepe tabernan dala
aurra jayo da Larraulen. (bis)
Echera juanta esan omendu
ezta neria izanen
ama orrek berak topa dezala
aurrorrek aita zein duen. (bis)

Ay au pena ta pesadumbria
zenarrak aurra ukatu. (bis)
Aurra onentzat beste jaberik
ezinlezake topatu
Pello Joshepe biyotz neria
aurrorrek aita zu zaitu. (bis)

Fortunosua nitzala bañan
Ni naiz fortuna gabia.
Abade batek eraman dizkit
Umea eta andria
Aurra beria bazuben ere
Andria nuben neria.

José Agustiñ nere biotza
Aren aditu nazazu.
Beti orrela egonda ere
Neke aundiya daukazu
Askos obeto izango zera
Arren eskondu biezu.

Pello Josepe nere biyotza
Zordizut milla graziya.
Eskontzekotzat dezio nuke
Gazte donzella garbiya
Ez mundu ontan zenbait bezela
Len beztek erabiliya.

Jose Agustin nere biotza
Arren aditu nazazu.
Gazte donzella mancha gabia
Baldiñ gustatzen basaizu
Bezterengana ez naiz fiyo ta
Ziraskatikan artzazu.

Pello Josepe nere biotza
Zordizut milla graziya.
Ziraskatikan etortzen alda
Donzellatazun guziya
Nik igualian nayago nuke
Amak maitecho aziya.

Bonapartian serbitzen dauskat
Iru anaya Londrezen.
Ayen zenire saspi lenguzu
Gobernadore Cadizen
Dontzelltasuna zertaraño dan
Nombait etzaizu aditzen.

Nere echian bi ollo dira
Batek daduka guiyua.
Ollarra berriz kukurrukuka
Kantoyan gora dijua
Jaunak orra despedida ta
Asko da diberziyua.