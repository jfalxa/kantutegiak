---
id: ab-4730
izenburua: Tristurá
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004730.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004730.MID
youtube: null
---

Guraso ta semiak
Agoni larriyan
Urak estalirikan
Gabaren erdiyan. (bis)
Dolorez ¡Ai! ill dira
Konsuegrako erriyan
Negarra zabaldurik
Españi guztiyan. (bis)

¡O zér tristura beltza
Nere biyotzian
Eriotz zulo artan
Pensatzen jartzian!...
¡Aur, andre ta gizonak
Pillan ikustian,
Ez eche ta ez ezer
Lurraren gañian.

¡Negar chiki aundiyak
Euskaldun lurrian!
¡Negar arriyak ere
Gure Donostian!
¡Korri konsolatzera
Alegin guztian!
Guztiyok egiñikan
Bat ¡karidadian.