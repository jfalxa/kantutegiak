---
id: ab-4782
izenburua: ¡Ama Nerea!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004782.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004782.MID
youtube: null
---

¡Malkoak ishuritzen
zaizkit beguitikan,
Nere ama maitea
gogoraturikan;
zortzi egun bakarrik
dira orain dikan,
Zeruraño joan dala,
mundu ontatikan!

¡Aingeruen artean
or zaude zeruan,
zedorrek mereziya
zenuben lekuan!

Zu zeruratu ziñan
ni emen nazu utzi,
Guerostikan badaukat
makiñabat antzi;
zu gabe mundu ontan
eziñ det nik etzi,
nere biyotzak zugan
Joan naidu igazi.

¡Aingueruen... etc.

Yzenik ederrena
mundu ontan dana,
da semien abotik
irtetzen dan "Ama"
au da biyotz nerean
beti daukatana,
bizi naizen artean
astuko etzaitana.

¡Aingueruen... etc.