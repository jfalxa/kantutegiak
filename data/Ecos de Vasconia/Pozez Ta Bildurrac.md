---
id: ab-4738
izenburua: Pozez Ta Bildurrac
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004738.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004738.MID
youtube: null
---

Dama gazte polit bat
bada Donostiyan
berdiñic ez dubena
cerubaen azpiyan. (bis)
Biyotza muguitzen zait
modu chit eztiyan
nere pensamentura
datorren guztiyan. (bis)

Graciyazco aingueru
adoragarriya,
Jaunaren ondotican
egan etorriya:
cerubac egintaco
milagro berriya
¡gloriyaz bete dezan
Donostico erriya!

¿Arpeguiya ceñec du
arrec becin fiña?
eta ¿ceñec gorputza
aiñ ongui eguiña?
ibilleran ere da
guztizco ariña,
euskerac ez du itzic
famatzeco diña!

Naiz plazara juan bedi
naiz juan iturrira
¿norc ez du arreta jartzen
arren ibillira?
bat bañan gueyagore
guelditutzen dira
ezcutatzen dan arte
berari beguira.

Bere jatorrez duben
pachara fiñian
biria pasatzen du
pauso ariñian:
lurric ez du zampatzen
choriyac añian
chutic juango litzaque
uraren gañian.

Orrembeste graciya
dituben meriyo
arentzaco gordia
cembat amoriyo
biyotzian dacatan
¿norc contatu diyo?
calian icusita
esan dit ¿ariyo?

Berriz icusitzeco
eguingo det modu,
juango diran baño len
oguei ta lau ordu:
¿ariyo? esaten badit
laister entzungo du:
¿nere arratseroco
ametsa, cer modu?

Ongui nai ta eciñ izan,
esango dit ambat;
nic erantzungo diyot:
nosqui besterembat
izango da sufritzen
dubena zuc aimbat...
¡cein da bada? esazu?
¡zu nai cinduzquen bat!!

Burlaz esaten dezu,
¡norc naico nau ni? ¿norc?
aberatsa ez danic
ez du maite iñorc:
baña nic ordubanchen
erantzungo diyot:
¡zoragarriya, naizu
maita gaiten biyoc?

Bayetz esaten baldiñ
badit erdi farrez,
erotu eguingo naiz
pozaren indarrez:
ezetz entzuten badet
nere suerte charrez,
¡itz orrec utzico nau
betico negarrez!.