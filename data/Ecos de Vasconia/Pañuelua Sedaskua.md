---
id: ab-4805
izenburua: Pañuelua Sedaskua
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004805.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004805.MID
youtube: null
---

Elizara dijua
Seriyo, seriyo,
Illiaren puntatik
Zintia dariyo.
Elizara juan eta
Belauniko eta jarri
Lembiziko estaziyua
Bere majuari.

Illia ederra dezu
Suria badezu,
Brechan erosi eta
Ipiñi aldezu,
Pañuelua sedaskua
Kolore bi churitakua
Asken-askeneko
Moda berrikua.