---
id: ab-4734
izenburua: Arbola Bat
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004734.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004734.MID
youtube: null
---

Arbola bat zan Paradisoan
Jaunak apropos jarria,
Mundu guztiak artu egion
Lotsa ta itza andia
Bere azpian bizitea zan
Aita Adanen gloria,
Andik kanpora zer topau eban
¿Espada negargarria?

Arbola santu arren azpian
Bizi zan, zeruan legez,
Eukazalako atsegin danak
Eta nekerik baperez;
¡Ay begiratu baleutsa beti
Lotsa on eta itzalez!
Mundua etzan gaur arkituko
Onenbeste atsekabez.

Arbola bat zan Bizkayan bere,
Neure anaya laztanak,
Zeiñen azpian, pozez beterik
Egoten ziran asabak,
Kerispe zabal artan jarririk
Eginda euren Batzarrak,
Bustarri baga, nasai ta libre,
Bizi ziran bizkaitarrak.

Bere azpian umildu ziran
Errege Gaztelakoak,
Eta lekurik ez eben izan
Sekula barriz moroak;
Atzera gura badogu izan
Libre len giñan lakoak,
Ichi daiguzan erdaldunentzat
Erdaldun diran kontuak.

Astu daiguzan geure artean
Izan diran aserreak,
Izan gaitean anaye eta
Euskaldun zintzo garbiak;
Betor guraso zarren fedea,
Betoz asaben legeak,
Bere negarrak leorto daizan
Ni jayo nitzan Erriak.

¡O neure Erri maite maitea!
Zakustaz triste negarrez,
Zure alabak euren buruak
Baltzez estaldu dituez,
Trentza mardoak tiraka atera
Ta sutara bota dabez
Gaur euskaldunak dirala orrek
Ezin ezaguta leikez.

Isildu ziran neskachen kantak,
Mututu artzain-chistuak
Zelai zabal ta jolas-lekuak
Gaur dira basamortuak:
Arroak beera ichasoruntza
Doiazan errekachoak
Gau eta egun chilioz dagoz
¡Ay gara Gazte la koak!

Bakarrik dakust pozgarricho bat,
Euskaldun amen fedea,
Benturaz onek bigundu leike
Jaungoikozko aserrea,
Eta zerutik jatsi barriro
Galdu zan libertadea,
Mundua mundu bizi dakigun
Guernikako Arbolea.