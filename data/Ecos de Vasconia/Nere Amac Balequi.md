---
id: ab-4731
izenburua: Nere Amac Balequi
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004731.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004731.MID
youtube: null
---

Cibillac esan naute
biciro egoqui
Tolosan biar dala
gauzau erabaqui.
Guiltza pian sartu naute
poliqui poliqui
Negar eguingo luque
nere amac balequi.

Jesus tribunalian
Sutenian sartu
Etziyon Pilatosec
Culparic billatu.
Neri ere arquitu
Ez dirate barcatu
Cergatic ez dituzte
Escubac garbitu.

Carcelatic aterata
Fiscalen echera
Abisatu ciraten
Juateco beriala
Ez etortzeco gueyago
Provintzi onetara
Orduan artunuan
Santander aldera.