---
id: ab-4774
izenburua: Iru Damacho
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004774.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004774.MID
youtube: null
---

Donostiyaco iru Damacho
Errenteriyan dendari (bis)
Josten ere badakite baña
Ardua eraten obeki.
Eta kriskitin kraskitin
Arrosa krabelin
Ardua eraten obeki.

Donostiyako gaztelupeko
Sagarduaren gozua,
Anchen eraten ari nitzala
Autzi zitzaidan basua.
Eta kriskitin kraskitin
Arrosa krabelin
Basua kristaleskua.

Donostiyako neskachachuak
Kalera nai dutenian,
Ama, piperrik ez dago eta
Banua salto batian.
Eta kriskitin kraskitin
Arrosa krabelin
Banua salto batian.

Donostiyako iru damacho
Irurak gona gorriyak,
Sartutzen dira tabernarata
Irtetzen dira ordiyak,
Eta kriskitin kraskitin
Arrosa krabelin
Irtetzen dira ordiyak.

Donostiyako iru damacho
Errenteriko kalian,
Egunez oso triste ibilli
Baña dantzatu gabian
Eta kriskitin kraskitin
Arrosa krabelin
Bertako arramalian.

Donostiarrak ekarridute
Guetariyatik akerra
Kanpantorrian ipini dute
Aita-santutzat dutela
Eta kriskitin kraskitin
Arrosa krabelin
Aita-santutzat dutela.

Donostiyako neskachachuak
Mandatuen aitzekiyan
Mutillarekin egoten dira
Kalian jolaskeriyan
Eta kriskitin kraskitin
Arrosa krabelin
Pozez algara aundiyan.

Donostiyako arrantzalian
Dira chit gizon bapuak
Gaztelupeko sagarduakin
Egiten ongi traguak
Eta kriskitin kraskitin
Arrosa krabeliñ
Maiz buztirikan abuak.