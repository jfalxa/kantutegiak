---
id: ab-4795
izenburua: Amorez Eria
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004795.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004795.MID
youtube: null
---

Amorez eri nago
Aspaldi onetan,
Zure gatic maitea
Gau ta egun penetan.
Arqui nezaque poza
Badaquit nic certan,
Sendatuco nitzaque
Zure besoetan.

Ez badazu sinisten
Esan dizutena
Demboraz izango da
Lutuco eguna;
Ala ere eguin bedi
Zedorrec nai dezuna
Ni illa gatic ere
Izazu osasuna.

Ala erabaquiric
Badaucat maitea,
Ez dit pena ematen
Mundua uzteac,
Andiagoac dizquit
Urratzen esteac,
Bestec gozatu bear
Zure birtuteac.

Arentzat dulcia ta
Neretzat samiña,
Besteren escuetan
Nere atseguiña;
Beti aguertuco zait
Zure imagiña,
Beste munduan ere
Emango dit miña.

Ay! nere amante
Nola da posible
Bici alnitequela
Zu amatugabe;
Ori nere biotza
Eguin zaitez jabe,
Ni naicela medio
Ilco ez zerade.

Oh! zure mingaiñ ezti
Espain gorri fiñac
Itz batequin sendatzen
Dizquidatzun miñac;
Amodio onean
Badira ordañac
Entregatzen disquitzut
Biotz ta entrañac.

Gustoz artuco det nic
Zure favorea,
Izana gatic ere
Amore lorea:
Eduqui bear nazu
Barrenen gordea
Beguira ez zaitecen
Mudatu ordea.

Ez dezazula artu
Bildurric maitea,
Ecin uca nezaque
Nere suertea:
Zurequin lotzen nauen
Amore catea
Ecin autsi liteque
Ain dago fuertea.

Eguia esateco
Egon naiz quezquetan
Para ote zenduan
Biotza bestetan;
Zeloac utzi eta
Begui ichuetan,
Entregatzen naiz oso
Zure escuetan.

Aditzen det maitea
Nai didazula esan
Bacarric nai dezula
Zuc nerea izan;
Ala zaduzcac bada
Biotzeco guelan,
Ez du bestec lecuric
Izango seculan.

Ez degu ofenditzen
Ustez Jaungoicoa
Garbia degulaco
Guc amodioa;
Deseo deguna da
Bion unioa
Eliz ama santaren
Matrimonioa.

Biotz baten lecuan
Milla banituque
Guztiya zuretzaco
Izango liraque;
Baña millaren lecuan
Bat besteric ez det,
Artu zazu maitea
Bat au milla bider.

Naitasuna gueyegui
Artzea iñori
Cer gauza gaiztoa dan
Probatzen det ongui,
Atsecabez bete ta
Gau ta egun beti
Ezur uts biurturic
Arquitutzen naiz ni.

Penaric andiena
Da mundu onetan
Azqueneco laztana
Artzeco demboran:
Estutzen da biotza
Itza faltatzen da
Ecin aterarican
Barrendic campora.

Ez da mundu onetan
Neretzat gustoric
Baldin bizi bear badet
Urruti zugandic:
Egunaz esnaturic
Gauaz ametzetan
Beti beti oi nago
Zu gatic penetan.

Nere maite polita
Zutzaz oroitzean
Odola pill pill dabil
Nere biotzean:
Campora irten nairic
Aleguin gucian
Miñ char au bearco zait
Sendatu iltzean.

Berotasun gozo bat
Zañetic zañera
Nere gorputz guztian
Maitea sentitzen da;
Guztoaren pasioz
Sartzen zait icara,
Itzican ere ecin
Asmatu dedala.

Eduqui dezazuque
Ongui sinistua
Ceruac naucala ni
Zuretzat artua;
Ez nazu icusico
Iñoiz mundatua,
Ala eguitendizut
Gaur juramentua.

Adios nere maitea
Seculan betico,
Ezconduco zerade
Ni natorreneco;
Maiteac erantzun dit
Pena andibatequin
Ez naiz ni ezconduco
Ez bada zurequin.