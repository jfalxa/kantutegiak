---
id: ab-4773
izenburua: Kapitan Pillotu
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004773.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004773.MID
youtube: null
---

Ni naiz kapitan pillotu (bis)
Neri beart zait obeditu
Bestela zenbaiten kasqueta
Bombillun Bombillunnera
Buruban jartzen bat zait neri
Bombillun bat
eta Bombillun bi
Eragiyoc Shanti
Arraun orri.

Kapitana da chit beltza
Diruriyena alkitrana
Alkitrana
Bestela... etc.

Barkua da oso fiña
Diruriyena bergantiña
Bergantiña
Bestela... etc.

Kapitan korajetzua
Eta gañera indartzua
Indartzua
Bestela... etc.

Deuseren bildurrik ez du
Iñork ez du au ikaratu
Ikaratu
Bestela... etc.

Chikota berga ta danak
Moldatzen ditu kapitanak
Kapitanak
Bestela... etc.

Ontziyan dabillenian
Marmarizoka geyenian
Geyenian
Bestela... etc.

Pipa badu españetan
Ez da beldurtzen sekuletan
Zekuletan
Bestela... etc.

Bizar luze gorriskara
Olan desaren tankerara
Tankerara
Bestela... etc.

Orra ariyazi dana
Nolakua dan kapitana
Kapitana
Bestela... etc.