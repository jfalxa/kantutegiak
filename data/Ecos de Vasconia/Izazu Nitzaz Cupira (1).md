---
id: ab-4762
izenburua: Izazu Nitzaz Cupira (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004762.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004762.MID
youtube: null
---

Loriac udan itzac becela
maite det dama gaste bat
ari aimbeste nai diyotanic
ez da munduban beste bat.
Yñoiz edo bein pasatzen badet
icusi gabe aste bat,
biyotz gustira banatutzen zait
alaco gauza trizte bat. (bis)

Nescacha gazte paregabia
Apirilleco arrosa.
Izarra beciñ dizdizariya
Choriya beciñ airosa:
Orainchen baño gusto gueyago
Nic eciñ nezaque goza,
Zori onian icusten zaitut,
¡Nere biyotzac au poza!!

Ez aldirazu antzic ematen
Nic zaitutala nayago
¡Ay! mariñelac gau illunian
Izarra baño gueiago;
Nere onduan zauzcatalaco
Pozez zoraturic nago,
Zu icustiac alegratu nau
Triste negüen lenago.

Nic aimbat iñorc nai dizunican
Arren etzazula uste,
Nere beguiyac beren aurrian
Betî desio zaituzte:
Eguzquirican icusi gabe
Choriya egoten da triste.
Ni ez nau ezerc alegratutzen
Zu icustiac aimbeste.

Arpegui fiña gorputza berriz
Ez dago cer esanican,
Izquetan ere graci ederra,
Ezer ez dezu charrican;
Mundu guztiya miratutare
Zu becelaco damican,
Aguiyan izan liteque baño
Ez det sinisten danican.

Nere betico pentsamentuba,
Nere consolagarriya,
Zu gabetanic ecin bici naiz
Esaten dizut eguiya:
Zu baciñaque arbola, eta
Ni baldiñ banitzchoriya,
Nic zu ziñaquen arbol artanchen
Eguingo nuque cabiya.

Amoriyuac nere biyotza
Zureganuntza darama
Erri guztiyan ceren daucazun
Nescach bicañaren fama
Beste fortunic mundu-onetan
Ez det desiatzen dama
Aur batec bérac izan gaitzala
Ni aita eta zu ama.

Falta dubenac logratutzeco
Itz eguitia chit on du
Eta nic ere sayatu biat
Ote guindezquen compondu
Gaur nagon becin atrebituba
Seculan ez naiz egondu
Argatic golpez galdetzen dizut
Nerequîn naizun ezcondu.

Ezcondutziac izan beardu
Preciso gauza char embat,
Ala esaten ari zait beti
Nere consejatzalle bat:
Alashen ere arren esanac
Oso utziric alde bat,
Ongui pozican artuco nuque
Zu becelaco andre bat.

Cerorrec ongui daquizu
Aspaldi ontan nagola,
Zuregatican penac sufritzen
Bañan ordia au nolá:
Alashen ere nigana ecin
Bigundu zaitut iñola.
Ni zuretzaco arguizaya naiz
Zu neretzaco marmola.

Nere biyotza urtzen dijua
Eta ez da misteriyo,
Penaren cargac estutu eta
Sumua quendutzen diyo:
Beguiyac dauzcat gau eta egun
Eguiñican bi erriyo,
Beti negarra dariyotela
Zu ceralaco meriyo.

Zu zeralaco meriyo baldiñ
Juaten banaiz lur azpira,
Guero damuba eta malcuac
Alperric izango dira;
Beiñ juan esquero oyen virtutez
Berriz ez niteque jira,
Ori guertatu baña lenago
¡Izazu nitzaz cupira.