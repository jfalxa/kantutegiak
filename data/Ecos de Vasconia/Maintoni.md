---
id: ab-4750
izenburua: Maintoni
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004750.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004750.MID
youtube: null
---

Gabaz eta egunaz
zugana pensatzen
zure beguiac naube
ni enamoratzen.
Atos atos neugana
Maintoni maitea
nere consueloa da
zu icustea.

Maintoni maitea
Zu icusitzean
Cerbait nic aditzen det
Nere biotzean
Begui beltz ederraquin
Colorea fiña
Maintoni zure ondoan
¿Cer da erreguiña?

Beguiz begui jarriric
Cutunchu maitea
Yruritu estzaidan
Cerua icustea
Nere biotza dago
Consueloz betea
Izango zera laco
Neure emaztea.

Nere biotza dago
Chit negargarria
Ecin consolaturic
Neure amorea
Testigo izango zera
Aitzgorri mendia
Oñaten guelditzenda
Neure amorea.