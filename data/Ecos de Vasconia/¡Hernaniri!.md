---
id: ab-4792
izenburua: ¡Hernaniri!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004792.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004792.MID
youtube: null
---

Ascoc daquite nola nic
luzaro egun bat onic
ez daramala Hernanic;
beti tiroca granada pian
dauca Santiagomendic
gaitz da libratzen argandic
caltiac dirade aundic
eguiten diranac andic.

Bala bat lertzen calian
bestia berriz aidian
beti badira bidian;
lo ezdaguenac
maiz aitzen ditu,
tiro soñubac aldian,
egunaz ala gabian
beti echietan bian
egon biarda gordian.

Urbietaren erriya
lenaz etzana berriya
alperric galdu da iya,
galbai zar baten
ichuran dago,
granada zulos josiya,
consejuba da auciya
joantzan aidian guciya
francoc galduric biciya.

Guerra zan ezquerotz asi
izanda cembait desgraci
gaitz asco dira icusi;
gende asco da
guelditu pobre,
erriya berriz itzusi,
orla ízate da guci
sufritzen diranac bici
placic eztute nai utzi.

Hernani dago penoso
beti tiropian preso
artzen du sembait balaso;
lenaz gain beste
bateriya bi,
berriyac dizcate jaso,
oiyec arturic eroso
gogor diyote eraso
puscatu nai dute oso.

Bombaquin granadac naste
carcac len ere bastante
tira izandu dituzte;
baldin erriya
purrucatuta
artzia badute uste,
beste maquiña bat urte
pasa biarco dituste
eta artuco eztute.

Maiz gaizqui dala ibildu
beñere ezta umildu
honrac ala ditu bildu;
gobiernubac
tituluba ta,
diruba dizca bialdu,
Hernanic dalaric auldu
oso ezdalaco galdu
bere fama du zabaldu.