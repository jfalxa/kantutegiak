---
id: ab-4755
izenburua: Nagusiya Eta Morroya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004755.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004755.MID
youtube: null
---

Francisco cer dacarrec
ic gure erritic
aspaldian ez diat
ango berriric
cerbait jaquin nai niquec
alderdi artatic
aserria niagoc
ez dec milagroric
ez diat egunic
ez eta arratzic
sinista nazac ic
pensamentuz echera
juan gabetanic. (bis)

Morroyac:
Nere nagusi jauna
Echeco berriyac,
Dira ecin gueyago
Negar-garriyac:
Ez ditu sinistuco
Ango picardiyac,
Arras galduric daude
Betico erriyac,
Jende aundi mendiyac
Naiz necazariyac
Chiqui eta aundiyac
Arras galduric dauca
Iya miseriyac.

Nagusiyac:
Zori gaistoz etorri
Zaigun guerratia,
Oroitu bearra duc
Ciñez gendia:
Au sortu zuenaren
Deabruz betia,
Naiz dala apaiz eta
Igual frailia,
Frascucho neria
Duc ascoz obia
Turcuen leguia
Ez oyec predicatzen
Duten fedia.

Morroyac:
Guezurrezco itzaquin
Ez ala ustian,
Chalma paradigute
Jaunac gañian:
Gendiac uste zuen
Ayec aditzian,
Guerra bucatzen zala
Bigaramunian
Biyen-bitartian
Goseac echian
Maquilca lanian
Anchen garabilsquite
Bala tartian.

Nagusiyac:
Pagu ori cenduten
Ondo mereciya,
Gende chiac dic Fraiscu
Culpa guciya;
Estimatu bearrian
Maisterrac nagusiya,
Cerontzat nai cenduten
Gure baserriya,
Gende aundi-mendi-ya
Ecin icusiya
Eta embidiya,
Orra zuen beltz eta
Zuen zuriya.

Morroyac:
Iñola ez guenduben
Nai guerrara sartu,
Fuerzan gaituzte jaunac
Onetaratu:
Ayec maquillaz jo-eta
Beoc predicatu,
Modu ontan gaituzte
Seculaco ondatu,
Jaincuac naibadu
Gaur edo biyar gu
Paquian paratu
Norbaitec egun gorriya
Icusico du.

Nagusiyac:
Fuerzan eguin dizute
Ayec aleguiña,
Jaquiñican norentzat
Cenduten griña;
Chacurrac gaizqui zaunca
Zueri eguiña
Duc oraindican Fraiscu
Daucazuten miña,
Baduc erreguiña
Zuentzaco diña
Ereman eciña
Pagatu bear dezute
Tema ciquiña.