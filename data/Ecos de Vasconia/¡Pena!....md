---
id: ab-4804
izenburua: ¡Pena!...
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004804.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004804.MID
youtube: null
---

Barren neria arkitutzen zait chit penatua
Samintasuna det sentitzen,
Aurcho Chikiya zirazkan daukat gashua,
¡Zerbait diyot nik galdetzen,
Eta ez dit erantzuten!
Naiz goshoro laztandu musuka
Oso triste dit beti begiratutzen¡!
¡¡Birjiñari nere biyotzen erditik
Belauniko oraiñ diyot erregututzen,

Aingeru zoragarriya maitecho neria
Diran alaitzen,
Bestela penarekin ni jarriko naiz
Negar malkoz urtutzen!! urtutzen!!

Malko sendoak ishurtzen zaizkit neri bay orain
Ikusirik aur maitatiya
Illa bezela arkitutzen da kotaru
Ona, asnaserik ere
Etzayo sentitzen iya;
Chit pozikan gogoz nere errayetatikan
Eman nai diyot titiya.

¡Jesus ona! arren ta arren alaitu
Zazu nere aingerucho soragarriya,
Eskeintzen det biyotzetik erregu otoitzak
Eta biziya;
Sendatutziarengatik nere aurcho
Chiki maitagarriya! maitia.