---
id: ab-4826
izenburua: Guernikako Arbola
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004826.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004826.MID
youtube: null
---

Guernikako arbola
Da bedeincatuba
Eskaldunen artean
Guztiz maitatuba.
Eman ta zabaltzazu
Munduban frutuba
Adoratzen zaitugu
Arbola Santuba. (bis)

Milla urte inguru da
Esaten dutela
Jaincoac jarrizubela
Guernikako arbola:
Saude bada zutican
Orain da dembora,
Eroritzen bacera
Arras galduguera.

Etzera erorico
Arbola maitea,
Baldin portatzen bada
Vizcaico juntia:
Lauroc artuco degu
Surequin partia
Paquian bicidedin
Euskaldun gendia.

Betico bici dedin
Jaunari escatzeco
Jarri gaitecen danoc
Laster belaunico:
Eta biotzetican
Escatu esquero,
Arbola bicico da
Orain eta guero.

Arbola botatzia
Dutela pentzatu
Euskal erri guztiyan
Denac badaquigu:
Ea bada gendia
Dembora orain degu,
Erori gabetanic
Iruqui biagu.

Beti egongocera
Uda berricua
Lore ainciñetaco
Mancha gabecoa:
Erruquisaitez bada
Biotz gurecoa,
Dembora galdu gabe
Emanic frutuba.

Arbolak erantzun du
Contus bicitzeco,
Eta biotzetican
Jaunari escatzeco:
Guerraric nai ez degu,
Paquea betico,
Gure legue zuzenac
Emen maitatzeco.

Erregutu diogun
Jaungoico jaunari
Paquea emateco
Orain eta beti:
Bay eta indarrare
Cedorren lurrari
Eta bendiciyoa
Euskal erriyari.