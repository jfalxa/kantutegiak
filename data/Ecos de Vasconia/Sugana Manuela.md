---
id: ab-4813
izenburua: Sugana Manuela
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004813.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004813.MID
youtube: null
---

Sugana Manuela
Nuanian pensatu
Ustedet ninduela
Deabruac tentatu
Icusi beziñ equin
Naiz enamoratu
Ojala es baziñan
Seculan agertu.

Amoriyoz beteric
Esperantza gabe,
Cergatic eguiciñan
Biotz onen jabe:
Suc esan bihar senduben
Emendican alde,
Equiyazqui es naiz ni
Bizar dunen sale.

Astu nai saitut baña
Esiñ, esiñ astu,
Zure amoriyuac
Buruba dit galdu...
Oraiñ bear senduque
Eta maita garri orrek,
Pizcabat lagundu.
Pizcabat lagundu.

Barcatu bear ditutzu
Nere eroqueriac,
Sure beguira daude
Nere bi beguiyac:
Garbi, garbi esanditut
Nere ustes eguiac
Soraturican nauca
Sure arpeguiac.

Gabian ibillinaiz
Gucisco amezetan,
Donostian nengoela
Andre marietan
Eta icusinubela
Erri artaco plazan
Erdiya cebillela
Manuela-cho dantzan.