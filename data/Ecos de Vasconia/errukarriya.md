---
id: ab-4811
izenburua: Errukarriya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004811.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004811.MID
youtube: null
---

Aspaldien ezta gure echean
Oguirik ikusi,
Bañan gaur chichi ta parra
Izango da nazkii.
Gaur bay jana ta edana
labirun lena
jango ote degu dana
labirun la ¡ju! (bis)

Marto; opillak egiteko
Arto egiteko,
Aitak atso saldu zuben
Anega bat arto.
Gaur bay...

Gure sabelak eske daude
Au da miseriya,
Erdi goziak dagona
Da errukarriya.
Gaur bay...

Oso arlote bizi gera
Ezin osaturik,
Zenbat aldiz eguardiyan
Oraindik baraurik.
Gaur bay...

Talo esnea jateko gu
Gaude saleturik.
Sei talo jango nituke
Neronek bakarrik.
Gaur bay...

Pitarra ere guretzako
Izango da noski,
Gure barrena alaya
Jarriko da aurki.
Gaur bay...