---
id: ab-4797
izenburua: Ezcon Berriac
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004797.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004797.MID
youtube: null
---

Ezcon berriac
pozquidaz daude
pozquidaz daude
eguindiralaco gaur
alcarren jabe,
elizan;
Gauza ederragoric
ecin izan ay
orain banengo ni
zuen dichan .
gauza ederragoric
ecin izan ay
orain banengo ni
zuen dichan.

Zori oneco ezcon berriac
Ezcon berriac,
Alai icusten ditutzuen beguiac
Pill pill pill.
Mingaña ishill biotzac itzeguin
Lo piscachoren bat nai dute eguin.

Joan dan gauean quezcaz beteric
Quezcaz beteric,
Lotaraco ez dute izan astirc
Chit asqui.
Zaletuac arquitzen dira nosqui
Jaungoicoac artaraco daucazqui.

Lotaratzean orain ta beti
Orain ta beti,
Jaungoicoaren leguea zuzen eduqui
Gogoan.
Etzai guztiac garaitu ondoan
Bera egon dedin zuen alboan.

Ay au gozoa ta atseguiña
Ta atseguiña,
Sendatuzait betico neducan miña
¡Maitea!
Biotza daucat pozquidaz betea
Ceren ceraden nere emaztea.

Cumplitu baita gure plazoa
Gure plazoa,
Nerea zaitut orain maite gozoa
Betico.
Zu baguetanic ez nuen etsico
Biotz neuretic etzaitut utzico.

Cembait ibilli zure ondoren
Zure ondoren,
Eguin baitet maitea oraiñ baño len
Portizqui.
Egunaz ere baita gauean beti
Nere biotzean zaitut eduqui.

Adi nazazu nere senarra
Nere senarra,
Bost aldiz etorri zait neri negarra
Beguira.
Jaquin nai eta ecin gueiegui fiya
Benaz ote zebiltzen nere billa.

Neure biotzat zure ganaco
Zure ganaco,
Daucan amoriyoa ez du lagaco
Beñere.
Jaquiñic arquitzen cera len ere
Zere imbidian cembait badaude.

Lotara goaz gende prestuac
Gende prestuac,
Ez gaitu icaratzen gaurco castubac
Iñondic.
Jan ta edan naierara eguiñic
Dantzatu gabe ez joan emendic.

Ezcon berriai ondo beguira
Ongui beguira,
Nere uzte guztian logale dira
¡Ez guchi!
Gau gozo au nequez dute irichi
Zoazte lembai len ta atea itchi.

Oeratzean, ay ura poza
¡Ay ura poza!
Gozotoro esanaz maite biotza
Alcarri.
Ezquer onac eman Jaungoicoari
Nai erara zaituztelaco jarri.

Itzaldi goisho ezti eztiac
Ezti eztiac,
Gau artan oidirade guzti guztiac
¡Enequi!
Cer esan ta eguin badute beti
Arras macalduric oidira jequi.