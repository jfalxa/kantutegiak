---
id: ab-4751
izenburua: Fueroac
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004751.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004751.MID
youtube: null
---

Adierazo naidet
izqueta claroan,
eusquera ta fueroac
noiztic nola diran.
Aspalditic neucan
jarria buruban,
iputzen oneraco
eguingo otenuan.

Noeren familico
Tubal ceritzona,
Armeniatic onera
citzaigun aurrena:
arengatican degu
izcuntz aiñ ederra,
zarragorican ez dan
maitatsu eusquera.

Egipcio, Caldeo,
Hebreo, Griegoac,
Cartagines, Romano,
Alano, Sueboac;
Vandalo, Godo, Arabe,
eta Sarracenoac,
Españira cituzten
chit guerra gaistoac.

Emengo mutillaren
corage firmeac,
Españiatic bota
citun erregeac:
eta maite gaituzte
gure erregeac,
mereci becelashe
euscaldun gendeac.

Ezda cer duda eguin
gu guera aurrenac,
Portugaldic Francira
denetan zarrenac:
Jafeten bostgarren seme
Tubal onarenac,
Españien asiera
eman guenduenac.

Noiceticacoa dan
gure izatea,
ez da posible orain
nic esplicatzea;
sayatu naiz aleguin
ori billatzea,
alferric izandu da
nere necatzea.

Libre vicitu ciran
republica onetan,
iñoren mende gabe
lenengo urtetan:
milla eta eun da
oguei ta irutan,
nafarraquin aurrena
adiskuldetu zan.

Ala seguitu zuten
aldicho batean,
asarres apartatu
ciraden artean:
ecin compondurican
bear zan paquean,
milla eta berreun
garrengo urtean.

Guerra besteric ez zan
dembora ayetan,
eta pensatu zuten
estadu onetan:
laguntza izateco
estutasunetan,
bear zala unitu
erdaldun gendetan.

Alfonso zortzigarren
zori onecoac,
beregana guinduzen
Guipuzcoanoac:
aguintzen cizquigula
guinducen fueroac
Erregue maitagarri
Castilletac oac.

Oec gustiyac ciran
tratu iltzezcoac,
ala seguitu zuten
dembora artacoac:
gordeaz osotoro
aleguin fueroac,
gure gobernatzalle
provinciacoa.

Emengo alcateac
bilduric Tolosan,
escribi citezela
denac agindu zan:
bai eta eguiñ ere
alzuten prestezan,
provinciac calteric
izandu ez cezan.

Milla irureun ta
oguei ta amabost zan,
alcateac bilduba
gure erri Tolosan:
eta escribituric
urte artan bertan,
erregueri eramana
aprobatu citzan.

Enrique bigarrena
cegoen Tronoan,
Abendu illac oguei
cituen orduan:
confirmatu cizquigun
bear zan moduan,
merecia dezala
gozatu ceruan.

Ez da bada dudaric
len ere baciran,
bestela confirmatzen
arico ez ciran:
ascotan ibilli dira,
gira eta biran,
gorde dituzteneco
guizonac baciran.

Unio on bat bear ta
elcarren artean,
erriac aurreratu
oidute lanean:
eta guerraric balitz
noiz bat ondorean,
españolac guerade
gustien ganean.

Ez beldurric izan,
euscaldun gendea;
gordea degu ongui
fueroen leguea:
erreguiña daucagu
eziñic obea,
maite gaituen ama
pareric gabea.

Obligacioz da eta
joan bear guerrara,
legue maite garriac
gordeco badira:
Tangerren ipiñiric
español bandera,
dembora labur barru
echeraco guera.

Oh soberana eder
pobrearen ama,
contentuz beterican
gaute gu zugana:
ongui mereci dezu
daucazun fama,
Jaunac eman deizula
zuri osasuna.

Viva euskal erria ta
viva erreguiña,
elcarri laguntzera
eguin aleguiña:
noiz bat campotarretaz
estutzen baciña,
emen daucazu gende
erne eta fiña.