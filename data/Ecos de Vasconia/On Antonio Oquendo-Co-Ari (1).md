---
id: ab-4742
izenburua: On Antonio Oquendo-Co-Ari (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004742.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004742.MID
youtube: null
---

Cantastea:
Gaur daucazu esanaz
Cere Donostiyá,
Eguiñ nay dizugula
Ongui etorriya.
¡Oh! Euskaldun erriyen
Izar guidariya!
¡Oh! gure biyotzeco
Oquendo aundiya!
¡Oh! umant (1)
¡Oh! illezcor (2)
Garaitez garriya (3)
¡Oh! pareric gabeco...
Itsas gudariya (4)

¡Ondo Euskerascua
Da zure biyotza!
Daquiyena cer diran
Deshonra ta lotsa.
Da FedeSantubaren
Atseguin ta poza,
Da Ceruban erdiyan
Dagoan arrosa. (5)

¡Cembat! ¡cembat! lezaquen
Benazco Fediac!
¡Lotsaturic uzteco
Lutero zaliac!
Mundubac badaquizqi
Zure birtutiac,
Dirala eun bider
Bentzutez-garriac. (6)

Argatican daucazu
Ceruban cantari
Biyotz biyotzetican
Aita Larramendi
Churruca, Bidazabal
Lezo ta Legazpi
Cantari daucazquitzu
Elcano ta guci. (7)

Euskaldun eguiyazco
Zar ta gazteriyac,
Zu icusteco gaude
Guztiz edarriyac.
Eguiñgo dizquitzu ¡bay!
Zure Cantabriyac,
Mereci dituzuben
Ongui etorriyac.

Bici diran artian
Ibero mendiyac,
Ez dira, ez, aztuco
Ez euskal gloriyac.
Adieracico du
Zure Oroicarriyac
Cein diran Guipuzcoan
Ondo meriyac. (9.