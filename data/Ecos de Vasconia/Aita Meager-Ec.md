---
id: ab-4791
izenburua: Aita Meager-Ec
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004791.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004791.MID
youtube: null
---

Ni naiz chit gauza gozoa
eta pozquida osoa
beltza naiz ta zuria
illuna ta arguia
indarra naiz ta garboa
eta icena det ardoa.

Noe guizon aditua
Zedorren pentsamentua
Izanzan gauza aundia
Ipintzea mastia,
Zu zaitugu ardoaren autore
¡Oh zorioneco Noe!

Edari maitagarria,
Tristearen alegria,
Dezu alaitzen beguia
Quentzen melancolia
Mutuba ipintzen cantari
Eta errena dantzari.

Ausi, atera, ebaquia,
Llaga dala, edo zauria
Kuratzeco belarra
Clareta edo nafarra
Moscatela edo malaga,
edoceiñ ardo ona bada.

Pasatzen du aste osoa
Aitzurtzen batec soroa
Necaturic guztia
Dariola izerdia,
Laster legoque au galduric
Ezpalu jayean ardoric.

Jateko ez bada gogoric
Eta eciñ eguin loric,
Eritasun obien
Edo beste edoceñen
Kuratzeco balsamoa
Da matz onaren zumoa.

Ardo gabe ez da funcioric
Ez mezaric eta ez eztairic,
Bada au faltatu ezquero
Ez da arquitzen guero
Baizic naigabeco tristura
Illuntasuna ta malura.

Galenoren medecinac
Ta farmacia guztiac
Du botiquic onena
Ardoaren taberna,
Todo lo sana guztia
Ardo ona daucan zaguia.

Barbero medicu guztiac
Erremedio aundiac
Dituzte besterentzat,
Baña bada berentzat
Erremedioric oberena
Ardo zarric dan onena.

Ardoa eranic neurriz
Beiñ illa pizten du berriz,
Baña bada asitzen
Belaunac limuritzen
Eta iraquiten burubac
Ez da gauza onic orduan.