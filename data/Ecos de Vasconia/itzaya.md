---
id: ab-4766
izenburua: Itzaya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004766.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004766.MID
youtube: null
---

Bautista Bazterreche
mutico pijua
neri gurdi ardatza
neri gurdi ardatza
ostuta dijua.
belchac eta churiyak
ibillico zaitu
neri gurdi ardatza
neri gurdi ardatza
emate ez baidazu.

Gurdi ardatza ezik
Itai ta aria,
Besteren biar gabe
Badaukat neria,
Sega poto, labaña
Gañera bostortza
Periyan erosiya
Daukat nik zorrotza.

Nere iri pariak
Dauzka zintzarriyak
Lepotikan zintzilik
Kollarez jarriyak
Gañera ustarriyak
Ta kopetekuak
Ontza urre onakik
Erositakuak.

Zeronek naiko gauza
Dezula diyozu
Bañan gurdi ardatza
Neri ostu nazu
Pagatu bear dezu
Larrutik ederki
Ez badezu ardatza
Entregatzen aurki.

Buru gogorrak badu
Berekin kaltia
Ez detan gauza nola
Naidezu ematia
Sentzuak galdurikan
Arkitzen zerade
Beztela behintzat orla
Ariko etziñake.

Gezur ta bar zabiltza
Bautista, tranpiyan
Zu beziñ gezurtirik
Ezta probintziyan
Zure berri dakite
Inguruko denak
Ibiltzen dituzula
Gauza besterenak.