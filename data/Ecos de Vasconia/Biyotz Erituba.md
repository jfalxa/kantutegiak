---
id: ab-4735
izenburua: Biyotz Erituba
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004735.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004735.MID
youtube: null
---

Ascoc diyote "cantetan beti
Amore contu naicela,"
Nic esaten det, arriturican
Motivo gabe daudela;
¡Ser eguin biat baldin cerubac
Doaytu banau orrela,!
¿Ez dute icusten usaya mendac
Bere jatorrez dubela?
Nere biyotzac amoriyua
Mendac usaya becela.

Triste bicinaiz eta
Illco banitz obe,
Badauzcat biyotzian
Sembait atzecabe;
Dama bat maitatzen det
Bañan...aren jabe,
Ceculan isateco
Ezperantza gabe.

Nere biyotz gashua
Penatuba dago,
Eciñ egon liteque
¡Ay! Penatubago;
Pasatzen ditutala
Azpaldiyan nago,
Egunac triste eta
Gabac tristiago.

Amorez erituba
Sembait dembora ontan,
Gauric ez det pasatzen
Soseguzco lotan;
Penaren cargarequiñ
Urtuta, ¡francotan,
Campora irteten zait
Biyotza malcotan!

Aushen da miña laister
Ill biar naubena,
Cupitutzen ez bada
Senda lezaquena;
Aiñ arquitutzen zaizquit
Sartubac barrena,
Animan tristura ta
Biyotzian pena.

Biyotza eciñ jaso det,
¡Au carga barrenac,!
Iya gastatu zaizquit
Indarric gueyenac;
Ez luque shinistuco
Progatu ez dubenac,
Sembat pisatzen duben
Eguiyazco penac.

Anima tristuraquiñ
Aiñ dacat negarti,
Nun eguiñ aldezaquen
Arriya bi parti;
Adorantzac ematen
¡Belaunico beti,!
Biyotzian daucatan
Imagiña bati.

Ceiñ ote dán eciñic
Contura erori,
Cuticiya jayoco
Zayote ascori;
Nic ordian ez diyot
Esango iñori,
Berac badaqui eta
Bastante det ori.

Nere maitatuba da
Guztiz dama fiña,
Biyotz onecuata ta
Ondo itzeguiña;
Bentaja guztiyaquiñ
Cerubac eguiña,
Munduba pasatzeco
Lagun atzeguiña.

¿Certaco desiatu
Ditu gausa on oyec,
Nere biyotz coitadu
Guisharashua onec,?
Culparen castigutzat
Datozquit pena obec,
Mereci ditutala
Badaquit neronec.

Aingueru zoragarri
Mirabe laztana,
Pentzamentu nerian
Beti zauzcatana;
Biyotzaren erditic
Maite zaitutana,
Zu cera neretzaco
Nai cinduzquetana.

Nere amoriyua
Aiñ fiña isanic,
Beti goguan zauscat
Aztu gabetanic;
Ez da mundu guztiyan
Iñor capaz danic
Dama, zu maitatzeco
Aimbeste nola nic.

Beguiyaren aurrian
Beti nay cinduzquet,
Icusten ez bazaitut
Nay baguia maiz det:
Gogoratu bagueco
Minuturic ez det,
Amoriyoz maitatzen
Ez daqui nic bestec.

Etzaitut icusitzen
Desio aimbeste,
Dicha ori logratzendet
Iñoiz edo beste;
Eta ez detanian
Logratzia uste
¡Ura triste ibiltzen naiz,!
¡Jesus, ura triste.!!

Eciñ bici zu gabe,
¡Au amoriyua,!
Logratzen ez bazaitut
¡Cer martiriyua;!
Ez badezu desio
Nere eriyua,
Zuregan arquitzen da
Erremeriyua.

"Erregututzen dizut"
"¡O Jesusen ama,!"
"Nitaz cupi derilla"
"Maite detan dama;"
"Biurtzen badirazu"
"Biyotzera calma,"
"Cantaz banatuco det"
"Milagruen fama.".