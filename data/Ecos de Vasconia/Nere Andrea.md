---
id: ab-4757
izenburua: Nere Andrea
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004757.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004757.MID
youtube: null
---

Ezcongayetan cerbait banintzan /
ezcondu eta ecerez / (bis)
Ederr salia banintzan ere
aspertu nintzan ederrez
Nere gustua eguin nuben ta
orain bici naiz dolorez.

Nere andrea andre ederra zan
Ezcondu nitzan orduan,
Mudatuco zan ezperantzaric
Ere batere ez nuan;
Suric batere baldin badago
Maiz dago arren ondoan.

Zocoac ciquin, chimac jario,
Aurra cincilic besoan,
Adobaquia desegoquia
Gona zarraren zuloan,
Iru chiquico botella andia
Dauca berequin alboan.

Nere andrea alferra data
Ez da munduan bacarric
Gauza gozoen zalea da ta
Ez du eguin nai bearric,
Seculan ere ondo izango ez da
Alacoaren senarric.

Larumbatetic larumbatera
Garbitzen ditu zatar bi,
Bere ayechec berotutzeco
Egur erretan zama bi;
Belaun bietan bana artuta
Ez da isiltzen cantari.

Nere andrea goiz jaiquitzenda
Festara bear danean,
Buruco miña eguiten zayo
Asi baño len lanean:
Zurequin zer guertatuco zan
Nic bildurric ez nuan.