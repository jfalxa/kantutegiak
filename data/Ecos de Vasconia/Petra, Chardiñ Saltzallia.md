---
id: ab-4759
izenburua: Petra, Chardiñ Saltzallia
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004759.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004759.MID
youtube: null
---

Treñerua kayera,
goizian goiz aldera,
chardiñ preskuarekiñ
sartzen bada:
naiz mototsa zintzillik
bi saltuan isillik,
lendabiziko Petra
kayian da.
Ekatzu baldecho bat
baña ongi betia,
Martiñ nere maitia
chestu ontan.
Segiduban kalera,
juan nariyen saltzera,
esnatubaz daudenak
oraindik lotan.

Coro:
Chardiñ berriyak,
bizi biziyak,
Chesto ontatik
zaizkit juaten,
mokaru goso
preskuagorik
Dukiak ere
ez du jaten.

Ez da ostaturikan,
Artzen ezdubenikan,
Udako fruta eder
ontatikan;
Ez det ezagutzen nik
Gustatzen etzayonik,
Bakar bat esateko
oraindikan.
Zartayian prejitzen,
Zabal zabal jarriyak
Chardiñ berri biziyak
masiyakua;
Eta beren mamiya,
Gogorra ta loriya,
Lupiyarena baña
gozuagua.

Coro:
Chardiñ berriyak,... etc.

Sagardotegiyetan,
Goiz eta artsaldetan,
An ere chit ederki
dira jartzen;
Brasa bizi biziya,
Estalirik guziya,
Biatz puntak erriaz
dira artzen.
Eta lenaz emanak
Badira bi gatz ale,
Nola ez izan zale
ayetara.
Prejitubak, erriak,
Zeiñ baña zeiñ obiak
Gosuak dira alde
denetara.

Coro:
Chardiñ berriyak, ...etc.

Ollagor ta eperrak,
Dirala chit ederrak,
Chiki chikitandik det
adituba:
Baña mokadu fiña,
Chardiñ bizkar urdiña,
Atze aldetik ondo
okertuba.
Arranbarrill gañian
Jarritzian erretzen,
Begira irrikitzen
bere gana.
Ez da nolanaikua,
Alafedekatua,
Galiziyan kapoitzat
jaten dana.

Coro:
Chardiñ berriyak, ...etc.

Ara goyan Benita,
Cheletaka jarrita,
Chokaturikan dago
farrez lertzen:
¡Baña begira orri
Koloriak zer gorri!
Aitatu detalako
zaizkan jartzen.
Oraiñ zubengandikan,
Penaz utzirik dana,
Nere aurraren gana
nua echera.
Pisoyala churiyak,
Badaukazki bustiyak,
Mudatu ta bularra
ematera.

Coro:
Chardiñ berriyak, ...etc.