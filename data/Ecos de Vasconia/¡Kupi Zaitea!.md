---
id: ab-4744
izenburua: ¡Kupi Zaitea!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004744.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004744.MID
youtube: null
---

Nere betiko ametz gozoa
izar eder gidariya
barren nerera begiyetatik
banatutako argiya.
Erotzeraño amoriyuak
naukazugana jarriya
zu zeralako meriyo daukat
biyotz gashua eriya.

Zuregatikan nere barrenak
¡Ai! Xenbat duben sufritzen,
Suspiriyoka biyotza beti
Dabillela det sentitzen;
¿Zu-gatik orren triste dagola
Ez aldirazu igartzen?
Eta jakiñik au orla dala
Nitzaz etzera kupitzen?

¡Kupi zaitea! Lenbailen nitzaz
Bestela jarko naiz miñez,
Estuasun ta naigabe abek
Geyago sufri eziñez;
Esaten dizut maite neria
Biyotzetikan au ziñez,
Zu maitatuaz naidet nik bizi
Beti amoriyo fiñez.