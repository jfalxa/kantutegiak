---
id: ab-4827
izenburua: Aita San Ygnacioren Marcha
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004827.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004827.MID
youtube: null
---

Ygnacio gure Patroi aundia
Jesusen Compañia
Fundatu
Eta dezu armatu:
Ez da ez atsairic
Jarrico, zaizunic
Iñolaz aurrean
Gaurco egunean:
Naiz betor
Lucifer deabrua
Utziric infernua.
Zure soldaduac
Dirade aingueruac,
Zure guidaria,
Da Jesus aundia,
Garaitu dituzte
zure anayac
Etsayac
Ez dauca
Fedeac
Ez cristau nereac,
Ez dauca bildurric
Iñungo, aldetic:
Ignacio or dago,
beti ernai dago
Or dauce gendea
Chit garaitzallea
Bandera alchaturic
Guerran azaldu nairic.
Gau eta egun
Guztioc pakea dezagun
Beti gau eta, egun.

Ygnacio, bildu dezu munduan
Arritzeco moduan
Gendea
Fedez bici betea,
Gende jaquintsua
Eta indartsua
Beti dabillena
Guerretan aurrena,
Elizaren etsayac billatzen;
Tapatu ta garaitzen;

Dituzu anayac
Guerra eguin-nayac,
Da oyen lenguea
Etsai garaitzea;
Oyec ditu bete gordetzalleac
Fedeac:
Dirade ezagun;
Dabitza gau ta egun
Europan, Asian,
Africa, American:
Legorrez ta ichasoz
Dijoaz ta datoz,
Dabiltza nequean
Indio tartean
Edo erregue-echean
Jesus-en icenean
Beti pelean,
Bicitzac dirauben artean
Beti beti pelean.

Ygnacio, dira zure anayac
Ichas-guizon arguiac
Arraunac
Bogatzen daquitenac;
Pedro-ren ontzia
Badago ertzia
Arroca tartean
Egunen batean,
Bertatic botean dira sartzen
Eta argana joaten.

Socaquin loturic
Arroquen artetic
Baldiñ bada etsairic
Oyec garaituric,
An daramate ontzia cayera,
Leorrera.
Naiz izan ecaitza
Bogatzeco gaitza.
Eta baguen goyac
Naiz busti odeyac,
Arraunac arturic,
Alcar alaituric,
Botean sarturic,
Bicitzaz azturic,
Boa boa deiric,
An dijoaz cayetic
Bultzeaz quilla,
Pedro-ren ontziaren billa,
Beti bultzeaz quilla.