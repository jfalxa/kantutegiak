---
id: ab-4798
izenburua: Parerik Gabea
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004798.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004798.MID
youtube: null
---

Dama gazte bat bada
gure donostiyan,
igualik estubena
españi gustiyan;
milla graci badauska
bere arpegiyan,
artuke nuke pozik
besuen erdiyan.

Aingueru ori maite
det aspaldi onetan,
zeñaz icusten detan,
jai arratzaldetan;
berak pentsatzen eztu
zer miñ klase detan,
sendatuko lirake
baleki zer detan.

Ainda ikusgarriya
dama gazte ori,
beguiratzeko gogoa
ematendit neri;
Donostiyan ez dira
orrelakose bi,
zerutik jechitako
izarra diruri.

Gorputz liraña dauka
arpegui chit fiña,
guerriyak badiruri
moldian eguiña;
biziro oñ politak
pausua ariña,
ezin fama liteke
ark mereci diña.

Buruan ille beltza
diz-diz-ka darama,
saleruan ere ark
du guztisko fama;
¿nork eztu maitatuko
orrelako dama?
Biotzak arengana
ni beintzat narama.

Ezin bizi naiz ondo
ni mundu onetan,
biziro suerte charra
det gauza denetan
biotzetik eskatzen
dizut chit benetan,
es eruko asteko
lusaro penetan.

Izar gustiz arguiya
goitik amilduba,
zuregatikan daukat
biotza sutuba
gañera arkitzen-naiz
oso argalduba,
amoriyua nauka
onela galduba.

Eguiya esaten det
sentimentubakin
ezcondu nai detala
betiko zurekin;
izango giñake gu
ondo elkarrekin,
¿zeñen ez izan dicha
aingueruarekin?

Zerutic jechitaco
izarra zera zu,
arguiya beguitican
bigaldutzen nazu;
planta charrian nago
arren arnazazu,
eta biotz sutuba
itzalduco nazu.

Nitzas cupiturican
arnazu betico
zu gabe ezdet bada
munduban etzico;
artutzen ez banazu
jarconaz tisico;
eta dolorietan
zugatic naiz ilco.