---
id: ab-4793
izenburua: Nere Maite Polita
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004793.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004793.MID
youtube: null
---

Nere maite polita
Nola zera bizi
Zortzi egun onetan
etzaitut ikusi.
Uste det zabiltzala
nigandic iguesi
ez dirazu ematen
atsekabe guchi.

Amoriyoz biyotza
daukat nik zulatzen
¡zuregatikan zenbat
detan nik sufritzen!
¿zergatikan etzera
zu nitzaz kupitzen?
¡zuregatik ari naiz
ezur utz gelditzen!

Zu zerala meriyo
nago argaldua
lengo trinkutasuna
osoro galdua
maitetasunak nauka
onela auldua
¡ez nazazula utzi
desanparatua!

Naitasunak indarra
artzen dubenian
erruak usten ditu
biyotz barrenian;
fiya zaite maitia
nere esanian
arnazazu betiko
zuk aldamenian.

Zuretzako jayo naiz
zuretzako azi,
zuregatikan nai det
mundu ontan bizi,
zu gabetanik ezin
det iñola etzi
éz nazazula bada
penaz iltzen utzi..

Kupitutzen bazera
naidetan moduan,
sendatu ta ikusiko
nazu ni orduan;
¡atsegiñez beterik
elkarren onduan,
zer ederki biziko
geraden munduan!

Aingeru bat bezela
zaitut maitatuko,
bear dezun gauzarik
etzaizu faltako;
ditutan gauza denak
dira zuretzako,
nere aingeru polit
maitiarentzako.