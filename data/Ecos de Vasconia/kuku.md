---
id: ab-4752
izenburua: Kuku
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004752.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004752.MID
youtube: null
---

Kukubak kantatzen du (Kukukuku)
mayatzian kuku.
Andriak badituste
milla matrikula. (Kukukuku)
Oyian etzaten dira
Gashorik daudela
Esta mauko chulotik
Sentitzen dutela.
Eta zergatik (kuku)
chokolatiagatik (kuku)
mama goshuagatik (kuku)
mishtelagatic (kuku)
kariñenagatik
Bisirik es alaiz jaikiko
su emanta baisik.
Kukukuku.

Kuku kanta achuak
Ondo aritzen dú;
Berarekin poztasun
Ona sentitzen dú,
Tripako miñ aitzekiz
Etzanik oyera
Ondo egiten dute
Mintsuben papera;
Eta zergatik,
Piper-opillagatik
Arkumiarengatik
Ardo onagatik,
Naparruakotik,
Bizirik ez alaiz jaikiko
Su eman ta baizik.

Zenbat barren triste du
Kuku kantak iku;
Biyotzean askotan
Sentitzen du kuku,
Ondo senti eztala
Esanik askotan,
Aitzekiz gelditzen dá,
Oi-gañean lotan
Eta zergatik,
Muthur okerragatik
Rape autzarengatik
Churrutagatik,
Sagarduagatik,
Bizirik ez alaiz jaikiko
Su eman ta baizik.