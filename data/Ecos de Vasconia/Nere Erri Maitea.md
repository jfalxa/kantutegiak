---
id: ab-4803
izenburua: Nere Erri Maitea
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004803.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004803.MID
youtube: null
---

Nere erri maitea
Nik zaitut maitatzen,
Gau ta egun zurekin
Nago ni pentsatzen;
Zuk diyazu biyotza
Pozkidaz betetzen
Naizelako lur gozo
Onetan arkitzen.

Donosti eder maite
Chukun ta garbiya
Zuretzako daukat nik
Naitazun guziya;
Zera atzegiña ta
Begira garriya
Euskaldun semearen
Pozezko Kabiya.

Erbestera emendik
Joaten naizenian,
Nai gabian sentitzen
Det nik Biyotzian;
Bañan kanpotik onuntz
Ni natorrenian
Poza nabaitutzen det
Nere barrenian.