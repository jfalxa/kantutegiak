---
id: ab-4771
izenburua: Damacho Bati
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004771.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004771.MID
youtube: null
---

Zure zure begiyak
dizdi dizdizariyak
badirudite izar bi
badirudite izar bi
aiñ dira argiyak
eta garbiyak
ichu mustutzen naute ni
ichu mustutzen naute ni.
aiñ egokiyak
non zure biyak
iñon ezin leikez arki
iñon ezin leikez arki.
¿Printza biziyak
nola eguzkiyak?
ala egiten dute argi
ala egiten dute argi.
Zure begiyak
Dizdi Dizdizariyak
ichu mustutzen naute ni. (bis)

Zure gerriya,
Aiñ egokiya,
Torniatua dirudi;
Zoragarriya
Ta pozgarriya
Dezu esku polit ori;
Eta arpegiya,
Zuri gorriya,
Aurrez jartzian iñori
Zuk embiriya,
Bañan aundiya
Jartzen diyozu denari.

Zutzaz pentsatzen,
Nago penatzen
Jarri zait biyotza eri;
¡Zembat zulatzen
Eta nekatzen
Dirazun barrena neri;
Zuknazu aultzen
Orla medartzen
Kandelen giza naiz arki
Orlashe urtzen
Eta simurtzen
Zugatik ilko naiz aurki.

Ara berrendik
Zer diyotan nik
Biyotzez egi egiya
Biyak gaur dandik
Asmo bat nairik
Ar zagun asmo berriya
Biyak maitarik
Chit elkar nairik
Egingo degu kabiya
Legez baturik
Elkar arturik
Sen-datuko da eriya.