---
id: ab-4761
izenburua: Nere Etorrera
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004761.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004761.MID
youtube: null
---

Ara nun diran mendi maiteac
Ara nun diran zelaiac.
Basarri eder zuri zuriac,
Iturri eta ibaiac!
Hendayan nago choraturican
zabal zabalic beguiac
Ara España lurr oberican!
es tu Europa guciac.

Guero pozic bai Donostiara,
Oquendo-arren lurrera,
Ceru polit au utzi bearra,
Nere anayac au pena!
Yru chulueta maitagarria
Lore toquiya zu zerá,
Veneciaren graci guciac
Gaur Donostian badirá.

Oh Euskal-erri eder maitea
Ará emen zure semia...
Bere lurrari mun eguitera,
Beste gabe etorria:
Zuregatican emango nuque
Pozic, bai, nere bicia,
Beti zuretzat ill arteraño
Gorputs ta anima guciac.

Agur, bai, agur Donostiaco
Nere anaya maitiac,
Bilbaotican izango dira
Aita-zarraren berriac,
Eta gañera itz neurtuetan
Garbi ezanez eguiyac,
Sud-American ser pasatzendan
Jaquin dezaten erriyac.