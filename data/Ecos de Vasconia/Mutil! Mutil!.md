---
id: ab-4776
izenburua: Mutil! Mutil!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004776.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004776.MID
youtube: null
---

Mutil, mutil
Jaiki ari
Eguna ote dan
begira ari.
Naguziya
Eguna da
Gure ollarra
kampoan da.

Mutil, mutil
Jaiki ari
Eudia ote dan
Begira ari
Naguziya
Eudia da,
Gure zakurra
Bustiya da.

Mutil, mutil
Jaiki ari
Suba ote dan
Begira ari
Naguziya
Suba bada
Gure gatua
Beroa da.

Mutil, mutil
Azkar abill
Echean zerdan
Begira ari
Naguziya
Aizea da
Gure leyua
Iriki da.

Mutil, mutil
Jaiki ari
Kanpuan zerdan
Begira ari
Naguziya
Elurra da
Kanpua churi
Eztali da.

Mutil mutil
Mugi ari,
Erdi lotatik
Esna ari
Naguziya
Egiya da
Beon mutilla
Logale da.

Mutil mutil
Azkar abill
Errekan zerdan
Begira ari
Naguziya
Ardiya da
Bañan aspaldi
Itua da.

Mutil mutil
Begira ari
Zer dan birian
Or ageri
Naguziya
Betroya da
Nonbaitetikan
Itzuli da

Mutil mutil
Zer duk ori
Auso echian
Dana ageri
Naguziya
Arbola da
Basotik gaur goiz
Ekarri da.

Mutil mutil
Esan zak ik
Baldin ta badek
Beste gauzik
Naguziya
Egiya da
Nere buruba
Nekatu da.