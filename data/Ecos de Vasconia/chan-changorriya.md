---
id: ab-4817
izenburua: Chan-Changorriya
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004817.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004817.MID
youtube: null
---

Arbol baten gañian
zegon jarriya
Choriyetan arguiyena
Chanchangorriya
Mutill muquizu batec
tiradiyo arriya
lurrera eroriric
gashua eriya
choriyetan arguiyena
Chanchangorriya.
Guraso eta maisu ta
jende guziya
zuben mende arturican
Chanchangorriya
esan mutill coscorrari
au dalaco eguiya
ez dala beñere ill
biar alaco choriya
guztiyetan arguiyena
Chanchangorriya.