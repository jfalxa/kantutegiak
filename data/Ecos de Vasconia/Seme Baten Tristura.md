---
id: ab-4788
izenburua: Seme Baten Tristura
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004788.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004788.MID
youtube: null
---

¡Zeru garbiya odoi illunak
zer nola duben tristetzen!
ala berian nere barrunak
tristura du gaur sentitzen;
pentsaturikan lenago egunak
nola nituben estitzen,
bañan gaur ¡ah, bai! samintasunak
gogorki nau ni estutzen.

Arraultz barrenan oidan moduan
gordetzen sumoa osorik,
ala berian nere sentzuan
nituben pozak gozorik;
bañan azala autzitakoan
ez da gelditzen sumorik,
ala, izate barrenekuan
ez det arkitzen pozarik.

Amaen biyotzak katetasuna
zeukan nerekin batian,
zori-ona ta paketasuna
zan beti gure artian;
amoriyo ta maitetasuna
genuben indar betian,
guretzat dena zan ontasuna
bizitzen giñan pakian.

Len ez nekiyen nik zer zan pena
bañan oraiñ det ikasi,
alegrantziya nuben len dena
pozez nitzan beti bizi;
bañan ontasun denen azkena
etortzen det gaur ikusi,
ta biyotzeko atzegin lena
ni-gandik joan da igasi.

Mundu ontatik joanda zerura
nere amacho maitia,
aingerucho bat oidan modura
merezimentuz betia;
etzan komeni nonbait poz gille
nerekin bat izatia,
kumplitu bedi gure Egille
jaunaren borondatia.

¡Au bakardade mingarritsua,
biyotz neria estu da,
nere atzegiñ maite gozua
joan zaitalako zerura;
barren neria aiñ gozotzua
joan zait larri chit estura,
nere amacho maite gashua
gabe, aushenda... ¡TRISTURA.