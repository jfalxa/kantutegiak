---
id: ab-4747
izenburua: Cembait Damaren Ezquer Charra (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004747.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004747.MID
youtube: null
---

Atzo provechu gabe
Echeco auzuan
Egondu nitzan
zure zay
egun osuan. (bis)
Gaur suertez icusitzen
zaitutan casuan
Itz eguiñ biar dizut
Izqueta gozuan. (bis)

Cein ta politac diran
Arrosa, jasmiña,
Azucena, liríyo
Eta claveliña:
Danac bat eguiñ tare
Ez dirade diña,
Igualatzeco nere
Maite atzeguiña.

Nay det jaquiñ dezazun
Oraiñ beriala,
chit asco nay dizutan
Nescacha cerala:
Gaur su icusitziac
Poztutzen nau ala
Nun iruritzen zaitan
Jay aundi bat dala.

Au da mundu guciyac
Ongui daquiyena,
Zu cerala dama bat
Graci utza dena:
Bat, oyetan badezu
Erotzen naubena,
Artan arquitzen dizut
Graciric gueyena.

Izan ere gorputza
Dezu aproposa,
Icusi utzarequiñ
Ematen du posa:
Beste iñor ez dala
Zu beciñ airosa,
Erri gusiyan dago
Banatuba vosa.

Pacharan ibiltzia
Dago gaur bolaran,
Pausage fiña oraiñ
Ser gatic moda dan:
Eta zu icustia
Modaco pacharan,
¡Ez daquizu neretzat,
Nolaco dicha dan!

Nic esan ta zuc jaquiñ
Biazu ecican,
Lau andre gay dacazquit
Artu nai ecican:
Ezconduco nitzaque
Gaur bertan casican,
Besterequiñ ez bañan
¡Zurequiñ, posican!!

Amoriyoric gabe
Pasatu eciñez,
Nic damac maite ditut
Aitortzen det ciñez:
Beguiyaquiñ guciyac
Milla bay mingañez
¿Bañan biyotzarequiñ?
¡Zu besterican ez!!

Zu cera, bay zu cera,
Damacho maitia,
Nere desiuaren
Ceruco atia:
Biyotz onez nerequiñ
Bat-eguiñ zaitia
Eta bicico cera
Contentuz betia.

Ortaraco aguro
Guenuque eguna,
Nic nay detana balitz
Biyoc nay deguna:
Bañan zure asmua
Dá chit esaguna,
Ez dezu estimatzen
Nere naytazuna.

Dicha bat ez dit opa
Fortuna sequenac,
Negarrez daramazquit
Orduric gueyenac;
Eciñ aundiyagoco
Oñase ta penac,
Sufritutzen bici naiz
Zuregatic denac.

Miñ abetaz cupitzen
Ez baldin bacera,
Nere buruna nua
Betico galtzera:
Machora iyoco naiz
Andic botatzera,
Duben altubenetic
Gastelu atzera.