---
id: ab-4745
izenburua: Iparragirre-Ri (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004745.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004745.MID
youtube: null
---

Lore eder bat galdu da
Gure mendietan
Usaya zabaldu da
Lau probintzietan! (bis)
Negarrez asko gaude
Euskalerrietan,
Bai ere euskaldunak
Mundu berrietan. (bis)

¡Ay! ¡ai! Iparragirre
¡Ay gure lorea,
Zutik nola iduki
Arbola maitea.

Etsai asko seuzkazun
Errabiz beterik,
Birali zinduzten zu
Kampora emendik,
Lurra-ri ta amari
Agur bat egiñik,
Joan ziñaden itsasoz
Omenez beterik.

Luzaro ez genduban
Izandu berririk,
Koitaduba ill zala
Geunden sinisturik;
Jakindu zubenian
Giñala galdurik,
Etorri zan onuntza
Su garrez beterik.

Antziñako dotriña
Zigun erakusten,
Odolak jartzeraño
Guri irakiten;
Eriotza latzean
Gertatu da aurten
Jayotako Erriyan
Lurperatu zuten.

Negar samiña egiñ
Zazu Gipuzkoa,
Galdu zaizubelako
Seme alakoa,
¡Ai! ¡Araba! ¡Bizkaya!
¡Ai! ¡ai! Nafarroa!
Isuri dezazute
Egiyaz malkoa.

Arbolari adar bat
Zayolako galdu,
¿Nola ez bada izan
Biotzean damu?
Osatu dediñ azkar
Laurok-bat lagundu,
Berak oñaze danak
Kenduko dizkigu.

José Mariren gisa
Beardegu izan,
Aitorren seme denak
Libre izan ditzan;
Euskeraz jayo eta
Ala bizitu zan,
Euskera maitatubaz
Koitaduba ¡¡Ill-zan!.