---
id: ab-4749
izenburua: Kataliñ
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004749.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004749.MID
youtube: null
---

Elizatikan konbenturaño
egin digute galtzara, (bis)
Azkeneko eguna da eta (bis)
Guazen guztiyan dantzara.
Chin, Chin, chilibitua soñua
Chin, Chin, chilibitua danboliñ.
Orrela bizi bagiña beti
Orrela bizi bagiña beti (bis)
ondo giñake Kataliñ.
Chin, Chin, chilibitua soñua
Chin, Chin, chilibitu danboliñ.
Kataliñ nere maitea.
Orrela bizi bagiña beti
ondo giñake Kataliñ (bis)
kataliñ, kataliñ, kataliñ,
maitea
kataliñ, kataliñ, kataliñ,
nere maitea.

Zori oneko danboliñ soñu
Ederra gastiarentzat,
Mutillarentzan atzegiñ ona
Baña obia guretzat;
Chin, Chin, chilibitu soñua
Chin, Chin, chilibitu danboliñ,
Orrela bizi bagiña beti
Ondo giñake kataliñ.
Kataliñ nere maitea.

Chit poz aundiya sentitzen degu
Gure biyotz barrenian,
Mutill gaztiak jolas onian
Gurekin dabiltzanian;
Chin, chin, ...etc.

Plaza berriyan mutill gaztiak
Dantzatutzeko gure sai,
Saleturikan gaude gu eta
Dantzatu gaitian lasai
Chin, chin, ...etc.

Euskal Erriko soñu alayak,
Irrintzi eta algara,
Pozkidatzera guazen guztiyak
Salto ta brinko plazara
Chiñ, chiñ, ...etc.

Zer atsegiña jai artzaldian
Kontzejuko atariyan,
Elkarturikan gabiltzanian
Guziyak erokeriyan:
Chiñ, chiñ, ...etc.