---
id: ab-4778
izenburua: Euri Zaparrada (1)
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004778.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004778.MID
youtube: null
---

Odei beltz arroetan
Ecaitz galanta da
Goibel eta lañosu
Eta aire bolada. (bis)
Bertan dago gañean
Euri zaparrada
Zaldisco bisutsakiñ
Egiñic pasa da. (bis)

Indar geyagorekiñ
Aizeac orroaz
Pillan da samaldaca
Emenche dijoaz.
Bultza ta bultza alcarri
Alegiñ eroaz,
Orbel da arri-cóscorrac
Zerura astinduaz.