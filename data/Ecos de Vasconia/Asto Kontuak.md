---
id: ab-4769
izenburua: Asto Kontuak
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004769.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004769.MID
youtube: null
---

Aita batek
Ikusirik pasatzen
astoak ugari
a, a,
Aita batek algaraz
diyo semeari
Beguirakiyok Pello
ire anaiyari (bis)
semeak bereala
semeak bereala
eranzundu
ene, ene, ene.
¡Oyek nere anaiyak!
ez nuben nik uste
berorrek zitubela
orrenbeste seme
seme, seme.