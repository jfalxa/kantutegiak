---
id: ab-4785
izenburua: ¡Nere Izarra!
kantutegia: Ecos De Vasconia
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004785.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004785.MID
youtube: null
---

Zu zera nere izarra
Zu nere eguzkiya (bis)
Aingueruen Artetic
Cerutic jachiya (bis)
Aingueruen Artetic
Cerutic jachiya.

Barren calera nua
bertan nai det bizi,
anchen icusico det
sarritan Maiñasi.

Nere biyotza dago
oso penaturic,
joan bear detalaco
laizter zure ondatic.