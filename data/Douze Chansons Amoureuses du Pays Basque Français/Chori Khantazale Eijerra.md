---
id: ab-682
izenburua: Chori Khantazale Eijerra
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000682.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000682.MID
youtube: null
---

l.- Chori khantazale eijerra
noun othe hiz khantatzen?
Aspaldian nik eztiat
hire botzik entzuten:
Ez orenik, ez arterik
eztiat eroaiten
Noun ehitzaitan orhitzen.

- Itchasoan umen duzu
khantazale eijerbat,
Tronpatzen ezpalinbaniz,
zirena deitzen denbat.
Arringanerazten tizu
itchason gainti joailiak,
Hala noula ni maitenak.

- Itchasoa, eguriok,
phaosa hadi memembat,
Ene phena ororen berri
eman nahi dereiat.
Bat maithatu, eta dena
kitatu behar diat
Zorthia ountsa kontre diat!

- Baratzian eijerrenik
da arrosa lilia:
Haren urrinak duluratzen
ene sendimentia.
Berthuterik ederrena
da fidelitatia:
Houra da ene sinhestia.

- Chori khantazale eijerra
khanta ezak eztiki:
Mundian zorgaitzdunik
eztuk sorthu ni baizi.
Eni adio erran gabe
ihes egin herriti:
Hark ditak nigarra bethi.