---
id: ab-686
izenburua: Choriñoak Kaiolan
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000686.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000686.MID
youtube: null
---

l.- Choriñoak kaiolan,
Tristerik du khantatzen:
Dialarik han zer jan,
Zeren, zere,
Libertatia zouñen eder den!

- Kanpoko choria,
So.giok kaiolari:
Ahal balin bahadi,
Harterik begir.adi,
Libertatia zouñen eder den!

- Barda amets egin dit
Maitia ikhousirik:
Ikhous eta ezin mintza,
Ala ezina!
Desiratzen nuke hiltzia...