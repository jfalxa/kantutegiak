---
id: ab-685
izenburua: Nik Badut Maiteñobat
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000685.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000685.MID
youtube: null
---

l.- Nik badut maiteñobat, oi! bena nolako?
Ezda ttipi ez handi, bai bien arteko;
Begia pollita du, dena amorio:
Bihotzian sarthu zaut, ezbaitzaut jalgiko.

- Zuri nuzu hersatzen, arrosa ederra,
Phena gaitz hoietarik, nezazun athera;
Balin badut hortarik hiltzeko malurra,
Bazindukea bada bihotzean phena?

- Amodioaren phena, oi! phena khiratsa!
Orai dut ezaguztzen harek daukan phena.
Amodioa ezpaliz den bezain krudela,
Ez nezakezu erran maite zaitudala.

- Munduan zenbat urhats oi! ezdut egiten!
Ez ahal dira oro alferrak izanen.
Jendek errana gatik guretako elhe,
orataz trufa neinte, zu bazindut neure.

- Zeruan zanbat izar, maiteam ahal da?
Zure parerik ene begietan ezda.
Neke da phartitzea, maitea, enetzat:
Adio nik derautzut, denbora batentzat.

- Zure erranik ere, maitea, adio,
Ez nezazula, othoi, ni ukhan hastio:
Bainan bai bihotzetik izan amodio,
Etzaitut kitaturen thonban sar artio.