---
id: ab-689
izenburua: Kalla Kantuz Ogi Petik
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000689.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000689.MID
youtube: null
---

l.- Kalla kantuz ogi petik uztaril'agorriletan;
Maitea ganik etcherakoan entzun izandut bortzetan:
Amodioak bainerabilkan haren athe leihoetan.

- Amodioa, amodio nahi duenak har diro.
Nik batenzat hartu dut eta sekula ez utziko
Ez sekula, tonbaren barnen sarthu artino.

- Kukuiak umeak chilho ttipian haritz gainean;
Ama, ni ere nahi niz ezkondu adinak ditudanian;
Ene lagunak eginak dire juan den aspaldi handian.

- Primaderan zoinen ede brioletaren loria!
Aspaldian nit ezdut ikhusi, neure maitiaren begia;
Balinba gaichoak eztu ahantze niri eman fedia.

- Orhoitzen nuzu, orhoitzen, ez zautazu ahantzten;
Madalena batek bezanbat munduian dut sofritzen.
Jaten dudan ogia ere nigarrez dut trenpatzen.

- Gaua ilhun, bidia luze, ezdea phena handia?
Zure ikhoustera jiten gira, izar charmagarria;
Bortha irek.aguzu, zuk, tendreziaz bethia.

- Gaua luze izanagatik, argi mentsik ez dugu;
Izar charmagarri hura leihoan omen dagozu:
Gu etchian sarthu artino, harek argituren derauku.