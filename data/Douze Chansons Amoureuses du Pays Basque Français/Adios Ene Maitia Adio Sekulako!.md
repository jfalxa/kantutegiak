---
id: ab-683
izenburua: Adios Ene Maitia Adio Sekulako!
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000683.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000683.MID
youtube: null
---

l.- Adios, ene maitia, adio sekulako! (bis)
Nik eztit beste phenarik, maitia, zouretako,
Zeren eizten zutudan hain libro bestentako.

- Zertako erraiten duzu, adio sekulako? (bis)
Ouste duzia eztudala amorio zouretako?
Zuk nahi banaizu enukezu bestentako.