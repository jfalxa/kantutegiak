---
id: ab-690
izenburua: Plañu Niz Bihotzetik
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000690.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000690.MID
youtube: null
---

l.- Plañu niz bihotzetik;
Gaitza zer dudant eztakit,
Tristeziabatek harturik;
Enuke acholarik,
Baliz erremedio
Ene gaitza sendo ahal lironik.
Ezta mundian barberik bat baizik
Ene gaitza zertarik den ezagutzen dianik:
Eta hura berant etsirik,
Baniagozu, gaichoa, tristerik.

- Beran etsi nunduzun
Segur beldurra banizun;
Bena ene kointak behar tuzu eztzun:
Zuk ouste gabetarik
Zutzaz ohart nunduzun;
Zoure mina pharte eneki zuzun;
Zoure ganateko heraberik enizun,
Bena beste manerarik arte hortan bazuzun.
Aspaldian goure etchian
Khasu horrez asarra guntuzun.

- Jin bazira jin zira,
Segur hounki jin zirela!
Erraiten neizun berantetsi zuntudala;
Noun egon zira hola,
Horren beste denbora,
Jin gabetarik ene ikhoustera?
Eztuzula acholik balikezu zounbait marka,
Orai artino etzirenian jin ene kontsolatzera!
Jin zite ardura, ardura,
Phenarazi gabe ni hola.