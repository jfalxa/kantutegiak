---
id: ab-681
izenburua: Ala Baita Dolu Egingarri
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000681.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000681.MID
youtube: null
---

Ala baita dolu egingarri
amodietan denari,
Bethi phenan baiteramatza
gaua eta eguna!
Ezdeia bada pena
ene bihozean dena?
Nihork ezin dezake
adi ene hatsberepena
Maitebat ezin konbertituz
nik sofritzen dudana.

- Charmagarri zu ere
sanjakorra zare;
Ene pena eta dolorez
ongi trufatzen zare.
Norbaitek deraizkitzu
beharriak ongi bethe,
Eta, dudarikan gabe, zu,
heien erranen sinhesle.
Gaizki mintzatu nahi dena
nork enphatcha lezake?

- Izar charmagarria,
zu bazine neuria,
Zu zintazke bakharrik
neure kontsolagarria.
Zure begi eztia
bihotzean dut sarthuia
Eta amodiozko sokez
han ongi amarratuia.
Ez zinukeia bada izanen
nitaz pietatia?

- Gizon gazte floria,
erraiten dautzut egia,
Eztudala hartu nahi
pietatezko bizia
Ez eta ere eman
elizako fedia,
Zeren zuk eman duzun
bertze norbeiti zuria.
Sinhets nezazu, ezin daiteke
biez baizik egin paria.