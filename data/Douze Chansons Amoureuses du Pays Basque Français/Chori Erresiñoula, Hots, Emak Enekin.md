---
id: ab-688
izenburua: Chori Erresiñoula, Hots, Emak Enekin
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000688.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000688.MID
youtube: null
---

l.- Chori erresiñoula, hots, emak enekin;
Maitiaren borthala biak algarrekin:
Deklara izok gero, botz eztibatekin,
Haren adichkidebat badela hirekin.

- Heltu ginenian maitiaren borthala,
Horak hasi zeizkun tchanphaz berhala,
Ni ere joan nintzan bertan gordatzera,
Erresiñoula igain haritchbatetara.

- "Nour dabila hor gainti! Nounko zirade zu?"
"Etchondorik eztizut, pharka izadazu;
Egarri gaichtobatek heben gabilzazu:
Uthurri hounbat, othoi, erakats.zadazu..

- "Egarr.izanagatik ezta mirakullu:
Igaran egunian berochko egin du;
Uthurri hounik, heben, batere eztuzu:
Zuk galthatzen duzuna, goure behar dugu.".