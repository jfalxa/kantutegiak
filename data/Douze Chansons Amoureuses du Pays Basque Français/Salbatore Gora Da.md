---
id: ab-684
izenburua: Salbatore Gora Da
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000684.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000684.MID
youtube: null
---

l.- Salbatore gora da Garazi aldian:
Ni ere han nunduzun igaran astian,
Debozone gabe senthoralian,
Ene gazte lagunak han beitziradian.

- Bortietan artzain, eta ez jaisten ardirik,
Ountsa jan, edan, eta egin lo zabalik,
Mundian ez ahalda ni bezañ irousik.
Enuke segur nahi bizitze hoberik.

- Izarbat jeikiten da goizerri aldeti,
Argi eder batetan, leñhuru bateki,
Erien sendotzeko photeriareki;
hounki jin egin diat nik hari segurki.

- Izar houra jiten da boztarioreki,
Zelialat eroanen naiala bereki;
Hitzaman diriozut nik hari segurki,
haren zerbutchari nizatila bethi.

- Amorio zaharra behar hait kitatu,
Hanitch phena dereitak hik eni kaosatu.
Maite berribat zitak ezpiritian sarthu
Hari behar deroat bihotza libratu.

- -Amorio zaharrak zutia jenatzen,
Berribaten jitiak hanitch agradatzen?
Zu ere gaztettorik hasi zinen maithatzen:
Hoberik duzunian orai naizu kitatzen.

- Zounbat aldiz nik eztut egin nigarrez uthurri,
Zu zinadiala kaosa amak eraginik,
Arrazou ere baziala sobera badakit,
Zeren zutzaz benintzan charmaturik bethi.

- Kitatzeko sujeta othe zer ahalden,
Ahal bezain etsatoki ari nuzu phentsatzen:
Ene buria deusetzaz ere eztit akusatzen
Inozent nuzu; eta joan zite arren.