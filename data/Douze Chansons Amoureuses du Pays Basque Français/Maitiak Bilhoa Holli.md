---
id: ab-680
izenburua: Maitiak Bilhoa Holli
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000680.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000680.MID
youtube: null
---

- Maitiak bilhoa holli
Eta koloria gorri;
Eskuko larria chouri, zilhar fina uduri,
Eta bera charmagarri best' ororen gañeti.

- Etchettobat badizut nik,
Jaoregi baten parerik;
Hartan barnen egonen zira zilhar kaideran jarririk;
Ihourk ezpeiteizu erranen nahi eztuzun elherik.

- Nik badutut mila ardi
Bortian artzañeki;
Katalouñan ehun mando zilhar diharureki
Hourak oro ukhenic ere eniz jinen zoureki.

- Maitenaren etchekoak,
Khechu umen ziradeie;
Alhaba ene emaztetako sobera umen zaizie.
Ez emazte bai amore: sofritu behar duke.

- Senhartako emozie
Frantziako errege;
Frantzian ezpada ere Españakoa bedere:
Bada errege, enperadore, phuntsela ja eztuke.