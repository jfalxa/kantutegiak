---
id: ab-687
izenburua: Chorietan Buruzagi Erresiñoula Khantari
kantutegia: Douze Chansons Amoureuses Du Pays Basque Français
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000687.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000687.MID
youtube: null
---

l.- Chorietan buruzagi
Erresiñoula khantari;
Chorietan buruzagi
Erresiñoula khantari;
Khantatzen dizu ederki,
Goizan, argi hastiari.
Oi! haren aire ederrak
Choraturik nai ezari.

- Erresiñoula khantari
Chori ororen buruzagi:
Erresiñoula khantari
Chori ororen buruzagi:
Hanitchetan behatu niz
Haren botz eztiari,
Jeikirik ne oheti,
Khanberako leihoti.

- Gazte niz et.alagera,
Bai et.erria goihera,
Gazte niz et.alagera,
Bai et.erria goihera,
Kountent, irous, alagera;
Deusek ez egiten phena
Ororekil.adichkide,
Estekamenturik gabe.