---
id: ab-662
izenburua: Irulia
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000662.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000662.MID
youtube: null
---

l.- Iruten ari nuzu
Urkila guerrian,
Iruten ari nuzu
Urkila guerrian,
Ardura dudalaric
Ardura dudalaric
Nigarra beguian,
Ardura dudalaric
Ardura dudalaric
Nigarra beguian
Ardura dudalaric
Nigarra beguian.

- Nigar eguiten duzu,
Haspera penekin,
Nigar eguiten duzu,
Haspera penekin,
Contsolaturen zira
Contsolaturen zira
Oi! demborarekin,
Contsolaturen zira
Contsolaturen zira
Oi! Demborarekin.
Contsolaturen zira
Oi! Demborarekin.

- Yendec erraiten dute
Ezcondu! ezcondu!
Yendec erraiten dute
Ezcondu! ezcondu!
Nic ezdut ezcont minic
Nic ezdut ezcont minic
Guezurra diote,
Nic ezdut ezcont minic
Nic ezdut ezcont minic
Guezurra diote.
Nic ezdut ezcont minic
Guezurra diote.