---
id: ab-661
izenburua: Charmagarria
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000661.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000661.MID
youtube: null
---

l.- Charmagarribat dizut
Maite bihotzeti,
Amodiotan gare
Biac elgarreki.
Haren aire ederraz
Agradaniz bethi,
Pareric baduiela,
Ezbaitzaut iduri.
Haren aire ederraz
Agradaniz bethi,
Pareric baduiela,
Ezbaitzaut iduri.

- Amodioa zerden
Yende gaztendaco,
Ardura bihotzian
Pena emaiteco.
Hala behar du izan,
Orai neuretaco,
Ukhaiten ezpazitut
Maitea neuretaco.
Hala behar du izan,
Orai neuretaco,
Ukhaiten ezpazitut
Maitea neuretaco.

- Aritza lilitzenda
Maiatzian bethi,
Ene bihotza ere
Zu ganat erori.
Zuzira arbolia
Ni zure lilia,
Gure bien frutuia
Fidelitatia.
Zuzira arbolia
Ni zure lilia,
Gure bien frutuia
Fidelitatia.