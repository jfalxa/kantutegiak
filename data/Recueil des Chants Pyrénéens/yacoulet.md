---
id: ab-665
izenburua: Yacoulet
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000665.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000665.MID
youtube: null
---

Tres taillues de lard et uo saücisso!
Lècho la mesturo Yacoulet,
Quoan tu t'en bas t'aü buffet,
N'ey lèchos arré de bou
Que séras toustem gloutou,
Tres taillues de lard et uo saücisso!
L'echo la mesturo Yacoulet.
U yentillet pastou
Sèn ba ta la mountagno,
Dap soun fidèl pigou,
Cantabo soun amou.
En saütillan que minyabo castagnos,
Dap drin de burré et drin de bou pà blanc,
Dap u còp de by de Gan,
Aü doubléban!
Saüto Yacoulet
Dap l'amie Barthoulet.
Diübybòs mèsté Paüllè
Qu'abet u herbè
Ta minya bròyo,
dap bèro poulòyo,
Qu'aymat tabé lou yambou
Dap bou by de Yurancou.
Diübybòs mèsté Paüllè
Qu'abet u herbè
Ta minya bròyo,
dap bèro poulòyo,
Diübybòs mesté Paüllè
Que courret coum u lebrè.
U yentillet pastou
S.en ba ta la mountagno,
Dap soun fidél pigou,
Cantabo soun amou.