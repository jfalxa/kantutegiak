---
id: ab-653
izenburua: Donostiaco Damachoac
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000653.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000653.MID
youtube: null
---

- Donostiaco irur damacho
Errenterian dendari,
Donostiaco irur damacho
Errenterian dendari,
Yosten ere badakite bainan,
Arno edaten hobeki.
Etan criskitin craskitin criskitin
Arno edaten hobeki.
Yosten ere badakite bainan
Arno edaten hobeki.
Etan criskitin craskitin criskitin
Arno edaten hobeki,
Arno edaten hobeki.

- Donostiaco gaztelupeco
Sagarda naren gozoa,
Donostiaco gaztelupeco
Sagarda naren gozoa,
Hartaric edaten ari ninzela
Hautsi zidaten basoa.
Etan criskitin craskitin criskitin
Hautsi zidaten basoa.
Hartaric edaten ari ninzela
Hautsi zidaten basoa.
Etan criskitin craskitin criskitin
Hautsi zidaten basoa,
Hautsi zidaten basoa.

- Arrosachoac bortz osto ditu
Claberinchoac hamabi,
Arrosachoac bortz osto ditu
Claberinchoac hamabi,
Maria Josefa nahi duben horrec,
Escabiotza amari.
Etan criskitin craskitin criskitin
Escabiotza amari
Maria Josefa nahi duben horrec,
Escabiotza amari.
Etan criskitin craskitin criskitin
Esca biotza amari,
Esca biotza amari.