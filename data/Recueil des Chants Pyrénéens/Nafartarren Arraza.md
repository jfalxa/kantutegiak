---
id: ab-651
izenburua: Nafartarren Arraza
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000651.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000651.MID
youtube: null
---

- Nafartarren arraza
Hil dey ala lo datza?
Ez dut endelgatzen,
Nafartarren arraza
Hil dey ala lo datza?
Ez dut endelgatzen,
Belzunze biskondia Hain kapitan,
handia Nehork ez aiphatzen;
Belzunze biskondia Hain kapitan,
handia Nehork ez aiphatzen;
Hori zaut gaitzitzen.

- Zuhauren herritarrek
Bai eta laphurtarrek
Garaki diote:
Zuhauren herritarrek
Bai eta laphurtarrek
Garaki diote:
Eskualdunen lilia
Eta ohoragailia
Zu zira, Belzunze;
Eskualdunen lilia
Eta ohoragailia
Zu zira, Belzunze;
Luzaz bizizite!

- Franziak gero ere
Hanitz dembora gabe
Etzayik baituzke;
Franziak gero ere
Hanitz dembora gabe
Etzayik baituzke;
Zure odolitetikako
Aitzindari ongizko
Erregek on duke;
Zure odolitetikako
Aitzindari ongizko
Erregek on duke;
Othoi, ezkont zite.