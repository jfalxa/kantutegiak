---
id: ab-666
izenburua: Lous Tillolès
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000666.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000666.MID
youtube: null
---

l.- Abetbous bis lous tillolés,
Abetbous bis lous tillolés,
Quoan soun brabes, harditz, laüyés,
Quoan soun brabes, harditz, laüyes?
Hesen le promenade,
Capsus Péyrehourade,
En tiran l.ahiroun
Tout dret, entaü patroun.
Hesen le promenade,
Capsus Péyrehourade,
En tiran l.ahiroun
Tout dret, entaü patroun.
Tra la lalalalalala lala la
Tra la lalalalalala la.

- Qu.en soun estatz deban peillic,
Qu.en soun estatz deban peillic,
Moussu lou Coumte qu.ous a dit:
Moussu lou Coumte qu.ous a dit:
"Un couple de pistoles,
Mes enfants, seront bonnes,
Pour boire á ma santé,
Vive les Tilloliés!"
"Un couple de pistoles,
Mes enfants, seront bonnes,
Pour boire á ma santé,
Vive les Tilloliés!"
Tra la...

- Moussu Verdié que-b saludam,
Moussu Verdié que-b saludam,
Dab lous nos berret á la man,
Dab lous nos berret á la man,
Escusatz le hardiesse,
D.ibe brabe yoennesse,
Qui bin per z.embita
A le bede saüta.
Escusatz le hardiesse,
D.ibe brabe yoennesse,
Qui bin per z.embita
A le bede saüta.
Tra la...

- Binetz, Mesdames, si bous platz,
Binetz, Mesdames, si bous platz,
Assi qu.em d.honestes gouyatz;
Assi qu.em d.honestes gouyatz;
En cregnitz le galerne,
Ni lou bin de citerne,
Dab nous qu.am Chatelié
Lou brabe Tillolié.
En cregnitz le galerne,
Ni lou bin de citerne,
Dab nous qu.am Chatelié
Lou brabe Tillolié.
Tra la...

- Per promena lou temps qu.es bét;
Per promena lou temps qu.es bét;
Embarcat-bous aü nos bachet;
Embarcat-bous aü nos bachet;
Le noste goubernante,
Qu.es fort broye et charmante;
Per esta de Paris
Que semble dou pays.
Le noste goubernante,
Qu.es fort broye et charmante;
Per esta de Paris
Que semble dou pays.
Tra la...

- En arriban aü poun Mayou,
En arriban aü poun Mayou,
Quartié de Bayoune lé flou,
Quartié de Bayoune lé flou,
Dou haüt de él tillole,
Qu.an héit le cabriole,
Dou poun de Panecaüt
Qu.an héit lou sibresaüt.
Dou haüt de él tillole,
Qu.an héit le cabriole,
Dou poun de Panecaüt
Qu.an héit lou sibresaüt.
Tra la...

- Pusch, en reprenen l.abiroun,
Pusch, en reprenen l.abiroun,
Que s.en ban dret á Sent-Léoun,
Que s.en ban dret á Sent-Léoun,
Ensegna le yoennesse,
Per bagna dab hardiesse,
Per aprene con caü
A ha lou sibresaüt.
Ensegna le yoennesse,
Per bagna dab hardiesse,
Per aprene con caü
A ha lou sibresaüt.
Tra la...