---
id: ab-655
izenburua: Choriñoak Kayolan
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000655.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000655.MID
youtube: null
---

- Choriñoak kayolan,
Tristerik du kantatzen,
Choriñoak kayolan,
Tristerik du kantatzen.
Duelarikan zer ian, zer edan,
Kampoa desiratzen, Zeren, zeren,
Libertatea zoin eder den, eder den.
Barda nigar eguin dut,
Ikhusirik maitea,
Barda nigar eguin dut,
Ikhusirik maitea.
Ikhusi eta ezin minza,
Oi! Hau da pena handia,
Eta ezin bertzea,
Desiratzen dut hilzea, hilzea.