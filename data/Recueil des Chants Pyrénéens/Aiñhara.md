---
id: ab-656
izenburua: Aiñhara
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000656.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000656.MID
youtube: null
---

- Gaztetarzunac banarabila,
Aiñhara arian bezala,
Gaztetarzunac banarabila,
Aiñhara arian bezala,
Gayac ere igaraiten ditut
Egunac balira bezala.
O! maitia, ni bethi zu gana!

- Amodioric badutala,
Etzaizia iduri,
Amodioric badutala,
Etzaizia iduri,
Zure gatic igaran nezake
Ichassoa igheri.
O! hambat zare charmagarri.