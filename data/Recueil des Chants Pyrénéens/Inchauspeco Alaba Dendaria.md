---
id: ab-654
izenburua: Inchauspeco Alaba Dendaria
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000654.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000654.MID
youtube: null
---

- Inchauspeco alaba dendaria,
Goizian goiz yostera yuailia
Inchauspeco alaba dendaria,
Goizian goiz yostera yuailia
Nigarretan pasatzen du bidia
Aprendiza consolatzailia.

- Mertchicaren liliaren ederra!
Barnian du hechurra gogorra
Mertchicaren liliaren ederra!
Barnian du hechurra gogorra
Maithatu dut izanen ez dudana
Harc emaiten bihotzian pena.

- Maite duzu izanen ez duzuna?
Harc derauzu bihotzian pena?
Maite duzu izanen ez duzuna?
Harc derauzu bihotzian pena?
Maithazazu izan dezakezuna
Bardin fama balin badu hona.