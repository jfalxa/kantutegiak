---
id: ab-659
izenburua: Izar Ederra*
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000659.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000659.MID
youtube: null
---

- Mendian zoin den eder
Epher zango gorri,
Ez da behar fidatu
izar eder hari.
Ene maitea ere
Bertzeac iduri,
Neri hitzeman eta
Guibelerat itzuli.
Ene maitea ere
Bertzeac iduri,
Neri hitzeman eta
Guibelerat itzuli.

- Aria zahar eta
Cantorea berri,
Nere maite pollita,
Zira charmagarri.
Coloreac churi, gorri
Arrosa iduri;
Sortu zinen mundurat
Ene iragarri.
Coloreac churi, gorri
Arrosa iduri;
Sortu zinen mundurat
Ene iragarri.

- Zuri adressatzen naiz
Arrosa ederra;
Pena handi huntaric
Nezazun athera.
Chagrin huntan hilzeco
Nic banu malurra,
Bihotzean bazinduke
Bethico nigarra.
Chagrin huntan hilzeco
Nic banu malurra,
Bihotzean bazinduke
Bethico nigarra.