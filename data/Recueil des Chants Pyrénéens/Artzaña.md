---
id: ab-657
izenburua: Artzaña
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000657.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000657.MID
youtube: null
---

l.- Bortian artzañ eta
Ez jeisten ardiric;
Ontza ian edan eta
Eguin lo zabalic.
Enuke desiratzen
Bizitze hoberic
Mundian ez ahal da
Ni bezaiñ irusic.
Enuke desiratzen
Bizitze hoberic
Mundian ez ahal da
Ni bezaiñ irusic.
Epherrac baziti zu
Sahiesetan hegalac,
Bai eta burun gañen
Kukula eiger bat;
Zuc ere balin bazunu
Gaztetar zun eder bat,
Neskatilen gogatzeco
Bilho hori propibat.
Neskatilen gogatzeco
Bilho hori propibat.
Bortian artzañ eta
Ez jeisten ardiric,
Ontza ian edan eta
Eguin lo zabalic.

- Zankhoua mehe eta
Buria pelatu;
Hori duzu señale
Zirela zahartu.
Zahartu izan eta
Ez orano cenzatu,
Oficiotto hori
Behar duzu kitatu.
Zahartu izan eta
Ez orano cenzatu,
Oficiotto hori
Behar duzu kitatu.
Egunac luze dira,
Arramaïatzian,
Ekhia ere bero
Zohardi denian;
Erremarc eguin dizut
Ene bihotzian,
Plazer pena gaberic
Eztela mundian.
Plazer pena gaberic
Eztela mundian.
Bortian artzañ eta
Ez jeisten ardiric;
Ontza ian edan eta
Eguin lo zabalic.