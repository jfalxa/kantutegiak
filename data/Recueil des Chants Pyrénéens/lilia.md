---
id: ab-652
izenburua: Lilia
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000652.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000652.MID
youtube: null
---

- Lore pulit bat badutnic,
Aspaldi beguistaturic,
Lore pulit bat badutnic,
Aspaldi beguistaturic,
Bainan eznaite mentura
Haren harzera escura.
Baitakit den lanyera,
Hari behatzia sobera.
Bainan eznaite mentura
Haren harzera escura.
Baitakit den lanyera,
Hari behatzia sobera.

- Lore ederra behazu,
Maite nauzunez errazu,
Lore ederra behazu,
Maite nauzunez errazu,
Zure beguiec ene bihotza
Betbetan dute colpatu,
Colpe artaric badutnic,
Malhur izaitia irriscu.
Zure beguiec ene bihotza
Betbetan dute colpatu,
Colpe artaric badutnic,
Malhur izaitia irriscu.

- Aïze hotz ari guardia,
Iguzkiari ez fida,
Aïze hotz ari guardia,
Iguzkiari ez fida,
Zure colore bizia
Izan daiteke histua,
Escu on hatec bilzia
Desiratzen dauzut maitia.
Zure colore bizia
Izan daiteke histua,
Escu on hatec bilzia
Desiratzen dauzut maitia.