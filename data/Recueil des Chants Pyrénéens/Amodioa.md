---
id: ab-664
izenburua: Amodioa
kantutegia: Recueil Des Chants Pyrénéens
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000664.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000664.MID
youtube: null
---

l.- Lurraren pian sar nin daiteke,
Maitia zure ahalguez,
Borz pensaketa eguinic nago
Zurekin ezcundu beharez.
Bortha barnetic zerratu eta
Bethi ganberan nigarrez,
Pensamenduac airian eta
Bihotzetican dolorez.
Ene changrinez hill arazteco
Sortua zira arabez.

- Oren onian sortua zira
Izar ororen izarra
Zure pareric ezda aghertzen
Ene beguien bistara.
Espos laguntzat galdeguin zintudan
Erran nerauzun bezala;
Eta zuri-ez iduritu
Zuretzat aski ninzala:
Ni baino lagun hobeagoarekin
Yaincoac ghertha zitzala.

- Maiatzian zoin den eder
Choria cantuz pagoan!
Amodioac niabilazu,
Maitia zure ondoan.
Charmagarria ezbazira
Tompatu amodioan.
Maithatu zaitut bihotzez eta
Ni eznaiz ganbiatuco;
Bihotzian sarthu zerazkit
Eternitate guzico.