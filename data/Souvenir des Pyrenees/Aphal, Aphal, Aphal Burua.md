---
id: ab-673
izenburua: Aphal, Aphal, Aphal Burua
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000673.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000673.MID
youtube: null
---

l.- Goizian goizik jeiki ninduzun,
Ekhia jelki zenian.
Bai eta zetaz bestitu
Espuj nizan goizian.
Etxe-andre zabal ninduzu
Eguerdiren gainian.
Bai eta gazte, alhargunsa gazte,
Ekhia sartu zenian.

- Zazpi urthez atxiki nuyen
Senharra hillik khanberan,
Egunaz kofre batian eta,
Gauaz besuen artean.
Bainan dolutu izan zauzu
Eneki espusatzia?
Aphal, aphal, aphal burua,
Irrumberriko maitena.