---
id: ab-668
izenburua: Argia Dela
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000668.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000668.MID
youtube: null
---

l.- Argia dela diozu,
Gauerdi oraino eztuzu;
Enekilako denbora
Luze iduritzen zaizu;
Amoriori ez duzu,
Orai zaitu ezagutu.

- Othea lili denian
Xoria haren gaiñean;
Hurrak joaiten airian,
Berak plazer duenian
Zure ta nere amodioa
Hala dabila mundian.

- Partitu nintzen herritik
Bihotza alagerarik
Arrajin ninzan herrian
Nigarra nuen begian;
Har nezazu sahetzian
Bizi naizeno mundian.