---
id: ab-671
izenburua: Cantique
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000671.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000671.MID
youtube: null
---

Sakramentu handia
Zu, gure haskuria.
Jesus han bera dugu,
Berak erran dereizu.