---
id: ab-667
izenburua: Txoriñoa Kayolan
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000667.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000667.MID
youtube: null
---

Txoriñoa kayolan
Tristerik du khantatzen.
Duelarikan zer jan, zer edan
Kanpoa du desiratzen,
Zern, zeren, zeren
Libertatia haiñ eder den.