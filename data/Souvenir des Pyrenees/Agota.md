---
id: ab-676
izenburua: Agota
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000676.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000676.MID
youtube: null
---

Arzaina.
l.- Argian zorian jenik
Ene aresekila
Bethi behar entzun nahiz
Nunbaitik zure botza;
Ardiak nun utzi-tutzu,
Erradazu egia;
Nigarrez ikhusten deizut
Zure begi ederrak.

Arzainsa
- Ene aitaren ixilik
Jin nuzu zuregana,
Bihotza erdiraturik,
Zihauri erraitera
Khanbiatu deitadala
Ardien alhagia;
Sekulakoz defendatu
Zureki mintzatzia.

Arzaina.
- Gor niza edo in duzu ta
Erran deitadazia
Sekulakotz adio eni
Erraitera jin zirela?
Etziradia orhitzen
Guk itzaman gunila
Lurrian bizi gineno
Algarren maitatzea.

Arzainsa.
- Atzo nurbait izan duzu ene aita ametara
Guk algar maite dugula haien abertitzera:
Huruntastez algarganik fitez diten lehia
Eta eztitian junta casta agotarekila.

Arzaina.
- Agota badiadila bedizut enzutia;
Zuk erraiten deitadazu ni ere banizala;
Egundaño ukhen banu, demendren leinhuria
Enunduzun ausarturen begila sogitera.

Arzainsa.
- Genteten den ederrena umen duzu agota;
Bilho holli, larru xuri eta begi ñabarra.
Nik ikusi arzainetan zu zira ederrena.
Eder izateko, amens agot izan behar da?

Arzaina.
- Soizu nuntik ezagutzen dien zoin den agota,
Lehen soa egiten zaio hari behariala,
Bata handiago dizu, eta aldiz bestia
Biribil, eta orotarik bilhoz unguratia.

Arzainsa.
- Hori hala balimbada, haietarik ez zira
Ezi zure beharriak algar uduri dira;
Agot denak xipiago badu beharri bata,
Aitari erranen diot biak bardin tuzula.