---
id: ab-674
izenburua: Baratzeko Geroflea
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000674.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000674.MID
youtube: null
---

l.- Baratzeko geroflea eder kolorea.
Xarmagarria, erradazut, ethoi egia;
Badudanez progotxuri zuten fidaturik,
Edo bestela ust nezazu khentzea bihotzetik.
Beste baten txerkatzera enseyaturen niz
Zureganako esperanza galdu geroztik.