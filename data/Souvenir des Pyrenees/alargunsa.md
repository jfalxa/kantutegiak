---
id: ab-669
izenburua: Alargunsa
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000669.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000669.MID
youtube: null
---

l.- Ene ama, othoi, errazu:
Mithil horiek zer duten?
- Ene alhaba, deüjerik ez.
Zamari beltza galduriken.

- Ene ama, othoi, errazu:
Neskato horiek zer duten?
- Ene alhaba, deüjerik ez,
Zilhar unzi bat haüxe duten.

- Ene ama, ez, othoi,
Ez othoi egin negarrik;
Errege jaüna ekharriko du
urhe eta zilhar armaretik.

- Ene ama, othoi, errazu:
Khantu horiek zer diren baiñ gora?
- Ene alhaba, deüjerik ez,
Prozesionia da juaiten.

- Ene ama, othoi, errazu:
Zer zaya ezar behar duden?
Zaya xuri edo gorria?
- Ederziago da beltza.

- Ene ama, othoi, errazu:
Thomba hori zer da hain gora?
- Ene rena, ezin deit gorda:
errege yaüna ehortzia.

- Ene ama, ori urhe,
Urhe eta zilharren gilza;
Ene xemea unas haz zazie,
Eztirtazunekila.

- Lur santia erdira bedi,
Ni ere barnen sar nadin;
Lur santia zen erdiratu,
Ni ere barnen sartu,
Espus jaüna bexarkatu.
Jinko Jaüna dela laüdatu.