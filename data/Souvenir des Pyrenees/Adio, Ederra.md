---
id: ab-672
izenburua: Adio, Ederra
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000672.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000672.MID
youtube: null
---

l.- Xarmagarri bat badut
Maite bihotzetik.
Amoriotan gira
Biak algareki.
Haren aire xarmantaz
Agrada niz bethi
Parerik badiala
Ezpeizeit iduri.

- Xori phapagorria
Tristerik khantatzen;
Gaiazko alojia
Mundian du txerkatzen;
Ni ere gaiza beria
Banuzu gerthatzen;
Maitia ezpadereit
Bortha idokitzen.

- Adios erraiten deizut,
Orai maitenena;
Phartitu triste huntan
In dazu eskia;
Nuiz ikhusiren dereizut
Harzara begia;
Bentura sekulakoz
Adio, ederra.