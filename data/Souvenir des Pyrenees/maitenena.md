---
id: ab-675
izenburua: Maitenena
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000675.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000675.MID
youtube: null
---

l.- Zeluko izarren bidia
Nik benaki,
Han nir ene maite gaztia
Xuxen kausi
Bena gaur jagoiti nik hura
Ez ikhusi.

- Zuhaiñ gazte bat nik aihotzaz
Trenkaturik
Uduri zait ene bihotza
Kholpaturik;
Herruak erroiko zeizola
Fiharturik.

- Zeren beitzen lili ororen
Ejerrena
Bai eta ene bihotzeko
Maitenena,
Haren izanen da ene azken
Hasperena.