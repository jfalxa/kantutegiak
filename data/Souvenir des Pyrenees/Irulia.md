---
id: ab-670
izenburua: Irulia
kantutegia: Souvenir Des Pyrenees
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000670.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000670.MID
youtube: null
---

l.- Iruten ari nuzu
Murkula gerrian;
Ardura dudalarik
nigarra begian.

- Gentek erraiten dute:
Ezkundu, ezkundu!
Nik eztit eskun-minik
Gezurra diote.