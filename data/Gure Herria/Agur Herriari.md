---
id: ab-3820
izenburua: Agur Herriari
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003820.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003820.MID
youtube: null
---

Urrundik ikusten dut, ikusten Mendia,
Zeinaren gibelean baitut nik herria !
Jadanik dut aditzen, zorion handia !
Ezkila maitearen hasperen eztia !

Ezkila, zer othe duk hik egun erraiten ?
Urrunera zer berri othe duk egortzen ?
Mendiek hedoi-petik dautek ihardesten,
Hik errana zerura ditek garraiatzen.

Landako langilea, artzain mendikoa,
Ithurriko bidean dohan neskatoa,
Aditurik, ezkila, hire boz lainoa,
Othoizten hasi dituk Ama zerukoa !

Nik ere dut othoizten Birjina Maria
Bas-herrietan galdu haurren gidaria,
Nieretzat othoi dezan ardiets grazia
Bozean Kausitzeko nik egun herria !

Mendiak utzi ditut urrun gibelean ;
Herria dut ikusten jadanik aldean ;
Zer duk, ene bihotza, saltoka barnean ?
Othe duk huts eginen, herrira heltzean ?

Agur, agur herria ! agur sor-lekua !
Agur nere haurreko leku maitatua !
Jainkoak aditurik haur baten oihua,
Hire gana duk haur bat egun hurbildua !

Mendiaren hegitik hartuz bazterrera,
Iduri chingola bat aldapa behera;
Bidechka, hi zuzen haiz jausten zelhaiera,
Zuzen ereman nezak ahaiden artera.

Bide hegiko haitza, bortz aldiz haurrean,
Igandetan mezatik etchera sartzean,
Bai bortz aldiz jarri nauk, amaren aldean,
Hire adar handiek egin itzalean !

Baratze gibeleko elhorri churia,
Bethi duk begiratzen haurreko tokia ;
Hik bezala zertako, aldaska garbia,
Nik ez dut sor-lekuan higatzen bizia ?

Bainan nere begitik nigar bat da jausten..,
Bozkarioak darot bihotza gainditzen.,...
Etchekoen boza dut jadanik aditzen...
Jainkoa, darozkitzut eskerrak bihurtzen !

(Berriz herritik urruntzean.)
Ezkila, berriz diat bihotzean pena,
Herritik urruntzean bakotchak duena ;
Neretzat hik baihuen jo lehen orena,
Agian hik orobat joko duk azkena .