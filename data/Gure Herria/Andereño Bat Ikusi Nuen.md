---
id: ab-3859
izenburua: Andereño Bat Ikusi Nuen
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003859.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003859.MID
youtube: null
---

I
Kantu berriak, charmegarriak, amodiuaren gainian,
Berri berriak emanak dire Bazko bihamunian.
Uso palomba churi eder bat, ene sarien gainian!
Sariak gero, usorik gabe, altchatu nituenian,
Harritu nintzen, lumatu gabe nola joan zen airian!

II
" Andereño bat ikusi nuien, igande arratsño batez;
Ichtant berian, ni agradatu haren begi ederrez.
Ez dut uste baduela parerik eztitasunez;
Ene bihotza trichterik dago, harekin egon beharrez, (bis)

III
- " Ez da barberik, ez mirikurik, munduan aski abilik,
Ene gaitzaren sendatzeko erremedioa duenik.
Arrosa bat, lili eder bat, begietarat emanik,
Niri eskain eta bertzer eman ! Zer eginen dut bada nik ?
Pazientzia, ikusten baitut ez dela bertze muaienik,
Ez dela bertze muaienik.

IV
" Arrosa hura zerbait bazela, ez da duda egiteko ;
Botikan ere, oihal peza bat ez da ororen gostuko...
Erran nahi dut charmanta zela, perfeta, neure gogoko.
Ene bihotzak ez zuen aski klaritate haren dako !
Galdu dut eta, mundu hunetan, ni ez naiz kontsolatuko,
Ni ez naiz kontsolatuko.

V
"Amodioa gauza dorphea, jende gaztearendako,
Ezin utzizko estaturaino hartzen duenarendako ;
Gaitz horrek berak harturik nago ; barberik ez sendatzeko !
Ez nuen uste amodioak ninduela huntarako !
Eritasunik aski izan dut, mundu huntarik juaiteko,
Mundu huntarik juaiteko.

VI
" Hamasei, hamazazpi urtheak eztut orinon komplitu;
Amodiotan buzi naizela, jadanik hiru baditu.
Uso palomba, churi eder bat, ene kaltetan maitau,,,
Gure Jainkoak baizik ez daki, nik zer dukedan sofritu, (bis)

VII
- " Hainitz maite baduzula, fama hedatu zaitzu;
Egun oroz maite berri bat, zuk egiten omen duzu;
Horiek hola balimbadira, nor fidaturen zaitzu?
Gambiatu beharko zira, ni nahi balimbanauzu, (bis)
Ni nahi baimbanauzu. .

VIII
- " Bazinukea beldurrik, nik maitatuz geroztik,
Nahi ere banukeiela ikusi zutaz bertzerik?
Ez nuke uste, balaitekela mundu hunetan gizonik,
Kontent bizi ez laitekenik, zu beretuz geroztik, (bis)
Zu beretuz geroztik. .

IX
- " Mihian ditutzun hitz eder horiek bihotzian bazintu,
Halère zure maitatzerat erresoli nindaitezu...
Bainan gero desplazerik eman gogo badautazu,
Othoizten zaitut, ordu berian lurrez estal nezazu (bis)
Lurrez estal nezazu ! .

X
- " Ez nabilazu ikusi nahiz zu lurrez estalia,
Sobera dizut, zure alderat, nik amodio garbia.
Horiek hola dirade, edo, khen derutazu bizia;
Hura enetzat deus ez laiteke, zu galduz geroz, maitia, (bis)
Zu galduz geroz, maitia. .

XI
- " Maite nauzula zuk diozu ; egia erriten baduau, (bis )
Diozun bezein bihotzetik maite balimbanauzu,
Ezlizaz feda nezazu eta, gero m, zurea nukezu, (bisI
Gero, zurea nukezu ! ".