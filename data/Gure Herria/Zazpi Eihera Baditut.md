---
id: ab-3814
izenburua: Zazpi Eihera Baditut
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003814.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003814.MID
youtube: null
---

Zazpi eihera baditut erreka batean,
Zortzigarrena berriz etche saihetsean;
Hiru uso doazi karroza batean,
Hetarik erdikua ene bihotzean.

Etchea homen duzu ongi teilaztatu,
Gambera berri bat ere bai pintatu;
Erdiko uso horren perilik ez duzu:
Horren emplegua hauzuan baduzu.

Ene aitak eta amak zutenean jakin
Amodiotan ginela biak elgarrekin,
Enganioz ninduten plazarat ekharri,
Eta, lagun batekin, komentuan ezarri.

Zure aita deia hoin gizon krudela,
Komentuan baitzerauzka kriminel bat bezala?
Zure eta ene amodioa hoztu nahia da!
Bainan ez du eginen, trompaturen da.

Aira banindadi ainhera bezala,
Ardura joan nindaite komentuko lehiora,
Eta maitiarekin han pasa dembora,
Bien pena dolorez elgar kontsola.

Komentuko paretak, oi pareta finak!
Ofiziale onek dirade eginak;
Doblezka badituzte leioetan burdinak...
Maitiaren ikustera ni ezin sar hara.