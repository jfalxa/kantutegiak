---
id: ab-3793
izenburua: San Bartholome Arratsian
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003793.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003793.MID
youtube: null
---

San Bartholom'arratsian,
Milafrankako bidian,
Hiru lagunek eman tugu kantu berriak bidian,
Mugerren zer gerthatu den, Agorrilaren erdian.

Jaun barbera erradazu,
Othoi zuk plazer baduzu,
Sendagailurik baldin bauzu, orai beharretan nauzu,
Eritasun handi batez, hurran akabatu nuzu.

Emanadazu besuia,
Mira dezadan folsuia...
Sukarrik batere ez duzu, frasko daukazu larruia,
Nondikan sofritzen duzun, erranedazu egia!

Erran enzake egia...
Harrituren da munduia!
Aspaldi badu mintzo zela, plaza hortako jendia,
Oilasko luma gorriak non othe duien kafia?...

Oilasko luma gorria,
Emazu othoi guardia,
Ez dela haizu kunkitzia oilanda chitan zarria,
Uste gabe jin baitake etcheko buruzagia!.