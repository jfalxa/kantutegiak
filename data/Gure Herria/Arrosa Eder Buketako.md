---
id: ab-3889
izenburua: Arrosa Eder Buketako
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Arrosa eder buketako, amodioa gaztendako.
Nahi dudana banu nik orai enetako,
Urusagorik ez laite munduan ni baino!

Arrosa buketako... Maiatzean izaiten lore!
Amodiorik ez da penarik gabe...
Adios berza, neure plazer mundukoak joan dire!.