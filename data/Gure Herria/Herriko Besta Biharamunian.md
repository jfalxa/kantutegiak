---
id: ab-3821
izenburua: Herriko Besta Biharamunian
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003821.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003821.MID
youtube: null
---

Iragan besta biharamunian
Berek dakiten choko batian
Lau andre, hirur mutchurdin,
Bat alarguna, jarriak itzalian.
Harri chabal bat belhaunen gainian,
Ari ziren, ari ziren trukia.