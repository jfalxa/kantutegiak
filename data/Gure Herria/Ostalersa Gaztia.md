---
id: ab-3893
izenburua: Ostalersa Gaztia
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

I
Ostalersa gaztia zirade zu,
Aizazu;
Gu ere zure jendetaik gitutzu,
Hala'uzu.
Maiz etchean gitutzu;
Zuk guti probetchu !...
Gaurko gure eskotaren (1) perilik ez duzu;
Bertzenak baitutzu,
Behrturen zautzu !

II
Esperantzan jina nuzu bidian,
Lorian ;
Begitarte egidazu, etcahian
Sartzian !
Ni etchian sartzian,
Chutik sukaldina,
Zerbienta eder bat emanazu besoan,
Botoila esuan,
Basoa bertzian.

(1) Eskotaren: notre écot.