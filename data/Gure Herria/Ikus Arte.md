---
id: ab-3786
izenburua: Ikus Arte
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003786.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003786.MID
youtube: null
---

Hasten naiz orai kantatzen,
Ene penen deklaratzen.:
Izar bat charmegarria
Aspaldi dut adoratzen ;
Haren ganik urruntzean,
Tralera, tralera, tralera!
Haren ganik urruntzean,
Bihotza zaut erdiratzen!

Adios beraz, maitea,
Ene penen ithurria,
Zure ganik urruntzea
Iduritzen zaut hiltzea !
Behin bethikotz jinen naiz,
Tralera...
Behin betikotz jinen naiz,
Galtzen ez badut bizia !

Zer derautazu erraiten,
Orai nauzula kitatzen ?
Egunak zaizkit urtheak,
Non ez zaitudan ikusten,
Zu gan eta ariko naiz
Tralera. ...
Zu gan eta ariko naiz
Pena changrinez higatzen !

Zazpi urthe ja baditu
Zinintudala maitatu !
Hamabortz urthe nintuen,
Zuk, hamazazpi komplitu !
Geroztik amodioak
Tralera ...
Geroztik amodioak;
Zure esklabo egin nu !

Urhe erraztuna dut nik .
Zure eskutik izanik:
Hura nigarstatu gabe
Ez dut pasatzen egunik !
Hortarikan ikus zazu
Tralera ...
hortarikan ikus zazu
Zonbat maite zaitudan nik!

Nik balimbanu ahalik,
Airez gateko hegalik,
Zure ganat gan nindaite,
Itsasoaren gainetik!
Zu ikusi arteraino,
Tralera ...
Zu ikusi arteraino,
Nehun ez nuke pausurik!

Itsasoaren gainean
Oren-memento guzian,
Zur'othoitzez behar naite
Elementaren azpian,
Makurrik gabe heltzeko,
Tralera ...
Makurrik gabe heltzeko,
Salbaturikan portuan .