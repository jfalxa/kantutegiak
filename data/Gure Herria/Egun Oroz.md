---
id: ab-3851
izenburua: Egun Oroz
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003851.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003851.MID
youtube: null
---

I
Egun oroz, goizetan, arratsetan,
Bethi nago pena doloretan;
Ezin dezaket jan, ez et.ere edan,
Ez dakit zer dudan, gaitz handi bat da ni baitan;
Amarratu nau bet betan...
Ez naiteke ni bizi gisa hunetan !

II
Antchu gazte bilo hori ederra,
Sar zaite, sar korrela barnean
Artaldean bada oi zure beharra.
Turna zaite, turna zure sortu lekura...
Desiratzen dut nik hura,
Zure ondoan ibili naiz ni ardura.

III
Artzain ona ? - Badantzut, ene jauna -
Othoi zato, othoi, ene gana.
Errebelatu denak derautzuia pena,
Antchu bilo urdina
Sorturen dela ordaina ?
Etzaitezela horrela chagrina .