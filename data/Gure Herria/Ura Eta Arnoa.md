---
id: ab-3798
izenburua: Ura Eta Arnoa
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003798.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003798.MID
youtube: null
---

Noe lege zaharreko gizon famatua,
Zuk landatu zinduen lehen mahastia ;
Aihen balios hura,
Nork eman zantzun burura,
Lurrian finkatzia ?
Gizona trichtezian
Kausitzen den tenorian,
Hark dauk kontsolatua! (bis).

Arnoak
Arnoa nizan bezala, naute ohoratzen,
Kompañia guzietan choilki ni maitatzen ;
Frantzian, Italian,
Bai eta're Espainian
Nik oro charmatzen ;
Erregeren goithian,
Bethi ni mahain-burian,
Ni naute ni ezartzen! (bis)

3Urak.
Hire balenoria arras duk handia ;
Ehiz bada, funtsian, hain maitagarria !
Gizona desordrian
Hik ezartzen duk, mundian,
Jotzen osarria ;
Hik in tatchen kentzera,
Eta heien Kasatzera,
Ni nauk obligatia ! (bis) »

Arnoak
Gizona dagonian auhenek harturik,
Zoinbait pena dolorez bihotza ilhunik,
Edan beza nitarik,
Basua ongi betherik,
Gupida gaberiki;
Haren pena doloriak,
Chagrin'ta pasioniak
Kasaturen tiat nik! (bis)
Urak
Ene charmek ez ditik batre gibelatzen...
Arnoan den gizona nitaz ez orhoitzen !
Hire nonchalentzia
Eta're balenorian
Burua dik galtzen!
Grazia, konstantzia,
Hala hala berthutia,
Hik ez duk ez segitzen! (bis)

Arnoak.
Bekhaiztia haizela zaitak iduritzen,
Arrazoinikan gabe baihitzaut mintzatzen ;
Hoimbertze erraiteko!
Hik halere ez oraino
Etsaiak ikusten!
Gizon ala andriak,
Aphez ala erregiak,
Nik oro adichkide! (bis)

Urak
Ez duk ez hi baizikan muduan bertzerik
Atchikiko nauenik desohoraturik ;
Ene edertasuna,
Nihauren garbitasuna
Hik dauk ehortzirik !
Hordia hor habila,
Hihaur idurien bilha,
Kasik ezeztaturik ! (bis)

Arnoak
Amodioa'ta ura, zaudete ichilik,
Zoin zuen termiñotau deus erran gaberik!
Bacchus bere hoinian,
Barrika baten gainian,
Dagok charmaturik!
Mundiaren ahoan,
Mahainen buru-buruan
Hark naukak ezarririk. (bis)

Urak
Zinezko laudorioak nik ditiat izaiten ;
Mundu guziko fruituak nik tiat freskatzer
Untziak itsasoan,
Bethiko komersioan,
Ditiat erabiltzen ;
Ta Batheio Sainduan,
Mezako Sakrifizioan,
Ni bainiz ni kausitzen! (bis)

Arnoak
Ni ere kausitzen nuk, hor, Meza Saindua
Hi baino lehenago Sakrifizioan ;
Hortan ez duk halere
Abantailik batere
Hiretzat mundian ;
Hik bihotza trichtetzen,
Eta nik alegeratzen,
Gizonen eretzian ! (bis)

Urak
Hire alegrantzia laster duk pasatzen,
Familietan deskantsu hik ez duk emaiten
Gizona, ostatuan,
Maiz disputaren buruan,
Duk ezarrarazten !
Hortarik nahigabiak ,
Ardura heriotziak,
Dituk eginarazten ! (bis)

Arnoak.
Ez duka ez emaiten hik erraposturik, ?
Ago beraz holache, bai konfonditurik!
Gizonak, hi utzirik,
Eta nihaur maitaturik
Hobeki eginen dik ;
Bethi bake onian
Ta osagarri hobian
Dembor'iraganen duk. (bis.