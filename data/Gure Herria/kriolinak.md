---
id: ab-3864
izenburua: Kriolinak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003864.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003864.MID
youtube: null
---

I
Bertsuak nahi ditut orai athera ;
Ichilik banindago, banuke pena;
Bainan nekhe da:
Kriolilnaz besterik, solasik ez da.
Haueche da besta!

II
Hamalau berga oihal, ez da itsusi,
Kriolilna (1) batentzat eztela aski !
Ez gaituk gaizki !
Moda hori lehenik nork du ikasi,
Egiten hasi ?...
Paga nezake ongi, non den banaki.

III
Batzuek aise eta bertziek nekez ;
Bazterrak bethe dire bai Kriolinez...
Gauza ederrez !
Dupela bezein zahal dire beherez,
Konkorrak gaiñez...
Gorde behar litezke, jenden ahalkez!

IV
Iragan egun batez zen komedia,
Plaza baten ginean, ikusgarria;
Hango hiria !
Andre bat zen, karrosan igan nahia:
Ezin abia !
Karrosan ez zion sar... gibeladia !

V
Postilluna - Kaskoina - juramentuka,
Bethi Kriolinari eskuz pusaka:
"Hori dun hanka !..."
Bazterrak hutsi nahiz, zalapartaka,
Kopeta joka,
" Karrosan ez daiteke ... hola embarka ! .

VI
Lehen mintzatu ginen gu, elgarrekin,
Artoz ezun asea zela gurekin ;
Bazen zer egin !
Orai andreak, denak kriolinekin,
Alabak bardin !
Nola hari zaizkigun, nork diro jakin ?

VII
Urgulu malurusa ! hi habilana
Guri emanarazten hoimbertze pena,
Bai eta gerla !
Kriolinak emaiten balauk orena
Hilen hauena,
Gal dadien bethikotz hire izena !!.