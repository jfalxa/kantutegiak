---
id: ab-3847
izenburua: Hots, Aingerueki
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003847.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003847.MID
youtube: null
---

Hots, Aingerueki,
Arkanjelieki,
Gaur, khantatzera (bis)
Goure Erregiaren,
Jinko Semiaren
Adoratzera.

Ez izotza gatik,
Ez ulhunpen gatik
Egon herabez;
Har fedia gida,
Jinkoatan fida;
Goatzan lasterrez!

Badoatza artzañak,
Arhinik belhañak,
Laster zoin lehen,
Ikhous diroienez
Eia sorthu denez
Jesus Bethlemen ?

Goatzan gu ere,
Bihotzez berere,
Lekhu berera,
Jesus maitiaren,
Goure bermiaren
Adoratzera.

Jesus establian,
Astoa aldian,
Sorthu Bethlemen :
Hartan gañen aldiz,
Sarthu zuzun zaldiz,
Jerusalemen.

(Berset hau, suthondoan kantika kantatzen deneko)

Abiseik, astoa,
Eman ostikoa
Jesus haurrari ;
Eztuk ouste, bena,
Hik huke ogena,
Mankha baledi!

So zaukon idia,
Utzirik jatia,
Phausu handitan ;
Jesus Leitzen hoztu,
Hatsaz zizun gorthu
Behar ordian.

Heben dut sinhesten,
Begiez ikhousten
Banu bezala,
Amak haur maitia,
Bethlemen sorthia,
Jinko diala.

(Berset hau suthondoan kantika khantatzen deneko, Jinko mukhurra sutan
delarik.)

Harentzat etcherik.
Hain guti oherik
Ihourk ez zian;
Ez zen emaginik,
Ez beitzian minik
Haur ukheitian.

Berri handiena
Heltzen ahal dena
Hau duziela:
Alhabak bere Aita,
Hori hala beita!

Aita badu ere,
Seme dizu ere,
Ikarnaturik,
Birjinitatian,
Sabel sakratian
Kontzebiturik.

Adam gizon harek
Egin du berarek
Falta lehenik.
Bai eta Mesiak
Phakatu guziak,
Lurrila jinik.

Hirour printze bertan,
Beren monturetan
Jin Bethlemera,
Jesus-Krist Jaunaren,
Jinko Gizonaren
Adoartzera.

Dezagun guk ere,
Bihotzez berere,
Jesus adora;
Ezar gitzan gero
Bere haurrak oro
Zelian gora.