---
id: ab-3817
izenburua: Chardin Saltzalea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003817.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003817.MID
youtube: null
---

Untzia jin deneko itsas-bazterrera,
Gachuchak erosi tu bi saskiñotara ;
Biak altchaturikan airoski burura,
Horra zoin lasler doan karrika behera...

Zaiak estekaturik gorachko gerrian,
Zango sagarrak ditu arras agerian,
Etchean bezain aise dabila hirian,
Irrintzin bat eskainiz guzier lorian !

Itzuliak eginik, "Au bon vin" ethorri.
Zintzurra. idor eta sudurttoa gorri,
Arras akhitua naiz behar dut sokhorri,
Onetik nahi nuke barnerat igorri...

Barnearen phizteko, hustu-ta basoak,
Mokoka hasten bada zer elhe gochoak!...
- Amak, gordazkitzue zuen haur gaichoak,
Garbi behar baitira hekien gogoak !!!

Mihia badu ere sobera zorrotza,
Zuzen gelditzen zaio nonbeit han bihotza...
Arnoa dukenean, ez maite ur hotza,
Ez da nehon ez dena Gachucharen gaitza!!!

Eskalerik badea, guziz beharrean,
Dabiltzaner othoitzka bide bazterrean?
Gachuchak pausaturik saskia lurrean,
Chardinak eman daizko laster ahurrean.

... Emanak othe ditu bertzer harrapatzen... ???
Etchekoeri bethi kario du saltzen !
Alferretan andrea mokoka da hartzen,
Ez dako behin ere sos bat atheratzen !!!

Iguzkia zerutik aphaltzen denean,
Oraino're Gachucha karrika gainean ;
Hasi zuen bezala boz ederrenean,
Eguna finitzen du irrintzin batean !

Gachucha moko biphil, Gachucha kokina,
Badinat Jinkoari othoitz bat egina:
Beira dezaunan luzaz hire zintzur fina
Guhauri merkechago igorriz chardina !!.