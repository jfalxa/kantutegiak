---
id: ab-3842
izenburua: Pilotariak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003842.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003842.MID
youtube: null
---

pilotak ohore du
Eskual herrietan,
Zeren den ederrena
Joko guzietan:
Eskualdunak gaztetik,
Pilota-plazetan,
Agertzen du baduen
Odolik zainetan.

Lehengo pilotari
handien izenak
Espantutan gintuzke
Erran behar denak;
Oraiko gizon gazte
Deus ernaraz detzan
Heien aiphamenak.

Perkain eta Azantza,
zer pilotariak!
Elgarren bethekoak,
Joile bihurriak.
Ikustekoak ziren
Esku huska biak,
Chichtuz zohazinean
Hoien ukhaldiak.

Bai besain, bai besazpi,
bai pumpa lasterrak,
Oro zituen eder
Kurutchet Ezkerrak,
Domingo Ezpeletar
Zafrari supherrak
Harrituak zaduzkan
Hark ere bazterrak.

Jandarme Betiri'ta
Heraizko Martin,
Bi Soldadueneko
osab'ilobekin ;
Ibarl'eta Echkanda
Duronearekin,
Eskuz bai eskularruz
oro ziren berdin.

Sustun Itsasun eta
Zuraiden Bettiri,
Zoin geihago ziren
erreferan ari.
Onddore-ta Puttiko
eta Franchistegi
Rumebarekin botan
ziren lau piklari.

Kaskoinak, eskularruz,
Kaskaka, chimchtan,
Pilota nora nahi
zabilkan chimichtan ;
Jokoz bezenbat kaskoz,
munduaren bichtan,
Nekhez agerturen da
haren parerikan.

Berrogoi-ta-zazpiko
Irungo partida.,.
Sekulan halakorik
nun ikusiko da !
Kaskoinaren trunkoa
ez zaileta bira...
Hark eman zituen
guziak azpira.

Kaskoinaren denboran baziren hainbertze ! Lopetegi, Gamio, Senjan,
Hauziartze, Tripero, Kandelero, Ottarre, Goietche, Harriaga, Norberto,
Larrondo, Bizintche.
Gero Espainiatik, guri nausitzera, Melchor, Antsa, Manuel,
zitzaizkun athera. Horien joa ez zen guk usatu bera ; Guante luzeaz
zohan atchik eta fuera.
Atchiki joko hori laster ikhasirik, Frantsesak ez zagozken ez lo,
ez geldirik. Mattiu Aheztarrak, gazterik hasirik, Españolak zituen
iduki hezirik.
Mattiuri bertze hauk zitzaizkon jarreiki : Bere hiru anaiak,
gazteena Betti ; Gregorio, Leitzelar, Orgambide, Halty ; Bi Eizartze,
Larralde, Chantrea gaindizki.
Eskularrua utziz, arte hortan, oro Chichteraz hasi ziren nork jo
urrunago. Jo urrun horren leia chuchenean dago, Eta hurbiletik du
indar gehiago.
Bidaur, hiru Larronddo. Chilhar eta Jatsa : Lau gathu, acheri bat,
seigarrena hartza. Seiek dute jokoa murritz eta gaitza... Damurik
chahartzeak oro geramatza.
Berterretche, Patzola eta Abadia... Sakhatik da bihurri hoien
ukhaldia. Ottarre, Belekabe, Ziki plekaria... Plazarat agertzeko oi !
zer gazteria !
Pilotari chaharrak joaiten has orduko, Berriak dire bethi ondotik
helduko. Eskualherrian bada bethiko muthiko, Ohore hoi baiuite chutik
idukiko.
Saratar hoiek dire sortzez pilotan. Hazparnen ere hautak badire
noiz-nahi. Ezpeletar, Khamboarrak zirenean ari, Hek ere segur lehen
bazuten behari.
Plaza bat izaiki'ta pilotaririk ez ; Asko herritan hortaz dagode
ahalkez. Eskualdun aztureri ukho hoi egitez, Eskualduna da hola
Kaskointzen, nahi-t'ez.
Igande'ta bestetan, ofizioz goiti, Pilota partida bat behar laite
bethi. Hartan herriko seme hoberen zombeiti Zor zaie agertzea guziz
ohorezki.
Askotan eder zaiku plekako jokoa, Trinket partidatto bat, orobat
lachoa... Bego hargatik nausi errebotekoa; Pilotako legea hartan da
osoa.
Gaztetik ez denean pleka beizik hartzen, Pareta hari joka besoa da
galtzen ; Bichta eta indarrak guti han largatzen, Zabaleko joari
nekhez gero jartzen.
Ez du bere parerik errebot zabalak ; Pilotak, airatzeko, han ditu
hegalak ; Botak bai erreferak, marrak bai gibelak, Han dituzte
agertzen nork bere ahalak,
Pik arrasak ez badu hiltzen errefera, Errebotaren joa doha
urrunera. Paso aldekoak du bihurtzen barnera, Ahalaz chacha edo kintze
egitera.
Bainan erne baitago errebotlaria, Zolak igorri joan du bere lona.
Errechacharieri orobat guardia ; Besoa dute zalu bai eta begia.

Butariaren joko latz eta borthitza... Maiz horren eskuan da
partidaren giltza. Paso marrako edo arte jo murritza... Kintze
gudukatuan hark du azken hitza.
Kintze, trente, kuarenta, Joko da laugarren ; Hoien artean zenbat
gora behera den ! Erreferaria da beldur arraiaren, Eta kuarentaduna
ados justearen.
Pilota partidetan hau ikusten dugu : Ez dela aski joa handi ez
mukuru ; Gehienik duenak begi eta buru, Kintze irabazteko maiz joa
chuhur du.
Oro plazarat bilduz, hantchet beha gaude. Bortz bortzi ari dire bi
gison aralde, Zoinek jo chuchenenik, bizi edo lore. Irabazleak gero
han du han ohore !
Eguerdik jotzen badu partida artean, Eskualdunak zer garen ezagun
gare han : Buru has eta chutik, denak othoitzean, Gu baino gorago bat
baitugu gainean.
Pilota zoin den joko ohoragarria, Ongi dakiena da Musde Abadia.
Gazteak pilotara ditezen lehia, Hark ematen diote bermatze saria.
Saridun partidetan, begira hargatik, Jukutria gaichto bat guri
egitetik ! Pilotan ari dena sarri horren gatik, Zinez artzen ez bada.
dohala bichtatik !
Barne edo zokhotan dagon gazteari Trufa egin dezogun, zeren den
hoin nagi. Kaskoinen jostetetan gostu badu an, Ez dela Eskualduna aise
du ageri.
Kampo aro ez bada, hortakotz barnean, Imbido-ta hordago, joka bai
musean. Orduan da arnoa gocho mahainean, Guziz irabazirik edaten
denean.