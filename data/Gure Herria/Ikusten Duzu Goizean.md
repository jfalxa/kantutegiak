---
id: ab-3790
izenburua: Ikusten Duzu Goizean
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003790.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003790.MID
youtube: null
---

Ikhusten duzu goizean,
Argia hasten denean,
Menditto baten gainean,
Etche ttipitto, aintzin churi bat
Lau haitz ondoren erdian,
Chakur churi bat athean,
Ithurriño bat aldean,
Han bizi naiz ni bakean.

Nahiz ez den gaztelua,
Maite dut nik sor-lekhua
Aiten aitek hautatua.
Etchetik kampo zaut iduritzen
Nombeit naizela galdua;
Nola han bainaiz sorthua,
Han utziko dut mundua
Galtzen ez badut zentzua.

Etchean ditut nereak,
Akilo, haitzur, goldeak,
Uztarri eta hedeak;
Jazko bihiez, ditut oraino
Zoko guziak betheak;
Nola iragan urtheak
Emaiten badu bertzeak,
Ez gaitu hilen goseak.

Landako hirur behiak,
Esnez hampatu ditiak,
Ahatche eta ergiak,
Bi idi handi, kopeta zuri,
Bizkar beltz, adar handiak,
Zikiro, bildots guriak,
Ahuntzak eta ardiak,
Nereak dire guziak.

Ez da munduan gizonik,
Erregerik ez printzerik,
Ni bezein urusa denik;
Badut andrea, badut semea,
Badut alaba ere nik;
Osasun ona batetik,
Ontsasun asko bertzetik;
Zer behar dut gehiago nik.

Goizean hasiz lanean,
Arratsa heldu denean,
Nausi naiz mahainean,
Giristino bat ona dut hartu
Nik emaztea hartzean;
Ez du mehe egunean,
Sarthuko uste gabean
Chingar hezurrik eltzean.

Piarres ene semea
Nahiz oraino gaztea,
Da muthiko bat ernea;
Goizean goizik bazken erdira
Badarama arthaldea;
Segituz ene bidea,
Nola baitu egitea,
Ez du galduko etchea.

Ene alaba Kattalin,
Bere hameka urhekin
Ongi doha amarekin;
Begiak ditu amak bezala,
Zeru zola bezin urdin;
Uste dut, demborarekin,
Oraiko itchurarekin,
Andre on bat dion egin.

Ez dugu behar lurrean,
Ongi bizirik etchean,
Utzi laguna gosean:
Ez du beharrak sekula jotzen
Gure etcheko athean,
Non ez duen mahainean,
Othuntza ordu denean,
Lakhu bat gure aldean.

Ene andrea Maria,
Ez da andre bat handia,
Bainan emazte garbia;
Irri batentzat badut etchean
Nik behar dudan guzia,
Galdegiten dut grazia,
Dudan bezala hasia,
Akhabatzeko bizia.