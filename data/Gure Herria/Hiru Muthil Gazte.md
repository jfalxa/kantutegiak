---
id: ab-3879
izenburua: Hiru Muthil Gazte
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Neska gaztea, bagi urdina,
Gorphutza duzu ongi egina!
Hiru muthil gaztek, zu nahiz emazte,
Elgarren artean dizputa badute.

Nahi badute izan bezate!
Ene perilik heiek ez dute.
Ez dut nahi ezkondu, ez disoputan sartu:
Komentu batera serora nioazu.

Oraichet duzu deliberatu
Behar zirela serora sartu?...
Hartuz geroz botu, behar da komplitu:
Zoaz komentura, fortuna egizu.