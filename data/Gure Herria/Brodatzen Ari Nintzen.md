---
id: ab-3785
izenburua: Brodatzen Ari Nintzen
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003785.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003785.MID
youtube: null
---

Brodatzen ari nintzen, ene salan jarririk ;
Aire bat entzun nuen itsasoko aldetik,
Itsasoko aldetik, untzian kantaturik,

Brodatzen utzirik, gan nintzen ama gana :
Hean jaliko nintzen gibeleko leihora,
Gibeleko leihora, itsasoko aldera ?

Bai, habil, haurra, habil, erron kapitainari,
Jin dadin afaitera, hemen deskantsatzera,
Hemen deskantsatzera, salaren ikustera.

Jaun kapitaina amak igortzen nau zu gana,
Jin zaiten afaitera, hantchet deskantsatzera,
Hantchet deskantzatzera, salaren ikustera.

Andre gazte charmanta, hoi ezin ditekena :
Iphar haizea dugu, gan behar guk aitzina ! ( Berriz)

Andre gazte charmanta igan zaite untzira,
Gurekin afaitera, eta deskantsatzera,
Hortchet deskantsatzera, salaren ikustera.

Andre gazte charmanta igaiten da untzian,
Han emainten diote lo-belharra papora. (Berriz}.

Jaun kapitania nora deramazu zuk haurra ?
Zaluchko itzulazu hartu duzun lekura,
Hartu duzun lekura, aita-amen gortera !

Nere mariñel ona, hedazak heda bela !
Bethi nahi nuena jina zaitak aldera ! (Berriz)

Jaun kapitaina nora ekarri nauzu huna ?
Zalu itzul nezazu hartu nauzun lekura,
HarTu nauzun lekura, aita amen gortera ! »

Andre gazte charmanta, hoi ezin ditekena.. .
Hiru ehun lekutan jinak gira aitzina. ...(Berriz}

Andre gazte charmantak hor hartzen du ezpata,
Bihotzetik sartzen'ta, hila doa lurrera !. ... (Berriz)

Nere marinel ona, norat aurthiki haurra ?
Norat aurthiki haurra ? Hortchet itsas zolara !

Oi Ama Anderea, so egizu leihora. . . .
Zur'alaba gaichoa, uhinak derabila (Berriz).