---
id: ab-3775
izenburua: Charmegarria Zira
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003775.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003775.MID
youtube: null
---

« Charmegarria zira eder eta gazte,
Ene bihotzak ez du zu baizikan maite.
Bertze zembait bezala, othe zira libre ?
Zurekin ezkontzeaz dudarik ez nuke.

Churi gorria zira, arrosa bezala ;
Profetak ere dira mintzo hola hola.
Araberan bazinu gorphutza horrela,
Iduriko zinuen... zeruko izarra.

« Oi maitea, zatozkit, plazer duzunean,
Nehork ikusi gabe, ilhun-nabarrean ;
Lagun bat badukezu joaiteko bidea-n,
Hark ezarriren zaitu trankil bihotzean.

«Plazer eginen duzu, ichiltzen bazira,
Haur iñorantak hola trompatzen baitira
Ez da enetzat ona holako segida. ...
Bertzalde zure bithan ez naiteke fida !

«Adios, beraz orai, ene arraroa,
Hori dela medio herritik banoa ;
Bihotza trichte eta kechuan gogoa,
Bethi jarraikiren zait zur'amodioa. .