---
id: ab-3890
izenburua: Argia Dela Diozu
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Argia dela diozu,
gauerdi oraino ez duzu.
Enekilako dembora luze iduritzen zaitzu.
Amodiorik ez duzu, orai zaitut ezagutu.

Ofizialetan duzu
Zure sinheste guzia.
Aitak eta amak ere hola dute gutizia.
Lehen bat, orai bertzia: Oi! Hau penaren handia!

Othia lore denian,
Choria haren gainian...
Hura juaiten da airian, berak plazer duenian;
Zur'et'ene amodioa, hala dabila munduian.

Partitu nintzen herritik,
Bihotza alegerarik;
Berriz jin nintzen herrira, nigarra nuen begian!
Har nezazu zure lagun, bizi nizeno munduian.

Amodioaren berri
Nihork ongi othe daki?
Ni gaichoa, hemen nago, frogaturikan ederki,
Erranez mila goraintzi ene gazte demborari.