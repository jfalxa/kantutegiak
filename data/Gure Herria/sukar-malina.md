---
id: ab-3818
izenburua: Sukar-Malina
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003818.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003818.MID
youtube: null
---

Agur, oi izar maitia, iduzki eder begia,
Zure gatikan nik utzi niro bizi naizen herria,
Eta alderat zure ganat jin, izar charmagarria.

Mintzo zira hai ederki, ederki eta oneski,
Zure ganikan nahi nuke nik jakindu bereziki,
Noiz izanen naizen zuretzat izar edo iduzki ?

Zazpi urthe badu orai, uda hunetan komplitu,
Amodio perfeta batez, zintudala maitatu,
Bainan orai ni beldur nuzu, zaizkidan kambiatu.

Eri nuzu bihotzetik, erraiten dautzut bi hitzez,
Sukar-malinak harturik nago, zu ez izan beldurrez
Othoi othoi zuk senda nezazu, hil ez nadin chagrinez.

Gaitz ororen kontra ere erremedioak badire :
Sukar-malina balimbaduzu, zerbitza hor barberez,
Eta ez ethor zu ene ganat, ni miriku beharrez.

Urh' erreztun bat badut nik, bere ederrez harturik,
Hura ikusi arteraino zuk, zur'erhian sarturik,
Ene bihotzak ez du izanen, oren bat deskantsurik.

Urh' erbeztuna baduzu, bertze norbeiti emozu;
Nere erhian sarturik segur ikusiren ez duzu ;
Bertze zembeit ahal duzunez entseia hobe duzu.

Oi Epher zango gorria, airean joaile propia,
Ez dazuia bada pena, ene hola ikustia ?
Othoi, othoi, har nazazu, utzirikan bertziak !

Norat nahi joan nadin, ene bihotza zurekin ;
Plazerikan ez dut hartzen, ene gazte lagunekin.
Nun ez zaitudan zu ikusten, zure begi ederrekin.

Arrosak eder botia, ederchago du loria...
Lehen bertzetan ibili eta, orai niri gorthia,
Zoazkit begien bichtatik, parabolaz bethia.