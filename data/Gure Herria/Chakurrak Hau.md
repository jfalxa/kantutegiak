---
id: ab-3827
izenburua: Chakurrak Hau
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003827.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003827.MID
youtube: null
---

Chakurrak hau!
Gathuak ñau!
Arnoak huntara ekarri nau.
Etchean sartzean,
Andrea koleran,
Anglesa buruan
Latina goikoan,
Eskuaraz mintzatzen.
Gizon kokina non hago gaur?
Cho! Cho! Mariaño
Mochkorra jina nun gaur.