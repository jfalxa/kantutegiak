---
id: ab-3854
izenburua: Oi Penarik Ez Banu !
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

1
- (( Oi penarik ez banu, ez nindake mintza !
Maitia, nik ez dakit zertan dudan hutsa.
Fidel izan bazinu niri eman hitza,
Zuria izanen zen oi ene bihotza. ))

2
- (( Charmanki mintzo zira, oi enen maitia ;
Zurekilako nintzan deliberatuia;
Handizki gertatu zait niri flakezia:
Erreportak sinhetsiz naiz gibelatuia.))

3
- (( Ez da erreportarik, ez arartekorik
Amodo perfeta gibelatzen duenik ;
Bi urthe baditugu biek hitz emanik ...
Ez nuen esperantza holako changrinik ! ))

4
- (( Hitzño bat badizut nik zuri erraiteko:
Oztibartarra zira, ni ere Baigorriko ;
Zure eta ene amodoa ez zen sekulako...
Bihotzian har zazu, nitaz orhoitzeko, ))

5
- (( Bihotzez eri zuzu, kampos alegera;
Ni ere ez nindauen gauza horren begira,
Fidel iLaster joanen ginen biak elizara. ).