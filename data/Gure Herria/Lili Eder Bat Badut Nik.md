---
id: ab-3885
izenburua: Lili Eder Bat Badut Nik
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Lili eder bat badut nik
Aspaldi begichtaturik;
Bainan ez nainte mentura haren eskuraT hartzera;
Banaki zer den lanjera,
Joan nindaiteke aldera.

Lili ederra, behazu;
Maite nauzunez errazu!
Zure begiek bihotza barna dautate kolpatu;
Kolpe huntarik badut nik
Granganatzeko hirrisku.

Pena nikezu, ni gatik
Balimbazindu malurik;
Ez nuke uste ene begiek eman dautzuten kolperik;
Ez duzu beraz lanjerik...
Etzira hilen hortarik.

- Mintzo zirade polliki,
Polliki eta tendreki.
Bisitaño bat nikezu zure ganat nahi laburzki;
Desira badut segurki,
Bazinezadat permeti.

Permeti niro, maitia,
Ez dut hain bihotz dorphia;
Bainan, lehenik, nahi dut jakin zure gogoko berria...:
Zu ene ganat jitiaz
Estona laite mundia!

- Ez ahal nuzu printzia,
Ez et'Aitoren semia!
Hola nehoren estonatzeko ez da sujet bat handia;
Zuhaur zirade chimplia,
Ilusionez bethia.

- Badakit chimple nizela;
Hori segurki hala da;
Iduritzen zaut atsolutoki ezagutzetan zirela;
Nik ere badut fortuna;
Bainan ez dut zuk emana.