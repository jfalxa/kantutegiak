---
id: ab-3850
izenburua: Amodio Garbia
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003850.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003850.MID
youtube: null
---

I
Orai zembeit dembora, pentsatu gaberik,
Ikhusi izan nuen lili bat carmantik ;
Ene begiak ziren bai admiraturik,
Ene bihotza ere anhitz trublaturik.

II
Lili pollitik, eta sasoin guzietan,
Errekontratu dizut bai orai artekan;
Hek ikhustearekin ez nuen sekulan,
Plazer bat hain eztirik... nola zurekilan.

III
Primadera huntako gau ederño batez,
Ilhargia zelarik, hedoirik baterez,
Delliberatu nuen juaitia oinez,
Tristeziak khentzeko, harekin egonez.

IV
Bidia nuen segur aiserik pasatu,
Amodioak hegalak berekin baititu;
Bonurrak han ninduen bai idurikatu,
Ene lili maitea leihoan kausitu !

V
Imposible litzaiket hemen emaitia,
Enetako zembat zen, oi, miragarria,
Bere behatze eztia zerura itzulia,
Zelarik arratseko othoitzian jarria !

VI
Ez ninduen ikhusten, gorderik bainintzan
Hurbildu nintzaioen mintzatziarekilan,
- (( Etzaitezila izi, ni ikhusirikan,
(( Hunat jin izan bainaiz desir onekilan

VII
(( O zu, edertasunez zirena bethia,
(( Primaderako lili ororen printzia,
(( Barkha diezadazu ene ausartzia,
(( Zure othoitz kharsua hola trublatzia !))

VIII
Ene ganat begiak zituen itzuli,
Erraiten zautalarik, boz ezti batekin
- (( Ez nuzu, ez, trublatzen orai ni, segurki
(( Zu zintudan goguan othoitzen artetik.

IX
(( Ene bihotzak ez du, erranen dut klarki,
(( Zu ikhusiz geroztik, trankilitaterik
(( Zure orhoitzapenak nau segitzen bethi...
(( Gauza hori nerautzun zuri erran nahi.

X
(( Jainkoak nahi zuen, duda gaberikan,
(( Sukar bera sor zadin gure bihotzetan;
(( Nik ere berehala maithatu zintudan,
(( Amodio fidelaz ; ez in dudarikan ! ...))

XI
Hura entzutearekin niri hola mintzo,
Aingeru baten boza zaitan enetako !
Eskuak ginituen eman partitzeko,
Promes fidelekilan elgarren itzaiteko.