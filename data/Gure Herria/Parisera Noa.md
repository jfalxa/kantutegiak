---
id: ab-3862
izenburua: Parisera Noa
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003862.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003862.MID
youtube: null
---

Ene gaicho ama, norat joan zira ?
Zeruen gainetik eni so zira ;
Zure othoitzaren indarrarekin,
Ene bihotza dut aingeruekin.

Zerutikan izan tudan donuak
Eskualdunendako ditut guziak ;
Heien bihotz ona, menden mendetan,
Bethi egonen da ene gogoan.

Pansera noa pena batekin :
Egon nahiagoz bethi zuekin !
Heldu den urthian berriz jiteko...
Phentsatze hori dut kontsolatzeko.

Adios, bainan ez ahaztekotan,
Egoitzaz bainago zuen bihotzetan ;
Usu izanen dut nigar begitan,
Ama gaichoaren orhoitzapenetan .