---
id: ab-3837
izenburua: Tocha
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003837.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003837.MID
youtube: null
---

Bi persu ditut kantaturen,
Zuer plazer egiteko,
Ene sakelako tocharen
Zer guzia kondatzeko. (bis)

Aspaldi du hasi nintzela,
Ez dakit nola, pipatzen,
Tocha hoy erosi nuela:
Ongi baitzaut baliatzen.

Lehenik batto izan nuen,
Pittika larruz egina ;
Gero bigarrena izan zen
Larru salorño batena.

Bainan larru ahuñarena
Deusik ez da guretako !
Zer da ere satorrarena,
Egungo Jaunttoendako ?...

Beraz orai, tocha, perlekin
Zethaz dut, bai, zetha finez,
Garnidura eder batekin,
Bethea tabako onez.

Nahi duena dabilala
Urhe, zilhar botoinekin...
Ni, gau eta egun nabila...
Ene tochahoarekin.

Muntra bat bere chenarekin
Eder zaiote orori ;
Bainan tocha tabakoarekin
Baliosago zaut niri.

Arratsetan oherakoan,
Tochañoa bethe betherik,
Bai, burukitaren azpian
Pulliki ezartzen dut nik.

Ez badezaket egin lorik,
Tocha dut hartzen eskura,
Eta naiz, pipattoa phizturik,
Entseatzen lokartzera.

Behin, norbait kecharaztera
Baldin heldu bazaut nin,
Tocha idok... adio kolera !
Egiten dazkot bi irri.

Hitz batez zuer errateko,
Ez naite bizi, hau gabe !
Iduri du dela, bethiko,
Ene zortearen jabe .