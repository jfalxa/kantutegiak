---
id: ab-3894
izenburua: Santibatek Andere
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

I
Santibatek, andere,
Aurthen bezala, gero're,
Santibatek igortzen gaitu chingarketa gu ere.

II
Ez gabiltza ederrez,
Ez eta erre beharrez,
Kostuma zaharraren utztez, erreprotcha beldurrez.

III
Tolos' eta Baiona ...
Jainkoak dautzul'egun ona !
Guri chingarraren emaitera, jeiki bazinte huna !

IV
Ez dugu nahi urdia,
Ez et' ere erdia,
Usaia gatik, konstuma gatik, liberako zathia,
Liberako zathia era gerrenaren bethia.

V
Ez dugu nahi tripotik,
Bertzian (1) izan ez denik,
Beldurrez eta trompa gaitzazten ... urdek ... kaz betherik.

(Etchek' anderiari lausenguak )

VI
Etchian eder gerrena...
Etchek' andere lerdena,
Zur' erhiko erhaztun hortaz eros niro Baiona !

VII
Etchian eder kortzeiru,
Dabiltzanian inguru...
Etche huntako etchek' anderia, Parabisuan aingeru !

VIII
Etchian eder ohako,
Haurra sortzen deneko...
Etche huntako etchek' anderia zaldiz alizarako,
Zaldiz elizarako, eta zilhar kadiran jarriko !

IX
Landan eder ilhlarra,
Haren pean belharra...
Etche hunttako etchek' anderia, zer emazte plachenta !

X
Etchian eder ferreta,
Haren gainean kaneta...
Etche huntako etchek' anderia, zer emazte placheta !

( Nausiari, erdi lausengu, erdi mehatchu )

XI
Etchian eder aitzurra...
Nausi bilo izurra,
Kolputto bat eman ezaguzu busti dezagun zintzurra !

XII
Kadiran zude jarririk,
Koloriak gorririk ;
Guri chingarren emaitera jeiki bazinte horitk !

( Eskerren Bihurtzea )

XIII
Eman duzu nobleki,
Kompainiak ere badaki;
Parabisuan sar zaitezela, hamabi aingeruekin,
Hamabi aingeruekin eta zure familiarekin !

(Deus ukaiten ez dutelarik)

XIV
Goazen, goazen hemendik,
Hemen ez duk chingarrik;
Etche huntako gazitei charrian sagiak umiak hazten tik !

XV
Etchian eder aihotza...
Etchek' andere hortz motza,
Su burdinaz hauts ditzatzula sudur eta kokotza!

(1) Bertzian : Pertzian, panderoan.