---
id: ab-3824
izenburua: Katadera Dantza
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003824.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003824.MID
youtube: null
---

Atxuri beltza ona da baño
obeago da txurie;
dantzan ikasi nai duan horrek
nere oñetara begire.
Rai, rai, rai...