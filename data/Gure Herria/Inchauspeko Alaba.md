---
id: ab-3846
izenburua: Inchauspeko Alaba
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003846.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003846.MID
youtube: null
---

Intchauspeko (1) alaba dendaria,
Goizian goiz jostera joailia ;
Nigarretan pasatzen du bidia...
Aprendiza kontsolatzailia.

Zeruan den izarrik ederrena
Jin balakit argi egitera,
Joan nindaite maitiaren borthara,
Ene penen hari erraitera.

Merchikaren loriaren ederra !
Barnian du hechurra gogorra...
Maitatu dut izanen ez dudana ;
Horrek baitaut bihotzian pena !

Maitatu duzia izanen ez duzuna ?
Harek dautzia bihotzian pena ?
Maitazazu izah dezakezuna,
Eta kita ezin dukezuna ! (2)

Zu eta ni, biak ginaudenian,
Akort ginen hitz batik barnian,
Eta orai, dembora jin denian,
Zu ez zaude zure erranian!

Adios, beraz, ene maitia, adios
Adios, beraz, orai sekulakotz!
Ezkont zaite (3) plazer duzunarekin,
Bainan beira en'errekontrutik !

Zer Iitake zure errekontria ?
Zer izain da zur'egin ahala ?
Baldin zerbait nitaz gerthatzen bada,
Suiet berri bat zutan izanen da!

Sorhuetako plazara jalietarik,
Gizon gazte -galtz'erdi churirik-
So egin dut, bainan ez ikusi nik,
Piarreni, zure parekorik.