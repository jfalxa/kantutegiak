---
id: ab-3803
izenburua: Bidarraitarra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003803.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003803.MID
youtube: null
---

Mila zortzi ehun eta hemeretzian,
Urriaren hilaren bederatzian,
Omore ona nuien kantatzia;
Gazte eta alegera, trankil bihotzian,
Ontasun frango badut intresian,
Deusik ez etchian:
Orai bezain aberats nintzan sortzian!

Nor nahik zer nahi eranikan ere,
Bidarraitarra nuzu, bai nahi ere;
Etcheko seme ona, segurki halere;
Nahiz baden herrian hobechagorik ere;
Baitut bortz haurride, enekin sei dire,
Oro adichkide...
Dotiaren gainetik samurtzen ezgire.

Aita ezkondu zen gure amarekin,
Ama aitarekin, biak elgarrekin;
Orduian gazte ziren, bertze zerbaitekin,
Eta orai zahartu miseriarekin:
Ontasun. igorri bertze haurridekin,
Parte bat enekin,
Enekin baino haboro bertze bortzekin.

Aita zen etcheko, ta ama kampoko,
Jainkoak egin.tu elgarrekilako;
Hirur murthiko eta hirur neskatoko,
Haurrik aski badute aisa bizitzeko.
Balimba bertzerik ez zaie sorthuko!
Ez da fidatzeko,
Arraza nasaia da; horra zertako.(1)

Adinen beha gaude egia erraiteko,
Gure ontasun ororen partitzeko.
Diru idorra ere bada bizikichko,
Jainkoak daki zembat bakhotcharendako;
Izaiten badugu, ez dugu utziko,
Auzokuen dako;
Bainan ez daiku sakelarik erreko.

Hemen bagirade orai zembait lagun,
Botoila bana arno edan dezagun.
Lehenik ona denez hor jasta dezagun.
Ta ona balin bada, bir.edan detzagun.
Hots, trinka dezagun, plazer har dezagun,
Bilha zembait lagun!
Diru dianak izanen dik ezagun.

Etchek.anderia, oi zure trichtia!
Uste duzu girela diru gabia?
Bai, dirua badugu, bainan da larria...
Zor utziren dautzugu gaurko afaria.
Etchek.anderia, emazu guardia,
Har pazientzia...
Noizbeit izanen duzu pagamendia.

(1) Ailleurs on dit: Landatuak hirriskatzen du phisteko.