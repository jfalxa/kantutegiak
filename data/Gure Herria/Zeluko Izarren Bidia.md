---
id: ab-3800
izenburua: Zeluko Izarren Bidia
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003800.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003800.MID
youtube: null
---

Zeluko izarren bidia
Nik baneki,
Han nir'ene maite gaztia
Chuchen kaousi;
Bena gaour jagoiti nik houra
Ez ikousi!

Haritch gazte bat nik aihotzaz
Trenkaturik,
Uduri zait ene bihotza
Kolpaturik,
Errouak eroriko zaitzola
Eiharturik!

Ahal baliz, ene begia
Zerraturik,
Ene maite gaztiarena
Argiturik,
Ezar niro ene odola
Ichouririk!

Zeren beitzen lili ororen
Eijerrena,
Bai eta ene bihotzeko
Maitenena,
Haren izanen da ene azken
Hasperena.