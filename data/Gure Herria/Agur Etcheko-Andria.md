---
id: ab-3796
izenburua: Agur Etcheko-Andria
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003796.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003796.MID
youtube: null
---

Agur etcheko andria,
Gure diruen jabia,
Berriz ere behar dugu arnoarekin ogia,
Bai eta're, arratsian, kartekilan argia.

Kartak hiru'ta kartak lau,
Kartak eman deraut hau:
Hamairu izan eta ezin galdu hamalau,
Sakelan tudan sosekin afalduren ez naiz gaur!

Utzak jokua burutik,
Partitu gabe mundutik!
Aditzia banian, bainan jokuak adarrak makur tik,
Hik nekez irabaziak ostalerak juanen tik!

Lau arrosa, lau begi,
Bortz arrosa'ta bortz begi,
Gizon debocharen haurrak maiz gose'do egarri,
Kampora jali ondoan haize hotza janari!.