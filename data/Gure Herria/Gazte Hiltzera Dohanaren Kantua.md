---
id: ab-3811
izenburua: Gazte Hiltzera Dohanaren Kantua
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003811.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003811.MID
youtube: null
---

Hogoigarren urthera hari naiz hurbiltzen,
Eta nere oinetan tomba dut ikhusten.
Ni zertako mundua ethorria nintzen ?
Ondikoz ! ez sortzea neretzat hobe zen.

Sekulan zoriona zer den jakin gabe,
Hainitz dut nik lurrean izan atsekabe.
Lore sortu orduko histu baten pare,
Biziaren uztera guti naiz herabe.

Ez-ez 'enan izitzen ez ni herioak,
Laster khen dezadala bizia jainkoak !
Sehaskan galdu ditut nere burasoak ;
Zer diren eztut jakin etcheko gozoak.

Aita ama maiteak, zertako mundutik
Zerura goan zineten ni hemen utzirik ?
Ez dut nik mundu huntan izan sosegurik.
Ez baitut ezagutu amaren musurik .

Hamazazpi urthetan, zorigaitz dorphea!
Izar bat maitatu dut parerik gabea ;
Aingeru bat zaiteken, zeruko umea ;
Nere bihotz eritik juan darot bakea.

Nahiz aingeiru ona, ez duzun sekulan
Munduan jakin zembat nik maite zintudan,
Urus zarenaz geroz jaunaren oinetan
Ez nezazula ahantz zure othoitzetan.

Anaia bat banuen haurra nintzelarik ;
Soldado juan zen eta nik ez du berririk:
Aita amekin dela ez daite dudarik,
Niri begira daude hirurak zerutik.

Ahituz banarama neire oinhazeak,
Burhasoen aldera deitzen nau zortheak,
Nork ditu arthatuko aita'ama maiteak,
Zuen tomba gainean nik eman loreak ?

Nere gorphutz flakoa lurrerat orduko,
Nor othe da munduan nitaz orhoituko ?
Nere ehortz lekhura nor da hurbilduko ?
Lore berririk ez da neretzat sortuko !

Mendian ur chirripa, harrien-artean,
Auhen eta nigarrez jausten da bidean,
Izan nahi bailuke zelhai ederrean...
Hala nere egunak badohaz lurrean.