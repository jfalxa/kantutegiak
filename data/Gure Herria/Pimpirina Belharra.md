---
id: ab-3858
izenburua: Pimpirina Belharra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003858.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003858.MID
youtube: null
---

I
"Baratzian zoin den eder pimpirina belharra !...
Zerk othe emana deraut nik deudan sukarra ?
Sukar - malina badut, dioten vezala...
Demborak joanen duke, jina den bezala. .

II
- " Maite zaitudan bezala, zuk maita mezazur,
Izar charmegarria, zuk, kontsoka nezazu!
Bertzek atcha zaitzaten, hinitz beldur nuzu !
Bai, pena deraitzut, bainan ... fortuna egizu ! .

III
- " Maite zaitut maite, bainan, ... bihotzetikan... ez !
Ikusi nahi're, bainan, hurbildanikan... ez !
Amodio badut, bainan, zuretako... oh ! ez!
Ezkond nahi dut, bainan, zurekilan...oh ! ez !

IV
- " Mendirik gorenian, hor, eiten du elhurra...
Trompatzen zuk ninduzula, banuien beldurra;
Izanen badut ere bethikotz malurra,
Halare nahi zinduzket ikusi ardura !...