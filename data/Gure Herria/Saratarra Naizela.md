---
id: ab-3866
izenburua: Saratarra Naizela
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003866.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003866.MID
youtube: null
---

I
Saratarra naizela
orok badakite, orok badakite,
Semperen badudala
choriño bat maite.
Hura bezen pullitik, hura bezen pullitik
Bertze bat balaite...
Bainan zer probetchu da,
nik hura dut maite.

II
Guziek badakite
naizela Piarres, naizela Piarres,
Choriño bat galdurik
nagola nigarrez;
Bera ibili baitzaut, bera ibili baitzaut
ondotik beharrez,
Ederki ase bainau
Kafe 'ta likurrez .