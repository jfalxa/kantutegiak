---
id: ab-3806
izenburua: Apecha Eta Lorea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003806.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003806.MID
youtube: null
---

Neguaz primadera zenean jabetu,
Sasi baten hegian apecha zen sortu ;
Lekhu berean baitzen lorea gerthatu,
Gaichoek elgar zuten bihotzez maithatu!

Iguzkiak ihintza histen duenean,
Apecha doha bethi hegalez airean ;
Nahi luke loreak segitu bidean,
Girthainak zertako du gelditzen lurrean

Loreak. « Nere besoak ditik ihintzak zerratzen,
«Ondikotz! iguzkiak goizean urratzen!
«Zertako haiz hi bethi nitarik urruntzen ?
«'Hi guan eta badakik, nigarrez nauk hurtzen...

Apechak. «Zertako nigar egin, ó lili maite.a ?
« Egunaz iguzkia dun nere jabea:
«Goizetik lorez lore dinat nik bidea ;
« Iguzkia sartzean bethi naun hirea...

Loreak. «Goiz guziez adio, arratsean agur,
« Egunak luze dituk gauak aldiz labur;
«Nere nigarrez othoi ez hadila samur,
« Zeren hire galtzeaz bethi nauk ni beldur...

Iguzkiak, goiz batez, zenean agertu,
Lorearen besoak zituen urratu ;
Bi maitek zutenean elgar besarkatu,
Aphechak haizeari hegalak hedatu

« Haize bihotz gabea zertako herritik
Ereman duk aphecha mendien gainetik ?
Zertako duk urrundu maitearen ganik ?
Hegastin gaicho horrek ez zian hobenik».

« Apech hegal flakoa, geldi hadi, geldi,
Utziz bozkarioa, doluan sar hadi:
Zertako haiz fidatu haize gaichtoari ?
Lorearen gainera lurra duk itzuli!...

Apechak. « Maitia kausitzen haut lurrez estalia !
«Jainkoak bere ganat deithu-hau, lilia!
« Hi lurreko hindudan sosegu guzia ;
« Hil haiz!... Orai neretzat deus ez dun bizia I,

Oraino iguzkia zohan inguruan
Aphecha zenean'hil bere sor lekhuan...
Izan baitzuen aski zorigaitz munduan,
Agian Lorea du khausitu zeruan.