---
id: ab-3819
izenburua: Ithurrira Goizean
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003819.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003819.MID
youtube: null
---

Goizean goiz jeikirik, argia gaberik,
Urera joan ninduzun, pegarra harturik,
Tra lera, la la la...

Jaun chapeldun gazte bat jin zautan ondotik:
Heia nahi nuenez urera lagunik?

Nik: Ez nuela nahi urera lagunik,
Aita beha zagola salako leihotik.

Aita beha zagola, ezetz erran gatik,
Pegarra joan zerautan, besotik harturik.

Urera ginenian, ez ginen egarsu.
Galdegin zautan ere: Zombat urthe tutzu?

Hamasei...hamazapi orain ez komplitu:
Zurekin ezkontzeko, gazteegi nuzu.

Etcherat itzultzeko, nik dutan beldurra!
Ezjakin nola pentsa amari gezurra!

Arreba, nahi duzu nik erakuts zuri,
Etcherat ethortzean zer erran amari?

Urtcho churi pollit bat, gabaz dabilana,
Hark ura zikindurik, egotu naiz, ama!

Dakigunaz geroztik zer erran amari,
Dugun pegarra pausa, oneski liberti.