---
id: ab-3815
izenburua: Mila Bat Zorki Ehun Eta...
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003815.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003815.MID
youtube: null
---

Mila bat zortzi ehun eta
Lau etan hogoiean,
Bertsu berri hauk emon dire
Aire zahar batean;
Gure aitaso Kantabreak,
Lo baitaude lurpean.
Ez othe dire atzarriko,
Aire hau aditzean?

Mendi gainetik hasten banaiz
Khanta hunen khantatzen,
Oihan zokhotik, oihartzuna,
Zer dautak ihardesten?
Hobietarik aiotasoak
Othe dire mintzazen?
Zer haiteke, hi, oiharzuna.
Hecien oihua baizen?

Gure aitaso kantabreak,
Mende zaharretan,
Gu orai gisa hedatuak
Alhor hautan beretan,
Anaiak ziren: Guzik bat,
Mendien bi aldetan:
Jainkoa chopilki zen errege,
Eskualdun etcholetan.

Nork ez dituzke gure aiten
Balentriak aditu?
Helkaietako zombat etsai
Heck ez zuten suntsitu
Semeak aita dik iduri,
Zeren nehork ez baitu
Bere herrian, egundaino,
Eskualduna garhaitu.

Larrun, Mundarrain, Altabizkar,
Zuen hegi muthurrak,
Mihise batek hila gisa;
Gorde ditik elhurrak;
Larru baletza orai ere

Erreka zepho zilhoetan
Bizi bada belea,
Toki goretan arranoak
Eiten du ohatzea;
Hala du mendi bizkarretan
Eskualdunak Etchea,
Biek handizki autelakotz
Maite libertatea!

Zerda liburu zaharrenek
Irakhasten dutena?
Hurrunegitik heldu dela
Eskualdunen omena;
Ezdakitela noiztik duen
Euskuarak hastapena...
Zeren Eskuara baita naski
Mintzoetan lehena.

Jakintzu batek, aditu dut,
Nonbait erran duela;
Eskualdun izan baino lehen
Gu herdaldun ginela.
Ichilik ago ahal baduk!
To! Jakintsu ergela,
Ez erran guri arranoa
Beletik datorrela!

Hire amaren besoetan
Lo hagoen, haurtchoa,
Aingeruekin ametsetan,
Irriz hago, gachoa;
Erregerentzat on badire
Phordoin eta khoroa,
Hobeago duk hiretako
Amattoren altzoa!...

Nola birundan arbolari
Da segitzen itzala,
Bakhotchak hala sor-lekhua
Bethi maitha dezala!
Anaiak gaituk Eskualdunak