---
id: ab-3880
izenburua: Aitak Erran Dio Alabari
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003880.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003880.MID
youtube: null
---

Aitak erran dio alabari:
Kita zan, kita muthil hori!
Horrekin ezkontzen bahaiz, ez dun izanen linjarik,
Ez eta ere dirurik,
Aitak et'amak emanik!

Alabak aitari errepostu,
Izpirituia prest baitu:
Nerekin ezkontzen denak badu linja, badu diru,
Bai neretzat, bai beretzat,
Bai eta ondoko haurrentzat.