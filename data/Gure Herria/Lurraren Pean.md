---
id: ab-3865
izenburua: Lurraren Pean
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003865.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003865.MID
youtube: null
---

I
Lurraren pean sar nindaiteke, maitea, zure ahalgez;
Bortz phentsamendu eginik nago, zuekin egon beharrez:
Bortha barnetik zerratu eta ganbaran bethi nigarrez,
Pentsamenduez aireaneta bihotzetikan dolorez...
Ene changrinez hilarazteko sortua zinen, arabez!

II
Oren onean sortua zira, izar ororen izarra,
Zure parerik ez da agertzen ene begien bichtara !
Espos-laguntzat nahi zintudan, erran neraitzun bezala;
Bainan zuri ez iduritzen zurtzat aski nintzala...
Ni baino hobechago batekin Jainkoak gertha zitzala !

III
Primaderan joaiten dira mariñelak untziko...
Zuretzat dudan amodioa ez dut sekulan utziko.
Charmegarria, nahiz ez giren elgarrekilan biziko,
Behin maitatu zaitut eta ez zaitut hastiatuko,
Ene bihotzean sartua zira eternitate guziko !

IV
Primaderan, oi zoinen eder den choria kantuz phagoan !...
Amodioak bainerabilka, maitea, zure ondoan;
Deusetan ere ez zaitut nahi nik borchatu amodioan...
Changrin huntarik ni hiltzen banaiz, satisfa zaite gogoan,
Malurusik aski izanen naiz nihaur bakharrik munduan .