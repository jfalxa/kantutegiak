---
id: ab-3779
izenburua: Akerraren Kantuak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003779.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003779.MID
youtube: null
---

Otsoa hor heldu da Akerraren jaterat!
Otsoak akerra,
Akerrak artoa.
Akerra khen, khen, kehn,
Artoa gure zen!

Herioa hor heldu da gizonaren hartzerat!
Herioak gizona,
Gizoank sagua,
Saguak soka,
Sokak idia,
Idiak ura,
Urak sua,
Sua makila,
Makilak chakurra,
Chakurrak otsoa,
Otsoak akerra
Akerrak artoa.
Akerra khen, khen, khen,
Artoa gurea zen..