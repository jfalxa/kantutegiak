---
id: ab-3873
izenburua: Amikuzeko Gilen
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003873.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003873.MID
youtube: null
---

I
Amikuzeko Gilen khilo-egileak
Etchetik khendu ditu bere langileak,
Sehi maiteak,
Zeren galdu ditiuen irabazbideak.

II
Ez dezake nihon sal khiloa, ardatza,
Nagusitu baitzaio galtz-erdi orratza,
Moko zorrotza...
Ez da izertu nahi oraiko neskatcha !

III
Elhè biltzen dabila, galtchoina eskuan,
Laneko gogorikan ez biru buran,
Alfer moduan,
Oihal guti emanez kutcharen chokuan.

IV
Gazte irule guti da Eskual -Herrian !
Bat ez dugu ikusten khiloa gerrian,
Athe egian...
Oi zer frendak horiek ... saltzeko ferian !

V
Oraino zembait bada, ahuzo herrietan,
Iruten ari denik, berant arratsetan,
Bihotz minetan,
Ilobek ez lagunduz behar orduetan.

VI
Askotan altchatzen da amasoren boza,
Bainan ilobak ez du hartu nahi ontsa;
Lanerat lotsa !...
Arrantze gabe nahi bailuke arrosa!

VII
Amak goizean oihu: " Jaiki hadi, haurra !
Ez deia aski luze hiretako gaua ?
Har zan haintzurra.
Itzuli beharra dun baratzeko lurra..

VIII
Alabak arrapostu ohetik, gostura :
" Bego bihar artino haintzurtzeko lurra,
- Arte laburra-
Joan behar bitut goizik, egun, merkatura..

IX
" Zuri galdatzen dautzut, izeba Maria,
Lakhet-lekhu othe da gaztentzat hiria?
Hain da egia:
Harat joaiteko badut hainitz gutizia. .

X
Horra nola dagiltzan oraiko neskatchak;
Lur-lanetako dira sobera beratzak.
Oi alfer hotzak !
Akhabo eginen du laster laborantzak !...

XI
Neskatcha gazte dena ... itchuraz da abil;
Espoosatu berrian, jaunarentzat umil ;
Ondoan zarpil,
Galtzetako ziloak erdirat ezin bil !!.