---
id: ab-3788
izenburua: Jaun Goikoari
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003788.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003788.MID
youtube: null
---

Jainko Jauna, zuk eginak dira mendi handienak,
Zerua hunkitzeraino gora altchalzen direnak,
Mendien zolan, bethi samurrik, lehertzen diren uhainak ;
Nork egin ditu eta nun sekulan holako lanak ?

Mendi punttan agertzen den iguzki handi ederra,
Ilhunabar guzietan jalitzen den arth'izarra,
Ilsas gainean zazpi kolore altchatzen den orz'adarra,
Zuk dituzu aurthikiak, zuhaurek nahi tokira.

Gaztena, haitz'tza phagoak, oihanetan landatuak,
Gure lur eskualdunetan zuk zerutik hostatuak,
Chutik dagozi, bethi ederrik, oro zhiar-okhituak,
Erroak ifernuraino barna kurumilkatuak.

Phentze polliten erditik kantuz doan ithurria,
Ur bazterreko sasian, leiratuz dagon choria,
Sorhopiletan churi'ta gorri luchatuz dohan lorea,
Obra miresgarri hori ez dea oro zurea ?

Kampoetan zabaltzen den ogi eder urrezkoa,
Harrien artetik irriz ageri mahats molkoa,
Nork erein ditu, zeru ganetik, baizen'ta zuk, Jaun-Goikoa ?
Ahurra zabalduz duzu hedatu zure eskua.

Arranoak bortuetan, kaskoetan bas-ahuntzak,
Oihan-zolan ihiziek, arbola beltzean huntzak,
Desertuetan orroaz dauden lehoinak, tigreak, hartzak,
Gora erraiten dauzkute munduan zuk utzi hatzak.

Bas-ihizi nandietan balinbazaitut landatzen,
Gauza ttipi ttipietan ez zaitut ez nik mendratzen :
Ottia eta chinaurrittoa eder zaizkit iduritzen,
Lehoin azkarrak bezeimbat erleak zaitu handitzen.

Chimichtak zeru guzia noiz baitu ere urratzen,
Zure hasarre sainduaz berehala naiz orhoitzen ;
Barrukietan kabalak dira marrumaka ikharatzen,
Eta supazter chokoan lazturikan gu zeinatzen.

Gogoetan nagolarik, gaua horra nun den jautsi,
Izarrek zeru beltzean bederazka phizten hasi:
Jainko onaren begi argiak zareztela dut ikasi...
Mahuma urrun zazue, igorrazue ihesi!

Gauza orotan zarena, maite zaitugu orotan.
Jainko on eta handia, atchik gaitzazu zu-baithan !
Itsas gainean hi marinela, hi artzaina mendietan,
Jainko handi hortaz orhoit ; ez ahantz zuen oneta.