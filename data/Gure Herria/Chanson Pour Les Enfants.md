---
id: ab-3825
izenburua: Chanson Pour Les Enfants
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003825.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003825.MID
youtube: null
---

Lendabiziko ori, txoporen ori,
Bestek guziek baño txoporagoa dek ori.
Txingilingitxongi,
Txingilingitxon.

Bigarren ori,
Subien ori,
Beste guziek baño
subiagodek ori.
Txingilingitxingi,
Txingilingitxon.

Irugarren ori,
luxien ori,
beste guziak baño
luxiago dek ori.
Txingilingi-txongi,
Txingilingi-txon.
Txingilingi-txongi,
Txingilingi-txon.

Laugarren ori,
mantxeoen ori,
beste guziak baño
mantxoago dek ori.
Txingilingi-txongi,
Txingilingi-txon.
Txingilingi-txongi,
Txingilingi-txon.

Bostgarren ori,
txikien ori,
beste guziak baño
txikiago dek ori.
Txingilingi-txongi,
Txingilingi-txon.
Txingilingi-txongi,
Txingilingi-txon.