---
id: ab-3833
izenburua: Oilanda Gazte
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003833.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003833.MID
youtube: null
---

Oilanda gazte polliño bat gure herrian badugu,
Hegal-petik lumaño bat falta ez balimbalu,
Munduan aski ezin liteke oilanda hortaz espantu.

Oilanda hori nongo oilarrak trompatu othe derauku?
Ez trompatu, bai trompatu, ez dutela enganatu!
Oilanda hori, gure herrian, hola zerauku bantatu.