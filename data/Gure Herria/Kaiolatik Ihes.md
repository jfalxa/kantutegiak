---
id: ab-3877
izenburua: Kaiolatik Ihes
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003877.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003877.MID
youtube: null
---

Kaiolan nintzenian, maiz nindagon triste,
Kampoko lagunetan bainuen sinheste ;
Librotasuna ona zela nuen uste
Orai arte...
Amets, amets zoroa ! hik egin nauk kalte !

Banuen zertarik jan, bai eta zer edan,
Deusen eskasik etzen enetzat kaiolan ;
Bainan guzien gatik nintzen errenkuran,
Banuen lan !
Aski zabalik' ezin arterik atzeman !

Ene arthatzailea fidaturik nitan,
Athea idekirik behin utzi zautan...
Nahi nuena egin, orduan ni hautan,
Lorietan !
Lagunetara laster goan nintzen airetan.

Lehen ni usatua lo trankil egiten,
Orai huna berri bat zer dudan aditzen :
Gau-ihizi batzuek direla ibiltzen
Chori biltzen,
Aztaparren artean dituztela hiltzen !

Gauaz beldur-bizian, egunaz ikaran :
Non-nahi irriskatzen sartzea segadan !
Holakorik ez zaitan burura, kaiolan
Nintzenian ;
Zoin gaizkiago naizen, hobeki ustean !

Hegalttoak oramo ditut laburregi,
Nik nola ihes- egin belatch gaichtoeri ?
Bertzeak bezain airos ez naiteke segi
Laguneri...
Zer lana eman dudan ene buruari !

Ene hegalak aski luzatu orduko,
Uda akhabaturik, negua hasiko ;
Berriz uda-lehenik ez da ethorriko
Enetako !
Izotz ,elhur, hormetan, nola naiz biziko ?

Gaztea nahi bada egon segurean,
Ez dadiela eror nik egin hutsean ;
Bainan asmu hoberik harturik buruan,
Deskantsuan
Egon bedi kaiola ongi zerratuan .