---
id: ab-3861
izenburua: Aitaso-Ilobak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003861.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003861.MID
youtube: null
---

Ikusten dugu gizon bat,
aitasotu denean,
Etch'aitzinean jarririk
alkhi baten gainean;
Bere haurrak ezarririk
bizi molde onean,
Ilobaso chume bati
dagona solhasean.

Ez da mendi muthurrean
elhurra churiago,
Aitasok buru gainean,
bilhua duen baino;
Bainan bozki eta chuchen
mintzo baita oraino,
Atseginez entzutera
ilobasoa dago!

Aitasok erraiten dio:
Zuk ene haur maitea,
Ikhasia duzu ongi
Jainkoaren legea;
Nahi dautzut erakutsi
lurreko bizitzea
Nola izan dezakezun
zorionez bethea.

Egun guziz, artharekin,
othoitzazu Jainkua,
Eta atchik ait'amentzat
aphaltasun osua :
Orhoit zaite badutela
manatzeko eskua ;
Hura khenduz, urrun dago
bizitze bakeskua !

Arth'izarra agertzean,
Jaiki zaite ohetik,
Zeren ez baitu egunak
oren ederragorik ;
Bas'oilarrak orduan du
kantatzen oihanetik,
Bai'ta burua choriak
azalrzen hegal petik.

Ihizi hek ernatzean,
eman zaite lanean,
Lana baita errechago,
goizik hasten denean ;
Bozkanatuko zira
iguzki' agertzean,
Phentze-lilien usaina
zure ganat heltzean.

Mahainean lagunekin
zaudenean jarria,
Izan zaite alegera
eta maitagarria ;
Lotsaz dagon beharrari
eginaraz irria,
Sor dakion bihotzean
bozkario berria.

Eskuarazko mintzatzea
atchik'azu mihian,
Ez baita ederragorik
zeruaren aspian ;
Nola arrano zaharra
pimpirinen artian,
Halaber dago Eskuara
hauzokoen erdian.

Eskualdun andretan dira
baliosenak bethi :
Erlea bezain langile,
bildotsa bezain ezti.
Iragaiten zarenean
hoigoi urthetan goiti,
Beha zozu, hurbil danik,
halako den bati.

Azken urthea ez badu
aurthen ene biziak,
Erakutsiko dauzkitzut
Eskual dantza jauziak ;
Nik, aitasotua gatik,
ez ditut ahantziak ;
Ikhasi behar dituzu,
ahal badut, guziak.

Aitaso hasi zen kantuz
muchikoen airean,
Piko guziak izartuz
bi zangoez lurrean ;
Eta ez dute etsitu
ongi ikas artean...
Halako aitasoa da
maitagarri etchean .