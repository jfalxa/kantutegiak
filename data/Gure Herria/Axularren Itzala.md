---
id: ab-3868
izenburua: Axularren Itzala
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003868.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003868.MID
youtube: null
---

Zuhaitz handiak, lore ttikiak
berekin dakar itzala,
Nahi bezembat argitu gatik,
gizon orok hala hala!...

Errepika:
Itzalik gabe, aditua naiz,
Aingeruak ditezkela...
Huna non dioten Axularrez:
Itzal gabe bizi zela!

Sara herrian, nere anaiak,
eskuak elgarri guk har,
Gure bihotzen erdi erditik,
gora guziek Axular!!!

Jainkoarenak hartu ondoan, diote, Axular jaunak
Salamankan hartu zituela eskolak... Debruarenak!
Saritzat zer utz Galtchagorriri gure aphez Eskualdunak?
Utzi ziozkan aztaparretan... Zituzken itzalak denak.
Sara-herrian, nere anaiak...

Itzalik gabe, ordutik harat, bizi zen beraz herrian,
Aphez saindua, handi egina, orotan Eskual-Herrian.
Orori beha, oro zakizkan, oro zartzen agerian...
Ezkuara zuen gora altchatu, hari doakon neurrian.
Sara-herrian, nere anaiak...

Itzalik gabe hura bezala, nahi girea, anaiak?
Gure Axular bezembat garbi, nahi girea guziak?
Galtchagorriri, harek bezala, utzi... Erdaldunkeriak,
Aztaparretan utz debruari beraren kaskoinkeriak!

Azken Errepika:
Sara-herrian, nere anaiak, eskuetan basoak har!
Gora Ithurry, Elissamburu! Gora guziez Axular.