---
id: ab-3876
izenburua: Zahar Ezkont-Gaieri
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003876.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003876.MID
youtube: null
---

Errepika
Zahar ezkont-gaieri
Begiratzen dut ongi:
Hainitzen manerak zaizkit nigarringarri.

Bertsu berri batzuek behar tut ezarri
Gizon donado eta mutchurdin endreri,
Berrogoi urthetaikan goiti direneri,
Hautaketan dembora goan zaioteneri.

Mutchurdineri penak zaizkote doblatzen,
Beren lagunetarik dutenean aitzen:
Senhar edo haurrekin direla jostatzen,
Amodioz onheski elgar besarkatzen.

Bethi penatua da langile pobria,
Partikularzki zahar jaberik gabia;
Bihotza trichterikan dago donadua,
Bihotza trichterik'ta... ageri larrua.