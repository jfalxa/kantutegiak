---
id: ab-3797
izenburua: Chori Errechiñoleta
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003797.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003797.MID
youtube: null
---

Chori Errechiñoleta ederki kantatzen,
Bazter guziak ditu choratzen!
Bard'arratsian,
Sasi batian,
Biga baziren;
Eder ziren,
Charmant ziren
Manera oroz,
Eta algarrez hainitz agrados!

Batto arra zuzun eta bertzia emia,
Ez mirakulu parekatzia!
Han, lehen ere,
Dudarik gabe,
Ikusi ez balu,
Arra hura
Ez zen joanen
Sasi hartara,
Emia ez balitz jin bid'ertira!

Oi choriño gaicho heien beldurti handia,
Ikustiarekin ihiztaria,
Batto han gaindi,
Hain inozentki,
Pasatzen zela;
Arra hura beha jarri,
Eta hor, gero,
Sasian sartu zen, han gordatzeko.

Eta orduan berean, ontsu zen emia,
Sasi handian barna sartua;
Hor aphaindurik,
Ta lumaturik,
bere papoa;
Kukurusta
Harroturik,
Jarri zenean,
Umez orhoit zen, ber'ohantzean.