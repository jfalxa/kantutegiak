---
id: ab-3853
izenburua: Mendian Zoin Den Eder
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003853.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003853.MID
youtube: null
---

Version Labourdine eta Bas-Navarraise

I
Mendian zoin den eder epher zango gorri !
Ez da behar fidatu itchur eder horri.
Ene maiteak ere bertzeak iduri :
Niri hitzeman eta, gibelaz itzuli.

II
Ene bihotza duzu zu ganat erori,
Den guzi guzia atchikia zuri;
Eta zurea aldiz harria iduri ! ...
Ene begi gaichoak nigarrez ithurri !

Version Souletine

I
Oherat ziradia,
lozale pollita ?
Oherat ezpzira,
jin zazkit leihota ;
Hitzño bat erran eta
banua berhala.

II
Gora düzü leizarra,
gorago izarra;
Oran eztiz oren bat
oherat nizala,
Ohetik jeikitzera
herage düdala .