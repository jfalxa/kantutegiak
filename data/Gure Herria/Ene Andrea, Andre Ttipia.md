---
id: ab-3795
izenburua: Ene Andrea, Andre Ttipia
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003795.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003795.MID
youtube: null
---

Ene andrea, andre ttipia,
Bainan emazte garbia !
Choriak sasian
Erne kantatzian,
Bethi jeikia da ;
Gero, pitcherra buruan doa
Laster ithurrietara.

Bildotcha bezain ezti da eta
Langile nola erlea ;
Sukalde gamberak
Ta zoko guziak
Bethi garbi dira.
Ai ene andre ttipi ttipia
Gu guzien iguzkia !

Arrotza jiten bada etchera,
Ezpainetan du irria ;
Hari ohoreak,
Grazia guziak,
Ta mahain churia.
Huna salda'ta arno gorria
Sototikan ekarria.