---
id: ab-3809
izenburua: Galerianoaren Kantuak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003809.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003809.MID
youtube: null
---

Mila zortzi ehun'ta hamabortz garrena
Ni Ahazparnen preso hartu nindutena ;
Plumagaineko premu orok dakitena,
Galeretan higatu beharko naizena.

Kantatzera nihazu alegera gabe,
Ez baitut probetchurik trichtaturik ere;
Nehun deusik ebatsi. gizonik hil gabe,
Sekulakotz galerak enetako dire.

Ala zorigaitzeko premu izaitea !
Harrek eman baiteraut bethikotz kaltea;
Aitari galdeginik sortzeko partea,
Galeretan eman nau, hauche duk dotea !

Ene aita da gizon kontsideratua,
Semia galeretan du segurtatua ;
Nun nahi othoitzean belaunikatua,
Saindu iduri debru madarikatua !

Ene lehen kusia, Cadet, Bordachuri,
Fagore bat banuke galdatzeko zuri ;
Ongi adreza zaite ene arrebari,
Ene saltzeko zombat ukan duen sari ?

Aita aitzinean'ta arreba ondoko,
Osaba burjes hori diru fornitzeko ;
Ez, ordian enetzat bi sei liberako,
Galeretan bederen leher egiteko !

Elizan sartzen dire debozionerekin,
Iduriz badohazila saindu guziekin ;
Beren liburu eta arrosarioekin,
Debruak pesta onik einen du heiekin.

Zortzigarren bertsua aneiaren dako ;
Kontseilu bat banikek hiri emaiteko ;
Ontsa goberna hadi, etzauk dolutuko,
Ni baitan etsenplua errech duk hartzeko.

Zuri mintzo nitzaizu, oi, aita zilarra !
Ardura dudalarik begian nigarra ;
Zure eta ene arraza Bordachuritarra,
Galeretan naizeno ni bainaiz bakarra.

Kantu hauk eman ditut Paube-ko hirian,
Burdinez kargaturik, oi! presondegian ;
Bai eta kopiatu denbora berian,
Orok kanta ditzaten Hasparne herrian.

Hok eman izan ditut, ez changrinatzeko ;
Ahide, adichkidek, kuraie hartzeko,
Eta portikulazki. aita, zuretako,
Kantu hok aditzean semiaz orhoitzeko.