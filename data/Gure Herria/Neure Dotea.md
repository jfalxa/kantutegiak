---
id: ab-3799
izenburua: Neure Dotea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003799.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003799.MID
youtube: null
---

Aitak eman daut dotea,
neurea, neurea: (bis)
Urdeño bat bere umekin,
Oilo koroka bere txitoekin,
Tipula korda heiekin!
Tipula korda heiekin!

Otsoak jan daut urdea,
neurea, neurea! (bis)
Acheriak oilo koroka,
Arratoin batek tipula korda
Adios neure dotea!
Adios neure dotea.