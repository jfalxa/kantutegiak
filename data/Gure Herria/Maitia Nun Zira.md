---
id: ab-3789
izenburua: Maitia Nun Zira
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003789.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003789.MID
youtube: null
---

Maitia nun zira ?
Nik etzutut ikhusten,
Ez berririk jakiten,
Nurat galdu zira ?
Ala khambiatu da zure deseiña ?
Hitzeman zenereitan,
Ez behin, bai berritan,'
Enia zinela.

Ohikua nuzu,
Enuzu khambiatu,
Bihotzian beinin hartu,
Eta zu maitatu.
Aita jeloskor batek dizu kausatu
Zure ikhustetik,
Gehiago mintzatzetik,
Hark nizu pribatu.

Aita jeloskoria !
Zuk alaba igorri,
Arauz ene ihesi,
Komentu hartara !
Ar'eta ez ahal da sarthuren serora:
Fede bera dugu,
Alkarri eman tugu,
Gauza segura da.

Zamariz iganik,
Jin zazkit ikhustera,
Ene kontsolatzera,
Aitaren ichilik ;
Hogoi eta lau urthe bazitit betherik:
Urthe baten burian,
Nik eztiket ordian
Aitaien acholik.

Alaba diener
Erranen diot orori:
So'gidaziet eni,
Beha en'erraner ;
Gaztetto direlarik untsa diziplina
Haduitu direnian
Berant date ordian,
Nik badakit untsa. (l°.