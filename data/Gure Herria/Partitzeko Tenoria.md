---
id: ab-3794
izenburua: Partitzeko Tenoria
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003794.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003794.MID
youtube: null
---

Mila zortzi ehun'ta berroi hemeretzi,
Kantore berri batzu nahi tut ezarri,
Penetan direnentzat, oi, kontsolagarri !
Gauzak har ez ditzaten sobera barnegi.

Kantu, hauk eman ditut neure buruari,
Zeren bihotzian dut haiinitz sofrikari :
Gorte eginik nago izar eder bati,
Eta ni malurusa zortian erori !

Partitzeko tenoria huna non den jina,
Urrikaltzeko baita kasu huntan dena ;
Izar baten uzteak emaiten daut pena,
Zeren hura bainuen munduan maitena.

Izar charmegarria, oi penetan gira,
Pena doloriak oro gure gainean dira ;
Zure hitzetan fidel egoiten bazira,
Jainkoaren graziak gu ganako dira.

Graziak hausten dira, oi demborarekin,
Eta partikulaski, zure'ta nerekin,
Ezkontzekotzat beraz biak elgarrekin,
Armadetako berri behar dugu jakin, ...