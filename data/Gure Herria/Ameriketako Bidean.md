---
id: ab-3839
izenburua: Ameriketako Bidean
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003839.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003839.MID
youtube: null
---

Huna jadanik hamar egun,
Itsasoko itsaso nohala,
Nere sor lekutik hain urrun,
Utzirik amaren hegala! (bis)

Ai, nere Amaren nigarrak
Orai ez daude chukaturik,
Aita zenaz seme bakharra
Baitzadukan kontsolaturik!

Arreba, egon zaite hor, zu,
Ama maitearen aldean ;
Jinen natzaiola errozu,
Urthe laburrikan barnean.

Iñharak huna, airez aire
Nun doazin alde hartara ;
Hegalak banitu nik ere,
Nork nintzazke joaitetik bara ?

Ametsetarik nintzen, barda,
Aberats etcherat itzuli ;
Bainan ene ametsa joan da,
Eta oro zaizkit itzali...

Lore bat bere baratzean
Ikusi dut eta han utzi ;
Berari deus ez diot erran.
Igorriren diot goraintzi.

Hor berean, lore gordea,
Egon zakizkit, histu gabe
Zu izanen zare nerea,
Eta nik ditudanen jabea.

Ene gazte lagun maiteak,
Sor-herriaz goza zaitezte ;
Oro utziz, hortik joaiteak
Ni naduka ni orai triste !

Diruaren gose zorrotza,
Hik nerabilak herraturik,
Hik dautak idortu bihotza,
Hik ezarri dautak hozturik.

Aberastu nahi naiz ; bainan
Zuzenez eta ohorezki.
Herrirat heltzen naizenean,
Dirudun fama ez zait aski.