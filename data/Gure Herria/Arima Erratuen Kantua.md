---
id: ab-3774
izenburua: Arima Erratuen Kantua
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003774.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003774.MID
youtube: null
---

Oi mirakul guziz sinheste nekea,
Arima gaicho baten berriz itzultzea;
Mundutik joana zela bazuen urthea;
Urthe-buruz egin du berriz itzultzea.

Ama zuten elizan urthe-buruzale,
Ahizparuk gaztena etcheari soile,
Hunek ez zuen uste bazela nehore,
Boz bat hor entzuten du, berak uste gabe.

Boz hura entzun eta, so'iten du gainean,
Ustez auzoko haurrak zituen etchean...
Bere ahizpa hila ikusiz bet-betan...
Harriturik, oihuka hasten da orduan.

Hilak ordu berean: "O ago ichilik!
"Ez daunat deus eginen, ez egin nigarrik!
"Ni hire ahizpa nun, lur gainerat jinik,
"Zerbait laguntza nahiz, orai hire ganik..

- "Zer nahi duzu beraz, ahizpa maitea?
"Purgatoriotan da zure izaitea?
"Zu hantik tiratzeko banu berthutea,
"Egin nezake segur en'ahal guzia...

- "Joan behar dun joan gure elizara,
"Eta hantikan gero, Santa Barbarara,
"Ta ororen buruan, San Madalenara...
"Boturikan ez egin, arinki sekula"...