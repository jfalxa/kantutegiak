---
id: ab-3874
izenburua: Baratzean Zoin Den Eder
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003874.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003874.MID
youtube: null
---

I
Baratzian zoin den eder julufri, arrosa !
Ez da aise enganatzen neskatcha amorosa !
Gorphutz abantailosa, berenaz kuriosa;
Mintzatzen ere badaki, plazer duen gisa.

II
Oi Azkaindar neskatchak, ez penarik hartu !
Ez zituztet nahi oro baltsan sartu;
Dendari gazteño bati nahi naiz mintzatu,
Suieta nolakoa den, zer zaion gerthatu.

III
Primadera eder bat aurthen du pasatu ...
Baratzeko loria goizegi da lortau;
Sasoin kontrako loriak gauz'onik ez baitu,
Gau batez kampoan egonik, izotzak jo daiku !

IV
Bethi mintzozinela bihotzaren erditik,
Ez zinuela behar nitaz bertze maiterik ...
Orduan hitzak eder, oi, zure ahotik !...
Batere ez nindabilan ni zure ondotik ...