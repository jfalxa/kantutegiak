---
id: ab-3805
izenburua: Zazpi Uso Badoazi
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003805.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003805.MID
youtube: null
---

Zazpi uso badoazi hamalau hegalez,
Beren mendi-lephoak atcheman beharrez;
Kampoan hiltzera noa, maitia, zure minez,
Bortha idek zadazu, Jaunaren amorez,
Jaunaren amorez.

Zombat ere baitakik ederki mintzatzen,
Ez nauk hik uste bezain errechki trompatzen;
Egiazki mintzatzeko derauiat erraiten,
Jin haizen bide beraz etcherat joan hadien!

Ene maite pullita, buruan zer duzu?
Gau et.eguna bezain sanjakor zira zu!
Nihon promes emanik zuk balimbaduzu,
Aintzin gibeletara guardia emazu!

Maitia, nik banuke aski borondate
Horrembertze penarik zuri eman gabe;
Bainan gure etchian sendi bazintzate,
Sekulakoak heiekin eginak zintuzke!

Maitia jiten bazira gauerdi heineko,
Zerbeiten egiterat naiz entseiatuko;
Ordukotz etchekoak dira lotarako,
Sokekilan bederen zaitut tiratuko.

Norbaitek egin dautzu herriki bisita,
Chinilla urheria lephotik jauzika;
Horrek utzi derautzu present bat pullita,
Kontsolaturen zira, hura ikusi.ta.