---
id: ab-3855
izenburua: Ene Maitia Barda Nun Zinen
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003855.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003855.MID
youtube: null
---

Version Bas- Navarraise

1
Ene maitia, barda nun zinen,
Nik borthanua joitian ?
- Buruan ere min nuen eta
Dudarik gabe ohian ... (bis)

2
- Bard'arrastiasn, ametsetarik,
Botz bat entzun dut charmantik,
Eztitasunez bethia baitzen,
Haren parerik ez baitzen ! (bis)

3
- Eztitasuna gauza ederra,
Nork nezake ni kondena?
Gauaz loale, egunaz ere,
Errepausurik batere ! (bis)

4
- Charmegarria, lo ziradia,
Eztitazunez bethia ?
Lo bazirade, iratzar zite,
Etziradea loz ase ? (bis)

5
- Arboletan den ederrena da
Oihan beltzian phagoa...
Hitzak ederrak dituzu, bainan
Bertzetan duzu gogoa ! (bis)

6
- Ni zure ganik despegitzia
Iduritzen zaut hiltzia !
Hiltzen ez baniz, bihurtuko niz !
Adios, ene maitia ! (bis)

Version Souletine

1
Gaiaz eder da argizaria,
Egünaz ere bai ekhia ;
Haien pare da enen maitia,
Arren haiñ da carmagarria !

2
Igaran gaian, ametsetarik,
Botz bat entzuün dit charmantik,
Eztitarzünez betherik beitzen,
Haren parerik ezpeitzen !

3
Charmagarria, lo ziradia
Eztiatarzünez bhethia ?
Lo bazirade, iratzar zite,
Ez othoi ükhen herabe !

4
Amudiua, gaiza erhua,
Jentia trumpa liuna !
Gaiak lo gabe, egünaz ere
Errepausürik batere !

5
Zure ganik orai phartitzia
Üdüritzen zait hiltzia !
Indazüt pot bat, ene maitia,
Mentüraz date azkena !...