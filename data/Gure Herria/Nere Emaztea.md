---
id: ab-3782
izenburua: Nere Emaztea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003782.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003782.MID
youtube: null
---

Ezkondu arte zerbait banintzan, ezkondu eta deusik ez,
Ezkondu arte zerbait banintzan, ezkondu eta deusik ez.
Ederrez gose banintzan, ere, ase naiz segur ederrez !
Nere gostua egin nuen'ta, orai bizi naiz dolorez.

Nere andrea alferra da'ta, ez da munduan bakarrik,
Nere andrea alferra da'ta, ez da munduan, bakarrik ;
Gauza gozoen zale da eta ez egiten. beliar denik ;
Sekulan ez litzakete behar holakoaren senarrik.

Larumbatetik larumbatera garbilzen ditu chatar bi, (berriz)
Chatartto horien berotzeko erretzen egur hamabi ;
Bi belaunetan bana hartu'ta ez da ichiltzen kantari.

Nere andrea goiz jeikitzen da, phestara behar denean; (berriz)
Buruko mina egiten zaio hasi denean, lanean...
Haren barnea ikusi banu ezkoatzeko aintzinean .