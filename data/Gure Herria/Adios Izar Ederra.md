---
id: ab-3863
izenburua: Adios Izar Ederra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003863.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003863.MID
youtube: null
---

I
Adios izar ederra, adios izarra!
Zu zare aingerua, munduan bakarra !
Aingeruekin, aingeruekin zaitut komparatzen;
Zembat maite zaitudan ez duzu phentsatzen.

II
Adios izar ederra eta khariua,
Nerure begietako lili arrarua !
Bihotz zure (1), bihotzez zure, gorphutzez banua...
Jarraikiren zaitazu zure zmodiua (2)

III
Izan naiz Araguan, puchaka bat Kastillan,
Hobeki (3) erriteko España guzian;
Ez dut ikhusi, ez dut ikhusi zu bezalakorik...
Nafarroa guzian zude famaturik.

IV
Jarraikitzen ninduzun izar eder hari
Untzian mariñeka nola orratzari (4);
Atentzione, atentzione en arrazoinari :
Ez zieztela fida amodioari(5) !

V
Aniduia dyzy arrisareb oare:
Ysauba bady eta... ondoan arrantze;
Maitia eznauke, maitia ez nauke zu gana jin gabe,
Hil behgar banu ere hirur egun gabe.