---
id: ab-3822
izenburua: Mendiak Bethe Belarrez
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003822.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003822.MID
youtube: null
---

Erregek (1) gizon ederrik, Guardetan badu segurik!
Lekumberriko brigadan, batto ororen gainetik,
Neskatchen enganatzen, ez baitu parerik!

Ene maitia zer duzu, zerk holakatzen zaitzu zu?
Ez du dembora luzia, zirela penetan sartu...
Plazeraren ondotik, desplazera duzu!...

Mendiak bethe belarrez, begitartiak nigarrez...
Lan eginaz urriki dut, bainan probetchurikan ez!
Orai hemen negozu, trompaturik, minez.

Mendian eder arbola, luze lerdena...
Zu zirade mundu huntan. Nik maitatzen zaitudana,
Bisitaturen zaitut, ahalik maizena...

Lekumberriko neskatchak, Zuberoara banoa,
Zuberoako kolombak daitzen bainau bere gana:
Hautau behar dela, zuzenez, lehena.