---
id: ab-3887
izenburua: Altabizkarreko Kantua
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

ALTABIZKARREKO KANTUA

I
Oihu bat aditu da deia lazgarritan...
Eskualduna gelditu ernerik athetan,
Erraten duelarik : " Nor da hor ? Zer nahi ?
" Holako bozik hemen ez aditzen ohi !.

II
Zakurra ere ilkhi lo zagon tokitik,
Ezin geldiraz nihork sainga egitetik ;
Altabizkar guzia harrabotsez behte,
Segitzen Ibañetak luze duen arte !

III
Behin, ilhun iduri, harrokaz harroka,
Ezker eskuin punpatuz, erreketan joka...
Armada seinalatzen hurbil behar dena:
Ez du lo atzemanen herriko gizona.

IV
Eskualdunak menditik ihardetsi gora,
Tutu adirazteko hazkar du bulharra;
Corrochtuz halzairua oihuka da hasi :
" Hinbertze gizon nihork hemen ez ikusi !

V
" Hara, hara non diren, zer lantza sasia !
" Erdi'oro banderaz nola estalia !
" Harmetarik chimichtak jarian iduri :
" Errak, haurra, zembat den, kontu lerroeri..

VI(1)
- " Bat, bira, hiru, lau, bortz, sei, azapi, hameka,
"Hamalu, hamazazpi, hoita bat, milaka;
"Bethi lerro segitzen ! Ez ditezke konda ;
" Zeruan izar baino gehiago bada ! .

VII
- " Ilkhi kampo, muthilak, beso hoiekmira !
" Menditako harrokak errotik athera !
" Leher, leher buruak, ez utzi bihirik ! ...
" Etzuten bertze galde, hunat ethorririk !

VIII
" Zertako nahi zuten herria nahasi ?
"Hek baino lehen gare gu hemen ikusi ;
" Jainkoak mendi' eman guretzat atheka ...
" Hekien artean nihor ez ager lerroka !.

IX
Gain behera ordutik harri' abiatzen ;
Gizonek ez balio ; odola ichurtzen ...
Zembat hezur lehertu ! Iholdez odola !
Tutua du Rolandek jotzen berehala.

X
Egiten du harrabots, emanez indarra ;
Mendiak gora, bainan gorago adarra;
Haren orroa gaten, joka harriz harri...
Karlomani ematen urrunean berri.

XI
- " Gerlan dire lagunak !" Erregek erraten;
Ganelonek, kontrarat, hari ihardesten :
- " Erregez kanpo, batek erran balu hori,
" Beiraturen guziek, ... nola gezurrari. .

XII
Ondikozko Rolandek orro berritz ere ;
Hats handiz, bortcharekin, egortzen dolore ;
Aho guzia odol, kanpo burru fuinak ...
Tutuaz adirazten bere bihotz minak !

XIII
Karlek oraino berritz aitzen lagunetan ;
Rolando dela , dio, behar etsaietan.
Ganelonek erraten :" Kondea, bidetan,
" Morgak dabilka handiz, nausi itchuretan.

XIV
" Aintzinat jo zaldiak, ez da egoterik,
" Lur handia oraino da urrun gutarik !"
... Rolandori odola zurrutan dohako,
Buru arrailatutik ageri barneko !

XV
Jotzen berritz adarra, eta Karlek aitzen ;
Dakharken hats luzeak ez guti izitzen !
" Segurki, dio Nesmek, hasi da gudua ;
" Dugun laster sokhorri gerlan den mundua !.

XVI
Karlomanek tutua joarzten gora;
Jausten ere gibelat, burdinaz bulharra,
Harmak zorrotz eskutan. Gauak argi guti ;
ur hainitz erreketan, jausteko behiti !

XVII
Bi armada burutan jotzen du tutuak ;
Karlomanen zaldiak zangounhatuak ;
Bere bizar zuria bulharrak behera ...

Beantegi agrtu oren bat sobera !!!

XVIII
" Indar eta zaldirik balinbaduzue,
" Ahalik lasterrena, ihes egizue;
" Karle, egik lasterka hire jakarekin ;
" Jaka gorri etzohak ... plumaia beltzekian !

IXX
Armadetako lili, Rolando gaizoa
Ikusiren, hara han, gure besoz joa!
Hurbil hunat guziak; jauts gaiten behiti!
Ihesi dohazinak... Ehortziak bethi!...

XX
Gehiago nihor ez! Non lantzen sasia!
Non hanbat koloreko banderen gaindia?
Goibeldu dira harmak; non dute dirdira?
Zembat diren oraino, eian konda, haurra!

XXI
Hogoi, hamabortz, hamar, bia, bat, batere;
Bihi bat ez ageri, bilhaturik ere!
Deskantsu etchetarat zohazte, nausiak;
Cahkurrak etcholetan sarraraz guziak!

XXII
Zuen emazte oank, bere haur chumekin,
Besarka bozkario, ikustearekin;
Chuka zuen puñalak, ez ahantz tutuak!
Eman buru azpian batean lothuak.

XXIII
Arranoak menditik sarri agerturen,
Zein lehen chukatzeko lehertu hezurren;
Mende luzen ondoan, hemen hezur zuri
Agertuko metaka, jenden izigarri!!!!

(1) Huna - persuak balimbadira eta ez - jendeak nola kantatzen dituen,
bertze persu hauk ( Bertzerik ez dakite ere gutiz gehienek ):
Bat, biga, hiru, lau , bost, sei, zazpi'ta zortzi,
Bederatzi, hamara'ta hameka, hamabi.
Hamahiru,hamalau, hamabost, hamasei,
Hamazazp'hemezortzi, hemeretzi, hogoi.

Hogoi, hemeretzi, hemezortz, hamazazpi,
Hamasei, hamabost, hamalau, hamairu,
Hamabi, hameka,hamar, bederatzi,
Zortzi, zazpi, sei, bost, lau, hiru, biga, bat .