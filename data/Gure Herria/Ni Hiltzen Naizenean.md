---
id: ab-3838
izenburua: Ni Hiltzen Naizenean
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003838.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003838.MID
youtube: null
---

Ni hiltzen naizenean,
Tra-la-la-la-la-la-la-la,
Ni hiltzen naizenean,
Ez ehortz elizan,
Ez ehortz elizan!

Ehortziren nuzue,
Tra-la-la-la-la-la-la-la,
Ehortziren nuzue,
Soto baten zolan,
Soto baten zolan!

Zangoz bortharat eta,
Tra-la-la-la-la-la-la-la,
Zangoz bortharat eta,
Ahoz dutchulura,
Ahoz dutchulura!

Dutchulua eroriz,
Tra-la-la-la-la-la-la-la,
Dutchulua eroriz,
Baduket zer edan,
Baduket zer edan.