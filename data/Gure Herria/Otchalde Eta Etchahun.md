---
id: ab-3891
izenburua: Otchalde Eta Etchahun
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Agur, adichkidia, Jinkoak egun hun;
Zer berri den errazu, zuk, othoi Chuberun?
Ezagutzen duzia Barkochen Etchaun
Halako koblaririk ez omen da nihun.
Bisitaz juan nindaite, ez balitz hain urrun.

Jauna, ezagutzen dit Etchahun Barkochen,
Egun oroz nintzaiko hullanik ebiltzen.
Bethi gazt'ezin egon, ari da zahartzen:
Lau hofei urthe ditu mundin igaraiten;
Orai ez da bersetez hanitch okupatzen.

Jauna, behar dautazu gauza bat onhetsi,
bainan ez zautzu behar hargatik ahanzti;
Huntaz egiten dautzut gomendiorik aski:
Komprenituren duzu hitz laburrez naski.
Errozu ene phartez milaka goraintzi.

Zure komisionia nahi dizit egin,
Kombenitzen zidazut hanitchplazerekin.
Egon nahia zira arren jaun harekin?...
Huna Etchahun bera, present da zurekin...
Nik ere ez diota zu nor ziren jakin?

Jakin ere behar duzu, dudarikan gabe,
Zeren egin dautazun errespetuz galde.
Entzutia baduzu nitaz lehen ere ;
Nor nizan erraitia ez dizit herabe :
Lapurdin deitzen nute Jean Baticht Otchalde.

Bihotzaren erditik egiten dit irri,
Zure ganik entzunik parabola hori.
Damutzen ez bazaiko zeluko Jaunari,
Behar ditzigu eman zumbeit berset berri,
Bai eta bei demboran eskiak elgarri.

Amodioz derautzut eskia hedatzen,
Zembat atsegin dutan ez duzu phentsatzen.
Gizon batzuek nute ainitz atakatzen,
Ez dutala ikhasi phersuen emaiten...
Zuk laguntzen banauzu, ez nute lotsatzen !

Etchahun Ziberun, Otchalde Lapurdin
Buruzagi dirade kantoren egitin.
Ez gitutzu gu beldur, nor nahi jin dadin :
Erranen dit orotan nik gur'Uskal-Herrin :
Jokaturen dugula hek plazer dutenin.

Ez zaitela bada sar sober'urgulian,
Etsaminatu gabe zur'izpiritian.
Irakurtu izan dut, lehen, liburian :
Jaunen jaunak direla agertzen mundian,
Erbia lo dagola ust'ez den lekian.

Lehen zaharretarik nik entzun dut hori :
Bethi badela norbeit denen buruzagi...
Arren, nahi badira gure kontra hari,
Unheski kantatzeko zombeit bertset berri,
Lotsagabe gituzu denen zerbitzari.

Huntan akabatzen dut erranez : Halabiz !
Ez dolu-minik hartu, erraiten dut berriz :
Hiltzeari nigarrez, sortzeari irriz ;
Hola joanik badugu hemen martir anitz...
Ochala ! hek bezala zeruetan banintz !

Egunaren pare da gizonen bizia :
Goizian azkartasuna, zer inozentzia !
Eguerdian da aldiz indar garhaitia ;
Arratsaldian berritz flakatzen hasia ;
Ilhuna zerratzian, akabo guzia !.