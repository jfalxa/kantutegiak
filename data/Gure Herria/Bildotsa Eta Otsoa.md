---
id: ab-3843
izenburua: Bildotsa Eta Otsoa
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003843.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003843.MID
youtube: null
---

Primadera goiz batez,
chirripa batera
Ama gabe, bildotcha
Gan zen edatera.
Hantchet ari zelarik
Churga churga bera,
Oihanetik otsoa
Zitzaion atera.

Otsoak, gorachago
urean sarturik:
Ur hau, erraten dio,
Zikhintzen dautak hik.
- Bildotchak errepusta:
Jauna, nik nihundik
Hemendik ez dezaket
Zikhin zure urik.

- Ichil hadi, kichkila,
jaz ari baihintzen
Ene fama onaren
Nun nahi funditzen.
- Jaz ni oraino, jauna,
sortzekoa nintzen;
Amaren ttittirat naiz
Oraino ibiltzen.

- Hire anaia zuan,
hi ez balin bahaiz.
- Ez dut nik anairik;
ume bakharra naiz.
- Hots! Zuetarik norbeit
entzun diat nik maiz,
Nitaz bazerasala
Zer nahi elhe gaitz.

Hortan zen bildotchari
oldartu otsoa,
Eta bi klaskaz zuen
Iretsi gaichoa...
Azkarra duenean
Partida flakoa,
Hauzia zoinena den
Erran gabe doa.

Otsoak bildotchari
egin zioena,
Jainkorikan ez balitz,
Nork liro kondena!
Jainko legetik kampo,
Zertan da zuzena?
Galde zazu indarrez
Zoin den nausi dena.