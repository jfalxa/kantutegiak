---
id: ab-3882
izenburua: Gerla Handiko Kantuak
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Hiru hogoi eta hamarreko soldado zahar maiteak.
Ez zaituztet nik mendratu nahi, sobera zaizte handiak;
Bainan zuhaurek erranik dakit, badirela jaunen jaunak
Gerla huntan ditugu ikusi soldadotan handienak.

Gerla handiko soldado gaitzak, zuen ongi goresteko,
Otchalde eta Elizamburu Koplari ziren beharko.
Heien eta Zaldubiren orde othe nauzue nahiko ?
Bozez mila banitu gaur nere, oro nuzke zuendako.

Aboztuaren bian, ezkila, gure Eskual-mendietan,
Bambaka hasi zitzaukularik, herri choko guzietan,
Oro nigarrez, ichil ichila, gure etche churietan !
Bazen, orduan, jauzi ederrik, Eskualdunen ..bihotzetan !

Adios aita, adios ama, adios ene laguna,
Adios ene herri maitea, herrietan maiteena !
Bihotzean ezarri ondoan gure Nausi Jesus ona,
Lerro lerro, joan ginen oro, etsai higuinaren gana.

Eta gero, handik berehala, oi egun izigarriak !
Belgikatik herrestaka heldu, gose eta egarriak ;
Launazka elgarren lotuak, lotuak'ta lokartuak...
Beharbada, jaten duelakotz, lokartzen den gosetuak.

Bainan huna Buruilako goiz bat, goiz orotan ederrena,
Huna gure zeruko Amaren munduratzeko eguna ;
Huna Joffre, huna Marne ibaia, huna bitoria, huna :
Gure baioneten aintzinean, ihes zoan Alemana !!

Bainan zorigaitzez, gero laster, gure soldado maiteak
Erreka zilotan sartu ziren, sator eginak guziak.
Eta nolako sator eginak ! Zer izigarrikeriak !
Verdunez aiphatzean, chut chuta, emaiten zaizkit ileak.

Errabietan, hilen gainetik igaranez, Alemana
Burrustan heldu zaukun orroaz, Parise handia gana.
Ziloa tapatu behar baitzen, ahalik'ta lasterrena,
Hats-hanturik harat hupatu zen soldadotan hoberena.

Gain hartara hurbiltzearekin, oi gerlarien antsia !
Zer burrumba, zer ihurzuna, burdinezko erauntsia !
Harat igan soldado gaichoa ez othe zen ehortzia?...
Chuti hadi, eta harriturik, behazak, beha, Frantzia !

Jeneral nausiak erran zuen : « On ne passe pas ! Halte-là!
Eta manu izigarri hori atchiki zen hala hala.
Etsai higuinak han higatzen du ,orroaz, bere ahala...
Trebes emanak hantchet zinezten Frantsesez mila'ta mila.

Verdungo berri ez dakienak deus handirikan ez daki ;
Ematzu nahigabe guziak, ez duzu erraiten aski :
Gose eta egarriarekin, indarrak orori gaki...
Eta halere, chichpa eskuan, soldadoak han egoki !

Zeru bat ! erranen baitzinuen Kalbarioko ilhuna ;
Erauntsiak gainetik burrustan, zangotan lohi zikina ;
Belhaunetarainokoa, basa, oro iretsiz zoana,
Gure alde aldean, askotan, jaten baitzaukun laguna !

Bazterrak oro ikaragarri, oro chirchirikatuak,
Haragi bizian bezen barna, mendichka hek irauliak ;
Ez ikusten non finka zangoak, ez eta ere begiak,
Oro zilo, oro zauri puska, non nahi soldado hi'lak.

Azken Judizioko erreka hola bide da izanen...,
Ez arbolarik, ez aldaskarik, loreño bat ez ikusten.
Herioaren erresuma da ilhunik hemen hedatzen,
Atzo horiek eta egun hauk... lerroka oro erortzen.

Eta herioa bethi heldu, orotarik, hegaldaka,
Gau et'egun chichtuka, orroaz, garrasian, burrumbaka,
Erhotuak bezala, kanoiak, ehunka eta milaka,
Zilo, harri eta bulharreri, urrundik ari zafraka.

Lanho eta khe pozoindatuek bulhar-begiak erreak ;
Zaurituak'ta zilokatuak, urtzen errechimenduak ;
Tresna izigarri hoien kontra, ezin du deus soldadoak...
Ama gaichoak ikus balitza Verduneko bere haurrak !

Gibeletik harat ezin ethor den gutien laguntzarik... Mundua ?
Bainan, hor, gibelean, othe da ere mundurik ?
Eta, lurrarekin bat eginak, zilo hetan ehortzirik,
Frantses soldadoak han egoki, hitz bat gabe, mututurik!

Aintzin, ezker edo eskuinean, norat ari da gudua ?
Deus bertzerikan hek ez dakite, baizen tokian hiltzea,
Bere aldean erori zaion lagunak odolstatua,
Noiz eroriko den othe bera?... Hona, han, bakar gogoa...

Eta, geroan, goiz-alde batez, noizpeit jautsi direlarik,
Ezin sinhetsia dute orok, hek ditezkela bizirik...
Ikaran eman hadi, Frantzia, ez dukek, hor, urguilurik,
Munduak sekulan ez dik izan hoin handiko soldadorik !

Nola ez eraman bitoria, holako soldadoekin ?
Hegalez zaflaka, bitoria, Frantzia, eman hirekin !
Bethidanik, gudu handietan, delakotz zuzenarekin,
Jainkoak begiratu zerutik, eta eman duk gurekin.

Zuizatik eta Nort'Itsasora, ikaran, hor, aintzinean,
Etsaia, chichpak bizkar gaineari, gineraman, azkenean :
Gure Foch gaitzak zafratzen zuen, lehengo Marne ibaian,
Castelnau, oldar izigarrian, saltatzera zen abian...
Eta gero, San Martin goiz batez, oi gu ororen loria !
Lau urthez entzun orro handia betbetan zen eroria !
Clemenceau zaharra erhotu zen, erhotu Frantses-Herria;
Elizan, ezkilen zalapartaz, dardarikatu dorrea.

Gora beraz Frantses soldadoa, gora, bai mila'ta mila !
Erran die : « Hire aintzinean, belhaunkatu behar zela !
Hiru hogoi eta hamarrean belek ebatsi hegala
Arranoari diok ekarri, ichuriz hire odola.

Zoin handiko zorra diotegun hil diren soldadoeri,
Soldado gaicho hoien artean, gure hil Eskualduneri !
Ja ehertu uhaineri esker nola urak leihorran,
Hil direnen uhaineri esker, heldu gaizkok, gu, ...gainari.

Azken hitza zuri, Jaungoikoa, zuri ere, Ama Ona,
Zueri esker bizi baigira, bizi Frantzia guena.
Eta hik, gerlan odol husturik, erori haizen laguna,
Lurrean badukek amodio, zeruan aldiz korona!!.