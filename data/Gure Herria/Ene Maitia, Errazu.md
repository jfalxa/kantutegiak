---
id: ab-3869
izenburua: Ene Maitia, Errazu
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003869.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003869.MID
youtube: null
---

Ene maitia, errazu;
Zerk hola changrintzen zaitu?
Ikustera jinen nitzaitzu,
Beraz kuraie har zazu;
Amodioz nahi ez badute,
Bortchaz eginen gitutzu.

Een maitia igorri
Segeretuian goraintzi:
Ikustera joan nakion,
Laster hilen dela naski;
Oi! Baduela aspaldi
Ez nauela ni ikusi!

Lau eta zortzi hamabi,
Abendoan da Eguerri...
Maitiarekin pasa nitzazke
Nombeit zombeit oren-erdi,
Orenak ere joan litazke,
Balimbalitz ongi-ethorri!

Ene maitiaren jendiak
Oro koleran jarriak:
Ezin jakin dezazketela
Ene gogoko berriak...
Eitekoak egin ditzatela,
Nik eginen tut neuriak.

Gaizki erakarri naute,
Batere ogenik gabe:
Ni muthil arin bat naizela,
Batere doterik gabe,
Ontsa eta aise nahia,
Lana're gutisko maite.