---
id: ab-3792
izenburua: O Jinko Handia, Zu Zira
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003792.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003792.MID
youtube: null
---

(Abrahamek Haranerik Chandorat (?) joaiteko)

O Jinok handia, zu zira
Ororen kreazalia,
Ezazu, othoi, nitara
zoure grazia eztia.
Zutarik dependitzen duzu
ororen kountentamentia,
Ezi ene indar tchipiek
deus eztizie balia.

(Loth bere illoba presountegitik libratu ondoan.)

Jinko handi lariousa,
ororen kreazalia,
Bethi zuk plazer duzuna
izan bedi kounplitia.

Mirakullu egiten duzu
plazer duzun aldi oroz,
Khantatu nahi dizugu,
Jauna, egun boztarioz.

Gitian algarreki
orai bertan erretira,
Eta joan Sodomako
erregiaren edireitera.

(Ainguriak abertitu zianian haurra ukhenen ziala.)

Ehun ourthetako gizonak
othian possible lizatia,
Adin hartan, seme baten
harek injendreatzia ? (1)
Eta 90 urthetan,
Sara ene emaztia,
Posible izanen deia
seme baten sortzia?

(Sodomak eta Gomorrak erre behar ziala, Jinkoak Abrahami erran
zeionian)

Jauna balimbada
hiri haietan 50 justo,
Othian hanko abitantak
galduren direia oro?
Ah! Jaun handia, zu
othian kechatu zirade,
Eta hiri eder haiek
arren finitu behar dizie?

O sodoma eta Gomorra,
herri kourrounpitiak,
Ziek ere ukhenen tuzie
jinkoaren azote kolpiak.

(Abrahamek, Izaaqui lephoan mouztou behar zeionian, libratu zialakoz,
Jinko hounari.)

Jauna, zuk ikhousi duzu
ene boronthatia,
Badakizu noula nian
gaiza hori prestatia.

Ene semiaren ikhoustez
mundu hountan bizirik,
Ez zeneitan emaiten ahal
phaku ederragorik.

Nihaur errezebi nezazu
zoureki bake handian,
Laid'ahal zitzadan bethi
eternitate guzian.

Adio, mundian, adio,
Korrupzionez bethia;
Ahatzerazi izadazut,
O Jinko kreazalia.

(Eror hilik.