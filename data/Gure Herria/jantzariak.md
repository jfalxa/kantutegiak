---
id: ab-3807
izenburua: Jantzariak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003807.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003807.MID
youtube: null
---

Igandia jin denian
Oinak arin bidian,
Soinian nun den jakinez geroz, harat juaiteko abian,
Soinian nun den jakinez geroz, harat juaiteko airian.

Astelehein goizian,
Ezin jeikiz oihian,
Sobera janzan arizanik, bezpera arratsaldian (bis).

Astearte goizian,
Nausia khechu etchian,
Ferretak hutsik hatzemanik, urik gabe sukaldian (bis).

Asteazken goizian,
Min bainien burian,
Artho jorran artzeko orde, jarri nintzen itzalian (bis).

Osteun goiz goizian,
Zoin goor jeikitzian!
Gogua ilhun nien segur, churiketak churitzian (bis).

Ortzirale goizian,
Etchetik jalitzian,
Nigarrak heldu zitzauzkitan igandiaz orhoitzian (bis).

Larumbate goizian,
Labiaren aintzinian,
Jatekuain untsa iteko min nien aztaparrian (bis).

Ba bainan igandian,
Oinak arin bidian,
Soinian nun den jakinez geroz, pena guziak airian (bis).