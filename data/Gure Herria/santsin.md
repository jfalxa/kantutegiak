---
id: ab-3810
izenburua: Santsin
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Igande ilhuntze batez
Etche aide zohala,
Erroz gora erori zen
Santsin zabal-zabala.
Izarrak nihon ez ditu
Ikhusi han bezala ;
Ilhargia ferekatuz
Gan zen gan bereh'ala.

Echker eskuin, andarkoka
Santsin abiatzen da,
Alabainan, mozkortua
Betbetan, ezin senda!
Ez nezakala, zioen,
Gau ilhuna izenda ;
Hi bahintut salatari,
Bahuke to zer konda!

Badaki ichilik dela
Egonen gau ilhuna,
Bainan, bainan, changoari
Maiz nausitzen chalduna!
Han dago, han Santsinen goait
Mari-Martin papuna!
Hura balakatzen baduk,
Santsin, hi, zer gizona.

Etchera'ta andreari
Erraten omen dio:
Eroriko hunek ez din
Samurtzea balio!
Erran no, Santsinen gaitza
Noiz gosta zain kario ?
Bethi badie, ostatutan,
Merke botikario ?

Hordi tzar, gizon higuina.
Urrun hakit bichtatik ;
Ochala ez bahintz jalgi
Putzuaren zolatik!
Zer sofriarazten dukan,
Jainko Jaunak bazakik!
Ai! nola bada gate hau
Sartu nikan lephotik!

Goiz aldera, ez errechki
Jeiki zen bada' Santsin,
Ziolarik lagunari:
Atch! hementche dinat min
Hunek aldiz: Sendatzeko
Hauche behar duk egin:
Enekin bezein ' maiz hadi
Samur botoilarekin.

Ichilik egonen hiza,
Emazteki ergela!
Ni bezen senhar gozorik
Hots badakin ez dela.
Hanka debru hunek zatan
Galdatzen ur ephela ;
Hik ere aise uzten dun
Ur hotzean igela.

Santsin gaizoaren ganat
Ethorri zen barbera,
Eta zion, han nonbeitik,
Elhorri bat athera.
Ez dakit egin zioten
Larderia sobera,
Bethi Santsin gelditu zen
Lehengo Santsin bera.

Santsin hunen iduriko
Zembat den bazterretan!
Zembat nigar, ezkatima,
Igande arratsetan!
Gazte danik dagoena
Berant ostatuetan,
Zahartzean izanen da
Bethi trapu tzarretan.