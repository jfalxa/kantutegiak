---
id: ab-3856
izenburua: Porru Salda
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003856.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003856.MID
youtube: null
---

Gazte nintzanian, dembora batian,
Maitia, ez nintzen kontsolatzen, ezkondu artian bis.
Tra la la la la la, tra la la la la... bis.

Ezkondu'ta gero, hilabete gabe...
Porru salda egiten ginuen, oilorik gabe! Bis.
Tra la la la la la, tra la la la la... bis.

Hamalau launetako familia batian,
Artorik ez ginuen, maitia, lau hilabetian! Bis.

Zohazi eta errozu, oi, gure amari,
Bi buru chardin erre detzala, buztana kendurik! Bis.

Goiko kalletik eta beko kaleraino,
Dontzelarik ez dago, maitia, gehiago, bat baño! Bis.

Haur bat sabelian, bertzia besoan,
Jauna tabernan, eta maitiak artoa hauzoan! Bis.

Ezkondu nintzenian, hiru soineko nituen,
Jauna tabernan, eta maitiak artoa hauzoan! Bis.

Iruñatik hor heldu da Betiri tamburin,
Soinua jotzen dauku, maitia, guzieri berdin. Bis.

San Zirrikitun, tamburin, gure omoria!
Utzi alde bat, ene maitia, Eskualdun jendia! Bis.