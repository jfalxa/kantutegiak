---
id: ab-3801
izenburua: Dugun Edan Eta
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003801.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003801.MID
youtube: null
---

Dugun edan eta gaitezen liberti!
Nork emanen derauku tatcharikan guri?
Guk duguna hauche da: maichko edan nahi!
Berandu ezkontzeko aski dugu hoai!
Matia Kattalin!

In nomine Patris eta et Filii!
Gora nintzelarikan behera erori!
Lephoko zainetatik hautsi tut hamabi,
Sahets-hezurretarik hogoi bat eta bi,
Maitia Kattalin!

Indan edatera, maitia Kattalin,
Egarriak ni hurran akhabatua nin;
Hi ikustiarekin, botoila horrekin,
Bihotza phizten zautan, edaitiarekin,
Maitia Kattalin.