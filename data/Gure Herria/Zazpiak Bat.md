---
id: ab-3772
izenburua: Zazpiak Bat
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003772.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003772.MID
youtube: null
---

ERREPIKA
Zazpi Eskual-herriek bat egin dezagun
Guziak bethi, bethi, gauden, gu, Eskualdun! (bietan)

Agur eta ohore Eskual-Herriari,
Lapurdi, Bachenabar, Zibero gainari,
Bizkai, Nabar, Gipuzko eta Alabari!
Zazpiak bat, besarka, loth beitetzelgarri!

Haritz eder bat bada gure mendietan,
Zazpi adarrez dena zabaltzen airetan,
Frantzian, Espainian, bi alderdietan:
Hemen hiru'ta han lau, bat da zazpietan.

Ekhalde Iberrian noiz othe sortua?
Lau mila urthe hunat aldatua;
Hain handi eta azkar lehen izatua,
Orai gure haritza zein den murriztua!

Gure haritz hau, azken mende hautarino,
Zaharturik ere, zen eder bezein sano;
Bere lur hoin garbian oso zagoeno,
Gainetik zoakona... behar zen arrano.

Hi, haiz, Eskualherria, haritz hori bera,
Arrotza nausiturik moztua sobera;
Oi! Gure arbasoak, ez othoi, ez beira
Zein goratik garen gu jautsiak behera.

Gureak ziren, lehen, bazter hauk guziak
Arbasoek utziak, hek irabaziak.
Guri esker, Frantziak eta Espainiak
Daduzkate dituzten Eremu handiak.

Mairu beltza zelarik Espainian nausi,
Nabasen, Eskualdunak egin zion jauzi,
Harkekharri gatheak han gintuen hautsi,
Eta hilez bertzeak igorri ihesi.

Orduan gure alde ohiuz zauden oro;
"Bere lurretan nausi Eskualduna bego.
"Frantziak, Espainiak, bai orai, bai gero
"Deus khendu gabe dute gerizatu gogo.

Gureez gure, lehen hain libro ginenak,
Ezin ahantziz gaude orduko zuzenak,
Zer ametsak ditugun, zer orhoitzapenak!
Jaungoikoak bakharrik badakizka denak.

Ez bahaitz, Eskualduna, lehen bezein handi,
Aphaldu gabe chutik bederen egoadi,
Odolez eta fedez bethi berdin garbi,
Bethi tink'atchikia hire Eskuarari.

XI.Zuri gaude othoitzez, Jaungoiko maitea,
Lagun zazu zerutik Eskualdun jendea,
Begira dezan bethi lehengo fedea,
Zor zaizkon zuzenekin ardiets bakea.