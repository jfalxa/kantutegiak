---
id: ab-3844
izenburua: Ai, Ai, Ai, Mutillak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003844.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003844.MID
youtube: null
---

Azpeitiko neskatchak, gona gorriekin, (bis)
Ez dute dantzatu nai chapel churiekin,
Ai! Ai! Ai! Mutillak, chapel churiekin! (bis)

Biba chapel gorriak, burlia (1) ferdiak! (bis)
Zaldi barian dathor Don Carlos gure,
Don Carlos maitea, gure erregea! (bis)

Biba don Carlos eta Doña Margarita, (bis)
Biba relijionea.ta! Fuera Republika!
Fuera Republika! Biba Margarita! (bis)

Gobernuak baditu bi pezetakoak,
Bai eta Don Carlosek bolontarioak,
Bolontarioak, ez jornaleroak.

Nere borondateaz hartua det arma...
Nigarretan utzirik aita eta ama...
Aita eta ama, hartua det arma!...

Gu lapurrak gerala Kalian diote;
Infame traidoreak, gezurra diote,
Gezurra diote.ta pagaturen dute...

Ez gerade ohoinak, ez pezeteroak,
Fedearen aldeko bolontarioak,
Bolontarioak, ez pezeteroak.

Ez gerade armetan, ohointzeendako,
Bainan bai bakarrikan Don Carlosendako,
Haren koroazteko, gortean zartzeko!

Don Carlosek eman du Frantzitik ordena
Champonian (2) saltzeko beltzetan onena,
Beltzetan onena, Frantzitik ordena.

Champonian ezikan, arditian ere,
Estellako kampotan dozenaka daude!
Dozenaka daude arditian ere.