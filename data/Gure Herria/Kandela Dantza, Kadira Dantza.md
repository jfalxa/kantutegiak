---
id: ab-3886
izenburua: Kandela Dantza, Kadira Dantza
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Tan, tan, tan, tan,
Rapetaplan!
Artzain zaharrak tafernan:
Hordi gira
Ez, ez gira! (bis)
Basoak, detzagun bira! (bis)
Johoho, johoho, johoho! (bis)
Basoak detzagun bira! (bis)

Nork jotzen derauku bortan?
Beharbada
Otsoa hor da (bis)
Nihor ez gaiten athera! (bis)
Johoho, johoho, johoho! (bis)
Nihor ez gaiten athera! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Uria hari karrikan:
Gauden hemen
Arno hunen (bis)
Gostu onean edaten! (bis)
Johoho, johoho, johoho! (bis)
Gostu onean edaten! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Babazuza tarrapatan:
Dugun edan
Hamarretan! (bis)
Aberats gira gau huntan! (bis)
Johoho, johoho, johoho! (bis)
Aberats gira gau huntan! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Ez dut minik sabelean!
Nahi nuke
Ehun urthe (bis)
Hola egon banindaite! (bis)
Johoho, johoho, johoho! (bis)
Hola egon banindaite! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Arnorik ez da botoilan;
Ostalera,
Ez ikhara; (bis)
Arnoko bethi sos bada! (bis)
Johoho, johoho, johoho! (bis)
Arnoko bethi sos bada! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Zer othe dut bagietan?
Non da borta?
Airatu da... (bis)
Mahaina dantzan dabila! (bis)
Johoho, johoho, johoho! (bis)
Mahaina dantzan dabila! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Zangoek amor bidean!
Haketan min!
Gaizo Martin, (bis)
Urhatsik ez dirok egin! (bis)
Johoho, johoho, johoho! (bis)
Urhatsik ez dirok egin! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Eri tchar naiz, hitzekotan!
Sendo nintzan,
Aski edan (bis)
Izan banu gau hunetan! (bis)
Johoho, johoho, johoho! (bis)
Izan banu gau hunetan! (bis.