---
id: ab-3834
izenburua: Ardo Zarraren Kantak
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Ai zein edari-gozua!
Zerutik jatxitakua!
Zembat eta geyago,
Anbat eta nayago!
Zer egingo ote dit barrenen!
Obe det sartu lenbailen.

Baso onetan ikustian,
Poz artzen det biotzian.
Baldin or ikusirik
Banago txoraturik,
Zer egingo ez det barrenen?
Sar-akit bada lenbailen.

Ai neri zer egin ote zat!
Oñez ibiltzen aztu zat!
Burua jaso ezin det,
Lurra igaro ezin det;
Ilun det biotza guziz;
Edan zagun bada berriz!

Galdu dirak neri burua,
Ardo zitalki gaiztua!
Edan det txit gogotik
Ardo sendo onetik;
Ai jakin banu lenago!
Edango nuan geyago...

Edazu kontuz ardua,
Naiz zarra ta naiz gozua;
Izanik ain gauz-ona,
Daki galtzen gizona;
Bañan edanikan neurriz,
Piztutzen du illa berriz.