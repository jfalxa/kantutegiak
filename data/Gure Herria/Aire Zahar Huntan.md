---
id: ab-3871
izenburua: Aire Zahar Huntan
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003871.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003871.MID
youtube: null
---

Version Labourdine
I
Aire zahar huntan, bi bertsu berririk
Alegrantziarekin kantatu nahi tut nik,
Bihotza libraturik pena orotarik,
Desir nuien maitia baitut enetu nik.

II
" Ez nukeien uste, ez zuien iduri,
Eneturen muiela maitia segurki ;
Mintzatzen nintzaioen ahalaz ederki...
Ene errana gatik, hura ezin gomberti !.

III
" Uhaina arrantzari arrain onik deno...
Gizon gaztiak tendre, amurus direino;
Mintzatzen badakite ezin gehiago...
Plazerak hartu eta ... enetzat adio..

IV
" Pikantki mintzo zira gizonez, maitia ;
Ezagut bazineza duzun estatuia ?...
Ez balimbanu maite fidelitatea,
Elitzaiket kombeni hola mintzatzia.

V
Zazpi urthe badizu, ene kariua,
Zutan eman zuiela ene amodioa;
Geroztik nabilazu, trichterik gogua,
Beldurrez ez zaitzadan zu izan, gaichoa. ".