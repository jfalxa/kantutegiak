---
id: ab-3892
izenburua: Primaderako Floria
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

I
" Primaderako florian,
Hoitabigarren urthian ! BIS
Neskatcha gazte inozent naiz pena changrinen erdian,
Ene bizia uzterat noa, adinik ederrenian !

II
" Badu jadanik lau urthe
Badudala norbeit maite; BIS
Burhasoer deklaratua, orai duela bi urthe...
Heiek ere, achola gabe, saldu izan nahi naute !

III
" Aitak erran zautan nerik
(Zoin bihotz-erdiragarri !) BIS
" Neur'aberastasunarentzat dela nik maite bakarra;
Esposarazi nahi naute Markis baten semeari !...

IV
" Othoiztu ditut aitàma...
( Alferretan ein dut dena !) ; BIS
" Neur'aberastasunarentzat dela nik maite bakarra ;
Sekulan ez duala hartukomaite ez dudan laguna !.

V
" Geroztik nago penetan,
Etzana oge batetan ! BIS
Midikuak ekartzen dauztet, ... bainan, oro alferretan,
Ustez ni sendaturen nauten barber eder horietan !!!

VI
" Ni sendatzeko barbera
Da neure maite bakarra... BIS
Othoiztu ditut ait'amak utz dezaten ikustera...
Atsegin hoi ez ein nahizik, uzten naute finitzera !!!".