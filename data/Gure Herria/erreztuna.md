---
id: ab-3778
izenburua: Erreztuna
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003778.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003778.MID
youtube: null
---

« Partitu nintzan herritik, zuri promesa emanik, (berriz),
Enetzat fidel egon zinten ongi gomendaturik, ...
Ez duzu egin kasurik ! »

«Fidelik ez egoiteko ez dut ez nik arrazoinik ;
Ene bihotza zuk daukazu arras enganaturik,
Hortaz ez izan dudarik ! »

«Partitzean eman nauzun seinaleño bat nik zuri ;
Seinaleño hura nik orai nahi nuke ikusi,
Ez bauzu eman nehori ».

« Zuk eman seinale hura ez dut eman ez nehori,
Begiratuko ere nuen, ez balitzeraut hautsi,
Bainan egin zaut bi zathi ! »

Nik eman erreztun hura ez zuzun bada kobria,
Ez zen eta ere zilarra, bainan zuzun urria,
Diamantezko begia !

Amodiorik bazinu zuk, batere, enetako,
Pochiak bilduren zintuen niri erakusteko,
Satisfos ni egiteko !...