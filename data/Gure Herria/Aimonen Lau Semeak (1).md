---
id: ab-3881
izenburua: Aimonen Lau Semeak (1)
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003881.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003881.MID
youtube: null
---

Heben girade Aimonen lau seme,
Heben balin bagunu Kusi Maugis (2) ere,
Ezkinate lotsa, Charlemaña, zure,
Frantses dosepariak (3) oro jinik ere,
Oro jinik ere.

Maugis zenian triaten gainean,
Burdianz kargaturik, etsaien artian,
Hargatik ez zen lotsa haien erdian,
Zeren belhar hunak berekin baitzutian,
Berekin baitzutian.

Maugisek badu protekzione,
Adreziaz parerik mundian batere:
Aita zumbait debru, Ama belhagile(4)
Eta hura aldiz haien bien seme,
Haien bien seme.

Bedatseko (5) berdurek lili ekarten,
Bai eta gur'egunek gaiak bellarazten;
Zuhain hunek (6) zer frutu deiku ekarriren?
Zohitu denian orok ikusiren,
Orok ikusiren.