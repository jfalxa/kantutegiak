---
id: ab-3848
izenburua: Ziburutik Sararat
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003848.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003848.MID
youtube: null
---

Ziburutik Sararat izatu naiz joana,
Neure lehenagoko maite baten gana;
Han arribatu gabe, aitu nuen fama,
Guarda gazte batekin zela hitzemana.

Maitia, bizi zaite gazte.ta lorios,
Bai eta fidel izan, hitzemanez geroz.
Ondoko urrikiak ez du balio deus;
Huntan kitatzen zaitut, maitia, adios.

Uso churi pullita, fin eta fidela,
Deus makhurrikan gabe joan zaizkit gibela;
Nik ere erran dezaket nombeit badirela...
Nik ez bainuen pentsatzen zu holakoa zinela.