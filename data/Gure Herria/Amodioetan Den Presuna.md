---
id: ab-3870
izenburua: Amodioetan Den Presuna
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003870.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003870.MID
youtube: null
---

I
Amodioetan den presunak badu hainitz sofrikari;
Ez naiz ez mintzo ni jakin gabe, bainan frogaturik ongi.
Gaichoa hemen nago, trompaturik,
Lili bat soberache maitaturik !
Ez dirot kita bihotzetik, ez dut mement baten onik !
Leihor itsasoak pasatu'ta, bethi maitia gogoan nik !

Errepika.
Goazen, goazen, gazte lagunak, zion gure maiteñoen ganat,
Gure pena eta doloren heieri deklaratzerat,
Heieri deklaratzerat,
Heieri deklaratzerat !

II
Oi ene maitia, zer diozu, urrundanik naiz mintzatzen,
Esperantzarekin bizi nuzu, nauzulakoan komprenitzen;
Zur' orhoitzapenak nau hiratzen,
Deusek ere ez nau kontsolatzen !
Choliki, gauaz, ametsetarik, fidel zaizkit iduritzen...
Eta, gero, ni iratzartzean, ene doloreak doblatzen !
Errepika
Goazen, goazen, gazte lagunak, etc...