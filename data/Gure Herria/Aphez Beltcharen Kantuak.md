---
id: ab-3849
izenburua: Aphez Beltcharen Kantuak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003849.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003849.MID
youtube: null
---

Igande-mezak erraitian, Harrichuriko gainian,
Eskopeta kargatu nuen - neuretzat zorigaitzian -
Bai eta ere deskargatu Aphez beltcharen gainian.

Haltzak eztu'maiten ezkurrik, ez gaztamberak hezurrik;
Ez nian nik uste bazela Jainko-semetan gezurrik,
Ez nian nik uste bazela Jainko-semetan gezurrik.

Aphez beltcharen arreba, serora begi-ñabarra,
Ni galaraziren nun, bainan, ez zain phizturen anaia,
Ni galaraziren nun, bainan, ez zain phizturen anaia.

Ehun behi nik baditut, bai beren zezenarekin;
Nik guziak emanen ditut, bizia ukhaitiarekin,
Nik guziak emanen ditut, bizia ukhaitiarekin.

Ene seme Erramuntto, Erramun goizian sortia,
Damurik aditu beahr duk: Ait'urkatiaren semia!
Ait'urkaitiaren semia bainan, ez ait'ohoinaren semia!

Aita, zoazi bai etcherat, ene arropak harturik,
Ene haurrer emazkotzute, beren neurrian josirik,
Ene haurrer emazkotzute, beren neurrian josirik.

- Ez, ze, ez nuk ez joanen, hire arropak harturik;
Nik, Baionan utziren diat arropa baino hoberik,
Nik, Baionan utziren diat arropa baino hoberik!

Borthegarai Ortzaizeko, Borthairu Ezpeletako,
Hi Bordelen, ni hor Baionan, ah! Zer gizonak galduko!
Hi Bordelen, ni hor Baionan, ah! Zer gizonak galduko!

Antzarak doazi karrankaz, Donibaneko karrikan,
Borthegarai hori badoa, semiaren minez marraskan,
Borthegarai hori badoa, semiaren minez marraskan.

Urthiak zombat egun ditu? Hemezortzitan hogoi tu...
Borthegarai gazteño horrek persuak hemen eman ditu,
Borthegarai gazteño horrek persuak hemen eman ditu.