---
id: ab-3787
izenburua: Argizari Ederra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003787.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003787.MID
youtube: null
---

Argizagi ederra, argi egidazu,
Bide luze batean joan beharra nuzu!
Gau huntan nahi nuke maitia mintzatu
Haren ganatu arte, argi egidazu!

Nekatua heldu naiz bide luze huntan,
Urhats pausu guzian bethi pentsaketan;
Athorra bustia dut bulhar errainetan:
Idortu nahi nuke su ondoño hortan!

Atorra busti hori garreia ezazu!
Nik nola manaturik bustia ez duzu!...
Bidean eman gabe, kontu zuk egizu,
Bidearen buruan nahi zaituzten zu?

Ez deia bada pena eta dolorea,
Nihauren maiteñoa hola suntsitzea!
Amets bat egin nuen zuk ni maitatzea...
Zombat deithoragarri hol'enganatzea!.