---
id: ab-3840
izenburua: Sarako Ihestiarrak
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003840.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003840.MID
youtube: null
---

Norat zoaste hor gaindi, ardi nahasiak?
Espainiara goazi, doi-doia biziak.
Jauna, ahantz bekitzu guk egin gaizkiak,
Gosez hil ez gaitezen, mendian, guziak.

Emazteak ikharan senharra bilhatzen;
Gizonak emaztea orori galdetzen!
Zer egin zare, ama? non da ene aita?
Horra urragarri den haur gaichoen pleinta.

Etchelarren heldu'ta, oi, gure lastima !
Lurrean sartu zuken nork bere arima ;
Nihork ez du leihorrik, ez norat anima ;
Zembat baigira bizi, haimbertze bitima !

Koko beltzak ez gaitu segur Urrikari,
Chizpaz eskaintzen dio Frantses gizonari ;
Eskualdunak bakarrik demagu janari ;
On eginaz, jauna, zuk emozu sari !

Gure galgarri haizen Martin Dulunia,
Bizirik jan darokuk bulhar-haragia ;
Athor eta ikusak gure hagonia,
Hire baithan Jaunak sar dezan argia.

Zeruetako Jauna, barkha ! barkha guri !
Eta gun bezala egizu orori ;
Barkhatu diozute zure etsaieri,
Halaber nahi dugu guk egin gureri.

Bainan bihur gaitzatzu gure Elizara,
Ikus dezagun orok, ikus gure Sara !
Begiz ikus detzagun, batzuk haurrideak,
Zembaitek burnasoak' ta orok etcheak !

Ez bazaitzu komeni mirakulu hori,
Indar emozu othoi gure bihotzari,
Zure nahi saindura dadien sumeti,
Zure manamenduak iduk detzan bethi.