---
id: ab-3804
izenburua: Itsua Eta Sastrea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003804.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003804.MID
youtube: null
---

Eskualdun bat zen itsu eta besulari;
Athez athe zabilan, iloba gidari;
Soineko eta diru, janhari, edari,
Nasaiki biltzen zuten, bersu eman sari.

Zuhur gisa baitzuten sos guti chahutzen,
Piko baten azpian eltze bat zaukaten;
Hiur ehun libera harat bildu zuten...
Bainan gaichtagin batek ebatsi zioten.

Bere gordelekutik galdurik eltzea,
Bat zagon hatsperenka, nigarrez bertzea.
Olobak esan zion: .Osaba maitea,
Horra zer den kampoan gauzen gordatzea!.

- .Azkeneko aldian hemen ginelarik,
Ikusi gaituztela ez diat dudarik..
- .Bai, sastrea heldu zen, pegarra betherik,
Behatu zuen guri, bainan urrunetik..

- .Iloba gida nezak sastrearen gana,
Adi dezan bersu bat itsuak emana.
Gure heltzeak badu harek eremana,
Bazakiat nik hura zer gisaz engana..

Ilobak esan zion aphal beharrira:
.Sastrearen etcherat orai heltzen gira,
Irri faltsuan dago leihotik begira,
Ez erran hitz gaichtorik, zuhurra bazira..

- .Iloba gida nezak arthoski bidean,
Zerbait daiat utziko, azken egunean:
Ehun luis baitiat piko baten pean,
Bertze ehun emanen, bihar, han berean..

Sastreak bere baitan: .Itsuak etzakik
Hartuak ditudala horren sosak, handik;
Eltze hau, diruarekin, berriz han emanik,
Bertze hiru ehunak bilduko tiat nik..

Sastrearen agintza balios izan zen:
Biharamun goizeko, eltzea han zuten!
Ilobak erran zion: .Hemen dugu, hemen
Oi zoin ongi sastrea enganatua den!.

- Ene moltsan behar diat dirua ezarri.
Lurpean, hola nola, utzak heltze hori,
Eta emok barnean bortz edo sei harri,
Sastreak aski pisu altcha dezan sarri.

Itsuak sastreari: Errak, to, egia,
Atzo bezala dukan arrai arpegia,
Ala trichterik dagon, doluan jarria,
Dirua erain eta... bildurik harria?

- Itsu harpagon, zikoitz, laido-emailea,
Habil, eta ez kolpa ene ohorea!
Dirudun bahaiz ere, eta ni gabea,
Baduk aski zorigaitz hire ilhumbea!

- Ni itsu, ilhumbean, hi, sastre erromes...
Dezagun lagun elgar biek ditugunez:
Nik egiten haut jabe ene ontasunez,
Okhertu nahi banauk hire begi batez...