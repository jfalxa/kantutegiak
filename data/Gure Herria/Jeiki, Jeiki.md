---
id: ab-3852
izenburua: Jeiki, Jeiki
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003852.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003852.MID
youtube: null
---

I
Jeiki, jeiki etchekoak, argia da zabala, argia da zabala !
Itsasotik mintzatzen da zilharrezko trompeta;
Bai eta re ikharatzen Olandresen ibarra, Olandresen ibarra.