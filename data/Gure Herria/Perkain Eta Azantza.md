---
id: ab-3780
izenburua: Perkain Eta Azantza
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003780.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003780.MID
youtube: null
---

«Norat zoazi, zu, adichkidea ? »
«Donapaleora dut egungo segida ;
« Urreño bat deiramat bertze baten bilha,
«Baldin Laphurrtar hoiek jalgitzen badira

Laphurtarrak jin ziren, trebesak doblezka : (1)
Joko askiren orde bazuten nobleza ;
Galdu duen gaichoak buruan har beza
Ez dela, egun oroz, Laphurtarren besta !

Azantzako semea nik ez dut mendratzen,
Bere parerik ez du pilota botatzen ;
Bai, bainan Perkain hori ez zuen latsatzen
Plaza guz iarentzat bera aski baitzen ! (2)

Hek zazpi joko eta gurek bederatzi ;
Halere Laphur'tarren plaza ez zen aski ;
Hasarrean bezala trebesian bethi:
Urguiluz nahi zuten guri irabazi!

Orai bethetzen ditui bederatzi urthe,
Baigorrin trufa hunik egin zinaukuten;
Gure gasna-sariak joan zinauzkuten...
Ordainak baitiaguk, hor kompon zaitezten!

Orai, Laphurtar jaunak, zuer zer zaitzu
Monedako etchea hurbilehko duzue ;
Eskasten bazautzue, hartarat zoazte
Ukhaiten baduzue, ... harturen duzue!

Ehunbat urhe galdu, gastuak bertzalde
Hok ere, ene ustez, zerbait baidirade!
Gizon bat aberatsa izanikan ere,
Holako zimikoak senditzen dirade!

Amikuzeko duzu bai Donapaleo ;
Hango ez bada ere, hurbildik darraio.
Aldudiarrak eta hainitz gehiago
Handik hasi zeraizku dihuru karreio.

Azantzako jaun Pedro, artecha zirade:
Mainuak hartu tutzu... ba bainan debalde
Akizko uretarat zoaz berritz ere...
Jiten zireneko, prest egonen girade.

Ur epelez mainuak, saldako oilaki!...
Gorputzari bereak emaiten badaki;
Antzara ichter zembait jan beharko ditu
Dirurik ekhartzekotz Donapaleotik!(3.