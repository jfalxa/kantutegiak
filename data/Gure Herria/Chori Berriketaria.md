---
id: ab-3808
izenburua: Chori Berriketaria
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003808.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003808.MID
youtube: null
---

Sor lekhua utzi nuen, ondikotz hala beharrez !
Jainko ona urrikalduz bethiko nere nigarrez,
Primaderan hasi orduko arbolak estaltzen lorez,
Choritto bat heldu da bethi nere herritik hegalez.

Nere gana hain urrundik, ethortzean unhatua,
Arbolarik hurbilena du bere pausa-lekhua;
Aldachka goren-gorenean horra non den lokhartua
Luma pean zango bat, eta hegal pean du burua.

Pausa hadi ; lo egizak, chori maitea, bakean,
Atzarria ni, hire zain, hemen nauk hire aldean,
Ur chortarekin huna hemen papurrak leiho gainean.
Bainan gero hango berriez orhoit hadi atzartzean...

Orhoit hadi ba, choria, herri maiteko berriez ;
Nere aita, nere ama nigarretan nik utziez.
Mintza hakit ahaidez eta mintza lagun on guziez,
Mintza nihor ahantzi gabe, maite ninduten hekiez !

Atzar hadi, atzar bada, berri-ketari abila :
Nere beldur izan gabe, jauts hadi hurbil-hurbila
Gure leihorat izan haizen erredak ichil-chila ;
Izan ere hunatekoan, solas ordainaren bilha !

Choria, lo hagolarik, ikharan nagok aldean.
Ez ahal duk zorigaitza maite nautenen artean,
Hala balitz, othoi, choria, berriz herrira heltzean,
Loretto bat, nigar batekin, pausa zak tomba gainean

Chorittoa gan denean, ostoen eror demboran,
Berriz ethor ez dadien nago beldurrez ikharan ;
Ihiztaria, hartzen baduk ene choria segadan,
Utzak, othoi, gaichoa libro, berriak ekhar detzadan .