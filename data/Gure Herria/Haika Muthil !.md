---
id: ab-3867
izenburua: Haika Muthil !
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003867.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003867.MID
youtube: null
---

I
"Haika, muthil,
Jeiki hadi,
Argia den,
Mira hadi ! "
- "Bai, nausia,
Argia da,
Gur'oilarra
Kampoan da".
- "Haika, muthil, etc...

II
Haika, muthil,
Jeiki hadi,
Uria den
Mira hadi !
- Bai, nausia,
uria da,
Gure orra
Bustia da.
- Haika, muthil, etc...

III
Haika, muthil,
Jeiki hadi,
Surik baden
Mira hadi!
- Bai, nausia,
Sua bada,
Gure gatua
Beroa da.
- Haika, muthil, etc...

IV
Haika, muthil,
Jeiki hadi,
Hortchet zer den
Mira hadi !
- Bai, nausia,
Haizea da,
Gure leihoa
Ideki da.
- Haika, muthil, etc...

V
Haika, muthil,
Jeiki hadi,
Kampoan zer den
Mira hadi !
- Bai, nausia,
Elhurra da,
Lurra churiz
Estali da.
- Haika, muthil, etc...

VI
Haika, muthil,
Jeiki hadi,
urrean zer den
Mira hadi !
- Bai, nausia,
Ardia da,
Aspaldian
Itho da.
- Haika, muthil ,etc...

VII
Haika, muthil,
Jeki hadi,
Zer oinon den
Mira hadi !
- Bai, nausia,
Egia da,
Mithiltto hau
Unhatu da.
- Haika, muthil,
Jeiki hadi,
Zer onon den
Mira hadi .