---
id: ab-3812
izenburua: Lorea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003812.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003812.MID
youtube: null
---

Alferretan haiz sortzen, oi lore maitea,
Baratze bazterretan, humilki gordea.
Bafada gochoenak hau bethi salatzen;
Hir.egoitza bakotchak errechki Kausitzen.

Oi erradak zertako haizen hoin herabe?
Eder duk modestia, bainan gaindi gabe.
Berthutean orobat, bertzetan bezala,
Jakizak, ez badakik, negurri badela.

Bertze lore guziak, sorthu bezain sarri,
Arrai, dantzan bizi tuk, zefir soinulari:
Hi aldiz belharpean, burua behera,
Alhargun bat iduri, lurrari begira.

Hire lagunek haute beren gana deitzen,
Zeren hauten guziek bihotzez maitatzen.
Altcha beraz lurretik, ez ukh.ethorkia!
Arrosaren aldean duk hire tokia.

Docteur LARRALDE concours de 1862.
Harmonisation du:R.P.J.A. DE DONOSTIA
NÉHOR ET DUFA.