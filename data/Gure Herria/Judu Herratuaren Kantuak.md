---
id: ab-3883
izenburua: Judu Herratuaren Kantuak
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Jesus heldu zelarik Kalbariora,
Kurutzeaz bizkarra ongi kargatua,
Galdegin izan zautan, humiliatua,
Eian utziren nuen borthan pausatua?

Nik izatu nuen hain bihotz krudela,
Ezetz erran bainion, ezetz berehala:
Aparta hadi hortik, gaichto kriminela,
Hi borthan pausatzia afruntu dudala!

Ez da munduan gizonik, ez alimalerik,
Judu herratua bezain malurus dagonik;
Ez nuke uste deus baden ere gai denik
Haren zorthe tristian kompara daitenik!

Pasatzen nintzelarik hiri batetarik,
Bi burjes zitzaizkidan jarraiki ondotik,
Ziotela etzutela ikusi gizonik,
Oi ni bezein luzeko bizarra duenik !

Sar zaite ostatura gurekin, gizona,
Izanen duzu edatera arno gorri ona ;
Zurekin pasa ginitzazke gau eta eguna,
Bai eta ere kompli desira duzuna

Sar ere banaiteke ostatura zuekin,
Edan ere badezaket zuen arno onetik ;
Jarriko ez naiz, bainan egonen naiz chutik...
Tormentek hartzen naute, oi, jarriz geroztik !

Erranen duzu gizona, kurios girela,
Zure adinaren berri jakin nahi' dugula,
Ehun urthe baditutzula iduritzen zaikula...
Edo zure bizarra trompakor da bertzela... ?.

Ene hezurrak dire arras gogortuak,
Eta demborak orainon ez akhabatuak :
Hemezortzi ehun urthe baitut pasatuak,
Hortaz izan zaitezte, oi, seguratuak.

Izatu naiz herrietan, bai hiri orotan,
Gertha ahal daitezken gerla guzietan ;
Etsempluak aise'tut hartu nik heietan,
Herioak ez duela indarrik ni baithan:

Mundua pasatu dut bortzgarren aldian ;
Hiltzen ere badire zoin beren aldian ;
Eta ni ezin hilez ibilki munduan !...
Azken jujamenduan... zeruko lorian.