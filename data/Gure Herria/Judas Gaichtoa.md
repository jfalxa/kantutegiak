---
id: ab-3835
izenburua: Judas Gaichtoa
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003835.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003835.MID
youtube: null
---

Apostolua, gau huntan,
Norat hoa, gau erditan?
Jesus Jauna, bakar bakarrik, arbola batzuen pean,
Bere Aitari othoitzean zagol, odol izerditan!

Nor dut ikusten hirekin?...
Apostolu bat hoiekin!!!...
Gibelerat, dohakabea! Bertze urratsik ez egin!...
Suge-zaharrak hor eremanen hau, hartu'ta berekin!

Oi Judas, zalu behazak!
Aldean nor den ikus-zak!
Makilekin, hor, ilhumbean, ongi dazkikan etsaiak...
Bertze aldetik, Jesus amultsua, zabalik besoak!

JESUSEK
Adichkidea, zertara
Ethorria haiz, hunara?
Hire Jesus, Jesus maitea, diru tzar hortan saltzera?
Hire Jesusi, ezpain zikin batzuz, musu emaitera?

Judas gaichtoa, zer ein duk?
Hire nausia saldu duk!!!
Bihur hadi beraren gana, baldin salbatu nai baduk!
Bestenaz, piko baten adarretik urkatu behar duk!

Dirua itzul?... Balio dik!
Jaunak ez diru beharrik!
Judas, zaren ez dukan izan... behar huen urrikirik,
Apostolu bat ikusten diaguk dilindan sokatik!!.