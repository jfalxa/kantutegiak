---
id: ab-3831
izenburua: Oi! Zer Egin Othe Zaut Niri?
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003831.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003831.MID
youtube: null
---

Oi! Zer egin othe zaut niri? (1)
Bidean ibiltzen ahantzi!...
Ez dut pausurik egiten,
Non ez naizen erortzen!...
Flako dut arras bihotza, (2)
Indazue arno hotza. (3)

Ene arreba Joana!...
- Badantzut, anaia jauna?-
- Ni hiltzen naizenenan,
Ez egin nigarrikan,
Ez ekhar dolurikan! (4)
Emazu flaskua burdian, (5)
Edateko pidaia hartan...

Ni hiltzen naizenenean, (6)
Een ehortz egunean,
Ene adichkideak,
Bil zaitezte guziak,
Eta mintza ahapetik,
Eian baden zeruan arn'onik?

Libera me kantatzean,
Ez busti isopa urean!
Arnoan busti zazu,
Han ongi trempa zazu;
Segur naiz nahiko dudala
Hilean, bizian bezala...