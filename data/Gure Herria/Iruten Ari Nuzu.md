---
id: ab-3857
izenburua: Iruten Ari Nuzu
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003857.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003857.MID
youtube: null
---

Iruten ari nuzu, khilua gerrian,
Ardura dudalarik (bis) nigarra begian (bis).

Nigar egiten duzu, oi, suspirarekin...
Kontsolaturen zira (bis), bai, demborarekin. (bis)

Ofiziale seme, kolore gorria.
Baigorriko neskatchen (bis) karesatzailea. (bis).

Nik ez dut karesarik, bertzek ez dutenik;
Berak dabiltza ondotik (bis), zer eginen dut nik? (bis).

Jendek erraiten dute: ezkondu ezkondu!...
Niri ez zait oraino (bis) gogorik berotu (bis).

Beltcharana naizela zuk omen diozu...
Ez naiz churi-gorria (bis), egia diozu (bis).

Churiak eta beltchak... Mendian ardiak...
Ez dituzu zuk ere (bis) mentaka guziak (bis).

Churiak churi dire, ni naiz beltcharana...
Ongi kontentik dago (bis) ni behar nauena! (bis).

Ezkon-mina dutala, zuk omen diozu...
Nik ez dut ezkon-minik (bis), gezurra diozu (bis).

Ezkon-minak dutenak seinale dirade:
Mathel hezurrak seko (bis) koloriak berde (bis).

Andrea, janezazu sagar gezamina!
- Zertako jan behar dut, jan dezakedalarik ona eta fina?

- Jendek erraiten dute hal' eztena franko,
Ene maite pollita (bis) zur' et' enetako (bis).

Ezkontzen balimbazira mariñelarekin.
Chardina janen duzu (bis) makallauarekin (bis).

Ezkontzen balimbazira mandozainarekin,
Arraina janen duzu (bis) oliuarekin (bis).

Anaia, nahi duzuia emazterik erosi?...
Baratze kantoinetan (bis), sosian hemezortzi! (bis).

Arreba, nahi duzuia gizonik erosi?
Eliza bazterretan (bis), bi sosetan zortzi! (bis.