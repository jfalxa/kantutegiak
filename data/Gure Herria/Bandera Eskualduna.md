---
id: ab-3773
izenburua: Bandera Eskualduna
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003773.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003773.MID
youtube: null
---

Gernikako haitza gure bandera da,
Zeruetaraino heltzen du kaskoa;
Lurraren zolara barnatzen du erroa;
Odolik garbienaz lothua da hura!

Ehun ortzantz kolpe ukhan'tu zerutik,
Ipharra jautsi da zafraka bortutik:
Haitza kendu nahi, bat batean, errotik...
Bai bainan hura bethi han egoiten chutik!

Zombat etsai dira lau hatzez erori!
Zer lelo handia dugun irabazi!
Gernikako haitza guzien aintzindari!
Indar emaiten zuen harek bihotzeri!

Ez, Gernikan dagon haitzaren (ondotik) gainetik,
Mundu huntan ez da arbola bat baizik...
Kalbariokoa, Jesusek landaturik.
Hoiek biak ez daude oinon eroririk!

Batek du erroan Jaunaren odola,
Odol hura dago lokarri bat ona!
Bertzeak baluke gure kar hoberena...
Ta behar balitz ere...odol garbiena!

Arbola saindu hark gintuen beiratzen,
Jenderi zuzena bai ere emaiten.
Gu ere orduan, Eskualdunak, baginen!...
Berriz denboratto hek ez direa jinen?

Bizi da oraino Eskualdun arraza;
Loak badu noizbeit bere atzartzea.
Chorrochteko ere gur'ezpaten burdina,
Badukegu herrian Arrolan-harria!

Ez, gure arbola, ez haiz ihartuko;
Adar gorak dituk oinon hostatuko;
Mendien gainetik berriz haiz hedatuko,
Ezkualdunen Eskualdun bethi beiratzeko.