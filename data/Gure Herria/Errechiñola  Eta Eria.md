---
id: ab-3828
izenburua: Errechiñola  Eta Eria
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003828.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003828.MID
youtube: null
---

Kantarien Errege
Hire kantatzeko
Gai denik nor daiteke
Hitaz beraz kampo
Geroztik itsumen du
zer bait othe dut nik
Ez esker onak ez dik
Neuritzen indarri.