---
id: ab-3832
izenburua: Begientzat Bakarrik
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003832.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003832.MID
youtube: null
---

Begientzat bakarrik badire loreak :
Dire usainik gabe, fruiturik gabeak ;
Ordean, zein dituzten eder koloreak !
Koloretik juiatuz, bertzen erregeak.

Haukien iduriko, ditugu ikusten
Neskatcha gazte hainitz orai distiratzen ;
Hauk beren itchuratik duztenak juratzen
Ez dirade, chimpleak, guti enganatzen !

Landetan sorturikan nahiz diren asko,
Ez dire hek eginak... lanean hartzeko :
Bulharrak tuzte flako, larrua churichko ;
Ez dute kuraierik kampo-lanetako.

Barnean daude beraz, lanetik ihesi ;
Burhasoek beharko heien partez hari ;
Aberats gutizian, aise nahi bizi ;
Z.er damua ez duten errentarik aski !

Balakite bederen barneko lanetan,
Jatera preparatzen bere tenoretan,
Zembeit pondu egiten philda zaharretan,
Ez litazke ez-deusak gure begietan.

Ehailetan ez dute sinheste phorrorik ;
Ez dute hunkituko hartakotz kilonk.
Kutcha zahar guziak daudezela hutsik . . .
Eginen da oraino, athorra hertsirik !

Errentak ttipi, bainan handi gutiziak,
Aberatsen lerroan agertu nahiak,
Beztimenda berriez nihoiz ez asiak;
Aitaren izerdien zer chukagarriak!

Ikusatzue gure printzesa berriak,
Urhatsak chehe eta ezpainak hertsiak
Oso-osoak gaten, iduri pampinak;
Pobre diren errazu ala aberatsak?

Badoazi bestarat begia arinik,
Arinogo bihotza, gorphutza chuchenik
Orai ez dute senti sabeleko minik,
Bulharrentzat batere ez dute beldurrik

Iduritzen zaiote, plazan direnean,
Denen begi samurrak duztela gainean
Destenorean hantche daudezi lorian,
Gau ilhunak ez ditu lotsatzen bidean.

Hauki lotzera hoan muthiko gaztea,
Egik kontu : ez ditek baizen kolorea
On baten bilhatzeko harrezak ephea
Baldin gogoan baduk hura izatea!

Andreak, ez gaitzetsi hitz bat aditzea
Loretto bat dautzuet ezartzen eskuan
Gaztean dabilana urhats holakotan
Labur higatuko da pena ahalketan.