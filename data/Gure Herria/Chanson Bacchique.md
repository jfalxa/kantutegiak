---
id: ab-3823
izenburua: Chanson Bacchique
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003823.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003823.MID
youtube: null
---

Aisa pensatu nuen
negorrek kusita
Artua yaten zuen
zartañan bustita
Arat xate,
Unat xate
Berriz edan gabe
Yuanen al zate?

Martin Ximon de Larralde
Beti adelante
Ardua edaten du
Botillatik gargante
Arat xate,
Unat xate,
Bularra zaharrian
Lukainkak, sar zaizte.

Artua yaten dezu
Zartañan busti ta,
Aisa pensatzen dezu
Neronek kusi ta.
Arat xate,
Unat xate,
Drago bat egin da
Oyan sar zaite.