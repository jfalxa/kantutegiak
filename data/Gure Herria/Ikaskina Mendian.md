---
id: ab-3829
izenburua: Ikaskina Mendian
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003829.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003829.MID
youtube: null
---

Gazterik hasi nuen gogoa alhatzen,
Bakezko estatua zein othe zaiteken;
Ez bainuen kausitzen egonez herrian,
Hortako jarri nintzen ikazkin mendian.

Urrikari nautenak dira enganatzen;
Urrikalgarriago ditut ezagutzen.
Bake osoa nun da hekien artian?
Ni hartaz gozatzen naiz ikazkin mendian.

Jaun onari diozkat eskerrak bihurtu,
Lagun zoro hetarik bainau aldaratu.
Beude beren atsegin galkorren erdian!
Prestuki biziko naiz ikazkin mendian.

Mendiko ur chirripak dohaz erasian;
Chori chocho, epherrek... kantazten sasian;
Urzo ihiztari arbolen gainian...
Nola trista naiteke ikazkin mendian?

Igandetan mezara menditik naiz jausten;
Jainkoaren manua hola ez dut hausten,
Egun hura gozatuz, ahaiden erdian;
Gero aste guzia... ikazkin mendian.

Zapetain, sastre, harotz, zurgin eta hargin,
Hek, hirian lanean igandearekin;
Astelehena besta, gero min burian!
Ni zuhur eta sano ikazkin mendian.

Gizon gazte, lorios merkatukaria,
Jan edan onen zale eta jokhairia,
Ontasunak horrela goaten direnian,
Erranen duk: Hobe zen ikazkin mendian!

Ene anaia goan zen Amerikan barna,
Laster zerbait eginik uztekotan lana;
Egun bezein aberats zen, sorthu zenian!
Hobe lukela dio ikazkin mendian.

Lili arrosa hainitz bada bazterretan,
Gazteen choragarri, herri guzietan;
Ez naute enganatu ni, orai artian;
Lorerik gabe nago ikazkin mendian.

Igande bestetan dut garbitzen larrua,
Plazara naiz agertzen ongi aphaindua;
Arrai dut arpegia, hitz onak mihian...
Nork ezagut nagola ikazkin mendian?

Osagarri banu zombeit urthez hola,
Zahartzean utz zombeit urthez hola,
Zahartzean utz niro mendiko etchola.
Ene moltsa luzea goritu denian,
Zertako egonen naiz ikazkin mendian.