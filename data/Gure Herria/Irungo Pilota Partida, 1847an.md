---
id: ab-3816
izenburua: Irungo Pilota Partida, 1847an
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Pilota partida bat, Irungo herrian,
Probintziarren eia Frantsesen artian ;
Jaun aphez Baztandar bat zen Frantsesekilan,
Gidari gobernador, kuraiarekilan,
Ethorria urrundik, molts-onarekilan,
Bitoriaren bila Kaskoinarekilan.

Probintzian jendia estimagarria,
Gazteria guzia da pilotaria :
Orai arte bazuten bethi bitoria,
Azpitik zadukaten Eskualdun herria,
Bainan orai Frantses bat, guti iduria,
Baztandar jaun batekin dute nagusia.

Jaun apezaz, kaskoinaz, girade mintzatu ;
Orai bertze lagunak, nahi'tut aipatu ;
Hazpandarrak diote ederki botatu,
Eta Uztariztarrak guziak charmatu ;
Baztane-ta Frantzia dire koroatu,
Eta Probintziarrak tristerik gelditu.

Ez da posible, Jaunak, kasik sinhestia.
Konfidantzia nola zuten ezarria :
Duroz kargaturikan mandoa ekarria,
Garbitu nahi zuten arraia guzia,
Bainan etcherakoan heien doloria !
Mandoak Irunen galdu kargaren erdia.

Probintziarrak ziren ongi preparatu,
Mandoa trebaseko duroz zen kargatu ;
Galtzeaz, duda gabe, ez ziren orhoitu ;
Abilak dire bainan, hek ere trompatu,
Español moneda da Frantzian frankatu,
Mandoa etcherakoan kargak ez kolpatu.

Piobintziarrek zuten prekozionia,
Berekin ekarria pertsu emailia ;
Berendako zaukaten arras bitoria,
Urrun zen pensatzia partida galtzia ;
Kanturik egin bada, ez daite egia,
Erretzen ahal dute oraiko kopia.

Urgulutan zirela, ez dugu dudarik,
Ez zuten ez pentsatzen partida galtzerik,
Uste zuten Frantzian ez zela gizonik,
Heien Kontra pilotan atherako zenik :
Jakin bezate beraz, Lapurtarren ganik,
Probintzia ez dela munduan bakarrik.

Irunen in derauku urrezko uria,
Bainan ez baliatu hanitz Probintzia;
Frantziako parterat zuen ichuria,
Aise doratu dugu Pausuko zubia.
Aintzinetik bantatzia ez da prudentzia,
Jaungoikoak eman daie punizionia.

Bitorios balire Irunen izatu,
Usoño bat behar zen berehala partitu,
Frantses kasta guzia zutela garbitu,
Miseria gorrian ginela gelditu;
Bainan usua zaie bidian trompatu.
Donostia utzirik, Lapurdin da sartu.

Arraio demonio! Kaskoin trunko hori
Bizitik bai loretik ederki duk hari !
Egundaino holakorik ez diagu ikusi ;
Horrekin behar diagu pilotan ikasi ;
Frantses pikaro hoier etzaie itsusi.
Lapurdi Probintzian pilotan nagusi!

Adios, Probintziarrak, orai bagoatzi.
Despendio sariak ditugu irabazi.
Baldin berriz haitzeko gosturik baduzi,
Etzaituztegu nahi errenkuran utzi ;
Mando mula eder hori ongi karga zazi.