---
id: ab-3791
izenburua: Donadoa Edo Muthil Zaharra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003791.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003791.MID
youtube: null
---

Bakharrik bizitzeaz unhatua frango,
Chede zarbait banuen lagun bat hartzeko;
Bainan orai andreak zembat ez du behar!
Ez, ez, nahiago dut egon muthil-zahar.

Herrunka guzietan estatua gaindi,
Egun oroz baitugu mila moda berri;
Hoinbertze gasturekin, nola menaian sar?
Ez, ez, nahiago dut egon muthil-zahar.

Grizetek utzi dute mokanes pullita,
Ustez hobeki dohan lore-dun boneta;
Zer! Boneta buruan neskatcha bat nik har!
Ez, ez, nahiago dut egon muthil-zahar.

Sederian sarthuak, printzesak iduri,
Fu! Egite diote soin telapintari;
Horrela uste dute zembait choro lokhar...
Ni ez banaiz logale, nago muthil-zahar.

Handiek badakite musian kantatzen,
Bainan badaklitea botoin baten lotzen!
Dandari jornaletan zembat urre, zilhar!
Ez, ez, nahiago dut egon muthil-zahar!

Mirail baten aldean, goizetik josiak,
Han dituzte miratzen aurkhintza guziak,
Egun gauza bat eskas, bertze zerbeit bihar...
Nor ez da lotsatuko? Gauden muthil-zahar.

Zenbat senhar munduan, gaizki gerthatuak,
Lephoraino sasian direnak sarthuak,
Oari egiten dute berantegi nigar;
Hanbat gaichto berentzat; egon muthil-zahar.

Ezkontzako bideak, lagun eta garbi;
Urrundik begiratuz, du belus'iduri,
Bainan da hurbiletik, dena harii, bizkar,
Nola bainaiz lautsuzko, nago muthil-zahar.

Segur da deithuko nau, batek galai hotza,
Behar bada bertzeak harpagon zikhoitza;
Laidoen jasaiteko senti baitut indar,
Askoz nahiago dut egon muthil-zahar.

Oi! Hauche da mendea gizon-gazten dako!
Orai andreak dire jaunen larrutzeko;
Ez othe da, diozu, chuhur zembait bakhar?
Ez nezake zin egin, gauden muthil-zahar.

Hobeago zuretzat othe-da zoria?
Zuri nago, neskatcha, goregi jarria?
Utzatzu sobraniak, izanen da senhar;
Non-ez, egonen zare bortchaz neska-zahar. (1.