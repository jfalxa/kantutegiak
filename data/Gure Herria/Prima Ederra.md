---
id: ab-3875
izenburua: Prima Ederra
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003875.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003875.MID
youtube: null
---

Prima ederra, zutan fidaturik,
Hainitz bagira, oro trompaturik.
Enea zirenez errazu, bai al'ez,
Bertzela, banoa
Desertutara, nigarrez urtzera!

Desertutara joan nahi bazira,
Beraz zoaza, orai berehala!
Ez zaitezela jin berriz nere gana,
Bertzela gogoa
Histuren zautzu, amoros gaichoa!

Nitan etsemplu, nahi duenak, hartu,
Ene malurrak parerik ez baitu!
Charmagarri bat nik izan dut maitatu...
Fidatu..., trompatu!
Ochala! Sekulan ikusi ez banu!

Mintzo zirare arrazoin gabetarik,
Ez dutala nik zur'amodiorik.
Zuhaur baino lehen banuen besterik
Maiterik, fidelik...
Hortaz ez dautzut egiten hobenik.