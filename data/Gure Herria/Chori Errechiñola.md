---
id: ab-3888
izenburua: Chori Errechiñola
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

Chori errechiñola udan da kantari,
Zeren orduan baita kampoan janari,
Neguan ez d'ageri! Ez ahal da eri!
Udan jin baladi,
Kontsola naite ni.

Chori errechiñola ororen gehien,
Bestek baino hobeki hark baitu kantatzen,
Harek du enganatzen, mundua bai trompatzen;
Bera ez dut ikusten,
Bai boza entzuten.

Boz hura entzun nahiz herraturik nago;
Ni hari hurbil eta hura urrunago...
Jarraiki nindaroke azken hatseraino!
Aspaldi handian
Desir hori nian.

Choria zoinen eijer kantuz oihenean!
Nihaurek entzuna dut iragan gauean.
Eian, goazen, maitia, biak ikustera:
Entzuten baduzu,
Charmaturen zaitu.

- Amak ützi ninduenn udaren hastean,
Geroztik nabilazü hegalez airean.
Gauaz erori nuzu sasiño batean...,
Han zuzun chedera,
Oi! ene malura!

- Choria, zaud'ichilik, ez egin kanturik,
Ez othoi niri hola zuk egin pleinurik!
Ez duzu probetchurik, hola ni zaurturik,
Ez eta plazerik,
Ni tomban sarturik!

- Bortuak churi dira elhur direnean,
Sasiak ere ilhun, osto direnean.
Ala ni malurusa, han sartu bainintzan!
Joan banintz aintzina,
Eskapatzen nintzan!

- Choria, zaud'ichilik, ez egin nigarrik!
Zer probetchu dukezu hola penaturik?
Nik eramanen zaitut chedera hautirik,
Ohiko bortutik,
Ororen gainetik!...