---
id: ab-3845
izenburua: Niri Maitiak Erran...
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003845.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003845.MID
youtube: null
---

Niri maitiak erran zerautan
Pollit nintzez? (bis)
- Pollit, pollit nintzela, bainan
Larrua beltz!
La-le-ra-la-la! Larrua beltz!

Niri maitiak erran zerautan
Primu nintzanez? (bis)
Primu, primu nintzela, bainan
Etcherik ez!
La-le-ra-la-la! Etcherik ez!

Niri maitiak erran zerautan
Moltsa banuenez? (bis)
- Moltsa, moltsa banuela, bainan
Dirurik ez!
La-le-ra-la-la! Dirurik ez!

Niri maitiak erran zerautan
Lanian nakienez?
Lanian, lanian nakiela, bainan
Aiherrik ez!
La-le-ra-la-la! Aiherrik ez.