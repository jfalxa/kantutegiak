---
id: ab-3836
izenburua: Lo, Lo, Nere Maitea
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003836.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003836.MID
youtube: null
---

Lo, lo, nere maitea!
Lo, ni naiz zurekin,
Lo! Lo! Paregabea,
Nigarrik ez egin;
Goizegi da! munduko
Gelditzen bazira,
Nigarren urtatzeko,
Baduzu dembora.

Lo ! nik zaitut higitzen ;
Lo ! lo ! nombait goza !
Ez duzuia ezagutzen
Amattoren boza ?
Etsai guzietarik
Zure begiratzen,
Bertze lanak utzirik,
Egonen naiz hemen.

Lo ! lo ! ner' aingerua .!
Bainan ametsetan
Dabilkazu burua.
Hirria ezpainetan,
Norekin othe zare ?
Non othe zabiltza ?
Ez urrun, ama gabe,
Gan, ene bihotza I

Lo ! lo ! zeruetarat
Airatu bazare,
Ez bihur zu lurrerat,
Ardietsi gabe,
Ongi zure altchatzeko.
Enetzat grazia ;
Guziz eni hortako
Zait ezti bizia.

Lo ! lo ! gauak oraindik
Nombait du eguna ;
Ez da nihon argirik,
Baizik izarrena.
Izarrez mintzatzean,
Zutaz naiz orhoitzen ;
Zein guti, zure aldean,
Duten distiratzen !

Lo ! lo ! dembora dela
Iduri zait, albak
Histen ari tuela
Ekhi gabazkoak ;
Choriak, arboletan,
Kantaz hasi dire ;
Laster nere besoetan
Gozatuko zare.

Bainan atzarri zare,
Uso bat iduri ;
Huna nik zembat lore
Zuretzat ekharri :
Ametsetan ait'amez
Othe zare orhoitu ?
Ai ! hirri maite batez,
Baietz erradazu .