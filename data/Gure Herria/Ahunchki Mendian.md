---
id: ab-3781
izenburua: Ahunchki Mendian
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003781.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003781.MID
youtube: null
---

Goazen oro Ahunchkira,
Goazen, ithurrira,
Goazen guziak mendira,
Harbiribilera !

Errepika.
Kanta zagun airetto hau orok betan, )
Kanta zagun guziek errepiketan.! )

Hedoien artetik guri
Jali zaukn ORHI ;
AHUNE, egizak irri,
Orratz bat iduri !

Heldu dira hor Artzainak,
Chuberatar onak ;
Huna heien irrintzinak,
Zinka ozenenak !

Choratuak horchet gaude:
Oi zombat arfchakle !
Bazkan igeri hor daude ;
Behiak bertzalde...

Arranoak burrumbaka,
Hegalez zaflaka,
Harat hunat, inguruka,
Doazi multzoka.

Arranoa, habil, habil,
Karrankaz ez ichil !
Badiaguk, guk, norat bil:
Huna Harbiribil .