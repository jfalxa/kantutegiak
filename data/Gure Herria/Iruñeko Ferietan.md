---
id: ab-3802
izenburua: Iruñeko Ferietan
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003802.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003802.MID
youtube: null
---

Iruñe-ko ferietan,
Iragan San Ferminetan,
Ehun zaldi arribatu Andaluziatik tropan ;
Merkatu'eder bat zauntan,
Zagolarik bi lerrotan.

Bat zen pikar'ta zuria,
Bota bainion begia :
Andaluz batek egin daut bi untz urheren galdia
Eskain'orduko erdia,
Harzak, hiria duk zaldia...

Han nintzan harrapatua,
Aginduaz dolutua,
Urruntzeko izan banu hirur arditen lekua.
Gizonez inguratua,
Iduri preso hartua!

Zeruko jinko jaun ona,
Zerek ekharri nu huna!
Andaluz bat zazpi urthez presondegian egona
Laburzki mintzo zauntana:
« Konda niri hitzemana!!»

Jauna nahi dut pagatu,
Bainan lehenik miatu .
Zaldiaren miatzeko astirik orai ez duzu
Lenik soma konda zazu,
Gero miaturen duzu.

Nik pagatu-ta kondua,
Eztitzen hasi mundua ;
Krachturirik hartu nuen delako behor maingua
Emanik behar dirua,
Hustu bainuen lekua.

Utzirik bide ederra,
Hartu dut larre bazterra:
Zaldia nuen desprra, begi batetik okerra,
Maiz jotzen zautan kitarra,
Eztularekin uzkerra !!

Belategiko bentetan,
Pasatu nintzen andetan ;
Nolazpeit arribaturik Urdazuriko errekan
Sartu baitzautan phartetan,
Athera nuen kordetan!

Nonbeitikan goizaldera,
Arribatu naiz etchera ;
Anderea jin zerautan argiarekin athera, jarri behorrari beira,
Ez baitzen kontent sobera!

Houche da behor tcharra
Eta bertzalde chaharra!
Hortan gastatu dautazu familiako beharra.
Sal nintzazke sos batera
Zaldia eta senharra !

Zaude ichilik andria,
Othoi emazu bakia ;
Behak et'arranuak bortutik daude goardia;
Hil azu beraz argia,
Hunat ez diten abia.

«Zaude ichilik zu ere
Holakorik erran gabe ;
Belerik ez arranorik hemen ez baita batere
Segur eginak baikire
Estofa on baten jabe!

Plazan zutenian ikusi,
Nahi zaundaten erosi:
Tratuan baikinen hasi, etzuten emaiten aski
Ez dut ez eskutik utzi,
Ume bat behar daut hazi.

Heldu den ume berria,
Behor edo zamaria,
(Eginen baitut goardia denez ama iduria
Balinba aski handia!)
Ai zer primako zaldia..