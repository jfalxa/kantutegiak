---
id: ab-3830
izenburua: Napoleon Iii
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003830.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003830.MID
youtube: null
---

Mila zortzi ehun.ta berrogoi hamabi,
Eman nahi nituzke zombeit bertsu berri,
Ohore emaiteko Napoleon-i,
Zori onean dela gu ganat ethorri;
Guziak nahi gaitu trankilik ezarri,
Bakea emanikan erresumari.

Huna nola jin zaukun gure iguzkia,
Argitzen duelarik Frantzia guzia;
Aspaldian ginauden berandetsia,
Zombeitek hartzen dute hortaz jelosia,
Zeren emaiten zaion bere merezia,
Orai ez baita lana gaizki hasia.

Napoleon Frantzian hirugarren zare,
Biga ezagutu ditut lehenago ere,
Oseba eta kusia, Emperadore;
Gu heien zerbitzuan ibiliak gare;
Zerbitzatuko dugu zuretako ere,
Adinez zahartuak izanik ere.

Luis Napoleon harrazu kuraie,
Zure zebitzatzerat borondatez gaude;
Erresuma guziak ikharan daude,
Norat nahi joaiteko orai gauzak gare,
Baldin zu gurekilan ethortzen bazare.
Etsai guzien kontra beldurrik gabe.

Autrichia jin zaiku umiltasunean,
Bere beharrarekin, chapela eskuan;
Leheneko obra onak dauzka gogoan,
Napoleon gaztea han galdu ondoan,
Ez baitzuten pentsatzen batere orduan,
Bertze Napoleon-nik bazen munduan.

Autrichia bezela Inglesa ere,
Uste dut ez dagoen beldurrikan gabe;
Afera pasatuez orhoitzen dire;
Napoleon zaharra han galdurik ere,
Iloba hemen dugu, harendako ordre,
Laster eginen dugu Emperadore.

Napoleon zaharra hortan zen trompatu,
Inglaterra zuen egoitzaz hautatu;
Kriminel bat bezala zuten desterratu;
Isla Santa-Elenarat zuten desterratu;
Harroka baten puntan hantchet zen finitu;
Ilobak nahikotu zorrak pagatu.

Egin izan diote horren bertze obra,
Onikan ez batere, gaichtorikan sobra;
Errekompentsa onekin iloba hor da;
Ez othoi berandetsi, jinen da dembora;
Zuek izana gatik burua gogorra,
Arrazoina nork duen ageriko d.