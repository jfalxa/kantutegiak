---
id: ab-3841
izenburua: Pipa Hartu'Ta
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003841.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003841.MID
youtube: null
---

Pipa hartu'ta, choratzen naiz,
Arno edan'ta, mochkortzen naiz,
Mochkortu eta, erortzen naiz!
Nola demuntre biziko naiz?
Tralera lera...

Uria ari zerutikan,
Nihor ez dabil karriketan;
Neure lagunak tabernetan,
Eta nik ez dut dirurikan!
Tralera lera...