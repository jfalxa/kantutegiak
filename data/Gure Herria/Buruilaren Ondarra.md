---
id: ab-3872
izenburua: Buruilaren Ondarra
kantutegia: Gure Herria
partitura: null
midi: null
youtube: null
---

I
" Buruilaren ondarra... Jon Doni Mikele...
Maitia, ikustera heldu naiz ni ere.
Baldin plazer baduzu, othoi, jeiki zaite !
Zure hor ikustia desiratzen nuke ...

II
- " Ez, ez, eaz naiz jeikiko, seguratu bainiz
Adichkide tutzula neskatilla ainitz...
Eskerrak Jainkoari, hola libro bainiz !
Malurus behar nintzen, fidatu ni banintz !.

III
- " Ez dut bada iziltzen sakelan limarik,
Ta are gutiago kontrako giltzarik...
Amodioik ez bauzu, ez idek bortharik;
Etchera janen nuzu jin nizan bidetik !

IV
" Lersunak, lerro lerro, bidian badoazi,
Beruaren lekurat, hotzaren ihesi...
Han ere bethi penaz beharko naiz bizi;
Zombait aho-kirets nik beharko iretsi !!!".