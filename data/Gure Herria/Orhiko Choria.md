---
id: ab-3783
izenburua: Orhiko Choria
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003783.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003783.MID
youtube: null
---

Orhiko choria Orhin bakean da bizitzen,
Bere larre-sasietan ez da hura unhatzen;
Han zen sorthu han handitu, han ari zen maithatzen,
Han bere umen artean gozoki du kantatzen.