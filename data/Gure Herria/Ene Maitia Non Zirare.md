---
id: ab-3860
izenburua: Ene Maitia Non Zirare
kantutegia: Gure Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003860.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003860.MID
youtube: null
---

Ene maitia non zirare? Zato, zato leihora,
Urrundanik jina nuzu ni zure ikustera;
Zatoia bada lehiora, sar nadientzat barnera.

Ene maitia, hor zirare, bainan alfer debalde;
Nik erran dautzut lehen ere, disimularikan gabe,
Nik ez dudala zuretako amodiorik batere.

Ez, ez daitazu deklaratu, are gutiago esplikatu,
Zeren ez daitazun aiphatu, gutiago esplikatu;
Orai zaitut ezagutu, amodiorik ez duzu!

Muthil gazte, buru handia, ez duzu hainitz balio;
Ez deia bada nihor, nihon, ni baizik ezkon-gai denik?
Ez duzu kredit handirik, ez badukezu bertzerik!

Bai, etcheko alaba, bada zu bezala bertzerik,
Zu bezein ala onik, ezkon-gai daudezenik;
Baduket bai nik hetarik; zuri ez errana gatik.