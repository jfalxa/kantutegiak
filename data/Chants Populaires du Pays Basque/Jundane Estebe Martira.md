---
id: ab-3450
izenburua: Jundane Estebe Martira
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003450.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003450.MID
youtube: null
---

Jundane Estebe martira,
Oihergiko patrua,
Agorrilan gerthatzen da;
Ainharbiarrak jun ginandin
Hanitch besta hartara,
Ez eskandal emaitera,
Ez aharra cherkhatzera,
Bena bai libertitzera.

Arrastirian dantzatü,
Plazer ere bai hartü,
Ete oro akort heltü;
Ülhüna zenin abantzü
Etcherat abiatü:
Bi adichkide baratü,
Abüsione gerthatü,
Eta hüllan bizia galdü.

Etchegoihen eta Benta,
Uste düt deitzen direla,
Desfortüna zen haiena!
Koki saldo bat gibeleti
Makhila khaldüka hasi
Gü lürrerat nahiz egotchi,
Guri biziak idoki
Nahi beitzien segürki.

Koki saldo bat gibeleti
Makhila kahldüka hasi
Ustez biziak idoki.
Makhilak hürretarat ützi
Ganibetak idoki
Eta tchichtaka han hasi:
Guri sabelak iaurri,
Odolez igeri jarri.

Mezü igorri berhala
Donaphaleü hirila
Musde Lojieren chrkhara;
Gimon barbera lagün hartürik
Jin zen gure herstera.