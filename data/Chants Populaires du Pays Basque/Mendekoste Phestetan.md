---
id: ab-3449
izenburua: Mendekoste Phestetan
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003449.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003449.MID
youtube: null
---

Mendekoste phestetan,
Aurthen Arnegiko errekan,
Istorio bat gerthatu da kasik espaitiot erran:
Bost emaztek edan dituzte hamalau pinta arno betan,
Jokhaturik kartetan.

Jokua zuten florian
Arnegiko Madrilenian;
Atso gachuek uste zuten zirela zeluko lorian,
Saindu ororen erdian, bere botoilak aldian,
Bai eta plazer handian.

Batak zion bertziari:

K... eta Kattalin,
Auzo biak elgarrekin,
Arno edaten ari ziren koraje handi batekin;
Gero ondoan bazuten gibel aldian zer egin,
Bere fanfarreriekin.

Hirurgarrenak ederki
Tanteatzen omen daki;
Hamarrekuak harek zituen omen markatzen orori,
Ondarrekotz nahasi zen eta lurrerat erori;
Orduan etzen egarri.

Laurgarren hori zer pheza,
M... zapatainesa;
Untsa esplikatzen zituen heskuara eta frantsesa,
Bai eta aisa egiten bide handian esa;
Horiche da haren letra.

Bostgarren hori zoin othe da?
Phentsatzeko ez da phena;
Z... gaizo horrek nola ezpaituke lehena;

Ahuntzez jokhatu eta
Hasi ziren kolpeka;
Kaston deitzen zen guarda gazte bat, kampotik jinik lasterka,
Hak phartitu izan zituen, ez uste bezain aisa,
Arrazoinik ezin phasa.

Zuk ere, Etchek'anderia,
Erran beharzu egia;
Solas hortan zu othe zinen harien buruzagia?
Bost emaztek egiteko horiche da komedia!
Hurren baitzen zahagia.