---
id: ab-3416
izenburua: Ene Izar Maitia
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003416.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003416.MID
youtube: null
---

Ene izar maitia,
Ene charmagarria,
Ichilik zur'ikhustera
Jiten nitzauzu leihora;
Koblatzen dudalarik
Zaude lo'kharturik:
Gauazk'ametsa bezala
Ene khantua zauzula!

Zuk ez nuzu ezagutzen,
Hori ere zaut gaitzitzen;
Ez duzu ene beharrik
Ez eta acholarik.
Hil edo bizi nadin
Zuretako berdin!
Zu aldiz, maite Maria,
Zu zare ene bizia!

Amodiozko phena zer zen
Oraino ez nakien!
Orai ez nuzu biziko
Baizik zu maithatzeko.
Norat den ichurkia
Hara joaiten da hura:
Orobat ni, maitenena,
Jiten niz zure gana!

(Vicomte de Belzunce.