---
id: ab-3423
izenburua: Kaiku
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003423.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003423.MID
youtube: null
---

Neure andrea andre ona da, gobernu ona du ... hauzuan;

Hartzen duelarik bele alaba, Mari-Kattalin, altzuan,

REFRAIN:

A'i zazu!
- Zer nah'uzu?
- Gero're horrela munduan ere biak biziko gerare gu,
Baldin bazara kontentu.
Nik bethi freskotchorik tabernakua
Sototchuan deraukat arno gozua.
Ai! zer kontentu! ai! zer alegre! ezkaintzen dautzut, neure maitia,
Arraultzetchuak eta kaiku esnia.

San Blas'alderat ninduelarik, makhiltcho baten gainean,
Arhantze beltz bat sarthu zitzautan, uspela nuien oinean!

AU REFRAIN.