---
id: ab-3426
izenburua: Iruten Ari Nuzu
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003426.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003426.MID
youtube: null
---

Iruten ari nuzu, khilua gerrian,
Ardura dudalarik nigarra begian.

Jendek erraiten dute ezkondu ezkondu:
Nik ezdut ezkon-minik, gezurra diote.

Ezkon-minak dutenak seinale dirade:
Mathel hezurrak seko koloriak pherde.

- Jendek erraiten dute hal' eztena franko,
Ene maite pollita, zur' et' enetako.

Ezkontzen balin bazira mariñelarekin
Chardina janen duzu makalluarekin.

Ezkontzen balin bazira mandazainarekin
Arraina janen duzu oliuarekin.

- Anaia, nahi duzuia emazterik erosi:
Baratze kantoinetan sosian hemezortzi.

- Arreba, nahi duzuia gizonik erosi:
Eliza bazterretan bi sosetan zortzi.