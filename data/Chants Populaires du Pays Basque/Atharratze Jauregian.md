---
id: ab-3446
izenburua: Atharratze Jauregian
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003446.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003446.MID
youtube: null
---

(Ziberutarrez)
Atharratz jauregian bi zitroiñ doratü;
Ongriako Erregek batto dü galthatü;
Arrapostü ükhen dü eztirela huntü,
Huntü direnian batto ükhenen dü.

Atharratzeko hiria hiri ordoki,
Hur handi bat badizü alde bateti;
Errege bidia erdi erditi,
Maria-maidalena beste aldeti.

- Aita, saldü naizü idi bat bezala;
Ama bizi ükhen banü, aita, zü bezala,
Enündüzün ez juanen Ongrian behera,
Bena bai ezkuntüren Atharratzeko Salala.

Ahizpa, juan zite portaliala,
Ingoiti horra düzü Ongriako Erregia;
Hari erran izozü ni eri nizala,
Zazpi urthe huntan ohian nizala.

- Ahizpa, enükezü ez sinhetsia,
Zazpi urthe huntan ohian zirela;
Zazpi urthe huntan ohian zirela;
Bera nahi dükezü jin zü zien lekhila.

Ahizpa jaunts ezazü arrauba berdia,
Ni ere jauntsiren dit ene churia;
Ingoiti horra düzü Ongriako Erregia;
Botzik kita ezazü zure sor etchia.

- Aita, zü izan zira ene saltzale,
Anaie gehiena dihariren hartzale,
Anaie artekua zamariz igaraile,
Anaie chipiena ene lagüntzale.

Aita, juanen gira oro alkharreki;
Etcherat jinen zira changri handireki,
Bihotza kargatürik, begiak bustirik,
Eta zure alhaba thumban ehortzirik.

Ahizpa, zuza orai Salako leihora,
Ipharra ala hegua denez jakitera;
Ipharra balimbada goraintzi Salari
Ene khorpitzaren cherka jin dadila sarri.

Atharratzeko zeñiak berak arrapikatzen;
Hanko jente gazteriak beltzez beztitzen,
Andere Santa-Klara hantik phartizten;
Haren peko zamaria ürhez da zelatzen.