---
id: ab-3439
izenburua: Zelüko Izarren Bidia
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003439.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003439.MID
youtube: null
---

(Ziberutarrez)
Zelüko izarren bidia
Nik baneki,
Han niro ene maite gaztia
Chüchen khausi
Bena gaur jagoiti nik hura
Ez ikhusi!

Haritch gazte bat nik aihotzaz
Trenkatürik
Üdüri zait ene bihotza
Kolpatürik:
Erruak eroriko zaitzola
Eihartürik!

Ahal baliz ene begia
Zerratürik,
Ene maite gaztiarena
Argitürik,
Ezar niro ene odola
Ichuririk!

Zeren beitzen lili ororen
Eijerrena,
Bai eta ene bihotzeko
Maitenena,
Haren izanen da ene azken
Hasperena.