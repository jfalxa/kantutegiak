---
id: ab-3422
izenburua: Ikhazketako Mandoa
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003422.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003422.MID
youtube: null
---

Hauche da ikhazketako mandoaren traza:
Lephoa mehe du eta ili latza,
Itchura gaitza;
Bastape guzitikan zauriak balsa,
Hauche da salsa!
Kristaurik ez daiteke aldetik phasa.

Lephoa mehe du eta buruia handi,
Mathel hezurra seko, dena beharri,
Begiak eri;
Bi sudur ziluetarik mukuia dari,
Ezpainak larri:
Hortzik izan badu ere ez duk ageri.

Bi hitzez a'izatzue laur hatzen failak:
Belhauna handi eta makhur gidailak,
Lurrera zailak;
Kortesia eskatzen belhaunez ari da,
Gero ezin ari da;
Haurren mantenatzeko zer abantaila!

Fantasiak asko badu mando lapurrak;
Laur zangoak trepel eta juntetan hurak,
Anka makhurra;
Ezpata bezain chorrotch bizkar hezurra,
Ez duk gezurra!
Noiz larruturen zautan ni naiz beldurra.

Mando itsusi, zikhin, lotsagaria,
Galtzera bota didak osagarria,
Okhagarria!
Sartzera eztio utzi nahi ostalerriak,
Haren sartziak
Urrintzen zaiotela etche guzia.

Buruko krapestuak grandeza badu;
Erostunikan balu nahi luke saldu,
Ahal bezain zalhu:
Chikibat arnoren saria 'iten ahal balu,
Chanrrantchak salbu,
Tratu hortan hainitzik ezliro galdu.

Aitzineko petralaz dago espartuz,
Zeren dako'ta duien erdia espartuz,
Bertz'erdia trapuz,
Hatzeman phuska guziak lurretik hartuz;
Ase niz tratuz,
Enfadaturik nago botigan sarthuz!

Sokak palupan eta lazoa trostan,
Errekari behera juan zazkit postan:
Ezpainak oskan;
Ehun goropillo eta berrehun buztan
Bakhotcharen phuntan:
Hek baino hobe likek batere ez ukhan.

Galtzera bota didak mantabat fina,
Atchunaren demboran Kadiztik jina,
Oi! Manta fina!
Oro goropillo eta zilo et'ezkina,
Petatchuz egina;
Sekulan etzaut juanen mantaren mina!

Sakerdibat berria badu bereki:
Beldurra dago ladronek nombeit edeki.
Dabil'ederki:
Kapusail tzar phuska bat larrubateki,
Kumpuntuz bethi,
Zirdina dariola anketan baiti.

Ene manduak duen zingilar khorda
Juan den zazpi urthian botigan zor da,
Dembora sobra!
Merkatariarekin erreitut bordak,
Testigu obrak!
Nik ere hartzekoak etzaizket kobra.

Ikhatz zaku ederren jabi dabilla:
Ahoak non dituen eztaite bilha,
Phorroska milla;
Zazpi zortzi petachu elgarrekilan
Ziloekilan:
Heien bethazaileak badik aski lan.

Eskuaraz: zingila eta erdaraz: chincha,
Horren gainean ere badut zer mintza:
Biluaren gisa
Trenka dakidan beldurrez eztirot tinka,
Kargak egin'ta;
Hortan gezurrik bada lephoa phika.

Mandoa zahartu zaut, krastuak hautsi,
Erostean 'in zorra oraino bizi,
Badut lan aski;
Norat nahi juan nadin zorra dut nausi:
Hobe dut naski
Ferrak athera eta larrerat utzi.

Neskatcha banintz eta majoa falta,
Ikhazkinik ezkontzaz ez niro trata,
Jainkoak barkha!
Lastiamgarri baita ikhazkin hauta,
Dabilan planta,
Bethiri eztirola etchetik athera!

Ikhatza saldu eta ondoko traza,
Gaitzurubat arthoren saria falta,
Etchera juan da;
Andriak nigar eta haurrek marraka,
Et'ezin balaka!
Talo baten gaiik eztaik harrapa.

Abarkatik has nadin emeki emeki,
Haragi ustel urrin bat badu bereki;
Haler'ederki
Aztal'eta behatza kampoan bethi,
Ziloa petik:
Zangoak erretzeko perilik eztik.

Galzazpien berriak errenentut garbi:
Botoinak ttiki eta chiluak larri,
Chotchak ezari;
Berrehun lekhutarik larri'ageri,
Bragetak irri...
Jainkoak pharte! Pikorrik ezduke sarri.

Barneko motchbat badut abhitz ederra;
Urratuiaren hutsez ezdirot zerra,
Andre alferra!
Orratza duenian hariz gerla,
Apho fardala!
Sukhaldian lo dago jaiki eta berla.

Hara ikhazketariek duten girua;
Bonetaren kaskoan chuchen zilua,
Phuntuz bildua;
Hirur laur erhi trebesetan chuti bilua,
Enemigua!
Nik estakit zer'iten duten dirua.