---
id: ab-3448
izenburua: Orai Banuazü Herriti
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003448.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003448.MID
youtube: null
---

(Ziberutarrez)
Orai banuazü herriti:
Ardüra dizüt nigarra begiti!
Bat maithatü nian gogoti,
Bihotzaren erdi erditi;
Kilatü behar dizüt tristeki,
Ai! Ei! Nula biziko niz ni!

Erresoli ezazü llabürski,
Lüze zira zü eskierki.
Kitaü nahi balin banaizü
Behin untsa so egidazü:
Zertzaz ziradian engajatü
Eta nur ükhen düzün maithatü.

Ene maitia, orai nik
Jakin nahi nüke zü ganik
Zer zadükan horren tristerik,
Zure so eztiak oro galdürik;
Ala badüzün beldürkünterik
Maithatzen düdan zütaz besterik.

Tristerik banago ere
Eniagozü arrazu gabe;
Zerk eginen dereit plazerik
Galdü düdanaz geroz bistarik
Izar bat zuñek ezpeitzin parerik,
Bihotza dizüt erdiratürik!

Galdü düzia izar bat,
Batere parerik etzian bat?
Bihar arratsan jelkhiren düzü argirik
Enkas ezpada odoi ülhünik;
Ükhen eztezazün phenarik
Har ezazü zelütik elkhirik.

Ezta zelian izarrik
Nik maite düdanaren parerik;
Lürrian sorthü izana gati
Argitzen dizü ororen gaiñeti;
Haiñ pollit izatiareki
Ez estone maite badüt nik.

Ene maitiak igorri
Choriño batekila goraintzi;
Khorpitzez hürrün izana gati
Bihotzez dela bethi eneki;
Ikhustera jinen dela llabürski,
Bizi nadin alageraki.

Gaiherdi erdi erditan
Nindiagozün jarririk leihuan
Nuntik ikhusiren züntüdan
Esparantcha handi handitan;
Hoztürik eta berantetsirik
Juan nündüzün oherat tristerik.

Amodio hanitch ükhen dit
Bena orai kitatzen zitit,
Büriala jitekoz zentzürik
Arauz ordu orai ahal dit:
Kitatü behar zütüt orai nik
Ezin beitüt egiten besterik.