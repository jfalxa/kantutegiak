---
id: ab-3444
izenburua: Gaiaz Eder Da Argizaria
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003444.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003444.MID
youtube: null
---

(Ziberutarrez)
Gaiaz eder da argizaria,
Egünaz ere bai ekhia;
Haien pare da ene maitia,
Arren haiñ da charmagarria!

Igaran gaian, ametsetarik,
Botz bat entzün dit charmantik,
Eztitarzünez betherik beitzen,
Haren parerik ezpeitzen.

Charmagarria, lo ziradia,
Eztitarzünez bethia?
Lo bazirade, iratzar zite,
Ez othoi ükhen herabe.

Amodiua, gaiza erhua,
Jentia trumpa liuna!
Gaiak lo gabe, egünaz ere
Errepausürik batere.

Zure ganik orai phartitzia
Üdüritzen zait hiltzia!
Indazüt por bat, ene maitia,
Mentüraz date azkena.