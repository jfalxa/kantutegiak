---
id: ab-3445
izenburua: Aitarik Ez Dut
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003445.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003445.MID
youtube: null
---

(Basa-Nabartarrez)
Aitarik ez dut eta ama ere zahartu,
Norbeiten beharra ere etchian badugu;
Zuk desirra zer den orai badakizu.

Etcheko anderia, zure niz ahalge,
Zure alaba prima gald'egiteko ene;
Ukhan dut adizkide bai eta erraile
Jarrikiten zaiola bertze zombeit ere.

- Neguaren ondotik jiten duzu uda:
Zertako egiten duzu horrembeste duda;
Zer uste duzu ala haizu dela bethi
Gorthiaren egitia prima gaztiari.

- Etcheko anderia, jiten niz zu gana
Entzunik baduzula lilibat charmanta;
Entzunik baduzula lilibat charmanta,
Lilibat charmanta eta bihotzbat net ona.