---
id: ab-3424
izenburua: Alageraz
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003424.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003424.MID
youtube: null
---

Alageraz nik eztüt khantatzen,
Bena bai sofritzen,
Bai eta komünikatzen;
Etsaia espantatzen
Nulaz düdan khantatzen
Alagera gabe.

Gai aiñhera, habilu'etcherat
Utz nezak lotarat
Gaiaren igaraitera!
Hire plazer handia
Lükek iratzartzia
Lo daguen ihizia.