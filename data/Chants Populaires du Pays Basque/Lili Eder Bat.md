---
id: ab-3453
izenburua: Lili Eder Bat
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003453.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003453.MID
youtube: null
---

(Basa-Nabartarrez)
Lili eder bat badut nik
Aspaldi begichtaturik;
Bainan ez nainte mentura haren eskura hartzera,
Banaki zer den lanjerra,
Juan nindaiteke aldera.

Lili ederra, so' idazu
Maite nauzunez errazu.
Zure begiak bihotza barnarik deraut kolpatu;
Kolpe huntarik badizu
Granganatzeko herrichku.

- Ez nuke nahi malurrik
Balin bazindu ni gatik;
Ez dut uste ene begiek eman dautzuten kolperik;
Ez duzu beraz lanjerrik,
Etzira hilen hortarik.

- Mintzo zirade polliki
Polliki eta tendreki.
Bisitañobat nahi nuke egin nik zure ganat laburski;
Desirra badut segurki
Bazinezadat permeti.

Permeti niro, maitia,
Ez dut hain bihotz dorphia;
Bainan lehenik nahi dut jakin zure desirra guzia:
Zu ene ganat jitiaz
Estona laite mundia.

- Ez ahal nuzu dukia,
Ez eta Aitorren semia!
Hola nehoren estonatzeko ez da sujet bat handia;
Zuhaur zirade simplia,
Opinionez bethia.

- Badakit simple nizela;
Hori segurki hala da;
Iduritzen zaut atsolutoki ezagutzetan zirela;
Orai nik badut fortuna;
Bainan ez dut zuk emana.