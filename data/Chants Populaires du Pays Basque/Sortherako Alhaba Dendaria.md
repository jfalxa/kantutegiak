---
id: ab-3460
izenburua: Sortherako Alhaba Dendaria
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003460.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003460.MID
youtube: null
---

(Ziberutarrez)
Sortherako alhaba dendaria,
Goizian goizik jostera jauilia,
Nigarretan phasatzen dü bidia,
Aperendiza konsolazalia.

Zelian den izarrik ederrena
Jin balekit argi egitera,
Juan nainte maitiren ikhustera,
Ene phenen hari erraitera.

Mündü huntan phenarik gabetarik
Bizitzeko nik eztüt perillik;
Maithatü düt ükhenen eztüdana,
Horrek beitereit bihotzian phena.

- Maithatü düzia ükhenen eztüzüna,
Horrek dereizia bihotzian phena?
Maith' ezazü ükhen diokezüna
Eta juanen da zure bihotz mina.

- Merchikaren floriaren ederra!
Barnian dizü echürra gogorra;
Gaztia niz, bai eta loriusa,
Eztizüt galtzen zure esparantcha.

Adios beraz, ene maitia, adios!
Adios dereizüt orai sekülakoz;
Ezkunt zite plazer d.zünareki,
Eta begira ene arrakuntrüti?

- Zer izanen da zure arrakuntria?
Zer ahal da zure egin ahala?
Zerbait khasü nitan gerthatzen bada
Süjet berri bat zütan izanen da.