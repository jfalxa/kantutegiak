---
id: ab-3442
izenburua: Goizetan Jelkhitzen Da
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003442.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003442.MID
youtube: null
---

(Ziberutarrez)
I
Goizetan jelkhitzen da izar bat ederrik,
Hura dela diote zelian ederrenik
Lürrian ikhusten dit bat ederragorik,
Zelietan ere ezpeitü harek bere parerik.

Aspaldiko demboretan, gaiaz eta bethi,
Ihizen nabilazü chori eijer bati;
Azkenekoz atzaman dit, oi! bena tristeki!
Lümarik eijerrena beitzaio erori!

Chori khantazale, eijer, charmagarria,
Aspaldian eztit entzün zure botz eztia;
Arren kuntsola zite, tristeziaz bethia,
Etzirade izanen gaïzki tratatia.

- Eijerki mintzo zira, üsatü bezala,
Trumpatü nahi naizüla badizüt beldürra;
Zü ziradila kausa galdü dit leibertatia,
Enezazüla kita, fidela bazira.

- Zük eztakizia jaon galant bat nizala,
Seküla trumpatzia phentsatü eztiana;
Ezpazira fidatzen gizon galant bati,
Etzitiala fida jagoiti besteri.

II
- Arrosa buketto bat, üztarilan sorthürik,
Igorri niriozün jaon hari goraintzi;
Lantharia niala haren baratzeti,
Untsa begira lezan nitzaz orhitürik.

Uste ükhen nükian plazer zükiala
Bere lantharetik ükheitez buketa;
Igorri ditadazüt nahi eztiala,
Lantharerik emanik orhitzen eztela.

Ene lili eijerra, hunki jin hizala!
Eztat, ez, nik eginen jaon harek bezala;
Freskorik hait etchekiren ene bulharrian,
Deithiratzen haidalarik jaon haren izenian.

Ene gazte lagünak libertitzen plazan,
Eta ni, malerusa, tristerik khamberan!
Jaon gazte eijer bati eman neron konfidantza:
Hura eni baliatü traidore bezala.