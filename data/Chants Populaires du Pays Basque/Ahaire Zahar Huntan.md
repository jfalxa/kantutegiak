---
id: ab-3456
izenburua: Ahaire Zahar Huntan
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003456.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003456.MID
youtube: null
---

Ahaire zahar huntan bi berset berririk
Alagrantziareki khantatü nahi tit;
Bihotza libratürik phena orotarik,
Desir nian maitia beitüt gogatürik.

Enükian sinhetsi, ezpeitzin üdüri,
Gogatüren niala maitia jagoiti.
Mintzatzen nintzirozün ahalaz eijerki,
Ene errana gatik hura ezin kumberti.

Zazpi urthe badizü, ene khariua,
Zütan ezarri nila ene amodiua;
Gerzoti anbilazü tristerik gogua,
Beldürrez etzizadan zü goga, gachua.

- Zazpi urthez zitzaitzat ebili ondoti,
Ene trompatü nahiz malezian bethi.
Orai egina gatik aisa eni erri,
Begiraizü Jinkuak zü etzitzan püni.

- Zelüko Jinko Jaonak badü pietate;
Bere denbora dizü errota egile;
Ürgüllütsian bortchaz ümilierazle;
Feit horrez orhit zite, maitia, zü ere.

- Ühaiña ihizlari arraiñ hurin deno,
Gizon gaztiak tendre amoros direno;
Neskatiler ordian doloruski mintzo:
Desirra kumplit eta haientzat adio.

- Pikantki mintzo zira gizoner, maitia,
Ez, sunja bazeneza düzün estatia;
Ez palimbanü maite fidelitatia,
Elitzeikezü kumbeni hola mintzatzia.

- ...

- Zaldi churibat badit zure zerbütchüko,
Zük plazer düzünian biak juaiteko.
Etchekuer errezü dolorez adio,
Ützüliren etzira mentüraz haboro.