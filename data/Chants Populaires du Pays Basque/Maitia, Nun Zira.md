---
id: ab-3412
izenburua: Maitia, Nun Zira?
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003412.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003412.MID
youtube: null
---

Maitia, nun zira?
Nik etzütüt ikhusten,
Ez berririk jakiten,
Nurat galdü zira?
Ala khambiatü da zure deseiña?
Hitz eman zenereitan,
Ez behin, bai berritan,
Enia zinela.

- Ohikua nüzü;
Enüzü khambiatü,
Bihotzian beinin hartü,
Eta zü maithatü.
Aita jeloskor batek dizü kausatü:
Zure ikhustetik,
Gehiago mintzatzetik
Har'k nizü pribatü

- Aita jeloskorra!
Zük alhaba igorri,
Arauz ene ihesi,
Komentü hartara!
Ar'eta ez ahal da sarthüren serora:
Fede bedera dügü,
Alkharri eman tügü,
Gaiza segürra da.

- Zamariz igañik,
Jin zazkit ikhustera,
Ene konsolatzera,
Aitaren ichilik.
Hogei ta laur urthe bazitit betherik:
Urthe baten bürian,
Nik eztiket ordian
Aitaren acholik.

Alhaba diener
Erranen dit orori:
So'gidaziet eni,
Beha en'erraner;
Gaztetto direlarik untsa diziplina:
Handitü direnian,
Berant date ordian,
Nik badakit untsa.