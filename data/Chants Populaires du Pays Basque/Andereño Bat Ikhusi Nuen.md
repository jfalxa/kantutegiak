---
id: ab-3451
izenburua: Andereño Bat Ikhusi Nuen
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003451.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003451.MID
youtube: null
---

(Basa-Nabartarrez)
Andereño bat ikhusi nuen igande goizño batez;
Bista berian agradatu nintzan haren begiez;
Ezdut uste baduela parerik eztitasunez:
Ene bihotza tristerik dago harekin izan beharrez.

- Anhitz maite baduzula fama hedatu zauzu;
Egun guziez maite berri bat egiten omen duzu;
Horiek hola balin badira, nor fidaturen zauzu?
Khambiatu behar duzu ni nahi balin banuzu.

- Baduzuia dudarik zu enetuz geroztik,
Nahi ere banukeela zutaz bertze maiterik?
Ezdut uzte balaitekela mundu huntan gizonik
Kontent bizi laitekeenik zu kitatuz geroztik.

- Mihian dutzun elhe ederrak bihotzian bazintu
Menturaz zure maithatzia erresoli noikezu;
Bainan gero desplazerrik eman gogo baduzu,
Othoizen zaitut hemen berian lurrez estal nezazu.