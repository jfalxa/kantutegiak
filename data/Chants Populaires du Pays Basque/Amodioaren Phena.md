---
id: ab-3438
izenburua: Amodioaren Phena
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003438.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003438.MID
youtube: null
---

(Basa-Nabartarrez)
Amodioaren phena, oi! phena handia!
Orai ezagutzen dut zer den haren phena;
Amodioa ez balitz den bezain krudela,
Ez nezakezu erran maite zaitudala.

Munduan zembat urhats, oi! dudan egiten!
Ez ahal dira oro alferrak izanen;
Jendek errana gatik guretako elhe,
Maitia, trufa nainte zu bazintut neure.

Zeruan zembat izar, maitia, ahal da?
Zure parerik ene begietan ez da;
Neke da phartitzia, maitia, enetzat;
Adio erraiten dautzut dembora batentzat.

Nik errana gatik, maitia, adio,
Ez nezazula ukhan zuk, othoi, hastio,
Bainan bai bihotzetik izan amodio:
Etzaitut kitaturen thumban sar artio.

Nik badut maiteño bat, oi! hura nolako!
Ez da ttipi ez handi, bai bien arteko;
Begia du ederra oro amodio,
Bihotzian sarthu zaut ezbaitzaut jelgiko.