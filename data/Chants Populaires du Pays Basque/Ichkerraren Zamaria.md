---
id: ab-3454
izenburua: Ichkerraren Zamaria
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003454.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003454.MID
youtube: null
---

(Ziberutarrez)
Ichkerraren zamaria
Bilho urdintzen hasia;
Zankho-besuak gogor eta bizkarrian zauria,
Bai eta istant güziez lürrilat erorlia.

Ichkerra sal'zak zaldia,
Balin badük eroslia;
Horrekin oihanilat juaitia, oi! eztük ez zühürtzia;
Belek eta arranuek horrenganat die lehia.

Ichkerraren zaldi hori
Establian charmagarri;
Nik ja nahiago nikek ardüra huñez ebili:
Zaldariz aserik ere batere eztaiteke igi.

Ichkerrak zaldia pherestatü,
Eni hartzia dolütü;
Belek eta arranuek bidian naie atakatü:
Jinkuak daki nula nizan jaon hetarik libratü.

Etziok cheha arthorik
Ahuan ezpeitü armarik;
Harentako ezta jiten Españatik olhorik;
Ichkerrak hartüz geroztik eztaki asiaren berririk.

Ichkerra, enaintek mintza,
Zamaria balü bichta;
Nurat nahi juanik ere eror eta ützülarrika
Aitzin gibelez ez kuka, sal'zak fite edo trüka.

- Eztiat, ez, nahi saldü,
Ez eta ere trükatü;
Ene zamariak behar dik üda bortian phasatü
Hantik jin datekinian nahi hait untsas trüfatü.

- Ichekerra, ago ichilik,
Eztük eginen trüfarik:
Hire zaldiak eztik janen üdan bortian belharrik;
Arte huntan eginen diek arranuek ase ederrik.

Kauter batek Ichkerrari
Eihera bortan berari:
Zamariaren so'egin zerola, hartü zinian, hobeki.

Ichkerra, othoi, pharkatü,
Etzaizü behar gaitzitu;
Alhabaizüna dela kaûsa koblatto hoik ükhen tüzü;
Etchenko berriak kampuan oro salhatzen dereitzü.