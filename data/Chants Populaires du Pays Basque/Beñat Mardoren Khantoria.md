---
id: ab-3459
izenburua: Beñat Mardoren Khantoria
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003459.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003459.MID
youtube: null
---

(Ziberutarrez)
Ni deitzen nük Beñat Mardo, Barkoche Bachabilako:
Othoitzen hait eztizadala khantorerik hunt haboro;
Eztakika ni nizala bürüzagi hartako?

- Kantore huntzale hizalakoz düka hartzen banitate?
Aprendizik etzaik ez jinen Gaiñeratik batere;
Bachabilako laurentako nihaur enük ahalke.

- Kobla horren emailia, ezta laboraria;
Zumbait markis, edo kunte, edo kardinalia!
Bürdüñarik dianian zintzarri egilia.

- Zintzarri egile nizala ehadila ez düda:
Ar' eta ehait interrumpitü izigarri ardüra
Bürdüñaren cherkhara jinik hire magasiala.

- Hitzazgütük estunatzen, Museñako kuntia,
Zerentako hartzen dian Gaiñetarren althia:
Ar' eta nik ere eztiat hiretako antsia.

Kunte, markis, kardinale ezin izan gintake;
Hobe diagü bai egonik hartü gabe banitate,
Hi tallür hizan bezala ni zintzarri egile.