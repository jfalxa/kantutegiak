---
id: ab-3429
izenburua: Mendian Zoinen Eder
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003429.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003429.MID
youtube: null
---

(Basa-Nabartarrez)
Mendian zoinen eder epher chango gorri!
Ene maitiak ere bertziak iduri;
Eni hitzeman eta gibelaz itzuli.

Ene bihotza duzu zu ganat erori,
Eta zuria aldiz harria iduri:
Ene begi gaichuak nigarrez ithurri.

(Ziberutarrez)
Oherat ziradia, lozale pollita?
Oherat ezpazira jin zazkit leihora,
Hitzñobat erran eta banua berhala.

- Gora düzü leizarra, gorago izarra;
Oran' eztiz' oren bat ohera nizala:
Ohetik jeikitzera herabe düdala.

- Bortü goretan eder epher zankho gorri;
Ene bihotza düzü züganat injogi,
Eta zuria aldiz botchia üdüri.

- Oreiña laster dua horen aitzinian,
Hurian sartzen düzü ahal dianian;
Ez amorioz bena bai bere beharrez:
Zü ere hala hala zabiltza arauez.