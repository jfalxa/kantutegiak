---
id: ab-3418
izenburua: Prima Eijerra
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003418.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003418.MID
youtube: null
---

Prima eijerra,
Zütan fidatürik
Hanitch bagira,
Oro trompatürik.
Enia zirenez erradazü bai al'ez,
Bestela
Banua
Desertiala nigarrez hurtzera.

- Desertiala
Juan nahi bazira,
Arren zuaza,
Oi! bena berhala!
Etzitiala jin harzara nigana,
Bestela
Gogua
Dolütüren zaizü, amoros gachua!

- Nitan etsemplü
Nahi dianak hartü,
Ene malürrak
Parerik ezpeitü.
Charmagarri bat nik nian maithatü,
Fidatü,
Trompatü;
Seküla jagoiti ikhusi ezpanü!

- Mintzo zirade
Arrazu gabetarik
Eztüdala nik
Zur'amodiorik;
Zü beno lehenik banian besterik
Maiterik,
Fidelik;
Horrez eztereizüt egiten ogenik.