---
id: ab-3452
izenburua: Belhaudiko Bortian
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003452.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003452.MID
youtube: null
---

(Ziberutarrez)
Belhaudiko bortian Orgambidesk'olha,
Bere deskantsiala han ardiak alha:
Goizan igorten tie olhape behera,
Artzan-hor gaizua emanik gida,
Arratsan ützültzian gomendatürik untsa.

Artzaiñ hurak zütien ardiek trumpatü;
Goiz batez juan eta arratsan ez sarthü.
Merkhatzale ziradin hurak abiatü,
Ilhiak zütiela behar ingajatü
Eta jarraikile bat hunik akordatü.

Otsogorrin behera ardiek, lasterka,
Zalhe igaran zien Gaztambid' ühaitza;
Besarkagiala baikoz barachtü ürhatsa,
Bide handin bathürik Arhaneko tropa,
Han ziren artzaiñeki jarri mintzatzera.

Artzaiñek mehatchüreki galthatü ardier
Nurat juaiten ziren ihesi nausier;
Haiek arrapostia, ezta hambat eijer:
"Berrien iruitera baguatza Pettarrer;
"Ahatzerik girela gure aitzaiñ jauner.

Artzaiñ horiek zien ardier galthatü
Eia etzienez ihur bidin bathü:
"Tirro, Elgoihen eta Odoronda tügü
"Gure igaraitian erriz borogatü,
"Ustez otsuek behar zien gützaz aihaltü.

Artzaiñ horiek ardier: "Hots Arhanera,
"Izanen zidie untsa, guriak bezala."
Ardiek arrapostua: "Zietzaz kuntent gira,
"Artzaiñ auherreki üsatürik gira,
Entzünik ziek ere hala ziradiela..

Artzaiñek zeren erran juaitiereki
Eia etzienez bathü Felipe Arainti:
"Bai, bathü ükhen dügü Petti Üharteki;
"Biek igaraitera beikütie ützi,
"Ari baitzen eüria, nahi gabez busti..

Khantoren egiliak eztizü beldürrik
Ez bortian, ez etchen, gal dezan ardirik;
Batño bat beitzian arras ñaphürtürik
Bestereniala biziatürik,
Kuntentik diagozü hura kitatürik.