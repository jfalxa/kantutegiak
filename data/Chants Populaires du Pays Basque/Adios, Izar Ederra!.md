---
id: ab-3435
izenburua: Adios, Izar Ederra!
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003435.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003435.MID
youtube: null
---

Adios, izar ederra, adios izarra!
Zu zare aingerua munduan bakharra!
Aingeruekin, (bis) zaitut komparatzen,
Zembat maite zaitudan ez duzu phensatzen!

Adios, izar ederra, eta khariua,
Neure begietako lili arrarua!
Bihotzez zurekin eta gophutzez banua,
Jarrikiren zautazut zur'amodiua.
Izan naiz Araguan eta Kastilloan,
Hitz batez erraiteko España guzian;
Ez dut ikhusi (bis) zu bezalakorik,
Nafarroa guzian zaude famaturik.

Jarrikitzen ninduzun izar eder hari
Nola mariñel ona bere orratzari.
Jende onak atentzione ene arrazoin huni:
Etzieztela fida amodioari!

Amodioa duzu arrosaren pare
Usaina badu eta ondoan arhantze;
Maitia, ni enainte egon zu gana jin gabe
Hil behar banu ere hirur egun gabe.