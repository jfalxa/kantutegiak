---
id: ab-3419
izenburua: Choriñuak Kaloian
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003419.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003419.MID
youtube: null
---

Choriñuak kaloian
Tristerik dü khantatzen;
Dialarik han
Zer jan, zer edan,
Kampua desiratzen,
Zeren, zeren
Libertatia zuiñen eder den!

Kampoko choria
So'giok kaloiari;
Ahal balin bahedi
Hartarik begir'adi,
Zeren, zeren
Libertatia zuiñen eder den!

Barda amets egin dit
Maitia ikhusirik:
Ikhus eta ezin mintza
Ezta phena andia
Eta ezin bestia?
Desiratzen nüke hiltzia.