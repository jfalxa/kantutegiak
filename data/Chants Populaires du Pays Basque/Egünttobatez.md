---
id: ab-3421
izenburua: Egünttobatez
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003421.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003421.MID
youtube: null
---

Egünttobatez nindaguelarik maitenareki leihuan,
Erran ükhen niriozün hura niala goguan,
Ene phena doloretzaz pietate har lezan.

- Zure phena doloretzaz pietate badit nik;
Ene khorpitz tristiareki eztirot eman plazerik;
Zelin promes emanik nago Jinkoari lehenik.

- Oro eijer, oro pollit, zü zira, ene maitia;
Zure eskütik nahi nikezü bizi nizano ogia,
Eta gero ni nükezü zure zerbütcharia.

- Eniz, ez, ni haiñ eijerra, nulaz erraiten deitazü?
Mündü huntako eijerrena berthütia lükezü;
Hari ogenik egin gabe, othoi, maitha nezazü.

- Baratzian zuiñen eijer jirofleia loratü!
Aspaldian desir niana orai dizüt gogatü;
Hura gogatü eztüdano gaiik eztizüt mankatü.

- Baratzian eijer deia jirofleia loratü?
Aspaldian desir züniana orai düzia gogatü?
Zerbütchatü zirenian berria khunta ezazü.

- Ene maite bihotz gogor, ezpiritü zorrotza;
Orai dizüt orai ikhusten etzitzakedala goga;
Amodiua ützi eta indarrez dügün boroga.

Jaon gaztia, othoi, pharka, haur gazte bat ni nüzü:
Zuri arrapostü emaitez debeiatürik nüzü;
Errandelako indartto hura bestetan empleg'ezazü.