---
id: ab-3425
izenburua: Zü Zira, Zü
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003425.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003425.MID
youtube: null
---

Zü zira, zü, ekhiaren paria,
Liliaren floria,
Eta mirail ezinago garbia!
Ikhusirik zure begithartia
Elizateke posible, maitia,
Düdan pazentzia,
Hambat zirade glorifikagarria!

Oi! krüdela, erierazi naizü,
ütsüerazi naizü,
Ülhümpian eradükiten naizü!
Argitüren dela erraiten deitadazü;
Dembora da aitzina juaiten,
Sekül'ez argitzen,
Üdüritzen zait naizüla abundenatzen.

Sarthüz geroz behin bihotzian barna,
Zerk dereizü phena,
Charmagarri ezinago zirena?
Zük diozü nik düdala ogena:
Detzagün beraz kausak kontsidera
Nik badüt ogena,
Eginen düzü nitzaz plazer düzüna.

Oi! Maitia, trüfatzen zira naski
Üdüritzen zait klarki
Amodiorik eztüzüla eneki.
- Amodio ükheitia ezta aski;
Behar tüzü bestiak oro ützi
Bai eni jarraiki,
Amodio perfeit bat nik diket zureki.