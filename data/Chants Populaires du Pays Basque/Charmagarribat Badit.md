---
id: ab-3427
izenburua: Charmagarribat Badit
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003427.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003427.MID
youtube: null
---

Charmagarribat badit maite bihotzeti,
Amodiotan gira biak alkharreki;
Haren aire charmantaz agrada niz bethi:
Parerik badiala ezpeitzeit üdüri.

Zertako erraiten düzü maite naizüla ni?
Khorte egiten zaude zü beste orori.
Enezazüla trompa, mintza zite klarki;
Esperantza falsürik ez eman nihuri.

Chori papgorriak eijerki khantatzen.
Gaiazko alojia kanpuan cherkhatzen;
Ni ere gisa berian nüzü erdireiten,
Maitiak ezpadereit bortha idekiten.

Orai zireia jiten, gaiherdi onduan?
Iratzarririk nintzan eta zü goguan;
Zure botza entzüten düdanin khampuan,
Ohetik jalki eta jartzen niz leihuan.

- Oi! ene bihotzeko lili haitatia,
Ene botza entzünik leihuan zaudia?
Aspaldin enereizün ikhusi begia:
Barnerat sar nadin indazüt eskia.

Bortürik gorenetan erortzen elhürra:
Tronpatüren naizüla badizüt beldürra;
Hargatik nahi züntüket ikhusi ardüra,
Eginen badüt ere, oi! ene malurra!

Lümarik ederrena pabuak büztanin:
Maitia, etzüntüdan ikhusi aspaldin.
Gaiaz eta egünaz bazüntüt enekin,
Ni enainte debeia, maitia, zureki.

Ai! Ei! Ai! Ei! Ai! Eia! hau da doloria!
Bi maite üken eta ez jakin zuiñ haita;
Batak dizü chapela, bestiak boneta:
Chapeldüntto hura da, oi! ene bihotza.