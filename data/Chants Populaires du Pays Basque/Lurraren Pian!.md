---
id: ab-3447
izenburua: Lurraren Pian!
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003447.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003447.MID
youtube: null
---

(Basa-Nabartarrez)
Lurraren pian sar nindaiteke, maitia, zure ahalgez!
Bost phensaketa eginik nago zurekin ezkondu beharrez;
Bortha barnetik zerratu eta bethi khanberan nigarrez,
Sendimenduiak airian eta bihotzetikan dolorez...
Ene changrinez hilerazteko sothuia zinez arauez!

Oren hunian sorthuia zinen izar ororen izarra!
Zure parerik etzaut jiten neure begien bistara.
Espos laguntzat gald'egin zintudan erran nerauzun bezala:
Bainan zuri ez iduritu zuretzat aski nintzala;
Ni baino hobebatekila Jainkoak gertha zitzala!

Mariñelak juaiten dira itsasorat untziko:
Zure ganako amodioa sekulan ezdut utziko.
Charmagarria, nahiz ez giren elgarrekilan biziko,
Behin maite izan zaitut eta etzaitut hastiatuko:
Bihotzian sarthu zitzautat eternitate guziko.

Primeberan zoinen eder den choria khantuz phaguan!
Amodiuak ibili un, maitia, zure onduan;
Deusetan ere etzaitut nahi bortchatu amodiuan:
Changri huntarik hiltzen banaiz satifazaite goguan,
Malerusik aski izanen naiz nihaur bakharrik munduan.