---
id: ab-3415
izenburua: Jeiki, Jeiki
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003415.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003415.MID
youtube: null
---

Jeiki, jeiki etchenkuak, argia da zabala;
Itchasoti mintzatzen da zilharrezko trompeta;
Bai eta're ikharatzen Olandresen ibarra.