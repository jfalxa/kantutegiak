---
id: ab-3458
izenburua: Chori Erresiñula,
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003458.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003458.MID
youtube: null
---

(Ziberutarrez)
Chori erresiñula, hots, emak eneki,
Maitenaren borthala biak alkharreki;
Botz ezti batez izok deklara segretki
Haren adichkide bat badela hireki.

Heltü ginenian maitenaren borthala,
Horak hasi zeizkün champhaz berhala,
Laster egin günian bertan gordatzera;
Erresiñula igaiñ zühaiñ batetara.

- Nur da edo zer da? maitenak leihoti.
- Adichkidiak gira, ziaude beheiti;
Eta bortha ideki emeki emeki
Mintza ahal zitzadan ahalaz segretki.

- Nur da edo zer da? Nunko zirade zü?
- Etche ondorik eztit, pharka izadazü;
Egarri handi batek hartürik niagozü:
Üthürri hun bat nun den, othoi, erradazü.

- Zure egarriaz ezta miraküllü,
Egünko egünian bero egiten dü.
Üthürri hun bat hortche bathüren beitüzü,
Zük galthatzen düzüna gure behar dügü.