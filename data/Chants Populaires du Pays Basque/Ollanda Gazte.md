---
id: ab-3441
izenburua: Ollanda Gazte
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003441.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003441.MID
youtube: null
---

(Basa-Nabartarrez)
Ollanda gazte pollitño bat gure herrian badugu;
Hegal petik lumaño bat falta ezpalimbalu
Munduian aski elizateke ollanda hartaz espantu.