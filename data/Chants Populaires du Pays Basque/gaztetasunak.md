---
id: ab-3420
izenburua: Gaztetasunak
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003420.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003420.MID
youtube: null
---

Gaztetasunak bainerabila airean ainhara bezala,
Gauak phasatzen ditut ardura egunak balire bezala,
Oi!
Ardura nabila maitia gana!

- Maite nauzula zuk erraiteaz ni eznaiz alegeratzen;
Baizikan ere nere bihotza arras duzu tristetzen,
Oi!
Zeren ez nauzun kitatzen!

- Amodiorik badudala ez zerauzuia bada iduri?
Itsasoa phasa niro zure gatik igeri,
Oi!
Zeren zaren hain charmagarri!

- Charmagarria banaiz ere ez nainteke izan zure:
Nitaz agrada direnik bertzerik munduan baitire,
Oi!
Ni ganik urrun zaite.