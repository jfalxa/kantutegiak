---
id: ab-3434
izenburua: Goizian Goizik Jeiki Nündüzün
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003434.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003434.MID
youtube: null
---

Goizian goizik jeiki nündüzün, espusa nintzan goizian;
Bai eta zetaz ere beztitü ekhia jelkhi zenian;
Etchek'andere zabal nündüzün egüerdi erditan;
Bai eta ere alhargüntsa gazte ekhia sarthü zenian.

Musde Irigarai, ene jaona, altcha izadazüt büria;
Ala dolütü othe zaizü eneki espusatzia?
- Ez, ez, etzitadazü dolütü zureki espusatzia,
Ez eta ere dol.türen bizi nizano lürrian.

Nik banizün maitetto bat mündü ororen ichilik,
Mündü ororen ichilik eta Jinko jaonari ageririk;
Buket bat igorri ditadazüt lili arraroz eginik,
Lili arraroz eginik eta erdia phozuatürik.

- Zazpi urthez etcheki dizüt gizon hila khamberan;
Egünaz lür hotzian eta gaiaz bi besuen artian,
Zitru hurez ükhüzten nizün astian egün batian,
Astian egün batian eta ostirale goizian.