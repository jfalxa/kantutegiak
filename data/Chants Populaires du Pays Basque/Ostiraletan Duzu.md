---
id: ab-3461
izenburua: Ostiraletan Duzu
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003461.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003461.MID
youtube: null
---

(Basa-Nabartarrez)
Ostiraletan duzu Garruzen merkhatu:
Ene maite pollita han nuen khausitu;
Potbat geld' egin neron chapela eskian,
Biga emen zautadan, nigarra begian.

Gero gald' egin neron. "Nigarrez zer duzu?
"Hola changrinatzeko sujetik ez duzu."
Arrapostu eman zautan: "Zuhaurrek badakizu;
"Ene nigar ororen sujeta zira zu.".