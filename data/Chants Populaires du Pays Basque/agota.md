---
id: ab-3432
izenburua: Agota
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003432.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003432.MID
youtube: null
---

Argi azkorrian jinik ene erresekila,
Bethi beha entzün nahiz numbaitik zure botza;
Ardiak nun ützi tüzü? Zerentako errada
Nigarrez ikhusten deizüt zure begi ederra?

- Ene aitaren ichilik jin nüzü zure gana,
Bihotza erdiratürik, zihauri erraitera,
Khambiatü dietadala ardien alhagia,
Sekülakoz defendatü zureki mintzatzia?

- Gor niza, ala entzün düt? Erran deitadazia?
Sekülakoz jin zaiztala adio erraitera?
Etziradia orhoitzen gük hitz eman dügüla
Lürrian bizi gireno alkharren maithatzia.

- Atzo nurbait izan düzü ene ait'ametara,
Gük alkhar maite dügüla haien abertitzera;
Hürüntaaztez alkhar ganik fite ditin lehia
Eta eztitian jünta kasta Agotarekila.

- Agotak badiadila badizüt entzütia;
Zük erraiten deitadazüt ni ere banizala:
Egündano ükhen banü demendren leiñhüria
Enündüzün ausartüren begila so'gitera.

- Jentetan den ederrena ümen düzü Agota:
Bilho holli, larrü churi eta begi ñabarra.
Nik ikhusi artzaiñetan zü zira ederrena:
Eder izateko aments Agot izan behar da?

- So'izü nuntik ezagützen dien zuiñ den Agotan:
Lehen sua egiten zaio hari beharriala;
Bata handiago dizü eta aldiz bestia
Biribil et'orotarik bilhoz üngüratia.

- Hori hala balimbada haietarik etzira,
Ezi zure beharriak alkar üduri dira.
Agot denak chipiago badü beharri bata,
Aitari erranen diot biak bardin tüzüla.