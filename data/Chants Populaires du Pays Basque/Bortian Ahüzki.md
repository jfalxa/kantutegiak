---
id: ab-3417
izenburua: Bortian Ahüzki
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003417.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003417.MID
youtube: null
---

Bortian ahüzki, hur hunak osoki;
Neskatila eijerrak han dira ageri;
Hirur badirade, oi! bena charmantik,
Basa-Nabarr'orotan ez düte parerik.

Neskatila eijerra, oi begi ñabarra,
Nuntik jin izan zira bortü gaiñ huntara?
Garaziko aldetik desir nian bezala,
Ahüzkik'üthürrila hur fresken hartzera?

Goizetan eder dizü ekhiak leiñhürü,
Mündi'argitzen dizü üngürü üngürü;
Ni ere zur'unduan hala nabilazü:
Eia maite naizünez, othoi, erradazü!

- Maithatü züntüdala badizü aspaldi,
Ezpenereizün erran orano zühauri:
Bihotzian edüki phena handireki,
Zühauri erraitera oi! ezin atrebi!

- Disimüla ezazü ahalaz lüzazki,
Maite ükhenen zütüt nik har gatik bethi;
Maite ükhenen zütüt nik har gatik bethi,
Gero izanen gira biak alkharreki.

Adio Ahüzki eta Nabolegi,
Dolü egiten deiziet Lakhasorhoreki;
Goraintzi erran izozie Ziprian Phinori,
Mil'esker derogüla haren karesari.