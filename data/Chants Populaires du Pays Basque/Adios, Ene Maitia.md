---
id: ab-3455
izenburua: Adios, Ene Maitia
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003455.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003455.MID
youtube: null
---

Adios, ene maitia, adios sekülako!
Nik eztit beste phenarik, maitia , zuretako,
Zeren üzten zütüdan haiñ libro bestentako!

- Zertako erraiten düzü adio sekülako?
Uste düzia nik eztüdala amodio zuretako?
Zük nahi balin banaizü enükezü bestentako.