---
id: ab-3430
izenburua: Chorittua, Nurat Hua?
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003430.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003430.MID
youtube: null
---

Chorittua, nurat hua bi hegalez airian?
Españalat juaiteko elhürra dük bortian:
Juanen gütük alkharreki hura hurtü denian.

San-Josefen ermita desertian gora da;
Españalat juaitian han da ene phausada:
Guibelilat so' gin eta hasperena ardüra!

Hasperena, habilua maitenaren borthala:
Habil, eta erran izok nik igorten haidala;
Bihotzian sar hakio hura eni bezala.