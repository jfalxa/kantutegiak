---
id: ab-3457
izenburua: Muthil Gaztia
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003457.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003457.MID
youtube: null
---

Muthil gaztia,
Bilo horia,
Burian duka banitatia?
Uste duk bai naski
Haizu dela bethi
Gorthiaren egitia andre orori.

- Eni haizu da
Galdegitia,
Zuri kombeni begiratzia,
Hirur muthil gazte,
Zu nahiz emazte,
Beren artian disputa badute.

- Ukhan bezate,
Nahi badute;
Ene perilik haiek ez dute;
Ez nahiz eskondu,
Ez disputan sartu,
Komentu batetarat orai baniazu.

- Andre gaztia,
Gomazu trompa;
Zure gogua khanbiakor da.
Komentu guziak
Beitira bethiak
Jin zaite ni ganat, untsa izanen gira.