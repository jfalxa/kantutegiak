---
id: ab-3443
izenburua: Mila Zortzi Ehun Hemeretzian
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003443.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003443.MID
youtube: null
---

(Laphurtarrez)
Mila zortzi ehun hemeretzian,
Urriaren hillaren bederatzian,
Umore ona nuien hoien khantatzian,
Gazte eta alegera, trankil bihotzian;
Ontasuna franko badut intresian,
Deusik ez etchian,
Orai bezain aberats nintzan sortzian.

Nork nahi zer nahi erranikan ere,
Bidarraitarra nuzu bai nahi ere;
Etcheko seme ona segurki halere,
Nahiz baden herrian hobechagorik ere;
Baditut bortz haurhide, eneki sei dire,
Oro adizkide,
Dotiaren gainetik samurturen ez gire.

Aita ezkondu zen gure amarekin,
Ama aitarekin, biak elgarrekin;
Orduian gazte ziren bertze zerbaitekin,
Eta orai zahartu miseriarekin;
Ontasuna igorri bertze haurhidekin,
Phartebat enekin,
Enekin baino haboro bertze bortzekin.

Aita zen etcheko, ama kampoko,
Jainkoak egin tu elgarrekilako.
Hirur mutiko eta hirur neskatoko,
Haurrik aski badute aisa bizitzeko
Balimba bertzerik etzaie sorthuko,
Ez da fidatzeko,
Landatuz geroz hirriskatzen du phizteko.

Adinen beha gaude, egia erraiteko,
Gure ontasun ororen phartitzeko;
Diru idorra bere bada bizikichko,
Jainkoak daki zembat den bakhotcharen dako;
Izaiten badugu ez dugu utziko
Auzokuendako,
Bainan eztaikugu sakelarik erreko.

Hemen bagirade orai zembait lagun,
Botoila bana arno edan dezagun,
Ona balimbada bira edan dezagun,
Tringa dezagun, plazer har dezagun,
Bilha zembait lagun:
Diru dianak izanen dik ezagun.

Etchek' anderia, zure tristia,
Iduritzen zaizu girela diru gabiak,
Diru badugu bainan ditugu larriak;
Zor utziren dautzugu gaurko afariak;
Etchek' anderia, emazu guardia,
Har pazientzia,
Noizbait izanen duzu zure pagamenduia.