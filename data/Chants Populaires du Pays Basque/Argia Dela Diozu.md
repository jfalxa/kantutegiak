---
id: ab-3413
izenburua: Argia Dela Diozu
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003413.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003413.MID
youtube: null
---

Argia dela diozu,
Gauherdi orain' ez duzu;
Enekilako dembora luze iduritzen zauzu;
Amodiorik ez duzu, orai zaitut ezagutu.

Ofizialetan deia
Zure sinheste guzia?
Aitak eta amak ere hala dute gutizia;
Lehen bat'et'orai bertzea: oi! hau phenaren tristia!

Othea lili denean,
Choria haren gainean;
Hura juaiten airean berak plazer duenian:
Zur'eta ner'amodioa hala dabila munduian.

Phartitu nintzan herritik,
Bihotza alegerarik;
Arribatu nintzan herrian, nigarra nuen begian:
Har nezazu sahetsian, bizi naizeno munduian.