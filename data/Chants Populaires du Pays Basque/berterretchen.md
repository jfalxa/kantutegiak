---
id: ab-3437
izenburua: Berterretchen
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003437.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003437.MID
youtube: null
---

(Ziberutarrez)
Haltzak eztü bihotzik,
Ez gaztamberak ezürrik:
Enian uste erraiten ziela Aitunen semek gezürrik.

Andozeko ibarra
Ala ibar lüzia!
Hiruretan ebaki zaitan armarik gabe bihotza.

Berterretchek oheti
Neskatuari estiki:
"Abil, eta so' ginezan ageri denez gizonik.

Neskatuak berhala,
Ikhusi zian bezala:
Hirur dozena bazabilzala leiho batetik bestera.

Berterretchek leihoti
Jaon kuntiari goraintzi;
Ehün behi bazereitzola bere zezena ondoti.

Jaon kuntiak berhala
Traidore batek bezala:
"Berterretch, aigü borthala, ützüliren hiz berhala..

- "Ama, indazüt athorra,
"Mentüraz sekülakua;
"Bizi denak ohit ükhenen dü Bazko biharamena..

Mari-Santzen lasterra
Bost-Mendietan behera!
Lakharri Büztanobian sarthü da bi belhaiñak herresta.

- "Büztanobi gaztia,
"Ene anaie maitia,
"Hitzaz hunik ezpalimbada, ene semia juan da!.

- "Arreba, ago ichilik
"Ez, othoi, egin nigarrik;
"Hire semia bizi balimbada, mentüraz Mauliala da..

Mari-Santzen lasterra
Jaon kuntiaren borthala!
"Ai! ei! eta, Jaona, nun düzie ene seme galanta!.

- "Hik bahiena semerik
"Berterretchez besterik?
"Ezpeldoi altian dün hilik; abil, eraikan bizirik..

Ezpeldoiko jentiak
Ala sendimentü gabiak!
Hila haiñ hüllan ükhen eta deüsere etzakienak!

Ezpeldoiko alhaba
Margarita deitzen da;
Berterretchen odoletik ahürkaz biltzen ari da.

Ezpeldoiko bühkata
Ala bükhata ederra!
Berterretchen athorretarik hirur dozena ümen da.