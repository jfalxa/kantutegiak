---
id: ab-3414
izenburua: Chori Erresiñula
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003414.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003414.MID
youtube: null
---

Chori erresiñula üdan da khantari:
Zeren ordian beitü kampuan janhari;
Negian ezt'ageri, balimban ezta eri:
Üdan jin baledi,
Konsola nainte ni.

Chori erresiñula ororen gehien,
Bestek beno hobeki har'k beitü khantatzen;
Harek dü inganatzen mündia bai trompatzen;
Bera eztüt ikhusten,
Bai botza entzüten.

Botz haren entzün nahiz herratürik nago;
Ni hari hüllant eta hura hürrünago;
Jarraiki ninkirio bizia gal artino;
Aspaldi handian
Desir hori nian.

Choria zuñen eijer khantüz oihenian!
Nihaurek entzün dizüt igaran gaiian.
Eia! guazen, maitia, bibiak ikhustera:
Entzüten badüzü,
Charmatüren zütü.

- Amak ützi nündizün bedats azkenian;
Gerostik nabilazü hagales airian.
Gaiak urthuki nündizün sasiño batetara,
Han züzün chedera,
Oi! ene malürra!

- Choria, zaude ichilik, ez egin khantürik,
Choria, zaude ichilik, ez egin khanturik;
Eztüzü profeitürik ni hola phenatürik,
Ez eta plazerik
Ni thumban sarthürik.

- Bortiak churi dira elhür dienian,
Sasiak ere ülhün osto dienian.
Ala ni malerusa! zeren han sarthü nintzan?
Jun banintz aitzina
Ezkapatzen nintzan.

- Choria, zaude ichilik, ez egin nigarrik;
Zer profeitü dükezü hola aflijitürik?
Nic eramanen zütüt chedera lachatürik,
Ohiko bortütik,
Ororen gañetik.