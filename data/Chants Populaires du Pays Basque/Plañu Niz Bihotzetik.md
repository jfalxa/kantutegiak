---
id: ab-3431
izenburua: Plañu Niz Bihotzetik
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003431.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003431.MID
youtube: null
---

Plañu niz bihotzetik,
Gaita zer düdant eztakit,
Tristezia batek hartürik;
Enüke acholarik
Baliz erremediorik
Ene gaitza sendo ahal lironik;
Ezta mündian barberik, bat baizik
Ene gaitza zartarik den ezagützen dianik,
Eta hura berantetsirik
Baniagozü, gachua, tristerik!

- Berantetsi nündüzün
Segür beldürra banizün;
Bena ene khointak behar tüzü entzün:
Zük uste gabetarik
Zützaz ohart nündüzün;
Zure mina pharte eneki züzün;
Zure ganat jiteko heraberik enizün;
Bena beste manerarik arte hortan bazüzün;
Aspaldian gure etchian
Khasü horrez asarra güntüzün.

- Jin bazira, jin zira,
Segür hunki jin zirela!
Erraiten deizüt berantetsi züntüdala;
Nun egongo zira hola
Horrembeste dembora,
Jin gabetarik ene ikhustera?
Eztüzula acholik balikezü zumbait marka,
Orai artino etzirenian jin ene kontsolatzera!
Jin zite ardüra ardüra
Phenaerazi gabe ni hola.