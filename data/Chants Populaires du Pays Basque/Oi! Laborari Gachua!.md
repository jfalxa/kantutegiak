---
id: ab-3433
izenburua: Oi! Laborari Gachua!
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003433.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003433.MID
youtube: null
---

Oi! laborari gachua!
Hihaurek jaten arthua:
Ogi eta ardu geñhatzen auherren asetzekua:
Halere haiñ haie maite nula artzaiñek otsua.

Artzaina bada beztitzen
Josliak tü gomendatzen
Zaragollen alderdi bata oihal hobez ezar dezen:
Halere higatüren dizü aitzinia beno lehen.

Dendaria berant jiten,
Arratsan goizik ützültzen,
Eta mündiaren jorratzen arte hartan abüsatzen:
Ezpeitü jaten diana hüllantzeko irabazten.

Orai ürüliak oro
Idorrian nahiago;
Hanitch lan agertü beharrez hari ore chori-lepho:
Hallikatzen balimbada zehian laur oropilo.

Ehülia hari galtho,
Ükhnenik ere han franko;
Undar harien ebatsi nahiz oihala üzten zerratzeko,
Fornizaliri erraiteko hariak zütila gaisto.

Harri' giliren adreta!
Harek badaki zer phentsa;
Mürria gaizki egin eta erdiruak lohiz thapa;
Etchia lurtatü denian, harrien gaistuak falta.

Menüser, maiastüriak,
Oi! lan güti egiliak!
Dena gaizki egin eta zurak dü estaküria:
Phasta zela adar-ondozü edo beta-bühürria.

Eskalampu egiliak
Ebasten tü materiak;
Zazpira sos balio dina saltzen beitü hamabia:
Ostatin jan hen saria eta etchen gosez familia.

Bigner batek bestiari
Estakürü eman nahi:
Zuñi bere kopadüra hobe beitzaio üdüri;
Aihen gabe nahi denak hura beza üsü berri.

Oi! Taharnari fidela!
Jüje ezpaliz igela!
Arraiñak jakile har eta hek litzakie kundena,
Haien etche lejitima guri saltzen deikiela.

Errejentbat bada hiltzen,
Har'k eztü prosesik üzten:
Huntarzünak beitütü harek heiñ hun batetan ezarten:
Züntzürrin kuntrolatü eta sabelin ipotekatzen.

Jaun aphezek etsortatzen
Karitate egin dezen
Berek aldiz phakatü gabe hitz bat eztie erraiten,
Herriko praubiak gosez eta haien ürhik ardollatzen.

Ilhaginak aberasten,
Arimak haiñ untsa galtzen;
Phezian eta khuntietan zer eztie hek ebasten!
Haiekila behar düke Jinkoak aizina ükhen.

Kinkillarien suiñ hütsak
Borthaz borthaz dabiltza;
Haien prenda ordinaria ichkilimb' eta ligeta;
Astin emazter ebatsiak igantia gero jokha.

Oihenzaiñak eta guardak
Kontzentziazko gizonak!
Gerak bazaitze farzitzen ihesiren tie postak;
Lagüner hareraziren bardin gaizo sinheskorrak.

Sarjanten goldenabarrak
Dirade gizon okherrak;
Haien egitekuetarik egiten tie Indiak:
Aisa pergüt izanen dira haier behatzen direnak.

Sarjant eta notariak,
Oi! arnes nesesariak!
Haien elhe ülhün gezürrek nahasten gaiza tchipiak;
Ezta lagün hobiagorik bertan hüsteko etchiak.