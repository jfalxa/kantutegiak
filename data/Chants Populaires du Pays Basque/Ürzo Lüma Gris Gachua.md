---
id: ab-3436
izenburua: Ürzo Lüma Gris Gachua
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003436.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003436.MID
youtube: null
---

Ürzo lüma gris gachua,
Ore bidajin bahua;
Baratzen bazaik musde Sarri,
joan apetitü hun hura,
Begiz ikhusten balin bahai Phetiriñalat bahua.

Ürzo gachuak ümilki
Diozü musde Sarriri:
Egündaino eztereiola ogenik egin jaon hari,
Ützi dezan igaraitera üsatü dian bideti.

- Auher dük, auher, ürzua,
Jüratü diat fedia!
Aurthen aurthen jin behar dük eneki Phitiriñala;
Han nik emanen dereiat arth' eta zahiz asia.

- Arthoz asia hun düzü
Libertatia bagünü;
Orhiko bago ezkürtto hurak guri hobeche zizkützü,
Anglesa Frantzian sartzen bada Españalat baguatzü.

- Ürzua, ago ichilik,
Frantzian eztük Anglesik;
Baiunara jiten badira Agaramuntek hilen tik;
Phetiriñalat eztük jinen zaragolla lüzetarik.

- Fida niz zure erraner,
Fidago ene hegaler;
Goraintzi erran behar derezü jiten badira Angleser,
Nik ere ber gisan erranen diet Español papo gorrier.

- Goraintzi erraile Angleser
Ni naika ezarten mezüler?
Orai diat orai ikhusten nitzaz hizala trüfatzen,
Ar' eta enaik beste urthe batez bortian freskeraziren.

- Jaona, zuaza etcherat,
Mauletik Phetiriñalat;
Chori eta bilhagarro gizen zumbaiten jatera,
Ürzo hegal azkartto hoiek ezkira zure bianda.