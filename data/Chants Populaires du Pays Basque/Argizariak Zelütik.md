---
id: ab-3428
izenburua: Argizariak Zelütik
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003428.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003428.MID
youtube: null
---

Argizariak zelütik
Argitzen dizü eijerki;
Ene maite pollita eztüzü ageri
Zelüko Jinko Jaona! zer eginen düt nik
Zer eginen düt nik.

- Fiatik batere eztüzü,
Mündia erriz ari zaizü;
Bathü orotzaz agrada zira zü,
Bat har ezazü, hura aski dükezü,
Horrez segür nüzü.

Ürso-aphalaren malürra
Galdüz geroztik laguna!
Triste da bethi bere bihotzetik,
Zeren ezpeitü maithatü bat baizik
Maithatü bat baizik.

Amodio berriak
Sendotzen tizü eriak;
Zure begiak haiñ dira eztiak
Zeren beitira eniak zuriak,
Zuriak eniak.