---
id: ab-3440
izenburua: Ezpeleta Herrian
kantutegia: Chants Populaires Du Pays Basque
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003440.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003440.MID
youtube: null
---

(Laphurtarrez)
Ezpeleta herrian, ostatu batian,
Gerthatu izan naiz bazkari batian,
Uste gabian, laur lagunetan;
Etchek' anderia bere lanetan,
Espantuia franko bere salsetan,
Ageriko da gero obretan.

Etchek' anderia goizik jeiki zen;
Zortzi orenetan suia hila zen,
Haragi' ere bucherian zen,;
Hamek' orenetan garburen sartzen,
Eguerditan bazkaria prest zen;
Manera onaz zopa on bat zen.

Zopa jan eta haragi ori
Ekharri dauku egosi berri,
Be'iratu eta egin neron irri;
Pochi pochi bat eman zautan niri,
Zer jenero zen etzen ageri,
Mamia guti, hezurra handi.

Arnoa zuten Bordalekotik,
Edo bertzela taula zokhotik,
Agollatuia ongi uretik;
Edan dezagun beraz gogotik,
Horditziaren beldurra gatik,
Horrek ez gaitik joko burutik.

Zalhu dabila zerbitzaria,
Salsa bat badila egin berria,
Nahi dugunez haren erdia;
Egin dezagun beraz guardia,
Salsa eztela egun berria,
Bezperakua, mintzen hasia.

Galdetzen derogu zerbitzaria
Ekhar dezagun erraki hori
Ongi errerik, gizenian guri,
Be'iratu eta egin neron irri,
Zikhiruaren fama tzarrari;
Mehe zen bainan etzen ernari.

Kafe on batek oro ahazten:
Galdetu orduko berehala prest zen,
Ura doidoia pegarrian zen;
Hasi zen beraz noizbeit ekhartzen
Ura doidoia hacia beisten:
Ttikera erdibat phurruska bazen.