---
id: ab-4189
izenburua: Marijesiak (VI)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004189.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004189.MID
youtube: null
---

Lazaretako tenplu santuan
dontzeila eder bar jarririk
izena bere Maria deuko
graziaz ordenadurik.

Ate-bentanak guztiek bere
barrutik deukoz itxirik
barrutik deukoz itxirik eta
inoz sartu ezteiten gizonik.

- Ez, ez plantadu dontzella ederra,
emen eztator gizonik,
Aite eternoak bidalduta dator
San Grabiel Zeruetarik.

- San Grabiel zeruetarik
enbajada zerori,
Jesusen ama izin iteko
badezu borondaterik.

- Nik borondatea badot baia
ez dot mereziminturik.
- Merezimintu deukozuleko
zau zagoz eskojidurik.

Doministiku peneaz pena,
peneaz doministiku,
zaure entrainetan
Salbadorea dala sortu.