---
id: ab-4161
izenburua: Peru Gurea (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Peru guria Londonen (16)
zirkun-bedarrak ekarten
a andik dan artian
danok dantzatu gaitezen.
Ai, oi, au egia:
Peru guria Londonen.

Peru dogula Londonen
zirkun-bedarrak ekarten
a andik dan artian
danok jolastu gaitezen.
Ai, oi, au egia: daigun inguru Marixe.

Neure ugezab'andra Maria,
otzarapian daukot nausia
ak andik urteten daunian
berotuko'tsu gerria.
Ai, oi, au egia:
otzarapian nausia.

Neure morroi txoria
ik esan eustan egia
dantzan ikusiko nebala
neure emazte Maria.
Ai, oi, au egia:
eutzat kortako manda zuria.

Eta alan bazan - sartu daitela kalabazan
eta urten daitela - Bitoriako plazan.

(16): Itxura baten berba batzuk aldatuak izan dira, esaterako
"London", "jolastu"...