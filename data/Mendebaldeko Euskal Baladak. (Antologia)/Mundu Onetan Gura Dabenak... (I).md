---
id: ab-4179
izenburua: Mundu Onetan Gura Dabenak... (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Mundu onetan gura dabenak
zeruko lorak ikusi
Arantzazuko mendia gora
bear du bidea ikasi.

Errodrigotxo Baktzategiko
Birjina bilatzailea
auntzaren bila juan zinian
Arantzazuko mendira.

Birjina (i)Ama bilatu zeben
arantzan zeuri begira
laister ta arin etorri zinan
enbajadiagaz erira.

Oñatiarrak juan ziraden
prozesinoz mendira.

- Oñatiarrak Oñatikuak,
zelan penatzen nazuten!
Zeuen San Migel Oñatikora
bi aldiz eratzi nazute.
Irugarrenez baramazute
goguan bearko dezute.

- Errodrigotxo Baltzategiko
neure esan bat eidazu:
aita zurea arotza da ta
iten bat emon eiozu.

Egin deidala ermitatxo bat
deritxona Arantzazu:
iru latatxo zazpi teilatxo
oietxek askoko ditu.

Ama maitien seme on askok
an izango du ostatu,
an izango du ostatu eta
sarri kaliza altxatu.