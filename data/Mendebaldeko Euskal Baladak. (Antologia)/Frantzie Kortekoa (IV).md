---
id: ab-4153
izenburua: Frantzie Kortekoa (IV)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004153.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004153.MID
youtube: null
---

Frantziera juan nintzan
ni gazte-gazterik
da antxe paratu nintzan, da antxe paratu nintzan
andrie arturik. (bis)

Etorri nintzanian
Frantzie aldetik
ama topadu neban, ama topadu neban
etxian bakarrik.(bis)

- Ama, zer eiten dozu
orrela tristerik?
- Nobedade barri batzuk, nobedade barri batzuk
entzun ditudaz nik. (bis)

- Ama, zer nobedade
entzu(n) dituzu gaur?
- Ezkondu ein zariela, ezkondu ein zariela
ai neure seme ona. (bis)

Ba urten bidera eta
il egin bear dozu.
- Baina bakarrik ezin, baina bakarrik ezin,
ai neure ama ona. (bis)

- Artuizu lagune
zeure anaie,
zeure anaie ta, zeure anaie ta
estudaintie. (bis)

- Egunon daukozula
Bizkaiko semia.
- Alan ekarri daiela, alan ekarri daiela
Frantziko alaba. (bis)

- Jesus, Maria ta Jose
ori da kutxiluaren zorrotza!
Ikusi ta beste barik, ikusi ta beste barik
ilten deus(t) biotza. (bis)

Amalau mando datoz
urrez kargaturik.
Arexek emongo'tsudaz, arexek emongo'tsudaz
larga neu bizirik. (bis)

- Orrek neureak dira,
zuk emon bagarik

Andrea jatzo zaitez
zu mando ganetik.
Amentxe il biar dezu, amentxe il biar dezu
kulparik bagarik. (bis)

- Zazpi izera ditut
odolez beterik
zortzigarrena ere, zortzigarrena ere
eie estaldurik. (bis)

Agur Bizkaiko seme
da amaginarreba,
sube musker andi bat, sube musker andi bat
tragauko al jana. (bis)

(11): jats.