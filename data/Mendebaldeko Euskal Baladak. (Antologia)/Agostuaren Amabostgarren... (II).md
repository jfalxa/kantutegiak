---
id: ab-4172
izenburua: Agostuaren Amabostgarren... (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Agostuaren amabostgarren,
Jesus Maria goizean,
Birjina Amaren debotatxo bat
Arantzazura zijoan.

Oinetan ere zapata gabe
abitu beltza soinian
San Frantziskoren kordoetxo bat
gerrian iru doblian.

San Agustinen liburutxo bat
eskutxo bien erdian,
pausua baratz botatzen zuan
Urtiagako gainian.

Izar ederra irten zitzaion
San Eliasen parian
Salbetarako elduko zala
Kurtzepizio gainian

Kurtzepizio gainian eta
Birjina Amaren etxian


Bein bide baten bilatu neban
angerutxo bat bidian,
Berriz ta ere bilatuko dot
bide artatik banua.