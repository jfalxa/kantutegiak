---
id: ab-4166
izenburua: Jaun Zuriano (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004166.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004166.MID
youtube: null
---

- Jaun Zuriano, Jaun Zuriano
lotan zagoza? Izartu,
zeure esposa doña Errosa
bihar dala esposa barri. (bis)

- Nor da ori Manbruori
Manbru erriko trukuori
Manbru erriko trukuori
trukukumian semiori? (bis)

- Ni neu ez naiz Manbrukua
da gitxiago trukukua,
ziertuago izan neinteke
aingeru zeruetakua. (bis)

- Emendik ara erauteatxik
zer emongo daustazu sari?
- Mila libra argizari
neure Amabirjiñari
beste edo geiago
San Migel aingeruari.

- Gortan dekodaz zazpi zaldi
zein onena dan artu bedi.
Jaun Zurianok artu dau eta
arin doa erriz erri.

- Oles oles Dios te salve
gaur ostatua gura neuke.
- Emen eztago gaur ostaturik
Jauna lagun bekizu zeuri.

- Oles oles Dios te salve
gaur ostatua gura neuke.
- Emen eztago gaur ostaturik
Jauna lagun bekizu zeuri.

- Jaun Zuriano bizi zanian
pobriak emen eben ostatua
Jaun Zuriano bizi zanian
pobriak eben ostatua.

- Nor da ori pobriori,
pobre inkietaduori?
Jaun Zuriano atxitu dau-te
betor barrure erromesori. (bis)

Berton daukat ezpada bat
Jaun Zurianok neuri itxia:
aixe gora altzaten dabena
izingo da neure gizon geia.

Gortan daukat ezpata bat
Jaun Zurianok neuri itxia:
axe altzaten dabena
izango da gizon-geia.

Or da gazte brabu ori, da asi de baia ezin deu altza ezpatie, da:
- Ia bigarrenak,...eta ezin. Ia orrek pobre inkietadu orrek.
Orrek begiz ondo adituten dotso.
- Asi zeitez ba pobria.
Asten da, eta gora daroa ezpatia.
- Au da neure Jaun Zuriano.
- Au izingo da neure emaztia.