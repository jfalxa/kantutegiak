---
id: ab-4185
izenburua: Marijesiak (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004185.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004185.MID
youtube: null
---

Abendua santua da ta
eldu da denporia
guztiok adora deigun
Kristoren jaietzia.
Maria Jesus, Jesus Maria, Abe Maria, Amen, Amen Jesus.

Gabonak dira eta
edegi idazu atia
geuk be ikusi daigun
jaio dan infantia.
Abe Maria, Jesus Maria, Abe Maria, Amen Amen Jesus.

Gabeko amabietan
emoten daben orduan
geure salbagilia
etor da mundura.
Abe Maria,...

Belengo portalian
lastotxu bian ganian
jaio da Jesus ume
ganado artian.
Abe Maria,...

Izar diratzilia
gabaren gaberdian
agertu zan zeruan
Jesus jaio zanian.
Abe Maria,...

Astuak eta idia
daukan aldaminian
arnasa berotzeko
otz dagonian.
Abe Maria,...

Astuak arnasia
idiak arnasia
biak berotzen dabe
jaio dan saintxua.
Abe Maria,...

Oilarrak in daban
Belenian kukurruku
Kristo jaio zanian
gaberi gaberdian.
Abe Maria,...

Gaberi gaberdian
sortu da miraria
Belenengo estalpian
jaio da eguzkia.
Abe Maria,...

Otzaran ikaraz dago
jaio den umetxua
beren amak dirautso:
"Ixi, nere semetxua".
Abe Maria,...

Zeu biloizik egon arren
ez zara zeu pobria
zeruan josirik dago
zeuretzako erropia.
Abe Maria,...

Nor ete da, zer ete da
erropiaren joskilia?
Andra Santa Isabela
Santa joskilia.
Abe Maria,...

Maria Santisima
edigi idazu atia
geu be ikusi deigun
jaio dan infantia.
Abe Maria,...

Edegi dira zeruak
ate urregorrizkuak
eta arekaz batera be
izar ditzarezkuak.
Abe Maria,...

Emoizu emongo baizu
emongoizu nau badozu
aizia be otza dago ta
laguna be urritxu.
Abe Maria,...

Emen ezpadau ezer
guazen beste batera
Jaungoikua dagon lekuan
ezer ezta falta.
Abe Maria,...

Zeura emokaiagaitik
eskerrik asko Jaunak
ta gabon zoriontsuak
bemoskula Jaungoikuak.