---
id: ab-4180
izenburua: Birjina Arratekoaren Baladak (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arrateko zelaiko
bai floridadea...
andixek gora dago
zerura bidea.

Aldatza igarota
zelai landa baten
Birjina ta Semia
atsegin artutzen.

Lirio berdez jantzirik
dago (Arraten) altaria,
aintxen kantatzen dabe
ederki Salbia.

Zerura gura neuke,
zerura boladu,
aingeruak balidakez
eguak prestatu.

Eguak balituzkez
argizarizkuak
urtuko lituzkela
eguzki klaruak.

Bai eta ausi bere
ifartxo senduak. (bis)

- Zerura nua ni ta
nork nai du etorri?
Orain etorri jataz
iru mandatari.

Atzo etorri jataz
iru mandatari,
uso zuri eder bat,
aingeru polit bi.

- Uso zuri ederra,
zeruan ze berri?
- Zeruan berri onak
orain eta beti.

- Zer dakazu bada
zeruko gauzarik?
- Olibi adartxo bat
Birjinak emanik.

- Birjinak emanik ta
noizko izentaurik?
- Erramu eguneko
bai señalaturik.

- Erramu urrin da ta
igar (27) ezpaledi.
- Ori igartzen bada,
beste bat ekarri.

Zeruan asko dagoz
olibi adarrik,
olibi adarrik eta
lilio lorarik.

Lilio lorarik eta
santu ta santarik,
ez dala igartuko
zeruko gauzarik.

Aita San Pedro dago
zeruan giltzadi
Ai! Jaunak egin banindu
giltzaren jabedi!

Aitak ereteko
Aita neuriari,
lenengo aitari ta
urrengo amari.

Andik eta aurrera
mundu guztiari,
andik eta aurrera
mundu guztiari.

(27): Biltzaileak "ipar" dakar.