---
id: ab-4186
izenburua: Marijesiak (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004186.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004186.MID
youtube: null
---

Abendu santua da ta
ia da denporia
orain kanta dezagun
Jesusaren jaiotzia.
Maria Jesus, Jesus Maria, Maria Jesus.

Izarrak bularrian
semia sabelian
auxe da bai dontzella ederra
itsasuaren ertzian.
Maria Jesus,...

Izarrak urtetzen dau
odaiaren ertzetik
baita semia geuri
amaren sabeletik.
Maria Jesus,...

- Josepa, zer ordu da?
- Maria, gaberdia.
Kalbarioko izar ederra
orra ba geure parian.
Maria Jesus,...

Au dala jaio jaio,
au dala jaiotzia
semea jaio ezkero
ama dala dontzella.
Maria Jesus,...

- Ama Birjina
nun dozu semia?
- Orra or daukazu
Belengo portalian.
Maria Jesus,...

Belengo portalian
santo biren zanian (?)
otzaren ikaraz dago
jaio dan infantia.
Maria Jesus,...

Otzaren ikaraz zagoze
zeu ez zara pobria
Belenen josten dago
zeuretzako erropia.
Maria Jesus,...

Belenen erropia
zeruetan silia,
antxe jesarriko da
guztioren erregia.
Maria Jesus,...

Orain geiago kantatzeko
laburtu jat arnasia,
andria emon egidazu
ogia ta lukainkia.
Maria Jesus,...