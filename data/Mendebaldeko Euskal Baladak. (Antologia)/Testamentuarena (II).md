---
id: ab-4164
izenburua: Testamentuarena (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004164.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004164.MID
youtube: null
---

Izer eder bat argitu jaku
zeru altuen bakarrik.
Ez da bakarrik, lagunak ditu
Jaun zerukoak emanik.

Zazpi angeru aldamenian
zortzigarrena gaisorik,
zazpi mediku ekarri neuntsan
bakotxa bere lekutik,
zortzigarrena ekarri neuntsan
Indixe Madrilletatik.

Aixek igerri, aixek igerri
nundik daroien gaisorik.
- Amore minak baidituz onek
bai entrainetan sarturik.

- Mediku jaunak guzurre dino
ez dot nik amore minik,
amore minak baiditut bere
ez entrainetan sarturik.

Gaur arratsian ilgo naz eta
etorriko zarie gaubelara,
guztiok errosario
zuri eder bat errezetara.

Bier goizien maroienien
eleizara enterretara,
andixik gora igongo dozu
Birjina Amaren korura,
andixik bera salto eingozu
Birjina Amaren ortura.

Antxe artukozu larrosatxua eskura
eta kabelinia mosura,
mundu guztiek esan ez daien
neure (20) lutua dozula.

(20): Lekukoak "zeure" dino baina, zentzuak "neure" eskatzen dau.