---
id: ab-4175
izenburua: Arantzazura Egin Dot Promes... (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arantzazura egin dut promes
ero gau ero egunez (21),
ero gau ero egunez eta
oinutsean ta dolorez.

Izar eder bat ateratzen da (22)
urtean egun batean,
urtean egun batean eta
ura San Juan goizean.

Aren argitan ni joan nintzan
Arantzazura-ustean,
Ama Birjina ta aren semea
opa nituan bidean.

Kontseju on bat eman ziraden
Ama-Semeen artean,
on ta umila ni izateko
munduan nintzan artean.

- Ai artzaitxoa,
nere esan bat egizu,
nere esan bat egizu eta
nerea izanen zera zu. (23)

Ermitatxo bat egin bear degu
deritzola Arantzazu,
iru lata ta lau bost teilatxo
asko izanen alditu.

Denborarekin ura izanen da
ermita kuriosoa
ermita kuriosoarekin
komentu poderosoa.

Asko amaren bai seme maitek
an izanen du ostatu,
an izanen du ostatu eta
urrekaliza goratu.

Itxeak bana-binaka dira
Arantzazura bidean,
itxea itxe señalea da
arbola eder bat aurrean.

Arbola eder bat aurrean eta
mats beltza beste aldean,
mats beltza beste aldean eta
iturri otsa bat aurrean.

Iturri atan bataiatu zen
Kristo Erredentorea,
Kristo Erredentorearekin
guztien Salbadorea.

Jende guzti miran zegoan
usoa nora zijoan,
ego txuriak an busti eta
Parabisua dijoa.

Ama Birjina Arantzazukoak
eguteran tu laioak (24),
batetik itzak darionako
bestera aldatzekoak.

Ama Birjina Arantzazukoak
urregorriz du koroa
urregorriz du koroa eta
India-sedaz (25) bueloa.

Ai! Berak iruin ta berak aundu (26)
beraren donarioa

(21): "ero": edo (Azkueren oharra).
(22): "ateratzen du". Arbizuko aldaeran (Azkueren oharra).
(23): "Ai artzaitxua, artxaitxua / nere esan bat egidazu / ait'ori
ere arotza da-ta / datorrera esan
zaiozu" (...) (Azkueren oharra).
(24): "laioak": leihoak (Azkueren oharra).
(25): "seda-indiaz" dino lekukoak (Azkueren oharra).
"bueloa": beloa (Azkueren oharra).
(26): "aundu": ehundu, ehuna egin (Azkueren oharra).