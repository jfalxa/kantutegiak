---
id: ab-4144
izenburua: Ana Juanita (1)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Egun batian ni banenguan,
kostela kostel gainian
ni banenguan kostel-saltzian
ezpabe gari-saltzian.
Iru damatxu etorri ziran
bata bestien atzian,
irugarrenak pregunta eustan
gariak zegan zirian.
- Bestientzako dirutan eta
zuretzat laztan-trukian.
- Ez nagizula lotsatu, arren
onelan plaza betian,
onelan plaza betian eta
ainbeste jente-artian.
Orrelan bere esan bazeustan
leku sekretu batian,
gariak eta zeu ta neu bere
an konponduko ginian.
Ama neuria labarako da
goiz edo arratsaldian,
paseito bat emoidakezu (6)
ama laban dan artian.

Ama labatik etorri eta
mutil usaina etxian...
- Ana Juanixe, Ana Juanixe,
zein da dauana etxian?
- Katalinatxu auzokua da
beroi laban dan artian.
- Eztona ori Katalinatxu,
bizarra dau okotzian...
- Itxagon daizu, mutil, itxagon,
argia piztu artian,
argia piztu artian eta
ikusi zeu nor zarian.
- Ama, berorrek ez dauela nai
neure osasunik etxian,
biotz-ilunak egiten jataz
argia datorrenian...
- Urten daik, mutil, urten daik, mutil,
alako bentanarian,
ama neuriak urten dagian
kanpora sospetxerian...

Egun batian ni banenguan
Londresko ziudadian
maitiarentzat joiak erosten
tableru baten ganian.
Dama bateri galdetu neutsan
joiaok zegan zirian
- Bestientzako dirutan eta
zeuretzat buru-trukian.
- Eskerrik asko dama galanta
ez dot armarik aldian.
Neure arma onak itxi nituan
Motriko erri onian,
Motriko erri onian eta
Ana Juanixen etxian.

Txalopatxu bat, txalopatxu bi
Santa Klararen parian
neure anaia antxe datorke
batian edo bestian...
- Neure anaia: ze barri diraz
Motriku errian?
- Barriak onak diradez baina
jazo da kalte sobria
- O, neure anaia ez edo-da il
bioren aita maitia?
- Bioren aita ez da il baina
jazo da kalte sobria
- O traidoria ez edo-da il
bioren ama maitia?
- Bioren ama ez da il baina
jazo da kalte sobria.
Ana Juanixe ezkontzen zala
iñuen atzo kalian,
Ana Juanixe ezkontzen zala
aita ta amaren lotsian...
- Ezin izan lei ori egia!
Neuk dot armia aldian,
armia eta Gurutza bere
neuk ditudaz bularrian
ta neure armak tiretuko dau
bizi diraden artian

- Kapitan jauna: arria bela
txalopa nabegantian
ainbat arinen sartu gaitezan
Mutriku erri onian,
Mutriku erri onian eta
Ana Juanixen etxian.

Ogeitamairu dama lindatxu
Motrikun moila-ganian,
Ana Juanixe antxe dalarik
arexen danen artian...
Batak "agur" ta bestiak "adios",
Juanixe negar batian
- Ana Juanixe: zer pasatu da
Motriku erri onian?
- Zaldun guztia: neuk bere ez dakit
zeu nundikoa zarian,
- Neuk badakit ba, Ana Juanixe
zeu nundikoa zarian,
zeu nundikoa zarian eta
noren alaba zarian.
Damatxu baten arma-gurutzak
daruadaz bularrian
Ana Juanixe, ondo dakizu...
Ana Juanixe, ez eizu esan
berbarik banidadian
despedidako gabian bere
neure besuan zinian:
zeuk niri laztan, neuk zuri laztan
an despedidu ginian

Joia asko bere banakartzuzan
zuretzat izan ustian
beste damatxuk gozako dituz
zeuk ezin al dozunian.