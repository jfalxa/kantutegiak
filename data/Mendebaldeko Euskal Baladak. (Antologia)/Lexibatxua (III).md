---
id: ab-4157
izenburua: Lexibatxua (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004157.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004157.MID
youtube: null
---

- Egunon daukezuela
andra gaztia.
- Alan ekarri bedi
aita frailia.

- Lexibatxu zuria dezu
andra gaztia.
- Ondo-ondo irazita
aita frailia.

- Senarrik edo badezu
andra gaztia?
- Indietan dodala
aita frailia.

- Famelirik edo badozu
andra gaztia?
- Semetxu bi dodaz
aita frailia.

- Ze ofezio dauke
andra gaztia?
- Ezkolak ikastera
aita frailia.

- Laztan bat emongo neuskitzu
andra gaztia.
- Ibaie dau bitartez
aita frailia.

- Ibai ori pasauko neuke
andra gaztia.
- Etxatela komuni
aita frailia.

- Suak erreko al zaituz
andra gaztia.
- Berori illuntzi dala
aita frailia.

- Urak erungo al zaituz
andra gaztia.
- Berori txalupa dala
aita frailia.

- Agur agur
andra gaztia.
- Ondo ondo beiuela
aita frailia.