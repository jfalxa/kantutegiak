---
id: ab-4191
izenburua: Orbelak Airez Aire (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004191.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004191.MID
youtube: null
---

Orbelak airez airez
pipertxo dulzia
Landamingo zubitik
doian errekia.

Antxe topatu neban
Birjina Andria
ebala orraztuten
buruko ulia.

Etzala ulia ze
ezpazan urria,
ulondo bakotxeko
erinoz perlia.

Ifini biar jako
uleorri zintia
eun duket kosta dala
Madrilen kania.

Kosta bedi berraun de
bai betor obea.
Nor izango ete da
arexen eulia?
Maria Madalena
aprostuladea

Nor izango ete da
aren/orren yoskillia
Errege don Felipen
alaba lindia.