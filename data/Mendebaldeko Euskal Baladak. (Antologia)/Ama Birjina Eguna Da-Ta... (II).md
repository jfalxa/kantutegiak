---
id: ab-4178
izenburua: Ama Birjina Eguna Da-Ta... (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Birjina Amaren eguna da-ta
goazen Arantzazure.
Arantzazura bidea luze
ara orduko kantsatu,
Birjina Ama ta bere Semia
bidean ditut upatu.

- Ari artxaitxua! Ai Artxaitxua!
Neri esan bat egizu,
aite eori zurgine dezu
onera biali dezazu.

Ermite oni para deiola
apiotxo bi aldian,
apiotxo bi aldian eta
zazpi teiletxo gainean.

Zenbait amaren seme maite
izanen de emen ostatu
baita ere kaliza goratu.