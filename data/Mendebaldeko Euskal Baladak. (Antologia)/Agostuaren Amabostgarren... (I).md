---
id: ab-4171
izenburua: Agostuaren Amabostgarren... (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Abuztuaren amabostgarren
Andra Marie goizean
Birjinearen debotatxu bat
joan zan erromerian.

Oinak ortozik zapatak barik
abitu baltzez jantzirik
onek guztiok alda batera
Natibitate goizean.

Arratsaldian joan ein nintzen
ardian bila mendira
Birjinatxu bat topau nenduan
arantzan neuri begira.

- Derrodigotxu, Derrodigotxu,
erdu ona da entzu'zu!
Aita zuria arotza da ta
arren egizu mandatu.

Egin daiela ermitatxu bat
deritxotzena Arantzazu.
Iru lata ta zazpi teilatxu
arren asko izengo'itu.

Amaika amaren semeok bere
an izango dau ostatu
San Franziskoren semeok bere
kalisfatuari (?) debotu.

Inpernurako bideak dagoz
arantza baltzez eginik,
zeru altura bideak dagoz
arri labraduz eginik.

Arri labraduz eginik eta
erdian aldazgorea,
mundu onetan ondo egin dauanentzat
ez dago bide luzea.

Urre bietan sartute dago
atetxu orren giltzea
zeruko salearen erdian dago
birjina koronadea.
Salbe batekin enkomendatu
arren iru abemaria.