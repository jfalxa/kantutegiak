---
id: ab-4155
izenburua: Lexibatxua (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

- Goizeti jagi zara,
o! andra gaztea.
- Lexibatxua egitearren,
jauna neurea.

- Lexibatxu zuria dozu,
o! andra gaztea.
- Ondotxu jabonauta,
jauna neurea.

- Senarrik edo bozu,
o! andra gaztea.
- Biaje urrunean da,
jauna neurea.

- Umerik edo bozu,
o! andra gaztea.
- Jaungoikoak emonda bi,
jauna neurea.

- Ze opizio emon deutsazue,
o! andra gaztea.
- Madrilgo estudioan dabiz,
jauna neurea.

- Bertan itoko alzara,
o! andra gaztea.
- Berori txalupa dala,
jauna neurea.