---
id: ab-4194
izenburua: Alabaren Heriotza
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004194.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004194.MID
youtube: null
---

Birjina Karmengue,
Jangoikoaren Ama,
pekatari guztien
ausilio dana,
konfiantza osuaz
nator ni zugana
emen amaituko
da asi'ogun lana.

Señora bat bizi zan
beren familiagaz
biotz gogorreko ama,
nire eretxian.
Eukan señora orrek
alabatxu bat be,
Jangoikuak emonda
fortunarik gabe.

Alabien bokaziua
eukan mojatzako,
amak naiago zuen
berriz ezkontzeko.
Nobiue topaute
zeukan alabentzat,
Don Isidoro Fernandez
zala aberatsa.

Leku onekue zala
eukela kondukta,
ondo biziko ziriela
biak ezkonduta.
- Naiozu au edo beste?
Sokiegaz lotuta
beko bodegan il zuen
kutxilue sartuta.

Sokorruka didarrez
alaba tristie
egiteko mesede
bizirik istie.
Biotz gogorreko ama,
leoian kastie,
beren eskuetan ilteko
alaba tristie.

Aite bator etxera
biaje batetik,
alabiaren preguntia
ein zuen bertatik:
ia nora juen zan
alabia etxetik,
amak ezekiela
nun zan aren berri.

Bazkaltzen asi ziran
aitarekin ama,
amari mudau jakon
senblantie dana.
Beren senarrak dirautso:
- Zer dozu Fernanda?
- Etxat ezer pasetan,
neronen senarra.

Jagi maitikan eta
juen zan bodegara:
bestidu eder bategaz
alaba ederra.
Lau kandela biztute
aurrien dabela
aite suspensatu zan
ikusirik ala.

Ama doie kalera,
asten da diadarrez,
aitak il ziola-ta,
falsian negarrez.
Amaren informia
guztie aldabres,
aite kulpe bagia
an zeuen bildurrez.

Aita kulpe bagerik
sartu eben preso
griloz ondo kargaute,
ainkak eta beso.
Orduaren begira
noiz ordure kaso,
falso testimonio
urkamendirako.

Ez eukela Fernandok
orregaz kulperik
inuzenzian bizia
kentzeko berari,
inuzenzian bizia
kentzeko berari,
eta urketuteko
andria bertatik.