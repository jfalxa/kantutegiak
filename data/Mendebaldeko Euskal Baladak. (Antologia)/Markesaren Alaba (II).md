---
id: ab-4201
izenburua: Markesaren Alaba (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004201.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004201.MID
youtube: null
---

Zeruak eta lurrak
egin zituanak
memoria argitzen
atoz niregana.
Esperantza badaukat
zerorrek emana,
kunplituko dedala
desio dedana.

Amodiozko pena
berso berrietan
publikau al baleza
Euskalerrietan,
enteratu naz ongi
enkargu oietan,
San Jose arratsaldeko
iru t'erdietan.

Markesaren alaba
interesadue,
marineruarekin
enamoradue.
Deskubritu bagerik
beren sekretue,
amodiua zeukan
barrena sartue.

- Zer esaten deustezu
Juanita Bitxori?
Tentatzen ai zerala
traza badirudi,
etxat zure gradurik
tokaduten niri,
ez egin burlerikan
marineruari.

- Iduki dezakezu
ongi sinistua,
aspaldion nauela
zeugaz gustadua.
Etzaitut ez itxiko
desanparadua,
alan egiten deutsut
gaur juramentua.

Konformatu ziraden
alkarren artian,
ezkonduko ziriala
urrengo urtian.
Eskolak ikesteko
bien bitartian,
beraren erritikan
guztiz apartian.

Ala disponiturik
jarri ziran biak,
kartaz entenitzeko
alkarren berriak,
formalidadiakin
jarteko egiak,
baina etziran lo egon
amaren begiak.

Alperrik izango dire
asarre guriak,
enabe mudatuko
eternidadian.
- Esposatu nai nuke
kariño onian,
Antonio Maria
etorten danian.

Ama ezin zan egon
itz ori sufritzen
beriala asi zan
kartak detenitzen,
intentzio aundiaz
ezkontza galdutzen,
Juanitak olakorik
ez eban pensatzen.

Amak esaten deutso:
- Juanita neuria,
galdu da dinuenez
Antonio Maria.
Nik bilatuko deutsut
beste bat obia,
maiorazko interes
askoren jaubia.

- Ez esan ama niri
orrelagorikan,
nik ez dot bestegana
amodiorikan,
ezin alegrau leike
nire barrenikan,
konbenenzia ona
egonagaitikan.

- Utzi alde batera
orrelango lanak,
nik ez dodaz ikusten
zu bezela danak.
Nai badituzuz artu
onrrak eta famak,
giatuko zaituez
aitak eta amak.

Amaren malizia
korriora joanda,
Antonio il zala
egin zuen karta.
Juanitaren tristurak
maitia izanda,
engainatu dau arren
gezur bat esanda.

Denpora kunpliturik
galaia ebilan,
zer pasatu ete zan
aren memorian.
Kartarik artu gabe
juen dan aspaldian,
inuzente sartu zan
jaio zan errian.

Au da lendabiziko
esan genduena
zer da musikarekin
onratzen duena?
Markesaren alaba
kalian barrena,
esposario zala
a biar zuena.

Desmaiaturik egin
zuen ordubete
gero nobia eske
itz bi egin arte.
Urreratu zitzaion
makine bat jente,
bigarren ordureko
il zan derrepente.

Gaba pasatu eta
urrengo goizian,
enterrua zijoian
bigarren klasian.
Markesaren alaba
guztien atzian,
zer pena izengo zan
aren biotzian.

Penarikan lertute
Antonio il zan,
akonpainatu zuen
Juanitak elizan.
Maitasuna bazion
esandako gisan,
etzuan geroztikan
osasunik izan.

Remediau al balei
sentimentu ori,
jarri bitarteko bat
Jesus maitiari,
orazino eginaz
Birjine Amari,
zeruan gerta dedin
Antonio Mari.