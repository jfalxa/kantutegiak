---
id: ab-4163
izenburua: Testamentuarena (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Izar ederrak urteten dabe (17)
zeru altuan bakarrik
ezta bakarrik, lagunak ditu
jaun zerukoak emanik.(18)

Zazpi aingeru aldean ditu
zortzigarrena gaixorik
zazpi mediku ekarri deutsez
India-Madriletatik.

(19) Arek igarri, arek igarri
nundik dagoan gaisorik.
- Amoreminak badituz onek
desendraiñetan (sic) sarturik.

- Guzurra dino mediku orrek
nik eztot amore-minik
amoreminak badaukadaz be
desendrainetan sarturik.

Gaur arratsean ilgo naz eta
etorri bedi gorpura,
errosario zuri eder bat
eskuartean/errezetara dozula.

Neu enterretan naroenean
igongo dozu korura,
andixek bera salto egingozu
Birjina Amaren ortura.

Larrosea ta kabelinea
artungo dozuz eskura,
jente guztiak esan daiala
neure lutua dozula.

Aingerutxuok esaten dabe
salbean Abe Maria.
gero guztiok goratu daigun
zeru altuan gloria. Amen, Abe Maria.

(17): argi egiten dau.
(18): emonik.
(19): 3. Ahapalditik aurrera ez dator.