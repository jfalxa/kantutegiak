---
id: ab-4156
izenburua: Lexibatxua (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004156.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004156.MID
youtube: null
---

- Egunon dakoela,
andra gaztie.
- Alan ekarri deiela,
aite prailie.

- Lexibetxu zurie dozu,
andra gaztie.
- Ondo-ondo egosite,
aite prailie.

- Senarrik edo bozu,
andra gaztie?
- Indietan dadukat,
aite prailie.

- Famelirik badozu,
andra gaztie?
- Umetxu bi ditudaz,
aite prailie.

- Ezagutuko zeunke,
senar zeurie?
- Lengo trajiaz baletor,
bai aite prailie.

- Laztan bat emongo neuskizu,
andra gaztie.
- Ibaie dau bitarte,
aite prailie.

- Laster igaroko neuke,
andra gaztie.
- Etxatela konbeni,
aite prailie.

- Agur, agur, agur,
andra gaztie.
- Beiuela ondo-ondo,
aite prailie.