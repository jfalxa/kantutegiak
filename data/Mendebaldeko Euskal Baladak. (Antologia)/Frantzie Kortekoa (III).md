---
id: ab-4152
izenburua: Frantzie Kortekoa (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Etorri nintzanean
Frantziako turitik (sic)
ama topadu neban, ama topadu neban
etxean bakarrik.

- Ama, zer dozu bada
zeuk orren tristerik.
- Nobedade barri batzuk, nobedade barri batzuk
jakin deutsudaz nik.

- Ze nobedade barri
jakin dozu ama?
- Ezkondu egiten zareala, ezkondu egiten zareala
neure seme ona.

Nundikoa dakazu
zerorrek andrea?
- Frantziako kortekoa, Frantziako kortekoa
bai ama neurea.

- Zertarako da emen
Frantziako umea?
- Zer egingo deutset bada, zer egingo deutset bada
bai ama neurea?

- Artuizu lagun eta
zeure arrebea
zeure arrebea eta, zeure arrebea eta
estudiantea.

- Egun on dakarrela
neronen andrea.
- Alan ekarri daiela, alan ekarri daiela
Bizkaiko semea.

Ori da kutxiloaren
puntaren zorrotza
ikusi ta beste barik, ikusi ta beste barik
ilten daust biotza.

Amalau mando datoz
urrez kargaturik
arek artuizuz baina, arek artuizuz baina
itxi idazu bizirik.

Ekarrizu abadea
konpesaduteko
sakramentu santua, sakramentu santua
errezibietako.

- Abadea urrun dago,
eleizea urrunago,
orretan ibilteko, orretan ibilteko
asti gitxi dago.

- Agur Bizkaiko seme
amaginarreba
suge-musker andi bat
sartuko al jana.