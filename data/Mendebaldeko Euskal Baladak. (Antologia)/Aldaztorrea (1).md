---
id: ab-4142
izenburua: Aldaztorrea (1)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Aldaztorrean nengoanean
irra goruetan
etorri jatan erroi zarra
grauetan, grauetan.

- Erroi zarra zer dakazu albiste?
- Alamarkea galdu dala dinoe.
- Galdu naz bada, alaba ditxa (2) bagea.
Antxe nebazan ogetabat lengusu ta nebea
arek baino bearragoa aite neurea,
arek guztiak baino azkarriago jaubea (3).

- Zer dinona, puerka (4) lotsa bagea?
Azkanengo esan dona jaubea?
Esan bebanan lenengotatik jaubea
izango ebanan Aldaztorrean partea.

- Nik neurea dot goian dagoan kaxea,
tapez taperaino diruz betea, betea.
Imineagaz neban urre gorria
anegeagaz neban urre zuria.
Mila dukat neban isil boltsea
a bere bazan alaba on baten dotea!

Aldaztorreak ateak dituz letuez
ango plater pitxerrak zidarrez.
- Joan ala egon eingo't, ama neurea?
- Joan, joan neure alaba maitea.

- Aurtxo txikinak sabelean deust ostiko
Jaun zerukoak al dau semea sortuko.
- Nai dan seme nai dan alaba
Aldaztorrean partea izango dona.

(2): zori
(3): yaubea
(4): urdang.