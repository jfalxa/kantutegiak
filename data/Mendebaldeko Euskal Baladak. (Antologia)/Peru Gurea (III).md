---
id: ab-4162
izenburua: Peru Gurea (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Peru guria Londresen
sillun-bedarrak ekarten
Peru guria andik dan artian
guztiak olgau gaitezan.

Neure kriau txorixe
zeuk esan zeunstan egixe
arratsian ikusiko nebala
dantzan neure emazte Marixe.

Ai oi ai egixe! Zeuretzat kortan dauen zaldi zurixe.