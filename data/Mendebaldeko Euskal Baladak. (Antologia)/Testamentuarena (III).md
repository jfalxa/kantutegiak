---
id: ab-4165
izenburua: Testamentuarena (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Zeru altuan urten iten dau
izer eder bat bakarrik
bakarrik eztau, lagunek deituz
jaun zerukoak emonik.

Zazpi aingeru ondoan deituz,
zortzigarrena geisorik.
Zazpi mediku ekarri'otsiez
bakotxa bere lekutik.

Zortzigarrena ekarri'otsie
Indie-Madriletati,
arexek bere igirri dotso
zetatik dauen geisorik.

- Mediku jauna guzurre dino
nik eztot amore minik,
amore minek badoraz bere
ez estrainetan sarturik.

Bier goizean ilgo naz eta
etorri naure gorpure,
errosario zuri eder bat
zuk errezetan dozule.

Andixe gora, andixe gora
Jesus gurian korure,
Jesus gurian korure eta
Ama birjinean orture.