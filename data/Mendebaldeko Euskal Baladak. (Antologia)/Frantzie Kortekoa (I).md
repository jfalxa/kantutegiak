---
id: ab-4150
izenburua: Frantzie Kortekoa (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Etorri nintzanean
Prantzie kortetik,
ama bilatu neban
suitian tristerik.

- Zer egiten dau, ama,
orrelan bakarrik?
- Nobedade barriek
entzun dodaz bai nik.

- Zer nobedade barri
entzun dozu ama?
- Ezkonetan zareala
bai seme laztana.

- Nondikoa dakazu
zeure emaztea?
- Prantzie kortekoa
bai ama neurea.

Ez dot gure prantzesik
ain gitxi errenik
esan dodaz berbiaz
urten biar dot nik.

- Esan dozun berbiaz
urten bihar bozu
- Urten bidera eta
il ein bear dozu.

- Ain de polite ba ze
nire emaztea
ez da posible ama
nik ori eitea.

- Zuk ezpozu posible
artu anajia,
anaje gaztea da
estudiantia.

Juanita jatsi zaite
zaldien ganetik,
sartu daitzudan puñalau
biotzen erditik.

- Ai orren puñalorren
puntien zorrotza
ikusiez beste barik
ilten deust biotza.

Zazpi zaldi deukadaz
izeraz jantzita
zortxigarrentxoa barriz
perlaz estalita.

Arek emongo'tsudaz
istiarren bizirik
arek neuretzat baina
ez itxi bizirik.

Orrez gainera dekot
urre-kalizia
urre-kalizia ta
sobrepelizia.

Arek emongo'tsudaz
istiarren bizirik.
- Arek neuretzat baina
ez itxi bizirik.

- Kanpai bat entzuten dot
ez dakit nongoa
igual izango da
agoniekoa.

Abadea ekarrizu
konfesadu naiten
animaren okerrak
arteztu daidazan.

- Abadea urrin dago
eleizea urrinago
oin orretan asteko
asti gitxi dago.