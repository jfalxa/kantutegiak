---
id: ab-4187
izenburua: Marijesiak (IV)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004187.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004187.MID
youtube: null
---

Gabonak dira eta
edeidazu atiau
ikusi daigun
jaio dan infantia.
Maria Jose, Jesus Maria.

Abendu santu onetan
Kristoren jaiotzian
kontentuaren alde
guztiak poz gaitian.
Maria Jose, Jesus Maria.

Adan formatu izan zan
lenengo lurrian,
pekatu egin eban
paraisu eternalian.
Maria Jose, Jesus Maria.

Bear zala bialdu
lurrera aingerua
erremediatzeko
Adanen pekatua.
Maria Jose, Jesus Maria.

Trinidade altuko
bigarren personia
trasformaturik dago
Belengo portalian.
Maria Jose, Jesus Maria.

Herodes erregia
falso traidoria
zeren damu eben
Jesusen jaiotza (?)
Maria Jose, Jesus Maria.

Herodes koitadue
burladu zaitue
erregeak joan dira
zu ikusi gabe.
Maria Jose, Jesus Maria.

Herodesek esa'utsen
erregeari gabien
emendik zetozela
neu ere joan naitian.
Maria Jose, Jesus Maria.

Iru errege mauak
ain bide luzian
ostatu artun eben
ain etxe pobrian.
Maria Jose, Jesus Maria.

Meltxorrek urria,
Gasparrek intzentsua,
Baldaserrek eroiela
mirra preziosua.
Maria Jose, Jesus Maria.

Astoa arrantzia,
idiak arnasia,
alan berotzen eben
jaio dan infantia.
Maria Jose, Jesus Maria.

Belengo portalian
estalpe batian
jaio da Jesus ona
abereen artian.
Maria Jose, Jesus Maria.

Bai Belenen da bai Belenen
jaio da Jesus Nazaren.

Nazareteko tenpluan dago
dama eder bat jarririk
izena bere Maria dauko
ondo graziaz beterik.