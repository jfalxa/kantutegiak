---
id: ab-6050
izenburua: Neska Ontziratua (Derio Ii)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Isabelatxu, Isabelatxu,
arren esan bat eidazu:
atian dauen kantore orri
etor dila esaiozu.