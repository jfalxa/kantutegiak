---
id: ab-4190
izenburua: Orbelak Airez Aire (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

- Orriak aidez aidez
ifartxu dulzia,
Landa mintz ederrian
doian errekia.

Antxe topatu neban
Birjina Maria
orraztuten zebala
buruko ulia.

Ez zan a ulia, ez
ezpada urria
ulondo bakotxeko
zerion perlia.

- Urreratu zakidaz
arima maitea
benetan maitatzen dot
nik zure pedea.

Eskintza orregaitik
zeuk, neure alabia,
artu izango dozu
zeruko koroia.

- Nun dago, Amandrea
zeroren Semea
zeruti jatsiriko
gure mesedea?

Berak argitu daidan
neure arimia
ikusi daian gero
Zeruko bidia?

- Or goiko munatxuan
oinak ortotzian
keriz emoten deutsan
ganeko arantzian.

An dago bada neure
arantz larrosia
zuri ta gorria da
neure amoria.

Begiak ditu baltzak
urrezko ulia
aoa txit ederra
ta zoragarria.

A maite ez dabenak
bere biotzean
zer maitatu deike
damurik bagean?

Bere ondoren dabiltzaz
zeruko aingeruak
ibarrak apainduten
lora usaintsuak.

Txoriak bere pozez
soinu alegrian
kantau daroakez
guztiak batian.

Irribarrez diardutso
landako loriak
jantzirik bere ondran
soineko barriak.

Lurreko abereak
bere mesedean
jaio ta arrazkero
dagoz ardurean.

Zeuek gaitik bakarrik
zeruti lurrera
jatsi da nire Jesus
gizon izatera.

Agaitik, neure alaba
kantau daigun, kanta
berari emoteko
betiko alabantza.

- Agur, Jesus laztana,
Jainko ta gizona
zu beti izango zara
gure zoriona.

Zeureak gara guztiz
gorputz eta ariman
izango gara bere
bizitza guztian.

Izan gaitezan Zugaz
Aitaren etxean
Espiritu Dontsuaz
Jainkotasunea.