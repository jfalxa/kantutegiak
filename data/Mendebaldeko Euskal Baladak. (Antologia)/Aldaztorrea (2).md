---
id: ab-4143
izenburua: Aldaztorrea (2)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Alostorria bai Alostorria
Alostorreko zulubi luzia.
Alostorrian nenguanian
goruetan
belarantza (5)
kua kua kua kua leioetan.

(5): bela baltza.