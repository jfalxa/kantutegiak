---
id: ab-6048
izenburua: Neska Ontziratua (Dima I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arantzazure joan bear dot
gabean edo egunez,
Birjina Amoni preguntaduten
izango bada edo ez.