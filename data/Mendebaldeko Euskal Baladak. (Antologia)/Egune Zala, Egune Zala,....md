---
id: ab-4170
izenburua: Egune Zala, Egune Zala,...
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Egune zala egune zala
bart idergie zanien
egunerako allegeu nintzan
zazpi legua bidien.
Zazpi legua bidien eta
Arantzazure nindoien
Birjina Ama ta beren semie
nituzen topeu bidien.

Oinetan bere ortozik eta
abitu baltza soinien
San Frantziskoren sinboloa
gerrien iru doblien.

- Oñetikara Oñetikorra
zelan penatzen nozu?
Zeuen Samigel Oñetikora
birriten banerakazu,
irugarrenez banerakazu
gogoan izengo nozu.

Erolitetxu Altzetariko
Amaren seme maitie,
Amaren seme maiterik asko
Birjina bilatzailie.
Zeuek ezenkien bera nor zan da
Birjina Andrie topau zenduen
arantzan zeuri begire.

Izan zaitezala on da umilde
munduan zinen artien
ondo gordia izengo zara
zeruko tribunalien.

- Pastore santu
erdu ona ta entzizu!
Zure aita arotza da ta
egin daiela ermitatxu bat
deritxodun Arantzazun.

Arotz maisuak asi ziraden
materiela bilatzen,
esan oi dabe ez dala posible
ermitatxuau an eiten.

Iru lata ta zazpi teilletxu
ganien askoko ditu.
Amaren Seme San Frantziskokok
an izengo dau ostatu.