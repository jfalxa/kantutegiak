---
id: ab-4193
izenburua: Orbelak Airez Aire (IV)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Odeiek aidez aidez,
ipartxu dultzia,
Landamin ederretan
doan errekia...

Antxe topadu neban
Birjina Amandria
orraztuten ulia.

Etzan a ulia
ezpazan urria,
ule-ondo bakotxian
erion perlia.

- Betzo amandria
ulerako zintia
kania bosteun duket
Madrilen kostia.

- Eskerrik asko dama,
damatxu gaztia,
neuk dauket iresien sedia
bilbien urria.

Nor ixango da
arentzat eulia?
Maria Magdalena
apostoladia.