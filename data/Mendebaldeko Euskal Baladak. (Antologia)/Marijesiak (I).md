---
id: ab-4184
izenburua: Marijesiak (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Abendu santu onetan
Kristoren jaiotzian
kontentuaren aundiz
guztiok poz gaitian.
Maria Jose, Jose Maria, Maria Jose.

Adan formadu zanian
lenengo lurrian
pekatu egin eban
Paradisu (e)ternalian.
Maria Jose,...

Linborian egozan deiez
bost mila urtian
Jauna zerutik lurrera
etorri zeitian.
Maria Jose,...

Erabagia izan zan
Trinidade altuan
biar zala biraldu
lurrera aingerua.
Maria Jose,...

Biar zala biraldu
lurrera aingerua
erremendiatzeko
Adanen pekatua.
Maria Jose,...

Nor emen mensajerua?
San Gabriel aingerua
egingo dabena ondo
Jaunaren agindua.
Maria Jose,...

Edegi dirade zeruan
ate diamantezkuak,
aretxekin batera
bentana izarrezkuak.
Maria Jose,...

Gizonik ez dot ezagun
lurraren ganian
eginik deukat boto
ni jaio nintzanian.
Maria Jose,...

Espiritu Santu Jaunak
emonik argia
zeure kontzebidea
izango da garbia.
Maria Jose,...

Ez deukat nik errurik
banaz ni esklabia
Jaunak ordenadu dau
ni onela izatia.
Maria Jose,...

Marabilla andi bat
zerutikan lurrera
konzebidua izan da
zeruko Erregina.
Maria Jose,...

Ez dator gizonetatik
ez gizonaren obrarik
Elejidurik eguan
Eternidadetik.
Maria Jose,...

Nabegadua izan zan
bederatzi ilabetian
mundu onetako
itxasuan ganian.
Maria Jose,...

Josepe ditxosua
ia da denporia
neugandik jaioteko
Jesus salbadoria.
Maria Jose,...

Eguzkia nola doian
kristalaren artetik
Semia jaio dala
Amaren sabeletik.
Maria Jose,...

Kristalian argia
bezela pasatu,
eginda gero bere
dontzella geratu.
Maria Jose,...

Eguzki dibinala
gaubaren gauberdian
etorri da mundura
gizonaren jantzian.
Maria Jose,...

Trinidade altuko
bigarren personia
transformadurik dago
Belengo estalpian.
Maria Jose,...

Otzaren ikaraz dabil
jaio dan infantia
bere amak dirautso:
"Isi ene semia".
Maria Jose,...

Astua ta idia
daukaz aldamenian
arnasaz berotzeko
oztuten danian.
Maria Jose,...

Astuak arrantzia
idiak arnasia
alan berotzen eben
jaio dan infantia.
Maria Jose,...

Astua ta idia
jaten egozanian
seina euki ebela
biaren bitartian.
Maria Jose,...

Iru Errege Maguak
ain bide luzian
milagrosamentian
juntatu zirian.
Maria Jose,...

Iru Errege Maguak
ain bide luzian
bakotxak eroiela
beren presentia.
Maria Jose,...

Iru Errege Maguak
ain bide luzian
ostatua artu eben
ain etse pobrian.
Maria Jose,...

Belengo portalera
eldu zirianian
ango zaldiaren otsak
espantadu zeukian.
Maria Jose,...

Aurra adoratzean
Iru Errege emen
ikusirik pobrezan
arritu ziraden.
Maria Jose,...

Erregaluak bere
ekarri zituen
urria, intzentsua,
mirra irugarren.
Maria Jose,...

Gasparrek urria ta
Meltxorrek intzentsua,
Baltasarrek eroian
mirra preziosua.
Maria Jose,...

Herodesek esan eutsen
erregiari gabian
emendik zetozela
ni bere juan neitian.
Maria Jose,...

Herodes koitadua
burladu zaitue
erregiak juan dira
zeu ikusi gabe.
Maria Jose,...

Herodes erregia
faltso traidoria
zeren damu eban
Kristoren jaiotzia?
Maria Jose,...

Mendian egozala
jagoten ganadua
manifestadu jakon
zerutik aingerua.
Jesucristo, ¡ay qué bonito
buen Niño Jesucristo!

Corrían los pastores
dejando el ganado
que daban grandes voces:
¡Jesús sea adorado!
Jesucristo,...

Txit egun ditxosua
degu Eguberri
zegaitikan Jesus dan
gugana etorri.
Jesucristo,...

Ai ze kontsuelua
orain guk deukagun
Jainkuaren Semia
etorri da lagun.
Jesucristo,...

Guztiok biar degu
disposizinoan jarri
graziak emoteko
gaur jaio danari.
Jesucristo,...

Erakutsiko deusku
zer egin biogun
beraren alabantzan
kantadu dezagun.
Jesucristo,...

Jose eman diote
Mariari esposo
biak izan deitezen
Jesusen guraso.
Jesucristo,...

Karguan jarri dira
Jose ta Maria
anparatzeko Seme
estimagarria.
Jesucristo,...

Agur dontzelia
graziaz betia!
Zeure izena dozu
Birjina Maria.
Jesucristo,...

Birjina arritu zan
agur egin eutsenian
onelango mensajia
nok ete leikian.
Jesucristo,...

Dontzella galant ori
orbanik gabia
zeure izena dozu
Birjina Maria.
Jesucristo,...

Arkanjel San Gabriel
Anunziadoria
ez gaizuz espantadu
ain jente umildia.
Jesucristo,...

Denpora desiadua
etorri zanian
ia gara eldu
Kristoren etxian.
Jesucristo,...

Josepek dirautso
Maria Birjiniari
noz deklaratuko daben
esperantza geure ori.
Jesucristo,...

Biak Belenera
dute kaminatu
ara eldu zirianian
ostatuaren itandu.
Jesucristo,...

Eskepe etxetxu baten
dagoz atian joten
gizona bentanara
txito nekez urteten.
Jesucristo,...

Gizonak bentanatik
au dau erantzuten:
- Nor da ordu onetan
atiak joten?
Jesucristo,...

- Nor eta nor bizi da
etxetxu zar onetan?
- Nor eta zer gura zenduke
Jaunaren ordu onetan?
Jesucristo,...

Ara or etxetxu zar bat
uriaren kabuan
aberiak eta baino
ezta sartzen barruan.
Jesucristo,...

Pobrezan izandu zan
Jesusen jaiotza
batetik elurra eta
besterik izotza.
Jesucristo,...

Sutxu bat egiten asi
pesebre basterrian
aiziak eta otzak
lenera eroian.
Jesucristo,...

Seina bilosik dago
lur otzaren ganian
ama beleniko
negarrez aurrian.
Jesucristo,...

Amiak artu eban
semia besuetan
Semia ta Jainkua
zituan adoretan.
Jesucristo,...

Bilosik basagoz bere
etzara zeu pobria
zeruetan josirik dago
zeuretzako erropia.
Jesucristo,...

Bedarrak almueda,
ogetzat pesebria,
aurrian beleniko
bere Ama umildia.
Jesucristo,...

Arantzaren arteko
jaio zanian
kulpa orijinalaren
mantxa gabia.
Jesucristo,...

Seme Jauna daukela
urre ardui eder baten:
bere Ama beleniko
aurrian adoratzen.
Jesucristo,...

Jose eta Maria
euren Jesusarekin
duaz Belenera
kunplietan legiakin.
Jesucristo,...

Obligazino bagarik
kunplietan legia
guri erakustiarren
Zerurako bidia.
Jesucristo,...

Ara gura badozu
ortixik dago bidia
zeren justoa dan
Kristoren legia.
Jesucristo,...

Sakramentu santua
zelebradu zanian
izena imini eutsen
Jesus Salbadoria.
Jesucristo,...

Emen esan badogu
berbarik deungaro
Eleiz Ama Santiari
parka eske naiago.
Jesucristo,...

Ogetalaugarrena
degu Abenduan
deseo genduena
logradu genduan.
Bai Belenen, eta bart Belenen,
jaio da Jesus Nazaren.

Gabeko amabiak
jo daben orduan
gure Salbadoria
jaio da munduan.
Bai Belenen,...

Belengo portalian
estalpe batian
jaio da Jesus ona
ganadu artian.
Bai Belenen,...

Belengo urian
gabon gaberdian
Birjina Santia dago
kontsolazino andian.
Bai Belenen,...

Mundu guztia dago
gaur pozez beterik
semia jaio dalako
mantxa bagarik.
Bai Belenen,...

Goiko mendietatik
pastoriak datoz
esanik aingeruak
jaio dala Jesus.
Bai Belenen,...

Pastore santu birtuosuak
balia zakiguz gaur emen
bart arratsian zer pasatu dan
konta dezagun Belenen.
Bai Belenen,...

Korraletako ardiak bere
jantzan ebizan orduan
gure Jesusa Salbadoria
jaio dalako munduan.
Bai Belenen,...

Jesus dibino! Gabaren otza!
Jausten da inetasie...
Erleju bien onelan bada
il da ganadu guztie.
Bai Belenen,...

Ortixik gora ninoiela
ospitaletxu zar baten
aita San Josepe topadu neban
astotxuaren txalmatzen.
Bai Belenen,...

Nazaretako tenpluan dago
dama eder bat jarririk
izena bere Maria dau-te
dana dao graziaz beterik.
Bai Belenen,...