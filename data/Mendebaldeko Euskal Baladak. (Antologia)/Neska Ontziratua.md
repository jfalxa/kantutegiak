---
id: ab-4159
izenburua: Neska Ontziratua
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

- Isabelatxu, Isabelatxu,
arren esan bat eidazu.
atian dauen kantore orri
etor dila esaiozu.

- Egun on asko dizule
ontziko maisu gaztia.
Alango asko ekar dizule
linde damatxu gaztia.

- Aitek eta amak bialtzen neude
au kantatxuau ikasten.
- Au kantatxuau ikis nai bozu
erdu neurekin bordera.

- Eskerrik asko mile biderrez
ez dot olango usterik,
aitek eta amak etxian neukie
neu ondo esitmadarik.

Berbau aotik esan bazuan ze
sartu zioen kapapean ta
eroen zuen bordera.

Lobedartxu bet ipini neuntsen
bular biaren erdian
linde damia loak daroa
bosteun legoa bidian.

Alan ta bere subertia zan
untziek alkar jotia,
ordu atantxe itxartu zala
linde damatxu gaztia.

- Eroan naizue, eroan naizue
aiten d'amaren errire.
- Aiten d'amaren errire joateko
bosteun legoak bear dire,
bosteun legoak olan eiteko
orren errezak eztire.

- Untziko maisu gaztea
badezu kainabetea?
- Ezteukat kainabetarik baina
emongo deutsut saflea.

Emon zioten saflea eta
sartu zeuen biotzean
ordu atantxe ilotzitu zen
linde damatxu gaztea.

- Ene mutilak, lagunek onak,
zer ingo'tsegu oneri?
- Intzentzoagaz intzentzatu te
erainotzagaz lurrundu,
linde damia zazpi astian
geurekin erabil daigun.

- Ene mutilek, lagunek onak
zer ingo'tsegu oneri?
- Emon deiogun zazpine mosu
ta bota deiogun ureri.
Emon zioten zazpine mosu
eta bota zioten ureri.