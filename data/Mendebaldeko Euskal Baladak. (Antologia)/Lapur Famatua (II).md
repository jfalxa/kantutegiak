---
id: ab-4198
izenburua: Lapur Famatua (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004198.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004198.MID
youtube: null
---

Orain kantu berri bat
nai nuke itz egin
munduko kriaturen
eskarmentugarri.

Trantza onetantxe
guztiz nago larri
neure pasadizue
esan barik garbi.

Bengantza onetantxe
gau triste batian
puñala ta trabukoa
arturik aldian.

Ondo loturik ninoiela
esanik atian
derrepente sartu nitzan
beraren etzian.

Apaltzera zegozan
senar emaztiak
ezkonberriak eta
ziraden gaztiak.

Ezosten kuidadurik
emoten iltiak,
suberte onari begira
an egozan tristiak.

Neronen ezkongeia
lenango iltzan ori
begirazko egoan
begira neroni.

Bost puñalada nitzaion
eman lehenago berari
ez "Jesus"ik ez "amen",
joan zan mundutik.

Senar tristia egoan
andriari begira
baina onek be bazeukan
ganian erida.

Berari tiratu nion
petxuaren erdira
orla zer pasatu zan
esanda egia.

Senar eta emazte
sukaldian biak
an gelditzen ziraden
derrepente ilak.

Ez izan balirade
sanoak eta finak
orra zer subertie
gaizki dabilenak.

Neuri itza emonda
besterekin ezkondu
orrek emoten dost neuri
biotzien damu.

Auxe zan motibua
baldin izan badu
orregaitik biar dogu
orain biok galdu.

Azino au eginda
urten nuan etzetik
justezie altoka
banuan atzetik.

Ni gelditu ta berriz
tiratu gogoti,
lau soldadu tiro baten
an zidaden jatzi.

Gaua pasatu eta
urrengo egunsentian
gizon bat topa nuan
kaminoaren erdian.

Apuntatzen ninoela
neure trabukoakin
bajeu biar izan neuan
zaldi aren ganetik.

Itxi egin deitzela
diru ta zaldie
egin gura ezpaeban
munduko aldie.

Zaldi ganian noa
mutil armatua
abidatu an nitzan
lapur famatua.

Atzetikan banuan
nok arrapatua
baina bilatzen nuan
non ezkutatua.

Partida bat zebien
gau eta egun
enemigua franku
banuan ingerun.

Alan da be deskantzurik
eneukan inun
gainera bi soldadu
neronek il nitun.

Urtebete osoa
beti lapur eginik,
batzutan kaminoan
bestietan mendin.

Lerida pasatu eta
urrengo errian,
ze obra egin nuan
nik Kataluñan!

Bost lagun bizi ziran
euren famelian
bostak il nituzan
gauaren erdian.

Pekatu zala ere
nik enuan uste
biotzari jartze'naz
parkazino eske.

Aitek biraldu eusten
neuri notizie
ez einkiela berak
asunto guztie.

Jausirik egoela
neure sententzie
arrapatu ezkero
kentzeko bizie.

Abisu au artuta
urrengo egunian
kontrario bat franku
baneukan aurrian.

Zaldia be ibili zidaden
il biarrian
zortzi soldadu tiratuten
zidaten aurrian.

Bi balazo nituzan
esku ta buruan,
odola ba zidaden
itur bi moduan.

Alan da be trabukoa
disparatu nuan
gainera bi soldadu
eridu nituan.

Bi bakarra zidiaden
neuri oratu an
balentiak izen arren
umildadu nituan.

Amarratu ninduen
zaldiaren ganian
ondo lotsa nitzala
egunaren erdian.

Joan biar izan neban
an guztiaren erdian.

Ailegatu nitzanian
neronen errira
betorren jentie franku
neroni begira.

Ai neure orduko
lotsa ta larria
zergaitik egin dodan
ainbat pikardia.

Ezkontza bat motibo
izen da guztia
amairu pertsonari
kentzeko bizia.

Jangoikoaren ofensa
guztiz dot damu
zergaitik egin dodan
ainbeste pekatu.

Pekatarien ama
Birjina Maria
Jangoikoaren aurrerako
bitarteko andia.

Emon egidazu
zeure grazia
ondo konfesatzeko
biar dodan guztia.

Amaika ordu santuak
bete dire urki,
orduentxe ilteko
guztiz nago larri.

Ai nok euki leikien
arimie garbi
kontua emateko
gure Jangoikoari.

Adios mundutarrak
neure aita eta ama,
senidekaz batera
adiskide danak.

Akabatu dirade
neure munduko lanak
lagun egin daistela
gure Jangoiko Jaunak.