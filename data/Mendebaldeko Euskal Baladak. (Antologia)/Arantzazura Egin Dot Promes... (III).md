---
id: ab-4176
izenburua: Arantzazura Egin Dot Promes... (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arantzazure joan bear dogu
gabaz edo egunez.
Etxekoandreari eskatu deutsat
etxeko andriak agindu ez,
Birjina Ama onen borondatea bada
bitartekorik bear ez.