---
id: ab-4167
izenburua: Jaun Zuriano (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

- Kanzuriano, Kanzuriano,
lo bazagoz iratzarri.
- Enago ni lo, enago ni lo,
moru erriko moruori.
Nok esan deutsu neure izena
Truku erriko trukuori?

- Enaiz ni moru errikoa
gitxiago truko errikoa,
obetuago izango naz ni
zeruetako aingerua.

Jaun Zerukoak bialtzen nau ni
gaur zugana mandatari:
zure esposa doñe Errose
biar da espose barri,
biar da espose barri eta
biar goizerako eldu bedi.

- Emetik eta ene etxera
bosteun legue da bide,
bosteun legue da bide eta
nik arek zelan ebagi?

- Oriek ebagitiarren
zer emongo zeunkio zuk ari?
- Berreun kana urre-zintxa
Ama Graziakuri,
amalau libra argiziritxu
Andra Santa Ursulari.

- Orra or goian zazpi zaldi
euretariko bategaz
gau eta egun ebagi.

- Oles, oles, Dios te salve,
nik ostatua nai neuke.
- Emen ez dago ostaturik eta
beijue pobreori aurrera.

- Oles oles, Dios te salve,
nik ostatua nai neuke.
- Emen ez dago ostaturik eta
beijue pobreori aurrera.

- Oles oles, Dios te salve,
nik ostatua nai neuke.
- Egun okupaue daukogu te
beijue pobreori aurrera.

- Kanzuriano bizi zanean
pobreak emen baeben ostatua.
- Kanzuriano aitetu dau eta
betor pobreori aurrera.

Sillan ezarri erromesori
oinak garbitu
pañua artun da sikatu.

- Begiak begi badirudi
miñak guzurra esan ezpaledi,
gaurko gure erromes onek
Kanzuriano dirudi.

- Ixilik on adi!
batxillerrori ori!
Antxina il dau enterraute daukon
Kanzuriano eure ori!

Ortxe kaxan dago ezpatie
Kanzurianok itxie.

Gaurko ezkongai orrek
ataraten badau ezpatie.
Juan da ezkongaiori,
ez dau atara ezpatie.

- Ia gaurko erromes orrek
ataraten badeu ezpatie.
Juan da erromesa ta
atara dau ezpatie.

Pozez beterik doña Errosak:
- Zu zara senar neurie.