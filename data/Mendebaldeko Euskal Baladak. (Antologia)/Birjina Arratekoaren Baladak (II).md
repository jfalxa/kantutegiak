---
id: ab-4181
izenburua: Birjina Arratekoaren Baladak (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Birjina Arrateko
zeruko gloria
Zuriz da enkarnada
"graziaz betia". (bis)

Arrateko zelaiko
bai floridadia
Andixen gora dago
zerura bidia.

Zerura gura neuke
zerura bolaru
Angeruak balidare
eguak prestaru.

Eguak balidare
argizarizkuak
Urtuko lituzkela
eguzki beruak.

Bai eta ausi ere
ipartxo senduak
- Aingerua, zer dakatzu
zeruko gauzarik?

- Oliba adartxo bat
Birjina emanik,
Erramu egunerako
bai seinalaturik.

Arraten altaria
erramuz jantzirik
lirio gorririk eta
lirio berderik.

Antxen kantatzen daue
Salbia ederki...
neu bere joango nintzake
ikastiagatik.

Bere Seme Jauna dago
mezaren ematen
Maria Santisima
meza adoratzen.

San Pedro ta San Paulo
meza erazoten.

Mila ta bat aingeru
koruan kantatzen
San Miel aingerua
organua joten.

Gabian Salbia ta
goizian mezia,
Birjina Arrateko
zeruko gloria.