---
id: ab-4200
izenburua: Markesaren Alaba (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004200.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004200.MID
youtube: null
---

Zeruak eta lurrak
egin zituena
memorie argitzen
nator neu zeugana.
Esperantza badeukat
berorrek emana
kunpliduko dedala
deseo dedana.

Amodiozko penak
berso berrietan
publika (i)al banituz
Euskalerrietan.
Enteratu naiz ongi
enkargu onetan
San Jose arratzaldeko
iru t'erdietan.

Markes baten alaba
interesadia
marineruarekin
enamoradia.
Deskubierto gaberik
bere sekretua
amorioa eukan
barruan sartuta.

Egun seinale izan zan
gogoan artzeko
esan ziola (i)arren
etxera joateko:
- Deseo dedan itza
manifestatzeko
zeurekin Antonio
nago izateko.

- Zer esaten didazu
Juanita itz ori
tentatzen asi zariala
traza bat irudi?
Etxat zeure gradurik
tokatutzen niri,
ez burlarikan egin
marineruari.

- Iduki dezakezu
ondo sinestua
aspaldion neuala
zeugaz gustatua.
Ez zaitut neuk itxiko
desanparatua
ala egiten ditzu
geur juramentua.

Konformatu ziraden
alkarren artian
ezkonduko zirala
urrengo urtian.
Eskolak ikasteko
bien bitartian
beraren erritikan
guztiz apartian.

Alan disponiturik
jarri ziran biak
kartaz entenitzeko
alkarren barriak,
formalidadeakin
jartzeko egiak
baina etziran lo egon
amaren begiak.

- Alperrik izango dira
asarre geuria
ez nabe mudatuko
eternidadian,
esposatu nai nuke
kariño onian
Antonio Maria
etortzen danian.

Ezin egon zan ama
itz ori sufritzen
berean ala asi zen
kartak detenitzen
intentzino baldiagaz
ezkontza galduten
Juanitak alakorik
ez zuen pentsatzen.

Amaren malezie
korreora joanda
Antonio il zala
egin zuen karta.
Juanitaren tristura
maitia izanda
amak engaine zuen
guzur bet esanda.

Amak esaten dio:
- Juanita neuria
galdu da biotzeko
Antonio Maria,
neuk bilatuko ditzut
beste bat obia
maiorazko interes
askoren jaubia.

- Ama ez esan neuri
orrelakorikan
ez dot nik bestegana
amoriorikan
ezin alegra leike
neure barrurikan
komenientzie onak
egonagaitikan.

- Itxi alde batera
orrelako lanak
ez ditzut nik ikusten
zu bezela damak,
nai badozuz artu
onrak eta famak
giatuko zaituze
aitak eta amak.

Denpora kunpliturik
galaia abian
zer pasatu ete zan
aren memorian.
Kartarik artu barik
joan dan aspaldian
inozente sartu zan
jaio zan errian.

Au da lendabiziko
esan genduena:
- Zer da musikarekin
onratzen dabena?
Markesaren alaba
kalian barrena
esposario zala
arek biar zuena.

Desmaiaturik egin
zuen ordubete
gero nobia eske
itz bi egin arte,
inguratu zirakon
makina bat jente
bigarren ordureko
il zan derrepente.

Gaua pasatu eta
urrengo goizian
entierrue zuen
bigarren klasian,
markesaren alaba
guztian atzian
ze pena izango zuen
arek biotzian.

Penarikan lerturik
Antonio il zan
akonpainatu zuen
Juanitak eleizan,
maitasuna bazion
esan dedan gisan
gerostikan ez zuen
osasunik izan.

Erremedio balei
sentimentu ori
bitarteko bat jarri
Jesus maitiari,
orazino eginaz
Birjina amari
zeruan gerta deidan
Antonio Mari.

Alkarren konpainian
gure bizi nai du
Birjina egizu
Jaunari erregu,
kristinau guztiogaitik
bardin al bazendu
Iturrionak orrela
desiatuten du.