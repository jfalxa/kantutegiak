---
id: ab-4160
izenburua: Peru Gurea
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Londresen dot neuk senarra
zirin-bedarrak ekarten
bera andik dan artean
gu emen dantza gaitezen.

Oi au egia! Daigun jira bi Maria.

Kapoian dagoz erreten
oilaskotxuak mutiltzen,
orrek ondo erre-artean
gu emen dantza gaitezen.

Oi au egia! Daigun jira bi Maria.

Neure oilanda nabarra
txikarra baino zabala,
zetan joaten az auzoetara (14)
etxean oilarra donala?

Oi au egia! daigun jira bi Maria.

Neuk ugazaba nekusan
Izurdiako zubian
orra zestorik (15) urten artean
gu emen dantza gaitezan.

Oi au egia! Daigun jira bi Maria.

Neure mutiltxo txoria
ik esan eustan egia:
gaur gabean dantzauko zala
nire emazte Maria.

Oi ai egia! Iretzat mando zuria.

(14): zetan oa i auzora.
(15): ori zestorik.