---
id: ab-4148
izenburua: Bart Amarretan (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004148.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004148.MID
youtube: null
---

Bart amarretan ezkongai nintzan
amabietan ezkondu
ordu bi santu goizekuetan
gelditu nintzan alargun.

Auxen da pena eta tristura
pesadunbria ganera
ordubeteko senarragaiti(k)
alargun gelditutzia.