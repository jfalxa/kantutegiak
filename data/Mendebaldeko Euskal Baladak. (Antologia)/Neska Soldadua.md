---
id: ab-4203
izenburua: Neska Soldadua
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Gaztiak kantuan ta
zarrak eragusian
ori da pasatzen dana
bardin mundu guzian.

Etxetik urten neban
Mariatzeko ilian
amasei urterekin
neure txoramenian.

Soldadu sartu nintzan
Agorraren bostian
kabo ero sarjento
izateko ustian.

Ni lako fortunia
notu (56) arrapatu?
Amaika ilabeteko
sarjento nonbratu.

Urtebete baino len
nintzan teniente
rejimentutikan be
nintzala ausente

Rejimentu bi izan ditut
amar urte onetan
Angel de Ondasetan
Buenas Airasetan.

Nerau emakumia
ofizialia
letrarik eskolarik
batere gabia.

Agustin Antonio
izan da neure izena
Ori aitu dezutenok
ezta neure izena.

Oraintxe da denboria
izena muratzeko,
Agustina Antonia
naizela esateko.

Batetik lau balazo
besterik enfermo
Jaunaren abisuak
oixek ditut klaro.

Moja sartzera noa
komentu batera
Jesusaren esposa
betiko izatera.

Amar urtian errege
serbitu detalako
doria gertu daukat
moja sartutzeko.

Adios ama neuria
probintziania
ongi lastimatzen naiz
zu despeditzia.

Baina oraindik ere
bizi naizen artian
gogoan izango zaitut
neure biotzian.

(56): nok du-ren forma ilundua.