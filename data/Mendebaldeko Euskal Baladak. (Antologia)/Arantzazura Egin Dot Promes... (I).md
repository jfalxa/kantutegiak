---
id: ab-4174
izenburua: Arantzazura Egin Dot Promes... (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arantzazura egin det promes
edo gau edo egunez,
edo gau edo egunez eta
oinutsetan ta dolorez.

Goazen Arantzazura
Santa Luziaren begi ederra
argi egiten digula
(gau on Jaungoikoak digula).

Izar eder bat ateratzen da
urtian egun batean,
urtean egun batean eta
ura San Joan goizean.

Arren argitan ni joan nintzan
Arantzazuko bidean,
Birjina Ama ta bere Semea
topa nituan bidean.

Konsejo on bat emon zidaden
Ama semeon artean:
ona ta umila izan nendila
munduan nintzan artean.
Berak jakinen zuala gero
zer egin al zezakean.

- Artzai ontxua, artzai ontxua,
nere esana egizu:
Aita zurea arotza da-ta
ermitatxo bat egidazu.

Nik zaituko'itut zure ardiak
Santa Triako aitzean,
Santa Triako aitzean eta
belartxi garbi batean,
belartxo garbi batean eta
elorri ondotxo batean.

Elorri orri para'izkiozu
bi pilartxo aldean,
bi pilartxo aldean eta
iru latatxo gainean.

Iru latatxu zazpi teilatxu
deritzala Arantzazu.
Denborarekin au izanen da
ermita kuriosoa,
ermita kuriosoa eta
konbentu poderosoa.

Asko amaren seme maitetxok
au izango du ostatu,
au izango du ostatu eta
bai kaliza goratu.

Birjina Ama Arantzazukoa
eguzki aldera leioak,
bata itzali dakionean
beste aldera dijoa.

Birjina Ama Arantzazukoa
urre gorriaz koroa,
urre gorriaz koroa eta
ziar zuriaz lepoa.
Zilar zuriz lepoa eta
seda dindilaz beloa.

Berak iruin, berak eundu
eta berak soinean daruka.

Zazpi donzeilak josi omen dute
Birjina Amaren belua.
Zazpi donzeilak ederrak ziran
belua ederragua.

Birjina Amaren madal santutik
iturri eder bat dijua.
Egunean bein au ura edanda
uso zurie dijua...
Ai ura ezta uso zurie!
Aingeru gaurdiakoa!

Berak kontuan artu gaitzala
anima gorputzekua.
Zori onean jaiorik dago
arren jabunaduria,
arren jabunaduria eta
Maria Magdalenea.

Superbo gaiztoz beterik dago
inpernuko sulezea (?),
umildadeak idikitzen du
Zeruetako atea.