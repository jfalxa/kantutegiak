---
id: ab-4197
izenburua: Lapur Famatua (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004197.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004197.MID
youtube: null
---

Berso berrian batzuk
bear ditut jarri
neure lagun guztien
eskrementugarri.

Tranze estu onetan
guztiz nago larri
neure pasadizua
esan gabe garbi.

Ai neure kristanaua
aditu kontuen
neure deskalabrua
nola dan munduen.

Amairu eriotza
urtebeten barruen
neskatxa bat motibo
nola egin nituen.

Benganza batarekin
gau triste batian
puñal eta trabukoa
arturik aldian.

Ondo lotuta nenguan
etsaian atian
derrepente sartu nintzan
beraden etxian.

Afaiten an ziraden
senar emaztiak
ezkonberriak eta
ziraden gaztiak.

Etzioten ematen
penarik ilteak
suerte onari begira
egozan tristiak.

Bos(t) puinalada lelengo
emon nion berari
ez Jesus eta ez amen
juan zan munduti.

Senarra triste zeguan
andreari begira
baina ezi'otsen kendu
zeukaten erida.

Berari tirau nion
petxuaren erdira
orra zer pasatu zan
esan det egia.

Senar eta emazte
sukaldian biak
an geratu ziraden
derrepente ilak.

Ez izan ein balire
sano ta abilak
etziran geratuko
derrepente ilak.

Neuri itzak emanda
besto batekin ezkondu
orrexek ematen dit
biotzean damu.

Utsa zan sikiera
motiboa izan balu
orain beragaitino
biok bioz galdu.

Azino au eginde
urte'naben etxeti,
justizia altoka
banoan ateti.

Jiratu ta tiratu
nik barriz gogoti,
lau gizon tiro bataz
an ziraden jausi.

Kristauaren biotza
ikeragarria
Jaunak bere eskuti
nendukan utzia.

Amairu personari
kentzeko bizia
neskatxa bat motibo
izan da guztia.

Zaldiaren gainian
gizon armatua
abiatu nintzanian
lapur famatua.

Utzi nai bazuan bertan
diru ta zaldia
bestela egingo diola
munduko aldia.

Urtebete osuan
beti lapur egin
batzutan kaminoz da
beste(i)etan mendiz.