---
id: ab-4173
izenburua: Agostuaren Amabostgarren... (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Agustubaren amabostgarren
Jesus Maria goizean,
Birjina Amaren debotatxo bat
Arantzazura zijoan.

Oinetan ere zapata gabe
abitu beltza soinian,
San Frantziskoren kordoetxo bat
gerrian iru doblian.

San Agustinen liburutxo bat
eskutxo bien erdian.
Pausua baratz botatzen zeban
Urtiagako gainian.

Ala bere ta baratzagua
San Eliaren parian.
Aingeru batek irten zioskan
San Eliaren parian.

Salbetarako elduko zala
Kurtzepizio gainian,
Kurtzepizio gainian eta
Birjina amaren etxian.

Erregu eder egin ziozkan
Birjina Amari aurrian,
Birjina Amari aurrian eta
Santiagori oinian.

Moroerriko katibu danak
libra zitzala aidian.
Moroerritik banator ere
neu ez naiz moro umia.