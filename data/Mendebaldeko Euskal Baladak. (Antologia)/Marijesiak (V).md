---
id: ab-4188
izenburua: Marijesiak (V)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Belelengo urian
gabon gabaren erdian
Birjina Santisemia dago
ondo graziaz beterik.
Ai Belenen, bart Belenen
jaio da Jesus Nazaren.

Andixik gora ninoiela ta
ospiteletxu zar baten
aite San Jose topatu neben
astotxu bere zar baten.
Ai Belelen,...

Korraletako ardiak bere
jantzan ebiltzen orduen
salbadorie jaio zala
Belenengo urien.
Ai Belelen,...

Jesus dibino! Gauaren otza!
Jausten dan inetasie
ordutxu bien onelan bada
il da ganadu guztie.
Ai Belelen,...

Pastore santu birtuz gozoa
gaietu zakiguz gaur emen
bart arratsian zer pasatzen dan
kantatu daigun gaur Belenen.
Ai Belelen,...

Nazatereko tenpluen dago
dama eder bat jarririk
izena bere Maria dau te
ondo graziaz beterik.
Ai Belelen,...