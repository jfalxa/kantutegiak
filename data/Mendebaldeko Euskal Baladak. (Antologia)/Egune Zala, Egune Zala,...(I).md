---
id: ab-4168
izenburua: Egune Zala, Egune Zala,...(I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Egune zala, egune zala
bart gauberdie zanean
egunerako allegau ginan
zazpi legua bidean.
Birginea ta bere semea
topa ginduzen bidean
emaoskuen konseju on bat
Ama Semien artean
ondo umil da ona izeteko
munduen garean artean.

Juan ginean Arantzazure
baina beti bidean
pastoretxo bat topau genduen
arbola baten gainean.
- Errodrigotxu Baltzategiko
erdu ona ta entzuizu!
Aita zuria arotza da ta
arren esan bat eidazu:
Egin deiela ermitatxo bat
deritxona Arantzazu.

Iru latatxo zazpi tellatxo
nunbaisten askoko ditu,
hamaika amaren seme on (?)
an izengo dau ostatu,
San Franziskoren semeak bere
antxe kalize altzadu.

- Oñatiarrak, oñatiarrak
gazkiro-gazkiro penatzen nozu
Aite San Miguel Oñatikora
birriten erona nozu,
irugarrenez banaroazu
gomuten eukiko dozu.