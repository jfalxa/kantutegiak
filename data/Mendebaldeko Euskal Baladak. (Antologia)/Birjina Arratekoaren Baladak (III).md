---
id: ab-4182
izenburua: Birjina Arratekoaren Baladak (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arrateko zelaiko bai probidaria
andikan gora dago zerura bidea.
Ai nere jende onak zerura noa ni
ia nork gura duan nerekin etorri.
Atzo etorri zaizkit iru mandatari:
uso zuri eder bat, aingeru laztan bi.

- Uso zuri ederra, zeruan ze berri?
- Zeruan berri onak orain eta beti.
- Neretzat zer dakarzu zeruko gauzarik?
- Olibo adartxo bat Birjinak emanik.

Erramu laster da-ta igertuko ezpalitz...
- Ezta bada igartuko zeruko gauzarik.
Igartzen bada ere berriz emango dit,
zeruan frango dago olibo-adarrik.