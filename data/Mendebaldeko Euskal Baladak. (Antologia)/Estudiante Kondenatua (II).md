---
id: ab-4196
izenburua: Estudiante Kondenatua (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Estudiantie naz
ondino gaztie
amazortzi urte bere
kunplidu barik nik.

Iru peseta zala
kartazko jokoa
bertan ilde itxi neban
estudiante majoa.

Au da kalabazuko
ilun eta triste
emen sartu nintzela
dira sei-iru urte

Pozik paseko nekez
beste orrenbeste
libretako esperantza
baldin baninduke.

Iru anaie gara
Jangoikoak emanik,
bata zan apaiza ta
beste bi fraile bi.

Atzo etorri jatan
arreba monjia
errukia zen bezala
neu olan ikustia.

Orain edaten dot nik
basutik ardaua
seguru izengo dala
gure Jainko jaunaz
akordu ordue.

Kanpaiaren otsa da
agoniakoa
seguru izango da
urkamendikoa.

Au da urkamendiko
aizearen otsa
Jaunaren aurrerako
nik daukadan lots.