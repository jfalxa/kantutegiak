---
id: ab-4192
izenburua: Orbelak Airez Aire (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Zeruan aingeruak
dabile gerria,
nok kantauko ete dau
Birjinan kantia?

San Miguel aingeruak
boz alegeriaz,
arexek kantauko dau
Birjinan kantia.

Orriak aidez-aidez
ipartxu dultzia,
Landamintz ederretik
doan errekia

Antxe topatu neban
Birjina Maria
orraztuten ebala
buruko ulia.

Etzan a ulia ez
ezpada urria,
ulondo bakotxetik
zerion perlia.

Nor ixango ete da
orrixen joslia?
Errege don Pilipen
alaba lindia.

Arratetik gorako
zelai eder baten,
zeruan Jesukristo
mezia emoten.

San Pedro ta San Paulo
mezia erasoten,
aita San Prantziskua
orgañua joten.

Amabi apostoluak
koruan kantatzen
mile ta bat aingeru
mezia entzuten.