---
id: ab-4202
izenburua: Tejeriaren Bertsoak
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004202.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004202.MID
youtube: null
---

Jose Tejeria da
au nere grazia
Gipuzkuako Aian
jaio ta azia.
Diruz aita il neban
orra desgrazia
publikatzera noie
ausento guztia.

Diabruek animue
emon biotzera
tiroz atrebitu naz
neure aita iltzera.
Gero Donostiara
zerbait ekartzera
egin neban okerra
disimulatzera.

Gizonak altxa zuten
aita bizirikan:
"O! Semiak galdu nau"
berak esanikan.
Etzuen gurasuak
esan gezurrikan,
gorputza il, da anima
juan mundutikan.

Paperian jarrita
alde aurretikan
aitari zenbat emon
señalaturikan.
Eske ori zan beti
aldamenetikan
gure enredo danak
sortuek ortikan.

Aitak eskatzen eustan
legez eta bidez
egiten niona zan
agindu t'emon ez.
Burue dana beteta
pentsamentu txarrez
tirua emon nion
diruaren ordez.

Soldadu armadunak
lotu kurioso
baita eraman bere
Azpeitira preso.
Deklarazio txarrak
emonda nik oso
libratzeko biar zan
gezurra preziso.

Aurreta egon nintzan
uka fuertian
salatzailie pranku
bazan bitartian.
Gero autortu neban
egunen batian
orrek guztiok ziran
neuretzat kaltian.

Andriak eta biok
emondako itza
konprenditzeko bere
oso dire gatxak.
Orrek guztiok ziran
gezurrezko saltsak
aitari ezarria da
zentzituen mantxak (?).

Gaizki egin nebala
sarri damutu jat
au eskarmentureko
beste guztientzat.
Munduan ez da izan
oraindikan beintzat
ume bat txarragorik
gurasuarentzat.

Donostian artue
nintzen albistie
etzala posible neuri
bizirik istie.
Ai neure orduko
lastime tristie
etzan asko alegrauko
nire emaztie.

Donostiatik ginan
Azpeitira etorri
bost guardiarekin
kotxien ekarri.
Kartzelan sartu eta
an nenguen larri
sentenzia triste
zuen irakurri.

Zuzen da garbi egin
eben justizia
badakit nuela nik
ondo merezia.
Ilbeltzaren amaseien
neure sentenzia
goizeko zortziretan
kentzeko bizia.

Gaiztua izen banaz
orra sujetatu
sentenzia aurre ziran
biotza erdiratu.
Aita Espurorekin (55)
gogoz konfesatu
egindako okerrak
danak akusatu.

Pausu bat ezin emon
neban ezergaitik
bi lagun banituen
beso banatatik.
Urkamendira juen
ginen kartzelatik
borreruak begira
egozan atzetik.

Urkamendian nintzan
eserita jarri
neure entraina zeuen
estu ta larri.
Bizia emon biar
oso lotsagarri
au beste guztientzat
eskarmentugarri.

Parkaziua neban
azkenez eskatu,
zergaitik egin neban
lenago pekatu.
Origaitik biar dot
nik orain urketu
Jangoikuari arren
nitzat erregutu.

Amabi ordu santuek
ie dire aurki
il biar dot eta
guztiz nago larri.
Bizia emon biar
geure Jesus onari:
adios mundutarrak
neure aite ta amari.

Neba-arrebakin batian
adiskide danak
akabatu dirade
nire munduko lanak.

Parkaziua neban
azkenez eskatu
zergaitik egin neban
lenago pekatu.
Origaitik biar diot
nik orain urketu
Jangoikuari arren
nitzat erregutu.

(55): "Aita Aizpururekin" bertso idatzi ezagunetan.