---
id: ab-4169
izenburua: Egune Zala, Egune Zala,... (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Egune zala ta egune zala
bart idargie zanien,
egunerako allegeu nintzen,
zazpi legua bidien.
Zazpi legua eginde bere
Arantzazure ninoien.
Birjinie ta bere semie,
topeu nituzen bidien.

Emon eustien kontzeju on bat
Ama-semien artien:
Izen nendile on da umilde
munduen nintzen artien.
Enenduela gero damuko
zeruko tribunalien,
zeruko tribunalien eta
Jaungoikuaren aurrien.