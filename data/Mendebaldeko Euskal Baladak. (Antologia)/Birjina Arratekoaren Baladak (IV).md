---
id: ab-4183
izenburua: Birjina Arratekoaren Baladak (IV)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Aldatza igaro-ta
zelai landa baten
an dago Jesukristo
mezia esaten.

San Pedro ta San Paulo
mezia erasoten,
amaika mila aingeru
koruan kantaten.

Aita San Franziskua
organua joten,
amabi apostoluak
mezia entzuten.

Arraten altarea
eramuz jantzirik,
lirio zuririk eta
lirio gorririk.

Aintxe kantaten dabe
Salbea ederki,
neu bere araxe noa
ikasi al benegi.

Gabean Salbea ta
goizean mezia,
Birjina Arrateko
zeruko loria.