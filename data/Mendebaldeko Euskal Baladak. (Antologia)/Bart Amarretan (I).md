---
id: ab-4147
izenburua: Bart Amarretan (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Bart amarretan dontzella nintzan
amaiketako senarra
gaubeko amabi santuetarako (7)
gelditu nintzan bakarra.

Sentimentua, sentimentua (8),
andiago penea (9):
ordu beteko senarragaitik
alargun gelditutea (10).

(7): santuetako
(8): atsekabea, atsekabea
(9): naibagea
(10): izan beharr.