---
id: ab-4146
izenburua: Ana Juanita (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Itsesontzuan nebilenian
Londresgo ziudadian
neure anaje topatu neban
an ebinezen artian.

- Neure anaje ze barri andi
Motriku erri onetik?
- Aite ta ama osasunegaz
Ez da barri onik andik.

Ana Juanita ezkontzen dala
errien publikatu de.
- Ezin ezkondu Ana Juanita,
neugaz dau konformatute.

Batak "agur" de besteak "adios",
Ana Juanitak bapere.
- Ana Juanita neure laztana,
eerien zer pasatu de?

Errien erbestetik datorrenari
agur eitea usu de.
- Agur eitea usu da baina...
Orain usue galdu de.

Zaldun gaztia kendu zakidez
neure begiaren aurreti,
neure senarra begira dauket
untziko bentanileti.

- Ana Juanita, neure laztana,
senardun bere bazara?
Lantza zorrotz bat neunken eskuan
jaurti neuntzen bularrera.

Lantzeau bere ain zen zorrotza,
bazan odola lurrera.
- Zaldun gaztia il nozu eta
enterradu egidazu.