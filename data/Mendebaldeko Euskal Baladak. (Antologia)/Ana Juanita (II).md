---
id: ab-4145
izenburua: Ana Juanita (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Untzi bi agertu zirian
Santamanterion parian
nere anajia atorriko da
batian edo bestian.

- Neure anjia ze barri dezu
Motriku erri onian?
- Berriak onak, berriak txarrak
Ana Juanitan gainian.

Ana Juanita ezkondutzen da
amaren da aitaren bozian,
amaren da aitan bozian eta
bere desborondatian.

- Despedidako gabian bere
txorruetara ginian
urragorrizko joia bere
imini notzan bularrian.

Batiorrek agur, besteorrek agur,
alan despedidu ginian.
Eguna be izan seinaladua
Santa Luzia gabian.

- Galai gaztia neiku al dezu
gab'onetako koplarik!
Nere senarra daukat entzuten
kuartoko bentanilati.

Zure semia besuan daukat
senarra aldamenian
orain tertzio txarra du eta
erdu tertzio onian.

Orain il bere ein nozu eta
enterra naizu lurrian
enterra naizu lurrian bere
Birjina Marian aurrian.

Urragorrizko iru lau letra
imin'idezu sepulturian.
Arek letrak esango dio
zegaiti nagon ni lurrian.