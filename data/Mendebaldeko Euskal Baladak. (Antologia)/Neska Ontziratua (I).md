---
id: ab-4158
izenburua: Neska Ontziratua (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Arantzazure joan bear dot
gabean edo egunez
Birjina Amoni preguntaduten
izango bada edo ez.

Birjina Amonek baietz dino ta
seme jaunonek entzun ez
bioren borondatea balitz
bitartekorik bear ez.

- Grazianatxu naure laztana,
zugana biraltzen naude,
gaur afariten etorritia
egin bazeinke mesede.

- Neure lagunok bai marinelak,
enaz ni afari gure.
Afari barik konsoladute
dau (12) neure estamangue.

Lobedartxu bat ifini nion
bere bularren erdien,
mila legua pasadute be
lotan topadu zebien.

Ontzitxo onek jo abenian
itxasoa ta mastria
orduterantxik irintxarri zen
Grazianatxu nauria.

- Neure lagunok eraman neizue
aitaren d'amen errire.
- Mila legua eginde dagoz
zure aiten da amen errire.

- Grazianatxu neure laztana,
berandu akordau zara,
mila legua pasadute dauz (13)
zure aiten d'amen errira.

- Neure lagunok bai marinelak
ateau idagi dezu,
mila urtean ezin dodana
oraintxe dot nik logratu.

- Deabru gaiztoak eraman deiela
dendari onen jostura!
Nonbaisten bere esturik dago
neure soineko txamarra.

Eskatu zeituen tijeratxuok
askatuteko jostura,
sartu zeituen tijeratxuok
biotzetikan barrura.

- Neure lagunok bai marinelak
oin oni zer ein geineio?
Belatxu bati amarradute
ureri emon geineio.

(12): dago
(13): dago.