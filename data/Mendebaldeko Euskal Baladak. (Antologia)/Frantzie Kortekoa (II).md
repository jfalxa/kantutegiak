---
id: ab-4151
izenburua: Frantzie Kortekoa (II)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004151.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004151.MID
youtube: null
---

Etorri nintzanian
Frantzie parteti
ama topadu neban, ama topadu neban
etzian bakarrik.

- Zer egiten dau ama
orrelan tristerik?
- Nobedade barriek, nobedade barriek
entzun dodaz bai nik.

- Ze nobedade entzun dau
bai ama neuria?
- Ezkonetan zariala, ezkonetan zariala
bai ene semia.

- Nondikua al dakazu
zeure emaztia?
- Frantzie partekoa, Frantzie partekoa
bai ama neuria.

- Ez dot gure frantzesik
ain gutxi errenik.
Egin dodau promesau, egin dodan promesau
konpridu biot nik.

- Ze promes dauko ba ama
konpridu eiteko?
- Joan bidera eta, joan bidera eta
andria ilteko.

- Ez da inposible (sic) ama,
nik ori egitia,
joan bidera eta, joan bidera eta
andria iltia.

Ain de ederra eta
ain de galanta be,
ez da inposible ama, ez da inposible ama
nik ori egitia.

- Zau ezpazara kapaz
artu eizu anajia,
Anajia ezpada kapaz, anajia ezpada kapaz
estudiantia.

- Joanita jatsi zate
zaldi orren ganeti
sartu deizuden puñela, sartu deizuden puñela
biotzen erditi.

- Ai orren puñelorren
puntiaren zorrotza!
Ikusiak beste barik, ikusiak beste barik
ilten dust biotza.

Hamalau mando dodaz
urre gorriz kargaurik.
Aik emongo'utzudez da, aik emongo'utzudez da
ni itxi bizirik.

- Aik artuko'odaz baie
ez itxi bizirik
lelengo puñelkadan, lelengo puñelkadan
zeartau bagarik.

- Abadea ekarrizu
konfesaduteko
neure arima tristia, neure arima tristia
konsoladuteko.

- Abadea urrin dago,
elexea urrinego;
orrelan ibilteko, orrelan ibilteko
asti gitxi dago.

- Zazpi izera dodaz
odolez mantxaurik,
zortzigarrentxu onek, zortzigarrentxu onek
naroia ni mundutik.

Eleizan kanpak dire
eztakit zegaiti
igual izingo dire, igual izingo dire
neure eriotzeagaiti.