---
id: ab-4199
izenburua: Lapur Famatua (III)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Orain verso verribac
nai nuque nic (29) jarri,
munduko kriaturen
eskarmentugarri.
Trantse (30) estu ontan
gustis nago laarri,
nere pasadisua
nola esan garbi.

Ya nere kristauak
aditu contuan,
nere descalabrua
sela dan munduan;
amairu eriotsa
urte bete barruan,
nescacha (31) bat motibo
nola egin ditudan.

Oguei ta bost urte
dauskat oraindik (32)
orain daño isan nais
beti neskasari (33);
eguinduen alperrik (34)
gurasoak asi,
estien (35) askenian
eman pena gichi.

Jaunak bere eskutik
nenduan (36) utsia,
neskacha bat motibo
isandan gustia,
amairu personari
kentseko bisia.

Neri itsak eman da
bestiarekin eskondu,
orrechek (38) ematen dit
biotsian damu;
utsa san sikiera
motibua isan balu,
beragaitik biar degu
orain biok galdu.

Benganza orrekin
gau triste batian
punal eta trabuko
arturik aidian;
ondo loturik nenguan
etsaien katian,
derrepente sartu ninzan (40)
beraren echian.

Afaltsen a'sidaren (41)
zenar emastiak,
escomberriak (42) eta
sidaren gastiak;
echioten ematen
penarik (43) ilchia
suerte onaren begira
seuazen tristiak (44).

Bos puñalada emanda
lenengo berari
es Jesus es amen
juansan mundutik (45),
senar tristia seguan (46)
andriari beguira,
baña esin kendu
sendukan erida.

Berari tiranion
pechuaren erdira,
orra ser pasatusan
esandet (47) egia;
senar eta emastia
sukaldian biak (48),
an guelditu siden
derrepente illak.

Es izan bazidaren (49)
sano eta finak,
orra ser suertia
gaiski dabillenak.
Asiño au eguinda
irten nuan echetik
justisia altoka
asi nuan echetik (50).

Jiratu ta tiratu
nik berris gogotik
lau gison tiro bates
an siran erori,
orra sei eriotza
gau batian egin,
animak nora ziran
nai nuke nik jakin.

Kristauaren biotsa
icaragarria,
gisonari ematen
alako larria;
utsi zezala bertan
diru ta saldia
egin nai espasuan
munduco aldia.

Saldiaren gañian
gizon armatua,
abiatu nitsanian
lapur famatua;
atsetican banuan
nork arrapatua,
baña billatsen nuan
nun escutatua.

Partian (52) an sebillen
gau ta (53) egunian,
enemigua franco
banuan inguruan;
ya descansurican
ez nenducan iñon,
ala ere saspi soldadu
nerokin (54) il nitun.

Urte bete osuan
beti lapur egin
batsuetan caminos
bestietan mendiz;
ainbeste eriotza
ganera egin,
ezdakit ser gertatu
biardan nerekin.

Aitak bialdu siran
nere neticia,
sekiela besela
asunto gusia;
erabakia zala
nere sentencia,
arrapatu eskero
kentseco bizia.

(29): Ezkuizkribuan (E): nit
(30): E: transtse
(31): E: necacha
(32): E: oraindit
(33): E: nekazari zentzuaz
(34): E: alperrit
(35): E: ez ziran, ez eustan zentzuaz.
(36): E: nenduak
(37): E: zan zentzuaz
(38): E: orrechet
(39): E: bera gak
(40): E: ninan
(41): E: asiraden, an ziraden zentzuaz.
(42): E: es combreriak
(43): E: penarit
(44): E: tristia
(45): E: mundutit
(46): E: seugan
(47): E: dot, dakar ganean idatzirik
(48): E: biat
(49): E: bakidaren
(50): E: Errakuntza bat izango da? Ez da izango "Banuan atzetik".