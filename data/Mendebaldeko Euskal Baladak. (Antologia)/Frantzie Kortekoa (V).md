---
id: ab-4154
izenburua: Frantzie Kortekoa (V)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Atzo etorri nitzen
Frantziako kortetik
ama topatu nuan
etxian bakarrik.

- Zer eiten dozu ama
orrela tristerik?
- Nobedade barriak
jakin ditudaz nik.

- Ze nobedade barri
jakin dozu ama?
- Ezkondu ein ei dala
bai neure semia.

- Ez dozu entzun ori
ain gezur andia,
ortxe atzian dator
nire emaztia.

- Prantzes ala ingles da
zure emaztia?
- Prantzieko kortekua
da nire emaztia.

- Ez dot bier prantzesik
ain gitxi errenik,
artu'otan promesa
kunplidu biot nik.

- Zer eingo deutset ba
da ama neuria?
- Urten bidera eta
il, ene semia.

- Ez da posible ori
nik egitia,
ain da eder ta galant
nire emaztia!

- Zeu kapaz ezpazara
artu anaia
anaie gainera
estudiantia.

Estudiantia ta
ondino gaztia
ogetazazpi urte
kunplidu gabia.

- Batoi zaituet senar
ta bestioi koinatu,
nundik nora naroizuen
esaidezue klaru.

- Bajau zaitez koineta
manduorren gainetik
sartu daizudan puñala
biotzan erditik!

- Ai orren puñalorren
zorrotzen zorrotza
ikusiaz beste barik
ilten deust biotza!

Zazpi aiztatxu dodaz
Prantzieko kortien
arek be balekie
ni zelan nabilen.

Zabalduko (leukie)
Frantzieko kortien
Frantzieko kortien da
España guztien.

Aita bizi'otan legez
bizi banuen ama
eniñuan salduko
España urrunera.

Aita saldu niñuan
idia bezela
Españan kenduteko
bai neure biotza.

Zazpi zaldi daukadaz
urrez doraturik,
orrek artizuz eta
itxi naizu bizirik.
- Orrek artu bai baina
ez itxi bizirik!

- Zazpi izera dodaz
odolez bustirik
zortzigarrentxu onek
naroie mundutik.

Ekarri abadia
konfesau naitien
betiko inpernuan
sartu ez naitien.

- Abadie urrun dago
elixie urrunago
oi ara joateko
astirik eztago!

- Neure amainerraba,
muskertxu berdie,
subiek ein al deutso
arratseko oie!

Agur Ana Juanita
amari gorantzi
sekule ta betiko
ikusi nozu ni.