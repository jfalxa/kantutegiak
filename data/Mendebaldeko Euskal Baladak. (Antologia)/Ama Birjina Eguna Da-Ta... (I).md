---
id: ab-4177
izenburua: Ama Birjina Eguna Da-Ta... (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: null
midi: null
youtube: null
---

Ama Birjina eguna da ta
kanta dezagun kantia.
Nik kantauko dot kantia, baina
ondo erantzun, jentia.

Arantzazura bidea luze,
gainera aldatz-goria:
arri labrauaz egina dago
paradisura bidia.

Arri labraua ona da, baina
galtzairu pina obia:
urre gorriaz eginda dago
paradisuan silia.

Sila artantxe jarrita dago
Birjina Ama Andria...
Bera bezela gura leukiden
Birjinak bere Semia.

Sabel garbian eduki zuen
bederatzi ilabetian,
bederatzi ilak igaro eta
jaio zan Gabon-gabian.

Mundu guztia salbatu zuen
Natibitate-giozian,
mundu guztia salbatu zuen
Natibitate-goizian.

Izar eder bat argitu zan da
abantau nitzan bidian,
bere argitan juan izan nintzan
Arantzazu'ra bidian.

Ama Andria ta bere Semia
topau nituzan bidian,
kontseju onak emon zizkiden
Ama-Semien artian.

On da umilde izan nedila
munduan nazen artian,
gero ez nebala damu izango
emendik nijuanian.

Odola pranko ixuri zuen
oin-utsetan da lurrian...
- Ene Semia, eutsi zapatak,
jantzi eidazuz soinian.

- Ama Andria, largatu bedi
onela noian artian.
Nik eriotzia artu biar dot
Kalbario'ko mendian.

Kalbarioko mendian eta
kurutze gogor batian:
nik eriotza artu biar dot
kurutze gogor batian.