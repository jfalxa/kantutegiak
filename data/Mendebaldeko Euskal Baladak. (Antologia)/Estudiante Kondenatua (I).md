---
id: ab-4195
izenburua: Estudiante Kondenatua (I)
kantutegia: Mendebaldeko Euskal Baladak. (Antologia)
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004195.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004195.MID
youtube: null
---

Estudiantea naz
oraindino gaztia
amazortzi urte be
kunplidu bagea.

Iru pezeta zala
kartazko jokoa
bertan ilde itxi neuen
estudiante majoa.

Au zen kalabazoa
ilun eta triste
ni ona sartu nintzela
diradez iru urte.

Pozik egin lezake
beste arenbeste
libratzeko esperantza
baldin bagendu be.

- Ama, gura dau berorrek
ni libradutea?
Eriotzea egin dot
ezta posiblea.

Atzo etorri jatan
arreba gaztia
lastime emoten daustela
onelan ikustea.

Ai ene Jangoikoa
kanpaiaren otza
igual izingo da
urkamendikoa.

Au de urkemendiko
aizearen otza!
Gure Jaunen aurrerako
nik deukedan lotsa.