---
id: ab-4993
izenburua: Euskaldun Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004993.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004993.MID
youtube: null
---

Esukuldun bat zen itsu eta betsolari,
Atez ate zebilen iloba gidari.
Soineko eta diru, janari, edari,
Nasaiki biltzen zuten berso eman sari.

Zuhur gisa baitzuten sos gutxi xahutzen,
Piko baten azpian eltze bat zaukaten;
Hirurehun libera harat bildu zuten...
Bainan gaixtagin batek ebatsi zioten.

Bere gordelekutik galdurik eltzea,
Bat zagon hatsperenka, nigarrez bertzea.
ilobak esan zion: Osaba maitea,
Horra zer den kampoan gauzen gordatzea!

- Azkeneko aldian hemen ginelarik,
Ikusi gaituztela ez diat dudarik.
- Bai, sastrea heldu zen, pegarra beterik,
Behatu zigu guri, bainan urrunetik.

- Iloba gida nazak sastrearengana,
Adi dezan bertso bat itsuak emana.
Gure heltzeak badu harek eremana,
Bazekiat nik hura zer gisaz engaina.

Ilobak esan zion apal belarrira:
- Sastrearen etxerat orai heltzen gira,
Irri faltsuan dago leihotik begira,
Ez erran hitz gaiztorik, zuhurra bazira.

- Iloba gida nezak artoski bidean,
Zerbait daiat utziko, azken egunean:
Ehun luis baitiat piko baten pean,
Bertze ehun emanen, bihar, han berean.

Sastreak bere baitan: -Itsuak ez zakik
Hartuak ditudala horren sosak, handik;
Eltze hau, diruarekin, berriz han emanik,
Bertze hiru ehunak bilduko ditiat nik.

Sastrearen agintza balios izan zen:
Biharamun goizeko, eltzea han zuten!
Ilobak erran zion: Hemen dugu, hemen
Oi, zein ongi sastrea engainatua den!

- Poltsan behar diagu dirua ezarri.
Lurpean, hola-hola, utzak heltze hori,
Eta emak barnean bortz edo sei harri,
sastreak aski pixu altxa dezan sarri.

Itsuak sastreari: -Errak, to, egia,
Atzo bezala dukan arrai aurpegia,
Ala tristerik dagon, doluan jarria,
Dirua erain eta... bildurik harria?

- Itsu Harpagon, zikoitz, laido-emailea,
Habil, eta ez kolpa ene ohorea!
Dirudun bahaiz ere, eta ni gabea,
Baduk aski zorigaitz hire ilunbea!

- Ni itsu, ilunbean, hi sastre erromes...
Dezagun lagun elkar biek ditugunez:
Nik egiten haut jabe ene ontasunez,
Okertu nahi banauk hire begi batez...