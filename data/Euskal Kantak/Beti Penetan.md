---
id: ab-4938
izenburua: Beti Penetan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004938.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004938.MID
youtube: null
---

Beti penetan, beti penetan,
bizi naiz mundu honetan.
Egunaz zerbait alegratzen naiz,
gauaz beti penetan.
Neureganako amorioa
joan zitzaizun batetan.