---
id: ab-4935
izenburua: Azpirozko Aldapatik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004935.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004935.MID
youtube: null
---

Azpirozko aldapatik Albisurakuan,
Triste neukan bihotza hangoxe basoan.
Zerk jarri ote zidan Albisu goguan?
Inola ere burutik ezin kendu nuan.