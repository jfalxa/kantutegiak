---
id: ab-4931
izenburua: Txorietan Buruzagi
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004931.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004931.MID
youtube: null
---

Txorietan buruzagi
erresinola kantari.
Kantatzen dizu ederki
goizan argi hasteari.
Oi, haren aire ederrak
zoraturik nau ezarri.

Erresinola kantari
txori ororen buruzagi.
Anitzentan behatu naiz
haren boz eztiari.
Jeikirik ene oheti
kanberako leihoti.

Esparber bat da ibili
haren ondotik igeri.
Atzemanen dizu sarri
bere boz ederragati.
Dezagun beraz prebeni
kanta dezan emeki.

Erresinola eskapi
esparberari eskuti.
Esparberari eskapi,
falkon haundiak harrapi.
Kantatzen dizu tristeki,
nigar egiten trendeki.

Gazte naiz eta alegera
bai eta ere goihera.
Kontent, urus, alegera,
deusek ez egiten pena.
Ororekila adiskide
estekamendurik gabea.