---
id: ab-5040
izenburua: Ume Eder Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005040.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005040.MID
youtube: null
---

Ume eder bat ikusi nuen
Donostiako kalean,
Hitzerditxo bat hari esan gabe,
nola pasatu parean?
Gorputza zuen liraina eta
oinak zebiltzan aidean.
Politagorik ez det ikusi
nere begien aurrean.

Aingeru zuri paregabea
Euskal Herriko alaba,
usterik gabe zugana beti
nere bihotzak narama.
Ikusi nahian beti hor nabil,
nere maitea, hau lana!
Zoraturikan hemen naukazu
beti pensatzen zugana.

Galai gazteak pensatzen dute:
aingeru hori non dago?
Nere maitea nola deitzen dan
ez du inortxok jakingo.
Ez berak ere, ez luke nahiko,
konfiantza horretan nago.
Amoriodun bihotz hoberik
Euskal Herrian ez dago.