---
id: ab-4962
izenburua: Oi, Betleem!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004962.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004962.MID
youtube: null
---

Oi, Betleem!
Hala egun zure loriak,
oi, Betleem!
Ongi baitu diztiratzen
zugandik heldu den argiak
betetzen itu bazter guziak.
Oi, Betleem! Oi, Betleem!

Zer ohore!
Hala baitzara goratua!
Zer ohore!
Zer grazia! Zer fabore!
Zeruaz zare hautatua,
Jesusen zare sorlekua.
Zer ohore! Zer ohore!

Artzainekin,
heldu naiz zugana lehiaz,
artzainekin.
Hek bezala nahiz egin
adoratzen zaitut, Mesias,
eta maite bihotz guziaz.
Artzainekin, artzainekin.

Ez dut deusik,
o, Jesus, zuri eskaintzeko,
ez dut deusik,
bihotz hobesun bat baizik,
eskerren zuri bihurtzeko,
hainbat emaitzen pagatzeko.
Ez dut deusik, ez dut deusik.