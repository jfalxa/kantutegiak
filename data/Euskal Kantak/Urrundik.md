---
id: ab-5046
izenburua: Urrundik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005046.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005046.MID
youtube: null
---

Urrundik ikusten dut, ikusten Mendia,
Zeinaren gibelean baitut nik herria.
Jadanik dut aditzen, zorion handia,
Ezkila maitearen hasperen eztia.

Ezkila, zer ote duk hik egun erraiten,
Urrunera zer berri ote duk egortzen?
Mendiek hodeipetik dautek ihardesten
Hik errana zerura ditek helaratzen

Landako langilea, artzain mendikoa,
Iturriko bidean dohan neskatoa,
Aditurik, ezkila, hire boz lañoa,
Otoizten hasi dituk Ama zerukoa !

Nik ere dut otoizten Birjina Maria
Baserrietan galdu haurren gidaria,
Neretzat, otoi, dezan ardiets grazia,
Bakean kausitzeko nik egun herria.

Mendiak utzi ditut urrun gibelean,
Herria dut ikusten jadanik aldean...
Zer duk, ene bihotza, saltoka barnean ?
Ote duk huts eginen herrira heltzean ?

Agur, agur herria ! agur sorlekua !
Agur nere haurreko leku maitatua !
Jainkoak aditurik haur baten oihua,
Hiregana duk haur bat egun hurbildua.

Mendiaren hegitik hartuz bazterrera,
Iduri xingola bat aldapa-behera;
Bideska, hi zuzen haiz jausten zelaiera,
Zuzen ereman nezak ahaideen artera.

Bide hegiko haritza, bortz aldiz haurrean,
Igandetan mezatik etxerat sartzean,
zenbatez en nauk jarri amaren aldean,
Hire adar lodiek egin itzalean !

Baratze gibeleko elorri zuria,
Beti duk begiratzen haurreko tokia ;
Hik bezala zertako, aldaska garbia,
Nik ez dut sorlekuan higatzen bizia ?

Bainan nere begitik nigar bat da jausten,
Bozkarioak darot bihotza gainditzen,
Etchekoen boza dut jadanik aditzen...
Jainkoa, darozkizut eskerrak bihurtzen.

(Berriz herritik urruntzean.)
Ezkila, berriz diat bihotzean pena,
Herritik urruntzean bakoitzak duena.
Neretzat hik baihuen jo lehen orena,
Agian hik orobat joko duk azkena .