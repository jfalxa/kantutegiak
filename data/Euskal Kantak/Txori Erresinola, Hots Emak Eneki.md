---
id: ab-4933
izenburua: Txori Erresinola, Hots Emak Eneki
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004933.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004933.MID
youtube: null
---

Txori erresinola, hots, emak eneki,
Maitenaren bordara biak alkarreki.
Botz ezti batez izok deklara sekretki
Haren adiskide bat badela hireki.

Heldu ginenean maitenaren bordara,
orak hasi zeizkun txanpazberehala,
Laster egin genian bertan gordatzera;
Erresinola igain zuhaitz batetara.

- Nur da edo zer da? maitenak leihoti.
- Adiskideak gira, ziaude beheiti;
Eta bortha ideki emeki-emeki
Mintza ahal zitzadan ahalaz sekretki.

- Nor da edo zer da? Nongo zirade zu?
- Etxe ondorik eztit, barka izadazu
Egarri handi batek harturik niagozu,
iturri on bat non den, othoi, erradazu.

- Zure egarriaz ezta mirakulu,
Egungo egunean bero egiten du.
Iturri on bat hortxe baturen beituzu,
Zuk galdatzen duzuna guk e behar dugu.