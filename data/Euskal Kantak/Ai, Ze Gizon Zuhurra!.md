---
id: ab-5011
izenburua: Ai, Ze Gizon Zuhurra!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005011.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005011.MID
youtube: null
---

Ai ze gizon zuhurra dan Matxintxo gurea,
prakatxo zaharrak eta ona umorea.
Iaz artorik ez ta aurten hil andrea!
Ze bizimodu dok hori, Matxintxo gurea.