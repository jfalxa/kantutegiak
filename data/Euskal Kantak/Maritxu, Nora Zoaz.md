---
id: ab-4946
izenburua: Maritxu, Nora Zoaz?
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004946.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004946.MID
youtube: null
---

- Maritxu, nora zoaz,
Eder galnt hori?
- Iturrira Bartolo
nahi badezu etorri.
- Iturrian zer dago?
- Ardotxo txuria.
- Biok edango degu
nahi degun guztia.

- Maritxu zuregana
biltzen naizenean,
poza sentitutzen det
nere barrenean.
- Bartolo, nik ere det
atsegin hartutzen,
ur bila noanean
banazu laguntzen.

- Maritxu, lagunduko
dizut gaur mendira,
ur fresko eder bila
hango iturrira.
- Iturri eder hortan
dagon ur garbiak,
Bartolo, alaituko
dizkizu begiak.

- Maritxu, pentsatutzen
badezu ezkontzea,
lehendabiziko nitaz
oroitu zaitea.
- Zure mende jartzen naiz
denbora guziko.
- Bartolorekin gaizki
ez zera biziko.