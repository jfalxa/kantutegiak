---
id: ab-5058
izenburua: Agur , Xiberua!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005058.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005058.MID
youtube: null
---

Agur, Xiberua
Baztek guzietako txokorik ederrena
Agur sorlekua,
Zuri ditut ene ametsik gozoenak
Bihotzan erditik
Bostetan elki deitadazut hasperena
Zu utzi geroztik
Bizi naiz tristerik,
Abandonaturik,
Ez baita herririk
Paris ez besterik
Zu bezalakorik

Sor lekuaa utzirik gazte nintzelarik
Parisen sartu nintzan korajez beterik
Plazerez gose eta buruan harturik
Behar nuela alegera bizi
Bostetan geroztik
Nigar egiten dit
Xiberua zuri.

Agur Xiberua...

Palazio ederretan gira alojatzen
Eta segur goratik aire freska hartzen
Gain behera so eginik baitzait iruditzen
Orin gainean nizala agitzen
Bena ez dira hemen
Bazterrak berdatzen,
Txoriak kantatzen!

Agur Xiberua...

Ametsa, lagun nazak ni Atharratzera
Ene azken egunen han igaraitea
Orhiko txoriaren kantuz behatzera
barka ditzan nik egin negarrak
Hots, xiberutarrak
aintzinean gora
Eskualdun bandera!

Agur Xiberua...