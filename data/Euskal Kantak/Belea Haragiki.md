---
id: ab-4992
izenburua: Belea Haragiki
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004992.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004992.MID
youtube: null
---

Belea haragiki
puska bat mokoan,
haren jateko zagon
haitz baten kaskoan.
Azeria ihizin,
han gaindi baitzohan.
Usainka eta beha trikatzen zaio han.

Egun on, musde bele,
zer jaun ederra zu!
Zure luma dizdiraz
liluratzen nauzu.
Kantari eder baten
fama ere duzu,
nahi zintuzket entzun,
otoi kanta zazu.

Belea lorietan
hasten da kantatzen,
zehe bat duelarik
mokoa zabaltzen.
Bere zitzia zaio
lurrerat erortzen,
azeria hain zuzen
altxatzeko han zen.

Azeriak: mil esker,
dio beleari,
ez gaitela, to, fida
lausenkariari,
eta ez ere gure
eder usteari.
Jaki hau daiat hartzen
hitz on horren sari.