---
id: ab-5035
izenburua: Biba Rioxa
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005035.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005035.MID
youtube: null
---

Biba Rioxa, biba Naparra,
arkume onaren iztarra,
hemen guztiok anaiak gera
hustu dezagun pitxarra.
Glu, glu, glu...

Ardo fina ta jateko ona
jartzen badute gaur ugari,
gure barrenak berdindurikan
jarriko dira guri-guri.
Glu, glu, glu...

Gure sabela bete behar da
ahal bada gauza onarekin,
dagon lekutik eragin bapo!
Aupa, mutilak, gogoz ekin!
Glu, glu, glu...

Ez ikaratu dagon artean,
jan eta edan gaur gogotik,
hustutzen bada ekarko degu
berriro lehengoko tokitik.
Glu, glu, glu...

Umorea da gauzik onena,
nahigabeak ditu ahaztutzen.
Uju ta aja hasi gaitean
euskal doinuak kantatutzen
Glu, glu, glu...