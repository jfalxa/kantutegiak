---
id: ab-4991
izenburua: Bautista Basterretxe
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004991.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004991.MID
youtube: null
---

Bautista Basterretxe
mutiko pijoa
neri gurdi-ardatza,
neri gurdi-ardatza
ostuta dihoa.
Beltzak eta zuriak
izango dituzu,
neri gurdi-ardatza
neri gurdi-ardatza
emate ez baidazu.

Gurdi-ardatza ezik
itai ta harria.
Besteren behar gabe,
besteren behar gabe
badauzkat nereak,
Sega, poto, labaina,
gainera bostortza,
perian erosia,
perian erosia,
daukat nik zorrotza.

Nere idi-pareak
dauzka zintzarriak,
lepotikan zintzilik,
lepotikan zintzilik
zildain jarriak.
Gainera uztarriak
ta kopetekoak,
ontz urre gorri zaharrez
erositakoak.

Zerorrek nahiko gauza
dezula diozu,
baina gurdi-ardatza,
baina gurdi-ardatza
lapurtu didazu.
Ordaindu behar dezu
larrutik ederki,
ardatza ez baidazu,
ardatza ez baidazu
neri ekartzen aurki.

Buru gogorrak badu
berekin kaltea
ez dedan gauza nola,
ez dedan gauza nola
nahi dezu ematea?
Zentzuak galdurikan
arkitzen zerade,
bestela behintzat horla,
beztela behintzat horla
ariko ez zinake.

Gezur ta abar zabiltza,
Bautista tranpean.
Zu bezain gezurtirik,
zu bezain gezurtirik
ez da Euzkal Herrian.
Zure berri dakite
inguruko denak,
gordetzen dituzula
gordetzen dituzula
gauza besterenak.