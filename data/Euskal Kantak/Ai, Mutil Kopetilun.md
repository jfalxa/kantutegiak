---
id: ab-5016
izenburua: Ai, Mutil Kopetilun
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005016.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005016.MID
youtube: null
---

Ai mutil kopetilun, begiak logura,
baietz uste duk bainan ez nauk hire gura.

Oraingo mutil zaharrek zer dute merezi?
Errekan beratuta, lixiban egosi.

Oraingo mutil zaharrek duten fantasia!
Soinean gerrikoa besteek erosia.