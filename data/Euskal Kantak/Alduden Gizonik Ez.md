---
id: ab-5005
izenburua: Alduden Gizonik Ez
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005005.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005005.MID
youtube: null
---

Alduden gizonik ez
eta Urepelen bai. Bainan eta
Alduden gizonik ez
eta Urepelen bai.
Aldudeko gizonak, batzuk tuzu itsuak,
bestek tuzu mainguak, bestek tuzu okerrak,
bestek tuzu konkorrak.
Ahorik ez, begirik ez, belarririk
bat ez duen hura, Urepelen da.