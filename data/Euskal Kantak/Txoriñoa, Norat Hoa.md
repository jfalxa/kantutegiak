---
id: ab-4928
izenburua: Txoriñoa, Norat Hoa?
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004928.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004928.MID
youtube: null
---

Txoriñoa, norat hoa
bi hegalez airian?
Espainiarat joaiteko
elurra duk bortian.
Joanen gaituk alkarreki
hura urtu denian.

San Josefaten ermita
desertuan gora da.
Espainiarat joaitean
hau da ene pausada.
Gibelerat so egin eta
hasperena ardura.

Hasperena, habil, hoa,
maitearen bordara.
Habil eta erran izok
nik igortzen haudala.
Bihotzean sar hakio,
hoa, eni bezala.