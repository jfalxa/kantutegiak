---
id: ab-5039
izenburua: Hara Nun Diran!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005039.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005039.MID
youtube: null
---

Hara, nun diran mendi maiteak
hara, nun diran zelaiak!
Baserri eder zuri-zuriak,
iturri eta ibaiak.
Hendaian nago zoraturikan
zabal-zabalik begiak.
Hara, España, lur hoberikan!
ez da Europa guztian!

Oi, Euskal Herri, eder maitea,
hara, hemen zure sema.
Bere lurrari muin ematera
beste gabe etorria!
Zuregatikan emango nuke
pozik bai nere bizia,
beti zuretzat, hil arteraino,
gorputz ta arima guztia.

Gero, pozik bai Donostiara,
Okendo haren lurrera.
Zeru polit hau utzi beharra,
nere anaiak, hau pena!
Irutxuloeta maitagarria,
nere tokia zu zera,
Veneziaren grazia guztiak
gaur Donostian badira.

Agur, bai, agur Donostiako
nere anaia maiteak,
Bilbaotikan izango dira
aita zaharraren berriak.
Eta gainera hitz neurtuetan,
garbi esanaz egiak.
Suamerikan zer gertatzen dan
jakin dezaten guztiak.
Agur, bai, agur Donostiaco
Nere anaya maitiac,
Bilbaotican izango dira
Aita-zarraren berriac,
Eta gañera itz neurtuetan
Garbi ezanez eguiyac,
Sud-American ser pasatzendan
Jaquin dezaten erriyac.