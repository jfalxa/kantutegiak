---
id: ab-4941
izenburua: Prima Eijerra
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004941.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004941.MID
youtube: null
---

Prima eijerra
zutan fidaturik,
anitz bagira
oroz tronpaturik.
Enea zirenez
erradazu bai ala ez,
bestela banoa
desertiala,
nigarrez urtzera.

- Desertiala
joan nahi bazira,
arren zoaza,
oi, baina, berehala!
Ez zitiala jin
berritan nigana,
bestela gogoa
doluturen zaizu,
amoros gaixoa.

- Nitan exenplu
nahi duenak hartu,
ene malurak
parerik ez baitu.
Xarmangarri bat
nik bainuen maitatu,
fidatu, tronpatu...!
Sekula jagoiti
ikusi ez banu.

- Mintzo zirade
arrazoi gabetarik
ez dudala nik
zur' amodiorik.
Zu baino lehenik
banuen besterik,
maiterik, fidelik.
Hor ez dereizut
egiten ogenik.