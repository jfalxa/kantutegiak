---
id: ab-4900
izenburua: Donostiako Hiru Damatxo
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004900.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004900.MID
youtube: null
---

Donostiako hiru damatxo
Errenterian dendari.
Josten ere badakite baina
ardoa-edaten hobeki.
Eta kriskitin, kraskitin,
arrosa krabelin,
ardoa edaten hobeki.

Donostiako Gaztelupeko
sagardoaren gozoa,
hantxen edaten ari nintzala
hautsi zitzaidan basoa.
Eta kriskitin, kraskitin,
arrosa krabelin,
basoa kristaleskoa.

Donostiako hiru damatxo
hirurak gona gorriak,
sartutzen dira tabernara ta
irtetzen dira hordiak.
Eta kriskitin, kraskitin...

Donostiako hiru damatxo
egin omen dute-apustu,
zeinek ardo gehiago edanda,
zein gutxiago mozkortu.
Eta kriskitin, kraskitin...

Donostiako hiru damatxo
Errenteriko kalean,
egunez oso triste ibili,
baina dantzatu gauean.
Eta kriskitin, kraskitin...

Donostiako neskatxatxoak
kalera nahi dutenean:
- Ama, piperrik ez dago eta
banoa salto batean.
Eta kriskitin, kraskitin...

Donostiako neskatxatxoak
mandatuen aitzakian,
mutilarekin egoten dira
kalean jolaskerian.
Eta kriskitin, kraskitin,
arrosa krabelin,
pozez algara haundian.

Donostiarrak ekarri dute
Getariatik akerra,
kanpandorrean ipini dute
Aita Santutzat dutela.
Eta kriskitin, kraskitin...

Donostiako arrantzaleak
dira txit gizon bapoak.
Gaztelupeko sagardoakin
egiten ongi tragoak.
Eta kriskitin, kraskitin,
arrosa krabelin
maiz bustirikan ahoak.

Arrosatxoak bost hosto ditu
krabelintxoak hamabi.
Mari Joxepa nahi duen horrek
eska bezaio amari.
Eta kriskitin, kraskitin,
arrosa krabelin,
eska bezaio amari.