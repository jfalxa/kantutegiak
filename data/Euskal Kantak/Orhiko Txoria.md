---
id: ab-5029
izenburua: Orhiko Txoria
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005029.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005029.MID
youtube: null
---

Orhiko txoria Orhin bakean da bizitzen,
bere larre sasietan ez da hura unhatzen;
han zen sortu, han handitu, han ari zen maitatzen,
han bere umeen artean, gozoki du kantatzen.

Urusa harek bezala egiten dakiena!
Orhin sortu eta Orhin gazte zahartzen dena!
Aditzez baizen ez daki zer den kanpoko ona,
bainan aldiz dastatzen du etxeko zoriona.

Bego etxeko txokoa oraino norberari!
Bego aitamen etxola haurrik hazierari!
Etxea beti etxea da, zenbat den itsusgarri
kanpoa beti kanpoa da, zenbat den edergarri!

Etxean pena guziak dirade konsolatzen,
etxean zauri minenak nolabait ere hersten,
irriak, solas gozoak, non dirade han baizen,
gau aldera familia denean bateratzen?

Familiako bizia, zein zaren plazertsua!
Zutaz gabetua dena zertaz da batetua?
Umezurtza baten para dabila erratua,
ezin bere bihotzaren aurkituz sosegua.

Zenbat urrikari dudan bizi dena kanpuan
bera da beretzateko mundu hontan osuan,
jendez da setiatua eta da desertuan!
Laguntza onik ez duke bere behar orduan.