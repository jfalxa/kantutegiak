---
id: ab-4970
izenburua: Toberak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004970.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004970.MID
youtube: null
---

Ave, María Purísima!
Jaungoikoak gabon!
Deklaratzera noa
zer desio dugun.
Toberak jotzera gatoz
bost edo sei lagun.

Toberak jotzera eta
dibertitutzera,
ez uzteagatikan
usarioa galtzera.
Borondaterik ez bada
joain gera atzera.

Nobio jaunak esan dio
andre nobiari:
- Zer egingo ote diegu
gizon horieri?
umore ona paratzea
gustatzen zait neri.

Andre nobiak errespuesta
jakinak bezela:
- Etorri diren kastuan
kanta dezatela,
lotsa haundia emango
diegu bestela
.
Dibertsio polit bat
elkarren artean
pasatzea hobe da
bitatik batean,
ez dut uste izain den
inoren kaltean.

Konformatu dirade
biek elkarrekin,
nere lagunak, zuek ere,
umore onarekin,
palanka pikatzeko
kantatu San Martin.

KORUA
San Martín de la moja
Moja de San Martin!
Toberak jo ditzagun
ordu onarekin,
ordu onarekin ta
Birjin Amarekin.