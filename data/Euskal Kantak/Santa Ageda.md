---
id: ab-4968
izenburua: Santa Ageda
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004968.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004968.MID
youtube: null
---

Zorion, etxe hontako denoi!
Oles egitera gatoz,
aterik ate ohitura zaharra
aurten berritzeko asmoz.
Ez gaude oso aberats diruz,
ezta ere oinetakoz.
Baina eztarriz sano gabiltza,
ta kanta nahi degu gogoz.

Santa Ageda bezpera degu
Euskal Herriko eguna,
etxe guztiak kantuz pozteko
aukeratu deguna.
Santa maitea gaur hartu degu
gure bideko laguna.
Haren laguntzaz bete gentzake
egun hontako jarduna.

Siziliako ugarte ederrak
emana digu lorea.
Han jaio baitzan gure Ageda,
zorioneko gaztea.
Txikitatikan gogoz hartu zun
Jesukristoren legea.
Maitasun honek ekarri zion
bihotzeko poz betea.

Edertasunez jaunak apaindu
zuan Ageda gaztetan.
Haren begiak izar bi ziran,
aurpegi gozo haretan.
Gizon gaiztoen grina zitalak
esna zitzakeen benetan.
Laister izutu zan Kintziano
Agedaren ederretan.

Agedak bere maite bakarra
Jesus aukeratu zuan.
Horregatikan beste maiterik
ez zan harentzat munduan.
Kintzianoren eske guztiak
arrez baztertu zituan.
Baina agintarik gaizto harek
kristautzat salatu zuan.

Emakume gaizto baten mende,
sartu zuten giltzapean,
Agedaren bihotz garbi hura
zikin ote zezakean.
Alperrik baina ahalegindu zan
andra lizuna lanean.
Honek esanak ez ziran sartu
Agedaren bihotzean.

Amorru biziz oinutsik jarri
dute Ageda zintzoa,
txingar gorien gainean gero
ibilarazi gaixoa.
Ondoren, bular bat ebakita,
uzten dute urratua.
Baina Jainkuak bihurtu zion
gau hartan bular mindua.

Alperrik gizon gaiztoak zuten
oinazez erdi hilik utzi.
Jesukristoren maitasun hartan
ez zuzna inola etsi.
Otoi gozotan gogoa isuriz
mundu hontatik ihesi,
zeruateko zori ederra
betiko zun irabazi.

ALKATE JAUNARI
Alkate jauna, legean gatoz
baimen eskean atera,
kanta zaharrak galdu ez ditezen
lehengo ohitura piztera.
Guraso zaharren legean pozik
goaz danok kantatzera.
Santa Agedak ekarri gaitu
berorren etxe aurrera.

ETXEKO JAUNARI
Mendi tontorrak elurrez zuri,
atai aldean laiotza.
Etxe hontako nagusi jaunak,
urrezkoa du bihotza.
Santa Agedak kendu digu gaur
eskean hasteko lotsa,
dirurik ez bada, berdin zaigu
gizendutako bildotsa.

BIKARIO JAUNARI
Apaizburu jaunaren atean
jo nahi degu gaur aurrena,
Jaungoikoaren ordezkoari
gure agurrik onena.
Zeruko Jaunak argitu beza
berorren jardun zuzena,
zeru goietan izan dezagun
guztiok gure azkena.

ETXEKO ANDREARI
Zelai erdian loreak argi,
larroz, krabelin ta lili.
Etxe hontako etxekoandreak
Ama Birjiña dirudi.
Etxe hontako urdaiazpiko
asko, kakotik zintzilik,
ta lukainka gorriak ugari
poltsa betetzeko guri.

SENAR-EMAZTE GAZTEEI
Itzal berria zabaldu digu
atai aurreko intxaurrak,
habe berriak sendotu ditu
etxeko sakape zaharrak.
Senar-emazte gazteak neurtu
dituzte beren indarrak:
Arto ta gariz beteko dira
aurten mandio baztarrak.

ETXEKO ALABARI
Mutil gazteak atean gera
non dago etxeko larrosa?
Atera bedi azkar leihora
begiak ditzagun goza.
Eguzki berak ikusi nahi du
horren aurpegi panpoxa.
Nere emazte izan nahi balu
betea nuke nik poza.

Mutil taldea hementxe gaude,
danok gazteak eta gai,
mundu guztia igaro degu,
itsas, lehor, mendi, zelai.
Neskatx ederrak utzi ditugu
negar malkotan gure zai.
Baina guk neska danen artean,
hemengotxe alaba nahi.

ETXEKO SEMEARI
Tantai luzeak gerria zuzen
darama zeru aldera,
etxe hontako seme jatorrak
badu maitale aukera.
Alaba gazte asko dabiltza
zugatik bihotz minbera,
mutilzahartuta bizi izateko
Jaunak egina ez zera.

ETXEKO HAURREI
Gau ederrean ilargi inguru,
izarrak zeru urdinean,
sukalde hortan amaren albo
haur ederrak jolasean.
Zerua hustu zaigu aingeruz,
horiek hemen jaiotzean
zeruko jaunak gordeko ditu
gizon egin bitartean.

DENDETAN
Eskean gatoz, baina ez gera
ez ijito ta ez praile,
Santa Agedak atera gaitu
kaletara kantatzaile.
Denda honetan kajoitxo asko,
danak giltza ta zerraile,
estraperloko kajoitik bota
peseta pare bat mile.

TABERNETAN
Aspaldi hontan ura garesti
dabil esne ta ardotan,
mahats gutxi izan arren, ardorik
ez da palta tabernetan.
Jesusek ura ardotu bazun
Kanaango eztaietan,
orain ikasle azkarrak sortu
zaizkio ardangeletan.

Ura gustatzen zaigu eskuak
ta aurpegia garbitzeko,
baina napar beltza behar degu,
eztarria bustitzeko.
Txanpaina ere edango genuke,
norbait balitz emateko.
Pattarra ez da gaizki etortzen
bizkarreko hotza kentzeko.

SARRERA
Bedeinkatua, alabatua
aldareko sakramentua.
Pekatuaren mantxarik gabe
zeina da konzebitua.

LELOA
Dios te salve, ongi etorri!
Gabon Jainkoak diela!
Legearekin kunpli dezagun
Santa Agedan bezperan.

KOPLAK
Hau haizearen epela,
airean dabil orbela.
Etxe hontako jende leialak,
gabon Jainkoak diela.

Zapata zuri paperez,
euri danean batere ez.
Nagusi jauna, esan bezaigu
hasiko geran edo ez.

Horroko hor goian izarra,
errekaldean lizarra.
Etxe hontako nagusi jaunak
urre gorrizko bizarra.

Horroko hor goian elorri,
hondoa jo ta erori.
Etxe hontako etxekoandreak
Ama Birjina dirudi.

Etxeko seme zalduna
Jesukristoren laguna,
horla bakarrik gaizki zaude ta
behar zenduke laguna.

Etxeko alaba, non zera?
Inon ageri ez zera.
Atoz honera ate ondora
gure ilunak kentzera.

Lau galai gazte badauzkat,
jarriko dizut aukera.
Neroni ere garaian nago
baldin gustatzen bazera.

Emango bauzu emazu,
bestela ezetz esazu,
ate ondoan hotzak hiltzera
amak ez gaitu bialdu.

Eskupekoa hartu degu ta
orain abia gaitean
adiosikan ez degu eta
agur ikusi artean.