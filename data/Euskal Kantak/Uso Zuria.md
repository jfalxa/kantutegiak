---
id: ab-4951
izenburua: Uso Zuria
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004951.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004951.MID
youtube: null
---

Uso zuria, errazu,
Norat joaiten zira zu?
Espainiako bortuak oro
Elurrez beteak dituzu.
Gaurko zure ostatu
Gure etxean baduzu.

Uso zuri bat egrtu zaigu
Frantziako mugatik.
Lumatxoren bat falta omen du
hegotxo biren erditik.
Luma huraxe falta ez balu,
ez da munduan parerik.

Ez nau izutzen elurrak,
ez eta gauaren ilunak.
Maiteagatik pasa nitzake
gauak eta egunak,
gauak eta egunak,
desertu eta ohianak.

Uso zuria airian,
ederragoa mahaian.
Zure parerik ez dut aurkitzen
Espainia guzian,
ez eta ere Frantzian,
eguzkiaren azpian.