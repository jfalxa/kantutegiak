---
id: ab-5060
izenburua: Goizean, Argi Hastean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005060.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005060.MID
youtube: null
---

Goizean argi hastean, ene leiho hegian,
txori bat pausatzen da eta goratik hasten kantan.
Txori ederra, hain alagera, entzuten haudanian,
ene bihotzeko tristura laster duak aidian.

Ene txoririk maitena, zeren jin hiz nigana?,
iratzarazi nauk nialarik ametsik ederrena.
Jinik hain goizik uste hian hik baniala anitz pena?
Ez, ez, habil kontsolatzera malerusago dena.

Txorittoa joan duzu, ta ez arra agertu,
dolutan ni ote zaoinez nik errana gaitzitu.
Goiz guzietan, ene kanberan, tristerik niz aiduru.
Nahiz haren aire xarmant mbat berriz ere behartu.