---
id: ab-5003
izenburua: Erreberentzia
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005003.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005003.MID
youtube: null
---

Erreberentzia Gorputz egunean
elizan sarturik Jaunaren aurrean,
ezpata birekin laudatuaz gure Jauna
badakigu dantzatzen dana.
Onestasun guziz handiarekin,
lehendabizi Jaunari agur egin,
hala agintzen baitigu legeak,
zeren gaituen guztiok bereak.