---
id: ab-5055
izenburua: Iparragirre
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005055.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005055.MID
youtube: null
---

Iparragirre abila dala
askori diot aditzen,
eskola ona eta musika
hori hoiekin serbitzen.
Ni ez nauzu ibiltzen
kantuz dirua biltzen,
komediante moduan.
Debalde festa preparatzen det
gogoa detan orduan.

Eskola ona eta musika,
bertsolaria gainera,
gu ere zerbait izango gera
horla hornitzen bagera.
Atoz gurekin kalera,
baserritar legera,
musika hoiek utzita.
Errenterian bizi naiz eta
egi zaidazu bisita.