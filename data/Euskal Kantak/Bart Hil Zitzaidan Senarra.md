---
id: ab-4917
izenburua: Bart Hil Zitzaidan Senarra
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004917.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004917.MID
youtube: null
---

Bart hil zitzaidan senarra
aspaldi egin beharra.
Zahartu zen, konkortu zen,
itsutu zen, maingutu zen,
txartu zen ta hil bedi!
Ez zuen balio lau zorri.

Tilili eta talala
dantza guztien ama da.
Rai, rai, rai...

Nik opilakin xingarra
zuk idiaren adarra.
Rai, rai, rai...