---
id: ab-4978
izenburua: Pipa Hartuta
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004978.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004978.MID
youtube: null
---

Pipa hartuta zoratzen naiz,
ardoa edanda mozkortzen naiz,
kortejatzean lotsatzen naiz,
nola demontre biziko naiz?
Tralara lara laralara,
tralara lara laralaralaralara.

Hamar botila txankraturik,
azken basoa klikaturik,
azken txirrista irentsirik,
poltsa deraukat tturrindurik.
Tralarala...

Urrundu zaizkit aiskideak,
eta hurbildu ostalerak.
Etxean aldiz emazteak
prest-prestak ditu erausiak
Tralarala...

Bazter guztiak inguruka,
bideak ere inarroska.
Munduak zer du? Hau da hinka!
Goazen etxera andarkoka
Tralarala...

Mutiko gazte ta leiala,
kontseju onak har zaitzala:
otoi bizian, ez zaitezela
sekulan mozkor ni bezala.
Tralarala...