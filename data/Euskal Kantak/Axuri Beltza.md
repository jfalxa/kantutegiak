---
id: ab-5002
izenburua: Axuri Beltza
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005002.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005002.MID
youtube: null
---

Axuri beltza ona da, baina
hobeago da zuria,
dantzan ikasi nahi duen hirrek
nere oinetara begira.