---
id: ab-4955
izenburua: Haur Eder Baten Bila
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004955.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004955.MID
youtube: null
---

Haur eder baten bila
gabiltza zoratzen
ta izarrak esan digu
galdetzeko hemen.
Jaungoikoaren herri
maite, Jerusalen,
esan zaiguzu laister
Jesus non jaio den.
Jaungoikoaren herri
maite, Jerusalen,
esan zaiguzu laister
Jesus non jaio den.

Artzai buru txuri bi,
Anton eta Peru,
Belengo estalpera
etorri zaizkigu.
Sartu dira barrena,
Manueltxongana,
eskeintza egin diote
arkumetxo bana.
Batek arkumea ta
besteak ardia,
on konpontzen dira
Jose ta Maria.

Ai, hau gauaren
zoragarria,
Jesus jaio da
Belenen!
Herririk herri
bila gabiltza,
bila gabiltza
beraren.