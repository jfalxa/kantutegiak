---
id: ab-5019
izenburua: San Juan De La Portaletaña
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005019.MID
youtube: null
---

San Juan de la Portaletaña,
sapatu arratsaldean,
hamalau atso tronpeta joten
motroiko baten gainean.
Urra, urra, urra, San Juanetan,
dantzan egingo gu ipar haizetan
ujuju, San Juanera goaz gu.

Gure herriko ekandu onak
garbi-garbirik ixteko
berba lohi eta dantza zikinak
bota daiguzan betiko.
Gora, gora gure garbitasuna,
aupa bizkor egin suari bira.
Ujuju, San Juanera goaz gu.

San Juan de la Portaletaña.
sapatu arratsaldean,
deune jone eltxo ta sorginak
guztiak dira erretan,
soroetako landara onak
garbi garatu daitezan.
Ujuju, San Juanera goaz gu.