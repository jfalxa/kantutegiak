---
id: ab-4899
izenburua: Atzo, Atzo
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004899.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004899.MID
youtube: null
---

Atzo, atzo, atzo,
atzo hil ziren hamar atso.
Baldin ardoa merkatze-ez bada
hilko dirade beste asko.
Atzo, atzo, atzo hil ziren hamar atso.