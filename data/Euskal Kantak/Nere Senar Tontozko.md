---
id: ab-4921
izenburua: Nere Senar Tontozko
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004921.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004921.MID
youtube: null
---

Nere senar tontozko
arto jale haundizko.
Zazpi talo ez ditu aski
goizean gosaritako,
zortzigarrena kolkoko
gose orduan jateko.

Lanak usten tu geroko
edo bertzek egiteko.
Maiz joaiten da merkatura,
alegia traturako.
Gero etxerat heltzeko
lagunak behartzen zaizko.