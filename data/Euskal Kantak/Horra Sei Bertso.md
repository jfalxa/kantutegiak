---
id: ab-5049
izenburua: Horra Sei Bertso
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005049.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005049.MID
youtube: null
---

Horra sei bertso kale
garbitzaleari,
Zeina bere izenez
dan Joxe Mari.
Erreza lezazkike
hiru Ave Mari,
Indarra etortzeko
zaldi gaixoari.
Animali hori
urriki zait neri,
falta du ugari
Egoteko guri.
Kartoiakin egina
dala dirudi.

Goizero behar dio,
eraso lanari,
zikinak bildu arte
herri danari.
Berriz ere hobeto
bizi ez danari,
nabarmenduko zaizka
hezurrak ugari.
Ez naiz txantxaz ari
eta, Joxe Mari,
zaldi gaixoari,
nahi aina janari
eman zaiozu, ea
jartzen dan lodi.

Dago eskeletuen
itxurak harturik,
dina jan ezak dauka
horrela galdurik!
Nola ez da egongo
oso argaldurik,
edukitzen badute
askotan baraurik?
Zaldi zahar horri nik
ez det esperorik
ezer ikusterik
hezurrak besterik.
Ez du behintzat izango
odol kolperik.

Lana sobran du bainan
janaria falta,
urrikigarria da
dakarren planta.
Gaixoak ez lezake
luzaro aguanta,
plakiarekin ezin
mugitu du hanka,
pentsu gutxi jan ta
kalean jiraka,
gaizki bizi da ta
indarrik ez daka.
Ezin karriatu du
karro bat kaka.

Munduan ez liteke
zaldirik arkitu,
duenik horrek aina
penak sufritu.
Bihotzik gogorrena
lezake kupitu,
ez du zahartzera txarra
gaixoak atxitu.
Pausua nagitu,
ezin du mugitu
zaldi horrek ditu
zazpi espiritu,
bestela bizirikan
ezin gelditu.

Gizagaixoaz danak
pena hartzazute,
baldin bihotz biguinak
badituzute.
Hasi zan egunetik
eta orain arte
serbitzua egin du
herrian bastante.
Igande ta aste,
jaiarekin nahaste,
makina bat urte
pasa dizkizute,
orain erretiroa
merezi luke.

Zaldiaren heriotza

Indarra zeukan arte
zikina karraio,
eragiten zioten
makina-bat saio.
Baina minez jarri da,
indarra joan zaio,
nekearen pagua
balaz eman zaio!

Hobizdea

Azkenak hemen daude
zaldi argal baten,
zeinak ez zuen inoiz
pentzu on bat jaten.
Gelditu zaizkan mami
puskak eramaten,
beleak lan askorik
apenas daukaten.
R.I.P. Amen.