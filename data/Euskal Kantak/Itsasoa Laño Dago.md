---
id: ab-4927
izenburua: Itsasoa Laño Dago
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004927.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004927.MID
youtube: null
---

Itsasoa laño dago ,
Baionako barraraino.
Nik zu zaitut maiteago
txoriek beren umeak baino.

Gure oroiz aita dago,
lainopean gaueraino.
Nik zu zaitut maiteago
arraitxoek ura baino.