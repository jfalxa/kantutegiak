---
id: ab-4956
izenburua: Haur Txiki Eder
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004956.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004956.MID
youtube: null
---

Huar txiki eder polit bat
jaio zaigu gaur guretzat,
lasterka goazen harat
nork lehenago ikusterat.
Jesus, Maria eta deun Jose.

Gauerdian agertu da
aingeru talde haundia,
ozen kantatzen dutela:
"Gora goian Jainkoa!"
Jesus, Maria eta deun Jose.

Gloria goian jaunari,
gloria Jesus haurrari,
lurrean kristau onari
zorion ta pakea.
Jesus, Maria eta deun Jose.

Belengo estalpean
sartu nahi genuenean,
ateak jo genituen
hamabiak aldean.
Jesus, Maria eta deun Jose.