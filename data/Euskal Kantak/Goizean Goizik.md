---
id: ab-4994
izenburua: Goizean Goizik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004994.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004994.MID
youtube: null
---

Goizian goizik jeiki nInduzun
espusa nintzan goizian
Bai eta setaz ere beztitu
ekia jelki zenian.
Etxekoandre zabal ninduzun
eguerdi erditan,
Bai eta alarguntsa gazte
ekia sartu zenian.

Musde Irigarai, ene jauna,
Altxa izadazu buria,
Ala dolutzen ote zaizu
Enekila ezkontzia?
- Ez, ez zitadazu dolutzen
zure esposatzia,
ez eta ere doluturen
bizi nizaino lurrian.

Nik banizun maitetto bat
Mundu ororen isilik,
Mundu ororen isilik eta
Jinko Jaunari ageririk:
Buket bat igorri ditadazut
Lili arraroz eginik,
Lili arraroz eginik eta
Erdia pozoaturik.

Zazpi urtez etxeki dizut
Gizona hila kanberan;
Egunaz lur hotzean, eta
Gauaz bi besoen artean,
Zitroin urez ukuzten nizun
Astian egun batean,
Astian egun batean eta
Ostirale goizean.