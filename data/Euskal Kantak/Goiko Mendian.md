---
id: ab-4926
izenburua: Goiko Mendian
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004926.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004926.MID
youtube: null
---

Goiko mendian elurra dago,
errekaldean izotza.
Neu zeugandik libre nago ta
pozik daukat bihotza.
Goiko mendian elurra dago,
errekaldean izotza.