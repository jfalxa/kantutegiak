---
id: ab-4907
izenburua: Ni Hiltzen Naizenean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004907.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004907.MID
youtube: null
---

Ni hiltzen naizenean,
trala, larala laralala,
ni hiltzen naizenean,
ez ehortz elizan.

Ehortziko nauzue,
trala, larala laralala,
ehortziko nauzue
soto baten zolan.

Zangoz bortara eta,
trala, larala laralala,
zangoz bortara eta
ahoz dutxulora.

Dutxuloa eroriz,
trala, larala laralala,
dutxuloa eroriz
baduket zer edan.