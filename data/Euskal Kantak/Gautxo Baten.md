---
id: ab-4943
izenburua: Gautxo Baten
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004943.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004943.MID
youtube: null
---

GautxO baten abiatu nintzan
dama gazte bat bilatzen,
Kox-kox Jo neban atea baina
ez osten korresponditzen.

Halako baten bentanatikan
- Ez dost niri amak itxiten.
- Bion borondatea bada,
zatoz ona puska baten.

- Zure ama lotara joan arte
ate ondoan gengokez.
- Atera joan, haizea fresko,
hotzik egon gindekez.

Nire kapea magala zabal,
biok tapauko gendukez.
- Handik tira eta hortik tira
apurtu egin genduke.

- Hatan-horretan gengokezala,
aita zurea letorke.
Hak bat esan ta guk bi erantzun,
okasioa legoke.

- Nire ateak hotsa dau zoli,
txakurrak bere salatu.
Nire txakurra isilduteko,
eper bizia biozu.

- Eper bizia kojiduteko,
mutil mantsoa zagoz zu.
- Neska txiki ta graziosea,
atxakiosa zagoz zu.

- Atxakiosa zagoz zu, baina
neuk eroango zaitut zu.
- Zubi txikitxo bat pasatu ta
da nire etxera-bidea.

- Borondatea dabenentzako
ez dago bide luzea.
Ta borondaterik ez badozu,
agur, neure maitea.