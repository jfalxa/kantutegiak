---
id: ab-4905
izenburua: Dringilindron
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004905.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004905.MID
youtube: null
---

Dringilindron, gaur Gabon,
tripea betea daukat eta
besteak hor konpon.

Galdara bete,
aza egosi,
koloretsuak, gorriak.
Berehala iruntsi neutsazan
azkenengoko orriak.

Hiru puntako
artzal batekin
sakatrapua bailitzan
ezti lapiko handisko bati
barrua huts-huts ein neutsan.