---
id: ab-4981
izenburua: Azkoitiko Azkaraten
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004981.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004981.MID
youtube: null
---

Azkoitiko Azkaraten
neska-mutilak ohe baten,
aitonak ta amonak beste baten
zikili-zakala egiten.

Zikili-zakala eginagatik
zuloa bere lekuan,
gorputzari gustua emanda
ibili ginduan.
Zuk eman eta nik hartu
hori ez da pekatu,
poltsikoa formatu,
ez ginan gaizki portatu.