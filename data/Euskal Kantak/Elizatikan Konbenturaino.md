---
id: ab-4947
izenburua: Elizatikan Konbenturaino
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004947.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004947.MID
youtube: null
---

Elizatikan konbenturaino
egin digute galtzara,
azkeneko eguna da eta
goazen guztiok dantzara.
Txin, txin, txilibitu soinua,
txin, txin, txilibitu danbolin.
Horrela bizi bagina neti,
ondo ginake, Katalin.
Katalin, nere maitea.