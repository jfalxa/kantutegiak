---
id: ab-4916
izenburua: Pello Joxepe
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004916.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004916.MID
youtube: null
---

Pello Joxepe tabernan dala
haurra jaio da Larraulen.
Etxera joanda, esan omen du:
- Ez da nerea izanen.
Ama horrek berak topa dezala
haur horrek aita zein duen.

- Ai, hau pena ta pesadunbrea,
senarrak haurra ukatu!
Haurtxo honentzat beste jaberik
Pello Joxepe, bihotz nerea,
haur horrek aita zu zaitu.

- Fortunosoa nintzala baina
ni naiz fortuna gabea.
Abade batek eraman dizkit
umea eta andrea.
Haurra berea bazuen ere,
andrea nuen nerea.