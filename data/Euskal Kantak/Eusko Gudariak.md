---
id: ab-5032
izenburua: Eusko Gudariak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005032.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005032.MID
youtube: null
---

Eusko gudariak gara
Euskadi askatzeko,
gerturik daukagu odola
bere aldez emateko.
Irrintzi bat entzun da
mendi tontorrean,
goazen gudari denok
ikurrinan atzean.