---
id: ab-4912
izenburua: Zer Deabrukeria?
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004912.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004912.MID
youtube: null
---

Zer deabrukeria
zaio burutara,
Batista, adin horretan
doa ezkontzera?
Haur jaio, gizon bizi,
haurtxo bihurtzea.
Horra, ikusten danez,
gure suertea.

Ia larogei urte
dituen gizonak
ez du andrerik behar
baizikan salda ona.
Gaztetako txingarrak
sentitzen baditu,
ur freskoa edanaz
itzali behar ditu.

Lanparak itzaltzera
dihoan orduan,
llamarak bota ohi ditu
argira moduan.
Baina dira llamarak
akaberakoak,
ez olio gehiegik
emanikakoak.

Davidek zahartu eta
Sunamit hartzeko
esan zuen zuela
oinak berotzeko.
Batista, adin horretan
dek askoz hobea
Palentziako manta,
ez andre gaztea.

Ez badik gauza horrek
erremediorik,
ez ezak behintzat egin
ahal badek semerik.
Esnatzen haizenean
goizean lotatik,
ostikoka bora zak
andrea aldetik.

Ez hadila fiatu
llamara horietan
ondo kontserba itzak
hire erraietan.
Kanpora ateratzen
enpeinatzen bahaiz,
hotzak hilik txit laster
lurperatuko haiz.