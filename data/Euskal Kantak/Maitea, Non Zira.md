---
id: ab-4939
izenburua: Maitea, Non Zira
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004939.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004939.MID
youtube: null
---

Maitea, non zira?
Nik ez zitut ikusten,
Ez berririk jakiten,
Norat galdu zira?
Ala kambiatu da zure deseina?
Hitz eman zenereitan,
Ez behin, bai berritan,
Enia zinela.

Ohikoa nuzu;
Ez nuzu kambiatu,
Bihotzean behin hartu,
Eta zu maitatu.
Aita jeloskor batek
dizu kausatu.
Zure ikustetik,
Gehiago mintzatzetik
Hark nuzu pribatu.

Aita jeloskorra!
Zuk alaba igorri
arauz ene ihesi
komentu hartara.
Har' eta ez ahal da
sarturen serora.
Fede bedera dugu
alkarri eman ditugu,
gauza segurra da.

- Zamariz iganik
jin zaizkit ikustera,
ene kontsolatzera,
aitaren isilik.
Hogei eta lau urte
bazitit beterik.
Urte baten buruan,
nik ez duket arduan
aitaren axolik.

- Alaba duener,
erranen dit orori,
sogidaziet eni,
beha ene erraner.
Gaztetto direlarik,
untsa disziplina.
Handitu direnean,
berant date orduan,
nik badakit untsa.