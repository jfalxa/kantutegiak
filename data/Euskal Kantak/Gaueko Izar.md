---
id: ab-4961
izenburua: Gaueko Izar
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004961.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004961.MID
youtube: null
---

Gaueko izar argitsuena
piztutzeakin batera,
haurtxo eder bat etorri zaigu
gaur arratsean etxera.
Ordu ezkeroz etxeko danak
poztuta zoratzen gera,
haurra dalako zerutik honuntz
datorren Jainko berbera.

Alai ta pozik kanta dezagun
haurtxo honen etorrera,
gure iluna alaitutzeko
jaio dan argi ederra.
Gaurtik aurrera atzerri hontan
zorionekoak gera,
haurra dalako zerutik honuntz
datorren Jainko berbera.

Gau eder hontan zeru goitikan
jetxi da Jauna berbera.
Zure hitz ona entzutearren
goaz leial Belenera.
Hitz egiguzu, Jaungoiko hitza,
isil-isilik bihotzera.
Zure ondoan Gabon gau hontan
egotekotan baikera.