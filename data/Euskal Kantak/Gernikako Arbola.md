---
id: ab-5037
izenburua: Gernikako Arbola
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005037.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005037.MID
youtube: null
---

Gernikako Arbola da bendikatua,
eskualdunen artean guztiz maitatua.
Eman ta zabal zazu munduan frutua
adoratzen zaitugu, arbola santua.

Mila urte inguru da esaten dutela,
Jainkoak jarri zuela Gernikako Arbola.
Zaude, bada, zutikan orain da denbora,
eroritzen bazera, arras galdu gera.

Ez zera eroriko, Arbola maitea,
Baldin portatzen bada, Bizkaiko Juntea.
Laurok hartuko degu zurekin partea
pakean bizi dedin eskaldun jendea.

Betiko bizi dedin Jaunari eskatzeko
jarri gaitezen danok laster belauniko.
Eta bihotzetikan eskatu ezkero,
Arbola biziko da orain eta gero.

Arbola botatzea dutela pentsatu,
Euskal Herri guztian danok badakigu.
Ea, bada, jendea, denbora orain degu,
erori gabetanik eduki behar degu.

Beti egongo zera udaberrikua,
lore aintzinetako mantxa gabekua.
Erruki zaitez, bada, bihotz gurekua,
denbora galdu gabe, emanik frutua.

Arbolak erantzun du kontuz bizitzeko,
eta bihotzetikan Jaunari eskatzeko.
Gerrarik nahi ez degu, pakea betiko,
gure lege zuzenak hemen maitatzeko.

Erregutu diogun Jaungoiko jaunari
pakea emateko orain eta beti,
bai eta indarra ere zerorren lurrari,
eta bendizioa Euskal Herriari.