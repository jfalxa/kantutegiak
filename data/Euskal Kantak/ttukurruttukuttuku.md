---
id: ab-4988
izenburua: Ttukurruttukuttuku
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004988.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004988.MID
youtube: null
---

Ttukurrukuttukuttu
ez neri ikutu,
aita ganbaran da ta
sentituko gaitu.
Aita ganbaran eta
ama sukaldean,
neska-mutil gazteak
eskilarapean.