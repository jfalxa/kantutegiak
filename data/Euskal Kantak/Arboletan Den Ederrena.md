---
id: ab-4923
izenburua: Arboletan Den Ederrena
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004923.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004923.MID
youtube: null
---

Arboletan den ederrena da
oihan beltzean bagoa.
Hitzak leunak zintuen, bainan
bertzetan zure gogoa.
Jaun zerukoak bihur dezala
niganako amorioa.

Bortz aldietan egondu zara
gustura nere onduan.
Zu bezelako ispilurikan
niretzat ez zen orduan.
Zer estaduan egoten zinen
eduki zazu goguan.