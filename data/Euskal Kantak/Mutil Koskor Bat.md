---
id: ab-4997
izenburua: Mutil Koskor Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004997.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004997.MID
youtube: null
---

Mutil koskor bat itsu aurreko
zuela aldamenean,
gizon burusoil, bizar zuri bat,
bi makuluren gainean,
kale izkinan ikusi nuen
inora ezin joanean,
ta bera nor zan jakin nahiean
inguratu nintzanean,
limosnatxo bat eskatu zidan
Jainkoaren izenean.

Zerbait emanaz galdetu nion
ahal zan modurik onenean,
jaiotzetikan al zeukan ala
gaitzak hartua mendean.
Erantzun zidan: -Ez, semea, ez,
nik sasoia nuenean,
ez nuen uste iritsitzerik
honetara azkenean...
Gaur limosna bat eskatutzen det
Jainkoaren izenean.

Eta segitu zuen esanaz:
- Lehengo gerrate denean,
ni aurren samar ibiltzen nintzan
beti edo gehienean.
Nekatu gabe aisa igoaz
aldaparik luzenean.
Ez zan burua asko etortzen
gu hala genbiltzanean...
Gero limosna eskatutzerik
Jainkoaren izenean.

Odola bero eta burua
harroa dagoenean,
edozein moduz sartutzen da bat
halako itsumenean.
Atsekabe ta negar saminak
badatoz ondorenean,
nolaerebait batek bizia
utzitzen ez duenean...
Limosnatxo bat eskatu behar
Jainkoaren izenean.

Inoiz pentsatzen nere artean
ni jartzen naizenean,
gaur ere asko dirala joango
lirakeenak zuzenean,
ez luke inork sinistatuko
zer pena ematen didan.
Ai, horla nintzan ni ere sano
ta gazte nengoenean...
Gaur limosna bat eskatutzen det
Jainkoaren izenean.

Ai, txorakeria asko egiten
da zentzurik ez danean,
sinista zazu ur hau pasea
daukanaren esanean.
Hobeagoa da, sasoi dan arte,
saiatutzea lanean,
negr eginaz ibili gabe
gero denbora joanean...
Limosnatxo bat bildu ezinik
Jainkoaren izenean.