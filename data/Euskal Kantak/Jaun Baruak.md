---
id: ab-4996
izenburua: Jaun Baruak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004996.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004996.MID
youtube: null
---

Jaun Baruak aspaldin
xedera bat hedatu zin.
Txori eder bat atzeman dizu
Paubeko seroren konbentin.
Orai harekin lotzen dizu,
aspaldian desir beitzin.

- Jauna, entzun duzu:
felizitatzen zitugu.
Musde la Plazaren alaba ederra
zuk duzula esposatu.
Andere hura irus duzu,
zuri ez dizugu dolu.

- Xedera balitz halako
merkatuetan saltzeko,
Xiberuko aitonen semeek
eros letzakeie oro,
halako txori edertto
zonbaiten atzemateko.

- Igaran apirilaren burian,
armadaren erdian,
zintudan bihotzian,
armak oso eskkian,
present espiritian,
manka besoen artian.

- Jauna maite banauzu,
erraiten duzun bezala,
kita ezazu, kita ezazu
Erregeren zerbitzua
eta maite herria,
uken dezadan plazera.

- Ez diot kita, maitea,
erregeren zerbiztua.
Sortu nauzu ohorezko,
erregeren zerbitzuko.
Maite dut ohorea
zugatik, xarmagarria.