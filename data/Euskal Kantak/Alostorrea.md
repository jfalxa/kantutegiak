---
id: ab-4990
izenburua: Alostorrea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004990.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004990.MID
youtube: null
---

Alostorrea bai, Alostorrea,
¡Alostorreko zurubi luzea,
Alostorrean nengoanean, goruetan,
bela beltzak kua, kua, kua, kua lehioetan.