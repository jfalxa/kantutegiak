---
id: ab-4954
izenburua: Hator, Hator
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004954.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004954.MID
youtube: null
---

Hator, hator, mutil, etxera,
gaztainak ximelak jatera,
gabon gaua, ospatutzeko,
aitaren ta amaren ondoan.
Ikusiko dok aita barrezka,
ama ere poz atseginez.

Eragiok mutil,
aurreko danbolin horri,
gaztainak erre artean,
gaztainak erre artean,
txipli, txapla, pun!
Gabon gaua pozik igaro daigun.