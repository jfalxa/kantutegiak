---
id: ab-4973
izenburua: Zezenak Dira
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004973.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004973.MID
youtube: null
---

Zezenak dira,
beltz-beltzak dira,
harrapatzen bazaitu,
harrapatzen bazaitu,
jo ta bertan
hilko zaitu.
Donostiako zezen suzkoa
izan zaitez zorionekoa.