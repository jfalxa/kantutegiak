---
id: ab-4898
izenburua: Andre Bat Ikusi Det
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004898.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004898.MID
youtube: null
---

Andre bat ikusi det,
biruntan tantan biruntena,
andre bat ikusi det
joan dan astean.
Gizonaren galtzekin,
biruntan tantan biruntena,
gizonaren galtzekin
taberna batean.

Bertan edan dezadan,
biruntan tantan biruntena,
bertan edan dezadan
pinta bat ekartzu.
Honoko galtza hauek,
biruntan tantan biruntena,
honoko galtza hauek
prendan har itzazu.