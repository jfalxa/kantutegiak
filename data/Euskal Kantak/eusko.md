---
id: ab-5031
izenburua: Eusko
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005031.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005031.MID
youtube: null
---

Eusko, euskotarrak gara
ta maite dugu aberria,
Euskal Herria.
Aupa, mutilak,
maite dezagun Euskadi
askatasuna emanaz
Euskadi berari.
Aupa, mutilak,
maite dezagun Euskadi
askatasuna emanaz berari.