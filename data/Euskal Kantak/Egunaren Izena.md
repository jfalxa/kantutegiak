---
id: ab-5048
izenburua: Egunaren Izena
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005048.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005048.MID
youtube: null
---

Egunaren izena
ez dakit nola zan,
bainan seinalagarri
partidu bat bazan,
pelotan jokatzeko
Oiartzungo plazan.
Nahi det nere orduko
gertaera esan,
hurrengoan beste bat
eskarmenta dezan.

Partidua jokatzez
bukatu zanean,
ostatura joan nintzan
zuzen-zuzenean.
Mahaiera jana ekartzen
hasi ziranean,
apaiz bat serbitzen zan
aurren-aurrenean.
Makina bat sartu zan
haren barrenean.

Zintzur ona zuela
klaro seinaleak
eman zituen apaiz
barbantzu zaleak.
Dozenaka tragatzen
zituen aleak,
maskatu ere gabe
tripazain jaleak,
halako aisa nola
antxoa baleak.

Tragatutzen zituen
ihar ta hezeak,
ez ziran haren dina
zetozen klaseak.
Ekarri orduko zeuzkan
tripara paseak
kortesia gabeko
txapela luzeak.
Harek nahikoa jan ta
ni berriz goseak.

Nire ditxa guztiak
badute ajea,
noski naiz jaungoikoak
ahaztutzat lajea.
Goseak egin nuen
hango biajea.
Kulpa duenarentzat
daukat korajea,
apaiz batek jan zuen
nere potajea.

Geroztik ostatuan
sartu ordurako,
galdea egin gabe
ez det nik lajako:
Egun hartan apaizik
ote dan harako.
Ez apaiz jaun guztiak
igual diralako,
baizik batek bildurra
sartu zidalako.