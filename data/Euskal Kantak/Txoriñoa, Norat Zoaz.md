---
id: ab-4929
izenburua: Txoriñoa, Norat Zoaz?
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004929.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004929.MID
youtube: null
---

Txoriñoa, norat zoaz
bi hegalez airian?
Espainiarat joaiteko
elurra duk bortian.
Bi-biñoak joanen gaituzu
elurra urtu denian.