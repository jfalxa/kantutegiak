---
id: ab-4984
izenburua: Gure Koadrilan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004984.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004984.MID
youtube: null
---

Gure kodrilan,
gure kodrilan,
gure koadrilan mozkorra franko
eta gainera,
eta gainera,
eta gainera ezkontzerikan
ez du inork espero,
ez orain eta ez gero.
Hau duk zorioneko
ezkondu beharra,
ezkondu baino lehenago,
biziko naiz mutil zaharra.