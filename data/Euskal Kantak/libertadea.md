---
id: ab-4976
izenburua: Libertadea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004976.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004976.MID
youtube: null
---

Libertadea eskatzen diogu
herri hontako alkate jaunari
eta jende alegartzera
gaur gera gu honera etorri.
Entzun, jende maitea,
alaitu jendeak,
bixiguak ez du eman
aurtengo neguan.
Antxoak emango du
azaltzen danean.
Irabaziko degu
hurrengo batean.