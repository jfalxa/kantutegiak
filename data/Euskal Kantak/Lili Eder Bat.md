---
id: ab-4949
izenburua: Lili Eder Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004949.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004949.MID
youtube: null
---

Lili eder bat badut nik
aspaldi begiztaturik.
Bainan ez nainte mentura
haren eskua hartzera.
Banaki zer den lanjerra,
joan nindaiteke aldera.

Lili ederra, so idazu,
maite nauzunez errazu.
Zure begiak, bihotza,
barnarik deraut kolpatu.
Kolpe hontarik badizu
granganatzeko arrisku.

Haize hotzari guardia,
eguzkiari ez fia.
Zure kolore bizia
gerta liteke istia.
Esku on batek biltzia
desiratze(n d)izut, maitia.