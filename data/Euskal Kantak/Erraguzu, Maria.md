---
id: ab-4960
izenburua: Erraguzu, Maria
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004960.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004960.MID
youtube: null
---

- Erraguzu, Maria,
haren sorlekua,
ala zen palazio,
ala gaztelua.
- Oi! Ez, ez zen horrela
mundurat agertu,
eia tipi bat zuen
sorlekutzat hartu.

- Nor etorri zitzaion
agur egitera?
Hurbildu zen handirik
besten emaitera?
- Gizon eta emazte
guti zen agertu,
esklaboer bezala
zaukuten behatu.

- Ez zen bada, Maria
bederen artzainik
Jesus adoratzera
lehiatu zenik?
- Bai, artzainak zituen
lehenik ikusi,
hekien bihotz onak
zituen onetsi.

- Erraguzu, Maria,
bazen erregerik
zure seme Jainkoa
ikusi zuenik?
- Hiru errege ziren
noizbait gutaratu
eta beren dohainez
Jesus ohoratu.