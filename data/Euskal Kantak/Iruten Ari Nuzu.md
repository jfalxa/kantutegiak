---
id: ab-4942
izenburua: Iruten Ari Nuzu
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004942.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004942.MID
youtube: null
---

Iruten ari nuzu,
kilua gerrian,
ardura dudalarik
nigarra begian.

Nigar egiten duzu,
oi, suspirarekin!
Kontsolaturen zira
oi, denborarekin!

Ezkonmina dudala
zuk omen diozu.
Ez dut amore minik,
gezurra diozu.

Ezkonminak dutenak
sainale dirade,
matel hezurrak seko,
koloreak berde.

Ezkontzen balin bazira
marinelarekin,
sardina janen duzu
makailuarekin.

Ezkontzen balin bazira
mandazainarekin
arraina janen duzu
olioarekin.

- Anaia, nahi duzuia
emazterik erosi?
Baratze kantoinetan,
sosean hemezortzi.

- Arreba, nahi duzuia
gizonik erosi?
Eliza bazterretan,
bi sosetan zortzi.