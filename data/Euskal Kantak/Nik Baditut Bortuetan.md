---
id: ab-4940
izenburua: Nik Baditut Bortuetan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004940.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004940.MID
youtube: null
---

Nik baditut bortuetan
ardiak artzainarekin.
Itsasoan hamar untzi
beren marinelekin.
Katalunian ehun mando
zilar monedarekin.

Zure amak omen dio
ni pobrea naizela.
Zakutto bat hartu eta
joanen nauzu eskera.
Orduantxe bai, errain dute
ni pobrea naizela.

Zure alabak beharko du
Frantziako errege.
Frantziako ez balin bada,
Espainiako bedere.
Espainiakoa ezin bada,
zenabit enperadore.