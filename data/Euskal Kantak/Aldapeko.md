---
id: ab-5008
izenburua: Aldapeko
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005008.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005008.MID
youtube: null
---

Aldapeko sagarraren
adarraren puntan,
puntaren puntan,
txoria zegoen kantari.

Txiruliruli!
Txiruliruli!
Nork dantzatuko du ote
soinutxo ori?

Sagarraren adarraren iharraren
puntaren puntan
txoria zegonean kantari.

Ai, txiruliruli!
E, txiruliruli!
Nork dantzatuko du soinu hori.