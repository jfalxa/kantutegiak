---
id: ab-4995
izenburua: Haltzak Ez Du Bihotzik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004995.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004995.MID
youtube: null
---

Haltzak ez du bihotzik,
ez gaztanberak hezurrik
Ez nian uste erraiten ziela
aitunen semeek gezurrik.

Andozeko ibarra,
hara zer ibar luzea!
Hiruretan ebaki zaitan
armarik gabe bihotza

Bereterretxek oheti
Neskatoari eztiki:
- Habil, eta so egin ezan
gizonik denetz ageri?

Neskatuak berehala,
Ikusi zuen bezala:
Hiru dozena bazabiltzala
bortha batetik bestera.

Bereterretxek leihoti
Jaun kontiari goraintzi:
Ehun behi bazereitzola
beren zezena ondoti.

Jaun kontiak berhala,
Traidore batek bezala:
- Bereterrtetx, aigu bortara,
itzuliren hiz berhala.

- Ama, indazut atorra
menturaz sekulakoa!
Bizi denak oroit ukenen du
Bazko gauerdi ordua!

Mari Santzen lasterra
Bost mendietan behera!
Bi belaunez herrestan sartu da
Lakari Bustanobira.

- Bustanobi gaztea,
ene anaia maitia,
hitzaz onik ez balinbada,
ene semea joan da.

- Arreba, hago isilik!
Ez otoi egin nigarrik!
Hire semea bizi bada,
Mauleara dun joanik.

Mari Santzen lasterra
Jaun kontearen bortara!
- Ai, ei, eta, jauna, non duzue
ene seme galanta?

- Hik bahuena semerik
Bereterretxez besterik?
Ezpeldoi altuan dun hilik
habil, eraikan bizirik...

Ezpeldoiko jenteak,
Ala sendimentu-gabeak!
Hila hain hurran uken eta deus
ere ez zakienak!

Ezpeldoiko bukata
Ala bukata ederra!
Bereterretxen atorretarik
hiru dozena omen da.