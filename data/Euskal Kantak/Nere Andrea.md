---
id: ab-4922
izenburua: Nere Andrea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004922.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004922.MID
youtube: null
---

Nere andrea andre ona da
gobernu ona du auzoan,
hartzen duela bere alaba,
Mari Katalin alboan.
Ai zazu! Zer nahi zu?
Gerore horrela mundu honetan
biak biziko gerade gu,
baldin bazera kontentu.
Zu beti Praisku Txomin tabernakoa,
zu ikustera nator pozez betea.
Ai, zer kontentu! Ai, zer alegre!
Eskaintzen dizut, nere maitea,
arraultzatxoak eta kaiku esnea.

Arantzazura nindoanean
makiltxo baten gainean,
arantza beltz bat sartu zitzaidan,
ospela nuen oinean.
Ai zazu! Zer nahi zu?
Gerore horrela mundu honetan
biak biziko gerade gu,
baldin bazera kontentu.
Zu beti Praisku Txomin tabernakoa,
zu ikustera nator pozez betea.
Ai, zer kontentu! Ai, zer alegre!
Eskaintzen dizut, nere maitea,
arraultzatxoak eta kaiku esnea.