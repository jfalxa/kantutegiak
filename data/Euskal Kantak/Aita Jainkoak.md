---
id: ab-4974
izenburua: Aita Jainkoak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004974.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004974.MID
youtube: null
---

Aita Jainkoak egin banindu
zeruetako giltzari,
azken orduan jakingo nuke
atea nori iriki.
Lehenengo aitari,
gero, amari,
gero, anai-arrebari
ta azken orduan, isil-isilik,
nere maite politari. (berriz)

Gazte-gazterik aitak eta amak
praile ninduten nonbratu,
estudioak ikasitzera
Salamancara bialdu.
Salamancara
nindoalarik,
bidean nuen pentsatu
estudiante tunante baino
hobe nuela ezkondu.
Ezkondu nintzan,
ezkondu eta
bai eta ere damutu,
praka beltzetan hari zuria
ez zait sekulan faltatu.