---
id: ab-4964
izenburua: Olentzero
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004964.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004964.MID
youtube: null
---

Olentzero joan zaigu
mendira lanera,
intentzioarekin
ikatz egitera.
Aditu duenean
Jesus jaio dela,
lasterka etorri da
berri ematera.
Horra, horra,
gure Olentzero,
pipa hortzetan dula
eserita dago.
Kapoiak ere ba'itu
arraultzatxoekin
bihar meriendatzeko
botila ardoakin.

Olentzero gure
ezin dugu ase,
osorik jan dizkigu
hamar txerri gazte.
Saiheski ta solomo,
horrenbeste heste,
Jesus jaio delako
erruki zaitezte.

Horra, horra...

Olentzerok dakarzki
atsegin ta poza,
jakin baitu mendian
Jesusen jaiotza.
Egun argi honetan
alaitu bihotza,
kanpo eta barruan
kendu azkar hotza.

Horra, horra...

Olentzero gurea
begira honera,
izar dizdizari bat
doa Belenera.
Bere argi dizdiraz
goazen estalpera,
Jesus haurtxoarekin
gaua pasatzera.

Horra, horra...