---
id: ab-4989
izenburua: Agur, Adiskidea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004989.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004989.MID
youtube: null
---

Agur, adiskidea, Jinkoak egun on.
Zer berri den errazu zuk, otoi, Xiberun?
Ezagutzen duzuia Barkoxen Etxahun?
Holako koblaririk ez omen da nehon.
Bisitaz joan nindaite, ez balitz hain urrun.

Jauna, ezagutzen dut Etxahun Barkoxen,
egun orotan nuzu hurranik ibiltzen.
Beti gazte ezin egon, ari da zahartzen.
Lau hogei urte dizu mundin iragaiten,
orai ez da bertsetan anitz okupatzen.

Jauna, behar dautazu gauza bat onetsi.
Bainan ez zautzu behar hargatik ahantzi.
Hortaz egiten dautzut gomendiorik aski.
Konprenituren duzu hitz laburrez naski,
ene partez errozu jaun hari gorainzi.

Zure komisionea nahi dizut egin.
Konbenitzen zaitazu mila plazerekin.
Egon nahia zira arren jaun harekin?
Hona Etxahun bera hemen da zurekin...
Nik ere ez diota zu nor ziren jakin?

Jakin ere behauzu dudarikan gabe,
zeren egin dautazun errespetu galde.
Entzutea baduzu nitaz lehen ere.
Nor naizen erraitera ez nuzu herabe,
Lapurdin deitzen naute Jean Baptiste Otxalde.

Bihotzaren erditik egin dizut irri
zureganik entzunik parabola hori.
Damutzen ez bazaio zeruko jaunari,
eman behar dizugu zenbait bertset berri,
bai eta ber denboran eskuak elgarri.

Amodioz derautzut eskua hedatzen,
zonbat atsegin dudan ez duzu pentsatzen.
Gizon batzuek naute lazki atakatzen,
ez dakiala ongi bertsuen paratzen...
Zuk laguntzen banauzu, ez naute lotsatzen.

Etxahun Xiberuan, Otxalde Lapurdin,
buruzagi girenak kantoren egitin,
ez baikirade beldur nehondik jin dadin
eraman deikienik hotan Eskual Herrin,
jokaturen dugula hek plazer dutenin.

Ez gira sartu behar hortaz urguiluan,
etsaminatu gabe gure izpirituan.
Irakurtu izandu lehen liburuan
jaunen jaunak direla jalgitzen munduan,
erbia lo dagola uste ez den lekuan.

Lehen zaharretarik entzuna dut hori
beti norbait badela denen buruzagi.
Arren nahi badira gure kontra jarri,
oneski emaitera zenbait bertset berri,
lotsa gabe dituzu denen zerbitzari.