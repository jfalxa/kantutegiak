---
id: ab-4936
izenburua: Begira Nago
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004936.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004936.MID
youtube: null
---

Begira nago, begira,
hor goiko bide barrira,
noiz etorriko ete dan
maitea neure herrira.