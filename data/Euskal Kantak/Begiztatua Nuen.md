---
id: ab-5028
izenburua: Begiztatua Nuen
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005028.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005028.MID
youtube: null
---

Begiztatua nuen aspaldi mendian
mizpirondo zuzen bat sasien erdian.
Handitzerat utzi dut eta harekila
aurten egin dut nere eskualdun makila.
Tra lala lala la, tra lala lala la,
aurten egin dut nere eskualdun makila.

Bi erroden gainean joaten ez ikasi,
burdinbidetan, aldiz, sabela nahasi.
Bainan non dut karroza? Non dut berebila
Oinez beti banoa, eskuan makila.

Askotan ikusi tut ibilki herrian
aitzindari erneak ezpata gerrian.
Bekaiztia niganik urrun joan dadila
zeren ez baitut nik eskualdun makila.

Igandeetan goiz-goiza, txamarra emanik,
etxekoandreari agur bat erranik,
entzun eta oihuka dorreko ezkila,
ni banoa harturik eskualdun makila.

Nere sakelak nahiz ontsaino miatu,
jin zait behin gizon bat baina zer gertatu ?
Segurki ez du erran batere ez nekiela
ibiltzen eskuetan eskualdun makila.