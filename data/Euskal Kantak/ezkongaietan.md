---
id: ab-4918
izenburua: Ezkongaietan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004918.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004918.MID
youtube: null
---

Ezkongaietan zerbait banintzan,
ezkondu eta ezer ez.
Ederzalea banintzan ere,
aspertu nintzan ederrez.
Nere gustua egin nuen ta
orain bizi naiz dolorez.

Nere andrea andre ederra zan
ezkondu nintzan orduan.
Mudatuko zan esperantzarik
ere, batere ez nuan.
Surik batere baldin badago,
maiz dago haren onduan!.

Nere andrea alperra da ta
ez da munduan bakarrik.
Gauza gozoen zalea da ta,
ez du egin nahi beharrik.
Sekulan ere ondo izango
ez halakoren senarrik.

Zokoak zikin, tximak jario,
haurra zintzilik besoan.
Adabakia desegokia,
gona zaharraren zuloan.
Hiru txikito boteila haundia
dauka berekin alboan.

Larunbatetik larunbatera
garbitzen ditu zatar bi.
Bere haietxek berotutzeko
egur erretan zama bi,
belaun bietan bana hartuta
ez da isiltzen kantari.

Nere andrea goiz jeikitzen da,
festara behar danean.
Buruko mina egiten zaio
hasi baino lehen lanean.
Zurekin zer gertatuko ote zan
nik bildurrik ez nuan.