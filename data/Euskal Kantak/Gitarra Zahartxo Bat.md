---
id: ab-5038
izenburua: Gitarra Zahartxo Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005038.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005038.MID
youtube: null
---

Gitarra zarhartxo bat det nik nere laguna
horrela ibiltzen da artista euskalduna.
Egun batean pobre, besteetan jauna,
kantatzen pasatzen det nik beti eguna.

Nahiz dela Italia, orobat Frantzia,
bietan bilatu det mila malizia.
Korritzen badet ere nik mundu guztia
maitatuko det beti Euskaldun Herria.

Jaunak ematen badit neri osasuna,
oraindik izango det andregai bat ona.
Hemen badet frantsesa, interesaduna,
bainan nik nahiago det hutsik euskalduna.

Adios, Euskal Herria, ez baña betiko,
bost-sei urte honetan ez det ikusiko.
Jaunari eskatzen diot grazia emateko
nere lur maite hontan bizia uzteko.