---
id: ab-5052
izenburua: Nagusi Jauna
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005052.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005052.MID
youtube: null
---

- Nagusi jauna, auxen da lana
amak bialtzen nau berorrengana.
- Ai, bialduko bazinduke maiz!
zu ikusita, kontsolatzen naiz.
Sinista zazu,
oso zoraturikan nakazu!!
Horren polita nola zera zu?

- Amak esan dit, etxeko errenta
pagatzeko dirurik ez zuten ta,
ia eguingo dion mesede,
hilabetean gehienaz ere,
itxoitea,
bada zierto dala ematea,
pasa bainoo lehen hilabetea.

- Logratutzea, errez halare,
nahiz hori ta beste zer nahi gauza're.
seguro dala esan amari,
zu baldin bazatoz mandatari.
Dudarik gabe,
nere borondatearen jabe
zu zera Juana Bixenta Olabe.

- Beraz bertatik noa etxera
poz haundi bat amari ematera.
Orain adio, nagusi jauna,
presaka joan biat amagana.
- Ez zoazela,
portatu zaite nik nahi bezela,
gaur etxera joan ez zaitezela.

- Zuen etxea, dago urruti,
bidean zer gerta izan bildurti.
Bada ia pasa da eguna,
arratsa berriz dator iluna,
orain joatea;
txartzat eduki, nere maitea,
gaur hemen bertan gera zaitea.

- Ez joan etxera, bide hoietan,
gauaz, bakarrik eta ilunpetan.
Kontura erori zaitea zu,
gauaz ilunpean joan biazu,
peligro haundiz
eta hobe dezu, ehun aldiz,
bihar goizean joan egun argiz.

- Ezetz etsia egon liteke,
ni hemen gelditu ezin niteke.
Amak espero nau gauerako,
eta hark espero nauelako,
joan behar det nik.
alaba ikusi bagetanik,
amak izango ez luke onik.

Hemen zuretzat dira parako,
jaki txit goxoak afaitarako.
Ederia berriz,-Jaungoikoa!-,
hilak piztutzeko modukoa.
Ardo ondua,
ontzen urteetan egondua,
Malagatikan da bialdua.

- Jauna berorrek, bainan alferrik,
badaki armatzen lazo ederrik;
lazoa ehiz bage gera dedin,
nere etxean nahi ditut egin,
afaldu ta lo.
Nahiago ditut hango bi talo,
ta ez hemengo mila erregalo.

Bihotz nereko, dama polita,
asmoz muda zaite, hauxen tori ta:
hamasei duroko urre fiña,
sendatu nahi zazun nere mina.
- Ez, jauna, ez, ez!
merezi bezin kopeta beltzez,
esango diot ezetz ta ezetz.

- Gainera berriz, Juana Bixenta,
utziko dizutet etxeko errenta.
Kito zor dirazuten guzia,
kunplitzen banazu kutizia.
- Lotsa gogorra!!
sufritzen dago gaur nere honra,
penaz malkoak darizkit horra!

- Nik ditut kulpak, ez eguin negar,
horlakorik ez nizun esan behar:
Animan sentitutzen det mina,
zuri ofensa hori eguina,
Maldizioa!
ez dakit non nekan juizioa,
eskatzen dizut barkazioa!

- Barkazioa, du ta eskatu,
nere bihotz onak ezin ukatu,
erakutsi didalako, jauna,
gaitz eguinaren damutasuna,
konforme nago;
ez egin ta ofensa gehiago,
gaurkoa oso ahaztutzat dago.

- Berriz nik egin zuri ofensa?
arren horlakorik ez bada pensa!
Zaukazkit neskatxa fin batetzat,
eta gusto haundiz emaztetzat
hartuko zaitut.
Bihotzetikan esaten dizut,
zuretzat ona baderizkizut.

- Hori egiaz baldin badio,
neregana egon liteke fio:
bainan usatuaz kortesia,
ama neriari lizentzia,
eska bezaio.
- Hori gustura egingo zao,
bihar goizean joango natzaio.

- Banoa orain. Atoz honuntza,
biderako hartu zazu laguntza.
adi zazu, morroi Joxe Joakin,
etxera farolan argiakin
aingeru honi,
gaur zu laguntzea da komeni,
kezkeetan egon ez nadinen ni.

Morroia:
Aingeru hari lagundu eta,
horra jauna egin ostera buelta.
bidean bai bildurtxo zan bera,
bainan ailegatu ta etxera,
kontentu zegon.
orain kezkeetan ez du zer egon,
ohera joan ta pasatu gau on.

Heldu zanean, hurrengo goiza,
nagusiak zuen kunplitu hitza:
jan zitzaion amari etxera,
andretzat alaba eskatzera.
amak txit firme:
hartuta gero mila informe,
gelditu ziran biak konforme.

Handik zortzi bat egunetara,
edo hamar ziran ez dakit bada.
Ez, hamar ezin zitezkeen izan,
handik zortzi egunera elizan,
ezkondu ziran.
Ezkondu ta etxera segidan,
jendea franko bazuten jiran.

Bost hilabete, edo sei hontan,
ez dirade beti egondu lotan.
eman dute zenbait jira-bira,
eta gozatutzen bizi dira,
ditxak ausarki.
Hobeto ezin litezke arki
espero dute haurtxo bat aurki.

Dama gazteak, ez egon lotan,
begira zaitezte ispilu honetan:
gustatuta birtute ederra,
andretzat hartu bere maizterra,
du nagusiak;
horla irabaztera graziak,
saia zaiteste beste guztiak.