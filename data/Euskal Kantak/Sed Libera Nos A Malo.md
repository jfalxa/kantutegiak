---
id: ab-4904
izenburua: Sed Libera Nos A Malo
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004904.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004904.MID
youtube: null
---

Sed libera nos a malo
sit nomen Domini.
Vamos a cantar un canto
para divertir.

Jan dugunez gerozti
xahalki oneti
eta edan ardoa
durunzunekoti,
chantons mes chers amis.
Je suis content parti,
trinquam d'aques bon vin.
Eta dezagun kanta
kantore berri.