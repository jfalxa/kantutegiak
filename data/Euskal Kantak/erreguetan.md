---
id: ab-4913
izenburua: Erreguetan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004913.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004913.MID
youtube: null
---

- Erreguetan ari nintzala
hartu nuen albistia.
Ama zureak nahi ez duela
biok alkarganatzia.
Nola ez neukan oraindiokan
gauz ori erabakia,
berbertatikan asmo hartzen det
lekaimetan sartutzia.

- Min-eriturik arkitzen danak
egin beharko negarra.
Nahiagatikan logratu ezin
desio nuen izarra.
Mundu hontatik eramango det
damutasun bat bakarra:
zu gabetanik nere bizitza
hemen igaro beharra.

- Ez egin arren, Iñaziotxo,
horregatikan negarrik.
Mundu honetan ez dago, bada,
egiterikan nahi danik.
Oroitu zaitez beste bat ere
badagoela oraindik,
goiz gauetako errezuetan
ez naiz ahaztuko zugatik.

- Zure izena badaukat eta
edukiko det goguan.
Besterenikan ez zait sartuko
nere bizitza osuan.
Debekatutzen duten ezkero
ezkondutzea munduan,
saia gaitezen ahaleginean
alkartutzeko zeruan.