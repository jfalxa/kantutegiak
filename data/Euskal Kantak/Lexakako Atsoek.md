---
id: ab-4901
izenburua: Lexakako Atsoek
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004901.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004901.MID
youtube: null
---

Lexakako atsoek
jakiten badute
nor den ofiziale
konponduren dute,
bonbila sakelean
aguardiente eske,
larrua estaltzeko
arroparik gabe.