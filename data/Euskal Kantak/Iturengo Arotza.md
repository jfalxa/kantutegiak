---
id: ab-5006
izenburua: Iturengo Arotza
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005006.MID
youtube: null
---

Iturengo arotza, Erramun Joakin,
haserre omen zaude zeren dugun jakin.
Santurik ez laiteke fiatu zurekin,
San Kristobal urtu ta joareak egin.

Arotzak erran dio bere andreari:
- Urtu behar dinagu, ekarran santu hori.
- Gizona, hago isilik! Bekatu duk hori.
- Ez zionagu erranen sekulan inori.

Arotzak erran dio bere andreari:
- Begira zaion ongi joare multzoari
soinuan bilatu ta denak zeuden ongi,
salatu ez bagindu koinatak, Joana Mari...

Kobrezko santurikan inon bazarete,
egoten ahal zarete hemendik aparte,
bertzenaz arostz horiek jakiten badute,
joareak eiteko urtuko zaituzte.