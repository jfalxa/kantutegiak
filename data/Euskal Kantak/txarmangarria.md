---
id: ab-4950
izenburua: Txarmangarria
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004950.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004950.MID
youtube: null
---

- Txarmangarria zira, eder eta gazte,
ene bihotzak ez du zu baizikan maite.
Bertze zonbait bezala, ote zira libre?
Zurekin ezkontzeaz dudarik ez nuke.

- Zuri-gorria zira, arrosa bezala,
profetak ere dira mintzo hola-hola.
Araberan bazinu gorputza horrela
iduriko zinuen... Zeruko izarra.

- Oi, maitea, zatozkit plazer duzunean,
behork ikusi gabe, ilunabarrean.
Lagun bat badukezu joaiteko bidean,
hark ezarriren zaitu trankil bihotzean.

- Plazer eginen duzu, isiltzen bazira,
haur inorantak hola tronpatzen baitira.
Ez da neretzat ina holako segida...
Bertzalde zure baitan ez naiteke fida.

- Adio, beraz, orai, ene arraroa,
hori dela medio, herritik banoa,
bihotza triste eta kexuan gogoa,
beti jarraikiren zait zur' amodioa.