---
id: ab-5044
izenburua: Ikusten Duzu Goizean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005044.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005044.MID
youtube: null
---

Ikusten duzu goizean,
argia hasten denean,
menditto baten gainean,
etxe ttipitto, aintzin zuri bat
lau haitzondoren erdian,
zakur zuri bat atean,
iturritto bat aldean?
Han bizi naiz ni bakean.

Nahiz ez den gaztelua,
maite dut nik sorlekua
aiten aitek hautatua.
Etxetik kanpo zait iruditzen
nonbait naizela galdua.
Nola han bainaiz sortua,
han dut utziko mundua,
galtzen ez badut zentzua.

Ez da lurrean gizonik,
printzerik ez erregerik,
ni baino hobeki denik.
Badut andrea, badut semea,
badut alaba ere nik.
Osasun ona batetik,
ontasun aski bertzetik...
Zer gehiago behar dut nik.

Goizean hasiz lanean
arratsa heldu denean,
nagusi naiz mahainean.
Giristino bat ona dut hartu
nik emaztea hartzean.
Ez du mehe-egunean
sartuko, uste gabean,
txhingar hezurrik eltzean.

Etxean ditut nereak
akilo, aitzur, goldeak,
uztarri eta hedeak.
Iazko bihiez ditut oraino
zoko guziak beteak.
Nola iragan urteak
emaiten badu bertzeak,
ez gaitu hilen goseak.

Landako hirur behiak
- esnez hanpatu ditiak-
aratze eta ergiak,
bi idi handi kopeta-zuri,
bizkar-beltz, adar handiak,
zikiro, bildots guriak,
ahuntzak eta ardiak...
Nereak dira guziak!

Ez dugu behar lurrean,
aise bizirik etxean,
utzi laguna gabean...
Jende beharrek ez dute jotzen
gure etxeko atean
non ez duten mahainean,
otoruntz ordua denean,
lekua gure aldean.

Piarres nere semea,
nahiz oraino gaztea,
da mutiko bat ernea.
Goizean goizik bazken erdirat
badarama artaldea.
Baitu bere egitea,
segituz nere bidea,
ez du galduko etxea.

Nere alaba Kattalin
bere hameka urtekin
ongi doa amarekin.
Begiak ditu, amak bezala,
zeru-zola bezain urdin.
Oraiko itxurarekin
uste dut, denborarekin,
andre on bat dion egin.

Nere emaztea Maria
ez da andre bat handia,
bainan emazte garbia.
Musu batentzat etxean badut
nik nahi dudan guzia.
Galdegiten dut grazia,
dudan bezala hasia,
akabatzeko bizia.