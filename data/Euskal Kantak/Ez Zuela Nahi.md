---
id: ab-4983
izenburua: Ez Zuela Nahi
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004983.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004983.MID
youtube: null
---

Ez uzela nahi ez zuela nahi,
atean atzean ez zuela nahi,
ganbaran bai, ganabran bai...
Mal gobierno etxean,
zakurran potroak eltzean.
Gaur gauean argirik gabe,
jo zan larrua beldurrik gabe...
Gau ilunean argiakin,
kontuz ibili zakilakin.
Errekaldeko Madalenak
haretxek omen dik zabalena...
Ta Errekaldeko Xixilik
hark eiten omen dik isilik.