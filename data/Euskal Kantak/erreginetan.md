---
id: ab-4969
izenburua: Erreginetan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004969.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004969.MID
youtube: null
---

GIZONARI
Sagar gorri mazatua,
denbora dela oroitua,
denborak ditu egin zeniola
andre nobiari mandatua.

Zure andreak igorri gaitu
zurekin dela mandatua,
aspaldiko denbora hontan
ez dela zurekin egondua.

ANDREARI
Etxekoandre zabala,
leku oneko alaba,
bide honetan aditu dugu
limosneroa zarela.

Saia duzu lurreraino,
sederia gerriraino,
zu bezelakorik ez dela jaio
Siberiatik honeraino.

MUTILARI
Ilarra zaigu loratu,
oraindik ez da lekatu.
Ezkongaia zarelarik,
nahi zaitugu koplatu.

Koplatu eta koplatu,
kopla sariak behar ditugu.
Gu ere neskatx gazteak gara,
dotea bildu behar dugu.

NESKATXARI
Hiru seme dituen amak,
hiruetatik bat, arrosa,
tatik hauta zuretako da,
dontzeila eder, garbosa.

EMATEN EZ DUENARI
Utzan, utzan isilik.
Horrek ez din dirurik,
zaku zahar bat hor diaraman
zorri zuriz beterik.

ERDALDUNARI
Pasa, pasito pasa,
pasa muralla gainetik,
a los ricos caballeritos,
echusté un par de realitos.