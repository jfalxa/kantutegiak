---
id: ab-4952
izenburua: Abenduko Hilaren
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004952.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004952.MID
youtube: null
---

Abenduko hilaren
hogeita lauean
Jesus gure jabea
jaio da gauean.
Gu ipintzeagatik
izate hobean,
bihotzak zuzenduta
zerura bidean.

Jaungoikoa estalpe
zokoan jaiotzen,
astoa ta idia
arnasaz berotzen.
Zeru lurren jabea
zaigu lurreratzen,
gu guziongatikan
honela umiltzen.

Jaungoikoa mundura
gizon eginikan,
horra nola jaio dan
Mariagandikan.
Batere galdu gabe
garbitasunikan,
beste hamar ez da
horrelakorikan.

Amari Jose eman
zitzaion senartzat,
Jesusek izan zitzan
biak gurasotzat.
Hemen bizi geranak
hartu behar ontzat,
mesede haundirako
dira guziontzat.

Nork pentsatu zezakeen
Jesus haur apala
gizona salbatzera
etorriko zala?
Bizi gitezen bera
maitatutzen dala,
gu zeru ederrera
eraman gaitzala.