---
id: ab-4945
izenburua: Goizean Goiz Jeikirik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004945.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004945.MID
youtube: null
---

Goizean goiz jeikirik,
argia gaberik,
urera joan ninduzun,
pegarra harturik.
Traleralala...

Jaun txapeldun gazte bat
jin zautan ondotik,
eia nahi nuenez
urera lagunik.
Traleralala...

Nik ez nuela nahi
urera lagunik
aita beha zagola
salako leihotik.
Traleralala...

Aita beha zagola,
ezetz erranagatik,
pegarra joan zitzautan
besorik harturik.
Traleralala...

Urera gainean
ez ginen egartsu
galdera egin zautan:
Zenabt urte tuzu?
Traleralala...

Hamasei, hamazazpi
orain ez konplitu.
Zurekin ezkontzeko
gazteegi nuzu.
Traleralala...

Etxerat itzultzeko
nik dudan beldurra!
Ez jakin nola pentsa
amari gezurra.
Traleralala...

Arreba, nahi duzu
nik erakuts zuri,
etxerat etortzean,
zer erran amari?
Traleralala...

Urzo txuri polit bat,
gauaz dabilana,
hark ura zikindurik,
egotu naiz, ama.
Traleralala...

Dakigunez geroztik
zer erran amari,
dugun pegarra pausa,
gaitezen liberti!
Traleralala...