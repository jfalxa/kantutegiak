---
id: ab-5042
izenburua: Zugana, Manuela
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005042.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005042.MID
youtube: null
---

Zugana, Manuela
nuanean pentsatu,
uste det ninduela
deabruak tentatu.
Ikusi bezin ekin
naiz enamoratu.
Ojala ez bazina
sekulan agertu!

Amorioz beterik
esperantza gabe,
zergatik egi zinan
bihotz honen jabe?
Zuk esan behar zenduen:
- Hemendikan alde.
Egiazki ez naiz ni
bizardunen zale!

Ahaztu nahi zaitut bainan
ezin, ezin ahaztu.
Zure amorioak
burua dit galdu...
Orain bear zenduke
bihotz ori bigundu
eta maitagarri horrek
pizka bat lagundu.

Barkatu behar dituzu
nere erokeriak,
zuri begira daude
nere bi begiak.
Garbi-garbi esan ditut
nere ustez egiak,
zoraturikan nauka
zure arpegiak.

Gauean ibili naiz
guztizko ametsetan,
Donostian nengoela
Andre Marietan.
Eta ikusi nuela
herri artako plazan,
erdian zebilela
Manuelatxo dantzan.