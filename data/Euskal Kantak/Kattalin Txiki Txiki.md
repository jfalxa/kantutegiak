---
id: ab-5018
izenburua: Kattalin Txiki Txiki
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005018.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005018.MID
youtube: null
---

Kattalin txiki-txiki,
Kattalin arina,
zenbana saltzen duzu
dozena sardina?

Hamalau marabedi,
prezio jakina
baldin nahi ez baduzu
banoa aitzina.

Ni naiz emakume bat
arrain saltzailea,
itsastik marinelak
dakarren guztia.