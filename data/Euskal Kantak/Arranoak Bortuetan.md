---
id: ab-4925
izenburua: Arranoak Bortuetan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004925.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004925.MID
youtube: null
---

Arranoak bortuetan
gora dabiltza hegaletan.
Ni ere beste orduz
andereki kanberetan
Orai aldiz ardura, ardura,
nigarra dizut begietan.

Ez dut ta nik pena handia,
entzulerik ukan gabia,
juratu dinat fedia
doluturen zen maitia,
eni behatu gabia,
galdu dinat libertatia.

Erlea doa mendiz mendi,
harek han biltzen du lili.
Hartarik egiten du ezti,
Eztiak oro ez bardin ezti,
munduko neskatila gazteak
ez dira bardin xarmangarri.