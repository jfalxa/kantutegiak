---
id: ab-4977
izenburua: Neri Deritzait Galtzaundi
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004977.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004977.MID
youtube: null
---

Neri deritzait Galtzaundi,
apellidoa det Goñi,
Aldasoro izengoitiz
deitutzen didate neri.
Hiltzerikan ez nuen
lehenago pentsatzen,
bainan oraingotikan
ez naiz eskapatzen,
testamentu egitera
noa ni lehenbailehen
nere parienteak
lasaitu ditezen.
Ta Galtzaundi,
ta Galtzaundi,
apellidoa det Goñi.

Aitaren ta Semearen,
Espiritu Santuaren,
Jaun zerukoak lagun zaidala
lan honetan neri arren.
Sentimentu bakarra
daukat barrenen:
alaba ipurtaundia
utzitzea hemen.
Horra neregatikan
hoiek nola dauden,
Jaunak eraman nazala
zerura lehenbailehen.

Ta Galtzaundi,...

Nere lehengusu Pakoflinori
nahi diozkat bada utzi,
ner'eskatzeko estanteria
bere jenero ta guzi.
Bainan hango kristalik
ez dezala hautsi,
bestela utziko diozkat
guziak Ramosi.
Egiten badiote
jaun honi hiesi,
laister joango zaizka
danak Rodríguezi.

Ta Galtzaundi,...

Ni hilda gero emaztea
penaz hilko balitzake
nere ondoan enterratzea
inola ahal balitz nahi nuke.
Eta gainean jarri
letra bat haundia:
"Hemen dago Galtzaundi
eta konpainia".
Poz haundia bat hartu zun
Tolosako herriak,
merkatu ziralako
ordun edariak.

Ta Galtzaundi,...

Entierroa nahi det egitea
neri bigarren klasean,
lau zapatarik eramatea
gainera berriz aidean,
jota nabarra joaz
Enpero kalean,
sobrekamak balkoitan
naramatenean,
Arramele zubian
aizken-aizkenean,
zahagi bat ardo jarri
kajaren gainean.

Ta Galtzaundi,...

Nik oraindaino egin izandu
ditudan testamentuak
gaur bertatik izango dira
haiek danak borratuak.
Eta orain firmatu
nere testiguak,
aditu baituzte
gaur nere kontuak,
isilik eduki arren
nere sekretuak.
Dametako iturrian
edan kuartilo bat.

Ta Galtzaundi,...