---
id: ab-5057
izenburua: Maria Solt Eta Kastero
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005057.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005057.MID
youtube: null
---

Maria Solt eta Kastero
bi amoros zahar bero.
Hirurogei hamarna urtetan hartu die amorio.
Kastero jelostu gero, Maria Solt ezarri kanpo.

Mari Solt doa nigarrez,
izorra dela beldurrez.
Barnets bordako andereak kontsolatu du elez:
emazte zaharrik okupu agitzen ez dela, ez.

Maria Soltek arrapostu:
- Santa Elisabet baduzu,
saintu zahar batenganik, okupu agitu duzu.
Kastero ere bada saintu, hala nizan beldur nuzu.

- Kastero ez duzu saintu.
Sobera biraoti duzu.
Elizarat joan eta tabernan egoten duzu.
Kastero denagatik saintu, Maria Solt antzu zira zu.