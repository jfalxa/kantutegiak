---
id: ab-4979
izenburua: Txantxibirin
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004979.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004979.MID
youtube: null
---

Txantxibirin, txantxibirin
galbiltzanean,
Elorrioko kalean,
hamalau atso tronpeta jotzen
zazpi astoren gainean.
Astoak ziren
txiki-txikiak,
atsoak berriz
kristonak.
Nola demontre konpontzen ziren
zazpi astoren gainean.