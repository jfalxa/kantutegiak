---
id: ab-4944
izenburua: Nere Maitea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004944.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004944.MID
youtube: null
---

- Nere maitea, jeiki ta jeiki.
Ez al zerade loz ase?
Zure ondoren gabiltza hauek
ez baikerade logale.
Iriki zazu portañe hori,
adiskideak gerade.

- Ez dizut irikiko
gau ilunean aterik.
Barrengoak ez daki
kanpokoaren berririk.
Zatoz eguna argitzen danean,
Egongo gera alakarrekin.