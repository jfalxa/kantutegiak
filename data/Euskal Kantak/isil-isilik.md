---
id: ab-4975
izenburua: Isil-Isilik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004975.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004975.MID
youtube: null
---

Isil-isilik dago
kaian barrenean
ontzi zuri polit bat
uraren gainean.

Goizeko ordu bietan
esnatutzen gera
arrantzaleak beti
joateko urrutira.

Pasatzen naizenean
zure leihopetik
negarra irteten zait
begi bietatik.

O,maitea! O, laztana!
Neure poza t'atsegina!
Zu gabe nire bizitza
mingots iluna da.

Zergatik, zergatik,
zergatik, zergatik?
Zergatik negar egin?
Zeruan izarra dago
itsaso aldetik.

Bat, bat, bat,
bart parrandan ibili,
bart parrandan ibili.
Bi, bi, bi,
ez naiz ondo ibili,
ez naiz ondo ibili.
Hiru, hiru, hiru,
golkoa bete diru,
golkoa bete diru.
Lau, lau, lau,
sardina bakalau.

Anteron txamarrotea,
Sinkerren bibotea,
haretxek ei dauko,
preso tximinoia.

Hau duk, hau duk,
hau duk humorea,
kontsolatzeko,
kontsolatzeko
euskaldun jendea.
Kalean gora
kalean behera
kalean gora
zezena,
ai, ai, ai, ai!

Kalean gora
kalean behera
kalean gora zezena.