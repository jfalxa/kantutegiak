---
id: ab-4957
izenburua: Belenen Sortu Zaigu
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004957.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004957.MID
youtube: null
---

Belenen sortu zaigu Jinkoa
arratseko gauerdian.
Hotzez dardaraz dago gaisoa,
lasto pixkaren gainean.
Bero ahalez bero dezagun,
apa emanez musuan.

Anitz maite dut nere ama,
ni sortu ninduelako.
Baina haurtxo hau maiteago dut,
nigatik jaio delako,
ta gurutzean gero odola
isuriko duelako.