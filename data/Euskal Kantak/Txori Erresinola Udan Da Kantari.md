---
id: ab-4932
izenburua: Txori Erresinola Udan Da Kantari
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004932.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004932.MID
youtube: null
---

Txori erresinola udan da kantari,
zeren orduan baitu kanpoan janari.
Neguan ez da ageri baldinba ez da eri.
Udan jin baledi
kontsola nainte ni.

Txori erresinola ororen gehien,
besteek naino hobeki hark baitu kantatzen.
Harek du enaginatzen munduan bai tronpatzen.
Bera ez dut ikusten,
bai boza entzuten.

Boz haren entzun nahiz, erraturik nago.
Ni hari illant eta hura urrunago.
Jarraiki ninkiro bizia gal ateino.
Alpaldi handian
desir hori nian.

Txoria, zoinen eder kantuz oihanean!
Nihaurek entzun dizut igaran gauean.
Eia, goazen, maitea, bi-biak ikustera.
Entzuten baduzu,
xarmaturen zitu.