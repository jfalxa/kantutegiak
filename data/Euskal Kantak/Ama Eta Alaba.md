---
id: ab-4911
izenburua: Ama Eta Alaba
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004911.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004911.MID
youtube: null
---

Alaba: Amak ezkondu ninduen
hamabost urtetan.
Senarrak gehixeago
larogei nonbait han
eta ni neskatila gazte
nola nindeke izan?
Nola nindeque izan.

Ama: Neska isilikan hago
aberatsa den hori.
Pazientzin pasa itzan
urte bat edo bi.
Laster hilko den zaharra eta
biziko haiz ongi.

Alaba: Ama, alperrikan ari da
hizketa moduan.
Egun bat pasatzeko
bizitzen munduan,
ez det nik agure zaharrikan
nahi nere onduan.

Ama: Neska, egin zan amaren
esana berehala.
Izango haiz ederki
markesa bezela,
ez hadinela izan bada
holako ustela.

Alaba: Ama, nola nahi du hori
gogoz egitea?
Beste gauzatxo bat det
nik borondatea:
Diru gabea nahiago det
senarra gaztea.

Ama: Neska, etzanala hartu
dirurik gabea.
Bestela izango den
nahiko lanbidea.
Txorakeri denak utzita
har zan agurea.