---
id: ab-5024
izenburua: Amerikara Noa
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005024.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005024.MID
youtube: null
---

SEME BAT
Amerikara noa nere borondatez
hemen baino hobeto bizitzeko ustez.
Aspertutxea nago hemengo izatez,
adio, aita eta ama, ondo bizi bitez.

AITA
Lehenago ere seme bat badut Amerikan
hamar bat urte dira joan zala hemendikan.
Topatzen baduk inoiz haren aztarnikan,
esaiok aita bizi dala oraindikan.

BESTE SEMEA
Kafea hartutzen du egunean bi aldiz,
bai paseatu ere nahi adina zaldiz.
Purua erre eta osasuna berriz...
Aita, hau bizimodua, Donostian balitz.