---
id: ab-4930
izenburua: Txoriñoa Kaiolan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004930.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004930.MID
youtube: null
---

Txoriñoak kaiolan
tristerik du kantatzen.
Duelarik han zer jan, zer edan,
kanpua du desiratzen.
Zeren, zeren,
libertatea zoinen eder den.

Hik kanpoko txoria
sogiok kaiolari.
Ahal baldin bahedi, bahedi,
hartarik begira hadi.
zeren, zeren,
libertatea zoinen eder den.

Barda amets egin dit
Maitia ikhousirik:
Ikhous eta ezin mintza,
Ezta phena handia?
Ala ezina!
Desiratzen nuke hiltzia...