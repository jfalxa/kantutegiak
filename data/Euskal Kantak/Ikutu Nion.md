---
id: ab-4985
izenburua: Ikutu Nion
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004985.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004985.MID
youtube: null
---

Ikutu nion, ikutu nion kopeta,
esan zidan, esan zidan ezetz,ezetz,
hura ez ela zaina sentitzen zuena
penaz ta dolorez:
"Un poco más abajo señor doktore
(pixkat beheraxeago señor doktore),
señor doktore".
Ikutu nion begia, ikutu nion begia...

Ikutu nion begia, ikutu nion sudurra...

Ikutu nion begia, ikutu nion ahoa...

Ikutu nion begia, ikutu nion lepoa...

Ikutu nion begia, ikutu nion bularra...

Ikutu nion begia, ikutu nion zilborra...

Ikutu nion begia, ikutu nion izterra...

Ikutu nion begia, ikutu nion zuloa (lorea)
Esan zidan, esan zidan baietz, baietz,
hura zla zaina sentitzen zuena
penaz ta dolorez:
"Un poco más adentro señor doltore
(Pixkat berruxeago señor doktore),
señor doktore".