---
id: ab-4937
izenburua: Bentatik Nator
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004937.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004937.MID
youtube: null
---

Bentatik nator,
bentara noa,
bentan da nere gogoa.
Bentako arrosa
krabelinetan,
hartu dut amorioa.

Zuk ederretan,
nik galantetan,
zuk ederretan gogoa.
Ederretan dan
galantena da
Ana presentekoa.

Horrek galai bat
behar omen du,
bera dan bezalakoa.
Bera ona ta
hobea luke
Joxe Mari presentekoa.