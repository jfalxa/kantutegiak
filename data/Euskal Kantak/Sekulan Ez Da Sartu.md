---
id: ab-4972
izenburua: Sekulan Ez Da Sartu
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004972.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004972.MID
youtube: null
---

Sekulan ez da sartu izandu
donostiarrik zeruan,
nonbait ez zuten meriturikan
irabazitzen munduan.
Gaisorentzat Jaungoikoaren
justizia dan moduan,
kondenaturik arkitzen ziran
haiek danan infernuan.

Behin bat San Pedro bultzaturikan
zeruetara sartu zan,
ta berehala presa guzian
jende tartean gorde zan.
San Pedro berriz bila zebilen
nonbait azaltzen ote zan,
ahalegin franko eginagatik
inondik ageri ez zan.

Orduan Pedro Jaunarengana
larri joan zan berehala,
eta esan zion donostiarra
zeruetan sartu zala,
jende tartean ezkutatuta
ezin bilatu zuela,
infernurako destinatua
anima hura zegoela.

Jaunak Pedrori modu honetan
eman zion errespuesta:
- Donostiarra izanez gero
bilatzen txit zaila ez da.
Idiarena danbolinakin
jotzen hastea da basta,
pozik saltoka ekingo dio
zeruan dalako festa.

Idiarena danbolinakin
jotzen ziranean hasi,
donostiarra danen artean
saltoka zuten ikusi.
San Pedro berris haren ondoren
egin baino lehen ihesi,
harrapatu ta zeruetatik
infernura zuten jetxi.

Berehalaxe han sartu zuten
bere lagunen ondoan,
Donostiako kontu guziak
gogoratutzen orduan.
Demonioak ikusirikan
pozturik galdetu zuan,
aber benetan ote ziraden
karnabalak infernuan.

Geroztik beti han arkitzen da
infernuan penatzen,
bere zeruko pasadizoa
herritarrai kontatutzen.
Halaxe dio:-Berriro banintz
ni berriz horla gertatzen,
idiarena joagatikan
ez naiz hasiko saltatzen.

Deabru haiek beren kontura
Donostiarra harturik,
zulo batera han bota zuten
kate gogorrez loturik.
Toki hartako berotasunez
biziro egarriturik,
galdetu zuen:-Ez al da hemen
inon sagardotegirik?

Orain hazitzen ari dalako
Donostiako herria
demonioak omen daukate
berebiziko larria.
Bildurturikan lehengoko zaharra
ez dala nahikoa haundia,
ta egitea pentsatu dute
beste infernu berria.

Baldin gehiegi hitzegiten badet
jaunak ez zaitezte harritu,
huts eginikan baldin banago
behar nauzute barkatu.
Donostiarrai gaizki nahi eta
ez ditut bertsoak sortu,
baizik hoiekin karnabaletan
broma nahi gendun pasatu.