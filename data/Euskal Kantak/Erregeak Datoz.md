---
id: ab-4959
izenburua: Erregeak Datoz
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004959.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004959.MID
youtube: null
---

Erregeak datoz presaz Belena,
guk bila dezagun, ahal dan lehena.

Ate txokoan oilar bi,
batak bestea dirudi,
etxe hontako etxekoandreak
Ama Birjina dirudi.
Ama Birjina,
Ama Birjina,
zu zera zeruko erregina.

Atean eder epelez
atal burua zilarrez,
nagusi jauna, esan bezaigu
hasiko geran ala ez.
Nagusi jauna,
nagusi jauna,
gabon emanaz gatoz zugana.

Eguberrien bezpera,
beti honela ez gera,
hori inolaz galdu ez dedin,
etorri gera eskera.
Galdu ez dedin,
galdu ez dedin,
txanpon eske gatoz zure etxera.