---
id: ab-5015
izenburua: Madalen Buzturingo
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005015.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005015.MID
youtube: null
---

Madalen Buzturingo,
gauean gauean,
errondean dabiltza
zure portalean.
Ai, Madalen, Madalen,
ai, Madalen!
Zure bila nenbilen
orain baino lehen.

Hasi goiko kaletik
eta behekoraino,
ez da ederragorik
Madalentxo baino.
Ai, Madalen, Madalen,
Madalen gaisoa,
Madalenek edaten du
edari goxoa.

Zein ote da herori,
honen famosia,
Madalen deritzona,
neskatxa airosia.
Ai, Madalen, Madalen,
Madalen neria,
zure ondoan dabil
bueltaka jendia.