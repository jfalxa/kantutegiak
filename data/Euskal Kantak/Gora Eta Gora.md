---
id: ab-5033
izenburua: Gora Eta Gora
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005033.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005033.MID
youtube: null
---

Gora eta gora Euskadi,
aintza ta aintza bere goiko Jaun onari.
Haritz bat Bizkaian da,
zahar, sendo, zindo bera ta bere lagia lakoa,
ta haritz gainean dogu gurutza
deuna, beti geure goiburu.
Abestu gora Euskadi,
aintza ta aintza bere goiko Jaun onari.