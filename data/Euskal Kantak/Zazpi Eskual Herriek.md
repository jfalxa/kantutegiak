---
id: ab-5030
izenburua: Zazpi Eskual Herriek
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005030.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005030.MID
youtube: null
---

Zazpi Eskual Herriek
bat egin dezagun.
Guztiak beti, beti,
gauden gu eskualdun.

Agur eta ohore
Eskual Herriari.
Lapurdi, Baxenabar,
Ziberu gainari.
Bizkai, Napar, Gipuzko
eta Arabari.
Zazpiak bat besarka
lot bitez elkarri.
Zazpi Eskual Herriek...

Haritz eder bat bada
gure mendietan
zazpi adarrez dena
zabaltzen airetan.
Frantzian, Espainian,
bi alderdietan,
hemen lau ta han hiru
bat da zazpiretan.
Zazpi Eskual Herriek...

Ez bahaiz eskualduna,
lehen bezin handi,
apaldu gabe, zutik,
bederen egon hadi.
Odolez eta fedez,
beti berdin garbi,
beti tink atxikia
hire Eskual Herriari.

Zazpi Eskual Herriek...