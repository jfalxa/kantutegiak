---
id: ab-5054
izenburua: Ia Gureak Egin Du
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005054.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005054.MID
youtube: null
---

Ia gureak egin du,
badegu zeinek egindu.
Ez oraindik umildu,
alkarrengana bildu,
gerra nahi duen guzia...
Berari kendu bizia.

Okasioa prestatzen,
lotan daudenak esnatzen.
Ez zait neri gustatzen
malezian jostatzen
inorekin ibiltzea.
Hark merezi du hiltzea.

Gerraren bila gau ta egun
etsaiak harturik lagun.
Arrazoi ote degun
edo zer datorkigun
hasitzerako pentsatu,
fusilak nola dantzatu.

Ez naiz ni gerraren zale,
baizik pakearen alde.
Zeinek nahi duen galde,
berari tira dale,
bala bat sartu buruan.
Aspertuko da orduan.

Umildadean alkarri,
errespetoa ekarri.
Lege eder bat jarri,
bizi gaitezen garbi,
hori deseo nuke nik,
isuri gabe odolik.

Gaitzak gerade umiltzen
eta pakean unitzen.
Ez da errez ibiltzen
anaiak alkar hiltzen,
zer dan entenditu gabe.
Hortan galduko gerade.

Gu gera hiru probintzi,
lehengo legerik ez utzi.
Hoieri firme eutsi,
nehiz hanka bana hautsi,
jaioko dira berriak.
Gu gera Euskal Herriak.

Alabes eta bizkaino.
Gu gera gipuzkoano
aitak hilziran baino
semeak oraindaino
legea degu guardatu.
Letra zaharrik ez ukatu.