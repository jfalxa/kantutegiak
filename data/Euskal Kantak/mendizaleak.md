---
id: ab-5034
izenburua: Mendizaleak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005034.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005034.MID
youtube: null
---

Mendizaleak aurrera,
goazen mendi gailurrera,
euskaldunak esnatzera,
tralarala, tralarala.
Aupa goazen, bide zidorrez,
behetik gora maldetatik.
Ederra da gure herria,
maite dugu bihotzetik,
ez da ludian hoberik.
Tralara larala.
Ez da ludian hoberik.

Goazen mendirik-mendi,
baserritik baserrira
esanaz: askatasuna
laster dator bai, anaia, gurekin.
Lasterka bide zidorretatik
erromerira etorri gara.
Alai-alai abes dezagun
ta dantza egin dezagun guztiok.
Zure ibilera, maitea,
gure alaitasuna,
erromeriara etorri gara.
Tralaralara,
tralaralara,
tralaralara...