---
id: ab-5051
izenburua: Mando Baten Gainean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005051.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005051.MID
youtube: null
---

Mando baten gainean
Domingo Kanpaña
ez zihoak hutsikan
mando horren gaina.
Azpian dihoana mandoa dek baina,
gainekoa ere badek
azpikoa aina
mando baten gainean
bestea alajaina.
- Indalezio, Indalezio
Indalezio Moko
uste al dek, uste al dek,
ez hautela joko?
- Berriz esaten banak
Indalezio Moko
hezurrak hautsi arte
ez diat lajako.