---
id: ab-4986
izenburua: Kakagalez Nator
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004986.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004986.MID
youtube: null
---

Kakagalez nator
Errenteriatik,
lau kuarto eman nituen
egiteagatik.
Rau, rau, rau, rakataplau
hau duk umorea,
utzi alde batera
euskaldun jendea.

Horra, bada, osaba,
alegratu bedi,
kaka galanki egin dut,
eder eta lodi.
Rau, rau,...

Gorputzetik libratzen
hasi naiz bastante.
Ohetik jaiki eta
orinala bete.
Rau, rau,...

Gure gazteak pozak
erotu nahiean,
ikusi dutelako
kaka orinalean.
Rau, rau,...

Iñor egiten bada
hitz hauekin nazka,
bere belarri biak
itxitzea dauka.
Rau, rau,...

Jendeak egin arren
irri-arra farra,
fuera mokordoa eta
biba kaka zaharra!
Rau, rau,...
Biba kaka zaharra.