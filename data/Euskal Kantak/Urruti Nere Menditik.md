---
id: ab-5022
izenburua: Urruti Nere Menditik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005022.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005022.MID
youtube: null
---

Urruti nere menditik
joan nintzan dirutu nahirik.
Urrutietan nere bihotza
nahigabez erdibiturik,
gelditu nintzan tristerik,
pake santua galdurik.

Nere bihotza zegoen
Euskal Herrian pentsatzen.
Egun guztian, gauaz orobat,
beti zitzaidan oroitzen,
zein gozo nintzan bizitzen
haurtxo nintzanean hantxen.

Gogoan nuen etxea,
gaztain artean gordea.
Mendizka gainean agiri zan
herriko eliz maitea,
ta hango kanpandorrea,
hainbat oroitzez betea.