---
id: ab-5036
izenburua: Gazte Gaztetatikan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005036.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005036.MID
youtube: null
---

Gazte-gaztetatikan herritik kanpora,
estranjeria aldean pasa det denbora.
Herrialde guztietan toki onak badira,
baina bihotzak dio: "Zoaz Euskal Herrira!.

Lur maitea hemen uztea da negargarria,
hemen gelditzen dira ama ta herria.
Urez noa ikustera, bai, mundu berria,
oraintxe, bai naizela urrikalgarria.

Agur, nere bihotzeko amatxo maitea!
Laster etorriko naiz, konsola zaitea.
Jaungoikoak, nahi badu ni urez joatea,
ama, zertarako da negar egitea?
Ni urez juatea
Ama certaraco da
Negar eguitea.