---
id: ab-5050
izenburua: Loreak Udan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005050.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005050.MID
youtube: null
---

Loreak udan ihintza bezela
maite det dama gazte bat,
hari hainbeste nai diotanik
ezta munduan beste bat.
Inoiz edo bein pasatzen badet
ikusi gabe aste bat,
bihotz guztira banatutzen zait
halako gauza triste bat.

Neskatxa gazte paregabea,
apirileko arrosa,
izarra bezin dizdizaria,
txoria bezin airosa.
Oraintxen baino gustu gehiago
nik ezin nezake goza.
Zori onean ikusten zaitut,
nere bihotzak hau poza!

Ez al didazu antzik ematen
nik zaitudala nahiago,
ai, marinelak gau ilunean
izarra baino gehiago?
Nere ondoan zauzkadalako
pozez zoraturik nago,
zu ikusteak alegratu nau
triste nengoen lehenago.

Nik hainbat inork nahi dizunikan
arren ez zazula uste.
Nere begiak beren aurrean
beti desio zaituzte.
Eguzkirikan ikusi gabe
txoria egoten da triste,
ni ez nau ezerk alegratutzen
zu ikusteak hainbeste.

Arpegi fina, gorputza berriz
ez dago zer esanikan,
hizketan ere garzi ederra,
ezer ez dezu txarrikan.
Mundu guztia miratuta ere,
zu bezelako damikan
agian izan liteke baina
ez det sinisten danikna.

Nere betiko pentsamendua,
nere kontsolagarria,
zu gabetanik ezin bizi naiz,
esaten dizut egia.
Zu bazinake arbola eta
ni baldin banintz txoria,
nik zu zinakeen arbol hartantxen
agingo nuke kabia.

Amorioak nere bihotza
zureganuntza darama,
herri guztian zeren dakazun
neskatx bikainaren fama.
Beste fortunik mundu honetan
ez det desiratzen, dama,
haur batek berak izan gaitzala
ni aita eta zu ama.

Falta duenak logratutzeko
hitz egitea txit on du,
eta nik ere saiatu biat
ote gintezkeen konpondu.
Gaur nagon bezin atrebitua
sekulan ez naiz egondu,
hargatik kolpez galdetzen dizut
nerekin nahizun ezkondu.

"Ezkondutzeak izan behar du
preziso gauza txarren bat!"
Hala esaten ari zait beti
nere kontsejatzaile bat.
Halaxen ere haren esanak
oso utzirik alde bat,
ongi pozikan hartuko nuke
zu bezalako andre bat.

Zerorrek ere ongi dakizu
aspaldi hontan nagola
zeregatikan penak sufritzen,
bainan ordea hau nola!
Halaxen ere nigana ezin
biguindu zaitut inola.
Ni zuretzako argizaia naiz,
zu neretzako marmola!

Nere bihotza urtzen dihoa
eta ez da misterio.
Penaren kargak estutu ta
zumua kendutzen dio.
Begiak dauzkat gau eta egun
eginikan bi errio,
beti negarra dariotela
zu zarelako medio.

Zu zarelako medio baldin
joaten banaiz lur azpira,
gero damua eta malkoak
alperrik izango dira.
Behin joanez gero horien birtutez
berriz ez ninteke jira.
Hori gertatu baino lehenago
izazu nitaz kupira.