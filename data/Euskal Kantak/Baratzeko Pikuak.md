---
id: ab-5017
izenburua: Baratzeko Pikuak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005017.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005017.MID
youtube: null
---

Baratzeko pikuak
hiru hosto ditu.
Dantzan dabilen horrek
oinak arin ditu.
Oinak arin-arinak,
burua arinago,
dantzan hobeki daki
artajorran baino.

Ama, nahi baduzu mutilik erosi,
eliza-bazterretan, txanponean zortzi.
Onak baldin badira, ez dira garesti,
gaizto baldin badira, probatu ta utzi.

Agure zahar batek gaur gure amari
mandatu egin dio behar nauela ni.
Gure amak errepostu nola eman badaki:
etxean baduela agurerik aski.

Oraingo mutil txarrek duten fantasia,
gerruntzeko berria bertzek erosia.
Oraingo neska gazteek daramaten gala
zapata koxkorrekin debantal nabarra.