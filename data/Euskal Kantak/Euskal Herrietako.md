---
id: ab-5004
izenburua: Euskal Herrietako
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005004.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005004.MID
youtube: null
---

Euskal Herrietako soinu ezti,
izan dana beti guzien gainetik,
bilatu det lurpean sartua
bai eta ahaztua
zeren dan zahartua.
Ateratu behar nuke plazara
nerekin dantzara,
oraindik gauza da.
Ikusiko da zein atsegina,
bizarra urdina, baina txit arina.