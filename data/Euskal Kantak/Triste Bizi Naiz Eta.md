---
id: ab-5053
izenburua: Triste Bizi Naiz Eta
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005053.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005053.MID
youtube: null
---

Triste bizi naiz eta
hilko banintz hobe,
badauzkat bihotzean
zenbait atsekabe.
Dama bat maitatzen det
bainan haren jabe,
sekulan izateko
esperantza gabe.

Nere bihotz gaixoa
penatua dago,
ezin egon liteke
ai, penatuago.
Pasatzen ditudala
aspaldian nago,
egunak triste eta
gauak tristeago.

Amorez eritua,
zenbait denbora hontan,
gaurik ez det pasatzen
soseguzko lotan.
Penaren kargarekin
urtuta, frankotan,
kanpora irteten zait
bihotza malkotan!

Hauxen da mina laister
hil behar nauena,
kupitutzen ez bada
senda lezakeena,
hain arkitutzen zaizkit
sartuak barrena,
animan tristura ta
bihotzean pena.

Bihotza ezin jaso det,
hau karga barrenak,
ia gastatu zaizkit
indarrik gehienak.
Ez luke sinistuko
probatu ez duenak,
zenbat pisatzen duen
egiazko penak.

Anima tristurakin
hain dakat negarti,
non egin ahal dezakeen
harria bi parti,
adorantzak ematen
belauniko beti,
bihotzean daukadan
imajina bati.

Zein ote dan ezinik
kontura erori,
kutizia jaioko
zaiote askori.
Nik, ordea, ez diot
esango inori,
berak badaki eta
bastante det hori.

Nere maitatua da
guztiz dama fina,
bihotz onekoa ta
ondo hitzegina.
Bentaja guztiakin
zeruak egina,
mundua pasatzeko
lagun atsegina.

Zertako desiatu
ditu gauza on hoiek,
nere bihotz koitadu
gizarajo honek?
Kulparen kastigutzat
datozkit pena hauek,
merezi ditudala
badakit neronek.

Aingeru zoragarri
mirabe laztana,
pentsamentu nerean
beti zauzkadana.
Bihotzaren erditik
maite zaitudana,
zu zera neretzako
nahi zintuzkedana.

Nere amorioa
hain fina izanik,
beti gogoan zauzkat
ahaztu gabetanik.
Ez da mundu guztian
inor kapaz danik
dama, zu maitatzeko
hainbeste nola nik.

Begiaren aurrean
beti nahi zintuzket,
ikusten ez bazaitut
nahigabea maiz det.
Gogoratu bageko
minuturik ez det,
amorioz maitatzen
ez dakinik bestek.

Ez zaitut ikusitzen
desio hainbeste,
ditxa hori logratzen det
inoiz edo beste.
Eta ez detanean
logratzea uste
hura triste ibiltzen naiz,
Jesus, hura triste!...

Ezin bizi zu gabe,
hau amorioa!,
logratzen ez bazaitut
zer martirioa!
Ez badezu desio
nere herioa
zuregana arkitzen da
erremerioa.

Erregutzen dizut
o, Jesusen Ama,
nitaz kupi dedila
maite detan dama.
Bihurtzen badidazu
bihotzera kalma,
kantaz banatuko det
milagroen fama.