---
id: ab-4971
izenburua: Bazatoxak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004971.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004971.MID
youtube: null
---

Bazatoxak, bazatoxak,
goiko kaletik beheko kalera.
Bazatoxak, bazatoxak,
goiko kaletik zezenak.
Lalaralaralala lala lala
lalaralaralala lala lala.