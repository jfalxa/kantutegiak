---
id: ab-4963
izenburua: Oi, Eguberri Gaua
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004963.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004963.MID
youtube: null
---

Oi, Eguberri gaua, bozkariozko gaua,
alegeratzen duzu bihotzean kristaua!
Mundua zorionez mukuru bete duzu,
Mesias jaio dela zuk erran baitiozu.

Gau ilunean ez da ageri eguzkirik
honetan dakusagu mirakuluz hilkirik.
Oi, gau zoragarria, argi bat duzu piztu
horrek bazter guztiak argitu behar ditu.

Zeruetan izarrak bozkarioz dantzatzen
beste gauetan baino gehiago dizdiratzen.
Aingeru hauek berriz kantari dihardute,
kristau hauek bakea eskaintzen ari zaizte.