---
id: ab-5014
izenburua: Erran Dut
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005014.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005014.MID
youtube: null
---

Erran dut, erranen ta errango,
ez naiz isilik egongo,
plaza hontako dama gazteak
ez dira monja sartuko.
Ala kinkirri, ala kunkurru
ala kinkirri kunkurru kanta!
ala kinkirri kunkurru kanta
gu bezalakoak dira, hola!

Erran dut, erranen ta errango,
ez naiz isilik egongo,
plaza hontako gizon gazteak
ez dira fraile sartuko.
Ala kinkirri, ala kunkurru
ala kinkirri kunkurru kanta!
ala kinkirri kunkurru kanta
gu bezalakoak dira, hola!

Erran dut, erranen ta errango,
ez naiz islik egongo,
plaza hontako gazte guztiak,
ezkontzan dira sartuko.
Ala kinkirri, ala kunkurru
ala kinkirri kunkurru kanta!
ala kinkirri kunkurru kanta
gu bezalakoak dira, hola.