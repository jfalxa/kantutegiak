---
id: ab-5007
izenburua: Jaiki, Jaiki, Maria
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005007.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005007.MID
youtube: null
---

Jaiki, jaiki, Maria
piztu zan argia,
ziladuan bada norbait
sorgina iduria.

Aita, egon isilik,
amets egin duzu.
Ziladuan gatuño bat
jostetan ari duzu.