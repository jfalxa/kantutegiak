---
id: ab-4965
izenburua: Olentzero Buru Haundia
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004965.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004965.MID
youtube: null
---

Olentzero buru haundia
entendimentuz jantzia,
bart arratsean edan omen du
hamar arruko zahagia.
Ai, urde tripahaundia!
Tralaralala, tralaralala.
Ai, urde tripahundia!
Tralaralala, tralalara.