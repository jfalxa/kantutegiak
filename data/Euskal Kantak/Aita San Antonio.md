---
id: ab-4909
izenburua: Aita San Antonio
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004909.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004909.MID
youtube: null
---

Aita San Antonio
Urquiolakoa,
askoren bihotzeko
santu debotoa, ai, ai!
Askoren bihotzeko
santu debotoa.

Askok egiten dio
San Antoniori,
egun batean joan ta
bestean etorri.