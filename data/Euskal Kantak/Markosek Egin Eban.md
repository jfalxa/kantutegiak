---
id: ab-4987
izenburua: Markosek Egin Eban
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004987.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004987.MID
youtube: null
---

Markosek egin eban
lastozko zubixe
bertatik pasatzeko
bera ta txarrixe.
Hasi ziran pasatzen,
jausi zan zubixe
Markosek hartu eban
kristoren ostixe.

Errekatik urten zan
goraino bustirik,
gainera sikatzeko
ez zan eguzkirik.
Egindakoarekin
guztiz damuturik,
berriz ez dau egingo
lastozko zubirik.

Horrelaxe hilko jako
makina bat pizti,
baina aurrera doa
euri zein eguzki.
Berari berdin jako
busti zein ez busti,
errekan zehar doa
txarri eta guzti.

Erreka bazterrean
guardarekin kezka,
lizentzi barik egin
ete dauan peska.
Eskuak gora jasoz
Markosen protesta:
"Hau nere txarrixe da
arrankarixe ez da".

Erreka pasa eban
berak ozta-ozta,
gero guarda dala ta
txarri horrek ospa.
Markos bere atzetik
mekaguen dioska:
"Erreka pasatzea
ez jat gitxi kosta".

Markosen txarrixe zan
artoz hazikoa,
San Martinetarako
berebizikoa.
Berari alde eginda
hor ihesi doa:
agur solomo eta
urdaiazpikoa.

Txarrixe maite eban
eta ez besterik,
munduan ez zegoen
holako piztirik.
Pentsatzen dago hilda
ala dan bizirik,
txarrixe topau ezin
holako ostirik.

Bere txarrixen bila
behera eta gora,
ihesi egin deutso
ta ez daki nora.
Eta pentsamentu hau
datorko gogora:
"Gatzelaskaren baten
gordeta egongo da".