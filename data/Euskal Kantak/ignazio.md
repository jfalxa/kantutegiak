---
id: ab-5026
izenburua: Ignazio
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005026.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005026.MID
youtube: null
---

Ignazio, gure patroi haundia
Jesusen Konpainia
fundatu
eta dezu armatu,
ez da ez etsairik
jarriko zaizunik
iñolaz aurrean
gaurko egunean,
nahiz betor Lucifer deabrua,
utzirik infernua.
Zure soldaduak
dirade aingeruak,
zure gidaria
da Jesus haundia,
garaitu ditu zure Konpainiak
etsaiak.
Ez dauka fedeak,
ez kristau nereak,
ez dauka bildurrik
inongo aldetik.
Ignazio hor dago,
beti ernai dago,
armetan jarria
dauka Konpainia,
txispaz armaturik
bandera zabalik,
gau ta egun
gu guztiok pakea dezagun.
Beti, gau eta egun.