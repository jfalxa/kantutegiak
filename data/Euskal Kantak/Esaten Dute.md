---
id: ab-4920
izenburua: Esaten Dute
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004920.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004920.MID
youtube: null
---

Esaten dute bada
ezkondu, ezkondu.
Ez zait neri sekulan
gogorik berotu.
A, zer zori orduko
ezkondu beharra!
Geroztik egiten det
sarritan negarra.

Bihar da urte bete
ezkondu nintzala,
egun bat gutxiago
damutu zaidala.
Nere bodako eguna
pasatu ezkero,
egun bat zer dan ona
ez dakit arrez gero.

Plazan, eder ta galant,
ezkondu artean.
Ni ere hala nintzan
denbora batean.
Zenbait ikusten ditut
ezkondu ta gero
esanaz dabiltzala:
- Ai, libre banengo!

Ai, hau bizi modua
sortu da neretzat!
Ez daukat oliorik
porru saldarentzat.
Azak eta borrajak,
nintzanean bakarrik,
beti jaten nituen
olioz beterik.

Harako gizon hori
da nere senarra.
Askotan ekartzen dit
begira negarra.
Posible baldin balitz
inposible dana,
pozik utziko nuke
nik nere senarra.

Nere senarrak ez du
beste ekintzarik,
tabernara joan eta
horditu besterik.
Etxera etortzeko,
ai, nere bildurra!
Nois ikusiko detan
bizkarran egurra.

Ezkondu ezkeroztik
musua garbitzen,
iturriko urikan
ez det nik gastatzen.
Begietatik behera
datorren negarra,
musua garbitzeko
bastante da bera.

Ezkondu nintzanean
bost gona nituen.
Bi prenda jarri eta
bi saldu nituen.
Soinean daukadana
guztiz daukat zaharra,
berriak egiteko
esperantza txarra.