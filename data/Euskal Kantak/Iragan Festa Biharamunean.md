---
id: ab-5045
izenburua: Iragan Festa Biharamunean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005045.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005045.MID
youtube: null
---

Iragan besta biharamunean
berek dakiten txoko batean,
lau andre, hiru mutxurdin, bat alarguna, jarririk itzalean,
harri zabal bat belaunen gainean,
ari ziren, ari ziren trukean.

Zer ote duten nik jakin nahi
pitxarrarekin bertze jaun hori.
Zorroa du biribila, moko meharra, tente zutik egoki,
zahato bat ote den nago ni,
hanpatua, hanpatua ederki.

Jesus, oraino zer dut ikusten?
zer hegalpean dute gordetzen?
Egoitza gozo hortarik, burua alto, zahatoaz trufatzen,
bihotzez azkarrago zeren den,
kuiattoa, kuiattoa ari zen.

Festa delakotz egun herrian
apaindu dira handi-handian,
ederki gazteen gisan panpinatuak, ez patxada ttikian,
bihotz onez partidaren erdian,
irriz daude, irriz daude lorian.

Handik ximiko, hemendik irri,
haurrak bezala jostetan ari,
tartetan amodiozko begi kolpe bat hegalpean denari,
hitz bat erran gogo diote sarri
belarrirat, belarrirat kuiari.

Gezurrik gabe goazen, Maria,
eman jokoan ezti-eztia,
hurbil zan untzi beltz hori, dezagun edan trago bat edo bia.
Bihotza dinat epeltzen hasia,
harek zuen, harek zuen grazia.

Zahagiaren seme joria,
non duk aristiko zorro lodia?
Gaixoa! Lehen birundan xurgatu dautek odolaren erdia!
Orai hor ago, zimurrik arpegia,
kokoriko, kokoriko jarria.

Seira truk dira zazpietarik,
tantoz orobat bi aldetarik.
Eroria da zahatoa, mokoz-ipurdi, naski odol husturik;
kuiattoa hantxe dago etzanik,
azken hatsa, asken hatsa emanik.

Nahiz zeraukan azken eskua,
Mariak zuen hasi gudua:
- Truk.- Jo zan. Mariak zango, Katixak zaldun, Marixumek hirua
- Marimartin, zoratu zain burua,
lauarekin hiruaren keinua!

- Hago, Maria, otoi, isilik,
ez dun ikusi nere keinurik.
Xorta bat edanez gero, begiak ñir-ñir, zer? Ez al dun ahalkeririk?
Edan hezakeen azkarr hortarik,
katilua, katilua beterik.

- Bazakinagu, tresna makurra,
ez dunala hik hatsa laburra.
Ez dun, ez, dionan bezala, horma xurgatuz, biribildu muturra
ez eta naski gorritu sudurra
milikatuz, milikatuz elurra.

Apo mutxurdin, moko bipila,
zeren ondoan ote habila?
Hatz badun atze aldean, ikusiko dun zenbat naizen abila,
zuhur badun, hago, neska zirtzila,
isil, isil, isil, isil, isila.

Kartak utzirik eta tantoak,
hantxe zituzten gero saltoak.
Bakarrik hiruren kontra, zer eginen du Marimartin gaixoak?
Nahiz izan azkar zango-besoak,
hartu ditu, hartu ditu paloak.

Herriko besta arrats apalean,
lau katu zahar angelusean,
bat maingu, hiru saltoka, sorginak puies, zoatzila bidean,
nik ikusi ditut amets batean,
akelarre, akelarre gainean.