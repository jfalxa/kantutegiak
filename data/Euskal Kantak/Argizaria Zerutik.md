---
id: ab-4924
izenburua: Argizaria Zerutik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004924.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004924.MID
youtube: null
---

Argizariak zerutik
argitzen dizu ederki.
Ene maite polita,
ez duzu ageri.
Zeruko Jinko Jauna,
zer eginen dut nik,
zer eginen duk nik?

Fiatik batere ez duzu,
mundua irriz ari zaizu.
Batto orotaz
agrada zira zu.
Bat har ezazu,
hura aski dukezu,
horrez segur nuzu.

Urzo apalaren malura
galduz geroztik laguna!
Triste da beti
bere bihotzetik,
zeren ez baitu maitatu
bat baizik,
maitatu bat baizik.

Amodio berriak
sendotzen dizu eria.
Zure begiak
hain dira eztiak
zeren baitira
eneak zuriak,
zureak eniak.