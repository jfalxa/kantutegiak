---
id: ab-4958
izenburua: Birjina Maite
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004958.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004958.MID
youtube: null
---

Birjina maite, lotan dago
zure seme laztana,
Sehaskatxo bat ipini iozu
gure bihotzen kutunai,
Ongi, bai, ongi igaro ditzan
gauak eta egunak.
Polliki, polliki eragiozu
egin dezan lo ona.