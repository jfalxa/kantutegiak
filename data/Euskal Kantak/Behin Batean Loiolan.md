---
id: ab-5047
izenburua: Behin Batean Loiolan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005047.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005047.MID
youtube: null
---

Behin batean Loiolan
erromeria zan;
hantxen ikusi nuen
neskatxa bat plazan.
Txoria baino ere
arinago dantzan,
Huraxen bai polita
han politik bazan!

Esan nion desio
senti nuen gisan,
harekin hizketa bat
nahi nuela izan.
Erantzu zidan ezik
atsegin har nezan,
adituko zidala
zer nahi nion esan.

Aurkitu ginanean
inor gabe jiran,
koloreak gorritu
arazi zizkidan.
Kontatuko dizutet
guztia segidan
zer esan nion eta
nola erantzun zidan.

Dama polita zera,
polita guztiz, ai!
Bainan halare zaude
oraindik ezkongai.
Ezkon gaitezen biok!
Esan zaidazu: bai!
- Ni zurekin ezkondu?
Ni zurekin? Jai-jai.