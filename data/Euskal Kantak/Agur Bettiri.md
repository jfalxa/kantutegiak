---
id: ab-5001
izenburua: Agur Bettiri
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005001.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005001.MID
youtube: null
---

- Agur, Bettiri, ongi etorri!
bizi ziradeia oraino?
- Bai, bizi naiz, eta bizi gogo
hartzekoak bil arteraino.

- Sei, zazpi, zortzi urte bizi
baduzu orduan oraino!
Bai, Jainkoak kontserba zaitzala
ene zorrak bil arteraino.