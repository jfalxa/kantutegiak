---
id: ab-4908
izenburua: Txakolin
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004908.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004908.MID
youtube: null
---

Txakolin, txakolin,
txakolinak on egin.
Maritxu, arintxo da
Martintxo.
Ase naiz naparrez,
zuri, gorri ta beltzez.
Jarri naute minez,
gabe ere onik ez.