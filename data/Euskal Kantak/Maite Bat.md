---
id: ab-4948
izenburua: Maite Bat
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004948.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004948.MID
youtube: null
---

Maite bat maitatzen det, maitagarria.
Begui ederra du ta guztiz argia.
Daukat urruti,
bainan ezin kendu det burutik
haren itxura,
saldu albaliteke pisura,
urriaren truke
nork erosi faltako ez luke.

Hogeita lu leguaz nago aparte
bitartean badauzkat miloi nat ate,
guztiak itxirik,
Nahi arren, ezin egon isilik.
Beti negarrez,
nere maite-maitearen galdez,
ote dan bizi,
bihotz-bihotz nereko Kontxesi!

Egunaz argi gutxi, gauean ilun,
kontsuelorik ez da neretzat inun,
maitea gabe,
Egin ote nadin haren jabe
oroitutzean,
zenbat pena nere bihotzean
ditut igaro!
Maite maite det, ez da milagro!

Ai! Hau bakardadea eta tristura!
Nere bihotz gaisoa ezin piztu da,
hain dago hila!
Beti dabil kontsuelo bila,
bere artean.
Banengo maitearen aldean,
ordutxo biko...
Pena guztiak lirake hilko!

Nere maitea zutaz ni oroitzen naiz,
egunaz, baita ere gauetan txit maiz.
Lotan ere bai,
zu ikusitzera ni joan naiz.
Libre banengo,
hor nintzake egun bigarrengo.
Nahiz orduan hil,
ez nuke izango batere min.

Lehengo gau batean egin det amets,
bainan pentsamentuak beti aldrebes
irtetzen dira.
Ustez nengoen zuri begira,
maite polita,
kofrearen gainean jarrita,
kontu kontari
ni nauen, baina ez nintzan ari.

Maite nerea daukat beti gogoan:
ai, orain banengo ni haren alboan
inoiz bezela!
Jaunak amets hau kunpli dezala,
balitz komeni,
kontsueloz hilko nintzake ni,
nere ustean,
maitetxoren bihotza ikustean.

Nere meite polita, ez da zer etsi,
bihar ez bada ere, hor nauzu etzi,
lehengo lekuan,
ailegatutzen naizen orduan.
Ai! Hura poza
Nere maite-maitetxo, bihotza,
zuri begira
pena guztiak ahaztuko dira.

Zure gauza polit bat ohi det nerekin,
izkribaturik dago letra birekin:
K eta B dira.
Askotan nago hoiei begira,
hain dira finak!
Maitetxo polita, zuk eginak
sedaz ederki,
kolkoan gorderik dauzkat beti.

Esperantzetan bizi, maite gozoa,
noizbait kunplituko da gure plazoa
eta orduan...
Gauza txarrik ez hartu buruan,
lehengoari utzi.
Ez degu pasatzen pena gutxi,
preso sei urtez,
onduko gaituzte nere ustez.