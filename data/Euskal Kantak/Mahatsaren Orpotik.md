---
id: ab-4906
izenburua: Mahatsaren Orpotik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004906.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004906.MID
youtube: null
---

Mahatsaren orpotik dator
mama gozoa, mama gozoa.
Edango neukeela beterik basoa,
klink, beterik basoa.
Nik zuri, zuk neri,
agur eginaz alkarri,
basoa txit garbi
beharko da ipini.