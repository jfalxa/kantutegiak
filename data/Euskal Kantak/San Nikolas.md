---
id: ab-4967
izenburua: San Nikolas
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004967.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004967.MID
youtube: null
---

San Nikolas ttikia
gure ganbaran hazia:
- Gure ganbaran zer dago?
- Andre Juana Maria
Ave, ave, Maria.
Dios te salve, makailu jale.
Bost eta sei hamaika.
Txorizorikan ez baldin bada
igoal dela lukainka.

Urte berri berri,
nik atorra berri,
maukarik ez
nik axilik ez.

San Nikolas coronado
confesor es muy hondrado.
ale, ale alegría,
todo el mundo alabaría.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de Dios.
Aingeruak gara, zerutik gatoz,
poltsa badugu, baina eskean gatoz.

San Nikolas coronero, konfesero Mari Andrés.
Alarguna dontzellea, cantaremos alegría.
Bost etxean sei ate,
zazpi etxean suete.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de Dios.
Aingeruak gara, zerutik gatoz,
poltsa badugu, baina eskean gatoz.