---
id: ab-4999
izenburua: Txanton Piperri
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004999.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004999.MID
youtube: null
---

Txanton Piperri hemen dago
limosna on bat balego.
Kanta behar dot,
baldin ahal badot,
neure sabelen gosea,
erruki dedin jentea.

Hiru lagunek Ipazterren
postua egin eben.
Jango ebela,
goiztik gauera,
hamar erraldeko txahala,
nerau laugarren nintzala.

Pozez nengoan zoraturik
jateko laurden bat osorik.
Nere kontuak
beti halangoak,
joan zan poza putzura,
ametsak ohi diran modura.

Lekeitioko kalean
bixigu denbora danean,
hogei txitxarro
eta gehiago
eta guztiak gauean,
sartzen dodaz sabelean.

Orduan kontentu nago
gauerdi osteradaino.
Handik goizera
neure sabela
beti dago gur, gur, gur, gur, gur, gur.
Bartko afaria agur.

Guztiz zabala dot sabela,
berdin-berdin dot gibela.
Heste bakoitzak
kabiduko luke
ganbela bete okela.
Baina noiz izango ete da?

Ez da errotan harririk
neure haginak langorik.
Neure haginak
zatituko leuke
ogi handi bat osorik,
balego bigun-bigunik.