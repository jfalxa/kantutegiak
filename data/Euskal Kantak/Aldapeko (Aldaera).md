---
id: ab-6045
izenburua: Aldapeko (Aldaera)
kantutegia: Euskal Kantak
partitura: null
midi: null
youtube: null
---

Sagarraren adarraren iharraren
puntaren puntan
txoria zegonean kantari.
Ai, txiruliruli!
E, txiruliruli!
Nork dantzatuko du soinu hori.