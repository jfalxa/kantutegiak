---
id: ab-6046
izenburua: San Nikolas (Aldaera)
kantutegia: Euskal Kantak
partitura: null
midi: null
youtube: null
---

Urte berri berri,
nik atorra berri,
maukarik ez
nik axolik ez.

San Nikolas coronado
confesor es muy hondrado
ale, ale alegría,
todo el mundo alabaría.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de Dios.
Aingeruak gara, zerutik gatoz,
poltsa badugu, baina eskean gatoz.

San Nikolas coronero, konfesero Mari Andrés.
Alarguna dontzellea, cantaremos alegría.
Bost etxean sei ate,
zazpi etxean suete.
Aquí estamos cuatro,
cantaremos dos,
una limosnita
por amor de Dios.
Aingeruak gara, zerutik gatoz,
poltsa badugu, baina eskean gatoz.