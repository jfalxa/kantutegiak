---
id: ab-4902
izenburua: Maritxu
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004902.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004902.MID
youtube: null
---

Maritxu, Maritxu
malas mañas tienes tú.
Maritxu, Maritxu,
bizitzen badakizu.

Goizean goizean txokolatea,
arratsalde erdian koipatsu.
Ama ta ahizparentzat porrusalda ta
zerorrek edaritzat juju.

Maritxu, Maritxu...

Txori batek kantatzen du
haritz adar gainean,
bera ez dala ezkonduko
aurtengo udazkenean,
arto gari zekaleak
merke ez diran artean.

Maritxu, Maritxu...