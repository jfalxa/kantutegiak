---
id: ab-5000
izenburua: Zeruak Eta Lurrak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005000.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005000.MID
youtube: null
---

Zeruak eta lurrak
egin zenituna
memoria argitzera
zatoz neregana.
Esperantza badaukat,
zerorrek emana,
kunplituko dedala
desio dedana.

Amodiozko penak
bertso berrietan,
publika ahal banitza
Euskal Herrietan.
Enteratu naiz ongi
enkargu horietan,
San Jose arraltsaldeko
hiru t'erdietan.

Markes baten alaba
interesatua,
marineruarekin
enamoratua.
Dreskubritu gaberik
bere sekretua,
amodioa zeukan
barrenan sartua.

Egun sinalea zan
gogoan hartzeko
esan ziola arren
etxera joateko.
- Desio dedan hitz hau
manifestatzeko
zurekin, Antonio,
nago izateko.

- Zer esaten didazu,
Juanita, hitz hori?
Tentatzen ari zerala
trazak badirudi.
Ez zait zure gradurik
tokatutzen neri
ez burlarikan egin
marineruari.

- Eduki dezakezu
ongi sinistua
aspaldian nengoala
zutaz gustatua.
Ez zaitut nik utziko
desanparatua,
hala egiten dizut
gaur juramentua.

Konformatu ziraden
alkarren artian,
ezkonduko zirala
hurrengo urtian.
Eskola ikasteko
bien bitartian
beraren herritikan
guztiz apartian.

Hala disponiturik
jarri ziran biak,
kartaz entenditzeko
alkarren berriak.
Formalidadeakin
jartzeko egiak,
baina ez ziran lo egon
amaren begiak.

Alperrik izango da
haserre gurian,
ez naute mudatuko
eternidadian.
Esposatu nahi nuke
kariño onian
Antonio Maria
etortzen danian.

Ezin egon zan ama
hitz hori sufritzen,
berehala hasi zan
kartak detenitzen.
Intentzio haundiagoz
ezkontza galdutzen
Jaunitak holakorik
ez zuen pentsatzen.

Amaren malezia
korreora joan ta,
Antonio hil zala
egin zuen karta.
Juanitaren tristura
maitea izan ta
engainatu du beste
gezur bat esanda.

Amak esaten dio:
- Juanita neria,
galdu da diotenez
Antonio Maria.
Nik bilatuko dizut
beste bat hobia,
maiorazgo interes
askoren jabia.

- Ama ez neri esan
horrelakorikan,
ez det nik bestengana
amodiorikan.
Ezin alegra leike
nere barrenikan
komenientzia ona
egonagatikan.

- Utzi alde batera
horrelako lanak,
ez ditut nik ikusten
zu bezela denak.
Nahi badituzu hartu
honra eta famak,
gidatuko zaituzte
aitak eta amak.

Denbora kunpliturik
galaiak abian,
zer pasatu ote zan
haren memorian,
kartarik hartu gabe
joan dan aspaldian,
inozente sartu zan
jaio zan herrian.

Hau da lehendabiziko
esan zenduana:
- Zer da musikarekin
honratzen dutena?
- Markesaren alabak
kaleak barrena,
esposario zala
hark behar zuena.

Desmaiaturik egin
zuen ordu bete,
gero nobia eske
hitz bi egin arte.
Inguratu zitzaion
makina bat jende,
bigarren ordurako
hil da derrepente.

Gaua pasatu eta
hurrengo goizian,
entierroa zuen
bigarren klasian.
Markesaren alaba
guzien atzian,
zer pena izango zan
haren barrenian.

Penarekin leherturik
Antonio hil zan,
akonpainatu zuen
Juanitak elizan.
Nahitasunik bazion,
esan dedan gisan,
geroztikan ez zuen
osasunik izan.

Erremedia bazan
sentimendu hori,
bitarteko bat jarri
Jesus maiteari.
Orazio eginaz
Birjina Amari,
zeruan gerta dadin
Antonio Mari.

Alkarren konpainian
guk ere nahi degu,
Birjina egiozu
Jaunari erregu.
Kristau guziongatik
baldin ahal bazendu,
Iruttinok horrela
desiatutzen du.