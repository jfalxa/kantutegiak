---
id: ab-5021
izenburua: Baserritarrak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005021.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005021.MID
youtube: null
---

Baserritarrak gera gu
ezin gentzake ukatu.
Baina kaletarrak bezin ongi
soinua jotzen degu.

Kalean dira bi gauza
neri gustatzen zaidanak,
ardotxo zuri gozo eta
opil koskor bigunak.