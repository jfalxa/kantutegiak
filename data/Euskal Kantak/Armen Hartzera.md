---
id: ab-5043
izenburua: Armen Hartzera
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005043.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005043.MID
youtube: null
---

Armen hartzera deitu ninduen
gazterik zorte etsaiak.
Urrundu nintzen herri alderat
itzuliz usu begiak,
itzuliz usu begiak.

Zorigaitzean baitut ikusi
Solferinoko hegia!
Alferretan dut geroztik deitzen
eguzkiaren argia,
eguzkiaren argia.

Nihoiz enetzat ez da jaikiren
goizeko argi ederra.
Zeru, gainetik nihoiz enetzat
dizdiraturen izarra,
dizdiraturen izarra.

Betiko gaua, gau lazgarria,
begietara zait jautsi.
Ene herria, ene lagunak
nihoiz ez behar ikusi,
nihoiz ez behar ikusi!

Ene amaren begi samurrak
betiko zaizkit estali,
maiteñoaren begitartea
behin betiko itzali,
behin betiko itzali.

Ez aipa niri landa hegitan
sortzen den lili ederra.
Otoi, ez aipa ur azalean
arinik doan ainara,
arinik doan ainara.

Larrainetako haritz, gaztaina,
mendietako iturri.
Horiek oro enetzat dira
amets histu bat irudi,
amets histu bat irudi.

Herriko festan, gazte lagunak
kantuz plazara doazi.
Eta ni beltzik etxe zokoan,
irri egiten ahantzi,
irri egiten ahantzi.

Oraino gazte, gogoz ez hoztu,
eta biziak lotsatzen...
Dohakabea! Zer eginen dut,
jaunak ez banau laguntzen,
jaunak ez banau laguntzen.

Ai, aski hola! Jainkoa, barka,
begira zure haurrari!
Kontsolamendu zerbait emozu.
Noizbait duzu urrikari,
noizbait duzu urrikari.