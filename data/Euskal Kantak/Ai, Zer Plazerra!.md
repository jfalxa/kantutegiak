---
id: ab-5059
izenburua: Ai, Zer Plazerra!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005059.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005059.MID
youtube: null
---

Ai, zer plazerra, bost urten buruan sor lekurat itzultzia,
ene herria etsitu nian, zure berriz ikustia.
Aski sufrituz nik anitzetan, desir nukian hiltzia,
ez bazintudan hain maite uken, oi, ama Euskal Herria.

Anitzen gisa partitu nintzan, etsaien gudukatzera,
gogoa bero, bihotza laxo, eta kasik alagera.
Ez bainakian, oarino orduan, zer zen amaren beharra,
hari pentsa eta biga bostetan, aiher zitazun nigarra.

Erresinola kantari eder libertadean denean,
baina tristuran higatzez doa kaloia baten barnean.
Gu ere ama hala gintuzun, tirano haien artean.
Zure ametsa kontsolagarri, tristura handirenean.

Orhiko txoria da den bezala, fidel Orhi bortuari,
euskaldun guziak gituzu, ama, zure zerbitzari.
Alageraki lehen bezala, egin izaguzu irri,
ikusten gituzu otoitzetan zure arbola saintuari.

Sortu berriak, bere kunatik, lehen elea du ama,
bere ilusione ederrean, ez dezazula abandona.
Zure amodioaz egar itzozu bizitze huntako penak
begira dezan azken hatsetan, azken hitz zure ama.