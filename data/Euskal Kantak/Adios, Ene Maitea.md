---
id: ab-4934
izenburua: Adios, Ene Maitea
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004934.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004934.MID
youtube: null
---

Adios, ene maitea,
adios sekulako.
Nik ez dut beste penarik,
maitea, zuretako.
Zeren uzten zitudan
libre besteendako.

Zertako erraiten duzu
adios sekulako?
Uste duzia ez dudala
amodio zuretako?
Zuk nahi balin banuzu
ez nuzu besteendako.