---
id: ab-5020
izenburua: Boga, Boga
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005020.MID
youtube: null
---

Boga, boga, marinela, marinela,
joan behar dugu urrutira, urrutira.
Bai, Indietara! Bai, Indietara!
Ez dut, nik ikusiko zure plai ederra.
Agur, Ondarruako itsaso bazterra.
Marinela, boga, marinela.