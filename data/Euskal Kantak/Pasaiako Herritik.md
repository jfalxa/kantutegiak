---
id: ab-5056
izenburua: Pasaiako Herritik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005056.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005056.MID
youtube: null
---

Pasaiako herritik
dator notizia
zezen bat izan dala
jenioz bizia.
Kontatutzera noa
bistan ikusia,
alegratzeko triste
dagoen guzia.

Santiago eguna
Pasaian seinale,
ailegatu ez danak
agin beza galde.
Hasieran jendea
zezenaren alde,
azkenean ez ziran
arrimatu zale.

Gure Pasaia beti
sonatua dago,
errapin gabe ez da
urte bat igaro.
Hurrengoan jarriko
dituztela nago
barrera estuago o
zezen haundiago.

Torilotik atera
zuten lehendabizi,
behi zahar bat gidaria
bandera ta guzi.
Harrapatzen zuena
burrukatzen hasi,
azkenean kanpora
plazatik ihesi.

Toilotik bazterreko
barrera tartean,
zezena pasa zaie
kolera batean.
Plazatikan kanpora
zeudenak pakean,
erdiak burrukatu
ditu hankapean.

Hiru komerziante
plazatik kanpoan,
pospolo ta barkilo
labana tratuan.
Pareta egon arren
hamar oin altuan
panparroi pasatu zen
putzura saltoan.

Horiek horrela eta
gehiago ere bai
lurrean ziraldoka
agin zuten lasai.
Ezkonberriak eta
zenbait dama galai,
zezenaren mendean
ez zuten jarri nahi.

Plazatik itsasora
salta da igari
adarrak besterikan
ez zuen ageri.
Bakeroa begira
bere zezenari,
eseri ta jan ditu
hiru libra ogi.

Torilotik hasita
gaztelura arte,
bide kaskarra dago
bastante aparte.
Begiak erne zeuzkan
belarriak tente.
Esan nuen ote zan
lehengo Almirante.