---
id: ab-4966
izenburua: Horra, Mari Domingi
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004966.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004966.MID
youtube: null
---

Horra Mari Domingi, begira horri,
gurekin nahi duela Belena etorri,
gurekin nahi baduzu Belena etorri,
atera beharko duzu gona zahar hori.
Atoz, atoz, zure bila nenbilen ni,
atoz, atoz, zure bila nenbilen ni.
Atoz goazen, adora dezagun Belenen jaio den
haur eder hori, haur eder hori.