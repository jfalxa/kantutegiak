---
id: ab-4998
izenburua: Ozaze Jaurgainean
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004998.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004998.MID
youtube: null
---

Ozaze jaurgainean
bi zitroin doratu.
Atharratzeko jaunak
bata du galdatu.
Arrapostu uken du
ez direla huntu.
Huntu direnean
batto ukenen du.

2:Atharratzeko hiria,
hiri ordoki.
Ur handi bat badizu
alde bateti,
errege bidea
erdi-erditi,
Maria Madalena
beste aldeti.

- Portaleara joan zite,
ahizpa maitea,
engoiti horra duzu
Atharratzeko jauna.
Otoi, erran izozu
ni eri nizala,
zazpi egun hoietan
ohean nizala.

- Bai, baina ez nukezu
hortan sinetsia,
hari erraiten badot
zu eri zirela,
zazpi egun hoietan
ohean zirela,
bera nahi dukezu
jin ziren lekura.

- Ahizpa, jaunts ezazu
arrauba zuria,
nik are jauntiren dut
ene zaia berdia.
Engoiti horra duzu
zure senargaia,
pozik kita
ezazu soretxia.

- Klara, zoaza orain
salako leihora
ifar ala hegoa
denez jakitera.
Ifarra balin bada
goraintzi Salari,
ene gorputzaren xerka
jin dadila sarri.

- Ama, joanen gira
oro elkarreki.
Etxerat jinen zira
xangri handireki.
Bihotza kargaturik,
begiak bustirik,
eta zure alaba
tonban ehortzirik.

- Ama, saldu nauzu
bigai bat bezala,
bai eta desterratu
oi, Espainiara.
Aita bizi ukan banu,
ama, zu bezala,
ez ninduzu ezkunturen
Atharratzeko Salala.

- Ozazeko zeñiak
du arrapikatzen,
Jaurgainek'anderea
herritik partizten.
Haren peko zaldia
urrez da zelatzen,
hango ttipi handiak
beltzez dira beztitzen.