---
id: ab-6047
izenburua: Santa Ageda (Aldaera)
kantutegia: Euskal Kantak
partitura: null
midi: null
youtube: null
---

Bularra moztu eta eraman
kalabozoan barrena.
Barbara bere hitzean firme,
aitak hori zuan pena.
Gobernadore batek eman du
erremateko ordena:
ezpatarekin bertan lepoa
kentzea dala onena.