---
id: ab-5025
izenburua: Amerikatara Joan Nintzan
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005025.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005025.MID
youtube: null
---

Ameriketara joan nintzan xentimorik gabe,
handik etorri nintzan, maitea, bost milioen jabe.
Txin, txin, txin, txin, diruaren hotsa,
haretxek ematen dit, maitea, bihotzean poza.