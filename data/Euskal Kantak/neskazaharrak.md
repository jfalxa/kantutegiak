---
id: ab-4910
izenburua: Neskazaharrak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004910.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004910.MID
youtube: null
---

Neskazaharrak josten dira
Madalenara, Madalenara, Madalenara,
santuari eskatzera senar on bana,
senar on bana komeni bada.
Santuak esaten die
buruakin ez, buruakin ez, buruakin ez,
zergatikan lehenago akordatu ez,
akordatu ez, orain batere ez.
Bat eta bi, hiru, lau eta bost,
hamar gehiago dira hamabost.

Nik baino biko gehiago
iten baituzu, iten baituzu, iten baituzu,
marama eder polita izango dezu,
izango dezu, erosten badezu.
Bat eta bi, hiru, lau eta bost,
hamar gehiago dira hamabost.