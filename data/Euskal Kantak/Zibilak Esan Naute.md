---
id: ab-5041
izenburua: Zibilak Esan Naute
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005041.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005041.MID
youtube: null
---

Zibilak esan naute
biziro egoki
Tolosan behar dala
gauza erabaki.
Giltzapean sartu naute
poliki-poliki,
negar egingo luke
nere amak baleki.

Jesus tribunalean
zutenean sartu
ez zion Pilatosek
kulparik bilatu.
Neri ere ez aurkitu
ez didate barkatu.
Zergatik ez dituzte
eskuak garbitu.

Kartzelatik atera,
fiskalan etxera
abisatu zidaten
joateko berehala.
Ez etortzeko gehiago
probintzi honetara
orduan hartu nuen
Santander aldera.