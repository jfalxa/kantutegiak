---
id: ab-5009
izenburua: Horra Hor Goiko
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005009.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005009.MID
youtube: null
---

Horra hor goiko hariztitxo baten
kukuak umeak egin dozak aurten,
kukuak egin, amilotxak jan ,
hauxe bere kukuaren zoritxarra zan.
Lalaralala, lala...

Amilotxari kukuak dirautso:
"Ez dauat itxiko gaur hazur bat oso."
Ume jaleak erantzunentzat
dino: "Hire ardurarik ez zaukat".
Lalaralala, lala...