---
id: ab-5010
izenburua: Aita San Migel
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005010.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005010.MID
youtube: null
---

Aita San Migel Idurretako,
zeru altuko lorea.
Agura zaharrak dantzan ikusi
t'hak egiten dau barrea.

Garaiko plazan otea lore,
Ermuan asentsioa.
Dantzan ikasi gura dabena
Barruetara beioa.