---
id: ab-4914
izenburua: Andre Nobia
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004914.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004914.MID
youtube: null
---

Andre nobia, ile horia,
edeki zazu ataria,
badakarragu bada,
Juan Martin bizar haundia.

Hemen heldu naiz, baina beldur naiz
penak izain ote ditugun maiz.
Penetaik libratzea
ez litzake izain gaitz.

Nere maitea, ez egon sustoz,
biziko gera munduan gustoz.
Palazio bat inen dugu ,
zekalez edo lastoz.

Eskalerak, gaztain abarrez,
gaineko sala, iratze berdez.
Balkonak ere basa lizarrez;
teilatua berriz, belar idorrez.
Lan horien iteko,
karriko Baigorriko Piarres.