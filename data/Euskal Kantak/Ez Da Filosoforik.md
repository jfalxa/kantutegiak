---
id: ab-4903
izenburua: Ez Da Filosoforik
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004903.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004903.MID
youtube: null
---

Ez da filosoforik,
ez teologorik,
ardoari neurria
hartuko dionik.
Gizon haundiak ere
ikusi ditut nik,
beren mozkorra ezin
disimulaturik.

Gizon bat ardo gabe
dago erdi hila.
Barbar dabiltza tripak
ardoaren bila.
Baina edanez gero
ardo ona ta ongi,
gizonik kaskarrenak
balio ditu bi.

Londresen ta Parisen,
Erroman ta Goan,
ardoaren usaina
daukate gogoan.
Ardoak kentzen digu
bihotzeko mina.
Berriz edaretako
eman dit zotina.