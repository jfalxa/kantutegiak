---
id: ab-4915
izenburua: Ezkonberriak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004915.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004915.MID
youtube: null
---

Ezkonberriak pozkidaz daude,
pozkidaz daude,
egin diralako gaur alkarren jabe
elizan.
Gauza ederragorik ezin izan,
ai, banengo ni orain zuen ditxan.