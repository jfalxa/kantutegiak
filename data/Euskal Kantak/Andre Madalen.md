---
id: ab-5013
izenburua: Andre Madalen
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005013.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005013.MID
youtube: null
---

Andre Madalen, Andre Madalen, laurden erdi bat olio,
aitak saria ekarritzian amak ordainduko dio.
Amak ordaintzen ez baldin baio aitak ordainduko dio,
aitak ez baio ordaindu nahi, hor konpon eta adio.