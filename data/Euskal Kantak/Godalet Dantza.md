---
id: ab-5012
izenburua: Godalet Dantza
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005012.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005012.MID
youtube: null
---

Godalet dantza,
godalet dantza.
Besta hunen akabatzekotz
emanen dugu pikoz
godalet hunen inguruan
dantza famatu ederrena.

Godalet dantza,
godalet dantza...
Segi gaiten usai onari,
kasu godalet horri,
erortzen balitz zangopean,
itsusi laite guzientzat.

Godalet dantza,
godalet dantza...
Godaletak arnoz beterik
ez duke itxi peilik,
sobera maite dugulakotz
mahats odola edaritzat.