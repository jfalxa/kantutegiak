---
id: ab-4953
izenburua: Arin, Arin Kristauak
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004953.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004953.MID
youtube: null
---

Arin, arin kristauak,
arin bai Belena,
Jesus adoratzera
ahal dan lasterrena.

Zer gertatzen ote da
Belengo herrian,
gure Jesus jaio da
estalpe batian,
ta haruntz honuntz lasterka
artzaiak mendian,
alkar bilatzen dira
ahalegin guztian.

Biolina, txistua,
danbolin soinua,
artzaiak jotzen daude
atsegin ta pozez.
Hotzak eta beharrak
hiltzeko zorian,
estalpe zahartxo baten
leku irikian.

Arin, arin kristauak...