---
id: ab-4980
izenburua: Agur Maria, Ia, Ia
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004980.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004980.MID
youtube: null
---

Agur Maria, ia, ia
ua, ia, ia, ia,
eunero moskortuta
ibiltzen gera gu.

Hamaika mozkor gera,
beti-beti mozkortzen,
beti-beti dispuesto
ehizara joateko.
Agur Maria,...

Brasileña jentila,
ibili kontuz-kontuz,
baina beti dispuesto
Donostira joateko.
Agur Maria,...