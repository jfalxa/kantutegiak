---
id: ab-5027
izenburua: Agur, Jaunak!
kantutegia: Euskal Kantak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/005027.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/005027.MID
youtube: null
---

Agur, jaunak,
jaunak, agur,
agur t'erdi.
Denak Jainkoak
inak gire ,
zuek eta
bai gu ere.
Agur, jaunak, agur,
agur t'erdi
hemen gire.
Agur, jaunak.