---
id: ab-4662
izenburua: Potagiarena
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004662.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004662.MID
youtube: null
---

Egunaren icena
Ez daquit nola zan,
Baña señalagarri
Partidu bat bazan
Pelotan jocatzeco
Oyanzurgo plazan:
Nai det nere orduco
Guertaera esan,
Urrengoan beste bat
Escarmenta dezan.

Partidua jocatzez
Bucatu zanean
Ostatura joan nintzan
Zucen zucenean;
Mayera jana ecartzen
Asi ciranean,
Apaiz bat servitzen zan
Aurren aurrenean,
Sartu zan maquiñabat
Aren barrenean.

Nere dicha guciac
Badute agia,
Nozqui naiz Jaungoicoac
Astutzat lagia;
Goseac eguin nuen
Ango viagia
Culpa zuenarentzat
Dacat coragia,
Apaiz batec jan zuen
Nere potagia.

Zinzur ona zuela
Claro señaleac
Eman cituen apaiz
Barbantzu zaleac:
Docenaca tragatzen
Cituen aleac
Mascatu ere gabe
Tripazai jaleac
Alaco aisa nola
Anchoa baleac.

Pasatutzen cituen
Igar ta ezaac
Ez ciran aren diña
Zetocen claseac;
Agertu orduco ceuzcan
Tripara paseac,
Cortesia gabeco
Chapela luceac;
Arrec naicoa jan ta
Ni berriz goseac.

Gueroztic ostatuan
Sartu orduraco
Galdea eguin gabe
Ez det nic faltaco
Egun artan apaizic
Ea dan araco;
Ez apaiz jaun guciac
Igualac diralaco,
Baicic batec beldurra
Sartu ciralaco.