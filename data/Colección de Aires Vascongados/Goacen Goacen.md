---
id: ab-4689
izenburua: Goacen Goacen
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004689.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004689.MID
youtube: null
---

Goacen goacen
gu peregrinatzen
Zampantzar jaunagana
Promesa cumplitzen. (bis)

Utzi Alemania
artu Donostia
Cantatzen ezque
gatoz licencia. (bis.