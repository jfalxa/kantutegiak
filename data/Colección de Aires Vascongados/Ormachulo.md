---
id: ab-4705
izenburua: Ormachulo
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004705.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004705.MID
youtube: null
---

Ormachulo batetic
gau batean iguesi,
arratoi beltz audi bat
nuen nic icusi, (bis)
cijoan pir cor qui:
Nora nai saltatzeco
et cegoen gaizqui,
bañan atzaparra cion
catu batec ezarri (bis)
humildu zan sarri.