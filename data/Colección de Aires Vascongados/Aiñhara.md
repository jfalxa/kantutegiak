---
id: ab-4712
izenburua: Aiñhara
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004712.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004712.MID
youtube: null
---

Gaztetarzunac banara bila,
Airian aiñhara bezala, (bis)
Gayac ere igaraiten ditut
Egunac balira bezala
O! maitia ni bethi zugana!

Amodioric badutala
Etzaizia iduri
Zure gatic igarau nezake
Itcha soa igheri
O hambat zare charmagarria.