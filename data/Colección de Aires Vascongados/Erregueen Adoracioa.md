---
id: ab-4691
izenburua: Erregueen Adoracioa
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004691.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004691.MID
youtube: null
---

Atozte icustera
Atozte gendeac
Jesusaren oñetan
Iru erregueac.
Emen berdintzen dira
Aberats pobreac
Berdiñac badirade
Gure birtuteac.

Aurcho jayo berria
Ceruco lorea
Zure oñetan dago
Melchor Erreguea;
Eman dizu biyotza
Escaintzen urrea
Ezdu bereric ezer
Guztia zurea.

Jesus zure aurrean
Da Gaspar arquitzen
Zaitu adoratzen ta
Zaitu alabatzen,
Escaintzen incensua
Biyotza ematen
Barcacioa eta
Gracia escatzen.

Mesias eguiyazco
Jabe on bacarra
Jesus cerubetatic
Jachitaco aurra;
Baltasaren escutic
Ar ezazu mirra
Eta indazu arren
Zure on esquerra.

Ona Jesus gure Jaun
Ceruco aundia
Emen zure oñetan
Auspez ipiñia;
Guizonaren lurreco
Aquinte guztia
Galdez barcacioa
Escatzen gracia.