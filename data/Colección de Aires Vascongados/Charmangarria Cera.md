---
id: ab-4690
izenburua: Charmangarria Cera
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004690.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004690.MID
youtube: null
---

Charmangarria cera
eder eta gazte
Nere biotza ezduzu
besterie maite.
Beste cembait becela
Banegoque libre
Zurequin ezcontzea
Dudaric eznuque.

Maitea jaiqui jaiqui
Plazer dezunean
Iñorc icusi gabe
Iluna barrean
Lagun bat emango dizut
Joateco bidean
Arec jarrico zaitu
Tranquil biotzean.

Maitea churi zera
Elurra bezela
Bai eta mintzo zera
Profeta bezela
Orain dainocoan ere
Badira zobera
Baldin placer badezu
Atozquit aldera.