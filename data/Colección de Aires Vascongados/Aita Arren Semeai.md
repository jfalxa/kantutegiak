---
id: ab-4680
izenburua: Aita Arren Semeai
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004680.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004680.MID
youtube: null
---

Goacen, goacen, mutillac,
Goacen Africará,
Moroac jaquin dezan
Gu ere an gueralá.
Bayonet zorrotzaquin
Sartu beretará,
Ecarri bear degu
Marroqui banderá.

Tercio euscaldunac
Chit mutill azcarrac,
Ez ditu garaituco
Moroen indarrac.
Gustiz beroac dira
Euscaldun odolac,
Icaratuco ez ditu
Chistu balarenac.

Euscaldun gende noble
Paregabecoac,
Valiente eta prestu
Gaiñ gañatecoac.
Doai au eman cigun
Gure Jaungoicoac,
Pruebac emanac daude
Gueren gurasoac.

Paviaco batallon
Juan de Urbietac,
Preso gelditu zuen
Erregue francesa.
Euscaldunentzat au dá
Oroipen galanta;
Iñorc dudaric badu
Historiac daca.

Legorrez eta ichasoz
Euscaldun gendeac
Daude oraiñ eta len
Gloriaz beteac.
Oquendo ta Churruca
Eta Otalorac
Guerrariac cirala
Badirade pruebac.

Lenagocoac ecen
Oraingo mutillac,
Ez dira escasago
Bay ayen berdiñac.
Honore andia dute,
Gorputza liraña;
Moro casta gustia
Ez da orien diña.

Isabel bigarrena,
Gure Erreguiña,
Euscaldunac dizute
Biotzetic griña.
Onen defensaraco
Eguin aleguiñac,
Naiz campoan guelditu
Luce, luce, illac.

D.Carlos de Latorre,
Gure Generala,
Zure mendean goaz
Laster peleara.
Zuc aguindu ezquero
Noiz nai listo guera,
Baita jartzeco ere
Tanger-en bandera.