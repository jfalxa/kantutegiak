---
id: ab-4656
izenburua: Adio Euscal-Erriari
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004656.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004656.MID
youtube: null
---

Gazte gaztetanican
Erritic campora
Estrangeri aldean
Pasa det dembora
Eguia aldegustietan }
Toqui onac badira }
Baña biotzac dio }
Zuaz euscal errira } (bis)

Lur maitea emen uztea
Da negargarria
Emen gelditcen dira
Ama eta Erria.
Urez noa icustera
Bai, mundu berria
Oranche bai naizela
Erruquigarria.

Agur nere biotzeco
Amacho maitea
Laister etorriconaiz
Consola zaitea.
Jaungoicoac bada naidu
Ni urez juatea
Ama certaraco da
Negar eguitea.