---
id: ab-4651
izenburua: Aita Meagher-Ec
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004651.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004651.MID
youtube: null
---

Ni naiz chit gauza gozoa
eta pozquida osoa
beltza naiz ta zuria
illuna ta arguia
indarra det eta garboa
eta icena det ardoa.