---
id: ab-4649
izenburua: Chanton Piperri
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004649.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004649.MID
youtube: null
---

Chanton Piperri emen dago
Limosna on bat balego,
Canta beardot
Baldin albadot
Neure sabelen gosia
Erruqui dedia gendia.

Iru lagunec Ispasterren
Apostuba eguin eben
Jango ebela
Goiztic gabera
Amar erraldeco chalá
Neau laugarren nitzala.

Pozaz neguan zoraturic
Jateco laurden bat osoric,
Nere contuac
Beti alanguac
Joantzan poza putzura
Ametza oidiran modura.

Lequeitioco calian
Bichigu dembora danian
Oguei chicharro
Eta gueyago
Eta guciyac gabian
Sartzen dodaz sabelian.

Orduban contentu nago
Gaberdi osteradaño,
Andic goicera
Neure sabela
Beti dago gur gur gur gur
Bartco afariya agur.

Guztiz zabala dot zabela
Bardin bardin dot guibela
Este bacochac
Cabiucoluque
Gambela bete oquela
Bañan noiz izango oteda.

Ez da errotan arriric
Neure aguiñac langoric,
Neure aguiñac
Zatiucoluque
Ogui andibat osoric
Balego bigun bigunic.