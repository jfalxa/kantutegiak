---
id: ab-4707
izenburua: Frascu Chomin
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004707.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004707.MID
youtube: null
---

Nere Andrea andre ona da
gobernu ona du auzuan
Artzen dubela bere alaba
Mari Catalin alboan.
Aisazu Cernaizu
Guerore orrela mundu onetan
Bia bicico guerare gu
Baldin basera contentu (bis)

Zu beti Frascu Chomin tabernacua
Zu icustera nator pozes betia
Ai cer contetu ai cer alegre!
escanzei dezut nere maitia
Arraultchachuac eta caicu eznia, (bis)
Caicu caicu caicu caicu
caicu caicu eznia
Caicu caicu caicu caicu
caicu eznia.

Arantzazura ninjuanean
Maquileho baten gañean
Arantza beltz bat sartu cizairan
Ozpela nuben onean
Aisazu Cernaizu (bis.