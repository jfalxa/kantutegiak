---
id: ab-4652
izenburua: Ezcon Berriac
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004652.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004652.MID
youtube: null
---

Ezcon berriac
pozquidaz daude
pozquidaz daude
eguindiralaco gaur
alcarren jabe
elizan gauza ederragoric
ecin izan
hai orain
banengo ni
zuen dichan
gauza ederragoric
ecin izan
hai orain
banengo ni
zuen dichan.