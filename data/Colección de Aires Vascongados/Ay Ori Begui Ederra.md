---
id: ab-4663
izenburua: Ay Ori Begui Ederra
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004663.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004663.MID
youtube: null
---

Ay ori begui ederra
irurisentzait eperra
Gauza oberican cerda
Oincho polita
sapata ederr
Chorcatilla guztis fiña
Jantci ederrqui eguiña
Telia modaco fiña
Ay neretzaco basiña.