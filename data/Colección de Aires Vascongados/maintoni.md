---
id: ab-4675
izenburua: Maintoni
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004675.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004675.MID
youtube: null
---

Gabaz eta egunaz
zugana pensatzen
zure beguiac naube
ni enamoratzen.
Atos atos neugana
Maintoni maitea
nere consueloa da
zu icustea.

Maintoni maitea
Zu icusitzean
Cerbait nic aditzen det
Nere biotzean
Begui beltz ederraquin
Colorea fiña
Maintoni zure ondoan
¿Cer da erreguiña?

Beguiz begui jarriric
Cutunchu maitea
Yruritu estzaidan
Cerua icustea
Nere biotza dago
Consueloz betea
Yzango zera laco
Neure e(s)maztea.

Nere biotza dago
Chit negargarria
Ecin consolaturic
Neure amorea
Testigo izango zera
Aizcorri mendia
Oñaten guelditzen da
Neure amorea.