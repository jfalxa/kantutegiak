---
id: ab-4671
izenburua: Toto Pintocho
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004671.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004671.MID
youtube: null
---

Peruduacoc uste baceben
Etzala diru biarric.
Bacallau ona eramateco
Bilbaoco barren caletic.

Gustiz dotore palo bategaz
Sartu zan barren calean
Chacur gorri bat oin churiyagaz
Ebela aldemenean.

Sartu zanean barren calean
Bacallau ona icusiric
Escatu otzan emanceiola
Lepua bete duaric.

O! guizon tonto cer penzatzen doc
Asto salbage andia
Ic ere ez euque duan emango
Eure cortaco iriia.

Bacallau orrec illic dagoz ta
Ene idia biciric
Usteldu eta lizuntzen dagoz
Eman litesque duaric.

Ene guizona gura badozu
Zeure lepoco carguia
Amalau cuarto costaco satzu
Bacallau orren libria.

Toto Pintocho guacen echera
Ambait lasterren emendic
Eznauque beingo beingoan irtengo
Bilbaoco barren caleti.