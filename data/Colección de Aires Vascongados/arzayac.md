---
id: ab-4694
izenburua: Arzayac
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004694.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004694.MID
youtube: null
---

Zortzicoa cantari
Menditic calera
Donostiara pozez
Gaur etorri guerá.
Ariñ maquil saltoan
Aldapetan berá
Iñauterietaco
Festac ecusterá.