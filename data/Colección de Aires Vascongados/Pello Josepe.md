---
id: ab-4706
izenburua: Pello Josepe
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004706.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004706.MID
youtube: null
---

Pello Josepe tabernan dala
aurra jaio dala Larraulen. (bis)
Echera juan ta ezan emen du
es ta neria isañen.
Ama orrec berac topa,
desala aurrorre aita seinduen. (bis)

Ay au pena ta pesadumbria
Senarrac aurra ucatu (bis)
Aurrac onentzat beste jaberic
Ezin nezaque topatu,
Pello Josepe biyotz neria
Aur onec aita zu saitu. (bis.