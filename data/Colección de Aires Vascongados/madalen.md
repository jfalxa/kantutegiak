---
id: ab-4700
izenburua: Madalen
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004700.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004700.MID
youtube: null
---

Madalen busturingo
Gabian gabian
Errondian da vilza
Zure portalian. (bis)
Ay! Madalen Madalen
Ay! Madalen
Zure billa nebillen
Orain baño len.

Asi goico caletic
Beco caleraño
Ez da ederragoric
Madalencho baño
Ay! Madalen Madalen
Ay! Madalen
Zure billa nebillen
Orain baño len.