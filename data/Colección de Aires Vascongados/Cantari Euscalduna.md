---
id: ab-4713
izenburua: Cantari Euscalduna
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004713.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004713.MID
youtube: null
---

Guitarra zarcho bat da
Nerezat laguna
Onela ibilcenda
Artista Euscalduna.
Egun batean pobre
Beste batez Jauna
Cantari pasatzen det
Nic beti eguna.

Naiz dela Italia
Oro bat Francia
Bita billatu det
Anitz malicia...
Icusten badet ere
Nic mundu gucia
Beti maitatuco det
Euscal-erria!

Jaunac ematen badit
Neri osasuna
Izango det oraindit
Andregai bat ona;
Emen badet francesa
Interesa duna...
Baña nic naiago det
Utsic Euscalduna.

Agur Euscal-erria
Baña ez betico
Bost edo sei urtean
Ez det icusico...
Jaunari escatcen det
Gracia emateco
Nere lur maite ontan
Bitcia uzteco.