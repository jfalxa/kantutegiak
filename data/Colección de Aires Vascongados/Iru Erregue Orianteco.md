---
id: ab-4687
izenburua: Iru Erregue Orianteco
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004687.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004687.MID
youtube: null
---

Iru erregue Orianteco
Gazpar Melchor ta Baltazar
Ayec irurac omen cequiten
Trinidadea nola zan
Trinidadea Trinidadea
Virgiña amaren semea
Urasen da gucion erreguea.

Iru erregue oriantetic
Jesus onaren bidera
Izar eder bat aguerturican
Bidea eracustera
Izarrarequin ciran guiatu
Zuten artean Jesus billatu.