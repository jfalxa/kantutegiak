---
id: ab-4650
izenburua: Arantza Zorrotza
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004650.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004650.MID
youtube: null
---

Amodioa bai da
Arantza zorrotza.
Ceñac Zulatzen duen
Portizqui biotza,
Eritu baguetanic
Ez liteque goza
Beragandic sarritan
Jayo oidan poza.

Naitasun gueyeguia
Artzea iñori
Cein gauza gaiztoa dan
Progatzen det ongui.
Atzecabez beteric
Gau ta egun beti
Ezur utz biurturie
Arquitutzen naiz ni.

Nere maite polita
Zutzaz oroitzean
Odola pill pill dabill
Nere biotzean,
Campora irten nairic
Aleguin gucian
Min char au bearco zait
Sendatu illtzean.

Itzuca nabil beti
Consuelo billa
Arquitzen zait dalaco
Biotza chit ílla,
Alperric izango da
Nere aleguiña
Sendatu ez det uste
Daducaran miña.

Icusiric galaya
Zure etzimena
Nere biotz gaishoac
Artutzen du pena,
Zu sendatzeco nerau
Nintzaque aurrena
Banequique seguru
Menetan zaudela.

Biotzean jayota
Jarriric beguian
Nere amodioa
Dago aguidian,
Egunaz bezalaishen
Gauaren erdian
Neure maíte maitea
Zauzcat memorian.

Galay guztea guztiz
Leunqui mintzocera
Bañan bide anitz da
Mitic biotzera,
Sinistmenic ain erraz
Ez noa artzera,
Lore aurreratuac
Oi datoz galtzera.

Goiz datorren lorea
Berez bada piña
Irme ichiquitzeco
Izango du griña,
Chertu onetic gogoz
Badator eguiña
Aren alea ez da
Izango samiña.

Ale gozoaz ere
Eldu baguetanic
Gogoz jan eta gaitz eguin
Icusten ditut nic
Dala erre egosi
Orobat gordiñíc
Alaco janariac
Ez dezaque onic.

Ez nuque iduquico
Biotza ain triste
Naitasun eltzaquea
Baldin banizuque,
Maitatzent zaiturala
Badira lau urte,
Zeurea eznazula
Ez dezu cer uste.

Galai gaztea ez da
Cer esan gueyago,
Damu det ez jaquiña
Gauza au lenago,
Atzecaberic esco
Dezula igaro
Nere biotz gaishoa
Penaturic dago.

Dama zuregan dago
Nai dezuna eguin,
Nere gogoco berri
Badaquizu berdin,
Oi penatuaz artzen
Badezu atzeguin
Lembait len ta beingoan
Obeco nazu ill.

Ez det nai zu illtzeric
Ain guchi penatu
Gogoratzea ere
Litzaque becatu,
Galaya ez dezu cer
Gueyago necatu
Zurea nazu betico
Nai banazu artu.