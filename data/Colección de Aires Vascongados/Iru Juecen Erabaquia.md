---
id: ab-4688
izenburua: Iru Juecen Erabaquia
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004688.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004688.MID
youtube: null
---

Chabalategui eta Zabala gaztea
Ascoc deseo zuen
Cantuz icustea.
Atseguin andietan
Guelditu da gendea
Bata adimbat eguin
Bear degu bestea.

Ecin guentzaque bada
Beste gauzaric eguin,
Nic gogoan neucana,
Zuc dezu itceguin:
Utci bear ditugu,
Biac berdin berdin,
Guipuzcoa guciac
Ardezan atseguin.

Jaunac mintzo cerate,
Griña charric gabe,
Pozquidatu nazute
Chit asco alere:
Eguia esateco
Ifiniac gaude,
Bata becin bestea
Cantari onac daude.