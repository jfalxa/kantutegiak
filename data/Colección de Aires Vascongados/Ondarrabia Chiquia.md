---
id: ab-4701
izenburua: Ondarrabia Chiquia
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004701.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004701.MID
youtube: null
---

Felipe bostgarrena
zanean etorri
Españiara aguintari
Yrunen dantzatu zan
Pachico chiquia
arritu zuen jende guztia,
arren dantzatzeco
jaquinduriac
Pozquidatu zituen
Españiaco guizon aundiac,
Erregue maite du
Ondarrabiac.