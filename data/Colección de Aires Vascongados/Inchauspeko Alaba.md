---
id: ab-4718
izenburua: Inchauspeko Alaba
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004718.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004718.MID
youtube: null
---

Inchauspeko Alhaba dendaria
Goizian goisik jostera juailiam
Nigarretan pazatzendu bidia
Aprendiza consolagarria.

Zelian den izarrik ederrena
Jin balekit argi egitera,
Juan nainte maitiren ikhustera
Ene phenen hari erraitera.

Mündü huntan phenarik gabetarik
Bizitzeko nik eztüt perillik;
Maithatü düt ukhenen eztüdana,
Horrek beitereit bihotzian phena.

- Maithatü düzia ükhenen eztüzüna,
Horrek dereizia bihotzian phena?
Maith'ezazü ükhen diokezüna
Eta juanen da zure bihotz mina.

- Merchikaren floriaren ederra!
Barnian dizü echurra gogorra;
Gaztia niz, bai eta loriusa,
Eztizüt galtzen zure esparantcha.

Adios beraz, ene maitia, adios!
Adios dereizut orai sekülakoz;
Ezkunt zite plazer duzunareki,
Eta begira ene arrakuntruti?

- Zer izanen da zure arrakuntria?
Zer ahal da zure egin ahala?
Zerbait khasü nitan gerthatzen bada
Sujet berri bat zütan izanen da.