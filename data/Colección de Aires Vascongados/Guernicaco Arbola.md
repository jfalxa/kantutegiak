---
id: ab-4648
izenburua: Guernicaco Arbola
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004648.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004648.MID
youtube: null
---

Guernicaco arbola
Da bedeincatuba
Euscaldunen artean
Guztiz maitatuba:
Eman ta zabaltzazu
Munduban frutuba,
Adoratzen zaitugu
Arbola santuba.

Milla urte inguruda
Esaten dutela
Jaincoac jarrizubela
Guernicaco arbola:
Saude bada zutican
Orain da dembora,
Eroritzen bacera
Arras galduguera.

Etzera erorico
Arbola maitea,
Baldin portatzen bada
Vizcaico juntia:
Lauroc artuco degu
Surequin partia
Paquian bici dedin
Euscaldun gendia.

Betico bicidedin
Jaunari escatzeco
Jarri gaitecen danoc
Laster belaunico:
Eta biotzetican
Escatu esquero
Arbola bicico da
Orain eta guero.

Arbola botatzia
Dutela pentzatu
Euscal erri guztiyan
Denac badakigu:
Ea bada gendia
Dembora orain degu,
Erori gabetanic
Iruqui biagu.

Beti egongocera
Uda berricua,
Lore ainciñetaco
Mancha gabecoa:
Erruquisaitez bada
Biotz gurecoa,
Dembora galdu gabe
Emanic frutuba.

Arbolac erantzun du
Contuz bicitzeko ,
Eta biotzetican
Jaunari escatzeco:
Guerraric nai ez degu
Paquea betico,
Gure legue zuzenac
Emen maitatzeco.

Erregutu diogun
Jaungoico jaunari
Paquea emateco
Orain eta beti:
Bay eta indarrare
Cedorren lurrari
Eta bendiciyoa
Euscal erriyari.

Orain cantaditzagun
Laubat bertzo berri
Gure provinciaren
Alabantzagarri:
Alabac esaten du
Su garrez beteric
Nere biotzecua
Eutzico diat nic.

Guipúzcoa urrena
Arras sentituric
Asi da deadarrez
Ama guernicari:
Erori etzeitzen
Arrimatu neri
Zure cendogarriya
Emen nacazu ni.

Ostoa verdia eta
Zañac ere fresco,
Nere seme maiteac
Ez naiz erorico:
Beartzen banaiz ere
Egon beti pronto
Nigandican etzayac
Itzurerazoco.

Guztiz maitagarria
Eta oestarguiña
Beguiratu gaitzasu
Ceruco erreguiña
Guerraric gabetanic
Bici albaguiña.
Oraindaño izandegu
Guretzaco diña.