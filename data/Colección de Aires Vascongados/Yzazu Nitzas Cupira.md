---
id: ab-4717
izenburua: Yzazu Nitzas Cupira
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004717.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004717.MID
youtube: null
---

Loriac udan intza becela
Maite det dama gazte bat
Ari ainbeste nai diyodanie
ez da munduban beste bat
Yñoiz edo bein pasatzen badet
Ycusi gabe aste bat
biyotz gustira banatutzen zait
alaco gauza triste bat. (bis)

Ez aldirazu antzic ematen
Nic zaitutala nayago,
Ay mariñalac gau illunian
Yzarra baño gueyago:
Nere onduan zauzcatalaco
Pocez zoraturic nago,
Zu icustiac alegratu nau
Triste negüen lenago.

Nere betico pensamentuba
Nere consolagarriya,
Zu gabetanic ecin bici naiz
Esaten dizut eguiya:
Zu baciñaque arbola eta
Ni baldiñ banitz choriya,
Nic zu ciñaquen arbol artanchen
Eguingo nuque cabiya.

Nere biyotza urtzen dijua
Eta ez da misteryo,
Penaren cargac estutu eta
Sumua quendutzen diyo:
Beguiyac dauzcat gau eta egun
Eguiñican bi erriyo,
Beti negarra dariyotela
Zu cerarela meriyo.

Zu ceralaco meriyo baldin
Juaten banaiz lur azpira,
Güero damuba eta malcuac
Alferric izango dira:
Bein juan esquero oyen virtutez
Berriz ez niteque gira,
Ori guertatu baña lenago
Yzazu nitzaz cupir.