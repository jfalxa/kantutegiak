---
id: ab-4655
izenburua: Nere Maitiarentzat
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004655.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004655.MID
youtube: null
---

Ume eder bat icusi nuben
Donostiaco Calean,
Itz erdichobat ari esan gabe
Nola pasatu parean.
Gorputza zuan liraña eta
Oñac cebiltzan airean
Politagoric ez det icusi
Nere beguien aurrean.

Aingueru zuri paregabea
Euscal-errico alaba,
Usteric gabe zugana beti
Nere biotzac narama:
Icusi nayan beti ornabil!
Nere maitea au lana!...
Zoraturican emen naucazu
Beti pensatzen zugana.

Galai gazteac galdetzendute
Aingueru ori nun dago
Nere maitea nola deitzendan
Ezdu iñorchoc jaquingo.
Ez berac ere ezluque naico
Confianza orretan nago
Amorio dun biotz oberic
Euscal errian ezdago.