---
id: ab-4724
izenburua: Abesoarren Bandera
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004724.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004724.MID
youtube: null
---

Abesoarren bandera
A nolacua otecta
Errecaldera eraman dute
erakutzitzera;
Leitzterra berrez bidera
ongi etorri ematera
Al zu len oiainic juatzen
yguez egitera.

Abesoarrak Leitzen,
Etziren ongi portatzen
Okaziyua berak ziyoten
Bai beti sortutzen
Gure erritik guchi zen
Bañan ayentzat askizen
Alatan ere ayen koluna
Laizter puzkatu zen.

Arrantza eta chistue
Eratziak balitue
Dambotiñan emateko
Guur kumplimentue;
Ez dago unukitue,
Bera goyan gelditue
Jachi diranak pasaportiak
Logratu ditue.

Iñauteri egunean,
Konbitearen biltzean,
O so panparroi zebiltzan ustez
Ibegur aldean;
Gero ehera aruktzean
Batzubek orra mayean
Jornal onak atera ditutzte
Ibigur aldean.

Arigo Abesoarrak
Zer dio koplari orrek
Beste leku gueyagotan ere
Badituk mutillek
Hogei ta bi ziren ayek
Uztez etzeuden bildurrez
Cinta arrotu diyozkate bai
Amar Leitzarrek.

Abesoarrak furtzakin
Orra gozoak nuñakin
Buru atera uzte zenduter
Zubek gurekiñ
Leitzako mutillarekin
Orra probatu ta jakiñ
Ez dut geyago esan nai eta
Asco dut onekiñ.