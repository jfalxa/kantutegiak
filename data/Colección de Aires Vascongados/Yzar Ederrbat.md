---
id: ab-4660
izenburua: Yzar Ederrbat
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004660.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004660.MID
youtube: null
---

Izar eder bat daucat gogoan
Biotzeraño sarturic,
Berac ere amodioa
Firmea dauca arturic,
Ez det deseo beste Indiaric
Banecaque lograturic.

Empeño andiric ez dezu bear
Bada nerau logratzeco,
Cerorri bezain deseos nago
Ni zurequin mintzatzeco,
Virgiña Ama det bitarteco
Bioc alcarganatzeco

Ni engañatzen chit erraza naiz
Memoria det auzoan,
Ea, ea, jarri nazazu
Satisfaccio osoan,
Nere biotzac zugana naidu,
Artu nazazu besoan.

Gure doctrinac esaten digu
Ceruan dala atseguin
Nere barrengo sentimentua
Claro nai dizut itz eguin:
Ni zurequin ceruan nago
Naiz besterequin uts eguin.