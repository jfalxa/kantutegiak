---
id: ab-4727
izenburua: Ay Au Penaren Andia
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004727.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004727.MID
youtube: null
---

Ay au penaren aundia
neretzat chit ilgarria
orain artu deberia
Bestec erama duela
Nere maitecho maitegarria
Baldin au bada eguia
ni naiz urriquigarria
illunducozait beguia.