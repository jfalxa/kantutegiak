---
id: ab-4679
izenburua: Lo! Lo! Lo!
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004679.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004679.MID
youtube: null
---

Aurcho chiquia negarrez dago
Ama emazu titia
Aita gaiztoa tabernan dago
picaro jocalaria.