---
id: ab-4684
izenburua: Nere Maite Polita
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004684.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004684.MID
youtube: null
---

Nere maite polita
Nola cera bizi
Zortci egun onetan
Etzaitut icusi. (bis)

Uste det zabiltzala
Nigandic iguesi
Ez dirazu ematen
Atsecabe guchi.