---
id: ab-4682
izenburua: Beltzerana
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004682.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004682.MID
youtube: null
---

Beltzerana naizela
calean diote
Ez naiz zuri ederra
Arrazoya dute. (bis)

Eder zuri galantac
Pausoan amabi
Beltzeran graciosac
Milla etatic bi. (bis.