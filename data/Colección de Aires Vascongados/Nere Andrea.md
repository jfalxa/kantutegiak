---
id: ab-4658
izenburua: Nere Andrea
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004658.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004658.MID
youtube: null
---

Ezcongayetan cerbait banintzan /
ezcondu eta eceres / (bis)
Ederr salia banintzan ere
aspertu nintzan ederres
Nere gustua eguin nubenda
orain bici naiz dolorez.