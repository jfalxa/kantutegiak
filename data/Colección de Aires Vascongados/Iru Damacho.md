---
id: ab-4647
izenburua: Iru Damacho
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004647.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004647.MID
youtube: null
---

Iru damacho Donostiyaco
Errenteriyan dendari
Josten ere badaquite baña
Ardoa eraten obequi.
Eta crisquitin crosquitin
Arrosa crabelin
Ardoa eraten obequi.

Donostiyaco gaztelupeco
Sagarduaren gozua,
Anchen eraten ari nitzala
Ausi zitzaidan vasua.
Eta crisquitin crosquitin
Arrosa crabelin
Vasua cristalezcua.

Iru damacho Donostiyaco
Irurac gona gorriyac,
Sartzen dirade tabernan eta
Irtentzen dirade ordiyac.
Eta crisquitin crosquitin
Arrosa crabelin
Irtentzen dirade ordiyac.

Donostiyaco nescachachuac
Calera naidutenian,
Ama piperric ez dago eta
Banua salto batian.
Eta crisquitin crosquitin
Arrosa crabelin
Banua salto batian.

Donostiyaco iru damachoc
Eguin omendute apostu,
Ceñec ardo gueyago eran eta
Cein guchiago moscortu.

Arrosachoac bost osto ditu
Crabelinchuac amabi,
Mari Josefa nai duben orrec
Esca biotza amari.
Eta crisquitin crosquitin
Arrosa crabelin
Esca biotza amari.