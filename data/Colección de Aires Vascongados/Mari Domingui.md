---
id: ab-4683
izenburua: Mari Domingui
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004683.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004683.MID
youtube: null
---

Orra Mari Domingui
beguira orri
Gurequin nai du bela
Belena etorri
Gurequin nai badezu
Belena etorri
Atera bearco dezu
gona zar ori
Atoz Atoz
zure billa
nebillen ni. (bis)
Atoz goacen
Adora dezagun
Belenen jaio dan
Aur eder ori. (bis)

Nondican ateraren
Gona zar ori
Berriric baldin baden
Gorde zan ori
Arrituric ciagon
Belengo erria
Ateraquin ortic
Farragarria
Atoz Atoz (bis)

Jodezan panderoa
Yc Antonica
Nondican etorri aiz
Orren corrica
Ecin esplicaturic
Misterioa
Belenen jaio dala
Aur divinoa
Atoz Atoz (bis.