---
id: ab-4714
izenburua: Nere Amac Balequi
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004714.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004714.MID
youtube: null
---

Cibillac esan naute
biciro egoqui
Tolosan biar dala
gauzau erabaqui.
Guiltza pian sartu naute
poliqui poliqui
Negar eguingo luque
nere amac balequi.

Jesus tribunalian
Sutenian zartu
Etziyon Pilatosec
Culparic billatu.
Neri ere arquitu
Ez dirate barcatu
Cergatic ez dituzte
Escubac garbitu.

Carcelatic aterata
Fiscal echera
Abisatu ciraten
Juateco beriala
Ez etortzeco gueyago
Provintzi onetara
Orduan artunuan
Santander aldera.