---
id: ab-4686
izenburua: Mutil! Mutil!
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004686.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004686.MID
youtube: null
---

Mutil, mutil
jaiqui ari
Eguna ote dan
beguira ari
Nausia
eguna da
Gure ollarra
campoan da.

Mutil, mutil
Jaiqui ari
Eudia ote dan
Beguira ari.
Nausia
Eudia da,
Gure zacurra
Bustia da.

Mutil, mutil
Jaiqui ari
Suba ote dan
Beguiari.
Nausia
Suba bada
Gure gatua
Beroa da.