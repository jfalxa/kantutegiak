---
id: ab-4659
izenburua: Nere Senarra
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004659.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004659.MID
youtube: null
---

Esaten dute bada
ezcondu ezcondu
Etzait neri seculan
Gogoric berotu
Acer soriorduco
ezcondu bearra
Guerostic eguitendet
Sarritan negarra.