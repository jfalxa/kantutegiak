---
id: ab-4699
izenburua: Jardinzayen Bigarren Zortzicoa
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004699.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004699.MID
youtube: null
---

Soñu eta cantaquin
humore onean
Aitzurzen eta dantzan
denbora berean.
Soñu eta cantaquin
humore onean. (bis)
Gaindu dedien festa
Baracetacoa
Berriro moldatua
Degu zortzicoa.
Euscaldunaren canta
Antriñetacoa
Ytz neurtu egoquia
Biotz gurecoa.