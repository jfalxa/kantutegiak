---
id: ab-4668
izenburua: Belaun Chingoa
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004668.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004668.MID
youtube: null
---

Euscalerrietaco soñu ezti,
izan dana beti,
gucien gañetic
billatu det lurpean sartua,
bai eta aztua,
ceren dan zartua.

Ateratu bear nuque plazara,
nerequin danzara,
oraindic gauza da
ecusico da ceiñ atseguiña,
bizarra urdiña,
baña chit ariña.