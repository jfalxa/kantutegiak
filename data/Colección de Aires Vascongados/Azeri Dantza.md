---
id: ab-4674
izenburua: Azeri Dantza
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004674.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004674.MID
youtube: null
---

Alpargatac urratu
ta zapataric ez
Bermeon guelditu nintzan
oñeco miñez.

Ausedec eguiya
zortzico berriya
iru chiqui ardorequin
libraco oguiya. (bis.