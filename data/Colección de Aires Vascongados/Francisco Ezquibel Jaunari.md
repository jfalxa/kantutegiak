---
id: ab-4702
izenburua: Francisco Ezquibel Jaunari
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004702.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004702.MID
youtube: null
---

Españan dao guizom bat
bear deguna maita (bis)
Francisco Ezquibel juana
escualdunen aita.
Chita guizon prestua
eta jaquinsua
errepeta dezagun
bai gure maizua.