---
id: ab-4678
izenburua: Sacristau Zar Baten Diabruqueria
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004678.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004678.MID
youtube: null
---

¿Cer deabruqueria
zayo burutara?
¿Batista adiñ orretan
Doa escontzera? (bis)

Aur jayo
gizon bici
Aurcho biurtzea
orra ecusten danez
gure suertea.