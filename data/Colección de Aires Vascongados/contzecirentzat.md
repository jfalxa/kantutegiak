---
id: ab-4657
izenburua: Contzecirentzat
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004657.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004657.MID
youtube: null
---

Cantatutzera indar aundico
desio batec narama
Contzeci
Zuri nai dizut eman
mereciya dezun fama
Gure Provintzi Guipuzcoacuan
Ygualic gabeco dama
Berincatuba izan dedilla
zu eguincinduben ama.