---
id: ab-4693
izenburua: Nere Maiteari
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004693.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004693.MID
youtube: null
---

Iñoiz icusi dezu
Venus pintaturic
Aniz ederragoric
nitzaz da azturic.
Bat daucat biotzaren
Erdian sarturic
Ecin nombranezaque
nagoan illic.

Calipso ta Eucaris
Ninfa guztiaquin
Ez dira neurritzeco
Nere maiteaquin;
Larru churi ederra
Ille beltzarequin
Choratuco cinduzque
Bere beguiaquin.

Corpuz tamañacoa
Guerri mearequin
Bularra chiquichoa
Escu politaquin;
Pantorilla ederra
Oiñ chiquiarequin
Edertasan guztiac
Ditu berarequin

Egun sentico arrosa
Zabaltzen asia
Da zure ezpain gozo
Ichumen eztia;
Adios nere biotz
Corputzta vicia
Adios ne maite
Chit maitagarria.