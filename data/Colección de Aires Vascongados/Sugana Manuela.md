---
id: ab-4715
izenburua: Sugana Manuela
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004715.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004715.MID
youtube: null
---

Sugana Manuela
Nuanian pensatu
Ustedet ninduela
Deabruac tentatu
Ycusi besiñ equin
Naiz enamoratu
Ojala es basiñan
seculan agertu.

Amoriyoz beteric
Esperantza gabe
Cergatic eguiciñan
Biotz onen jabe:
Suc esan bihar senduben
Emendican alde
Equiyazqui es nais ni
Bizar dunen sale.

Astu nai saitut baña
Esiñ esiñ astu
Sure amoriyuac
Buruba dit galdu...
Oraiñ bigundu senduque
Eta maitagarri orrec
Biotz ori bigundu
Pizcabat lagundu.

Barcatu bear ditutzu
Nere erogueriac
Sure beguira daude
Nere bi beguiyac:
Garbi garbi esanditzut
Nere ustes eguiac
Soraturican nauca
Sure arpeguiac.

Gabian ibillinaiz
Gucisco amezetan
Donostian nengoela
Andre marietan
Eta icusinubela
Erri artaco plazan
Erdiya cebille la
Manuela-cho dantza.