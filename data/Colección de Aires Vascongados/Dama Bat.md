---
id: ab-4697
izenburua: Dama Bat
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004697.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004697.MID
youtube: null
---

Dama bat biotzean
zartu sait betico
señen icusi gabe
es unque etzico.
Berarequi gustora
nintzaque bizico
nere besuetatic
esnuque utzico.