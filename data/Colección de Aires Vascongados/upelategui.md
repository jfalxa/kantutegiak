---
id: ab-4670
izenburua: Upelategui
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004670.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004670.MID
youtube: null
---

Edan ezac goicean
churitic onetic
arratsaldean gorritic obetic. (bis)
Ala eguiten oidiat nic,
eta ic eguic
albadaguic on eguingodic. (bis.