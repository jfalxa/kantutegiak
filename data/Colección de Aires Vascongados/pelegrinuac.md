---
id: ab-4692
izenburua: Pelegrinuac
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004692.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004692.MID
youtube: null
---

Pelegrinuac datoz
Santiagotican
Atea iriqui besa
guicustia gatica.
Chomin jotza trompeta
Pachi non dec conqueta
Berdina balen baciagoc
ecarri beteta.