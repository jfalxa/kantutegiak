---
id: ab-4708
izenburua: Choriñoa Caiolan
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004708.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004708.MID
youtube: null
---

Choriñua caiolan caiolan
Triste dubela cantazen
Duelarica cer jan cer eran
Campoa deciatutzen ceren ceren
Libertadia cein eder den.

Bart nic negar eguin det
Ycusirican maitea,
Ycusi, eta ezin mintza
Ay auda pena aundia
Eein bertzea
Desiatudet Ylzi.