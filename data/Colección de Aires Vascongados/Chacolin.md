---
id: ab-4676
izenburua: Chacolin
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004676.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004676.MID
youtube: null
---

Chacolin chacolin
chacoliñac on eguin
Maricho ariñcho da
Martincho. (bis)

Ase naiz na farrez
churi gorri ta beltzez
jarri naute miñez
gabe ere onic ez.