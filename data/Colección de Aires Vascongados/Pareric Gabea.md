---
id: ab-4696
izenburua: Pareric Gabea
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004696.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004696.MID
youtube: null
---

Belchera graziosa
Pareric gabea
Mundu gustiyoc dio
Serala nerea.
Munduba jaquiñ eta
zu es jaquitaa
Ondo eguiton dezu
Disimulazi.