---
id: ab-4669
izenburua: Amorez Eria
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004669.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004669.MID
youtube: null
---

Amorez eri nago
Aspaldi onetan,
Zure gatic maitea
Gau ta egun penetan:
Arqui nezaque poza
badaquit nic certan,
Sendatuco nitzaque
Zure besoetan.

Ez badezu sinisten
Esan dizudana,
Demboraz izango da
Lutuco eguna:
Ala ere egin bedi
Zedorrec nai dezuna
Ni illa gatic ere
Izazu osasuna.

Ala erabaquiric
Badaucat maitea
Ez dit pena ematen
Mundua uzteac;
Andiagoac dizquit
Urratzen esteac,
Bestec gozatu bear
Zure virtuteac.

Arentzat dulcia ta
Neretzat samiña,
Besteren escuetan
Nere atseguiña;
Beti aguertuco zait
Zure imagiña,
Beste munduan ere
Emango dit miña.

Ay! nere amantea
Nola da posible
Bici alnitequela
Zu amatu gabe;
Ori nere biotza
Eguin zaitez jabe,
Ni naizela medio
Ilco ez zerade.

Oh! zure mingaiñ ezti
Espain gorri fiñac
Itz batequin sendatzen
Dizquidatzun miñac;
Amodio onean
Badira ordañac
Entregatzen disquitzut
Biotz ta entrañac.

Gustoz artuco det nic
Zure favorea,
Izana gatic ere
Amore lorea:
Eduqui bear nazu
Barrenen gordea
Beguira ez zaitecen
Mudatu ordea.

Ez dezazula artu
Bildurric maitea,
Ecin uca nezaque
Nere suertea:
Zurequin lotzen nauen
Amore catea
Ecin ausi liteque
Ain dago fuertea.

Eguia esateco
Egon naiz quezquetan
Para ote zenduan
Biotza bestetan;
Zeloac utzi eta
Begui ichuetan,
Entregatzen naiz oso
Zure escuetan.

Aditzen det maitea
Nai didazula esan
Bacarric nai dezula
Zuc nerea izan;
Ala zaduzcat bada
Biotzeco guelan,
Ez du bestec lecuric
Izango seculan.

Ez degu ofenditzen
Ustez Jaungoicoa
Garbia degulaco
Guc amodioa;
Deseo deguna da
Bion unioa
Eliz ama santaren
Matrimonioa.

Biotz baten lecuan
Milla banituque
Gustiac zuretzaco
Izango liraque;
Baña millaren lecuan
Bat besteric ez det,
Artu zazu maitea
Bat au milla bider.

Naitasuna gueyegui
Artzea iñori
Cer gauza gaiztoa dan
Probatzen det ongui,
Atsecabez bete ta
Gau ta egun beti
Ezur uts biurturic
Arquitutzen naiz ni.

Penaric andiena
Da mundu onetan
Azqueneco laztana
Artzeco demboran;
Estutzen da biotza
Itza faltatzen da
Ecin aterarican
Barrendic campora.

Ez da mundu onetan
Neretzat gustoric
Baldin bici bear badet
Urruti zugandic;
Egunaz esnaturic
Gauaz ametsetan
Beti beti oi nago
Zu gatic penetan.

Nere maite polita
Zutzaz oroitzean
Odola pill pill dabil
Nere biotzean:
Campora irten nairic
Aleguin gucian
Miñ char au bearco zait
Sendatu iltzean.

Berotasun gozo bat
Zañetic zañera
Nere gorputz guztian
Maitea sentitzen da;
Gustoaren pasioz
Sartzen zait icara,
Itzican ere ecin
Asmatu dedala.

Eduqui dezazuque
Ongui sinistua
Ceruac naucala ni
Zuretzat artua;
Ez nazu icusico
Iñoiz mudatua,
Ala eguiten dizut
Gaur juramentua.

Adios nere maitea
Seculan betico,
Esconduco zerade
Ni natorreneco;
Maiteac erantzun dit
Pena andi batequin
Ez naiz ni ezconduco
Ez bada zurequin.