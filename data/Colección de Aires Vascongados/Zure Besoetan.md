---
id: ab-4720
izenburua: Zure Besoetan
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004720.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004720.MID
youtube: null
---

Amorez eri nago
Aspaldi onetan
zuregaitic maitea
gau ta egun penetan.
Arqui nezaque poza
Arqui nezaque poza
badaquit nic certan
Sendatuco nitzaque
Zure besoetan. (bis)

Biotz baten lecuan
Milla banituque
Guztiyac zuretzaco
Izango liraque;
Baña millaren lecuan
Bat besteric ez det,
Artu zazu maitea
Bat au milla bider.

Nere maite polita
Zutzaz oroitzean
Odola pill pill dabil
Nere biotzean:
Campora irten nairic
Aleguin gucian
Miñ char au bearco zait
Sendatu iltzean.

Berotasun gozo bat
Zañetic zañera
Nere gorputz guztian
Maitea sentitzen da;
Guztoaren pasioz
Sartzen zait icara,
Itzican ere ecin
Asmatu dedala.

Adios nere maitea
Seculan betico,
Esconduco zerade
Ni natorreneco;
Maiteac erantzun dit
Pena andibatequin
Ez naiz ni ezconduco
Ez bada zurequin.