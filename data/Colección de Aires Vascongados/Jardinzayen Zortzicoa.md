---
id: ab-4695
izenburua: Jardinzayen Zortzicoa
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004695.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004695.MID
youtube: null
---

Erderaren ondoren
Gueroc ipiñia
Degu baratzguilleac
Zortzico berria.
Euscaldunaren canta
Biotz pozgarria
Itz neurtu dantzaraco
Guztiz egoquia.