---
id: ab-4673
izenburua: Nere Consolagarria
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004673.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004673.MID
youtube: null
---

Zu ecuztera nijoanean
chacurrac zaunca eguitean
cembait alditan botatzen nion
isiltzeagatic oguia
nere consolagarria.