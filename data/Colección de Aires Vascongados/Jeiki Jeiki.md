---
id: ab-4719
izenburua: Jeiki Jeiki
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004719.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004719.MID
youtube: null
---

Jeiki Jeiki etchecuak
arguia da zabala
arguia da zabala.
Itchasotic mintzatzen da
zitharezco trompeta.
Bai eta ere icaratzen
Olandresen ibarra
Olandresen ibarra.