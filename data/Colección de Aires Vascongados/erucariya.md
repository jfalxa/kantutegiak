---
id: ab-4704
izenburua: Erucariya
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004704.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004704.MID
youtube: null
---

Aspaldien esta
gure echean
Oguiric icusi
Bañan gaur
chichi ta para
isango da
naiz qui.
Marto
Opillac eguiteco
arto ta Ogui teco
Aitac atzo
saldu suan
anega bat arto.
Gaur bay
jana ta edana
labirunlena
jango ote degu dana
labirunla. (bis.