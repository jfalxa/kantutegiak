---
id: ab-4685
izenburua: Artzai Canta
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004685.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004685.MID
youtube: null
---

Bigar eros dezagun
erregue opilla
Vezperatican gatoz
aguirando billa.
Ez guerade bacarrac
badabiltza milla
Atez ate cantari
alegre mutillac.

Dibertitua dabil
Gendea naiqueran
Atarian, echean,
Eta escalleran:
Aguirando galdezca
Erregue vezperan,
Bigar izan dezagun
Cer jan, ta cer eran.