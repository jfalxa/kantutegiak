---
id: ab-4661
izenburua: Terciyuen Etorrera
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004661.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004661.MID
youtube: null
---

Euscal errico gende
Gazte eta zarrac,
Eman zaizcagun gogoz
Jaunari esquerrac
Echera dira etorri
Gure seme ederrac,
Bucaturic osoro
Africaco guerrac.

Guraso ta anayac
¡Nola esplicatu!
Cer atseguintasuna
Gaur dutena artu.
Icusi arren ecin
Degu sinistatu,
Egun aundiagoric
Ez zaigu arguitu.

Lau illabete ez dira
Igaro oraindic,
Guerrara joan cirala
Ontziz Pasayatic.
Echean daucazquigu
Vueltan Africatic,
Guztioc guera arquitzen
Contentuz beteric.

Erropac urratuac,
Colorea beltza,
Lengo antzic batere
Iñorc ere ez dauca.
Africaco ondarrac
Die eguin mudanza,
Guipuzcoa zuritceco
Toqui charra ez da.

Tetuango ondarzan
Ez da sagardoric,
Jana bigaltcen zuten
Español lurretic.
Ardoa ere izaten zan
Merque ta ugari,
Bañan asco erateco
Ez oizan diruric.

Mutill octatican
Batzuec dira ill,
Colerac eta balac
Die calte eguin.
Biciric datocenac
Ez dute izango miñ,
Emen dira jarrico
Sano eta berdiñ.

Bein bacarric dute
Sutan parte artu,
Bañan biciro ongui
Ciraden portatu.
Euscaldunican ez du
Moroac garaitu,
Aurretican aguro
Cituzten bialdu.

D. Carlos de Latorre
Guztion maitea
Gudari andia cera
Virtutez betea.
Badaquizu ederqui
Aguintzen gendea,
Guretzat ez diteque
Arquitu obea.