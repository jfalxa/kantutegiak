---
id: ab-4721
izenburua: Erreguiña Maitea
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004721.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004721.MID
youtube: null
---

Erreguiña maitea
Bigarren Isabel
Salve Donostiarrac
Salve milla bider:
Salve zuri Cristina
Bere Ama andrea
Eta Salve Luisa
Aizpacho gaztea
Isabel bigarrena
Aingeru maitea
Erreguiña gurea
Berdiñic gabea.
Euscaldunaren doai
Atseguiñ betea
Eta Donostiarren
Zorion jabea.