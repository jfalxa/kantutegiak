---
id: ab-4654
izenburua: Amairu Puntucua
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004654.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004654.MID
youtube: null
---

Lagunac arturican
Gutizcotirrian
Nere billa embillen
Bart gau erdian
Erroncac bota tuaz
Prestu quero, an
Ez dit ongui ematen
Norc bere errian
Egunazco arguian
Eusquera garbian
Jocatu nainian
Bañan ez nau ifini
Yñon premian.