---
id: ab-4722
izenburua: Á Guetaria
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004722.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004722.MID
youtube: null
---

Oroitz bat zorniyoten
Guetariarrari
Viotz barrengo oroitza
leyaltadeari.
Batian pagatzen det
Erri maiteari maiteari
maiteari maiteari maiteari
ta nere semearen
lenagocoari.
Erri chiqui batentzat
Cer icen aundiya
Orain baño lenago
Dezu mereciya. (bis)

CORO:
Yñois ez da galduco
Zure memoriya
Mundu gucian dezu
Banatu gloriya
zuc gloriya zuc gloriya
zuc gloriya.