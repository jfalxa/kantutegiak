---
id: ab-4665
izenburua: Il Nai Eta Ecin Ill
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004665.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004665.MID
youtube: null
---

Milla ta zorci egun
zorcigarren urtian
Aprilleco illaren
Oguei garrenian
Berso bi paratzeco
Beta detanian
Aleguiña eguigo det
Gaurco egunian
Geñaleda nere ustes
Oraingo leguian.