---
id: ab-4653
izenburua: Contzeci
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004653.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004653.MID
youtube: null
---

Maite bat maitatzen det
maitagarria
Begui ederra du
ta guztiz arguia
daucat urruti
Bañan ecin quendu det
burutic arren ichura
saldu albaliteque pisura
urrearen truque
norc erosi faltaco ezluque
norc erosi faltaco ezluque.