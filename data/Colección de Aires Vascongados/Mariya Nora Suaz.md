---
id: ab-4709
izenburua: Mariya Nora Suaz
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004709.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004709.MID
youtube: null
---

Mariya nora suaz
ederr galant ori
Iturrira Bartolo
nai badezu etorri.
Iturrian cer dago?
ardocho churia
Biyoc erango degu
nai degu guztia.