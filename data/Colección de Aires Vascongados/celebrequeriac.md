---
id: ab-4672
izenburua: Celebrequeriac
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004672.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004672.MID
youtube: null
---

Cantatzera nijoa
Celebrequeriac
Damacho batec berac
beretzat jarriac.
Berri berriac
Ez dira desonestac
Bay farragarriac.

Oguei eta bat urte
Daucat edadea
Amar urte serbitcen
Nagon mirabea
Miserablea
Umazur inocenta
Jaberic gabea.

Orain aguertcen dira
Nondic nay jabeac
Catu zarraren guisa
Gauza on zaleac
Arranzaleac
Gazte inocentien
Egañatzaleac.

Nere gazte lagunac
Au gogoan artu
Garbitasun ederra
Bear da conserbatu
Consideratu
Murmuracioari
Bidea trabatu.