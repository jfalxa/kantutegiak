---
id: ab-4677
izenburua: Burni-Bideari Jarritaco Zortzicoa
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004677.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004677.MID
youtube: null
---

Arrituric dezagun
era bat aitortu,
guizonac gaur (orain) duela
gucia garaitu.

Cer eguin aldezaquen
Españolac beti
urez eta legorrez
¿Noiz ez da icusi?
¡Elcano ta Oquendo!
ez esan besteric
oyec cituen ondo
munduac miretsi.

Guerozco demboretan
icusi ta esan
guerran aimbat astean
cintzoa ote dan.
Gendea pozturic da
aguertzen batean
Ur-mea deitua dan
ibai bazterrean.

Aberatsac, pobreac,
an dira arquitzen
Guiputza dutelaco
gucia icusten.
Gañera asitzen dira
apaizac cantatzen
ala galdetzen dute
¿cer zaigu guertatzen?

Donostia zuretzat
mentura onecoac
dira soñu ta canta
osdun andicoac.
Burnizco bidea da
onela asitzen
zori onac ditugu
dudagabecoac.

Arrizco murru beltzac
zaitu oraiñ lotzen
¡ah! icusi bear degu
lurrera erortzen.
Ondasun aundientzat
lecu izan ez ta
laster arico cera
campora zabaltzen.

Bedeinca zazu Jauna
guizonaren obra
lenen zuri deituric
gaur asi duena,
len bait len bucaturic
ortic datorquigun
uguritasuna, ta
gueroc nai deguna.