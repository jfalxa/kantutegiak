---
id: ab-4664
izenburua: Zaldi Baten Bicitza
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004664.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004664.MID
youtube: null
---

Orra sei berso
cale garbitzalleari
ceñac bere icenez
dan José Mari
erreza lezazquique
iru avemari
indarra etortzeco
zaldi zuriyari
animali ori
urriqui zait neri
falta du ugari
egoteco guri
cartoyaquin eguiña
dala dirudi.