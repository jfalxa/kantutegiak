---
id: ab-4698
izenburua: San Sebastian
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004698.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004698.MID
youtube: null
---

Ardoac para gaitu cantari
bai eta dantzari
ez gaituc mutillac egarri
ez ori segurqui.
Gure condicioac
beti tabernaraco
ezquean gure fiñac
berdin bearco:
Ecin faltatu condicioac
guc onac ditugu,
goicean
Ioaten guera tabernara
eta arratsean
beranduan echera
guero ioateguera.
Ardoac para gaitu cantari
bai eta dantzari
ez gaituc mutillac egarri
ez ori segurqui.