---
id: ab-4710
izenburua: Nere Onguille Maiteari
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004710.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004710.MID
youtube: null
---

Ez daquit nola esquerrac eman
ah nere onguille maiteac
Nere lurrera bear nau eraman
gaur zuen borondateac.
Eguin dezute cer obra ona
Aberatz eta pobreac
Orra cumplitu aguintcen duana
Jesu Cristoren legueac. (bis)

Nere oneraco, erriac dira
Buenos Airesen alcartu,
Bide emanic, Correo Españolac
Noblezaz zayo seguitu;
Arritzeco da, bai, gaur erriac,
Nola diraden arguitu,
Mundu gustian liberal onac
Adisquideac baditu.

Biotz oneco daunac badira;
Lista dutenac edertzen;
Zori oneco nere zartcera
Ah! nola duten honratzen:
Munduan bada zorionican,
Andre onac du eguiten,
Guizonac izar oyetan
Cerua degu icusten.

Oh! zu Romero Gimenes jauna,
Nere adisquide maitea,
Zure biotz onac consolatu du
Nere anima tristea;
Eguiac dira nere ametsac,
Mendiatera juatea:
Agur Romero, auda neretzat
Gozamentu eta paquea.

Agur, zu ere Olaso jauna
Vizcaitar, prestu noblea,
Beti betico zure icena;
Biotzean det gordea;
Gaztetanic zait gusta biziqui
Mendiataco aicea,
Agur, banoa, naidet icusi
Euscal-erri maitea.