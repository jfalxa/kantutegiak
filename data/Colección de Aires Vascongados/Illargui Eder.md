---
id: ab-4666
izenburua: Illargui Eder
kantutegia: Colección De Aires Vascongados
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004666.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004666.MID
youtube: null
---

Ceru altuban illargui eder /
Yzarcho biren erdiyan / (bis)
Nere maitea icusten nuen
danzara zijoanean
Alacorican ez zuqueaten
lagun gucien artean.