---
id: ab-794
izenburua: Ezteietara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000794.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000794.MID
youtube: null
---

Ezteietara, norenetara,
Mari Juanarenetar.