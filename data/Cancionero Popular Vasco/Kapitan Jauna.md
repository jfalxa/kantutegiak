---
id: ab-1120
izenburua: Kapitan Jauna
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001120.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001120.MID
youtube: null
---

Kapitan jauna, erria belok
Mutrikun bistara artean,
Mutrikun bistara artean eta
barrura sartu artean.

Neure anaia ¿ze berri on da
Mutriku erri onean,
Mutriku erri onean eta
Ama Juanisen etxean.