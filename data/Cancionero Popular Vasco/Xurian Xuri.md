---
id: ab-180
izenburua: Xurian Xuri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000180.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000180.MID
youtube: null
---

Xurian xuri, gorrian gorri,
zoragarria zera zu.
Liparr batean in al badaike
bi erresomaren yabe,
hoyek guziak deus elirazke,
maite biotza, zu gabe;
maite biotza zu gabe.

Zure ikhustera hemen eldu niz,
nola ziraden maitea.
Bizitze on bat opa dereitzut
dohain orotaz bethea
eta ondoren, hiltzen zelarik,
Paradisuan sartzea,
Paradisuan sartzea.