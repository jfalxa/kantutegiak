---
id: ab-556
izenburua: Berterretxek Oheti
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000556.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000556.MID
youtube: null
---

Berterretxek oheti
neskatoari eztiki
ageri denez gizonik>
Neskatoak berhala,
ikhusi zian bezala,
hirur dozena bazabilzala
leiho batetik bestera.

Berterretxek leihoti
Jaun konteari goraintzi:
ehun behi bazereitzola
beren zezena ondoti.
Jaun konteak berhala,
traidore batek bezala:
Berterretxe: haigu bortala,
utzuliren hitz berhala.