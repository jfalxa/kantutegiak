---
id: ab-340
izenburua: Arta Kamara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000340.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000340.MID
youtube: null
---

Arta kamara motxoliñoa
Domingilloa lepoan,
onetxek yaiok pasau artean
eztot besterik gogoan.