---
id: ab-1086
izenburua: Asto Zarra Tudelatik (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001086.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001086.MID
youtube: null
---

Asto zarra Tudelatik
ardo gozoz kargaturik,
portu beltzean topatu nuan
otso gaiztoa bakarrik.