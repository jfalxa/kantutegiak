---
id: ab-728
izenburua: Dulun Dulun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000728.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000728.MID
youtube: null
---

Dulun dulun yoale beiak eldu dire,
doniango plazan iru atso dantzan,
batek bertzeari bere graziari
latxikon latxikon giri giri on.