---
id: ab-817
izenburua: José Mondragonek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000817.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000817.MID
youtube: null
---

Jose Mondragonek dauko
katua por gala,
geurean bere eztago ta, Monona,
ain bere ta argala.

Berrogeta amar erralde
zerbait gutxiago,
errotako pisuetan, Monona,
pisaturik dago.

Goiko sala barrian
emon notsan gatza,
artuko etedostan, Monona,
daukat bildur gatxa.

Irrietan arrauetan
zortzi egun onetan
gure andrak derautse, Monona,
koipeak urketan.

Iru sebo-opiltxo ditu
irurak andiak,
iruditen d'at niri, Monona,
errotako arriak.

Anaiari bialdu notsan
erregalu mortzilla,
ezpanak koipatuta, Monona,
an dabil mutilla.

Surmintz eta belarriak
ondo gordetako,
auzoko jenteentzat, Monona,
San Kristobaleko.

Ay si si. Ay no no
Markero liño Marketxuleta
Marketxo txuria no.