---
id: ab-243
izenburua: Rrau Rrau Rrau Rrau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000243.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000243.MID
youtube: null
---

Rrau rrau rrau rrau rraketa plau
artzain zaharra taferrnan,
orrdi gira, ez, ezkira,
dezagun basoak bira;
orrdi gira, ez, ezkira,
dezagun basoak bira.