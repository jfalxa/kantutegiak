---
id: ab-1179
izenburua: Urdiaingo Andra Maria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001179.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001179.MID
youtube: null
---

Urdiaingo Andra Maria guzitan señale,
brilijioak antxe gordetzen dirade
zazpi giltzen artean daude ala ere
aietatikan bida gaureak dirade
San Padre zeru altuko giltzaren yabea
goizean mezea ta arratsean saldea.