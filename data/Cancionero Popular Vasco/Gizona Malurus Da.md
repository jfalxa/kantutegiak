---
id: ab-570
izenburua: Gizona Malurus Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000570.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000570.MID
youtube: null
---

Gizona malurus da kampañan bizi dena,
egon bedi hirian urus izan nahi dena,
hango libertimenduak
ahantz arazten dituzte penarik handienak
han dire munduan diren gauzarik ederrenak
agradagarrienak agradagarrienak.