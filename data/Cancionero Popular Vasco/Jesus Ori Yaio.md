---
id: ab-1028
izenburua: Jesus Ori Yaio
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001028.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001028.MID
youtube: null
---

Jesus ori yaio eta Am.ori bakarrik,
sua ere egiteko etzaukan egurrik.

Jose, ekarri zazu sortaño bat egur,
aurra otzak ilen du, ni naiz agitz bildur.

Aur ori eztu otzak inoiz ere ilen,
aur orrek berez dauka dibinidadea.

Jose, ekarri zazu auzotik eznea,
aur oni eman bear diogu munduko legea.