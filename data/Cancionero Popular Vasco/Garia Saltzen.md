---
id: ab-129
izenburua: Garia Saltzen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000129.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000129.MID
youtube: null
---

Garia saltzen nengoelarik
Donostiako kalean
dama gazte bat etorri zitzaidan,
garia zenbana nukean
Besteendako amar ta erdian,
zuretzat ezkontzorrdean.
Ezkontzordean balin badezu,
yarri zaite ezkonbidea.