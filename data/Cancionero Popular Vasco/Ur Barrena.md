---
id: ab-1267
izenburua: Ur Barrena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001267.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001267.MID
youtube: null
---

Ur barrena ur goiena,
urte berri egun ona,
egun onaren señalea
emen dakargu ur berria.
Iriki zazu ataria,
presta dezazu gosaria.

Izotzetan eta lurretan
abenduko gau luzeetan,
gu gaixuok emen gabiltza
otzak iltzen oinutsetan.