---
id: ab-789
izenburua: Ene Senar Yaun Bizartsu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000789.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000789.MID
youtube: null
---

Ene yaun bizartsu,
bianda mintu yan ezazü,
ogi xuria labean dit,
hura nihaurek behar dizit.
Ene senar mokoriko,
suondoan kokorizko,
apostolüak yinendirela
belain beltzen askatzeko.