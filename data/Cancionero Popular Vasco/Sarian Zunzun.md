---
id: ab-749
izenburua: Sarian Zunzun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000749.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000749.MID
youtube: null
---

Sarian zun zun,
sarian zun zun zena,
launkirinkun launkirinkun
launkirinkun lena.