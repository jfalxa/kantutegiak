---
id: ab-112
izenburua: Begi  - Urdin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000112.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000112.MID
youtube: null
---

I
Begi-urdin, bilo-ori, zoragarria;
gorputzttipi, gerripolit, pare gabea;
elhurra bezain xuri da zure larrua,
arrosa bezain gorri begi bitarrtea:
zoraturik deraukazu mundu guzia
¿Ezotezait zilegi zuri erraitea?
II
¿Jinen niza, bai ala ez, zure ikustera?
- Bai yiten alzira hunat, ene maitea.
- zurekin nahi nuke ereman bizia,
nere izerdiz egin zuretzat ogia.
- Zato fite, leya zite, zu zaitut nere;
nihola ere ni ez nauke zu ikusi gabe.