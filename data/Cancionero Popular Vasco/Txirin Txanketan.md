---
id: ab-845
izenburua: Txirin Txanketan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000845.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000845.MID
youtube: null
---

Txirin txanketan Bilbora noa
txirin txanketan zer egiten.
Txoria txoritxo txilibitaria

Txirin txanketan makailautxoak
txirin txanketan ekarten.
Txoria txoritxo txilibitaria.

Aurten ezkondu gura dabenak
erein bearko dok garia.
Txoria, txoritxo txilibitaria.