---
id: ab-242
izenburua: Pozaldi Huntako
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000242.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000242.MID
youtube: null
---

Pozaldi huntako oyuki
ederren otseztiz alairik
ta fernan niagon gaurr ¡Oi!
Poz pozik girade gaur.
Gaua dut igaraiten,
arrnoak ni xoratzen:
kanpotik etxera,
ondoan andrea,
ohean motea,
latina golkoan,
frantzesez mintzatzen nau:
"ordia, galdua, alferra ¿nun ago gaur?
¡Ixo, ixo! Arnoak humela ezarri nau.