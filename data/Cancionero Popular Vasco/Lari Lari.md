---
id: ab-826
izenburua: Lari Lari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000826.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000826.MID
youtube: null
---

Lari lari lari lari biba sentalari,
arratsetan mozkor eta goizetan egarri.
Amatto: nik badizut andreño ederra iduri,
apairian behar baitu lau libera ogi.
Amatto: nik badizut etxeño bat egiño batean,
aizeak ezpeitu yoten eztabilanean.