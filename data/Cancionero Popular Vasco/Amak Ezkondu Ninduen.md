---
id: ab-94
izenburua: Amak Ezkondu Ninduen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000094.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000094.MID
youtube: null
---

Amak ezkondu ninduen
amabost urtekin,
senarrak bazituen
laurogei berakin:
Oi, Ai neskatila gazte
agure zarrakin, agure zarrakin.