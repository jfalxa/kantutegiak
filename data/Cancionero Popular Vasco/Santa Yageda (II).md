---
id: ab-1256
izenburua: Santa Yageda (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001256.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001256.MID
youtube: null
---

Santa Yageda Yageda
bihar da Santa Yageda
bihar da Santa Yageda eta
gaur aren bezpera gaba.

Libertatea eskatzen diot
etxeko printzipalari
Santa Yagedaren alabantzak
kantatutzera noa.

Grazia diot eskatzen
Santa bedeinkate oni
kristiandadean onen izena
beti bedeinkatu bedi.
Emoizu bada emoizu,
baldin emongo badozu.
Aize au bere otza dago
ta lagunok bere irritu.
Andra Santearen amorearren
limosna on bat eiguzu.