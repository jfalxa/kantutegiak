---
id: ab-262
izenburua: Aurra, Egizu Lo Ta Lo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000262.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000262.MID
youtube: null
---

Aurra, egizu lo ta lo!
main dizkizut bi kokoilo,
orai bat eta gerobestea,
arratsaldean txokolatea.