---
id: ab-278
izenburua: Dringilin Drangon
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000278.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000278.MID
youtube: null
---

Dringilin drangon eragiozu
laster aziko natxatzu,
alan bere ta lastertxuago,
lepoan banarabiltzu.
Dringilin drangoa bai,
enaz neu zoroa;
len bere makur makur
daukat nik lepoa.