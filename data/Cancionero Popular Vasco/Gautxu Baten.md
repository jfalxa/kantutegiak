---
id: ab-130
izenburua: Gautxu Baten
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000130.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000130.MID
youtube: null
---

Gautxu baten abiatu nintzan
dama gazte bat bilatzen
Koxkox yo neban atea baina
ez osten bayetz esaten.
Alako baten leyo batetik
"eztost niri amak itziten"
Bioren gurarioa alan bada,
zatoz ona puzka baten.
Zure aita lotara yoanarte
ateondoan gengokez.
Atera yoan, aizea presko:
otzitu eingo gindekez.
Nire kapeak magala zabal,
biok tapauko gendukez.
Andik tira eta ortik tira
urratu eingo genduke.
Atan-orretan gengokezala,
Aita zurea letorrke.
Ak bat esan ta guk bi eranzun,
okasinoa legoke.
Nire ateak otsa dau zoli,
txakurrak bere salatu.
Nire txakurra isilduteko,
eperr bizia biozu.
Eperr bizia koyiduteko,
mutil mantsoa zagoz zu.
Neska txiki ta graziosea,
atxakiosa zagoz zu.
Atxakiosa zagoz zu baina
neuk eroango zaitut zu.
Zubi txikitxu bat igaro eta
da nire etxera-bidea.
Gurari ona dabenentzako
eztago bide luzea.
Gurari onik zuk ezpadozu,
agur ¡ai! neure maitea.