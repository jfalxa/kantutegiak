---
id: ab-615
izenburua: Zorabiatua Naiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000615.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000615.MID
youtube: null
---

Zorabiatua naiz
munduko bizitzan
kantuz azalduko dut
zer moduz gabilzan.
Andre y eder batekin
esposatu nintzan
egun on bat arekin
eztezaket izan
nere begietarat
ona agertu zan.