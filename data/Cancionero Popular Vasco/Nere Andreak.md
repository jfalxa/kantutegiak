---
id: ab-234
izenburua: Nere Andreak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000234.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000234.MID
youtube: null
---

Nere andreak alperra data
egina dauka (y)apoztu:
zeinek lenago, katilu gabe
bon bil aunditzar bat ustu.
zeinek lenago, katilu gabe
bon bil aunditzar bat ustu.

Barrua bero da du kan ean
senar bat entzat bi ditu;
yaikialdian eztu yakiten
zeini eskua luzatu.
yaiki aldian eztu yakiten
zeini eskua luzatu.

Makila batez lagundu bear
zayon istilu ayetan,
makila dakus gora ta bera
bi senaren eskuetan.
makila dakus gora ta bera
bi senarén eskuetan.

La ra la la la la ra la la la
la ra la la la la ra la la la
la ra la la ra la la ra la la la
la ra la ra la la la
la ra la la ra la la ra la la la
la ra la ra l.