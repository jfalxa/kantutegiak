---
id: ab-897
izenburua: Kalesantak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000897.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000897.MID
youtube: null
---

Kalesantak Larraineko
leriun lero leriun letan bego,
Santa Graziko xara kotarrak
lerium lero leliruletan bego,
Xoro gorriak Ligiko leriun lero
leliruletan bego,
Xokorrak Athereiko leriun lero
leliruletan bego.