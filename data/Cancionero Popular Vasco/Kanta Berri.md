---
id: ab-149
izenburua: Kanta Berri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000149.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000149.MID
youtube: null
---

Kanta berri ederrik badakit nik,
maitea, zure ta enetako ezarririk.
Kanta horiek zeren enetzat?
ni ez aipha mazte gaitzat
ez peiteraitzut hitz eman
gaur daino behintzat.

Itsu alai zaharxkot bat txirulari
bihotzen boztariotzat zauku ari.
Agure hordi arraila,
txirula ukhak isila
ezkonminez bizi denak
bertze bat bilha.