---
id: ab-861
izenburua: Amak Dio Alabari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000861.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000861.MID
youtube: null
---

Amak dio alabari
alakanzila txarra hi.
¿Hik behar duna ibili
aitonen seme hoieki?
Ibeltekoz ibili hadi
hihaur uduri zonbeiteki.