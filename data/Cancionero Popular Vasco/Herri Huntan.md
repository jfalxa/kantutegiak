---
id: ab-123
izenburua: Herri Huntan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000123.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000123.MID
youtube: null
---

Herri huntan badire bi mutil gazte adiskide,
begi beltx eder bat badute beren bihotzekoa,
hura ezin aski ikhusiz tristerik dirade gaixoak.

Gure maite ¿nun zira? Yalki zaite leihora;
hemen bi lagun bagira yinak zure ikhustera,
emaguzu edatera izanen gira alegera.