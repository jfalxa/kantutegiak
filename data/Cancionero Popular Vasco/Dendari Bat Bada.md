---
id: ab-956
izenburua: Dendari Bat Bada
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000956.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000956.MID
youtube: null
---

Dendari bat bada gure auzoan,
irabazten baitu ofizioan;
bere orratzez,
bere ariez
irabazten du;
badu diru
boltsa badu,
aberats da;
senar on bat baizik
orrek eztu falta.