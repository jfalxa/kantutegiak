---
id: ab-735
izenburua: Yoan Nintzan Merkatura
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000735.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000735.MID
youtube: null
---

Yoan nintzan merkatura
erosi nuen txerriñoa
txerriñoa kurris kurris kurris,
ni sakelean dirurik gabe
eta deusen yabe.

Yoan nintzan merkatura
erosi nuen arrazkoa,
arrazko orrek ttun ttun ttun,
txerriñoak kurris kurris kurris,
ni sakelean dirudik gabe
eta deusen yabe.

Yoan nintzan merkatura
erosi nuen txulubitatxua,
txulubitak txiruliruliru,
arrazko orrek ttun ttun ttun,
txerriñoak kurris kurris kurris,
ni sakelean dirurik gabe
eta deusen yabe.