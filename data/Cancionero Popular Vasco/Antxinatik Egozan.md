---
id: ab-1082
izenburua: Antxinatik Egozan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001082.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001082.MID
youtube: null
---

Antxinatik egozan esaten
Profeta santuak
jaioko zala seintxo eder bat,
zein etzan munduan;
berez ezin eukiko ebala
ain leku zabalik,
ezta zer yantzi, zer yan ez edan
ondo datorkanik.

Jeremiasek dirautso notarrak (??)
zelan Isaiasek,
Belenen jaioko dala
seina Mikeasek.
Zakariasen Siongo alabak
deadarrez dagoz,
pozik zabaldu eizuz begiak
sein au ikusteko.

Seintxoa, Jose eta Maria
irurak bakarrik
tellape baztar baten egozan
gauzonik bagarik:
seina biloizik, zer yantzi ezta
otzez da bisutsez (??)
ezin geldirik egon ta aidean
darabilz irurek (sic).

Au entzun dabenean, zelan dan
Txomin dendaria,
arturik zorroztuta aldean
bere artaziak,
etxera yoan eta dirautso
beko Patxikari:
ebagi bear neukez oialtxo
kana bat edo bi.

Txominek yosten deutso Seinari
yazteko osoa,
burutik beatzera zelanbait
estaldutekoa.
Baldasar errotakok emongo
dau uruna pozik,
bai ta bere Josepek baetxo bat
askoz pozagorik.

Artzaintxoak datoz saltoka pozik
ezneagaz
aia... egitearren
ogi-urunagaz.