---
id: ab-233
izenburua: Mertxikaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000233.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000233.MID
youtube: null
---

Mertxikaren lorearen ederra!
Barrenean du ezurra gogorra.
Nik eztut maiterik besterik alerik,
arnoak naduka ni liluraturik.
Ni iltzen naizenean
ez egin nigarrik,
ez artudolorik;
bi bonbilkada ardo eman burruketan,
edanditzadan gogoz
biak zeruetan.

Txoriño bat mertxika ederr orretan
txintaz ari da umeak pozketan.
Nik aldiz txintarik eztut nik alerik,
arno gorriak txoilik txintarazten dit.
Ni iltzen naizenean
zagiak osorik,
tabernak ilunik
izango dirade nere oroi penetan,
ni zoriz nagolarik
goiko mastietan.