---
id: ab-305
izenburua: Agur Estebe (1)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000305.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000305.MID
youtube: null
---

Agur Estebe eizari trebe
¿nola asi hiz sarez uso eizan
Bihotzak, naiz hotzak,
eztu nahi bakhar izan;
bertze bat sartu zat
ene hunen gerizan.

Eztei eguna, egun leguna,
egaz gautu oi dakiguna
Goiargi hitsari,
hurruntzeko iluna,
ots egin dezagun
yin dakigula hunat.