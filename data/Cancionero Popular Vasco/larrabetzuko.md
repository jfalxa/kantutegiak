---
id: ab-907
izenburua: Larrabetzuko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000907.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000907.MID
youtube: null
---

Larrabetzuko sastre agurazar ori
len ona zoan baia galdu dok oin ori.

Zabala ta Fernando erri onekoak
zerbait egin ginduzen gure Jaungoikoak.