---
id: ab-150
izenburua: Kantoreñoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000150.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000150.MID
youtube: null
---

Kantoreñoak gure herrian
amodioaren gainean
berri berriak emanak dire
Bazko biharamunean.
Urtzo kolonba xuri polit bat
neraukan sare barnean;
ene sareak ur txorik gabe
miatu nintuenean
harritu nintzan joan ote zenez
hegalik gabe airean,
hegalik gabe airean.