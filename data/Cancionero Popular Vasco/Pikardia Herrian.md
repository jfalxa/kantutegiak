---
id: ab-1159
izenburua: Pikardia Herrian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001159.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001159.MID
youtube: null
---

Pikardia herrian
gizon handi bat zen
eta haren izena
Jean de Calais zen.
Jinkoak egün hün daiziela,
kunpaña uhuratia;
eta eman dizaiziela bere
beneditzione handia.