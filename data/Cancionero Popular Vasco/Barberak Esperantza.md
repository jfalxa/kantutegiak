---
id: ab-872
izenburua: Barberak Esperantza
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000872.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000872.MID
youtube: null
---

Barberak esperantza neri egun batez,
sendatuko nintzala ile bete batez;
medikua etorri eta arrek berriz bat ere ez,
udan eztakit baiño otz ebekin da nekez.

Nere zangoko mina dena malizia
pasmo batek artu dit zango-sagar guzia;
kurizek sartu eta kuarta erdi bat barna
ala ere etzen allegatzen aragi biziengana.

Izebak saietsetik "ai Jesus ¡au lana!"
yoan bearko degula barberarengana.
Nik eztiot barberari deusik ere kentzen
ezeta ere etzaiola deusik entendatzen.