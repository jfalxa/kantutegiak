---
id: ab-1020
izenburua: Jaungoikoak Salba
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001020.MID
youtube: null
---

Jaungoikoak salba zaizala
Erregiña Maria,
miserikordiazko Amaren
guztiz da miragarria,
esperantza osoarekin
nago zugana jarria.

Jaungoikoak salba zaitzala
noa ni berriz esaten,
zure anparoaren premian
bati gerade gertatzen.
Zu bezelako amrik iñor
eztei bestetan arkitzen.

Ea bada gure Señora
bitartekoa zera zu,
miserikordiazko begiak
gugana itzul itzatzu,
etsaiaren tentaziotik
arren defendi gaitzatzu.

O clementisima deitzen dizu,
Ama guztizko garbia,
o piadosa zein andia dan
zure miserikordia.
Denen artetik zugana nator
o dulce Virgen Maria.

Ama Andrea, zuk badakizu
gu nola bizi geraden;
tentazio eta peligroa
besterik eztago emen;
zere konpaiñian ar gaitzatzu
zeruko glorian. Amen.