---
id: ab-1015
izenburua: Geure Jesus
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001015.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001015.MID
youtube: null
---

Geure Jesus biotzekoa,
Jaqungoikoaren semea.
Geure Jesus biotzekoa,
Jaungoikoaren semea.