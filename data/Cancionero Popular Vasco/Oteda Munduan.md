---
id: ab-604
izenburua: Oteda Munduan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000604.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000604.MID
youtube: null
---

Oteda munduan gizonik
ni bezen malurus denik?
Egunak yoanezgeroztik,
maitea zuganik aditurik
eznai zela yinen gaurdanik,
xerka dezadala bertzerik.