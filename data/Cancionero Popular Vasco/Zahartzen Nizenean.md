---
id: ab-612
izenburua: Zahartzen Nizenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000612.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000612.MID
youtube: null
---

Zahartzen nizenean hirurogei urte,
emaztea orduan balitz on laiteke.
Estofa balin bada, salda egin lezake;
deusik ezpalin ba da, esker txarra merke.