---
id: ab-1264
izenburua: Txiri Txiri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001264.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001264.MID
youtube: null
---

Txiri txiri txanponeko,
urte berri eguneko
nik dakarrat ur berria,
etxeko andre giltzaria:
idiki zazu ataria.