---
id: ab-282
izenburua: Lo Lo Lo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000282.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000282.MID
youtube: null
---

Lo lo lo aingerua ¿zeruan ze barri?
Lo lo lo aingerua ¿zeruan ze barri?
zeruan barri onak orain eta beti,
zeruan barri onak orain eta beti

- Goiz ta gau ¿zelan bizi da an goiko Erria?
- An ezta goiz ez gaurik, beti da eguerdia.

- Eguerdi ain luzean ¿zer dozu egiten?
- Aingeru barrientzat egoak eioten.

- Aingeru barri orrek ¿nun sortu oi datzuz?
- Lurreko seasketan begiz yoten doguz.