---
id: ab-1110
izenburua: Gure Ama Birjina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001110.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001110.MID
youtube: null
---

Gure Ama Birjina
Arantzazukoari
para bear litzake
bertso bat edo bi,
grazia eman balekit
artarako neri,
aditzera emateko
mundu guztiari
nola presentatu dan
gugana berori.

Emakume eder bat
arkumea besoan,
kanpanilla txiki bat
alboan bazuan,
iñork beartu gabe
soñu jotzen zuan,
belauniko jarrita
adorzten zuan:
Arantzazu izena
andik para zuan.

Pausu santuak ongi kontatzeko
mundu onetan lenena
Ama Birjiña izan zen
ilezkeroztik seme ona.
Dudarik gabe, egia dela
nai duzu klaridadea:
Ana Maria (1) Agredakoak
emanen dizu fedea.