---
id: ab-1208
izenburua: Ea Bada, Josepe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001208.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001208.MID
youtube: null
---

Ea bada, Josepe,
arotza zera zu,
aurtxo txiki onentzat
seaska egizu.
Ea Maria Jose,
Jose Maria,
era Maria Jose.