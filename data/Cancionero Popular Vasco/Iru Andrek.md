---
id: ab-225
izenburua: Iru Andrek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000225.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000225.MID
youtube: null
---

Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
iru uso, bi tortoilo, eper bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
lau antzara iru uso, bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
bost idi lau antzara, iru uso, bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
sei txerri bost idi,
lau antzara, iru uso,
bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
zazpi karrga txardin berri
sei txerri, bost idi,
lau antzara, iru uso,
bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
zortzi karrga ogi erre
zazpi karrga, txardin berri,
sei txerri, bost idi,
lau antzara, iru uso,
bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
bederatzi karrga arrno zuri
zortzi karrga ogi erre,
zazpi karrga, txardin berri,
sei txerri, bost idi,
lau antzara, iru uso,
bi tortoilo, eperr bat:
etziran goseak etzin bart.
Iru andrek lau emainek
¡¡ai zer aparia zuten bart!!
amar idi adarr aundi
bederatzi karrga arrno zuri
zortzi karrga ogi erre,
zazpi karrga, txardin berri,
sei txerri, bost idi,
lau antzara, iru uso,
bi tortoilo, eperr bat:
etziran goseak etzin bart.