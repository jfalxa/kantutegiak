---
id: ab-582
izenburua: Izarretan Den Ederrena Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000582.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000582.MID
youtube: null
---

Izarretan den ederrena da
¡oi! Artizarra goizetan.
Mutil gaztea, edeik atea
lotsarik neskatiletan.
Amodioa goan zait eta
bizi naiz lazki penetan.