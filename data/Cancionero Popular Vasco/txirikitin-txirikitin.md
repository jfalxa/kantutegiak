---
id: ab-301
izenburua: Txirikitin-Txirikitin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000301.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000301.MID
youtube: null
---

Txirikikin txirikikin seintxo bat dago,
txirikikin txirikikin Otzandion,
sein ederr orrek izentzat dauko,
Amak emonda, Manu Anton;
sein eder orrek izentzat dauko,
Amak emonda, Manu Anton.

Txirikitin txirikitin, ene Manuko,
txirikitin txirikitin, lo egik lo.

Txirikikin txirikikin otsotzar batek
txirikikin txirikikin Otzandion
seaskan lo lo eukan seintxoa
amari ostu eutsan atzo:
txirikikin txirikikin ardura barik
txirikikin txirikikin eik, Manu, lo.

Txirikitin txirikitin otsotzar orrek
txirikitin txirikitin eteaduko
ondorengorik ume-yalerik
ezpelduietan Otzandion?
Txirikitin txirikitin bizi nazala
ezau, Manu, otsotzarrek eroango.
Manu Antontxo, lo egik lo.