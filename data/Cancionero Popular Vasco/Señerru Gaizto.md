---
id: ab-1166
izenburua: Señerru Gaizto
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001166.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001166.MID
youtube: null
---

Señerru gaizto denemanesok
¿beti itxasorako deia?
¡Lau sagar gordin ta ogi puzka bat
egun guztiko yakia!
Amarretako ekarriko na
eutzako ardao zuria.