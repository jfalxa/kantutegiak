---
id: ab-1018
izenburua: Gure Jesus
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001018.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001018.MID
youtube: null
---

Gure Jesus ¿nora zoaz
ain triste gugatik?
Kurutzean il nai duzu
pena latzez beterik?
Damuz nago, urriki dut
zaudelakoz negatik.