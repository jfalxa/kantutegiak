---
id: ab-198
izenburua: Arno Hun Hun Huntarik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000198.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000198.MID
youtube: null
---

Arno hun hun huntarik dugun edan alegerarik
hordi beldurragatik edan gogotik,
hordi beldurragatik edan gogotik.
Mintzaldi erranez eta zerbait yanez,
edariz beirea betez atseginik bonbila hustez
noiz helduren gituzun bertze aldi batez,
noiz helduren gituzun ber tze aldi batez.