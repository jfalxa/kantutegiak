---
id: ab-1065
izenburua: Zergatik Den Ain Aundia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001065.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001065.MID
youtube: null
---

Zergatik den ain andia,
San Jose, zure gozoa,
laudatzen dut Jaungoikoa
eta Birjina Maria.