---
id: ab-1255
izenburua: Santa Yageda (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001255.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001255.MID
youtube: null
---

Santa Yageda Yageda
bihar da Santa Yageda
bihar da Santa Yageda eta
gaur aren bezpera gaba.

Bere baimena eskatzen diot
etxeko yaunari
Santa Yagedaren ontasunak
erakustere noa ni.

.Zapata xuri paperez
euriak jota bat ere ez
errian damak badira baina
zeu bezelakorikan ez.

Ole ta ole etxekotxuak
jotzen ditut nik gaur ateak,
jotzen ditut nik ateak eta
konsola bediz jenteak.