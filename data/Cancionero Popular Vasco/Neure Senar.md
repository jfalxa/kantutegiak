---
id: ab-386
izenburua: Neure Senar
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000386.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000386.MID
youtube: null
---

l.- Neure senar tontozko,
artoyale aundizko.
Zazpi talo bear ditu
otorduan yateko:
tra la ra la la ra la la la la ra la la la la ra la la la
zortzigarrena kolko ko
gose orduan yateko.

- Neure senar tentene,
ez yaut maite bat ere.
I baino maiteago diat
auzoko auntza bedere:
tra la ra la la ra la la la la ra la la la la ra la la la
i baino maiteago diat
auzoko aun tza bedere.