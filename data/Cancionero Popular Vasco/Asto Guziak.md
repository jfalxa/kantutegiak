---
id: ab-776
izenburua: Asto Guziak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000776.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000776.MID
youtube: null
---

Asto guziak eztira
goizean ogean,
Purrutuneko mutila
yeikia denean.
Karrika guzia badauka
marumaz airean:
Amikusen sortua da
maiatzaren erdian.