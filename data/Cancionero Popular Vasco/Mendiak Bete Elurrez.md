---
id: ab-592
izenburua: Mendiak Bete Elurrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000592.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000592.MID
youtube: null
---

Abenduko gau luzeetan ardura hor ma bortetan,
mutiko gazte arinak ibiltzen neskatiletan:
asu egin bezate bere aldietan.