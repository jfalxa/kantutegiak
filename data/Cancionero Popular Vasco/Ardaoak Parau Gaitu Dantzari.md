---
id: ab-339
izenburua: Ardaoak Parau Gaitu Dantzari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000339.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000339.MID
youtube: null
---

Ardaoak parau gaitu dantzari,
bai eta koplari
ez gaituk mutillok egarri
ez orain segurki.

Neure kontsuelua beti
taberna rako
berdin dau gure finak
ara bearko.

Ezin faltadu,
kondizioak
onak ditugu:
goizean goizetikan
yoaten gara
tabernara
eta arratsean
beranduan etxera
gero yoaten gera.

Atzo ta egun
ta a rai ne gun ere bai
izan bear du
gure errian jai.

Betor ardoa eta
gagodezan alegere.

Erropa zarrak
eta eztarri garbiak
zorra pranko degu
baiña arturen gitxi.

Bart gure auzoan aserre,
bai batzuetan geu bere,
ez orain ere.
Gagodezan alege rementean
egongo geraden artean
leku batean pitxarra aldean
nordin bearko yok
gure finak ospitalean
aurrera San Sebastian.

Orain dan tza gaitean
ia goaz erdian aurrera
dantzan dabilenak
oinak yituk arinak,
ezpaditu oinak arinak,
etxok luzitzen
baiña onek poliki yok yantzatzen.

Ardu onen bentajak
ain dirade andiak ur otzaz
geure biotzak
ardorikan edan gabe
dago dez otzak.

Atzo goizean yoan nintzan
tabernara
edan nuan nik
txopin erdi bat.

Konpesa dezadan orairn egia
Ur otzak il zidan
neure egarria.

Ardoa ta kartak
ditut adiskideak,
eztot neuk
besterik maiterik
ez emakumerik
eztot adiskiderik,
ardoagaz nago
enamoraturik.

¡Oi! au pena ardorikan eza,
neure biotzeko pena guztia!
ur otzak ilten nau,
ardau onak biztu
ein dezu nai
dedan sortzetik:
ongi gerta dedila
guk gaur edan
dogun ardao guztia.