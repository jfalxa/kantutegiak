---
id: ab-6025
izenburua: Maria Jesus (Variante 3)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006025.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006025.MID
youtube: null
---

Gaur dala jaio, jaio,
gaur dala jaiotzea,
ai zer dontzella ederra
jaio dan infantea
Maria y Jesus,
Jesus Maria,
Maria y Jesus.