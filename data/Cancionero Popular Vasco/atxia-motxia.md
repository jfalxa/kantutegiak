---
id: ab-256
izenburua: Atxia-Motxia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000256.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000256.MID
youtube: null
---

Atxia motxia perolipan,
nire semea errotan;
errotara ninoiala
topa neban asto bat,
kendu neutsan beste bat,
ipini neutsan beste bat
errota txikinak klin klan.
Din dan nor il da?
Peru zapataria.

Atxia motxia perolipan,
nire semea errekan;
errekara ninoiala
topa neban errbi bat,
kendu neutsan begi bat,
ipini neutsan barri bat
erekta txikinak plist plast.