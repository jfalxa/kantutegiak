---
id: ab-1272
izenburua: Urte Barri Barri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001272.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001272.MID
youtube: null
---

Urte barri barri
txarri belarri,
dekonak eztakonari
nik eztekot eta niri.
Apalazio zalduna
iru erregen eguna,
zotzak eta paluak
txori biorren kontuak.
Emongo bozu emoizu
baldin emongo badozu,
zuri begira gagozan arte
egiten yaku berandu.
Apalazio miri montaña
iru intxaurta lau gaztaiña.