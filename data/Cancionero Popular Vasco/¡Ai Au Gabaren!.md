---
id: ab-1190
izenburua: ¡Ai Au Gabaren!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001190.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001190.MID
youtube: null
---

¡Ai au gabaren preziosoa!
Jesus yaio da Belenen.
Etxe onetan sartu otedan
bila gabiltza beronen.

Erdiok gara olagizonak
beste erdiok arotzak;
limosnatxo bat egitekotzat
bigundu bediz biotzak.

Ama gureak bere beiari
kaikua ezne bildurik,
epel-epelik, giro-girorik,
artu baleio gustorik.

Or goitik eta beraiñokoak
artzain zarraren saltoak,
bildots andia lepoan duela
altxa ezinik orpoak.

Belaunbikoka yoan ginaden
Nazaretikan Belena,
geuk adoratu bear gendula
Jesus Maisua lenena.