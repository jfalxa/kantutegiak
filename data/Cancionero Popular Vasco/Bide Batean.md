---
id: ab-997
izenburua: Bide Batean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000997.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000997.MID
youtube: null
---

Bide batean topatu nuen
Aingeru guardakoa,
nik hura zer zan enekian ta
largatu nion zijoan.

Nik berriz ere billatuko det
bide luzean banoa,
bide luzean banoa eta
bide onetik banoa (sic).

Ordu artantxe emango diot
anima gorputzarena,
bera dijoan lekura eta
Birjina Amaren etxera.