---
id: ab-791
izenburua: Ezkondu Nintzan-Orduan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000791.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000791.MID
youtube: null
---

Ezkondu nintzan orduan
fraka zuriak nituan,
axelburua pasa liteke
aien atzeko zuloan.

Frakak atzean zuloa,
yakea beso bakoa,
nigaitik zeinek eztau esango
arlote gizagaixoa.

Gure sala ta kamara
txerri tokia bezala,
txixilupeko ontziak berriz
txakurrak garbi ditzala.