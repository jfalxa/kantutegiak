---
id: ab-648
izenburua: Ots Agileko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000648.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000648.MID
youtube: null
---

l.- Ots agileko ilabetean
Basaburuko erri batean,
nobiarekin arreglatzera
sartu nintzan bezela atean,
aren etxean bela
iru gizon neure atzean
erne zebiltzen epereizean.

- Bata erriko alkate zela,
ark esan zidan preso nintzala,
emazte baten etxean ala
gizon bakarra zilegietzala,
yarrai nentzala
animatzeko Aitona zarra
¡etzidan eman alderdi txarra.