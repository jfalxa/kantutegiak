---
id: ab-834
izenburua: Neskatila Gazte
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000834.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000834.MID
youtube: null
---

Neskatila gazte gazte,
gazte ile horia, aixolik gabea:
eztiat beldurrik, ez eta era berik
neskatxak maitatuko ez nautenik.
Neskatila gazte gazte
gazte ile horia
aixolik gabea.