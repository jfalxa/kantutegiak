---
id: ab-589
izenburua: Maiatzeko Lehen Liliak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000589.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000589.MID
youtube: null
---

Maiatzeko lehen liliak eder du irudia.
Zoragarri horrek errazu ¡othoi! egia:
badudanez profeiturik zutaz pentzuturik
edo bertzainez oil nezazu ¡arren! bihotzetik.
Zureganako itxaropen hau galduz geroztik
hilarrietan xoko bat hauta beharra niz.