---
id: ab-6021
izenburua: Eguen Zuri (Variante 4)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006021.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006021.MID
youtube: null
---

Eguen zuri, eguen baltz,
txakurra arrautza
ganean datz,
zuri zuritxuok
geuetzako dira,
enparaduak maisuentzat.
On dau saltseak espezia
Bartzelonatik ekarria.
Nagosi jauna gura genduke
kopla onetako lizentzia.
Ateok dira leizarrez,
ateburuok zidarrez.
Etxeonetako etxeko jauna
urregorrizko bizarrez.
Urregorrizko bizarra dauko,
urrezurizko bizkarra.