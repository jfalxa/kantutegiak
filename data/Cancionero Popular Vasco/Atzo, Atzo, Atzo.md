---
id: ab-204
izenburua: Atzo, Atzo, Atzo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000204.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000204.MID
youtube: null
---

Atzo, atzo, atzo, atzo il ziran amarr atso;
baldin arrdoa merrkatzén ezpada,
ilko dira beste aski;
atzo, atzo, atzo, atzo il ziran amarr atso.