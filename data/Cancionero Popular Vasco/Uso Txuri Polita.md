---
id: ab-1181
izenburua: Uso Txuri Polita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001181.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001181.MID
youtube: null
---

Uso txuri polita
¿zeruan zer berri?
Zeruan berri onak
orain eta beti.