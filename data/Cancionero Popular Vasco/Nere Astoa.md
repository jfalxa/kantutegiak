---
id: ab-831
izenburua: Nere Astoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000831.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000831.MID
youtube: null
---

Nere astoa zartua,
Aitak etxean sartua,
mundu onetan sortua:
falta billatu ditio
albeitaruak ortan.
Niri eman didan lotsak
utsik egon bear boltsak.