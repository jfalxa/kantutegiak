---
id: ab-1145
izenburua: ¡Oi Elhürra!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001145.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001145.MID
youtube: null
---

Ekhiak (el Sol):
¡Oi elhürra, oi elhürra!
Banian bai beldurra,
fresko dük sudurra
tapatzen dük lurra,
jiten hiz negüan
hire sasbian (sic)
pausatzen lurrean.

Elhürrak (la nieve):
Anderea, anderea,
ez othoi hastea,
xuri dit kolorea
ene (y) esklat ederra
hüruntik ageri da,
gizentzen dit lürra (bis).

Ekhiak:
Hire esklata, hire esklata
bertan dük kanbiatzen,
ekhiak bahai unkhitzen
kolorea dük galtzen,
hire furia gordina
uretan yoaiten (bis).