---
id: ab-773
izenburua: Artoa Beroa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000773.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000773.MID
youtube: null
---

Artoa beroa gaztaiarekin,
munduak gero ori baleuko.
Martin Etxebarrialdekok
arto hotzik ere eztauko.