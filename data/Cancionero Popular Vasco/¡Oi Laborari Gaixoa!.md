---
id: ab-1146
izenburua: ¡Oi Laborari Gaixoa!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001146.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001146.MID
youtube: null
---

¡Oi laborari gaixoa!
Ihaurrek yaten artoa;
ogi eta arno jeñatzen dük
alferren bizitzekoa,
ihaur aldiz maite aie
nula artzainek otsoa.