---
id: ab-1069
izenburua: Agostuaren (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001069.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001069.MID
youtube: null
---

Agostuaren amaboskarren
Andra Maria-goizean
Birjinearen debotatxu bat
zijoian erromerian.

Oinak ortozik zapata bage
abitu baltza soinean,
San Frantziskoren kordoetxu bat
gerrian iru doblean,
San Agustinen liburutxu bat
eskutxu bien artean.

Birjinea ta Semea bere
topau nituan bidean:
konseju on bat emon leidela
Ama-Semeen artean.

On da umila izan nendila
munduan nintzan-artean;
enabela ez damu izango
mundurik niñoianean.