---
id: ab-1094
izenburua: Birjina Arantzazukoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001094.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001094.MID
youtube: null
---

Birjina Arantzazukoa:
Semea egin duzu
Semea egin duzu eta
Semea daukazu,
Semea prezioso garen
¡oh! Ama zera zu:
zeruarekin lurra
zaureak dituzu.

Goiek zaureak tuzularik
bulux gorrian zu,
tori nere mantua
estal egin zazu,
Jesussek nai eztuela beztimenturik
eta ere gutiago fantasiarik,
zeruetara fan bear dugu,
Birjina Maria.

- ¿Zertara zeruetara,
Birjina Maria?
- Ene Semea an dago eta
garen ikusitzera.
- Eztugu ezagutuko,
Birjina Maria.

Ezagutuko dugu, bai,
Madalenea;
gurutzea soinegian dauka,
gurutze berdea,
gurutze berdearekin
¡oi! Yake oria.

Orai guraxe badateke
¡oi! Nere Semea.
- Gontarik eztiakezut,
Birjina Maria;
gontan pasatu bear
dut pasionea,
pasionearekin eriotzea,
eriotzearekin mundu guzia:

Amen amen dela Jesus
guzien salbazalea,
zure izenean erranen dugu
gogo onez Ave Maria,
baita Pater noster ere
Jauna dakigun balia
eta bertze munduan gloria.