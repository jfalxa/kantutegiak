---
id: ab-120
izenburua: Emazteki Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000120.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000120.MID
youtube: null
---

Emazteki bat xoragarria
dendari gazte plazatik aren larruak
dizdiratzen du nola artizarrak
zerutik mutil gazterik
elike igaro loreño orren paretik.