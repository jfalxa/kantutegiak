---
id: ab-338
izenburua: Arrankin Trankin Trankun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000338.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000338.MID
youtube: null
---

Arrankin trankin trankun mailoaren otsa,
Arrankin trankin trankun mailoaren otsa:
orroko mutil orren ezkontzeko poza.
La la ra la la la ra la la la la la la
la la ra la la la ra la ra l.