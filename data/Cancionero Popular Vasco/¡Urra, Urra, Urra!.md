---
id: ab-1178
izenburua: ¡Urra, Urra, Urra!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001178.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001178.MID
youtube: null
---

¡Urra, urra, urra seme nerea!
Prantsez edo española egin dek andrea.

Prantsesa da baiña txit da noblea
txit da noblea eta aunitzen yabea.

(Ez dago)

Andikan yoan nintzan anaiarengana
anaiarengana eta zarrenarengana.

Anaie zarren orrek, am'orrek bezala,
etxera ekarri gabe deboila nezala.

Andikan yoan nintzan arrebarengana
arrebarengana eta zarrenarengana.

Arreba zarren orrek, am'orrek bezala,
etxera ekarri gabe deboila nezala.

Andikan yoan nintzan anaiarengana
anaiarengana eta gazteenarengana.

Anaie gazteen orrek, amak ez bezala,
etxera ekarri eta maitatu nezala.