---
id: ab-882
izenburua: Ezpeletan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000882.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000882.MID
youtube: null
---

Ezpeletan ezarri dute
xamal okerra auzapez,
karga hori zeren duen
nehork ere ezin onhets
xamal okerra auzapez
behin bainan berriz ez.