---
id: ab-1193
izenburua: Alimu ¡Arren! Ezazu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001193.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001193.MID
youtube: null
---

Alimu ¡arren! Ezazu;
alimu, Josepe,
geuronen Jesus onak
gaur yaio bear luke.
Ea Maria Jese, Jese Maria,
ea Maria Jose.
Au zan gabon, au zan Belen,
yaio da Jesus Nazaren,
Birjina Andreak bere semea
aurtxoa djantzan erabilen.