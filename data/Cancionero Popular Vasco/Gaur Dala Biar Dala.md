---
id: ab-732
izenburua: Gaur Dala Biar Dala
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000732.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000732.MID
youtube: null
---

Gaur dala biar dala doniane,
etzi san Juan biaramone,
gure soloan lapurrik ez,
badagoz bere errebeitez.