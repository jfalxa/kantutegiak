---
id: ab-849
izenburua: Zubietako Neskatxentzat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000849.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000849.MID
youtube: null
---

Zubietako neskatxentzat
yoana da fama,
berak omendabiltza
galleguengana.

Zubietako neskatxak
amabost urtetako
Amontegin dabiltza
gallegoekin bapo.

Gorputzera sendo ta
animara flako,
aurten gertatuko da
bekatua franko.

Txotx bat aski lukete
soldatu yaztea,
gerostik bilatuko du
Zubietan emaztea.