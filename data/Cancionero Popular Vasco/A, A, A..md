---
id: ab-184
izenburua: A, A, A.
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000184.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000184.MID
youtube: null
---

A-a-a, ardo gorri naparra
elikatura ona da,
edan al balin bada:
a-a-a: ardo gorri naparra.

E-e-e, ni ardoaren alde,
edan ardura gabe,
balin bada de balde:
e-e-e: ni ardoaren alde.

I-i-i, ardoak gaitu bizi,
noizik bein ongi busti,
edan gabe ez utzi:
i-i-i, ardoak gaitu bizi.

O-o-o: au biontzako dago,
edan dezagun oro
zuk orain ta nik gero:
o-o-o: au biontzako dago.

U-u-u: ardoak gaitu galdu,
geyegi edan degu
lurra irago digu:
u-u-u: ardoak gaitu galdu.

Be-be-be: beterikan basoa,
konsolatu egidazu
neure biotz gaixoa:
be-be-be: beterikan basoa.