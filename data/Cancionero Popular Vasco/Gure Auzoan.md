---
id: ab-632
izenburua: Gure Auzoan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000632.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000632.MID
youtube: null
---

Gure auzoan orok dugu yai,
bi neska-mutil ziren ezkongai;
Elizaltzi nean ginaden erpai,
ordu onean izan dezagun eztai.
Senar berria, apain yantzia,
aspaldi nintzen berantetsia;
maipuru ortan zaude yaria,
izan dezagun orok zure zoria.