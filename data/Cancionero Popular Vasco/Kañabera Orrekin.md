---
id: ab-1240
izenburua: Kañabera Orrekin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001240.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001240.MID
youtube: null
---

Kañabera orrekin zagozan gizona,
Jaungoikoak dizula zuri egun ona.
¿Aditu aldezu ixerren zaratarik
edo zeruetako aingeru kantarik?
Orain pare gabea Jesus gure maitea
gaur zeure yaiotzea zelebratutzera.
Aingeruak kanta, artzain onak dantzan,
guztiok alabantza zuri zor dizugu.
Astoak a, idiak mu, atseginik asko badegu.
Artu makilak eta jantzi abarkak,
artzaiak joan dirade Jesusen bila.
¡Ai ze alegria dan
dantzan jende guzia!
Salto brinko eginik
danak joan dira.