---
id: ab-607
izenburua: Partitu Ginen Herritik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000607.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000607.MID
youtube: null
---

Partitu ginen herri tik
gure paketak harturik,
soldadu legeak emanik.
Gure aita eta amer anitz dolu eginik,
begiak nigarrez bustirik
eta maitea utzirik,
adio triste erranik.