---
id: ab-1202
izenburua: Belerengo Urian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001202.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001202.MID
youtube: null
---

Belerengo urian gabon gabaren erdian
Birjina Maria zeruan kontsolaziño aundian.
Bart Belenen da bai Belenen
jaio da Jesus Nazaren.

¡Jesus dibino! ¡Gauaren otza! Jausten da linetazia,
ordu bi orretan horrela bada, il da ganadu guztia.
Bart Belenen da bai Belenen
jaio da Jesus Nazaren.

Korraletako ardiak bere dantzan zebizen orduan
salbadorea jaio ta geugaz bizi da munduan.
Bart Belenen da bai Belenen
jaio da Jesus Nazaren.

Nazareteko templuan dago dama eder bat yarririk,
izena bere Maria dau ta ondo graziaz beterik.
Bart Belenen da bai Belenen
jaio da Jesus Nazaren.