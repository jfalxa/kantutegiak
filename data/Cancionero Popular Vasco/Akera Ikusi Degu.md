---
id: ab-703
izenburua: Akera Ikusi Degu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000703.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000703.MID
youtube: null
---

Akerra ikusi degu baratzean yaten,
makila ikusi degu akerr ori yoten:
makilak akerra akerrak artoa
akerra Ren baratzetik
akerra ken, ken, ken, ken, ken.

Sua ikusi degu makila ori erreten,
ura ikusi degu su ori itzaltzen:
urak sua, sua makila...
Idia ikusi degu ur ori edaten, ura ikusi degu...
Sokea ikusi degu idi ori lotzen,...
Sagua ikusi degu soka ori etetzen,...
Katua ikusi degu sagu ori arrapatzen:
katuak sagua, saguak sokea,
sokeak idia, idiak ura, urak sua,
suak makila, makilak akerra,
akerrak artoa, akerra ken, baratzetik akerra ken: ken, ken, ken.
Atsoa ikusi degu katu orri yarraitzen, balbea ikusi degu atso ori
zemaitzen:
balbeak atsoa, atsoak katua, katua sagua...