---
id: ab-1074
izenburua: Aingeru Zuri Eder Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001074.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001074.MID
youtube: null
---

Aingeru zuri eder bat zapata gaberik
aldatza gora doa atseden gaberik.

Aldatza igaro ta zelai landa baten
Birjina ta Semea atseden zegoden.

- Birjina umildea ¿or zer darabiltzu?
- Jesusen bila nabil, ikusi badezu.

- Jesus erakuskuizut bai gorputz utsean,
gurutze berde andi(1) bat sorbalda-gaienan.

- Indazu, ene Semea, gurutze ori neri.
- Ezin eman leioke gurutze au inori.

Emen izan bear det nik gaur bai eriotzea,
salbatua izan dedin bai mundu tristea,
bai kristiandadea (2).