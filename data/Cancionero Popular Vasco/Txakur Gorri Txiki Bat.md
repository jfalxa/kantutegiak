---
id: ab-937
izenburua: Txakur Gorri Txiki Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000937.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000937.MID
youtube: null
---

Txakur gorri txiki bat
faltatu zait neri,
arras ezkutatu da ta
ezta inon ageri.
Eztiot maldeziorik
bota nai inori,
artan baliatu dana
ondo bizi bedi.

Manterolako errotan
gizon abilada,
nere txakur gorritxoa
ark berak ilada.
Bere orrekin eskean
or ibilia da,
zerbait bildu naia badu
ark familiara.