---
id: ab-138
izenburua: Igaran Ebiakoitzean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000138.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000138.MID
youtube: null
---

Igaran ebiakoitzean,
goizean goizik,
urera joan nintzan,
pegarra harturik:
larala la
urera joan nintzan,
pegarra harrturik.