---
id: ab-1012
izenburua: Espiritu Santuaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001012.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001012.MID
youtube: null
---

Espiritu Santuaren egunak dira
Pazkua Maiatzekoak.
Misioaren Santo Kristori
paratzen dazkiot bertsoak,
eztitut damu nik arengana
egin ditudan pausuak.

Misio oiek Santo Kristoa
badarabilte Elizan,
ezta pertsona au ikusita
ezpazerate (sic) arritzen.
Pensa dezagun gurutze artan
ea zergatik jarri zen.