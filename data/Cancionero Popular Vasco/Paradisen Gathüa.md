---
id: ab-1155
izenburua: Paradisen Gathüa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001155.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001155.MID
youtube: null
---

Paradisen gathüa
oren tristean sortüa.
Salazar harmatürik,
haren erhailea,
beste bi lagunekin
dobatan yalea (bis).

Gathüa zer marfundi,
zahar eta itsusi.
Badüzü zazpi urte
yaten züla xixari
barbalot xuxkandera
arrato berhilki
¿etzena greugarri.