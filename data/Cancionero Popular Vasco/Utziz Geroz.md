---
id: ab-1061
izenburua: Utziz Geroz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001061.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001061.MID
youtube: null
---

Utziz geroz bekhatua
bertze bat naiz ni yartzen,
ene barnea boztua
bet betan dut sentitzen.
Oi hau da alegrentzia!
Bakerik gozoenak
bethe daut bihotz guzia,
khendurik hango penak.