---
id: ab-186
izenburua: ¡Ai! Arrnoak Balaki
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000186.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000186.MID
youtube: null
---

¡Ai! Arrnoak balaki
zein maite dudan nik,
zein maite dudan nik,
yin laiteke peral tatik nigatik,
berak zangoak eginik.
Anitz arrdoz aseak,
ni berriz urutsik,
ni berriz urutsik.
Orra zer dion iturriño orrek:
iretzat eztik arrnorik.