---
id: ab-271
izenburua: Bonbolonxeta
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000271.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000271.MID
youtube: null
---

Bonbolonxeta bonbonxe,
loan sar zite aur ori;
nik emanen koko bi,
orai bat eta gero bi,
loan sar zite aur ori.