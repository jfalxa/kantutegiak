---
id: ab-254
izenburua: Aita Ttun-Ttun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000254.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000254.MID
youtube: null
---

Aita ttun-ttun,
Ama ttun-ttun,
alaba ttun ttun,
alabaren senarra
txerka zan ttun-ttun:
guziak iza nen zrei ttun-ttun.