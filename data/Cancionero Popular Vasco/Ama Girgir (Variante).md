---
id: ab-6035
izenburua: Ama Girgir (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006035.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006035.MID
youtube: null
---

Ama girgir ¿noren haurrak zirete?
Errepublikaren serbitzariak baikire
Zubitto huntan pasatu behar duzute.
Zubitto hori falsua dela diote, espazio gutixe.
Zatozte, zatozte, frogaturen duzute.