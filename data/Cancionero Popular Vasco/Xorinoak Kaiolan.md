---
id: ab-1167
izenburua: Xorinoak Kaiolan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001167.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001167.MID
youtube: null
---

Xoriñoak kaiolan
tristerik du kantatzen
duelarikan zer yan zer edan
kanpoa du desiratzen
zeren, zeren
libertatea zoin eder den.

Barda amets egin dut
ikusirik maitea,
ikusi eta ezin mintza, ezin mintza
¡oi hau penaren handia
eta tristea!
Desiratzen nuen hiltzea.

Xori banintz, bele banintz,
giristino bezala,
zure ganberan sar nindaite
saguño bat bezala,
neure pena-dolorien
zuri deklaratzera.