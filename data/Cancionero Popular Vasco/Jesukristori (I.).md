---
id: ab-1021
izenburua: Jesukristori (I.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001021.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001021.MID
youtube: null
---

Jesukristori kendu ezkero
pekatuakin bizitza,
baldin ezpadot negar egiten
arrizkoa dot biotza.
Guztiok lagun kantadu daigun
bere penazko eriotza.

Mundua baiño asko lenago
señalaturik eguna,
eldu izan zan obratarako
gizonaren osasuna,
argia bera alde batera
bestetik egun iluna.