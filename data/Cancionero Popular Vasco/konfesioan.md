---
id: ab-905
izenburua: Konfesioan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000905.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000905.MID
youtube: null
---

Konfesioan zer egin eta zer diran kondezioak
egingo ditut al dan moduan zerbait esplikazioak.
Gauza grabeak dirade eta iduki atentzioa,
salbatu naia daukat munduan onelako lezioak,
nere mingaina sutu dezala Jaunaren bendezioak.

Konfesatzeko Jaungoikoari eskatu bere grazia
eta urrena esaminatu norbere kontzientzia,
dolorez eta proposituaz eginaz dilijentzia
gero aitortu txiki ta andi sustrai ta zirkunstantzia,
barkazioa alkantzatuta konplitu penitentzia.