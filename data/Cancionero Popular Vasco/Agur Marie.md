---
id: ab-946
izenburua: Agur Marie
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000946.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000946.MID
youtube: null
---

Agur Marie,
ederra den arie,
graziaz betie,
dena orapiloz betie,
Jauna dago zurekin,
atorra ederra din berekin.