---
id: ab-187
izenburua: Arrnoak Balaki
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000187.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000187.MID
youtube: null
---

Arrnoak balaki
zein maite dudan nik,
zein maite dudan nik,
yin laiteke peral-tatik nigatik,
berak zangoak eginik.
Anitz arrnoz aseak,
ni berriz urutsik,
ni berriz urutsik.
Orra zer dion iturriño orrek:
iretzat eztik arrnorik.