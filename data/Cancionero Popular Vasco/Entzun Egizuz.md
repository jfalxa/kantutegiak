---
id: ab-1008
izenburua: Entzun Egizuz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001008.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001008.MID
youtube: null
---

Entzun egizuz, pekataria,
erima baten negarrak;
zeure burua salbatzerako
guztiz dirala bearrak.

Doloreminez zauden arima,
zer dezun esan eidazu;
zeure negarrez neure biotza
erdiratuten deustazu.