---
id: ab-862
izenburua: Amerikara Noa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000862.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000862.MID
youtube: null
---

Amerikara noa nere borondatez
aspertu naizelako emengo suertez;
onezkero yoan bear, nai badet eta ez:
emen gelditzen dianak ondo bizi bitez.

Len seme bat badet nik nunbait Amerikan
hogei urte badira yoan zala emendikan.
Ik ara bilatutzen badek biderikan,
esaiok Aita bizi dala oraindikan.