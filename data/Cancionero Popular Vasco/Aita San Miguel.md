---
id: ab-313
izenburua: Aita San Miguel
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000313.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000313.MID
youtube: null
---

l.- Aita San Miguel Idurretako
zeru altuko lorea.
Agura zarrak dantzan ikusi ta
¡¡ ak egiten dok barrea !!
Agura zarrak dantzan ikusi ta
¡¡ ak egiten dok barrea !!.

- Garaiko plazan otea lore
Ermuan asentzioa.
Dantzan ikasi gura dabena
Baruetara be yoa.
Dantzan ikasi gura dabena
Barruetara be yoa.