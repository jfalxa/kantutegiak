---
id: ab-1271
izenburua: Urriaren Iruan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001271.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001271.MID
youtube: null
---

Urriaren iruan
ginaden atera,
egualdi ona zalako,
gure biajera,
Santanderren barrena
azkar pasa gera,
bainan bazan zer egin
andik aurrera.
Gabon gabon pasa dezagun
Aita ta Amarenaldean,
ekusiko degu aita farrez,
ama ere bai txit kontentuz tuluntun.
Bai eta neuk ere
¡zelango tragua!
Lendanik esanez Jesus:
arriba y arriba, altzate la pata
goazen Espainiara
Frantzia lagata.