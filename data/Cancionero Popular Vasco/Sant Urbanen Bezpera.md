---
id: ab-932
izenburua: Sant Urbanen Bezpera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000932.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000932.MID
youtube: null
---

Sant Urbanen bezpera
hogei eta laugarren,
Maiatzeko ilean
uste det ala zen;
bi karga garikin
errotan nindagoen,
batre bildurrik gabe
Pettiri yaunaren.