---
id: ab-136
izenburua: Gure Baratzean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000136.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000136.MID
youtube: null
---

Gure baratzean lili bat loratu
ene bihotza dizu ¡oi! harrek xarmatu,
zurekin ezkontzeko desira nukezu:
izar xarmegarria konsola nezazu.

Izar xarmegarria, zira pare gabe;
badakit eznaizela zuretzak kapable;
bertze zonbait bezala aberats banintz ni ere,
gortearen egiteko dudarik eznuke.

Izar xarmegarria, ez izan herabe,
baldin amodiorik badazu bat ere;
ukanak ta gabeak jendetan dirade
ene bihotzak eztu zu baizikan maite.

Komentuko pareta ¡oy! Pareta fina
ofiziale hautaz dirudi egina;
leihoetan ere du ausarki burdina,
ene maitearenganat eznaite arrima.