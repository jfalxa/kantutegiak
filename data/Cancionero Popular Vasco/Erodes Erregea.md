---
id: ab-1219
izenburua: Erodes Erregea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001219.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001219.MID
youtube: null
---

Erodes Erregea,
falso traidorea,
ik ala damu yuen
kristoren yaiotzea:
Maria Jose, Jesus Maria.