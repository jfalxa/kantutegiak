---
id: ab-122
izenburua: Ene Maitea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000122.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000122.MID
youtube: null
---

Ene maitea, errazu
¿zerk hola iluntzen zaitu?
Ikhustera jinen nitzaitzu,
bertan adore har zazu;
amodioz nahi ezpadute,
bortxaz eginen gituzu.