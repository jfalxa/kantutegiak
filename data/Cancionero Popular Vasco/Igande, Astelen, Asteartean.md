---
id: ab-223
izenburua: Igande, Astelen, Asteartean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000223.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000223.MID
youtube: null
---

Igande astelen asteartean
bai eta asteazkenean
tabernan kausi bide nindaike
beti edo geienean.
Buruan atz eta etxerat laster,
dirurik eztudanean.

Eztei-gau ilunez bero bero
oilariteak yo ta gero
elemeleka noaielarik
lagunak utzi-ezkero,
nere buruari erraten dakot
"ezteia balitz gabero".