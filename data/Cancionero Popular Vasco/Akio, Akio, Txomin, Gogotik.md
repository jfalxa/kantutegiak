---
id: ab-315
izenburua: Akio, Akio, Txomin, Gogotik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000315.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000315.MID
youtube: null
---

l.- Akio, akio, Txomin gogotik
ateraz ateraz abarka orpotik:
txin txin demoniena,
orixen bera dek aingeruena.

- Txomin, buruori gogorra daukak
¿ ikasi aldituk aingeruen kantak ?
Txin txin demoniena
orixen bera dek aingeruena.