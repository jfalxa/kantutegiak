---
id: ab-269
izenburua: Bolon Bat Eta Bolon Bi (Var. I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000269.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000269.MID
youtube: null
---

Bolon bat eta bolon bi,
Bolon zubitik erori,
erori bazen, erori,
orduan etzen egarri.