---
id: ab-121
izenburua: Ene Maite
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000121.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000121.MID
youtube: null
---

Ene maite, bihotz trande, yeiki zaite;
ene atearen zabaltzera ez, othoi, ukhan herabe
Habil habil hebendik,
heben eztuk hiretik
ezta hire hundenik
zu yin bazinde ene etxera,
atherbe galde egitera,
ez nindukezu ez ausartzuri
hola mintzatzera.
Badiat nik non hartu,
bai eta ere ostatu,
enuk hire behartu
Baduzu, bai, zuk ostatu,
bai eta ere urgulu,
oro bat ni bezalako batek
enganaturen baitzutu

(1) Krreaturaño. (2) Xarmegarria. (3) Eliteke pasa. (4) Lore
xarmant. Barbarismos del original.