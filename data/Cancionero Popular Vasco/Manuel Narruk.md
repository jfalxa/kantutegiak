---
id: ab-916
izenburua: Manuel Narruk
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000916.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000916.MID
youtube: null
---

Manuel Narruk esan dit
bertso berriak jartzeko,
beor tratua nola egin duen
bere burua galtzeko,
Don Blas Querido empeñatu da
neri gezur hau sartzeko
ganadu txur au enkajatu dit
kotxeru bati saltzeko.

Azaroaren hogei ta bian
ontzako baten tratua,
zortzi urte eta ernari dala
egin dit juramentua;
hamasei ere bai omen ditu
gainera berriz antzua,
begi bakarra zala uste ta
suzeditu zait itsua.