---
id: ab-993
izenburua: Arantzazu-Aldean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000993.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000993.MID
youtube: null
---

Arantzazu-aldean izar bat da ageri,
izar orrek izentzat darakusku Mari
¿Izen gozoagorik, eztitsu samurragorik
entzun otedakioke aingeru-eliari?

Beste izen batek zoli an dagi durundi,
Jesus da izen ori. Adituz du zori,
Jesusek eman dio bere gozotasuna Amari (1)
goazen Arantzazura eta aguden adi.