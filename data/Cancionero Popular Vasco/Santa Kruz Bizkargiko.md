---
id: ab-1163
izenburua: Santa Kruz Bizkargiko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001163.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001163.MID
youtube: null
---

Santa kruz Bizkargiko,
kindarra lorea,
andixek gora dago
zerura bidea.