---
id: ab-408
izenburua: Zarrak Eta Gazteak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000408.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000408.MID
youtube: null
---

Zarrak eta gazteak guztiak batean
Jesus yaio da eta dantzatu gaitean
Alkar maitatzen bizi bagera pakean
aisa santuko gera zeruko atean.