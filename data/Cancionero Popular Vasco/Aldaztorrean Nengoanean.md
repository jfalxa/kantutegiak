---
id: ab-1075
izenburua: Aldaztorrean Nengoanean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001075.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001075.MID
youtube: null
---

La nuera:
Aldaztorrean nengoanean irra goruetan,
etorri datan erroi zarra grauetan.(bis)
Erroi zarra ¿Zer dakazu albiste?
El cuervo:
Ala markea galdu dala dinoe
Galdu naz bada alaba zori (1) bagea.
Antxe nebazan ogeta bat lengusu ta nebea,
arek baiño bearragoa aita neurea.
Arek guztiak baino azkarriago jaubea.
La suegra:
¿Zer diñona, urdanga(2) lotsa bagea?
¿azkanengo esan dona yaubea?
Esan bebanan lenengotatik yaubea,
izango ebanan aldaztorrean partea.
La nuera:
Nik neurea dot goian dagoan kaxea,
tapez taperaino diruz betea. (bis)
Imineagaz neban urre gorria,
anegeagaz neban urre zuria.
Mila dukat neban isil poltsea
¡a bere bazan alaba on baten dotea!
Aldaztorreak ateak ditu letuez,
ango plater pitxeruak zidarrez.
¿Yoan ala egon eingot, ama neurea?
La suegra:
Yoan, yoan, neure alaba maitea.
La nuera:
Aurtxo txikinak sabelean deust ostiko,
Yaun zerukoak al dau semea sortuko.
La suegra:
Nai dan seme nai dan alaba,
aldaztorrean partea izango dona.(bis.