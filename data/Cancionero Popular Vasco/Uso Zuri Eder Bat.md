---
id: ab-1182
izenburua: Uso Zuri Eder Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001182.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001182.MID
youtube: null
---

Uso zuri eder bat
oinetan ortozik (sic)
aldatzean gora doa
egoak zabalik.

Aldatzen goieneko
zelai eder baten
Birjinea topau neban
etseden egiten.

Kristoren Birjinea
¿or zer eiten dozu?
Kristo billetzen nabil
ikusi bedozu.

Kristo zeruan dago
Mezea ezaten,
Aingeru San Gabriel
Mezea erosten.

Zeruko aingeruak
orgañua yoten,
amabi apostoluak
Mezea entzuten.

¡Jesusen jardineko
lorearen argia!
Bera ta Santa Klara
dontzella garbia.

Amaren erraietan
zenduan grazia,
munduan ibilteko
ejemplu andia.