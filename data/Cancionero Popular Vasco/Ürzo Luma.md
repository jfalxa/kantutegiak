---
id: ab-181
izenburua: Ürzo Luma
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000181.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000181.MID
youtube: null
---

Ürzo luma arre gaixoa
hire txangotan bahoa;
batzen badük Mus de Sarrhi
jaun gose handidun hura;
begiez ikhusten balin bahai,
Phetirinalat bahoa.

Ürzo gaixoak aphalki
diozü mus de Sarhiri
egundano eztereiola
ogenik egin jaun hari,
ützi dezan igaraitera
oi duen bide bereti.