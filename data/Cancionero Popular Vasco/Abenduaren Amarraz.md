---
id: ab-853
izenburua: Abenduaren Amarraz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000853.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000853.MID
youtube: null
---

Abenduaren amarraz
oroitzen nauzu goiz arrats,
bide batean nindualarik
kargaturikan gabaz
izutu nintzan ni arras
eldu ote zen Barrabas.

Karga nuyen bi partetan
azukre eta kafetan,
berrogei ta amar libera ziran
bana bertze bietan,
guardiak zauden lorietan.
Cadet ¿zer duk orietan.