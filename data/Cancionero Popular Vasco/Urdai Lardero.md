---
id: ab-1270
izenburua: Urdai Lardero
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001270.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001270.MID
youtube: null
---

Urdai lardero, urdai lardero,
urdaia yan dagar baia gero,
piperrarekin espezia
Bartzelonatik etorria;
etxeko Jaunak emoten badosku
koplatuteko lizentzia.

Jauna doa zaldian, urre-silea azpian,
bedorren pozaz egon gerade yoan dan urte guztian;
atea dago lizarrez, ateburua zidarrez...

Etxeko andre-noblea, giltz andie yabea;
giltz andiak artu ez eze egizu egin peria (??).