---
id: ab-990
izenburua: Amabost Pausuz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000990.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000990.MID
youtube: null
---

Amabost pausuz konponitzen da
Galbarioko bidea,
aren neurriak artu dituenak
da Jesus gure maitea.

Pausu sainduak ongi kontatzen
mundu gonetan lenena
Ama Birjina asi izan zen
Ilez geroztik seme ona.

Dudarik gabe egia dela
¿Nai duzu klaridadea?
Ama Birjina Agredakoak
Emanen dizu fedea.

Alabatue izan dadiela
Kristoren eriotzea,
Bedinkatua izan dadilela
Aren Amaren sortzea.