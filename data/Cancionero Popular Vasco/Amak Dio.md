---
id: ab-860
izenburua: Amak Dio
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000860.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000860.MID
youtube: null
---

Amak dio alabari:
kita kita mutil hori.
Horrekin ezkontzekotan
eztun izanen doterik
ez eta ere linjarik,
aitak eta amak emanik.

Alabak amari arrapostu,
izpiritua pronto baitu,
enekin ezkontzen denak
badu linja eta diru
enetako eta beretako
eta ondoko aurrendako.