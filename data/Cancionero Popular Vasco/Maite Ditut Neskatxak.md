---
id: ab-155
izenburua: Maite Ditut Neskatxak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000155.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000155.MID
youtube: null
---

Maite ditut neskatxak,
neskatxa gazte politak,
eskuarte zerbait dutenak;
maite ditut segurki
bi hotzaren erditik
neure buru onengatik.