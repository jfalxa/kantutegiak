---
id: ab-295
izenburua: Santa Loa (Var.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000295.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000295.MID
youtube: null
---

Santa Loa, Santa Loa,
zeruetako Jaungoikoa:
geure aur oni emaiozu
iru lau orduko loa.