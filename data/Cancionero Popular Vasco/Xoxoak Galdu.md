---
id: ab-975
izenburua: Xoxoak Galdu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000975.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000975.MID
youtube: null
---

¡Xoxoak galdu!¡Nik hura banu!
Ederretan dut gogoa,
ederretan dut gogoa.
Aintzinean de ederrena da
Teresa lagundikoa,
Teresa lagundikoa.
Teresa orrek ederki
Fermin Indako egoki;
Teresa orrek eramanen du
ezkontze puska ederra:
etxea egin berria,
mandio txukun txuria,
beiak ernari,
bildotsa ugari,
alorra bigiz betea:
Oro duk, mutil, oro irea.