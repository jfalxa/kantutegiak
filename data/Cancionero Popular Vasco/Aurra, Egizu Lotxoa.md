---
id: ab-260
izenburua: Aurra, Egizu Lotxoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000260.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000260.MID
youtube: null
---

Aurra, egizu lotxoa,
emanen dizut goxoa.
Aitak bat eta Amak bi,
Jaun ze rukoak amabi.