---
id: ab-1220
izenburua: Erregeak Datoz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001220.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001220.MID
youtube: null
---

Erregeak datoz
presaz Belena,
guk bila dezagun
al dan lenena.
Ate txokoan oilar bi,
batak bestea dirudi;
etxe ontako etxekoandreak
Ama Birjina dirudi.