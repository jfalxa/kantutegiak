---
id: ab-1129
izenburua: Mutil Gazte
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001129.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001129.MID
youtube: null
---

Mutil gazte aise daudenek biotz trende dute.
Baba guti eskuetan karrayatzen dukete.
Neskatilen karesatzeko arrazoina badute,
baina zerbaitek halakoak ez nahiz uzten tuzte.