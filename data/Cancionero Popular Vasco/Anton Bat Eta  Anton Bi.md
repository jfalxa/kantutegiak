---
id: ab-710
izenburua: Anton Bat Eta  Anton Bi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000710.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000710.MID
youtube: null
---

Anton bat eta Anton bi,
Anton putzura erori,
edan zituen pitxar bi
etzan geiago egarri.