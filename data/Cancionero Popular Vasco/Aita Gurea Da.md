---
id: ab-985
izenburua: Aita Gurea Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000985.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000985.MID
youtube: null
---

Aita gurea da
lenbiziko itza,
lenbiziko itza.
Aita ona, lagun bezait,
baldin baderitza.
Ongi erresponditzen
zeruetan zaudena,
santifika dadiela
berorren izena.

Betor guregana
Zure erreinoa,
Ala esplikatu da
Jesus dibinoa.