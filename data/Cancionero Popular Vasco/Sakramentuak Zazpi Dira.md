---
id: ab-930
izenburua: Sakramentuak Zazpi Dira
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000930.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000930.MID
youtube: null
---

Sakramentuak zazpi dira
ta oraintxe noia kantatzen,
neure kristauak egon zakidaz
atentzinoiaz entzuten

Lendabiziko sakramentua
batiatzen garenean,
erejiatikm sartu oi gara
Jaungoikoaren legean.