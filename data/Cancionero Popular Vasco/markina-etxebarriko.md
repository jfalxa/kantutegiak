---
id: ab-383
izenburua: Markina-Etxebarriko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000383.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000383.MID
youtube: null
---

l.- Markina Etxebarriko senar emazte bik
a laba eder bat eukan zeruak emonik
eta amar mila dukat lurpean galdirik
ikusten ezebela egunaren argirik.

- Sartu zanean dama au amazortzi urtean,
aiztu bat topau eban etserik urrean;
au asidakan berbaz eraso gurarik
este euken ezkontzeko emonda berbarik.

- Barreka erantzun eutsan bere bizarretan
zelan ezeban ver ak igari kartetan;
alper aiztua izan igesten arina,
eldu dakan zankora txakurraren agina.