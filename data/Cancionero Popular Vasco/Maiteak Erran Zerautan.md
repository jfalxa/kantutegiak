---
id: ab-1123
izenburua: Maiteak Erran Zerautan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001123.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001123.MID
youtube: null
---

Maiteak erran zerautan
pollit nintzanez.
- Pollit pollit nintzala, bainan
larrua beltz, larrua beltz.

Maiteak erran zerautan
primu nintzanez.
- Primu, primu nintzala, bainan
etxerik ez, etxerik ez.

Maiteak erran zerautan
moltsa banuenez.
- Moltsa, moltsa banuela, bainan
dirurik ez, dirurik ez.

Maiteak erran zerautan
lanean nakianez.
- Lanean, lanean nakiala, bainan
gogorik ez, gogorik ez.