---
id: ab-160
izenburua: Mendirik Gorenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000160.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000160.MID
youtube: null
---

Mendirik gorenean
egiten du elhurra,
tronpatzen ninduzula
banuen beldurra,
ala ere nai zinduzket
ikusi ardura,
izanen badut ere
neretzat malurra.