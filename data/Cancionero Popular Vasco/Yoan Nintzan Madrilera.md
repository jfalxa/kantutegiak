---
id: ab-815
izenburua: Yoan Nintzan Madrilera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000815.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000815.MID
youtube: null
---

Joan nintzan Madrilera bai etorri bere, bai etorri bere,
ango neskatotxuak enabe maite enabe maite:
larala larala larala larala.

Ango neskatotxuak maite nabenean, maite nabenean,
praile sartuko naz ni komendu batean, komendu batean:
larala larala larala larala.

Kerizak eldu dira bai Kanalekoak, bai Kanalekoak;
trapu punta-luzeak bai Angelukoak, bai Angelukoak;
garagar-errikoak Natxitu-adekoak, Natxitu-aldekoak;
abarka manta-zale bai Izpazterkoak, bai Izpazterkoak;
Larala lara lalara lalara la.

Guztizko panparroiak Lekeitio-aldekoak (bis);
neska gona-gorriak Amoto-aldekoak (bis);
uraldeko berdetxak Gizaburukoak (bis);
dantzari ballesteak Allustiakoak (bis): Larala..., e.a.

Guztiz gerri-mardoak bai Nabarrizkoak (bis);
eskribau-errikoak Gernikaldekoak (bis);
eskuara-burukoak Arrietakoak (bis);
artobero-zaleak Emerandokoak (bis): Larala..., e.a.