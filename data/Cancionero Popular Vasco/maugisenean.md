---
id: ab-1126
izenburua: Maugisenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001126.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001126.MID
youtube: null
---

Maugisenean teatren gainean
bürduñaz kargaturik etsaien artean,
hargatik etzen lotsa ororen artean
zeren belhar hunak sakolan beitzutean,
sakolan beitzutean.

Heben bagire orai lau anaie,
heben baldin bagünü kosi Maugis ere,
ezkinaiteke lotsa, Charle Magne, zure
ez eta jinik ere Frantziako doze, doze pareak ere.