---
id: ab-1136
izenburua: Nik Badaukat Arkondarea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001136.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001136.MID
youtube: null
---

Nik badaukat arkondarea moso fina,
galtzerdia txilindrona,
zapatarekin kodona:
emazte gazteak emana.