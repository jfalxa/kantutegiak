---
id: ab-170
izenburua: Oyan-Zugatzik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000170.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000170.MID
youtube: null
---

Oyan-zugatzik ederrena da
aritza edo pagoa
¡¡nik maiteño bat bakarr izan ta,
harek bestetan gogoa!!
Yaun zerukoak eman dezola
nereganako asmoa.

Maitetasuna xoroa dela
nornahik ontsa badaki;
nik nuen maiteño bakarr hura,
¡¡beste batek eramanik!!
arrenkurarik enuke balitz
enekin baino hobeki.