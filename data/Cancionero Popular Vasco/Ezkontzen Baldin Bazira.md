---
id: ab-216
izenburua: Ezkontzen Baldin Bazira
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000216.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000216.MID
youtube: null
---

Ezkontzen baldin bazira
mandazainen batekin
maiz arrtuko dituzu
mandokarrotekin.
Lau lau lau, dotea badut baina
lau lau lau arrnoak galtzen nau.
Ezkontzen baldin bazira
errota zainarekin
maiz arrtuko dituzu
zorro makilekin.
Lau lau lau, dotea badut baina
lau lau lau arrnoak galtzen nau.