---
id: ab-803
izenburua: Gure Gelariak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000803.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000803.MID
youtube: null
---

Gure gelariak galde egin deraut
soinu gidariak zoin ginen.
Ahalde gabe moko pherdea
¿zertaz hiz hi enbrasatzen?
Herri guzia dun nahasten,
zaharrak eta gazteak oro
gitun dantzaturen,
arno hunik badugu edanen
eta hitazkasu guti egiten.

Bertsu berriak moldaturik
sujet ederrez eginik,
bai eta ere aspaldian
sujetak arribaturik,
atxiki tugu ixilik
nehon ere ez izanez
bertsu hoien huntzalerik.
Orai badugu bat hunik,
igorri deraizkot eginik.

Sujetak dire xarmagarri
bai eta irri-egingarri.
Eztenez hala dezagun galda
Aiherreko barberari,
harek badaki hobeki
zeren eta sujet hura
tratatu du zonbeit aldi,
hurbil beitziren elgarri,
marketarik du ageri.

Orai hasten niz daklaratzen
sujetak zer diren erraiten
bai eta ere neska gazte
orori etsemplu emaiten,
mutil gazten abertitzen
zeren eta uste gabez
anhitzak beitira tronpatzen,
orai bat hala gertatzen
hartaz gerade mintzatzen.

¿Eniza bada dolugarri
bai eta irri-egingarri?
Damua damu desohorea
Elizalat ezin yalgi.
Oro irriz daude niri,
zeren eta flakezian
erori beiniz handizki,
eta orain ezin yaiki
estekatua bortizki.