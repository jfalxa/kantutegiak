---
id: ab-189
izenburua: Ahizpa ¿Nahi Duna?
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000189.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000189.MID
youtube: null
---

Ahispa ¿nahi duna xerri bat arrdosari?
Ahispa ¿nahi duna xeri bat ardosari?
Eztun sortu bena sorturen dun sarri:
la kri kun ferima rirena
la kri kun ferirun

Gure zerria erdi, guk emerre tzü xerri;
¡Gure zerria errdi, guk e me retzü rerri;
hu rak handituta hogei urde handi:
la kri kun ferima rirena
la kri kun feriru.