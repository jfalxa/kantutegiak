---
id: ab-292
izenburua: Nere Maitea, Maite Polita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000292.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000292.MID
youtube: null
---

Nere maitea, maite polita, ez egin lorik basoan,
Azeritxoak yan etzagizan oiloa zeralangoan
Oiloa galdu dako auzoko Manuri
igartatzak baditu agertu baledi:
buztana mardo nardo, lepoa beilegi,
aren arrautzatxoak arila diru di
Binbilin bolon bolua urrak daroa zorroa:
garia bada betorge (y)eta artoa bada bixoa.