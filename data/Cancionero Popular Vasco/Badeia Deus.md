---
id: ab-548
izenburua: Badeia Deus
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000548.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000548.MID
youtube: null
---

Badeia deus mündüan
hain susprenigarririk,
hain miseria handirik,
Juif errant miserablea
den pora beti phenaturik,
bera dela kausa da
hola turmentatürik,
mündüan eztu pausurik.