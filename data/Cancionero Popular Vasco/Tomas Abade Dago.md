---
id: ab-1261
izenburua: Tomas Abade Dago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001261.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001261.MID
youtube: null
---

Tomas abade dago
Ermuan portalean,
kartak dituazela
partikara barruan.
Qué bonito ay qué bonito
de Niño Jesucristo.
Txirikilla maratilla
barri barritxuak dira
Qué bonito ay qué bonito
de Niño Jesucristo.

Batean galtzen dau ta
bestean irabazten,
irabazi ezkero
ondotxo dau edaten.
Qué bonito...etc.