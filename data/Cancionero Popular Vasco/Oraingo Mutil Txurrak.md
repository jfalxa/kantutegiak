---
id: ab-392
izenburua: Oraingo Mutil Txurrak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000392.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000392.MID
youtube: null
---

¿Oraingo mutil txurrak?
zer dute merexi?
Oraingo mutil txurrak?
zer dute merexi?
Urean beratuta
lexiban egosi.
La la la la la ra la ra la la
la la la la la ra la ra la
la la ra la la la ra la la la ra la la la ra la
la la ra la la la ra la la la ra la la la ra la

Eperrak kantatzen du
goizean intzean,
Eperrak kantatzen du
goizean intzean
eztaiteke fiatu
mutilen itzean
La la la la la ra la ra la la
la la la la la ra la ra la
la la ra la la la ra la la la ra la la la ra la
la la ra la la la ra la la la ra la la la ra la

Sagarra lore lore
denbora denean:
gazteak ankak arin
soñua denean.

Artajorrara noa
lur berri berrira;
belarra jorratuta
artoa sasira.

Egin dut eginala
(ezin dut gehiago)
asteaz artajorran:
nekaturik nago.