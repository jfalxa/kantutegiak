---
id: ab-784
izenburua: Belea Dago Aitzean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000784.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000784.MID
youtube: null
---

Belea dago aitzean
beretzat zori gaitzean:
balazo gaiztok
azertauko aldik
atzeko zulo beltzean.