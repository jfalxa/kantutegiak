---
id: ab-1275
izenburua: Urtebarrikari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001275.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001275.MID
youtube: null
---

Urtebarrikari mtxarri belarri,
daukanak eztaukanari,
nik eztaukat eta niri,
niri emoten eztostena
beti dabilela larri.

Natxituako gehigarria:
Urte zarrak yoanda barriak etorri.