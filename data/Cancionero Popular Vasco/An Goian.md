---
id: ab-705
izenburua: An Goian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000705.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000705.MID
youtube: null
---

An goian atsotxo bat,
lixibea dago egosten,
an plux bibiadun bibiamante
li lirikon karran plaxet.