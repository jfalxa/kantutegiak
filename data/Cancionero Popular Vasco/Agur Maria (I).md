---
id: ab-980
izenburua: Agur Maria (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000980.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000980.MID
youtube: null
---

Agur Maria graziaz betea,
Jauna da zurekin
bedinkatua zera zu
andre guztien artean
ta bedinkatua da
zure sabeleko frutua Jesus.
Santa Maria, Jaungoikoaren Ama
erregutu ezazu gu bekatariokgatik
orain da gure eriotzako orduan,
Amen Jesus.