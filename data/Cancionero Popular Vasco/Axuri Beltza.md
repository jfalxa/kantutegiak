---
id: ab-343
izenburua: Axuri Beltza
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000343.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000343.MID
youtube: null
---

Axuri beltza ona dut bainan
xuria berriz obea,
dantzan ikasi nai duen orrek
nere oinetara begira.

¿Zertan ari aiz bakar dantzatzen
agertzen gorputz erdia?
Su ilun orrek argitzen badik
agiriko aiz guzia.