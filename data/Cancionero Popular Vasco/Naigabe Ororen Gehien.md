---
id: ab-594
izenburua: Naigabe Ororen Gehien
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000594.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000594.MID
youtube: null
---

Naigabe (1) ororen gehien
ni niz heben;
sofritzeko sortü nizala
düt ikhusten,
pazientzia, Jinko Jauna;
othoi arren!

Emaztea gaizto denün,
naturalki
hura eztaiteke kanbia
hitzez baizik;
ezpalitz bezala behar du
harek ützi.

Haurrek ere yiten dira
ama iduri,
sonü eta solasetara
dira yarri;
bost sos ezin irabaz aldiz
arto-sari.

(1) Malur ororen cantaba el colaborador.