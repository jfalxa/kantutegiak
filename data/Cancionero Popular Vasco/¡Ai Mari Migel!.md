---
id: ab-188
izenburua: ¡Ai Mari Migel!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000188.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000188.MID
youtube: null
---

¡Ay Mari Migel! ¿nun don senargei ori?
Ai Mari Migel ¿noiz don eztegue?
Zapi ta zapi esanagaitik ezin dot kendu katue,
sekula bere eztot ikusi onelan neure burue.
¡Ai Mari Migel!

¡Ay Mari Migel! Katu ori kentzeko
¡Ai Mari Migel! Ken ein zaragie.
Asto andiok, niri zirika zetan agertzen zarie?
Itzungi alda, orrdi alperron lantegiko sutegie?
¡Ai Mari Migel.