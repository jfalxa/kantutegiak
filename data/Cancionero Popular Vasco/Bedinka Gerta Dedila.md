---
id: ab-1199
izenburua: Bedinka Gerta Dedila
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001199.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001199.MID
youtube: null
---

Bedinka gerta dedilla
sekula betiko yaio dan
Sein ederra gizon ta Jaungoikoa,
bakean imintzeko lurra ta zerua,
Jaungoiko ta gizon bat,
gizon ta aingerua.
Zu nor, zu zein, zu nor,
zu emen zeruko Erregea.
Jesus geure laztana,
Jesus geuretzat dana,
dontzella utzirik Ama
Zu gaur yaio zar.