---
id: ab-796
izenburua: Garagartzan Dagoz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000796.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000796.MID
youtube: null
---

ESTRIBILLO
Udan, udan, udan, San Juanetan
dantzan eingo dogu ifar airetan.

Agura zarra baldin baneuko senartzat
botako neuke bentanarean kostaltzat;
inok gura ezpadau, bego neuretzat.

Emongo dautsadala apari galant bat:
sardiña-buru bi ta garagar-opil bat;
axek yan gura ezpadau, atara begi bat.

Agura zirkin-zarkin begiok logura,
ikusten banok bere, enok ire gura.