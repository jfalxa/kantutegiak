---
id: ab-250
izenburua: Zortzi Ta Sei Dira Hamalau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000250.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000250.MID
youtube: null
---

Zortzi ta sei dira hamalau,
apaldu bearrak gaituk gaur:
kapoi erre ta oilasko
egoan aparitato,
ardan gi ona edateko
lagunarte onetarako.