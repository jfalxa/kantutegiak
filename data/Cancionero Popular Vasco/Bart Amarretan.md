---
id: ab-551
izenburua: Bart Amarretan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000551.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000551.MID
youtube: null
---

Bart amarretan don tzela nin tzan,
amaiketako senarra,
gaubeko amabi san tuetako
gelditu nintzan bakarra.
¡Atsekabea, atsekabea,
andiago naibagea:
ordu beteko senarragaitik
alargun izan bearra.