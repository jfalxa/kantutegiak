---
id: ab-203
izenburua: Atsoari Ostu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000203.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000203.MID
youtube: null
---

Atsoari ostu diot uleko petea
salduta edan daigun aguardientea
merke merkea fuerte fuertea,
buru motzik egon daila atso tronpetea.

Atsoa negar baten bart agertu yatan
isilarazoteko erdia emon neutsan
neure edaria gorri gorria,
gero buru estalkia salgei emon eustan.