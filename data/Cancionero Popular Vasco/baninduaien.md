---
id: ab-782
izenburua: Baninduaien
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000782.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000782.MID
youtube: null
---

Baninduaien kantatzera
oro gezurren erraitera:
zaldi zuri bat ikusi nuen
Sevillarik Bayonara.
Nik nere begiz ikusi dut
sagua gitarra yoten,
arratoin bat saltua gora,
katua argi egiten.