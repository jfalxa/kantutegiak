---
id: ab-618
izenburua: Agure Zar Zar Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000618.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000618.MID
youtube: null
---

- Agure zar zar batek
bart gure amari,
bart gurek amari,
mandatu egin dio
bear nauela ni,
mandatu egin dio
bear nauelani.

- Gure amak yardespena
nola eman baitaki,
nola eman baitaki:
etxean baduela
gizon zarrik aski,
etxean baduela
gizon zarrik aski.