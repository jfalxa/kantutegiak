---
id: ab-1092
izenburua: Birjina Amaren Bezpera Da Ta (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001092.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001092.MID
youtube: null
---

Birjina amaren bezpera da ta
goazen Arantzazura,
Santa Luziren begi ederrak
argi egiten digula.

Arantzazura bidea luze
ara orduko kantsatu,
Birjina Ama ta bere Semea
bidean ditut topatu.

Kontseju on bat eman zidaten
Ama-Semeen artean:
ona ta umila izan nendilla
munduan naizen artean.

Ai artzaitxoa, ai artzaitxoa,
nere (y) esan bat egizu:
ait'ori ere zurgiña dezu,
onera bidal ezazu.

Ermita oni ema izkiozu
apeotxu bi aldera
apeotxu bi aldera eta
zazpi teilatxo gainera.

Denporarekin izanen baita
ermita kuriosoa,
ermita kuriosoa eta
komentu poderosoa.

Zure ardiek ongi dabiltze
Santa Triako aitzean,
belartxo goxo batean eta
iturri baten aldean.

Iturri artan jabonatzen da
Birjina Amaren beloa,
berak iruiña, berak egoa,
Birjina Amaren beloa (sic).