---
id: ab-348
izenburua: Biztuko Balitzate
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000348.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000348.MID
youtube: null
---

Biztuko balitzate
orain Jaun Zuria,
ezagutuko e leu ke
gure gizaldia
Ordurik ona egin dau
mundu zarrak bira
len goian egoana
etorri da azpira.

Erakusten zirean
lenago arpegiak,
orain baina gabiltzaz
mozorroz erdiak
Aiztuta bizi gara
euskara gozoa
eztogu e zagututen
lengo Jaungoikoa.