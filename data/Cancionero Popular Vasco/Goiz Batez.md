---
id: ab-1106
izenburua: Goiz Batez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001106.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001106.MID
youtube: null
---

Goiz batez goiz yeikirik,
argi gaberik,
urera yoan ninduzun
pearra harturik.
Lar la la la la la la la la lara la la
urera yoan niduzun
pearra harturik.