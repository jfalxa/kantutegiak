---
id: ab-573
izenburua: Goizuetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000573.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000573.MID
youtube: null
---

Goizuetan bada gizon bat
deritzen zaio Trabuko
Itzak ederrak biotza faltso
sekula etzaio paltako,
egin dituan dilijentziak
berari zaizko damuko.

- Ongi ongi oroitu adi
zer egin uen Elaman
Difuntu orrek izatu balu
yarraikilerik Lexakan,
orain baino len egongo intzan
ni orain nagoen atakan.

- Nere andreak ekarri zuen
Aranaztikan dotea.
Obe zukean ikusi ezpalu
Berdabioko atea;
orain etzuen idukiko
dadukan pesalonbrea.

- Nere buruaz eznaiz oroitzen,
zeren eznaizen bakarra.
Azitzekoak or uzten ditut
bi seme ta iru alaba.
Jaun zerukoak adi dezala
oien amaren negarra.