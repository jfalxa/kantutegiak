---
id: ab-1085
izenburua: Asentsio-Eguna
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001085.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001085.MID
youtube: null
---

Asentsio eguna da señalaturik
Kristo zerura doia besoak zabalik
bere disipuluai adios eginik (bis).

Napar-irargi bai, Napar-irargi
alargun dirudunok ezkongei aundi (bis),
dirurik eztaukenok or doaz igesi.

Agura zar bat daukat oian gaixorik,
oilo bat bere il dotsat, eztauka obekuntzarik
beste bat ilgo neuskio, urten ezpalekit.

Agura zar zar batek ni barko gabean
dantzan erabili nau belaunen gainean,
belaunen gainean da beretzat ustean (bis).

Agura zar zar orrek ama neureari
mandatu egin deutso bear nabela ni,
ama gureak badaki, zoroa danarren,
enaizela ni gura agura zar orren.