---
id: ab-388
izenburua: Nik Maite Tut Neskatxak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000388.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000388.MID
youtube: null
---

l.- Nik maite tut neskatxak,
neska gazte politak
eta zerbaito du tenak.

- Halako bat behar dut,
ezkontze ko ordu dut
adinak ere baditut.