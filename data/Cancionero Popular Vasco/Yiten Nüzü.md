---
id: ab-142
izenburua: Yiten Nüzü
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000142.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000142.MID
youtube: null
---

Yiten nüzü hurruntik
gayaren ülhünagatik
zure bihotzean sarrtüna
hiz beste (y) ororen artetik.
Gogorra zira zü ene,
bena sogiten dizüt emeki.