---
id: ab-1087
izenburua: Asto Zarra Tudelatik (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001087.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001087.MID
youtube: null
---

Asto zarra Tudelatik
ardo gozoz kargaturik
lara lai lar lara lalai
ardo gozoz kargaturik.
Otso gaiztoa opatu du
oian beltzean bakarrik
lara lai lara lara lalai
oian beltzean bakarrik.
Agur agur otsoko
ongi etorri, astako.
Baldin egarri baldin bazera
badet ardo gozo franko
lara lai lara lara larai
badet ardo gozo franko.
Ez nauk ni egarri,
sobra nauk ni gosegi
laralai lara lara larai
sobra nauk ni gosegi.
Or goian iru artalde
zakurrik ere eztie
aiengana dijoanentzat
txikiro parea badie
laralai lara lara larai
txikiro parea badie.
Oinean diat arantza
eta ezin nauk ongi mugitu
laralai lara lara larai
ezin nauk ongi mugitu.
Or goian ermita
San Bartolome deritza,
gero yanen banuzu ere
entzun bear diat Meza
laralai lara lara larai
entzun bear diat Meza.
¡Oi zer luzea Meza ori!
Erramu eguna dirudi
laralai lara lara larai
erramu eguna dirudi.
Otso zarra gazia
sobra da gaizki azia.
Gero yango banauk ere
badiat gaurko bizia.
Asto zarra mukizu,
malezi asko dakizu:
baldin nik inoiz oyan beltzean
arrapatzen bazaitut
larai lara lara larai,
Mezarik entzunen eztuzu.