---
id: ab-6020
izenburua: Ene Maitea (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006020.MID
youtube: null
---

Nere maitea yeiki yeiki
¿etziradea loz ase?
Zure ondoan gabiltzen gaixuok
ezkiradea lo gose.
Irekizazu bortaño oiek,
adiskideak gerade.