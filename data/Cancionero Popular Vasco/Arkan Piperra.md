---
id: ab-951
izenburua: Arkan Piperra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000951.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000951.MID
youtube: null
---

Arkan piperra, pikor anixko,
musuan usain gozoa.
Agerian den ederrena da
Ana lagun artekoa.
Orrixe galai nor eman diogu
berori bezalakoa?
Berau ona da,
obea luke Anton Goienetxekoa.