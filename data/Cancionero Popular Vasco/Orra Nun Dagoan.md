---
id: ab-239
izenburua: Orra Nun Dagoan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000239.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000239.MID
youtube: null
---

Orra nun dagoan botea,
ardao klaret onez betea:
guztiok aren zaleak gaituk,
guztiok aren zaleak.
Orra nun dagoen sokea
estu ebana botea.