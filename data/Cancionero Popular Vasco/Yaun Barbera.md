---
id: ab-806
izenburua: Yaun Barbera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000806.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000806.MID
youtube: null
---

Yaun barbera ¿hilen otheniz?
Oi Maria hori eztakit
¿Egiten duzuia kakarik?
Bai yauna terrina beterik
Par mafua konten niz:
etzira hilen hortarik.