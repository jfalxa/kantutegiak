---
id: ab-628
izenburua: Eznaiz, Eznaiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000628.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000628.MID
youtube: null
---

Ez naiz, ez naiz, ez naiz estonatzen
eztala inor ezkontzen.
Neskaña hori lujuño hori ezbazaie ttipitzen,
bizi denak ikusiko du mu xurdin franko gelditzen.