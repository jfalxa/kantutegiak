---
id: ab-219
izenburua: Goizean Diru Kontatzen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000219.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000219.MID
youtube: null
---

Goizean diru kontatzen,
arratserako gastatzen,
bazkari ona yokatzen;
kafeak artuz kopak edanez
edari finak garbitzen;
eginik eztet ukatzen,
orain ez painak nekatzen
sardin ederez betetzen.