---
id: ab-566
izenburua: Gazte Gazterik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000566.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000566.MID
youtube: null
---

Gazte gazterik,
gogo gaberik,
Amak ezkondu ninduan;
ezkontidetzat
osaba zar bat
agertu zidan orduan,
ark izuturik
nere ondotik
zoriak iges ein zuan.

Aurtxo bat banu,
ark maiz ta manu
lekarket zorion hura;
bainan gauta egun
maite eztegun
batekin ¡au errenkura!
Ama, Amako
¡ni ontarako
karri ninduzun mundura.