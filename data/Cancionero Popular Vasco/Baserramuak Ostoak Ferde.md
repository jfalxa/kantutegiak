---
id: ab-553
izenburua: Baserramuak Ostoak Ferde
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000553.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000553.MID
youtube: null
---

Baserramuak ostoak ferde,
sasoin oroz du kolore.
Neskatila gazteak
ez nautela maite,
deliberatu dutfraile.
Olorongo komentuan
nere begira baitaude.