---
id: ab-724
izenburua: Buhameak Dakitena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000724.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000724.MID
youtube: null
---

Buhameak dakitena
trikun trakun egiten,
oiloñoa ebats eta
altzopean gordetzen.