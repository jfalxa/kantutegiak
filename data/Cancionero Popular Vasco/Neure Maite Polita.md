---
id: ab-164
izenburua: Neure Maite Polita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000164.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000164.MID
youtube: null
---

Neure maite polita
¿zelan zara bizi?
zortzi egun onetan
etzaitut ikusi
Uste dot zabilzala
neugandik igesi
ezteustazu emoten
atsakabe gitsi
munduak jakin
eta ¡zeuk ez jakitea!
Ondo egiten dozu
zurikatutea.