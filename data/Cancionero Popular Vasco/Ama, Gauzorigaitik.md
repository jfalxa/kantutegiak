---
id: ab-859
izenburua: Ama, Gauzorigaitik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000859.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000859.MID
youtube: null
---

Ama, gauzorigaitik ez egin negarrik,
ezteutsudala emongo errementaririk.
Errementari baiño oba da zesturu
lepoa bete ailaragaz altzo bete diru.