---
id: ab-281
izenburua: Katalina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000281.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000281.MID
youtube: null
---

Katalina, Katalina,
begia duzu urdina,
emaiozu aur uneri
bi orduko loaren mina:
Katalina, Katalina.