---
id: ab-1138
izenburua: Nik Baditut Mortuetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001138.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001138.MID
youtube: null
---

Nik baditut mortuetan
ardiak artzanorekin,
Katalonian eun mando
zilar bulunbarekin,
itsasoan amar ontzi
euren mariñelekin;
al badezue emaiezue
Frantziako Errege;
Frantziakoa ezpada ere
Espaiñiakoa bederen.