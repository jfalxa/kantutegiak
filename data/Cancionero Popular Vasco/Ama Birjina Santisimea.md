---
id: ab-988
izenburua: Ama Birjina Santisimea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000988.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000988.MID
youtube: null
---

Ama Birjina Santisimea,
indazu zeure grazia
aingeruakin asiko naz
ni esaten Abe Maria.

Zeru altutik sabel zurera
etorria da frutua,
zuen artean izanik askoa
Jesus bedeinkatua.

Santa Maria Jaungoikoaren
Amatzat eskojidea
zure erreguz yaritxiguzu
zeruetara bidea.