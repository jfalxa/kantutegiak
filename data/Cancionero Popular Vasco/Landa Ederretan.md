---
id: ab-964
izenburua: Landa Ederretan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000964.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000964.MID
youtube: null
---

Landa ederretan eperrak dantzan,
pagaduietan usoak;
orain emen dan ederrena da
Mari Otalorakoa

Orrek bear dau laguna bere
beroi dan bezalangoa;
beroi dan bezalangoa leuke
Julian Artabekoa.