---
id: ab-1029
izenburua: Indietako Apostolua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001029.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001029.MID
youtube: null
---

Indietako Apostolua,
Aita Jabier geurea,
pelegrinuen yantzian dator
bideztiarren trantzean.
Ez aurrera yoan
gelditu emen
geure uria goratzen.
Bear zaituguz betiko,
Aita Jabier Frantzisko.

- Lekeitioko uri noblea,
o zeure zoriz betea,
Napolestikan ekarri dabe
Naparroako semea.
Ez aurrera yoan
gelditu emen
geure erreguak entzuten.
Bear zaituguz betiko,
Aita Jabier Frantzisko.