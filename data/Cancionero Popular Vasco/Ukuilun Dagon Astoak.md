---
id: ab-1266
izenburua: Ukuilun Dagon Astoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001266.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001266.MID
youtube: null
---

Ukuilun dagon astoak
ganbelan daukan lastoa;
Arkakusuak itoko al du
etxe ontako atsoa.