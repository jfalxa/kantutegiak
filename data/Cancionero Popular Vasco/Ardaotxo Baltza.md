---
id: ab-192
izenburua: Ardaotxo Baltza
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000192.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000192.MID
youtube: null
---

Ardaotxo baltza ¿zer dakak ik?
Larregi edanak ez gauza onik,
gizonak iminten badakik ik
zutunik ibili ezinik.

Ur zikin orrek ¿zer diraustak?
Gizonak ito daroazak .
Balbeak arabil laukidetzat
¡ut! sapaburuak edaritza.