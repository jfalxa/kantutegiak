---
id: ab-847
izenburua: Urondoetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000847.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000847.MID
youtube: null
---

Urondoetan ahate,
bortü goretan lanhape,
hi bezalako inpertinentak
ez enetako ahalke.