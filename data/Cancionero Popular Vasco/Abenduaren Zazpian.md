---
id: ab-528
izenburua: Abenduaren Zazpian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000528.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000528.MID
youtube: null
---

Abenduaren zazpian
sartu ginaden Frantzian,
ogei ta amar lakari gatz
azukre bion erdian,
ustez sekreto andian
pasa ginaden mendian.