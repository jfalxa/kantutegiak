---
id: ab-227
izenburua: Larretik Etxera Yin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000227.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000227.MID
youtube: null
---

Larretik etxera yin
ta odola hoztua
orduan kausitzen da
arno onaren kutsua.
Arno onak, arno onak
berotzen dizkit zainak;
arno onak, arno onak
ahantzen zinkurinak.

Alargundu-berritan
nigarrez gira urtutzen,
ilari-bazkaritan
gira berriz poztutzen.
Arno onak...

Osailuak hiltzen du
gizadirik erdia,
bertze erdia edailuak
dauka zutik bizia.
Arno onak.