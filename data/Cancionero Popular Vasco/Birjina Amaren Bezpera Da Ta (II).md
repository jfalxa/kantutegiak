---
id: ab-1093
izenburua: Birjina Amaren Bezpera Da Ta (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001093.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001093.MID
youtube: null
---

Birjina Amaren bezpera da ta
goazen Arantzazura,
Arantzazura bidea luze
ara orduko iluna,
Santa Luziren begi ederrak
argi eginen digula.