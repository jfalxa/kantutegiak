---
id: ab-816
izenburua: Yoan Nintzen Ainoara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000816.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000816.MID
youtube: null
---

Joan nintzen Ainoara ¡a zer zoria!
Joan nintzen Ainoara ¡a zer zoria!
Astoño baten saltzera,
eskalapoin erostera
¡ai ai ai zoin zen bitxi!
¡ai ai ai zoin zen bitxi!

Hasi nintzen hautaketan
eskalapoinen,
arratoin batek handik yauzi,
astoa lasterka igesi.
¡Ai ai ai, zoin zen bitxi.