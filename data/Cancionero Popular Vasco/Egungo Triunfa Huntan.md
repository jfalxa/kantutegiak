---
id: ab-1006
izenburua: Egungo Triunfa Huntan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001006.MID
youtube: null
---

Egungo triunfa huntan
Jesus da gurekin baltsan,
emana ostia huntan,
emana ostia huntan.
Zeruan delarikan
hemen da sakramenduan,
aphezaren eskuetan,
aphezaren eskuetan.

Satanen bentzutzailea
Zu zare, Jesus maitea;
Ostia miragarria,
benedika gaitzatzu
othoitzen plazer baduzu,
hori galdeiten dautzugu.

Bethiereko loria
eman bekio Aitari,
gure Kreatzaileari
Seme gizon eginari,
Izpiritu Sainduari
Trinitate Sainduari.