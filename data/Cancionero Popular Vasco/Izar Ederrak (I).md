---
id: ab-1116
izenburua: Izar Ederrak (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001116.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001116.MID
youtube: null
---

Izar ederrak argi egiten dau
zeru altuan bakarrik;
ezta bakarrik, lagunak ditu
Jaun zerukoak emonik.

Zazpi aingeru aldean ditu,
zortzigarrena gaixorik;
zazpi mediku ekarri deutsez
India-Madriletatik

Gaur amarretan ilgo naz eta
erdu neure entierrura,
gorputz santu au enterraduta
igongo dozu korura.

Andixek bera yatsiko zara
Birjina Amaren ortura.

Artu kabeliña bat eskura,
larrosatxu bat mosura,
mundu guztiak esan eztaian
neure lutua dozula.

Aingerutxuok esaten dabe
Salbean Abe Maria,
gero guztiok gozadu daigun
zeru altuan gloria.
Amen Abe Maria.