---
id: ab-936
izenburua: Ttakur Ttipi Gorritto Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000936.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000936.MID
youtube: null
---

Ttakur ttipi gorritto bat
faltatu zait neri,
artaz baliatzen dena
ongi bizi bedi.
Kliskitin, klaskitin
arrosa krabelin,
artaz baliatzen dena
ongi bizi bedi.