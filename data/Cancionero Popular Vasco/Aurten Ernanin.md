---
id: ab-870
izenburua: Aurten Ernanin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000870.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000870.MID
youtube: null
---

Aurten Ernanin San Juanetan
zezenak dira yokatu.
Orain aurrera amairu bertso
bear nituzke paratu:
gizon batzuek dirala kausa
neri zer zaidan gertatu.

Preso nintzala esan zidaten,
enakian nik zer egin;
esku bietatik eltzen zidaten
Jaunak erori enendin;
laster asko erakutsi zidaten
kartzela non zen Ernanin.