---
id: ab-701
izenburua: Aita Aita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000701.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000701.MID
youtube: null
---

Aita aita da txuntxulunberde
¡Ama Ama da masusta!
Aitak ekarriko tso Amari
gona barritxo ederra.