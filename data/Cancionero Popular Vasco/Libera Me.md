---
id: ab-229
izenburua: Libera Me
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000229.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000229.MID
youtube: null
---

l.- Libera me Domine kantatzean,
ez busti ixaporik urean,
arnoan busti zazu han ongi sartu zazu:
hori da nahiko dudana;
hilean bizian bezala.

- Pater Noster aphezak erraitean,
ilariliarrak isilean
arno saltzale bati pinta bat kharrarazi,
zerraldo beltzaren ondoan
nere osagarriari edan.