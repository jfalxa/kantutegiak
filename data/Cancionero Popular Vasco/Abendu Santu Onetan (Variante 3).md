---
id: ab-6016
izenburua: Abendu Santu Onetan (Variante 3)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006016.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006016.MID
youtube: null
---

Abendu santu onetan
Kristoren jaiotzea
semea jaio ta
ere ama dontzellea.
Ia Maria y Jose
Jesus Maria
ia Maria y Jos.