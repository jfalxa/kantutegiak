---
id: ab-6007
izenburua: Agur, Agur Otsoko (I) (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006007.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006007.MID
youtube: null
---

Asto xarra müküzü,
gazterik anitx dakizü.
Ni berriz trunpaturik
erririk eginen eztüzü,
erririk eginen eztüzü.