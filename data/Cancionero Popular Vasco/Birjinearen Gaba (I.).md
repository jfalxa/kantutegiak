---
id: ab-999
izenburua: Birjinearen Gaba (I.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000999.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000999.MID
youtube: null
---

Quarrentetatican
nai nituzque nic
nere ixil gauzac
arguitaratu
nere Jangoicoa
zuc eracustazu
cer eguin nola itzeguin. asdfasdfasdf

Laga nituen nic
nere ixil gauzac
maitecho batengan
saiatu bear det
Jaquitera
mudatu ote dan.

Triste daucat biotza
nere laztana
zugatic
cer eguin ez daquit
erremedia nazazu
pena onetatic.

Aingueruchoa
Jarri cinaden
biotz nerean betico
ala deritzat nerequico:

damu det bestec eramatea
zu biotz neretican
ez deritzat mudatuco cerala
oraingo fedetican.

Aingerucho oni
esanaz nago beti
bioc alcarrequin
bear degula bici:

Sartu zat biotcean neurgana
eta ecin apartatu
cer eguin
eracustazu.

Eguin det aleguiña
sartu zat zure miña
egun da santa seculaco
nere aingueru piña:

Ay au penaren aundia
esan dezadan eguia
zabala dago zure beguia
nere pena gucien
consolagarria

Erramuren mezpera
ezqueroztican
et cerade nere maite polita
beiñ ere ateratuco
neronen biotcetican.

Biotcean sartuta
ecin apartatu
arren consola nazazu:

Beti penaz oi nago urriqui
el zaizquit Ceru altua
cerorrek ondo daquizu
nere trabajua.

Nere buru gucia
galdurican
ni emen nago
erremedio on bat arren
indazu niri
ill baño lenago.

Esperanza daducat nic
maitea zugandic
sendatuco nazula
miñ onetatic.

Nere maiteac
ezagutcen bait du
daramaizquidana
pena andiac
aditcera ematen
asi dirade
noizbait aren beguiac:

Nere maitea ez bada arren
gueiago eguin negarric
zure suspirioac neuri
biotza erdiratcen dit.

Bostetan esan dizut
nic zuri
biotx nereco berri:

nere itzaren Jabe nago ta
badaquizu ez dirana bi
beragatican zor dizquiratzu
graciac particularqui
aspaldi da esan nizula
zuretzaco nengoela ni.