---
id: ab-533
izenburua: Aita Aitzinerako
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000533.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000533.MID
youtube: null
---

Aita aitzinerako,
semea ondoko,
osaba burges
ori dirua ornitzeko¡
eztirade neuretzat
bi bost liberako,
galeretan bedere
¡oi! ler egiteko.