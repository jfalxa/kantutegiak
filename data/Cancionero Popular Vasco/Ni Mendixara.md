---
id: ab-387
izenburua: Ni Mendixara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000387.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000387.MID
youtube: null
---

l.- Ni Mendixara San Pedroetan
banoa zezenetara.
Ni Mendixara San Pedroetan
banoa zezenetara.
¡Arek bai neskatxak ederrak galantak
Lekeitiarren aldean Mendixan Elizaldean!
Da San Dxuan da San Dxuan
beti zaukadaz goguan.

- Igaztik ona urte bete da
¡aspaldi onen luzea!
Igaztik ona urte bete da
¡aspaldi onen luzea!

Arik onako gau egunetan
marastu leike gaztea.
Da San Dxuan, da San Dxuan
beti zaukadaz goguan.