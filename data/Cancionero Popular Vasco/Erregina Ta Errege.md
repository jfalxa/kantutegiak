---
id: ab-1221
izenburua: Erregina Ta Errege
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001221.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001221.MID
youtube: null
---

Erregina ta Errege,
egun Santa kurutze,
ek guziak onak dire
zazpi zilar gurutze.

Eman digute nobleki,
konpañak ere badaki,
zuk emaniko sari orrekin
egin bear ttugu tortxa bi.
Tortxak bear du aria,
ariak argizaria,
hamalau mila aingeruekin
Paradisuan gloria.