---
id: ab-850
izenburua: Ahargu Eta Kanbillu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000850.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000850.MID
youtube: null
---

Ahargu eta Kanbillu,
ardüra da hetan lanhu.
Aspaldian ebili iza
dendariaren onduun;
gantza loditzen ari ziok
¡gaixoa! Sabel onduun.

Barkoxe bürgü berriak
aizo dü Larragorria.
Amoros batek salatu dizü
kurrunkaz bere bürüa,
zeren etzen leihotik jausten
asto bürü-handia.