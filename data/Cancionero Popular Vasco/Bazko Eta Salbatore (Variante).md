---
id: ab-6033
izenburua: Bazko Eta Salbatore (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006033.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006033.MID
youtube: null
---

Bazko eta Salbatore,
orai eldu Mendekoste,
eta ni ohean triste,
xutik ere ezbainauke!