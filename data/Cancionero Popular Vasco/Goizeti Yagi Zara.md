---
id: ab-1109
izenburua: Goizeti Yagi Zara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001109.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001109.MID
youtube: null
---

- Leisibatxu zuria dozu, andra gaztea.
- Ondotxu egosita, aita praillea.

- Eskutxu zuria dozu,andra gaztea.
- Guantepean erabilita, aita praillea.

- ¿Semerik edo badozu, andra gaztea.
- Semetxu bi daukadaz, aita praillea.

- ¿Semak nun dituzu, andra gaztea.
- Eskolal erakusten, aita praillea.

- ¿Senarrik edo badozu, andra gaztea.
- Senarra biajean, aita praillea.

- ¿Ezagutuko etezenduke senar zeurea?
- Lengo erropakin baletor bai, aita prailea.

- Laztan emongo neuskizu, andra gaztea.
- Ibaia dago bitarte, aita praillea.

- Berton itoko alzara, andra gaztea.
- Berori txalupa dala, aita praillea.

- Suak erreko alzaitu, andra gaztea.
- Berori egurra dala, aita praillea.

- Agur, agur, agur, andra gaztea.
- Beyoa ondo ondo, aita praillea.