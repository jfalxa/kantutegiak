---
id: ab-709
izenburua: Anton Bat Eta Anton Bi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000709.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000709.MID
youtube: null
---

Anton bat eta Anton bi,
Anton putzura erori,
edan zituen pitxar bi,
etzan orduan egarri.
Bonbolon bat eta bonbolon bi,
eragiok Santi arraun orri.