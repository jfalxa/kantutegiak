---
id: ab-954
izenburua: Begira Begira Nago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000954.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000954.MID
youtube: null
---

Begira begira nago
zein giren ederrago
emen den ederrena
da Antonio lagun arteko.
Orrentzat konponiturik
emen nior eztago,
orrentzat konponiturik
Anizeta Lartxean dago.