---
id: ab-953
izenburua: Aza Gazte
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000953.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000953.MID
youtube: null
---

Aza gazte lantare,
konpaña dugu logale,
barda eta gaur ere.
Frantzizka orrek ziozu
logale dela logale,
barda eta gaur ere,
Mendigatxa jinen ziozu
andere iratzarzale,
ezpeita izan logale.