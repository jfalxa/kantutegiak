---
id: ab-1080
izenburua: Ama Ezkondu (IV)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001080.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001080.MID
youtube: null
---

Ama, ezkondu; ama ezkondu, gazte geraden artean.
Ene alaba, etzeukanagu ogi ta artorik etxean.
Galburutxo bat ekarri nuen garia ondu zanea,
eun erregu berexi nizkan irriarratxo batean.

Ama, ezkondu; ama ezkondu, gazte geraden artean.
Ene alaba, etzeukanagu ardo tantorik etxean.
Masmordotxo bat ekarri nuen mastia ondu zanean,
eun kantaro berexi nizkan irriarratxo batean.

Ama, ezkondu; ama ezkondu, gazte geraden artean.
Ene alaba, etzeukanagu aragirikan etxean.
Egun batean ari nintzala yosten or labe gaienan,
txantxangorri bat etorri eta eseri aldamenean.