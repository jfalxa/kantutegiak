---
id: ab-1235
izenburua: Iru Errege
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001235.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001235.MID
youtube: null
---

Zeruko erregea
yaio zan orduan;
munduan
Herodes erregea
enbidiaz betea
bildurtuten asi zan.

Herodes erregea
¿zek bildurtzen zaitu?
Esaizu.
Yaio dan infantea
zeurorrek il eitea
lograduko eztozu.

Iru Errege datoz
Orientetikan Belenera,
aurretik dakarrela
izar argi eder bat
bidea erakustera.

Eldu zireanean
Belengo erriko
portalera
Yauna adoratzera,
auspaz yausi zirean
belaunbiko lurrera.

Ofrezietan deutsez
euren presenteak
nobleak.
Batak dakar urrea,
besteorrek mirrea
ta insiensoa besteak.

Beste bide batetik
biurtuten dira
errira.
Herodesen asmuak
eta pentsamentuak
orain burlatu dira.

Herodes burlaturik
gelditu zanean,
berenean
egindu eban ilteko
inozente guztiak
Belengo errian.

¡Ai ze negar andiak
orduan Belenen
eukezen!
Amaren bularretik
errukia bagarik
daroez umeak ilten.