---
id: ab-825
izenburua: Lagongon
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000825.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000825.MID
youtube: null
---

Lagongon Maria gongon
xibiribiri gongon (bis).
Bitoria, bitoria xibiribiri gongon,
bitoria, bitoria xibiribiri gongon.