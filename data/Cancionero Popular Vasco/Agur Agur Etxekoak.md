---
id: ab-1186
izenburua: Agur Agur Etxekoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001186.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001186.MID
youtube: null
---

Agur agur etxekoak
atsa onean zadete,
atalondoan pobreak eta
arren iratzarri zaitezte.