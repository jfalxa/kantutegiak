---
id: ab-1158
izenburua: Peru Gurea Londresen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001158.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001158.MID
youtube: null
---

La mujer canta:
Peru gurea Londresen
zingulun-larrak (1) ekarten
hura (y) andikan etorriarte
guztiok dantza gitezen.
¡Ai ori egia!
Zingulun zangulun Maria.

El intruso dice:
Kaponak daude erretzen,
oilaskoak mutiltzen;
eztin ark asko orai pentsatzen
onela gerala dantzatzen.
¡Ira-jira-bira!
Aztan-zingulun Maria.

El criadito alega:
Ugazaba topau neban
Santo Tobeko zubian
neugaz atzera ekarri neban
sardin-sestotxo barrian.
¡Ai ori egia!
Daigun inguru, Maria.

Otra versión pone en boca del criado estas palabras
Nagusia Bitorian
plazako ostatu berrian,
osoro larrugorrian
sartu zait saski-erdian.
¡Ira-jira-bira!
Orain duk ire aldia.

Al decir el muchacho estas palabras dio un puntapié al cesto. Salió de
él su amo y empezó a cantar:

Neure mutiltxo txikia,
ik esan uen egia
medikuarekin dantzatzen zala
nere emazte Maria.
¡Ai oi egia!
Iretzat mando zuria.

Otra versión pone estas palabras en labioos del marido:
Nere mutiltxo txoria,
ik erran datak egia:
dantzan arrapatuko dudala
nere andre Maria:
¡Ira-jira-bira!
Iretzat mando aundia.