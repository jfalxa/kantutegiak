---
id: ab-538
izenburua: Aitak Eta Amak Neri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000538.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000538.MID
youtube: null
---

Aitak eta Amak neri
konseju ona franko,
festak eta feriak
beren gisa uzteko,
aiek ez obedituz nabila
ni emenzo koz zoko.
Aspaldi erran zidaten
zer zen gertatuko.
Mando luzek omendio
ni pobrea naizela,
gezurra aditu ezpalu
nonbait bertzela.
Nik baditut bi etxe,
bat aski eztela:
bat daukat ospitalean
ta bertzea kartzelan.