---
id: ab-300
izenburua: Txangolo Mangolo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000300.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000300.MID
youtube: null
---

Txangolo-mangolo ¿gure aurra nongo?
Ona bada, itxeko; gaiztoa bada, kanpoko.
Markelaingo soroa: garia bada,betor onera,
artoa bada, bijoa.
Txinguren-minguren ¿gure aurra noren?
Ona bada, amaren; gaiztoa bada, bertzeren.
Aralargo mndira
goiztirietan, âstirietan
eperrak agertzen dira.