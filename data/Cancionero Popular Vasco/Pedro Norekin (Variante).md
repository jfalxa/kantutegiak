---
id: ab-6011
izenburua: Pedro Norekin (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006011.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006011.MID
youtube: null
---

Kristok Pedrori galde egin zion
¿norekin zaude orrela?
Anime gaixo bat dago emen,
barrena bear duela
Ezta lekurik, ezta lekurik,
bijoa infernura ortik.