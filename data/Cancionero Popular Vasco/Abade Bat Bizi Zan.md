---
id: ab-851
izenburua: Abade Bat Bizi Zan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000851.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000851.MID
youtube: null
---

Abade bat bizi zan
Castrejana aldean
mesede asko eginda
beartsu artean.
Aurkitzen zirenak
bear izanean
sorosten zituela
atsegin andian.