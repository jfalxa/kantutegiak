---
id: ab-369
izenburua: Iru Xito
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000369.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000369.MID
youtube: null
---

Iru xito izan eta lau galdu
¿gure txitoaren Amak zer yan du?
Gure txitoaren Ama oiloa
axariak kendu dio lepoa.