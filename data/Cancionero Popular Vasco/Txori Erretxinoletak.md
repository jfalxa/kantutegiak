---
id: ab-1173
izenburua: Txori Erretxinoletak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001173.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001173.MID
youtube: null
---

Txori erretxinoletak eder kantatzen,
itzuliak oro tu xarmantzen.
Barda arratsean sasi batean
biga baziren, eder ziren,
xarmant ziren manera oroz,
bai eta elgarrez ongi agradoz.