---
id: ab-1157
izenburua: Pedro Norekin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001157.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001157.MID
youtube: null
---

- ¿Pedro norekin ari aiz horrela?
- Yauna, emen arima bat da barrura nai lukeala.
- Ezta lekurik, ezta lekurik, doaiela inpernura ortik.

- Gaixoak negar egiten zuen, zorigaiztoko munduan etxeagatik
etxekoengatik galdu zuelako zerua.
- Ai ze orainik, ai ze orainik, itxaropen dut (1) Amarengatik.

- Seme, erruki diat gaixo au.
¿Odol beroz garbildu batek orai samurtzen ezalau?
- Ama, neola egin al balitz, zure naia negike itzez itz.

- Mendi artan ni zutik bezala, maiterik au belauniko
opalartean lagun maiz izan ¿ta ezaldek onetsiko?
- Onen bizitza zerorrek ikus bedazakezu, galdu da betiko.