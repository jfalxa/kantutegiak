---
id: ab-336
izenburua: Altuan San Bernabe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000336.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000336.MID
youtube: null
---

Altuan San Bernabe,
bajuan Gordobil,
Otzandioko plazan morena
tanbolina dabil.
La ra la la ra la la ra la la la la ra la la ra la la ra la la.