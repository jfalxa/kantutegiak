---
id: ab-600
izenburua: Oro Poz Pozik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000600.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000600.MID
youtube: null
---

Alegereak bagiñan ere
tristura andiak yo gaitu,
zazpi librako oilo zuria
bart axariak yan digu.
Nere andre Mari Katalin,
aurten badinau zer egin:
Au zoan iltzen dan txerri
txikiak etxean koipe gutxi din.
Urrun labirun labi run
lena etxean koipe gutxi din.