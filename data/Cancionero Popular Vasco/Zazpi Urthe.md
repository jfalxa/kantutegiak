---
id: ab-613
izenburua: Zazpi Urthe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000613.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000613.MID
youtube: null
---

- Zazpi urthe baduzu uda hunetan kunplitu.
Amodio perfet batez zintudala maitatu
bainan orai beldur nuzu behar zitudala galdu.

- Arrosaren bo tigak ederra duzu lorea.
Nik aldiz uste bainizun zintudala enea;
ilun beltzean zu gabe izan niz zori gabea.