---
id: ab-1176
izenburua: Udabarrian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001176.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001176.MID
youtube: null
---

Udabarrian zarra erara
Londresko ziudadean,
maitearentzat oiak erosten
denda aberats batean.

Dama galant bat topatu neban
tabladu baten ganean,
kortesiagaz itandu neutsan
oi orrek zegan zirean.

Bestearentzat diruetan da
zuretzat biotz-trukean.
- eskerrik asko, dama galanta,
eztot armarik aldean.

Armak etxean itxi nituzan
Motriku erri onean,
Motriku erri onean eta
Ana Juanaren etxean.

Txalupatxu bat, txalupatxu bi,
Santa Klararen parean,
neure anaia ara antxe dator
Ana Juanaren legean.