---
id: ab-941
izenburua: Urlia Dago Negarrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000941.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000941.MID
youtube: null
---

Urlia dago negarrez
masaila llertu bearrez
¡ufa! Labiru labiru lera
¡ufa! Labiru labiru lera
¡ufa! Labiru labiru lera
¡ufa! Labiru labiru lera.