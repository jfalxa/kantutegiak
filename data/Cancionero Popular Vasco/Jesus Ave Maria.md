---
id: ab-1229
izenburua: Jesus Ave Maria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001229.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001229.MID
youtube: null
---

Jesus Ave Maria graziaz betea,
Jauna da zugaz eta da zure semea,
bedeinkatea zera ta bedeinkatua
gaur zure sabeletik jaio dan frutua.

Jesus Santa maria Jaun goikoaren Ama,
erregutu egizu amatxu laztana,
pekatariokaiti jaio da Belenen (1)
geure erotzako orduatan Amen.
Jesus maite laztana geure ontasuna,
zatoz ordu onean mundutarregana.
Arren emon egizu gaur gabon saria,
munduan bakea ta ariman grazia
gero gerta dakigun zeruan gloria.