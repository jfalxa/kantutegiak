---
id: ab-550
izenburua: Baldomeko Oihana
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000550.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000550.MID
youtube: null
---

Baldomeko oihana eta mendi handia,
Gayarre mineko igargu latz garria.
Harma balaz kargaturik, handik bidaria
yin izan zaitanean, ene isterbegia
ezarri niozun puñala bularreala.
Harma balaz kargaturik bere sabeleala.