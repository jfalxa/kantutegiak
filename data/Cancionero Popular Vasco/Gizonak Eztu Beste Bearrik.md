---
id: ab-888
izenburua: Gizonak Eztu Beste Bearrik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000888.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000888.MID
youtube: null
---

Gizonak eztu beste bearrik
zabarkeria aski du.
Ostatuan guri orduak
laster joaten zaizkigu.
Gure jainkoak eztigu emaiten
merezirenbat kastigu.
Gizon deboxak abusatzeko
tiraka gutxi aski du.

Arnoa edan ta pipa artu
gero burua xoradu.
Familian bear diren sosak
ostatuetan gastatu.
Zure denboran arla eginak
orain sakelean bazintu,
jenden artean izain zenuke
duzun adin bat kreditu.