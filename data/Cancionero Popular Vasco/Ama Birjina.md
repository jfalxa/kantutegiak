---
id: ab-1076
izenburua: Ama Birjina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001076.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001076.MID
youtube: null
---

Ama Birjina zegoenean
ganbera ilun batean,
ilunik ere argitzen zuen
Andrea zegoen lekuan.
Yaun San Gabriel xarri zitzoen
belauriko aurrean,
izu andiak artu zuen
gura antxe ikusi zuenean.
- Ez izutu, ez Maria,
ni naiz mandataria.
- ¿Mandataria nondik, nondik?
- Zeruko koroetatik,
zeruko koroetatik eta
Errege Aitarengandik.
- Mandataria ¿Zer dakarrazu
berze munduko berririk?
- Berriak berri eta berriak
guziz tristerik.
Gure Jesus omendago
guzia lantzaztaturik,
guzia lantzaztaturik eta
irur iltzez xosirik.
- ¡Oi, ene seme onetsia,
oi zer dugun merexia!
Obenik eta kulparik gabe
¡oi zure llagen utsusia!
- Ama Andrea, zaude isilik,
ez egin ¡otoi! Nigarrik:
gaurko nere odol isuriez
eztaukazula konturik.
Egarri naiz bai egarri.
Biotzean du ageri.
Judeo krudel traidore oiek
eman ziraten edari,
edaria dolorea
ozpinarekin gedarrea,
ark erretzen zuen
nere biotza,
altzairu urrak (?) bezala.
Amen, amen dela Jesus
guzien Salbatzalea,
zure izenean erranen dugu
gogo onez Abe Maria,
baita pater noster ere
Jauna dakigun balia,
Jauna dakigun balia eta
berze munduan gloria.