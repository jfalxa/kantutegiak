---
id: ab-332
izenburua: Aldapeko Sagarraren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000332.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000332.MID
youtube: null
---

Sagarraren adarraren igarraren
puntaren puntan
txoriñoa zegoen kantari
Ai txiruliruli ai txiruliruli.
Nork kantaturen du soñu ori.
Alangoxea naiz, alangoxea naiz
ulangoxea alangoxe fraidea naiz
eta dantzari naiz eta yakin ez
norekin bai eta norekin ez.