---
id: ab-926
izenburua: Ondarrutar Oriek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000926.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000926.MID
youtube: null
---

Ondarrutar oriek egin dute plana
egin bear dutela egun artan lana,
amurratzen jarri da Lekeitio
aurten urrikari den oien kontra dana.

Estropadan asi ziran biak alkarrekin
aurrera joan zitzaion bentajearekin
"ea nere mutil maiteak piska bat egin"
San Antonera-orduko biak ia berdin.

Zumaiara-artean aurretik ederki
nobedade egin zaio tostartean bati
erremuratutzeko Felipe jaun ori
San Antonera-orduko ukatuta geldi.

Ondarrutar oriek zuten pantesia
olatua popatik txalupan grazia,
atzetik or eldu da txalupa bestea
gaizki egin zaioten popatik eltzea.

Popatik eldu eta arrauna kentzera,
orduan artu zuen txalupak bestera;
oiek baiñon gutxiago gu bat ere ezkera
¡zer lotsa daramaten oiek erbestera!

Erbestera joanda endreruan asi,
ederrak ematea zutela merezi,
apostu irabazten oiek ez nau utzi:
Lekeitiarrak ziran Getarian nagusi.