---
id: ab-6003
izenburua: San Migel Dogu (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006003.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006003.MID
youtube: null
---

San Migel dogu geure patroia,
yarraitu ondo berari,
zapalduteko zintzo burua
infernuko dragoiari,
mundu onetan egin eztaigun
sekula gatxik inori.
San Migel aingerua
zeru altuetan
dago gure arimak
errezibietan.