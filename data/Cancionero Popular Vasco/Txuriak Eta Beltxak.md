---
id: ab-407
izenburua: Txuriak Eta Beltxak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000407.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000407.MID
youtube: null
---

Txuriak eta bel txak men dian ardiak
zuk ere eztituzu bentaja guztiak.
La la ra la la la la ra la la la ra la ra la
la la ra la la la la ra la ra la la la la.

Beltxarana nai zela zuk o men diozu,
enaiz zuri ederra, egia diozu.
La la ra la la la la ra la la la ra la ra la
la la ra la la la la ra la ra la la la la.