---
id: ab-393
izenburua: Orra Errekaldeko Iru Neskatila
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000393.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000393.MID
youtube: null
---

Ora errekaldeko iru neskatila
kalabazea yanda lodituten dira.
¡Oi ai oi! lodituten dira
¡Ai oi ai! iru neskatila

Bata da yalperra ta bestea nagia.
orra irugarrena erritxal garbia.
¡Oi ai oi! bestea nagia
¡Ai oi ai! Erritxal garbia

La ra la ra la ra la la ra la la la la
la ra la ra la ra la la la ra la la l.