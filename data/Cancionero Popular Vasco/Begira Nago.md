---
id: ab-113
izenburua: Begira Nago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000113.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000113.MID
youtube: null
---

Begira nago, begira,
or goiko bide barrira,
noiz etorriko etedan
maitea neure errira.