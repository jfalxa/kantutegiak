---
id: ab-1147
izenburua: Oies Errondan Dabil
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001147.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001147.MID
youtube: null
---

Oies errondan dabil,
berak baki nundik,
Karmen gerri-politen
ate-ondotxutik. Oies:
sarritxutan errondan
ibili zaitekez.

Itxasoko aldetik
dauko bentanea,
andixek erasten dau
Karmenek sokea. Oies:
dirua emon nai ta
iñork artu nai ez.