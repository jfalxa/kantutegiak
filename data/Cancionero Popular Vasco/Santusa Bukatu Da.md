---
id: ab-1058
izenburua: Santusa Bukatu Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001058.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001058.MID
youtube: null
---

Santusa bukatu da,
Meza sagaran sartu da,
¡oi bekatari tristea!
Altxa diezak burua;
Jesusen adoratzeko
oraintxe yo duk ordua.

Kaliza altxatzearekin
mementu artan nik beti
eskuz yotzen dut bularra!
Badakit oben dula,
egin ditudan ofentsak
Jaunak barka dietzadala.