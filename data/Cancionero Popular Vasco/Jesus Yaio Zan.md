---
id: ab-1231
izenburua: Jesus Yaio Zan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001231.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001231.MID
youtube: null
---

Jesus yaio zan bainan
lenagotik aritu zan
Profetaren agotik
bazetorrela Jesus mundura
dontzella eder baten erraietara,
erraietara.