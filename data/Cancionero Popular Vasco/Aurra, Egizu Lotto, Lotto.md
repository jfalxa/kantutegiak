---
id: ab-261
izenburua: Aurra, Egizu Lotto, Lotto
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000261.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000261.MID
youtube: null
---

l.- Aurra, egizu lotto, lotto
nik emanen gaur bi kokotto
lore arrteko yinkoilo tsuri
atzemain dizut gero nik zuri.

- Euli gereka mizkalariok
arrgita aurrzale asperr ezinok
¿zeri zakioz burrun ta burrun?
Zoazte emendik urrun bai urrun.

- Aurra lo gozo gozoak arrtu
dezan oro isil egon bearr du;
amak txoil txoilik daki txintatzen,
txorik ere eztu lokarrarazten.