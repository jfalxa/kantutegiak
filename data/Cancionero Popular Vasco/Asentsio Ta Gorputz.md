---
id: ab-867
izenburua: Asentsio Ta Gorputz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000867.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000867.MID
youtube: null
---

Asentsio ta gorputz eta San Juanak
elkarren artean antzekoak.
Otaba ta danak ¡oi! nekazariari para dizten lanak!
¿zer pentsatzen otedu Jesus gure Jaunak?
Noiz bait konbertitu du semea bere amak.

San Juan asi zaigu kolera onean,
orrek beti segitzen du asten dan lanean.
Beti tulubioak zazpi egunean
gero aita San Pedro etorri danean
islak txukatu ditu biaramonean.