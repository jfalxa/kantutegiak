---
id: ab-6019
izenburua: Erregeak Datoz (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006019.MID
youtube: null
---

Erregeak datoz,
Erregeak datoz
Bretxan barrena,
Bretxan barrena,
ikatza erostera,
ikatza erostera
txanpon batena,
txanpon batena.
Aguinaldo.