---
id: ab-885
izenburua: Gabaz Zegoen Txit Argi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000885.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000885.MID
youtube: null
---

Gabaz zegoen txit argi,
zeren bait zegoen ilargi.
Dona Marrtirin goiz meza entzunik
andik yo nuen Baigorri,
atera ziren guarda bi
ene denborain galgarri.

Ama Birjiña fidela, alabatua zirela:
ene mandoak atera ituzu minikan gabe librorat,
xangrin bat artuut ederra: ala konbeni bideda.