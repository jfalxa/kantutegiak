---
id: ab-981
izenburua: Agur Maria (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000981.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000981.MID
youtube: null
---

Agur Maria graziaz betea
Jauna dago zurekin
bedeinkatua zu zera
andre guztien artean
ta bedeinkatua da
zure sabeleko frutua Jesus.
Santa Maria birjina,
Jaungoikoaren Ama,
erregutu ezazu gu pekatariokatik
orain eta geren eroitzeko ordu
estu artan Amen,
eriotzeko ordu estu artan Amen.