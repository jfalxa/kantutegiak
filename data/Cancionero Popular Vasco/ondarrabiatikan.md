---
id: ab-925
izenburua: Ondarrabiatikan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000925.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000925.MID
youtube: null
---

Ondarrabiatikan Tolosara (y)arte
zazpi ofizialek errekista naute,
zazpiok ezagutzen itz batean daude,
libre zala mundutik urte asko gabe.

Nere Jesus maitea, dei nazazu ¡arren!
Zere konpaiñiara zu non ere zaren,
San Pedrok sartuko nau atarian barnen,
ala gerta dadiela Biba Jesus Amen.