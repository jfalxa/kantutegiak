---
id: ab-727
izenburua: Dondon Kandel
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000727.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000727.MID
youtube: null
---

Dondon kandel
Barriola Barrriola plist
Aitona Martxelon
Mispi ta Mispi kaska melo.