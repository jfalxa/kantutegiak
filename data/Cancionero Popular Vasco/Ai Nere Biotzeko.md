---
id: ab-532
izenburua: Ai Nere Biotzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000532.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000532.MID
youtube: null
---

Ai nere biotzeko
begi urdindune,
Berastegiko kalean.
ondo ezagune.
Zurekin espero nuen
munduko portune,
neretzat ilundu dire
gaba ta egune.