---
id: ab-6036
izenburua: San Pedro Zeruetako (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006036.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006036.MID
youtube: null
---

San Pedro zeruetako
giltzaren yaubea
zeurea txalupea
neurea sarea
Antonek badaki
Antonek badaki...etc.