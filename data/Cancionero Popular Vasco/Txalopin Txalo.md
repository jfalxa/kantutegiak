---
id: ab-297
izenburua: Txalopin Txalo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000297.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000297.MID
youtube: null
---

Txalopin txalo txalo ta txalo,
katutxoa mizpila ganean dago;
badago, bego; bego, badago;
zapatatxo barrien begira dago.