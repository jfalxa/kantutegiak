---
id: ab-140
izenburua: Iguzkiaren Pare
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000140.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000140.MID
youtube: null
---

Iguzkiaren pare
zoragarri zirade,
ni haur nuzu yakile,
enauke erran gabe.
Osotasun oroz
zira ornitua
¿laket zinuke
ya enekin ezkontzea,
zuk ene maitea?
Erradazu, othoi, egia.