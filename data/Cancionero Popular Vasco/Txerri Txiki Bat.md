---
id: ab-938
izenburua: Txerri Txiki Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000938.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000938.MID
youtube: null
---

Txerri txiki bat erosi nuen
bizimoduan sartzeko,
irur peseta pagatu nuen
kasta onetik zelako,
sastabinaren zulotik joanta
ezta (y) ageri yagoko.