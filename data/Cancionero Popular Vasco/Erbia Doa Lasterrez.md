---
id: ab-627
izenburua: Erbia Doa Lasterrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000627.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000627.MID
youtube: null
---

Erbia doa lasterrez
ihiztarien beldurrez;
herri hunetako neska gazteak
urturik daude nigarrez
ezkon eztiten herabez
ezkon eztiten herabez (1)

Usoa dabil egalez
elikaturik pago alez;
etxe hunetako ezkonberriak
lertzen ari dire parrez,
zoria galezina ustez
zoria galezina ustez.

(1) beldurez decía otra vez el cantor. El folklorista puso este lindo
vocablo, que significa "con temor", así como beldurrez "con
miedo".