---
id: ab-166
izenburua: Ni Zuregana
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000166.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000166.MID
youtube: null
---

l.- Ni zuregana yoaten nintzanean,
atea giltzez ertsirik
¡ameka aldiz igaro izan dut
bide luzea alperrik!
Eztut ikusi eztut aditu
neor ain azkarr ta indarrtsurik,
zureganako onerespen au
neri kenduko didanik.
- Agurr, maitetxo, ni banoakizu
¡¡ azken itz onen itzala !!
Il ezpanendi agertuko naiz;
agurr, maitetxo laztana.
Amak semetzat enauela nai
¡¡ ta zuk ni oil zure ondotik !!
Ni il banendi, bizi zaitea
iletaz beti yantzirik.