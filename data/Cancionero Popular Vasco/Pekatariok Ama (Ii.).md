---
id: ab-1056
izenburua: Pekatariok Ama (Ii.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001056.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001056.MID
youtube: null
---

Pekatariok Ama, Birjina, zaitugu,
arren zeure semetzat eskojidu gaizuz.
Egizu zeuk Maria, gugaitik erregu
eriotzako orduan ezkaitezan galdu.