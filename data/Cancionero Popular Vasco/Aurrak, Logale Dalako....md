---
id: ab-263
izenburua: Aurrak, Logale Dalako...
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000263.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000263.MID
youtube: null
---

ADAPTACIÓN del folklorista:

Aurrak, logale dalako,
lotxo nai luke egin,
begi bat ertsia duta
bestea zabal ezin.

Logale da gure aurra,
seaskarik bat eztu,
San josek, zurgina data,
orrentzat egingo du.
Oba, Oba (1).