---
id: ab-274
izenburua: Campo De Volantiñen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000274.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000274.MID
youtube: null
---

Campo de volantinen
aretxarro baten
kukuak azi dituz
iru ume aurten
kukuak azi eta
txamilotxeak yan,
a bere kukuaren
desportunea zan.