---
id: ab-147
izenburua: Jose Joakin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000147.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000147.MID
youtube: null
---

Jose Joakin nere laguna
zor dizkitzut esker mila
¿Seaskatikan etortzen alda
dontzelatasun guzia?
Nik berdinean nayago nuke
amak besoan azia.