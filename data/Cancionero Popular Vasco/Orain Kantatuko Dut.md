---
id: ab-1051
izenburua: Orain Kantatuko Dut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001051.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001051.MID
youtube: null
---

Orain kantatuko dut
nik Agur Maria,
onetatikan dator
gure alegria.
Esaten degunean
graziaz betea,
kontentuz gelditzen da
Birjina maitea.
Nai luke beregana
gu eramaitea,
irikiko liguke
zeruko atea,
Jauna dago zurekin
bedeinkatua zu,
bertute miragarriak,
Maria, dituzu.
Andre guztien artean
zaude eskojitua,
zure sabeleko Jesus
frutu bedeinkatua.