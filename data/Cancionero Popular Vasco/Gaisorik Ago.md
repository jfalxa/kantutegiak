---
id: ab-360
izenburua: Gaisorik Ago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000360.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000360.MID
youtube: null
---

Gaisorik ago etika ori, agiri dona antzean;
izerdi on bat artu bearkon geure kortako saspean.
Geure kortako saspean zazpi txarrien artean
amilu moltso bat eperdian da ni ilentiaz atzean:
orduantxe bai iku isikogu zein arinago garean.