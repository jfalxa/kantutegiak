---
id: ab-858
izenburua: Agur Agur Juantxo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000858.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000858.MID
youtube: null
---

Agur agur Juantxo
¿zer modu daukazu?
Probe ta alegera
¿zer esan biozu?
Beste interesarik
munduan eztozu.
Soka bat ekazu,
lotu bear dozu;
baina ezin lotu:
agintzen deuskuena
egin bear dogu.