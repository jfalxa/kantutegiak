---
id: ab-359
izenburua: Eskalapoin-Dantzan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000359.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000359.MID
youtube: null
---

Eskalapoin dantzan, Baigorriko plazan,
zagi bat ardo pontzan rrau rrau rrau.

Atsoño ta agure yaukiza ari dire
atsa dutenarte rrau rrau rrau.
Rrai ta rai rrai ta rai rrai ta rai rrai ta rai
rrai ra ra ra rrai ta ra ra rrai rrai rrai

Iru urrats orrera, iru urrats onera,
itzuli bat gero rrau rrau rrau.

Unela urtez urte dantzatu oi dute
zargaz tek jaiero rrau rrau rrau.
Rrai ta rai rrai ta rai rrai ta rai rrai ta rai
rrai ra ra ra rrai ta ra ra rrai rrai rrai

Unela urtez urte dantzatu oi dute
zargaztek jaiero, rrau-rrau-rrau.
Rraitarai, rraitarai..., etc.