---
id: ab-1033
izenburua: Kantatutzera Noa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001033.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001033.MID
youtube: null
---

Kantatutzera noa
ni Abe Maria,
orretatikan dator
gure alegria.
Alabatu nai nuke
Birjina Maria
Jesusen Ama maite
dontzella garbia:
alkantza degigula
zeruko gloria.