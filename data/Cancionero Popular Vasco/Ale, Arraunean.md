---
id: ab-948
izenburua: Ale, Arraunean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000948.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000948.MID
youtube: null
---

Ale, arraunean bagere
Ale, palan palan bagere.
¡Eup! Ale, mutilak;
palan palan bagere.
Ale, mutilak arraunean bagere.
A mutilak San Antonen bagere.
A mutilak palan palan bagere.
¡Eup! Aurrera, arraunean bagere.
Aurrera palan palan bagere.
Aurrera oraintxe bai mutilak
aurrera bagere.
Aurrera oraintxe bai mutilak
aurrera bagere.