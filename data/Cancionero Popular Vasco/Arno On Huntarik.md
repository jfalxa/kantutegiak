---
id: ab-197
izenburua: Arno On Huntarik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000197.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000197.MID
youtube: null
---

Arno on huntarik beirea beterik,
lagun,edan dezagun:
ene pena xangrinak hunek sendatzen tik,
zer nahi duk amodiotik,
hark erepenak baitik ondotik:
trai lai larala lai larala lai to, lagun, sinets nezak.
Andreak alde bat utz eizak,
libertatea eta bakea hoberestzak,
utz ezak betiko Venus eta har laguntzat
Bakus opa dayat bizi on bat
eremanen dik hola yarraituz.