---
id: ab-879
izenburua: Etxera Etorrita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000879.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000879.MID
youtube: null
---

Etxera etorrita estelen goizetan
etxekoak aserre ¡an kontuak ziran!
ia errian nintzen igande arratsean
gezurrik ezin esan testiguak baziran.

Etxera etorrite lenengo pregunte:
artzaie ¿ardi oiek ondo yan aldute?
- Eztiet galarazi ondoan yarrite;
ni aski ezbanintzen Pettan an alzuten.

Lorentza ta Joxepa, Mari Juan ta Pettan
aserre omenziren igande-arrats artan.