---
id: ab-1194
izenburua: Ana Ta Joakin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001194.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001194.MID
youtube: null
---

Ana ta Joakin, Peru ta Martin
goazen zortziko dantzara,
gabon gabean zoro eztana
zororik andiena da.
La ra la ra la ra ...e.a.

Portala zarra loraz beterik
gabon gabaren erdian
marabillea jaiotzen dago
Jaungoikoaren semea.
La ra la ra la ra...e.a.