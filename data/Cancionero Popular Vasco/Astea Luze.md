---
id: ab-199
izenburua: Astea Luze
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000199.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000199.MID
youtube: null
---

- Astea luze gan dun Maria
¿non dun asteko matazaria?(1).
- Matazaño bat in diat eta
tabernarako erdia.
- ¿Gainerakoa non dun Maria,
zertaz izain naiz nerau yantzia?
- Sukalondoan nengoelarik,
erre zitakan bertzea.
- ¿Non dun ardatza, non dun linaya,
len or baitziren sukaldean?
- Linai-ardatzak erabiltzean (2)
min artzen diat beatzean.
- Irule baino edale azkar
azkarrago aiz, emaztea.
- Bonbil-ardoaren (3) itzultzean
atsein artzen dut biotzean.

(1) Aria, decía el original con dos sílabas de menos.
(2) Ardatzaren ibiltzean con falta de otras dos.
(3) Baso-ardoaren cantaba el agote Santxotena.