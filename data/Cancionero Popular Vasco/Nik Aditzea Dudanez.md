---
id: ab-836
izenburua: Nik Aditzea Dudanez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000836.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000836.MID
youtube: null
---

Nik aditzea dudanez,
Leitzan mutil finik ez,
yolas ederrak badituzte
ala balira biotzez!
Etzaituzte erreko
bereala ur otzez.