---
id: ab-746
izenburua: Mox Mox, Katuko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000746.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000746.MID
youtube: null
---

MOLDAKETA
Mox mox katuko
¿noiz aiz eltzeko,
aur edertxo y onekin yostatzeko?
Erpe zorrozko
oik eretzako
gorde itzak, aurrak negar ez eiteko.