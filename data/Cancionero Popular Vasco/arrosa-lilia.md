---
id: ab-952
izenburua: Arrosa-Lilia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000952.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000952.MID
youtube: null
---

Arrosa lilia lore
¿nork bear du senarrik?
Bat ere bat ere eztut,
bat bear nuke nik.
Tomas lagunartetik
emaiten dizut nik,
Tomas lagunartetik
emaiten dizut nik.