---
id: ab-1281
izenburua: Zorioneko Yorduan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001281.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001281.MID
youtube: null
---

Zorioneko (y) orduan
bartako gau erdian
yaio da Manueltxo
Belengo errian,
gloria zeruan da
bakea lurrean
gure salbadorea
etorri danean,
lenago egon gera
etsaien mendean:
graziak orain Jesus
libre gaudenean.
Jesus yaio da ta
goazen ikustera,
pozez ta atseginez
musika ematera,
alegera gaitean
guztiok batera,
Jaungoikoa daukagu
yatsirik lurrera.