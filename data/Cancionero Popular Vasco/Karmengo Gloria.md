---
id: ab-1034
izenburua: Karmengo Gloria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001034.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001034.MID
youtube: null
---

Karmengo gloria,
aien loratua,
zeruko argia,
Ama Birjina singularea.
Ama piadosa,
gizonik ezagutu gabea,
Karmelitari eman diezu pribilejioa,
izarra itsasokoa.
Birjina Amandrea,
esperantza geurea,
orbanik etzuen
bere purezan
Birjina gelditu zen.
Erregina soberana,
gure esperantza eta alibioa,
bekatariari zabal egiozu,
Birjina Amandrea,
zeruetako atea.