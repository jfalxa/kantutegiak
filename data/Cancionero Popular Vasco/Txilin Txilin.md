---
id: ab-939
izenburua: Txilin Txilin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000939.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000939.MID
youtube: null
---

Txilin txilin txilin txilin
txilin txilin txoria,
olajaunak egin eidabe
kontzilio berria,
ikazkinai kenduteko
pagarik erdia.