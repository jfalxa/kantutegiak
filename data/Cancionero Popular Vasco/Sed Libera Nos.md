---
id: ab-244
izenburua: Sed Libera Nos
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000244.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000244.MID
youtube: null
---

Sed libera nos a malo
sit no men Domini
vamos a cantar un canto
para divertir
Yan dugunez gerozti
xahalki huneti
eta edan ardao
durunzunekoti,
chantons mes chers a mis
jesuis con tent par ti
trin quam d'aquest bon vi
eta dezagun kanta
kantore berr.