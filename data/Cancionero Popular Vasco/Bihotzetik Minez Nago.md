---
id: ab-117
izenburua: Bihotzetik Minez Nago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000117.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000117.MID
youtube: null
---

Bihotzetik minez nago,
ezta ageri zauririk;
ikhustera yin balekit
nik nahi dudan barberik,
bertan bertan sendo naite
eta libra gaitzetik.