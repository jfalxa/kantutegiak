---
id: ab-535
izenburua: Aita, Saldü Naizü
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000535.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000535.MID
youtube: null
---

Aita, saldü naizü
idi bat bezala
eta abandonatü
ezpaninduzün bezala,
ezpaninduzün bezala.