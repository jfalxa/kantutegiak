---
id: ab-798
izenburua: Gizona, Goazen Isilik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000798.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000798.MID
youtube: null
---

Gizona, goazen isilik pakean oiera,
eguna argitutzean konponduko gera,
kreditu ederren bat bazoaz artzera.

Betor matsaren kasta bere adarrakin,
enazu ikaratuko zure indarrakin
gizona geldi dedin kredituarekin.

Gizonak galdetzen du ¿non da ardo ona?
Ardaoak erantzuten du "emen nago, jauna".
- Neuk artu nai zinuzket betiko laguna,
beldur gabe pasatzeko gaua ta eguna.

Eztago filosoforik ez teologorik
ardoari neurria artuko dionik,
gizon andiak ere ikusten ditut nik
beren mozkorra ezin disimulaturik.

Preso para ninduten upela betean,
geroztik emen nago sei illabetean,
- Nik lagunduko dizut amodio onean.
- Begira izan eztedin zuretzat kaltean.