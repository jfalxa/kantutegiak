---
id: ab-631
izenburua: Goizean Gelan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000631.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000631.MID
youtube: null
---

Goizean gelan, arratsean gelan
beti gela ¡gelaren iluna!
eztet nai nik alargunik senartzat,
mutil gazterik erian delarik.
Maitañoa: genteak kejo dire
suk eta nik eztegula etxerik;
alako aiek emango aldigue
sobraturik dauzkienetarik.