---
id: ab-231
izenburua: Maritxu, Maritxu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000231.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000231.MID
youtube: null
---

Maritxu, Maritxu, goxo zále zera zu;
Maritxu, Maritxu: bizitzen badakizu.
Goizean goizean txokolatea
arratsalderdian koipatsu,
Ama ta aizparentzat porrusalda ta
zerorrek edaritzat juju.
Maritxu, Maritxu, goxozale zera zu,
Maritxu, Maritxu, bizitzen badakizu.

Txori batek kantatzen du
aritz adar gainean
bera eztala ezkonduko
aurtengo udazkenean,
arto gari sekaleak
merke eztetiran artean.
Maritxu, Maritxu, goxo zale zera zu
Maritxu, Maritxu, bizitzen badakizu.

Maritxu, Maritxu, goxo zále zera zu
Maritxu, Maritxu, bizitzen badakizu.
gabean bigiran eta
biaramonean lozurru,
Ama ta aizpatxoak etxe-lanetan
izerdi-patsetan aiduju:
Maritxu, Maritxu, goxo zalezera zu
Maritxu, Maritxu, bizitzen badakizu.

Gau ilunez ontza dabil
teilaturik teilatu,
bigiretan zeratenon
gezur egiak batu
ta lagunai banatzea
eztezakela asmatu.
Maritxu, Maritxu, goxo zale zera zu,
Maritxu, Maritxu, bizitzen badakizu.