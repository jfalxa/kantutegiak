---
id: ab-983
izenburua: Aingeru Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000983.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000983.MID
youtube: null
---

Aingeru batek Mariari
dio graziaz betea.
Agur Maria, zeruko Erregiña,
Agur Agur Ama maitea.