---
id: ab-6008
izenburua: Arrosa Xuriaren Azpian (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006008.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006008.MID
youtube: null
---

Arrosa xuriaren azpian
anderea lokhartu
elurra bezain txuri,
ekhia bezain ederrik,
hirur kapitainek zeraukaten
gortez inguraturik.