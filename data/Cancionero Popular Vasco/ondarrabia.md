---
id: ab-390
izenburua: Ondarrabia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000390.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000390.MID
youtube: null
---

l.- Ondarrabia erri txikia, erdian plaza zabala.
Ondarrabia erri txikia, erdian plaza zabala:
alargun batek or emen dauzka ederrak iru alaba:
A, a, a! Isabela, Manuela ta argarita
Peru utz izkizak Peru utz izkizak Peru utz izkizak.

- Biguñ biguñik azaldu nion bat eman bearzidala.
Biguñ biguñik azaldu nion bat eman bearzidala:
amorrek ilun eran tzun zidan mutil txiroa nintzala:
E, e, e! Isabela, Manuela, Margarita ere
Peru utzi aute Peru utzi aute Peru, utzi aute .