---
id: ab-172
izenburua: Ortzilaretan Dute
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000172.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000172.MID
youtube: null
---

Ortzilaretan dute Gardozen merkatu
neure maite polita an nuen opatu.
Pot bat galde egin nion txapela eskuan,
bida eman zaizkidan nigarra begian.

¡Ai ei! ok dire penak nik orai tudanak!
bi maite izan eta ez jakin zein auta.
Batek dizu txapela, besteak boneta,
txapeldunarengana doa ene biotza.