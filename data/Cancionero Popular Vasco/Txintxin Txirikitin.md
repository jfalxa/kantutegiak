---
id: ab-1172
izenburua: Txintxin Txirikitin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001172.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001172.MID
youtube: null
---

Txintxin txirikitin txorie
txoritxo txilibitxrie
erein neuen garie guztiz arlo andie,
aun anega itxaron neuen,
imina bat zan guztie,
txoritxo txilibitarie.

Etorri djatan auzoko andrea
emon naiola erdie,
erdiai bere gitxi eretxi
emon neiola guztie,
txoritxo txilibitarie
txintxin txikitin txorie.