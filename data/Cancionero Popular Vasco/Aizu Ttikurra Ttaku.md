---
id: ab-702
izenburua: Aizu Ttikurra Ttaku
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000702.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000702.MID
youtube: null
---

Aizu ttikurra ttaku ni enaiz eroa,
sardin burua baiño obe dik oiloa.

Senartzat, naizelako neskatxa zurra,
gaztea nai dut eta ez zar makurr.