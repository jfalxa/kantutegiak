---
id: ab-1019
izenburua: Jaungoikoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001019.MID
youtube: null
---

Jaungoikoak salba zaiz
ala Ama Erregiña Maria,
miserikordiazko Ama,
guztiz da miragarria,
konfiantza osoarekin
nago zugana jarria.