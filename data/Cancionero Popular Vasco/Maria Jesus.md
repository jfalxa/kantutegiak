---
id: ab-1242
izenburua: Maria Jesus
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001242.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001242.MID
youtube: null
---

Maria Jesus, Jesus Maria, Maria Jesus.
Abendu santua da ta
ia da denporea,
orain kontdu daigun
Jesusen jaiotzea.
Maria Jesus, Jesus Maria, Maria Jesus.

¡Au dala jaio jaio
au dala jaiotzea!
Semea jaioezkero
ama da dontzellea.
Maria Jesus, Jesus Maria, Maria Jesus.

Ama Birjinea
¿nun dozu semea?
- Orra nun daukazun
Belengo portalean.
Maria Jesus, Jesus Maria, Maria Jesus.

Josepe ¿zer ordu da?
- Maria, gaberdia;
zeruko izar ederra
laster dogu parean.
Maria Jesus, Jesus Maria, Maria Jesus.

Berandu da ze ondo litzake
kuma eder bat bageuko,
eragiteko Jesus onari
uauatxoa lo ta lo.