---
id: ab-365
izenburua: Jai-Egunez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000365.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000365.MID
youtube: null
---

Jai egunez txakolinez
barrua bete ezkero,
abarkadun auzola gun
guztiok gagoz bero.
Txakolin, txakolin, txakolin eta txuzpin,
txakolin, txakolin, txakolinak on egin.

Matsurina pitxar bina
edanarren zutunik;
batzuk motel motelik;
irradari, santzolari
ager gara zororik.
Txakolin, txakolin, txakolin eta txuzpin,
txakolin, txakolin, txakolinak on egin.

Bi ailara kurtzetara
lurrean irarri ta
ikotika artazika
irradan bat astenda.
Txakolin, txakolin, txakolin eta txuzpin,
txakolin, txakolin, txakolinak on egin.

Batzuk uju, besteak juju,
berein agura an dira
bekaitzarren begira,
bizkor ta azkar lena bakar
dabil jira ta bira.
Txakolin, txakolin, txakolin eta txuzpin,
txakolin, txakolin, txakolinak on egin.