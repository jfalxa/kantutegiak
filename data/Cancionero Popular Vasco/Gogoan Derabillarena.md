---
id: ab-960
izenburua: Gogoan Derabillarena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000960.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000960.MID
youtube: null
---

Gogoan derabillarena
neri onek ekarri,
nik orain gogoan
Patxiko eruki.
Sarri sarri emango nioke
Kattalin Iurteti (1),
ondotxo goza dezala
besoetan polliki.