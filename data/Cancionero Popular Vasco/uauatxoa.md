---
id: ab-302
izenburua: Uauatxoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000302.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000302.MID
youtube: null
---

Uautxoa lo ta lo,
zuk orain eta nik gero;
biok batera lo egiteko
eguna lar bero dago
Lo.
Neure seme polit au azita baneuko,
aitari tabernatik ardau a ekarteko,
amari esantxoak zeatz egiteko
¡Lo, lo.