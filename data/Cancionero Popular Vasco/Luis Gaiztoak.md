---
id: ab-911
izenburua: Luis Gaiztoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000911.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000911.MID
youtube: null
---

Luis Gaiztoak eman lenengo arponada,
balea arrapatzeko txit gizon ona da;
bederatzi milla erreal zutela jornala,
estimatzekoa zan orduko arponada.

Jose Karamelo zan bigarren golpean,
ederki portatu zan bere suertean,
baleari odola bizkarren betean
etzitzaion galditu ito zan artean.

Jose Manuel Serenok indarrak baditu,
aren zartadakoa Getarian aditu,
jende egin zuen orduan arritu:
izugarrizko famak ark ekarri ditu.