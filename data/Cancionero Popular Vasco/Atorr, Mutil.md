---
id: ab-200
izenburua: Atorr, Mutil
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000200.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000200.MID
youtube: null
---

Atorr mutil etxera
gaztaina zimelak yatera
gabon gabon igarotera
aitaren ta amaren ondoan;
ikus iko gu aita barreka
ama be pozkorr pozkorrik.
Gaztainak errearrtean
oskol a lerrtu artean
zipli zapla plost
gabon gaba igaro daigun zoritsu.