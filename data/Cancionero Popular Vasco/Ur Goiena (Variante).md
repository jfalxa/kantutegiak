---
id: ab-6032
izenburua: Ur Goiena (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006032.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006032.MID
youtube: null
---

Ur goiena ur barrena
urte berri egun ona.
Urte berri egun ona
Jainkoak digula egun ona.
Etxe onetan sar dedila
bakearekin osasuna.