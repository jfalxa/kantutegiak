---
id: ab-764
izenburua: Aker Isaso Majoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000764.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000764.MID
youtube: null
---

¡Aker isaso majoak
duen okela gisoa!
Bere biziko gisadu zaion
agurea ta otsoa
day aren bilintzikoak!
Etzituk errez arrapatuko
erren dabilen atsoak.