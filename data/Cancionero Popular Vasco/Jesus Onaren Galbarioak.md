---
id: ab-1027
izenburua: Jesus Onaren Galbarioak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001027.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001027.MID
youtube: null
---

Jesus onaren galbarioak bigarreneko pausua debozioaz
pentsa dezagun, zeren den lastimosoa.

Gurutze pisu izugarri bat ipinirikan soinean,
Jesus gurea paratu zuten Galbariora bidean.