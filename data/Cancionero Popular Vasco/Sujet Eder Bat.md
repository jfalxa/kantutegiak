---
id: ab-611
izenburua: Sujet Eder Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000611.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000611.MID
youtube: null
---

Sujet eder bat deklaratzen da gure artean gertatzen dena,
aditzeak berak emaiten deraut nere biotzean pena.
Guztiak konpreni dezaten laur urteko aur batena.
Gisa ontan martir iltzeko ¿gaixoak non zuen obena?.

- Aurraren Aita gertatzen da
gaitz batez akabatua.
Kontzertatu ere izan du
bere dotor medikuak,
jende-urinez frotatzeko
eman dio konseilua,
bertzela eztela izanen
gaitz artarik sendatua.