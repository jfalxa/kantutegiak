---
id: ab-1183
izenburua: Zubi Berrian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001183.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001183.MID
youtube: null
---

Zubi berrian gera ginaden
astelen iluntze baten;
boz mudatuak aditu eta
yarririk erreparatzen,
Monjen paretan zulo batean
ontza zegoan kantatzen. U U.

Non zegoan ondo begira eta
eman gendion arrika;
ala ere etzan, etzan mugitzen
guri begira yarrita
lanik asko artu genduan
eskalerak ekarrita. U U.