---
id: ab-534
izenburua: Aita Nuen Saltzaile
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000534.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000534.MID
youtube: null
---

Aita nuen saltzaile,
Ama diruren artzaile,
neure anaia Bernardo
moru errira entregatzaile;
neure anaia Bernardo
moru errira entregatzaile.

- Saldu nenduen dirutan,
dirutan ere aunitzetan;
neunek pisaala urretan
eztitako bi kupeletan (1).

- Neure alaba Miarrez
¿zer dun horrela nigarrez?
ire yauntziak eginik tziauden
urregorriz eta zilarrez (2).

(1) Bi ezti-kupeletan, dijo el cantor.
(2) Urrearekin zilarrez, decía el original.