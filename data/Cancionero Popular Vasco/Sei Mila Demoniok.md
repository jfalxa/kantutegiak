---
id: ab-399
izenburua: Sei Mila Demoniok
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000399.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000399.MID
youtube: null
---

Sei mila demoniok eraman din neska
gona gorri ori ¡ori da bai ori!
Sei mila demoniok eraman din neska gona gorria
Bai ¡ala Jainkoa! neskatxa pijoa,
nik ori ondo jakin, Praiska katalin.
¿Non dira bada, non dira? emen agiri eztira;
ezpata soiñua aditu eta San Bizentera joan dira.
Txibiribiri ¿nork kantatzen du kantuori?
Txori txiki batek arbola gainean txibiribiribiri
Ezpata soñua aditu eta San Bizentera joan dira.