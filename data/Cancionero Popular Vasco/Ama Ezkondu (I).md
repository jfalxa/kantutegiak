---
id: ab-1077
izenburua: Ama Ezkondu (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001077.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001077.MID
youtube: null
---

Ama ezkondu; ama, ezkondu, gaztea naizenartean.
- Neska, ago isilikan, etzion ogirik etxean.
- Galburutxo bi gorde nituen garia genduenean,
eun anega atera nizkan irriarra batean:
Urra labirun labirun pena garia genduenean,
Urra labirun labirun pena irriarra batean.

- Ama ezkondu; ama, ezkondu, gaztea naizenartean.
- Neska, ago isilikan, etzion ardorik etxean.
- Masmordotxo bi gorde nituen matsa genduenean,
eun karga atera nizkan estutu-aldi batean:
Urra labirun labirun pena estutu-aldi batean,
urra labirun labirun pena matsa genduenean.

- Ama ezkondu; ama, ezkondu, gaztea naizenartean.
- Neska, ago isilikan, etzion aragirik etxean.
- Bein batean jarri nintzan josten labe-gainean,
txantxangorri bat etorri eta eseri aldamenean,
jaurti nion titara eta ausi nion bernea,
eun erralde pisatu zuen aren bularraldeak:
Urra labirun labirun pena estutu-aldi batean,
urra labirun labirun pena eseri aldamenean.