---
id: ab-364
izenburua: Gorako Saltoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000364.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000364.MID
youtube: null
---

Gorako saltoa ta berako brinkoa,
gorako saltoa ta berako brinkoa,
mutilak goian goian galtza gerrikoa
Trontxolari, trontxolari, trontxolari lena
ari arin arin arin itzuli morena.