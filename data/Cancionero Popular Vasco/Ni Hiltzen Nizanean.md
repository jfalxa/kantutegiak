---
id: ab-235
izenburua: Ni Hiltzen Nizanean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000235.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000235.MID
youtube: null
---

Ni hiltzen nizanean
trala la ra la la ra la la;
ni hiltzen nizanean,
ez ehortz Elizan,
ez ehortz Elizan;
bainan ehortz nezazue
trala la ra la la ra la la;
bainan ehortz nezazue
xai zola batean,
xai zola batean;
eta buruz barrikara
trala la ra la la ra la la;
eta buruz barrikara
ahoz dutxulu ra,
ahoz dutxulura.