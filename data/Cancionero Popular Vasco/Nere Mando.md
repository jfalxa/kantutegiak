---
id: ab-832
izenburua: Nere Mando
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000832.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000832.MID
youtube: null
---

Nere mando laztan laztan laztana:
buru baino gibelaxago du buztana
ene mandoak.
Abere tonto dirurik askorik
kostarik kokorikakoa (sic)
ene mandoa, ene mandoa.

Asto txiki txiki zar bat du lagun.
Mandoak irrintzi iten dionean,
arrantzaka oi du erantzun:
abere apal, iletsu, ulitsu,
urtetsu, ezurtsu gaixoak,
ene astoak, ene astoak.