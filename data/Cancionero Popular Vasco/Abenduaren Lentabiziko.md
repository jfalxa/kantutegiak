---
id: ab-855
izenburua: Abenduaren Lentabiziko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000855.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000855.MID
youtube: null
---

Abenduaren lendabiziko
goizeko zazpietan zen
tenplu santuan egondu nintzan
sermoi eder bat aditzen.
Jesusen llaga preziosoak
astero noa berritzen:
zeruko aita, lagun zaidazu
entendimentua argitzen.