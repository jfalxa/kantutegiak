---
id: ab-715
izenburua: Aurrak Aurrak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000715.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000715.MID
youtube: null
---

Aurrak aurrak aurrak ederrak aurrak
¿nongoak zerate?
Espainiako don Feliperen
seme alabak gerade.
Aurren gaixoa pasa liteke,
azken gaixoa preso de.