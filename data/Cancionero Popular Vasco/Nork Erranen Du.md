---
id: ab-237
izenburua: Nork Erranen Du
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000237.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000237.MID
youtube: null
---

Nork erranen du ni enaizela ostatuetan ibiltzen;
bai, ibiltzen, egoiten, horditzen;
hirugarren egunean etxera yoaiten;
hirugarren egunean etxera yoaiten.