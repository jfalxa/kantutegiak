---
id: ab-333
izenburua: Alduden Gizonik Ez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000333.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000333.MID
youtube: null
---

Alduden gizonik ez
eta Urepelen bai -bainan eta-
Aldupen gizonik ez
eta Urepelen bai.
Aldudeko gizonak batzuk tuzu itsuak,
bestek tuzu mainguak, bestek tuzu okerrak,
bestek tuzu konkorrak:
agorik ez begirik ez beharririk
bat eztuen hura
Urepelen da.

Ainoan neskatxik ez
eta Urdaxurin bai -bainan eta-
Ainoan neskatxik ez
eta Urdaxurin bai.
Ainoako neskatxak batzuk tuzu ezkelak,
bestek tuzu totelak, bestek tuzu motelak,
bestek tuzu ergelak:
mutxurdina ezkonmina, ehun urtez
bizitzeko dina,
Urdaxurin da.