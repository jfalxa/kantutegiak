---
id: ab-273
izenburua: Bulun-Bulun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000273.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000273.MID
youtube: null
---

Bulun bulun betik gora
doaz ardiak larrera,
ene aurr onentzat aukera,
ezne gozoa ekartzera.

Aurra lo dantzan artean
bazka zaitezte larrean;
itzarri dakidanean,
zatozte bulunba otsean.

Oraindik ezta luzaro
igo dirala emaro.
Ardi-bildotsak abaro-
lotxotan datzaz gozaro.

Nere bildots otzan oni
lo lo kantatzen nago ni;
biok atseden artzeko,
txoriak, txinta neroni.