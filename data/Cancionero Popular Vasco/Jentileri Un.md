---
id: ab-635
izenburua: Jentileri Un
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000635.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000635.MID
youtube: null
---

l.- Jentileri un ta konpaini un,
Jaungoikoak diola egun on.
Idiak doazi uztarritzera,
uztarri ori labratzera.
Nobio yaune ta andre nobie
oraintxe doazi fedatzera.
- Idiak daude uztarriturik,
uztarri ori labraturik.
Nobio yaune ta andre nobie
oraintxe daude fedaturik.