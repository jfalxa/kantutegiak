---
id: ab-139
izenburua: Iguzkia Ta Izarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000139.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000139.MID
youtube: null
---

Iguzkia ta izarrak, ilarrgia salbo,
zu bezalako damarik munduan eztago.
Oi ni zuri begira txoraturik nago,
amoriorik bade zu mintza zaitez klaro.

Aizu nere biotzeko, dama kuriosa,
gorputza polita dezu, pausua airosa.
Begia ere daukazu guztiz amorosa,
ojala bear ba zendu zuk neure esposa.