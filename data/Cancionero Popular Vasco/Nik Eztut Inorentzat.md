---
id: ab-922
izenburua: Nik Eztut Inorentzat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000922.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000922.MID
youtube: null
---

Nik eztut inorentzat gauza gaitzik esanen,
frantsesaren menean umil gaude emen.
Ark Jesus dionean guk erantzun amen,
bainan eztakit gero zer etorriko den.

Espaiñian sartu da milloi bat gizona,
billatzen ezta errez alako persona:
geiena erejez, guzia ladrona,
berrogei milla ezta bizirik egona.

Gure errege zarra desbenturatua,
gaizoak falta zuen entendimentua.
Erregiñagatikan Bayonan autua,
baiñan eztu izango arekin tratua.