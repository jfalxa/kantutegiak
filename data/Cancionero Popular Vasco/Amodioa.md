---
id: ab-768
izenburua: Amodioa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000768.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000768.MID
youtube: null
---

Amodioa amodioa
baldin ezpazait mudatzen,
egunentxo bat sañalatuta
biok esposa gaitezen.
Zori onean yarri zinaden
Mari lukainken serbitzen,
gona luzeaz orpo zikinak
ikasi uen garbitzen.

Aurpegi eder musu zabala
esku ederren yabea
¡¡azak eltzean garbitu gabe
sartzen ditunen urdea!!
Udaberriko lore ederrak
erortzen dire negura:
beti gustura biziko danik
ezta yayoko mundura.