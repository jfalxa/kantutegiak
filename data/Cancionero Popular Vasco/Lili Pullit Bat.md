---
id: ab-154
izenburua: Lili Pullit Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000154.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000154.MID
youtube: null
---

Lili pulit bat badut nik
aspaldi begiztaturik
bainan ez naite mentura
haren harrtzerat eskura,
zeren baitakit lanjera
hari behatzen sobera.