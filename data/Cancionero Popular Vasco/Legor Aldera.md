---
id: ab-585
izenburua: Legor Aldera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000585.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000585.MID
youtube: null
---

l.- Legor aldera zoazten
uginok ari erran:
bakar ezin dudala
bizia eraman.
Poza igeska doakit,
minak naduka baituran.
Artaz oroitzea bezait
argi ilun ontan.

- Arganontz aize ta odei
zirizara a lai duaz;
ari begira dakust
ilargi osoa.
¿Ni zer arren nabila
arengandik urrunduaz?
Oa, biotz, gaur i ere,
arganontz ona.