---
id: ab-1023
izenburua: Jesus Estali Zaio
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001023.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001023.MID
youtube: null
---

Jesus estali zaio
Birjina Amari,
galdera egin dio
bai San Joseperi.