---
id: ab-1037
izenburua: Kristinau Onak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001037.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001037.MID
youtube: null
---

Kristinau onak bear luke yaian-yaian pentsadu
aste guztian zeinbat bider ein etedaben pekatu.

Kristinau on bat illen danean egiten deba bi parte,
gorputza eroan elizara ta lurraz estalduten dabe.

Arima ona badoia eta nora dan iñork eztaki,
Paradisuko atean dago isilik eta tristerik.

Kristo gure Jaunak urteten deutso, dana piedadez beterik,
¿arima ona, zeuk zer dakazu zeure munduko gauzarik?

Umildadea, karidadea; Jauna, dana piedadez beterik,
- Orretxek biok dakazan orrek eztau besteren bearrik.

Aingerutxuak, artu egizu arima on au eskutik,
eroan eizu Paradisura beste guztien aurretik.