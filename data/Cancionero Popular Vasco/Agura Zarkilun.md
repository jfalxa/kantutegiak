---
id: ab-760
izenburua: Agura Zarkilun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000760.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000760.MID
youtube: null
---

Agura zarkillun barkillun bat
geure ategian zegoan bart;
ezetz ta ezetz ziñoan baia
andrea faltako zuen ark.
Bart arratsean beranduan
atea nekez yo genduan.
Agura zarkillun barkillun bat
geure ategian zegoan bart.
Andik aurrera sutondotxoan
panparreria bagenduan,
deiez asi zan andreari
yagi zaitez nai yagi adi.
Agura zarkillun-barkillun bat
geure ategian zegoan bart.
Asi zan aratzen inguruak
nun eteegozan akuluak,
akulu edo makuluak,
agura paraketamalloa.
Agura zarkillun-barkillun bat
geure ategian zegoan bart.