---
id: ab-731
izenburua: Eragiozu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000731.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000731.MID
youtube: null
---

Eragiozu eskutxu orri,
bein batari eta bein besteari,
eskutxo edertxo galantxu orri.