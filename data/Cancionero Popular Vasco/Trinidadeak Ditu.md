---
id: ab-1169
izenburua: Trinidadeak Ditu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001169.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001169.MID
youtube: null
---

Trinidadeak ditu iru aldare,
iru aldare eta zazpi lanpare.

Iru aldareetan zazpi lanpare,
aiek iru erriak eginikan daure.

Trinidadea baitu Trinidadea,
eskuti saltatu da gabilarea.

Eskuti saltatu da gabilarea,
egotxo nabarrean zeraman kartea,
urrez eskribiturik gainean letrea.

Andra Santa Marina, goiko aitzekoa
- ¿Zer dezu, nere aipa Erkuraingoa.