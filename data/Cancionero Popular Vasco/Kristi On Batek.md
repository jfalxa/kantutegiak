---
id: ab-1038
izenburua: Kristi On Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001038.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001038.MID
youtube: null
---

Kristi on batek bear bai luke
igande egunean pentsatu
aste guztian zenbat aldiz
egiten duien bekatu.