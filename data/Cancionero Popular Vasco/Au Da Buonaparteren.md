---
id: ab-868
izenburua: Au Da Buonaparteren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000868.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000868.MID
youtube: null
---

Au da Buonaparteren lendabiziko lana:
Espainiako tropa nai ezik darama,
gizon orrek badauka guzientzat lana
ezagutuko zuten traidorea zana.

Buonapartek utzidu lengo emaztea
zarra enpleatuta artu du gaztea.
Gogoak eman dio munduan astea:
katolikoen gauzak horrela eztira.