---
id: ab-289
izenburua: Maro Marotxu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000289.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000289.MID
youtube: null
---

Maro marotxu, Santa Marotxu
Maro maro txuk eramango zaitu.
Eragiozu eskutxu orri,
bein batari ta gero besteari.
Maro marotxu, Santa Marotxu,
Maro marotxuk eramango zaitu.