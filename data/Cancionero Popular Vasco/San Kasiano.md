---
id: ab-1252
izenburua: San Kasiano
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001252.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001252.MID
youtube: null
---

San Kasiano Obispo jauna,
eskola maisu entzuna;
zeure koprade garean legez
egizu laguntasuna.
Orain artean usu dan legez
gatoz gu zeure etxera;
etxeko andrea, bota egizu
limosna on bat zakura.

Emon egizu maisu jaunari
irakasteko grazia;
baita guri bere ikasteko
gomuta ona ta argia.
Orain artean usu dan legez
gatoz gu zeure etxera;
etxeko andrea, bota egizu
limosna on bat zakura.