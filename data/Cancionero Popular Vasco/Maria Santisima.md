---
id: ab-1042
izenburua: Maria Santisima
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001042.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001042.MID
youtube: null
---

Maria Santisima
bekatoren debota,
Espiritu Santu Jaunaren
biotzeko Esposa maitea,
berak irikiko digu
zeruko atea.

Anima bat bakarra degu gorputzean,
¡nola gelditzen geran hura partitzean!
Emen orain ta eriotzako orduan
esan dezagun Amen.