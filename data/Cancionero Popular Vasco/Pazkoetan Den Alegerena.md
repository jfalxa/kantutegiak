---
id: ab-1156
izenburua: Pazkoetan Den Alegerena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001156.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001156.MID
youtube: null
---

Pazkoetan den alegerena Pazkoa maiatzekoa,
Pazkoa maiatzekoa zan da jaiki nintzanean goizean.

Jaiki nintzanean goizean eta paseatzeko kalean,
paseatzeko kalean eta egunean da gabean.

Egunean da gabean eta izarra argi donean (1),
izarra argi donean eta laztantxuaren aurrean.

Gure laztantxua kalean dabil aideak konbidatutzen,
egun bat edo gehiago atalondoan sartutzen.
Beste arenbeste edo gehiago eskaleretan igotzen.

Gure laztanaren eztaietan guk jan genduen usoa,
beste guztiak konplitu eta laztanandako egoa.

Ark niri gainu, nik ari gainu, alkar genduen penatzen,
alkar genduen penatzen eta arima ere lainatzen (2).

Sekularean gerta estailarik (3) Meza berrian ematen,
Meza berrian ematen eta Epistolaren kantatzen,
Epistolaren kantatzen eta urre-kalizea goratzen.