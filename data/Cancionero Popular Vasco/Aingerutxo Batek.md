---
id: ab-1073
izenburua: Aingerutxo Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001073.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001073.MID
youtube: null
---

Aingerutxo batek esan omen du bart gure auzoan
Jesus jaio dela gauerdi-inguruan.

Jesus jaio ta aur ori bakarrik
ta sua egiteko etzauken egurrik.

Jose, ekarrizazu txortatxo bat egur,
aurra otzak ilen zaigun ni naiz agitz bildur.

Aurra eztu otzak ilen, Birjina Maria,
aur onek berekin du dibinidadea.