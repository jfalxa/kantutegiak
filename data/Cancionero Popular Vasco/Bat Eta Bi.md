---
id: ab-625
izenburua: Bat Eta Bi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000625.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000625.MID
youtube: null
---

Bat eta bita iru ta lau
bost eta sei ta zazpiamalau.
Txomin ek dauko dendea,
sagar us telez betea;
aik sal du ordu ko topako dau an drea.