---
id: ab-555
izenburua: Bazterretik Bazterrera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000555.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000555.MID
youtube: null
---

Bazterretik bazterrera
¡oi! munduaren zabala.
Eztakienak erran lezake
ni alegera nizala
hortzetan dizut irria eta
bi begietan nigarra.