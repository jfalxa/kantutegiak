---
id: ab-891
izenburua: Iruñako Perietan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000891.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000891.MID
youtube: null
---

Iruñako perietan
iragan San Ferminetan
ehun zaldi ziren eldu
Andaluziatik tropan,
merkatu ederra zitzautan
zaudelarik bi lerrotan.

Bat zen pikarta xuria,
hartan bota nuen begia.
Andaluzak egin zautan
lau ontza urren galdea
eskaini orduko erdia
"hartzak, hirea duk zaldia.".