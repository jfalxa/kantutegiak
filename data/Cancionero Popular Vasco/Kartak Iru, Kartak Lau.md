---
id: ab-819
izenburua: Kartak Iru, Kartak Lau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000819.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000819.MID
youtube: null
---

Kartak iru kartak lau,
kartak onetara nau,
amiru diru izan eta
ezin galdu amalau.
Poltsan dedan diruarekin
eztiat apalduko gaur.

Kartak diru lau egi,
seiko urrea sei begi,
gizon deboxaren aurrek.
Ardura gose ta egarri.
Atarira aterata eguzkia gosari.