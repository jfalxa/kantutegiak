---
id: ab-102
izenburua: Arranoak Bortuetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000102.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000102.MID
youtube: null
---

Arranoak bortüetan gora
dabiltza hegaletan
bai eta ni ere anderekin tsostakan
eta orai ardüra ardüra
nigarra ene haurrideak.

Arrosatzeak eder lilia zu hain berak
du il horria maitarzun gaiztoak
berareki du zigorra
hala dio azturatsuak
begira ene haurr ideak.