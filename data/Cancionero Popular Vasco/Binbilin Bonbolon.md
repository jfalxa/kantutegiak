---
id: ab-720
izenburua: Binbilin Bonbolon
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000720.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000720.MID
youtube: null
---

Binbilin bonbolon yan ta lon,
Errege frantzian balego,
idiak soinu, akerrak dantza,
astoak danbolina yo.

Danbolin ori da berria
Donostiatik ekarria,
bazter guztiak perlazbeteak,
erdian urregorria.

Talotxin, talotxin
gure aurrak bost otxin amatxi,
zako opil andi batekin
zopaz ase gaitezin.

Gure aurraren aurr ona
balio baitu Bayona,
Bayona diruz banuke,
bainan ez orrelako aurr ona.