---
id: ab-228
izenburua: Lexakako Atsoek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000228.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000228.MID
youtube: null
---

Lesakako atsoek yakiten badute
nor den ofiziale konponduren dute,
bonbila sakelean edari gozo eske,
larrua estaltzeko atorrarik gabe.