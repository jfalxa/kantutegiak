---
id: ab-104
izenburua: Arrosa Ederr
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000104.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000104.MID
youtube: null
---

Arrosa ederr xortetako
maitetarzuna gaztendako.
Bearr dudana banu
bi begien aitzineko,
zoritsuagorik ez litake
munduan ni baino.