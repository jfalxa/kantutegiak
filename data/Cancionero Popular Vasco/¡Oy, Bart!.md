---
id: ab-238
izenburua: ¡Oy, Bart!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000238.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000238.MID
youtube: null
---

¡Oy bart! ¡Oy bart! usapal bi ta eper bát
yanaritzát, yanaritzát
iru atsórekin ámabost agurek;
ai zer aparia genuen bart!
ai zer aparia genuen bart!
¡Oy bart! ¡Oy bart! bederatzi zezen aundi,
zor tzi i di ta zazpibei,
sei âri ta bost antzar, lau oilo, i ru u so,
usapal bi ta eper bat
yanaritzat, yanaritzat
iru atsorekin hamabost agurek
ai zer aparia genuen bart!
ai zer aparia genuen bart!
Amabi zama ardo zuri
hamaika zama arrdo beltz,
am arr anega ogi berri
bederatzi zezen aundi,
zortzi idi ta zazpi bei,
sei âri ta bost antzar, lau oilo, i ru u so,
usapal bi ta eper bat
yanaritzat, yanaritzat
iru atsorekin hamabost agurek
ai zer aparia genuen bart!
ai zer aparia genuen bart.