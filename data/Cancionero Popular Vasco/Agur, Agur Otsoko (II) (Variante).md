---
id: ab-6006
izenburua: Agur, Agur Otsoko (II) (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006006.MID
youtube: null
---

Agur, agur astoko
ongi etorri, otsoko.
Ardo zuri Tudelatik,
egarri bayaiz edateko
eta rraitararrarai
eta rraitararrarai
egarri baiaiz edateko.