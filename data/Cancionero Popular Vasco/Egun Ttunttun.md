---
id: ab-1216
izenburua: Egun Ttunttun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001216.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001216.MID
youtube: null
---

Egun ttunttun, bihar ttunttun,
etzi ere ttunttuna;
etxe untan arnoa bada,
guk ez yakin zemana,
zemana den yakiteko
bear du ttantto bana.
Aingeruak gara, zerutik eldu gara,
bolsa badu bainan dirurikan ez,
konbida gaitzazu txikiño batez,
guk ereinen zaitu bertzeño batez.
Urtats urtats.