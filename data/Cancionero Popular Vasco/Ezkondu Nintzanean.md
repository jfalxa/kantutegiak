---
id: ab-215
izenburua: Ezkondu Nintzanean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000215.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000215.MID
youtube: null
---

l.- Ezkondu nintzanean biotza berorik
andretzat artu nuan obea uste izanik.
Etxean daukat orain goiz ta arrts alferik,
lan bat eragiteko zer esan eztakit;
bost iragoten diot, nerea dalarik;
or nabil zarpazarka argal biotzetik.

- Josten eta ardatzean etzaku asitzen,
untzi ta sukalderik eztigu garbitzen.
Ezkurik eztarabil atorra txuritzen,
aitzurrak zokoetan dirade gelditzen,
or det nunbait andrea kukusoak iltzen.

- Ardorikan edaten etzuan lenago,
ni txurikatu arte zebilela nago.
Orai edango luke, tanto bat balego,
egunean pitxar bat baino anitz gehiago;
katalingorri balitz, are ta naiago:
andre ordirik bada, nere etxean dago.