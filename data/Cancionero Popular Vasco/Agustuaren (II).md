---
id: ab-1070
izenburua: Agustuaren (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001070.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001070.MID
youtube: null
---

Agustuaren amaboskarren
andra Maria goizean
Birjina Amaren debotatxo bat
Arantzazura zijoan.

Oin ortozean zapata gabe
abitu beltza soinean,
San Frantziskoren kordoetxo bat
gerrian iru doblean.

San Frantziskoren liburutxo bat
eskutxo bien erdian,
pausua baratz botatzen zeban
Urteagako gainean.

Urteagako gainean eta
San Eliasen parean (1),
Salberako elduko etezan
Kurtzepizio-gainean.