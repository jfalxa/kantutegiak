---
id: ab-808
izenburua: Igande Arratsaldean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000808.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000808.MID
youtube: null
---

Igande arratsaldean gezurra frango gurean.
Señale ondo artuak direla plazan eta bidean,
señale ondo artuak direla plazan eta bidean.

Astelen goiza yin denean ezin yeiki ofean,
sobera dantzan ari izanduta bezpera arratsaldean,
sobera dantzan ari izanduta bezpera arratsaldean.