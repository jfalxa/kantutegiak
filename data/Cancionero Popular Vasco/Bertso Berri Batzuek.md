---
id: ab-873
izenburua: Bertso Berri Batzuek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000873.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000873.MID
youtube: null
---

Bertso berri batzuek nai nituzke yarri
afizio duenen entretenigarri,
gogoan daukedana esango det sarri,
bei zar bat saldu arte txit nengoan larri
aren truke y obeak eztitut ekarri.

Ogei ta amar ziraden bei-truke ardiek,
itzez bai aziendak sendo ta garbiek.
Eznindun engañatu orduko ordiek,
fiatzen danarentzat daude pikardiek:
aje zarra duenak baditut erdiek.