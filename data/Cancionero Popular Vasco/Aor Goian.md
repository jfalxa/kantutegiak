---
id: ab-1195
izenburua: Aor Goian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001195.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001195.MID
youtube: null
---

Aor goian etxea, jo daigun atea.
¡Gizonak bentanara nekez irtetzea!

Gizonak bentanatik digu erantzuten:
¿gabaz ordu onetan zer darabiltzute emen?

Dontzella eder bat dakart nerekin aldean,
semea dakarrela bere sabelean.

Semea ta dontzella ¿nola izan litezke?
Embustero, falsoa, ortikan zoazte.