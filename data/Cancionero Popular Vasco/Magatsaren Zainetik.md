---
id: ab-230
izenburua: Magatsaren Zainetik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000230.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000230.MID
youtube: null
---

Magatsaren zainetik datorren urin gozoa,
edango neukeala pitxar bat osoa.
Zuk niri, nik zuri agurr eginaz alkarri
basoa txit garrbi bear degu yarri.