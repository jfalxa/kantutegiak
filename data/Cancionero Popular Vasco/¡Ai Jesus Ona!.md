---
id: ab-982
izenburua: ¡Ai Jesus Ona!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000982.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000982.MID
youtube: null
---

Ai Jesus ona
guregatik gizon egina,
guregatik gizon egina!
Zeren zeraden guziz ona,
zu ofenditua dut pena.
¡Ai Jesus ona
guregatik koronatua,
guregatik koronatua!
¡Zeren zeraden guziz ona
zu ofenditua dut pena,
zu ofenfitua dut pena.