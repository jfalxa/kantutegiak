---
id: ab-100
izenburua: Antxu Gazte
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000100.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000100.MID
youtube: null
---

Antxu gazte bilo hori ederra,
zato hunat, zato ene ganat.
Artaldean bada oi! zure beharra;
turna zaite bada zure sortu lekura.
Desiratzen dut nik hura,
zure bila nabilazu ardura.