---
id: ab-918
izenburua: Milla Zortzireun Eta
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000918.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000918.MID
youtube: null
---

Milla zortzireun eta larogeita bian
joan dan garizumako asteazkenean
txasko bat pasatu zat goierrialdean,
Tolosatik iru legua bidean:
gogoan izango det eternidadean.

Aitaren genioa mudatzen ezpada
igual joango naiz Ameriketara:
bitartean bear zaitut nik zu, ongi bada.
Orduak diraneko etorri onera
orduan lorrez ere ezkonduko gera.