---
id: ab-394
izenburua: Orra Or Goiko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000394.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000394.MID
youtube: null
---

Orra or goiko ariztitxu baten
kukuak umeak egin dozak aurten
kukuak egin amilotxak yan
¡axe bere kukuaren zoritxarra zan.
La la ra la la la la la ra la la ra la la
la la ra la la la la la ra la la ra la la
la ra la la ra la la la la ra la la la la

Amillotx orri kukuak dirautso
"eztauat itxiko gaur azur bat oso."
Ume yaleak erantzu mentzat
dino "ire ardurarik etxaukat".