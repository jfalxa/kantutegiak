---
id: ab-915
izenburua: Mami-Jaleak Dira
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000915.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000915.MID
youtube: null
---

Mami jaleak dira Errazkingoak,
zapata opo makurrak Albisukoak,
izazkilekumeak bai Allikoak,
babatxior yaleak or Astizkoak.

Gazur ontzi zikinak Barabarkoak,
sardina saltzaleak Iribaskoak,
neskatxa nabarttoak Odorizkoak,
beti enboto dire bai Madozkoak.