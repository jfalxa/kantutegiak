---
id: ab-6018
izenburua: Belengo Urian (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006018.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006018.MID
youtube: null
---

Belengo urian
gabon gabaren erdian
Maria Santisimea dekola
konsolaziño andian.
Coro:
Bai Belenen da
bart Belenen jaio da
Jesus Nazaren.
Solo:
Nazareteko tenploan
dago dama eder bat jantzirik,
izena bere Maria dauko
ondo graziaz beterik.
Coro:
Bai Belenen da
bart Belenen jaioa da
Jesus Nazaren.