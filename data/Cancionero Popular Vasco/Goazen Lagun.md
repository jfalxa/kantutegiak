---
id: ab-132
izenburua: Goazen Lagun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000132.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000132.MID
youtube: null
---

Goazen lagun, goazen lagun;
goazen Atizanera.
Urzo xuri bat yalgitzen omen da
Atizaneko plazara,
Hura nahi nuke bildu
neure sareetara,
neure sareetara.

Xori banintz, bele banintz,
yoan nindaite airean.
Urzoño hura nik arrapaniro
airean edo lurrean,
naiz bertzela sar nindaite
haren urtxotegian,
haren urtxotegian.