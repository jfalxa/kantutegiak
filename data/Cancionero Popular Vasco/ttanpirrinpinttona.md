---
id: ab-751
izenburua: Ttanpirrinpinttona
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000751.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000751.MID
youtube: null
---

Ttanpirrin pin ttona ttipi
ttipi ttipi ttipiripirona
ttipitipi tipi tipiripirona.