---
id: ab-391
izenburua: Orain Kantatuko Det (1)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000391.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000391.MID
youtube: null
---

Orain kantatuko det Bizkaiko berria,
goieriko guztien atsegin garria.
La ra la ra la la, la ra la ra la la,
la ra la ra la ra la la ra la ra la la.

- Iru ta iru sei dira ta iru bederatzi,
neskatxak mutilakin eztabiltza gaizki.

- Orain etorri zaigu Madrildik ordena:
marabidi batean mutilik onena.

- Onak baldin badira eztira garesti;
txarrak baldin badira, probatu ta utzi.

- Pikuaren ostoa itzala egiteko:
ederregia zara nekeraziteko.

- Arrosaren pareko kolorearekin,
¿zeinek otedaduka portuna zurekin?

- Baratzeko pikuak iru txorten ditu,
mutil andrezaleak ankak arin dite.

- Ederra zera baina etzera arrosa
kolore zuri-gorri neskatxa amorosa.

- Ederra zera baina etzera sekreta (sic)
maizago yoko nuke leioko krisketa.