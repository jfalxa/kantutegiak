---
id: ab-995
izenburua: Asko Lagunengandik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000995.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000995.MID
youtube: null
---

Asko lagunengandik
ala det aditzen
al degunean ari
gerala kojitzen.
An egon bearko degu
gero padezitzen,
infernuko injuri
gaiztoen sufritzen:
orra zer peligrotan
geraden arkitzen.