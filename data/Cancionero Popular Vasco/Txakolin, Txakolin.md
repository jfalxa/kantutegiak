---
id: ab-403
izenburua: Txakolin, Txakolin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000403.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000403.MID
youtube: null
---

Txakolin txakolin
txakolinak on egin
txakolinak egin dio
Mariari txax koa
txaxkoa txaxkoa
¡¡txakoliñen goxoa!!
Ai Salome Salome Salome!
ay Salome gaixoa!
ea bada nere lagunak
erabili yorraia,
aspaldietan ezta yorratu
eta badauka premia;
yorratzaile nik zintzoen denak
izango du pitxerdia.
Txakolin, txakolin,
txakolinak on egin,
txakolinak egin dio
Mariari txaxkoa,
txaxkoa, txaxkoa
¡¡txakoliñen goxoa!!
Zumayatikan gora
Artadirako goizerdirako
izerdia lodi atera bearko
bestelan ezin ara elduz
or zinake zu lertuz.
Txakolin, txakolin,
txakolinak on egin;
txakolinak egin dio
Mariari txaxkoa;
txaxkoa txaxkoa
¡¡txakoliñen goxoa!!.