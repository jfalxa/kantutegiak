---
id: ab-1243
izenburua: Ogei Ta Laugarrena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001243.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001243.MID
youtube: null
---

Ogei ta laugarrena
degu abendua,
alkantzatu dezagun
deseo deguna.