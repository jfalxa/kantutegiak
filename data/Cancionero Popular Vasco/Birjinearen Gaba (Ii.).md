---
id: ab-1000
izenburua: Birjinearen Gaba (Ii.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001000.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001000.MID
youtube: null
---

Birjinearen gaba da gaur da esaigun bere kantua,
nik kantauko dot kantea baia gogoz erantzun, jentea; (bis)

Jaioten danak eriotzea da gauza eskusa bagea,
ni nerau bere jaio nintzan da zor daukat eriotzea; (bis)

- Eriotzari gauza bategaz
oi natxako neu kontentu,
aberatsari diruakaitik
ezin leiola parkatu.

Aberats orrek diruak ditu
pobreak umildadea,
umildadeak edegiko itu
zeru altuan ateak.