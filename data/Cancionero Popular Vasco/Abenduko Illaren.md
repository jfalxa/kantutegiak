---
id: ab-856
izenburua: Abenduko Illaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000856.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000856.MID
youtube: null
---

Abenduko illaren ogeigarrenean
Arroara eldu nintzan ilun nabarrean;
gau ori pasa degu umore onean,
tratu bat ere egin degu alabearrean.

Tratu ori nola dan esango det garbi:
nik lau txarri nituan besteak sei ardi,
ezkinion tatxarik an esan alkarri:
tratuak zer egina gero dau ekarri.