---
id: ab-549
izenburua: Bakarrik Aurkitzen Naiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000549.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000549.MID
youtube: null
---

Bakarrik aurkitzen naz
neure amparuan,
neure konpesinoa
egitera noa.

Aita, akusatzen naiz
neu lenengoagaz,
geure Jaungoiko ona
amadu ezagaz.

Praile santua dago
guztiz arriturik
dama ikusten zuela
belaunetan ilik.

Ezpan koral ederak
ondo lur eginik
begi zuri ederrak
guztiz ezaindurik.