---
id: ab-177
izenburua: Xori Erresinula
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000177.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000177.MID
youtube: null
---

Xori erresinula
üdan khantari,
zeren ordüan beitu
landetan janari,
negüan ezta ageri
balinba ezta erí,
üdan jin baledi
atseden naite ni.

Xoria zaude isilik
ez egin nigarrik
¿zer atzeman dü kezu
hola atsankaturik?
Nik eramanen zütut
xedera hazkatürik
ohi ko bortüan barna
ororen gainetik.