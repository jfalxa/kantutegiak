---
id: ab-1022
izenburua: Jesukristori (Ii.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001022.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001022.MID
youtube: null
---

Jesukristori kendu ezkero
pekatuakin bizitza,
baldin ezpadot negar egiten
arrizkoa dot biotza.
Guztiok lagun kantadu daigun
bere penazko eriotza.