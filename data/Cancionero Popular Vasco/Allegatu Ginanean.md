---
id: ab-766
izenburua: Allegatu Ginanean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000766.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000766.MID
youtube: null
---

Allegatu ginanean bortara
lendabizikorik ala
adieraztera norbait bazela,
etxekoandrea eldu da,
al dadien bezala,
gonakotilon kordea
esku ezkerrean zuela:
Jaunak pasa zitezte isil isila.

Agur, etxeko andrea;
piztu ziguzu argia,
edan bear baitugu
kutxulerdia.
- Erraiten dizuet egia,
kontzientziaz fedea,
akabatua dudala
barrikako neurria,
xorta xortaño bat ez
bertze guzia.