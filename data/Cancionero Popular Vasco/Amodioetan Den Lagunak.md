---
id: ab-97
izenburua: Amodioetan Den Lagunak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000097.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000097.MID
youtube: null
---

Amodioetan den lagunak
badu anitz barrneko min
Ez naiz mintzo jakin gaberik
baizen ni hula izanik
Maite bat geyegi maitaturik
leihor itsasoak igarorik
ay! ezin utziz burutik
iratzartu eta ondoren
ay! ene penak haunditzen
goazen goazen ene lagunak
zoin gure maitearen aldera,
gure pena nayez ororen
ayeri erakustera
bai eta atsegintzera.