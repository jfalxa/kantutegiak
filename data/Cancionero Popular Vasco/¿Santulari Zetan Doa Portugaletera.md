---
id: ab-398
izenburua: ¿Santulari Zetan Doa Portugaletera?
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000398.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000398.MID
youtube: null
---

Santulari zetan doa Portugaletera?
Ontziaren aitzakian damok ikustera.
Ontziaren aitzakian damok ikustera
Santulari sarri doa Portugaletera.
La la la la la la ra la la la ra
la la la la la la ra la la
La la la la la la ra la la la ra
la la la la la ra la la

Egun baten Santulari aran tza yoala
biderdira urten eutsen ta emon emonala.
Arrezkero azurmiñez Santi dagoala
Bilbo raño sartu oi da bere on tzi txaltala.
La la la la la la ra la la la ra
la la la la la la ra la la
La la la la la la ra la la la ra
la la la la la ra la l.