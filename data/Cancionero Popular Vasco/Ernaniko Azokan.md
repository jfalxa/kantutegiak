---
id: ab-790
izenburua: Ernaniko Azokan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000790.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000790.MID
youtube: null
---

Ernaniko azokan txalak merke,
emaztegaiak merkeago.
Neskatxak, emendik bazoazke
Goierrin diraden azoka-egunez:
aietan sarritan dituzute
senargai erosle eder ta aurrez.

Ernaniko azokan urrak gora,
mutil ezkongaiak gorago;
salgaia merkatu ezpaledi,
¡amaika mutil zar erririk eri
ikusi bear dek, Joxe, aurten
merkatuz, uxtertuz ¡ai! usteltzen.