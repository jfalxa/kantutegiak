---
id: ab-711
izenburua: Atsoa Zirri Zarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000711.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000711.MID
youtube: null
---

Atsoa zirri zarra,
errokan aitzeko gogo txarra,
pinterdia iraultzen du,
atsein artzen du biotzean.