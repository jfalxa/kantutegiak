---
id: ab-173
izenburua: Oski Pare Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000173.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000173.MID
youtube: null
---

Oski pare bat galtza pare bat
eman nereizun hatsarrez,
berrogei eta bost libera,
zetafularr bat beste alde:
zure maitetar zun hauta
kosta zitazüt hanbeste.

Gezurrerraitez, gezurrerraitez
uste zuniana ni tronpa?
Zure hitzetan fida den a
ardüra gaizki izanen da:
beste baten gibelean
xuxen ezazü urratsa.