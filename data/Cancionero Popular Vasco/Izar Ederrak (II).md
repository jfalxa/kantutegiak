---
id: ab-1117
izenburua: Izar Ederrak (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001117.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001117.MID
youtube: null
---

Izar ederrak argi egiten dau
zeru altuan bakarrik.
Ezta bakarrik, lagunak ditu,
Jaun zerukoak emanik.

Zazpi aingeru aldean ditu,
zortzigarrena gaixorik;
zazpi mediku ekarri deutsez
India-Madriletatik

Arek igarri, arek igarri
nundik dagoan gaixorik;
amoreminak badituz onek
errai artean sarturik.

Guzurra diño mediku onek.
Nik eztot amore miñik.

Gaur arratsean ilgo naz eta
etorri gorpu-ondora,
errosario zuri eder bat
esku-artean dozula.

Neu enterretan naroanean
igongo dozu korura,
andixek bera salto egingo zu
Birjina Amaren ortura.

Larrosea eta krabeliñea
artungo dozu eskura
jente guztiak esan daiela
neure lutua dozula.

Aingerutxuok esaten dabe
Salbean Abe Maria,
gero guztiok gozatu daigun
zeru altuan gloria.
Amen Abe Maria.