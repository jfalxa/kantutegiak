---
id: ab-1122
izenburua: Londresen Dot Neuk Senarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001122.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001122.MID
youtube: null
---

Londresen dot nauk senarra
zirin-bedarrak ekarten.
Bera andik dan artean
gu emen dantza gaitezen.
¡Oi au egia!
Daigun jira bi Maria.

Kapoiak dagoz erreten
oilaskotxuak mutiltzen,
orrek ondo erre.artean
gu emen dantza gaitezen.
¡Oi au egia!
Daigun jira bi Maria.

Neure oilanda nabarra
txikarra baina zabala,
¿zetan oa i auzora
etxean oilarra donala?
¡Oi au egia!
Daigun jira bi Maria.

Neuk ugazaba nekusan
Izurdiako zubian.
Ori zestorik urten artean
gu emen dantza gaitezan.
¡Oi au egia!
Daigun jira bi Maria.

Neure mutiltxu txoria,
ik esan eustan egia:
gaur gabean dantzauko zala
nire emazte Maria.
¡Oi au egia!
Iretzat mando zuria.