---
id: ab-6031
izenburua: Urte Berri Berri (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006031.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006031.MID
youtube: null
---

San Nicolas coronero confesero Mari Andres,
alargun dontzellea cantaremos alegria,
bost etxetan sei ate,
zazpi etxetan suete.

Aingeruak gara
zerutik yatsi gara,
una limosnita por amor de Dios.