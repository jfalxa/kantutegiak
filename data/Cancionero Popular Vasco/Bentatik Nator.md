---
id: ab-114
izenburua: Bentatik Nator
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000114.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000114.MID
youtube: null
---

Bentatik natorr, bentara noa
bentan da nere gogoa
bentako arrosa krabelinetan
artu det amorioa.

Eztakit zerdan, neure maitea,
ene aspaldiko bizia
gurina sutan oi dan bezala
urtu zait gorputz guzia.