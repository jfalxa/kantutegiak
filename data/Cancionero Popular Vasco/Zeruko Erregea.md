---
id: ab-1279
izenburua: Zeruko Erregea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001279.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001279.MID
youtube: null
---

Zeruko Erregea yaio zan orduan
munduan Herodes erregea
enbidiaz betea bildur tuten asi zan.