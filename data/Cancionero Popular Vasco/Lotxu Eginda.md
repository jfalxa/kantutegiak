---
id: ab-287
izenburua: Lotxu Eginda
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000287.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000287.MID
youtube: null
---

Lotxu eginda, lotxua gozo,
lotxu eginda kalera,
tanbolintxua kalean dago
umetxu oni begira,
tanbolintxua kalean dago
umetxu oni begira, lo, lo.