---
id: ab-1204
izenburua: Biar Eguberri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001204.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001204.MID
youtube: null
---

Bihar Eguberri dugu,
Noelak erranen tugu;
zori onean dela
Yainko Semea sortu,
zori onean dela
Jainko Semea sortu.