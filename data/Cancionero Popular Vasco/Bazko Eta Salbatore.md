---
id: ab-554
izenburua: Bazko Eta Salbatore
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000554.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000554.MID
youtube: null
---

Bazko eta Salvatore,
hulanena Mendekoste
¡eta ni gaixo au triste,
xutik ere ezpeinaite.