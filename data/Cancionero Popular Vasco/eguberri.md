---
id: ab-1209
izenburua: Eguberri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001209.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001209.MID
youtube: null
---

Eguberri eguberri
agur dala eguberria,
guzien Yauna yaio beita
Beleneko irian.
Beleneko iria
¡zure doiaren andia!
Zrentzun beitze yaio beitze
yaungoiko onaren semea.
Amari zeizko Maria beti
Birjinik erdia eta
Jesus seme ori
zeruan izen emana.
Eri denen dugu eridenen
guzien Yauna Belenen,
goazen guziok Belenera
Jesusen adoratzera.
Galde eginez, galde eginez
non duzie sortu berria,
guziak junto sartuak dira
Jerusalenen barrena.