---
id: ab-1174
izenburua: Txoritxoa Sasian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001174.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001174.MID
youtube: null
---

Txoritxoa sasian kantatzen,
ezta erraz nerau engañatzen.

Oreganu San Juan goizean,
abarea (y) okaran-ganean.

Lobatxoa azpian negarrez,
yausi eztakionen bildurrez.

Alegereak bagintzan bere
tristeza andiak yo gaitu:
zazpi librako oilo zuria
bart azeriak yan dosku.