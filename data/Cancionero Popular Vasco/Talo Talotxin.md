---
id: ab-750
izenburua: Talo Talotxin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000750.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000750.MID
youtube: null
---

Talo talotxin atoz etxera Matxin
taloa dago adarrean
bere alkitxo nabarrean
tulut bikoxalean,
txirritiko marratiko
Bernardo muxarrean.