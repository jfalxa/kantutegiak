---
id: ab-998
izenburua: Birjina Karmengoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000998.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000998.MID
youtube: null
---

Birjina Karmengoa
lore eder ori,
eskerrak ematera
zugana nator ni.
Astean era ezpada
goazen jaiean
¡noiz allega gintezken
aren konpaiñian!
Obra onak egiteko
nork bere gaiean.