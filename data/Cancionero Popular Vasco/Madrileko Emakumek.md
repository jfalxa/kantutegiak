---
id: ab-914
izenburua: Madrileko Emakumek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000914.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000914.MID
youtube: null
---

Madrileko emakumek egin dute balentria,
beren eltze eta pegarrak aurtiki karrikara.
Lur baxera egilendako ori ezta malur bat,
aintzineko bien prezioan saltzen dute orain bat.

Espaiñian Herodes eta Frantzian Bonaparte,
gerla horrek iraun dezake horiek biak hilarte;
duienari edek eta eztuenari galde,
laster naski izanen gaituk kornadurik gabe.