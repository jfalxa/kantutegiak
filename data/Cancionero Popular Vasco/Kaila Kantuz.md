---
id: ab-148
izenburua: Kaila Kantuz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000148.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000148.MID
youtube: null
---

Kaila kantuz ogipetik
uztail agorriletan:
maitea ganik etxerakoan
aurthen entzun dut bostetan:
amodioak nindera bila
haren ate leihoetan.