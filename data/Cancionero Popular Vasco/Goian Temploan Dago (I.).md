---
id: ab-1016
izenburua: Goian Temploan Dago (I.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001016.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001016.MID
youtube: null
---

Goian tenploan dago amandre Santa Ana,
iru lili-arrosa eskuan dauzkala.

Iru eskuan eta bai zazpi buruan,
¡aiek nondikan nora ark ote zituan!


Maria Santisimen eskutik zituan.

San Juan ¿ekusi dezu, ay nere semea?
Eztut bada yekusi, ez Ama yandrea.

Kalea gora die, berorren semea,
Soiñean deramala Kurutze berea.

Gurutze ori ekarzu ai nere semea.
Eztiot bada emanen, ez, Ama yandrea.

Onekin pasa bear det nik eriotzea,
Eriotzearekin pasio luzea.

San Pedro apostolua mezaren emaiten,
Maria Santisima mezaren entzuten.

Amika mila aingeru koruan kantatzen,
Beste hainbeste ta yago musikari jotzen.

Kantau akabatzen da, jenteak, parkatu;
Obeki dakienak aurrera segitu.