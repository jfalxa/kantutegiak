---
id: ab-1254
izenburua: Santa Agata Andere
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001254.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001254.MID
youtube: null
---

Santa Agata andere
aurten bezala gero ere;
Santa Agatak
egortzen gaitu
badukezun deus ere.

Goazen goazen hemendik,
hemen eztuk xingarrik,
etxe huntako gazitegian
saguak umeak egin tik.