---
id: ab-785
izenburua: Ea Bada, Mutil
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000785.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000785.MID
youtube: null
---

Ea bada, mutil gogotik erantzik
arin abarkea orpotik,
gora salto, bera brinko,
onela bagaituk ezkaituk ilko.
¡Ai oi au zerua!
Dantzan dabilt nere burua.