---
id: ab-787
izenburua: Ene Andrea Yeiki-Orduko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000787.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000787.MID
youtube: null
---

Ene Andrea yeiki orduko
zazpiak edo zortziak,
andikan gora behar ditu
agiri diran guziak
eta don farirun,
irule gaizto, dantzari on,
dantzari ona irule gaizto,
edale on.