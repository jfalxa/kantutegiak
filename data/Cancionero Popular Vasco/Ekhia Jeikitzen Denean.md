---
id: ab-119
izenburua: Ekhia Jeikitzen Denean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000119.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000119.MID
youtube: null
---

Ekhia yeikitzen denean
zabaltzen hasten da argia.
Kantu berria nahi dianak
zabal beza beharria.
Baratze zainak eregiten du
üda berrian hazia
hazi hura üdan gora
üdan gora lili ederra
üdan gora, esperantza
agian sortüren ahal da
agian sortüren ahal da.