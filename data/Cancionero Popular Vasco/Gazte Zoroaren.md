---
id: ab-797
izenburua: Gazte Zoroaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000797.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000797.MID
youtube: null
---

Gazte zoroaren pentsamentua,
zarrari emon ezinik satisfazioa.
Ezeizu siniztu zarraren esanik
Jesus Maria ez pa da besterik:
aparta zaite zar zoroetatik.