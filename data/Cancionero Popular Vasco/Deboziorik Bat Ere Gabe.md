---
id: ab-1003
izenburua: Deboziorik Bat Ere Gabe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001003.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001003.MID
youtube: null
---

Deboziorik bat ere gabe,
plaza batera bezala,
maiz Elizara doazin oiek
¿zer ari dire horrela?
Gorputzez Mezan, gogoz tabernan
egon izaten gerala:
gure arimain lazakeriak
Yinkoak barka ditzala.