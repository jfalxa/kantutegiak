---
id: ab-562
izenburua: Egun Eta Larogei
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000562.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000562.MID
youtube: null
---

Egun eta larogei zazpi ardirentzat
iru zatoke osto nituan nik behintzat,
etzan pen tsu andi a izango guretzat
aurtengo beire zeu ken Jaun goikoak guretzat.