---
id: ab-823
izenburua: Kuku Bat Badut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000823.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000823.MID
youtube: null
---

Kuku bat badut kaiola batean
nun oraino ezpaitu kantatzen.
Hura asten bada kantatzen Kyrie; kyrie;
hura asten bada kantatzen a la maison, Kyrie eleison.