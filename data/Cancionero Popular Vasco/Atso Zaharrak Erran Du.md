---
id: ab-778
izenburua: Atso Zaharrak Erran Du
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000778.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000778.MID
youtube: null
---

Ene senarra gaur hilen,
agian bai;
ene senarra gaur hilen,
agian bai,
gaur hilen eta bihar ehortz
tralara tralara lalarai,
gaur hil eta bihar ehortz
tralara tralara la.