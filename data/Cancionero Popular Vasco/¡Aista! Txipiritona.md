---
id: ab-310
izenburua: ¡Aista! Txipiritona
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000310.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000310.MID
youtube: null
---

¡Aista! txipiritona, txipiritona, txipiritona.
¡Aista! bataren gaitza bestaren ona,
Auxe bai dala txipiritona.

Altzineko orrek kendu dezala
txipiritona, txipiritona.
¡Aista! kendu dezala txamarret ori, txamarret ori.

Bigarren orrek kendu dezala
txipiritona, txipiritona.
¡Aista! kendu dezala koratilo ori, koratilo ori.

Irugarren orrek kendu dezala
txipiritona, txipiritona.
¡Aista! kendu dezala garriko ori, garriko ori.

Laugarren orrek kendu dezala
txipiritona, txipiritona.
¡Aista! kendu dezala atorra ori, atorra ori.