---
id: ab-1067
izenburua: Zori Gaiztoan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001067.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001067.MID
youtube: null
---

Zori gaiztoan izan eztedin
gu jaio giñan eguna
asi gaitean konsideratzen,
sobra da galdu deguna.
Nai ezpadegu burlarik egin
Jesus onaren pausuaz,
etsi dezagun alde batera
mundu ontako gustoaz.