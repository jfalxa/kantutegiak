---
id: ab-345
izenburua: Baltzerana Izanarren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000345.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000345.MID
youtube: null
---

Baltzerana izan arren grazia badezu,
sekulan kortejoa faltako eztezu;
txikitxo polit ori zeu neure laztana,
kateatu ninduzun librea nintzala.