---
id: ab-547
izenburua: ¡Au Da Urkamendiko...!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000547.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000547.MID
youtube: null
---

¡Au da urkamendiko
aizearen otza!
¡Jaunaren aurrerako
nik daukadan lotsa!
Zerren egin dodazan
zazpi eriotza,
zazpi eriotza ¡ta
non dot neuk biotza.