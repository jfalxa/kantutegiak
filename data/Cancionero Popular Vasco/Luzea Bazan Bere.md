---
id: ab-912
izenburua: Luzea Bazan Bere
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000912.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000912.MID
youtube: null
---

- Luzea bazan bere
Santiago eguna,
laster etorri yakun
orduan iluna:
bai, neure laguna;
bai, neure laguna.

- Zeu barruan zegozan
eta neu kanpoan,
gure autualdia nok entzun
ba egoan eta neu kanpoan,
eta neu kanpoan.