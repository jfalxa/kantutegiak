---
id: ab-1280
izenburua: Zirkunzidatu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001280.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001280.MID
youtube: null
---

Zirkunzidatu eben Jesus gugaitik,
gaugana zeuskun amodioak
obligatzen zaitu Jauna goizetik:,
parkatu egidazu zareanagaitik.

¡Au da amodiozko gorputz sotila!
Badakizu nola oztu odola;
puskaka puskaka, Jesus maitea,
ikusi bearko guz munduan ilak.