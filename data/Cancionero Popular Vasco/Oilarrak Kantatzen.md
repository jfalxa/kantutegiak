---
id: ab-838
izenburua: Oilarrak Kantatzen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000838.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000838.MID
youtube: null
---

Oilarrak kantatzen kukurruku,
erretiratzen baikira gu.
Kukurruku ¿nor gira gu?
Landibartarrak baikirade gu.