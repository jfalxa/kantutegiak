---
id: ab-206
izenburua: Au Egunaren Andia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000206.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000206.MID
youtube: null
---

¡Au egunaren andia!
¡Au poza ta gogo argia!
txir txir yartzeko begia
maian naikoa edaria:
glin glan bonbiletik,
naiz gorri naiz txuritik.

Ago (y) oneko kafea:
au da lenengo aldea;
bigarrena edatea
katalingorri maitea:
glin glan bonbiletik,
naiz gorri naiz txuritik.

Ondoren dira besteak
edari nai lakoxeak,
pozik yartzeko tristeak
bustiaz ongi esteak:
glin glan bonbiletik,
naiz gorri naiz txuritik.

Gure nagusi jaunari
ez inortxori besteri
zor zaio mesede ori
adan zagun bada ongi:
glin glan bonbiletik,
naiz gorri naiz txuritik.