---
id: ab-865
izenburua: Antonio Fagoaga
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000865.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000865.MID
youtube: null
---

Antonio Fagoaga,
badezu gradua;
pobreari lagundu;
ori da modua!
Eztuzu irabaziko
bestela zerua.