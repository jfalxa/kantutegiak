---
id: ab-747
izenburua: ¿Nora Zoazte?
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000747.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000747.MID
youtube: null
---

¿Nora zoazte orren alairik?
Aita dator, Ama dator Bayonaldetik,
gozoz beterik.
¿Nora zoazte orren ilunik?
Aita doa, Ama doa Donostiatik,
diru gaberik.