---
id: ab-1007
izenburua: Elizako Bost Sakramentuak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001007.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001007.MID
youtube: null
---

Elizako bost Sakramentuak
zeru altutik jetxiak
beste bi oiek borondatetaz
Jaunak beretzat utziak.

Lendabiziko sakramentua
bataiatzen geranean
herejiatik biortzen gera
Jaungoikoaren legean.