---
id: ab-781
izenburua: Babak Zabal Ostoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000781.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000781.MID
youtube: null
---

Babak zabal zabal ostoa
Iholdin da Olzoa
bai hemengo etxeko andereak
golkoan du gezurra.