---
id: ab-863
izenburua: Andoaingo Elizan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000863.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000863.MID
youtube: null
---

Andoaingo Elizan olio ugari
aisa kendu zioten karlistak beltzari,
karlistak a cuchillo: an ziran kontuak!
Milagroak egin ttu gurutze santuak.