---
id: ab-812
izenburua: Ikusi Nuenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000812.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000812.MID
youtube: null
---

Ikusi nuenean nik zure begia (bis),
iruditu zitzaidan, maitea, anpolai gorria (bis).

Ikusi nuenean nik zure okotza (bis),
iruditu zitzaidan, maitea, gaztaña-lokotza (bis).

Ikusi nuenean nik zure sudurra (bis),
iruditu zitzaidan, maitea, lukainka-muturra(bis).

Ikusi nuenean nik zure agoa (bis),
iruditu zitzaidan, maitea, karabi-zuloa (bis).

Ikusi nuenean zure belarria (bis),
iruditu zitzaidan, maitea, azaren orria (bis).