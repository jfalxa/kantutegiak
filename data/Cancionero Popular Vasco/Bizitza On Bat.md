---
id: ab-1001
izenburua: Bizitza On Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001001.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001001.MID
youtube: null
---

Bizitza on bat mundu onetan
pasatu nai badezu,
Birjina Amaren errosarioa
gau edo egun esazu.

Birjina Amaren errosarioa
beti esaten badezu,
orain grazia gero gloria
zeruan izango dezu.