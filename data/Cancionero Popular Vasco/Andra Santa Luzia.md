---
id: ab-707
izenburua: Andra Santa Luzia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000707.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000707.MID
youtube: null
---

Andra Santa luzia puxkat artilleria.
¿Nok edan dau emen ura?
Aingerutxu ederr ederrenen batek
¿Zeinbat bidar?
Iru bidar. Jesus mila bidar.