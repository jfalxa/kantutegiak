---
id: ab-886
izenburua: Gaztelu, Aspaldion
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000886.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000886.MID
youtube: null
---

Gaztelu aspaldion etzaitut ikusi,
uste det zabiltzala nigandik igesi.
Irun-aldean lanean omen ziñan asi,
janda edan bapo egin, galanki irabazi:
kanpoan dabiltzanak eztabiltza gaizki.

Askok uste du ondo dala kanpoetan dana,
jornala badaukate diran ainbat fama;
gaiñera lagunakin ipiñi nai broma,
mantendutzeak ere parte bat badama
azkenerako sobrak egiten du marma.

Amairu-hamalau erreal irabazte' utela
nunnai ikusten ditut kontari daudela.
Jenteak geiegian artu zuen gala,
gauza reglakorikan estimatze' eztala:
orain ere bost batekin mantenduko alda.

Amalauri bost kenduta bederatzi daude,
erreal bat eskas degu sagardotako ere.
Gaiñera jai ta egualdi txar noiznai badirade,
jan-bearra paltatzen ez orietan ere:
kontuak oiek, Senpelar, probatuta gaude.

Mundu onetan asko gabiltza txit justu,
au egia esango det: nai duenak sinistu.
Gutxiagoan jardun izan zan zenbait gizon prestu,
denbora aitako kontuak ondo ziran aztu:
orain sobratze eztuenak sekula izango eztu.

Beti jornalean da sobratua piskat
ni aurrena naizela zembaittxe gabiltza,
tabernetan ugari ere bulla eta driska,
nekez irabaziak kaxtatzen errez da
irutik bi berenik kitatsu gabiltza.