---
id: ab-1222
izenburua: Estalpe Zar Batean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001222.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001222.MID
youtube: null
---

Estalpe zar batean
Belengo urian
gure Jaun yaio zan
gabon gaberdian,
Birjina Amagandik
tristura andian,
gu salbatzeagatik
il zan kurutzean.
Goazen, goazen artzaiak
goazen Belenera.
Aurtxo jaio danari
biotza eskintera
salto ta brinko eginaz
estlpe aldera,
izar zerukoaren
argitasunera.