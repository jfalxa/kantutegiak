---
id: ab-560
izenburua: Bi Berseten Huntzera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000560.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000560.MID
youtube: null
---

- Bi berseten huntzera
nüzü abiatzen,
nigarra begietarik
zaitazüt yoaiten,
ene sortze tristea
mün düan argitzen:
horrek beiterit eni
bi ho tze an pe neiten.

- O ene ama maitea
zure haurretako,
enai zu ez ezagütü
gisala orano,
besteak hoberetsi
ni beno haboro
eta e ni gai neratü
huts ho ge nak oro.