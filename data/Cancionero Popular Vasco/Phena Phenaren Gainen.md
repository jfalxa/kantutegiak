---
id: ab-608
izenburua: Phena Phenaren Gainen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000608.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000608.MID
youtube: null
---

Phena phenaren gainen
¡hau da dolorea!
adiorik erran gabe
joan zait emaztea.
Pentsamentukan na go
ene traidorea
zuin izanen ote ginen
lehen do lutua.