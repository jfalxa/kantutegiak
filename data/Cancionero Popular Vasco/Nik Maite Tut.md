---
id: ab-167
izenburua: Nik Maite Tut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000167.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000167.MID
youtube: null
---

Nik maite tut neskatxak,
neska gazte politak
eta zerbaito dutenak.
Halako bat behar dut,
ezkontzeko ordu dut,
adinak ere baditut.