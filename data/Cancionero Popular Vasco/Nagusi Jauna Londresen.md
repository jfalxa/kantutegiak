---
id: ab-1131
izenburua: Nagusi Jauna Londresen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001131.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001131.MID
youtube: null
---

Nagusi jauna Londresen
zingulu belarrak biltzen,
y ura andik etorri arte
gu biok dantza gitezen
¡Oi, ai egia! uztar ingulun Maria.