---
id: ab-738
izenburua: Iru Errege Magoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000738.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000738.MID
youtube: null
---

Iru Errege Magoak
ain bide luzean,
zeruan musikea,
munduan tronpetea,
andra kosepa tronpeta yoten,
kriston erreberentzia,
yarri belauniko,
adora Jesukristo.
¿Gure zelaian nor dabil?
Azaburuak loratzen.
Aitatxu, Amatxu,
urra urra pitxitxu.