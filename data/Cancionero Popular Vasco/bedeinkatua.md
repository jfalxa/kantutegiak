---
id: ab-1200
izenburua: Bedeinkatua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001200.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001200.MID
youtube: null
---

Bedeinkatua izan dedilla
etxe ontako jentea,
pobre ta umil dabiltzanentzat
badute borondatea.
Libertatea eskatzen diot
etxeko printzipalari.
Santa yagedaren alabantzak
kantatutzera noa ni.