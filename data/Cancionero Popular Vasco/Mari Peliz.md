---
id: ab-382
izenburua: Mari Peliz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000382.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000382.MID
youtube: null
---

l.- Mari Peliz ¿zelan zabilz?
ederr ori galant ori ¿ondo zabilz?
Ondo ibiltea gitxi eraltea
eztagonerako danean gordetea.
Ondo egiten dozu zur zur izatea.

- Mariako, Mariako,
emoidana atorrea Bilborako.
Zin giratzan yantzi eta antxe erantzi
bien bitartean astiro ibiliko.
Ezdautsanat ezer be ezer bere egingo.