---
id: ab-597
izenburua: Ogoigarren Urterat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000597.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000597.MID
youtube: null
---

Ogoi garren urterat
ari naiz urbiltzen
eta neure oinetan
tonba dut ikusten.
¿Ni zertako mundurat
etorria nintzen?
Ondikotz ez sortzea
neuretzat obe zen.