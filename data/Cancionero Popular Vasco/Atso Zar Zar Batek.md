---
id: ab-779
izenburua: Atso Zar Zar Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000779.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000779.MID
youtube: null
---

Atso zarzar batek
egin daut mandatu
ea nari dudan
arekin ezkondu.
Atso zar zar ortzik gabea,
deabruai presente eiteko
a zer udarea.