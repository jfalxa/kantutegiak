---
id: ab-889
izenburua: Gorogi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000889.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000889.MID
youtube: null
---

Gorogi paltsuaren lenengo papera
Espaiñiako soldaduak bialtzeko Nortera;
bai eta frantsesa bere Madrilgo kortera,
baiña eztitudaz itzak konpondu arela.

Balentziako suan ginaden atzera,
artzaia denpora artan lo izan artera (??)
Ogeta bi jeneral joiazan galtzera
¿zergatik etorri etzara arek libratzera?

Bosteun boluntario lengo egunean
Lekeition sartu diraz buenamentean (1):
Mina de bebia (??), Longa de mi amor,
Antsotegi, Campillos de mi corazón.

(1) Hubo otro poeta (?) que al cantar el incendio de Urnieta en la
segunda guerra carlista dijo: Orra nun dan Urnieta malamentean
erreta..., etc.