---
id: ab-609
izenburua: Plañitzen Niz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000609.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000609.MID
youtube: null
---

Plañitzen niz bihotzetik,
gaitza zer dudan eztakit.
Dolore handi batek harturik,
eznuke axolarik
balitz erremedirik;
bena eztuzu barberik
mundu guzian bat baizik,
ene gaitza sendo dezakenik
hura berant etsiturik
hola nago tristeturik.