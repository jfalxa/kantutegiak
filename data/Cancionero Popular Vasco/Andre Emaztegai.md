---
id: ab-623
izenburua: Andre Emaztegai
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000623.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000623.MID
youtube: null
---

An dre e maz tegai bilo oria,
zabal ezaguzu ataria:
ortxen eldu da senargai yauna,
iduri duela iduzkia.

Orra senargai, zure kuttuna
baratzeko lili ederduna.
Sar zite ta ar zazu gaur beronek
eskaintzen dizun zoriona.