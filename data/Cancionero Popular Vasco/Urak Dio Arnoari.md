---
id: ab-247
izenburua: Urak Dio Arnoari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000247.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000247.MID
youtube: null
---

Urak dio arnoari
¡zer gizon eslayoa hizen hi!
Dantzarazten tuk gizonak, andreak;
aberats, ga be ak; zaharrak, gazteak,
tipiak andiak;
baitan ere ahantz arazten
min eta arren kura guziak.

Arnoak darantzu urari
"zer bihotz go gor hizan hi!
hilarazten tuk aur zurtz eta al hargun,
alai ta be tilun, eri ta zoridun,
gaiz kin ta bihotz hun;
baita ere isilarazten
kantariak yai ta astelegun.