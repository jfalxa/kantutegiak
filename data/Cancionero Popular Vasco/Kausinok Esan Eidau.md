---
id: ab-901
izenburua: Kausinok Esan Eidau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000901.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000901.MID
youtube: null
---

Kausinok esan eidau lelengo tonuan
alegrentzia baiño oberik eztago munduan,
ez diamante perla ez urre gorririk
alegrentzia baiño zer gauza oberik.

Mendi garaietatik artzainak eidatoz,
esanik aingeruak jaio dala jesus
Ea bada Josetxo goazan ogera
aurtxo gizaixo ori berotuko bada.