---
id: ab-1160
izenburua: Sai Juan Bagilean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001160.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001160.MID
youtube: null
---

San Juan bagillean
denpora ederrean,
hamalau otso tronpeta yoten
motraillu baten ganean.
Udan udan udan
Sai Juanetan
dantzan (dxantzan) eingo dogu
ifar-airetan.

Lirio lora ederra
jaio da begillean
lora ederrez beterik dago
Paskua-maiatzean.
Udan udan udan, etc.

Maiatzean lirio ederra
baia orain bagillean
irukotxa ederrago da
Paskua maiatzean (sic).
Udan udan udan, etc.

Gure ortuan ginda-lorea
Sai Juanetako eldua,
Sai Juanak baiño lenago
gure usoak batua.
Udan udan udan, etc.