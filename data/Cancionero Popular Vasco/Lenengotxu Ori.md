---
id: ab-828
izenburua: Lenengotxu Ori
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000828.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000828.MID
youtube: null
---

Lenengotxu ori
punta-beatz ori,
beste guztien aldean
trokotza dok ori.

Bigarrentxu ori
punta-beatz ori,
beste guztien aldean
bizkorra dok ori.