---
id: ab-740
izenburua: Kukubiku
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000740.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000740.MID
youtube: null
---

Kukubiku anianiku
zozoak umeak sasian ditu,
birigarroak adarrean
kaka egin zuen bizarrean.