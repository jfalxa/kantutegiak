---
id: ab-1177
izenburua: Uriko Jaia Baita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001177.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001177.MID
youtube: null
---

Trinidadeak eta Kopo Kristoak (sic)
eldu dirade Paskuak maiatzekoak.

Trinidadeak ditu iru aldare,
iru aldare eta zazpi lanpare.

Trinidadea baita bai Trinidadea
eskutik bolatu zait gabinarea.

Egotxu nabarrean derama kartea
urrez eskribiturik gainean letrea.

Gure baratzan ginda , ondu ezta ta mina,
gure kalean daude iru dama linda.

Irurak iru dira, irurak emen dira,
irurak elkarturik Arantzazun dira.

Gure ganbaran kutxa, kutxa bera utsa;
asko senartzat baino obe kutxa utsa.