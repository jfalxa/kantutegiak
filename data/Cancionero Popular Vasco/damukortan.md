---
id: ab-877
izenburua: Damukortan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000877.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000877.MID
youtube: null
---

Damukortan etxetzea,
Imintolan gisatzea,
Larruskaingo basatia,
Garramiolan aldapea,
Agoritan alkatea,
Alkate parerik gabea.
Olazarko printzipea,
Olabarriko joilea,
Amallobietako lukenagea
Egurrolako torrea,
aurrean daukat errotea
atzo askoren botikea,
an ei dago Maripena
dontzella estimadea:
aren atzeko putzak darabil
Gabiolako errotea.