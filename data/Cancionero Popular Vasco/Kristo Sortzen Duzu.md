---
id: ab-1241
izenburua: Kristo Sortzen Duzu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001241.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001241.MID
youtube: null
---

Kristo sortzen duzu gaur,
alegra giten guziok gaur;
Kristo sortzen duzu gaur,
dantza giten guziok gaur.

Natibitate-gabean oilarrak xo zuenez,
ama-semeak maiterik zeuden bi beso zainduen artean.

Eguberri, eguberri, gaur dala eguberria
Kristo xin duzu munduan eta dugun orok alegria.

Kristo xin duzu mundura guzion reskatatzera,
goazen guztiok gaur Belenera Jesusen adoratzera.

Verbum caro factum est
Maria beti Birjinik
Deus cari natus est
Maria Birjinaganik.