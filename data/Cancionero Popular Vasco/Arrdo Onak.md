---
id: ab-194
izenburua: Arrdo Onak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000194.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000194.MID
youtube: null
---

Arrdo onak, arrdo onak ema teiz kit indarrak,
arrdo onak, arrdo onak kentzen min ondarrak.
Miarritzen neskato erdaldunik gutixko
batailun bat balitzake Bordelera yoaiteko

Arrdo onak, arrdo onak ema teiz kit indarrak,
arrdo onak, arrdo onak kentzen min ondarrak
Ziburun bada atso bat urik edan gabea
osta tu orotan bego atso orren irudi.