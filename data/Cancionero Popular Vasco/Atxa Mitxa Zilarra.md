---
id: ab-712
izenburua: Atxa Mitxa Zilarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000712.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000712.MID
youtube: null
---

Atxa mitxa zilarra,
joan joan olara,
andi beltxen billara.
O Pitxon, biribiltxon,
txin txan txon.