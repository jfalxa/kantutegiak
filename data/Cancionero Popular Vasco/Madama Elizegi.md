---
id: ab-913
izenburua: Madama Elizegi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000913.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000913.MID
youtube: null
---

Madama Elizegi
ohean dago eri
¿nor du serbitzari?
Muxu delako hori
Tra la la ra lara la la ra la ra la ra.