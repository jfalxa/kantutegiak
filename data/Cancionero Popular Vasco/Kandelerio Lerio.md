---
id: ab-739
izenburua: Kandelerio Lerio
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000739.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000739.MID
youtube: null
---

Kandelerio lerio
atxari ura dario,
makatzari madari:
eutsi, Peru, ankeari.
Domurun Santurun Santurun
txarri andi bat il dogu,
buztana ez bestea yan dogu:
bera lapikoan daukagu.
Euria dakar menditik
Ondarroaren ganetik,
eztaukat zapatatxurik
banoa Lekeitiotik.