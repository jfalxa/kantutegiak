---
id: ab-306
izenburua: Agur Estebe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000306.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000306.MID
youtube: null
---

Agur Estebe ¿nola zirade?
Bizi zi reia oraino
Baibizi naiz eta gogo
hardiak bil arteraino.