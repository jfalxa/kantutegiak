---
id: ab-1017
izenburua: Goian Tenpluan Dago (Ii.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001017.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001017.MID
youtube: null
---

Goian tenpluan dago Amandre santa Ana,
iru lilia larrosa eskuan tuela;
iru eskuan eta bai zazpi buruan,
amabi mila aingeru koroia santuan.