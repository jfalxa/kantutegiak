---
id: ab-144
izenburua: Ikustera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000144.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000144.MID
youtube: null
---

Ikustera yiten nitzaitzu
nula ziraden maitea,
bizi luze bat opa deraitzut
dohain orotaz betea
eta gero hartarik landa
Paradisuan sartzea.

Ene maite, ikus ezazu
atean gaizki nizala;
eskuño batez aupa nezazu
zure etsarian barrnera,
aho azpian nahi dereitzut
edatsi ene izakera.