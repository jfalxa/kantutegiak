---
id: ab-1130
izenburua: Nafartarren Arraza
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001130.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001130.MID
youtube: null
---

Nafartarren arraza
¿hil ala lo datza?
Eztut endeglatzen,
Beltzuntze Bizkondea,
ain kapitan maitea,
ez baitzaut mintzatzen
hori zaut gaitzitzen.