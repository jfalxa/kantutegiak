---
id: ab-626
izenburua: Emen Eldu Naiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000626.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000626.MID
youtube: null
---

Emen eldu naiz bainan beldur naiz
atsekabeak izain ditudala maiz.
Etxe txar bat inen dugu
iratzez edo maltzoz,
eskallera kastainostoz
gaineko sala iratze maltzoz
biziko gara mundu onetan gustoz.

Aitzur goldeak akatz gabeak
daukazkit, senar, lantze ko lur ireak.
Nere lurrak ugaitz batek
barda era man zitun,
oy anak arturen gaitun
xorita abere pistiak ere
gurekin izain dire zorion lagun.

Traitara rai rai trai ta ra rai rai trai ta ra rai rai rai rai rai rai
rai rai rai rai tra la ra la.