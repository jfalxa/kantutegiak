---
id: ab-545
izenburua: Amodioaren Berri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000545.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000545.MID
youtube: null
---

¿Amodioaren berri
inork ongi otedaki?
Ni emen nago ¡gaixo au!
Ikusirik ongi
emanik mila goraintzi
neure gaztezaroari.