---
id: ab-595
izenburua: Negu-Goizalde Batean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000595.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000595.MID
youtube: null
---

Negu goi zalde batean,
albakoaren otsean,
eskopetea kargadu neban
arri otzaren gainean,
bai eta gero deskar gau bere apaiz
jau norren aldean.