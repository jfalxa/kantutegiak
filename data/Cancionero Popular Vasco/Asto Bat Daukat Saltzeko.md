---
id: ab-775
izenburua: Asto Bat Daukat Saltzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000775.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000775.MID
youtube: null
---

Asto bat daukat saltzeko,
Peliz erosten asteko,
kuartotxo bi nai nituke
eskuartean sartzeko.
Geure astotxo zartxua
Aitak etxean sartua.
Bost aldiz garbatu dako
berori ezagutua.

Asto belarri tristea,
bata bezela bestea;
dirurikan ezin artu
izanarren ustea.
¿Zergatik etzan gaztea
itxurabato trastea?

Peliz bizi da Zegaman,
astorik eztu eraman.
Albeiteruak bazekien
tratu orren berri boladan.
Ark esango dio nola dan.
¡Aiegon balitz Malagan.