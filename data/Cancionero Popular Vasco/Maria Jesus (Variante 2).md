---
id: ab-6026
izenburua: Maria Jesus (Variante 2)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006026.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006026.MID
youtube: null
---

Abendu Santua da ta
auda la jaiotzea,
semea egin dagero
Ama da dontzellea.
Maria Jesus,
Jesus Maria,
Maria Jesus.