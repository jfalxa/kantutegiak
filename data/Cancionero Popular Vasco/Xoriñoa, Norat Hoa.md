---
id: ab-179
izenburua: Xoriñoa, Norat Hoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000179.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000179.MID
youtube: null
---

Xoriñoa, norat hoa
bi hegalez airean.
Espainiala yoaiteko
elhurra duzu bortuan
bi biñoak yoanen
elhurra urtu denean.