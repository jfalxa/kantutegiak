---
id: ab-151
izenburua: Landetan Iratze
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000151.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000151.MID
youtube: null
---

Landetan iratze, behi ederrak aretxe, zu bezelako politetarik rai-tara
rai tai rai rai rai desir nuke bi seme.