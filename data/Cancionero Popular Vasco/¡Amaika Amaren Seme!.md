---
id: ab-544
izenburua: ¡Amaika Amaren Seme!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000544.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000544.MID
youtube: null
---

¡Hamaika amaren seme
dolorez aziak
era man bearko ditu
orain goauziak!
Ilak ilagatikan
aurrera biziak,
alduten guztiak,
eukitsu ezeukiak
ama etxean utxiak:
gerra au irabazteko
lau probintziak.