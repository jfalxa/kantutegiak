---
id: ab-649
izenburua: Oxinaren Azpian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000649.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000649.MID
youtube: null
---

l.- Oxinaren azpian
perejila jaio,
jaio bada jaio da,
bera dago sendo.

- Martinatxo andrea
bentanean (sic) dago
sartu bedi barruna
¿zer egiten dago?

- Luis gaztetxo jaun ori
biolina joten,
Martinatxo andrea
galantutzeatzen (sic).