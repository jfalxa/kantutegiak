---
id: ab-617
izenburua: Agura Zarkillun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000617.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000617.MID
youtube: null
---

Agura zarkilun barkilun bat,
geure ategian zegoan bart.
Ezetz ta ezetz esanagaitik
andrea faltako zuen ark.
Bart arratsean beranduan
atea nekez yo genduan.
Agura zarkillun barkilun bat,
geure ategian zegoan bart.
Andik aurrera sutondotxoan
panparreria bagenduan.
Deiez asi zan andreari
yagi zaitez nai yagiadi.
Agura zarkillun barkilun bat,
geure ategian zegoan bart.
Asi zan aratzen inguruak
nun ete egozen akukuak,
akuku edo makuluak,
agura paraketa mailoa.
Agura zarki llunbarkillun bat
gure ategian zegoan bart.