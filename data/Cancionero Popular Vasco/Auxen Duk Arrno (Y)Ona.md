---
id: ab-209
izenburua: Auxen Duk Arrno (Y)Ona
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000209.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000209.MID
youtube: null
---

Auxen duk arrno (y)ona Peraltakoa Peraltakoa.
San Antonek gorde dezala dezala
au karri duen mandoa
au karri duen mandoa.

Urak eztu gizona neoiz poztutzen, neoiz postutzen,
bainan mats arrno (y)onak urik gabeak
kantari eder egiten,
kantari ederr egiten.