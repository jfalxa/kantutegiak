---
id: ab-143
izenburua: Yiten Nuzu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000143.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000143.MID
youtube: null
---

Yiten nüzü hürrüntik
gayaren ülhünagatik
zure bihotzean sarrtüna
hiz beste (y) ororen artetik.
Gogorra zira zü ene,
bena sogiten dizüt emeki.