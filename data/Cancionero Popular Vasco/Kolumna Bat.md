---
id: ab-902
izenburua: Kolumna Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000902.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000902.MID
youtube: null
---

Kolumna bat igo zan
arkozko torrera,
milla kaballeria
lasterka aurrera;
gu a la bayoneta,
naiz erori bera;
sekula etzait aztuko
orduko sarrera.