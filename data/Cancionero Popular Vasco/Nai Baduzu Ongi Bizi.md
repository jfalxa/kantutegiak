---
id: ab-1044
izenburua: Nai Baduzu Ongi Bizi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001044.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001044.MID
youtube: null
---

Nai baduzu ongi bizi
bear duzu, Kristua,
Kristoren pasiua
biotzean iduki.
Maiz pentsatzearekin
alako eriotzea,
Ai Jesus nola liteke
berriz ofenditzea.