---
id: ab-842
izenburua: Salbatore-Arratsean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000842.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000842.MID
youtube: null
---

Salbatore arratsean
Arnegin etxe batean
hiru neska gazte elgarrekin,
andre ezkondu bat heiekin,
plazer hartuz ilunekin
gattulu gorriarekin.
Alabak Amari yayaya
gattulua betea da.
Amak dio alabari
edan, haurra, ttanka hori;
hire koloreak gorri horrek
behar tin ezarri.