---
id: ab-1251
izenburua: Sagartxo Txiki
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001251.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001251.MID
youtube: null
---

Sagartxo txiki betea,
aurten da zure urtea,
oain urrenako plazakon
itxeko andre onen partea.