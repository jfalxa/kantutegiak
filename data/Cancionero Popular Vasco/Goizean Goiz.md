---
id: ab-221
izenburua: Goizean Goiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000221.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000221.MID
youtube: null
---

Goizean goiz tabernara,
etxera berandu.
Ai zer mutil ederrak gu,
errenta bagendu.
Gure aitak irrika
bost aldiz esan du
enauk nere egun auetan
orrela izandu:
aukeran beti bizi
nai duenak lan du..

Egun mozkortzen bagera
¡biarko zer lana!
berriz laster biltzen gera
oro alkargana.
Edan dezagun bada
pinterditxo bana,
kaltzaren zulotikan
ageriz belauna:
gure diru guzia
ardoak derama.