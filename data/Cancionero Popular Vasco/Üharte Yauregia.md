---
id: ab-1175
izenburua: Üharte Yauregia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001175.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001175.MID
youtube: null
---

Üharte yauregia
bi uren artean,
kanthore hoien sujeta
han du aspaldian.
Bilhoa frijatürik
xapela bürüan:
haren parerik ezta
beste bat mündüan.

Adio Katalina
eta Mariana,
ni orai banoazu
Ezpainian barna.
Ni orai banoazu
Ezpainian barna,
ezpeiniz ützüliren
segürki berala.