---
id: ab-991
izenburua: Amen Aita Gurea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000991.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000991.MID
youtube: null
---

Amen Aita gurea
zeruetan zaudena,
santifikatua da
berorren izena
mundu guztian,
alkar ama dezagun
gure bizian.