---
id: ab-1030
izenburua: Irugarrengo Mandamentuak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001030.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001030.MID
youtube: null
---

Irugarrengo mandamentuak
onela digi agintzen:
obligazio degun denboran
guziok Mezara yan giten.
Laxakeriaz uzten duenak
eztue ondo egiten.

Meza santura dijoanari
egiten diot enkargu
jaun amoroso dultzearekin
¡oi! Kontenplazten ezpadu,
mezarik entzun eztuela
¡ai! Konsideratzen aldu.

Asko persona yoaiten gara
jai-egunean mezara
diferentzia batere gabe
plaza batera bezala:
gure gorputzean flakatasunak
Jinkoak entzun ditzala.