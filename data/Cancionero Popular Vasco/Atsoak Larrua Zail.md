---
id: ab-780
izenburua: Atsoak Larrua Zail
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000780.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000780.MID
youtube: null
---

Atsoak larrua zail
ta bear ere bai,
Mutrikun atsorik ez
eta bearrik ere ez:
triku triku Mutriku.