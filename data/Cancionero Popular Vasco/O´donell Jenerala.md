---
id: ab-923
izenburua: O´donell Jenerala
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000923.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000923.MID
youtube: null
---

O.Donell jenerala zutela agintari
panparroi or zebilzen etxeak erretzen.
Solamente jaun ori ezta gaitz izutzen,
txapela galdurikan Ernanira joan ze.