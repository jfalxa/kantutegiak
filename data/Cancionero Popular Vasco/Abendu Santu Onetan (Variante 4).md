---
id: ab-6015
izenburua: Abendu Santu Onetan (Variante 4)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006015.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006015.MID
youtube: null
---

Abendu santu onetan
Jesusen jaiotzea
atsegin aundiagaz
guztiok pozgaitean.
Maria y Jese
Jese Maria.