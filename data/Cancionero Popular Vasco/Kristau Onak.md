---
id: ab-1036
izenburua: Kristau Onak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001036.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001036.MID
youtube: null
---

Kristau onak bear luke yaian-yaian pentsatu
aste guztian zenbat bider egiten duen bekatu.

Bekatuak konfesatu, barkazioa eskatu,
gera Jaunaren mai-aurrean biotz samurrez auzpeztuz.

Kristau on bat il-orduan egiten da bi parte,
lenbizikoa obian datza aingeruak dei in arte.

Anima ona ¿zer dakazu beste munduko berririk?
Umildadea, karidadea; Jauna, nik eztet besterik.

Oriek biek baldin baituzu, ezta besteren bearrik,
Jaungoikoaren yauregian bizi izateko poz-pozik.

Aingerutxoak, ar zazute anima on au eskuti,
Paradisuan yarri zazute beste guztien aurreti.