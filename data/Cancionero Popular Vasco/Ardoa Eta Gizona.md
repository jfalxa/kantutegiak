---
id: ab-195
izenburua: Ardoa Eta Gizona
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000195.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000195.MID
youtube: null
---

Ardoa eta gizona dijoaz kantatzera.
ardanandere gizena entzule dutela (1)
Gizonak galdetzen dio: "ona y ote zera"
- Bonbilean (2) nago eta asi zaiz edatera (3)

Ardoak erantzuten du pozak zoraturik
"ezta nigana etorri zeu bezelakorik,
baituran izan (4) naute kortxoz estalirik (5);
zuk askatu (6) nazu ta ez utzi tantorik

- Arrdoa: klin klin klin zatoz bonbiletik (7) pozez,
puxkak nayago zaitut, ez guztia kolpez;
usaiz ona zera ta obea kolorez;
agoan muxtatzeko (8) barruratu (9) zaitez.

Agoan sarrtuezkero banuan esperantza,
eztarria dezulako (10) elkorra ta latza,
ondotxo arr nazazu, gozatu al baneza;
negarra eldutakoan (11); ori dezu (12) poza.