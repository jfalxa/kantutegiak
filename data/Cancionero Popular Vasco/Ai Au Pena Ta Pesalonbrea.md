---
id: ab-531
izenburua: Ai Au Pena Ta Pesalonbrea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000531.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000531.MID
youtube: null
---

¡Ai au pena ta pesalonbrea!
Zer erranen dut gehiago?
Sudurra ere luzatu zaigu
kokotza bainon berago.
Gure etxean bihar baino
gaur ilik banengo naiago.