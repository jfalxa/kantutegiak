---
id: ab-1115
izenburua: Itsu Gaisoa (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001115.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001115.MID
youtube: null
---

Itsu gaisoa ¿nondikan zatoz,
nondikan zabiltza
bide galdu onetati?
Kirrin kirrin kirrin kin.
Lengo batean nik ikusi det
zure emaztea ederrik eta galantik
kirrin kirrin kirrin kin.