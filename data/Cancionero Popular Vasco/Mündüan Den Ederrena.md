---
id: ab-162
izenburua: Mündüan Den Ederrena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000162.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000162.MID
youtube: null
---

l.- Mündüan den ederrena zeluan ekhia
lüreko anderetan oi ene maitea:
begia düzü nabara zuri gorri larrüa;
ene begietako xarrmagarria.

- Errak amodioa ¿niganik nuiz hoa?
Hilarazi gabetanik othoi, habilua.
Eztezadala galdü osoki gogoa,
xurit eztaquidan gazterik bilhoa.

- Badakizü aspaldi banabilala
ürzo baten ondoan oihaneala.
Orai etsitzen dizüt ezin dirodala,
ezin ikhusten beitüt ¡oi! baten egala.