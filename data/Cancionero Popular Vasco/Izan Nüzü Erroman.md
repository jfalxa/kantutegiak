---
id: ab-896
izenburua: Izan Nüzü Erroman
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000896.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000896.MID
youtube: null
---

Izan nüzü Erroman,
Jakan eta Loretan;
aita santuaren meza entzun
kardinalen konpaiñiin.
Arropa txarra ükhenagatik
ihürk etzaitan deus erran.

Barkoxeko apezak
oro dirade manexak
¿ezkinion othean ükhen ahal
Ziberuko seme bat,
aberatsaren bezain praube
afable lizatian bat.