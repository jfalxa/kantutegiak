---
id: ab-762
izenburua: Ai Angela
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000762.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000762.MID
youtube: null
---

¡Ai angela! Tumbalanbelan jajai
¡Ai Fermintxo! Edaizu gutxitxo.
Usoak papora papora nean
elorri baten gainean
¿nai dozu artu Simontxo
auzoko urregin gaztea senartzat?
Ez, aita; ez, Ama;
ezta neuk gura dodana.
¡Ai Anjela! Tunbalanbela jajai
¡Ai Fermintxo! Edaizu gutxitxo.