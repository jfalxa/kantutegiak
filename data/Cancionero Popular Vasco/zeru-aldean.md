---
id: ab-1278
izenburua: Zeru-Aldean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001278.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001278.MID
youtube: null
---

Zeru aldean izarra, errekaldean lizarrra,
etxe ontako nagusi yaunak urregorriaz du bizarra.

Urregorriz du bizarra eta zilar labratuz bizkarra,
eskutu lodiz eginik dauka elizarako galtzada.