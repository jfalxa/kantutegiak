---
id: ab-241
izenburua: Pello Joxepe Tabernan Dela
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000241.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000241.MID
youtube: null
---

Pelo Joxepe tabernan dela
atxo bat il da Garralden.
Emazte zurtzak atetik zion
"ator etxera len bailen
Xortatxo bana edan dezagun
sekulan il ezkaitezen"
Pelo Joxepe etxe ra orduko
lurr a e man zien ilari,
"ne gar mal koak noizko zi tuen"
galde e gin zion hurari
Gauzo yek gero.
Gotorr zerantzun,
orai ekartzan gosari".