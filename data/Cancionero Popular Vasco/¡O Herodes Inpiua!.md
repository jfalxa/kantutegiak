---
id: ab-1048
izenburua: ¡O Herodes Inpiua!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001048.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001048.MID
youtube: null
---

¡o Herodes inpiua!
Zerren hiz ahin hütsützen?
¿Zerren hik duk, tiranua,
Yesus persekutatzen?
Inozenter eriua
zerren derek emaiten?
Oro bat gizon yainkua
eztuk hik atzemanen.