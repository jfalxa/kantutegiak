---
id: ab-191
izenburua: Antonek Dauko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000191.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000191.MID
youtube: null
---

Antonek dauko besigua,
zezina legez gazitua,
gabon gabean arek bearrko yok
aukadan aukadan tragua
¡Ai oi! Alejo zatoan beterik baleuko
¡ai, oi! Maria Belants eradale barria.