---
id: ab-601
izenburua: Oteda Mundu Huntan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000601.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000601.MID
youtube: null
---

¿Oteda mundu huntan deus ere gai denik
Judu erratuari konpara daitenik?
Ez, ez da mundu huntanzioten gizonik
horrek bezain bizarra luzea duenik.