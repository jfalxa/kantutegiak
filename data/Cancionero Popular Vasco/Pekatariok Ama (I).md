---
id: ab-1055
izenburua: Pekatariok Ama (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001055.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001055.MID
youtube: null
---

Pekatariok Ama, Birjina, zaitugu,
zugan gure biotza guk ipintzen degu.
Egizu zuk, Maria, gugatik erregu,
eriotzako orduan ezkaitezen galdu.