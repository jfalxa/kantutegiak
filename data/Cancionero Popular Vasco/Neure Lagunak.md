---
id: ab-1134
izenburua: Neure Lagunak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001134.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001134.MID
youtube: null
---

Neure lagunak, lagun zakidaz
Santa Isabelgo lurrera.
Bart gauberdian jaioko seina
gaur belauniko jarri da.

Mirari ori eskribitzeko
bearra dogun papera,
tinterua ta plumea
bere zerutik etorriko da.

Kalbarioko pausua da (la)
pausu (txit) dolorosoa,
Jesukristo (gaur) il dala eta
triste dagola munduan.

Kalbarioko pausua da (la)
pausu (txit) dolorosoa,
Jesukristo (gaur) biztu dala ta
alegre dala mundua.