---
id: ab-1262
izenburua: Trebola Baratzekoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001262.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001262.MID
youtube: null
---

Trebola baratzekoa,
larrosa Maiatzekoa;
zuri begira ementxe gaude,
damatxo biotzekoa.
Zapata txuri paperez
euriak jo ta ezer ez;
errian damak badira baina
zure parekorik ez.
Lezok urre du erria,
Pasaiak Errenteria,
erostunean pasa diteke,
damea, zure gerria.
Eroztunean gerria,
mardula bularteria,
neure ustean ezta faltako
zuretzat merkataria.