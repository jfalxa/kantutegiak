---
id: ab-6037
izenburua: Atsoa ¿Zer Diñoe?
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006037.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006037.MID
youtube: null
---

Atsoa ¿zer diñoe iturriok gaitik,
Eztala geldituko uri au urbarik.(bis)
Mari Peliz ¿zelan zabiz?
Ondo ibili, gitxi gastadu,
doala astoxoa ta ortik olgadu.

Erromerian noa, eztaukat dirurik,
balentzianatxo zara saldu ezpadagit.
Mari Peliz...

Saldu egiten badot balentzianea,
zer erabiliko dot euritzan, maitea?
Mari Peliz...