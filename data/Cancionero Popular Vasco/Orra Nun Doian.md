---
id: ab-1247
izenburua: Orra Nun Doian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001247.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001247.MID
youtube: null
---

Orra nun doian
mendiak bera
Yoane ezneagaz,
seinari aia egietarren
ogi urunagaz.

Batak uruna, bestea baia,
artzainak eznea:
orra nun doian seinarentzako
bear dan guztia.