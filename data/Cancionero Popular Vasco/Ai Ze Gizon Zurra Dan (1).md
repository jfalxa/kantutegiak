---
id: ab-307
izenburua: Ai Ze Gizon Zurra Dan (1)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000307.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000307.MID
youtube: null
---

l.- ¡ Ai ze zurra dan Matxintxo gurea!
Prakatxo zarrak eta ona umorea.
¡Igaz artorik ez ta aurten il andrea!
¿Ze bizimodu dok ori, Matxintxo gurea?
¡Igaz artorik ez ta aurten il andrea!
¿Ze bizimodu dok ori, Matxintxo gurea?

- Eztot iñoiz ikusi gure Matxin lotan,
orduan bere al dauko pipea ezpanetan.
Pipeak ilten dautso barruko gosea,
pipa orri esker bizi da Matxintxo gurea.
Pipeak ilten dautso barruko gosea,
pipa orri esker bizi da Matxintxo gurea.