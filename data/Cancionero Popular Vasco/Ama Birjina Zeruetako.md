---
id: ab-989
izenburua: Ama Birjina Zeruetako
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000989.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000989.MID
youtube: null
---

Ama Birjina zeruetako,
indazu zere grazia,
zure semearen goralditzea
orai emen naiz asia;
orren bitartez geldi banendi
zerua irabazia.