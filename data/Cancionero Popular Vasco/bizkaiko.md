---
id: ab-1207
izenburua: Bizkaiko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001207.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001207.MID
youtube: null
---

"Bizkaiko tierras tienes,
erderaz ein on i"
asi dira esaten,
Adios mi txiki.
Si quieres venir paje
Por tal has beniro
Noche de Gabon Gaba
pasaras conmigo.

Goazen bada, goazen laster
guziok alkarrekin,
zar da gazte guziok naste
benaz serbitzekin.
Aur ederrra konsolatzen
kanta berri onekin,
goazen bada goazen laster
umore onarekin.
Artzai buru zuri bi,
Anton eta Peru,
Belengo portalera
etorri zaizkigu,
sartzen dira barrena
Manueltxogana,
obari (1) an egin diote
arkumetxo bana.