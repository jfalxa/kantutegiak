---
id: ab-895
izenburua: Justo Gizagaisoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000895.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000895.MID
youtube: null
---

¡Justo gizagaisoa!
eztek indar asko,
i baino obe likek
zamerdi bat lasto.
¿zertan autsi eztiok
sudur edo kasko?
Ta ra ra ra rai,
ta ra ra ra ra ra ra ra ra ra ra ra ra ra rai.