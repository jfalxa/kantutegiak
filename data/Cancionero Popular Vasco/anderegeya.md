---
id: ab-98
izenburua: Anderegeya
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000098.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000098.MID
youtube: null
---

Anderegeya zira garbia,
buruan duzu arrokeria:
hirur mutil gaztek
zu naiz emazte
elgarren artean
hizkaldi dute.
Izan bezate,
nahi badute,
ene ardurarik
heyek eztute.
Eztut nai ezkondu
ez hizka orrtan sartu,
komentu batera
serora noazu.