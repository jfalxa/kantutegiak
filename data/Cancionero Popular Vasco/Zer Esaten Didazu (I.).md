---
id: ab-1063
izenburua: Zer Esaten Didazu (I.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001063.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001063.MID
youtube: null
---

¿Zer esaten didazu, Madalenea?
Bizturik daukagula Zure semea
ta badoala Asentsio goizean zeruetara.