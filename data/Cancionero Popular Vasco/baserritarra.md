---
id: ab-783
izenburua: Baserritarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000783.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000783.MID
youtube: null
---

Baserritarra zegoanean
dirua kontatzen zirzirzir,
eskribau jauna aldamenean
barreaz itotzen kirkirkir:
Zun eta zun zun eta zun,
Isabela, Manuela,
Margarita, Katalina zun:
Peru largaidazu,
Mari oratuidazu.

Etxeko andrea ari zanean
sardinak erretzen txir txir txir
katar saloa zoko batean
muturra mizkatzen pzi pzi pzi:
Zun eta zun... E.a.