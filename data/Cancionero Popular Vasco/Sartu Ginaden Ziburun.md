---
id: ab-933
izenburua: Sartu Ginaden Ziburun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000933.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000933.MID
youtube: null
---

Sartu ginaden Ziburun, ustez ginaden seguru;
bi ezkongaiak ziren gazteak, gu zarrak ginan iru,
irabazi naiz zerbait diru: kukuak makur yo digu.

Garda txiki bat gaskoina, fier zebilen dragoina,
etzuela estimatzen ankapean gizona,
bizia ere gauz'ona, galdetu nion perdona (???).

Andik bidaramonean ama Birjina-egunean
Vintin garda ikusi nuen ene karga bizkarrean,
aski umore onean sartu zan aduanean.

Vintin garda begi-zuri, afer ona egin dauk ori,
lenago ere aditze da, banedukat igarririk (1)
zer mutikoa intzan i: orai prebatu aut ongi.