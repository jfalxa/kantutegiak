---
id: ab-917
izenburua: Mila Zortzireun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000917.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000917.MID
youtube: null
---

Mila zortzireun eta larogei ta bian
Burgosen sartu nintzan Maiatzaren bian.
Soldadua zer zan nik asko enekian,
orain asi naz sartzen gogoaren erdian.