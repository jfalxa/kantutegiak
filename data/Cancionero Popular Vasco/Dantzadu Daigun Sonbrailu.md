---
id: ab-353
izenburua: Dantzadu Daigun Sonbrailu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000353.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000353.MID
youtube: null
---

Dantzadu daigun sonbraillu bagilla zaigu azaldu.
bagilla datorrenean zarrok il go ituk
zarrok il go ituk biziko gaituk gazteak.
Begiratu ekiok sutako egurrari
dantzan dabilen neskatilla orrek ardatza dirurik;
erre mentauko leukek dantzan ezpalekik.