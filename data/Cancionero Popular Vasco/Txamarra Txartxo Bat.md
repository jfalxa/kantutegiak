---
id: ab-1263
izenburua: Txamarra Txartxo Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001263.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001263.MID
youtube: null
---

Txamarra txartxo bat, Jauna, zuri,
gorputz eder orren estalgarri,
emango nizuke gustu andiz,
luze-laburrean ongi balitz.
¡Ai, oi gabaren on!
¿yaio berri ori non degu, non?

Lagunak ere baditugu;
lau artzai eta bost unai gartsu (1):
Santxo, Peru eta Martin Anton,
Frantzisko, Paulo, Beltxior, Simon.
¡Ai, oi gabaren on!
¿yaio berri ori non degu, non.