---
id: ab-529
izenburua: Adios Izar Eder
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000529.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000529.MID
youtube: null
---

l.- Adios izar eder, adios izarra
zu zera aingerua munduan bakarra,
izar baten uzteak emaiten daut pena
zeren eta bainuen munduan maitena.

- Adios ta ongi bizi, ene maiteñoa,
neure begietako lili arraroa.
Biotzez zurekin ta gorputzez banoa:
yarraikiren zautazu zure amorioa.

- Izan naiz Aragoan bai eta Kastilan
hitz batez erraiteko Espaiña guztian:
eztuz ekhusi zu bezalakorik,
Nafarroa guztian zaude famaturik.

- Jarraikitzen ninduzun izar eder hari
nola mariñel ona bere orratzari
jende onak, atentzione ene arrazoin huni:
etziteztela fida amorioari.

- Amodioa duzu arrosaren pare
usaina badu eta ondoan arantze,
maitea, ni enainte egon zugana jin gabe
hil behar banu ere hirur egun gabe.