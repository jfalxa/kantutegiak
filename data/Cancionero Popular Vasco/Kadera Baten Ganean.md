---
id: ab-373
izenburua: Kadera Baten Ganean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000373.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000373.MID
youtube: null
---

Kadera baten ganean, txotxo;
kadera baten ganean;
beste kadera motxa daukazu
arri otzaren ganean;
onezkero kantzau zara
eta bestera barau zaitean.