---
id: ab-185
izenburua: Ay Ay
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000185.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000185.MID
youtube: null
---

Ay ay eta zer othe dut? ¿ala mozkor oteniz?
Hiltzera yin ban indadi
heritarzun hautarik,
arno hunik balin bada ez ekhar niri barberik.

Benedika dakiola ayenari ondoa.
Zeren eman bai terayo arnoari gozoa.
Fraidek ere e daten dute
ta serorek yastatzen;
beraz, ene Yinko ona ¿nork eztu maite arrnoa.