---
id: ab-125
izenburua: Eztut Nehoiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000125.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000125.MID
youtube: null
---

Eztut nehoiz onerespen sugarrez
ene bulharr hau hunen oinazez izan;
hedoik eztu, garr hunen itzaltzeko,
ur hotzik aski goitik isuri ukhan;
min gaitz hau ezin dezaket nik eraman.

Zuhaur zira ene bihozmin hauek
ezti ta yabal ditzakezun atxeter.
Maite, othoi, urriki zite nitaz;
hitz goxoño bat berio zure ezpainer,
zu ere nitaz zoritzeko halaber.