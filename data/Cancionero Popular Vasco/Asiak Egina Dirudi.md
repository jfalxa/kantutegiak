---
id: ab-774
izenburua: Asiak Egina Dirudi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000774.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000774.MID
youtube: null
---

Asiak egina dirudi,
eginak urre gorria:
milla neska baiño gurago neuke
etxean zorro andia.
Oterre barberua
Basabarleten koiñatua.
Aupa labirun labirun lena,
aupa labirun txatxalen.

Bart amarretan etxean nintzan,
amabietan basoan,
dama gazte bat bilatu neban
basabarleten besoan.
Atsoak yo ninduan bekokian,
ni ilentiaz ipurdian.
Aupa labirun labirun lena,
aupa labirun txatxalen.

Andra galantak Azkoitin dira,
galantagoak Azpeitian,
eldu nintzan neu Arratian
artazukua zirakian. Aupa...

Gure sala ta ganbara
urdetokia bezala,
ontziak ere izilupean
txakurrak garbi ditzala. Aupa...