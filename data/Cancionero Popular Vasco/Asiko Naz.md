---
id: ab-1198
izenburua: Asiko Naz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001198.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001198.MID
youtube: null
---

Jesus zeure arbolea
neguaren erdian
ekatxaren bildur baga
oraintxe dago loran.
Erantzuten arkaleri
klariñagaz, orgaiñuagaz,
musika kontzertaduagaz.

Salomonen tronu orrek
orain dauko famea,
Birjina Amak besoetan
geure salbadorea.
Erantzuten...

Asi dira pastoreak
arakaska propia:
Zein lenago eltzen diran
Belenengo portalean.
Erantzuten...