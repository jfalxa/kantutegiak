---
id: ab-864
izenburua: Andrea Gauza Baliosa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000864.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000864.MID
youtube: null
---

Andrea gauza baliosa da,
dudarik ezta, etxeko;
ni ere baten premian nago
beti deskantsatzeko.
Manera txarrak ikusten ditut
agudo esposatzeko;
dotea irabazi bear da
emaztegaia sortzeko.