---
id: ab-1201
izenburua: Belen Belen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001201.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001201.MID
youtube: null
---

Belen Belen da
Belenen jaio da
Jesus Nazaren.
Belen Belen da
Belenen jaio da
Jesus Nazaren.

Gabon gabeko amabietan
altzoan dantzan zerabilen.
Gabon gabeko amabietan
altzoan dantzan zerabilen.