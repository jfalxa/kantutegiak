---
id: ab-344
izenburua: Atzo Bilbon
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000344.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000344.MID
youtube: null
---

l.- Atzo Bibon nengoan
ta gaur (1) Bitorian,
sarri ura agortu da
gure iturrian.
O neska farandona
akerran bekoki:
zazpi arto zatita
baba lapiko bi.
La ra la ra la le ra la ra la ra la le ra la ra la ra la ra la
ra la ra la ra la re la la ra la ra la le ra la ra la
ra la le ra la ra la ra la le ra la ra la ra la ra la ra la ra
la ra le re la.

- Mendiolan eidagoz
iru neskatila;
asko egoten dira
eurari begira.
Bata da Mari Pepa,
bestea karlota,
irugarrena bere
txairo txairoa da.
La ra la ra la le ra la ra la ra la le ra la ra la ra la ra la
ra la ra la ra la re la la ra la ra la le ra la ra la
ra la le ra la ra la ra la le ra la ra la ra la ra la ra la ra
la ra le re la.

- Tirikitikitauki
¿or dona senarra?
Tirikitikitauki
basora yoan da.
Bart gaberdian
arek eroan katua!
A zekusenak utsik
ezeukan barrua.

(1) Gaur nau Bitorian, dijo la cantor.