---
id: ab-1226
izenburua: Goazen Goazen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001226.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001226.MID
youtube: null
---

Goazen, goazen Belenera
zar eta gazte guztiok
eramaten seintxuari
gabonsari kapoiok:
erdi bi ta lau bildots,
pitxar andi bat ezne,
sagar intxaur da gaztaiña
altzoa bete bete.