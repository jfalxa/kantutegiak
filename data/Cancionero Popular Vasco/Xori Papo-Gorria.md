---
id: ab-178
izenburua: Xori Papo-Gorria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000178.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000178.MID
youtube: null
---

Xori papo gorria
udan da kantari,
neguan ezta ageri
¿ez alda eri?
Udan yin baledi,
konsola naiten ni.