---
id: ab-1215
izenburua: Eguen Zuriko Astea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001215.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001215.MID
youtube: null
---

Eguen zuriko astea
Jaungoikoak emona guztia,
eskola mutillak ibilteko
limosna on baten billa.