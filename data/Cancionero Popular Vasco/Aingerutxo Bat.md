---
id: ab-984
izenburua: Aingerutxo Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000984.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000984.MID
youtube: null
---

Aingerutxo bat etorri zitzaizun,
ez otoi Maria durditu,
zeruko mandataria naiz
ta ondo aditu bear naizu,
ondo aditu, ondo aditu:
Jesusen ama bear dezu.

Jesusen Ama izateko
eztut merezimenturik,
Aita eternoak zu zaduzki
beretzat eskojiturik,
artuko dut nik zure fruitua
salba dadien mundua,
izanen da bedeikatua.