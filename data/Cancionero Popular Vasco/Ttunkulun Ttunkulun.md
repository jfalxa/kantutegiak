---
id: ab-843
izenburua: Ttunkulun Ttunkulun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000843.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000843.MID
youtube: null
---

Ttunkulun ttunkulunte
oherat nahi nuke Lope Lope
oherat nahi nuke zazpi arroltze
eta ogi andi-bat han baitaude
ene goaite ttunkulun ttunkulunte.