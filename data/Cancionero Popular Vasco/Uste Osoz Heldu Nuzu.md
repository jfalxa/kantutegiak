---
id: ab-249
izenburua: Uste Osoz Heldu Nuzu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000249.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000249.MID
youtube: null
---

Uste osoz heldu nuzu bidean, bakean,
ongi etorri on bat neula etxean sartzean.
Etxean sartzean, xutik sukaldean,
zapito eder bat duzula besoan,
bonbila eskuan, gandola bertzean.

Ostalersa berriñoa zira zu soizu;
gu ere zure hegokiak gituzu ai zazu.
Maiz e txen gituzu, beti egarritsu
"zuk neri nik zuri" gandola eskuratuz,
kantari hasteko, oi bezala gertu.