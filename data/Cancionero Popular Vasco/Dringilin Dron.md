---
id: ab-211
izenburua: Dringilin Dron
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000211.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000211.MID
youtube: null
---

Dringilin dron, gaur gabon,
sabela betea daukat eta
besteak or konpon
Mazkelo bete aza egosi,
orizuri ta gorriak:
bere ala iruntsi neutsazan
azkenengoko orriak.
Iru ortzeko tresnatxo batez
morokilore bailitzan,
ezti lapiko andisko bati
barrua uts uts ein neutsan.