---
id: ab-596
izenburua: Ni Sortzeko Denboretan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000596.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000596.MID
youtube: null
---

Ni sortzeko denbore tan
gaiza fran ko baz terretan;
atheratü nintzalarik
mündütako portaletan,
miseria yin zerai tan
zaldi zela bridetan,
zaldi zela brideta.