---
id: ab-1164
izenburua: Sarara Yin Da Berria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001164.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001164.MID
youtube: null
---

Sarara yin da berria
hamalau gizonen galdea,
delarik erri ttipia baina noblea,
pare gabea; aparantziaz
bazakiten bazela yendea.