---
id: ab-111
izenburua: Basoilarrak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000111.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000111.MID
youtube: null
---

Basoilarak kantatzen düzü
Iratiko basoan;
ihurke lezakezu pentsa
nik zer düdan gogoan:
gayak oro igaraiten tüt
maitearen ondoan.