---
id: ab-1119
izenburua: Xinkoak Dizula Egun On
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001119.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001119.MID
youtube: null
---

- Xinkoak dizula egun on, andere aurena
- Bai eta zuri ere, galai (1) zalduna.

- ¿Bokata latsan ari zira, andere aurena?
- Ekusi badukezu, galai zalduna.

- Esakuak gorri gorri tuzu, andere aurena.
- Urain barnean inik, galai zalduna.

- ¿Orain nun duzu senarra, andere aurena?
- Erregeren serbitzuan, galai zalduna.

- ¿Zonbat denbora fan zela, andere aurena?
- Orai zortzi urte, galai zalduna.

- ¿Eztuzie familiarik, andere aurena?
- Bi alabatto tit, galai zalduna.

- ¿Aurrak non tuzu goizean, andere aurena?
- Eskolan tit biak, galai zalduna.

- Senarrari , fan zenean ¿zer eman zinakon?
- Seta-makones bat nik eman niozun.

- So egizu ean au denez, andere aurena.
- Iduria dukezu hura bera dela.

- Hura ta bera duzu kau, Margarita.