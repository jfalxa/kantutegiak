---
id: ab-741
izenburua: Landan Eder Aitzurra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000741.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000741.MID
youtube: null
---

Folkloristaren moldaketa
Landan eder aitzurra,
artzain zangamakurra,
estudiante tunante batek
enganatzera nerama,
ark engana nik engana,
bakoitza bere launengana.

Eder datza goizean
artizarra itzaltzean
idiak marru, zakurrak zainga
alor aldera yoaitea;
aintzin gibel dabiltzala,
ni ere banoa yauzala.