---
id: ab-1227
izenburua: Goazen Goazen Korrika
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001227.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001227.MID
youtube: null
---

Goazen goazen korrika
Belena mendiak barrena,
al degun lenena,
ikusitzera jaioperria,
Jaungoiko aundia,
Jose ta Maria.
Eramango dizkagu
aurrari gaztaiña ezne guri,
bera da gidari,
adora dezagun biotz biotzetik,
bendizioz bete gaitzan
berak zerutik.