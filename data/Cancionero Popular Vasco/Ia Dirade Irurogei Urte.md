---
id: ab-890
izenburua: Ia Dirade Irurogei Urte
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000890.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000890.MID
youtube: null
---

Ia dirade irurogei urte
sartu nintzala pastore,
eguno bere eztot nik izan
otzaz onenbat portune (sic).

Mendi onerik illarmendira
irureun bat pastore (2),
Jaun zerukoak gorde nau, eze
ilgo nintzan neu bere.

Pastore santu benturosoak (3),
geldi zakiguz gaur emen,
geldi zakiguz, kanta dezagun
zer igaro den Belenen.

Kortesiakin gelditzearren,
ze ezin nengoke luzaro,
ganaduakin mendi altuan
gau txarra pasauta nago (4).

Oi au indriska ta iñetasia!
Oi au gabaren larria!
Ordu-erdirik luzatu bage
il zan artalde guztia (5)

Geure ama maite Andra Maria (6)
gure barri zeuk dakizu:
ordu-erdiko bizi izaterik
seguru iñoiz eztogu.

Zazpi oin luze, iru oin zabal
zerraldo bat apaindua (7),
antxe sartuta jartzen gaituez:
orra munduko pagua.

Errodillatxu, errodillatxu,
Birjiña billau-zalea:
aita zeurea arotza da ta
egin daiala esaizu.

- Zazpi tella ta iru latatxu
urrean askoko dituz,
egin daiala ermitatxu bat
deritxona Arantzazu.

- Oñatiarrak, oñatiarrak
ea bada gaur dirautsut,
zeuen San Migel Oñatikora
birritan eratsi nozu
irugarrenez erasten banazu
kontuan edukiko' tsut.

(2) Pudiera en el segundo verso decirse il dire por pastore, no sólo
por extirpar el barbarismo sino aun para que el cuarto verso tenga
sentido.
(3) Pudiera corregirse diciendo Goitiko artzain zoriz beteak "oh
vosotros, pastores de las alturas".
(4) Podría corregirse: Gizabidez gaur geldituko naiz, ezin nengoke
luzaro, abere-artean mendi garaian gau txarra igarota nago.
(5) Por indriska decia gaba, como en el segundo verso; por luzatu,
tardatu, y ganadu por artalde.
(6) En vez de Ama Birjiña Santisimea que dijo la cantora.
(7) Kajo bat adornatua dice el original.