---
id: ab-1206
izenburua: Birjina Maite
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001206.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001206.MID
youtube: null
---

Birjina maite, lotan dago
zure seme laztana,
seaskatxo bat ipinizazu
gure biotzen kutunai,
ongi bai ongi igaro ditzan
gabak eta egunak.
Polliki polliki ergin zazu
egin dezan lo ona.