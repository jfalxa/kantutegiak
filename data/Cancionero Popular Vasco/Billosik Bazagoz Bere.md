---
id: ab-1205
izenburua: Billosik Bazagoz Bere
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001205.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001205.MID
youtube: null
---

Billosik bazagoz bere
etzara zu, Jauna, pobrea.
Zeruan josirik dago
zeuretzako erropea,
Jesukristo.
Ay qué bonito
el Niño Jesucristo.