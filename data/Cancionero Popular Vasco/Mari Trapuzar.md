---
id: ab-830
izenburua: Mari Trapuzar
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000830.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000830.MID
youtube: null
---

Mari Trapuzar, iñarrartekoa
sasi artean yaioa:
egundo bere ezton ikusi
eure garrian gorua.

Aita ta seme tabernan dagoz
ama ta alaba yokoan:
ostera bere egongo ditun
abarka zarrak kakuan.