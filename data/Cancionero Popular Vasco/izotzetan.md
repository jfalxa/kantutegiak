---
id: ab-1239
izenburua: Izotzetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001239.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001239.MID
youtube: null
---

Izotzetan, elurretan, abenduko gau luzetan,
beste guztiak oian eta gu gaixuok oin utsetan.

Arantzazu ¿zer dakazu? Ur goiena, ur barrena.
Ur goiena, urbarrena, urte berri egun ona

Etxe onetan sar dadiela bakearekin osasuna,
bakearekin osasuna, onarekin ondasuna.

Etxe ontako nagosi jaunak poltsea txintxirrinduna,
poltsea txintxirrinduna, zirarrez ezpataduna.

Gure baratzean berar ona, ark botatzen du usai ona.
Goian goian irargia, erdi-erdian krabelina.