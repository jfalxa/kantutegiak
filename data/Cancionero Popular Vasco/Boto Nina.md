---
id: ab-252
izenburua: Boto Nina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000252.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000252.MID
youtube: null
---

Boto nina, boto nina,
boto nina, boto bi.
En Vitoria lo aprendí:
una moza neskato bat,
dos huevos arroltze bi.
Boto nina, boto nina,
boto nina, boto bi.