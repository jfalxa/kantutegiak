---
id: ab-293
izenburua: Oba-Obatxua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000293.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000293.MID
youtube: null
---

Oba obatxua txuntxulun berde,
oba obatxua masusta,
Aita gurea Bitorian da
Ama mandoan artuta.

Berreun du kattxu kastatu ditu
Bentabarrian sartuta,
beste hainbeste emongo leuke
Ama salduta baleuka.