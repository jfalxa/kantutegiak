---
id: ab-272
izenburua: Buba Ñiña
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000272.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000272.MID
youtube: null
---

Buba, ñiña;
lotzeko mina,
haurra dugu ñimiño,
lok hartzeko gaiztoño;
bubato, ñiñato,
haur tipia lo dago.