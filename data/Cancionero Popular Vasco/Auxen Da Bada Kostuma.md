---
id: ab-208
izenburua: Auxen Da Bada Kostuma
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000208.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000208.MID
youtube: null
---

Auxen bada kostuma, arnoz aseta deguna:
goazen aguardientera, ene lagunak.
Goazen beraz hemendik, oro alegerarik,
ikusi ezak orai ik ageri denetz argirik
aguardientea saltzen den hartarik.

Au heltzean lehen lana iten dugu "hela, hela"
adierazteko norbait badela.
- Heldu nuzu bereala, ahal dudan bezala
kotilon fola-alderdia esku-ezkerrean dudala:
yaunak, sar zaitezte, sar isil-isila.

Agur, etxeko anderea, pitz eraikuzu arrgia,
edan behar dizugu kutxot-erdia.
- Erraiten dautzuet egia, bihotzez garbi-garrbia,
akabatu hurran dela ene untzito tipia,
xorta xorta bat ezik bertze guzia.