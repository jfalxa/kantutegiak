---
id: ab-1091
izenburua: Behin Batez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001091.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001091.MID
youtube: null
---

Behin batez yoan ninduzun
Isturitzeko plazalat,
andre eder bat ikusi nuen
dantza-buruan zuhala.

Atsoño bati galde egin nion
- ¿Nongoa da andre eder hori?
- Murde Belzuntz: hori duzu
Agerreko alaba.

- Agerreko anderea,
hitz bate adi nezazu:
zure alaba Kattaliñaño
emaztetako indazu.

- Ena alaba Kattaliñaño
hitz emanikan dagozu:
haren ondoko pupuñañoa,
Jauna, plazer baduzu.

- Ai-ei ai-ei penaz eta
ai-ei ai-ei ai dolorez:
eiherala yoan behar eta
Murde Belzuntzen beldurrez.

- ¡Habil hortik, ergel xarra!
Ezdun (1) Belzunzen perilik:
Ihizira yoana ziagon
goizean goiztto yeikirik;
goizean goiz yeikirik eta
bere orak harturik.

- Kattaliñaño ¿norat xoaxi
xuhaur hola bakarrik?
- Eiherala, Murde Beltzuntz,
nihaur honela bakarrik.

- Kattaliñaño ¿nai duxuia
nik emanikan lagunik?
- Nik eztut lagun beharrik.
Lagunak nik ukaitekotz,
neure aitamen emanik.

- Kattaliñaño: baldin banaki
nor duxun eskolatzaillia,
paga ere nezakexu
xure eskolasaria.

- Ena eskolatzaillea da
herriko jaun bikaria,
ene aitamez pagatuia da
ene eskolasaria.