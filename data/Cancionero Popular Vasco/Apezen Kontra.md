---
id: ab-866
izenburua: Apezen Kontra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000866.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000866.MID
youtube: null
---

Apezen kontra giñan
alditxo batean,
eztutela ematen
artorik zorrean.
Errenkura y aundiak
alkarren artean,
ezkaudela yarririk
lenengo klasean.