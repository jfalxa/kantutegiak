---
id: ab-169
izenburua: Nun Zirade
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000169.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000169.MID
youtube: null
---

¿Nun zirade, ene maite oi xarmagarria,
ene phena doloren konsolagarria?
Nik banu gaurr zoria zuri mintzatzeko,
ene phena doloren zuri erraiteko.