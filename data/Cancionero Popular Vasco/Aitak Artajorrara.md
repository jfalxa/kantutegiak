---
id: ab-314
izenburua: Aitak Artajorrara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000314.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000314.MID
youtube: null
---

l.- Aitak artajorrara aginduezkero,
Aitak artajorrara aginduezkero
buruan min det eta soroan otz dago,
buruan min det eta soroan otz dago.
La la ra la lai la lara la la la la ra la lai lara lara la la.

- Agura trinkin-trankun ganbelapekoa,
Agura trinkin-trankun ganbelapekoa
baiño naiago nikek ogei urtekoa,
baiño naiago nikek ogei urtekoa.
La la ra la lai la lara la la la la ra la lai lara lara la.