---
id: ab-575
izenburua: Jauliera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000575.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000575.MID
youtube: null
---

Jaulierra, othoi beha:
egidazu plazera,
zu ¡othoi! goan behar zitzaitzat
ene adiskidetara
ea zergatik ageri naien
ilunpe triste huntara.