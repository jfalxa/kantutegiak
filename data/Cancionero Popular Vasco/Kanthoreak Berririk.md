---
id: ab-900
izenburua: Kanthoreak Berririk
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000900.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000900.MID
youtube: null
---

Kanthoreak berririk Baxe Nabarren eginik.
Nihor nihor ere balinbada ikasi nahi tuenik
kostaren etzaio deus ere atentzionea baizik.