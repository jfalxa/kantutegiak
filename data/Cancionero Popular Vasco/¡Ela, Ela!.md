---
id: ab-1217
izenburua: ¡Ela, Ela!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001217.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001217.MID
youtube: null
---

¡Ela, ela! ¿Nor da nor?
Ni naiz, urteberri
¿zer dakartzu berri?
Urarengaina, bakea ta osasuna,
etxekoandre ona,
nagusi obea, ireki ireki ateak,
bota bota urtatsak.
Txantxilipitona goiz afari ona,
kupela barrenean mamagoxo,
aurra yostatzeko ai zer afaritxo,
tira tira artilleria como la varia?
Grande borracho.