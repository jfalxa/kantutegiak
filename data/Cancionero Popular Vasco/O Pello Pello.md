---
id: ab-1143
izenburua: O Pello Pello
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001143.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001143.MID
youtube: null
---

- ¡O Pello Pello!
Irun diat eta
¿yinen niza bada oherat?
Kotxeira zan eta
gero gero gero,
kotxeira zan eta
gero gero bai.