---
id: ab-1103
izenburua: Garagarrilean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001103.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001103.MID
youtube: null
---

Garagarrilean eguna bero
goizean intza denean,
goizean intza denean eta
eguna klaro denean (1).

Nere laztana kalean dabil
aide aundiak konplitzen,
aide aundiak konplitzen eta
konbitearen emaiten.

Egun bat lagun ikusi nituen
eztai-etxean sartutzen,
beste orrenbeste ta bi geiago
eskaleretan igotzen.

Nere laztanaren eztaietan
nik yan nuen usoa,
nik yan nuen usoa eta
nere laztanak egoa.

Nere laztana mai-goienean,
ni barrenean egoiten;
ark neri ginu, nik ari ginu,
alkar genuen penatzen.

Alkar genuen penatzen eta
animearen lainatzen,
estudiante Bailarangoa
asi zitzaigun ubartzen.

Estudiante Bailarangoa
¿noiz eman bear dok mezea?
Ire mezearen diruarekin
egin bear diat lobea.