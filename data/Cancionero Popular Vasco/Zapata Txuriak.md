---
id: ab-1277
izenburua: Zapata Txuriak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001277.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001277.MID
youtube: null
---

Zapata txuriak paperez
euria danean bat ere ez.
Nagusi jauna, esan zaiguzu
asiko geran edo ez.
Erregeak datoz
presaz Belena,
guk billa ditzagun
all dan lenena.

Atari ondora eldu gera,
sartu gaitezen barrena,
etxe onetan esperantza det
agirandorik onena.
Erregeak datoz
presaz Belena,
guk billa ditzagun
all dan lenena.