---
id: ab-137
izenburua: Yarraiki Nezakezu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000137.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000137.MID
youtube: null
---

Yarraiki nezakezu, maiteño,
ezpazine huin gazte gaztexe;
zuri egin eregu, agintsari mailegu
salhazi nezazke, beldurr nindaiteke.

Elizan ziranetan estaliz,
ilun milun bederen, zazauzkit:
zuretzat dagit othoi zerorrezaz elizkoi
nizala kusirik ¡oi! maite zaitut nik.