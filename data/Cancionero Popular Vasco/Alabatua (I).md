---
id: ab-1191
izenburua: Alabatua (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001191.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001191.MID
youtube: null
---

Alabatua bedeinkatua
da Sakramentu santua,
pekatuaren mantxarik gabe
zeina dan kontzebitua.

Orrak or goian begira
otea dena lorez betea;
itxe ontako itxeko andreak
kofrean dauka dotea.