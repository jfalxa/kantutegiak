---
id: ab-610
izenburua: Sekula Santan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000610.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000610.MID
youtube: null
---

Sekula santan oritzeko dut
gaizumeko igandea
saindu guziak estaltzen ziran
egun oroipen garria,
Lazaro eguna señalamente
iluna eta tristea
orduantxen arrtugenduen
gure malurren astea.
¡Oi ai! orain artean
eztut deklaratu;
bainan amore on batean
bear ginduzke kantatu,
bear ginduzke kantatu.

- Goizeko lauak eta erdietan
ginen kostarat erori,
oiuka abiatu ginen gero
bornuko txanparterrari;
mastak errotik pikatu eta
urera ziran erori,
ongi tristeki so eiten ginion
gure untzien plantari.
¡Oi ai! orainartean
eztut deklaratu;
bainan umore on (?) batean
bear ginduzke kantatu (bis).

- Gure bornuko bi gizon gaztek
kuraya emanik elgarri,
bat zen bornuko txanparter eta
bertzea Beiñat Labadi,
larru gorrian biluxi eta
eman ziraren igeri,
beren arimak ofreiturik
zeruko Jinko jaunari.
¡Oi, ai! ..., etc.