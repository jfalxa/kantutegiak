---
id: ab-308
izenburua: Aiko, Juaniko (1)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000308.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000308.MID
youtube: null
---

l.- Aiko Juaniko Olabarriko
¿zer daukak aparitako?
Orio aza gozo gozoa
arto beroaz yateko: rrai ta rai lara la ra la ra la lai

- Kaspel andian intxaur saltsea,
pitxar morkoan txunpletin,
eragin eta beti eragin,
zarrok badaukek zeregin: rrai ta rai lara la ra la ra la la.