---
id: ab-92
izenburua: Ainoarra Ñimiño
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000092.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000092.MID
youtube: null
---

Ainoarra ñimiño,
ñimiño bainan agudo;
usin batetik bertzeraino
lasterr ingura bailiro.
Neskatxak ezetz erranagatik,
neskatxak amore dio.