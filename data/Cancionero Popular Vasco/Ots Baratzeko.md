---
id: ab-605
izenburua: Ots Baratzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000605.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000605.MID
youtube: null
---

- Ots baratzeko lili übelko,
urrin gozozko ats lagün
¿ezaldüzüte bihotz saminte
hunen berririk hor entzun?
Maitena düt isil ni behar nüzü hil
oinaze gorriz ¡oi! egün.

- Hodei gaineko zelü urdineko
izarrak haur balakite,
gain gainik yautsiz, nihaur hunetsiz
gaur eda liro argite,
izarren izar zori ororen gar
zihaur zirade ¡o maite.