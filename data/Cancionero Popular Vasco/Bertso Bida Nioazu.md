---
id: ab-559
izenburua: Bertso Bida Nioazu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000559.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000559.MID
youtube: null
---

Juan martín Bidaurreta
ni Erroko semea,
Amaren seme maitea
¡galdurikan utzi ditut
bi seme eta andrea!
eskuarterik gabea,
gaixo errukarria!.