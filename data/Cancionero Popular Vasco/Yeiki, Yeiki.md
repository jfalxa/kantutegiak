---
id: ab-811
izenburua: Yeiki, Yeiki
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000811.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000811.MID
youtube: null
---

Yeiki, yeiki Maria
pitz dezagun argia,
solairuan zerbait badun
sorgin iduria.
La la la la...

Aita zaude isilik
amets einbide duzu,
solairuan bi gatuño
yostetan dituzu.
La la la ...