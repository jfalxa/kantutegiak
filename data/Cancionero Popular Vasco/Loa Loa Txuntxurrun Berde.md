---
id: ab-283
izenburua: Loa Loa Txuntxurrun Berde
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000283.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000283.MID
youtube: null
---

Loa loa txuntxurrun berde,
loa loa masusta,
Aita gurea Gazteizen da
ama mandoan artuta: lo lo
Aita gurea Gazteizen da
ama mandoan artuta,
aita gureak diru asko du
ama bidean salduta: lo l.