---
id: ab-1025
izenburua: Jesus Onaren Ama Maitea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001025.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001025.MID
youtube: null
---

Jesus onaren Ama maitea,
grazia dizut eskatzen
bost misterio gozoz beteak
lagun ezazu esaten,
lagun ezazu esaten.