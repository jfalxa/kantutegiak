---
id: ab-957
izenburua: Dendari Gazte Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000957.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000957.MID
youtube: null
---

Dendari gazte bat gure auzoan
detreminatzen da ofizioan;
bere ariaz, bere orratzaz
irabazten du:
badu diru, badu boltsa
aberats da,
senar on bat eztu falta.