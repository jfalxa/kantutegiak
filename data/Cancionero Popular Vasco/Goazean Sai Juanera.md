---
id: ab-1104
izenburua: Goazean Sai Juanera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001104.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001104.MID
youtube: null
---

Goazean Sai Juanera,
Sai Juan kantatzera,
neu ezin joan nindaike
ain bere urrinera.

Katea luze dauko
ak ene biotzera
¿noz etorriko eteda
kate au libratzera?

Donianek urteoro
bere egunez goizean
señale ederra dakarsku:
izarra bularrean.

Izarra dogu Batista,
eguzkia Jesukristo