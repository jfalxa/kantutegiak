---
id: ab-578
izenburua: Itxas-Gizonto Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000578.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000578.MID
youtube: null
---

l.- Itxas gizontto bat zidan
senartzat nere Amak eman
¿Zerren ez legor aldera
yoan beste bat kartzera?
Orain zoria nuen nik
zu ri ta gorriz yantzirik.

- Gaixoa urak eraman
zuen San Markos goizean
¡oi! egun andi argia
neretzat ilun tristia
¡Arrainen yaki izateko
sor tu al zinan, senarko.