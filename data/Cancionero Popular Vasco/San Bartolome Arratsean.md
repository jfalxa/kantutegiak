---
id: ab-931
izenburua: San Bartolome Arratsean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000931.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000931.MID
youtube: null
---

San Bartolome-arratsean, Villafrancara jitean,
bersu berriak eman güntean hirur lagunek bidean,
Mugarre horlan zer gertatü den agorrilaren erdian.

Barber gaztea (y) adi zazü hizño bat, plazer badüzü,
eritarzun handi batek abantzu nüzü finitü;
arremediorik balin badüzü, orai beharretan nüzü.

Eda (y) ezazu besoa, mira dezadan foltsua;
sukhar-minik zuk eztüzü, fresko düzü larrua,
nundikan ere sufritzen düzün errazu, othoi, egia.

Nik erranen düt egia, anitz niz susprenditua,
azkarki dela mintzo herri huntako jendea.
Oilasko luma-gorri horrek nun egiten duen kafia.

Oilaxko luma-gorria, eman ezak guardia,
eztuk haizu unkitea oilanta txitan yarria,
ustekabean jin beitaite etxera haren jabea.