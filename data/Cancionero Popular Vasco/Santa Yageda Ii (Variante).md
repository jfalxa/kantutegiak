---
id: ab-6030
izenburua: Santa Yageda Ii (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006030.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006030.MID
youtube: null
---

Libertatea eskatzen diot etxeko printzipalari,
Santa y Agedaren alabantzak kantatutzera noani.

Graziaz diot nik ipini Santa bedinkatu oni,
kristiandadean onen izena beti santifika bedi.