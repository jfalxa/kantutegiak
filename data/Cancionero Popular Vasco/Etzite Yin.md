---
id: ab-124
izenburua: Etzite Yin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000124.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000124.MID
youtube: null
---

I
Etzite yin lan-egunez,
zato igande arratsaldez.
aitamak izain tun bezperetan,
bertze aurrideak abelzaiketan.
Nerau egoin naiz bakarrik,
neure maitea kusi nairik.
II
Asti gabe nuzu egun,
bertze igandea ezta urrun.
Emen ditugu pilotariak,
trebe-trebeak guzi-guziak:
yokaldi hunen koi nuzu,
nere yitea neke duzu.