---
id: ab-248
izenburua: Urin Zoragari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000248.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000248.MID
youtube: null
---

Urin zoragarri arritzekoa,
berdin gabea, ardoa:
artan dadu kat nere-biziko gogoa
Ekar balite Peral takoa,
ar niro agokoa,
benedika daki ola matsari erroa
bai eta ondoa
¡¡Urinen goxoa!!
Urin gozo indargarri ortarik
dastatze agatik
atera liro biotza saltzera bularretik.
Balitz era onik, urrikigabenik
edango nuke nik:
xortaz xorta eror bedi
zintzur orrengandik
poliki poliki
osailuño hori.