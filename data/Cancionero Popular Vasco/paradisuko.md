---
id: ab-1054
izenburua: Paradisuko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001054.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001054.MID
youtube: null
---

Paradisuko fruten artean
debekatu zan sagarra,
Eva ta Adanek hura yatean
ekarri zuten negarra,
etsai gaiztoak danak batean
egin bazuten an parra,
ugaldu zaite zu izaitean
infernua su ta garra.