---
id: ab-158
izenburua: Mendi-Goyetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000158.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000158.MID
youtube: null
---

Mendi-goyetan (1) aurrten ezeida
erromerurik loratu;
nik iminita igarrtu izan zan,
zuk iminita ezetu (2);
ama gure alde yarri badaigu,
alkarregaz bearr dogu (3).

Larrosea ta krabelinea
bana dira euren soinez (4):
aitak alaba aginduarren,
amak egin lei emon ez:
biak erara (5) izanezkero,
bitarrtekorik bearr ez.

Larosatxoak bost orri daukaz
kabelineak amabi.
Nire umea gura dabenak
eskatu bere amari:
ama zur daukan neskatilea
bizi lei arrdura barik (6).

- Goiko mendian edurra dago
errekaldean izotza:
neu zuregandik urre nagota
pozik dadukat biotza.
Zure ondoan udako bero
egingo dat neguko otza (7).

(1) Mendi altuan, decía él.
(2) Berrdatu.
(3) Frutuok ere onak dituzu, grazia alan badezu: decía trabucándose.
(4) Diferenteak kolorez.
(5) kontentu.
(6) Los cuatro primeros versos figuran también por lo menos en otras
dos piezas de este cancionero. El quinto y el sexto son del
folklorista; no los recordaba el cantor.
(7) Tampoco recordaba el cantor los dos últimos versos de esta
estrofa.".