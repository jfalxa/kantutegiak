---
id: ab-637
izenburua: Kantatzera Nuazu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000637.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000637.MID
youtube: null
---

Kantatzera nuazü
züekin batean,
parkamendü galtatzen
aphalki hastean.
Orok emanen dügü
nork gure heinean
bertsoxko zunbeit hunik
eztei egunean.

Xuberon sortu lili
eder hunek egun
Manex bat derakargu:
düela zori hun.
Gü ezkütüzü izanen
emazteka nihun:
uskaldün plaina daite
gure bizi lagün.