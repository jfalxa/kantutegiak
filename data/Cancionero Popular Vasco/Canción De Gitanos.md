---
id: ab-6017
izenburua: Canción De Gitanos
kantutegia: Cancionero Popular Vasco
partitura: null
midi: null
youtube: null
---

Heldu nuzu urrundanik
gaua bidean emanik,
gaua bidean emanik eta
ihaute dela entzunik.

Etxeko andere galanta
begia duzu xarmanta,
zure eskutik desiratzen dugu
xingara eta lukainka.

Etxeko andere ¿nun zira?
hila hala bizia zira?
lehengo hura balin bazira
emazte galanta zira.