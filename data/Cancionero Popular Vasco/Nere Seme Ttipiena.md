---
id: ab-1132
izenburua: Nere Seme Ttipiena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001132.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001132.MID
youtube: null
---

Nere seme ttipiena,
ik emaiten dautak pena.
Abil arat, abil onerat
masta gainaren puntarat,
ean ageri den untzirik
edo bertzenaz leiorrik.

Ai aita, eldu nuzu
biotza arras tristerik,
eztut ikusi ez untzirik,
ez eta ere leiorrik;
yoan bear baldin baduzue
il nazazue leenik.

Nere seme ttipiena,
ik emaiten dautak pena.
Abil arat, abil onerat
masta gainaren puntarat,
ean ageri den untzirik
edo bertzenaz leiorrik.

AI aita, aldu nuzu
biotza alegerarik;
ikusi dut orain untzia
bai eta ere leiorra:
zure alaba Maria
leioan yosten yarria.