---
id: ab-135
izenburua: Gorputz Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000135.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000135.MID
youtube: null
---

Gorputz ederra duzu arpegi pintatua
bein maitatuz geroztik ezin kitatua
Izarrez eta iruzkiz zare adoratua,
arbola ederra zera, ongi loratua

Duda anditan niagok, Piarres edo Gilen;
penak ematen tidak, berdin eznauk ilen.
Erremedio onak uste nuke badiren,
kausi al banezake saltzeko non diren.

Pausu pasatu eta itsas-egalean
erremedioketa udalen-lenean (1),
lagun bat ere banuen nik aldamenean,
ongi presuna fina, egiten denean.

¿Zer eginen dut bada, Kristo Lezokoa,
bertzek eraman berautet maite biotzekoa?
Orain bilatuko dut beste bat preskoa,
neure pena guztien konsolatzekoa.

(1) Udalen por primavera, además de las citadas localidades de AN, se
dice también en Bera y se decía un tiempo en Lequeitio (B).