---
id: ab-1084
izenburua: Arrosa Xuriaren Azpian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001084.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001084.MID
youtube: null
---

Arrosa xuriaren azpian anderea lokhartu,
elurra bezain xuri, iruzkia bezain ederrik,
hirur kapitainek zeraukaten gortez inguraturik.

Kapitainek yoan ziran beren zaldiak harturik
hartu zuten anderea mantoz hontsa trozaturik
gero Pariserat ereman zuten Aitak jakin gaberik.

Parisen ostaliersak bazuen salutatu berriak ere galdatu
¿bortxaz ala amodioz jina zara, andrea? Errazu.

Ez, ez, ene bihotza arras bortxaz jina duzu:
hirur kapitainek galeriatik harturik jina nuzu.

Kapitainek hori entzunik jiten dire andereagana
- Anderea, afal zite eztirik eta trankilik;
hirur kapitain izanen tuzu gaur zure zerbitzari.

Anderea ori entzunik hila zuzun erori,
kapitainak yoan ziren beren zaldiak harturik,
nigar egiten zutelarik andereari dolu eginik.

Handik hirur egunen buruan andereak aitari
aihu: ai aita, ni orai hemen niagozu,
birjinitatearen nahiz begiratu hilaren egon nuzu.