---
id: ab-650
izenburua: Santa Luzietan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000650.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000650.MID
youtube: null
---

l.- Santa Luzietan gaua da luzen,
San Bernebeetan dogu eguna.
Aukera betean bizi dan orrek
eztau. ez, erruki bere laguna.

- Azaro-egunetan ogia gogor,
eztegu-egunetan dogu biguna.
Senartxorik gabe bizi dan orrek
udan ikusten du negu iluna.