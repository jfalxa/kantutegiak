---
id: ab-564
izenburua: Elkitzeko Tenorea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000564.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000564.MID
youtube: null
---

Elkitzeko tenorea huna nun den yina,
urrikaltzeko baita kasu hortan dena
Maitearen uzteak emaiten daut pena,
zeren hura bainuen orotan maitena;
urrikaltzeko baita kasu hortan dena.