---
id: ab-6014
izenburua: Alabatua (II) (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006014.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006014.MID
youtube: null
---

Alabatua izan dedila
¡oi! Sakaramentu Saindua,
Birjina Amaren erraietan
zeren den kontzebitua.
Bai gugatikan, bai gugatikan,
bertzenaz ginaden galdurikan,
bertzenaz ginaden galdurikan.