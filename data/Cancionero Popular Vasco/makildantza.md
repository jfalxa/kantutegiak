---
id: ab-379
izenburua: Makildantza
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000379.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000379.MID
youtube: null
---

Lauri lauri lauri anka ezin irauli,
arratsean mozkorra, goizean egarri.
Lau lau lau dotea badut bainan
lau lau lau ardoak galtzen nau.