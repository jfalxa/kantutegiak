---
id: ab-541
izenburua: Ama, Eldu Naiz Dolorez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000541.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000541.MID
youtube: null
---

Ama eldu naiz dolorez
eskua bete odolez
apez gazte bat ilik eldu naiz
oi neure esku traidorez.