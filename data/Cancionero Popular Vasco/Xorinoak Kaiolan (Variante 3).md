---
id: ab-6012
izenburua: Xorinoak Kaiolan (Variante 3)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006012.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006012.MID
youtube: null
---

Xoriñoak kaiolan
tristerik du kantatzen,
dualarikan zer yan
zer edan,
kanpoa du desiratzen,
zeren zeren
libertatea zeiñen eder den.