---
id: ab-1265
izenburua: Txomin Da Patxi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001265.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001265.MID
youtube: null
---

Txomin da Patxi, Anton da Peru
datoz or goiko menditik,
aditurikan aurtxo eder bat
dagola estalpe batean ¡ai ori!
Lastogainean umildadean
jaun da jabea izanik,
lasto gainean umildadean
jaun da jabea izanik.