---
id: ab-1249
izenburua: Orrako Or Goian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001249.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001249.MID
youtube: null
---

Orrako or goian errota,
irina dakar egota,
etxe ontako etxekoandreak
Ama Birjina dabota.
Erregeak datoz
presaz Belena,
guk billa ditzagun
al dan lenena.

Orrako or goian ermita,
Santa Yageda deritza,
orko izkila jotzaileentzat
limosna billa gabiltza.
Erregeak datoz
presaz Belena,
guk billa ditzagun
al dan lenena.