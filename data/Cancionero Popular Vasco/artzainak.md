---
id: ab-1196
izenburua: Artzainak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001196.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001196.MID
youtube: null
---

Artzainak, berri onak: Aingiruak dio.
Aurtxo bat guziz ona Belenen da iaio.
Jesusa jaio eta amori bekarrik,
suaren egiteko etzaukan egurri.