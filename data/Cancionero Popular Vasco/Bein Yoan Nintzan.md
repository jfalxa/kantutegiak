---
id: ab-719
izenburua: Bein Yoan Nintzan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000719.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000719.MID
youtube: null
---

I: Bein yoan nintzan azokara,
erosi neban txakur bat:
txakurrak txau txau txau
nik txitxi berari;
ekarri neban etxera.

Bein yoan nintzan azokara,
erosi neban txori bat:
txoriak txio txio,
nik barriz negar lodi:
iges ein nostan basora.

Bein yoan nintzan azokara,
erosi neban txistu bat:
txistuak fli fli fli,
txakurrak txau txau txau
eroan notsan amari.