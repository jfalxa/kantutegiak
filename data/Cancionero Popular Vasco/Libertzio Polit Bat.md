---
id: ab-910
izenburua: Libertzio Polit Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000910.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000910.MID
youtube: null
---

Libertzio polit bat
buruan det pasa
iruritzen zaitala
txit ondo den gauza,
bertsoak paratzeko
artu det amesa,
san Joseren bizitza
kontay al baneza.

Birjinak artu zuan
San Josey esposo,
familiako kargak
al bazitzan jaso,
bizitza artako zazpi
dolore ta gozo
munduari nai diozkat
kantuz adirazo.