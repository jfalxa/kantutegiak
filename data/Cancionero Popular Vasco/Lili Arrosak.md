---
id: ab-588
izenburua: Lili Arrosak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000588.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000588.MID
youtube: null
---

Lili arrosak eder Maiatzean flore;
maite bat ukhen eta orai hu ra gabe.
Amodiorik ezta penarikan gabe.
Adios eneatseginak; mündükoak yoan dire.