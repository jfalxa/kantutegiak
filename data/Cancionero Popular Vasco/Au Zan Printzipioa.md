---
id: ab-869
izenburua: Au Zan Printzipioa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000869.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000869.MID
youtube: null
---

Au zan printzipioa San Juanetan
Amarekin Frantzian gasnaketan.
Zilatu digute aituna kopetan,
ardoa konketan, ogia ortzetan,
yan ta edan orretan kabala ta
Mukurru biak burrukan.