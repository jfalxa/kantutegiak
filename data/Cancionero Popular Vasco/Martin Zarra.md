---
id: ab-1125
izenburua: Martin Zarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001125.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001125.MID
youtube: null
---

Martin zarra barkatu;
zuk aitzakiak dituzu
Alaba yori baldin neretzat
azi ezpaldin badezu,
oraindaiñoko nere pausuak
pagatu bearko dituzu.

Gau eta egun emen nabila
etxe ontara puzkatzen;
desengañua izango otedan
bildurra dizut gastatzen,
Martin zarrari alaba baizik
bein ere eztiot eskatzen.