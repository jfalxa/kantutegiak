---
id: ab-1236
izenburua: Iru Errege Marruakoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001236.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001236.MID
youtube: null
---

Iru Errege Marruakoak
jetxi dirade Belena.
Belena joanda esan zioten
gazteenari zarrenak:
Ai gure Umea Jesus gurea,
Birjina Amaren Semea,
Birjina Amaren Semea.
Amen Amen Pello Patxiko barrenen.

Ekusi nuen Josepe
astotxoari txalmatzen,
astotxoari txalmatzen eta
Birjina Amari laguntzen.