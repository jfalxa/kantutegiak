---
id: ab-1088
izenburua: Astoa Heldu Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001088.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001088.MID
youtube: null
---

Astoa heldu da Baztandik,
arno txuriz kargaturik
tralara la la la lai
tralara la la la lai
arno txuriz kargaturik.

Otsoa berriz menditik
goseak eta egarririk
tralara la la la lai
tralara la la la lai
goseak eta egarririk.