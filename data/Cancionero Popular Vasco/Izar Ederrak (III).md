---
id: ab-1118
izenburua: Izar Ederrak (III)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001118.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001118.MID
youtube: null
---

Izar ederrak argi eiten dau
zeru altuan bakarrik;
eta eztakit jaio dan bertan
ala datorren zerutik.

Beraren argitan joan nintzan ni
Arantzura goizean,
Birjina (y) Ama eta Semea
topau nituzan bidean.

Kontseju on bat emon eustien
Ama Semien artean:
on da umila izan nendila
munduan nintzanartean,
eurak lagunduko eustiela
mundutik niñoanean.