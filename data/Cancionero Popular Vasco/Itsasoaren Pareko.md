---
id: ab-1031
izenburua: Itsasoaren Pareko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001031.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001031.MID
youtube: null
---

Itsasoaren pareko mundu
hau salbamenduko,
xume handiak garen guziak
etsaiarekin guduka
hari beldurrik borroka,
garen guziak
etsaiarekin guduka
hari beldurrik borroka.