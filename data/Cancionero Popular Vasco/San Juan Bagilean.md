---
id: ab-1161
izenburua: San Juan Bagilean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001161.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001161.MID
youtube: null
---

San Juan bagillean
denpora ederrean
edozein bedarrek usaina dauko
doniane goizean.
Da San Juan da San Juan
beti oi zaukadaz gogoan.
Ia da doniane edo
jentea triste dago
San Jaun bera bere
loak artuta dago.
Atzo San Juan San Juan
Zan da gaur da
San Juan Batista,
Jesukristoren lengusua da
San Juan Ebanjelista.
Da San Juan da San Juan
beti oi zaukadaz gogoan.