---
id: ab-296
izenburua: Sein Au
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000296.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000296.MID
youtube: null
---

ADAPTACIÓN del folklorista:

Sein au larrosa eder bat da,
kabelina da sein au;
sein au izpilu dala ta
amak begiratzen dau.
Oba, oba.