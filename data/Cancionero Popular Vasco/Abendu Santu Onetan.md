---
id: ab-1185
izenburua: Abendu Santu Onetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001185.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001185.MID
youtube: null
---

Belengo portalean
teila bako etxean
semea jaio ezkero
bera dala dontzellea.
Maria Jose, Jose Maria, Maria Jose.