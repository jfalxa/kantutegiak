---
id: ab-1189
izenburua: Agur Agur Yende Noblea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001189.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001189.MID
youtube: null
---

Agur agur yende noblea,
berri on bat dekargu,
atentzio on bat iduki beza
iñork aditu nai badu.
Adan da Ebak egin zutela
Paradisuan bekatu.
Ai ai ai Amandrea
¡ori da umildadea!
Nola dukazun narru gorrian
zeru ta lurren yabea.