---
id: ab-1002
izenburua: Bozkario
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001002.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001002.MID
youtube: null
---

Bozkario, bozkario,
bozkario munduan,
dugun lauda misterio
asmatua zeruan,
beti dela adoratu
zure jinkotarzuna,
beti dela adoratu
zure jinkotarzuna.