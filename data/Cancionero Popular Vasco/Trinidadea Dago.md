---
id: ab-1060
izenburua: Trinidadea Dago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001060.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001060.MID
youtube: null
---

Trinidadea dago Ergako lurrean,
zerutikan jetxirikan aitz baten gainean
¡fedea fedea! Zeruarekin lurraren bai kreadorea.
Berrogeigarren egun hura egun ezaguna zen,
bera bere birtutez zerura igan zen.
Bolatu bolatu, Aita eternoak dio eskutik oratu
¡fedea fedea! Zeruarekin lurraren bai kreadorea.