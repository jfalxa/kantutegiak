---
id: ab-807
izenburua: Yaun Zerukoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000807.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000807.MID
youtube: null
---

Yaun zerukoak laundu ninduen
andre eder baten xerkatzen;
ederra da bainan, galanta da bainan,
badaki arno edaten:
neronek ez dut ikusi bainan
yendeak ala erraiten.