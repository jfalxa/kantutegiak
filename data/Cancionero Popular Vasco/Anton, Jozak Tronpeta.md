---
id: ab-769
izenburua: Anton, Jozak Tronpeta
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000769.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000769.MID
youtube: null
---

Anton, jozak tronpeta.
Txomin ¿nun dek konketa?
Edaririk ezpaldin bada,
ekarrik beteta ¡ja-jai!
Peregrinuak datoz
Santiagotikan ¡ja-jai!
Iriki zak atea
ikusteagatikan ¡ja-aji.