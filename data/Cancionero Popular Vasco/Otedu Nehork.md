---
id: ab-929
izenburua: Otedu Nehork
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000929.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000929.MID
youtube: null
---

¿Otedu nehork sujetik
yaun hunek bezain politik?
Denborarik galdu gabe
Marexalen ondotik
eriotze bat egin izan du
behi ernari handitik.

Bederatzi hilabete
zituenean bete
behi gaixoa ezin erdiz
peritua eta gosea
estatu hartan denean
behar liteke ase.