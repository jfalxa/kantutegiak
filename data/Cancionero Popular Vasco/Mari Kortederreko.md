---
id: ab-381
izenburua: Mari Kortederreko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000381.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000381.MID
youtube: null
---

l.- Mari kortederreko
ostatu lorea,
Mari kortederreko
ostatu lorea,
berak bear dabela
senartzat eulea
¡Ai ene! ostatu lorea
¡Ai ene! senartzat eulea.

- Mari kortederrekok
gabean gabean,
Mari kortederrekok
gabean gabean,
errondea darabil
bere atartean
¡Ai ene! gabean gabean
¡Ai ene! bere atartean.