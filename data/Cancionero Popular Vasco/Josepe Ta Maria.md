---
id: ab-1233
izenburua: Josepe Ta Maria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001233.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001233.MID
youtube: null
---

Josepe ta Maria
beren Jesusakin
yoan ziran Belenera
gaurko gabarekin.
Belenen etxe batean
yo dituzte ateak,
erantzun baliete
beren amanteak.