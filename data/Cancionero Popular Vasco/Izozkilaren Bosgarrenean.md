---
id: ab-818
izenburua: Izozkilaren Bosgarrenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000818.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000818.MID
youtube: null
---

Izozkilaren bosgarrenean bosgarrenean
gazte bat sebdatzera torri zenean barbera,
altairatua zaukala emasabela,
ordenatu zioten zikiroltela,
ordenatu zioten zikiroltela.