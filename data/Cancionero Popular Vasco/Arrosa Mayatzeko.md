---
id: ab-105
izenburua: Arrosa Mayatzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000105.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000105.MID
youtube: null
---

Arosa mayatzeko kolore xurrigori polit polita,
zuri eman neraitzu lehen itza;
maitatu zaitut amodio betegin batez
zuk ere egin nezazu bertze batez.