---
id: ab-110
izenburua: Banabilazu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000110.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000110.MID
youtube: null
---

Banabilazu karriketan
indar gutixkoz huinetan:
isterbegiak soz ari zaitzat,
hats labür nüzü ta hil hülan,
bihotz hits hunek hitz bat
bentzu bizi enaitezun ezbayan.

Karrikan otz izanen zira
gai ülhün beltzez gaixo hori.
Amak pitz eta garretan düzü
oihaneko hoberen zurtxuri;
ene eretzean har ziniro
jaberik gabea den alkhi.