---
id: ab-1024
izenburua: Jesus Gure Jaun Maite
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001024.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001024.MID
youtube: null
---

Jesus gure Jaun maite,
amodio osoa damu dogu guztiok,
damu dogu guztiok zeu ofenditua,
zeu ofenditua.