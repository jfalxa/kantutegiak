---
id: ab-1114
izenburua: Itsu Gaisoa (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001114.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001114.MID
youtube: null
---

Itsu gaisoa ¿zer eiten dozu
pagu ondoan bakarrik?
Tantian tiarian lairu lena
pagu ondoan bakarrik.