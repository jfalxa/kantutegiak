---
id: ab-1107
izenburua: Goizean Parisen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001107.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001107.MID
youtube: null
---

Goizean Parisen, gabean Parisen
¡¡au da Parisko kaleen luzea!!
Auxe kale au pasata baneuko,
atzoko mando gorri au neurea.

Izerdi patsetan alboka-osketan
zazpi legoa bete dan kalea.
Zazpi legoak eginda baneukaz,
arrati etsera mando-yaubea.