---
id: ab-6027
izenburua: Iru Errege (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006027.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006027.MID
youtube: null
---

Iru Errege dator
orientetikan Belenera
auretik dakarela
argi izar eder bat
bidea erakustera.