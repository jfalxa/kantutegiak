---
id: ab-996
izenburua: Barkhamendu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000996.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000996.MID
youtube: null
---

Barkhamendu galdez nago
Jinkoari eta gero,
jende onak, zier orori.
Egin badauziet hobenik,
dolu dut segurki;
Jinkoak barka dezagula
bat bederari.