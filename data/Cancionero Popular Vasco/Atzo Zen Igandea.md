---
id: ab-205
izenburua: Atzo Zen Igandea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000205.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000205.MID
youtube: null
---

Atzo zen igandea egun astelena,
egia erraitea izain da oberena:
goan den igande arratsean zer zaidan gertatu,
soberaxko edan eta burua berotu.

Goizean yaiki eta mazte neureari
buruan min nuela ¡aunitz arrigarri!
Errespuesta eman zidan labur arrigarri
pultsua artzera nindoakiola tabernariari.

Yainkoak eman dio mazte neureari
matel-ezurra idorr ta mingaina zauli;
zenbait bezala izan balitz apez predikari,
garrgoitik zeruan egon zitien zenbait tabernari.