---
id: ab-1121
izenburua: Kurutze Santuaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001121.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001121.MID
youtube: null
---

Kurutze Santuaren
bai misterioa, bai misterioa,
ondo ari dezagun
jende umanoa.
Goian tenploan dago
amandre Santa Ana, Amandre Santa Ana
iru lili eskuan tuela;
iru eskuan, eta bai zazpi buruan,
bai zazpi buruan,
amabi krabelina
korona santuan.