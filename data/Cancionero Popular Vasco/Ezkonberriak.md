---
id: ab-629
izenburua: Ezkonberriak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000629.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000629.MID
youtube: null
---

Soineko zuriz, ain ge ru iduriz
etxeko alaba amak darama.
Se narrez goiz ornitzera,
opal mai aurrera,
malkoz begiak, biotza parrez.
Lorez yan tzia opaltegia,
orain yai kiada etxe ko alaba.
Sen arrez goiz orniturik,
zori uztarturik
ai banen go neu zu zeradenez.