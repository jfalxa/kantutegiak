---
id: ab-733
izenburua: Gaur Dala Biar Dala (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000733.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000733.MID
youtube: null
---

Gaur dala biar dala Doniane,
Etzi San Juan biaramone,
gure saloan sorgiik ez,
badagoz bere erre beitez.