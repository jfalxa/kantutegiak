---
id: ab-347
izenburua: Biri-Biri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000347.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000347.MID
youtube: null
---

Biri-biri pandangoa
zeu zera nere gogokoa;
zeuk bere bai ta neuk be re bai-
esan biozu ez ado bai
Rai rrai rrai rrai trara rrai
traka traka rrai traka trai
Rai rrai rrai rrai trara rai
traka tra ka tra.