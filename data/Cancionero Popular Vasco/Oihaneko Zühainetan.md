---
id: ab-924
izenburua: Oihaneko Zühainetan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000924.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000924.MID
youtube: null
---

Oihaneko zühainetan
eder zühainik gorena,
Europako populuetan
famaturik üskalduna;
hura dago ederrena
kantabriaren semea,
glorius bere lurretan
beti libro egon dena.