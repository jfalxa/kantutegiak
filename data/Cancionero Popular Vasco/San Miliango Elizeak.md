---
id: ab-1253
izenburua: San Miliango Elizeak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001253.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001253.MID
youtube: null
---

San Miliango elizeak altuan dauko kapela,
yaian-yaian juntetan djako andre eder-erreskada.

Eder altaran liburu, usoak aren inguru,
etxe onetako txiki-aundiak Paradisuan aingeru.

Or goian dago iturrim ura pil-pil etorri,
etxe onetako aingerutxuak imajinea dirudi.