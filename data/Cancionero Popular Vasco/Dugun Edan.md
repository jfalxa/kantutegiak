---
id: ab-212
izenburua: Dugun Edan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000212.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000212.MID
youtube: null
---

Dugun edan dugun, edan
diruak zitiat sakelean.
Goazen errekara amorraiketara
gogoz apaltzeko gabean:
dugun edan dugun edan
diruak zitiat sakelean.
Amorrai bat amorrai bi,
bertze bortzek egiten tu zazpi.
Nor ezten egarri kusiren da sarri,
tabernan gaitzanean yarri:
amorrai bat amorrai bi,
bertze bortzek egiten tu zazpi.