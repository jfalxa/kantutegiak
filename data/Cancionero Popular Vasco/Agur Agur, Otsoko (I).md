---
id: ab-1071
izenburua: Agur Agur, Otsoko (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001071.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001071.MID
youtube: null
---

Agur agur otsoko
ongi etorri, astoko.
Arno zuria badiat emen,
iri edatera emateko.
Ez nauk ez egarri,
sobera nauk goseegi.
Atseko neri afaritako
ire buru bearri.