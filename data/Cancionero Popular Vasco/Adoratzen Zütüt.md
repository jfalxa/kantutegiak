---
id: ab-977
izenburua: Adoratzen Zütüt
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000977.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000977.MID
youtube: null
---

Adoratzen zütügü Jesus,
erregien erregia,
errege imortala
eta monarka insibilia.