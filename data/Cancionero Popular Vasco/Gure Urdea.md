---
id: ab-805
izenburua: Gure Urdea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000805.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000805.MID
youtube: null
---

Gure urdea erdi emeretzi txerriz,
hurak anditzeareki ogei urde larri;
lakrikun firariarena, lakrikun fira Marirun.