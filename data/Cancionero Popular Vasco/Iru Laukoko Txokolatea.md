---
id: ab-371
izenburua: Iru Laukoko Txokolatea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000371.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000371.MID
youtube: null
---

l.- Iru lauko txokolateak
alper eingo dok lauko:
gona gorriak salduagaitik
eztok usua galduko,
gona gorriak salduagaitik
eztok usua galduko.

- Urioleak eroan deustaz
neuri aurtengo liñoak.
¡Ai sekulako izutu nintzan!
¡arrek eroezan brinkoak!

- Gorua balitz lo egotea,
barriketea ardatza,
neure emazteak egingo leuke
bazterrak bete matasa.

- Urioleak eroan deustaz
umeak eta andrea;
eztot besterik sentitzen baia
bai Matxalentxo neurea.