---
id: ab-718
izenburua: Bat Txardina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000718.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000718.MID
youtube: null
---

Bat txardina, bi txardina,
gorexema Bazkua xina,
xera xera xera,
la cuaresma fuera,
entre la carnal, sale la abadexal.

Urzo txuriak elizara
urzo beltxak eburñeara.