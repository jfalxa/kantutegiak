---
id: ab-141
izenburua: Jin Bazira
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000141.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000141.MID
youtube: null
---

Jin bazira, jin zira;
segur hunki jin zirela.
Erraiten dizit egia
berantetsi zútúdala.
¿Nun egon zire hola,
horren beste denbora,
jin gabetanik ene ikhustera?
Han badüzü harremanka
etzira dolu ohi nitzaz agrada,
etzi denean nahi izan
nian ene konsolatzera;
jin zaizkit othoi ardüra,
debei erazi gabe nihola.