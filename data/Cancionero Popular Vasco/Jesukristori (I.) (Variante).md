---
id: ab-6002
izenburua: Jesukristori (I.) (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006002.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006002.MID
youtube: null
---

Jesukristori kendu ezkero
pekatuakin bizitza,
baldin ezpadot negar egiten
arrizkoa dot biotza.