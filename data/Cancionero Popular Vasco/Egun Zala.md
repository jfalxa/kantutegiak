---
id: ab-563
izenburua: Egun Zala
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000563.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000563.MID
youtube: null
---

Eguna zala, eguna zala,
bart irargia zanean,
egun erako yoan nintzan ni
zazpi legua bidean,
zazpi legua bidean eta
neure maitean kalean.

- Neure maiteak yaurtigi (1) eustan
lantzea bentanerean.
Orain il bere banozu eta
beatu (2) bere naikezu,
María Madalenean aurrean eta
Jesukristoren oinean.

- Iru letratxu bearko ditu
nire obiak (3) ganean,
neure maiteak irakurteko
Madrildik datorrenean.

(1) Bota egin eustan, dijo la anciana.
(2) Beatu se decía por enterrar en B hace no más que medio siglo.
Enterrau dijo la cantora.
(3) Sepultureak dijo ella por nire obiak.