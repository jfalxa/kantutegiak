---
id: ab-1165
izenburua: Senarra Degu Londresen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001165.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001165.MID
youtube: null
---

Senarra degu Londresen
senda belarrak ekartzen,
eztu ark asko pentsatzen
gu nola geraden dantzatzen
¡Ai oi egia! Uzta ttunkulu Maria.

Nere mutiko txoria,
ik esan uan egia:
Perutxorekin Maria
dantzatzen zala zolia (1)
¡Ai oi egia! Iretzat zaldi gorria.