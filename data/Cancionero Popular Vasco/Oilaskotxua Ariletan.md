---
id: ab-839
izenburua: Oilaskotxua Ariletan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000839.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000839.MID
youtube: null
---

Oilaskotxua ariletan,
oiloak uruna eralten,
sagutxu txikia yosten,
arratoetxua labratzen,
astoa tanbolin yoten,
aren guztien artean bere
zorria barrez itotzen.