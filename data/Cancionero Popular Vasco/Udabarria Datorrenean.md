---
id: ab-846
izenburua: Udabarria Datorrenean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000846.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000846.MID
youtube: null
---

Udabarria datorrenean
neuk yakingo dot zer egin:
artu atxurra eta palea
Naparroara tellagin.
Larala..., etc.