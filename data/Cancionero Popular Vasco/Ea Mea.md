---
id: ab-729
izenburua: Ea Mea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000729.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000729.MID
youtube: null
---

Ea mea gurutzetara dea,
ea mea ofizioetara dea.