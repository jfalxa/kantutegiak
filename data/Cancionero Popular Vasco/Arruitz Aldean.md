---
id: ab-342
izenburua: Arruitz Aldean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000342.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000342.MID
youtube: null
---

Arruitz aldean izandu dela
amar ezkutu galdu tuela
galduak galdu nik nuen pena
inork yakitea nai enuela.
Lalara la la ra la la ra la ra la ra la la la ra la la ra la ra la ra
la ra la.
La la la la la ra la la la la la la la ra la la ra la ra la ra la.
La la la la ra la la la ra la la la ra la la ra la la ra la la la la
ra la.