---
id: ab-852
izenburua: Abenduan Ogeian
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000852.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000852.MID
youtube: null
---

Abenduan ogeian
San Tomas bezperan
mundu guztiak daki
nola irten geran.
Ayan Txantonenean
txiki bana edan,
geroztikan eztakit
laguna non dedan.

Hogei ta bakarrena
San Tomas eguna
txangoa Donostira
guk artu deguna.
Neu an gelditurikan
izan det laguna,
eztet nik arren damu
Lezoraino juna.

Pasayan onuzkoan
pronto zan batela
izena lonbratzen det
aditu bezela:
bat Joxepa zan eta
bestea Mikela,
amociomentean
pasa ninduela.

(1) Birajea dijo la cantora.