---
id: ab-108
izenburua: Aurra, Begira
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000108.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000108.MID
youtube: null
---

Aurra, begira ezazu leyotik atarira,
ea mutil oriek yoan ote diran.
Ez, or somatzen ditut solasari begira;
gargiro landan barrena igaro dira.

Nik aditzea dudanez, Leitzan mutil onik ez;
lazatuko zaituzte solas ederrez,
Ala balira biotzez nola diraden itzez;
etzaituzte erreko bela ur otzez.