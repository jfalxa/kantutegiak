---
id: ab-810
izenburua: Igaz Ere San Juan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000810.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000810.MID
youtube: null
---

Igaz bere Sai Juan
Sai Juan zan da aurten bere
Sai Juan Batista,
Jesukristore lenkusua da
Sai Juan Ebanjelista.