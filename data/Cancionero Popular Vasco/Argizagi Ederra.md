---
id: ab-103
izenburua: Argizagi Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000103.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000103.MID
youtube: null
---

Arrgizagi ederra,
arrgi egidazu;
orainobide luzean
joan beharr nuzu.
Maitea nahi nikezu
gaur behin mintzatu;
haren bortaragino
argi egidazu.

Urrundanik heldu naiz
bide luze huntan
urrats pausu guziez
bethi pentsaketan.
Atorra bustia dut
bulharr errainetan;
plazer bazindait idor
zure sahets hortan.