---
id: ab-1078
izenburua: Ama Ezkondu (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001078.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001078.MID
youtube: null
---

- Ama, ezkondu, ezkondu, denporea danartean.
- Ena alaba ¿zer diraustazu? Ogirikan ez etxean.
- Galburutxu bat topatu neban bidean ostikopean,
zortzi anega kendu neutsazan aurreko bote batean.
Ukulun dringin maspelen dringin
sankulun de blanko txuntxurrun berde
txitxibiribiri rrak eta plau
ardauak onela ipinten nau.

- Ama, ezkondu, ezkondu, denporea dan artean.
- Ene alaba ¿zer diraustazu? Ardoarikan ez etxean.
- Masmordotxu bat topatu neban bidean ostikopean,
zortzi kantara atara niozkan aurreko lakaiña batean.
Ukulun dringin maspelen dringin
sankulun de blanko txuntxurrun berde
txitxibiribiri rrak eta plau
ardauak onela ipinten nau.