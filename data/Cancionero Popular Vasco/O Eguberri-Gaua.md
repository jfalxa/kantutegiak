---
id: ab-1047
izenburua: O Eguberri-Gaua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001047.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001047.MID
youtube: null
---

¡O eguberri gaua,
bozkarioako gaua!
Alegeratzen duzu
biotzean kristaua.