---
id: ab-1153
izenburua: Orriak Airez Aire
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001153.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001153.MID
youtube: null
---

La doncella:
Orriak airez aire ifartxo dultzea
landa-mintz ederrean doan errekea.
Antxe topatu neban Birjina Maria
orraztuten zebala buruko ulea.
Etzan a ulea ez, ezpada urrea,
ulondo bakotxeko zerin perlea.

La Santísima Virgen:
Urreratu zakidaz, arima maitea,
benetan maitatzen dot nik zure fedea.
Eskintza orregatik zeuk, neure alabea,
artu izango dozu zeruko koroea.

La doncella:
¿Nun dago, Amandrea, Zerorren Semea,
zerutik yatsiriko gure mesedea,
berak argitu daidan neure arimea,
ikusi daian gero zeruko bidea?

La Santísima Virgen:
Or goiko munatxoan oinak ortotzean,
keriza emoten deutsan ganeko arantzean.
An dago bada neure arantz-larrosea,
zuri ta gorria da neure amorea.
Begiak ditu baltzak, urrezko ulea,
aoa txit ederra ta zoragarria.
A maite eztabenak bere biotzean
¿zer maitatu daike damurik bagean?
Bere ondoren dabiltz zeruko aingeruak
ibarrak apainduten lora usaintsuak.
Txoriak bere pozez soñu alegrean
kantadu daroakez guztiak batean.
Irribarrez diardutso landako loreak
yantzirik bere ondran soineko barriak.
Lurreko abereak bere mesedean
yaio ta arrazkero dagoz ardurean.
Zeuekaitik bakarrik zeruti lurrera
yatsi da nire Jesus gizon egitera.
Agaitik, neure alaba, kantadu daigun kanta
berari emoteko bere alabantza.

La doncella:
Agur, Jesus laztana, Jainko ta gizona,
zu beti izango zara gure zoriona.
Zeureak gara guztiz gorputz eta ariman,
izango gara bere bizitza guztian.
Izan gaitezan zugaz aitaren etxean
Espiritu doatsuaz Jainkotasunean.