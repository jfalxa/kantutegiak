---
id: ab-927
izenburua: Orain Abiatzen Naiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000927.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000927.MID
youtube: null
---

Orain abiatzen naiz
bertsoen paratzen:
gazterik iltzen eztena
ariko da sartzen,
batzuek ontzen eta
besteak gaiztatzen:
zeinbat orrelako da
munduan pasatzen.

Lenik konfesatu ta
geroko komulgatu;
propositu firmerik
ezpezuten artu,
horrela egitekotan
ak zer balio du?
Jauna onratu bearrean
desonratzen degu.