---
id: ab-841
izenburua: Sai Juan, Sai Juan Dala
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000841.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000841.MID
youtube: null
---

CORO
Sai Juan, Sai Juan dala
engañadurik nago
ezedo bada Sai Juan
jentea tristea dago.

Estrofa 1
Oreganu Sai Juan goizean
yoan nintzan ortura zarbatan,
zarbearen ondoan batana
ta orixe da neuronen laztana.
Estrofa 2
Sai Juanetati ile bi negura
agura zarra daukan andrea galdu da
neuronek gaztea daukat eta eztost ardura.