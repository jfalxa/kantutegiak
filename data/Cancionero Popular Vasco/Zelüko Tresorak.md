---
id: ab-1062
izenburua: Zelüko Tresorak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001062.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001062.MID
youtube: null
---

Zerüko tresorak idekirik
orai dirade guregatik
Jin zaizku zelüko mezulerak
Jinkoaren partez igorriak,
jakitate oroz argiturik,
photer oroz beztiturik,
bekahtütik gure soltatzeko,
ifernutik begiratzeko.