---
id: ab-131
izenburua: Gizon Bainiz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000131.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000131.MID
youtube: null
---

Gizon bainiz eta ama baten semea
hatsanka gorritan
igaraiten dut bizia
eta zu, maitea,
ene oinazea, bihotza atxei ratia
¿maitetar zunaren gaizki ezartzea,
ezteya min handia?

Mündüan ederrena zelüan ekhia,
lurreko anderetan
¡oi! ene maitea:
nabarra begia,
begitarteko larrua xoragarria,
ene begietako xoragarria,
zutut ene maitea.