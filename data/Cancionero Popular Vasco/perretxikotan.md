---
id: ab-175
izenburua: Perretxikotan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000175.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000175.MID
youtube: null
---

Perretxikotan nenbilenean
Mendiolako basoan
dama gazte bat aurkitu neban
areiztxu baten ondoan.