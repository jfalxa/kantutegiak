---
id: ab-107
izenburua: Atea Ttan Ttan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000107.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000107.MID
youtube: null
---

Atea ttan ttan yo nizun
eta etzaundan erresponditu.
Berriki ere yo nizun
eta zer nahi nuen galdatu.
Ene maitea, nahi nike
zu zure hitz bat beharriratu
zure hitz bat beharriratu.
II
Xakurra xaingalari dizugu
harek salaturen gaitu.
- Ogia sakelan dizut eta
hartarik emanen diogu (bis)
III
Gure xakurrak eztitu nahi
bertzek emanek ogirik.
- Urzo ta eper emanen diogu
ezpalin bada bertzerik. (bis)
IV
Atea ere karrankari dugu
harek salaturen gaitu
- Olioa sakelan dut eta
hartarik emanen diogu. (bis.