---
id: ab-928
izenburua: Orain Para Biaitut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000928.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000928.MID
youtube: null
---

Orain para biaitut
Agedaren kausak,
propinaz eldu zaizka
aukerako gauzak:
zapata txarolatu
zilarezke bletxak,
mantalin zedazkoa
enkajea beltza;
damorren graduari
tokatutzen zaizka.