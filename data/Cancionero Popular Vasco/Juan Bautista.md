---
id: ab-580
izenburua: Juan Bautista
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000580.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000580.MID
youtube: null
---

Juan Bautista da nere izena,
bakardadean bizi naizena.
Maria ori alargun zela,
ni aren gana pretenditzera,
arreglatzera;
sartu orduko aren itxera,
iru gizonek preso artzera.