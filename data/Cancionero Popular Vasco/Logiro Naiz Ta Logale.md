---
id: ab-639
izenburua: Logiro Naiz Ta Logale
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000639.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000639.MID
youtube: null
---

Logiro naiz ta logale
argi oilarrak yo gabe,
bart arratsean gure atean
gazteak etziran logale.
Nagusi yauna ateratzen zaie
¿gazteak zer gura dezute?
Bautista yauna naizela eta
Josepa Andrea nai nuke.

Josepa andrea eramain duenak
dirua bear lituzke.
- Diruak ere baitugu eta
ortaz autatzen (1) gaituzte.

(1) El que dio la segunda versión decía famatzen.