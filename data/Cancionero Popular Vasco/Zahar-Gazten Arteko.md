---
id: ab-942
izenburua: Zahar-Gazten Arteko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000942.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000942.MID
youtube: null
---

Zahar-gazten arteko
hau da parabola
zuzen esplikatzeko
anitz gogor da;
gaztea eztitake
adin batez molda,
gorputza sendo eta
azkar du odola,
zaharra ezta hola
iragan denbora
heldu zait gogora
eta ezin konsola
nekez biurtzen baita
zahartu arbola,
nekez biurtze baita
zahartu arbola.