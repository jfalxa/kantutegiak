---
id: ab-565
izenburua: Ene Senar Au Ilotzik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000565.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000565.MID
youtube: null
---

Ene senar au ilotzik,
nerau iletaz yantzirik,
aur gaixotto auek umezurtz,
laratzondoa otz ta uts;
gogorra akigu, Erio
¡¡antzia animari berio!!

- Bekaizti naun lur, erroren,
senar-yabe aizen orren;
nik dakarnadan bezela
osorik izan ditela
ta ortxe beste xoko bat
gerta zan errorek neretzat.