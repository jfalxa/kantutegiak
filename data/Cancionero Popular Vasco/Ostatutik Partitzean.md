---
id: ab-1154
izenburua: Ostatutik Partitzean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001154.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001154.MID
youtube: null
---

Otatutik partitzean
alegera bihotzean,
gero tristatu nindian bidean.
Etxera yoaitean,
zer behar nuen aditu
bortaren joitean,
emaztea jin zitzaitan
kolera handian.