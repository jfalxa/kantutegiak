---
id: ab-583
izenburua: Kantuok Yartzen Ditut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000583.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000583.MID
youtube: null
---

l.- Kantuok yartzen ditut
Paubeko urian,
burdinaz loturikan
gogor baitegian.
¡Otoi! ikas ditzaten
gazteak herrian,
nere bidez hunera
etorr eztitean.

- Aita, nai zuelako
semea zuzendu,
semeak bere eskuz
gogorki yotzendu.
Nerau naiz gaizkin andi
nintzana andiendu.
¡Nizaz, zeruko yauna,
oi! erruki mendu.