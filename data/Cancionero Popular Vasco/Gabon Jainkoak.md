---
id: ab-630
izenburua: Gabon Jainkoak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000630.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000630.MID
youtube: null
---

I Gabon Jain koak dizue la
agur, itxekoak,
agur itxekoak eta
auzo guztikoak;
agur agur bereziki
maztea ren ekoak.
San Martin de las monjas
monjas de San martin,
toberak yo ditzagun
Birjina Amarekin
Birji na Amarekin eta
ordu onarekin.
Oia yan tzi rik dago
burruko birekin
burruko birekin eta
maindire berriekin
bigiretan iruniko ari
sen dos koekin.
A zer eguna duen
zuentzat argitu
eztei soinekoak yantzi
iduzkiak ditu,
gure laztan galantak
ederragoak bitu.