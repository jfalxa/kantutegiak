---
id: ab-357
izenburua: Esan Dot Len Bere Ta
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000357.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000357.MID
youtube: null
---

Esan dot len bere ta esatera noa,
uztarri gogorra da ezkontzeko arloa.
¡Ai Mari katalin! ezkontzeko arloa
¡Zu beti mutxurdin! esatera noa.