---
id: ab-835
izenburua: Neure Astotxu Maltzina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000835.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000835.MID
youtube: null
---

Neure astotxu maltzina,
yaio zinean antxina.
Pelizek dauko zurekin
kuadratu ezina,
ikusi eta mosina:
ez, gazte izan bazina.