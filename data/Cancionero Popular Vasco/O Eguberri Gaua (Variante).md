---
id: ab-6004
izenburua: O Eguberri Gaua (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006004.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006004.MID
youtube: null
---

O eguberri gaua,
pozkariozko gaua,
alegeratzen duzu
biotzean kristaua.