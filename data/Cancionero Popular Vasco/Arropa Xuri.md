---
id: ab-994
izenburua: Arropa Xuri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000994.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000994.MID
youtube: null
---

Arropa xuri ederra
Jaun onaren ohoretan,
jende onak jauntz dezagun
bozkarioz Bazkoetan.