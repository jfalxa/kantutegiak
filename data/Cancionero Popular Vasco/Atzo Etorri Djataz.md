---
id: ab-1089
izenburua: Atzo Etorri Djataz
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001089.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001089.MID
youtube: null
---

Atzo etorri djataz iru mandaturi:
Uso zuri eder bat ta aingeru eder bi.
Uso zuri ederra ¿zeruan ze barri?
Zeruan barri onak orain eta beti.

¿Niretzat zer dakazu zeruko gauzarik?
Olibo adartxo bat Birjinak emonik.
Erramu laster da ta igartu ezpaledi.
Ezta bada igartuko zeruko gauzarik.

Igartu balei bere barriz dakarket nik.
Zeruan asko dira olibo adarrik.