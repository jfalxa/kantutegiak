---
id: ab-1079
izenburua: Ama Ezkondu (III)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001079.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001079.MID
youtube: null
---

Ama, ezkondu; ama, ezkondu, gaztea nagon artean.
Neure alaba, ezkonduteko eztaukat ezer etxean.
Galburutxo bat topatu neban gal denporea zanean,
berreun anega atera niozan lenengo igortzi batean.