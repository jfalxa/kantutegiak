---
id: ab-1083
izenburua: Arantzazura
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001083.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001083.MID
youtube: null
---

Arantzazura egin dut promes
ero gau ero egunez (1),
ero gau ero egunez eta
oinutsean eta dolorez.

Izar eder bat ateratzen da (2)
urtean egun batean,
uretan egun batean eta
hura San Juan-goizean.

Aren argitan ni joan nintzan
Arantzazura- ustean,
Ama Birjina ta aren semea
opa nituan bidean.

Kontseju on bat eman ziraden
Ama-Semeen artean,
on ta umilla ni izateko
munduan nintzan-artean.

Ai artzaitxoa, artzaitxoa,
nere esan bat egizu,
nere esan bat egizu eta
nerea izanen zera zu (3).

Ermitatxo bat egin bear degu
deritzola Arantzazu,
iru lata ta lau bost tellatxo
asko izanen alditu.

Denborarekin ura izanen da
ermita kuriosoa
ermita kuriosoarekin
komentu poderosoa.

Asko amaren bai seme maitek
an izanen du ostatu,
an izanen du ostatu eta
urrekilaza goratu.

Itxeak bana binaka dira
Arantzazura-bidean,
itxea itxe señalea da
arbola eder bat aurrean.

Arbola eder bat aurrean eta
mats beltza beste aldean,
mats beltza beste aldean eta
iturri otz bat aurrean.

XI.Iturri atan bataiatu zen
Kristo Erredentorea,
Kristo Erredentorearekin
guztien Salbadorea.

XII.Jende guztia miran zegoan
usoa nora zijoan,
ego txuriak an busti eta
Parabisura dijoa.

XIII.Ama Birjina Arantzazukoak
eguteran ttu laioak (4),
batetik itzal darionako
bestera aldatzekoak.

XIV.Ama Birjina Arantzazukoak
urregorriz du koroa
urregorriz du koroa eta
India-sedaz (5) bueloa.

XV.¡Ai! Berak iruin ta berak aundu (6)
beraren donarioa