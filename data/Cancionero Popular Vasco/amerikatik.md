---
id: ab-767
izenburua: Amerikatik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000767.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000767.MID
youtube: null
---

Amerikatik itzuli eta
onuntzen Burdeosen,
ango fondetan eperrengatik
propina ederrik omen zen.
Andik itxera etorri eta
dirurik etzen agertzen;
yango bazuen, aizkora artuta
laster sasira yoan zen.

Belaunak pisu, ertzeak zimur
begietako tristura,
¿zer deabrutako yoan zinaden
goiko mendian kaskura?
Osasunaren konserbatzeko,
gorputzarentzat freskura,
¡bazter guziak iratsi eta
liberal oien eskura.