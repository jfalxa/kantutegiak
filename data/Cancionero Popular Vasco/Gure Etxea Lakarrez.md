---
id: ab-800
izenburua: Gure Etxea Lakarrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000800.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000800.MID
youtube: null
---

Gure etxea lakarrez,
batzuetan bat ere ez,
auzoko gizon xuhur orrez baino
yago gasta ezpalez,
etxea beteko ginuke
urrez eta zilarrez.