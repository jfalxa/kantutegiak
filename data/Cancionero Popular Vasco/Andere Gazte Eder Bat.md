---
id: ab-1081
izenburua: Andere Gazte Eder Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001081.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001081.MID
youtube: null
---

Andere gazte eder bat zegoen gaixorik,
neork etzakiela gaitzaren berririk,
miriku bat ekarri zakoten argitzat
eta unek aginduta sendagarritxo bat,
eta unek aginduta sendagarritxo bat.

Andereak goiz batez zion gorriturik
etzela berarentzat bear mirikurik,
ez botikariorik, bazen agurea:
bera gaitz hura zela ezkondugalea,
bera gaitz hura zela ezkondugalea.