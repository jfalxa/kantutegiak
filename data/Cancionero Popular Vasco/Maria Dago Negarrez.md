---
id: ab-742
izenburua: Maria Dago Negarrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000742.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000742.MID
youtube: null
---

Maria dago negarrez
masaila urtu bearrez
urra labiru labiru lena,
urra labiru labiru lon.
Urra labiru labiru lena
urra labiru labiru lon.