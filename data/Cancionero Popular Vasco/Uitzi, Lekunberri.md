---
id: ab-940
izenburua: Uitzi, Lekunberri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000940.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000940.MID
youtube: null
---

Uitzi, Lekunberri, Etxarri ta Aldatz,
probe gertatu dire aurten lurrak babaz;
Arruitz ori bide da guzien burue,
en izango y omen da saltzeko modue.

Madotz eta Odoritz, Astiz te Mugiro,
Jaungoikoak oietan zerbait eman digo;
Barabar ta Iribes, goazen Aillire,
orietan eztue eraiteko diñe.

Errazkindik Albisu, Azpirotz, Gorriti;
babak pasea libre kamio berritik.
Amazazpigarrena an dago Lezeta:
ikazkin baterikan orietan ezta.

Baba zarrik saltzeko iñork baldin badu,
anegak sei ezkutu egiten omendu;
eztut bada nik senti Larraunen bestetan,
Uitziko errien iru.lau etxetan.