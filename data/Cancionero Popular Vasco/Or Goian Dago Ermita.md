---
id: ab-1245
izenburua: Or Goian Dago Ermita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001245.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001245.MID
youtube: null
---

Or goian dago ermita
Santa Yageda deritza.
Ango Serore mutilak gera
limosna billan gabiltza.