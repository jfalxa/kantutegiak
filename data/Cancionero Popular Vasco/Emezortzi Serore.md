---
id: ab-214
izenburua: Emezortzi Serore
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000214.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000214.MID
youtube: null
---

Emezortzi serore eta bortzemagin
buruz arindu eta sartu dire sorgin,
uxta egiten dute larun bat gauerdin
erratz girtain gainean zaldiz doazanin.
Ujuju jupa labirulena jupa jupa beti
odei beltzen azpiti sasien gaineti.
Sorgin oroin Errege da Akelarren yarri
¡Jupa! erran dezagun beti bizi bedi.
Oilo beltz ederr orrek daukan egitea
emakume zarrenak sorgin biurtzea,
karakakak eginta aren arroltzea.
Ujuju jupa labirulena jupa jupa beti
jupa Manuel Antoni jupa zanpantzarri.
Sorgin oroin Errege da Akelarren yarri
¡Jupa! erran dezagun beti bizi bedi.