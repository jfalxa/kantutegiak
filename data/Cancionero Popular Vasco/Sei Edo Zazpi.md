---
id: ab-1059
izenburua: Sei Edo Zazpi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001059.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001059.MID
youtube: null
---

Sei edo zazpi umeren amak
bat iltzeaz du negarra.
¡Maria Santisimak bat izan
eta hura gugatik il bearra!
Yende umanuok, adi dezagun
Ama Santaren nigarra.