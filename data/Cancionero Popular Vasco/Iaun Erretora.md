---
id: ab-1112
izenburua: Iaun Erretora
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001112.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001112.MID
youtube: null
---

Yaun erretora, yoaiten zara
Goienetxeren bortara;
andikan oiuz ast'emen zara:
Katalin gaixoa ¿non zara?
Grado (y) ortako gizonarentzat,
yauna, erretira-ordu da.

Bertsu ori ongi emana da ta
urranena aldiz obeki.
Nari duenak adituren du,
nik esanen dut klaroki:
ilunduz geroz eztela propi
apeza bortan andreki.