---
id: ab-400
izenburua: Soñuzale Ona Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000400.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000400.MID
youtube: null
---

Soñuzale ona da Josepe danbolina,
soñu oren erara dantzattzen bagina.
Ezpal din badaik ere eragiten ankari,
ilunabarra ezkero guziok dantzari.