---
id: ab-721
izenburua: Bonbolostina
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000721.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000721.MID
youtube: null
---

Bonbolostina bonbolostina,
otsoak yan du gure artzaina,
otsokumeak bildotsen zain:
Adios gure artalde zaina.