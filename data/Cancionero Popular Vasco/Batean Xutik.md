---
id: ab-210
izenburua: Batean Xutik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000210.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000210.MID
youtube: null
---

Batean xutik
eta bestean erori,
andreari heltzen zayo
gizun mozkor hori.
Lala lalara lala lara lala la lala la
andreari heltzen zayo
gizun mozkor hori.

Ni nuzu Martin
eta zu zira Katalin
leihoñoa idokazu
barnera sar nadin.
Lala lalara lala lara lala la lala la
leihoñoa idokazu
barnera sar nadin.