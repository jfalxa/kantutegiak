---
id: ab-264
izenburua: Aurtxo Txikia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000264.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000264.MID
youtube: null
---

Aurtxo txikia negarrez dago.
Ama, emaiozu titia.
Aita gaiztoa tabernan dago
pikaro jokalaria.

Aita-semeak tabernan eta
Ama-alabak jokoan,
ostera bere izango dira
atora zarak kakoan.