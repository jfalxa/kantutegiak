---
id: ab-251
izenburua: Abu Nina, Katalina (I)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000251.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000251.MID
youtube: null
---

Abu nina, katalina,
gure aurrak lotzeko mina;
Abu nina katalina,
lokar zite tia Fermina.
Bolon bat eta Bolon bi
en Vitoria yo lo aprendí:
buena moza neska ona,
dos huevos arroltze bi
bolon bat eta bolon bi.