---
id: ab-1140
izenburua: Nora Zoazen Erran Daztazu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001140.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001140.MID
youtube: null
---

- Nora zoazen erran daztazu,
Andre dena Maria.
- Ni banoaie Belenera,
Beleneko irira,
Beleneko irira eta
seroren ospitalera.

Altzina kamina zazie...

Ospitalekoak galde ziguten:
¿zek nongoak zerate?


Gori aitu zuenean
Josefe jaunak
nigarrari eman zue.
Gortxen goiti artzainak dira
goazen gu ere arara.

Arara bidea luze dago,
erortzeko peligro da.

Ama Birjina erditzen sarri
borda idiki batean,
borda idiki batean eta
idi-ganbela batean.

Amak Semea maiolatzen du
arrí otzaren gainean,
idiak atsez berotzen eta
mandoak ostiko emaiten;
idia benedikatu eta
mandoa madarikatu.