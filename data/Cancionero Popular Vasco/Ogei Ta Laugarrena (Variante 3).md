---
id: ab-6028
izenburua: Ogei Ta Laugarrena (Variante 3)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006028.gif
midi: null
youtube: null
---

Or goian jok etxe bat, Josepek jotzen du,
Gizona bentanatik nekez atatzen du.

Gizona bentanatik dago ots egiten:
¿Gau onen ordu bitan zer zalbiltza emen?

Birolin-soinuekin gloria kantatzen,
pozik eta kontentuz Jesus ateratzen.

Artzai bi buruzuri Anton eta Peru,
erregaloa franko ekarri daizkigu.