---
id: ab-1052
izenburua: Orra Bertso Berriak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001052.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001052.MID
youtube: null
---

Orra bertso berriak nik ditut paratu,
amar mandamentuak nola gobernatu.
Landabizikoa dela Yaungoikoa maitatu
eta lagu urkoa ongi estimatu.

Bigarren mandamentuan juramentu guti,
nai duena esatera mingaiña ez utzi;
eztu bada horrela diferentzia guti,
zerura igan edo infernura yautsi.

Meza osoa entzun irugarrenean
obligaturik gaude igande-egunean.
Obra (y) onak eginez orai al danean,
gloria gozatzeko eternidadean.

Laugarren mandamentuan geren gurasoak
ongi sokorri ditzagun, eztuten gaixoak.
Urrikaltzeko dire oien trabajuak,
alarik ere eztire ongi pagatuak.

Bosgarren mandamentuan inor ez iltzea
tristura andia da ¡o! Eriotzea.
Aski da Kristo Jauna ereparatzea
guregatik daduka (1) soiñean gurutzea.

Garbitasun egidazu seigarren orretan
gogoz, itzez, obraz ta ez ala lizunetan.
Kontu andia emazu mandamentu ontan
tentazio gaizto aunitz bada mundu ontan.

Zazpigarre mandamentuan deusik ez ebatsi,
kaso bereari ta bertzenari utzi.
Debruak egiten du, egiteko gaizki,
gu arrapatzeko sareak edaturik dauzki.