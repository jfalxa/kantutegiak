---
id: ab-217
izenburua: Gabilzan Kalez Kale
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000217.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000217.MID
youtube: null
---

Gabilzan kalez kale
umore onean.
Gabilzan kalez kale
umore onean.
Txomin yo zak tronpeta.
Patxi ¿non da konketa?
Edaririk ezpadago,
ekarri beteta.