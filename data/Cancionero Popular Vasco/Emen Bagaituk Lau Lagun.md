---
id: ab-213
izenburua: Emen Bagaituk Lau Lagun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000213.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000213.MID
youtube: null
---

Emen bagaituk lau lagun
dantza bat eman dezagun.
Ail errayok danbolinari
etor dadiela bereala
bere tuntun txirularekin
lok artu ezkaitzan onela.

Mandataria ¿Zer berri
ekartzen diraiguk guri?
Eldu dela bereala.
Gaiten guziok irribera
eta edan zurrutño bana
bonbilean baldin bada.