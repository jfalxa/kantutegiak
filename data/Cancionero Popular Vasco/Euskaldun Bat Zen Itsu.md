---
id: ab-1102
izenburua: Euskaldun Bat Zen Itsu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001102.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001102.MID
youtube: null
---

Euskaldun bat zen itsu
eta bertsolari,
atez aye zabilen
iloba gidari.
Soineko eta diru
yanari eta edari
nasaiki ibiltzen zuen
bertso eman sari.

Oi nere pikapeko
eun dukattxoak
orain agertu ditu
gure Jaungoikoak.
Todo lo quiere, todo lo pierde
berreun diralako
atoz bihar ere.