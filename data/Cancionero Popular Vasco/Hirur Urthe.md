---
id: ab-577
izenburua: Hirur Urthe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000577.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000577.MID
youtube: null
---

Hirur urthe baditutzu uraren gain ean ginela,
bi urtheren bitan txa baizik gurekin ezkinuela
irurgarrenean yan gititzun xakhurrak eta gatuak.