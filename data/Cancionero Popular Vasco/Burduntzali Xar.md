---
id: ab-955
izenburua: Burduntzali Xar
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000955.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000955.MID
youtube: null
---

Burduntzali xar arranbillote
errokeri soiñua jo diote
eta joko diote eta joko diote;
atea jo eta leiora,
Errokeren semea jaio da
eta jaio da eta jaioko da.
A un.