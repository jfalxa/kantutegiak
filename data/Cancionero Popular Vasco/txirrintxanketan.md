---
id: ab-754
izenburua: Txirrintxanketan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000754.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000754.MID
youtube: null
---

Txirrintxanketan akerra dantzan
astoa binbolina yoten,
Azegaria suari puzka,
erbia urun eralten.