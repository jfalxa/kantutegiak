---
id: ab-788
izenburua: Ene Andrea Yeiki Orduko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000788.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000788.MID
youtube: null
---

Ene emaztea yeiki orduko
zazpiak edo zortziak.
Hartik harat bere buruari
nahi dituen guziak,
neuretzat xardinarik ez eta
beretzat xokolatea.