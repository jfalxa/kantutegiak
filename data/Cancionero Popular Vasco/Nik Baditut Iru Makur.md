---
id: ab-837
izenburua: Nik Baditut Iru Makur
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000837.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000837.MID
youtube: null
---

Nik baditut iru makur,
asko nuke bat ere:
ardoedale, jokulari,
ez egin nai bearrik.
Ni artzen nauen dama gaixoak
errukarria bear dik.
Astan purrutan ustan Maria
a zer etxe saria.