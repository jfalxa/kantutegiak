---
id: ab-530
izenburua: Agur Maiteño
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000530.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000530.MID
youtube: null
---

Agur, maiteño, ola izateko
yoana dut ezkonmina.
¿Amorren begi bete beteko
bertze bat ager dakina.