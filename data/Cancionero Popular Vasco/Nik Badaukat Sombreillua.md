---
id: ab-1137
izenburua: Nik Badaukat Sombreillua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001137.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001137.MID
youtube: null
---

Nik badaukat sombreillua,
kastor fina, emaztegaiak egina.
Nik badaukat txaketea,
pañu fina emaztegaiak egina,
sonbreillua kastor fina,
txaketea pañu fina,
zapatakin kordonak
emaztegaiak egia.