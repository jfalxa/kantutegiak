---
id: ab-133
izenburua: Goiz-Oilandatto
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000133.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000133.MID
youtube: null
---

I
Goiz-oilandatto
ederen horrek,
barrnatik naizü hoskatü.
Arhantzeño bat
nahi nikezü
zure lurretan lanthatü.
- Zihaurenetan
lantha ezazü
arhantze-lürrak han tüzü.
II
Ene lürretan
barhe gorriek
lanthare oro tüzü gal.
Zihaur yin zite,
haitzur bat kartzu,
biok ditzagun hil ahal.
- Atharratzeko
zuinnahi harotzek
haitzurrak merrke liro sal.