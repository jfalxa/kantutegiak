---
id: ab-892
izenburua: Hirur Gizon Gaztek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000892.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000892.MID
youtube: null
---

Hirur gizon gaztek
zu nahiz emazte
elgarren artean
disputa omen dute.
Nahi badute izan bezate
neure perilik aiek eztute.
Eztut ezkondu nai
ez disputan sartu,
komentu batera
serora nioazu.