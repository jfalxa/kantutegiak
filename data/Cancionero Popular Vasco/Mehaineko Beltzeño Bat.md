---
id: ab-157
izenburua: Mehaineko Beltzeño Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000157.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000157.MID
youtube: null
---

Mehaineko beltxeño bat omen da penetan
antxuño bat baduela kolpatüa beretan.
Abisatüa izan banintz joan den denboretan,
etzen, ez orai izanen pena horietan;
beldur nai oraino trunpatu zaizkidan
ene erranak oro utsik joan daizkidan.

Ileori, begiargi, xarme garria,
gorputzpropi, taila lerden, pare gabea;
arrosa baten iduri duzu koloria,
elurra bezain zuri duzu zure larruya;
eznaiteke ausart zuri erraitera
einan jinen naizenez gortearen eitera.

Jiten ahal zire ausarki, neure maitea,
zurekin nahi dizut nik pasatu bizia.
Zato fite, presa zaite, zu nahi zaitut neure
neholere ni eznauke zureganat gabe.