---
id: ab-969
izenburua: Labiru Labiru
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000969.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000969.MID
youtube: null
---

Labiru labiru
bai ganadu zaiña,
bai ganadu zaiña;
Malkorreko ganauak
artoa guraiña,
artoa guraiña.