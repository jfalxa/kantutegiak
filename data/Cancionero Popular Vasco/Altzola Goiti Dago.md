---
id: ab-337
izenburua: Altzola Goiti Dago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000337.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000337.MID
youtube: null
---

Altzola goiti dago bai Mendaro baiño
mutila merkeago bai neskatxa baiño
mutila nai duenak beretzat erosi
Arrateko zelaian txanponean zortzi

Onak bal din badira eztira garesti,
txarrak baldin badira probatuta utzi
lau bost sei sardiña makalero
sei zazpi ezkabiltza gaizk.