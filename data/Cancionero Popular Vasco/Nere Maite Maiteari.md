---
id: ab-291
izenburua: Nere Maite Maiteari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000291.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000291.MID
youtube: null
---

Nere maite maiteari
uso ederrak ekarri,
birigarroak eta xoxoak
yan ditzan maite gozoak.

Nere maite maitearentzat
opila kut xan dadukat:
erdi erdia emango dizut,
nere maiteño, zeretzat.