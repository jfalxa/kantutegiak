---
id: ab-1148
izenburua: Oies Errondan Dabil (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001148.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001148.MID
youtube: null
---

Oies, sarri sarri errondan ibili zaitekez.
Oiesek emoniko
bosteko gorria
andreak kaixan dauko
gona beilegi.