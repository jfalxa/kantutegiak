---
id: ab-1162
izenburua: San Pedro, Zeru Altuko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001162.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001162.MID
youtube: null
---

San Pedro, zeru altuko
giltzaren yaubea:
zeurea txalopea,
neurea sarea.
Gora, gora Pedro Juan.