---
id: ab-857
izenburua: Adios, Aita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000857.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000857.MID
youtube: null
---

Adios aita, beraz Amari goraintzi,
Montevideora noa bihar edo etzi,
hango bizimodua nahi dut ikusi,
noiz turnaturen naizen Jainkoak badaki.

Ontzian sartu nintzan zintzurretik minez,
zer bait jan nahi eta deusik ezin irets,
halaxeto egon niz han zen bait egunez,
orhoitzen nintzelarik neure Aita ta Amez.