---
id: ab-770
izenburua: Anton Lumera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000770.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000770.MID
youtube: null
---

Anton Lumera gaixoa,
bota poliki pausua.
Anton Lumera gurea,
urrea baiño obea.
Urrea gorietan da,
baiña ez Anton Lumera gurea.
Txuxta purruxta txuxta puxta.
Txuxta purruxta txurruxta txuxta.