---
id: ab-101
izenburua: Aritz Adarrean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000101.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000101.MID
youtube: null
---

Aritz adarrrean belatxinga dantzan,
esku zuri ederrean lorea.
Bart amarretan eraman dute
gure auzoy onetan andrea.
¿Zein izan ote da nor izan ote da
eraman duen morroskoa?
Bai izan ote da ez izan ote da
gure aldameneko oinordekoa?

Ederr da amilotxak uda len goizean
txioka ta adarrik adarr astea.
Txori dantzariok buru beltsak dira,
ezkonberri hura buru txuria.
¿Nundiko, noiztiko,zezazkoy ote du
buruan zeukan estalkia?
Paristik senarrak atzo ekarririko
arreun ta perlazkoa guzia.