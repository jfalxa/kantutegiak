---
id: ab-546
izenburua: Anaiai Gaztigatu Nion
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000546.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000546.MID
youtube: null
---

Anaiai gaztigatu nion
isilik karta batean
ea lagunduko zidanez
gizona ortzitzen lurrean,
ea lagunduko zidanez
gizona ortzitzen lurrean.

Anaiai gaztigatu nion
isilik karta batean
ea gizonik galdu zenez
o Erregeren gortean,
ea gizonik galdu zenez (1)
o Erregeren gortean.

- Bai galgu (2) da don Juan de Flores
yoan dan zazpi urtean;
zu bezalako dama batek
Idukiko du etxean.

- Nere aitak ¡ai! balaki
nik zer daukaten etxean,
aitak ni ilen ninduke (3)
eta amak egosi bertzean.

(1) El original decía ea falta zen gizonik bai ala ez.
(2) El original: bai falta da don Juan de Flores.
(3) Este tercer verso consta así en el original, sin alargarlo como en
los tercero de las estrofas
precedentes.