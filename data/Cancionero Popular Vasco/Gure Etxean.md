---
id: ab-222
izenburua: Gure Etxean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000222.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000222.MID
youtube: null
---

Gure etxean lau ardi,
lauak zortzi belarri:
sekularen enaiz izandu
oraintxe bezain egarri.
Urra labiru labiru lena
oraintxe bezain egarri.

Gure etxean ardoa saltzen,
ezpaitakit zeinbana.
Zeinbana dagoan yakiteko
edan dezagun pinta bana.
Urra labiru labirulena
edan dezagun pinta bana.

Nere alaba Mari Martin,
ganbaran norbait bazerabil.
Atso gaizto idurikorra:
ganbaran inor eztabil.
Urra labiru labiru lena
ganbaran inor eztabil.