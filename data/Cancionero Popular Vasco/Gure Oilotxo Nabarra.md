---
id: ab-1111
izenburua: Gure Oilotxo Nabarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001111.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001111.MID
youtube: null
---

Gure oilotxo nabarra,
kurkurubil eta xabala,
oilarra etxean denean
zertan goaten da landara.
Ai oi egia, as tan dringulun Maria.