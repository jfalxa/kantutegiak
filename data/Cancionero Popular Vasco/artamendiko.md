---
id: ab-624
izenburua: Artamendiko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000624.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000624.MID
youtube: null
---

Artamendiko lili, oa ezkontzera,
nerabe lagunez eli etorri gera;
ororen lera erakustera,
ez gero damuz soegin nes katx artera.