---
id: ab-304
izenburua: A Mutil Kopetillun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000304.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000304.MID
youtube: null
---

A mutil kopetillun,
begiek lo gure;
A mutil kopetillun,
begiek lo gure:
baietz uzte duk bainan,
baietz uzte duk bainan
eznauk ire gure,
eznauk ire gure,
eznauk ire gure.
Larala larala larala larala
larala larala larala la

Oraiko mutil zarrek
¿zer dute merezi?
Oraiko mutil zarrek
zer dute merezi?
Errekan beratuta
errekan beratuta
lexiban egosi,
lexiban egosi
lexiban egosi.
Larala larala larala larala
larala larala larala la

Oraiko mutil zarrek
duten fantasie,
Oraiko mutil zarrek
duten fantasie;
soinean gerrikoa
soinean gerrikoa
bestek erosie,
bestek erosie,
bestek erosie.
Larala larala larala larala
larala larala larala l.