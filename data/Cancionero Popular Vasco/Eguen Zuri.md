---
id: ab-1213
izenburua: Eguen Zuri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001213.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001213.MID
youtube: null
---

Eguen zuri eguen baltz
txakurra arrautza ganeandatz,
baltz balztxoa maisuentzat,
zuri zuritxoa geuretzat.