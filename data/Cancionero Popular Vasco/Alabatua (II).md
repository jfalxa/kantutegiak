---
id: ab-1192
izenburua: Alabatua (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001192.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001192.MID
youtube: null
---

Alabatua izan dedila
sakramantu santua.
Ama Birjina, zure erraietan
izan zan kontzebitua.
Belerengo portalean
estrabi pobre batean
isiak arnasaz berotzen zaubien
jakindurik hura nor zan.
¡Ama Birjina, orio pobreza!
Izanik zeruko Erregina,
lasto gainean duzu Semea,
otzez ikaraz betea.