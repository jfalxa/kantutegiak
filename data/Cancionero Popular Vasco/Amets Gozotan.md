---
id: ab-96
izenburua: Amets Gozotan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000096.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000096.MID
youtube: null
---

Nik zu maite, zuk ni ez:
ene maitea ¿zeren ez?
Nik zu maite, zuk ni ez:
ene maitea ¿zeren ez?
so egidazu bi begiez
eta maita bihotzez.