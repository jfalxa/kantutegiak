---
id: ab-6005
izenburua: Nahi Duzue Yakin
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006005.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006005.MID
youtube: null
---

¿Nahi duzue yakin
nere gutizia?
Yainko onarentzat da
nere min guzia.
Ene baitan nago
pentsatzen noiz
ikusiko dudan ene Yainkoa,
ezen bertzela biziz
ah! hiltzera noa.