---
id: ab-153
izenburua: Lili Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000153.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000153.MID
youtube: null
---

l.- Lili bat ikhusi dut baratze batean,
desiratzen bai nuke neure sahetsean,
lorea eztu galtzen udan ez neguan:
haren kiderik ezta bertze bat munduan.

- Deliberatu nuen gau batez joaitera
lili arraro haren eskura hartzera;
ezpainuen pentsatzen guardiatzen zutela:
gau hartan uste nuen han galtzen nintzela.

- Abiso emaiten dut orai munduari
eta partikularzki jende gazteari:
gabaz ibiltzen dena eztela zuhurregi,
ni eskapatu bainaiz, eskerrak Jinkoari.