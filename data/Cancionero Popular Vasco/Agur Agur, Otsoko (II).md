---
id: ab-1072
izenburua: Agur Agur, Otsoko (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001072.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001072.MID
youtube: null
---

Agur agur otsoko
ongi etorri astako.
Egarri, bazaz egarri,
badakat nik ardoa franko
Egarri, bazaz egarri,
badakat nik ardoa franko.

Orra or goion artalde,
txakurrik bere eztauke.
Gaur or gora yoan gura badok,
txikiro parea baeuke;
Gaur or gora yoan gura badok,
txikiro parea baeuke.