---
id: ab-883
izenburua: Eztira Koplak Jartzen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000883.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000883.MID
youtube: null
---

Eztira koplak jartzen gabaz ametsetan,
artzaiak ondo bizi dira (y) Amezketan.
Eulia bezalaxe dabiltza esnetan,
argatik eztaukate kostarrik ezertan.

Zuk esan omendezu lagunen artean
eztezula senarra mariñela bear.
Zuk pentsatu aldezu daudela "de balde":
Zarauzko mariñelik ezta zure zale.

Alferrik izango da neskatxa galanta,
zer dan jakiña dago aurzaien soldata.
Allegaten danean urtearen buelta,
gona bat egin eta txanbrarentzat falta.

Uri eder andiko (1) alaba zera zu,
Zarautz markatutzea ondo egin dezu;
zure erriko fama baldin nai badezu,
eulientzat osto bat artu bear dezu.

(1) Ziudare andiko dijo la kantora.