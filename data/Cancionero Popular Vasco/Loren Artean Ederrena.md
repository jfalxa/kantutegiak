---
id: ab-1039
izenburua: Loren Artean Ederrena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001039.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001039.MID
youtube: null
---

Loren artean ederrena da
lirio Maiatzekoa,
alan bere ta ederragoa
San Juan Bagilekoa,
da San Juan da San Juan
beti zaukadaz gogoan.