---
id: ab-757
izenburua: Zozoak Eperrak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000757.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000757.MID
youtube: null
---

Zozoak eperrak orain
nai dizkizut ekarri:
besteari bana ta bina,
Josetxori amabi.