---
id: ab-1090
izenburua: Baditu Zazpi Urthe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001090.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001090.MID
youtube: null
---

Baditu zazpi urthe
uraren gainean girela,
sei urtheren bitalla
baizik gurekin eman dugula,
zazpigarrenean yan ditugu
gatua eta xakhurra.

Ene mutilik ttipiena,
ene mutilik maitena,
habil hara ta habil hunat,
mastaño horren gainera,
mastaño horren gainerat
eta ageri denez leihorra.