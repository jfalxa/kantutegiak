---
id: ab-723
izenburua: Buhameak Badakite
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000723.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000723.MID
youtube: null
---

Buhameak badakite
zaldunaren egiten,
baie taere ango haurrek
gatelu txarrean edaten.
Trunlapeiro trunlapeiro
trunlapeiro peiro peiro
gatelu txarrean edaten.