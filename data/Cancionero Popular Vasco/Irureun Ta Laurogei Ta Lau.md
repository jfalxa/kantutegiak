---
id: ab-893
izenburua: Irureun Ta Laurogei Ta Lau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000893.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000893.MID
youtube: null
---

Irureun ta laurogei ta lau urte badira
Birjina zerutikan gugana etorri da.
Leku basamortura Aloñako mendira
toki au eskojitzeaz ikaratzen dira,
au pentsa dezakezu ala konbenida,
au pentsa dezakezu ala konbenida.