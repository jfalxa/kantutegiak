---
id: ab-1011
izenburua: Errosarioko Larrosa Eder
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001011.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001011.MID
youtube: null
---

Errosarioko larrosa eder,
Birjina Ama bakarra:
bost misterio gozoz beteak
gatoz gaur kontempletara.