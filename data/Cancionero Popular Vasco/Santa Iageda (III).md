---
id: ab-1258
izenburua: Santa Iageda (III)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001258.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001258.MID
youtube: null
---

Etxe onetako etxeko andrea giltza (y) aundien yabea,
giltza (y) aundiak artu (y) ezkero eguzu limosnea.

Limosnea ona da baya borondatea obea.
Yaun zerukoak paga dezala berorren borondatea.

Urresila da bera yarritzen, urrespiluan miratzen
txokolatea artu (y) artean ezta (y) andikan yakitzen.

Idia edo idisko, urdai-puzkea lodisko;
urdai-puzkarik ezpaldin bada, iru lukainka "lo mismo".

Ezkilak errepikatzen, yendea zer dan galdetzen,
etxe ontako etxeko andrea kotxean da elizaratzen.

Or goian dago urrutietan, goien goiengo leioetan;
zazpi zezenak yoka dezala Anjelesen eztaietan.