---
id: ab-190
izenburua: Andre Bat Ikusi Det
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000190.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000190.MID
youtube: null
---

Andre bat ikusi det biruntan tan tan biruntena,
andre bat ikusi det yoan den astean,
yoan den astean, yoan den astean,
gizonaren galtzakin biruntan tan tan biruntena,
gizonaren galtzakin taberna batean.

Bertan edan dezadan biruntan tan tan biruntena,
bertan edan dezadan pinta bat ekartzu,
pinta bat ekartzu, pinta bat ekartzu.
Onoko galtza auek biruntan tan tan biruntena,
onoko galtza auek baituran artzatzu.