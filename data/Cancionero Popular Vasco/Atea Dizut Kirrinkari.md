---
id: ab-106
izenburua: Atea Dizut Kirrinkari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000106.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000106.MID
youtube: null
---

Atea dizut kirrinkari
eta potzoa xanpari.
Atea eskuz urinda
eta eman potzoari ogi,
eman potzoari ogi.

Gure potzoak eztizu nahi
bertzek eman ez ogirik.
Usoak dire landetan
eta eperrak mendi gainetan,
eperrak mendi gainetan.