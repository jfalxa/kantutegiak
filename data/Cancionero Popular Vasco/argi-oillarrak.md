---
id: ab-950
izenburua: Argi-Oillarrak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000950.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000950.MID
youtube: null
---

Argi-oillarrak yo due,
eguna sentitu due.

Mitxelenera bide bat
Juandortzenera beste bat.

Oien bien bitartean
sortu da grazia onenbat.

Graziak ondoan zillara,
punten amoradu-belarra.

Pañuelua gorrie,
ziriko oriz yosie.

Juan Bautista yaunak poltsetan dauke,
Joakina bereak emanik,
zapatatxoak berriek ebilez urreztatuek.

Joakina orrek anketan dauzka
Juan Bautista bereak emanik...

Joakina putzura erori, orduan etzan egarri.
Juan Bautisten orduko laxterra
"Joakina nerea ¿non zera.