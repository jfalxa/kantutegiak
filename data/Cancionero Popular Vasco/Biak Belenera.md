---
id: ab-1203
izenburua: Biak Belenera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001203.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001203.MID
youtube: null
---

Biak Belenera daude kaminadu
an eldu zaitezanean (sic) ostatuaren itandu
Jesucristo ay qué bonito Jesucristo.

Arrano galant bat zerutikan lurrera
sujetadu izan zeruko Erregea. Jesucristo ...etc.

Ameak artu eban semea besoetan
semea ta jaungoikoa zituan adoretan. Jesucristo...etc.

Linboak egozan deiez bost mila urtean:
Jauna, yatsi zakiguz zeruetarean. Jesucristo...etc.