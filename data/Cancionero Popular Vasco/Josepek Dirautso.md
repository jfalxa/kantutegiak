---
id: ab-1234
izenburua: Josepek Dirautso
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001234.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001234.MID
youtube: null
---

Josepek dirautso
Maria Birjineari
noz deklaratuko daben
esperantza geureari
Jesu Kristo ¡Ay que bonito
el Niño Jesucristo.