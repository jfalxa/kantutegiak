---
id: ab-1269
izenburua: Ur Goiena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001269.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001269.MID
youtube: null
---

Ur goiena ur barrena
urte berri egun ona;
etxe onetan sar dedila
pakearekin ondasuna,
pakearekin ondasuna,
onarekin osasuna,
gure baratzean belar ona:
Jainkoak digula egun ona.