---
id: ab-384
izenburua: Mutil Txaleko-Gorri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000384.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000384.MID
youtube: null
---

Mutil txaleko-gorri ¿zegan dok fanderu ori?
Amar ogerleko t'erdi karu dot fanderu ori.
Zeuk esan emen dozu eztala badala,
kondearen alaba monja sartudala;
kondearen alaba pipita kolore
ez emen dau balio kuarta bat labore.