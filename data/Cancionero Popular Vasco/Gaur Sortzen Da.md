---
id: ab-1014
izenburua: Gaur Sortzen Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001014.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001014.MID
youtube: null
---

Gaur sortzen da
Jesus Aurra,
oro giten alegera,
goatzin adoratzera.