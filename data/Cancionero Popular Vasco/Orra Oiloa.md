---
id: ab-1152
izenburua: Orra Oiloa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001152.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001152.MID
youtube: null
---

Orra oiloa mahain gainean
arrautzea zen amaren pean,
gero txitoa sortu zenean
arrautzerik ez mundu astean.
Lehen oiloak errun artean
¿nork du lehena eman munduan?

Mendi gaineko Piarres ttiki
lotsarik gabe zitzaion yaiki:
isilik zaite or, ez mintza gaizki,
yakintsun zira, bainan ez aski,
arras txorotzat gauzkizu naski;
tontoak ere zerbait badaki.