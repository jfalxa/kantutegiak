---
id: ab-1273
izenburua: Urte Barri Barri (II)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001273.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001273.MID
youtube: null
---

Urte barri barri
txarri belarri,
daukanak eztaukanari
emon gabonkari.
Nik eztaukat eta
zu amon neuri;
ezpanazu emoten,
beti kaka larri.
Atsoak dauko
iletea sarri
Agureak dauko
ardaua pozgarri.