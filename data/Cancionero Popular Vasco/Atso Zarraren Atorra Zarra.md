---
id: ab-201
izenburua: Atso Zarraren Atorra Zarra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000201.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000201.MID
youtube: null
---

Atso zarraren atorra zarra;
berea balu, ezer ez;
kedarra baino beltzagoa du
garrbitutzeko alperrez;
kedarra baino beltzagoa du
garbitutzeko alperrez
Txirurin brin kulin, brin kulin, txirurin brin kulin atsoa
Su bazterrean dagoenean,
eztauka mozkorra gaiztoa.
Su bazterrean dagoenean
eztauka mozkorra gaiztoa.
Atso zarra belendrin
ire azkenak egin din,
Gure atsoak gau ta egun
eztu besterik zer egin,
ortzak ere yoan zaizkin eta
deabruzarra dirudin,
orrtzak ere joan zaizkin eta
deabru zarra dirudin.
arkukusoak arrapatuta
atze aldeari atz egin
arrkukusoak arrapatuta
atze aldeari atz egi.