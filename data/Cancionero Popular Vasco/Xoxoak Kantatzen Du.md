---
id: ab-935
izenburua: Xoxoak Kantatzen Du
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000935.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000935.MID
youtube: null
---

Xoxoak kantatzen du
gaueko amarretan
guardia jarri dute
kale kantoietan:
satreak erosi nau
hamalau pesetan
ezetz erantzun nion
enabil klopetan.