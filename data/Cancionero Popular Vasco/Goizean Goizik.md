---
id: ab-571
izenburua: Goizean Goizik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000571.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000571.MID
youtube: null
---

- Goizean goizik yeiki ninduzun
emazte nintzen goizean,
bai eta ere sedaz beztitu
iguzkia ateratzean.

- Etxeko andere aundi ninduzun
eguerdiaren gainean
bai eta ere alhargun gazte
iguzkiaren sartzean.