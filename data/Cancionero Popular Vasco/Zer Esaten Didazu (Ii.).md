---
id: ab-1064
izenburua: Zer Esaten Didazu (Ii.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001064.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001064.MID
youtube: null
---

¿Zer esaten didazu, Madalenea?
Resuzitatu dela berorren semea
¡Ai au gozoa! Ai au gozoa!
Pazko eguna da ta gloriosoa.

Aingeru orendako zeruan leioak,
gu ere emen gaude arako yaioak
geuk au gogoz nai baldin badegu
esperantza aundian bizi bear degu.

Zeruko jardinaren lore eder-artean
antxe eseriko gera egunen batean
¡Ai au gozoa, ai au gozoa!
Pazko-eguna da ta gloriosoa.