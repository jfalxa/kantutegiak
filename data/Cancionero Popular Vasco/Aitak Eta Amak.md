---
id: ab-537
izenburua: Aitak Eta Amak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000537.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000537.MID
youtube: null
---

Aitak eta Amak erritik,
maitea, ni zugatik.
Donibanera bidali naute Arbonatik,
ez izaiteko zurekila arremanik,
arremanik.