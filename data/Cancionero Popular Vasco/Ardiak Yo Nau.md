---
id: ab-772
izenburua: Ardiak Yo Nau
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000772.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000772.MID
youtube: null
---

Ardiak yo nau ostiko
¿ez ote naiz biziko?
Baiñañ arraiña;
zarra da eta ilbedi
duran dirandon,
ekarriko diñat gaztea
durandirandon.