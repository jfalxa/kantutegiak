---
id: ab-874
izenburua: Bertso Bi Paratzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000874.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000874.MID
youtube: null
---

Bertso bi paratzeko nago ni zalantzan,
espiritu purua saiatu laguntzan.
Gure fabore dabiltz gau ta egun arrantzan,
argatikan egiak bear ditut esan
san Migel aingeruaren ondran ta alabantzan.