---
id: ab-218
izenburua: Gizon Pipartzaleak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000218.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000218.MID
youtube: null
---

Gizon pipartzaleak
selebreak dire
selebreak dire
tabernan alkarekin
opatzen badire.
Alkarri belarr eske
maiz egoten dire
maiz egoten dire
edan eta pipatu
alditik aldire
alditik aldire
farra egin lezake
oyei begire.
Basoa eskuan ta
aren zain yendea
aren zain yendea
lurrera dariola
agoko lerdea.
Lena berriz esaten
patxadan ordea
patxadan ordea
begiak zabal zabal
argal ikustea
argal ikustea
naskagarria da txit
olako urdea.