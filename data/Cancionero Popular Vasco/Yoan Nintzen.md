---
id: ab-736
izenburua: Yoan Nintzen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000736.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000736.MID
youtube: null
---

Yoan nintzen yoan nintzan montera
monte zela mendia,
ikusi nuen ikusi nuen
pajaro zela txoria,
tiratu nion tiratu nion
piedra zela arria,
atera nion atera nion
odol sangre gorria.
Pinpin txoria txori papo gorria,
yan yan koñata, txori saltsa on bat da.