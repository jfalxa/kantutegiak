---
id: ab-748
izenburua: San Juan, San Juan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000748.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000748.MID
youtube: null
---

San Juan San Juan,
nik eztaukat besterik gogoan,
arrautza bi kolkoan,
beste bi altzoan,
artoak eta gariak gorde gorde,
lapurrak eta sorginak erre erre.