---
id: ab-165
izenburua: Neure Maiteak
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000165.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000165.MID
youtube: null
---

l.- Neure maiteak igorri
isil misilka mila goraintzi.
Kusterat yinen dela laburzki,
bizi nadin alegeraki;
gorputzez urrun izanagatik,
bihotzez dela beti enekin.

- Orai elkitzen niz herritik,
arrdura dut nigarra begitik.
Maite bat maitatzeraz geroztik
bihotzaren erdi erditik,
horr utzi beharr baitut laburzki
¿ ¡ai! nola bizi izanen naiz ni?.