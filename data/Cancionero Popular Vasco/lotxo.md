---
id: ab-286
izenburua: Lotxo
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000286.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000286.MID
youtube: null
---

Lotxo engañadore
falso traidorea
letxok engañako dau
bai gure ume.