---
id: ab-919
izenburua: Motrikun Dan Ederrena
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000919.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000919.MID
youtube: null
---

Motrikun dan ederrena
Maria Prantziska,
begiak beltxak eta
arpegi polita.

¿Zer esperantza dezu
horrela ibilita?
Sosegatuko dezu
mariñelan pleita.

Erromerian giñan
San Isidroz Olatzen
bere Aitakin batean
meriendatutzen.