---
id: ab-303
izenburua: Urundiko Uso
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000303.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000303.MID
youtube: null
---

Urundiko uso oien
urrumadaz gozaro
nere altzoño berotan
egik, maite, lo ta lo:
bonbolon bon,
bonbolon bon, lo, lo

Pago tantai zut zut ori
oian-usoz bete da,
bat eztakust ¡¡au zoria!!
aurra bezen ederra:
bonbolon bon,
bonbolon bon, lo, l.