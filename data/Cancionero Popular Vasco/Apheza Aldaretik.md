---
id: ab-992
izenburua: Apheza Aldaretik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000992.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000992.MID
youtube: null
---

Apheza aldaretik orai jausten,
debozionea du finitzen,
bere populua du komendatzen
bere Yainkoari, itzuli gaiten;
bere populua du komendatzen
bere Yainkoari, itzuli gaiten.

Barkha jauna, barkha, barkhamendu,
gure uts guziez dolu dugu,
emendik aitzina zureak gituzu
othoi ar gizazu, plazer baduzu;
emendik aitzina zureak gituzu
othoi argizazu, plazer baduzu.