---
id: ab-949
izenburua: Alegera Gaitean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000949.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000949.MID
youtube: null
---

Alegera gaitean onela,
ezin bagera bestela
ermakaritxuak yatera
goazen seintxua ikustera.