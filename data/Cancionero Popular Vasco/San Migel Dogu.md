---
id: ab-1057
izenburua: San Migel Dogu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001057.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001057.MID
youtube: null
---

San Migel dogu geure patroia,
yarraitu ondo berari,
zapalduteko zintzo burua
infernuko dragoiari,
mundu onetan egin eztaigun
sekula gatxik inori.
Izanik aldi atan
zeruko jentea,
alde bat guztiz ona
bestea deungea,
esetsi gogor batek
birrindurik Luzifer,
geratu zan kapitan
zeruan San Migel.