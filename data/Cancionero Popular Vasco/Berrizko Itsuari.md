---
id: ab-346
izenburua: Berrizko Itsuari
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000346.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000346.MID
youtube: null
---

l.- Berrizko itsuari
Patxiko deritxo,
alabea daukanak
ari emon leio.

- Alabea daukanak
emon dautsalako
Berrizko itsu orrek
atsotxu bat dauko.