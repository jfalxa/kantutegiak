---
id: ab-93
izenburua: Aldixe Batez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000093.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000093.MID
youtube: null
---

I
Aldixe batez nindagoelarik
ene etsarian birjosten
Aiton-seme bat auteman nizun
itsasaldetik kantatzen eta
eresi ederren emaiten.
II
¿Zer nahi düzü, ereslari zaldun
ene etsarian zük egün?
- Othoi, andere, nahi nikezü
zurekilan mintzo-lagün,
zurekilan mintzo-lagün izan,
birjosten ere ikhastün.
III
- Aitonsemeak gezi ta azkonen
izan ohi zire beharrez;
nik eztakizüt geziz birjosten,
lan dagit soilik orratzez;
zilharr-arizko eskü-lantoak,
orraxko luze mehailez.
IV
- Guduz gaintiko eskü-lanetan
ene erhiak tüzü bigun;
itsasegiko txürülaketan
hau izan düzü ezagün.
Zilegi bezait zilhar-bihotz bat-
en izaitea birjostun.
V
- Zilhar-bihotzak nihaurek ere
badakizkizüt egiten.
Zilhar-urheak ene gogo hau
eztite zoriz bethetzen.
Beste galthapen gabe bazira
zoazkit, zaldun, lehen bait lehen.
VI
- Sorr bite zapi hortan bihotz bat,
nihauren hau biz eredu;
üdako ekhia bezen sutsu da
ta eztiaren goxoa du.
Nik besteño bat hor birrjosteko,
zure bihotza indazü.