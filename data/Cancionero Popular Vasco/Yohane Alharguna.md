---
id: ab-636
izenburua: Yohane Alharguna
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000636.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000636.MID
youtube: null
---

Johane alharguna
¿zertarako dük laguna?
Ehortzen urgazteko
sakristaua dük ona.

Hüna gira hürrunti
Paris gaineko aldeti,
Perrotinaren uhuratzera
ahal bezain uneski.