---
id: ab-1004
izenburua: Denbora Da Mintzatzeko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001004.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001004.MID
youtube: null
---

Denbora da mintzatzeko
pasione santüaz
eta nigar egiteko
Jesukristen hiltzeaz,
haren erremestatzeko
egin daikün graziaz.

Sujet hunez mintzatzeak
bihotza deit trenkatzen,
zu ikusiz baratzean
odol hutsez izertzen,
nuiz ere erran beitzunean
tristeziaz isiltzen.