---
id: ab-704
izenburua: Ama Girgir
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000704.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000704.MID
youtube: null
---

bertsioa
Ama girgir ¿nora yendeak? atozte atozte,
zugi txar onetan pasa zaizte.
Aurrak aurrak ¿nongoak zerate?
Erragearen portalekoak gerate.