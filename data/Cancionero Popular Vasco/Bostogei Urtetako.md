---
id: ab-561
izenburua: Bostogei Urtetako
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000561.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000561.MID
youtube: null
---

Bost ogei urtetako dontze llatxo bat
oiean daukat gaixorik.
Oilo bat ere il diot bainan
eztauka obekuntze rik.
Beste bat ere ilen nioke
yeikiezpaledi oietik.

¡Ai au bizitze larri erdiragarri
zer ikusteko otenago!
Sudurra ere zinzilizka det
kokotza baino berago.
Nere etxean naiko lukete
len bai len ila banengo.