---
id: ab-634
izenburua: Izar Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000634.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000634.MID
youtube: null
---

Izar ederra, txarmagarria,
zu zerade bai nere maitea:
ez egon triste, ez artu sustoz,
biziko gera munduan gustoz
palazio bat eginen degu
zebadaz edo lastoz,
bidean ere sei eskalera,
gaztaiña ostoz goiko sala
eta antxen biok biziko gera
plazer dezun bezala,
leioak ere moda berrira
begiratzeko egun argiz
bidrierak ere egiten ari dira
armiarmakariz,
ikusgarria izanen da auxe,
bazter guziak euliz beteak
ezta izanen ituxurarik
euria danean baizik.