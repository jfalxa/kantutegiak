---
id: ab-947
izenburua: Aldapeko Maria
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000947.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000947.MID
youtube: null
---

Aldapeko Maria
sein eginik dago,
aretxen bisitara
yoateko nago.
Imilaun bi gaztaiña
txorta bi berakatz,
hamazortzi matasa,
bederatzi ardatz.