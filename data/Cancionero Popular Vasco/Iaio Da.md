---
id: ab-1228
izenburua: Iaio Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001228.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001228.MID
youtube: null
---

Yaio da yaio da
Jaungoikoaren Semea,
yaio da geure pozgarria.
Askatxo pobre batean
lastoen ganean
portale triste baten
Belengo urian.
Yaio da, yaio da
Jaungoikoaren semea,
yaio da geure pozgarria.