---
id: ab-356
izenburua: Erramu Igandean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000356.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000356.MID
youtube: null
---

Erramu igandean, urrengoa Pasko,
Erramu igandean, urrengoa Pasko;
dantzatu geralako eskerrikan asko,
dantzatu geralako eskerrik an asko.
La la ra la la la ra la la ra la ra la ra
la la ra la la la ra la la ra la ra la la

Burua goien goien, badauke estadu:
Bunua goien goien, badauke estadu:
gabe geldituko da bilatzen ezpadu,
gabe geldituko da bilatzen ezpadu.

Bilatzen ezpadu ta gelditu donadu;
Bilatzen ezpaduta gelditu donadu,
libre dago an mutilak umo re (y)onadu,
libre dago an mutilak umore (y)on adu.