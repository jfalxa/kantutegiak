---
id: ab-801
izenburua: Gure Etxean Lakarrez
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000801.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000801.MID
youtube: null
---

Gure etxean asarrez
zenbait aldiz bakarrez;
auzoko gizon txur orrek baino
gehiago gastatuko ezpaientz
aisa beteko ituke poltsak
urrez eta zilarrez.
Sujetak bertsoa bear du
naiz irabazi ta naiz galdu.
Auzoko gizon txur orrek ere
bentaja aundiren bat aldu;
nundik deabru gastatukodu
irabaziko ezpalu.