---
id: ab-716
izenburua: Haur Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000716.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000716.MID
youtube: null
---

Haur ederra ¿nunko nunko zirade.
Erregeren serbutxüko girade
Errege yauna hila dela dioie
Ezta hila, bizi dela dioie.
Zubu hori falso dela dioie
Ezta falso ez eta ere traidore.