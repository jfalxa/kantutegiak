---
id: ab-1149
izenburua: Ondarribia, Erri Txikia
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001149.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001149.MID
youtube: null
---

Ondarribia erri txikia
erdian plaza zabala,
alargun batek or emendauzka
ederrak iru alaba.
Zon zon zon, Isabela, Manuela,
Margarita zon eta zon eta zun.
Peru larga ta jun (bis).

Kortesiakin eskatu nion
bat eman bear zidala.
Errespuesta aditu nuan
"mutil pobrea nintzala".
Zon zon zon, etc.

Mutil pobrea banintzan ere
zu okin baten alaba,
ogi berotan jango genduan
zurekin neure kaudala.
Zon zon zon, etc.