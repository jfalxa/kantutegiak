---
id: ab-621
izenburua: Alaitu Gitean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000621.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000621.MID
youtube: null
---

- Alaitu gitean elgarrekilan,
ez gogo ilun yar, maiteak sekulan.
Izanen dugu emazte gazte edo zahar,
nahi badugu gartsuki andrea har:
bakhoitzak daki zer dian behar,
bakhoitzak daki zer dian behar.

- Itzali gitean elgarrekilan,
ez gogo argiz bizi izan sekulan,
eztugu izanen senergai gazte ez zar batño,
atzerrira doaz mutilak goizoro,
sain du yaunzle izain gire oro,
saindu yaunzle izain gire oro.