---
id: ab-156
izenburua: Martxoaren Leneko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000156.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000156.MID
youtube: null
---

Martxoaren leneko igande gabean
ames pat egin nue oilarraz batean:
ustez ote nindagon maitearen aldean;
iletzarri ta gero bakarrik goatzean.