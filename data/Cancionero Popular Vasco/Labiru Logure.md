---
id: ab-963
izenburua: Labiru Logure
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000963.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000963.MID
youtube: null
---

Lbiru logure bai
ganadu zaina,
or goiko landetan
zozoa kantetan
Arrako mutiltxoak
beti barriketan.