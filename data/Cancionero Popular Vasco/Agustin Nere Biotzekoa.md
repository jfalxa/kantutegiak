---
id: ab-91
izenburua: Agustin Nere Biotzekoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000091.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000091.MID
youtube: null
---

Agustin nere biotzekoa,
iztxo bat esango dizut:
beti horrela ibiliz ere
neke aundiak dituzu.
Horrela ibiltzea baino
ezkondu obeko dezu.