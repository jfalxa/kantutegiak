---
id: ab-567
izenburua: Gazte Nintzalarikan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000567.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000567.MID
youtube: null
---

Gazte nintzalarikan,
denpora batean,
enin tzan des kantsa tzen
ezkondu artean;
ezkondu eta gero
urte bete gabe,
porrusalda artzen nuen
oliorikan gabe.