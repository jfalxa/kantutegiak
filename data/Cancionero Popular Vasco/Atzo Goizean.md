---
id: ab-258
izenburua: Atzo Goizean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000258.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000258.MID
youtube: null
---

Atzo goizean goiz yagi nintzan,
birigarroa goizago;
birigarroan txinta soinuan (1)
lo eiten dogu gozaro.

Eperrak kantatzen dau
goizean bostetan:
ez egon, neskatilak,
mutilen pentzutan.

(1) Kanta, dijo la cantora, en vez del roncalés txinta.