---
id: ab-1151
izenburua: Ontzi Bat Egin Da
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001151.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001151.MID
youtube: null
---

Ontzi bat egin da Parkean
Bayonaz bertze aldean.
Partitu behar gira flota batean,
berrehun uraren gainenan,
primederaren aldean.

Adios Aita, adios ama,
Adios ene maitena.
Ontzi baten gidari,
garlaren buruzari,
ezin eman diogu ihesari:
hala baikira urrikalgarri.