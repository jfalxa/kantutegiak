---
id: ab-363
izenburua: Gizonak Eztau Bear
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000363.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000363.MID
youtube: null
---

l.- Gizonak eztau be ar ezkondua damu,
Jaungoikoak emonik andra on bat badu.
Jaungoikoak emonik alan dau kat euki
andra ona daukana eztot nik erruki.
Ai ai trula lai ta trula trula rai ta rai.
Ai ai trula lai ta rai ta rai ta rai.

- Gizonak gura badau bere deskantsua,
andrea artu begi zarra ta antzua.
Andrea artuten badau gazte umatsua
¿zelan bilatuko dau arentzat pentsua?
Ai ai trula lai ta trula trula rai ta rai.
Ai ai trula lai ta rai ta rai ta rai.