---
id: ab-978
izenburua: Agur Dolorezko Ama
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000978.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000978.MID
youtube: null
---

Agur dolorezko Ama larritua,
erregina laztan errukiz urtua:
artu gauzuz, Ama, semenekatuok.

Zeure sabeleko seme dan frutuak
artu izan deuskuz geure pekatuak:
artu gaizuz, Ama, seme nekatuok.

Zeure zazpi ezpata arma zorrotuak
belan apurtzeko deabru galduak:
artu gaizuz, Ama, seme nekatuok.