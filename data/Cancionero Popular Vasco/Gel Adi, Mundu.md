---
id: ab-568
izenburua: Gel Adi, Mundu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000568.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000568.MID
youtube: null
---

Gel adi, mundu; gel adi, lagun;
nai ezpadozu yarraitu, larga zaidazu,
neure moduan dagidan Jauna serbitu.
Bere lagunak dirautse orain etzaitez yoan gugandik.
guk bere salbau uste doguta
¿zegaitik zoaz emendik.