---
id: ab-1232
izenburua: Josepe Gizon Ona
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001232.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001232.MID
youtube: null
---

Josepe gizon ona,
arotza zera zu,
aurtxo txiki onentzat
seaska egin zazu.
Perla ta perla negarrez dago
aurtxo ederra gugatik.
Goazen bada, goazen guztiok
konsolatzera bertatik.