---
id: ab-771
izenburua: Ara Nora Joan Ziran
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000771.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000771.MID
youtube: null
---

Ara nora joan ziran hamalau nobio,
zortzi mediku eta lau botikario.

Zazpi kintalekoak zortzi armario
gizon batek a pultso bizkarrera igo.

Lau arrautza zituen, zazpi talo yaki,
txantxangorri batenak sei libra tripaki.

Baiak irina igo, errotak eraulki,
gainibetak erre ta labeak ebaki.

Itsasoan ari dira arbolak aldatzen,
itsuak ikusi du etxea erretzen.

Gizon bat ikusi det irureun urtetan
marinel dabillela ontziko soketan.

Zapatorde txalupak zeduzkan oinetan
aren aitona berriz itsasoa urketan.

Mutua berriz ari da jendeari deitzen,
ankamotzak badaki bidea korritzen.