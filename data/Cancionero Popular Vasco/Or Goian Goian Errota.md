---
id: ab-1246
izenburua: Or Goian Goian Errota
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001246.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001246.MID
youtube: null
---

- Or goian goian errota,
garia dakar iota
etxe ontako etxekoandrea
Ama Birjina debota.

Or goian goian iru fago,
erdikoa lerdenago,
etxe ontako nagusi jauna
guziak baino ederrago.

Or goian goian lainoa,
lain azpian otsoa,
arkakusoak itoko aldu
etxe ontako otsoa.