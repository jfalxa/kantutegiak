---
id: ab-1238
izenburua: Izarrak Irtetzen Du
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001238.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001238.MID
youtube: null
---

Izarrak irtetzen du
odeiaren erditik
baita semeak ere
Amaren sabeletik (bis).
Artzaintxuak alboka,
gure monjak arpak,
zelebratu dezagun
gaurko gabon gaba.
Goazen alkarrekin,
nerau aurrena,
artzainak lagun artuta
goazen Belena.