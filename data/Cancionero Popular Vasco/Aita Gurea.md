---
id: ab-986
izenburua: Aita Gurea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000986.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000986.MID
youtube: null
---

Aita gurea zeruetan zaudena
santifikatua bedi zure izena
betor gugana zure erreiñua
egin bedi zure borondatea
nola zeruan ala lurrean.
Eman iguzu gure eguneroko ogia
barkaizkitzu geren zorrak
guk geren zordunai
barkatzen diegun bezela
ezkaitzatzula utzi tentazioan erortzen
baizikan libratu gaitzazi Amen Jesus.