---
id: ab-1250
izenburua: Ortixik Gora
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001250.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001250.MID
youtube: null
---

Ortixik gora ninoiala
ospitaletxu zar baten
aita San Jose topatu neban
astotxu zar bat txalmatzen;
dabai Belenen da bai Belenen
jaio da Jesus Nazaren.

Belengo urian,
gabon gabaren erdian,
Ama Maria topatu neban
konsolaziño andian;
dabai Belenen da bai Belenen
jaio da Jesus Nazaren.