---
id: ab-355
izenburua: Eder Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000355.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000355.MID
youtube: null
---

Eder ederra baiño
obozu komuna,
ederrak kendu leizu
buruko zentzuna.
Trai larai liraliralirala.
Trai larai liraliraliralai.

Sarritan egoten da
zozoa kantetan,
niri bere etxat faltatu
gogoa orretan.
Trai larai liraliralirala.
Trai larai liraliraliralai.