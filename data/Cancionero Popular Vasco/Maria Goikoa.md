---
id: ab-745
izenburua: Maria Goikoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000745.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000745.MID
youtube: null
---

Maria goikoa.
¿Zer dezu, bekoa?
Atoz iturrira.
Baina ezin etorri.
¿Zer dezu ba?
Senarra etorri
¿Zer ekarri?
Zapata galtzerdi
¿Ze kolore?
Txuria ta gorria ta kirriki
txuria ta gorria ta kirriki.