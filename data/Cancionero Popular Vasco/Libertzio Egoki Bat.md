---
id: ab-909
izenburua: Libertzio Egoki Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000909.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000909.MID
youtube: null
---

Libertzio egoki bat
oraingo aldian
paratzeko asmoa det
dadukan neurrian
zer funtzio pasatu dan
Ayako errian
esplikatzera noa ni
euskera garbian.