---
id: ab-934
izenburua: Sujet Berri Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000934.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000934.MID
youtube: null
---

Sujet berri bat gertatu izan da
Ahierre etxe garaian.
Kauserak egin izan dituzte
isilik segeretuan,
isilik segeretuan
eta hirur ahizpen artean.

Aita merkatu batera gan zen
nagusi gaztearekilan,
Ama zaharra ezin higituz
ohean goiz ta arratsean:
haurriderikan gazten gaztena
hari zen latsan urrean.