---
id: ab-6001
izenburua: Agur Itziarko (Variante)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/006001.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/006001.MID
youtube: null
---

Agur Itziarko Birjna ederra,
agur Ama maite itsasoko izarra.