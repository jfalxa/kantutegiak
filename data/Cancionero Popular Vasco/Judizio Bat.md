---
id: ab-894
izenburua: Judizio Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000894.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000894.MID
youtube: null
---

Judizio bat unibertsala
orain dezadan kontatu;
Bere grazia Jaunak didala
lenik nai diot eskatu:
konbeni bada deklaratzea,
ezpeza y arren faltatu.