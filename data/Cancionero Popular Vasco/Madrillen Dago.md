---
id: ab-1040
izenburua: Madrillen Dago
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001040.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001040.MID
youtube: null
---

Madrillen dago bataiatua
doai txit aundiz (1) yantzia
ta San Andresko parrokian du
zeruetako erentzia.
Egunik egun bete arteraiño
ontasunezko iturria,
apal-apalik aingerutxoak
bezain arima txuria.

Isidro orrek Moisesen gisa
jo du akulluz arria
esanez "Jaunak nai zuenean
emen bazan iduria"
esan da egin an agertu zan
ujaltegi izugarria,
orduan ari ta orain askori
iltzen dio egarria:
Madrillen dagon printzipalena
uraxen dago jarria.