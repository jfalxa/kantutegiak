---
id: ab-1010
izenburua: Eriotza On Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001010.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001010.MID
youtube: null
---

Eriotza on bat, Jauna, eman begi
bere eriotza onagatik
Maria graziaren Amak
miserikordiaz beterik,
geren azkeneko ordu artan
libra gaitzala etsaiagandik.
Amen Jesus.