---
id: ab-987
izenburua: Ama Birjina Erkudengoa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000987.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000987.MID
youtube: null
---

Ama Birjina Erkudengoa
elurretako zuria,
esku batean iguzkia
ta beste eskuan euria:
ark guretak gorderik dauka
egoki zaigun guztia.

Igurai on bat zeukagularik (?)
ermita txiki artatik
odeiak eta tempestadeak
bialtzen gaitu (?) bertatik
eta frutuak oso ta leial
gordetzen ditu gugatik.