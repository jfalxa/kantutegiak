---
id: ab-1099
izenburua: Errak Bat
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001099.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001099.MID
youtube: null
---

Esaik bat
gure jauna bera dok bat,
berak salbuako gaizoak.
Esaik bi.
Erromako altarak bi,
gure Jauna bera dok bat,
berak salbako gaizoak.
Esaik amairu.
Oilarrak yoten dau munduan,
aingeru ederrak zeruan,
neure arimea emoten deutsat
Birjineari altzoan.