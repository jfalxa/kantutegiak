---
id: ab-619
izenburua: Ai Neure Baltzerana
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000619.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000619.MID
youtube: null
---

- Ai neure baltzerana, alperrik galdua,
mandazaina zenduan zuk lagun artua.
Itxarongo neuskizu pare bat urtean
gazte zoroa nai ta galdu zenaitean.
Txio txio txoria,
poztu daiten abia.

- Alargun naizalako, eztegu gauean,
arranoska mutilik ager zedaitean,
kale mustur bietan suak eingo doguz
donianeko suak, otz danaberotuz.
Txio txio txoria,
poztu daiten abia.