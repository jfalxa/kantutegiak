---
id: ab-265
izenburua: Aztanen Portalean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000265.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000265.MID
youtube: null
---

Aztanen portalean
arbola eder bi,
bata da laranjea,
bestea madari.
Larrosatxuak bost orri daukaz,
kabelineak amabi,
gure umea gura dabenak
eskau bekio Amari.
Txikitxua naizela
nago neu astuna,
ezalnazu botako
kar kaban barruna.