---
id: ab-1237
izenburua: Iruñ-Erantzun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001237.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001237.MID
youtube: null
---

Iruñ-erantzun erri ona da
are (y) obea Oiartzun.
Nere lagunak Dios te salbe
garboarekin erantzun.