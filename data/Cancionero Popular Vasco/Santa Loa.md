---
id: ab-294
izenburua: Santa Loa
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000294.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000294.MID
youtube: null
---

Santa Loa Santa Loa,
gure aurari emazu loa:
gaubean gau guzikoa,
egunez ordubikoa.