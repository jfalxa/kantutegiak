---
id: ab-1035
izenburua: Kreatura Damnatua
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001035.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001035.MID
youtube: null
---

Kreatura damnatua:
zerk hauen tormentatu,
zer den hire infernua,
hire penak zer diren erraguk,
argitu nahi gaituk.