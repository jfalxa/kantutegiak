---
id: ab-622
izenburua: Aldude Eta Banka
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000622.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000622.MID
youtube: null
---

Aldude eta Banka,
urbilena Baigorri,
arte ortan badira
zen bait miga ernari.
Libre dagon gaixoak
aisa egiten du irri,
guardia eman diozola
ai hurran aldi horri.