---
id: ab-906
izenburua: Kontrbandoan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000906.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000906.MID
youtube: null
---

Kontrabandoan asi ziraden
goizeko iruretako,
kontrabandoan asi ondoren
atalondora eskapo.

Atalondotik erraten zion
enauk bela arrapatuko,
sasiartean ezkutatu zen
etsaia etxera orduko.