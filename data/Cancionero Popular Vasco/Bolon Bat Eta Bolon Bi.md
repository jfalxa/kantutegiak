---
id: ab-270
izenburua: Bolon Bat Eta Bolon Bi
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000270.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000270.MID
youtube: null
---

Bonbollon bat eta bonbollon bi,
Bonbollon putzura erori;
erori bazen, erori,
bizkarra eta lepoa autsi.

Bonbolon bolon la pastora
que parió Nuestra Señora
corderita la presente
con el agua de la fuente.