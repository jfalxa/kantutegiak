---
id: ab-569
izenburua: Girgilluek Anketan
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000569.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000569.MID
youtube: null
---

Girgilluek anketan lepoa zepoan,
umil umile antxe paratu ninduan;
beste anparorikan ezneu ken on doan,
ila iduri bainan bizirik nengoan.