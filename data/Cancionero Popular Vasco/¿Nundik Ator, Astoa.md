---
id: ab-1141
izenburua: ¿Nundik Ator, Astoa?
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001141.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001141.MID
youtube: null
---

¿Nundik ator, astoa?
Naparrotik, otsoa.
Iri ere emango diat
Napar ardotxo gozoa
tuaria ria ria rai rai rai
Napar ardotxo gozoa.
Enok bada ez egarri,
baia banok ni gosegi,
ire burubelarri orrekin
egin bear djoat apari
tuaria ria ria rai rai rai
egin bear djoat apari.
Otso zarraren ezia
basoan gaizki azia,
bihar ilena badjoat bere,
badjoat gaurko bizia
tuaria ria ria rai rai rai.