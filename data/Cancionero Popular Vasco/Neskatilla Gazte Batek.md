---
id: ab-921
izenburua: Neskatilla Gazte Batek
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000921.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000921.MID
youtube: null
---

Neskatilla gazte batek
asarazo nau ni,
atera diodazala
bertso bat edo bi.
Zerbait ostu eidabe
erlauntzen batetik:
aren pasadizua
esatera noa ni.