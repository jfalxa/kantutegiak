---
id: ab-871
izenburua: ¡Auxen Da Lana!
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000871.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000871.MID
youtube: null
---

¡Auxen da lana, emen dudana,
gizon artean sartuta!
Batzuek zer nai esaten dute
ardo ttanttoak poztuta.
¿Pellok bi nola jauntziko ditu,
bat besterikan eztu ta?

Ikusi ezik izango nuke
sinesten lan txit gogorra,
Pellok bezala gizon zur batek
erabiltzea bi otarra.
Edan zar eztuk ori egiten:
orren argia nik orra.