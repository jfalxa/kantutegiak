---
id: ab-844
izenburua: Txantxun Lariun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000844.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000844.MID
youtube: null
---

Txanxtunlariun Kaminok eztauka dirurik,
lau alabatxo ditu ezkondu gurarik;
boskarrena semea kapote zarrakin,
konejua diruri bere bizarrakin.