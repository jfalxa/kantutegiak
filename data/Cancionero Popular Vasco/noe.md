---
id: ab-236
izenburua: Noe
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000236.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000236.MID
youtube: null
---

Noe lege zarreko
gizon omentia,
zuk landatu zintuen
lehen mahastia;
aihen domutsu hura
¿nork eman zaitzun burura
lurrean ¡ai! landatzea?
Gizona ilun beltzean
kausitzen den tenorean
hark dauka pozez betea,
hark dauka pozez betea.