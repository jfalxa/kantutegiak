---
id: ab-840
izenburua: Ortik Kuku
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000840.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000840.MID
youtube: null
---

Ortik kuku,
emendik kuku,
orra gure kuku
ere ezkontzen jaku.