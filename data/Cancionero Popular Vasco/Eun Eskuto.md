---
id: ab-1100
izenburua: Eun Eskuto
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001100.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001100.MID
youtube: null
---

Eun eskuto ditut
pikope batean
ertze eun para bear tut
hekien gainean
Todo lo quiere
todo lo pierde
berreun direlakoan
oa berriz ere.

Nere eun eskuto
pikopekoak
orain agertu dizkit
Zeruko Yainkoak
Todo lo quiere
todo lo pierde
orain agertu dizkit
zeruko Yainkoak.