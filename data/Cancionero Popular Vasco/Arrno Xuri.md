---
id: ab-196
izenburua: Arrno Xuri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000196.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000196.MID
youtube: null
---

Arrno xuri, arrno gorri,
arno kolore edera,
mundu huntan den arrgalenari
hik emaiten dakok indarr.