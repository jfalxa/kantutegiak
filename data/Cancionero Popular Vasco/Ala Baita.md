---
id: ab-765
izenburua: Ala Baita
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000765.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000765.MID
youtube: null
---

Ala baita laket leku
Probintzia aldea,
bereziki Oyartzungo
erri noblea.
An pasatu dut iauteri
azken astea
ongi yan-edan
ongi liberti:
auxe da bizia.
Pena ere bada
obekiago atsain artzea.
Neri etzitzaidan urrikitu
ara yoatea.

Goizean goiz yaiki-orduko
prest txokolatea.
Gero kurri pasaitara
eguerdi-artean;
eguerdian zopa ona
oiloa gaineko.