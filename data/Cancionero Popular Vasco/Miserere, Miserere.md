---
id: ab-1127
izenburua: Miserere, Miserere
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001127.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001127.MID
youtube: null
---

Miserere, miserere,
Jauna fan da Salbadore;
Salbadore, Salbadore
ene anima ¡otoi! zure;
ene anima ¡otoi! zure
bai orai ta beti ere.

Ortzegun sandu arratsean
triste zindaude biotzean,
triste zindaude biotzean
apostoluen konpainian,
apostoluen konpainian eta
oin-esku ederren garbitzean;
oin-esku ederren garbitzean eta
oin-esku ederren txukatzean.

Jesus fan tzen baratzera
an odolez izertzera,
an odolez izertzera eta

sokaz zuten arrastatu,
artaz etziren kontentatu;
buruan zuten koronatu,
artaz etziren kontentatu;
oin-esku ederrak itzeztatu,
artaz etziren kontentatu,
biotza lantaztatu.