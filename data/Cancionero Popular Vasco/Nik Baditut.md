---
id: ab-645
izenburua: Nik Baditut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000645.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000645.MID
youtube: null
---

Nik baditut galtzetak ere
emaztegaiak emanak,
emaztegaiak emanak.
Gal tzerdiak txi lindro nak,
ebillak ere zidar finak
zapatetan kordonak
omaztegaiak emanak,
emaztegaiak emana.