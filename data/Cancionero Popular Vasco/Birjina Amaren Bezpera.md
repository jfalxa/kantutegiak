---
id: ab-875
izenburua: Birjina Amaren Bezpera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000875.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000875.MID
youtube: null
---

Birjina amaren bezpera da ta
goazen Arantzazura,
Santa Luziren begi ederrak
argi egiten digula.
Arantzazura bidea luze,
ara-orduko nekatu;
Birjina Ama ta bere semea
bidean ditut opatu.

Burubide on bat eman zidaten
Ama-semeon artean
ona ta apala izan nendila
munduan nintzan artean.
¡Ai artzaintxoa, ai artzaintxoa!
Nere esan bat egizu:
Ait'ori ere zurgina da ta
onera bidal ezazu.