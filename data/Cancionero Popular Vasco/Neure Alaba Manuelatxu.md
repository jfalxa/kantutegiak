---
id: ab-1133
izenburua: Neure Alaba Manuelatxu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001133.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001133.MID
youtube: null
---

Neure alaba Manuelatxu
nure esan bat eidazu
atostean dagon kantari orri
datorkidala esaiozu.

Bein banengoan kalean
arri otzaren ganean
dama polit bat etorri djatan
an zer egiten nengoan.

Aitak eta Amak biraldu naude
damatxu orii estera
datorkigula geurera
eta geurekin apaldutera.

Geurekin apaldu eta
gero kantuz irakastera.
Nire kantua gura dabena
betor neurekin bordera.