---
id: ab-643
izenburua: Nere Maitea
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000643.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000643.MID
youtube: null
---

Nere maitea, ez egon sustoz,
biziko gera munduan gustoz:
palazio bat eginen dugu
sekalez edo lastoz.
Balkona ere basalizarrez,
ala goizean enkargatu da
Bayonako Pierres.