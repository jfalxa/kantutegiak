---
id: ab-945
izenburua: Zurriola Aldetik
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000945.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000945.MID
youtube: null
---

Zurriola aldetik
autxi zuten bretxa
¡ai hura egun artako
ango destetxa!
Nork bere aldetikan
enkomenda beza
bitorioso atera
eztedin prentsesa.