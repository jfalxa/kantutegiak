---
id: ab-232
izenburua: Matsarrno Gozoaren
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000232.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000232.MID
youtube: null
---

¡Matsarrno gozoaren urin sutsu dena!
ark irriberatzen daut eni biotza.
¿Oteda mundu onetan zarrik naiz gazterik,
ark bezala ene biotza irrikorrtzen duenik?
Orainik ere baneuka tanto bat artarik,
irribera nin dai ke ezbai gaberik.