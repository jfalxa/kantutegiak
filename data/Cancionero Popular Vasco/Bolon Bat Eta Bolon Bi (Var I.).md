---
id: ab-267
izenburua: Bolon Bat Eta Bolon Bi (Var I.)
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000267.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000267.MID
youtube: null
---

Bolon bat eta bolon bi,
Bolon putzurat erori;
erori ba zen erori,
etzen ya goi koi ketorri.