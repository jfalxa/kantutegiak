---
id: ab-389
izenburua: Oiek, Mutiko Txiki Politok
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000389.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000389.MID
youtube: null
---

Oiek, mutiko txiki politok
¿nun egingo dute igari?
Ibai guziak legortu dira,
arrai gaixoak egarri.
Au da guzia danau egia,
konsolatzeko mundu guzia.