---
id: ab-406
izenburua: Txiriririri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000406.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000406.MID
youtube: null
---

Txiririririri geurea dok umea,
tanpa rran pan tan tan pa rran pan tan
geurea dok umea,
etxaukat saltzeko diruakaitik ume au.