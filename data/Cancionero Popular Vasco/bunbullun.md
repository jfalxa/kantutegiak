---
id: ab-725
izenburua: Bunbullun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000725.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000725.MID
youtube: null
---

Bunbullun bunbullunete
botillan aguardiente,
bunbullun bat eta bunbullun bi
bunbullun putzura erori;
edan zituen pitxar bi,
etzan orduan egarri Lo

Mendi aldera goazen,
goazen gaztaiña biltzen:
lakatz zorrotza badu,
zorrotzagoak gera gu;
zakurrik an ezpagendu,
kolkoa beteko degu Lo.