---
id: ab-763
izenburua: Aitak Eman Daut
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000763.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000763.MID
youtube: null
---

Aitak eman daut dotea,
neurea, neurea:
Urdeño bat bere umekin,
oilo koroka bere txitoekin,
tipula korda eiekin.

Otsoak yan daut urdea,
neurea, neurea:
Axeriak oilo koroka,
arratoin batek tipula korda
agur ¡ai! neure dotea,
agur ¡ai! neure dotea.