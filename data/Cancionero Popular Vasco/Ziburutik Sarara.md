---
id: ab-943
izenburua: Ziburutik Sarara
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000943.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000943.MID
youtube: null
---

Ziburutik Sarara izandu naiz gana
nere gazte denborako maite batengana,
arribatu gabe tanik aitu nuen fama
guarda gazte batekin zela itz emana.