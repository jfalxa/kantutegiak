---
id: ab-756
izenburua: Zirrin Zorron
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000756.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000756.MID
youtube: null
---

Zirrin zorron con el carro
¡pontxearen otza!
¡Endaiako abadearen
edateko poza.