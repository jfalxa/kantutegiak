---
id: ab-1105
izenburua: Goazen San Juanera
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001105.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001105.MID
youtube: null
---

Goazen San Juanera gaur arratsean,
etorriko gerala bihar goizean.

Goazen San Juanera berduratara
berduratara eta an egoitera.

Egungo egun onek San Juan dirudi,
ezta San Juan, baia ala lombra bedi.

San Juango iturriaren ondo-ondotik
zazpi iturri urre-kañoetatik.

Zortzigarrena ere metal zurietatik


Lenbiziko eskuak ta gero musua
¡San Juanen iturriko uraren freskua.