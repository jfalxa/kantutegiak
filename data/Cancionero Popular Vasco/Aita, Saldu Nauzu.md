---
id: ab-536
izenburua: Aita, Saldu Nauzu
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000536.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000536.MID
youtube: null
---

Aita, saldu nauzu
idi bat bezala,
bai eta destinatu
¡oi! Espainiala.
Ama bizi izan banu,
Aita, zu bezala,
eninduzun ez salduko
saldu nauzun bezala.