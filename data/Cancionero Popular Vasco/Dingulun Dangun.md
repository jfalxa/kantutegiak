---
id: ab-275
izenburua: Dingulun Dangun
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000275.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000275.MID
youtube: null
---

Dingulun dan gun petrala
goazen Alira yaita ra.
Yoan den urtean goseak eta
aurten ¿arera zertara? Lo lo.

Binbilin bolon gure aurr au
balio ditu beste lau,
balio ditu beste lau eta
jitokumeak amalau.Lo lo.