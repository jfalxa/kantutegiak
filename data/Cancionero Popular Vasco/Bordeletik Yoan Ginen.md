---
id: ab-876
izenburua: Bordeletik Yoan Ginen
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000876.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000876.MID
youtube: null
---

Bordeletik yoan ginen
uraren gainean
Sain Paul izena zuen
ontzi ederrean.
Erten kapitaina zen
gurekin batean
Calcutan arribatu
osasun onean.