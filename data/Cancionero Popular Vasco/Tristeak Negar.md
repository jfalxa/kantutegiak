---
id: ab-1171
izenburua: Tristeak Negar
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001171.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001171.MID
youtube: null
---

Tristeak negar egiten zuen
zori gaiztozko munduan,
etxeagatik, etxekoengatik,
galdu zuela zerua;
baino ez oraindik, baino ez oraindik,
esperantza dut Birjinearengandik.
Semeak Amari begiratuta,
Am'ori berriz triste zen
berealaxe gelditu izan zan,
berriz penatu etzezan.
¡Ai hura umea, ai hura umea,
Birjina Amaren semea.