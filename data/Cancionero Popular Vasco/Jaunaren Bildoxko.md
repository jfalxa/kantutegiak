---
id: ab-280
izenburua: Jaunaren Bildoxko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000280.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000280.MID
youtube: null
---

ADAPTACIÓN del folklorista:

Jaunaren Bildoxko, lo gozo batek
len bai len ar zaikez Bildoxko ori
ots zegion seaskaratuz
Amak Jesus aurrari:
Oba, oba.

Arrosak lau zeru goi goienetik
kabelinarik bat yetxi zaigu:
aingeruak dira arrosa oiek,
kabelina da Jesus:
Oba, oba.