---
id: ab-620
izenburua: Aizkora Itxean
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000620.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000620.MID
youtube: null
---

- Aizkora itxean ta kertena basoan,
Aizkora itxean ta kertena basoan,
eztauka zu arturik senarra besoan,
eztauka zu arturik senarra besoan.

- Itxeak zein on duen gainean tellatu,
itxeak zein on duen gainean tellatu,
errian bear ttugu senarrak billatu,
errian bear ttugu senarrak billatu.

- ¿Oraiko mutil zarrak zertako dirade?
Sos bat izan-orduko tabernan dirade.

- Oraiko mutil zarrak apo-zangoekin
ezkonduko lirake balute norekin.