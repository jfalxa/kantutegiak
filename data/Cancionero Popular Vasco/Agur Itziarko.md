---
id: ab-979
izenburua: Agur Itziarko
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000979.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000979.MID
youtube: null
---

Agur Itziarko Birjna ederra,
agur Ama maite itsasoko izarra.

Gabriel Santuak zizun abisatu
zeugan nai zebala Jaunak enkarnatu.

Evaren izena mudatu zenduan,
zeugatik bakea artu izan genduan.

Errezibi gaitzazu zeruko kortean
erregu egiten degunak lurrean.

Kautiboak askatu, itsuak argitu,
onak eman eta gaitzak kendu itzazu.

Kendu izkutzu arren gure miseriak
baita bakeatu (sic) gerra gaitz guztiak.

Penaz dagoena zugana dijoa,
biraldu zaiozu etsegin gozoa.