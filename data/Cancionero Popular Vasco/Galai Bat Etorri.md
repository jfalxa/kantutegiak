---
id: ab-127
izenburua: Galai Bat Etorri
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000127.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000127.MID
youtube: null
---

Galai bat etorri zan larri eta estu
josten ginan artean egiteko leku
¡Aura Anton kaiku!
Txartes baten neurria bear zuala artu
¡Aura Anton kaiku.