---
id: ab-1032
izenburua: Kalea Gora
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001032.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001032.MID
youtube: null
---

Kalea gora bera dakust (1) nik semea
soinean deramala kurutze berea.
Kartzu gurutze ori, bai nere semea.
- Eztizut bada emanen ¡o Ama nerea!

Mendi-burura eldu naiz, nerekin semea;
an iltzatu nai dute ¡oi! Oben gabea.
Belengo aingeruak ¿orain non zerate?
Isiltasun otzean erantzun diote.