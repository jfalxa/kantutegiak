---
id: ab-118
izenburua: Dama Ederra
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000118.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000118.MID
youtube: null
---

l.Dama ederra: gorarik zaude etxean,
zure senarra aphalik dago Frantzian.
Hura hantik yin-artinoko aldian
ogipeko har nezazu etxean.

ll.¿Ene senarra aphalik deya Frantzian?
Ta hura hantik yina daiteken-artean
beskotari nihaur aski niz etxean:
hauzoak hurbil utzi derauzkit joaitean.