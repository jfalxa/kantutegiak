---
id: ab-1005
izenburua: Egun, Jauna
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001005.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001005.MID
youtube: null
---

Egun, Jauna, biziko naiz sainduki,
bekatua utzirikan osoki.
Belaurikaturik umilik biotza,
adoratzen natzaizu Jaungoiko bakoitza,
Jaun goiko bakoitza.