---
id: ab-726
izenburua: Dirurik Zuririk
kantutegia: Cancionero Popular Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/000726.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/000726.MID
youtube: null
---

Dirurik zuririk ez beltzikan
Joxantoniok etzeukan
zurrutño bat edateko ujolegun batean
errekara joan zan keskalutako ustean:
errota txikiak klin klan,
errota txikiak klin klan.

Errekan zegola keskalurik Joxantoniok alerik
ezin atxituzgogaitu zuen bizi bizirik
itzuli zan utsik loben ez ardit gaberik:
sabel jan gabeak klikklik,
sabel jan gabeak klik klik.