---
id: ab-3582
izenburua: Asko Jendek Ez Dute Uste
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003582.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003582.MID
youtube: null
---

Asko jendek ez dute uste
Zeruan badela loriarik:
Jaun Jinkoaz orhoitu gabe
Bethi bekhatuan bizi. (bis)
Halere galde dezogun miserikordia bethi miserikordia bethi
Hil eta juan gitzan berekin.

Azken jujamenduan,
Jauna jiten denean,
Ethorriko da aire ezti batean
Hedoi baten gainean: (bis)
Daunatuen urrikia
Zoinen handi den orduan!
Bere faltaz ifernuan.