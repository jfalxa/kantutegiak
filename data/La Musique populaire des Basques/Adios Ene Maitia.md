---
id: ab-3574
izenburua: Adios Ene Maitia
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003574.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003574.MID
youtube: null
---

Adios, ene maitia, adio sekulako! (bis)
Nik eztit beste phenarik, maitia, zouretako,
Zeren eizten zutudan hain libro bestentako.

Zertako erraiten duzu, adio sekulako? (bis)
Ouste duzia eztudala amorio zouretako?
Zuk nahi banaizu enukezu bestentako.