---
id: ab-3571
izenburua: Triste Niz Bihotzetik
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003571.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003571.MID
youtube: null
---

Triste niz bihotzetik,
Badakit zergatik:
Neure faltarik balin baliz,
Ez nuke phenarik:
Bainan bertzen faltaz hainitz pobre niz.
Jinko onaren esperantzan geldituren niz.