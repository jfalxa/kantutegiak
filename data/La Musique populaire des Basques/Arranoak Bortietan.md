---
id: ab-3566
izenburua: Arranoak Bortietan
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003566.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003566.MID
youtube: null
---

Arranoak bortietan gora dabiltza hegaletan;
Ni ere lehen andereki ebilten khanberetan;
Orai aldiz, ardura ardura nigarra dizut begietan

Hartzen dit hartzen ofizioa Iratiat ulhañ benoa;
Nola beitut bizioa oihanetan khantatzekoa,
Abis hounik emaitez eta egia erraitez banoa.

Ahaire hao zahar umen da; duda gabe, ene adin beita,
Hountan nahi dit khantatu eia zouin den gazte perfeita,
Gezur guti erraile beita, umil, eta serious feita.

Arrosatziak eijer lilia, zuhain berak du ilhorria;
Amorio traidoriak berarekila azotia.
Hala dio borogatiak, begira tronpa, ene aorhidiak!

Aitak diozu alhabari: "Noun abila, laidogarri?".
Alhabak bertan aitari gezur zounbait, abis hounen sari,
Orai bera dolugarri martir beitateke sarri.

Orai bera doluturik dago, bai bena berantu zaio;
Herritik joan nahiago, egoitia laido beitzaio;
Hil baledi aldiz orano hobian sarthu nahichago.

Hanbat duzu aflijiturik, libertatia galdurik,
Phena dolorez kargaturik, osagarriz gabeturik,
Horik oro berak erazirik, amorioak traditurik.

Lao ourthetako haor gachoa, leial duk hire amorioa:
Hitan dezagun har etsenplu houna, maitha protsimoa,
Izunkeriak eitzi, eta zerbutcha zeluko Jinkoa.

Khantore hoien egitian ulhañ nintzan ni bortietan;
Phasatzen peko oihanian abis hounik emaiten benian,
Deusen ere ezin sinhets eraziz, bankarot egin aiherrian.

Hartz handidat oihan hartan nihaori so jarri zeitan;
Hortzak chouri, lephoa lodi, begiak gorri beitzutian,
Khantoren egitia eitzirik, arra bankarot egin nian.