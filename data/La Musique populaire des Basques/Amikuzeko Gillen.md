---
id: ab-3587
izenburua: Amikuzeko Gillen
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003587.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003587.MID
youtube: null
---

Amikuzeko Gillen, khilo egileak
Etchetik khendu ditu bere langileak,
Sehi maiteak
Zeren galdu dituen bere irabaz bideak.

Ez dezake nihon khilo ardatzak
Nausitu baitzaio galtzerdi orratzak,
Moko zorrotzak;
Ez dira nekatu nahi oraiko neskatchak.

Elhe bilha dabiltza, galchoina eskuan;
Laneko gogorikan ez dute buruan,
Alfer moduan,
Oihal guti emanikan khutcharen chokhuan.

Gazte irule gutida Euskal Herrian
Nihor er'ezdug'ikhusten khiloa gerrian
Athe hegian;
Oi! zer prendak horiek saltzeko feiretan!

Oraino zonbeit bada, zaharño horietan,
Iruten ari denik berant arretsetan,
Bihotzminetan,
Ilobek ez lagunduz behar orduetan.

Askotan altchatzen da amasoren botza:
Bainan alabak ez du nahi hartu ontsa,
Lanerat lotsa,
Arrantze gabe nahi beiluke arrosa.

Amak goizean oihu: "Jeiki hadi, haurra;
Ezdea aski luze hiretako gaua?
Harzan haitzurra,
Itzuli beharra beita baratzeko lurra..

Alhabak arrapostu ohetik gustura:
"Bego bihar artino baratzeko lurra,
Arte laburra,
Zeren joan behar beiniz egun merkhatura..

Zuri galde egiten dut izeba tanta Maria,
Hea lakhet lekhu den gaztentzat hiria!
Hain da egia
Hara joaiteko badut hainitz gutizia.

Ez dira izertu nahi oraiko neskatchak:
Lur lanetako dira sobera beratzak,
O alfer hotchak!
Akabo eginen du laster laborantzak.

Neskatcha gazte direno, itchurak maithagarri;
Esposatu berrian senharrentzat umil,
Ondoan zarphil,
Galzetako zilhoak erdirat ezin bil.