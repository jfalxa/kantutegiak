---
id: ab-3570
izenburua: Alageraz
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003570.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003570.MID
youtube: null
---

Alageraz nik eztut khantatzen bena bai sofritzen
Bai eta komunikatzen
Etsaia espantatzen
Noulaz dudan kantatzen alagera gabe.
Alagera gabe,

Gai añhera abilou' etcherat
Eitz nezak lotarat
Gaiaren igaraitera
Hire plazer handia
Ene iratzartzia
Gaiaren minian.