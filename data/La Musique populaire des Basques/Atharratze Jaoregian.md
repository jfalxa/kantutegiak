---
id: ab-3579
izenburua: Atharratze Jaoregian
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003579.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003579.MID
youtube: null
---

(Ziberutarrez)
Atharratze jaoregian, bi zitrou doratu:
Hongriako erregek batto du galthatu.
Arrapostu ukhen du eztirela hountu.
Hountu direnian batto ukhenen du.

Atharratzen den hiria, hiri ordoki:
Hour handibat badizu alde bateti,
Erregeren bidia erd' erditi,
Maria Maidalena beste aldeti.

- "Aita saldu naizu idibat bezala;
Ama bizi ukhen banu zu bezala,
Ez nunduzun ez, joanen Ongrian behera
Bena bai ezkounturen Atharratze Salala.

"Ahizpa, joan zite portaliala,
Ingoiti horra duzu Ongriako erregia;
Erran ezozu, othoi, ni eri nizala,
Zazpi ourthe hontan ohian nizala..

- "Ahizpa, ez nukezu, ez, sinhetsia,
Zazpi ourthe hontan ohian zirela,
Ez eta ere hañ eri gaitz zirela
Bera nahi dukezu jin zu ziren lekhila.

"Ahizpa, jaonts ezazu arropa berdia,
Nik ere jaontsiren dit ene chouria.
Ingoiti horra duzu Ongriako erregia:
Botzik kita ezazu zoure aitaren etchia..

- "Aita, zu izan zira ene saltzale;
Anaie gehiena dihariren hartzale,
Anaie artekoa zamariz igaraile
Anaie tchipiena ene laguntzale.

"Aita, joanen gira oro algarreki;
Hounat jinen zira bihotzmin handireki,
Bihotza kargaturik, begiak boustirik,
Eta zour' alhaba hobian ehortzirik.

"Ahizpa, zoaz'orai salako leihora,
Ipharra ala hegoa denez jakitera:
Ipharra balinbada goraintzi Salari,
Ene khorpitzaren bilha jin dadila sarri..

Atharratzeko zeñiak berak arrapikatzen;
Hanko jente gazteriak belzez bestitzen.
Andere santaKlara hantik phartizen,
Haren zaldia urhez da zelatzen.