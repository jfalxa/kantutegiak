---
id: ab-3588
izenburua: Hauche Da Ikhasketako
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003588.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003588.MID
youtube: null
---

Hauche da ikhasketako mandoaren traza
Burua handi du eta itchura gaitza,
Ilia latza;
Basta peguzitikan zauria balsa;
Hauche da salsa!
Kristaurik ez diteke aldetik phasa. (bis)

Lephoa mehe eta burua ez ttipi,
Mathel hezurrak seko, begiak eri,
Oro beharri
Bi sudur zilhoetarik mukua dari,
Ezpainak larri:
Hortzik izan badu ere ez dik ageri. (bis)

Hauche da mandoaren urhats aphurra;
Lau zangoak tremel ete anka makhurra,
Juntetan ura;
Ezpata bezain zorrotz bizkar hezurra,
Ezduk gezurra;
Noiz larraturen zautan ni naiz beldurra. (bis.