---
id: ab-3578
izenburua: Errege Jan
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003578.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003578.MID
youtube: null
---

Errege Jan, zaoriturik:
Jin izan da armadetarik:
Am'anderia baratu ziozu
Etchian alagerarik.

"Errege Jan, kontsola zite,
Korajereki sar zite:
Zur'emaztiak errege tchipibat
Barda sorthu ukhen dizu..

- "Ez ene emaztia gatik,
Ez errege tchipibat gatik,
Ni enaiteke kontsola:
Haiek biek jakin gabe,
Ama, hiltzeko ohebat..

- "Am'anderia, zer die mithil hoiek,
Hainbeste nigar marrasketan?"
- "Ene alhaba, ezin begira,
Galdu diñe zaldi gris bat..

- "Am'anderia zer die neskato hoiek,
Hainbeste nigar marrasketan?
- "Ene alhaba, ezin begira,
Haotse diñe urh' ountzibat..

- "Ez zaldi gris baten gatik,
Ez urh' ountzi baten gatik,
Ez othoi egin nigarrik:
Errege Janek ekharriko dizu
Urhe eta zilhar armadetarik..

- "Ene ama, othoi, errazu,
Khantu hoiek zer diren hain gora."
- "Ene alhaba, deuserik ez,
Prosesionia dun joaiten..

- "Am'anderia, zer zaia behar dut jaontsi,
Ohe hontarik jalkhiteko?"
- "Ene alhaba, chouria, gorria,
Ederrena duken beltza..

- "Am'anderia, zer du lur saintu hounek,
Hain gora dagoenian?"
- "Ene alhaba, ezin begira,
Errege Jan dun ehortzirik!.

- "Am'anderia, oritzu giltz hoiek,
Urhe eta zilharren hoiek,
Eta errege tchipitto hori
Artha handieki eraik..

- "Lur saintia, erdir'ardi,
Ni barnen sar ahal nadin!...
Lur saintia erdiratu,
Eta nik errege Jan besarkatu!

"Lur saintia, zerr'adi,
Ni barnen bara ahal nadin!...
Lur saintia da zerratu,
Ni errege Janeki baratu.".