---
id: ab-3563
izenburua: Chorinoak Kaiolan
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003563.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003563.MID
youtube: null
---

Choriñoak kaiolan,
Tristerik du khantatzen:
Dialarik han zer han,
Zer edan, zer edan,
Kanpoa desiratzen (Bis)
Zeren, zeren, zeren,
Libertatia zouñen eder den!

Kanpoko choria,
So.giok kaiolari:
Ahal balin bahedi,
Harterik begir.adi,
Zeren, zeren,
Libertatia zouñen ederden!

Barda amets egin dit
Maitia ikhousirik:
Ikhous eta ezin mintza,
Ezta phena handia?
Ala ezina!
Desiratzen nuke hiltzia...