---
id: ab-3591
izenburua: Urzobat Jin Izanda
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003591.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003591.MID
youtube: null
---

Urzo bat jin izanda Katalouña aldetik
Pharagolan phaosatu da
Bere linjarekin.
Tra la la, etc
Bere linjarekin.

Urzoa nourat houa,
Hegalez aidian,
Aita ama zaharrak
Eitzirik habian?
Lumasutu nuk eta
Acholik eztiat.
Tra la la, etc.

Janak triste niz eta
Enaite kontsola;
Bi maite ukhen, eta
Otsoek jan bata:
Ezpenian gogatzen
Holako malurra.
Tra la la la, etc.