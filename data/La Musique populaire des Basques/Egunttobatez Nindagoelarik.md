---
id: ab-3585
izenburua: Egunttobatez Nindagoelarik
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003585.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003585.MID
youtube: null
---

Egunttobatez nindagoelarik maitenaren leihoan,
Erran ukhen niriozun houra niala gogoan,
Ene phena eta dolorez pietate har lezan.(bis)

Zoure phena eta dolorez pietate nik badit
Ene khorpitz tristiaz eztiot egin plazerik
Zeren promes egin beitut zeluko Jaonari.

Or'amorio, oro eijer zira zu, ene maitia.
Zoure eskutik nahi nikezu bizi nizano ogia
Eta ni izanen nuzu zoure zerbutcharia.

Enuzu ez ni hañ eijerra noula zuk beitiozu.
Mundu hountako eijerrena berthutia lukezu,
Hari ogenik egin gabe maithatu nahi banaizu.

- Baratzian zouñan eijer julufreia loratu!
Aspaldian nahi niana orai dizut gozatu:
Houra gozatu eztudano gaiik eztiot mankatu.

- Baratzian eijer deia julufreia loratu?
Aspaldi nahi zuniana orai duzu gozatu?
Houra gozatu duzunian berriak eman itzatzu.

Ene maite bihotz gogor, ezpiritu zorrotza
Orai ikousten dizut etzitzakedala goga.
Amorioa eitzi eta indarrez dezagun boroga.

Jaona othoi, pharka izazu, haor gaztebat ninuzu.
Zouri arrapostu emaitez debeiaturik nuzu;
Errandelako indartto hori bestetan bali'ezazu.