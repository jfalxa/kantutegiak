---
id: ab-3573
izenburua: Ala Baita Dolu Egingarri
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003573.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003573.MID
youtube: null
---

Ala baita dolu egingarri
Amoditan dena,
Bethi phenan baiteramatza
Gaua eta eguna.
Ez deia bada pena
Ene bihozean dena?
Nihork ezin dezake
adi ene hats bere pena
Maitebat ezin konbertituz
nik sofritzen dudana.

Charmagarri zu ere
sanjakorra zare;
Ene pena eta dolorez
ongi trufatzen zare.
Norbaitek deraizkitzu
beharriak ongi bethe,
Eta, dudarikan gabe, zu,
heien erranen sinhesle.
Gaizki mintzatu nahi dena
nork enphatcha lezake?

Izar charmagarria,
zu bazine neuria,
Zu zintazke bakharrik
neure kontsolagarria.
Zure begi eztia
bihotzean dut sarthuia
Eta amodiozko sokez
han ongi amarratuia.
Ez zinukeia bada izanen
nitaz pietatia?

Gizon gazte floria,
erraiten dautzut egia,
Eztudala hartu nahi
pietatezko bizia
Ez eta ere eman
elizako fedia,
Zeren zuk eman duzun
bertze norbeiti zuria.
Sinhets nezazu, ezin daiteke
biez baizik egin paria.