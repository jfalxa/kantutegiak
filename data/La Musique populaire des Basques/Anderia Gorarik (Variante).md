---
id: ab-6038
izenburua: Anderia Gorarik (Variante)
kantutegia: La Musique Populaire Des Basques
partitura: null
midi: null
youtube: null
---

Anderia gorarik zaude leiohan
Zure senharra behera dago Frantzian.
Hura handik jin daiteken artian,
Eskutari ni har nezazu etchian.