---
id: ab-3581
izenburua: Huna Bertsu Berriak
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003581.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003581.MID
youtube: null
---

Huna bertsu berriak nik orai paratu,
Hamar manamenduak nola goardatu:
Lehen eta bizikoa Jinkoa maithatu,
Lagun protsimoa're bethi estimatu.

Bigarren mandamendua juramendu guti,
Mihiari nahi duena ez erratera utzi.
Egiteko hortan ezda liferentzia ttipi:
Zerurat igaran edo ifernurat jautsi.

Mezabat osoa entzun igande egunean
Obligatuak gerare hirurgarrenean;
Obra hon hainitz dezagun egin azkenean,
Loria goza dezagun eternitatean.

Laugerren mandamendua bere burhasoak
Egin ahalaz laguntzen ez dutien gachoak.
Handiak izan dira hekien trabajuak
Halarik ere ezbaitira ongi pagatuak.

Bortzgarren manamendua nihor ez hilzea
Baita gauza lazgarria den heriotzea.
Aski da Jesus Jaunari arraparatzea
Gure gatik hartu duen harek gurutzea.

Garbitarzuna goardatu sei garren hortan,
Obraz, phentsamenduz eta solaz lizunetan.
Zer khondu garratzak mandamendu horrek derauzkan!
Tentazione gaichto frango bada mundu huntan.

Zazpigarrenak dakharke, deusik ez ebatsi
Nor berearekin eden, bai bertzena utzi.
Debruak erakasten du egiteko gaizki:
Hatzemaiteko sareak hedaturik dauzki.

Zortzi garren manuan hau egin behar dugu:
Jakilegoa izunik nihori ez altchatu.
Jakilegoa izunetan goratzen badugu,
Egun batez khondu garratza beharko dugu.

Bederatzigarrenean ezdago bertzerik
Ezdezagun nahi bertzen senhar emazterik.
Sekulan ez har hekien odolean pharterik
Arima gaichoak izan ezdezan kalterik.

Gure Jinkoak emanik hamar manamendu,
Horien begiratzeaz har dezagun khondu;
Obra hon egiteaz ez beldurrik hartu;
Maria sainduak gu lagunturen gaitu.

Mila zortzi ehun eta berrogoita hameka
Jesus Kristoren urtheak nonbeit hor dabiltza,
Manamendu horietan chuchen badabiltza
Salbaturen ginela eman zuen hitza.

Esteban dut izena, deitura Llanda.
Khantu ori eman ditut gogoan eta
Salbamenduaren ontzeko segida horida.
Ezdakitenik balinbada jakitea honda.