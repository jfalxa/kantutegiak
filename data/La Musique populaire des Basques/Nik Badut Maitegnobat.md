---
id: ab-3592
izenburua: Nik Badut Maitegnobat
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003592.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003592.MID
youtube: null
---

Nik badut maiteñobat, oi! bena nolako?
Ezda ttipiez handi, bai bien arteko;
Begia pollita du, dena amorio:
Bihotzian sarthu zaut, ezbaitzaut jalgiko.

Zuri nuzu hersatzen, arrosa ederra,
Phena gaitz hoietarik, nezazun athera;
Balin badut hortarik hiltzeko malurra,
Bazindukea bada bihotzian phena?

Amodioaren phena, oi! phena khiratsa!
Orai dut ezagutzen harek daukan phena.
Amodioa ezpaliz den bezain krudela,
Ez nezakezu erran maite zaitudala.

Munduan zenbat urhats oi! ezdut egiten!
Ez ahal dira oro alferrak izanen.
Jendek errana gatik guretako elhe,
Orotaz trufa neinte, zu bazindut neure.

Zeruan zenbat izar, maitea, ahal da?
Zure parerik ene begietan ezda.
Neke da phartitzia, maitia, enetzat:
Adio nik derautzut, denbora batentzat.

Zuri erranik ere, maitia, adio,
Ez nezazula, othoi, ni ukhan bastio:
Bainan bai bihotzetik izan amodio,
Etzaitut kitaturen thonban sar artio.