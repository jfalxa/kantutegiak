---
id: ab-6040
izenburua: Nik Badut Maitegno Bat (Variante 2)
kantutegia: La Musique Populaire Des Basques
partitura: null
midi: null
youtube: null
---

Chori erresiñula, hots, emak eneki,
Maitenaren borthala biak alkharreki;
Botz ezti batez izok deklara segretki
Haren adichkide bat badela hireki.