---
id: ab-3580
izenburua: Anderia Gorarik
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003580.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003580.MID
youtube: null
---

Anderia gorarik zaude leiohan
Zure senharra behera dago Frantzian.
Hura handik jin daiteken artian,
Eskutari nihar nezazu etchian.

Ene senharra behera deia Frantzian?
Hura hantik jin daiteken artian,
Beskotari nihaur aski niz etchian,
Hausoak hurbil utzi derauzkitan juaitian.

Izan niz Españian eta Frantzian,
Bai eta ere Anglaterra orotan;
Andere ederrik ikhusi dizit heietan;
Zure parerik ez zen ene begietan.

Muthil gaztia, ederki zira mintzatu,
Zure ganako ostatu gure etchian baduzu;
Ohia churi, ganbarak garbi diaudetzu;
Ni ere aldian hurbil izanen nuzu.

Zure senharra ni baino gizon hobia;
Zazpi urthez harek eman deraut ogia;
Zortzi garrenian Frantziarako bidia,
Eta harekin bere peko zaldia.

Uri haizia, ura duienian uherlo
Andretto hok girare guziak parlero;
Hitzño hori juan zerautazu lachero,
Ene senharra zutan truka ezniro.

Anderia hirain zautzu denbora
Eskeintu tuzu ihia eta ganbara
Eta haiekin zure gorkhutz propia,
Ezteia bada, errazu, hori egia?

Beskotari bilho urdin falsuia,
Hi othe hiz ene lehen semia?
Altchazadak eskerreko begia,
Ene semiak han dik sor seinalia.