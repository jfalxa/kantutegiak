---
id: ab-3590
izenburua: Urzo Chouria, Urzo Chouria
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003590.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003590.MID
youtube: null
---

Urzo chouria, urzo chouria,
Erran izadak othoi egia:
Nourat buruz houndouen bidajez,
Aldatu gabe pasajez.

Ene herritik, phartitu nintzan
Españalako deseñian;
Heltu nunduzun Ahansusera
Ene plazeren han galzera.