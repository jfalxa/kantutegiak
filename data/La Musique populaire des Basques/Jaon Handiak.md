---
id: ab-3577
izenburua: Jaon Handiak
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003577.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003577.MID
youtube: null
---

Jaon handiak ikhousazie
Jinko bat egun nigarrez;
Eta iziturik horrez,
A! ikhara zitie,
A! ikhara zitie.

Ororen buruzagi dugu:
Ainguriak, erregiak
Bai eta diren guziak
Beraren peko dutu. (berriz)

Debriaren pian beikinen,
Karitatiak goithurik,
Jin da, khupusten zelarik,
Libratzera guzien. (berriz)

Gibeltzen deiku ifernia,
Minak gutu destorbatzen;
Halaber gutu sendotzen
Den senthagallu hounak. (berriz)

Salbazale haor goure minaz
Nahi izan da kargatu?
Hountarzuna da gerthatu
Garhaitzen justiziaz. (berriz)

Nokugabe, gaichtoentako
Guzia sakrifikatzen;
Eta barrukian sortzen,
Gizounen salbatzeko. (berriz)

Barrukian da hedaturik:
Jinkoak zer khuña dian !
Gu umil izan hitian
Haor hola ikhousirik. (berriz.