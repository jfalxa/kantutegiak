---
id: ab-3586
izenburua: Urzo Luma Gris Gaicho
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003586.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003586.MID
youtube: null
---

Urzo luma gris gaichoa ore bidajian bahoa.
Haiduru duk mousde Sarhi, ihizlari zorrotz houra;
Begireik; ikhousten bahai, oi! Phetiriñalat bahoa.

Urzo gaichoak umilki
Erraiten mousde Sarriri:
Nahibada utsian den haren saretan erori
Eitz dezan igaraitera bere usatu bideti.

- Aoher duk, aoher, urzoa;
Juratu diat fedia
Aorthen behin jin behar hiz eneki Phetiriñala.
Han nik emanen dereiat arthoz eta ziz asia.

- Bai houn lukezu asia,
Denian libertatia.
Orhiko ezkurra zitazut janharibat hobia:
Angleser ihes joaiteko, eizten dut eizten Frantzia.

- Urzoa, ago ichilik,
Frantzian eztuk Anglesik.
Agaramountek Baiounan jinak oro hilen tik:
Eztuk Phetiriñalako zaragolla luze hetarik.

- Fida niz zoure erraner
Fidago ene hegaler:
Goraintzi erran behar ziezu, jiten badira, Angleser,
Halaber nik ere erranen diet Españoul papogorrier.

Mezuler naika ni eizten,
Ene sariak hol'esten?
Ountsa diat ora ikhousten nitzaz hizala trufatzen;
Ez enaik beste ourthe batez bortian hotzeraziren.

Urzoederra, airian
Arhin bahoa bortian
Jaon larru chouriak hiri mintzo tuk aoherrian;
Hire adin heñeko gutik lumak garbi hen kaiolan.