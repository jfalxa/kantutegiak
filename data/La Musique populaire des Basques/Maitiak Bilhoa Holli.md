---
id: ab-3589
izenburua: Maitiak Bilhoa Holli
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003589.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003589.MID
youtube: null
---

Maitiak bilhoa holli
Eta koloria gorri
Eskuko larria churi, zilhar fina uduri
Eta bera charmagarri best' ororen gañeti.

Etchettobat badizut nik
Jaoregi baten parerik
Hartan barnen egonen zira, zilhar kaideran jarririk
Ihourk ezpeiteizu erranen nahi eztuzun elherik.

Nik badutut mila ardi,
Bortian artzañeki;
Katalouñan ehun mando bere zilhar kargeki:
Hourak oro badutukezu, jiten bazira eneki.

Baduzia mila ardi
Bortian artzañeki?
Katalouñan ehun mando, zilhar diharureki?
Hourak oro ukhenik ere, eniz jinen zureki.

Maitenaren etchekoak
Khechu umen ziradeie:
Alhaba ene emaztetako sobera umen zaizie.
Ez emazte, bai amore: sofritu behar duke.

Senhartako emozie
Frantziako errege;
Frantzian ezpada ere, Españakoa bedere:
Bada errege, enperadore, phuntsela ja ez tuke.