---
id: ab-6041
izenburua: Nik Badut Maitegno Bat (Variante 3)
kantutegia: La Musique Populaire Des Basques
partitura: null
midi: null
youtube: null
---

Chori erresiñoula, hots, emak enekin,
Maitiaren borthala biak algarrekin;
Deklara izok gero botz ezti batekin,
Haren adichkide bat badela hirekin.