---
id: ab-3576
izenburua: Khanta Beza
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003576.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003576.MID
youtube: null
---

Khanta beza bitoria boztarioz mundiak.
Phorrokatu du buria heren suge handiak.
Gaitza zen haren aidia, bena goithu Mesiak;
Ikharazi ifernia Salbazale sorthiak.

Ulhunpeko zekuriak;
goza goza argia;
Hitzemanik zen Mesiak
ekharri du bakia;
Asmeziak bethe dira,
agertu da egia:
Gaiaz ekhiak argitu
Betlemen barrukia.

Bazakian Jinko Jaonak
goure behar handia,
Noula gu ezkintakian
gour' indarrez balia:
Halaz, arren, bethi danik
haitatu du Semia,
Sorthuren zena lurrian,
bekhatoren bermia.

Adam goure lehen aita
Satanek inganatu:
Egin zian bekhatia
berak ezin bardintu.
Laor mil' ourthez etzen ihour
pharadusian sarthu:
Azkenekoz Jesus haorrak
hanko borthak zabaltu.

Haor handibat da jaiotu,
alagera gitian.
Har'k eztu bere bardinik
zelian ez lurrian.
Agertu balitzeiku
Jinkoren urhatsian
Noul' etzaikeion eginen
batzarre houn lurrian.

Elaz! goure Jinko Jaona,
establia batetan
Behar zinena jaiotu,
animalen artian?
Guk merechi guniana
hola khuputs zentian?
Ala gizona betzaizu
hanitch khosta lurrian.

Mundian zen jaoregirik
haituz ederrenian
Zinatialarik sorthu,
printze kalitatian;
Borda tcharbat irekirik
haitatu'zu lurrian,
Kargaturik goure zorrez,
sorthu aberen artian.

Zelian zunialarik
hanitch khorte egile,
Aingururik ederrenak
dira berak jakile,
Guk merechi gabetarik
gison egin zirade;
Ginelarik bekhatore
hartu zoure aorhide.

Gour' arima pheretchatu
duzu gaiza bekhana:
Gora zian prezioa
hala noula erostuna.
Hao da hao amorioa,
nour eleite estona,
Jinkoa du haor' erazi
salba lezan gizona.

Zelietan ainguriak
urgulliak utsutu
Zeren nahi izan diren
Jinkoa uduritu;
Mundu hountan dabilana
nahi bada salbatu
Haien hutsari soginik
behar d'umiliatu.

Photeria da Aitari
bethireko bizian;
Semiari zuhurtzia
heben eta zelian;
Ezpiritia biekin
bat da Trinitatian;
Phitz dezala bere suia
guzien bihotzian.