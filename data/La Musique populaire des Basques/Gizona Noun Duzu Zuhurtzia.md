---
id: ab-3565
izenburua: Gizona Noun Duzu Zuhurtzia?
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003565.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003565.MID
youtube: null
---

Gizona noun duzu zuhurtzia?
Zer! eztakik
Eztela deusere bizia
Khebat baizik!
Hungut ezak mundu erhoa
Orai danik;
Ah! Eztuk hurrun herioa
Hire ganik.

Munduko plazer erhoetan
Habilana,
Jinko Jaona hire gogoan
Eztiana,
Ohart emak hil behardela
Eta bertan;
Oren bat segurrik estela
Mundu hontan.

Aberatsa, hi dihariak
Hai utsutzen,
Mundu hountak'utehur'izunak
Enganatzen,
Laster hire plazer maitiak
Tuk galduren,
Jinkoari hire khountiak
Tuk emanen.

Gaztia, eztuk zeren ari
Khorphitz horren
Manier jarraikitzen bethi,
Bai edertzen.
Herioa gaztetarzunaz
Duk trufatzen,
Gazteriari jatzartziaz
Duk gozatzen.

Tratulant irabaz gosia,
Deusek ere
Mundu hountan ezin asia
Behinere,
Sarri hozia duk ukhenen
Ostatutzat,
Diharu guziak eitziren
Besterentzat.

Laboraria, aoherretan
Hiz nekatzen:
Herioa ouste gabian
Zaik hullantzen;
Ereitzetan egiteko lanak
Tuk hiretzat,
Eta haien frutu ederrak
Primientzat.

Bat etzaio herioari
Ezkapiren,
Zorrozki zaio bakhoitzari
Jarraikiren:
Bilhaturen tu erregiak
Alkhietan,
Hala noula jente chehiak
Etcholetan.