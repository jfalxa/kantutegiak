---
id: ab-3568
izenburua: Argizagi Ederra
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003568.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003568.MID
youtube: null
---

Argizagi ederra, argi egidazu:
Oraino bide luzean joan beharra nuzu;
Gaunhuntan nahi nuke maitea kausitu.
Haren bortharaino argi egidazu,

Lotara ziradea, lozale pollita?
Lotara ez bazira so'gin dazu leihora,
Eta egiaz mintza, oi! izar ederra,
Zur'ama othe denez oraino lotara.

Etcheak eder du, bai, saihetsean labe;
Zer ala zu ez zauke goardiarik gabe?
Maitea, ni ez nauke egia erran gabe,
Noiztanka holakoak tronpatzen dirare.

Kanpotik sarthu, eta barnera ondoan,
Maitearekin nindagon, oi! gustu onean;
Amak, hautemanikan, oi, uste gabean
Gianetik jautsi zaukun kolera handitan.

Zu zinela ez nuen gogoan phasatzen,
Niri heben berean afrontu egiten;
Jenden erranez ez niz ez orai estonatzen,
Etsnplu dudanean nihaurek ikhusten.

Orai banohako, adios erranik;
Berriz jiteko ere ez esparantzarik.
Kitatzen ez banuzu arras bihotzetik,
Zure ganako bidea hautseko dut nik.

Jendek erraiten dute hal'ezdena frango,
Izar charmagarria, zur'et'enetako,
Gu ez girela gisan elgarrekilako:
Bi hok akort bagire, nori zer dohako.