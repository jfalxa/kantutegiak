---
id: ab-3603
izenburua: Guernikako Arbola
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003603.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003603.MID
youtube: null
---

Guernikako arbola da bedeinkatuba,
Eskualdunen artean guztiz maitatuba. (bis)
Eman ta zabal zazu munduban frutuba
Adoratzen zaitugu arbola santuba. (bis)

Mila urthe inguru da
Ezaten dutela
Jainkoak jarri zubela
Guernikako arbola:
Zaude bada zutikan
Orain da denbora.
Eroritzen bazera
Arras galdu gera.

Etzera eroriko
Arbola maitia
Baldin portatzen bada
Bizkaiko juntuba
Haurok artuko degu
Zurekin partia
Pakian bizi dedin
Euskaldun jendia.

Betiko bizi dedin
Jaunari eskatzeko
Jarri gaitezen danak
Laster belauniko:
Eta bihotzetikan
Eskatuez gero,
Arbola biziko da
Orain eta gero.

Arbola botatzia
Dutela pentsatu
Euskal herri guztian
Denak badakigu:
Ea bada jendia
Denbora orain degu
Hori gabetanik
Iruki biazu.

Beti egongo zera
Uda berrikua
Lore aintziñetako
Mancha gabekoa:
Erruzaitez bada
Bihotz gurekoa,
Denbora galdu gabe
Emanik frutuba.

Arbolak erantzun du
Kuntuz bizitzeko,
Eta bihotzetikan
Jaunari eskatzeko:
Gerlarik nai ezdegu,
Pakia betiko,
Gure lege zuzenak
Emaen maitatzeko.

Erregutu diogun
Jaungoiko jaunari
Pakia emateko
Orain eta beti:
Bai eta indarrare
Zedorren lurrari
Eta bendizioa
Euskal-Erriari.

Orain kanta ditzagun
Laubat bertso berri
Gure probintziaren
Alabantzagarri:
Alabak esatendu,
Su garrez beterik,
Nere biotzekua
Eutziko diat nik.

Gipuzkoa urrena
Arras sentiturik
Asi da deadarrez
Ama gernikari:
Etorri etzeiten
Arrimatu neri
Zure sendogarria
Emen nukezu ni.

Ostoa berdia eta
Zaiñak ere fresko
Nere seme maiteak
Enaiz eroriko:
Beartzen banaiz ere
Egon beti pronto
Niganikan etsaiak
Itzurreraz teko.

Gutiz maitagarria,
Eta oestargiña,
Begiratu gaitzatzu,
Zeruko Erregiña,
Gerlarik gabetanik
Bizi albagiña
Oraindaño izandegu
Guretzako diña.