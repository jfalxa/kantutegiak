---
id: ab-3572
izenburua: Goizian Goizik Jaiki Ninduzun
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003572.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003572.MID
youtube: null
---

Goizian goizik jaiki nindüzun esposa nintzan goizian,
Bai eta ere zetaz beztitu iguzkia atherat zian,
Etchek'andere handi nindüzun eguerdi gainian,
Alhargun gazte gelditu nintzen iguzkia sarthu zenian.

Musde Hirigarai, ene jauna, altcha dautazu buria:
Ala dolutu othe zuzu enekin esposatzia?
- Ez, ez, etzi zautazu dolutu zurekin esposatzia,
Ez eta ere doluturen, bizi nizeno munduian.

Nik banizun maiteñobat grazia ederrez betherik,
Mundu ororen ichilik, eta Jinko jaonari jakinik;
Buket ederbat igorri zautan, lili arraroz eginik,
Lili arraroz eginik, eta barnean phozoaturik.

Zazpi urthez atchiki dizut, senharra hila etchian,
Egunaz arrosa pian, eta gauaz bi besoen artian
Zitroin urez berekatuz astian egun batian,
Astian egun batian eta ortzirale goizian.