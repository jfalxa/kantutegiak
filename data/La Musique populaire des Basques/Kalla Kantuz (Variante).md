---
id: ab-6039
izenburua: Kalla Kantuz (Variante)
kantutegia: La Musique Populaire Des Basques
partitura: null
midi: null
youtube: null
---

Bazterretik bazterrerat
Oi! Munduaren zabala,
Eztakienak lirozu
ni alagera nizala
Hortzetan dizut erria
eta begietan nigarra.