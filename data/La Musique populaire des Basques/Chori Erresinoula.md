---
id: ab-3593
izenburua: Chori Erresinoula
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003593.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003593.MID
youtube: null
---

Chori erresiñoula, hots, emak eneki;
maitiaren borthala biak algarrekin:
Deklara izok gero botz eztibatekin,
Haren adichkidebat badela hirekin.

- Heltu ginenian maitiaren borthala,
Horak hasi zeizkun tchanphaz berhala,
Ni ere joan nintzan bertan gordatzera
Erresiñoula igain harichbatetara.

"Nour dabila hor gainti! Nounko zirade zu?"
- "Etchondorik eztizut, pharka izadazu;
Egarri gaichtobatek heben gabilzazu:
Uthurri hounbat, othoi, erakats' zadazu..

- "Egar' izanagatik ezta mirakullu:
Igaran egunian berochko egin du;
Uthurri hounik, heben, batere eztuzu:
Zuk galthatzen duzuna, goure behar dugu.""