---
id: ab-3569
izenburua: Gastetasunak Bainerabila
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003569.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003569.MID
youtube: null
---

Gastetasunak bainerabila airean ainhara bezala,
Gauak phasatzen ditut ardura egunak balire bezala,
Oi! Ardura nabila maitia gana!

Maite nauzula zuk erraitiaz ni ez naiz alegeratzen;
Baizikan ere nere bihotza arras duzu tristetzen,
Oi! Zeren ez nauzun kitatzen!

Amodiorik badudala ez zerauzuia bada iduri?
Itsasoa phasa nezake zure gatikan igeri,
Oi! Zeren zaren hain charmagarri!

Charmagarria banaiz ere eznaiteke izan zure,
Nitaz agrada direnik bertzerik munduan beitire;
Oi! Ni ganik urrun zaite.

Khantu haukien ontzaliak etzuien eskripularik;
Kaderan dago jarririk, eta segur alegerarik,
Oi! Penarik gabe, bat galdurik.