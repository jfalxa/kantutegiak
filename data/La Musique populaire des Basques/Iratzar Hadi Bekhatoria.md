---
id: ab-3575
izenburua: Iratzar Hadi Bekhatoria
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003575.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003575.MID
youtube: null
---

Iratzar hadi bekhatoria,
Orai duk, orai, ordia;
Gogomak, gaichoa,
Kontre duk Jinkoa;
Ordu diano egin itzak
Hareki lako bakiak.

Orai heltu duk Gorochuma,
Egin ezak penitentzia;
Kobesa gogotik
Bekhatiak osoki,
Eta gero erresoli
Ez utzulzera jagoiti.

Arartekari ezagut ezak
Andere Dona Maria,
Aithortzen diala
Min handirekila,
Hanitchetan Jinko Jaona
Hik ofentsatu diala.

Igain hadi mendingaña
Kalvario saintiala:
Ikhousiren duk han
Jesus khurutchian,
Odoletan sountsiturik,
Goure salbatzia gatik.