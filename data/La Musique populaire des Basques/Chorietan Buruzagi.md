---
id: ab-3562
izenburua: Chorietan Buruzagi
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003562.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003562.MID
youtube: null
---

Chorietan buruzagi
Erresiñoula khantari:
Chorietan buruzagi
Erresiñoula khantari:
Khantatzen dizu ederki,
Goizan, argi hastiari;
Oi! haren aire ederrak
Choraturik nai ezari.

Erresiñoula khantari
Chori ororen buruzagi.
Erresiñoula khantari
Chori ororen buruzagi.
Hanitchetan behatu niz
Haren botz eztiari,
Jeikirik ene oheti,
Khanberako leihoti.

Gazte niz et.alagera
Bai et.erria goihera;
Gazte niz et.alagera
Bai et.erria goihera;
Kountent, irous, alagera,
Deusek ez egiten phena;
Ororekil.adichkide
Estekamenturik gabe.