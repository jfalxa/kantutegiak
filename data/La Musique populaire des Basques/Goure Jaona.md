---
id: ab-3564
izenburua: Goure Jaona
kantutegia: La Musique Populaire Des Basques
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003564.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003564.MID
youtube: null
---

Goure Jaona, ostia saintu hontan
Gorderik gizon guzien hounetan,
Bihots umil batez dezagun adora
Eta egiazki, maitha houra bera. (Bis)

Jesus houna, amorioz bethia,
Zu zirade ororen hazkurria.
Ah! betha gitzatzu beneditzionez,
Eta bethi lagunt zoure houn guziez.

Ziek orok, ainguru gloriousak,
Gourekechi ematzie eskerrak,
Orai eta bethi, Trinitatiari,
Hainbeste ukhen dugulakoz fabori.