---
id: ab-4234
izenburua: Antonio
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004234.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004234.MID
youtube: null
---

Antonio
Monio
Ziribitonio;
goiko kalian hiru gizon.
- Ez jauna;
bai, jauna:
zakurrak jan dio belauna.