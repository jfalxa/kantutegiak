---
id: ab-4263
izenburua: Bisitan Izandu Naiz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004263.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004263.MID
youtube: null
---

Bestek aplikatu dit
neronei andria,
zarra eta itsusiya,
juiziyoik gabia;
ortzak ere juanak,
alperra ta dongia,
ura ere nere gisa
arlote pobria.

Bixita egin niyon
lengo arratsian,
ziyon aritzen zala
iñoiz ardatzian;
orduan lan aundirikan
etzeukala etxian,
etzanda egondu zan
denbora luzian.

"Emakumia, zaude
kristaban moduan,
juiziyoik baldin badezu
behintzat buruan;
artzaia ardi-kontu
daguan orduan,
olaxe egoten da
ziarka soruan".

Gau artan argirik ere
pixtu etzenidazu,
aizeka lau txikortari
eman nioten su.
"Txantxetan edo alai
etorri al zeran zu,
jakin nai nuke eta
esan bihar dirazu".

Illia urdindu eta
kolore oriyak,
atzaparrak motel eta
zankuak nagiyak;
xoxo bat ainbat badira
dam'orren begiyak,
gezur aundirikan gabe
esan ditut egiyak".