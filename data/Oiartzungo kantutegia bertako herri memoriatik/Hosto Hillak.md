---
id: ab-4322
izenburua: Hosto Hillak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004322.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004322.MID
youtube: null
---

Oroitzea hainbeste naiko nuke
lagun minak ginan egunetaz;
garai hartan bizitza gendun maite.
Oroitu eguzki beroetaz.
Hosto hilak palaka biltzen dira;
- astutzea ez nuke nai-
Hosto hilak palaka biltzen dira;
oroitzak eta penak ere bai.
T'aize hotzak daramazki
aztu-aldi tristera.
Baina ez daukat aztua
Kantatzen zendun kantua:

Errepika:
Kantatuaz gera elkartzen,
Zuk ni maite,
nik zu maite.
Poz beteaz gera maitatzen,
zuk ni maite,
nik zu maite.
Bizitzak maiz banatzen ditu
maitale ziran gaixoak;
itsasoak urratutzen ditu
maite hostuen pausoak.

Hosto hilak palaka biltzen dira,
oroitzak eta penak ere bai.
Maitasun beroa guri begira
jartzen zitzaigun pozez ta alai.
Polita zinan ta maite zintudan;
Oroi orduko egunetaz.
Garai hartan bizitza gendun maite.
Oroitu eguzki beroetaz.
Lagun minak ginan biok,
ez daukat ez azturik.
Benetan nauzu oroitzen
zuk nola zendun kantatzen.