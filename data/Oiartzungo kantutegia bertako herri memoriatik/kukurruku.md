---
id: ab-4348
izenburua: Kukurruku
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004348.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004348.MID
youtube: null
---

- Kukurruku!
- Zer diozu?
- Buruan min.
- Zeinek egin?
- Axerkok.
- Axerko non da?
- Errotapian.
- Zertan?
- Baxakanak biltzen.
- Baxakanak zertako?
- Olluari emateko.
- Ollua zertako?
- Arrautza egiteko.
- Arrautza zertako?
- Apaizari emateko.
- Apaiza zertako?
- Meza emateko.
- Meza zertako?
- Gu ta mundu guziya salbatzeko.