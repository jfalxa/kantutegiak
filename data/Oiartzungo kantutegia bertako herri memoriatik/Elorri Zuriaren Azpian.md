---
id: ab-4284
izenburua: Elorri Zuriaren Azpian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004284.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004284.MID
youtube: null
---

Elorri zuriaren azpian
anderea lokarturik,
Arrosa bezain eder
elurra bezain xuririk,
Hirur kapitainek hor zeraukaten
gortez engainaturik.

Parisen ostalertsa batek
ederki salutatu,
Ederki salutatu
berriak ere galdatu:
- "Bortxaz ala amodioz jina ziren
anderea, errazu..

- "Ez naiz, ez, amodioz jina,
ongi bortxaz jina nuzu,
Hirur kapitainek
bortian eneganaturik,
Pariserat ekarri ninduen
aitak eta amak jakin gaberik..

Kapitainak hitz hori entzunik,
jin ziren andereagana:
- "Anderea, afal zite
eztirik eta trankilik,
Hirur kapitain badituzkezu
gaur zure zerbitzari..

Anderea hitz hori entzunik
hilotza zen erori.
Kapitainak ere partitu
beren zaldiak harturik.
Nigar egiten zutelarik
andereari dolu emanik.

Handik hirugarren egunera
hilak aitari oihu:
- "Aita, entzun nezazu
ni orain hemen nuzu.
Birjinitatea beiratu nahiz
hilarren egona nuzu.