---
id: ab-4417
izenburua: Sarjentua Moxkorra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004417.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004417.MID
youtube: null
---

Sarjentua moxkorra
txarretera galdu;
neskatxak dirua emanta
berria erosi du.
Ai, ai, ai mutilla
txapela gorriya.