---
id: ab-4390
izenburua: Neure Bihotzeko Amatxo Zarra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004390.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004390.MID
youtube: null
---

Neure bihotzeko
Amatxo zaharra,
aintzinako ama Euskera.
Seme leial bat
orain datortzu
azken agurra emotera.
Hainbeste gerra
gehitu ezin da
danori atsotu zara.
Zaurien zauriz,
galdu galduta
Amatxo zuaz hiltzer.