---
id: ab-4285
izenburua: Enakela, Banakela
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004285.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004285.MID
youtube: null
---

Enakela, banakela,
Enak hartakua;
buztinakin ingo nikek
hi bezelakua.