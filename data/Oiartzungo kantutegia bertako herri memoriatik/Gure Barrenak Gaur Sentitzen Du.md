---
id: ab-4304
izenburua: Gure Barrenak Gaur Sentitzen Du
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004304.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004304.MID
youtube: null
---

Gure barrenak gaur sentitzen du
eztakit zenbat poztasun,
denok txoratzen arkitzen gera
ta ez al degu ezagun?
Zorionean ezagutua
egun batez guk Oiartzun,
bertsolari on, maitagarriak,
zuek nai zaituztet lagun,
kabitxo kutun zoragarri au
pixkat goraldu dezagun!

Mendi lerdenak alde guzitik;
erdian dago gaxoa,
zelai baratzez inguraturik
parrez dagon erritxoa;
sagasti denak lorez jantzirik,
marmarka errekatxoa,
aize otxanak emen dakarki
lilien usai goxoa...
oiartzuarrak, poztu zaitezte,
auxen da paradisoa!

Gau paketsutan kiñuka dabiltz
goi artako izar denak,
maindire zuriz bilduta Oiartzun
lotan ikusten dutenak.
Zuentzat dira berak dituzten
argirik diztikorrenak,
zuentzat udaberri goizetan
zeruko intzik fiñenak,
Jaunari eskerrak emaizkatzute
emen jaio ziñatenak.

Bein ezagutu zindutan eta
geroztik zaitut maitea,
neretzat ain gauz pozgarria da
lur eder au zapaltzea.
Ainbat gizon on eta jakintsuk
izan duten aterpea,
Mendiburu'ren kabi goxo au
bego dirdiraz betea,
gizaldietan izan dezala
zoriona eta pakea.