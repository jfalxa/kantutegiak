---
id: ab-4353
izenburua: Maiteagoak  Udazkenean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004353.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004353.MID
youtube: null
---

Maiteagoak udazkenean zuhaitzen osto gorriak,
oritasunez apainduriko makal zuzenen gerriak,
hegoizetan lokartutako iratze okre geldiak.

Maiteagoak, azken mugetan, iraganaren zantzuak,
lore xumeen petalo eme, ur izkutuen kantuak,
euri epelen gozotasuna, gaueko zeru altuak.

Maiteagoak sua ta jaia, fruitu berrien haziak,
uda beteko musiketatik ernaldutako antsiak,
Eros ausartak gorputz gaztetan ixuritako graziak.

Maiteagoak behin joan zirenak sekulan ez itzulitzeko
begiek zabal dirauten arte argitan blai itzaltzeko:
sufritu duen gizonak ez du lotsarik behar galtzeko.