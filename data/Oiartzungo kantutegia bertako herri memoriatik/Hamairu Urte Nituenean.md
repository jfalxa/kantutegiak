---
id: ab-4307
izenburua: Hamairu Urte Nituenean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004307.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004307.MID
youtube: null
---

Hamairu urte nituenean,
oraindik galtza motxetan,
bere atzetik ibiltzen nintzen
herriko kale hotzetan
prinzesa hura behar bezela
kortejatzeko lotsetan,
bere etxera lagundu nahirik
eskolatik arratsetan.
Oh ez, esaten zuen, oraindik ez,
gaztetxo naiz;
oh ez, iñolaz ez, etxera joan behar dut
garaiz.

Hemezortzira allegatuta
romantizismoz beterik,
etzan posible eromen hura
bazter batera uzterik,
aingeru harek ez zidan sortzen
sufrimentua besterik,
sekulan ere ez nuen lortu
bi besoetan hartzerik.
Oh ez, esaten zidan, maitemintzeko
gaztea naiz,
oh ez, iñolaz ez, lotsa ematen dit ta ez
dut nahi.

Lotsaren lotsez pasa ta gero
dozenerdi bat eskutik,
pentsatu zuen komeni zela
sartzea bide hestutik.
Eta nik nola oraindik ere
jarraitzen nuen gertutik
ezkondutzia eskatu nion
ez bazitzaion inportik.
Oh ez, erantzun zidan, aspertzen nauzu,
zoaz apaiz,
oh ez, iñolaz ez, beste batekin
eskontzen naiz.

Festa batean ikusi nuen
handik zortzi bat urtera,
ta hotz-hotzean ausartu nintzen
sagar helduen hartzera,
irrifar doble makur batekin
izkutatu zen atzera,
ez zen ausartzen bere zezena
adarrez adornatzera.
Oh ez, ez da posible, horrelakorik ez
nuke nahi,
oh ez, iñolaz ez, ni etxekoandre jator
bat naiz.

Mutilzartuta bizi nintzela
laugarren pisu batian,
behin uste gabe topatu nuen
gure etxeko atian.
Bere onenak gastatutako
alarguna zen artian.
Eta honela erantzun zidan
horaingoz bion kaltian:
Oh ez, orain ia ez, nahiz eta zutaz
pentsatu maiz,
oh ez, iñolaz ez, oheratzeko zahartu
naiz.