---
id: ab-4233
izenburua: Andre Madalen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004233.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004233.MID
youtube: null
---

Andre Madalen! Andre Madalen!
laurden erdi bat oliyo;
aitak jornala ekarritzian
amak pagatuko diyo.

Aita-semiak aritzen giñan
uda partian labrantzan;
andria, berriz, ganbaratikan
tabernarako arrantza.