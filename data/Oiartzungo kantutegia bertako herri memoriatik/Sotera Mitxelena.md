---
id: ab-4421
izenburua: Sotera Mitxelena
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004421.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004421.MID
youtube: null
---

Sotera Mitxelena
da horren konduta;
onik batere ez dauka
gaiztua kenduta.
Korrituan ipini
neri harrapatuta,
ezin leike garbitu
lixiban sartuta.