---
id: ab-4437
izenburua: Xango-Mango
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004437.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004437.MID
youtube: null
---

- Xango-mango,
haurra nongo?
- Ona bada etxerako;
gaixtua bada kanporako.