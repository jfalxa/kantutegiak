---
id: ab-4394
izenburua: Nik Baditut Zapatillak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004394.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004394.MID
youtube: null
---

Nik baditut zapatillak, emartzegaiak (sic) emanak.

Nik baditut galtzetiñak, emartzegaiak emanak: galtzetiña ziriko
finak, zapatillak txarola
fiñak, ebilletan kordoinak, emartzegaiak emanak.

Nik baditut pantalonak, emartzegaiak emanak: pantalonak oial fiñak,
galtzetiña ziriko
fiñak, zapatillak txarola fiñak, ebilletan kordoinak, emartzegaiak
emanak.

...gerrikoak,...: gerrikoa seda fiña, y sigue con pantalonak oial
fiñak, como el número 3.

Nik baditut alkandorak, emartzegaiak emanak: alkandora olanda fiña,
gerrikoa seda
fiña... y los del número 3.

Nik baditut txalekuak,...: txalekua korte fiña, alkandora...,
gerrikoa..., y los del número 3.

Nik baditut relojuak emartzegaiak emanak: relojua zillar fiña,
txalekuak korte fiña,
alkandora olanda fiña, gerrikoa seda fiña, pantalonak oial fiñak,
galtzetiña ziriko fiñak,
zapatillak txarola fiñak, ebilletan kordoinak, emartzegaiak emanak.