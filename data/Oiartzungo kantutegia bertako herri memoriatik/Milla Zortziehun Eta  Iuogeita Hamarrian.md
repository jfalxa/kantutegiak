---
id: ab-4374
izenburua: Milla Zortziehun Eta / Iuogeita Hamarrian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004374.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004374.MID
youtube: null
---

Milla zortziehun ta
iuogeita hamarrian
San Lukasen bezpera,
illunabarrian.
Funtziyua polita
begiyen aurrian,
ikusirikan gaude
"Gaztelu" aldian.

Dama Mariya Juana
jostagura al-zera?
Dozenerdi bat bertso
banua jartzera
Horla etortzen dira
txoruak zuhurtzera;
atoz nahi dezunian
bañuak hartzera.

Bi gauetan errenko
itxolara harrika,
ez dutela ametitzen
goierriarrika.
Afaria laga ta
atzittu korrika;
orain beratzen daude
urian jarrita.

Dama ez zabiltzala
iñori peatzen,
ez badezu egon nahi
urian beratzen.
Antoniok badaki
neskatxak bañatzen;
horlakuak ez dira
dirutan pagatzen.

Beste dama bat ere
bazuen laguna,
berarekin batian
ekarri ziguna.
Ilargi ederra zegon
ez zen gau illuna;
lehorrian zegoen
harentzat fortuna.

...