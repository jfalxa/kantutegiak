---
id: ab-4252
izenburua: Behin Batian Loiolan
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004252.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004252.MID
youtube: null
---

Behin batian Loiolan,
erromeriya zan;
hantxe ikusi nuen
neskatxa bat plazan.
Txoriya bera baino
arinago dantzan.
Huraxe bai polita
han politik bazan.

Aldaera:
Behin batian Loiolan
erromeria zan;
hantxe ikusi nuen
neskatxa bat plazan.
Txoriya bera baino
arinago dantzan
huraxe bai polita,
han poli...