---
id: ab-4349
izenburua: Lehen Negarrez
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004349.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004349.MID
youtube: null
---

Lehen negarrez,
orain parrez:
soldado xaharrakin
ezkondu nahi ez.