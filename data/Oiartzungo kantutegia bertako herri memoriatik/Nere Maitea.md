---
id: ab-4385
izenburua: Nere Maitea
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004385.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004385.MID
youtube: null
---

Nere maitea ez egon sustoz,
biziko gera munduan gustoz.
Palazio bat eginen dugu
sekalez edo lastoz.
Balkona ere basa lizarrez
teilatua belar igarrez.
Ala goizean enkargatu da
Baionako Pierres.