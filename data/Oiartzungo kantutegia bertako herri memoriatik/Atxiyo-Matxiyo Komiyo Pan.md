---
id: ab-4244
izenburua: Atxiyo-Matxiyo Komiyo Pan
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004244.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004244.MID
youtube: null
---

Atxiyo-matxiyo komiyo pan.
Nere semia errotan.
Errotara nijuala topatu nuen erbi bat;
kendu niyon begi bat:
para niyon berri bat...
Errota-pio-klak-klak.