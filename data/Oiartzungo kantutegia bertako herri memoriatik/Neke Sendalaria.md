---
id: ab-4382
izenburua: Neke Sendalaria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004382.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004382.MID
youtube: null
---

Neke-sendalaria,
buru argitzaillea,
lanaren saria...
Amestegi guria,
naigabeen azkaia,
nagien nausia.
...
Bitez agur nereak
afalonduon behintzat
guztiak zuretzat
Ta "aita gurea" bat ere
biyoa, zu asmatu
zinduanarentzat.

Baina... Lasatu nadin
orain "aita-gure" ka
eraso baiño leen,
yaun zintzo orren baimenaz,
mordoka datozkidan
neurtitzok esaten...

Afaldu det oparo.
Betazalok otoika,
eskatzen zaudate.
Aurrean zu, zabalik,
elurra ainbat zuria,
begiok zakuste.

Sartu ditzadan leenen
estalkiok azpitik,
dagokion neurriz;
oiñik irten eztaidan
gaberdian -oi baitu!-
lotsagabekeriz...

Apatx, orain!... Biyoaz
oiñatekuok gora
abea yo arte:
nere pozaren berri
auzo goi eta beekoak
ikasi bezate!

Txamarra kendu, bota
galtzok ere: ez atseden...
Orrenbeste neke!...
Zuurragoak izanak
dituk ene guraso
Adan eta Ebe!...

Noizbait! Gerturik nago.
"Aitaren" egin eta
yo dezadan murkil.
Bat, bi, iru, lau... aguro!
negu-gauaren otzak
ez nazan bertan il.

Orra barruan, yaunok,
maindire leun-artean
bilduta, buru-oin.
Sarrerako otz-ikara,
baiñan, samurtu arte,
arren, pixkat itxoin.
...
Oraintxe, ba, muturra
zerbait atera daiket,
berriketa-yarrai.
Zu goratzen, maitea,
logurak autsi arte
ez dut ixildu nai.

Ai, nolako zoroak
gauez kalerik-kale
ibil oi-diranak!
Arkiko aal-dituzte
asun miñez beterik
iñoiz beren oiak!...

Atean aize latza
dabil aserre... Noski!:
oirik ezpaitauka...
Leioan txingor-otsa
da, neri loa, nonbait,
galerazi-billa.

Lan gaitza, gaitzik bada.
Ateko giro txarra
lotarako onena;
seaska ustu genduan
aur samur azitxuon
llo-llo biguiñena.

Biar?... Gogapen txarrok
urrun! Egun banari
gau bat darraikio
egiñaz atsartzeko,
ez eginkizunarren,
orde, kezkatzeko.

Ta... irakurle maitea,
laburrerako oraintxe
berriketan nuzu...
Begiak itxi zaizkit,
burutzar au moteldu...
arnasa...ba...kan...du...

...Ames dagit, basoan
il nauela, parreaz,
maitagarri batek,
ta, gozorik, illeta
abesten didatela
berreun milla lorek...

Ames dagit nautela
tximirritek eortzi
abaraska baten
ta illobia, lasaki
ia ixilka, ari naizela
parra-parra yaten...

...Ames...Baiña, zer dantzut?
Irakurle guztiok
zurrungaka lorik?
Nere neurtitzok orren
lo-erazle bizkorrak
uste ez nun ziranik!

Beraz, gabon, gazteok...
Baiña... nere buru au,
yas...¡nola burutu?...
Oi-asmatzaillearen
onez agindutako
"Aita gurea" aaztu!

Gizagaxoa..., noski,
on-utsa bide-zanez,
zeruan aal-dago!
Nere "aita-gureen"
bearrik, beraz, ez du,
alegia, izango.

Gauzok ala dirala,
laga dezaket "aita-
- gurea" biarko...
Atean euri-otsa...,
aize-zotin luzeak...
Agur...Da...gi...dan lo...o...o
...