---
id: ab-4391
izenburua: Ni Naiz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004391.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004391.MID
youtube: null
---

Ni naiz
erreka zikiñen iturri garbiak
aurkitu nahi dituen poeta tristea,
ni naiz
kaleetan zehar neguko eguzkitan
lanetik datorren gizon bakartia.
Ni naiz ostorik gabe gelditzen ari den ardaska lehorra,
ni naiz pasio zahar guztiak kiskali nahi dituen bihotz iheskorra.

Ez zaidazu galdetu gauza illun guztien arrazoi gordeaz,
nora ote dijoan denbora aldakorrak daraman bidea.

Ni naiz
burrukaren erdian illunpetan etsita
amur ematen duen pizti beldurtia,
ni naiz ezerezetik ihes
munduaren erdian ezin aurkitutako amets urrutia.
Ni naiz irrifar bakoitzean gaztetasun hondarrak galtzen dituena,
ni naiz itsasoko haizeak gogor astintzen dituen laiñoen negarra.

Ez zaidazu galdetu...

Ni naiz
xori hegalari bat lurtasun zabarrari
etsipenez loturik dadukan katea,
ni naiz
beste asko bezela neguko eguzkitan
hotzez hiltzen dagoen gizon bakartia.
Ni naiz lorerik gabe gelditzen ari den ardaska lehorra,
ni naiz pasio zahar guztiak kiskali nahi dituen bihotz iheskorra.

Ez zaidazu galdetu...