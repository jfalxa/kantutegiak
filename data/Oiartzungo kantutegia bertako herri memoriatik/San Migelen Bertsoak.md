---
id: ab-4413
izenburua: San Migelen Bertsoak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004413.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004413.MID
youtube: null
---

San Migelen bertsoak
noa kantatzera,
aditu nahi duenik
inor balin bada
berri txarra dabiltze
munduan barrena
notizioso geienak
izanen al dira:
Aingerua lapurrek
eramana dela.