---
id: ab-4261
izenburua: Bideetako Harriak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004261.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004261.MID
youtube: null
---

Bideetako harriak,
egin karraxi!
Herri barruko zuhaitz zikinduak,
kaioletako txori marrantatuak,
señoriten zakur perfumatuak,
ibaietako ur apartsuak.
Egin karraxi, egin, bai, karraxi.

Iñoren menpe zaudeten arrazak,
fusilen beldur zeraten herriak,
hautatzerik ez duzuen nazioak,
propagandak burua antzututakoak,
goseak hiltzen zaudeten guztiak.
Egin karraxi! egin, bai, karraxi.