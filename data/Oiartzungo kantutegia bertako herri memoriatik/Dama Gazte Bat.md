---
id: ab-4270
izenburua: Dama Gazte Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004270.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004270.MID
youtube: null
---

Dama gazte bat irukitu dut
bihotzian ta goguan.
Ni bezelako ispillurikan
ez zen zuretzat orduan.
Despeira emanta, orain utzi nau
urte beteren buruan.