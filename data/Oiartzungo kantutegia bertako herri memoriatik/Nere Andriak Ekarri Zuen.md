---
id: ab-4383
izenburua: Nere Andriak Ekarri Zuen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004383.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004383.MID
youtube: null
---

Nere andriak ekarri zuen
Aranaztikan dotia.
Hobe zukian ikusi ez balu
Berdabioko atia.
Gaur ez zukian hark idukiko
dadukan pesalunbria.

Berdabioko semia naiz ta
adios, auzo nobliak.
Pena haundirik ez baitu utziko
ni hemendikan joatiak.
Negarra franko egingo baitu
nere emazte tristiak.

Neriak ez nau hainbeste oroitzen;
baina ez naiz ni bakarra.
Hazitzekuak han uzten ditut
bi seme ta hiru alaba.
Jaun Zerukoak adi dezala
haien amaren negarra.

Goizutan bada gizon gaizto bat
deitzen diote Trabuko
Hitzak eder ta bihotza faltso
behin ere etzaio faltako
Egin dituen dilijentziak
berari zaizka damuko.

Ongi bai ongi oroitzen al haiz
zer egin huen Elaman.
Jarraikin onik izandu balu
difuntu harek Lesakan,
ehorrek egon bearko huen
ni orain nagon atakan.

Martin Josepe Aranaztarra
zegoen gizon prestua.
Disimuluan botatzen zuen
aldian bere puntua.
A ver lehenago ote nekien
moldetariân kontua.