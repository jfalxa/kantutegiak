---
id: ab-4406
izenburua: Pilotari Ederrak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004406.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004406.MID
youtube: null
---

Pilotari ederrak
Oiartzunen daude:
jokatzale garbiak
tranparikan gabe.
Horiek ziotenaz
baziren lehen ere.
Guziok dakitena
bai eta orain ere.
Nik holakorik ez det
goguan beiner.