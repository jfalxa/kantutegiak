---
id: ab-4245
izenburua: Ave Maria Purisima (Toberak)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004245.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004245.MID
youtube: null
---

Ave Maria Purisima!
Jaungoikoak gabon!
esplikatzera nua
zer desio dugun:
toberak jotzera gatoz
bost edo sei lagun.

Toberak jotzera eta
libertitutzera,
ez uztiagatikan
usariyua galtzera;
borondaterik ezpada
joain gera atzera.

Agur, agur itxekuak:
Jaungoikuak gabon!
Ezer ere esan gabe
ezin giñezke egon:
hementxen eldu gera
lau edo bost lagun.

Gure desiua zer den
nua deklaratzera:
usatzen dan moduan
toberak jotzera;
borondaterik ezpada
joain gera atzera.

Libertitu-zaliak
zuek ezik gu ere;
bestela ezgenduen pasako
horrenbeste bire;
borondaterik ezpada
lehen aña adixkire.

Nobiyo jaunak esan diyo
andre nobiyari:
- "Zer egingo ote-diyegu
gizon horiyeri?
Umore ona pasatzia
Gustatzen zait neri".

"Libertsiyo palit bat
alkarren artian,
pasatzia hobia
bitatik batian:
eztut uste izain den
iñoren kaltian".

Andre nobiyak errespuesta
Jakiña bezela:
- "Etorri diraden ezkero
kanta dezatela;
lotsa haundiya emango
diyegu bestela".

Nobiya andriak errespuesta
leyala ta prestua:
- Umore ona pasatzia
litzake nere gustua;
erdi bana pagatuko degu
horiyen gastua.

Konformatu dirade
biyok alkarrekin:
guk ere segi dezagun
umore onakin;
lagunak, palanka juaz
kantatu "San Martiñ".

Biyen berriyak horrela
jakindu ezkero,
besteren esan-mesanik
ez nuke espero:
lagunak San Martin kantatu ta
palanka jo gero.

Hanka bat abajo daukat
bestia levanta;
xinistatuko duzute
egiya esan-ta:
orain zuek egin zerate,
hurren bestien txanda.

Oya jarriya dago
maindire zuriyakin;
maindire zuriyakin ta
bi almoadakin
nobiya andria etzateko
nobiyo jaunakin.

Mayatzaren hamabiya
sekula etzait aztuko;
Ezkontza hau ikusi-ta
zein ezta poztuko?
Gaur arratsian hoyek biyek
eztira oztuko.

Hemen burruntziya deitzen diyogu
Napar-aldian gerrena:
batzuentzat konsuelua ta
bestientzat pena;
nobiyo jaunak agindu digu
sartzeko barrena.