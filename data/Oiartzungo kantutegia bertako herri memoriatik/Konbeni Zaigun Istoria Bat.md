---
id: ab-4345
izenburua: Konbeni Zaigun Istoria Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004345.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004345.MID
youtube: null
---

Konbeni zaigun historia bat
kantatu ahal bageneza.
Horretarako lagun zaiguzu
zeru lurreko Jueza.
Beljikan ziran bihotz oneko dukia
eta dukesa;
haien alaba Jenobeba zan
Rin-Moselako kondesa.