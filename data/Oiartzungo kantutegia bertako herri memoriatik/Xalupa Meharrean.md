---
id: ab-4436
izenburua: Xalupa Meharrean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004436.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004436.MID
youtube: null
---

Bizkar oker
arraun zuzen
xalupa meharrean
arrantzale.

Itsaso beltz
ilunabartze
kaiko barean
arraun hots eme.

Xalupa lerden
itsaso haize
arrantzaleak
azal beltzarre.

Esku latz bi
goian ilargi
zapelapean
neke begi.

Arrantzale
isil langile
lanera doa
iñor gabe.

Bizkar oker
arraun zuzen
ur ta txalupa
gauean haize.