---
id: ab-4294
izenburua: Ez Da Gernikan
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004294.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004294.MID
youtube: null
---

Ez da Gernikan
negarra baizikan;
- Herria sutan-
Egazkiñek askotan
bonbaz dute josi;
jendea igesi.
- Herria sutan-.

Nazi doixtarrak
eta franco-tarrak,
- Herria sutan-
Arratsaldez bostetan
dena dute erre,
errukirik gabe.
- Herria sutan-.

Su ta garrak,
inpernuko indarrak,
- Herria sutan-
Gernika ilunpetan
digute estali,
bizia itzali.
- Herria sutan-.

Bonba-txistua,
heriotz gaixtua,
- Herria sutan-
etxeko ateetan
indarrez da sartu,
bihotzak urratu.
- Herria sutan-.

Emaztem aurrak,
ta hauen negarrak.
- Herria sutan-
oiñaza odoletan
zoraturik dabiltz,
karrasi ta antsiz.
- Herria sutan-.

Gizon, abere,
ferian han daude.
- Herrian sutan-
Hauetxen bizkarretan
lertu dute bonba.
Justizia non da?
- Herria sutan-.

Franco zitalak,
egiñik ahalak,
- Herria sutan-
"Gorriak" gezurretan
zituen salatu,
lotsa denak galdu.
- Herria sutan-.

Pablo Picasso
minbera da oso,
- Herria sutan-
Gernikaren penetan
indarra du hartu,
triskantza margoztu.
- Herria sutan-.

Urruti lehen,
oraingoz Madrilen,
- Herria sutan-
Prado-ko saloietan
jarri dute. Horra!
bigarren zigorra!
- Herria zutan-.

Hor herriz-herri
dabil penagarri,
- Herria sutan-
arrotz-atzaparretan
Ohialak ikara;
Betor Gernikara!
- Herria sutan-.

Piztu bihotzak
eta esperantzak,
- Herria sutan-
Esna gaiten benetan
Euskaldun guztiak,
zahar ta gaztiak.
- Herria sutan-.

Euskadi zahar
hor dabil su ta gar.
- Herria sutan-
Arbolaren oiñetan
baturik gabiltzan,
zutik iraun dezan.
- Herriak zutan-.