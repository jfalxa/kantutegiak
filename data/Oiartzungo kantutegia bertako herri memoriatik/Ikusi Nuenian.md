---
id: ab-4324
izenburua: Ikusi Nuenian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004324.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004324.MID
youtube: null
---

Ikusi nuenian
nik zure kokotza,
iruditu zitzaidan, monona,
gaztainân morkotsa.

Ikusi nuenian
nik zure ahoa,
iruditu zitzaidan, monona,
karobi zuloa.

Errepika:
"Sí, morenita, sí,
si es para divertir;
tu llevarás la manta, morena,
yo llevaré el candil..

Ikusi nuenian
nik zure sudurra,
iruditu zitzaidan, monona,
odolki muturra.

Ikusi nuenian
nik zure begia,
iruditu zitzaidan, monona,
anpolai gorria.

Errepika:
"Sí, morenita, sí,...

Ikusi nuenian
zure belarria,
iruditu zitzaidan, monona,
azaren orria.

Ikusi nuenian
nik zure kopeta,
iruditu zitzaidan, monona,
frontoiân pareta.

Errepika:
"Sí, morenita, sí,...".