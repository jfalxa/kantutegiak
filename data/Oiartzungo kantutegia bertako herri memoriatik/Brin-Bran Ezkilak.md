---
id: ab-4266
izenburua: Brin-Bran Ezkilak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004266.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004266.MID
youtube: null
---

Brin-bran ezkilak...
- Nor duk hila, mutilak?
- Martin zapatariya.
- Zer egin du pekatu?
- Erregeren txakurra urkatu.
- Hori ez duk pekatu.