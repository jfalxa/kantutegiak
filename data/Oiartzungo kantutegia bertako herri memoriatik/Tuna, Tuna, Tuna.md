---
id: ab-4429
izenburua: Tuna, Tuna, Tuna
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004429.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004429.MID
youtube: null
---

Tuna, tuna, tuna,
Joakina tuntuna.
Ari, ari, ari,
Juan Mari dantzari.