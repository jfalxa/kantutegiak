---
id: ab-4402
izenburua: Ordu Bata Jo Du
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004402.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004402.MID
youtube: null
---

- Ordubata jo du?
- Ez.
- Ordubiyak?
- Ez.
- Hiruak?
- Ez.
- Labak?
- Ez.
- Bostak?
- Ez.
- Seiak?
- Ez.
- Zazpiyak?
- Ez.
- Zortziyak?
- Ez.
- Bederatziyak?
- Ez.
- Hamarrak?
- Ez.
- Hamaikak?
- Ez.
- Hamabiyak?
- Bai.
- Nere opilla erre da?
- Baita erreta jan ere.
- Neretzat zer da?
- Txakurraren bost arrautzak.