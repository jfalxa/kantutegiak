---
id: ab-4346
izenburua: Kontuz Ibil Laitezke
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004346.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004346.MID
youtube: null
---

Kontuz ibil laitezke gaztiak karrikan,
Egin gabetanikan azio txarrikan.
Dama orren pausuak ez dira alperrikan.
Iñork hautsitzen badu albaka adarrikan,
pagatu bear dute multa ederrikan.