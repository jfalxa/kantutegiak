---
id: ab-4443
izenburua: Zazpi Laguneko Famili Batean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004443.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004443.MID
youtube: null
---

Zazpi laguneko famili batean
harotza zen gure aita.
Gure herriko alkate ez izan arren,
lan egiten zuena.

Itsasoan urak handi dire,
murgildu nahi duanentzat.
Gure herriko lanak
handi dire,
astun dire,
gogor dire,
zatiturik gaudenentzat.

Markatzen ari diren bide nabarra
ez da izango guretzat.
Lehengo sokari ezarririk datorren
oztopo latz besterik.

Itsasoan urak handi dire,...