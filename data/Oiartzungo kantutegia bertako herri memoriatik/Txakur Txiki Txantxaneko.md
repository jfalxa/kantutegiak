---
id: ab-4432
izenburua: Txakur Txiki Txantxaneko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004432.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004432.MID
youtube: null
---

Txakur txiki Txantxaneko (bis),
Maria Lorentxaneko.
Lara-lara-lan, lara-lan,
lara-lara-lan, lara-lan.
Txakur txiki Txantxaneko
Maria Lorentxaneko.