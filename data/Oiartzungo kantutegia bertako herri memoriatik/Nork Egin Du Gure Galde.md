---
id: ab-4395
izenburua: Nork Egin Du Gure Galde
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004395.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004395.MID
youtube: null
---

Nork egin du gure galde?
Aiako Arritan gaude,
baña beti bezin trebe...
lanean gogoz aritzen gera
soldatarik jaso gabe,
berreun nagusi badaude
gure artaldeân jabe...
Guk umore ona alare!

Otz-aldi eta goseak
ta barau egun luzeak
guk baditugu paseak;
gorputzari on egiten nunbait!
gallur ontako aizeak,
nola baigeran gazteak,
sano dauzkagu esteak,
ez egonarren beteak.

Goizean ekin lanari,
egunsentian pozgarri,
txoriak guziak kantari;
eziñ atera gentzake orain
iñoiz adinbat izardi,
erdietan gaude geldi,
al degunean exeri...
zertan nekatu geiegi?

Oiartzungo neskatillak,
muxu-gorri biribillak,
izan zaitezte abillak;
zertan maitatzen dituzute
kaleko ume zirtzillak,
Aitxulei'n daude trankillak
saltzeko dauden opillak,
pare gabeko mutillak.