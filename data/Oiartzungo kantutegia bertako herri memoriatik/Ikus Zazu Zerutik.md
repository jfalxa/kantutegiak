---
id: ab-4325
izenburua: Ikus Zazu Zerutik
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004325.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004325.MID
youtube: null
---

Ikus zazu zerutik zure Euskalerria
nola datorren gaur zuregana
pozturik guztia.
Ama Euskera pizturikan
zuk zerutik lagunduaz;
zuri esker emateko
dago gaur esanaz.
O euskaldunak, esker Jaunari
eta laztan bat gure hizkuntzari;
agur eginaz Mendiburu maisuari
oiartzuarrari.
(Antonio Arzak.