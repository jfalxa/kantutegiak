---
id: ab-4416
izenburua: Santu Bata Baterako
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004416.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004416.MID
youtube: null
---

Santu bata baterako,
besterako bestea:
San Jabier, zu zara
nai dutenen onerako.
Dago gure bizia
neke penaz beterik.
- Iguzu, Jabier, eskua
artzeko zure bizia.

Ez andi, ta ez txikirik
arkitzen da munduan
ez duenik askotan
behar asko Zerutik.
Zuk hau bai dakizula
ez degu guk dudarik:
- Iguzu, Jabier...

Len ta gero bat zara,
ongi-gillea beti;
ez arren uka niri
orain behar dudana.
Uste dut, ez nazula
utziko ni galdurik.
- Iguzu, Jabier...

Bi toki zuk batetan
bete zenituen len:
arki zaitez gaur emen
or zaudela Zeruan.
Zu ba-zatoz gu gana,
ez da geroz nekerik.
- Iguzu, Jabier...