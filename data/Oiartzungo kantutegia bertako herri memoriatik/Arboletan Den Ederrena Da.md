---
id: ab-4237
izenburua: Arboletan Den Ederrena Da
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004237.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004237.MID
youtube: null
---

Arboletan den ederrena da
oihan beltzian pagua.
Nik maiteño bat bakarra izan da
bestetarako gogua.
Jaun Zerukoak emango al-dizu
niganako amodiyua.

bertsioa
Arboletan den ederrena da
oian beltzian pagua.
HItzak ederrak dituzu baina
bestetarako gogua;
Jaun zerukuak emango al dizu
niganako amodiyua.

Amodiyua zein den zorua
mundu guztiyak badaki.
Nik maiteño bat bakarra izanda
beste batek eramaki.
Ez nuke penik biziko balitz
nerekin baina hobeki.

Gure dotrinak erakusten du
zeruan dala atsegin.
Mundu hontako pasadizuaz
klaro nahi nizuke hitzegin:
ai! ni zurekin kontent nitzake
nahiz besterekin hutsegin.

Izar eder bat ateratzen da
Santiyo goiko lepuan.
Hari begira egoten nauzu
ateratzen dan orduan.
Nere bizitza pixka da baina
zeinen dolorez dijuan.