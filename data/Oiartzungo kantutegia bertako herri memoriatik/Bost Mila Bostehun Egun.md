---
id: ab-4265
izenburua: Bost Mila Bostehun Egun
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004265.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004265.MID
youtube: null
---

Bost mila bostehun egun bost mila bostehun gau
eta erlojauk lehengo sentiduan dirau
kartzeletako intxaurrak beti dira hamalau
gerturatzerik ez duen esaera añ da hau?

Gora demokraziaren autoritatea
betetzen ez badute beraien legea
gosearen indarra duen boterea
gaurko urak sortuko duen biharko lorea.

Piper gorri-berdeei bota bexamela
askatasun gordinez berdindu sabela
baietz gose-egarriak ase-bete berehala
euskal sukaldeetako aulkiak bezela.

Gosea preso dago ta presoak gose
heste hustuen uztak joko al du luze?
burni hotza aterpe, barne-hatsa haize
baina urak mantentzen du esperantza heze.