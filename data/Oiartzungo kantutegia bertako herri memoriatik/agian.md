---
id: ab-4210
izenburua: Agian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004210.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004210.MID
youtube: null
---

Agian zuk bazenekien
berak bazekiela
ez zeniola dagoeneko
leialtasunik gordetzen.

Agian berak bazekien
zuk bazenekiela
ordu oroko galderaren
erantzun beti estaliaz.

Orduan udaberriko lore xumeak
ikusten zenituen gauez
mesanotxe gainean
portzelanazko ontzitxoan.

Agian berak negar egiten zuen
malko isilez goizaldean
zure lo sorgorrak
kulparen angustia
estaltzerakoan.

Agian horrela behar zuen izan
isiltasuna, loreak, malkoak,
portzelanazko ontzitxoa buruki ondoan,
esker txarraren arauak
bait ditu bizitzak gehienetan
leial irauten dutenentzat.