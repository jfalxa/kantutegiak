---
id: ab-4226
izenburua: Alpargatak Urratuta
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004226.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004226.MID
youtube: null
---

Alpargatak urratuta
zapatarik ez;
Erniyon gelditu nitzan
oineko miñez.

Hauxen duk egiya,
zortziko berriya:
Hiru txiki ardorekin
librako ogiya.