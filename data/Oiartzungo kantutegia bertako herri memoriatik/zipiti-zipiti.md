---
id: ab-4446
izenburua: Zipiti-Zipiti
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004446.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004446.MID
youtube: null
---

Zipiti-zipiti bandera
Errege Frantzian dagoena
nahiz betor, ta nahiz bego
ni haren begira ez nago.
Akerrak kanta, idiak dantza
astoak danbolina jo.
Atariko intxaurraren gainean
kukuak jotzen badaki,
zipiti titi, zipititi.