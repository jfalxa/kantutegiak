---
id: ab-4371
izenburua: Milla Zortziehun Eta / Hogeita Bostian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004371.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004371.MID
youtube: null
---

Milla zortzieun eta hogeitabostian
au amairugarrena Urri'ko illian
Martin Itzeberri'ko (1) Altzibarrenian,
atiak jo nituen ongi illunian,
baita ireki ere borondate onian.

Ixtimatu ninduten konde bat bezela;
sillan exeri eta apaldu nezala;
konformatu ziraden familiyan ala
nagusiya ta biyak etziñen giñala...
berari sartu niyon petxutik puñala.

Oitikan saltatu zan eriyo-ihesi;
nigandik halakorik etzuen merezi.
Alabak zuenian aita hala ikusi
apaizaren eskaka bertatik zan hasi;
aizkora trixte batez niyon burua hautsi.

Ogeita hiru urteko neskatxa gaztia...
ez zen bada lastima hala tratatzia?
Aizkora trixte batez muñak saltatzia;
ez da milagro izain ni laixter galtzia;
aisa merezi nuen bertan urkatzia.

Ikusi zitunian senarra ta alaba,
Katalina gaxuak, ai, hura negarra!
Ez bazuan jarri nahi bestiak bezala
dirua non zeguan esan beriala;
ongi banekiyela dirurik etzala.

Santo Kristo, zurekin konfesa nadiyan
zer obra egin nuen nik Kataluniyan.
Lerida'n pasa eta Urbil'en erriyan
bost lagun bizi ziran beren familiyan;
giziyok hil nituen gabaren erdiyan.

Gorputzeko ikara, animako lotsa
zeren egin ditudan hamalau heriyotza.
Aditu ezkeroztik diruaren hotsa
bidean atera-ta botatzeko poltsa
aterako niola bestela bihotza.

Dirua eman ta gero kentzia biziya
hori egiten nuen gauza itsusiya.
Jaunak bere eskutik nenguen utziya;
orain errezatzen dut ahal dudan guziya
arren alkantzatzeko Beraren graziya.

Hogeitabost urte da nere edadia
- nagusi on batentzat ai zer mirabia!-
Ez naiz pekatuaren mantxarik gabia,
beti okerrerako abilidadia
penetan pasatzeko eternidadia.

Jainkoaren aurrerako nik daukadan lotsa
zeren egin ditudan hainbat eriyotza;
azkeneko sententziya izain da zorrotza;
lau laurden egin eta kentzeko bihotza
ai! hura urkaberako haiziaren hotza!

Jose det izena ta Larreina lombría:
Mendaro'n jayua naiz, bertako semia;
deskalabratua naiz munduan lajia;
amak egin dezala beste bat obia,
hain da izugarriya nere maldadia.

Lenguan izandu zait anaya gaztia;
lastima egiten zuen nere ikustia.
- Ai zer nola ote diran aita-ama tristiak!
Nitzaz eskarmentatu balitez bestiak...
Ni ere izutzen nau biziya kentziak.

(1) Markina Etxeberriko.