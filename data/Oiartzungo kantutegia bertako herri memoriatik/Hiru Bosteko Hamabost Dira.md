---
id: ab-4317
izenburua: Hiru Bosteko Hamabost Dira
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004317.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004317.MID
youtube: null
---

Hiru bosteko hamabost dira,
hamabosten kontua tira;
nahi duk jokatu pinta bat ardo
hamabost dirala.