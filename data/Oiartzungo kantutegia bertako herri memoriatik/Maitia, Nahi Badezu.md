---
id: ab-4354
izenburua: Maitia, Nahi Badezu
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004354.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004354.MID
youtube: null
---

Maitia, nahi badezu
nerekin ezkondu,
goizian lendabizi
jeiki biarko dezu
oietik, oietik,
oietik, oietik;
gero gosaritera
ni zure ondotik.

Goizian jeiki eta
lenbiziko lana:
eman biarko dirazu
neri pelangana
urakin, urakin,
urakin, urakin;
ta gañera kafia
bere koparekin.

Gosaldu eta gero
lagun-arterako
eman biarko dirazu
patrikararako,
arako, onerako,
jokurako, lagun-arterako;
nerekin bizimodurik
ez dezu aterako.

Gatia zera eta
juiziyorikan ez,
aspertuko zerade,
maitia, senarrez;
maitia, tristia,
zu zera neria;
orain zuria naiz eta
kontsola zaitia.

Zer zala uste zenduben
enamoratzia:
sillan exeri eta
gitarra jotzia?
Maitia, tristia,
zu zera neria;
orain zuria naiz eta
kontsola zaitia!

Preso sartu ninduten
txori bat bezela,
esatiagatikan
zuria naizela;
maitia, maitia,
maitia, maitia,
orain zuria naiz eta
kontsola zaitia.