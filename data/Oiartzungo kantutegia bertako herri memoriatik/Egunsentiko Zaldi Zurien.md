---
id: ab-4281
izenburua: Egunsentiko Zaldi Zurien
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004281.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004281.MID
youtube: null
---

Egunsentiko zaldi zurien
ihes larriaren otsa,
odol gorriak taupadatzen du
dontzeillatxoen bihotza,
maitasun amets erregarrien
deuseztapenaren hotza,
liho artean izkutatzen da
gorputz biluztuaren lotsa.

Lekunberritik Baigorriraino
mendi goitiak harresi,
zaldun ederrak ihiztari eta
orkatz emeak ihesi,
zer den ametsa zer den egia
gogoak ezin berezi,
iraganaren zaunka larriak
orainean ezin etsi.

Donapaleuko arratsaldetan
enarak leiho gainetan,
udaberriak tristura dakar
haizearen hegaletan,
melankolia ezinesan bat
jauregiko zuhaitzetan,
urte geldiak isilik doaz
lanbroaren ametsetan.

Udazken gorri gogorapentsu
malko isilen getari,
Orhy mendian elur xuriak
neguaren aintzindari,
urte aroan joan etorrian
batzuk besteen atari,
heriotzaren begi uzkurra
gau ta egun zelatari.