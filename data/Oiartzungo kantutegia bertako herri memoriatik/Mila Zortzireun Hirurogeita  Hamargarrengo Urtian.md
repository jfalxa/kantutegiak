---
id: ab-4370
izenburua: Mila Zortzireun Hirurogeita / Hamargarrengo Urtian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004370.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004370.MID
youtube: null
---

Mila zortzireun hirurogeita
hamargarrengo urtian
gizon abilak berexi ziran
Gipuzkoako partian.
Junta Erriela egina zuten
Aitxuleiñoko tartian,
Carlos Septimo behar zutela
lehen bait lehen para kortian.

Fede santua aumentatzera
atera ziren mendira;
obra ederrak egin dituzte;
denboz agertuko dira.
Motibo gabe eman diote
bosti negarra begira.
Mantxa gabeko gizon justuak
horretan ibiltu dira.

Kañoi ederrak Madriden eta
Kastilo finak Zaldinen;
"Calle Mayor" ta "Café Marina"
eskribiturik Berinen.
Alondegia bere sisakin
para zuten Ezkorrinen.
Hasi baino lehen bagenekien
ez zutela deus eginen.

Baleunak pisu, estiak zimur
begietako tristura
zer deabrutako atera ziren
goiko mendien kaskura.
Osasunaren kontserbatzeko
hartziagatik freskura.
Bazter guziak ondatu eta
liberalaren eskura.

Gerra zibila horren despeira
ez zen berehala ikusi;
egia klaro esan nezake
inork nahi badu ikasi,
asko sujetok desio zuen
Carlos Septimo nagusi
Peña Platian konjuratuta
hantxe joan huen ihesi.