---
id: ab-4256
izenburua: Bertso Batzuek Jarriko Ditut
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004256.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004256.MID
youtube: null
---

Bertso batzuek jarriko ditut
jendiai adierazteko
kulpikbe sartu ginduztela
giltzapian egoteko.
Hori gertatu behar zuenik
inork ez zuen usteko.
Ez det bihotzik gertatu dana,
aita, zuri esatek.