---
id: ab-4310
izenburua: Haurtxo Txikia, Negarrez Dago
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004310.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004310.MID
youtube: null
---

Haurtxo txikia negarrez dago,
ama emaiozu titia;
aita gaixtua tabernan dago
pikaro jokalaria.