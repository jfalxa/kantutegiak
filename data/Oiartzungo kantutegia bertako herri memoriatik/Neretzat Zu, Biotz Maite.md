---
id: ab-4388
izenburua: Neretzat Zu, Biotz Maite
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004388.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004388.MID
youtube: null
---

Neretzat zu, biotz maite,
zara udarako ifar aize.
egin nai zaitut nik orain laixter
nere emazte.

Itz goxo oiek entzunik
nere arima dago pozik
munduan dena ez da nai baizik
bizi mundik?

Gure etorkizuna
dago iluna
galduko dugula
itxaropena.

Nere asmoan gaur dago
saiatzea zuretzako
argatik ezkontzea naiago
zer obeago?

Zure gogoak badaude
gazte guztietan nun nai
lanarik eta dirurik gabe
gera ezkongai

Gure etorkizuna
dago iluna
galduko dugula
itxaropena.