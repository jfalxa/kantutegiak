---
id: ab-4250
izenburua: Bat. Gure Jauna Bera Duk Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004250.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004250.MID
youtube: null
---

Bat.
Gure Jauna bera duk bat.
Bi.
Erromako bi aldariak,
gure Jauna bera duk bat.
Hiru.
Hiru Trinidadiak.
Erromako bi aldariak,
Gure Jaunak bera duk bat.

Hamabi.
Hamabi Apostolu Santuak.
Hamaika mila Birjinak.
Hamar mandamentuak.
Bederatzi ordenamentuak.
Zortzi zeruak.
Zazpi gozuak.
Sei aingeru argi egilleak.
Bost llaga preziosuak
Lau Ebanjelista Santuak.
Hiru Trinidadiak.
Erromako bi aldariak.
Gure Jauna bera duk.