---
id: ab-4313
izenburua: Hemen Heldu Gerade
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004313.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004313.MID
youtube: null
---

Hemen heldu gerade
berri on batekin,
gure enbajadore
Olentzerorekin.

Olentzero joan zaigu
mendira lanera,
intentziyuarekin
ikatz egitera.

Aditu duenian
Jesus jaio dela,
laisterka etorri da
parte ematera.

Horra! Horra! Gure Olentzero:
Pip'ortzian duela
eserita dago;
kapoiak ere baitu
arrautzatxuakin
bihar merendatzeko
botila arduakin.

Olentzero inondik
ezin dugu ase;
osorik jan dizkigu
hamar zerri gazte.

Sahieski ta xolomo
makina bat heste.
Jesus jaio da eta
kontsola zaitezte

Horra! Horra!...