---
id: ab-4283
izenburua: Ekaitzak Du Tximista
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004283.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004283.MID
youtube: null
---

Ekaitzak du tximista
erronka ezaugarri.
Zu nere telebista
bizitza gozagarri.
Maite minduen erreka
bihotzen freskagarri.
Zure begien oreka
nere nahien oinarri.
Begien irudimenez
zerua zuregan nuen ikusi.
Estalirik zorionez
zure kateetan murgilduz jausi.
Xerka ninduzun gau beltzez
ametsetan zinen beti nagusi.
Zu gabe ezin biziez
ezer ez eta dena zitzaidan hautsi.

Maiteenetan izar
argitan argiena,
mendebaldean nabar
haizerik epelena.
Bortuetan itxas adar
uzta emankorrena.
Itzazu besoak zabal
har nazazu den dena.
Bizitzaren eztaietan
noizean itzala, noizean argi.
Itxaropenik airetan
ez uka galdetzen, ote den bizi.
Tristeziaren malkoak
baratzeko lorez xukatu beti.
Maite iturri gozoa
lehortu ez dadin ametsez busti.

Udaberriak zuregan
hartutzen du atsegin.
Arrano xuriak hegan
sorgindurik zurekin.
Pazko loreen dizdira,
gorteetan erregin.
Zure besoeen kabira
maite, nahi nuke hurbil.
Adiorikan gabeko zelata eguna
nabarmentzean,
piztu zazu zoriona,
etzaite erori hodei beltzetan.
Amoranteen zalduna
loratuko dugu bien artean
bataiatzeko iluna
dedin argi berri lur zabalean.