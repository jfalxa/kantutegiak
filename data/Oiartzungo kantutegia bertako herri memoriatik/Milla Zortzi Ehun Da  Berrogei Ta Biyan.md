---
id: ab-4367
izenburua: Milla Zortzi Ehun Da / Berrogei Ta Biyan
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004367.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004367.MID
youtube: null
---

Milla zortzi ehun da
berrogeita ta biyan,
penak esplikatzera
orain naiz abiyan.
Bi milla ta gehiago
legua bidian,
gu saldu gaituztenak
honela tranpiyan,
ez daude sarturikan
zeruko loriyan.