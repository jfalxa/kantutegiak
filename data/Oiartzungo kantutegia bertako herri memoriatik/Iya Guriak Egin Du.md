---
id: ab-4323
izenburua: Iya Guriak Egin Du
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004323.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004323.MID
youtube: null
---

Iya guriak egindu,
badegu zeinek agindu.
Ez oraindik umildu,
elkarrengana bildu;
gerra nahi duen guziya,
berari kendu biziya.