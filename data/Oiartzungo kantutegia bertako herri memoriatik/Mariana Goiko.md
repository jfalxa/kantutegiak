---
id: ab-4360
izenburua: Mariana Goiko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004360.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004360.MID
youtube: null
---

- Mariana goiko!
- Zer dun beheko?
- Goazeman iturrira.
- Ez zeukanet betarik.
- Zer dun, bada?
- Senarra etorri.
- Zer ekarri?
- Zapata ta galtzerdi.
- Hire senarra ta nere senarra
haserretu în omen-ttun.
- Zeren gaiñetik?
- Aza-zorten baten gaiñetik.
- Hiriak dizkin kulpak
eta kikirriki.