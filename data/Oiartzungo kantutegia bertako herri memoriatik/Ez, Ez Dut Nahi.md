---
id: ab-4296
izenburua: Ez, Ez Dut Nahi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004296.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004296.MID
youtube: null
---

Ez, ez dut nahi
ez, ez, ez,
Ez holako
zibilizaziorik.

Kaiolaren adarrak
ditugu gogoan.
Eskuindarrek eskua
zabaldu juxtuan.
Zigorren indarren menpe
tiroak gogoan.
Horrela bizitzerik
ez dago munduan.

Errep.:
Ez, ez dut nahi...

Polizia nun-nahitik
zerbait egitean,
pentsatu arazi nahi
makil indarpean,
Irudi du gerala
kaiola batean
jarritako txoriak
fusilen menpean.

Errep.:
Ez, ez dut nahi...

Horregatik ez dut nahi
gizonen kutsurik,
zibilizazioaren
sasi jaungoikorik.
Libre nahi dut bihotzez,
libre loturetik,
basurdearen gisa
hortzak estuturik.

Errep.:
Ez, ez dut nahi...