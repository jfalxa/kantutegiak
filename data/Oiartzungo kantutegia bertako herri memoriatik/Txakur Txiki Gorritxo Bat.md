---
id: ab-4431
izenburua: Txakur Txiki Gorritxo Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004431.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004431.MID
youtube: null
---

Txakur txiki gorritxo bat
faltatu zait neri,
arras galdu zan eta
ez da iñon ageri;
ez diot maldiziorik
bota nai iñori,
artaz baliatzen dana
ongi bizi bedi.

Manterolako errota
Elorditik bera,
auzo legian edozein
allegatzen gera;
lendabizi etorri zait
neronen etxera,
gezurrezko paperakin
arraultzak biltzera.

Manterolan errotari
gizon abilla da,
nere txakur txiki ori
ark berak illa da;
azeria duela
or ibillia da,
zerbait bildu naia badu
ark familiyara.

Amoriyomentian
dizut barkatuko,
ez dakit aurrera nola
zeran portatuko;
gure artian orrenbeste
balitz gertatuko,
ez giñake zu eta ni
zuzen dantzatuko.

Aiako erri noblea
pasatu orduko,
guziok eman ezkero
pranko du bilduko;
gaztañ edo arraultzak,
txanpon eta kuarto
aurtengo bastimentua
badauka zierto.

Nere txakur txiki leial
salati klarua,
azeri kolorekua
illajez arrua;
arekin ibilli danak
ona du garbua,
itxu aurreko balebil
a zer pajarua.