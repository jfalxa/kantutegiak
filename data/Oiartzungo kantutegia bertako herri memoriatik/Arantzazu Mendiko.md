---
id: ab-4235
izenburua: Arantzazu Mendiko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004235.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004235.MID
youtube: null
---

Arantzazu mendiko
paguaren ondoan.
Mariya Santisima
nedukan goguan.

Laran-laran-laran-lan,
laran-laran-laran-lan,
laran-laran-laran-lan,
laran-lan-lan-lan.