---
id: ab-4227
izenburua: Amak Egin Ninduen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004227.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004227.MID
youtube: null
---

Amak egin ninduen
ume bihurriya;
ederki pagatzen det
neure txarkeriya.
Hiruna duro multa
neskaren zirriya;
kamabostna errial
kartzela sariya.