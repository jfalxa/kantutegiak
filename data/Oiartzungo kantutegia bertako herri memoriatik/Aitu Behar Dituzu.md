---
id: ab-4219
izenburua: Aitu Behar Dituzu
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004219.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004219.MID
youtube: null
---

Txirrita:
Aitu behar dituzu
nere abisuak,
hauek izango dira
gauza prezisuak:
jakiñaren gañian
jarri gurasuak,
bestela ez du baliyo
nere desiuak.

Martzelina:
Ama ta aitarekin
izaten naiz ondo
hoientzat eskusarik
batere ez dago;
jakiñaren gañian
dirade lenago,
libertadia artuta
aspaldiyan nago.