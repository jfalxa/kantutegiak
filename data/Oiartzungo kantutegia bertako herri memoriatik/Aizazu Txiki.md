---
id: ab-4223
izenburua: Aizazu Txiki
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004223.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004223.MID
youtube: null
---

Aizazu txiki;
aizazu, maite:
Nundik bera dagon
Portugalete?

Portugalete
"Vino clarete"...
Erango nuke, bai,
baso bat bete.

Baso txikiya baño
haundiya nayago...
Tabernariyarekin
aserre nago.