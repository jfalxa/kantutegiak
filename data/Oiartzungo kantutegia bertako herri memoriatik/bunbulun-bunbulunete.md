---
id: ab-4268
izenburua: Bunbulun-Bunbulunete
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004268.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004268.MID
youtube: null
---

Bunbulun-bunbulunete,
botikan aguardiente,
bunbulun bat eta bunbulun bi
bunbulun! putzura erori;
eran zittuen pitxar bi...
ez zen orduan egarri.

Looo...