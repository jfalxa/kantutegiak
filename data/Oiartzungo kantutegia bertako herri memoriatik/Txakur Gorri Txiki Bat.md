---
id: ab-4430
izenburua: Txakur Gorri Txiki Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004430.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004430.MID
youtube: null
---

Txakur gorri txiki bat faltatu zait neri
erraz eskutatu da eta ez da inon ageri.
Ez diot maldeziorik bota nahi inori,
hartaz baliatu dana ondo bizi bedi.

Manterolako errotan gizon abila da
nere txakur gorria harek berak hila da.
Bere horrekin eskean hor ibilia da,
zerbait bildu nahi du hark bere ondora.

Aiako herri noblia pasatu orduko
guztiek emanez gero asko du bilduko.
Gaztain edo arraultzak txanpon eta kuarto,
aurtengo bastimendua badauka zierto.

Nere txakur txiki lehial salati klarua
azeri koloreko ilajez arrua.
Berarekin ibili danak ona du garbua,
itsu-aurreko balebil a zer pajarua.