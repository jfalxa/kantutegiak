---
id: ab-4216
izenburua: Aita Gurea
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004216.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004216.MID
youtube: null
---

Aita gurea, zeruetan zaudena;
santifikatua izan bedi zure izena;
betor gugana zure Erreinua;
egin bedi zure borondatea
nola zeruan hala lurrean.
Eman iguzu gure eguneroko ogia;
eta barka izkiguzu gure zorrak
guk ere zordunei barkatzen diegun bezela;
eta ez gaitzazula utzi tentazioan erortzen,
baizikan libratu gaitzatzu gaitzetik. Amen.