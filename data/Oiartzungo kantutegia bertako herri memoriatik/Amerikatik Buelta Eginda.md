---
id: ab-4229
izenburua: Amerikatik Buelta Eginda
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004229.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004229.MID
youtube: null
---

Juan Kruzek
Aldaera:
Amerikatik buelta eginda
Honuntzian Burdeosen,
Eperragandik ango fondetan
propiña ederrik joan zen;
etxera etorri, sillan exeri,
eztu dirurik azaltzen,
jango bazuen aizkora artuta
ikaskitera joan zen.

Aldaera:
Amerikatik buelta egin da
"ontzean" Burdeosen,
hango fondetan Eperragandik
propina ederrik omen zen.
Handik etxera etorri eta
dirurikan ez omen zen.
Jango bazuen aizkora hartuta
ikaskitera juan zen.

Eperrak
Errotaria asi omen da
agure zarrak mendratzen,
diruan galik ez daukat bañan
badakit itzez pagatzen;
oriek orla baldin badira
egiñik ez det ukatzen,
lau probintziyak jira ta alare
andrerik ez dek topatzen.

Juan Kruzek:
Lau probintziyak jira ta alare
andrerik ez da neretzat,
nunbait e berak desiako nau
sui izatia beretzat;
orren alaba zarrena ori
neri asko gustatzen zat,
orpuak eta gerri-aldiak
lirañak zeduzkak behintzat.

Eperrak
Neskatxa eder paregabiak
Maria eta Kontxezi,
oik kortejatzen errotariak
etzuen oraintxen asi;
ustez disimuluan ibilli,
gero jendiak ikasi,
uso politak eskuetatik
aixa joan zaizka igesi.