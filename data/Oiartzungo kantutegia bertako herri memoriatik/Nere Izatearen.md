---
id: ab-4384
izenburua: Nere Izatearen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004384.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004384.MID
youtube: null
---

Nere izatearen ordu ilunenak maite ditut orain
haiek zirelako kontzientziaren ingude garratza,
itzaltzear delarik zentzu iratzarrien lorezko baratza,
ezin baitut hazia, gatzak estali zuen, lur antzuan erein.

Zuhaitz heldua naiz hilobiari itzal isil dagiona,
menturazko ametsa gazte batek noizpait abesti egina:
atsedena balitz ausaz iraunkorra eta lotan grina
bere soseguan argitsu litzateke lurrezko gizona.

Itzuliko dira exaltazio baten elikagai zaharrak,
uda ederrarekin kantak asmatutako irudi galkorrak:
gorputzak, irriak, mahastien urrea, pozaren abarrak.

Ez du idatziko liburu zaharren mutu eta gorrak
zorabio batetan nahasten direlarik begi hain nabarrak,
azken arnasarekin ordainduko ditugu aintzinako zorrak.