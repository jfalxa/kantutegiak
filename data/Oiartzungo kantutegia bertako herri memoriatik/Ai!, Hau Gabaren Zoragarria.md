---
id: ab-4212
izenburua: Ai!, Hau Gabaren Zoragarria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004212.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004212.MID
youtube: null
---

Ai! Hau gabaren zoragarria!
Jesus jaio da Belenen.
Etxe onetan sartu ote dan,
bila gabiltza beraren.