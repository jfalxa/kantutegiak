---
id: ab-4239
izenburua: Ari Naizela
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004239.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004239.MID
youtube: null
---

Ari naizela, ari naizela,
nekatu zaizkit ezpaiñak.
Etxe hontatik espero ditut,
sei errial ta gaztaiñak.