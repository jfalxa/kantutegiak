---
id: ab-4279
izenburua: Egunaren Izena
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004279.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004279.MID
youtube: null
---

Egunaren izena
ez dakit nola zan,
baina señalagarri
partidu bat bazan,
pelotan jokatzeko
Oiartzungo plazan.
Nahi det nere orduko
gertaera esan,
hurrengoan beste bat
eskarmenta deza.