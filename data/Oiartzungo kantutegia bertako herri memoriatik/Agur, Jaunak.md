---
id: ab-4211
izenburua: Agur, Jaunak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004211.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004211.MID
youtube: null
---

Agur, jaunak; jaunak, agur,
agur t'erdi! (bis).

Denak Jainkoak, iñak gire,
zuek eta bai gu ere.

Agur, jaunak, agur.
agur t'erdi, hemen gire.