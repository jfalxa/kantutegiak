---
id: ab-4335
izenburua: Jeupa, Jeupa Irrintzika
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004335.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004335.MID
youtube: null
---

Jeupa, jeupa! Irrintzika
iratzebailleak herritikan;
jeupa, jeupa! Irrintzika,
zatotik ttantta bat egiñikan.

Goizean goiz, aizkaturikan
atso ta agureak leyotikan
galdezka daude arriturikan
zein ote-datorren herritikan,
herritikan.

Sasoi onaren jabeak,
napar beltzaren zaleak,
naiz igitayan,
naiz belagayan
iñun berdiñik gabeak,
ristiti-rrasta, bristiti-brasta (igitai-keñuak egiñaz)
gu gera iratzebailleak.