---
id: ab-4312
izenburua: Hemen Gehienak Bazenekiten
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004312.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004312.MID
youtube: null
---

Hemen gehienak bazenekiten
ni nola neguan eri,
orregatikan bastoi eder au
eman dirazute neri;
au ikusi nai duenak orain
ondo beiratu berari,
erromatarrak bentzitutako
lizar makila diruri.

Irurogei ta amasei urte,
garaia det umiltzeko,
bultza bearrik ez nadukake
goitik bera amiltzeko,
bi makil oiek aski nitun nik
munduz mundu ibiltzeko;
irugarrena andregaiari,
arkakusuak iltzeko.