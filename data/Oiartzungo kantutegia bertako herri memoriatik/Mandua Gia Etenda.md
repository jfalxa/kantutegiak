---
id: ab-4356
izenburua: Mandua Gia Etenda
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004356.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004356.MID
youtube: null
---

Mandua giya etenda
astua lehen hilla,
karretero haundi hori
gelditu duk iya;
oihu eta karraxi
errekatik hontzak,
lurrera botatzen du
euli baten hotsak.