---
id: ab-4343
izenburua: Kantatutzera Noa / Zelebrekeriak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004343.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004343.MID
youtube: null
---

Kantatutzera noa
zelebrekeriak, (bis)
ez dira onak baina
bai parregarriak
berri-berriak
neskatxa gazte batek
beretzat jarriak.

Galai bat etorri zait
larri eta eztu, (bis)
ohiaren basterrian
egiteko leku,
xirri eta muxu.
Traje baten neurria
nahi zidala hartu.

Onian ezin eta
gero gogorrian, (bis)
Neurtu nahi ninduela
luze laburrian,
ohiaren aurrian.
Orinalez jo nuen
kopeta-hezurrian.

Errukaldu nitzaion
t'egin nion kura, (bis)
etxian zen botika:
gatza eta ura.
Joxe Bentura;
ez gehiago etorri
nere konbentura.

Egin badizut ere
xirri edo mirri, (bis)
nere kopeta-hezurrak
pagatu du ongi.
Josepa Anttoni,
hartzekuak hartuta
etxera noa ni.