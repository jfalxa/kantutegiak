---
id: ab-4305
izenburua: Gure Gusto, Gure Atsegin
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004305.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004305.MID
youtube: null
---

Gure gusto, gure atsegin,
gure Jesus maitea,
zere bihotzeko suan
erre zazu gurea.

Gure hobenak, gure lohiak
lenik kendu behar dira.
Ura rara, ken itzazu,
galdu gara bestela.

Badakigu zaudela
zu gure amorez betea,
zere bihotzeko suan
erre zazu gure.