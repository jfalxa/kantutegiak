---
id: ab-4306
izenburua: Gure Sermolariyak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004306.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004306.MID
youtube: null
---

Gure sermolariyak
kulpitotik dakar,
gorrotorikan gabe
maitatzeko alkar.
Zerurako bidea
egongo da mehar;
akordatu giñezke
egun edo bihar.