---
id: ab-4379
izenburua: Nagusia Dugu Londresen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004379.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004379.MID
youtube: null
---

Azkuek
Nagusi jauna Londresen
zingulun-belarrak biltzen.
Hura handik etorri arte
gu biok dantza gitezen.
Oi, ai, egia!
uztar ingulun Maria.

M.Lekuonak
Nagusiya dugu Londresen
senda-belarrak biltzen;
Hark ez daki gu nola gabiltzen
ta, goazen, dantza gaitezen.
Ai, oi, egiya!
Uxtan ringulun Mariya.