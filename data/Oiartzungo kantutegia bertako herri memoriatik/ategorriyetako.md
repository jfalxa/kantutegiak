---
id: ab-4242
izenburua: Ategorriyetako
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004242.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004242.MID
youtube: null
---

Ategorriyetako
Danboliterua:
bizkarra makurtuta
jo zazu soiñua.

Ay que la vi,
que la vi, que la vi!

Ay que la vi,
que la vi, que la vi!

(Eranskinaren 2. aldaera):
Ay, que la vi,
que la vi, que la vi!
Como era de noche
no la conocí.
Ella lloraba,
le dije que sí:
no llores morena,
que soy para ti.