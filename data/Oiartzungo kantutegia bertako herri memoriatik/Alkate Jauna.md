---
id: ab-4225
izenburua: Alkate Jauna
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Alkate jauna, konparaziño:
Zu izan baziña urruaziño.
In izan balizu kozka-mozka.
Hasi izan baziña salto ta rota,
Hautsi bazenitu ehuntamar eltze...
Haik zeñek pagatuko zitun.