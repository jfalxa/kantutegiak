---
id: ab-4315
izenburua: Hiltzaile Bat Bezela
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004315.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004315.MID
youtube: null
---

Hiltzaile bat bezela
hemen naramate;
bi eskuak loturik
mundutik aparte.
Errurik ez dudala
azaldu nahi nuke.
Egia nahi dunântzat
emana dut merke.

Egia esan ta
zigorrez erantzun.
Gure bizkar moretan
diteke irakur.
Egia utzi eta
obe'te da entzun?
Munduak esandako
zortzi mila gezur.

Nola nahi dela, ere,
egirik ez utzi.
Nahiz zigorrez laztandu
zure gorputzari.
Gure eginkizuna
eman munduari.
Egiak gihartzen dun
indar berri hori.