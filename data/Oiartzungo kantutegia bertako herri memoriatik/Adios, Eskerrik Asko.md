---
id: ab-4207
izenburua: Adios, Eskerrik Asko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004207.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004207.MID
youtube: null
---

Adios, eskerrik asko,
adios, etxekuak;
hurrengo urte arte
hemendik baguaz.