---
id: ab-4241
izenburua: Artzai Buru-Xuri Bi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004241.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004241.MID
youtube: null
---

Artzai buru-xuri bi
Anton eta Peru
Belengo Portalera
etorri zaizkigu.
Sartu dira barrena
Manueltxo'gana:
Presente egin diyote
arkumetxo bana.

Batek arkumia ta
besteak ardiya:
ongi konpontzen dira
Jose ta Maria.

Aingeruak esan du
hor goiko mendiyan,
Jesus jaio zaigula
gabaren erdiyan.
Gloriya zeruan ta
pakia lurrian
borondate oneko
gizonen artian.