---
id: ab-4298
izenburua: Ezagutzen Dut Gizon Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004298.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004298.MID
youtube: null
---

Ezagutzen dut gizon bat
kale kantoian jarrita
ipuin harrigarriak kontatzen dituena
txanpon baten truke.

Dragoia eta printzesaren
ezinezko maitasuna
zahartutako basoko loti ederraren
azken egunak,
printze urdinak abandonatuta
Txanogorritxuren dibortzioa
otsoaren suizidioa.

Jendea presaka dabil ordea
entzuteko astirik ez dauka
aspaldiko garaietan, urrutiko
lurraldetan
gertatutako ixtorioak.