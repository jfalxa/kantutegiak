---
id: ab-4213
izenburua: Ai, Martin Zaharra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004213.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004213.MID
youtube: null
---

Ai! Martin Zaharra! Barkatu;
zuk aitzakiak dituzu.
Zure alaba hori neretzat
hazi ez baldin baduzu,
oraindainoko pausu neriak
pagatu beharko dizkidazu.
Hortik ibili, handik ibili
Maria Fandango;
konposiziorikan ez duzu
nerekin izango.
Ai! Oi!
Martzial zikina!
Aurten zizarra merke duk eta
egin ahalegina.

Zure lurrian ereinia da
berberiñaren haziya;
jaiotzen bada egingo diyot
altzairu finez hesiya;
altzairu finez hesiya eta
urre galoizko jantziya.
Hortik ibili, handik ibili...

Gau eta egun beti hemen nabil
etxe hontara puskatzen.
Desengainua izango ote dan
beldurra dizut gastatzen.
Martin Zaharrari alaba baizik
behinere ez diot eskatzen
Hortik ibili, handik ibili...