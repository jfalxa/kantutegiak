---
id: ab-4415
izenburua: Santa Maria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004415.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004415.MID
youtube: null
---

Santa Maria,
Jaungoikoaren Ama
erregutu ezazu gu pekatariogaitik,
orain eta gure heriotzeko orduan.
Amen, Jesús.