---
id: ab-4389
izenburua: Neskatxa Eder, Balientia
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004389.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004389.MID
youtube: null
---

Neskatxa eder balientia
hartu nahi baldin badezu,
etorri zaitez Oiartzuna ta
hantxen billatuko dezu;
kale eta baserri,
kolorez zuri gorri,
ederra franko badezu,
San Estebanez erromeriyan
ikusi nahi badituzu.