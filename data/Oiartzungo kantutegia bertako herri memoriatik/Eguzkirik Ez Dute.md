---
id: ab-4282
izenburua: Eguzkirik Ez Dute
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004282.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004282.MID
youtube: null
---

Biotzean min dut, min etsia,
negar ixilla darion miña.
Amonatxo bat nun: gaur enarak
etorri-garaiez, aldegiña.

Eguzkirik ez dute
leiotan zapiak;
aur yolasik ez dago
gaur zure kalean.
Odeiak lits urratu;
itsasoak orro.
Gizadi bat geldirik;
gero, bat batean:
Ots!
Ots!
bizion oñok!...
bizion oñok!...

Errezka bi gizonek
argizai oriak;
gorputza lau billobok;
atzetik andreak.
Ai, ene lor arin au
zein dudan larria!
besoek ez lan arren,
biotzak nekeak...
Ots!
Ots!
bizion oñok!...

Mendea ia betea,
ta ain illaun, gorputz xar!
Bezak gogoan goien
i bezain ariña,
ta bezat nik nerea,
dei nazatenean,
ezin-itzal argiruntz
egatzeko diña!
Ots!
Ots!
bizion oñok!...

Orain larogei urte,
kale poz gabeok
eguztiak zitun ta
zure aur-yolasak
alaitzen. Gaur arriak
ere baitira itzal...
nork lekuskez, ikusi,
urte urdin igesak!
Ots!
Ots!
bizion oñok!...

Egun zarrak joan ta
berriak etorri,
gaur poz bihar oñaze
noiz burni noiz zillar,
zenbaitek ondamendi
baituten katea
lore-sorta bekizu
goi muñoak zear!
Ots!
Ots!
bizion oñok!...

Bat batean eten da
bizion ots ori.
Gorputza laga dugu
nunbait. Al-dakit nun!
Bultza naute barrura,
ñir-ñir aunitz argi...;
ordia-antzo sar nauzu,
begi-lauso ta urgun.
Ots!
Ots!
enegan biotz,
bizion oñok!...

Amonatxo! Bakarrik
zagoku atean,
otz ta gogor, entzuten
zure azken meza,
zerraldo olen artetik
(negu-gaua iduri)
ez al datortzu, uluka,
aiza erruki-eza?
Ots!
Ots!
enegan biotz!...

Begiok ero ditut,
noranai doazkit;
dantza naasi bat dabilt
irudimenean.
Goruntz egik, biotza:
otoika lasa adi!
ixilla sor badadi
ire barrenean!
Ots!
Ots!
enegan biotz!...

Ordu bete luzea
igaro zan. Noizbait,
berriro lor illauna
besoek. Urrena
muño baten gañean
uri ixil bat arki...
Poz oñaze kateak
emen dik etena.
Ots!
Ots!
bizion oñok!...

Ol bat yaso ta ikusi
dut musu ximela:
parre-antxa bizirik
balirau bezela.
Ots!
Ots!
ler adi, biotz!

Ordun, ixar bat biztu
du nere gau beltzak.
Begioi malko ixil bat,
dardarka darite.
Noizbait itxaro-kabi
biur dan biotzak:
Ots!
Ots!
jauzika, biotz!

Agur! (oiuka dio),
gorputz ximur maite!
Agur, amonatxoa!
Egun Aundirarte!...

Biotzean min dut, min etsia,
zotiñik gabeko negar-miña.
Alatsu txoriak, uda-ondoz,
negua du galazi-eziña.