---
id: ab-6057
izenburua: Ezkondu Eta Ezkongai (2.Aldaera)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Ezkondu eta ezkongai,
guztiok ondo bizi nahi;
dirua degunian jai.
Familiako buruzariak
kontu gehiago ere bai;
len ardua edaten lasai,
orain urrutitik usai;
etxean gaude andre zai.