---
id: ab-4248
izenburua: Barku Zarrian Jon
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004248.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004248.MID
youtube: null
---

Barku zaharrian jon;
berriyan etorri:
nere nobiyuaren
suertia den hori!

Nere nobiyuaren ama,
asto baten jabe dana:
hartxek ekarriko zigun
etxean behar degun dana.