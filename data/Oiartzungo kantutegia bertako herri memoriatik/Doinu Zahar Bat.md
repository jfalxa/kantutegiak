---
id: ab-4273
izenburua: Doinu Zahar Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004273.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004273.MID
youtube: null
---

Doinu zahar baten inguruz dator
euskal kantuen mezua.
Hitz eta doinu bi-diak dute
osatzen gure salmua.
Herri kantuen aide artean
ikusi nuen mundua.
Ipuiak ziren edo kondaira
edo ta fruitu santua.
Haien oihartzun eraginzalez
erein zidaten barrua.

Geroztik erne ta hazi zitaidan
lore gorri bat ariman.
Hemen daramat, hemen bizi dut,
ertetzen zaidan kantuan.
Gerrako garrak ez zuten erre
Gernika kiskarratuan.
Izotzak arren, bizirik dago
ondoko giro nahastuan.
Gogorragoa giroa eta
gorriagoa orduan.