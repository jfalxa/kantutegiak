---
id: ab-4373
izenburua: Milla Zortzireun Hirurogeita / Zortzigarrengo Urtian
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004373.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004373.MID
youtube: null
---

Milla zortzireun irurogei ta
zortzigarrengo urtian,
alborotuak izan ditugu
españolaren artian;
denbora askuan izandu degu
doña Isabela kortian,
estimatua eduki dute
aspertu diran artian.

Urtian eta ogei ta zortzi
urriko illak zituan,
asko kostata zeukaten prenda
bialdu zuten orduan;
nagusi gabe arkitzen gera
lotsagarrizko moduan,
libertadea publikatuaz
berak nai duten lekuan.

Planta txarrian arkitu gera
orain daukagun klasian,
lasaikeriak aurreratzen ta
errespetua atzian;
pozoikeriai libertadea
eman ezkero etxian,
garbitasunak eztu iraungo
agitz denbora luzian.

Judio, ereje, protestantiak
beti dabiltza artian,
Jesu-Kristoren relijioa
ezin sufrituz pakian;
gurutzetxo bat eskribituta
parako zaie atian,
aginte gabe izan ditezen
katolikuen oinpian.

Irureun urte pasatua da,
eskrituratik badakit,
gobernu dana jarri zen ia
protestantien mendetik;
orduan ere euskaldunari
kosta zitzaion gogotik
belar gaiztuak ateratzia
Españiako lurretik.

Aita Santuak eskribitu du,
Españiyara goraintzi,
Santiyagoren erregaliaz
eztezagula gaitz etsi;
errespetua nai eztuena
irten dedilla igesi,
gure reinuan ezta tokatzen
libertadia nagusi.

Nagusi gabe guziok saltsan,
ai au reinoko modua!
Atoz, don Karlos, zuk biar zaitugu
jarri guzien burua:
gobernatu ongi paisanajia,
armadun eta klerua,
bizi dan arte kontserba ditzan
ezpata eta korua.

Gure don Karlos azaldu zaigu,
bazegon emen bearra,
illun gaudenak alaitutzeko
orienteko izarra;
nundik nora den deskubritu da
soka faltsuen amarra,
zure ezpatak ebakiko du,
etzaio faltako indarra.

Ama Birjiña Pillarekua
arturik geren amparo,
soka ori milla puska egiteko
indar eskasik ez dago;
aita eta seme irtengo gera
Karlos Borbonen soldado,
bandera beltzak jarri ezkaitzan
lege zikiñen esklabo.

Reinoz kanpora bota dituzte,
ordendu errenta gabiak,
oiek baño obeto estimatuko
al ditu beren Jabiak?
Teatro eta publikoak
or daukazkite jarriak,
dudarik gabe au eskatzen du
oiek nai duten legiak.

Onez-onian edo gaitzian
Karlosek bear du etorri,
lege garbi bat ipintzera
obedituaz berari;
al ditekien aguruena
obe litzake ekarri,
alborotorik eztedin sortu
lengo guziyon askarri.

Liberal buru gogorrekuak,
laja zazute kolera,
umildadian jarri zaitezte
Jesu-Kristoren legera;
guziok alkar artu dezagun,
atozte arrazoiera,
tokatzen zaion gizon justua
besterik baño obe da.

Estranjeroa errege biarrik
dabiltza beren artian,
zernai pikardi esan digute
auko zulua betian.
"Biba don Karlos!" eju egingo degu
euskaldun denak batian,
arrazoiari obedituaz
bizi gaitezen pakian.

Despedidako ipinitzen det
amalaugarren bertsua,
uste det zerbait esplikatzia
gertatzen zaigun kasua;
injuri asko sufritzen degu,
len dago ezin pasua,
beantaetsian bagera baño
kunplitu da ia plazua.