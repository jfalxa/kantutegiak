---
id: ab-4295
izenburua: Ez Egin Negar
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004295.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004295.MID
youtube: null
---

Ez egin negar, nik emango bai,
sagar bat ximel ximela,
amona bezin goxua
eta ximurra bera bezela.