---
id: ab-4366
izenburua: Milla Beatzirehunta / Hogei Ta Sei Urte
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004366.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004366.MID
youtube: null
---

Milla beatzirehunta
hogei ta sei urte;
bertsoak jarritzeko
enkargatu naute.
Preso sartu giñala
iya hillabete.
Zerutik aingeruak
laguntzen digute.

San Migel aingerua
gugatik erregu,
heriotzeko orduan
ez gaitezen galdu.
Jaunak ematen digu,
bai, zenbait kastigu.
Penak sufritutzeko
jaioak gera gu.