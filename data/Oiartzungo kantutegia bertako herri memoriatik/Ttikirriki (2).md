---
id: ab-4425
izenburua: Ttikirriki (2)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004425.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004425.MID
youtube: null
---

Ttikirriki-ttikirriki-ttiki-tton ki
- rriki-ttiki-ttikirriki-tton;
ttikirriki-ttikirriki-ttiki-tton ki
- rriki-ttiki-tton.
Ttikirriki-ttikirriki-ttiki-tton ki
- rriki-ttiki-ttiki-tton;
ttikirriki-ttikirriki-ttiki-tton ki
- rriki-ttiki-tton.