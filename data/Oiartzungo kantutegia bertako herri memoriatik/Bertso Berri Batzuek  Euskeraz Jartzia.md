---
id: ab-4257
izenburua: Bertso Berri Batzuek / Euskeraz Jartzia
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004257.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004257.MID
youtube: null
---

Bertso berri batzuek
euskeraz jartzia,
gustua det neki hau
neregan hartzia;
ezpanaiz suertatzen
gazterik hiltzia,
penagarriya daukat
munduko zahartzia.

Kantatutzera nua
milla sentimentu,
nere mingañak balu
ainbeste talentu;
bizi-modu berri bat
al baneza artu;
aurten senarrarekin
ez nabil kontentu.