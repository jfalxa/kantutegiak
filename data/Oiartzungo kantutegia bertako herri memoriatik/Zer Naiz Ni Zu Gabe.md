---
id: ab-4445
izenburua: Zer Naiz Ni Zu Gabe
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004445.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004445.MID
youtube: null
---

Errepika:
Zer naiz ni zu gabe,
maitasun nerea?
Zer naiz ni zu gabe?
Zurezko bihotza;
geldirik dagoen
erlojo tristea.
Zer naiz ni zu gabe?
ikara ta otza!

Zugandik dakit nik
mundua maitatzen
ta guztia dakust
gaur zure argitan.
Horla iturrian
ura da xurgatzen;
zeruko izarrak
pozez irakurtzen;
oihuka erantzun
oihuei menditan.
Zugandik dakit nik
bizitzen pozetan.

Zugandik dakit nik
ta daukat barruan:
eguardiz dela egun,
urdiña zeruan;
poza ez dagola
taberna-zuloan.
Eskutik hartuaz
gaurko inpernuan,
agertu diazu
maitasun beroa,
agertu diazu
luzatuz eskua.

Zoriona aitatuz
maiz triste begiak.
Naiz besterikan ez,
malkotxo galdua;
Hitz bitan esan-da,
gitar-garrasiak.
Baina dizdiz daude
zorion-izpiak.
Aitortzen dizutet,
onartuz mundua,
ez dela amets hutsa
maitasun-kantua.