---
id: ab-4378
izenburua: Nafarroa, Arragoa
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004378.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004378.MID
youtube: null
---

Nafarroa, arragoa, argiaren pausaleku
haizearen ibiltoki zabal, hesirik gabea.
Nafarroa, arragoa, sua eta ura,
kondaira urratu baten oihartzun oihukatua;
amets bat baino gehiago, ez oparotasun osoa,
nahiaren eta eziñaren burrukatoki ekaitzez betea.

Nafarroa, arragoa, mahats eta gari
maitasun eta gorroto, gezur eta egia gurea,
Nafarroa, arragoa, lur beroan sabel emankorra
eta goiko mendi hotzen mendetako loa.

Nafarroa, Nafarroa, zuhur eta eroa
lurrik maitatuena, hormarik gabe utzitako etxea,
erreka gardenen negarrez ibai lodietara
isurtzen ari den odol bustikorraren iturri bizia.

Nafarroa, arragoa, izen bat eta oinarri
eta oinaze, eta bidean zure billa ibiltzea...
piñudiek usaitzen dituzten
harritxigorrezko bidetan gora
oh, Nafarroa, Nafarroa, betikoa.