---
id: ab-4264
izenburua: Bortian Ahuzki
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004264.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004264.MID
youtube: null
---

Bortian ahuzki
urhunak osoki,
neskatilla eijerrak
han dire ageri.
Hirur badirade
oi bena xarmantik;
Nafarroa guztian
ez dute parerik.