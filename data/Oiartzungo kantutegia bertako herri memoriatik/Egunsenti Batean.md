---
id: ab-4280
izenburua: Egunsenti Batean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004280.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004280.MID
youtube: null
---

Egunsenti batean maitea zen hilla,
kalera irten nintzen Jainkoaren billa,
nerekin aurrez-aurre esplika zedilla,
behingoz utzi zezala zeru-goi ixilla.
Zutaz, maite, egiña dut galde.

Bakardade haundi bat neukan bihotzean,
negar egiten nuen zutaz oroitzean,
galdera egin nion Jaunari hotzean:
gizonok nora goaz mundutik hiltzean.
Zutaz, maite, egiña dut galde.

Orduak pasa ziren, ezer ez nun entzun,
nik ez dakit, maitea, konprenditzen duzun,
arratsaren illunak eguna bildu zun,
Jainkoak iñundikan etzidan erantzun.
Zutaz, maite, egiña dut galde.

Etxeruntz nindoala oihukatzen nuan:
Jainkoa ez da bizi gizonen onduan,
urrutik zegok hura han bere zeruan
gu bakarrik utzirik umezurtz munduan.
Zutaz, maite, egiña dut galde.
Zutaz, maite, galdera egin dut
baiña Jainkoak ez zidan erantzun.