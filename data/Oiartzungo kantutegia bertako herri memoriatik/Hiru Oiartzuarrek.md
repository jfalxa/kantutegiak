---
id: ab-4318
izenburua: Hiru Oiartzuarrek
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004318.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004318.MID
youtube: null
---

Hiru oiartzuarrek
Frantziako lauri,
desfio în die
nahi badute ari.
Laja derrobotiai,
heldu luziari;
plazan ageriko da
zein den pilotari.