---
id: ab-4228
izenburua: Amandre Santa Ines
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Amandre Santa Ines!
Bart egin dut amets;
ez dakit onez ala txarrez.
Iñ ez dezadan berriz
Itten dizut promes.