---
id: ab-4372
izenburua: Milla Zortzireun Hirurogeita / Hau Da Bostgarren Urtia
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004372.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004372.MID
youtube: null
---

Mila zortziehun hirurogeita
hau da bostgarren urtian,
dibertsiyo bat jartzera noa
gazte jendien artian.
Aszensio zen erromeri bat
ermita txiki batian,
bi damatxo han ikusi ditut
ezin pasarik atian.