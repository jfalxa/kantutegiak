---
id: ab-4236
izenburua: Arantzazura Nua-Nua
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004236.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004236.MID
youtube: null
---

Arantzazura nua-nua: (bis)
Ai! nere poza! (bis)
Hantxe ikusiko det
praile kaska-motxa.

Rau-rau-rau
dotia badet baina;

Rau-rau-rau
arduak galdu nau.