---
id: ab-4414
izenburua: San Nikolas Ttikiya
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004414.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004414.MID
youtube: null
---

San Nikolas Ttikiya,
gure ganbaran haziya.
- Gure ganbaran zer dago?
- Andre Juana Maria,
haren avemaria,
Dios te salve bakallo-jale,
bost eta sei hamaika;
txorixorikan ez baldin bada,
igual dela lukainka.