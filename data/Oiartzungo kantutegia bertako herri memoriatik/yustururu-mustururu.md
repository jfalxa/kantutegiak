---
id: ab-4441
izenburua: Yustururu-Mustururu
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004441.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004441.MID
youtube: null
---

Yustururu-mustururu,
Mariya Juan Piriri.
Trun-la-lan, lan-laran-lan
mukulun trat.
Peregrino poco pan celebrando.
"Bai Pernando Muño, Mikela Muño, Marzela Muño"
Santun Klert.