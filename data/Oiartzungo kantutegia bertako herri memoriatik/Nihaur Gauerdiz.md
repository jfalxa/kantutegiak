---
id: ab-4393
izenburua: Nihaur Gauerdiz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004393.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004393.MID
youtube: null
---

Nihaur gauerdiz nintzan barda,
bihotz hotzean elhur-min
amets bideetan galtzen nintzan
ez bazinan, maitea, jin,

ez bazinan arimaz heldu
haragiz nintzelarik lo.
Lotarik zure begi blüa
ezagun nizun eni so,
et'ezpainetan zure ezpaina,
Potha... Orduan atzarri naiz:
nihaur ninduzun berriz ere...
Zertako, ai, atzarri naiz.