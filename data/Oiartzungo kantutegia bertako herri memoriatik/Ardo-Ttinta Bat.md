---
id: ab-4238
izenburua: Ardo-Ttinta Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004238.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004238.MID
youtube: null
---

Ardo-ttinta bat eran dut biño
ez nago oraindik moxkorra;
Patrik-onduan somatzen nuen
bultua edo koxkorra;
Nor demoniñok sartu ote dit
toxaren ordez zatorra.