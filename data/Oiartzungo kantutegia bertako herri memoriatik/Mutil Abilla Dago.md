---
id: ab-4377
izenburua: Mutil Abilla Dago
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004377.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004377.MID
youtube: null
---

Mutil abilla dago
Tagati Altziko;
aziendikan zoluan
ez duela utziko.
Ezin atera badu ere;
ez duela etsiko;
gizon azkarra da-ta
gero egeriko.