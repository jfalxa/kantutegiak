---
id: ab-4442
izenburua: Zatozte Gurekin
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004442.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004442.MID
youtube: null
---

Zatozte gurekin
anai geranekin.
Zabal zagun gure atea
has gaitean maitasun bila.
Eskuak alkartu,
bihotzak uztartu.
Sortu dedin gure baitan
euskal lorea.

Esan dezagun, ez.
Ondo dakigunez,
gure artean sortzen baita
ostoporik luze eta latzena.
Begiak luzatu
dezagun asmatu.
Zein ote den guretzako
bide bakarra.

Gure bizikera
dezagun aukera,
zabal dedin denentzako
aspaldiko udaberria.
Eguzkiarekin,
osasunarekin
egingo da itsasoan
baretasuna.