---
id: ab-4205
izenburua: Abenduaren Zazpian (II)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Sartu ginaden Ziburun,
ustez ginadez seguru;
bi ezkongaiak ziren gazteak,
gu zaharrak ginan hiru,
irabazi nahiz zerbait diru:
kukuak makur yo digu.

Garda txiki bat gaskoina,
fier zebilen dragoina,
ez zuela estimatzen
hankapean gizona,
bizia ere gauz'ona
galdetu nion perdona.

Handik biharamonean,
Ama Birjina egunean,
Vintin argi zegoen ikusi nuen
ene karga bizkarrean,
aski umore onean
sartu zan aduanean.

Vintin garda begi-zuri,
afer ona egin dauk hori,
lehenago ere aditze da,
banedukan igarririk.
Zer mutikoa hintzan hi:
orain probatu haut ongi.