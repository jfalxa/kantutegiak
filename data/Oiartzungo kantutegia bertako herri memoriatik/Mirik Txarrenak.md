---
id: ab-4375
izenburua: Mirik Txarrenak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004375.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004375.MID
youtube: null
---

Mirik txarrenak izaten ditut
lupustela eta lapitza.
Su-egurretan onenak berriz
pagoa eta aritza...
Merindarekin hemen duk laister
Berinzaharreko "Zakitza".