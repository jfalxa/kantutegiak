---
id: ab-4386
izenburua: Nere Maitia, Jeiki Ta Jeiki
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004386.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004386.MID
youtube: null
---

- "Nere maitia, jeiki ta jeiki
ez al-zerade loz ase (bis)
Zure ondoren gabiltzan hauek
ez baigerade loale,
iriki zazu bortaine hori;
adiskideak gerade".

- "Ez dizut bada nik irikiko
gau ilunian aterik (bis)
Barrenen dagonak ez baidaki
kanpokoaren berririk.
Zatoz eguna argitzen danian
egongo gera elkarrekin".

- "Bihar goizian etorritzeko
urrun bizi naiz, maitia! (bis)
Txakurra sangaz hasitzen bada
nork egingo du pakia":
- "Harrekikoa nik egingo det
arren, etorri zaitia".

- "Hona etorri, dama gaztia,
esan zenidan bezela". (bis)
- "Ongi etorri, galai gaztia,
neronek nahi nuen horla,
Hitz bi gustora egin ditzagun
pasa zaitia salara".

Salara joan da kutxan eseri,
begiratuaz elkarri. (bis)
- "Inposible da ez zaituela
Ama Birjinak ekarri.
Beti-betiko hartu nazau
penaren kontsolagarri".