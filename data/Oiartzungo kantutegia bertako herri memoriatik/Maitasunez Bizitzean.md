---
id: ab-4351
izenburua: Maitasunez Bizitzean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004351.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004351.MID
youtube: null
---

Maitasunez bizitzean
ez da gerrikan gehiago,
egun ederrak aurrean
baina hilik gera egongo.
Dena da berri izango,
pakea munduan gidari,
soldaduak bertsolari
baina hilik gera egongo.

Bizitzaren gurpilean,
zamarikan astunena,
sufritu beharra zena
hartu gendun gure bizkarrean.

Bizitzaren gurpilean,
bizi obearen bila,
mundu hau zuzen dadila,
gu guztion penaren trukean.

Maitasunez bizitzean
ez da gerrarikan gehiago,
gizonen oroimenean
baina hilik gera egongo.
Gutaz dira oroituko,
gure maitasun bilaketaz,
agian gure penetaz
baina hilik gera egongo.

Maitasunez bizitzean...