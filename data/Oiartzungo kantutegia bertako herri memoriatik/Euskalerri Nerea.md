---
id: ab-4291
izenburua: Euskalerri Nerea
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004291.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004291.MID
youtube: null
---

Euskalerri nerea, ezin zaitut maite,
baiña nun biziko naiz zugandik aparte...
Anaien aurpegian begirada hotza:
ezpainetan iraina, harrizko bihotza.

Arraltsaldeak berez dakarren tristura
neretzat izaten da bakardade hura:
argirik gabe begiak, balio al du gure egiak?
Bide bat aukeratzean gelditu nahi nuke gure lurrean.

Erabaki baten billa zenbat urrats galdu
arbasoen aberria ez nuka nahi galdu...
arauaren beharrak lotzen dizkit indarrak,
gogoa zekentzen susmoz
iturri bizia agortzean.

Urruti joan ezkero berriz itzultzean
nolako etsipena nere bihotzean...
ekin dezagun aurrera, lehia hontan saiatuko gera.
Tristea etxerik eza, denok behar dugu haren babesa.

Egun bakoitzak baitakar bere zeregiña
ekintzetan gauzatzen da bizitzaren griña...
errespeta bizia elkarri merezia,
guziok berdiña gera:
hauskorrak, hilkorrak azkenean.