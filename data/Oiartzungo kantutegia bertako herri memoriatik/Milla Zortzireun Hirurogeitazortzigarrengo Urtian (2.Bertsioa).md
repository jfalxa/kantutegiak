---
id: ab-6058
izenburua: Milla Zortzireun Hirurogeita/Zortzigarrengo Urtian (2.Bertsioa)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Aita Santuak eskribitu du
Espainiara goraintzi.
Santiagoren erregaliaz
ez dezagula gaitzetsi.
Errespetua nahi ez duena
irten dedilla ihesi,
gure reinuan ez da tokatzen
libertadia nagusi.