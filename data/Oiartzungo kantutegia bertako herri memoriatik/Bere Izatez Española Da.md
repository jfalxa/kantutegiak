---
id: ab-4254
izenburua: Bere Izatez Española Da
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004254.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004254.MID
youtube: null
---

Bere izatez española da,
izena ere Martin du;
Enintzan ola esplikatuko
zierto espanu jakindu.
Anima garbira biltzeko,
azio txarra egin du;
Gurutzetxo bat emain niyoke,
nik biar banu agindu.