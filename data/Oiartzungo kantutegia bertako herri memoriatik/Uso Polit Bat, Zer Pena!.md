---
id: ab-4435
izenburua: Uso Polit Bat, Zer Pena!
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004435.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004435.MID
youtube: null
---

Uso polit bat zer pena!
ezer txarrik ez zuena;
bihotz oneko begi zabala
borondatia diena;
herrian zen ederrena,
arrosa zirudiena.

Ez zen kanpuan txoririk
hori zen bezain alairik;
orain tristura, kolore hilla
petxua penaz beterik,
begiak malkoz bustirik,
ez dakit duen besterik.