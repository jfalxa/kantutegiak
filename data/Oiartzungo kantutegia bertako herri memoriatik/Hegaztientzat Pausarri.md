---
id: ab-4311
izenburua: Hegaztientzat Pausarri
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004311.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004311.MID
youtube: null
---

Hegaztientzat pausarri,
arrainentzako uharri,
hegaztientzat pausarri;
ai ninduke lur maiteak
zerbaitetan lagungarri.

Maite dut eta banabil
maitea nola zerbitu;
urteak zehar lora ta
- maite dut eta banabil-
ipuinak dizkiot bildu.

Giroak landu egurrik
ekarri diot menditik;
uren oldeak luzaro
mizkatu hibai-harririk,
giroak landu egurrik.

Nekatzeraino ihardun dut
atsegin eta dolore;
nekatzeraino ihardun dut,
ukabilak erabili
ditut maitearen alde.

Maitearen amorez naiz
aritu kalerik kale,
herriaren amorez naiz;
jadetsi diot hiriko
zenbait oroigailu ere.

Badakit aski ez dena,
besterik behar duena,
badakit huskeri dena;
semearen falta baita
nire maitearen pena.

Bihotzean haurra eta
haurrik esnatu ezina;
bihotzean haurra eta,
gizalditakoa dela
Euskal Herriaren mina.