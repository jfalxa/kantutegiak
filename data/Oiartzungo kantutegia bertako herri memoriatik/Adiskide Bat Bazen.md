---
id: ab-4209
izenburua: Adiskide Bat Bazen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004209.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004209.MID
youtube: null
---

Adiskide bat bazen orotan bihotzbera,
poesiaren hegoek
sentimentuzko bertsoek antzaldatzen zutena.
Plazetako kantari bakardadez josia,
hitzen lihoa iruten
bere barnean irauten oiñazez ikasia.

Nun hago, zer larretan
Urepeleko artzaina,
mendi-hegaletan gora
oroitzapen den gerora
ihesetan joan hintzena...

Hesia urraturik libratu huen kanta,
lotura guztietatik
gorputzaren mugetatik aske sentitu nahirik...
azken hatsa huela bertsorik sakonena,
iñoiz esan ezin diren
estalitako egien oihurik bortitzena.