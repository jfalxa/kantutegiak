---
id: ab-4208
izenburua: Adios Izar Eder
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004208.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004208.MID
youtube: null
---

Adios izar eder,
adios izarra,
zu zera aingerua
munduan bakarra.
Izar baten uzteak
emoiten daut pena
zeren eta bait nuen
munduan maitena.