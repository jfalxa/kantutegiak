---
id: ab-4396
izenburua: Ohekide Zaidan Dama
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004396.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004396.MID
youtube: null
---

Errepika:
Ohekide zaidan
dama,
zuk non ote
ogei urte?

Maite jokoz,
hortik datoz:
begi goibel,
ezpain ubel;
beharrezko
musu asko;
maitagintza
hain murritza,
ilunbean,
nekepean,
bai ikaraz,
nai algaraz.

Bular astun
hortz-markadun,
jostagailu
gerta zaigu.
Gorputz oker,
ez da eder.
Ba dirudi
lehenari
ihesika
ta muzinka,
hartu dula
lan mardula.

Ez irririk!
Ez negarrik!
Zuen penak
gorde denak!
Gau hotzean
elkartzean
biok estu
zaigu piztu
gu biontzat,
pizgarritzat,
argi labur,
bero apur.