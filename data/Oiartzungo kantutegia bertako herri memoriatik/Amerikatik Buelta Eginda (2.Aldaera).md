---
id: ab-6052
izenburua: Amerikatik Buelta Eginda (2.Aldaera)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Amerikatik buelta eginda
"ontzean" Burdeosen,
fondetan Eperragandik
propina ederrik omen zen.
Handik etxera etorri eta
dirurikan ez omen zen.
Jango bazuen aizkora hartuta
ikaskitera juan zen.