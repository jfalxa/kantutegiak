---
id: ab-4342
izenburua: Kantatutzera Noa / Bertso Bat Edo Bi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004342.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004342.MID
youtube: null
---

Kantatutzera nua
bertso bat edo bi,
Kaskazurik jarriyak
bere buruari;
gustoik ez diot artzen
bizimoduari,
deskalabrua pranko
gertatu zait neri.

Kojo gelditu nintzan
txiki-txikitandik,
paretera iyo ta
erorita andik;
informe txarrak ditut
gaur askorengandik,
neretzat berri onik
ez dator iñundik.

Dama batek nai ziran
karga au arindu,
pretenditu ninduen,
ez nion agindu;
amets gaixtua pranko
geroztik egin du
gaixua urriki nuen
ni artu banindu.

Aitak ere nai zuen
biyok ezkontzia,
bañan etzan posible
ni ondo portatzia;
obe da zer datorren
konsideratzia,
nolanai artzekua
ez da gurutzia

Dama orren abisua
etorri zanian,
aitak nai zun ezkontzia
zuzen-zuzenian;
ez nintzan konformatu
aien esanian,
oraiñ ementxe nabil
gaztetasunian.

Txiki-txikitandikan
arro ta tunante,
kontrariyua pranko,
gutxi dut amante;
griña gaizto guziak
tiratzen dirate,
aitzurrak eta palak
mudatuko naute.

Parrandan ibiltzen naiz
bastante itxuski,
kargu artzen dirate
ez bat eta ez bi;
zierto dakidala
ibiltzen naiz gaizki,
jeniyo gaixtu onek
galduko nau noski.

Ez nau inkomoratzen
besteren lujuak,
deklaratzera nua
nere enojuak:
zurrutak tiratzen nau,
pixka bat jokuak,
atarramentu onik
ez dauka kojuak.

Kartzelan sartu naute
kulpa dedalako,
erraz ematen zaio
pobriari topo;
aberatsa izan banitz
ez niñuten joko,
gaixtua banaiz ere
pagatzen det bapo.

Mundu ontan bagera
zenbait gizon bakar,
ikusi nai ezikan
gabiltzanak alkar;
San Jose bezperakin
bazkaldu eta azkar;
botilla arduarekin
juan zitzaidan Gaspar.

Orra bertso berriyak
Kaskazurik para,
jendiak jakitia
komeni dana da;
nitzat gustatzen danik
iñor baldin bada,
Zurko'n bizi naiz eta
juan zaitezte ar.