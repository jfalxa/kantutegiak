---
id: ab-4269
izenburua: Burruntxali Xar
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004269.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004269.MID
youtube: null
---

Burruntxali zahar,
xarranbillote!
Pepari muturra jan diote.

Hiru gorontza,
gonarik ez;,
ezkondu nahi eta
majorik ez.