---
id: ab-4357
izenburua: Manix Donibanetik
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004357.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004357.MID
youtube: null
---

Manix Donibanetik
etxera etorrita,
Martina beltza ez zen
ez anitz kontenta.
Katainetik hartuta
alkipera bota,
lara-larai...

Manix gizarajoa
ez duk indar asko!
Hi baino hobe likek
zamerdi bat lasto.
Zertan ez diok hausi
sudur edo kasko,
la-larai...