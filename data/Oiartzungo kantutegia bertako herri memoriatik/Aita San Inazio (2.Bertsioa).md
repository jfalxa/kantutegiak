---
id: ab-6051
izenburua: Aita San Inazio (2.Bertsioa)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Aita San Inaziyo,
Azpeitiakua
Loyolan etxe eder
palaziyokua,
munduan gustable da
zure ejenplua;
zure alabantzan
ni kantatzera nua
basta da zaitudala
probintzianua.