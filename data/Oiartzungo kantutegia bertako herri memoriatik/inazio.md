---
id: ab-4326
izenburua: Inazio
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004326.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004326.MID
youtube: null
---

Inazio,
Palazio,
jan ta edan despacio;
ogiya purra-purra
Inazio lapurra.