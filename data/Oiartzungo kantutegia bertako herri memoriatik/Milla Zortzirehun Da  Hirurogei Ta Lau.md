---
id: ab-4369
izenburua: Milla Zortzirehun Da / Hirurogei Ta Lau
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004369.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004369.MID
youtube: null
---

Milla zortziregun da
irurogei ta lau,
Kristo mundura zala
orain dijuan au;
bertso bi paratzeko
kausak biartu nau,
orain abiatzen den
mingañak badirau.

Nik nai nituen beti
kontuak garbitu,
berreun bitartekokin
ez naute obeditu;
euskeraz esan eta
ondo konprenditu,
Santanasek animak
aixa eramain ditu.

Eramanak eraman,
bestiak geroko,
"mal pecado" bazion
orrelako franko;
nik egin dizkiñet ba
egiñalik asko,
"hermano" ta "hermana",
eskerrikan asko.

Sotera Mitxelena
da orren konduta,
onik batere ez dauka
gaiztua kenduta;
korrituan ipiñi du
neri arrapatuta,
ezin leike garbitu
lixiban sartuta.

Frantzisko Mitxelena
du sozidantia,
estimatu lezake
zerbait esatia;
zenbat familietan
egin du kaltia?
Au da asto guziyen
errematantia.