---
id: ab-4289
izenburua: Eskualdun Xasurretatik
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004289.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004289.MID
youtube: null
---

Eskualdun xasurretatik
baginen hirur batallun.
Hartu behar gintuela,
Tun, tun, tuntuluntun,
Irun eta OIartzun.
Tun, tun,
eta beti tuntuluntun.

Gerla behar ginuela
aspaldi zuan ezagun.
Itsaso bazter hoitarik,
tun, tun, tuntuluntun,
zonbait peza tiro ilun,
tun, tun,
eta beti tuntuluntun.