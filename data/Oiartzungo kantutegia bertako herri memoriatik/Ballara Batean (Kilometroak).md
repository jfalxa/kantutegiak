---
id: ab-4246
izenburua: Ballara Batean (Kilometroak)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004246.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004246.MID
youtube: null
---

Ballara batean, toki ezaguna,
euskararen deia izan da entzuna;
Euskal Herri osoan sendo erantzuna:
Oiartzunen euskarak badu oihartzuna.
Kilometroak, kilometroak
Kilometroak aurrera-pausoa.
Euskal Herri, entzun,
euskaraz gogor erantzun.