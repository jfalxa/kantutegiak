---
id: ab-4331
izenburua: Izarren Hautsa
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004331.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004331.MID
youtube: null
---

Izarren hautsa denboran zehar bilakatu zen bizigai
materiaren ordenamenduz noizpait ginen handik ernai,
eta horrela bizitzen gera sortuz ta sortuz gure aukera
atsedenik artu gabe: lanaren bidez goaz aurrera
kate horretan denok batera lurrari loturik gaude.

Gizakiak gu ingurune bat menperatzeko premia
borroka ortan bizi da etahori du bere egia,
ekin eta ekin bilatzen ditu, saiatze hortan ezin gelditu,
araua eta argia: bide berriak nekez aurkitu
bizilegeak minez erditu, hortan jokatuz bizia.

Gizonen lana jakintza dugu: ezagutuz aldatzea,
naturarekin bat izan eta harremanetan jartzea,
eta indarrak sakon errotuz, gure sustraiak lurrari lotuz
bertatikan irautea: ezaren gudaz baietza sortuz
ukazioa legetzat hartuz beti aurrera joatea.

Gu sortu ginen enbor beretik sortuko dira besteak,
heriotzaren ukasioa diren ardaska gazteak,
beren aukeren jabe eraikiz ta erortzean berriro jaikiz
ibiltzen joanen direnak: gertakizunen indar ta argiz
gure ametsa arrazoi garbiz egiztatuko dutenak.

Eta ametsa zilegiztatuz egiaren antziduri
herri zahar batek bide berritik ekingo dio urduri:
guztien lana guztien esku jasoko dugu zuzen ta prestu
gure bizitzen edergai: diru zakarrak bihotzik ez du
lotuko dugu gogor ta hestu haz ez dedin gizonen gain.