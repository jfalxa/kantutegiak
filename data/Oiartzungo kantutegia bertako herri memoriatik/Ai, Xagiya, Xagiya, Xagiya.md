---
id: ab-4214
izenburua: Ai, Xagiya, Xagiya, Xagiya
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004214.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004214.MID
youtube: null
---

Ai Xagiya! Xagiya! Xagiya!
Ai Xagiya panpoxa!
Tanta-tarata-tanta-tarata,
tan-tarata-tanta-ta.