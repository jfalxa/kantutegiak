---
id: ab-4412
izenburua: San Migel Aingerua
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004412.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004412.MID
youtube: null
---

San Migel Aingeruak
Goizuetan bisita;
Naparruan gelditu zen
zerutik jatxita.
Kontsolatu egiten naiz
esaten hasita,
bere plan ederrekua
hala ikusita.
Uste nuen zetorrela
zeruko musika.