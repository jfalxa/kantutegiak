---
id: ab-4440
izenburua: Xubiri-Xubiri
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004440.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004440.MID
youtube: null
---

- Xubiri-Xubiri, nongorik-nongo
alkatea zerade?
- Frantzia aldeko errege baten
seme-alabak gerade.
- Hurren-hurren zubi hontatik
pasatzen dena,
bertan geldituko dela.
Antzalako ta zerurako,
antzalako ta infernurako.