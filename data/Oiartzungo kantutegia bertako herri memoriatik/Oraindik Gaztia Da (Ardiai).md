---
id: ab-4400
izenburua: Oraindik Gaztia Da (Ardiai)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004400.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004400.MID
youtube: null
---

Oraindik gaztia da:
bederatzi urte;
ibilpiria galdu,
zorrua ezin bete;
etzanta egoten da
alegindu arte,
gero belaunikoka
barkaziyo eske.

Geiago ere baditut
orren modukuak,
ezurrez gizenak bañan
aragiz flakuak:
iñoiz tokatzen baira
pauso bi altuak.
amilka ta boteka
madarikatuak.

Askotan egoten naiz
neregan pentsatzen,
ardiyak ez diratela
bererik ematen:
berririkan azi ez ta
lengo zarrak iltzen,
aurten bapo ari naiz
putriak mazkatzen.