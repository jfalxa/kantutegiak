---
id: ab-4253
izenburua: Bera Dago
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004253.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004253.MID
youtube: null
---

Bera dago,
bakarrik dago;
bakar-bakarrik dago.