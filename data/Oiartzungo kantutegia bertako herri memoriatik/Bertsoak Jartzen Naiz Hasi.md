---
id: ab-4258
izenburua: Bertsoak Jartzen Naiz Hasi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004258.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004258.MID
youtube: null
---

Bertsoak jartzen naiz hasi,
kopiatuaz ta guzi
nahi dituenak ikasi;
Afrika aldera oriango aldian
gazteri haundiak doazi;
diote adi erazi
ez dela joan behar ihesi;
amak negarrez daudezi.