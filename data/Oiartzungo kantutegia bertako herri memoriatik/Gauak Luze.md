---
id: ab-4301
izenburua: Gauak Luze
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004301.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004301.MID
youtube: null
---

Gauak luze
bizitza otz
zoritxarrez
gaixo biotz.

Ori zergatik?
pobre izateagatik.

Zure irudia
eta ukatziak
itsutu naute
min eman arte.

Baina laister urrutira
ni banoa astutzera
baina laister urrutira
ni banoa sendatzera.

Amoriuan askatasuna
gorputzaren osasuna
amoriuan askatasuna
animaren alaitasuna.