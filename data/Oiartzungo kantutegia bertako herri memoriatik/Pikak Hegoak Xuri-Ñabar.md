---
id: ab-4405
izenburua: Pikak Hegoak Xuri-Ñabar
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004405.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004405.MID
youtube: null
---

Pikak hegoak xuri-ñabar, (bis)
amorantiak baditut hamar (bis)
Ni batek banarama bihar (bis)
pena duena ein beza nigar (bis)

Bortainoan eder giltza (bis)
amorantiak badabiltza (bis)
Nik badizut baten hitza (bis)
zeinek beriak xerkatu hitza (bis)

Itsasoa laino dago (bis)
Baionako barraraino (bis)
Maite zaitut, maitiago (bis)
txoriak bere umiak baino (bis.