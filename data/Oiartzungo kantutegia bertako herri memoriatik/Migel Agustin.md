---
id: ab-4364
izenburua: Migel Agustin
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004364.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004364.MID
youtube: null
---

Migel Agustin Izuela'ko
auspez leyuan jarririk,
burua ere makurtu zaizu
bakartasunez beterik:
almiraturik'oi nago zutaz,
ez dezula artzen andrerik.

Orain ordaña emango dizut
iñolaz ere al bada;
zer egingo det ba, Jose Joakin,
portunarikan ez bada?
Zure esperantzan egongo naiz, ta
azi zaidazu alaba...

Nere alabak portuna on bat
izan baleza munduan,
mandatu ori egingo diyot
etxera nuan orduan:
iya ezkontzeko pentsamenturik
ote dadukan buruan.

Jose Joakin, nere laguna;
esango dizut egiya
ni ezkontzekotan, izan bearko du
gazte dontzella garbiya,
ez, mundu ontan asko bezela,
len bestek erabiliya.

Migel Agustin, lagun maitia,
enkarguz ari natzaizu;
gazte dontzella mantxa gabia
baldin guztatzen bazaizu,
beste oyetaz ez naiz piyatzen;
seaskatikan artzazu.

Jose Joakin, lagun maitia,
zor dizut milla graziya;
seaskatikan etortzen al da
dontzellatasun guziya?
Igualian nayago det nik
amak besuan aziya.

Iru osaba Paris'en dauzkat
Bonaparte'ren serbitzen;
ayen semiak, zazpi lengusu,
Gobernadore Kadiz'en...
Dontzelltasuna zertaraño den
etzait neroni aditzen.

Bertsu au neri aditu arte
artu zazute deskantsu;
dontzeltasuna zertaraño den
zeuri aditzen ezpazaizu,
enpeño batez beartuta ere
parte aundiyak dauzkatzu.

Egiyatxo bat esango dizut,
kontsola dedin jendia.
Alperrik dezu serbitu naya
beste gizonen legia:
Migel Agustin, aguretuta
zertako dezu andria?

Bokable ori erantzuterik
Goguak ematen etzian;
alkandora zuri eder, katillu salda on
atsegin dira zartzian;
emekume onak serbitzu asko
izaten dizu etxian.

Gure etxian bi ollo dira;
bati egin zayo kiyua;
ollarra berriz kukurrukuka
tantayan gora iyua...
Eskerrik asko, jende leyalak;
basta da libertsiyua.