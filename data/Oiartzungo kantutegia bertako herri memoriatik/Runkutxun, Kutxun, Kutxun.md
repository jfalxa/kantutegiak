---
id: ab-4410
izenburua: Runkutxun, Kutxun, Kutxun
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004410.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004410.MID
youtube: null
---

Runkutxun, kutxun, kutxun, kutxun,
kutxun, kutxun, kutxun, kutxun, kutxun.
Runkutxun, kutxun, kutxun, kutxun,
kutxun, kutxun, kutxun, kutxun.
Runkutxun, kutxun, kutxun, kutxun,
kutxun, kutxun, kutxun, ku.
Runkutxun, kutxun, kutxun, kutxun,
kutxun, kutxun, kutxun.