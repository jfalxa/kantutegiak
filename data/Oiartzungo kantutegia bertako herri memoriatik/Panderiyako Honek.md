---
id: ab-4403
izenburua: Panderiyako Honek
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004403.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004403.MID
youtube: null
---

Panderiyako honek
memoriya erne;
Lexotik jartzen digu
zenbait umore;
txarrik ezin bota du
Ibarrek nahitare;
nik hoiekin ezin dut
luzitu batere;
Xorrola da trebe;
baita Bortondore;
Ardotx pare gabe;
Arotxa kantore;
Larraburu jartzen det
gobernadore.