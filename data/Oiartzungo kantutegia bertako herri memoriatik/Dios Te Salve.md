---
id: ab-4272
izenburua: Dios Te Salve
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004272.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004272.MID
youtube: null
---

Bedeinkatua, alabatua
Aldareko Sakramentua:
pekatuaren mantxarik gabe
zeña dan kontzebitua.

Hau haiziaren epela!
Airian dabil orbela;
etxe hontako jende leyalak,
gabon Jainkuak diyela.

Zapata txuri paperez;
euri denian baterez;
Nagusi Jauna, esan bizaigu
hasiko geran edo ez.

Gizon txiki bat golbero;
deitzen diote barbero;
lizentziyarik izan ezpalitz
esan ziguten gaurgero.

Lezo ta Errenteriya;
irugarrena Oiartzun;
nere lagunak, Dios te salve
garbuarekin erantzun.

Korua:
Dios te salve! Ongi etorri!
Gabon Jainkuak diyela!
Legiarekin konpli dezagun
Urteberriren sarrera.

Urteberriren bezpera da ta
eskian gatoz atera,
Ama Birjinan Seme garbiya
dijualako pontera.

Horroko hor goyan ermita,
San Gregorio deritza;
hango prailiak agindu eta
limosna biltzen gabiltza.

Esku txuritan papera;
mayian urrez platera;
lotsa gogorrez heldu gerade
Jauna, berorren atera.

Horroko hor goyan izarra;
errekaldian lizarra;
etxe ontako Nagusi Jaunak
urre gorriz du bizarra.

Urre gorrizko bizarra eta
zilar zurizko espalda;
errial bikoz egiña dauka,
elizarako galtzara.

Horroko hor goyan elorri,
onduan jo-ta-erori;
etxe hontako Nagusi Jaunak
erregidore dirudi.

Nagusi Jauna, barkatu
ezbanaz ongi mintzatu;
zure lizentziyarekin nai dut
Etxeko Andria koplatu.

Etxeko Andre zabala,
toki oneko alaba;
birian nator informaturik
emaile ona zerala.

Horroko hor goyan errota,
iriña dago iyo-ta;
etxe hontako etxeko Andria
Ama Birjiñan debota.

Horroko hor goyan oilo bi,
batek bestia iduri;
etxe hontako etxeko Andriak
Ama Birjiña diruri.

Horroko hor goyan antzara;
Etxeko seme non zara?
Gu zuregana etorri eta
inon ageri ez zara.

Etxeko Alaba non zara?
Iñon ageri ez zara...
Lau galai gazte ba'ditut eta
Jarriko dizut aukera;
neroni ere gayian nago
baldin gustatzen-ba-zara.

Horroko hor goyan otia,
guziya lorez betia;
Etxe hontako alabatxuak
kutxan daduka dotia.

Horroko haor goyanantzara;
Joxepantoni, non zara?
Mundu guziya ezkondu eta
zu zertako in ez zara?

Ari nazela, ari nazela
nekatu zaizkit ezpañak;
etxe hontatik espero ditut
errial bi ta gaztañak.

Emango bauzu emazu,
bestela ezetz esazu;
ate-onduan hotzak hiltzera
amak ez gaitu bialdu.

Eskupekua hartu degu ta
orain abia gaitian;
adiosikan ez degu eta
agur urrengo artian.