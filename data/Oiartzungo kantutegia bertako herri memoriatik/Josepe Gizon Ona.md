---
id: ab-4337
izenburua: Josepe Gizon Ona
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004337.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004337.MID
youtube: null
---

Josepe gizon ona:
arotza zera zu.
Haurtxo eder honeri
sehaska egiozu.

Josepe, ekar zazu
sortatxo bat egur;
haurra hotzak dagota
berotu dezagun.