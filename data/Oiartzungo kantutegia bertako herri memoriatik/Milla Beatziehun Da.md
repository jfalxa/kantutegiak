---
id: ab-4365
izenburua: Milla Beatziehun Da
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004365.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004365.MID
youtube: null
---

Milla beatzi eun da
berrogei ta seia,
bertsuak jarritzera
dijua artzaia;
guarda bat ikustia
neretzat etsaia,
al dedan zintzuena
egiten naiz saia.

Mundu ontan ogei ta
emeretzi urte,
ez ditut bete bañan
ez dira aparte;
ardiyan atzetikan
segitzeko fuerte,
desapiyoka oiek
iya ito naute.

Erri-lur guziyetan
piñua sartzera,
mendiko jende dana
dijua galtzera;
ortatik gauz onikan
ez dator etxera,
obligatu gaituzte
ardiyak saltzera.

Ardi bakoitzântzako
munta ogei pezta,
ez dago egiterik
iñon e protesta;
ortan deskuidatzia
milagrua ezta,
artaldia salduta
gero jan zak gazta!

Mendiyan bizi geran
guraso ta aurrak
ez gaitu mantenduko
piñuan ezkurrak;
ikaraturik gaude
gosiân beldurrak,
enkargatzera guaz
zurazko muturrak.

Ardi (y) oiek eztute
inportantzi gutxi,
akordatuko zaigu
bigar edo etzi;
eta orain zer egin
dute erakutsi:
atze aurriak biak
txikortakin itxi.

Aitzurra zorroztuta
arkaitzari golpe,
bera ezin sartuta
berriz bueltan bote;
or ez da landarerik
aunditutzen bate,
surangilla erriân,
isatsa diruite.

Landaretxo oiekin
daukaten gustua,
eoziñ egon laitake
oietaz poztua;
lautatik iru badu
beti diskustua,
bera igartuta're
ardiyak moztua.

Piñua aldatu zala
Aieko Arriyan
amairu urte dira
udaraberriyan;
tantiua emana daukat
nik bere neurriyan:
metro ta erdi luzian,
lau ontza loriyan.

Piñadi ori dago
Oiartzungo parten,
material ederrik
egingo da erten;
estranjeri aldera
baguazke aurten,
aber piño trunkorik
nai ote luteken.

Beti gauz bakarra
daukat mingañian:
oik ez dutela emango
ardiyak añian;
hamaika aritzen dek
alperrik lanian,
kosetxa eder gutxi
arriyân gañian.

Maiatzaren ogeian,
Oiartzungo erriyan,
orra amabi bertso
euskera garbiyan,
artzai batek jarriyak
bere errabiyan,
ez dakiyenik bada
entera dadiyan.