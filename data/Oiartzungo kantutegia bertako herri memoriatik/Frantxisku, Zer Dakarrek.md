---
id: ab-4300
izenburua: Frantxisku, Zer Dakarrek
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004300.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004300.MID
youtube: null
---

Nagusiak:
Frantxisku, zer dakarrek
hik, gure herritik?
Aspaldian ez diat
hango berririk.
Zerbait jakin nahi nikek
alderdi hartatik:
haserria niagok
ez dek milagrorik...
Ez diat egunik,
ez eta arratsik,
sinista nazak hik,
pentsamentuz etxera
juan gabetanik.

Morroiak:
Nere nagusi jauna,
etxeko berriak,
dira ezin gehiago
negargarriak:
ez ditu sinistuko
hango pikardiak,
arras galdurik daude
betiko herriak,
jende haundi-maundiak
nahiz nekazariak
txiki eta haundiak,
arraz galdurik dauka
ia miseriak.

Nagusiak:
Zorigaiztoz etorri
zaigun gerratia
oroitu beharra duk
ziñez jendia;
hau sortu zuenaren
deabruz betia,
nahiz dala apaiz eta
igual frailia,
Prantxisku neria,
duk askoz hobia
turkuen legia,
ez hoiek predikatzen
duten fedia.

Morroiak:
Gezurrezko itzakin
ez hala ustian,
txalma para digute,
jaunak, gaiñian:
jendiak uste zuen
haiek aditzian,
gerra bukatzen zala
biharamunian,
bien bitartian
goseak etxian
makilka lanian,
hantxen garabilzkite
bala tartian.

Nagusiak:
Pagu hori zenduten
ondo merezia,
jende txiak dik Praixku
kulpa guzia:
estimatu beharrian
maizterrak nagusia,
zerontzat nahi zenduten
gure baserria,
jende haundi-maundia
ezin ikusia,
eta enbidia,
horra zuen beltz eta
zuen zuria.

Morroiak:
Inola ez genduen
nahi gerrara sartu,
"fuerzan" gaituzte jaunak
honetaratu:
haiek makillaz jo eta
beok predikatu,
modu hontan gaituzte
sekulako hondatu,
Jainkuak nahi badu,
gaur edo bihar gu
pakian paratu,
norbaitek egun gorria
ikusiko du.

Nagusiak:
"Fuerzan" egin dizute
haiek ahalegiña,
jakinikan norentzat
zenduten griña:
txakurrak gaizki zaunka
zueri egiña,
duk oraindikan Praixku
daukazuten miña,
baduk erregiña
zuentzako diña,
ereman eziña,
pagatu behar dezute
tema zikiña.