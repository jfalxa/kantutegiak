---
id: ab-4434
izenburua: Txuri Batzuen Esku
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004434.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004434.MID
youtube: null
---

Txuri batzuen esku
afrikar arrotza
ostiaka gertu zuen
ikusi heriotza, ikusi heriotza
beltz bero beltz hotza
baten azala eta bestearen bihotza

Gibraltar inguruan
arabiar ugari
bizi berri batekin
dator ameslari
ai ameslari
zenbat mugalari
hauen kontura diren
aberasten ari.

Gay eta lesbianek
burruka astuna
"gaitzak hartuta daude"
gure erantzuna
ta ez dakiguna
gure burua dela
gaixorik duguna.

Gaitz hau gizarte osoa
sufritzen ari da
sendatu nahi badugu
saiatu sikira, saia sikira
eta ez joan urrutira
nor bera jarri bere
barrura begira.