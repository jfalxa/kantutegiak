---
id: ab-4419
izenburua: Sendagillena Dijoa
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004419.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004419.MID
youtube: null
---

Sendagillena dijoa
gizona gizajoa
burua dauka nastuta
egiazko aztuta.

Sendagillek berriz
tratamendu jarriz
berak artzen duna
dio, da onena.

Ok biok ta danak
gaude gaixoak;
jarri gaitu lanak,
gaurko giroak.

Sendagillena dijoa
gizona gizajoa
burua dauka nastuta
egiazko aztuta.