---
id: ab-4339
izenburua: Joxe-Joxe Gitarra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004339.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004339.MID
youtube: null
---

Joxe, Joxe gitarra
auntzak aiñeko bizarra.
Zopa gutxi janda
gizon kaxkarra.