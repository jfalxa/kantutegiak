---
id: ab-4404
izenburua: Paratu Behar Ditut
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004404.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004404.MID
youtube: null
---

Paratu behar ditut
zenbait bertso berri
"Mastaborda"-ko alaba
Arrosa Inaziri.
Hitzak eman genizkan
gu biok elkarri;
zenbat bider în duzu
neregana etorri.