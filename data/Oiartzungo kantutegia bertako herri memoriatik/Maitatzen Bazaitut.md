---
id: ab-4352
izenburua: Maitatzen Bazaitut
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004352.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004352.MID
youtube: null
---

Maitatzen bazaitut, itaundu dautsozu
leyuan klisk dagin illari-izpijari.
Larrosa zuriko txindor kantubari
itxartu eziñik gaba igaro dozu.

Axiari itaundu dautsozu legunki:
berak baña jolas dagi zeure ulian.
Ete dakije ezer ixarren unian?
Mosu dagitzube gustijak kutunki.

Itaundu dautsozu zeru garbijari,
itxasoko uiñai ta ixadi orori,
maitasun orretzaz baña, maite ori,
ez dautsozu itaundu zure bijotzari.