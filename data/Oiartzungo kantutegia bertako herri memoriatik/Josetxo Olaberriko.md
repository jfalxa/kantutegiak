---
id: ab-4338
izenburua: Josetxo Olaberriko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004338.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004338.MID
youtube: null
---

- Josetxo Olaberriko:
zer dezu negarrez?
- Amak Donostirako
dirurik eman ez.

Josetxok egin zuen
lastozko zubiya
andikan pasatzeko
bera ta nobiya.

Pasatzen asi-eta
erori zubiya;
Josetxok artu zuen
sentimentu aundiya.

Nobiya gelditu zen
begiko miñakin;
Josetxo jon zitzayon
txokolatiakin.