---
id: ab-4293
izenburua: Exkerra Beratarra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004293.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004293.MID
youtube: null
---

Exkerra Beratarra,
zuretzat goraintzi,
lengo kostunbre zaharrak
etzaizkigu ahantzi.
Sakelak hutsak eta
poltsan diru gutxi,
inbusteriz mantendu,
bertsuetan jantzi...
Horla bizi denak eztik
hiltzia merezi.

Exkerra, nai badezu
nerekin jarduna,
señalatu itzazu
hilla ta eguna;
erritikan hartzazu
nahi dezun laguna.
Bertsotan jokatzera
zatoz Oiartzuna:
etzaizu paltatuko
zeñek erantzuna.