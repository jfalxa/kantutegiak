---
id: ab-4314
izenburua: Hesirik Gabeko
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004314.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004314.MID
youtube: null
---

Hesirik gabeko zerupe hartan,
haize eta lur bategin ziren tokian,
amaierarik gabeko eguzki garaia da zure saioa.
Gari-belar horien itsasoan oinak dituzu txalupa,
eta pausoak, bat bezela mila.
Norbaiten artalde hori, zu neke gabeko zaindari.
Haize-errota baten azkenengo harria hartu dezu aulki.
Denboraren jabe etsia, bakardadearen lagun,
azala illun, begiak urrun, Mantxako artzaia, noiz arte ordea?
Aspaldi ahaztu ziren bide galdu ixiletan barna,
azala illun, begiak urrun, Mantxako artzaia, noiz arte ordea.