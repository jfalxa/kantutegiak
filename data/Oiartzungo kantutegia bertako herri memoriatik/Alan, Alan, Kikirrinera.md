---
id: ab-4224
izenburua: Alan, Alan, Kikirrinera
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004224.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004224.MID
youtube: null
---

Alan-alan, kikirrinera!
Alan-alan zamarrunera!
Maritxo Plazaberriko
jirari beste aldera.