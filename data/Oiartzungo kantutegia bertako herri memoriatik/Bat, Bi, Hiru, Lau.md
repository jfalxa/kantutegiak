---
id: ab-4249
izenburua: Bat, Bi, Hiru, Lau
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004249.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004249.MID
youtube: null
---

Bat, bi, hiru, lau,
sei, zazpi, zortzi,
bederatzi, hamar,
hamaika, hamaba;
hamairu, hamalau,
tralara, lala;
hamabost, hamasei,
tralara, lala;
hamazazpi, hemezortzi,
hemeretzi, hogei.
Hogei, hemeretzi,
hemezortzi, hamazazpi,
hamasei, hamabost,
hamalau, hamairu,
hamabi, hamaika,
tralara, lala,
hamar, bederatzi
tralara, lala,
zortzi, zazpi, sei,
bost, lau, hiru,
bi, bat.