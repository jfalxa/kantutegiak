---
id: ab-4259
izenburua: Bi Mando Aurretikan Dituzula
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004259.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004259.MID
youtube: null
---

Bi mando aurretikan dituzula.
Gorputz karro zahar baten gainean.
Mundu guztiak zurekin batera,
nabaitu du otzikara bere bihotzean.
Berotu zaizkigu gure erraiak.
Sinistu ezinetik igesi.
Gorputz zaharrean indarra bezala,
zure maitasun bideak
tiro zakarrak ahaztu arazi.

Ez, ez etsi!
Gurea degu bizitza.
Goizeko eguzki gorria
gure itxaropenen iturria.

Zure bizitza burrukatu beti,
giza eskubidearen bila.
Maitasuna hartuaz zure gidari
izan dituzu bidean estropotzu mila.
Bakezaleen gidari haundia,
askatu nahi zure anai beltzak.
Gaizkileen atzaparretan gorputz
arkitzen dirade orain zure maitasunaren ametsak.

Ez, ez etsi!...

Atzenik arkitu dezu bakea
ilhotzik lortu askatasuna.
Atzenik zabaldurikan zuretzat
eguzkia begiz ikusiko dezun eguna.
Orhoi zaitez emen gelditzen geran
zure anai behartsu guztiaz.
Askatzeko behingoz eta betiko
gizona gogor loturik daukaten kate
izugarriak.

Ez, ez etsi!...