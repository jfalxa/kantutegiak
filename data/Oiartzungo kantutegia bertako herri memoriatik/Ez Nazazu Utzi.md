---
id: ab-4297
izenburua: Ez Nazazu Utzi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004297.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004297.MID
youtube: null
---

Errepika:
Ez nazaz'utzi,
Ez nazaz'utzi,
Zuk ni ez utzi,
Zuk ni ez utzi!

Ez nazaz'utzi!
Behar da aztu,
guztia aztu,
aztu ta etsi:
Aldi galdua,
joan den garai,
kontrako zernai
hor pilatua;
orduen otsa,
garratza zena,
hausten zuena
zorion-poza.

Ez nazaz'utzi...

Dizut emanen
eurizko txirla,
euririk ez den
herriko perla;
lurra haustuko
indar berriaz
zu apaintzeko
urre gorriaz;
nai nuke egin
ner'eremua
mait'erreinua
ta zu erregin.

Ez nazaz'utzi...

Ez nazaz'utzi!
Dut asmatuko
nik zuretzako
t'adierazi:
Zuk ausnartzeko
zu biguntzeko
erran zoroak
erran beroak:
errege baten
bihotz-arnasa,
zurekin zuen
maite-ametsa.

Ez nazaz'utzi...

Ikus'izan da
sumendi zarra,
lotan egon-da,
berritzen garra;
gari-zelaian,
- suak errea-
dator garaian
gari obea;
ilunabarrez,
beltz eta gorriz,
datorkit berriz
maitea parrez.

Ez nazaz'utzi...

Ez nazaz'utzi!
Ez dut hitzikan,
ez negarrikan,
hain naiz gauz gutxi.
Ni naiz gordeko
zu ikusteko
gogoz dantzatzen,
pozez kantatzen.
Nai nuke izan,
mirabe gisan,
zure itzala,
txakur leiala.

Ez nazaz'utzi...