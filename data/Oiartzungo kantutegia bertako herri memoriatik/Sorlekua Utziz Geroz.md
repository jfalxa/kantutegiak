---
id: ab-4420
izenburua: Sorlekua Utziz Geroz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004420.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004420.MID
youtube: null
---

Sorlekua utziz geroz
ondikotz ala bearrez,
Jainko ona urrilkaldu da
beti gure nigarrez.
Primaderan asi orduko
arbolak estaltzen lorez,
xoritto bat eldu zait beti
nere herritik hegalez.