---
id: ab-4320
izenburua: Horra Donostiako Linternadoriak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004320.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004320.MID
youtube: null
---

Horra Donostiako
linternadoriak:
zer zirala uste zenduten
Oiartzungo gaztiak?
Eskasagoak ez dira
lehengo zahar tristiak.
Gehiago balio du
hoien korajiak.