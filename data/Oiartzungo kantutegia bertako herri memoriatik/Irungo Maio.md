---
id: ab-4329
izenburua: Irungo Maio
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004329.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004329.MID
youtube: null
---

Irungo Maio,
A zer afaria geniken bart.
Erretilluan eper bat.
Irungo Maio,
a zer afaria geniken bart.
...
Hamar karga zardin berri;
bederatzi basaurde;
zortzi idi;
zazpi zezen;
sei zikiro;
bost pirilo;
lau antzara;
hiru oilo;
bi tortoilo;
erretilluan eper bat.
Irungo Maio,
a zer afaria geniken bart.