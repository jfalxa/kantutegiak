---
id: ab-4276
izenburua: Don Jose De Altzunaga
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004276.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004276.MID
youtube: null
---

Don Jose de Altzunaga,
probintzianoa,
deskantsuz egoteko
a gizon sanoa!
Adinaren konforme,
mundutik banoa,
urrikal dakidala
Jaun Soberanoa.
Gizon umanoa,
maite du ardoa,
ze perillanoa;
denbora gutxitako
amerikanoa.