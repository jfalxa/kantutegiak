---
id: ab-4358
izenburua: Mañuel
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004358.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004358.MID
youtube: null
---

Mañuel,
Kaxuel!
Pikua janta hil huen;
haizemanta pixtu...
Biba Mañuel-xixtu!