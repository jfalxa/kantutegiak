---
id: ab-4288
izenburua: Errenteria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Errenteria,
mal-herria.
Irun,
kopet-ilun.
Oiartzun,
dakarren monedan erantzun.
Altziber,
beti ber.
Iturriotz,
beti hotz.
Karrika,
harrika eta makilka.