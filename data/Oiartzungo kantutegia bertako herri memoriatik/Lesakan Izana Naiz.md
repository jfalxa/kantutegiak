---
id: ab-4350
izenburua: Lesakan Izana Naiz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004350.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004350.MID
youtube: null
---

Lesakan izana naiz
ginan San Ferminez;
Aitxuleiko gainetik
joanak ginan oinez.
Guri zer gerta zaigun
askotxok jakin ez.
Bertsotan jarriko dut
guztizko atseginez.