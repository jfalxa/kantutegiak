---
id: ab-4387
izenburua: Nere Sentimentua
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004387.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004387.MID
youtube: null
---

Nere sentimentua
nahi det deklaratu;
Ameriketan nago,
hau ezin ukatu! (bis)
Hemen egiñagatik
ondo gobernatu,
horkuaz egiten naiz
asko akordatu.

Honuntz etorri nintzen
utzirikan ama;
anai-arrebak eta
familia dana. (bis)
Egitiagatikan
deseo nuena.
Geroztik triste dabil
ai! ene barrena.

Hamazazpi urtetan
banuen segira;
neska gazte guztiak
neroni begira. (bis)
lurrik ikuitu gabe
gorputzari gira.
Orduko arintasunak
asentatu dira.

Gorputz ederra nuen
dantzari arina.
Baiña ez da izaten
betiko egina. (bis)
Hemen eginagatik
orain ahalegina,
ez gera dibertitzen
orduan adina.

Horko bizimoduaz
oso aspertuak,
honerako ziraden
gure desiuak. (bis)
Orain atzera, berriz,
emanik pausuak.
Hemen bizi gerade
erdi muatuak.

Despedida dijua
sei bertso hoiekin;
anaia, konformatu
horrenbesterekin. (bis)
Nere gustua zer dan
nahi badezu jakin,
beste horrenbeste jarri,
aio! Joxe Joakin.