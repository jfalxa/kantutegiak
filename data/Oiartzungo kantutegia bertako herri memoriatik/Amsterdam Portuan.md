---
id: ab-4232
izenburua: Amsterdam Portuan
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004232.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004232.MID
youtube: null
---

Amsterdam portuan
marinelak dabiltz,
kantuz ta karraxiz,
Amsterdam galduan;
Amsterdam portuan
marinelak lotan,
enborra-moduan
kaiako lanbrotan;
Amsterdam portuan
inoiz dira hiltzen,
edaritan lertzen,
gauaren barruan;
baita han portuan
jaio dira ere
mila itsaskume
kaia-inguruan.

Amsterdam portuan
marinelen jana,
gehiena arraina
haien otorduan;
parranda-orduan
lotsa dute galtzen;
indarrak gailentzen
eromen-munduan.
Bakailu usaia
danean nagusi.
Jolaseko nahia
dute erakutsi.
Eta altxa dira,
bragetak estutuz,
kalera begira
parrandan pentsatuz.

Amsterdam portuan
marinelak dantzan,
damekin balantzan
zoroen moduan;
eskuzko soinuan
musika indarrez;
marinelak trakets
pauso nekatuan;
Salto ta jiraka
parrez dira lertzen;
azkenik musika
penaz da ixiltzen.
Eta gero berriz
bueltatutzen dira,
kantuz eta irriz
edaritegira.

Amsterdam portuan
zurrut marinelak,
edan eta edan
eginez ahalak;
edan bai gehiago
putak dezaten on,
Amsterdam, Hamburgon
edo haruntzago.
Eman dutenekin
anima ta gorputz,
ezin da egin huts
mirabe hoiekin.
Gauakin katazkan
malkoz dira bete;
pisa dariete
damatxoen salan,
AMSTERDAM PORTUAN!.