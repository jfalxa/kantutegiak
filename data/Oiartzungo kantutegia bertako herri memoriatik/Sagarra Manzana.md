---
id: ab-4411
izenburua: Sagarra Manzana
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004411.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004411.MID
youtube: null
---

Sagarra manzana,
ikatza carbón
mujer andria,
hombre gizon.
Attona Martzelon
Kiski-ti-kaska
Kaska-melon.