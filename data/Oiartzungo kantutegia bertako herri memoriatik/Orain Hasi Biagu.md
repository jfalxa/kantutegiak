---
id: ab-4399
izenburua: Orain Hasi Biagu
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004399.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004399.MID
youtube: null
---

Xen.:
Orain asi biagu
asunto berriyan,
zer gertatutzen zaigu
Lezoko erriyan;
neska gaztiak daude
emen alegriyan.
Kantaren aitzakiyan
mutillen erdiyan.

Ard.:
Emakume-kontuan
al zerade asi?
Gerenez konsidera
dezagun lenbizi;
jendiari nai diot
emen adirazi
ia zein joaten dan
neskatxen igesi.

Xen.:
Ia, dama gaztiak,
ondo entzun arren:
ez utzi sartutzen
nai dan bezin barren;
asiko lirakenak
Xenpelar baño len,
bizar aundi batzuek
badirade emen.

(Ardotz eta Xenpelar kantuan..