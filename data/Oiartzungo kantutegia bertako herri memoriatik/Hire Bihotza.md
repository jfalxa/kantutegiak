---
id: ab-4316
izenburua: Hire Bihotza
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004316.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004316.MID
youtube: null
---

Hire bihotza, xori zauritu, ihes joan zain atseden billa,
ari mehe bat hunan bizitza urratua dun keiñu bortitzez,
oroitzapenen labe gorian gorputz arima tormentu luzez
azkeneraino irauneraziz bizi behintzenan deserri hortan.

Nahiko hunan, Beatriz, Chile libre batean
adiskide artean maitasuna dastatu,
nahiko hunan zentzuak, lore goiztarren gisan,
zorrik ordaindu gabe desiotan askatu.

Hire aitaren heriotzari ordaindu dion zerga astuna,
oihu lehertu bat utzi digunan zauri guzien siñu bezala:
ez dun nahi izan iraganaren erru haundia besteri itzul,
hainbeste beldur, hainbeste odol isil-silik irentsi ditun.

Nahiko hunan, Beatriz, poeta baten bertsoz
hire gaztetasuna ispilluan mirestu,
nahiko hunan, gaur ere, bizitzaren kaliza
ardo gorriz beterik gozamenetan ustu.

Errugabea hintzenan baiña pizti zordunak ez ditun urrun,
hire eskuen dardara hautsiak haien ogena oihukatzen din.
Har zan atseden betiko loan, gu gaitun orain azken beilari,
heriotz horren mezu larria ez baidinagu nehoiz ahaztuko.