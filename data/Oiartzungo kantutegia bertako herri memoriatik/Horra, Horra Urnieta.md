---
id: ab-4321
izenburua: Horra, Horra Urnieta
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004321.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004321.MID
youtube: null
---

Horra, horra Urnieta
ez da besterik pareta,
malamentian erreta.
Horien botoz, pasa beharko du
nekazaliak dieta.
Erdaldunaren kopeta!
morralak ongi beteta,
gero, ihesi lasterka.