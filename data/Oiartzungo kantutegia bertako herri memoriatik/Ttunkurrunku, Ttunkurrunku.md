---
id: ab-4428
izenburua: Ttunkurrunku, Ttunkurrunku
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004428.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004428.MID
youtube: null
---

Ttunkurrunku, ttunkurrunku, ttunkurrunkuttuna;
ttunkurrunkuttun, kuttun, kuttun, kurrun, kuttuna.
Lo, lo, ttun, kurrun, kuttuna.