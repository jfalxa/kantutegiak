---
id: ab-4336
izenburua: Jexux, Joxe, Maria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Jexux, Joxe, Maria
Xantana ta Xan Joakin!
Bostak dirala gurekin;
gu ere bai haiekin;
zerura joteko
denak alkarrekin.