---
id: ab-4303
izenburua: Gaur Goizetikan Oiartzun Eta
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004303.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004303.MID
youtube: null
---

Gaur goizetikan Oiartzun eta
arratsaldian Bergara...
geldi daudenak ez dakit baiña
gabiltzanak bizi gara.
Lege Zahar gabe beldur zara zu
hilko ote zaigun euskara...
Dabillen arte ez dago hillik,
atera zagun plazara.