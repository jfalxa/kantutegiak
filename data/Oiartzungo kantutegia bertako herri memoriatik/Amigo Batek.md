---
id: ab-4231
izenburua: Amigo Batek
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004231.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004231.MID
youtube: null
---

Amigo batek or topatu nau
Ubalditxoko parian,
uste gabeko enkontru txarrik
izan ez nezan birian;
kulparik gabe nola nabillen
pertsona askoren abian,
ai ori pena eman dirate
Iru Errege gabian!

Errenteriyan esan omen du
larogei urteko atsuak:
"Orroko orretxek para zituen
Palaziyoko bertsuak".
Gazte bat balitz emain niozkake
belarrondoko batzuak,
konfesakizun aundiyak ditu
testimoniyo faltsuak.

Bertsolariya engañatzeko
eskuan madamusela,
uste al dezu orrenbestekin
engañatutzen naizela?
Ume txikiyak engañatzeko
nola egiña dan pastela,
kulparik gabe saldutzen naute
Kristo Judasek bezela.