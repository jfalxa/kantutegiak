---
id: ab-4392
izenburua: Ni Naiz Ganbelu Zaharra
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004392.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004392.MID
youtube: null
---

Ni naiz ganbelu zaharra
maite dut nere bizia
mila esaldiz hautsia,
zer-nolaren hotzikara.
Bizi senaren galdara
jaso nuen oparia.
Bihotz taupaden neurria
ixurtxen zait begitara.
Irudimen nabaria
jaiotzen zait bizitzara.
Noizbait egin dut algara
dastatuz mundu berria.

Hemen behar nuen ausart,
beste guziak bezala.
Ez dut irudi apala,
albokoen grina dakart.
Behar nuke egin zirt-zart,
har askatasun zabala,
hautsi legeen itzala
errespetatuaz alkar.
Zorionaren mantala
bihotz aterpean damart.
Murmurio hautsa mar-mar
maitasunaren ezpala.

Bat dut nere bihotzean
egin behar denerako.
Hartu ditut kanturako
mila ordu magalean.
Testigutza bortitzean
zerbait utzi gerorako,
nahiz ez dudan ez altxako
oihartzunik ohianean.
Ez dut nahi azkenerako
uzterik nere aurrean,
negar malkorik airean
gau ilunabarrerako.