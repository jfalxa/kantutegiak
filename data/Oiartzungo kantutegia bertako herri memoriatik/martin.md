---
id: ab-4363
izenburua: Martin
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004363.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004363.MID
youtube: null
---

Martin!
Talua ta zardin,
tipula ta gatza...
Martin ipurdi batza.