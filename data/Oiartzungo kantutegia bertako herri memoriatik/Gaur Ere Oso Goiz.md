---
id: ab-4302
izenburua: Gaur Ere Oso Goiz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004302.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004302.MID
youtube: null
---

Gaur ere oso goiz esnatua nau,
ordu gutxi eta loalea.
Lehen hanka bat ateratzen det, barruan bero bestea.
Hotz! eta kalean lanbro zuria.

Gaur ere oso goiz esnatua nau,
despertadore zakarra!
Muturra busti, bizarra moztu, galtzak eta bi zapata.
Hotz! eta kalean lanbro zuria.
Oraingoz behintzat jaso nahi nuke gaur daukaten zalantza
Atzo gaur eta bihar ere; honek ez du berririk,
hau azken eguna izan litekeela pentsatzen det,
baina halere oraindik bizi nahi det.

Goizean gauez kalean nago oraindik ametsak kontuaz,
argi batzuek oraindik esna, auzoko neska gaztea.
Horra! autobus zaharraren ke bektza.
Egunkari beroaren berri hotzak, jendearen begi
zorrotzak, kunplitutzera gaurko otoitzak.
Honek ez du berririk, hau azken eguna izan litekeela
pentsatzen det, baina halere oraindik bizi nahi det.