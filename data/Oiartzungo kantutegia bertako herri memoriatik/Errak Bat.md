---
id: ab-4287
izenburua: Errak Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004287.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004287.MID
youtube: null
---

Errak bat,
gure Jauna bera duk bat.
Errak bi,
Erromako bi aldareak,
gure Jauna bera duk bat.
Errak hiru,
Hiru Trinidadeak,
Erromako bi aldareak,
gure Jaunak bera duk bat.
Errak bost,
bost llaga preziosoak,
lau Ebanjelista santuak,
Hiru Trinidadeak,
Erromako bi aldareak,
gure Jaunak bera duk bat.