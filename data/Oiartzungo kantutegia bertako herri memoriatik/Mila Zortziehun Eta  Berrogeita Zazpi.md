---
id: ab-4368
izenburua: Mila Zortziehun Eta / Berrogeita Zazpi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004368.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004368.MID
youtube: null
---

Mila zortziehun eta
berrogeita zazpi,
urte hartan jo nuen
Oiartzungo Txipi.
Geroztik hemen nabil
gotti eta behetti;
behin ere hil ez baña
arriskutan beti.

Ez al-da bada pena
ni nabilen planta?
duro bat ez dut izan
nere bizi santan.
Egunian txanpon bat
alderatu faltan,
nere denbora juan da
geroko esperantzan.

Denbora juan eta
hasi naiz pensatzen,
zahartziak ez duela
gauz onik ekartzen.
Pixa laburragoa
iaz baino aurten;
zapata ere puntatik
hasi zait usteltzen.

Iaz zapatari eta
aurten belaunari,
Nolako diferentzia;
urte batez hori!
Lehen paretak hautsi nahi;
orain xirri-xirri...
non dudan bilatzeko
behar ditut ordu bi.

Aginak galdu eta
puskatuak hortzak;
zintzurra hor jotzen dit
azpiko kokotzak.
Espain-ertzak erreak
pipa kider motzak.
Horra nora joan diren
gaztetako erronkak.