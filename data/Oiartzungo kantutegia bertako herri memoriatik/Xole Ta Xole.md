---
id: ab-4439
izenburua: Xole Ta Xole
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004439.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004439.MID
youtube: null
---

Xole ta Xole
Xole gaxua!
Xolek eran du
Mama goxua.
Hernaniko plazan
hiru atso dantzan;
hirurak ez dute
ardit bana poltsan.