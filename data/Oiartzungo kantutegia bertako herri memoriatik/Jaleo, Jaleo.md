---
id: ab-4332
izenburua: Jaleo, Jaleo
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004332.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004332.MID
youtube: null
---

Jaleo, jaleo.
Tikiyok amari eltzia
atzetik, atzetik,
dena babaz betia.