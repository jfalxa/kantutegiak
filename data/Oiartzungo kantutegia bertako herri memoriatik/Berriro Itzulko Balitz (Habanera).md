---
id: ab-4255
izenburua: Berriro Itzulko Balitz (Habanera)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004255.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004255.MID
youtube: null
---

Berriro itzulko balitz iragan denbora arrotza
berdin kontsumi nezake vainilla gozo artean,
itsaso urrun batetan irudimena galdurik
udaberriko euritan larrosak bizten ikusiz...
Osaba komertzianterik ez nuen izan Habanan,
pianorik ez zegoen bizi nintzen etxe hartan,
neskatxen puntilla fiñak udako arratsaldetan,
errosario santua neguko gela hotzetan.

Ezpainek gordetzen dute ezpain bustien gustoa
desiozko hotzikaran etsipenaren tamalez,
gauak zelatan dakusa kontzientzia bilutsik
badoaz ordu geldiak gogorapenen hegalez.
Jaio giñen, bizi gera, ez dugu ezer eskatzen,
itsasontzia astiro kaiatik ari da urruntzen,
Antillak zintzilik daude argazkien paretetan
karta bat idatziko dut norbaitek erantzun dezan.

Tabako, ron eta kanelaz sukartutako ametsetan
algarak entzuten ziren Habanako putetxetan,
abanikodun mulatak gauari haize egiten
musiken aide nagiek odola erretzen zuten.
Jaio giñen, bizi gera... ... ... ...