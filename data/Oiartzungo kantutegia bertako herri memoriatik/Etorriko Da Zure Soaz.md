---
id: ab-4290
izenburua: Etorriko Da Zure Soaz
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004290.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004290.MID
youtube: null
---

Etorriko da zure soaz heriotza
gor, logabe, alhadura zaharren gisa,
goiznabarretik gauera alboan dugun
ohidura zentzugabe bat bailitzan.

Zure begiak alferrikako hitza izanik
garraisi mutu, isiltasun oro isil,
goiz bakoitzean aurkitzen dituzu adi
ispilura begiratuz, ilun, hurbil.

Jakinen dugu egun hartan, oh
itxaropen!
bizitza zarela eta ezereza,
guziontzat du heriotzak soa zorrotz
bakar, mutu, leizera jetsiko gera.

Urratuko da ezpain hertsien keinua,
aurpegi arrotza leihoaren ondoan,
usadioen guneak desitxuratuz
biluztasuna nagusitzen denean.

Zure begiak argi grisaren errainu,
mendi ilunen goizorduko izotza,
esnatzearen dardara eta ikara
kale hutsetik hurbiltzen zarenean.

Jakinen dugu egun hartan, oh
itxaropen!
bizitza zarela eta ezereza,
gizontzat du heriotzak soa zorrotz
bakar, mutu, leizera jetsiko gera.