---
id: ab-4355
izenburua: Malkoak Euri Balira
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004355.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004355.MID
youtube: null
---

Malkoak euri balira udazken traidorean
maitalerik gabe kolpez bazter utzia
emazte adintsua gertatzen denean,
ehun hodei ilunen jasa izugarriek
kaleak uholdetan itoko lituzkete,
errotik garbituko ohizko zekenkeriak
hondakinez gainezka dituen bihotzak.

Malkoak euri balira kupidarik gabe
haurtxo baten gorputza gerra izugarriek
burniz eta metrallaz hausten dutenean,
ama denen oihuek mendekua eskatuz
izoztu lezakete itxaso zabala,
gorrotozko irrintziz sorgorra asaldatuz
bizidunari egotzi eresi makurra.

Malkoak euri balira adoleszente baten
ezpain oihukariak harresi baten kontra
torturaren espantuz zauri direnean,
etxerik altuenak lotsaz eror litezke
gizadi osoa redimitu nahirik...
eta lore bitxiek entona lezakete
ehortze letani bat zerraldo gainetan.