---
id: ab-4218
izenburua: Aita San Inazio
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004218.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004218.MID
youtube: null
---

(1. Bertsioa):
Aita San Inazio
Gipuzkoakua,
Loialako etxe eder
palaziyokua,
munduan gustable da
zure ejenplua,
arren argi zadazu
entendimentua,
nola maitatu behar dan
lagun projimua.

(2. Bertsioa):
Aita San Inaziyo,
Azpeitiakua
Loyolan etxe eder
palaziyokua,
munduan gustable da
zure ejenplua;
zure alabantzan
ni kantatzera nua
basta da zaitudala
probintzianua.

Aita San Inazio
arratseko obra
nere buru gajuak
sentitzen du sobra.
Gizona izatia
haiñ biyotz gogorra,
iturritik bezela
zetorren odola,
projimuen legiak
ez dirade orla.

San Inazio arratsian
ai ura afariya!
Nere suerterako
zegoen jarriya.
Lagun maite batek
burura arriya,
ez du edozeñek egiten
olako sangriya,
Jaunak digula arren
miserikordiya.

Aita San Inaziyo
biyamon-goizian,
auspez erori nintzan
meza entzutian.
Lau lagunek artuta
kanpora airian,
pena asko zegoen
nere barrenian,
goguan idukiko det
eternidadian.

Nere denborako señalia
sekulako erida,
ikusten nauten guziyok
burura begira.
San Inazio zala
oraindik berri da,
arriyaren golpiak
itsusiyak dira,
nere pekatuentzat
nunbait konbeni da.

Oroitzen naizenian
ni San Inaziyoz,
erregututzen diyot
beti deboziyoz.
Nere lagun maitia,
hik ere egiyok,
elkarren konpañiyan
giñukian biyok,
orlako aziyorik
ez dik egiñ iñork.

Amoriyua diyot
beti lagunari,
nere buruarentzat
sobra ta geiegi.
Erregututzen diyot
San Inaziori,
arren barkatutzeko
pekadoriari,
gustua kunplitu ta
ongi bizi dedi.

Nere pasadizuaz
horra zortzi bertso,
nai dituenak ikasi
ez dirade asko.
Legiak agintzen al du
gizona jotzeko?
Esamiñatuak badaude
gu korrejitzeko,
ez daukat desiorik
zorra hau pagatzeko.