---
id: ab-4267
izenburua: Bringun-Brangulun
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004267.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004267.MID
youtube: null
---

Bringun-brangulun eragidazu,
laister haziko natzaizu;
laister hazi ta ongi lagundu...
fabore izango nauzu.