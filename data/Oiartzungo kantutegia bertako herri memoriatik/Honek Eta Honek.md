---
id: ab-4319
izenburua: Honek Eta Honek
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Honek eta honek eta honek eta honek
bart eztaiak zituzten.
Honek esan zion honi
konbidatzeko hau eta hau;
Konbidaturik hau eta hau
hoitxek dittuk hogeitalau.