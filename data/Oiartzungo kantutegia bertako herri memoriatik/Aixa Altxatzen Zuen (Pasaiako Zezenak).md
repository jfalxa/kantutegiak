---
id: ab-4220
izenburua: Aixa Altxatzen Zuen (Pasaiako Zezenak)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004220.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004220.MID
youtube: null
---

Aixa altxatzen zuen
ezkerreko hanka;
jendiak ihesi ta
xexenak arronka.
Eskaileretan gora
itzuririk kaka.
Inori barkatzeko
muttur ona dauka.