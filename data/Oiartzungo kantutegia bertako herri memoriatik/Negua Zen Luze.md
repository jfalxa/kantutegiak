---
id: ab-4380
izenburua: Negua Zen Luze
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004380.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004380.MID
youtube: null
---

Negua zen luze, negua
izotzetan galdua.
Sutondoan bero katua,
goxo loak hartua.
Mahiaren azpian sartua
ardi txakur sutsua.
Amonaren giran bildua
sendi zoriontsua.
Ilunaren zai.
Ilunaren zai.

Atzeko leihotik haizea,
ulu kantuz ernea.
Laratzetik pertza betea,
mugiz gure gosea.
Talo egiteko orea,
maira baitan gordea.
Amonaren zentzu luzeak
zaintzen du sukaldea.
Aitonaren zai.
Aitonaren zai.

Afaldu aurretik kantua,
bihotzetik sortua.
Bertso zahar paperen lekuak
hartzen du astindua.
Gero errosario santua,
zintzo errezatua.
Amonak ohera besuan
darama aingerua.
Ametsaren zai.
Ametsaren zai.

Ilunaren zai.
Aitonaren zai.
Ametsaren zai.