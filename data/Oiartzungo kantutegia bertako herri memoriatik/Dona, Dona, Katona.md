---
id: ab-4277
izenburua: Dona, Dona, Katona
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004277.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004277.MID
youtube: null
---

Dona-dona, katona,
kaka zaharra mantzona.
Kukulumera-kukulumera,
atsua goitik behera.
Nongo atsua?
Tarrapateko atsua;
eskaileran beheti eroita
hautsi du kaskua.