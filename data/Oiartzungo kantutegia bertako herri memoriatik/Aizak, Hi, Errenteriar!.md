---
id: ab-4221
izenburua: Aizak, Hi, Errenteriar!
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004221.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004221.MID
youtube: null
---

Pello Errotak:
Aizak, hi, errenteriar
txatxu elbarria;
deusere ez dakiken
endredagarria.
Ai! Buru gogorreko
traidore garbia;
hi ez bezelakua dek
Pello Errotariya.

Txirritak:
Hamaika mantentzen dik
errotarren trankak,
doblatuan kenduaz
zakuari lakak.
Infernura joatian
ai! zer muturjokak;
Pello, istilu gogorrak
ikusteko dauzkak.

Pello Errotak:
Hoiek aditu beharrak
errotarienak,
kulpa gabetandikan
senti ditutenak.
Likidatzen ditudala
zaku besterenak...
Nonbaitik sortu behar dik
berak ez duenak.

Txirritak:
Errotaren pisu hoikin
biolina juaz,
dantzan purrakatzen dabiltz
Luzifer eta Judas.
Lehen ere soziyua
zinaden benturaz;
lagunak ikusiyaz
hoiengana zuaz.