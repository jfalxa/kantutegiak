---
id: ab-4247
izenburua: Baratzeko Pikuak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004247.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004247.MID
youtube: null
---

Baratzeko pikuak
hiru xurten ditu; (bis)
neska mutilzaliak (bis)
hankak arin ditu. (ter)
Ai ene!
Nik ere nahi nuke
Ai ene!
Zuk nahi bazenduk.