---
id: ab-4328
izenburua: Intxauspeko Alaba Dendaria
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004328.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004328.MID
youtube: null
---

Intxauspeko alaba dendaria,
goizian goiz jostera joalia;
nigarretan pasatzen du bidia;
aprendiza du kontsolatzailia.