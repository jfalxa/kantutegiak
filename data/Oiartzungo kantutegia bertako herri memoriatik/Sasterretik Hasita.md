---
id: ab-4418
izenburua: Sasterretik Hasita
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004418.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004418.MID
youtube: null
---

Sastarretik hasita
"Arandaran" arte,
atzetik debillenak
beretzat dik kalte.