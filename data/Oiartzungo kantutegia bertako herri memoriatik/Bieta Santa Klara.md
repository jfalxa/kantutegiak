---
id: ab-4262
izenburua: Bieta Santa Klara
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004262.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004262.MID
youtube: null
---

Bieta Santa Klara
euskeraz Argiya:
Zeu zara zerubetan}
Dontzella garbia. } bis

Amaren entrañetan
zenduan graziya
munduan ipinteko
exenplu aundiya.

Zure haurtasuneko
denbora guziya
izan zan erreguzko
exenplu biziya.

Alkargana baturik
harri koskortxuak
kontatu ohi zenduzan
errosariyuak.

Hamazazpi urtera
allegatu gabe
Jesus'en tesoruen
egin ziñan jabe.

Palma eder bategaz
erramu goxian
agertu izan ziñan
Asis'ko kalian

Hamazortzi urtetan
moja sartu ziñan
moja sartuta bere
Klara izan ziñan

Aita San Prantziskua
Padrino harturik
esposa izan ziñan
Ait'amen ixilik.

Berrogeitamar moja
ditu janaritu
ogi bakar bategaz:
erdiya gelditu.

Eskuan daruazun
gauzaren andiya;
Kustoriyo batian
Jesusa biziya.

Zu ikusirikan
moruak igesi:
Sakramentu Santua
zizuten ikusi.

Uso zuri ederra,
zeruan zer berri?
- zeruan berri onak
orain eta beti.

Ontziyaren punteko
bandera gorriya
zu zera, Santa Klara
dontzeila garbiya.

Mojen erregaluak
intxaurrak ohi-dira:
Ayek ere guztiyak
bai piñak balira...