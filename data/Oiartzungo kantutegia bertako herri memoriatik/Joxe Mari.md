---
id: ab-4340
izenburua: Joxe Mari
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004340.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004340.MID
youtube: null
---

Joxe Mari,
miri-mari,
lau hanka ta
bost belarri!
- Non duk andria?
- Kutxan gordia;
kutxan ez bada
surtan erria.
(Ai, demonio urdia!.