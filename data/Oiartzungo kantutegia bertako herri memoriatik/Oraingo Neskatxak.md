---
id: ab-4401
izenburua: Oraingo Neskatxak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004401.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004401.MID
youtube: null
---

- Oraingo neskatxak
zer dute merezi?
- Lisiban beratuta
gobaran egosi.