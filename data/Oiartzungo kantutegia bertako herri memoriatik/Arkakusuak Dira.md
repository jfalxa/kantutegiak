---
id: ab-4240
izenburua: Arkakusuak Dira
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004240.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004240.MID
youtube: null
---

Arkakusuak dira
ganadu ariñak.
Gure pertsegidore
mundura egiñak.
Lehenguan jan nituen
baten giltzurdiñak;
libra bana baziran
haren letaginak.

Andriak esaten dit
pistazu argiya;
Zimiko bat egin dit
txit izugarriya...
Sorgiñak diralako
beldurrak jariya;
horla pasatzen degu
gabaren erdiya.