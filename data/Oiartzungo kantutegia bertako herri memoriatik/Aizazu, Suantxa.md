---
id: ab-4222
izenburua: Aizazu, Suantxa
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004222.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004222.MID
youtube: null
---

- Aizazu Suantxa,
zer modu daukazu?
- Pobre baiñan alegre;
zer esain degu?
- Soka bat ekatzu:
lotu behar zaitugu,
baiñan ez izutu...
Agintzen digutena
ein beharko degu.

- Jaunak, esazute
zer dan motibua,
ni preso eramateko
sokaz lotua.
- Guk degu kargua
zu eramatekua
behar dan lekuâ.
Han aginduko dute
gainerakua.

- Joan behar degu, goazen,
geren biajian,
bainan luzatu gabe
gaituk etxian.
- Norbaitek tranpian
galdu nau mendian;
bainan bitartian,
ez da libertaderik
gure artian.

Hau da komeria,
nere Jaungoikua!
Arratoi bat da hemen
ni hainekua.
Horren alkantzua
ahuntza ainekua,
ez du indar gaixtua.
Ogi osuarekin
noiznahi dijua.

Hamabi urtetan
- ai zenbat gauetan!-
negarrez egondu naiz
kartzela honetan:
grilluak hanketan,
zorriak mantetan,
jan ona bestetan.
Ezer gutxi ziok
zortzi sosetan.