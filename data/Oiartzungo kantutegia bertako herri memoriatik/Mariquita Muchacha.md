---
id: ab-4362
izenburua: Mariquita Muchacha
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004362.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004362.MID
youtube: null
---

- Mariquita muchacha!
- Qué mandais, madre?
Txin-txibirin-txin-txin.
- Quita la manteliña,
vamos a baile.
Txin-txibirin-txin-txin.
Bolinen-bolinen; fardin-fardin, gordin-gordin.
Halere baskune.
Que burdun, que bardan
arrua le burdun; turun-taran; taria-riaran.
Halare baskune...
frantses euskaldune.