---
id: ab-4299
izenburua: Ezkondu Eta Ezkongai
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004299.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004299.MID
youtube: null
---

Aldaera:
Ezkondu eta ezkongai ,
guztiak ondo bizi nahi,
dirua degunian jai.
Familiako buruzagiak
kontu gehiago ere bai.
Lehen ardua edaten lasai,
orain urrutitik usai.
Etxian niok andre zai.

Aldaera:
Ezkondu eta ezkongai,
guztiok ondo bizi nahi;
dirua degunian jai.
Familiako buruzariak
kontu gehiago ere bai;
len ardua edaten lasai,
orain urrutitik usai;
etxean gaude andre zai.