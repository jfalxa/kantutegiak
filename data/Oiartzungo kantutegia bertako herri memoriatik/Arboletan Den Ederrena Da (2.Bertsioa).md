---
id: ab-6054
izenburua: Arboletan Den Ederrena Da (2.Bertsioa)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Arboletan den ederrena da
oian beltzian pagua.
HItzak ederrak dituzu baina
bestetarako gogua;
Jaun zerukuak emango al dizu
niganako amodiyua.