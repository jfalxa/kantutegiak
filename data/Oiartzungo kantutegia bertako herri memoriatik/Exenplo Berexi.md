---
id: ab-4292
izenburua: Exenplo Berexi
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004292.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004292.MID
youtube: null
---

Exenplo berexi;
nik axak egoxi,
egoxi ta klik
- gezurra diyok hik.