---
id: ab-4376
izenburua: Morerira Joan Ta
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004376.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004376.MID
youtube: null
---

Morerira juan ta
sutan behar dut hasi;
enplio txarrerako (bis)
ez niñuten hazi.
Zer egingo degu ba?
Hil artian bizi.
Adios aita ta ama (bis)
ta Mikel Iñazi.

Adios aita eta
agur, berriz, ama;
eskergabetasunak (bis)
soldado narama.
Adios maite, nere
bihotzeko dama:
bizi banaz bueltan (bis)
banator zugana.