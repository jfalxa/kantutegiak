---
id: ab-4230
izenburua: Amets Bat Izan Nuen
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004230.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004230.MID
youtube: null
---

Amets bat izan nuen
eztizko amets bat
zure besoetan babesturik
maitasuna eskainiz.

Iluntasunarekin ikasi nuen
amets egiten
argitasunarekin zu maitatzen
milaka hitz goxoren artean

Baina fabriketako keak
zapuzten zuen bitartean
lurrak, urak irentsi zituen.

Behinola egin nuen emets
betirako lokartuz
zure ametsa izan nahi bainuen
iluntasuneko lur-ur hezean.