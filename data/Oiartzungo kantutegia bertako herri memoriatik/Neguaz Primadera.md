---
id: ab-4381
izenburua: Neguaz Primadera
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004381.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004381.MID
youtube: null
---

Neguaz primadera
zanean jabetu,
sasi baten hegian
apexa zesortu.

Leku berean bai zen
lorea gertatu.
Gaixoek elkar zuten
bihotzez maitatu.