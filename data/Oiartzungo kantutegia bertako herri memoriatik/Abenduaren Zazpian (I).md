---
id: ab-4204
izenburua: Abenduaren Zazpian (I)
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004204.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004204.MID
youtube: null
---

Abenduaren zazpian,
sartu ginaden Frantzian.
Hogeita lau lakari gatz
azukre biren erdian;
ustez sekreto handian
pasa ginaden mendian.

Jo genuen Ziburu,
ustez guztiak seguru.
Bi gazteak ezkongaiak,
zaharrak ginaden hiru.
Irabazi nahiz zerbait diru,
kukuak makur jo digu..