---
id: ab-4330
izenburua: Isla Batean
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004330.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004330.MID
youtube: null
---

Isla batean kargatu omen da
ezkurrarekin barkua;
orain landarez hornituko da
lehen hutsa zegoen portua.
Sozidantiak badira baina
batek bearko du kargua;
leku onean ereinia da,
azalduko da fruitua.