---
id: ab-4341
izenburua: Kanpaien Mintzaera
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004341.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004341.MID
youtube: null
---

Errenteria:
Beti mixeriya;
beti mixeriya...

Lezo:
Guk ere bai,
guk ere bai...

Pasaia:
Izan-da izango,
izango-izango...

Oiartzun:
Or konpon, or konpon;
gu ongi gaude,
gaude, gaude...