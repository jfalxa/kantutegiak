---
id: ab-4334
izenburua: Jaungoikoak Gorde Zaitzala Erregina
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004334.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004334.MID
youtube: null
---

Jaungoikoak gorde zaitzala,
Erregina,
errukitasunezko Ama.
Bizitza, gozotasun eta gure ustea.
Jaungoikuak gorde zaitzala.
Zure deiez gaude,
Ebaren ume erbestetuok.
Zugana gaude nahi gabez,
samintasun eta negarrez,
negarrezko ibar honetan.
Ea, bada, Señora,
gure bitartekoa,
itzuli itzazu gugana
zure begi xamur goxo hoiek.
Eta erbeste honen ondoan,
erakutsi egiguzu Jesus,
zure sabelak emandako Bedeinkatua.
O! Bihotz bera.
Ama laztana.
Ama maitea,
gozo Maria,
otoitz egizu gugatik,
Jaungoikoaren Ama ona,
iritxi dezagun Jesukristok agindua.
Amen Jesus.