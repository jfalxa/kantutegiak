---
id: ab-4271
izenburua: Dama Gazte Galant Bat
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004271.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004271.MID
youtube: null
---

Dama gazte galant bat
gure probintziyan,
alabatzera noa
eginal guziyan;
lenago gabiltzanak
Jaunaren graziyan,
orain paratu gaitu
kolera biziyan,
tratabiria on da
saldu erosiyan.