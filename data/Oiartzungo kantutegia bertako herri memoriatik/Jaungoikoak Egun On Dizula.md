---
id: ab-4333
izenburua: Jaungoikoak Egun On Dizula
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004333.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004333.MID
youtube: null
---

- Jaungoikoak egun on dizula, Andre Brijida.
Aspaldian ez zaitut ikusi horren goiz jeikia.
Beti bezela dakarzkizu kolore gorriak...
- Ez dira, bada, eman dizkidalako gosari txikiak.
- Neroni ere baraurik nator; kulpak dituzte ardiak.
- Buenos días tengan ustedes: Señora cómo lo pasa usted?
- Osasunakin... gaztelaniaz yo no sabe.
- Goizian goizik etorria naiz zutaz enkontratzera
Prezisua da nik zure alabakin ezkontzia.

- Nere alaba bihotzekua: aizan arapiko!
Hartzan kopa bat, kopa bat hartzan, tratua egiteko;
holako propoziorik ez dun nolanahi galtzeko.
- Ama neria bihotzekua: ez naz horrekin ezkonduko.
- Zer aitzen dinat, arapiko!
Ez haizela horrekin ezkonduko?
...goiko larrian bi ardi antzu ta hiru artantxo;
beheko soruan baria abasto...
Ez ahal da asko
gu bizitzeko.