---
id: ab-4397
izenburua: Oiartzungo Festak
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004397.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004397.MID
youtube: null
---

San Esteban egunez
Oiartzun'go plazan,
nai zuenak ikusi
festa franko bazan;
bei bat atera zuten
zezenaren trazan,
asko korritzen zuan,
oso azkarra zan.

Plazarako lau zezen
eskatu erriyak,
iru zezen da bei bat
ziran ekarriyak;
jendia arritu du
ariyen korriyak,
astindu zitun ango
bandera gorriyak.

Plazara aterata
asi zan saltoka.
inguratzen zanari
segitu ta joka;
ez da ona izaten
alako epoka,
nik ez nuen asi nai
arekin borroka.

Festa onak zirala
emanikan traza,
hamabost minutuan
an bete zan plaza;
egoten al gerade
kontentu edo lasa,
desgrazia aundiyagorik
eztanian pasa.

Aurten Oiartzun ontan
izan da festatik,
ttunttuna alde batian,
musika bestetik;
bazan abundantziya
neskatxa gaztetik,
aumentua eldu da
senar-emaztetik.