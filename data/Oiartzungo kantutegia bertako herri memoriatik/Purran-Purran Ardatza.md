---
id: ab-4408
izenburua: Purran-Purran Ardatza
kantutegia: Oiartzungo Kantutegia Bertako Herri Memoriatik
partitura: null
midi: null
youtube: null
---

Purran-purran ardatza,
astian-astian mataza;
hilian hilian pieza...
Non da nere pieza.