---
id: ab-3896
izenburua: Beneditzioneko
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Goure Jaona, ostia saintu hontan
Gorderik gizon guzien hounetan,
Bihotz umil batez dezagun adora
Eta egiazki, maitha houra bera. (bis)

Jesus houna, amorioz bethia,
Zu zirade ororen hazkurria.
Ah! betha gitzatzu beneditzionez,
Eta bethi lagunt zoure houn guziez.

Ziek orok, ainguru gloriousak,
Gourekechi ematzie eskerrak,
Orai eta bethi, Trinitatiari,
Hainbeste ukhen dugulakoz fabori.