---
id: ab-3901
izenburua: Arima Konbertitu Baten Sendimentiac Khurutchiaren Aitzinian
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Ene arrerosteko
Kruzifikatu, zirena.
Ingrat niz zouretako:
Bena, othoi, pharka Jaona;
Ah! orhit zite odola
Or'ichouri duzula.

Ene gaztigatzeko
Sobera sujet duzuna.
Ingrat niz zouretako:
Bena, othoi, pharka Jaona;
Ah! orhit zite odola
Or'ichouri duzula.

Zoure maitetarzunak
Ezpanai, Jaona, salbatzen,
Nihaoren bekhatiak
Suiala nizu egoichten;
Lotseria handiak
Hanitch nizu duluratzen.

Arrap.: Ah! Oriht zite, etc.

Behar nintzan behtatu
Ene kreazaliari;
Bena behatu nuzu
Gaichtoki kreaturari;
Nik merechi nikezu
Iferniala erori.

Deithu naizu ardura
Zoure grazia saintiaz:
Liluratu sobera
Nunduzun mundu faltsiaz,
Ountsa konbertitzera
Herabe nizun egiaz.

Maite ukhen dereitzut
Munduko erhokeriak;
Orai ikhousten tizut
Haren ilusimentiak;
Sobera badakizut
Zer dadukan zekuriak.

Izan zakitzat ezti,
Zoure dikezut bihotza;
Nahi nikezu bethi
Gaztigatu ene beitha
Zeren beitu merechi
Ountsa zehadoi garratza.