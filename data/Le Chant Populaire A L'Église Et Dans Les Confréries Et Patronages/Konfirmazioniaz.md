---
id: ab-3897
izenburua: Konfirmazioniaz
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Ezpiritu Saintia
Jaitch zite goure gana,
Ezar goure bihotzak amorioz bethiak.

Zoure zazpi dohañak
Emagutzu guziak,
Bizi ahal gitian
Bethi zoure grazian.

Zuhurtziaz gutuzu
Mundiaz hastiotzen,
Gortzen eta indartsu
Berthutian bilhatzen.

Enthelegu argia,
Othoi, zuk emaguzu;
Har'k zeluko bidia
Erakasten dereiku.

Aholkiaz ountsala
Bethi boulka gitzatzu,
Bide zeiharretara
Zeren aiher gutuzu.

Emaguzu indarra
Buhurtzeko Satani;
Bide horrez begira
Haren pian jarterik.

Jakitate handia,
Salbamentiarena!
Dohañ horrez bethiak
Ezar gitzatzu guziak.

Zuk debozionian
Tink eduki gitzatzu;
Jaonaren zerbutchian
Zintzinez gorth gitzatzu.

Zoure beldur saintian
Bethi etchek gitzatzu;
Bide horrez mundian
Izan gitian saintu.

Egizu maitha zitzagun
Bethiere mundian,
Gero goza dezagun
Jinko Jaona zelian.