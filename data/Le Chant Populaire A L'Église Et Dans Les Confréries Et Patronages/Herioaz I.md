---
id: ab-3903
izenburua: Herioaz I
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Gizona noun duk zuhurtzia?
Zer! eztakik
Hugunt ezak mundu erhoa
Orai danik;
ah! eztuk hurrun herioa
Hire ganik.

Eztela deus ere bizia
Khebat baizik!
Hugunt ezak mundu erhoa
Orai danik;
ah! eztuk hurrun herioa
Hire ganik.

Munduko plazer erhoetan
Habilana,
Jinko Jaona hire gogoan
Eztiana,
Ohart emak hil behardela
Eta bertan
Oren bat segurki eztela
Mundu hontan.

Aberatsa, hi diharriak
Hai utsutzen,
Mundu hountak' utchur' izunak
Enganatzen,
Laster hire plazer maitiak
Tuk galduren,
Jinkoari hire khountiak
Tuk emanen.

Gaztiak, eztuk zeren ari
Khorpitz horren
Manier jarraikitzen bethi,
Bai edertzen.
Herioa gaztetarzunaz
Duk trufatzen,
Gazteriari jatzartziaz
Duk gozatzen.

Tratulant izabaz gosia,
Deusek ere
Mundu hountan ezin asia
Behinere,
Sarri hozia duk ukhenen
Ostatzutzat,
Diharu guziak eitziren
Besterentzat.

Laboraria, aoherretan
Hiz nekatzen:
Herioa houste gabian
Zaik hullantzen;
Ereitzetan egiteko lanak
Tuk hiretzat,
Eta haien frutu ederrak
Primientzat.

Bat etzaio herioari
Ezkapiren,
Zorrozki zaio bakhoitzari
Jarraikiren:
Bilhaturen tu erregiak
Alkhietan,
Hala noula jente chehiak
Etcholetan.