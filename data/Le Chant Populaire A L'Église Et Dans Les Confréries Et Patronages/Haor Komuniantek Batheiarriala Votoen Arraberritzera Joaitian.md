---
id: ab-3898
izenburua: Haor Komuniantek Batheiarriala Votoen Arraberritzera Joaitian
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Bagoatzu batheiuko uthurri saintiala.
Khiristitu gineneko votoen berritzera.
refrain
Arnegatzen dut
Bihotzez dut Satani,
haren solaz erhouk eizten,
Zelian, lurra, enetzat hartzen.

Sortzian ekharri nian
Arrest haren haosteko,
Hitzeman dut Bathei-ian
Jaonari amorio.

Arrap: Arnegatzen dut, etc.

Hitz handi horren ondotik
Apheza zen mintzatu:
Hurrunt zedin eneganik
Satan zian manhatu.

Khurutchiaren señalez
Zeitaden erakatsi
Behar niala khurutchez
Zelia irabazi.

Gatzak zeitan señalatu
Saintuki bizitzia,
Behar niala maithatu
Goizdanik zuhurtzia.

Tenplu saintian sartzera
Nundien gero deithu,
Jaonari kontsekratzera
Hain sarri khumitatu.