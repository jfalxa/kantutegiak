---
id: ab-3895
izenburua: Ama Birjinaren Bestez
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Salutatzen zutut Maria,
Grazia oroz bethia,
Jinkoaren ama maitia,
Eta Birjinen gloria.

Zu erregiña ama zira,
Errege zoure semia;
Halaz zelietan zu zira
Photere guziz bethia.

Orai eta bethi izan da
Jinko handia zoureki,
Zu hortakoz izanen zira
Dohatsu deithia bethi.

Zoureganik sorthu izan zen
Gizoun Jinkoa lurrian;
Jinko Aita ganik jiten den
Seme Jinkoa zelian.

Zu ber ordian ama zira
Bai eta ere Birjina:
Zu izanen zira bakhoitza
Ouhoure hori diana.

Beraz Birjina dohatsia,
Hounki aiphatu zirela;
Halaber zuk ukhen frutia
Hounki aiphatia dela.

Dona Maria, zuk zelian
Gugatik othoi egizu;
Orai eta goure hiltzian,
Bethi balia zakizku.