---
id: ab-4632
izenburua: Charmegarria
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004632.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004632.MID
youtube: null
---

"Charmegarria zira eder eta gazte,
Ene bihotzak ez du zu baizikan maite.
Bertze zembait bezala, othe zira libre?...
Zurekin ezkontzeaz dudarik ez nuke.

"Churi gorria zira, arrosa bezala:
Profetak ere dira mintzo hola hola.
Araberan bazinu gorphutza horrela,
Irudiko zinuen... zeruko izarra.

"Oi maitea, zatozkit, plazer duzunean,
Nehork ikusi gabe, ilhun-nabarrean;
Lagun bat badukezu joaiteko bidean
Hark ezarriren zaitu trankil bihotzean".

- "Plazer eginen duzu, ichiltzen bazira,
Haur iñorantak hola trompatzen baitira;
Ez da enetzat ona holako segida...
Bertzalde zure baithan ez naiteke fida! .

- "Adioz beraz orai, ene arraroa,
Hori dela medio herritik banoa:
Bihotza trichte eta kechuan gogoa,
Bethi jarreikiren zait zur'amodioa".