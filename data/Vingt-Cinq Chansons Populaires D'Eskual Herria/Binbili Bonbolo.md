---
id: ab-4614
izenburua: Binbili Bonbolo
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004614.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004614.MID
youtube: null
---

Binbili, bonbolo, sendal lo
Akerra Frantzian balego
Astoak soindua, idiak dantza,
Auntzak damboria jo.

Dambori berri-berria
Donostiako ekarria
Bazterrak ditu perla churiak
Erdian urre gorri.