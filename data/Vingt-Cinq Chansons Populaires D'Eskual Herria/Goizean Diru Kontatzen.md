---
id: ab-4622
izenburua: Goizean Diru Kontatzen
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004622.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004622.MID
youtube: null
---

Goizean diru kontatzen,
Taberna dago plantatzen
Bazkari ona jokatzen,
Kafiak harturik, kopak edanik
Edarri fina jokatzen.
Egiñik ezdut ukatzen;
Orain ezpainak nekatzen
Zardina'zurra zupatzen.