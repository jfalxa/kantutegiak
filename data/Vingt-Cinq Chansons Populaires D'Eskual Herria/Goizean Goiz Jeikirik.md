---
id: ab-4636
izenburua: Goizean Goiz Jeikirik
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004636.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004636.MID
youtube: null
---

Goizean goiz jeikirik, argia gaberik
Urera joan ninduzun, pegarra harturik.
Tra la la la...

Jaun chapeldun gazte bat jin zautan ondotik?
Heia nahi nuenez urera lagunik?

Nik, ez nuela nahi urera lagunik,
Aita beha zagola salako leihotik.

Aita beha zagola ezetz erran gatik,
Pegarra joan zerautan besotik arturik.

Urera ginenian, bia musuz musu,
Galdegin zautan ere: zombat urthe duzu?

Hamasei...Hamazazpi orain'ez komplitu:
Zurekin ezkontzeko, gazteegi nuzu.

Etcherat itzultzeko, nik dutan beldurra,
Ez jakin nola pentsa amari gezurra.

Arreba nahi duzu ni erakuts zuri
Etcherat ethortzean zer erran amari?

Urtcho churi pollit bat, gabaz dabilana,
Hark ura zikindurik, egotu naiz, ama!

Dakigunaz geroztik zer erran amari,
Dugun pegarra pausa: gaitezen liberti.