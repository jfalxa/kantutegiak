---
id: ab-4618
izenburua: Trilili Eta Tralala
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004618.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004618.MID
youtube: null
---

Trilili eta tralala
Kantu guzien ama da.
Nik ogi eta singarra
Zuk idi baten adarra.