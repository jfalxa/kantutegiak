---
id: ab-4637
izenburua: Nere Maitia
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004637.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004637.MID
youtube: null
---

Nere maitia, charmegarria,
Dorain ederrez zirare betia.
Zuk egin afrontua
Sartu zait bihotzean,
Heldu baizait ardura gogorra.
Nik orai pena: zuri maitea
Demborak emanen du agertzera.

Trumpatu nauzu, choratu nauzu
Zure malura hortan egin duzu.
Zuk egin afrontua
Sartu zait bihotzean,
Heldu baizait ardura gogorra.
Hobe bat ustez, nauzu kitatu
Zure malura hortan egin duz.