---
id: ab-4628
izenburua: Aitak Et'Amak
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004628.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004628.MID
youtube: null
---

Aitak et'amak herritik, Oi! zugatik
Donibanerat igorri naute Arbonatik:
Ez izateko zurekilan entradarik.

Ez izan hortaz penarik, ez changriñik;
Amodioa fidalago da urrundanik
Maitia ez zaitut kitatuko sekulanik.