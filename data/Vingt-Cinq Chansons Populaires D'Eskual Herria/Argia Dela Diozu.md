---
id: ab-4630
izenburua: Argia Dela Diozu
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004630.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004630.MID
youtube: null
---

Argia dela diozu,
Gauerdi orain ez duzu:
Enekilako dembora
Luze iduritzen zauzu;
Amodiorik ez duzu,
Orai zaitut ezagutu.

Ofizialetan leia
Zure sinheste guzia?
Aitak eta amak ere
Hala dute gutizia:
Lehen bat et'orai bertzia:
Oi! hau penaren tristia!

Otia lili denean,
Choria haren gainean:
Hura joaiten du airean.
Berak plazer duenian:
Zur'et ene amodioa
Hola dabila airean.

Partitu nintzen herritik,
Bihotza alegerarik:
Arribatu nintzan herrian
Nigarra nuen begian:
Har nezazu sahetsian,
Bizi naizeno munduan.