---
id: ab-6044
izenburua: Choriñoak Kaiolan (Deuxième Version)
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: null
midi: null
youtube: null
---

Choriñoak kaiolan
Tristerik du kantatzen
Dialarik han zer jan zer edan
Kampora du desiratzen
Zeren Zeren
Libertatia
Zuñen eder den.