---
id: ab-4620
izenburua: Bethiri Uhartekoa
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004620.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004620.MID
youtube: null
---

Bethiri Uhartekoa,
Ah qu'il était bon chasseur.
Herbi bat sasi chokoan
Atcheman zuen par bonheur:
Hamabortz egun hil zela,
Ah qu'il avait bonne odeur.
Hartarik jatekotan
Il faut avoir un bon coeur.