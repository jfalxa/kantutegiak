---
id: ab-4635
izenburua: Jaiki Jaiki
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004635.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004635.MID
youtube: null
---

"Jaiki, jaiki, nere maitea,
Etzirare loz ase,
Zure ondoan dabiltzen gaichoak
Ez dirade loale.
Idek dazu bortaiño hori
Adiskadiak girare..

"Ez ez ezdut; ez idekiren
Gau ilhunian bortarik;
Barnian denak ezbaidaki
Kampokuaren berririk.
Zato bihar eta
Kausituren naiz bakarrik..

"Bihar etortzeko gaua da labur
Ilhargi ere beran du.
Pochuak ere hor dire eta
Nork eginen du bakea?"
"Nik eginen dut bakea eta
Bihar etorri maitia..

"Sala klaruan lotchatzen naiz'ta
Argia itzal bazinu...?"
"Zu othe zare
Ama Birgiñak egorri?"
"Behin betikotz nahi zaitut hartu
Nere penaren konsolagarri.".