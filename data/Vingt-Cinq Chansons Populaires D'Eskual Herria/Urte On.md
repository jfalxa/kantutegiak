---
id: ab-4623
izenburua: Urte On
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004623.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004623.MID
youtube: null
---

Dios te salbe; ongi etorri.
Gabon Jainkoak digula eta
Urte onean sar gaizela.