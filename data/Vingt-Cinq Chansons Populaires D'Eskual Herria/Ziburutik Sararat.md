---
id: ab-4631
izenburua: Ziburutik Sararat
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004631.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004631.MID
youtube: null
---

Ziburutik Sararat izatu naiz gana,
Neure le'nagoko maite batengana;
An arribatu gabe aitu nuen fama
Garda gazte batekin zela itzemana.

Maitia bizi zaite gazte.ta lorios,
Bai eta fidel izan, itzemanez geroz.
Ondoko urrikiak ez du balio deus;
Huntan kitatzen zaitut, maitia, adios.

Uso chori pollita, fin eta fidela,
Deus makurrikan gabe joan zaizkit gibela:
Nik ere erran dezaket nombeit badirela...
Nik ez bainuen pensatzen zu olakoa zinela.