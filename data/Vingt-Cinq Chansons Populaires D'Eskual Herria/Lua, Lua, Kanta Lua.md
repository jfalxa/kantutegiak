---
id: ab-4615
izenburua: Lua, Lua, Kanta Lua
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004615.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004615.MID
youtube: null
---

Lua, lua kanta lua
Zerutako Jaingoikua
Aur uneri emaziozu
Lau orduko lua.