---
id: ab-4634
izenburua: Chori Errechiñoletak
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004634.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004634.MID
youtube: null
---

Chori errechiñoletak
ederki kantatzen,
Bazter guztiak
ditu choratzen.
Bard'arratsian
Sasi batian,
Biga baziren:
Eder ziren
Charmant ziren
Manera oroz,
Bai eta elgarrez aski agrados.

Batto arra zuzun eta bertzia emia,
Ez mirakulu parekatzia.
Han, lehen ere
Dudarik gabe
Ikusi ez balu,
Arra hura
Ez zen joanen
Sasi hartara
Emia ez balitz jin bid'erditara.

Oi choriño gaicho heien beldurti handia
Ikustiarekin ihiztaria
Batto han gaindi
Hain inozentki,
Pasatzen zela:
Arra hura,
Beha jarri,
Eta hor, gero,
Sasian sartu zen, han gordetzeko.

Eta orduan beran, nontsu zen emia,
Sasi handian barna sartua:
Hor aphaindurik,
Ta lumaturik
Bere papoa;
Kukurusta
Harroturik,
Jarri zenean
Umez orhoit zen, ber'ohantzean.