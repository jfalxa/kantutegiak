---
id: ab-4627
izenburua: Bazterretik Bazterrerat
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004627.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004627.MID
youtube: null
---

Bazterretik bazterrerat
Oi munduaren zabala.
Ezdakienak erran lezake
Ni alegeran naizela.
Hortzetan dizut hiria eta
Bi begietan nigarra.