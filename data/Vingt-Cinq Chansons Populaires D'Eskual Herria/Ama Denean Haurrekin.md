---
id: ab-4613
izenburua: Ama Denean Haurrekin
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004613.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004613.MID
youtube: null
---

Ama denean haurrekin
Zandu behar du berekin.
Alabaño bat bezik ez eta
Hura motiko gaztekin.
Erranen zaitut norekin
Heraile seme batekin.