---
id: ab-4625
izenburua: Choriñoak Kaiolan
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004625.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004625.MID
youtube: null
---

Choriñoak kaiolan,
Tristerik du kantatzen:
Dialarik han zer jan zer edan,
Zeren, zeren
Libertatia zuñen eder den.

Kampoko choria,
So.giok kaiolari:
Ahal balin bahedi
Hartarik begir.adi,
Libertatia zuñen eder den.

Barda amets egin dut
Maitia ikusirik:
Ikus eta ezin mintza,
Ezta pena handia?
Eta ezin bertzia?
Desiratzen nuke hiltzea.