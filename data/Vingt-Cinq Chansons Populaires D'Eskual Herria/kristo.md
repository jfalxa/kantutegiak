---
id: ab-4640
izenburua: Kristo
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004640.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004640.MID
youtube: null
---

Kristo, guziek dezagun egun adora (bis).
Hauche da izarra agertzen dena;
Berri bat dakarke den handiena,
Jesus maitearen sortzearena.

Kristo guziek dezagun egun adora.
Hirur erregeak ditu gidatzen;
Jesus haurra gana dire hedatzen
Presentekin dute han adoratzen.