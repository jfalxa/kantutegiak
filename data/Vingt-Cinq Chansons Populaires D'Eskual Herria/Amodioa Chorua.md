---
id: ab-4633
izenburua: Amodioa Chorua
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004633.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004633.MID
youtube: null
---

Amodioa chorua dala
Mundu guztiek badaki,
Nik maitetcho bat
Bakarri zan'ta
Ura bestek eramaki ez nuke penik
Bereko balitz
Neureko baino hobeki.