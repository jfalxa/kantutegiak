---
id: ab-4629
izenburua: Urre Erreztun Bat
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004629.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004629.MID
youtube: null
---

"Urr'erreztun bat badut nik
Neure maitiak emanik."
"Nun ezdutan hura ikusten
Zure erian sarturik
Neure bihotzak eztu izanen
Sekulan deskansurik".