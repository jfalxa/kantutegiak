---
id: ab-4617
izenburua: Zerri Tchikia
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004617.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004617.MID
youtube: null
---

Zerri tchiki bat erosi nuen
Bizi moduan sartzeko.
Iru peseta pagatu nuen
Jenero ona zelakotz.
Ate zilotik pasa da eta
Ezda ageri geiago.