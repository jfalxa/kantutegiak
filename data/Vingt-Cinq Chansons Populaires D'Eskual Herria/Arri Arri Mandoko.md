---
id: ab-4616
izenburua: Arri Arri Mandoko
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004616.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004616.MID
youtube: null
---

Arri, arri, mandoko,
Chari Iruñarako
Handik zer ekarriko?
Zapat eta gerriko.
Hek oro norendako?
Mutil eder horrendako.