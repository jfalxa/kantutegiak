---
id: ab-4621
izenburua: Aldapeko Sagarraren
kantutegia: Vingt-Cinq Chansons Populaires D'Eskual Herria
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/004621.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/004621.MID
youtube: null
---

Aldapeko sagarraren
Adarraren, puntiaren puntian
Choriñoa zeguen kantari
Eta liralirali (bis)
Zeñek da danzatuko soinu ori ongi.