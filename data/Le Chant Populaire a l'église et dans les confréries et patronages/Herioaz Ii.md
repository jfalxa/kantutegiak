---
id: ab-3904
izenburua: Herioaz Ii
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Ala beita ikharagarri
Kriminel baten hiltzia!
Adio dero mundiari
Erraiten, esteiaria!
Joaiteko daunatieki
Sekulakoz erratzera.

Ezpeititake deusek ere
Alagera ahal dezan;
Eztuzu Jaonaz behinere
Orhitu behar ordian:
Bere plazerak bethi ere
Zutian bere gogoan.

Igaranak du dezolatzen,
Zertarik beitu sobera.
Zerbait hounik du ez okhousten
Egin diala sekula.
Herioa aldiz hullantzen,
Hari ezin buhurtzenda.

Azken orenaren jitian
Haren beldurra sordeisten:
Milakaz dutu bekhatiak
Bere bekhatiak khountatzen;
Eta haien athe handiak
Bihotza deio iresten.

Maradikatzen tu plazerak
Hartu zutinak mundian;
Hez eman behar den khountiak,
Jar erazten du ikharan.
Ukhenen dian sententziak
Harritzen du ber ordian.

Bere begiez iferniak
Ikhousten dutu zabaltzen:
Hanko zorthe lotsagarriak
Osoki du duluratzen,
Suairen thurmentu handiak
Noula tian egariren.

Aithortzen du zinez ordian
Ountsa nahi ikhen balu
Elizatiala mundian
Bestelakorik kobratu;
Bena lehentche behar zian
Horiez oroz orhitu.

Ihourk eluke desiratzen
Holako hilze tristia;
Has gitian hounki egiten,
Denboraz ountsa balia:
Bekhatian dena bizitzen
Ah! Bekahtian hiltzen da.

Ahalaz indarka gitian
Jaon hounaren maithatzera;
Bekhatier goure bizian
Bethiere buhurtzera;
Jaonareki gero zelian
Irous izanen beikira.