---
id: ab-3902
izenburua: Kobesioniaz
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Persouna batek, beste orduz,
Egin zian bekhatu ichilian;
Elas! haren ez kobesatuz
Egun dago, egun dago, ifernu beltzian.

Kontzentziaren libratzeko
Zer egin zian? Sarthu komentian,
Oustekeriatan aisago
Houra aithort kobesionian.

Bena ahalke malerousak
Utsuturik, gogatzen du ordian,
Ahatzeren zerola Jaonak,
Bihotzetik dolumen bazian.

Hañ ountsa zen han kobratzen,
Noun berthutek merechitu beitzeren
Houra guziek haita lezen
Komentuko seroren gehien.

Hiltzerakoan ver gisala
Ahalkian utsutu ukhen zian;
Elas! Hil bizi zen bezala,
Egoitchirik izan ifernian.

Hantik lester jarri zirezen
Komentuko seroren artian;
Nigarrez han erran zirezen
Daunaturik zela ifernian.

Kobesatu niz hainbestetan,
Eta bethi bekhatu bat ukhatu;
Dohatsu nintzate zelian,
Houra banu ordu zen'aithortu.

O, ahalke galgarria!
Enganatu naik, bai, enganatu
Dohatsu hi goitzen haiana,
Zeren beita izanen salbatu.

Bekahtien aithortu izanez,
Egun da hanitch dohatsu zelian;
Aldiz, bat gorde ukhenez,
Ni galdurik su bethirekoan.

Adio, sekulakoz, adio;
Banoa ifernian erratzera.
Ziraie ziek zuhurrago,
Eztuzien orok ver zorgaitza.

O bekhatugil' ahalkorra,
Aithor itzak hire falta guziak,
Ukhen eztitzan thurmentiak
Bethiere irañen direnak.