---
id: ab-3899
izenburua: Eguberriz
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Khanta beza bitoria
Boztarioz mundiak.
Phorrokatu du buria
Heren suge handiak.
Gaitza zen haren aidia,
Bena goithu Mesiak;
Ikharatu ifernia
Salbazale sorthiak.

Haor handibat da jaiotu,
Alagera gitian;
Har'k bere parerik eztu
Zelian ez lurrian:
Agertu balitzeiku
Jinkoren urhatian
Nourk etzereion eginen
Batzarre houn lurrian?

Elas! goure Jinko Jaona,
Establia batetan
Jaiotu behar zinena
Animalen artian?
Guk merechi guniana
Hola aphal zentian?
Ala betzaizu gizona
Hanitch khosta mundian.

Zelietan zunialarik
Hanitch khorte zibile,
Aingururik ederrenac
Zouri khorte egile,
Guk merechi gabetarik
Gizon egin zirade,
Bekhatore ginelarik
Hartu zoure aorhide.

Mundian zen jaoregirik
Haituz ederrenian
Sorthu zinatian,
Printze kalitatian;
Borda tcharbat irekirik
Haitatu'zu mundian,
Goure zorrez kargaturik,
Sorthu aberen artian.

Adam goure lehen aita
Luziferrek trompatu;
Egin zian bekhatia
Ezin erreparatu.
Laor mila ourthez etzen ihour
Pharadusian sarthu:
Azkenekoz Jesus haorrak
hanko borthak zabaltu

Merechian pheretchatu
Bihotzez gabezia,
Ihourk ere eztu haren
Demendren tendrezia:
Aldiz Mesiak haitatu
Hartan bere sortzia;
Aiei! eta nourk behardu
Maite zikhoitzkeria!

Zelietan ainguriak
Urgulliak utsutu
Zeren nahi izan diren
Jinkoa uduritu;
Mundu hountan dabilana
Nahi bada salbatu
Ikhousirik bere hutsak
Behar du umiliatu.