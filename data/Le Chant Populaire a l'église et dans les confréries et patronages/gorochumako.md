---
id: ab-3900
izenburua: Gorochumako
kantutegia: Le Chant Populaire A L'Église Et Dans Les Confréries Et Patronages
partitura: null
midi: null
youtube: null
---

Iratzar hadi bekhatoria,
Orai duk, orai ordia.
Gogomak, gaichoa,
kontre duk Jinkoa;
Ordu diano egin itzak
Harekilako bakiak.

Orai heltu duk Gorochuma,
Egin ezak penitentzia;
Kobesa gogotik
Bekhatiak osoki,
Eta gero erresoli
Ez utzulzera jagoiti.

Arartecari ezagut ezak
Andere Dona Maria,
Aithortken diala
Min handirekila,
Hanitchetan Jinko Jaona
Hik ofentsatu diala.

Igain hadi mendigaña
Kalvario saintiala:
Ikhousiren duk han
Jesus khurutchian,
Odoletan sountsiturik,
Goure salbatzia gatik.