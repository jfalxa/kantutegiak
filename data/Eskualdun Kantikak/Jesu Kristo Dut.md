---
id: ab-3649
izenburua: Jesu Kristo Dut
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003649.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003649.MID
youtube: null
---

Jesu kristo dut nik nausitzat.
Horentzat ene bizia.
Giristino naiz bethikotzat,
Hori da ene loria.
Giristinoa naiz bathaioaz
Bekatutik garbitua;
Jaunaren grazia gozoaz
izan naiz aberastua.

Giristino naiz, eran beraz
Goraki mundu guzian,
Aita, Seme izpirituaz
Naizela hartua izan.

Giristino naiz, zure haurra,
O eliza, naiz egina;
Hautsiko dut heien herra,
Hartuz zure irakaspena.

Giristino naiz, desterruan
Bizi naiz hits eta triste,
Bainan gero zure zeruan.
O Jesus, menderen mende.