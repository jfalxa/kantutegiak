---
id: ab-3607
izenburua: Bozkario
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003607.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003607.MID
youtube: null
---

Bozkario, Bozkario,
Bozkario munduan
Dugun lauda Misterio,
Asmatua zeruan.
Gizonaren erosteko
Jainko gizon egina!
Gure zorren pagatzeko
Bera zordun emana.

Jainko gizon, gizon Jainko
Mirakulu handia!
Phitz dezagun sinhesteko
Fedearen argia.
Bethi dela adoratu
Haren Jainkotarsuna
Mundu guzian laudatu.
Haren gizontarsuna.

Jaunen, jaunaren sortzeko
Nunda hiri ederrik?
Zoinda haren ikusteko
Aski egun argirik?
Eguberri gau bethean
Atzar mundu zoroa.
Bethleem hiri chumean
Adora haur Jainkoa.

Jesus ororen jabea,
Ororen beharretan.
Sasoinen manazailea,
Dago hotz ikharetan.
Guretzat esklabo deno
Nork ez du miretsiren?
Hola maite gaituena
Nork ez du maitaturen.