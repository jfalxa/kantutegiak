---
id: ab-3650
izenburua: Maiatza Da
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003650.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003650.MID
youtube: null
---

Maiatza da urtheko
Sasoin ederrena,
Amaren laudatzeko
hautatzen duguna.
Lore berezienak
Eman diotzogun
Bihotz khartsuenak
Ofreiturik lagun.

Sasoin berrian oro
Dire maitagarri:
Bainan komparatzeko
Zer da Mariari?

Eder da iguzkia
Agertzen zeruan;
Ederrago Maria
Jarria munduan.

Loriaz da agertzen
Goizeko argia;
Bainan du ahalgatzen
Mariak iguzkia.

Oihuz liri churia
Dago haranean;
Ama zein den garbia
Guri nahiz erran.

Humilki brioleta,
Nahi da mintzatu.
Ama zein den modesta
Guri erran nahi du.

Arrosa mudatzen du
Tempesta gogorrak;
Amodioa ez du
Nihoiz galtzen Amak.

Arrosa zabalduak
Dauku seinalatzen
Birjina amultsuak
Zein maite gaituen.

Bertuten iduria
Da lorea landan,
Bertuteak, Maria,
Ematzugu gu baithan.

Gutarik urrun zatzu
Atsegin galkorrak,
Begiak iduk zatzu
Gu ganat hedatuak.

Iguzu baltsaturik
Zeruetan bozak
Detzagun kanta bethi
Zure ontasunak.