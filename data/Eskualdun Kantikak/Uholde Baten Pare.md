---
id: ab-3635
izenburua: Uholde Baten Pare
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003635.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003635.MID
youtube: null
---

Uholde baten pare orai bekhatuak.
Gaindiz bazter guziak ditu hondatuak.
Nun dire fededunak? nun dire justuak?
Azken eguna hurbil othe du munduak!
Othoi, Ama maitea,
Urrikal zaizkigu,
Jainko zure Semea
Hasarre baitugu.

Nor da gure artean erran dezakenik?
"Ez naiz ni deusen beldur, nik ez dut hobenik."
Zuk dirozu, Maria, hain garbi izanik,
Gure grazi'adiets Jainkoaren ganik.

Gizonen arbuioek aserik Jainkoa,
Bere azotearen higitzera doha.
Nork orai atchik liro Jaunaren besoa?
Zu zare zu, Maria, indar hartakoa.

Jaunaren justiziaz ikharetan gaude...
Othoi, Ama, jat zaite zure haurren alde.
Haren gana nor hurbil, nor ager zu gabe?
Zu zaitugu zu haren bihotzaren jabe.

Jauna, aithortzen dugu: gutarik hainitzi
Zuri zer zor dautzugun zitzaikun ahantzi.
Gure Fedea dugu aphaltzerat utzi..
Eskualdunaren ganik hori zaitzu gaitzi!...

Lehengo Fede hura berritz phitz-dakigun,
O fededunen Ama, zuk gaitzatzu lagun.
Bai! Eskualdunek hemen hitzeman dezagun
Bethi zinez izanen garela fededun.

Elizaren argia Erroman hain handi;
Gaichtaginen nahia laite hil baladi...
Zuri gare, Maria denak oihuz ari...
Emozu bitoria Aita Sainduari.