---
id: ab-3624
izenburua: Zer Zorion
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003624.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003624.MID
youtube: null
---

Zer zorion, oi hau da fagorea!
Horra, horra zeruko Jainkoa.
Jautsia da gure aldaretarat
Guregatik sakrifikatzerat.
Zato, zato, ene Jesus maitea,
Zu zare, zu, ene ontasuna!
Oi zer dohain ezin przatuzkoa
Huna Jesus, ene zoriona
Huna Jesus, huna Jesus!

Sinhesten dut, ene Salbatzailea,
Zu zarela ostian emana,
Zeru gorenean zaren bezala
Majestatez guzia bethea.

Jesus ona, saindutasuna bera.
Nola izan ni zure egoitza,
Nola beraz bekhatore tristea,
Errezebi ene Jaun handia.

Zutan Jesus dut ene esperantza,
Ah! Zuk hazten dautazu bihotza.
Hazkartua zure gorphutz sainduaz,
Eta zure odol sakratuaz.

Zure oinetan emaitan naiz ahuzpez,
Urrikitan ene bekhatuez.
Aithortzen dut errebel bat naizela.
Orhoit zaite ene Aita zarela.