---
id: ab-3617
izenburua: Oi Arima
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003617.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003617.MID
youtube: null
---

Oi! arima, yainkoaz onhetsia,
Orhoit hadi badela lekhu bat
Hemen arras zeruko justizia
Satisfatu ez duenarentzat. (bis)

Othoi, Jauna! othoi! urrikalmendu
Othoi! Purgatorioko arimentzat,
Darotzugu galdetzen barkhamendu
Heien mundu huntako hutsentzat. (bis)

Borratua izanaren hobena
Ez ziaukuk Jainkoak barkhatzen,
Gaizkiari bethi zor zaion pena
Non ez dugu osoki pagatzen. (bis)

Hurbil hadi purgatoriotarat
Jainko botherea bera denak
Egin duen suzko leze hartarat;
Entzunen tuk auhenik minenak. (bis)

Oi! Menturaz, gure adiskideak
Gure gatik dire punituak;
Gure hauzo, burhaso, ahaideak,
Gure faltaz, sutan hondatuak. (bis)

Zuek, gure adiskide maiteak,
Diote maiz, nigar marrasketan,
Othoitz, barur eta karitateak
Egitzue gure fagoretan. (bis)

Othoi, Jauna! othoi urrikalmendu
Purgatoriotan daudezenez:
Darotzugu galdetzen barkhamendu
Heien mundu huntako hobenez. (bis)

Bara zaite hekier jazartzetik;
Gehiago ez utz hek erretzen:
Ah! has beitez, atherarik su-petik
Zutaz, Jauna, zeruan gozatzen! (bis.