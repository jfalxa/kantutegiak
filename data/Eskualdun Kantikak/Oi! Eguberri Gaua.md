---
id: ab-3609
izenburua: Oi! Eguberri Gaua
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003609.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003609.MID
youtube: null
---

Oi! Eguberri gaua,
Bozkarioz- gaua,
Alegeratzen duzu
Bihotzean Kristaua.
Mundu guzia zorionez bethetzen,
Zeren zuk baidiozu Mesias dela sortzen.

Gau ilhunean ez da
Ikhusten iguzkirik;
Huntan agertzen zaiku
Mirakuiluz ilkhirik.

O! gau desiratua,
Argia dizu phiztu,
Zeinak mundu guzia
Behar baitu berritu.

Argizagiak dire
Bozkarioz dantzatzen,
Dutela ohi baino
Gehiago argitzen.

Aingeru onak berriz
Hasi dire kantatzen,
Bosterat dituztela
Tristeak gombidatzen.

Errepikatzen dute
Maniuterez artzainek,
Hekieri diote
Ihardesten larrainek.

Arrainak hari dire
Jauzika ur barnean,
Abreak mendietan,
Hegastinak airean.

Gauza guziak dire
Orai alegeratzen;
Orobat egiterat
Gaituzte erakhartzen.

Ifernuko ostea
Gau huntan da tristatzen
Haren zentzatzailea
Zeren den orai sortzen.