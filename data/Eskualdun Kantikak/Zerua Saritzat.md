---
id: ab-3616
izenburua: Zerua Saritzat
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003616.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003616.MID
youtube: null
---

Zerua saritzat,
Har kuraje Kristauak,
Hemengo nigarrentzat
Derauka Jainkoak.
Zerua, zerua,
Zerua saritzat. (bis)

Zerua saritzat,
Salba beraz arimak;
Utzi mundutarrentzat
Atsegin galkhorrak.

Zerua saritzat,
Hots bada, o gaztea,
Dezazu bethikotzat
Maita berthutea.

Zerua saritzat,
Hots bada aita amak,
Egin zuen haurrentzat
Othoitz kartsuenak.

Zerua saritzat,
Ikhus zuen adinak,
Iragan demborentzat
Egin nigar minak.

Zerua saritzat,
Nola ez bozkaria?
Dugu sarri guretzat
Bethiko loria.