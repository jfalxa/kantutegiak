---
id: ab-3606
izenburua: Gau Erditan
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003606.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003606.MID
youtube: null
---

Gau erditan aingeruek
Betbetan dute kantatzen,
Eta aldeko mendiek
Baltsan dute ihardesten:
Gloria Jaungoikoari. (bis)

Errazue, artzain onak,
Norentzat besta horiek?
Nork tu merezitu kantak,
Zertako kantu horiek?

Horiek dute erraiten
Jesus Haurra dela sortu.
Behar garela erraiten
Alegrantzietan egun sartu.

Elgar aditurik goazen
Mesiasen ikhusterat,
Jesusi gure bihotzen
Eta bozen eskaintzerat.

O Jesus Salbatzailea,
Orok zaitugu laudatzen!
Jesus gure erregea
Guziek zaitugu deitzen.