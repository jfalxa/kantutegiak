---
id: ab-3640
izenburua: Agur Ama
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003640.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003640.MID
youtube: null
---

Agur Jesus maitearena,
Ametan zu zare eztiena;
Zer zorion Zure izaitea
Zer loria zure maitatzea!
Birjina Maria, noiz bada.
Ni zure lorian, zeruan?

Haur gaicho bat nahigabez hartua,
Den bezala amaren alderat
Lehiatzen; gu bihotza urtua
Gare heldu zure oinerat.

Bidean dohanari ilhargiak
Ilhunbeak diozka argitzen,
Arimetan penarik ilhunenak
Hola tutzu eztiki suntsitzen.

Ez da deusik barnea darotanik
Hambat ere pizten eta bozten,
Ez atsegin ez eta ontasunik
Nola zuk nauenik deskantsatzen.

Ziten beraz, Ama ohoratua,
Zeru lurrez bethi laudatua
Zarelakotz gure ihes lekua,
Bethidanik gure kontseilua.

Hel bada, hel, zu gure esperantza
Hel bada, hel, oi gure laguntza;
Ah! akhabo, bertzenaz ifernua
Laster daite gutaz nausitua.

Zaude beraz, oi Birjina Maria!
Zeruetan bathi atzarria;
Egiguzu Jesus ganik grazia
Dugun segi guk zure bizia.