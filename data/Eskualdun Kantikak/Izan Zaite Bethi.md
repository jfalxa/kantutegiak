---
id: ab-3632
izenburua: Izan Zaite Bethi
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003632.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003632.MID
youtube: null
---

Izan zaite bethi Jesus maitea
Nere bihotzaren Jaun'ta jabea.
Ez da zeruruko bertze biderik.
Jesusen Bihotza bezalakorik.

Bihotz hori dago bethi zabalik,
Bekatorosentzat hetsi gaberik.

Gure bihotz denak, bihotz tikiak,
Handitu detzala zure graziak.

Zer gauz'ederra den Jesus hartzea
Zurekin gu, Jesus, bat egitea.