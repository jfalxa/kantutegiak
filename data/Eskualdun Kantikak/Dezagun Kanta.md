---
id: ab-3614
izenburua: Dezagun Kanta
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003614.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003614.MID
youtube: null
---

Dezagun kanta gogotik
Gaiten guziak boskaria;
Gure salbatzeagatik,
Jesus, eman duzu bizia,
Bai, bihotzaren erditik.
Dezagun kanta
Dezagun kanta
Dezagun kanta gogotik

Dezagun kanta gogotik
Zure odol preziatua
Ichuri duzu gu gatik;
Zaite, o Jesus, laudatua.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
Nahiz arimak zeruratu,
Leze beltz ifernukotik,
Jesus gaitutzu begiratu.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
Jesus dela lorifikatu;
Hark gaitu egunda inotik
Amodio choilez maithatu.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
O Jesus zure ohoretan,
Bozak errepikatzetik
Ez beitez bara bazterretan.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
Oi! hau da gure zoriona!
Jesusek, zeru goratik
Galdetzen gaitu bere gana.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
Deilha Jesus maitearenak
Behartu orai beretik,
Gure eskerrik handienak.
Bai, bihotzaren erditik.

Dezagun kanta gogotik:
Bethi zaitugu laudatuko
Gauza guzien gainetik,
Jesus zaitugu maitatuko,
Bai, bihotzaren erditik.