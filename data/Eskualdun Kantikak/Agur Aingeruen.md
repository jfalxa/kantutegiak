---
id: ab-3644
izenburua: Agur Aingeruen
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003644.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003644.MID
youtube: null
---

Agur aingeruen eta zeru lurren
Erregina puchanta!
Ederra zare Maria,
Isten duzu iguzkia,
Zein zaren distiranta!

Graziaren Ama,
Ama guziz ona.
Urrikalmendutsua!
Zein den espantagarria
Oi zer ihes lekhua!>bis.

Bethi on, amultsu
Kausitzen zare zu
Bekhatorearentzat;
Nola baitzare, Maria,
Ene gostuarentzat.>bis.

Penatuak diren
Haur dohakabeen
Kontzolazionea;
Bekhatoreen bizia
Loriak athea.>bis.

Zutaz maitatuak
Eta lagunduak
Ez ohi dire galtzen;
Deskantsaturik bihotza
Bethikotz dut emaiten.>bis.

Lur triste huntarik
Noizbeit atherarik,
Zu ganat hel gaitzatzu;
Zein den on eta gozoa
irakuts ezaguzu.>bis.