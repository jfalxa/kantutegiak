---
id: ab-3648
izenburua: Ama Guziz Puchanta
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003648.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003648.MID
youtube: null
---

Ama guziz puchanta
Nola zu goraki
Nola zu nik ez gorets
orai eta bethi!
Othoitz, othoitz egizu guretzat
othoitz, othoitz egizu guretzat!

Su hortarik gu baithan
Phitz zazu pindar bat:
Agian hel gaitezen
Gu ere zerurat!

Maita dezagun beraz
Ama bat hain ona,
Bekatoros guzien
Ihes leku dena!

Har gaitzatzu, oi ama!
Zure besoetan
Bai orai eta guziz
Azken orenean.