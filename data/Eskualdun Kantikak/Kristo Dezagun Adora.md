---
id: ab-3611
izenburua: Kristo Dezagun Adora
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003611.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003611.MID
youtube: null
---

Kristo guziek dezagun egun adora.
Hauche da izarra agertzen dena!
Berri bat dakharke den handiena,
Jesus maitearen sortzearena.

Hirur erregeak ditu gidatzen,
Jesus haurra gana dire hedatzen;
Presentekin dute han adoratzen.

Mirra, isentsua eta urhea
Ekhartzen dautzute, Salbatzailea,
Zuri eskaintzeko present bedera.

Gaspar Erregeak, Jainko handia,
Adiarazten du mirraz egia,
Gizona, zarela larruz beztia.

Baltazarrek emaiten urhea,
Bai, dio zarela, Jesus maitea,
Errege guzien lehen printzea.

Melkhiorrek ere du sinhestea
Dauku isentzuaz eman fedea
Egiaz zarela Jainko Semea.

Hitz batez hirurek fede osoa
Badute zarela Jaun zerukoa,
Gizona, Monarka eta Jainkoa.

Izar distirant bat heier egorririk,
Atheratzen tutzu ilhumbetarik,
Zure gana deitzen tronuetarik.

Ni ere argitzen nauzu fedeaz;
Egizu oraino heien chedeaz
Adora zaitzadan neure aldeaz.

Egun egor giuzu izar gurea,
Aurkhi dezaguntzat chuchen bidea
Zu ganat heltzeko, gure jabea.