---
id: ab-3661
izenburua: Zeru Lurren Erregina
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003661.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003661.MID
youtube: null
---

Zeru lurren erregina,
Bozkario!
Bozkario!
Phiztu dela Seme Jauna
Egia dario.
Jainkoak graziaz aphaindurik
Garbi zinauden.
Jesusen amatzat gaiagorik
Nehor etziteken!

Zu gainik athera Seme Jainko,
Gizon egina,
Berberak zionez bizi dago
Oi zer atsegina!

Urrikal, oi Ama Jainkoaren
Hain Bihotzpera!
Guziak gaitzatzu gaizkiaren
Hobitik athera!

Irakats urhatsa bethi hartzen
Kristau hartzen,
Orobat Phizturik sar gaitezen,
Ospetsu zeruan:

Zer atsegina gaur bihotzetan
Kristo phizturik!
Bethiko loria dugu izan
Ama, zur'eskutik.