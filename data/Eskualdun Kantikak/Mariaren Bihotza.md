---
id: ab-3645
izenburua: Mariaren Bihotza
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003645.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003645.MID
youtube: null
---

Mariaren bihotza
Lauda haurrak, egun.
O! Bihotz aberatsa,
Bethi maita zagun.
Bekhatore tristea
oihuz nago zuri:
Nitaz, bihotz maitea,
Izan urrikari.

Zuk duzu bekhatua
Bihotzatik khentzen,
Arima penatua
Duzu kontsolatzen.

Zuk duzu ifernua
Gerhaitzen osoki,
Segurtatzen zerua
Duzu justuari.

Mariñelen tempestan
Izar gidaria,
Arimaren gudutan
Zare sokorria.

Zutan bere egoitza
Jaunak du moldatu
Bai eta zure baithan
Templo bat hautatu.

Bihotz guziz puchanta
Nola zu goraki,
Nola zu nik ez lauda
Orai eta bethi.