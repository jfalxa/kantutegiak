---
id: ab-3618
izenburua: Zeru Gorenetarik
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003618.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003618.MID
youtube: null
---

Zeru gorenetarik mirakuilu handia,
Aitaren eskuinetik hemen da jautsia,
Jesus, Jesus.

Zeruko aingeruak lurreat jauts zaitezte
Hegalez estaliak adora zazue.
Jesus, Jesus.

Grazia dibinoak gutarat heda zatzu
Bekhatore guziak gomberti gaitzatzu,
Jesus, Jesus.

Zeruko Erregea ogiaren itchuran,
Guretzat estalia, dago aldarean.
Jesus, Jesus.