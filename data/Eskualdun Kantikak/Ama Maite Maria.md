---
id: ab-3637
izenburua: Ama Maite Maria
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003637.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003637.MID
youtube: null
---

Ama maite Maria
Eginguzu lagun,
Zure bitartez Jesus
Ikusi dezagun!
Aingeruzko lilia estimagarria
Usain gozoko lore,
Zerutik jautsia.
Gorde dezagun ama guziz maitatua.
Ezdezadan zikindu
On bide garbia.

Zure garbitasuna
Gizonek balute
Bertutetan maitena
Gora lezakete.
Othoi! Ama maitea
Lagunt zakizkigu,
Zu ganik sokorria
Ai! ethor bekigu.

Egizu Ama ona
Azken orenean.
Ordu lazgarri hura
Hurbilzen denean,
Izan dezagun orok
Jesus bihotzean
Zurekin hart gaitzatzu!
Zeruko lorian.

Gomendatzen nitzaitzu
Mundu ilhun huntan
Othoi! lagunt nazazu
Behar orduetan.
Ene azken orena
Hurbilzen denean,
Hart nazazu Maria
Besoen artean.