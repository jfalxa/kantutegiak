---
id: ab-3636
izenburua: Amodio Ohore
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003636.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003636.MID
youtube: null
---

Amodio ohore
Ama Birjinari
Maria notha gabe
Kontzebituari
Agur, agur,
Agur Maria. (bis)

Gure lehen aitamen
Bekhatuak ez du
Sekulan Mariaren
Arima goibeldu.

Bekhatu gabekorik
Adamen arrazan
Ez zu bezalakorik
Ez da nihoiz izan.

Maria zu hain garbi,
Hain saindu sorthua,
Zuk duzu sugeari
Lehertu burua.

Jaunak zu bethidanik
Zintuen beiratzen
Zure seme zu ganik
Sortzekoa baitzen.

Jainkoaren eskutik
Ez da atheratu
Obrarik hain ederrik
Nola baitzare zu.

O Birjina Maria,
Zer kompara zuri?
Elhur egin berria
Ez zait aski churi.

Zure baithan, Maria
Berthutea dago.
Mendiko ithurria
Baino garbiago...

Zutan gauza handiak
Jaunak egin ditu,
Eta haren graziak
Zaizkitzu gainditu.

Jaunaren baratzeko
Lore hautatua
Gizon da eta Jainko
Zure fruitua.

Emazte pare gabe
Jainkoak egina,
Amatuz geroz zare
Gelditu Birjina.

Oi zu, Birjina Saindu
Jainkoaren Ama!
Amatzat guri zaitu
Semeak emana.