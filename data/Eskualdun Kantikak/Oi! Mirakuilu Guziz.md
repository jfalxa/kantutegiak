---
id: ab-3625
izenburua: Oi! Mirakuilu Guziz
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003625.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003625.MID
youtube: null
---

Oi! Mirakuilu guziz Espantagarria!
Ogiaren iduriz Jesus estalia!
Hura dut adoratzen, Aldare gainean;
Hura bera dut jaten Komunionean.

Ene begiek Jesus han ez dute ikusten,
Gophutzeko sentsuek ez dute sentitzen;
Bainan duda gabe dut han dela sinhesten;
Fedeak argiturik, han dut ezagutzen.

Jesusek egin zuen azken afaria,
Janharitzat emanik bere haragia;
Haragi saindu hura jaten dut nik ere;
Hura janik uste dut bizi bethi ere.

Kurutzefikaturik, enetzat hil dena.
Lorioski phizturik, zerurat igana;
Hura da sakramendu hortan ezarria,
Hura bera lurrean ene janharia.

Biziaren arbola guziz aiphatua.
Baratze dohatsuan jaunak landatua.
Aurkitzen da neuretzat komunionea;
Nik han dut edireiten ene bizitzea.

Israelek jan zuen desertuan mana,
Gostu guziak bere baithan zituena;
Jesusen gorphutza da neure janharia,
Desertu huntan ene kontsolagarria.

Jainkoaren mendirat izan zen Eliaz,
Sainduki borthizturik jan zuen ogiaz;
Jaten dut aldareko ogi sakratua;
Hartaz nik ardietsi uste dut zerua.

Urerat du lehia orkhaitz nekhatuak,
Beroz eta ihiztariez betan penatuak;
Hala dut desiratzen, Jauna zure ogia:
Othoi sosega zazu ene gutizia!

Choriek badituzte beren ohantzeak.
Urzo ttorttoilek ere non eman umeak;
Zure aldaretan da ene iheslekhua:
Nik han dut edireiten ene sosegua.