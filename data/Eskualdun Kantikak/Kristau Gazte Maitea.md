---
id: ab-3615
izenburua: Kristau Gazte Maitea
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003615.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003615.MID
youtube: null
---

Kristau gazte maitea, zuretzat naiz mintzo.
Adizkitzu gogotik zembait solas gozo.
Zerurako bidea
Zaitzu irakasten,
Eman zaite gogotik, zu, haren ikasten.

Goiz danik behar duzu maithatu Jainkoa
Nahi baduzu izan bethiko gozoa.
Gazte gaichtoak dire gehienak galtzen;
Aldiz gazte prestuak zeruaz gozatzen.

Biziaren lorea da gaztetasuna,
Adin guzietan den prezagarriena:
Gaizkira makur bedi, ez daki non bara;
Ongira itzul bedi, ah! zein den suharra!

Gaztea, ibil hadi oraitik prestuki,
Mugarik ez denean ez dukan urriki;
Urrun lagun airatu mundutarretarik,
Nahi ez baduk hartu hekien kutsurik.

Onhets zak etchea, bizitze gordea;
Ichiltasuna, lana, hire eginbidea
Jarraiki othoitzari, irakurtzeari,
Irakurriak gogoan erabiltzeari.

Sakramenduetara hurbilduz arthoski,
Hire chede onetan egonen haiz tinki,
Hetan zaizkik emanen dohain bereziak
Errechki garhaitzeko hire etsai guziak.

Zuk, o Jesus maitea, gure Jabe ona,
Jende gazte prestuak hain maite tutzuna,
Egiazko prestuak bethi zatzu lagun
Bere chede onetarik ez daitezen urrun.

Gazte errebelak ere tutzu urrikari,
Heientzat ere duzu odola ichuri;
Suntsi etzatzu, Jauna, heien ilhumbeak;
Bai phorroska lothuak dauzkaten gatheak.