---
id: ab-3619
izenburua: Humilki Zaitut Adoratzen
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003619.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003619.MID
youtube: null
---

Humilki zaitut adoratzen
Ahuspez zure oinetan.
Jauna zuk nauzu ni laguntzen
Behar ordu guzietan.
Orai egidazu grazia
Bihotzaren zundatzeko:
Han den phozoin gaichto guzien
Lehen bai lehen khentzeko. (bis)

Ikhus detzadan ene faltak
Othoizten zaitut, Jainkoa!
Eta heien gogoa ikhuste klarak
Trista dezat gogoa.

Hurbildik etsaminatzerat
Egin ditudan gaizkiak.
Milaka zaizkit aitzinerat
Heldu neure flakeziak.

Gogoz, obraz, orobat hitzez
Hautsi tut zure manuak;
Itchurazko bere plazerez
Enganatu nau munduak.

Oi! urrikal nakizu, Jauna.
Higuintzen dut bekhatua.
Bakharrik zeren zaren ona,
Ez zeren den ifernua.

Leialki dut begiratuko
Orotan zure legea;
Ez naiz andik aldaratuko;
Hori da neure chedea.

Zembat diren urrikaltzeko
Gaizkian oi daudezenak!
Heientzat daude sekulako
Tormenta latzgarrienak.

Zer zuhurtzia orhoitzea
Ifernuko suzko kharrez,
Zeruaren merezitzea
Urrikiaren nigarrez.