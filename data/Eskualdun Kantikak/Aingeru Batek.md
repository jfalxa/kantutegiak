---
id: ab-3660
izenburua: Aingeru Batek
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003660.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003660.MID
youtube: null
---

Aingeru batek Mariari
Dio graziaz bethea.
Jaun Jainkoaren Semeari
Emanen duzu sortzea.
Agur, agur,
Agur Maria.
Agur, agur,
Agur Maria.