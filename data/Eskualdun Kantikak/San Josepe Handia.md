---
id: ab-3653
izenburua: San Josepe Handia
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003653.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003653.MID
youtube: null
---

San Josepe handia
Oi zure loria!
Jesusen ta Maria zure meneko dira!
Egin tutztezu zure manuak
Hartu kontseiak,
Zeruan lurrean bezala duzu ahala.
Galdetzeko manako,
Guzien ardiesteko.

Galdezazu grazia
Guziz berezia:
Heriotze saindua,
Eta Parabisua.
Jesus eta Maria ziaren
Zure kontsolatzen
Zuk azken hatsa emaitean
Bien besoetan.
Hiltze hori izan bedi
Emanu zu guzieri.