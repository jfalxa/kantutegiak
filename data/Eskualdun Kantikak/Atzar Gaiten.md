---
id: ab-3605
izenburua: Atzar Gaiten
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003605.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003605.MID
youtube: null
---

Atzar gaiten, atzar lotarik,
Gau huntan da Jesus sortzen:
Amodioak garhaiturik,
Guregatik da ethortzen!
Gu zerurat nahiz altchatu
Jesus jausten da lurrerat;
Heldu da, gu nahiz salbatu,
Gorphutz hilkor bat hartzerat!

Ala haren laudorioak
Behar baititugu kantatu?
Ala gure bozkarioak
Behar baitu seinalatu?

Bekhatua, munstro tristea,
Hoa hire leze beltzerat;
Ethorri duk Jesus maitea
Hire obren urratzerat.

Zerk gaitu bada gibelatzen
Hel gaiten hel Jesus gana;
Bekhatoren gatik da sortzen
Saindutasuna bera dena!

Erregen errege delarik,
Ez du nahi distiratu;
Ez du nahi palaziorik,
Heia tehar bat du hautatu.

Hotzaz ere da penatua
Sasoinen manatzailea;
Guziez daza gabetua
Guzien kreatzailea.

Amodioak du sustatzen
Haren bihotza guretzat;
Has gaiten has haren maitatzen,
Guduka gaiten harentzat.