---
id: ab-3620
izenburua: Oi Gurutzea!
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003620.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003620.MID
youtube: null
---

Zure besotan, oi Gurutzea,
Hil zaiku Jesus maitea:
Haren gurutze fikatzailea
Ni naiz ni bekhatorea.
Oi Gurutzea! oi Gurutzea! Gurutze saindu maitea. (bis)

Jesu Kristoren odolak zaitu
Galbarioan gorritu
Deusek geroztik munduan ez du
Zuk bezembat dirdiratu.

Bertzorduz hemen etsai gaichtoa
Zutaz izan zen ehoa:
Zure indarra da Jainkozkoa,
Deusek ezin hautsizkoa.

Etsaiarekin bethi guduan
Hemen gabiltza munduan:
Pharatzen bazaizkigu buruan,
Oi gu nork bentzu orduan!

Jesu Kristori zurekin baizen
Ez da nihor jarraikitzen:
Zure bidetik da bethi zuzen
Kristaua zerurat heltzen.