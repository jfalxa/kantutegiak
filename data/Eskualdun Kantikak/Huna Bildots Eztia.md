---
id: ab-3626
izenburua: Huna Bildots Eztia
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003626.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003626.MID
youtube: null
---

Huna bildotz eztia,
Egiazko ogia,
Zerutik da jautsia;
Dugun adora!
Da gur'Artzain ona,
Kreatzailea,
Aita hoberena
Salbatzailea.

Ostian zarela
Dugu sinhesten;
Zeruan bezala
Hor zu kausitzen.

Adoratzen zaitut
Jesus eztia;
Othoi emadazut
Fede bizia.

O Bihotz maitea
Deitzen gaitutzu;
Gutan egoitea,
Bilhatzen duzu.

Eskaintzen derautzut
Ene bihotza.
Othoi bero zazu!
Ezen da hotza.

Ene nahia da
Zu maithatzea,
Maithatzean baita
Choilki bethea.

Bethe zazu, Jauna,
Ene desira.
Nik nahi dudana.
Jesus, zu zira!

Zaitut maitatuko
orai lurrean.
Gero maithatzeko
Zure gorthean!

Zoin naizen flakoa
Ikhusten duzu!
Mana zerukoa,
Azkar nezazu!

Horra non heldu den,
Gaitezen ichil!
Prest egin gaitezen,
Hemen da hurbil.