---
id: ab-3631
izenburua: Oi Zer Zoriona
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003631.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003631.MID
youtube: null
---

Urrun zaitezte munduko plazerak
Urrun ene pentsamendutarik
Jesusekin tut atsegin bakharrak,
Ez, othoi, ni separa hetarik.
Oi zer zoriona ene bihotzean dagoena,
Oi zer zoriona, zuk emanik, ene Jesus Jauna.
Zoriona; zuk emana, Jesus Jauna.

Hurbildu naiz mahain zaindu hortarat,
Huna zer den orduan gerthatu.
Jesus jautsi da ene bihotzerat
Hartaz nahi duela jabetu.

Jesus nitan eta ni Jesusekin.
Biek bat dugu orai egiten.
Zer zoriona! Nahi nuke jakin
Jesusentzat amodioz urtzen.

Ene Jesus Jainko eta Gizona,
Jainko guziz maitagarriena;
Zarelakotz hor neretzat emana,
Zurea naiz ni ere naizena.

Jesus gu baithan delarik egoiten
Zer graziak deraizkun emaiten;
Gu ere hil eta lorios phitz gaiten
Bera zauku janhari emaiten.

Hobe bada Jesus ona gurekin
Komunionean bat egiten,
Giristinoak, artha handirekin,
Mahain saindurat hurbil gaiten.

Ez dugu, Jauna, zure ikhusteaz,
Baizen guk bertze plazerik;
Oraitik zurekin bat egiteaz
Mundu huntan bertze esirarik.

Lauda zuek, zeruko aingeruak,
Zuek ere, dohatsu guziak,
Lauda guziz, zuk, ô Ama Birjina,
Zure Semearen ontasuna.