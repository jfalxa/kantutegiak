---
id: ab-3641
izenburua: Goazen Mariaren
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003641.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003641.MID
youtube: null
---

Goazen Mariaren haurrak
Gure Amaren oinetarat;
Lehia gaiten guziak
Bihotzez hari erraiterat:
Agur, agur, agur Maria. (bis)

Gure guzien Ama da;
Beraz egungo besta hautan,
Dezagun goraki lauda
Kantatuz mil eta miletan.

Aingeruei junta gaiten,
Gure bihotzak sustaturik.
Hek dutena errepikatzen
Errepikatzen dugularik.

Zein puchunta den Maria,
Zeruetako erregina!
Haren medioz grazia
Jausten da bethi gure gana.

Hura da gure laguntza
Munduko behar guzietan;
Hura gure esperantza
Hirriskurik handienetan.

Maria bai hitz dautzugu
Zuri fidel gare egonen,
Zu zerbitzatuz ditugu
Gure urtheak iraganen.

Gure bizia dautzugu
Orai gogotik kontsekratzen;
Bethi, othoi, hel zaizkigu.
Zuk lagun gaitzatzu salbatzen.