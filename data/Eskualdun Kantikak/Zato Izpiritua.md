---
id: ab-3622
izenburua: Zato Izpiritua
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003622.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003622.MID
youtube: null
---

Zato, Izpiritua
Kreatzaile saindua,
Bisita zatzu hotzak,
Othoi gure bihotzak!
Bethe zatzu graziez.
Zerutik egorriez.
Gure adimenduak
Zeronek moldatuak.

Kontsolatzaile ona
Izendatzen zarena,
Dohain baliosena
Jaun zerukoarena.

Ithurburu bizia,
Amodio garbia,
Karitate khartsua,
Gantzukari saindua.

Fededunen bihotzak
Berotu tutzu hotzak,
Hek argitu dohainez,
Izpirituarenez.

Egor azu saindua,
Othoi, Izpiritua;
Hola mundu guzia
Egin azu berria.