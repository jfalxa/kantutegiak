---
id: ab-3610
izenburua: Airerik Ederrenetan
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003610.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003610.MID
youtube: null
---

Airerik ederrenetan }
Dugun orok betan. }(bis)
Kanta Jesusen sortzea
Haren zerutik jaustea
Ethorri da gure gana }
Oi zer zoriona! } (bis)

Zeruko aingerueri }
Eta saindueri } (bis)
Juntatzagun gure botzak,
Bai eta gure bihotzak.
Jesusen adoratzerat
Goazen Betlemerat.

Utz kitzue, utz, artzainak }
Zuen arthaldeak; } (bis)
Zuek zaituzte galdetzen
Pobreak tu gombidatzen
Errege sortu berriak,
Mesias maiteak.

Ez bilha jauregietan, }
Ez gazteluetan; } (bis)
Magnateran da etzana,
Hauche da humiltasuna
Deraukuna erakusten
Gure gatik sortzen!

Amodioz sustaturik, }
Urrikiz betherik, } (bis)
Dugun, egun saindu huntan,
Orok gure bihotzean
Errezebi Jesus Jauna,
Salbatzaile ona.