---
id: ab-3621
izenburua: Phiztu Da Lorios
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003621.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003621.MID
youtube: null
---

Phiztu da lorios Jesus hilen artetik!
Igan bitorios Zerua guregatik;
Eta ni munduan
Hila bekhatuan:
O heriotze tristea!
Bekhatututan bizitzea!

Nik ere nahi dut,
Jauna, zurekin phiztu;
Bethi nahi zaitut
Orai danik maitatu.
Uzten dut mundua
Arras bekhatua;
Ez, ez dut nahi bizirik
Zu zerbitzatzeko baizik.

Bekhatoros nintzen,
Ondikosko zorthea!
Orai naiz aurkhitzen
Zurekin, Jaun maitea;
Zu zaitut bilhatzen
Bakharrik maitatzen;
Biziaz zait gabetzea
Zure ganik urruntzea.

Zein naizen flakoa
Miletan dut frogatu;
Baiki, Jaun-goikoa,
Maiz zaitut ofentsatu.
Bethikotz naiz galtzen
Ez bazaizkit heltzen;
Mundua zait nausituren.
Laster nau eskuraturen.

On bada mundua
Zuri jarraikitzea;
Legez da zeruan
Hobe zu laudatzea.
Han sarturen dena
Da dohatsuena
Oi! noiz naiz harat helduko?
Noiz naiz zutaz gozatuko?

Usoa bezala
Noiz naiz hegaldaturen
Noiz naiz berehala
Zu ganat hedaturen?
Gaizkia uzteko,
Zurekin phizteko,
O Jesus! ene guzia,
Othoi, indazu grazia.