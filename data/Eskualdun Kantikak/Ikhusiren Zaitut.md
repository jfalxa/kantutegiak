---
id: ab-3639
izenburua: Ikhusiren Zaitut
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003639.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003639.MID
youtube: null
---

Ikhusiren zaitut Andre dena Maria;
Jesusen aldean zeruan jarria.
Zeru gorenean ikhusiren zaitut. (bis)

Ikhusiren zaitut amultsuki onhesten,
Agur bat lurretik zaitzunean heltzen.

Ikhusiren zaitut fededunen artean
Bihotzak beiratzen garbitasunean.

Ikhusiren zaitut Birjina Lurdekoa
Frantziaren dako othoizten Jainkoa.

Ikhusiren zaitut zeruan ama ona,
Ikhusiren zaitut oi zer zoriona.