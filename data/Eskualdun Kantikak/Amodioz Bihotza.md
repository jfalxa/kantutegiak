---
id: ab-3651
izenburua: Amodioz Bihotza
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003651.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003651.MID
youtube: null
---

Amodioz bihotza sustaturik,
Ama ona, huna zure haurrak.
Heda zatzu, othoi zure tronutik,
Amultsuki gu ganat begiak.

Errepika
Ama eztia, Ama ona,
Zu zare zu Maria,
gure zoriona.
Zu zare zu Maria,
gure zoriona!

Gure esperantza,
oi Maria,
Salbazazu Eliza
Bai eta Franztia,
Salbazazu Eliza
Bai eta Franztia!

Iguzkiak zalbatzen tu loreak.
Oi Birjina, zure ohoretan,
Nola iduk detzazkegu bihotzak
Zerratuak zure aintzinean.

Alferretan suge amarrutsuak
Errabian bilhaturen gaitu;
O Maria, zabaldurik besoak,
Amultsuki harturen gaituzu.

Bihotz onez bethi eginen dugu,
O Birjina, zuk nahi duzuna;
Ditugunak kontsekratzen daitzugu
Amodioz, gure Ama ona!

Bere Ama Jesusek eman dauku
Ararteko, oi zer fagorea!
Ama hura ohoratzen badugu,
Maitatua daiteke Semea.

Ai! Ez dugu orai bertze nahirik
Baizen bethi zure maitatzea;
Har gaitzatzu, eta iduk oraitik
Hiltzeraino, oi Ama maitea.