---
id: ab-3633
izenburua: Kristo Erregeri Agur
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003633.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003633.MID
youtube: null
---

Kristo Erregeri
Eskualdunek agur,
Kristo Erregeri }
Agur, agur. } (bis)
Itsaso, mendiak, ibai, zelaiak;
Zuretzat eginak, dira zureak;
Ureko uhainek,
Mendiko oihanek, Kristo, Agur!

Airean doatzan chori ederrek
Lurreko ihizi handi hazkarrek.
Pentzeko loreak, Zeruko izarrak,
Kristo, Agur!

Aingeru guzien Errege Jauna,
Errege're zira Gizonarena:
Nun da Erregerik zu bezalakorik?
Kristo, Agur!

Orotarik kampo emana zira...
Zato gurekilan Eskual-Herrira:
Zure gira oro, orai eta gero,
Kristo, Agur!

Lur hau oro zuri kendurik ere,
Zerua gelditzen zauzu halere.
Zeru hortarat gu, helaraz gaitzazu!
Kristo, Agur.