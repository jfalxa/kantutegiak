---
id: ab-3608
izenburua: Oi! Bethleem
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003608.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003608.MID
youtube: null
---

Oi Bethleem!
Ala egun zure loriak,
Oi Bethleem!
Ongi baitu distiratzen!
Zuganik heldu den argiak
Bethetzen tu bazter guziak
Oi Bethleem! (bis)

Zer ohore!
Ala baitzare goratua!
Zer ohore!
Zer grazia! zer fagore!
Zeruaz zare hautatua,
Jesusen zare sor lekhua!
Zer ohore! (bis)

Azkeneko!
Hor heldu da Jesus justua,
Azkeneko!
Gu dohatsu egiteko;
Hetsi nahi du ifernua
Guretzat zabaldu zerua,
Azkeneko! (bis)

Gure gatik!
Jainko bat botherez bethea,
Gure gatik!
Jausten da zeru goratik;
Bai, sortzen da Jesus maitea,
Biktima notarik gabea.
Gure gatik! (bis)

Maniateran!
Datza haurrik aberatsena,
Maniateran!
Nork zuken sekulan erran
Zeru lurren Jabea dena
Ikhusiren zela etzana
Maniateran! (bis)

Artzainekin!
Heldu naiz zu gana lehiaz,
Artzainekin!
Hek bezala nahiz egin.
Adoratzen zaitut, Mesiaz,
Eta maite bihotz guziaz,
Artzainekin! (bis)

Ez dut deusik!
O Jesus! zuri eskaintzeko,
Ez dut deusik!
Bihotz hobendun bat baizik:
Eskerren zuri bihurtzeko
Hambat emaitzen pagatzeko
Ez dut deusik! (bis.