---
id: ab-3652
izenburua: Ohore San Joseperi
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003652.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003652.MID
youtube: null
---

Ohi hilabe huntan
Zaitugu laudatzen
botzak ditugu betan,
San Josep altchatzen.
Ohore San Joseperi
Patroi botheretsuari.

Josep zare Jesusen
Aita lekhukoa
Bai eta Mariaren
Espos autatua.

Zeruan gora zare,
Oi saindu handia,
Eliza guziaren
Patroin berezia.

Zure garbitasuna
Gizonek balute,
Berthutetan maitena
Preza lezakete.

Zure humiltasuna
Zoin miragarria!
Zure iehiltasuna
Zoin laudagarria!

Zure urhatser bethi
Gare jarraikiko;
Ongi zoin denezti
Dugu frogatuko.

Ah! San Josep handia
Urrikal zaizkigu!
Esperantza guzia
Zure baithan dugu.

Guziz gaitzatzu lagun
Azken oronean;
Etsaiak zaitzu urrun
Gu ganik orduan.