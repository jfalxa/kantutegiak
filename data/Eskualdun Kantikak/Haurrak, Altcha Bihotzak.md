---
id: ab-3630
izenburua: Haurrak, Altcha Bihotzak
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003630.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003630.MID
youtube: null
---

Haurrak, altcha bihotzak zuen Jainkoari;
Himilia zaitetzte Jesus maiteari.

Ahuspez suspiretan eta nigarretan
Gaude hemen guziak Jesusen oinetan.

Huna non ethorri den Jaunaren eguna;
Zuen ganat heldu da Salbatzaile one.

Oi! Egun dohatsua ethorri zaukuna!
Nork dezake kompreni gure zoriona!

Othoiz zaitzue, haurrak egun handi huntan
Zeruko dohatsuak zuen fagoretan.

O! Birjina Maria, gure Ama ona,
Guretzat othoitz zazu zure Seme Jauna.

Ofentsatu tutzue zuen burhasoak,
Umilia zaitezte egun, haur maiteak.

Ah! Othoi, barkhamendu, aita eta ama!
Bekaturik gohazen Jainkoaren gana.