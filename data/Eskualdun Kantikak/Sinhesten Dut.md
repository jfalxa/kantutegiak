---
id: ab-3628
izenburua: Sinhesten Dut
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003628.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003628.MID
youtube: null
---

Sinhesten dut, sakramendu sainduan
Jesus ona, zu zarela ostian.
Sihesten dut, sinhesten dut,
Jesus Jauna, oi! Ene Nausa.
Amodioak zaitu hola ezeztatu.
Ni ere ordainez zurea naiz guzia.
Nahi zaitut bethi ere maithatu.

Jainko guziz puchant eta handia,
Ene bekhatuek naute ahalgetzen.
Errezebi zazu nik dudan urrikia,
Bihotzetik ditut oro hastiatzen.

Jauna, zure hitz bakhar bat aski da,
Bekhatutik arras gu garbitzeko,
Errazu hitz hura mediku dibinoa,
Aski izanen da gure sendatzeko.