---
id: ab-3627
izenburua: O Jesusen
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003627.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003627.MID
youtube: null
---

O Jesusen bihotz kartsua,
Zu zare nere gutizia;
Zure alderat sustatua
Izanen da bethi nere bizia.
Aingeruen amodioak,
Aingeruen amodioak,
Erre gaitzala guziak,
Jesusen ongi laudatzeko,
Maitatzeko benedikatzeko.(Bis)

Oi! Zu bihotz aberatsena,
Graziaren ithurria,
Hemen naiz, bihotz samurrena,
Zu ganat zerbeit bilha ethorria.

Oi bada zer erhokeria
Zuri ez jarraikitzea;
Bihotz bethi maitagarria,
Zu ez bethi ere zinez maitatzea!

Bihotz ihes leku segura
Kreatura guzientzat;
Amodiozko gaindidura,
Zer erranen dut, zer eginen zuretzat?

Othoi errezebi gaitzatzu,
Jesus, zure bihotzean;
Lagun, susta, maita gaitzatzu,
Orai eta bethi, eternitatean.