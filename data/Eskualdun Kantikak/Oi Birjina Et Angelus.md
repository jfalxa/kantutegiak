---
id: ab-3642
izenburua: Oi Birjina Et Angelus
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003642.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003642.MID
youtube: null
---

Oi! Birjina, Ama ona,
Entzun zatzu zure haurrak,
Senda zatzu, garbi zatzu,
Bekhatorosen bihotzak. (bis)
- Aingeru batek Mariari
Dio graziaz bethea,
Jaun Jainkoaren Semeari
Emanen duzu sortzea. (bis)

Garhaitia o Maria!
Satan beltzaren gainean
Emaguzu; ez zaitugu
Ahantziren guk sekulan. (bis)

Karitate eta fede
Esperantza biziena
Gure baithan dadin izan
Egizu, Ama ona. (bis)
,
Bihotzeko bake gozo,
Bertutearen fruitua
Emaguzu; iduk-azu
Gure baithan finkatua. (bis)

Ifernutik, bekhatutik,
Urrun gaitzatzu, oi Ama!
Bethi garbi, khartsu bethi,
Iduk dakuzu arima. (bis)

Heriotzeak gure arimak
Ez detzala bekhatuan
Betan altcha eta honda
Bethikotz suzko lezean. (bis)

Gure othoitzak, oihu minak,
Baldin entzuten baitutzu,
Korona bat zure haurrentzat
Zeruan prepara zazu. (bis)

A! orduan lorietan
Zure izen maitagarria
Laudatuko, kantatuko
Dugu bethi, oi Maria! (bis.