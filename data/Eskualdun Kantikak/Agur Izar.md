---
id: ab-3647
izenburua: Agur Izar
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003647.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003647.MID
youtube: null
---

Agur, Izar kontsalagarri,
Kristau argitzailea;
Birjina bethi maitagarri,
zeruetako athea.
Onhets zazu, Maria,
Gure hitz garbia;
Jesus dugu nahi Aitatzat,
Jesus gure erregetzat.
Jesus dugu nahi Aitatzat,
Jesus gure erregetzat!.

Misterio espantagarri,
Jainko da zure Semea;
Sabelean duzu ekharri
Ororen Kreatzailea.

Gabriel Aingeruarekin
Agur dautzagu erraiten;
Jaunen Jauna dela zurekin,
Orok dugu ezagutzen.

Lehen Amak zuen hedatu
Munduan heriotzea;
Munduari duzu bihurtu
Bizia eta bakea.

Ama urrikalmenduzkoa.
Gaude gu zure oinetan;
Zure bothere guzizkoa
Dugun senti beharretan.