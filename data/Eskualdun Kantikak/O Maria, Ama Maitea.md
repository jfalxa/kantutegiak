---
id: ab-3638
izenburua: O Maria, Ama Maitea
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003638.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003638.MID
youtube: null
---

O Maria Ama maitea
Beira zazu Eskualdun fedea:
Suhar eta khartsu, azkar egon bedi }
Hersturen erdian bethi. } (bis)
Gauden gu Eskualdun bethi.
Salutatzen zaitut, Maria,
O Ama guziz eztia!
Maite dut zurekilan Jesus
Gure Errege handia!

Zu Erregina Ama zira,
Errege duzu Semea,
Jainko Seme Erregearen.
Agur, Ama maitea!

Zu zaitu, zu Jainko handiak
Guzietarik hautatu,
Zu emazte guziak baino
Gehiago ohoratu.

Zure ganik sorthu izan da
Gizon Jainkoa munduan.
Jainko Aita ganik sortzen den
Seme Jainkoa zeruan.

Birjina, Jainkoaren Ama,
Aingeruen gortea,
Gizonen ere zare Ama,
Satanen bentutzailea.

Ez dut oraino behin ere
Mundu huntan nik aditu,
Zure haurretarik duela
Jainkoak abandonatu.

Gu bekhatore trichteentzat
Othoiz egizu zeruan:
Baitira lanjer handietan
Bizi direnak munduan.

Hiltzerakoan bereziki
Lagun gaitzazu Maria!
Zure bidez ardiesteko
Zeruetako loria.