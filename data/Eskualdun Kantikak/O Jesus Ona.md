---
id: ab-3629
izenburua: O Jesus Ona
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003629.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003629.MID
youtube: null
---

Oi Jesus ona zato gugana;
zu zituena, da urusena.(Errepika)
O Jesus ona! Zein diren dohatsuak
maite zaituzten bihotz garbi kartsuak!

O Jesus ona! Hor gure aldaretan
Zaude gau egun indar gaben onetan.

O Jesus ona! Urrikal zakizkiku,
Bethi eskerrak emanen dauzkitzugu.

O Jesus ona! Egun zaitugu betan
Goretsi nahi kantu errepiketan.

O Jesus ona! Galde hau entzunazu:
Etsai gaichtotik othoi, zaindu gaitzatzu.