---
id: ab-3604
izenburua: Zato Jaun
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003604.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003604.MID
youtube: null
---

Zato, Jaun amultsua, biziaren ithuria,
Mesias dibinoa,
Zato, Zato, Zato!
Ah! lehia ethortzera,
Zato laster gu tartera,
Ifernutik libratzera...

Duela bi mila urthe
Guri agindua zare;
Zato beraz ager zaite.

Iguzkia, noiz ilkhiko?
Zare gu argitzeko,
Ilhumbetarik khentzeko.

Satan beltzak hersten gaitu.
Zuk libraturen gaitutzu,
Esperantza zutan dugu.

Beha gure miseriak,
Ah! ez adi justizia,
Bainan miserikordia.

Entzun zatzu auhen minak,
Bai othoitzak, bai nigarrak,
Hambat demboraz eginak.

Zuk gaitutzu salbatuko,
Ifernutik urrunduko,
Zeruan sarraraziko.

Zato beraz, Jainko ona,
Zato gure ontasuna
Eta gure zoriona.