---
id: ab-3613
izenburua: Mundu Zoro
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003613.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003613.MID
youtube: null
---

Mundu zoro enganioz bethea,
Iduki nauk ondikotz gathetan;
Lausengarri, enganakor trichtea,
Atzeman nauk eta zembatetan!
Lore pean arrantzeak gorderik,
Sarrarazi nauk hire saretan,
Hire baithan sobera fidaturik,
Erori nauk asko bekhatutan. (bis)

Non zen bada neure adimendua
Bizitzean plazer galkorretan!
Ikhusten dut ene zoramendua,
Eta nago auhen minenetan. (bis)

Itsumendu ezin ahantzizkoa!
Salbatzeko izan naiz kreatu;
Egiteko guzien gainekoa,
Nola bada nik zaitut baztertu! (bis)

Zeru lurren, guzien Erregea,
Salbatzea behar dut bilhatu;
Gidaritzat hartu haren legea,
Bekhatua urrikiz borratu. (bis.