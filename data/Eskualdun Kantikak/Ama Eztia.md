---
id: ab-3646
izenburua: Ama Eztia
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003646.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003646.MID
youtube: null
---

Jainkoaren Ama guziz garbia,
Jesusek zuri eman nintuen;
Zure haurra naizen geroz Maria,
Oro zuri naitzu kontsekratzen.
Ama eztia, othoi har nezazu,
Naizen guzia emaiten nitzaitzu.

Har nezazu beraz Ama eztia
Bethikotzat zure gerizean,
Ikhus zazu Satanen malezia
Hel zakizkit lanjeren artean!

Lekhukotzat harturik Jaun handia
Zinez dautzut egun hitzemaiten,
Mundu huntan nik bethi zaitudala
Ahal oroz Ama maithaturen.

Behar banu nere hitzak hautsirik
Egun batez maithatu munduan,
Ardiets nazu Seme maitea ganik
Orai berian hiltzeko grazia.