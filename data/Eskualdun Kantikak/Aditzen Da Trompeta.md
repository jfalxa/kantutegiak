---
id: ab-3612
izenburua: Aditzen Da Trompeta
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003612.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003612.MID
youtube: null
---

Aditzen da trompeta latzgarria,
Ethorri da Jaunaren eguna:
Egun trichte, egun lotsagarria,
Bai egun guzien handiena.
Adotzen da trompeta latzgarria,
Ethorri da Jaunaren eguna!

Ihurtzuri eta chimista pean
Lehenago Jauna mintzatu zen;
Bainan egun bere majestatean
Handiago guziei agertzen.

Aspaldi lur bihurtu haragiak,
Erhaustuak ziren hezurretan,
Osaturik, istant batez guziak
Phizten dire, chutituz batean.

Aingeru onek ematen dituzte,
Merezitu duten arabera.
Gaichtaginak eta onak bi parte
Jesu Kristoren bi aldetara.

Hobendunak ezkerreko aldetik
Ikhusten du Juje hasarrea;
Eta onak Jaunaren eskuinetik
Bake osoaren beithartea.

Orduraino gordeak egon dire
Krima beltzak eta itsusiak.
Agertzen tu guzion bistan hemen,
O Jesus! zure argi biziak.

Zohazkidate ah! madarikatuak
Zohazkidate urrun ene ganik;
Sekulako surat kondenatuak,
Zoazte erretzerat orai danik!

Itzultzen da eskuineko alderat,
Arraiturik Jaunaren bisaia,
Ichurtzen du justuen bihotzerat
Oi! zer bozkariozko hibaia.

O! zer gozo aditzean: Zatozte
Ene aitaz benedikatuak.
Har zazue ene lorian pharte:
Han dire zuen khoro, tronuak.

O! egun segur bezain latzgarria,
Zein aldetan ikhusi behar nauk!
Bekhatoros hain lastimagarria,
Ikhusgarri hortaz bai hiri zauk.