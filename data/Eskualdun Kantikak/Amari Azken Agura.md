---
id: ab-3643
izenburua: Amari Azken Agura
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003643.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003643.MID
youtube: null
---

Agur Jesusen Ama,
itzaleik gabea
Zeru gainean
izar distira zailea.
Agur iguzki bero
hain gora jarria.
Bekatoros guzien
geriza zailea.(bis)

Su hortarik ga baithan
Phitz zazu pindar bat.
Agian, hel gaitezen,
Gu ere zerurat.

Maita dezagun berez
Ama bat hain ona.
Bekatoros guzien
Ihes leku dena.

Har gaitzatzu oi Ama!
Zure besoetan.
Bai orai eta guziz
Azken orenean.