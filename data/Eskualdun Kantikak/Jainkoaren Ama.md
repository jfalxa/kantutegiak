---
id: ab-3634
izenburua: Jainkoaren Ama
kantutegia: Eskualdun Kantikak
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003634.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003634.MID
youtube: null
---

Jainkoaren Ama, ama guziz ona,
Zaitzagun maitha, bethi, bethi. (bis)
Guk gure amatzat hartzen zaitugu,
Bethi zu maithatuz bizi nahi dugu;
Azken hatseraino kantatuko dugu
Bethi, bethi.

Hemengo hersturak ikhusten ditutzu
Zoin den lotsagarri bizitzea dakigu;
Beraz zure haurrak hemen beirazkitzu
Bethi, bethi.

Gure flakeziek zure bihotza
Dute erdiratzen, samurra baita,
Gutaz izan zazu, oi! urrikalmendu
Bethi, bethi.