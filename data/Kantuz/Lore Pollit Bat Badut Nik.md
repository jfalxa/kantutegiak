---
id: ab-4095
izenburua: Lore Pollit Bat Badut Nik
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Lore pollit bat baduk nik
Aspaldi begiztaturik
Bainan ez naite mentura
Haren hartzera eskura;
Baitakit zer den lanjera
Hari behatzia sobera.

Lore ederra, behazu:
Maite nauzunez errazu.
Zure begiek ene bihotza
Betbetan dute kolpatu.
Kolpe hortarik badut nik
Malhur izaitia irrisku.

Aize hotzari guardia,
Iguzkiari ez fida:
Zure kolore bizia
Izan daiteke histua.
Esku on batek biltzia
Desiratzen dautzut, maitia.