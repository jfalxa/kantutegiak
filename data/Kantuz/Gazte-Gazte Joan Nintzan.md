---
id: ab-4034
izenburua: Gazte-Gazte Joan Nintzan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gazte gazte joan nintzan
Herritik kampora
Estran jere aldean
Pasa det dembora
Herri alde guztietan
Toki onak badira,
Bañan bihotzak dio
Zoaz Euskal herrira
Herri alde guztietan
Toki onak badira,
Bañan bihotzak dio
Zoaz Euskal herrira
Bañan bihotzak dio
Zoaz Euskal herrira.

Agur, nere bihotzeko
Amacho maitea,
Laster etorriko naiz:
Kontsola zaitea.
Jaungoikoak nahi badu
Ni urez juatea;
Ama zertarako da
Nigar egitea ?

Lur on hunen uztea
Da negargarria;
Emen gelditzen dira
Ama ta herria.
Urez noa ikustera,
Bai, mundu berria;
Orantche, bai, maizela
Urrikalgarria.