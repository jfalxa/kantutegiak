---
id: ab-4131
izenburua: Zahartzea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mila zortzi ehun'ta berrogoita zazpi,
Sortherritik joan nintzen ahul eta ttipi.
Geroztik hemen nabil goiti'ta beheiti,
Nehoiz hil gabe, bainan irriskutan bethi.

Ez deia bada pena bizi naizen plantan!
Urhe bat ez dut izan nik sekula santan.
Egunean ardit bat alderatu faltan,
Ene dembora joan da geroko peskizan.

Dembora joan zait eta hasi naiz pentsatzen:
Zahartzeak ez dauku gauz onik ekhartzen:
Picha laburrago dut jaz-danikan aurthen:
Zapatak hasi zaizkit puntatik usteltzen...

Jaz zapatari eta aurthen belhaunari...
Diferentzia gaitza, urthe batez, hori!...
Lehenago paretak kolpez hautsi nahi,
Orai, ezin heldurik, chortak chirri-chirri!

Haginak galdu ditut, puskatuak hortzak!
Sudurra joiten deraut engoiti kokotsak;
Ezpainak erre dauzkit pipa gider motzak...
Orai zer gostu duke munduan bihotzak.