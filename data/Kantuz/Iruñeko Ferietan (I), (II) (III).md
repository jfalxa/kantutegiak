---
id: ab-4126
izenburua: Iruñeko Ferietan (I), (II) (III)
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Iruñeko ferietan. Iragan San Ferminetan,
Ehun zaldi arribatu Andaluziatik tropan ;
Merkhatu eder bat zauntan, zaudelarik bi lerrotan.

Bat zen pikarta-churia; hartan bota dut begia.
Andaluzak egin zautan bi untza urrhe galdia
Eskain orduko erdia, harzak, hire duk zaldia.

Han nintzen arrapatua, aginduaz dolutua:
Urruntzeko izan banu hirur arditen lekhua,
Gizonez inguratua, iduri preso hartua.

(II).Iruñeko ferietan, etc...

Zeruko Jainko Jaun ona! Zerk ekharri nu ni huna?
Andaluz bat zazpi urthez presondegian egona;
Laburzki mintzo zautana; konda niri hitzemana.

Jauna, nahi dut pagatu, bainan lehenik miratu .
Zaldiaren miratzeko demborarikan ez duzu.
Orai soma kondazazu, gero miratuko duzu.

Han nintzan plaza bethean, jende hainitzen artean:
Gogoeta hau egin nuen orduko estremitatean
Bertze nunbait pasa niro jaun hori puñal batean.

(III).Iruñeko ferietan, etc...

Eman orduko khondua, eztitzen hasi mundua.
Garistutik hartu nuen delako behor mainkua
Emanik behar dirua, hustu bainuen lekhua.

Utzirik bide ederra, hartu dut oihan bazterra:
Zaldia nuen desferra, begi batetik okherra:
Usu soinatzen kitarra, eztularekin uzkerra.

Belategiko bentetan pasatu nintzan andetan ;
Ahal bezala arribatu Urdazubiko errekan,
Zaldia sarthu partetan, athera nuen khordetan.

Nunbaitik argi aldera, arribatzen niz etchera.
Nere andrea ethorri zaut argiarekin athera.
Jarri behorrari beha, ez baitzen kontent sobera.

Horiche da behor tcharra, eta bertzalde chaharra.
Hortan gastatu dautazu familiako beharra?
Bi sosetan sal nintzazke zaldia eta senharra.

Ichilik zaude, Andrea; joan ez dakidan zaldia!
Bele eta arranoak borthatik daude gardia.
Othoi hil-zazu argia, hunat ez diten abia.

Ichilik zaude zu ere, gezurrik khondatu gabe.
Bele eta arranorik borthan ez baita batere.
Eginak gira gu ere estofa on baten jabe.

Plazan zutenean ikhusi, nahi zautaten erosi
Tratuan bainitzen hasi, ez zuten emaiten aski.
Ez nuen eskutik utzi, ume bat behar du hazi.

Jinen den ume berria, behor edo zamaria
Eginen baitut guardia, denez ama iduria
Heldu bada handia, oi zer primako zaldia.

(Joanes OXALDE.