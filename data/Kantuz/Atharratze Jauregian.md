---
id: ab-4063
izenburua: Atharratze Jauregian
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Atharratze jauregian bi zitroin doratu
Hongriako erregek batto du galdatu;
Arrapostu ukhen du ez direla huntu,
Huntu direnian batto ukhenen du.

2
Atharratzeko hiria hiri ordoki;
Hur handi bat badizu alde bateti,
Errege bidia erdi erditi,
Maria Madalena beste aldeti.

3
Aita, saldu naizu idi bat bezala,
Ama bizi ukhen banu, aita, zu bezala,
Enunduzun ez juanen Hongrian behera,
Bena bai ezkonturen Atharratze Salala.

4
Ahizpa, juan zite portaliala,
Ingoiti horra duzu Hongriako erregia;
Hari erran izozu ni eri nizala,
Zazpi urthe huntan ohian nizala.

5
Ahizpa, ez nukezu ez sinhetsia,
Zazpi urthe huntan ohian zirela,
Zazpi urthe huntan ohian zirela;
Bera nahi dukezu jin zu ziren lekhila.

6
Ahizpa, jauntz ezazu arropa berdia.
Nik ere jauntziren dit ene churia.
Ingoiti horra duzu Hongriako erregia;
Botzik kita ezazu zure etchia.

7
Aita, zu izan zira ene saltzale,
Anaie gehiena dihariren hartzale,
Anaie artekue zamariz igaraile,
Anaie tchipiena ene laguntzale.

8
Aita juanen gira oro alkarreki,
Etchera jinen zira changri handireki,
Bihotza kargaturik, begiak bustirik,
Eta zure alaba tumban ehortzirik.

9
Ahizpa, zuza orai Salako lehiora,
Ipharra ala hegua denez jakitera;
Ipharra balin bada goraintzi Salari,
Ene korpitzaren cherka jin dadila sarri.

10
Atharratzeko zeñiak berak arrapikatzen,
Hanko jente gazteria beltzez beztitzen,
Andre Santa Clara hantik partitzen,
Haren peko zamaria urhez da zelatzen.