---
id: ab-4041
izenburua: Orhiko Choria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Orhiko choria Orhin bakean da bizitzen,
Bere larre sasietan ez da hura unhatzen;
Han zen sorthu, han handitu, han ari zen maithatzen,
Han bere ume artean, gozoki du kantatzen.

Urusa harek bezala egiten dakiena!
Orhin sorthu eta Orhin gazte zahartzen dena!
Aditzez baizen ez daki zer den kanpoko ona,
Bainan aldiz jastatzen du etcheko zoriona.

Bego etcheko chokoa oraino nor-berari!
Bego ait.amen etchola haurrik hazienari!
Etchea bethi etchea da, zembat den itsusgarri,
Kampoa bethi kampoa da, zembat den edergar!

Etchean pena guziak dirade kontsolatzen;
Etchean zauri minenak norabait ere hersten,
Irriak, solas gozoak, non dirade han baizen,
Gau-aldera familia denean bateratzen?

Familiako bizia, zein zaren plazertsua!
Zutaz gabetua dena zertaz da gabetua?
Ume zurtza baten pare dabila herratua,
Ezin bere bihotzaren aurkhituz sosegua.

Zembat urrikari dudan bizi dena kampoan
Bera da beretzateko mindu huntan osoan;
Jendez da sethiatua eta da desertuan!
Laguntza onik ez duke bere behar-orduan.

Etcheko tcharra hobe da ezenetz atze ona;
Atzeak ez du balioetchekorik tcharrena:
Hori da alde orotan kurri doan errana,
Bainan ongi aditzeko frogatu behar dena.

Nik segurik zinez dakit nolako den kampoa:
Itchura hainitz du eta funts hainitz tcharrekoa.
Etzaitela hari fida, leguna du mintzoa,
Bainan gero sartzen dautzu ezten phozoatua.

Beraz gutarik bakhotcha etchean egon dadin,
Orhin sorthu balimbada, egon dadiela Orhin!
Zertako hainbertze urhats zorionketa egin?
Etchean du zoriona nork berak berarekin!!.