---
id: ab-4023
izenburua: Gernikako Arbola
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gernikako arbola da bendikatua
Eskualdunen artean guziz maitatua :
Eman ta zabal zazu munduan fruitua
Adoratzen zaitugu arbola saindua
Adoratzen zaitugu arbola saindua.

Mil.urthe inguru da erraiten dutela
Jaunak jarri zuela Gernikan arbola:
Zaude bada chutikan, orain da dembora:
Eroritzen bazira, arras galdu gira.

Ez zira eroriko, arbola maitea,
Baldin portatzen bada Bizkaiko juntia:
Denek hartuko dugu zurekin partea,
Bakean bizi dadin Eskualdun jendea !

Bethiko bizi dadin othoitz egiteko
Jarri gaitezen denak laster belhauniko:
Eta bihotzetikan eskatu-ez gero,
Arbola biziko da orai eta gero...