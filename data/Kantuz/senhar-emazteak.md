---
id: ab-4127
izenburua: Senhar-Emazteak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ene hauzotegian bi senhar emazte
Bakean bizi dira hasarratu arte
Goardia elgarrekin (berriz) behin samurtzetik
Heien chuchentzaileak lan ona behar dik!

Lehenik intzirinka, gero deihadarka;
Gero berriz eskainka, azkenekotz joka;
Horra zer bizi modu (berriz) duten maiz segitzen,
Dudarik gabe gostu diote kausitzen.

Gizonari on zaio zorta bat gorritik,
Emazteari berriz ttotta bat horitik,
Emaztea ichilka (berriz) senharra haltoki,
Bakhotcha bere alde dabiltza abilki.

Ostaturik lekora, bapo eta eder,
Senharra ez dadila sukhalderat ager!
Goardia eman bezo (berriz) bere andreari
Ttottaren ondorio omore tcharrari.

Gizon etche galgarri, arno edalea,
"Ahalke behar huke hola bizitzea!
"Haizen bezalakoa (berriz) arraila goizetik,
"Hoakit, higuin tzarra, begien bichtatik.

"-Nere andre gaichoa, ez bada hasarra!
"Hi bezalako baten badinat beharra.
"Emazte baliosa (berriz) eztia, ichila
"Holako bakhar batek balio-tin mila!

"-Gizon alfer-tzar, gormant, zikin itsusia!
"Aspaldian nauk hitaz asetzen hacia!
"Bethi gerla gorria (berriz) sekulan bakerik!
"Ifernuan bide-duk hi baino hoberik!

"-Othoi, ago ichilik, ene emaztea!
"Indan-bai, laster gero, biarten bakea!
"Zer nahi dun enekin (berriz) gerla irabazi?
"Lehen ere badakin nola hautan hezi!.

Emazteak orduan hartzen du erkhatza
Eta chiminiatik gizonak laratza:
Batek uma-ahala (berriz) bertzeak: ai! ai! ai!
Oi zein eder guduan holako bi etsai!

Eltze, zartain, gathilu, oro nahasketa,
Lurrerat arthikiak dabiltza jauzketa.
Joaiten naiz jaun-andere (berriz) heien bakhetzera.
Umaturik hor naute igorri etchera.

(Airea: Iruten ari nuzu n.67.