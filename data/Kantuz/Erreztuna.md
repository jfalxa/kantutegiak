---
id: ab-4081
izenburua: Erreztuna
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Partitu nintzen herritik, zuri promesa emanik, (berriz),
Enetzat fidel egon zinten ongi gomendaturik,
Ez duzu egin kasurik !

«Fidelik ez egoiteko ez dut ez nik arrazoinik ;
Ene bihotza zuk daukazu arras enganaturik,
Hortaz ez izan dudarik ! »

«Partitzean eman nauzun seinaleño bat nik zuri ;
Seinaleño hura nik orai nahi nuke ikusi,
Ez bauzu eman nehori ».

« Zuk eman seinale hura ez dut eman ez nehori,
Begiratuko ere nuen, ez balitzeraut hautsi,
Bainan egin zaut bi zathi ! »

Nik eman erreztun hura ez zuzun bada kobria,
Ez zen eta ere zilarra, bainan zuzun urria,
Diamantezko begia !

Amodiorik bazinu zuk, batere, enetako,
Pochiak bilduren zintuen niri erakusteko,
Satisfos ni egiteko !...