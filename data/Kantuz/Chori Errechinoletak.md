---
id: ab-4065
izenburua: Chori Errechinoletak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Chori errechiñoletak ederki kantatzen
Bazter guziak ditu choratzen
Bard.arratsian,
sasi batian
biga baziren
Eder ziren
charmant ziren
manera oroz
eta elgarrez hainitz agrados!

2
Batto arra zuzun bertzia emia,
Ez miraluku parekatzia!
Han, lehen ere,
Dudarik gabe,
Ikusi ez balu,
Arra hura
Ez zen juanen
Sasi hartara,
Emia ez balitz jin bid.erdira!

3
Oi choriño gaicho heien beldurti handia,
Ikustiarekin ihiztaria,
Batto han gaindi
Hain inozentki,
Pasatzen zela;
Arra hura,
Beha jarri,
Eta hor, gero,
Sasian sartu zen, han gordetzeko.

4
Eta orduan berean, nontsu zen emia,
Sasi handian barna sartua;
Hor aphaindurik,
Ta lumaturik,
Bere papoa;
Kukurusta
Harroturik,
Jarri zenean, umez orhoitu zen, ber.ohantzean.