---
id: ab-4115
izenburua: Donostiako Hiru Damatcho
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Iru damatcho Donostiako Errenterian dendari
josten ere badakite, baino ardo edaten obeki
Ta kriskitin, krosketin arrosa klabedin
Ardo edaten obeki.

Iru damatcho Donostiako, irurak gonak gorriak,
Sartzen dirate tabernan eta ilkhitzen dira ordiak
Ta kriskitin, etc.

Donostiako neskatchatchuak kalerat nai dutenian,
Ama, eztago piperrik-eta banua salto batian
Ta kriskitin, etc.

Iru damatcho, Donostiakok, egin ei-dute apostu
Nork ardo gehiago edan eta zein gutchiago mozkortu
Ta kriskitin, etc.

Donostiako iru neskatchak, zaragiaren onduan,
Etcherat bear dutenik ez-dadukate goguan
Ta kriskitin, etc.

Donostiarrek ekarri dute Getariatik akerra;
Kampantorrian paratu dute, Aita-santua dutela
Ta kriskitin, etc.

Donostiako kalle nausiko erdi erdiko etchian,
Neskatcha batek mozkorra zeukan kamisa uts utsian.
Ta kriskitin, etc.

Donostiako molle gainian, egun senti-sentian,
Haztan eder bat eman ziraten bular biaren-erdian.
Ta kitarra, kajolin, atabal, dambolin
Donzella maya zureki.