---
id: ab-4087
izenburua: Mendian Zoin Den Eder
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mendian zoin den eder epher zango gorri !
Ez da behar fidatu itchur'eder horri.
Ene maiteak ere bertzeak iduri
Niri hitzeman eta gibelaz itzuli.

Gaztena, ontu-eta, lurrean ihaurri:
Ene bihotza duzu zu ganat erori,
Eta zurea aldiz harria iduri:
Ene begi gaichoak nigarrez ithurri.

Airea zahar eta khantorea berri:
Ene maite pollita zira charmagarri:
Kolore churi-gorri arrosa iduri,
Mundurat jina zira ene ihargarri.

Heldu naiz zure ganat arrosa ederra
Ezin bertze huntarik nezazun athera.
Changrin huntan hiltzeko jin balait malura,
Begian bazinuke bethiko nigarra.