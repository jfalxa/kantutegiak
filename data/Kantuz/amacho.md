---
id: ab-4047
izenburua: Amacho
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Oilarra kukuruka etchean da ari,
jadanikan Amacho othoitzez Jaunari;
Bi eskuak ikharan, so kurutzeari:
Jaunak beha diezon eguberriari!

2
Atcho zinez pollita da gure amatto,
Chahar izana gatik ez oraino ttontto.
Diruz balimbad.ere pochi bat pobretto,
Omor.onez bederen dago aberastto.

3
Igandetan ederrik, iduri pampina,
Zinez erran liteke: Horra rregina!
Begia irriz dago, halaber ezpaina:
Oi zer atcho pollita, Jainkoak egina!

4
Buru sahetsetikan motho-buztanduna,
Lepho-gibel aldean mokanez urdina,
Balenarik gabeko soineko fin fina,
Zaiaño bat pollita ber.eskuz egina.

5
Meza-deia burrumban, eliza dorretik,
Eta amacho johan landa bazterretik;
Buruaren zaintzeko iguzki gaichtotik,
Kaputchina pleguan eman du burutik.

6
Artho-bizardun pherde, sorhopil gizenak,
Sagar hori.ta gorri, nahasti-aihenak,
Amacho gaichoari irriz daude denak:
Ezagun da ez direla adichkide minak!

7
Intzaur, piko, gaztenak, zirezte handitu,
Zuen landatzailea da aldiz chahartu;
Ainitz neguetako elhurra gelditu,
Eta ile urdinak zeraizko churitu.

8
Huna ur-chirripa bat intzirez doana,
Gain hartako zerua bezambat urdina:
Lehenago jauzian igaiten zuena...
Orai, doi-doi, emanez harri chabalduna.

9
Agur, amatto, agur! agur, ene haurra!
Bidean orok hari bihotzez agurra.
Amacho erreposki ez dadien lerra,
Ttapa, ttapa badoa chendrari behera.

10
Herrirat heldu eta, zaia du zafratzen,
Zapeteri erhautsa eskuaz chahutzen;
Kaputchina burutik laster beheititzen,
Eta ichil ichila elizan da sartzen.

11
Zer othoitzak orduan aita Jainkoari!
Aingeruak barrandan chahar maiteari.
Bertze guziak kantuz errepikan ari...
Atcho ona mezuka Ama-Birjinari.

12
Hilen herri chokoan, mezatik lekora;
Amatto nigarretan othoitzez hobira;
Loretto bat harturik lurretik eskura,
Ichil eta trichte da itzultzen herrira.

13
Ilhun zeinuetako etcherat heltzean,
Gozo handi bat zuen amak, bihotzean.
Gaztena adar beltchek, hura aditzean
. Gau on! . zioten orok, betan aphaltzean.

14
Oi Amacho maitea, agian luzachko,
Jainko Jaun onak zaitu gurekin utziko!
Ainitz urthez, agian, zaitu ahantziko!
Luzaz, agian luzaz gaituzu zainduko.