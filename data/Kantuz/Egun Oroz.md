---
id: ab-4069
izenburua: Egun Oroz
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Egun oroz, goizetan arratsetan,
Bethi nago pena doloretan;
Ezin dezaket jan, ez eta-re edan.
Ez dakit zer dudan,
Gaitz handi bat da ni baitan;
Amarratu nau bet betan...
Ez naiteke ni bizi gisa hunetan!

Antchu gazte bilo hori ederra,
Sar zaite, sar korrela barnera:
Artaldean bada oi zure beharra.
Turna zaite, turna zure aiten sor lekura.
Desiratzen dut nik hura,
Zure ondoan ibili naiz ni ardura.

Artzain ona? -Badantzut, ene jauna-
Othoi zato, othoi, ene gana.
Errebelatu denak derautzuia pena,
Ez izan dudarik, oi Antchu bilo urdina.
Sorturen dela ordaina?
Etzaitezela horrela changrina.