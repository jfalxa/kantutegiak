---
id: ab-4046
izenburua: Ama
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Maitatua sobera, nintzelarik haurra,
Ez nakien nik zer den amaren beharra;
Bortu batean orai naiz bakar bakarra,
Amaz orhoit orduko heldu zaut nigarra!

2
Hegalñoak azkartzen sendituz geroztik,
Baitoazi choriak ohantze gozotik;
Halaber joan ninduzun amaren altzotik,
Ahantzia bainute herrian engoitik!

3
Bainan ama gaichoak ez nu ez ahantzi,
Maiz daut Euskal-Herritik igortzen goraintzi;
Hautsi nion bihotza nuenean utzi,
Neretzat baizik ez da halere han bizi.

4
Nik ere bero daukat bihotzean garra:
Jinkoak piztu zauntan eskualdun pindarra,
Ama baitut oraino minetan indarra,
Ama, gau ilhunean argidun izarra!!!

5
Ene bortu gainetan baniz enheatzen,
gaizkirat balimbazaut bihotza lerratzen,
zure begi garbiez, ama naiz orhoitzen
Eta lanari gero kuraiekin lotzen!

6
Oi! Maiz ametsetarik izan dut gogoan
Gochoki nindagola lehen ohakoan:
Aingeruer begira, gambara chokoan,
Eta, zu, hantchet irriz ama ner.ondoan!

7
Frangotan huts ohiez orhoitzen ere niz...
Ama! Gazte dembora berriz haste balitz?
Nigar eginarazi dautzut anhitz aldiz,
Nigarrak niri zauzkit jauzten orai borthitz!

8
Aspaldi du naizela sor-etchetik joana,
Hamargarren urthea hurbil iragana;
Noiz othe naiz hemendik jinen zure gana,
Ama, saristatzeara zor dautzutan lana?

9
Untziak noiz nu berriz horrat eremanen?
Etcherako bidechka noiz dut nik iganen,
Eta zutatik, ama, musu bat ukanen??
Oi! Ba, laster dautazu agian emanen!!

10
Aho batek eztiki diolarik: Ama!
Hitz gocho bakar horrek mundu bat derama.
Ama baino hoberik zer ditake asma?
Horren maitatzen beraz gaiten oro thema.