---
id: ab-4048
izenburua: Lo, Lo, Nere Maitea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Lo! Lo! Nere maitea;
Lo! Ni naiz zurekin
Lo! Lo! Paregabea,
Nigarrik ez egin;
Goizegi da munduko
Gelditzen bazira,
Nigarretan urtzeko
Baduzu dembora.

2
Lo! Nik zaitut higitzen;
Lo! Lo! Nombait goza.
Ez duzu ezagutzen
Amattoren boza?
Etsai guzietarik
Zure begiratzen,
Bertze lanak utzirik,
Egonen naiz hemen.

3
Lo! Nere aingerua,
Bainan ametsetan
Dabilkazu burua,
Hirriz ezpainetan,
Norekin othe zare?
Non othe zabiltza?
Ez urrun, ama gabe,
An, ene bihotza.

4
Lo! Lo! Zeruetarat
Airatu bazare,
Ez bihur zu lurrerat
Ardietsi gabe
Zu ongi alchatzeko
Enetzat grazia:
Guziz eni hortako
Zait ezti bizia.

5
Lo! Lo! Gauak oraindik
Nombait du eguna;
Ez da nihon argirik
Baizik izarrena.
Izarrez mintzatzean,
Zutaz naiz orhoitzen:
Zein guti, zur.aldean,
Duten disdiratzen!

6
Lo! Lo! dembora dela
Iduri zait albak
Histen hari tuela
Elkhi gabazkoak;
Choriak, arboletan,
Kantaz hasi dire;
Laster nere besoetan
Gozatuko zare.

7
bainan atzarri zare,
Uso bat iduri;
Huna nik zembat lore
Zuretzat ekharri.
Ametsetan ait-amez
Zereia orhoitu?
Ai! Hirri maite batez
Baietz erradazu.