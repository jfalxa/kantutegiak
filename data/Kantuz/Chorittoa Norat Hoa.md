---
id: ab-4094
izenburua: Chorittoa Norat Hoa
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Chorittoua, nouat houa,
Bi hegalez airian?
Españalat jouaiteko
Elhurra duk bortian.
Jouanen gutuk algarreki
Houa hourthu denian.

San Josefan ermita
Desertian gora da:
Españalat jouaitian
Hau da ene pausada
Gibelilat so egin eta,
Hasperena ardura.

Hasperena, abil, houa,
Maitiaren bortala,
Abil, eta erran izok
Nik igorten haidala!
Bihotzian sar hakio
Houa eni bezala.