---
id: ab-4055
izenburua: Gaztetasuna Eta Zahartasuna
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Ertaz dutan solasa emazue kasu,
Esplikatuko baitut ahal bezain justu zahar bezain prestu.
Odola zaut hoztu,
Bihotz.ere laztu.
Gorputza beraztu
Oraino gazte banintz banuke gostu!
Oraino gazte banintz banuke gostu!

2
Zahar gazten arteko hau da parabola:
Zuzen esplikatzia hein bat gogor da-
Gaztea ez daiteke adin batez molda,
Gorphutza sendo eta azkar du odola;
Zahar.ez da hola;
Iragan denbora
Ethortzen gogora,
Et.ezin kontsola...
Nekez bihurtzen baita zahar arbola!

3
Egunak badoazi egunen ordotik
Ez dir.elgar iduri joaiten hargatik:
Atzo iguzkiaren distira zerutik,
Egun hobela jalgi itsaso aldetik:
Euria ondotik
Hasi da gogotik
Hedoien barnetik
Hortakotz badakit
Erituko naizela bustiz geroztik!

4
Uda lehen arrosa neguan arrado;
Osto baino arrantza aise gehiago:
Oroz ohoratua primaderan dago,
Izotza jinez geroz lorea akabo,
Handik goiti gero
Ez du gehiago
Zeren ez den bero;
Hortaz segur nago...
Hori bezala gira zaharrak oro?!

5
Huna gaztetasuna zoin den loriosa:
Horri komparatzen dut nere arrosa;
Usain gozo ezti bat, kolorez airosa,
Bere arropa ere hainitz baliosa,
Zahar odol hotza
Nondik ez da lotsa?
Flako du bihotza,
Baitaki bai ontsa
Lur barnean duela laster gorputza!

6
Aslapdi du naizela ethorri mundura,
Geroztik pasatu da hainitz denbora,
Atsegin batendako frango arrangura,
Miseria kantatuz bizi naiz ardura!
Geroko mentura
Nik banu segura,
Egin nio hura
Ainhitzen gostura,
Zaharren gaztetzeko baginu moda!

7
zahar batentzat gazte badoha mundutik,
nahiz zaharragoak goazin hargatik...
eriotzek .orena ez ahantz geroztik,
seguragorik ez da sortzeraz geroztik,
Joaitean hemendik
Gizonak etzakik
Zer dago ondotik,
Bekatuen gatik,
Barkatzen ez badaizku Jaunak zerutik.