---
id: ab-4089
izenburua: Nere Maite Pollita
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Nere maite pollita, ez nauzu maithatzen!
Bihotz erdigarria, zer duzu pentsatzen?
Hambat dembora huntan nauzu abusatzen
zertako hari zara ni hola flatatzen.

Primaderaren khoro oi liliz egina!
Goizeko choriaren oi kantu arina!
Ez duzue eztitzen nere bihotzmina,
Zeren sobera baitut amodio fina.

Maiz erraiten dautazu zuk maite nauzula,
Eta jendek erasten bertzerikan dela,
Muthik gazte propitan mudakor zarela,
Gizonen abusatzen zuk lakhet duzula.

Sinhesten baditutzu jendien erranak
Eta guti prestatzen nere solas onak,
Deus ez dira izanen zure ontasunak;
Segur sinhetsiren nau ni maite nauenak.

Lilitan pollitena elhorri churia;
Lilia joan ondoan, zein bihi gorria!
Bihi barnean dauka oi hezur gogorra;
Barnez gor, kampoz eder da bihotz elkorra.

Zelhaiak buluz, triste, dirade neguan;
Hegaztinek ez dute kantatzen orduan.
Ez da amodiorik zure bihotzean,
Kita nezazu beraz nahi duzunean.

Etzitezela urrunt hola samurturik!
Gau egunak naukazu nigarrez urthurik;
Nik ez dakit zer erran, zeren maithaturik
Ez baitezaket izan nik zutaz bertzerik.

Nahi eta beldur naiz! beldur eta nahi!...
Gizon gaztea baita oi enganarri.
Ez naite behinere jar deskantsuari;
Zure beldur, zu maite, ni naiz urrikari.