---
id: ab-4035
izenburua: Gazte-Gira Gazte
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gazte gira, gazte, hogoira urthetan !
Ez egon pentsaketan joaiteko harmetan:
Goazin oro betan, kuraie handitan.

Amak etchen nigar, aita ere trichte:
Etchean badituzte hiru anai gazte:
Laguntza badute, ni ethorri arte.

Heldu girenean, denborak eginik,
Bazterrak ikusirik, frantsesa jakinik,
Ama , othoi, ichilik ! Ez egin nigarrik .