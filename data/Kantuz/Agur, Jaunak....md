---
id: ab-4141
izenburua: Agur, Jaunak...
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Agur, jaunak! jaunak, agur!
Agur t'erdi... jaungoikoak eginak gire
Zuek eta bai gu ere...
Agur jaunak!... jaunak agur!
Hemen gire...
Agur jaunak...