---
id: ab-4059
izenburua: Amacho Bere Ilobarekin
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Emakume chahar bat buruko churitan,
O pularda beltch eta zai ile gorritan,
Neguko egun batez zagon iguzkitan,
Altzorat alabaren haurra harturikan.

2
Ana ttikiak dio bere amachori:
Nolaz duzu, amacho, ilea hoin churi?
- Ikus.azu mendian, haurra, elhur hori:
Ene adinak ere negua dirudi.

3
- Arphegi pollit hori zertako, amacho,
ez duzu amak bezen legun eta gocho?
- haurra, urak maldari errekaz doatzo,
nigarrek naute egin ni ere hoin atcho.

4
- zertako egin nigar, amacho maitea?
Zure kontsolatzeko ez niza zurea?
- Bertze asko neureez, ai, ene umea,
ikusi dut demboraz etche hau bethea.

5
- Amacho, zein barache zu zaren ibiltzen?
Zalhukiago naiz ni ttikitik higitzen.
- haurra, ni ere lehen, zu bezala nintzen...
Urthe soberek naute orai dorphegitzen.

6
- Bizitzen baldin banaiz zu bezenbat urthe,
amacho, zu bezela izanen naiz othe?
- haurra, zenbat munduan luzaz bizi uste,
menturazko agintza beizik ez baitute...

7
- Jainko maiteak bethi, amacho, zertako
ez gaitu elgarrekin biziaraziko?
- Bizi huntaz bertzerik, haurra, guretako,
badagoka hoberik, eta sekulako.

8
- Ai, amacho, biak gu bagine zeruan!
Ai...zer dut nik? Eri naiz...Ai min dut buruan!
- Haurra, goazen barnera; hobeko dugu han:
Chakurra pleinuz dago gure inguruan.

9
Handik bi aste gabe bi gorphutz elizan
Etche hartarik ziren ekharririk izan
Eta bien arimak zeruan dabiltzan
Lurreko gaitzetarik bethikotz gerizan.