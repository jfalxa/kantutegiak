---
id: ab-4096
izenburua: Argia Dela Diozu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Argia dela diozu,
Gauerdi orain' ez duzu
Enekilako dembora
Luze iduritzen zaitzu;
Amodiorik ez duzu
Orai zaitut ezagutu.

Othea lili denian,
Choria haren gainian,
Houa jouaiten airian
Berak plazer diainian;
Zure eta nere amodioa
Hala dabila mundian.

Phartitu nintzan herritik
Bihotza alagerarik;
Arrajin nintzan herrian
Nigarra nian begian;
Har nezazu saihetsian
Bizi naizeno mundian.