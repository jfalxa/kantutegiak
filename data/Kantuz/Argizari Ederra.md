---
id: ab-4073
izenburua: Argizari Ederra
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Argizari ederra, argi egidazu, bide luze batean joan beharra nuzu!
Gau huntan nahi nuke maitia mintzatu,
Haren ganatuarte, argi egidazu!

"Nekaturik heldu naiz bide luze huntan,
Urhats pausu guzian bethi pentsaketan;
Athorra bustia dut bulhar-errainetan,
Idortu nahi nuke su ondoño hortan!.

"Athorra busti hori garreia ezazu
Nik hola manaturik bustia ez duzu!...
Bidean eman gabe, kontu zuk egizu,
Bidearen buruan nahi zaituzten zu?

"Ez deia bada pena eta dolorea,
Nihauren maiteñoa hola suntsitzea!
Amets bat egin nuen zuk ni maitatzea...
Zombat deithoragarri hol'enganatzea!!!".