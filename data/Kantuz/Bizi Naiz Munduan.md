---
id: ab-4080
izenburua: Bizi Naiz Munduan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Bizi naiz munduan
Aspaldi handian
Bethi trichtezian
Penak bihotzian
Amodiotan nintzan
Izar batekilan
Hura ezin ikhus
Plazer dudanian
Ez baitut etchian
Ez eta herrian.

Aira banindadi
Ainhara bezala,
Ardura joan nainte
Maitearen gana:
Nere pena-changrinen
Hari erraitera
Bai eta doloren
Hari salhaltzera,
Aitaren ichilik
solas egitera.

Izan niz Barkochen
Kusiaren etchen,
Ahuntz-jaki jaten,
Segur ona baitzen
Kostaletak errerik,
Biriak egosirik,
Ogia muflerik,
Arnoa gozorik:
Han etzen eskasik,
Maitea, zu baizik...