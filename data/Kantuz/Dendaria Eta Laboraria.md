---
id: ab-4043
izenburua: Dendaria Eta Laboraria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Zertako egin zare Anna laborari
Hobe zinuke aise bazine dendari
Ni barnean gochoki naiz lanean ari,
Eskuetan ibiliz orratz eta hari
Orratz eta hari.

LABORARIA
Laborantzan emanik ez dut nik urriki,
Kampoko aire onak bainauka sanoki.
Nork du osagarria beiratzen hobeki,
Irabaziz lan hortan bizia ederki.

DENDARIA
Bethi garbiki duzu bizi dendaria,
Ongi bezti delarik lana du garbia.
Hoin garbi ez ditake ar laboraria,
Zango eskuetan du ardura lohia.

LABORARIA
Laborantzak ez du ez, Andere Maria,
Behinere aphaldu neskatcha gaztia.
Zure lana ez duzu batere hobia:
Nereak emaiten daut irabazi doblia.

DENDARIA
Uria denean naiz barnean egoten,
Bethi aterbean ni ari bainaiz josten.
Zu kampoan zaitugu lanean ikusten,
Uria balinbada ederki bustitzen.

LABORARIA
Bustiak ez nau ez batere izitzen,
Laboraria baita denetarat jartzen.
Dendarietan asko ditut ezagutzen,
Kamporat ilki gabe aise mafrunditzen.

DENDARIA
Dendari hobe dela daukat nik neguan,
Lanean artzen bainaiz suaren ondoan.
Laboraria dago bethi han kampoan,
Elhur eta ichtila bere zangopean.

LABORARIA
Elhur eta ichtila landetan denean,
Sartzen naiz ilhuneko lanetik etchean.
Ez duzu lana uzten hoin laster neguan,
Hor zaude berant josten, argia ondoan.

DENDARIA
Udako beroetan oi zer izerdiak
Hartu behar dituen maiz laborariak.
Ez ditu hola joko iguzki beroak
Itzalean dagozin dendari gaztea.

LABORARIA
Ez nau ni erituko iguzki garbiak:
Barneko gaitzak ditu kentzen izerdiak.
Hortako nau laguntzen ni osagarriak;
Lan guti du nerekin gure mirikuak.

DENDARIA
Ez ginuke behar guk gaur hemen samurtu,
Hortako hobe dugu huntan akabatu.
Behar dautazu hatik lañoki aithortu,
Beztitzeko zarela zu nitaz behartu.

LABORARIA
Behar dela dendari dugu aithortuko,
Bainan laboraria lehen ezarriko.
Horrek baitu jendea bizi atchikiko;
Hortako laborari naiz geldituko.