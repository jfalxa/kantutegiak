---
id: ab-4090
izenburua: Ene Maitea, Barda Nun Zinen
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ene maitea, barda nun zinen
Nik bortañoa joitian,
Buruyan ere min nuyen eta
Dudarik gabe ohian. (bis)

Bard'arratsian, ametsetarik
Boz bat entzun dut charmantik,
Eztitasunez bethia beitzen,
Harren parerik ez beitzen. (bis)

Eztitasuna gauza ederra
Nork nezake nik kondena,
Gauaz loale egunaz ere
Errepausurik batere. (bis)

Charmagarria, lo ziradia,
Eztitasunez bethia?
Lo bazirade iratzar zite,
Etziradia loz ase. (bis)

Arboletan den ederrena da
Oihan beltzian phagoa;
Hitzak ederrak ditutzu bainan
Bertzetan duzu gogoa. (bis)

Nik zure ganil despegitzia
Iduritzen zaut hiltzia,
Hiltzen ez baniz bihurtuko niz
Indaitzut bi pot, maitia.(bis)

(Airea: Solferinoko itsua n.18.