---
id: ab-4129
izenburua: Hitz Zuhurren Kantua
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Othoi denak zathozte plazaren erdira,
Nere erran zuhurren gogoz ikhastera:
Mendiak baino ere zaharrago dira;
Arnoa ontuz doa urthetik urthera.

Itsasoak, bichtan da, ez du ez adarrik,
Ez-eta gaztanberak erdiko hechurrik;
Nehoiz ez da ikhusi su-gabeko kherik,
Bainan ez daite juja deus itchuretarik.

Duenak azer bipher emaiten ohi du;
Ez da behar ichtupa suari hurbildu;
Tripa hutsari ez da emaiten konseilu;
Barur egonez norbait noiz zaitzu arraildu?

Chakhur tcharra gisala da dena kukuso;
Haurrak aita-ameri dagotzi bethi so;
Be begi baino hobe dauzkagu bi beso
Baso bakharra baino ez othe bi baso?

Ez ar ziminoari sino erakhusten;
Orotan jaunen jaunak dirade kausitzen;
Arbola fruitutik dugu ezagutzen;
Pozoinek osasuna maiz dute sendatzen.

Asko kampoan bildotch, ta otso barnean;
Zoin den eder itzala, iduzki denean;
Ziminoa gorago igaiten arbolan,
Ipurdia hobeki ageri orduan.

Laguna lagunari bethi iduria:
Nolako aita eta, halako semea;
Uriaren ondotik heldu atheria...
Aita biltzale batek haur chahutzailea.

Hala-beharra bethi denen nausi dago.
Norat doan haizea, harat aiseago.
"Gero" dionak usu erran nahi "bego"
Presatzeak dakharke ondore tchar frango.

Amiltzean harria ez da gorolditzen.
Gizon ibilkariak zer ez du ikhasten?
Busti nahi ez dena urean ez sartzen.
Irriskatu gaberik nork du deusik biltzen?

Indarrari nausi da lausengu eztia;
Azkarren arrazoina da bethi hobea;
Nola bizi hala hil, askoren leloa.
Jaunak bakharri daki zer daiken geroa.

Gauzarik hoberenak finitze bat badu;
Behar niz ba sail huntan nolazpait gelditu.
Aho hetsian ez da nehoiz fitsik sartu,
Bainan ez trompatzeko behar da ichildu.