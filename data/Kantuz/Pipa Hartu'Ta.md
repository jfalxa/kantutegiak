---
id: ab-4109
izenburua: Pipa Hartu'Ta
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Pipa hartu'ta, choratzen naiz,
Arno edan'ta, mochkortzen naiz,
Mochkortu eta, erortzen naiz!
Nola demontre biziko naiz?
Tralera lera...

Hamar botoila chankraturik,
Azken basoa klikaturik,
Azken chirrichta iretsirik,
Moltsa deraukat tturrindurik...

Urrundu zaizkit aichkideak,
Eta hurbildu ostalerak;
Etchean aldiz emazteak
Prest-presta ditu erasiak.

Bazter guziak ingurika,
Bideak ere inharroska:
Munduak zer du? Hau da hinka?
Goazen etcherat andarkoka...

Muthiko gazte ta leiala,
Konseilu onak har zaitzula;
Othoi bizian ez zaitela
Sekulan mochkor ni bezala...