---
id: ab-4079
izenburua: Buruilaren Ondarra
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Buruilaren ondarra... Jon Doni Mikele...
Maitia, ikustera heldu naiz ni ere.
Baldin plazer baduzu, othoi, jeiki zaite !
Zure hor ikustia desiratzen nuke ...

Ez, ez, ez naiz jeikiko, seguratu bainiz
Adichkide tutzula neskatilla ainitz...
Eskerrak Jainkoari, hola libro bainiz !
Malurus behar nintzen, fidatu ni banintz !.

Federik ez duzula doizu ni baithan,
Entseiatu beharko bertze zenbaitetan...
Pena handiagorik zer da mundu huntan
Jarraiki baitzitzautzut bethi alferretan...

Ez dut bada ibiltzen sakelan limarik,
Ta are gutiago kontrako giltzarik...
Maitatzen ez banuzu, ez idek bortharik;
Etchera joanen nuzu jin nizan bidetik !

Lersunak, lerro lerro, bidian badoazi,
Beruaren lekurat, hotzaren ihesi...
Ni ere bethi penaz beharko naiz bizi;
Zombait aho-kirets nik beharko iretsi !!!".