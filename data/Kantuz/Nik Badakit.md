---
id: ab-4108
izenburua: Nik Badakit
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Nik badakit, arbasoek erranik,
Arno onak ez duela parerik:
Orai ere, balin banu hartarik,
Edan niro basoa betherik.

(Airea: Inchauspeko alaba n.63.