---
id: ab-4123
izenburua: Chakur Eta Gathia
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Chakur eta gathia, Minitz eta Potzo
Gasailan ari ziren kasik egun oro
Bakean egon zaizken ez izanez zoro,
Bainan gerlan zuketen hatzemaiten gozo.

Chakurra, unatua, noizpait kalapitaz,
Gogoeta hasi zen... bakearen onaz:
Gathuari bi hitzen erraitera beraz
Deliberatu zuen laster gauza hortaz.

Potzok zion laburzki gathuari erran:
"Ez dik ainitz balio, ar gaitezen gerlan,
Gaiten bakean bizi gu elgarrekilan,
Bizi izana gatik aspalditik herran..

Gathiak errepostu irkaizño batekin:
"Bakea nahi diat bai segur nik egin;
Hitzemaitea baita errechenik behin,
Segurtatu dezagun zerbait bermerekin.

"Kaiolan egonen haiz hi bethi nagusi;
Sukaldea enetzat beharko duk utzi:
Hire uzten dauzkiat, ez izan gosegi,
Hezur, artho-achalak balimbazauk aski!.

Berri direno gauzak churi zauzku denak,
Zahartuz aldiz dira laster histen berak.
Minitz eta Potzoren arteko bakeak
Bururatu zituen asteko egunak!

Ahantzirik tratua, igande goiz batez,
Potzoren kaiolara Minitz juan zen... chichtez
Han chakurraren bazka hatzemanik...sortzez!!
Urrikirikan gabe jan zuen... bi kolpez!

Gathuaren lanari Potzo zen ohartu;
Ordu beretik zuen bakea trenkatu.
Sukaldean nagusi gogotik zen sartu,
Eta Minitzekila gasailari lotu.

Chakur eta gathuen iduriko ainitz
Gutartean berean dabil egun guziz:
Bakia nahi lukete, bai berdin erosiz,
Beren hutsen kentzeko bakia aski balitz.

(S.E..