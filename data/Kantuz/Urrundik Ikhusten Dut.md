---
id: ab-4032
izenburua: Urrundik Ikhusten Dut
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Urrundik ikhusten dut, ikhusten mendia,
Beraren gibelean baitut nik herria;
Jadanik dut aditzen, zorion handia!
Ezkila maitearen hasperen eztia.

Ezkila, zer othe duk hik egun erraten ?
Urrunera zer berri othe duk igortzen ?
Mendiek hedoipetik dautek ihardesten,
Hik errana zerura ditek helarazten

Landako langilea, artzain mendikoa,
Ithurriko bidean dohan neskatoa,
Aditurik, ezkila, hire boz lañoa,
Othoizten hasi dituk Ama Zerukoa.

Nik ere dut othoizten Birjina Maria,
Basherrietan galdu haurren gidaria;
Neretzat othoi dezan ardiets grazia
Bakean kausitzeko nik egun Herria.

Mendiak utzi ditut urrun gibelean,
Herria dut ikhusten jadanik aldean.
Zer duk, ene bihotza, saltoka barnean ?
Othe duk huts eginen herrira heltzean ?

Agur, agur, herria ! agur, sor-lekua !
Agur, nere haurreko lekhu maitatua !
Jainkoak aditurik haur baten oihua,
Hire gana duk haur bat egun hurbildua.

Mendiaren hegitik hartuz zeiharrera,
Iduri chingola bat, aldapa behera,
Bidechka, hi chuchen haiz jausten zelaiera,
Chuchen ereman nezak ahaiden artera.

Bide hegiko haitza, zembatez haurrean,
Igandetan mezatik etchera sartzean,
Zembatez ez naukjarri, amaren aldean,
Hire adar lodiek egin itzalean !

Baratze gibeleko elhorri churia,
Bethi duk begiratzen haurreko tokia,
Hik bezala zertako, aldaska garbia,
Ez diat sor-lekhuan higatzen bizia ?

Bainan nere begitik nigar bat jausten;
Bozkarioak darot bihotza gainditzen;
Etchekoen boza dut jadanik aditzen.
Jainkoa, darozkitzut eskerrak bihurtzen .