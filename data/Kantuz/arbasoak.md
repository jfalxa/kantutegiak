---
id: ab-4029
izenburua: Arbasoak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mila bat zortzi ehun eta
Lau etan hogoiean,
Bertsu berri hauk eman dire
aire zahar batean;
Gure aitaso kantabreak
Lo baitaude lurpean;
Ez othe dire atzarriko,
Aire hau aditzean ?

Mendi gainetik hasten banaiz
Khanta hunen khantatzen,
Oihan zokhotik, oihartzuna,
zer dautak ihardesten ?
Hobietarik aitasoak
othe dire mintzatzen ?
Zer haiteke, hi, oihartzuna,
Heien oihua baizen ?

Gure aitaso kantabreak,
Mende zaharrenetan,
Gu orai gisa hedatuak
Alhor hautan beretan,
Anaiak ziren : Guziak bat ,
Mendien bi aldetan:
Jainkoa choilki zen errege,
Eskualdun etcholetan.

Nork ez dituzke gure aiten
Balentriak aditu ?
Ekaldetako zombat etsai
Hek ez zuten suntsitu !
Semeak aita dik iduri,
Zeren nehork ez baitu
Bere herrian, egundaino,
Eskualduna garhaitu.

Larrun, Mundarrain, Altabizkar,
Zuen hegi muthurrak,
Mihise batek hila gisa,
Gorde ditik elhurrak;
Larru baletza orai ere
Azaletik haitzurrak,
ikhus gintzazkek agerian
Aita beren hezurrak.

Erreka zepho zilhoetan
Bizi bada belea,
Toki goretan arranoak
Eiten du ohatzea;
Hala du mendi bizkarretan
Eskualdunak Etchea,
Biek handizki dutelakotz
Maite libertatia !

Zer da liburu zaharrenek
Irakhasten dutena ?
Hurrunegitik heldu dela
Eskualdunen omena:
Ez dakitela noiztik duen
Eskuarak hastapena...
Zeren Eskuara baita naski
Mintzoetan lehena.

Jakintsun batek, aditu dut
Nonbait erran duela:
Eskualdun izan baino lehen
Gu herdaldun ginela.
Ichlik ago ahal baduk !
To ! jakintsun ergela,
Ez erran guri arranoa
Beletik heldu dela !

Hire amaren besoetan
Lo hagoen, haurtchoa,
Aingeruekin ametsetan,
Irriz hago, gaichoa;
Erregerentzat on badire
Phordoin eta khoroa,
Hobeago duk hiretako
Amattoren altzoa !...

Nola birundan arbolari
Da segitzen itzala,
Bakhotchak hala sor-lekhua
Bethi maita dezala !
Anaiak gaituk Eskualdunak;
Orai ere, ochala,
Jartzen bagine "Guziak Bat"
Lehenago bezala .