---
id: ab-4038
izenburua: Gerla Ondoan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Behatu diotet, gaur, gure mendietari
Eta ikusi ditut arthaldez igeri !
Behature diotet oihan handieri,
Ederki hostaturik hirriz zauden neri !

Karrankaz airatuak, han, bortu gainean,
Arranoak, lerroka zeru gorenean
Harat hunat zoazin, librotasunean...
Soldado-Choririkan bat ez, urrunean !

Ur-chirripa heldu zen, garbirik, goizean,
Othoitzean Jaunari, kantuz-erasian!
Chori papo-gorriak, leiretan sasian,
Hurrupaño bat dio hartu airatzean!

Barrandan egotu naiz, kanoia, zafraka,
Noiz behar othe nuen aditu, milaka...
Bildotch-ardiak ditut entzun marrumaka;
Artzaina ondotikan, heldu zen chichtuka!

Nere etchea churiak - hirriz dena ari
Dir dir egiten zion argi-zirrintari!
Erleak, burrunbaka, lothuak lanari...
Andre-gaia zabilan... bihotza kantari !

Hil-mezuen orde, nik alegrantzietan,
Espos-oihuak ditut entzun, elizetan!
Kapa ilhunak, orai, beude chokoetan...
Oi, zombat joia pullit, aurthen herrietan!

Izatua naiz beilan, gerla-tokietan,
Eta zer ikusi dut, hildo ilhunetan ?
Ogi urhezkoa zen heldu, lorietan !
Chori bat kantuz ari, kanoi baten puntan !

Ikusi izan ditut hango hil-herriak,
Milak han, martirak, lerroka zarriak;
Hauchet zioten orok, hantchet eroriak:
Bakea duzuela, Bakea, Herriak !

Eguerri zen, hain chuchen, nik uste gabean;
Sartu nintzen, goiz batez, eliza batean;
Jesus-Haurra mintzo zen, bi taulen artean:
Loria zeruetan! Bakea lurrean.