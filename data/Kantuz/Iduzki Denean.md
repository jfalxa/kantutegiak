---
id: ab-4068
izenburua: Iduzki Denean
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Iduzki denean, zoin den eder itzala!
Maïtia, mintzo zira, plazer duzun bezala.
Egiten zuzula mila disimula,
Iñorantzirela;
Erraiten duzula
Bertzetaik naizela
Falsuki mintzo zira.

Zazpi urthez bethi, jarreiki zait ondotik,
Erraiten zaundala, ez dut bertze maiterik;
Hitzer fidaturik
Nago trompaturik
Gaichoa trichterik,
Ez dut eiten lorik,
Ez jan edan onik,
Maitia, zure gatik.

Oi! ene maitia, nik ere zure gatik,
Pasatzen dizut nik, zombeit sabeleko min,
Eskonduz zurekin
Samur ait'amekin,
Behar nindeite jin;
Ez dakit zer egin,
Ez eta nola jin,
Ni neure etchekoekin.

Oi! ene maitia, nun duz'izpirituia,
Fediaren jabe, aita deraukazuia;
Atchikazu fedia
Maita berthutia
Libra kontzentzia,
Munduko bizia
Labur da maitia,
Ez zaitela kambia.