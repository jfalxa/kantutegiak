---
id: ab-4037
izenburua: Deserturra
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Biziaren primaderan harmetara joaitea
Zorigaitzik dorpheena zitzautan jasaitea:
Desertatuz uste nuen hobeki izaitea;
Pentsamendu zoroa, hik egin nau kan kaltea ?

Buru-kolpe itsua, hik egun batzuen gatik
Sekulako zertako nauk galarazi herritik,
Aita ama haurrid.eta adichkiden artetik ?
Gehiago othe naitek ikhusiren bizirik ?

Gaizkirik den gutiena egin gabe nehori,
Gaichtaginik handiena naizela dut iduri.
Zer malhura, gehiago etzautala zilhegi
Agur baten erraitea nere sor-lekhuari !

Nere baitan gozo onik orai ez da sekulan,
Bihotzeko har batekin izpiritua alhan,
Ametsetan ere bethi mugarrien ikharan,
Bethi beldur aintzinegi zangoa sar dezadan !

Gauaz eta egunaz dut begien aitzinean
Sor-etchola nere hura larrain bichkar batean.
Bichta-bichtan hantche dago ithurri bat aldean
Oh ! nik nuen zoriona han bizi nintzenean !

Non zaituztet, mendi urdin, chirripa ithurriak,
Nere bethi, ahuntz eta ardi bizkar guriak;
Non lehengo nere bide, chingola iduriak,
Amets histu bat neretzat zaretenak guziak ?

Besta egun herrikoan nere gazte lagunak
Chirola joz, badohantzi kantuz plazara denak:
Gaur pilotan, bihar dantzan hekien atseginak.
Ni, hetarik urrundua, zer nere bihotz minak !

Oh ezkila ! hi neretzat lehenik jo huena,
Hik orobat joko bahu nere azken orena !
Hire itzalean nian bizirik urosena,
Aditzean mendi hetan hire hats-beherena.

Nere ama orai urrun kausitzen dut niganik:
Gehiago ez dut hemen haren begitarterik.
Nihor, ez adichkide bat nitaz artha duenik;
Nihor, nere oinazetan kontsolatzen nauenik.

Hemen jende arrotzetan ni hiltzen naizenean,
Bihotz minik gabe naute ezarriren lur-pean.
Nork daut gero, noiz edo noiz, orhoitzapen batean,
Loretto bat pausaturen nere tomba gainean ?

Bozkarioz bizitzera, ukaturik herria,
Desterrurat bertze behin ez nindake abia.
Bai ondikotz ! Kiratsa zait desterruko ogia !
Hemen ezin bertzean dut higaturen bizia !

Ur-chirripa ez da galtzen nihoiz bere ohetik,
Ez eta ere, harroka, sarthua den menditik.
Hek bezala sor-lekuaz amodio izanik
Zuhur denak kanta beza bethi bere herritik.