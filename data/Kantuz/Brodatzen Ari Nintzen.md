---
id: ab-4064
izenburua: Brodatzen Ari Nintzen
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Brodatzen ari nintzen ene salan jarririk;
Aire bat entzun nuen itsasoko aldetik;
Itsasoko aldetik untzian kantaturik

2
Brodatzen utzirik, gan nintzen ama gana:
Hean jaliko nintzen gibeleko lehiora,
Gibeleko leihora, itsasoko aldera?

3
- Bai, habil, haurra habil, erron kapitainari
Jin dadin afaitera, hemen deskantsatzera
hemen deskantsatzera, salaren ikustera.

4
- jaun kapitaina, amak igortzen nau zu gana
jin zaiten afaitera, hantchet deskantsatzera
Hantchet deskantsatzera, salaren ikustera.

5
- Andre gazte charmanta, hoi ezin ditekena!
Iphar haizea dugu, gan behar dut aitzina!
Ezin ilkia baitut, hauche da ene pena.

6
Andre gazte charmanta zu sar zaite untzira,
gurekin afaitera, eta deskantsatzera,
Hortchet deskantsatzera, salaren ikustera

7
Andre gazte charmanta igaiten da untzira,
han emaiten diote lo-belharra papora
eta untzi handian lo dago gaicho haurra.

8
Jaun kapitaina nora deramazu zuk haurra?
Zaluchko itzulazu hartu duen lekura,
Hartu duzun lekura, aita-amen gortera!

9
- Nere mariñel ona, hedazak heda bela!
Bethi nahi nuena jina zaitak aldera!
Ez duk hain usu jiten zoriona eskura!

10
- Jaun kapitaina, nora ekarri nauzu huna?
Zalu itzul nezazu hartu nauzun lekura,
hartu nauzun lekura, aita-amen gortera.

11
- Andre gazte charmanta, hori ezin egina.
Hiru ehun lekutan juanak gira aitzina...
ene meneko zira, orai duzu orena...

12
Andre gazte charmantak hor hartzen du ezpata
Bihotzetik sartzen ta, hila doa lurrera!...
Aldiz haren arima hegaldaka zerura!

13
- Nere kapitain Jauna, hauche duzu malurra!
- Nere mariñel ona, norat aurthiki haurra?
- Norat aurthiki-haurra? Hortchet itsas zolara!

14
Hiru ehun lekhutan dago itsas-leihorra:
Oi Ama Anderea, so egizu leihora...
Zur.alaba gaichoa, uhinak derabila.