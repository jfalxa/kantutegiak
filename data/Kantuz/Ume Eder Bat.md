---
id: ab-4083
izenburua: Ume Eder Bat
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ume eder bat ikhusi nuben
Donostiako kalean,
Itz erdichko bat ari esan gabe
Nola pasatu parean.
Gorphutza zuan liraña eta
Oñak zebiltzan airean...
Politagorik ez det ikusi
Nere begien aurrean.

Aingeru zuri paregabea,
Euskal-herriko alaba,
Usterik gabe zuganak bethi
Nere bihotzak narama:
Ikusi nayan bethi or nabil!
Nere maitea, au lana!
Zoraturikan emen naukazu
Bethi pensatzen zugana.

Galai gazteak galdetzen dute:
Aingeru ori nun dago?
Nere maitea nola deitzen dan
Ez du iñorchok jakingo.
Ez berak ere ezluke naiko
Konfiantza horretan nago:
Amorio dun bihotz hoberik
Euskal-herrian ez dago.