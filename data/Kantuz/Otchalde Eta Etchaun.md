---
id: ab-4056
izenburua: Otchalde Eta Etchaun
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Agur, adichkidia, Jinkoak egun hun;
Zer berri den errazu, zuk, othoi Chuberun?
Ezagutzen duzia Barkochen Etchaun?
Holako kolblaririk ez omen da nihun...
Bisitaz joan nindaite, ez balitz hain urrun.

2
Jauna, ezagutzen dit Etchaun Barkochen:
Egun oroz nitzaiko hullanik ebiltzen.
Bethi gazt.ezin egon, ari da zahartzen.
Lau hogoi urthe ditu mundin igaraiten;
Orai ez da bertsetez hanitch okupatzen

3
jauna, behar dautazu gauza bat onhetsi,
Bainan ez zautzu behar hargatik ahantzi;
Huntaz egiten dautzut gomendiorik aski
Komprenituren duzu hitz laburrez naski:
Errozu ene phartez milaka goraintzi.

4
zure komisiona nahiz dizit egin,
kombenitzen zidazut hanitch plazerekin.
Egon nahia zira arren jaun harekin?
Huna Etchaun bera, present da zurekin...
Nik ere ez diota zu nor ziren jakin?

5
jakin ere behar duzu, dudarikan gabe,
zeren egin dautazun errespetuz galde.
Entzutia baduzu nitaz lehen ere;
Nor nizan erraitia ez dizit herabe:
Lapurdin deitzen nute Jean Baticht Otchalde

6
bihotazren erditik egiten dit irri,
zure ganik entzunik parabola hori.
Damutzen ez bazaiko zeluko Jaunari,
Behar ditzigu eman zumbeit berset berri,
Bai eta ber demboran eskiak elgarri.

7
amodioz derautzut eskia hedatzen,
zembat atsegin dutan ez duzu phentsatzen.
Gizon batzuek nute ainitz atakatzen,
Ez dutala ikasi phertsuen emaiten...
Zuk laguntzen banauzu ez nute lotsatzen.

8
Etchaun Ziberuan, Otchalde Lapurdin
Buruzagi dirade kantoren egitin.
Ez gitutzu gu beldur, nor nahi jin dadin;
Erranen dit orotan gure Uskal-Herrin:
Jokaturen dugula hek plazer dutenin.

9
Ez zaitela bada sar sober.urgulian
Etsaminatu gabe zur.izpiritan.
Irakurtu izan dut, lehen, liburian:
Jaunen jaunak direla agertzen mundian,
Erbia lo dagola ust.ez den lekian.

10
Lehen zaharretarik nik entzun dut hori:
Bethi badela norbeit denen buruzagi...
Arren, nahi badira gure kontra hari,
Unheski kantatzeko zombeit berset berri,
Lotsa gabe gituzu denen zerbitzari.

11
Huntan akabatzen dut erranez: Halabiz!
Ez dolu-minik hartu erraiten dut berriz:
Hiltzeari nigarrez, sortzeari irriz;
Hola joanik badugu hemen martir ainitz
Ochala! Hek bezala zeruetan banintz!

12
Egunaren pare da gizonen bizia:
Goizian azkartasuna, zer inozentzia!
Eguerdian da aldiz indar garhaitia;
Arratsaldian berritz flakatzen hasia;
Ilhuna zerratzian akabo guzia!!.