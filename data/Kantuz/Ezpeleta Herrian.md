---
id: ab-4119
izenburua: Ezpeleta Herrian
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ezpeleta herrian ostatu batian
Gerthatu izan naiz bazkari batian
Uste gabian laur lagunetan;
Etchek' anderia bere lanetan,
Espantuia franko bere salsetan
Ageriko da gero obretan.

Etchek' anderia goizik jeiki zen;
Zortzi orenetan suia hila zen,
Haragia' ere bucherian zen,;
Hamek'orenetan garburen sartzen,
Eguerditan bazkaria prest zen;
Manera onez zopa on bat zen.

Zopa jan eta haragi ori
Ekharri dauku egosi berri,
Begiratu eta egin neron irri;
Pochi pochi bat eman zautan niri,
Zer jeneros zen etzen ageri,
Mamia guti, hezurra handi.

Arnoa zuten Bordalekotik,
Edo bertzela taula zokhotik,
Agollatua ongi uretik;
Edan dezagun beraz gogotik,
Horditziaren beldurra gatik,
Horrek ez gaitik joko burutik.

Zalhu dabila zerbitzaria,
Salsa bat badu egin berria
Nahi dugunez haren erdia;
Egin dezagun beraz guardia,
Saltsa eztela egun berria,
Bezperakua, mintzen hasia.

Galdetzen derogu zerbitzariari
Ekhar dezagun erraki hori
Ongi errerik, gizenian guri,
Begiratu eta egin neron irri,
Zikhiruaren fama tzarrari;
Mehe zen bainan etzen ernari.

Kafe on batek oro ahazten
Galdetu orduko berehala prest zen,
Ura doidoia pegarrian zen;
Hasi zen beraz noizbeit ekhartzen
Ura doidoia hasia belchten:
Kikera erdi bat phurruska bazen.

(OTXALDE.