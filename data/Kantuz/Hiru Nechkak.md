---
id: ab-4026
izenburua: Hiru Nechkak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ithurritik jitean hiru nechka eder
Ari ziren kantu bat irakasten bester:
Pollita kausiturik ikasi dut laster
Bai gora Euzkadi Orai eta bethi
Bai gora eskuara Azkar bizi bedi.

Batek zaia churia zakharren polliki,
Aingeru baten pare arin-arin joaki...
Eskualdunak fedea beiratzen badaki.

Bigarrenak gorria zaukan dabantala,
Begia urdin bezain argi ta leiala...
Eskualdunek garbirik daukate odola.

Hirugarrena berriz, airos bidez-bide,
Esperantza-kolore, soinekoak pherde,
Nehoiz etsitu gabe gure hartan gaude.

Hiru nechka ederren hiru soinekoak
Eskualdun banderaren dira parekoak
Gora gure herria eta herrikoak .