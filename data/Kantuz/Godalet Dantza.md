---
id: ab-4114
izenburua: Godalet Dantza
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Godalet dantza,
Godalet dantza...
Besta hunen akabatzekotz
Emanen dugu phikoz
Godalet hunen ungurian
dantza famatu ederrena.

Godalet dantza,
Godalet dantza...
Segi gaiten usai onari;
Kasu godalet horri:
Erortzen balitz zangopean,
Itsusi laite guzientzat.

Godalet dantza,
Godalet dantza...
Godaletak arnoz betherik
Ez duke ichur peilik:
Sobera maite dugulakotz
Mahats odola edaritzat.