---
id: ab-4036
izenburua: Ainhoarraren Kantuak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Zorigaitzez egun batez zortheak atzemanik
Nere burhaso zaharrak nigarretan utzirik
soldado nintzela bada harmetan paraturik,
Ninduten urrun eraman, urrun Eskual herritik.

Sor-lekhutik urruntzea zem den lastimagarri !
Maite guzien uztea oi zein erdiragarri
Nik ez nuke sekulakotz adios erran nahi
Ez nere etchekoeri, ez Eskual-herriari.

Laster igorri ninduten Afrikaren barnera;
Lau urthe han egon eta deithu Italiara;
Lekhu orotarik bethi hegalekin bezala,
Gogoa zaitan airatzen arin Eskual-Herrira.

Italiako lurrean sartuz geroz Frantsesak.
Oi nolako sarraskiak ! oi zer gizon galtzeak !
Odol-hibaiez dirade han gorritu bazterrak:
Holakorik ez dezala ikhus Eskual-herriak.

Burhaso gaicho hoitarik semeak galdurikan,
Zenbatak ez dire jarri auhenik minenetan !
Gure jainko zerukoak hoin bihotz-min handitan
Beira gaitzal erortzetik, guziz Eskual-herrian

Eskualdunen omenari ez diot egin ukho,
Nere sabre-baionetak, maiz agertu lekhuko;
Solferinon kolpaturik zioten gehiago !
Ez nintzela ez bizirik ni euskal-herrirako !

Atsulaiko bizkarrean Ainhoako kapera:
Han du nere ama onak othoitz egin ardura,
Aingeruen erreginak bida nezan etchera,
Bizirik eta osorik ekhar Eskual-herrira.

Nere ama entzun duzu, oi Andre Amultsua!
Azken hatserat nindagon jadanik hurbildua;
Zuri esker baizen ez dut nik beiratu bizia,
Zuri esker ikhusi dut berriz Eskual-herria!

Mundarraingo mendi hori bethi kopeta gora,
Ikhusteak phizten deraut bihotzean pilpira;
Italiako lurrtik eri nintzen athera,
Bainan sendatu heltzean gure Eskual-herrira.

Irriskuak iraganik trankil orai etchean,
Hauche diot Jainkoari galdetzen azkenean:
Bizi dadin hainitz urthez aitamekin batean
Nere egunak higatuz Eskual-herri maitean.