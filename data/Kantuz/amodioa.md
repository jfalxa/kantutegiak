---
id: ab-4066
izenburua: Amodioa
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Amodioa zoin den zoroa
Mundu guziak badaki.
Nik maiteño bat izaki eta beste batek eramaki...
Jainko maiteak gertha dezala enekin baino hobeki...

2
Azken bestetan egin nituen
Izarraren ezagutzak;
Denen artean ageri ziren
Haren begi urdin-beltzak;
Irri pollit bat egin baitzautan,
Piztu zauztan esperantzak.

3
Geroztik ere mintzatu gira,
Eia nahi ninduenetz...
Harek bietan baietz erranik,
Pentsa kontentu nintzanez!
Ez nuen uste haren agintza
Bethea zela gezurrez.

4
Orai bihotza urthua daukat
Galtza urean bezala
Izar onaren argien ordain,
Izigarriko itzala!
Ohoin tzar batek berriki joanik,
Nola naiteke kontsola.