---
id: ab-4085
izenburua: Charmegarria Zira
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

« Charmegarria zira eder eta gazte,
Ene bihotzak ez du zu baizikan maite.
Bertze zembait bezala, othe zira libre ?
Zurekin ezkontzeaz dudarik ez nuke.

Churi gorria zira, arrosa bezala :
Profetak ere dira mintzo hola hola.
Araberan bazinu gorphutza horrela,
Iduriko zinuen... zeruko izarra.

« Oi maitea, zatozkit, plazer duzunean,
Nehork ikusi gabe, ilhun-nabarrean ;
Lagun bat badukezu joaiteko bidean
Hark ezarriren zaitu trankil bihotzean.

«Plazer eginen duzu, ichiltzen bazira,
Haur iñorantak hola trompatzen baitira
Ez da enetzat ina holako segida. ...
Bertzalde zure baithan ez naiteke fida !

«Adios, beraz orai, ene arraroa,
Hori dela medio herritik banoa ;
Bihotza trichte eta kechuan gogoa,
Bethi jarraikiren zait zur'amodioa. .