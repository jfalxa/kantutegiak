---
id: ab-4088
izenburua: Iruten Ari Nuzu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Iruten ari nuzu, khilua gerrian,(berriz)
Ardura dudalarik (berriz) nigarra begian.(berriz)

Nigar egiten duzu, oi suspirarekin! (berriz)
Kontsolaturen zira (berriz) oi demborarekin. (berriz)

Ezkont-mina dudala zuk omen doizu (berriz)
Ez dut amore minik (berriz) gezurra diozu.(berriz)

Ezkont-mina dutenak seinale dirade (berriz)
Mathel-hezurrak idor (berriz) koloreak berde.(berriz.