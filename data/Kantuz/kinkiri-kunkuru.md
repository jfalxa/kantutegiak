---
id: ab-4100
izenburua: Kinkiri-Kunkuru
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Errandut, erranen et'errango
Ez naiz ichilik egongo:
Plaza huntako dama gazteak
Ez dira monja sartuko
Ala kinkiri, ala kunkuru ala kinkiri kunkuru kanta!
Ala kinkiri kunkuru kanta
Gu bezalakoak dira, hola!

Erran dut, erranen et'errango,
Ez naiz ichilik egongo:
Plaza huntako gizon gazteak
Ez dira fraile sartuko...

Erran dut, erranen et'errango,
Ez naiz ichilik egongo:
Plaza huntako gazte guziak,
Ezkontzan dira sartuko...