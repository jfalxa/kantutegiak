---
id: ab-4121
izenburua: Aitak Eman Zerautan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Aitak eman zerautan zerurat juaitean
Enetzat mahastia hartzeko lanean;
Artha nezala
Aitak bezala
Haintzur eta jorra
Laguntzat hor nuela... sotoko ophorra.

Etche churi ttipi bat, badut mahastian,
Sagar, udare, phiko, mertchiken artean,
Etch'aitzinean,
Loren erdian,
Baratze alhorra,
Dutchuluaren pean...sotoko ophorra,

Mahastian choriek goiz dute kantatzen;
Orduan bozkarioz lanari naiz lotzen;
Gero zait hasten
Bihotza hosten,
Agortzen zintzurra,
Laster dut eskuratzen...sotoko ophorra.

Ez dut hiritar gisan gosari ttikia:
Bichkotchatto batekin edari bichia:
Nik dut ogia
Achal gorria,
Bai gasna gogorra,
Eta arno churitik...sotoko ophorra.

Neguan ez da deusik eder mahastian.
Aldachketan molkoak agertu artian,
Ikhusiz huntan,
Osto artean
Larritzen pikorra,
Nola ahantz dezaket...sotoko ophorra.

Igandetan debokti meza dut entzuten
Gero lagun onekin plazetan jostatzen
Han dut aditzen
Nitaz erraiten:
"Zer muthil gotorra!
"Ez ahal dik hastio...sotoko ophorra.

Gorphutza baitut sano, arpegia gorri
Hortako dute nitza zerbait erran nahi,
Bainan ez naiz ni
Nihoiz ibili
Oraino mochkorra,
Izanagatik maite...sotoko ophorra.

Badut moltsa luze bat urhez hampatua,
Hal'ere ezkontzeko lotsaz gelditua,
Beldur bainitzen
Gerta zadien
Andrea mokorra,
Eta hauts ostikoka...sotoko ophorra.

Ene gazte lagunak ezkondu direnak,
Haurrez betheak dire orai gehienak;
Balute ere
Bertze hainbertze,
Ez naiz jeloskorra,
Haur hainitz bezain on dut...sotoko ophorra.

Herriko zahar batzuk, sotorat ethorriz,
Han edukitzen naute lehertua irriz.
Konkorrek kanta,
Mainguek dantza,
Oihuka elhkorra...
Biba! zaharrak eta...sotoko ophorra.

Burlaka hasten baita artzaina menditik,
Zaharrek errepostu mahasti gainetik:
To edantzak hik
Chirripakotik
Gibel aldez gora,
Guk husteko demboran...sotoko ophorra.

Ophor hori ez baitzen andrentzat egina,
Hekier dut eskaintzen gathelutto bana;
Hastio naute
Erraiten dute:
Donado zingorra!
Chehaturen ahal zaik...sotoko ophorra.

Bertsu hauk egitean iphar-gochoa zen
Sotoan hari nintzen arnoen aldatzen;
Donado hemen
Zein ongi naizen,
Egin dut aithorra,
Tente chutik aldean...sotoko ophorra.

(Pierre DIBARRART. Baigorrikoa.