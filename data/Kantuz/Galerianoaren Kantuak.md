---
id: ab-4062
izenburua: Galerianoaren Kantuak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Mila zortzi ehun.ta hamabortz garrena,
Ni Ahazparnen preso hartu nindutena;
Plumagaineko premu orok dakitena,
Galeretan higatu beharko naizena.

2
Kantatzera nihazu alegera gabe,
Ez baitut probetchurik trichtaturik ere;
Nehun deusik ebatsi, gizonik hil galbe,
Sekulakotz galerak enetako dire.

3
Ala zorigaitzeko premu izaitea!
Harrek eman baiteraut bethikotz kaltea;
Aitari galdeginik sortzeko partea,
Galeretan eman nau, hauche duk dotea!

4
Ene aita da gizon kontsideratua,
Semia galeretan du segurtatua;
Nun nahi othoitzean belaunikatua,
Saindu iduri debru madarikatua!

5
Ene lehen kusia, cadet Bordachuri,
Fagore bat banuke galdetzeko zuri;
Ongi adreza zaite ene arrebari,
Ene saitzeko zomnbat ukan duen sari?

6
Aita aintzinean.ta arreba ondoko,
Osaba burjes hori diru fornitzeko;
Ez ordian enetzat bi sei liberako,
Galeretan bederen leher egiteko!

7
elizan sartzen dire debozionerekin;
iduriz badohazila saindu guziekin;
beren liburu eta arrosarioekin,
debruak pesta onik einen du heiekin.

8
Zortzigarren bertsua aneiaren dako;
Kontseilu bat banikek hiri emaiteko;
Ontsa goberna hadi, etzauk dolutuko,
Ni baitan etsenplua errech duk hartzeko.

9
Zuri mintzo nitzaizu, oi, aita zilarra!
Ardura dudalarik begian nigarra;
Zure eta ene arraza Bordachuritarra,
Galeretan naizeno ni banaiz bakarra.

10
Kantu hauk eman ditut paube-ko hirian,
Burdinez kargaturik, oi! Presondegian;
Bai eta kopiatu dembora berian,
Orok kanta ditzaten Hazparne herrian.

11
Hok eman izan ditut, ez changrinatzeko;
Ahide, adichkidek, kuraie hartzeko,
Eta partikulazki, aita, zuretako,
Kantu hok aditzean, semiaz orhoitzeko.