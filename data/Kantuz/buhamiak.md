---
id: ab-4134
izenburua: Buhamiak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Buhamiak erosi du lau sosetan ogia;
Lagunari saldu dio bortz sosetan erdia
Lou Peyroutoun lou Peyroutoun
lou praubo praubo Peyrotoun.

Buhamiek badakite trikun-trakun egiten;
Ahatea ebats-eta galtzarpean gordetzen
Lou Peyroutoun...

Buhamiek badakite salda ona egiten;
Oiloñoa ebats-eta eltze zaharrian egosten.

(Athaka ongi pesta egin ondoan):
- Neure andriak iduri du zaku gaizki josia
Gaztain errez ase eta ai zer trompet-airia

(Errepostua):
- Hautsi zitan senharra nik nahi nuen bezala;
Zahartu, konkortu, gaizotu, jeus balio ez zuela
(Akabatzeko kolpeka partida bat...