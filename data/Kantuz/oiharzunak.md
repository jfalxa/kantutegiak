---
id: ab-4052
izenburua: Oiharzunak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Bizia niri baitzautan
Ezin jasanezko zama,
Ahuen minez betherik dut
Erran: zer egin, zer asma.
Jinko Jaun zuzenik othe da,
Mundu hau zerek derama?
Oiharzunak erreka zolan
Ihardetsi deraut: Ama!

2
Ama...! zion oiharzunak
Hegaldatuz inguruan...
Ezti zitzauzkitan minak
Zintudalarik munduan...
Orai nun zaude jakiteko
Nigarrez hemen zer dudan?
...Oiharzunak aldiz airoski
Dio: zeruan, zeruan!...

3
Ama! Zeruan bazaude
Jainkoak zaitu hor emana.
Ez bazitake jauts hortik
Zure semearen gana,
Igor dezadazu bederen
Gizonen zain dabilana?
...Oiharzunaren oihua zen
Orotarik: lana, lana!

4
zeruko Jinkoak, zer dut
Hoi behar nere minekin?
Badut aski pairaturik
Hemengo biziarekin!
Lan horrek nu eta chahutzen
Bere dohain tzarrenekin:
Oiharzunak bortuan gora
Zion: enekin, enekin!...

5
Zurekin? Oi eskuz-esku
Zatozkit ba laster, Jauna,
Zorigaitzak jo dituen
Gizon flakoen laguna!
Zer!...orai zu zaitut entzuten,
Aditzean oiharzuna
Haizearekin bazterretan
Diolarik; huna huna!


6
Urrun etzatera zoan
Odoletan iguzkia;
Osoki karra galdurik
Goizdanik baitzen jeikia.
Eta nik jakin dut orduan
Lur huntan zer den egia:
Behar dugula noizpeit orok
Nigarrez busti begia.

7
Bainan iguzkia nola
Bethi berriz phizten baita,
Gu berma gaiten othoiztuz
Zeruetan dugun aita,
Hots, auhen minetan dezagun
Bihotz bihotzetik kanta:
Garraituren dugu segurki
Zorigaitzaren zaparta!

8
Lehen izarrek argitzen
Zuten zeru gain urdina,
Nik igorri dutanean
Euskaldunen irrintzina:
Haren hegalekin bortuan
Aira zadin ene mina...
...Arrats hartan zombat gocho zen
Oiharzun airos arina!.