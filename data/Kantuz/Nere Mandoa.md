---
id: ab-4130
izenburua: Nere Mandoa
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Nik erosi dut mando zaharzahar bat
Pagatu baitut asto bat eta joar'zar bat
Nere mandoa!
Abere gaichto dirurikan
askorikan gostarik pagaturikakoa.
Nere mandoa!

Nere mandoa jaten dizi garagar,
Hura jan-eta egiten baitu far-far-far...
Nere mandoa!
Abere gaichto, etc...

Getharian kargatu nuen arrañez:
Nere arrañer nehor ezin hurrant urriñez...
Nere mandoa!
Abere gaichto, etc...