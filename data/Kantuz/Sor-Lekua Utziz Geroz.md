---
id: ab-4053
izenburua: Sor-Lekua Utziz Geroz
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Sor lekhua utziz geroz
Ondikotz hala beharrez!
Jainko ona urrikaldu
Da bethi nere nigarrez:
Primaderan hasi orduko
Arbolak estaltzen lorez,
Choritto bat heldu zaut bethi
Nere herritik hegalez.

2
Nere ganat hain urrundik
Ethortzean, unhatua,
Arbolarik hurbilena
Du bere pausa lekua.
Aldachka goren gorenean,
Horra nun den lokhartua:
Luma pean zango bat eta
Hegal pean du burua.

3
Pausa hadi, lo egizak
Chori maitea, bakran
Atzarria ni hire zain
Hemen nauk hire aldean,
Ur chortarekin horra gero
Papurrak leiho gainean;
Baina gero hango berriez
Orhoit hadi atzartzean!

4
Orhoit hadi bai, choria,
Herri maiteko berriez:
Nere aitaz, nere amaz,
Nigarrez han nik utziez,
Mintza hakit aurhidez eta
Mintza lagun on hekiez,
Mintza nihor ahantzi gabe,
Maite nintuen guziez.

5
Atzar hadi, atzar bada,
Berriketari abila,
Nere beldur izan gabe
Jin hakit urbil urbila;
Ene leihuan izan hizanez
Erradak ichil ichila,
Izan ere hunatekoan
Solas ordainaren bilha.

6
Choria, lo hagolarik
Ikharan nagok aldean:
Ez ahal duk zori gaitza
Maite nautenen artean!
Hala baliz, othoi choria,
Berriz herrirat heltzean
Loretto bat nigar batekin
Pausazak tomba gainean

7
Chorittoa joan denean
Ostoak eror-demboran,
Berriz ethor ez dadien
Beldurrez nago ikharan:
Ihiztaria, hartzen baduk
Nere choria saretan,
Utzak, othoi, gaichoa libro
Berriak ekhar ditzadan.