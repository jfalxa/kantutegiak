---
id: ab-4070
izenburua: Ziburutik Sararat
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ziburutik Sararat izatu naiz joana,
Neure lehenagoko maite baten gana;
Han arribatu gabe, aitu nuen fama,
Guarda gazte batekin zela hitzemana.

Maitia, bizi zaite gazte'ta lorios,
Bai eta fidel izan, hitzemanez geroz.
Ondoko urrikiak ez du deus balio;
Huntan kitatzen zaitut, maitia, adios.

Uso churi pullita, fin eta fidela,
Deus makhurrikan gabe joan zaizkit gibela;
Jende hitsak badakit nombeit badirela...
Ez bainuen pentsatzen holakoa zinela.