---
id: ab-4117
izenburua: Iragan Besta Biharamunian
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Iragan besta biharamunian,
Berek dakiten choko batian,
Lau andre hirur mutchur din, bat alarguna, jarriak itzalian,
Harri chabal bat belhaunen gainian
Hari ziren hari ziren trukian.

Zer othe duten nik jakin nahi
Pitcharrarekin bertze jaun hori;
Zorroa du biribila, moko mehea, tente chutik egoiki,
Chahako bat othe den nago ni
Hampatua, hampatua ederki.

Jes! nik oraino zer dut ikhusten?
Zer hegalpean dute gordetzen?
Egoitza gocho hortarik, burua alto, chahakoaz trufatzen,
Bihotzez azkarrago zeren den,
Kuiattoa, kuiattoa ari zen.

Handik chimiko, hemendik irri,
Haurrak bezala jostetan ari;
Gogotik ematen dute begi kolpe bat hegalpean denari;
Hitz bat erran gogo diote sarri
Beharrirat, beharrirat kuiari.

Seira truk dira zazpietarik,
Tantoz oro bat bi aldetarik.
Chahakoa eroria da, mokoz iphurdi, naski odol husturik;
Kuiattoa han dago etzanik,
Azken hatsa, azken hatsa emanik.

Gezurrik gabe goazen, Maria;
Eman jokoan ezti eztia;
Hurbil zan untzi beltz hori, dezagun edan trago bat edo bia,
Bihotza dinat epheltzen hacia.
Harek zuen, harek zuen grazia.

Zahagiaren seme joria,
Nun duk achtiko zorro lodia?
Gaizoa, lehen birundan zurgatu dautek odolaren erdia!
Orai hor ago zimurrik arpegia,
Kokoriko, kokoriko jarria.

Nahiz zeraukan azken eskua,
Mariak zuen hasi gudua.
Truk.- Jozan.- Mariak chango, Katichak chaldun, Marichumek , hirua.
Mari-Martin, choratu zain burua,
Lauarekin hiruaren keinua!

Ago, Maria, othoi, ichilik,
Ez dun ikhusi nere keinurik.
Chorta bat edanez geroz, begiak ñirñir; zer ez dun ahalkerik?
Edan nezaken azkarren hortarik,
Gatilua, gatilua betherik.

Bazakinagu, tresna makurra,
Ezdunala hik hatsa laburra.
Ez dun, ez, dionan bezala, horma churgatuz biribildu muthurra,
Ez eta ere gorritu sudurra,
Milikatuz, milikatuz elhurra.

Apho mutchurdin moko biphila,
Zeren ondoan othe habila?
Hatz badun gibel aldean, ikhusiko dun zembat naizen abila;
Chuhur badun, ago, neska chirtchila,
Ichil, ichil, ichil, ichil, ichila.

Kartak utzirik, eta tantoak,
Han zituzten han gero saltoak.
Bakharrik hiruen kontra, zer eginen du Mari-Martin gaizoak?
Nahiz izan azkar zango besoak,
Hartu ditu, hartu ditu paloak.

Herriko besta arrats aphalian
Lau gatu zahar anjelusian,
Bat maingu, hirur saltoka, sorginak pujes! zohatzila bidian.
Nik ikhusi ditut amets batian,
Akhelarre, akhelarre gainian.

(ELISSAMBURU.