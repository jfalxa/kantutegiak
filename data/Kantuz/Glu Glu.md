---
id: ab-4110
izenburua: Glu Glu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Biba Rioja, Biba Naparra
Haren famaren izarra.
Hemen guziak aneiak gira
Hustu dezagun Pitcharra
Glu glu glu glu
Glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu
glu glu glu glu
glu glu glu glu
glu glu glu glu glu glu glu glu glu glu.

Biba Burgoña, biba Chanpaña!
Maite dut zuen tisana.
Nahiz orotan ez den berdina,
Mahatsak bethi jus ona!

Biba herriko lur aberatsak,
Irulegiko mahatsak.
Heien arnotik brauki edazak,
Bai joanen zaizkik ez ontsak...
Glu glu glu glu glu glu glu glu glu glu.