---
id: ab-4107
izenburua: Kaiku
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Nere andrea andre ona da:
Gobernu ona du hauzuan;
Hartzen duelarik bere alaba
Marikattalin altzuan:
Aizazu! zer nah'uzu!
Gerore horrela munduan ere
Biak biziko geraregu,
Baldin bazera kontentu,
Baldin bazera kontentu.

Nik bethi freskotchorik tabernakua
sototchuan deraukat arno gozua.
Ai! zer kontentu! Ai! zer alegre!
Ezkaintzen dautzut, neure maitia,
Arraultz etchuak eta kaiku esnia,
Arraultz etchuak eta kaiku esnia.
Kaiku, kaiku, kaiku, kaiku, kaiku, kaiku esnia,
Arraultz etchuak eta kaiku esnia.