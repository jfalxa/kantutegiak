---
id: ab-4050
izenburua: Chardina-Saltzalea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Untzia jin deneko itsas bazterrera,
Gachuchak erosi tu bi saskiñotara;
Biak altchaturikan airoski burura,
Beha zoin laster doan karrika behera.

2
Zaiak estekaturik gorachko gerrian,
Zango sagarrak ditu arras agerian,
Etchean bezain aise dabila hirian,
Irrintzin bat eskainiz guzier lorian!

3
Itzuliak eginik, "Au bon vin" ethorri,
Zintzurra idor eta sudurttoa gorri,
"Arras akhitua naiz behar dut sokhorri,
Onetik nahi nuke barnerat igorri...

4
Barneareb phizteko, hustu-ta basoak,
Mokoka haste bada zer elhe gochoak!
- Amak, gordazkitzue zuen haur gaichoak,
Dauden garbi bederen hoikien gogoak!!!

5
Mihia badu ere sobera zorrotza,
Zuzen gelditzen zaio nonbeit han bihotza...
Arnoa dukenean, hastio ur hotza,
Ez da nehon ez dena Gachucharen gaitza!!!

6
Eskalerik bada, guziz beharrean,
Dabiltzaner othoizka bide bazterrean?
Gachuchak pausaturik saskia lurrean,
Chardinak eman daizko laster ahurrean.

7
Emanak othe ditu bertzer harrapatzen?
Etchekoeri bethi kario du saltzen!
Alferretan andrea mokoka da hartzen,
Ez dako behin ere sos bat atheratzen!!!

8
Iguzkia zerutik aphaltzen denean,
Oraino.re Gachucha karrika gainean;
Hasi zuen bezala boz ederrenean,
Eguna finitzen du irrintzin batean!

9
Gachucha moko biphil, Gachucha kokina,
Badinat Jinkoari othoitz bat egin:
Beira dezaunan luzaz hire zintzur fina,
Guhauri merkechago igorriz chardina!!.