---
id: ab-4132
izenburua: Jaun Kaputchin
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Jaun kaputchin bizar handi,
Jesusen ganat ethorri;
Astoak jan zion bizarra,
Ustez eta zen belharra.

- Asto zikhin milatua,
Handia duk bekhatua:
Makhil huntarik jasta zak;
Joanen dituk hire hatzak.

- Jaun Kaputchin bizar gorri,
Barkha-zozu asto horri.
Garbi bazinu bizarra,
Ez lidurike belharra.

- San Josepe, barkhamendu!
Ez nuke behar sumindu:
Bainan oraiko laidoa
Izariz goiti badoa.

Jesusek othelakotik
Gauzak oro entzun ditik
Jaun Kaputchin belhauniko
Othoitzean hasten zako.

Erranikan Lauda Sion,
Bizarra pusatu zion;
Erranikan Tantum ergo,
Astoarentzat belhar frango.

Hola biek beren hura
Izan zuten muthurrera.
Ez baziren kontent hola
Freskatzerat joan ditela.