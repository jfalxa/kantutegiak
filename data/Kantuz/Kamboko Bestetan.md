---
id: ab-4137
izenburua: Kamboko Bestetan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Agur jendeak eta idek beharriak,
Baditut zenbeit pertsu eginik berriak:
Zer arrakasta duen gure Eskual-Herriak,
Bainan zer minak ere Atzek ekharriak.

Aurten ezagutu dut - behingotzat treina,
Bidarrai-ko menditik Cambo-rako heina...
Frangotan oinez egin izan dut joan jina,
Bi belhaunen gainerat jautsi zaut adina!

Nere uztez ederki sartu nintzen... gelan,
"Agur jaun andereak" gora nuen erran;
Jarririk lau chapeldun ja baitzagozin han,
Etzauntan bihi batek elhe motz bat eman.

Bi neska sudur luze, mathel hezur idor,
Lore guti dutenak Jainko Jaunari zor;
Ederraren ederrez galdu bada nehor,
Pika hoik ez ditazke gaitz beretik eror!

Meharrek eskas eta bertzek soberakin...
Hauk senhar emazteak zirela dut egin...
Ez da fidatu behar oraiko atzekin!...
Ipurdiz eta zorroz dire biak bardin.

Nungo sorgin gaichtoak nau hunat ekharri!...
Ichil ichila nuzu chokoñoan jarri.
Ez nakien zer debru zioten elgarri!
Etzena neure planta nigar egingarri!

Gantzaduna pokerka, hankak ezin itzul,
Bafadan zemagula bipher eta tipul!...
Neska meharrak aldiz: yes ol rait bioltiful
Ni neure chokoñoan, bethi bardin ahul.

Camborat heldu nintzen, ez baitzen goizegi.
Jaustean hor ikusi Hazparne-ko Hegi.
Hau gose ta... pokerrak nik ezin liseri,
Arnauk eman zeraukun jaterat bieri.

Arnau-ren bazkariak piztu daut bihotza,
Ahantzi dut goizeko kompainia beltza.
Plazan ariko dela Mondragones gaitza,
Partidarat airoski kantuz bagoatza.

Gu harat hel orduko han zagon mundua!
Inguruak hetsirik athea zaindua;
Sartzeko pagatu dut sei pinten dirua,
Camboarra ez naukan hoinbertze judua!

Alkiak lerro, lerro, estaliak jendez,
Bainan hanitz kachketa bonetaren ordez:
Andrez ere leherra, itzalaren galdez,
Lanik aski jar eta... ichtak ezin gordez!

Partidaz ez dezaket erran orai pitsik,
Ordu bat egon bainaiz bi begiak hetsik!
Aintzinean neska bat, buluzia kasik,
Oi bainan etzuena parasol eskasik!

Debru neska zirtzila nehun ezin koka,
Nere belhauner bethi hari zen pusaka;
Errabiatua ni, ez lotzeko joka,
Han numbeit egin diot zapetaz phereka!

Haztatu dueneko, ez "kontu" ez "alo!"
Chutiturik hasi zaut: e di don bie balo!
Haizen bezalakoa, nahi tun bi talo,
Heia bada nork nuen ni emanen a lo!

Erran banuen erran, bazen gero jestu,
Juramentu, kriola, irri eta chichtu,
Ez nuela samurtuz pesta behar histu,
Zer gertha ere, handik lekua dut hustu.

Aski badut erranik, jaun andreak, gab'on!
Ez nadukate berriz ikusirik Cambon.
Frantses, Angles, Español, pormenenbu bagon!
Nahiago dut aise Bidarrain nik egon!

(OXOBI)
(Airea: Dama gazte n.57.