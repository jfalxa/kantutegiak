---
id: ab-4031
izenburua: Makhila
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Begiztatua nuen aspaldi mendian
mizpirondo chuchen bat sasien erdian
Handitzerat utzi dut eta harrekila
Aurten egin dut nere ... eskualdun makila
tra la la la la la tra la la la la la
Aurten egin dut nere eskualdun makila.

Bi erroden gainean joaiten nez ikasi,
Burdin bidetan , aldiz, sabela nahasi.
Bainan nun dut karrosa ? nun dut beribila ?
Oinez beti banoa...eskuan makila.

Kana delako batzu hirian baituzte;
Hoitarik gehienak deus guti, nik uste.
Eztut hoik bezain herbail nik nere kabila,
Eskuan dudalarik...eskualdun makila.

Askotan ikusi dut ibilki herrian
Aitzindari erneak ezpata gerrian.
Bekaiztia ni ganik urrun joan dadila
Zeren ez baitut nik... eskualdun makila.

Igandetan goiz-goiza, chamarra emanik,
Etcheko andreari agur bat erranik,
Entzun eta oihuka dorreko ezkila,
Ni banoa harturik... eskualdun makila.

Nere sakelak nahiz ontsaño miatu,
Jin zait behin gizon bat...Baina zer gertatu ?
Segurki eztu erran bater.ez nakila
Ibiltzen eskuetan...eskualdun makila.

Jaunak atchik nezala luzezki munduan
Bai eta ezar gero betikotz zeruan.
Orduan, haur maiteak, otoi ! nerekila
Emazue hobian...Eskualdun makila.

Gero zeruetako mendi ederretan
Eta hedoietako izar churietan,
Jainkoaren alkitik han urbil-urbila,
Ibiliko naiz lerden...eskuan makila.

Begiztatu nuen aspaldi mendian
Mizpir ondo chuchen bat sasien erdian.
Handitzerat utzi dut eta harrekila
Hemen egin dut nere...zeruko makila.