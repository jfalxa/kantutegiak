---
id: ab-4061
izenburua: Bereterrechen Kantoria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Haltzak ez du bihotzik, ez gaztamberak ezurrik
Enian uste erraiten ziela aitunen semek gezurrik
Enian uste erraiten ziela aitunen semek gezurrik.

2
Andozeko ibarra,
Ala zer ibar luzia!
Hiruretan ebaki zaitan armarik gabe bihotza

3
Bereterretchek oheti
Neskatuari eztiki:
Abil, eta so egin ezan gizonik denetz ageri?

4
Neskatuak berhala,
Ikhusi zian bezala,
Hiru dozena bazabiltzala bortha batetik bestera.

5
Bereterretchek leihoti
Jaun kuntiari goraintzi:
Ehun behi bazereitzola beren ondoti.

6
Jaun kuntiak berhala,
Traidore batek bezala:
"Bereterrtech, aigu borthal: utzuliren hiz berhala..

7
- Ama, indazut athorra
menturaz sekulakua!
Bizi denak orhoit ukhenen du Bazko-gaiherdi ondua!

8
Marisantzen lasterra
Bostmendietan behera!
Bi belhañez herresten sartu da Lakharri-Bustanobira.

9
- Bustanobi gaztia,
ene anaie maitia,
hitzaz hunik ez balinbada, ene semia juan da.

10
- Arreba, ago ichilik!
Ez othoi egin nigarrik!
Hire semia bizi bada, Mauliala dun juanik.

11
Marisantzen lasterra
Jaun kuntiaren borthala!
"Ai, ei, eta, jauna, nun duzie ene seme galanta?.

12
"-Hik bahiena semerik
Bereterretchez besterik?
Ezpeldoi-altian dun hilik; abil, eraikan bizirik...

13
Ezpeldoiko jentiak,
Ala sendimentu-gabiak!
Hila hain hullan ukhen-eta deusere ez zakienak!

14
Ezpeldoiko alaba
Margarita deitzen da:
Bereterretchen odoletik ahurka biltzen ari da.

15
Ezpeldoiko bukhata,
Ala bukhata ederra!
Bereterrechen athorretarik hirur dozena umen da.