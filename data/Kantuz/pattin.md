---
id: ab-4125
izenburua: Pattin
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ez du urte hambatik
sor lurretik joanik
Pattin ethorri zaiku
aberats jadanik.
Agur eta ohore
nahiz denen ganik.
Nor da herrian orai
hori bezain jaunik?

Pattin aberasteaz
balin bada jaundu
Nola gertatzen zaio
ez baita gizondu?
Jainkozko sinhestea
utzia omen du:
Diru hutsak bihotza
nekhez dio ondu.

Pattinen balentria
herrira jin eta
Gu fededun ikusiz
irriz hasi baita:
Jainko legea, Pattin,
hik ez dirok maita;
Hura utzirik bahaiz
ibili sosketa...

Gero Pattini moltsa
diote ebatsi!
Pattin hire ohoinak
milaka goraintzi.
Haren egitatea
ez dakikek gaizki:
Jainkorikan ez zela
charman hau sinhetsi.

Zer egia handia
Aitachok ziona:
Ez dik ez diru hutsak
egiten gizona,
Zinezko gizonari
hau da dohakona:
Jainkoaren beldurra
eta bihotz ona.

(ZALDUBI.