---
id: ab-4136
izenburua: Sant-Ibatek, Andere
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Santibatek, andere,
Aurthen bezala, gerore,
Santibatek igortzen gaitu chingarketa gu ere.

(Egun hartako eskaleak, etche aintzinerat,
beren gerrenekin heldu direlarik.)

Ez gabiltza ederrez,
Ez eta ere beharrez,
Kostuma zaharraren ustez erreprotcha beldurrez.

Tolos'eta Baiona...
Jainkoak dautzul'egun ona!
Guri chingarraren emaitera, jeiki baizinte huna!

Ez dugu nahi urdia,
Ez et'ere erdia,
Usaia gatik, kostuma gatik, liberako zathia,
Liberako zathia eta gerrenaren bethia.

Ez dugu nahi tripotik,
Bertzian izan ez denik,
Beldurrez eta trompa gaitzazten... urdekeriaz betherik.

(Etchek'anderiari lausenguak)

Etchian eder gerrena...
Etchek'andere lerdena,
Zur'erhiko erhaztun hortaz eros niro Baiona!

Etchian eder kortzeiru,
Dabiltzanian inguru...
Etche huntako etchek'anderia, Parabisuan aingeru!

Etchian eder ohako,
Haurra sortzen deneko...
Etche huntako etchek'anderia zaldiz elizarako,
Zaldiz elizarako, etazilhar kadiran jarriko!

Landan eder ilharra,
Haren pean belharra...
Etche huntako etchek'anderia, zer emazte chilharra!

Etchian eder ferreta,
Haren gainean kaneta...
Etche huntako etchek'anderia, zer emazte plachenta.

(Nausiari, erdi lausengu, erdi mehatsu)

Etchian eder aitchurra...
Nausi bilo izurra,
Kolputto bat eman ezaguzu busti dezagun zintzurra!

Kadiran zaude jarririk,
Koloriak gorririk;
Guri chingarren emaitera jeiki bazinte hortik!

(Eskerren bihurtzea)

Eman duzu nobleki,
Kompainiak ere badaki;
Parabisuan sar zaitezela, hamabi aingeruekin,
Hamabi aingeruekin eta zure familiarekin!

(Deus ukaiten ez dutelarik)

Goazen, goazen hemendik,
Hemen ez duk chingarrik;
Etche huntako gazeiti charrian sagiak umiak hazten tik!

Etchian eder aihotza...
Etchek'andere hortz motza,
Su burdinaz hauts ditzatzula sudur eta kokotsa.