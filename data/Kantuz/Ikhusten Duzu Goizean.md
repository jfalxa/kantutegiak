---
id: ab-4042
izenburua: Ikhusten Duzu Goizean
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ikhusten duzu goizean
Argia hasten denean
Menditto baten gainean
Etche ttipitto aintzin churi bat,
Lau haitz ondoren erdian
Chakur churi bat athean
Ithurriño bat aldean:
Han bizi naiz bakean

Nahiz ez den gaztelua,
Maite dut nik sor-lekua,
Aiten aitek hautatua.
Etchetik kampo zait iduritzen
Nombeit naizela galdua;
Nola han bainaiz sorthua,
Han utziko dut mundua,
Galtzen ez badut zentzua.

Etchean ditut nereak,
Akilo, haitzur, goldeak;
Uztarri eta hedeak;
Jazko bihiez, ditut oraino
Zoko guziak betheak:
Nola iragan urtheak
Emaiten badu bertzeak,
Ez gaitu hilen goseak.

Landako hirur behiak,
Esnez hampatu ditiak,
Ahatche eta ergiak,
Bi idi handi kopeta zuri,
Bizkar beltz, adar handiak,
Zikiro bildots guriak,
Ahuntzak eta ardiak,
Nereak dire guziak.

Ez da munduan gizonik,
Erregerik ez printzerik,
Ni bezein urusa denik;
Badut andrea, badut semea,
Badut alaba ere nik;
Osasun ona batetik,
Ontasun aski bertzetik,
Zer behar dut gehiago nik?

Goizean hasiz lanean,
Arratsa heldu denean,
Nausia naiz mahainean;
Giristino bat ona dut hartu
Nik emaztea hartzean,
Ez du mehe egunean
Sarthuko uste gabean
Chingar hezurrik eltzean.

Piarres ene semea,
Nahiz oraino gaztea,
Da muthiko bat ernea;
Goizean goizik bazken erdira
Badarama arthaldea;
Segituz ene bidea,
Nola baitu egitea,
Ez du galduko etchea.

Ene alaba Kattalin,
Bere hameka urthekin,
Ongi doha amarekin;
Begiak ditu ama bezala,
Zeru zola bezin urdin;
Uste dut demborarekin,
Andre on bat dion egin.

Ez dugu behar lurrean,
Ongi bizirik etchean,
Utzi laguna gosean;
Ez du beharrak sekulan jotzen
Gure etcheko atean,
Non ez duen mahainean,
Otruntza ordu denean,
Lekhu bat gure aldean.

Ene andrea Maria
Ez da andre bat handia
Baina emazte garbia;
Irri batentzat badut etchean
Nik behar dudan guzia:
Gald.egiten dut grazia,
Dudan bezala hasia,
Akhabatzeko bizia.