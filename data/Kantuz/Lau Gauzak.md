---
id: ab-4104
izenburua: Lau Gauzak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gizon gazteak ezkontzeko behar ditik lau gauza:
Lehenlehenik iduria alegera bihotza,
Dirua frango sakelan eta bizitzeaz ez lotsa...

Horietarik hiru baitut,
laugarrena dut falta.
Bai iduria badut eta
alegera bihotza;
Dirurik ez dut, bainan ordainez
bizitzeaz ez lotsa.

Aldiz baditut hiru tatcha,
batto bainuke aski:
Arno-edale, jokolari
lanian ez ari nahi.
Horien gatik guzien gatik,
andrek maite naute ni.