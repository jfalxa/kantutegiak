---
id: ab-4024
izenburua: Haurrak Ikas-Azue
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Haurrak ikas-azue eskuaraz mintzatzen
Ongi pilotan eta oneski dantzatzen.
Haurrak ikas-azue eskuaraz mintzatzen
Ongi pilotan eta oneski dantzatzen.
Airetun chikitun airetun lairé
Airetun chikitun airetun lairé
Airetun chikitun airetun lairé
Airetun chikitun airetun lairé...Olé!

Gure kantu chaharrak konserba ditzagun;
Aire pollitagorik ez da sortu nehun...

Ez ahantz behin ere sort-herri ederra,
Haren mendiak eta itsaso bazterra...

Bihotz leiala ere atchik aitameri
Eta nunbait goait dagon gazte maiteari...