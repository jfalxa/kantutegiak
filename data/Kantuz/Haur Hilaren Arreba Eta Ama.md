---
id: ab-4060
izenburua: Haur Hilaren Arreba Eta Ama
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Ama, ene aneiak egun zer othe du?
Begitarte guzia baitzaio ilhundu
Sehaska huni buruz kantu eta kantu,
Irri baten aiduruz astia dut galdu

2
Ama, gure Battittak deus eztu erraiten;
Orai geldi-geldia hortchet da egoiten;
Hatsa urbildik ere ez diot entzuten...
Hila dela diote...nik ez dakit zer den!

3
Hau hila dugulakotz, Mariak ochtion
Ichilik eta prestu egon nadin zion.
Zer! Kantatu gaberik behar niz ba egon?
Hil direner kantua ez othe zaie on?

4
Ama, zer dut ikusten? Zu ere nigarrez?
Zorigaitza etcherat jin zaiku arabez?
Ez dakit nigar egin behar diotanez,
Zuk erradazu, ama, garbiki bai.al.ez.

5
- Ez haurra, ez dun behar nigarrik ichuri,
zorigaitzik etzaion gertatu nehori.
Ire aneia orai zeruan dun ari
Kantatzen errepikan ospe Jainkoari

6
sehaska hotz hunetan gorphutz huts bat baizik
ez dun orai gelditzen;
ez dun bertze fitsik...
Arima ilki zion Jaun goikoak goizik;
Aingeruen gorthean hartu din jagoitik.

7
Maddalen, ikusi dun joan den egunean
Pinpirin-arroltze bat zur horren gainean.
Kuchkua hor utzirik apecha beha zan,
Hegaldaka johaki ederrik airean.

8
Hala hala Battittak gorputza utzi din:
Gero joan dun zerurat eder eta arin;
Irriz hegaldatu dun eta lehiarekin.
Handik, gutaz gaichoa oi! Urrikal dadin.

9
- Ama, nahi nintzake apecha bezala;
agurka lorez-lore polliki dabila.
Zeruko lilietan ibiltzekotz hola,
Biak Battittarekin Jaunak deit gitzala.