---
id: ab-4045
izenburua: Artzaintsa Mendian
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Zelhaietarik ikhusten duzu
gure etchea mendian
Eta gorago etchola hura
harroka baten aldean
Mendi kaskoan, andre gazte bat
arthaldearen erdian
Ni nago hemen ardien zaintzen
nere kilua gerrian
Egon ditezen ene aitamak
bakotcha bere lanean.

2
Goizetan utziz egun guziko
Aitamen begitartea,
Argi hastean idekitzen dut
Etchola haren atea,
Alharazteko mendi gainean
Ene artalde maitea,
Han iraganik, bake gozoan,
Argitik ilhun artea,
Bozik daramat etcherakoan
Kaiku esnez betea.

3
Herriko ene gazte lagunak
nitaz dira urrikaltzen,
heien jostetas zoroetalat
ez naizelakotz hurbiltzen...
bainan gaichoek erran bezate
zer zorion dutan galtzen!
Bakea eta librotasuna
Osoki ditut gozatzen,
Eta menditik ez ote dut nik
Gauza ederrik ikusten?

4
Argi-hastean histu orduko
artizarraren argia,
nik ikusten dut iguzkiaren
agertze miragarria,
bainan oraino ilhun dagozi
zelhaiak eta herria;
Ohean dago ezin atzarriz
Hiritar gazte nagia;
Nola ezagut dezake handik
Hemengo neure loria?

5
Eliza dorre goren hura dut
Oroz gainetik ikusten,
Eta ezkilak errepikatuz
Herria du inharrosten,
Bi aldetako mendi auzoek
Oiharzunka ihardesten,
Haur demboratik huna baititut
Toki berean aditzen
Zorion hunen iraupena dut
Jainko Jaunari galdatzen.

6
Ordu berean hedatzen ditut
Itsas alderat begiak
Eta ikusten arraintzariek
Dituzten untzi ttipiak;
Iduri dute pentze gainetan
Dabiltzan altcha liliak,
Halaber dira ur achalean
Hekien bela churiak...
Othoitzez nago suntsi ez ditzan
Itsaso lotsagarriak!

7
Ez badut ere aditzen ahal
hiriko kantu aderrik,
entzuten ditut bas-ihiziak
alderdi guzietarik:
kukuak kuku pago gainetik,
epherrak kantu beretik,
chori ttipiek arboletarik,
chochoek sasi barnetik,
heien titchoak etortzen zaizkit
bihotzera bihotzetik.

8
Haitz eder baten azpira noa
Otruntzaren egiteko,
Uda liliez beztitua den
Sorhopila dut jartzeko;
Ardi bildotsak eta churiko
Aldean ditut lkuko,
Ama maiteak emanik badut
Behar dutana jateko;
Erasiaka dohan chirripan
Ur garbia edateko.

9
Arratsaldean begira nago
choratua kampoeri,
nola egin den dembora gutiz
zeru berri bat iduri:
pentze, mahasti, baratze, herri,
etchalde eta jauregi,
zenbait lapurtar nun izan diren
hor dira obrak ageri,
heia dutenetz amerikatik
urhea nasai ekarri!

10
Bertsu haukien aditzaleak
nahi luke orai jakin
artzaintsa hori bere mendian
egon behar den mutchurdin.
Bai egonen naiz, ez ezkontzekotz
Hitzeman dautanarekin,
Bainan maitea amerikatik
Jinen baita zerbaitekin,
Bi menditarrek nahiago dugu
Hitzari ohore egin.