---
id: ab-4140
izenburua: Churiko
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mehetegiko chakhurra zangoz,
errainez makhurra
Berga bat luze muthurra,
Larrutik urbil hezurra,
Urthe guzia barura,
Zeren den toki chuhurra.

Amak ttikitik utzia,
Esnerik gabe hazia,
Tripa errainer josia,
Uliek jaten hacia;
Churikok bere bizia
Hola derama guzia.

Mahanpetik kokoriko,
Begira dago Churiko
Artho achaltto bat noizko
Othe zaion eroriko.
Nausiak ematen daizko
Doan hortik bi ostiko!

Chakhur gaichoa holakoz
Merke ase zutelakotz,
Tripa hutsik, ilhun gogoz,
Pleinuz zagon egun oroz;
Hatzka zangoz eta besoz
Nihun ez zauken kukusoz.

Ohoinak ere noiz-nahi
Horren sainga ilhaunari
Gabaz ziren irriz ari:
"Chakhur prestua, hi haiz hi!"
Gero ichiltzea sari,
Jo harrika gaichoari.

Egun batez baratzera
Huiatzen dute lanera,
Doan oilo haizatzera...
Chakhur gose-ta-minbera,
Buztana tripaz behera,
Ihes doa gordetzera.

Martin etcheko muthila,
Zalu harturik makhila,
Haren ondotik dabila.
Churiko chakhur habila
Lastoan ichil ichila
Dago alegia hila.

Zenbeit egun gau bakharrez
Gorde egonik herabez
Athera zen jan beharrez,
Hamikatua, nigarrez,
Estalia zaragarrez
Lakhastaz eta zakharrez.

Sukaldean nihor ez da;
Sartzen da chackur herresta.
Errekia zen prest presta,
Zeren zen herriko besta.
Usain, milik'eta jasta,
Dena jaten du has eta.

Etcheko andre khechua
Hor heldu da hats-hantua:
"Zakhur madarikatua,
"Eia, hor usu usua,
"Nik hauts artean burua,
"Egik hire ordenua..

Ordenuak hau du beltza,
Denen uztea bertzentzat;
Churikok badaki ontsa
Nori zer utz, nola phentsa;
Etchekandreari gantza;
Nausiari azken putza.

Bizian gosez egonik,
Hiltzean sobera janik,
Huna Churikok erranik
Zer entzun dugun azkenik:
"Gose dagonaren ganik
"Ez daitekela lan onik".

(ZALDUBY.