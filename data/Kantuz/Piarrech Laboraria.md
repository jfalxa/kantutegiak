---
id: ab-4044
izenburua: Piarrech Laboraria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Eskualherriko jaun gazte batek
laguntza bat daut galdatu,
Edozoin moldez nun bait
hirian nahi delakotz plazatu,
Etchean omen akhiturikan
aski duela pairatu.
Arapostutzat bertsuño batzu
horri baitazkot moldatu,
Bertze zonbaiti, oren berean,
nahi nintuzke kantatu.

Udaberriak eder heldu tuk
Eskualdun herrietara:
Oihan-mendiak, zelhai-bazterrak
Zoin choragarri bichtara:
Liliak irriz, choriak kantuz,
Esker bihurtzen zerura!
Beraz, Piarrech laboraria,
Zer zaik hiri jin burura?
Eskualdun haurra, ez zaika jausten
Nigarra begietara?

Dudarik gabe noizbait entzun duk
Hiritar lagun gaichtoa.
Irri aditu faltsu batekin
Erran dauk: Gaicho tontoa,
Zertako bada lurrari buruz
Ar izerditan ithoa?
Hortik aberats sinestekotzat
Behar dik izan zozoa.
Haugi gurekin, hirian dukek
Hiretzat bizi gozoa.

Urgulu zoroz ez ahal dituk
Hire aitamak utziko?
Zahartuz geroz, hi gabe, haurra,
Nola dituk ba biziko?
Ihesi joanez hire aitari
Bihotza diok hautsiko.
Hik hilarazi burhaso onak
Auzo batek ehortziko:
Zer bihotz mina seme batentzat
Eternitate guziko.

Ez othoi sinhets hirian choilki
Direla gauza ederrak:
Ez duk eus ere ederragorik
Nola sort-herri-bazterrak,
Beren usain on gozoekilan
Lorez estali zelhaiak...
Beha zak bada zer lezakeien
Hiritarchkilen indarrak,
Langile faltan alfer eta lo
Gelditzen balire lurra?

Iguzki-suak ontu jus hura
Mahats pikor molkoetan
Behar diagu ardura edan
Odolik izaitekotan.
Berdin udetan, urhe distiran
Den ogia zelhaietan
Hik guretako bildu behar duk
Cheha dezaten errotan...
Mahats-ogirik ez duk ez sortzen
Hiriko jauregietan.

Urthe bakhar bat geldi baledi
Laboraria lanetik,
Asunez kanpo, lapharrez kanpo
Zer bil litake lurretik!
Orro handiak entzun litazkek
Hiri-murruen barnetik
Erhoturikan, hilak iduri,
Heldu hobien zolatik:
Batak bertzea jan lezakeie
Jendek gosearen gatik.

Erainzkik, beraz, laboraria,
Hire lurrak artharekin:
Alhorretako lanak ez derauk
Bihotzean emanen min.
Bai, chutik ago, zeruari so,
Besoz azkar, gogoz arin:
Emak lanari, emak airoski:
Beldur ez har phildak zikhin.
Jaun-andrechkilen janhari hauta
Duk ongarritik behar jin.

Eskual-herrian uda berria
Bethi jinen duk ederra:
Burzoro-aldi itsu batean
Hirirat othoi ez lerra...
Soizak mendiko belharretarik
Nola bizi den epherra...
Soizak phentzeko loren gainean
Erlea dagon alferra...
Othoi, Piarrech, ez utz ariman
Sartzerat lurraren herra.

Azken oihua, zeruko Jauna,
Zuri derautzut egiten,
Amentzat haurren amodioa
Zuk duzulakotz emaiten.
Eskualdun seme lur langileak
Bethi chuchen ibil diten;
Bai, beren ama, zuk eman lurra
Bihotzez maitha dezaten,
Hola eginez ez ahanzteko
Zeru hortarat igaiten.