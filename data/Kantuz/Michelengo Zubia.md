---
id: ab-4139
izenburua: Michelengo Zubia
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Michelengo zubia, zubi famatua,
Arraintzariendako toki hautatua;
Haize-hegoak han du azkarra chichtua,
Niri laister egin daut chapelaz tratua,
Chapelaz tratua.

Hameka orenetan, oraino barurik,
Zubi artan arraintzan ari nintzelarik,
Arrain bat naukalarik amuan lothurik,
Haizeak ereman daut chapela burutik,
Chapela burutik.

"Hasarre heldu hintzen, hozturik mendian
Niri mendekatu haiz; hobenik ez nian...
Zer huen hartzekorik ene chapelian,
Gisa hortan sartzeko hartuta urian?

"Aphezari burutik chapela kentzia
Barkhatu erran gabe, hoi duk ausartzia!
Hegoa erho zela nian aditzia...
Noiz ikhasi-gogo duk zer den zuhurtzia?

"Hastiagarria haiz, egunaz bai gauaz,
Oro erhokeria; ez dakik zer nahas...
Ez duka urrikirik egin dukan lanaz,
Gisa huntan emanik apheza buru-has?

"Nungo Satan gorriak igorria hintzen?
Ez haut ikusi ere chapelaren hartzen!
Hartu dukan lekhurat ez baduk bihurtzen,
Jakinen duk enekin zertarako haizen.

"Haizea, izanen duk justiziaz gerla,
Segur duk preso ere harturen hutela;
Baionan galtzen badut, Pau-erat apela,
Ez badautak bihurtzen ebatsi chapela.

"Aphezen chapelari aski aiher denik
Baduk bai mundu huntan haizeaz bertzerik
Khentzen ahal baluzte hik bezain ichilik,
Ez ginezakek ibil buruan chapelik,
Buruan chapelik!.

(P.DIBARRART)
(Airea: Dendaria eta laboraria n. 22.