---
id: ab-4027
izenburua: Gitarra Joilea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gitarra tchartcho bat det nik nere laguna
Horrela ibiltzen da artist Eskualduna
Egun batian pobre bestietan jauna;
Kantatzen pasatzen det nik bethi eguna.

Nahiz den Italia, orobat Frantzia,
Bietan billatu det mila malizia;
Korritzen badet ere nik mundu guzia,
Maitaturen det bethi Eskualdun herria.

Jaunak emaiten badet neri osasuna,
Orañik izango det andregai bat ona;
Nahi badet Frantsesa interesa duna,
Bainan nik naiago det hutsik Eskualduna.

Adio Eskual herria, bañan ez bethiko;
Bospasei urthe huntan ez det ikhusiko !
Jaunari eskatzen diot grazia emaiteko
Lur maite gocho huntan bizia uzteko.