---
id: ab-4067
izenburua: Hasten Naiz Orai Kantatzen
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Hasten naiz orai kantatzen
Ene penen deklaratzen
Izar bat charmegarria
Aspaldi dut adoratzen;
Haren ganik urruntzean,
tralera tralera, tralera!
Haren ganik urruntzean,
Bihotza zaut erdiratzen.

2
Adios beraz, maitea,
ene penen ithurria,
zu ganik urruntzea
iduritzen zaut hiltzea!
Behin bethikotz jinen naiz,
Tralera, tralera, tralera!
Behin bethikotz jinen naiz
Galtzen ez badut bizia!

3
- Zer derautazu erraiten,
orai nauzula kitatzen?
Egunak zaizkit urtheak,
Non ez zaitudan ikusten;
Zu gan eta ariko naiz
Tralera, tralera, tralera!
Zu gan eta ariko naiz,
Pena changrinez hiratzen!

4
Zazpi urthe ja baditu
zinintudala maitatu!
Hamabortz urthe nintuen,
Zuk, hamazazpi komplitu!
Geroztik amodioak
Tralera; tralera, tralera!
Geroztik amodioak
Zure esklabo egin nu!

5
Urthe erraztuna dut nik
zure eskutik izanik:
Hura nigarstatu gabe
Ez dut pasatzen egunik!
Hortarikan ikus zazu
Tralera, tralera, tralera!
Hortarikan ikus zazu
Zombat maite zaitudan nik!

6
Nik balimbanu ahalik,
airez gateko hegalik,
zure ganat gan nindaite,
itsasoaren gainetik!
Zu ikusi arteraino
Tralera, tralera, tralera!
Zu ikusi arteraino,
Nehun ez nuke pausurik!

7
Itsasoaren gainean
oren memento guzian,
zur.othoitzez behar naite
elementaren azpian,
makurrik gabe heltzeko,
tralera, tralera, tralera!
Makurrik gabe heltzeko
Salbaturikan portuan.