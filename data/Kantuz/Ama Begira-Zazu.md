---
id: ab-4103
izenburua: Ama Begira-Zazu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ama, begira zazu leihotik plazara:
Ni bezalakorikan plazan ba othe da?
Lai, lai, lai, tra la ra la ra lai. (bis)

Begiak erne ditut, bi zangoak arin:
Hoinbertze dohainekin ez nauke mutchurdin.
Lai, lai, lai, tra la ra la ra lai (bis)

Muthikoen artean itzul-inguruka,
Gizon geitzat baduzket dotzenan hameka:
Lai, lai, lai, tra la ra la ra lai (bis)

Heldu den primaderan arropa churitan
Ederra izanen naiz mahain-sainduetan,
Lai, lai, lai, tra la ra la ra lai (bis)

- Haurra, ahal badaite, hala izan bedi:
Lehen gure aldi zen, orai zuen aldi.
Lai, lai, lai, tra la ra la ra lai (bis.