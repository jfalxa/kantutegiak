---
id: ab-4128
izenburua: Bidarraitarra
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mila zortzi ehun hemeretzian,
Urriaren hilaren bederatzian,
Omore ona nuien hauien kantatzian;
Gazt'eta alegera trankil bihotzian,
Ontasuna frango badut intresian,
Deusik ez etchian:
Orai bezain aberats nintzen sortzian.

Nor nahik zer nahi erranikan ere,
Bidarraitarra nuzu, bai nahi ere;
Etcheko seme ona, segurki halere;
Nahiz baden herrian hobechagorik ere;
Baitut bortz haurride, enekin sei dire,
Oro adichkide,
Dotiaren gainetik samurtzen ez gire.

Aita ezkondu zen gure amarekin,
Ama aitarekin, biak elgarrekin;
Orduian gazte ziren, bertze zerbaitekin,
Eta orai zahartu miseriarekin:
Ontasuna igorri bertze haurridekin,
Parte bat enekin:
Enekin baino haboro bertze bortzekin.

Aita zen etcheko, ama kampoko:
Jainkoak egin.tu elgarrekilako.
Hirur muthiko eta hirur neskatoko,
Haurrik aski badute aisa bizitzeko.
Balimba bertzerik ez zaie sorthuko,
Ez da fidatzeko;
Arraza nasaietik baitire, horra zertako.

Adinen beha gaude egia erraiteko,
Gure ontasun ororen partitzeko.
Diru idorra ere bada bizikichko,
Jainkoak daki zembat den bakhotcharen dako;
Izaiten badugu ez dugu utziko
Auzokuen dako;
Bainan ez daikegu sakelarik erreko.

Hemen bagirade orai zembait lagun,
Botoila bana arno edan dezagun.
Lehenik ona denez jasta dezagun,
Ona balin bada bira edan dezagun,
Trinka dezagun, plazer har dezagun,
Bilha zembait lagun.
Diru dianak izanen dik ezagun.

Etcheko anderia, zure trichtia,
Iruditzen zaitzu girela diru gabia.
Dirua badugu, bainan dugu larria;
Zor utziren dautzugu gaurko afaria.
Etcheko anderia, emazu guardia,
Har pazientzia.
Noizbeit izanen duzu pagamendia.

(J. OXALDE.