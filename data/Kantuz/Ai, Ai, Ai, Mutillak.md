---
id: ab-4135
izenburua: Ai, Ai, Ai, Mutillak
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Azpeitiko neskatchak, gona gorriekin (bis)
Ez dute dantzatu nai chapel churiekin,
Ai! ai! ai! mutillak, chapel churiekin (bis)

Biba chapel gorriak, burlia ferdiak! (bis)
Zaldi batian dathor Don Carlos gure,
Don Carlos maitea, gure erregea (bis)

Biba don Carlos eta Doña Margarita (bis)
Biba relijionea.ta Fuera Republika!
Fuera Republika! Biba Margarita! (bis)

Gobernuak baditu bi pezetakoak,
Bai eta Don Carlosek bolontarioak,
Bolontarioak, ez jornaleroak.

Nere borondateaz hartua dit arma...
Nigarretan utzirik aita eta ama...
Aita eta ama, hartua dit arma...

Gu lapurrak gerala kalian diote;
Infame traidoreak, gezurra diote
Gezurra diote.ta pagaturen dute...

Ez gerare ohoinak, ez pezeteroak,
Fedearen aldeko bolontarioak,
Bolontarioak, ez pezeteroak.

Ez gerare armetan, ohointzeendako,
Bainan bai bakarrikan Don Carlosendako,
Haren koroazteko, gortean zartzeko!

Don Carlosek eman du Frantzitik ordena
Champonian saltzeko beltzetan onena,
Beltzetan onena, Frantzitik ordena.

Champonian ezikan, arditian ere,
Estellako kampotan dozenaka daude!
Dozenaka daude arditian ere.