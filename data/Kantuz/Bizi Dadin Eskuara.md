---
id: ab-4028
izenburua: Bizi Dadin Eskuara
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Haurretik ibilirik mundu zabalean
itsasoan marinel tratulant lurrean
Mintzaira mota asko badut ikasirik,
Eskuarak nihun ere bat ez du parerik.

Ardia alhatzen da bazka gizenetan,
han kausitu ondoan mendi hegaletan.
Eskuarak ere badu eremu zabala.
Han, han berma ditake jakinen ahala !

Nun da Eskuara bezen mintzaira berorik ?
Munduan batek ez du khar handiagorik !
Maitea maiteari mintzo bada gabaz,
Leihorat itzulia mintzo da... eskuaraz !

Haurra bere amari nola mintzo zaio ?
. Ama, ama ., eskuaraz sehaskatik dio.
Amari lakhet zaio horren entzutea,
Gure Eskuara hain da mintzaira maitea!

Mintzairetan Eskuara omen da lehena:
Dena deen, Eskuara da segur ederrena.
Othoi atchik dezagun eder bezen garbi,
Eskuaraz mintzatuz noiz bai eta non-nahi !

Herritik urruntzean, Eskualdun gaztea,
Ez duzu ahantziko Eskuara maitea.
Ondikotz zenbat ez da ahanzten duenik !
Merezi othe dute Eskualdun izenik ?

Eskuara hain da eder, hain da maithagarri,
Non behar baitiogu laguntza ekharri.
Bizi dadin maitea orai eta gero,
Berma gaiten hortarat Eskualdunak oro.