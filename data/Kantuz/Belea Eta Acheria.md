---
id: ab-4118
izenburua: Belea Eta Acheria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Belea haragiki puska bat mokoan
Haren jateko zagon haitz baten kaskoan.
Acheria ihizin, han gain di baitzohan.
Usainka eta beha trikatzen zaiohan.

Egun on, Musde Bele;
"zer jaun ederra zu!
"Zure luma dirdiraz
"liluratzen nauzu.
"Kantari eder baten
"fama ere duzu
"Nahi zintuzket entzun:
"Othoi kanta zazu..

Belea lorietan
hasten da kantatzen,
Zehe bat duelarik
mokoa zabaltzen:
Bere zitzia zaio
lurrerat erortzen.
Acheria hain chuchen
Altchatzeko han zen.

Acheriak: Milesker
dio beleari:
"Ez gaitela, to, fida
"lausenkariari,
"Eta ez ere gure
"eder usteari.
"Jaki hau daiat hartzen
"hitz on horren sari..

(ZALDUBI.