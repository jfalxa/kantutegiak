---
id: ab-4091
izenburua: Adios Ene Maitea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Adios, ene maitia, adios sekulako!
Nik eztit beste phenarik,maitia zuretako,
Zeren uzten zutudan libro bestendako.

Zertako erraiten duzu adio sekulako?
Uste duzia eztudala amodio zuretako?
Zuk nahi balin banaizu enukezu bestendako.