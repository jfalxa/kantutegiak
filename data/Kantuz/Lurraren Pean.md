---
id: ab-4086
izenburua: Lurraren Pean
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Lurraren pean sar nindaiteke,
maitia, zure ahalgez;
Bost phentsamendu eginik nago
zurekin egon beharrez:
Bortha barnetik zerratu eta
gamberan bethi nigarrez...
Phentsamenduak airean eta
Bihotzetikan dolorez!
Ene changrinez hilarazteko
sorthua zinen arabez.

Oren onean sorthua zira
Izar ororen izarra,
Zure parerik ez zait agertzen
Ene begien bichtara,
Espos laguntzat nahi zintudan
Erran neraitzun bezala:
Ordean zuri iduritzen
Zuretzat aski nintzala,
Ni baino lagun hobe batekin
Jainkoak gertha zaitzala!

Mariñelak joaiten dire
Itsasorat untziko.
Zuretzat dudan amodioa
Ez dut sekulan utziko.
Charmegarria nahiz ez giren
Elgarrekilan biziko,
Behin maithatu zaitut eta
Ez zaitut hastiatuko:
Ene gogoan sarthua zira
Eternitate guziko.

Primaderan zoinen den eder
Choria khantuz kampoan.
Amodioak bainerabilka,
Maitea, zure ondoan:
Charmegarria, ez zaitut nahi
Bortchatu amodioan.
Ni changrin huntan hilikan ere
Satisfa zaite gogoan:
Bai maluruski aski banuzu
Bakharrik nihaur munduan.

(Airea : Piarrech laboraria n.23.