---
id: ab-4124
izenburua: Bildotsa Eta Otsoa
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Primadera goiz batez,
chirripa batera
Ama gabe bildotcha
gan zen edatera.
Hantchet ari zelarik
churga churga bera,
Oihanetik otsoa
zitzaion atera.

Otsoak gorachago
urean sarturik:
"Ur hau, erraten dio
"zikhintzen dautak hik!"
Bildotchak errepusta:
- "Jauna, nik segurik
"Beheretik ez diot
"zikhin zure urik..

- "Kichkila, bazakiat,
"jaz hi ari hintzen
"Ene fama onaren
"denetan biphiltzen".
- "Jaz ni oraino, Jauna,
"sortzekoa nintzen,
"Amaren esnez orai
"ari naiz handitzen.".

- "Anaia duk hobendun
"hi ez balin bahaiz."
- "Ez dut nik anairik,
"ume bakharra naiz."
- "Hots zuetarik bat duk
"entzun dudana maiz,
"Niri nahi zautala
"zer nahi zorigaitz..

Hortan zen bildotchari
oldartu otsoa...
Eta bi klaskaz zuen
iretsi gaichoa.
- Azkarrak duenean
partida flakoa.
Hauzia zoinena den
erran gabe doha.

Otsoak bildotchari
egin zioena,
Jainko legetik kampo,
nork orai kondena?
Gizon leger ez galde
noren den zuzena:
Galde zazu indarrez
zoin den nausi dena.

(ZALDUBI.