---
id: ab-4113
izenburua: Mendekoste Phestetan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Mendekoste phestetan,
Aurthen Arneik errekan,
Ichtorio bat gerthatu da kasik ezpeitiot erran:
Bost emaztek edan dituzte hamalaur pint'arno betan
jokhaturik kartetan.

Jokua zuten florian
Arneiko Madrilenian;
Atso gaichoek uste zuten zirela zeruko lorian,
Saindu ororen erdian, bere botoilak aldian,
Bai eta plazer handian.

Batak zion bertziari:
<Arnoño hunek alegrantzen ditan bihotzñua niri,
Ene plazerra hola luken banu zembait ogi pochi
Arno ezti huneki.>>

Katrina eta Kattalin,
Auzo biak elgarrekin,
Arno edaten ari ziren kuraia handi batekin;
Gero ondoan bazuten gibel aldian zer egin,
Bere fanfarreriekin.

Hirugarrenak ederki
Tanteatzen omen daki;
Hamarrekuak harek zituen omen markatzen orori,
Ondarrekotz nahasi zen eta lurrerat erori;
Orduan etzen egarri.

Laurgarren hori zer pheza,
Maria zapatainesa;
Untsa esplikatzen zituen eskuara eta frantsesa,
Bai eta ere aisa egiten bide handian esa;
Horiche da haren letra.

Bostgarren hori zoin othe da?
Phentsatzeko ez da phena;
Garachane gaizo horrek nola ezpaituke lehena;
<Arno ekhartzen duena!>>

Ahuntzez jokhatu eta
Hasi ziren kolpeka;
Gaston deitzen zen guarda gazte bat, kampotik jinik lasterka,
Hak phartitu izan zituen, ez uste bezain aisa,
Arrazoinik ezin pasa.

Zuk ere, Etchek'anderia,
Erran beharzu egia;
Solas hortan zu othe zinen horien buruzagia?
Bost emaztek egiteko horiche da komedia!
Hurren baitzen zahagia.