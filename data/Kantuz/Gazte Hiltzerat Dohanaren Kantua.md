---
id: ab-4057
izenburua: Gazte Hiltzerat Dohanaren Kantua
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Hogoigarren urthera hari naiz hurbiltzen,
Eta nere oinetan tomba dut ikusten,
Ni zertako mundura ethorria nintzen?
Ondikoz! Ez sortzea neretzat hobe zen.

2
Sekulan zoriona zer den jakin gabe,
Hainitz dut nik lurrean izan atsegabe.
Lore sorthu orduko histu baten pare,
Biaiaren uztera guti naiz herabe.

3
Ez-ez enau izitzen ez ni herioak,
Laster khen dezadala biziz Jainkoak!
Sehaskan galdu ditut nere burasoak;
Zer diren ez dut jakin etcheko gozoak.

4
Aita ama maiteak, zertako mundutik
Zerura goan zineten ni hemen utzirik?
Ez dut nik mundu huntan izan sosegurik,
Ez baitut ezagutu amaren musurik.

5
hamazazpi urthetan, zorigaitz dorphea!
Izar bat maitatu dut parerik gabea;
Aingeru bat zaiteken, zeruko umea;
Nere bihotz eritik juan darot bakea.

6
Nahiz aingeru ona, ez duzun sekulan
Munduan jakin zembat nik maite zintudan,
Urus zarenaz geroz jaunaren oinetan
Ez nezazula ahantz zure othoitzetan.

7
Anaia bat banuen haurra nintzelarik;
Soldado juan zen eta nik ez dut berririk:
Aita amekin dela ez daite dudarik,
Niri begira daude hirurak zerutik.

8
Ahituz banarama nere oinhazeak,
Burhasoen alderat deitzen nau zortheak.
Nork ditu arthatuko, ait. ama maiteak,
Zuen tomba gainean nik eman loreak?

9
Nere gorphutz flakoa lurrerat orduko,
Nor othe da munduan nitaz orhoituko?
Nere ehortz-lekhura nor da hurbilduko?
Lore berririk ez da neretzat sortuko...

10
Mendian ur chirripa, harrien artean,
Auhen eta nigarrez jausten da bidean,
Izan nahi bailuke zelhai ederrean...
Hala nere egunak badoaz lurrean.