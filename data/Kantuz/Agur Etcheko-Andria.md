---
id: ab-4111
izenburua: Agur Etcheko-Andria
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Agur etcheko andria,
Gure diruen jabia,
Berriz ere behar dugu arnoarekin ogia,
Bai eta're, arratsian, Kartekilan argia.

Kartak hiru'ta kartak lau,
Kartak eman deraut hau:
Hamahiru izan eta ezin galdu hamalau,
Sakelan tudan sosekin afalduren ez naiz gaur!

"Utzak jokua burutik,
Partitu gabe mundutik!"
Aditzia banian, bainan jokuak adarrak makur tik,
Hik nekez irabaziak ostalerak juanen tik!

Lau arrosa, lau begi
Bortz arrosa'ta bortz begi
Gizon debocharen haurrak maiz gose edo egarri,
Kampora jali ondoan haize hotza janari!!.