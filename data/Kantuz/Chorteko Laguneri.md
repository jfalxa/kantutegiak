---
id: ab-4040
izenburua: Chorteko Laguneri
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Nahi derauzkitzuet gaur bi pertsu egin
Zuekin izaitea bai tut nik atsegin
Mahatseko esneak suzko ala sorgin
Pizten deraut zintzurra eztitasunekin
Huna nere chortea chehetasunekin.

Karrosan firurika joan ginen herritik,
Oihuz, irrintzika, kantatuz goizetik...
Lili, chingol, bandera edo zoin aldetik,
Pettirren chirula boz bere chokotik,
Mundua harritua zagoen urrundik.

Sortzean bezalache chortean agertu:
Buluz gorri bainintzen han naute ikertu.
Aitzindaria etzen lan hortan izertu;
Dudamudarik gabe ninduen on hartu,
Mainku, oker naizela ezpaitzen ohartu.

Hamar izana gatik, bai! Hamar erronkan!
Ez nuen mintzatzeko segur beldurrikan:
Mainku naiz eta oker... abantzu errekan
Ihardetsi baitzuen : Ale fiche lekan :
Joan nintzen eta hasi kantuz errepikan.

Ez naute alferretan hartu harmetako.
Hartua banaiz huna, nik uste zertako:
Nere okerkeria zait baliatuko,
Bereziki zizpetan, chut begiztatzeko...
Begiaren hesteko lanik ez beharko.

Egia erraiteko, zangotto ezkerra
Ibiltzeko dadukat pochi bat uzkurra.
Urratsa balinbadut sobera makurra,
Zaldiz emanen naute. Zer zaldun ederra!
Kapitain eder, bainan kapitain okerra.

Gero zer gerta ere, ni orai egarri!
Zuen osagarrira nahi dut edari...
Edan eta, kantatuz jarraik bideari,
Etcherat itzultzeko sudurttoa gorri,
Bainan buru-barnea... ez sobera eri...