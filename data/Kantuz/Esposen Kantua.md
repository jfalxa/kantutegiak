---
id: ab-4105
izenburua: Esposen Kantua
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Zonbait pertsu banuzke egun kantatzeko
Hemengo besta hunen ontsa laudatzeko;
Horra bi bihotz gazte elgarrekilako
Ene hitzak dituzte biek altchatuko
ho, ho, ho, ho, ho, ho... gogoz ikhasteko.

Agur, espos gaztea, oros maitatia;
Agur, andere gazte, bihotzez eztia;
Egun ukan duzue atsegin handia,
Bizi guzikotz biez baten egitea,
Ha, ha!... Aspaldi nahia!

Huna eginbideak ezkonduendako:
Lehen-lehena daite hen bien arteko;
Bigarrena zaharren ontsa maitatzeko;
Hirugarrena berriz haurren altchatzeko,
Ho, ho!...saila segitzeko.

Kobla huntan mintzo niz no espos jaunari:
Kasu egin diezon bere lagunari:
Sanorik heldu zaio lili eder hori,
Negua gabe nahiz fruitua ekharri
Hi, hi...aitamen lokharri...

Jostetak utzi-eta serioski lanari,
Bethi azkar egonez edozoin paneri.
Amodio-karesa hartu lagunari,
Baina ez utz sekulan hura buruzagi,
Hi, hi, hi...denen irringarri.

Andere espos gazte iduri lilia,
Erhaztunak derautzu obedientzia;
Elhekak utzi eta altcha familia.
Arropa horren pare ereman bizia,
Ha, ha... ohora etchea.

Ikus dezagula guk urtheren ondoan
Aitaso-amasoak supazter chokoan,
Gochoki atchikitzen haur bana altzoan,
Aitamak aldiz bertze biekin kampoan,
Ha, ha...lur lantzen gostuan.

Pertsu hauk eman ditut hemen konpainian,
Espos gazte hauientzat denen izenean;
Jaunak entzun gaitzala oraiko nahian
Bai eta berekin har, azken orenean,
Ha, ha...eternitatea.