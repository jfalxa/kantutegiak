---
id: ab-4102
izenburua: Ezkont-Gaiten
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Inchauspeko alaba dendaria
Eskont-minez abantchu eria.
Zahar bati egin zion galdea,
Gaizki zenetz espos bat hartzea.

Chaharñoa pertsulari hauta zen,
Ezin mintza koblakatuz baizen:
Hiru kobla hor diozka moldatzen:
Bildu ditut, ahantz ez ditezen.

Huntz-ostoa goiti doa pherderik;
Herbailtzen da ez badu zurkhaitzik;
Izaitekotz zainetan kalipurik
Bego hura haitzari lothurik.

Lanhoz dagon egunaren trichtea,
Iguzkia delarik gordea!
Berdin trichte, gau ilhargi-gabea!
Izar gabe deus ez da zerua...

Ez, mertchika ez da pollit neguan,
Makhildua agertzen denean.
Primaderan, zoin eder lore-pean!
Joriago, fruituz betherikan!

Hortan zuen chaharrak bururatu,
Eta nechkak ontsa komprenitu:
Zurkhaitz, izar, lore bai eta fruitu,
Ezkontzeak emaiten baititu.

Ezkontzari aihertzen zaizkionak
Higuin ditu zeruko Jaun onak:
Ezkont gaiten beldurrik gabe denak,
Donadoak eta mutchurdinak.

(Airea: Inchauspeko alaba n.63.