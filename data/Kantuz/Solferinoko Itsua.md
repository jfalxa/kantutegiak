---
id: ab-4039
izenburua: Solferinoko Itsua
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Harmen hartzera deitu ninduen
gazterik zorte etsaiak
Urrundu nintzen, herrialderat
Itzuliz usu begiak
Iotzuliz usu begiak

Zorigaitzean baitut ikusi
Solferino-ko hegia!
Alferrikan dut geroztik deitzen
Iguzkiaren argia,
Iguzkiaren argia.

Nihoiz enetzat ez da jeikiren
Goizeko argi ederra,
Zeru-gainetik nihoiz enetzat
dizdiraturen izarra,
Dizdiraturen izarra.

Betiko gaua, gau lazgarria!
Begietara zait jautsi:
Ene herria, ene lagunak
Nihoiz ez behar ikusi,
Nihoiz ez behar ikusi.

Ene amaren begi samurrak
Bethiko zaizkit estali,
Maiteñoaren begitarteak
Behin betiko itzali,
Behin betiko itzali.

Larrainetako haritz, gaztaina,
Mendietako ithurri:
Horiek oro enetzat dire
Amets histu bat irudi,
Amets histu bat irudi.

Ene herrian, gazte lagunak
Kantuz plazar dohazi,
Eta ni beltzik, etche zokoan...
Irri egiten ahantzi,
Irri egiten ahantzi ¡

Oraino gazte, gogoz ez hoztu,
Eta biziak lotsatzen,
Dohakabea! Er eginen dut
Jaunak ez bana laguntzen,
Jaunak ez banu laguntzen.

Ai, aski hola! Jainkoa, barkha!
Begira zure haurrari,
Kontsolamendu zerbeit emozu:
Noizbait duzun urrikari,
Noizbait duzun urrikari.