---
id: ab-4077
izenburua: Ithurrira Goizean
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Goizean goiz jeikirik, argia gaberik,
Urera joan ninduzun, pegarra harturik.
Tra lera, la la la...
Urera joan ninduzun, pegarra harturik.

Jaun chapeldun gazte bat jin zautan ondotik:
"Heia nahi nuenez urera lagunik?"
Tra lera, la la la

- Nik:" Ez nuela nahi urera lagunik,
Aita beha zagola salako leihotik."
Tra lera, la la la

Aita beha zagola, ezetz-erran gatik,
Pegarra joan zerautan, besotik harturik.
Tra lera, la la la

Urera ginenian, ez ginen egartsu
Galdegin zautan ere: " Zombat urthe tutzu?"
Tra lera, la la la

"Hamasei...Hamazazpi orain ez komplitu:
Zurekin ezkontzeko gazteegi nuzu."
Tra lera, la la la

Etcherat itzultzeko, nik dutan beldurra!
Ez jakin nola pentsa amari gezurra!
Tra lera, la la la

- Arreba, nahi duzu nik erakuts zuri,
Etcherat ethortzean zer erran amari?...
Tra lera, la la la

"Urcho churi pollit bat, gabaz dabilana,
Hark ura zikindurik, egotu naiz, ama!"
Tra lera, la la la

Dakigunaz geroztik zer erran amari,
Dugun pegarra pausa, gaitezen liberti!
Tra lera, la la la...