---
id: ab-4033
izenburua: Ohantzetik Kampo!
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Hantchet oihanean oihan ederrean
zeruari urbil inahala,
adar gorenean adar gorenean
Tra la la la la la la la la la la
la la la la la la la la la la la la la la la la

Ohantze bat bazen, oh ! bainan pollita !
Segur maita baizik etzitaken , behin ikusi-ta

Musa, belhar, hosto bildurik pochika
Gaicho amak zuen ohantzea egin pirka-pirka

Zorionak gero pizturik bihotza,
Bazarion hichtu, eta kantu, chorien othoitza...

Ohianean bazen kantu ta iguzki
eta ohantzean, egun batez, bortz arroltze ttiki,

Choriak behatu zeien amultsuki:
Geroztik beroki hegalpean zituen atchiki...

Handik laster bortzer kuskuñoak hautsi,
Hain ume pollitik egundaino ez zuen ikusi.

Egunen buruan ez zaion itsusi
Zoin gehiagoka huchtuz, kantuz zirelarik hasi.

Etzituen luzaz gaichoak gochatu
Zeren egunekin baitzaizkoten hegalak luchatu.

10 .Papa, tchichi, papa, noiz da, ama, Pazko
Ohantzetik ume largatzeko ez othe da goichko ?

Ohantze echkinan amatto nigarrez:
Umeak ohiuka ohantzetik airatu beharrez !

Amak bakothari emanik bi musu:
Zoazte, gaichoak, bainan othoi mirurari kasu !

13 .Ttiuka-ttiuka ziren hegaldatu...
Gero zer gertatu, nehork ez dauku salhatu...

Sort-herritik kampo bada kantu, irri !
Bainan, Eskualdunak, kampo hortan kasu mirueri
Kasu hatzerriko otsoeri eta lagun tzarreri !
Tralala ...