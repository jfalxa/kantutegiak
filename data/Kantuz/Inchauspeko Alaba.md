---
id: ab-4084
izenburua: Inchauspeko Alaba
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Inchauspeko alaba dendaria,
Goizian goiz jostera joailia, nigarretan pasatzen du bidia,
Aprendiza konsolatzailia konsolatzailia. (berriz)

Zelian den izarrik ederrena
Jin balekit argi egitera,
Juan naite maitiaren ikhustera
Ene phenen hari erraitera.

Mundu huntan phenarik gabetarik
Bizitzeko nik eztut perilik;
Maithatu dut ukhanen eztudana,
Horrek beitereit bihotzian phena.

Maithatu duzia ukhanen eztuzuna,
Horrek dereizia bihotzian phena?
Maith'ezazu uhkan diokezuna
Eta juanen da zure bihotz mina.

Mertchikaren floriaren ederra!
Barnian dizu hechurra gogorra;
Gastia niz, bai eta loriosua,
Eztizut galtzen zure esparantcha.

Adios beraz, ene maitia, adios!
Adios dereizut orai sekulakotz;
Ezkunt zite plazer duzunareki,
Eta begira ene arrakuntruti?

Zer izanen da zure arrakuntria?
Zer ahal da zure egin ahala?
Zerbait khasu nitan gerthatzen bada
Sujet berri bat zutan izanen da.