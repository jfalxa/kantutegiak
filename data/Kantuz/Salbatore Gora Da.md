---
id: ab-4076
izenburua: Salbatore Gora Da
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Salbatore gora da Garazi aldian;
Ni ere han nunduzun igaran astian,
Debozione gabe bai sentoralian,
Ene gazte lagunak han beitziradian.

Bortietan artzain'ta ez jaisten ardirik,
Ountsa jan, edan, eta egin lo zabalik,
Mundian ez ahalda ni bezañ irousik.
Ez nuke segur nahi bizitze hoberi!.

Izar bat jeikiten da Goizerri aldeti,
Argi eder batetan, leñhuru bateki,
Erien sendotzeko photeriareki;
Hounki-jin egin diot nik hari segurki.

Izar houra jiten da bozkarioreki,
Zelialat emanen naiala bereki;
Hitzaman diriozut nik hari saminki,
Haren zerbutcharia nizatela bethi.

Amorio zaharra, behar hait kitatu,
Hanitch phena dereitak hik eni kausatu;
Maite berri bat zitak ezpiritian sarthu,
Hari behar deroat bihotza libratu".

- "Amorio zaharrak zutia jenatzen,
Berri baten jitiak hanitch agradatzen?
Zu ere gaztettorik hasia maithatzen,
Hoberik duzunian orai ni kitatzen!

Zoumbat aldiz nigarrez egin dut uthurri,
Zu zinadiala kausa amak eraginik!
Arrazou're ziala sobera badakit,
Zeren zutzaz benintzan charmaturik bethi.

Kitatze'ko sujeta zer othe ahal den,
Ahal bezain barnati niagozu phentsatzen
Ene buria deusetzaz eztit akusatzen...
Inozent nuzu eta joan zite arren.