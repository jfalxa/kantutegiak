---
id: ab-4022
izenburua: Kantuz
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Kantuz sortu naiz eta kantuz nahi bizi,
kantuz igortzen ditut nik penak ihesi;
Kantuz izan dudano zerbait irabazi,
kantuz gostura ditut guziak iretsi;
Kantuz ez du ta beraz hiltzea merezi ?

Kantuz iragan ditut gau eta egunak;
Kantuz ekharri ditut griñak eta lanak;
Kantuz biltzen nituen aldeko lagunak:
Kantuz eman dauztate, obra gabe, famak;
Kantuz hartuko nauia zeruko Jaun onak !

Kantuz eman izan tut zonbaiten berriak,
Kantuz atseginekin erranez egiak;
Kantuz egin baititut usu afruntuiak,
Kantuz aithortzen ditut ene bekatuiak;
Kantuz eginen ditut nik penitentziak.

Kantuz eginez geroz munduian sortzia,
Kantuz egin behar dut, ontsalaz, hiltzia.
Kantuz emaiten badaut Jainkoak grazia,
Kantuz idekiko daut San-Pierrek athia,
Kantuz egin dezadan zeruan sartzia...

Kantuz ehortz nezate, hiltzen naizenian,
Kantuz ene lagunek harturik airian;
Kantuz ariko zaizkit lurrean sartzian;
Kantu frango utziko diotet munduian,
Kantuz has diten bethi nitaz orhoitzian.