---
id: ab-4071
izenburua: Saratarra Naizela
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Saratarra naizela
orok badakite, orok badakite
Semperen badudala
choriño bat maite.
Hura bezen pullitik, hura bezen pullitik
bertze bat balaite...
Bainanzer probetchu da,
nik hura dut maite.

Guziek badakite
naizela Piarres, naizela Piarres,
Choriño bat galdurik
nagola nigarrez;
Bera ibili baitzaut bera ibili baitzaut
ondotik beharrez;
Ederki ase bainau
Kafe'ta likuriez.