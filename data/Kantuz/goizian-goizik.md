---
id: ab-4054
izenburua: Goizian-Goizik
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Goizian goizik jeiki nunduzun
espusa nintzan goizian
Bai eta zetaz ere beztitu
ekhia jelki zenian.
Etchek .andere zabal nunduzun
eguerdi erditan
Bai eta alhargun gazte
ekhia sarthu zenian.

2
Musde Irigarai, ene jaona,
Altcha izadazu buria!
Ala dolutu othe zaitzu
Eneki espusatzia?
- Ez, ez etzitadazu dolutu
zureki espusatzia,
Ez eta ere doluturen
Bizi naizeno, maitia.

3
Nik banizun maitetto bat
Mundu ororen ichilik,
Mundu ororen ichilik eta
Jinko Jaonari ageririk:
Buket bat igorri ditadazut
Lili arraroz eginik,
Lili arraroz eginik eta
Erdia phozuaturik.

4
Zazpi urthez etcheki dizut
Gizona hilik khamberan;
Egunaz lur hotzian eta
Gaiaz bi besuen artian,
Zitru urez ukhuzten nizun
Astian egun batian,
Astian egun batian eta
Ostirale goizian.