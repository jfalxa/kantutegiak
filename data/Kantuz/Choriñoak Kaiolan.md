---
id: ab-4092
izenburua: Choriñoak Kaiolan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Choriñuak kaiolan
tristerik du khantatzen,
Duelarik han zer jan, zer edan,
kampua du desiratzen
zeren, zeren,
libertatia zoinen eder den.

Hik, kanpoko choria,
So.giok kaiolari,
Ahal balin bahedi
Hartarik begir.adi,
Zeren, zeren
Libertatia zoinen eder den.

Barda amets egin dit
Maitia ikhusirik,
Ikhus eta ezin mintza
Eztea phena handia
Eta ezin bestia?
Desiratzen nuke hiltzia.