---
id: ab-4093
izenburua: Arranoak Bortietan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Arranuak borthietan gora dabiltza hegaletan;
Ni ere beste orduz andereki khamberetan;
Orai aldiz, ardura ardura nigarra dizut begietan

Eztuta nik phena handia, entzulerik ükhengabia,
Juratu diñat fedia; doluturen zen maitia,
Eni behatu gabia, galdu diñat libertatia.

Erlia doa mendiz mendi, harek han biltzen du lili,
Hartarik egiten du ezti, eztiak oro ez bardin ezti,
Munduko neskatila gaztik eztira bardin charmagarri.

Eni anaiak igorri letera batez goraintzi,
Entzun diala bai berri banabilala andereki,
Utzi ditzadan anderiak, zer nahi dudan ideki.

Egin diot arrapostu nola beitu merechitu,
Harek ere zounbait beitu, nik hetzaz guti profeitu,
Uzten dudala hura bereki, utzi nezan ni eneki phausu.

Hartzen diat ofiziua, Iratiat ulhaiñ banua,
Nula beitut biziua oihanetan khantatzekua,
Abis hunik emaitez eta egia erraitez banua.

Ahaire hau zahar umen da, duda gabe en'adin beita,
Hountan nahi dit khantatu, noula den gazte perfeita,
Gezur guti erraile beita, umil eta serious hura.

Arrusatziak ejer lilia, zuhain berak du elhorria,
Amorio traidoriak berekilan azotia,
Hala dio borogatiak, begir'troump'adichkidia.

Aitak dio alhabari, noun habila, laidogarri?
Alhabak gezur aitari, zoumbait abis hounen sari.
Orai da bera doluturik, esklabo delakotz jarri.

Orai bera doluturik dago, bena berantu zaio,
Herritik joun nahiago egoitia laido beitzaio,
Hil baledi aldiz oraino, thoumban sarthu nahiago.

Hambat duzu aflijiturik, libertatia galdurik,
Phena dolorez kargaturik, osagarriz gabeturik,
Oro berak kausaturik, amouriouak traditurik.

Laur ourthetako haur gaichoua, leial duk hik amurioua.
Hitan etsemplu har dezagun, maitha goure protsimoua,
Lazukeriak kita eta zerbutcha zeluko Jinkoua.

Khantore hoien egitian, ulhaiñ nintzan borthian
Pasoulapeko oihanian, abis hounik emaiten nian,
Deuser'ezin sinhets eraziz, mankarrot egiteko aiherrian.

Hartz handi bat, oihan hartan, nihauri so ikhusi nian,
Hortzak churi, begiak gorri, lephoua lodi beitzutian,
Khantore hoien huntzia utzirik, mankarrot doble egin nian.