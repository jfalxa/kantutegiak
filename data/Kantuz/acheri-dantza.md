---
id: ab-4138
izenburua: Acheri-Dantza
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Acheri zar bat bainan ernia,
Ladron eta oilo jalia,
Inguru guzietan higuindia,
Izan zen artean hartia
Acheri zarra, nun duk buztana
Acheri zarra, nun duk buztana
Jinkoak nasaiki emana...

Bainan acheria beitzen goserik
Athera zen buztana utzirik:
Athera, diot, gibela mozturik,
Gauza hortaz ahalgeturik.

Nik ez dakit, egia erraiteko,
Hauche nolaz zen gerthatu;
Dakidana zuei kondatzeko,
Artetik zela eskapatu...