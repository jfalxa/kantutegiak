---
id: ab-4058
izenburua: Nigarrez Sortu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

1
Nigarrez sortu nintzan, nigarrez hiltzeko;
Ez ordean munduan luzaz bizitzeko:
Jendek izanen dute erraiteko frango,
Aita ama maiteak zuen biendako.

2
Aita-ama maiteak, mundura zertako
Eman izan nauzue zorigaitz huntako?
Ik ez nuen hobenik hola tratatzeko:
Federik eman gabe abandonatzeko.

3
Sortu nintzen ordutik hiru oren gabe,
Berho-choko batean gaicho eman naute
Nihaur bakharrik bizi, choriak haurhide
Ene kontsolatzerat kantuz heldu dire.

4
Ni goizeko aroan hementche paratu;
Geroztik iguzkia gainetik pasatu:
Aita gorde da eta amak nau ukatu:
Ez othe naute behar norbaitek altchatu?

5
Sortze trichte huntan naiz...mundura ethorri,
Oi hemen niagozu, bildotch bat iduri:
Lurra sehaska eta zerua estalgi,
Norena othe naizen Jainkoak badaki.

6
Zeruan iguzkia zelarik aphaldu,
Andre gazte bat zaitan aldetik pasatu;
Aingeru bat nintzela zenran ohartu
Amultsuki ninduen besoetan hartu.

7
- zatho enekin, zatho, aingeru maitea!
Dolugarri laiteke zu hemen uztea...
Bozkario enetzat zutaz jabetzea;
Erakutsiko dautzut zeruko bidea...

8
bathaiatu ondoko goiz alba gorrian
jainkoak bildu zuen agur ederrean:
Haurra ez da galduko munduko bizian:
Aingeruekin dago zeruko lorian.