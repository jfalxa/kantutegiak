---
id: ab-4097
izenburua: Urtzo Churia, Errazu
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Izar batek zerutik klaritatez betherik,
Gauaz ere argitzen du, bertze ororen gainetik;
Dudatzen dut baduienetz mundu huntan parerik mundu huntan parerik.

Izar haren begia, hain da charmagarria,
Koloreak churi-gorri, perfetzionez bethia
Eria-re senda liro haren begitharteak.

Uso churia, errazu, norat joaiten ziren zu
Espainiako mendiak oro elhurrez bethiak ditutzu,
Gaurko zure ostatu gure etchean baduzu.

Ez nu izitzen elhurrak ez-eta gau ilhunak,
Maitea gatik pasa nintzazke gauak eta egunak;
Gauak eta egunak, desertu eta oihanak.