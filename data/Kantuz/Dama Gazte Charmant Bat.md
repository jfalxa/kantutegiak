---
id: ab-4078
izenburua: Dama Gazte Charmant Bat
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Dama gazte charmant bat jiten zaut bichtara,
Iduritzen baitzeraut zeruko izarra !
Ama batek hazteko, hori zerbait bada !
Gure salbailzaileak kontserba dezala !
Trala, tralara, tralara, kontserba dezala !

«Gau on gizon gaztea, kompañiarekin !
Gostu du egoitiak ichtant bat zurekin ;
Badakizu mintzatzen errespeturekin,
Jainkoak har zitzala hil-ondoan berekin »!

« Ene maite pollita non othe den bizi,
Bazinakikeia zuk haren zerbait berri ?
Aspaldi ikustera joan nahia bethi ...
Errozu ene phartez., milaka goraintzi ».

«Ez zinukeia hobe zauhaurek joaitea,
Nitaz igorri gabe komisionea ?
Samurturen othe den ez zaitela fida ;
Zato enekin eta, biak joanen gira ".

«Jin ere banindaite zurekin gogotik,
Ez banu bihotzian deus errangurarik :
Kambiatua dela norbaitek erranik,
Hortako ez naiz joaiten, deus ez dut bertzerik.

Zato enekin eta errozu berrari
Leheneko hitzetan dagoenez bethi ?
Disimulatzen badu, utzazu laburzki.
Ni hemen izanen naiz zure zerbitzari ».

« Ene maite pollita, nolache zirare ?
Berantchko ikustera heldu naiz ni ere.. .
Ni pobrea naizela mintzatu zirare,
Gure alientziak bethikotz joan dire ! »

« Ez zirea orhoitzen, o gizon gaztea,
Duela zazpi urthe -ez dut ahantzia !-
Ez zinuela aski goguan hartzea,
Ez baitzinuen ekartzen hitzeman dotia ? »

« Duela zazpi urthe, etche huntan, behin,
Ez zirea orhoitzen zer zinuen egin ?
Ezkonduko zinela segurki enekin,
Hitzeman zinerautan juramenturekin!

« Ene hitzemaiteak ez zuen balio,
Gogoetan egon naiz ait'amekin gero ;
Doterik ez duzula mintzo dira oro,
Hortaz ez dut gogoa lehen bezen bero ".

« Emazte-gai bat badut egina berria,
Hogoi mila pichtola duena dotia ;
Bertze bat atzemazu ni baino hobia,
Ni neureaz kontent naiz, adios, maitea ! »

« Zazpi urthen buruan, despegida hori !
Aise egiten duzu orai nitaz irri!
Arrallerian zerbait nik errana gatik
Ez nauzun galdaturen sekulan doterik...

Zuhauren falta dela, kontsidera zazu,
Aspaldian baitzira zu nitaz trufatu:
Dolutu zautzu bainan ez duzu probetchu,
Adios, bai, maitia, fortuna egizu ! »

Eni emanarazi hitz eta fedea,
Hortan kolpatzen duzu Jaunaren legea;
Arrasta bat han bada ikaragarria:
Fede ukhatuarentzat ifernu gorria!

« Kontzientzia garbi, adios matiea,
Nere ganik hor duzu orai despegida ;
Zure ala enea othe den egia,
Lekuko izanen da zeruk'Erregea!

Belhaunikatzen nuzu zure aintzinean
Ene falta guziez pena bihotzean,
Ez nezazula kita orai ez bizian,
Biak biziko gira gostura munduan!»

« Chuti zaite, maitea, indazu eskua,
Bihotza arrailtzen zaut hola ikustea!
Ez zaitut kitaturen egun, ez bizian,
Biak biziko gira gostura munduan !

Urtubiak' alaba begira zaudena,
Zuk emaiten dautazu, bihotzean pena!...
Zu zaitu bigarrena, hau nuen lehena,
Hau galtzen dutanean, jinen naiz zu gana.

« Ez othe naiz bada ni hori bainon hobe,
Nahiz gorphutzez eta ontasunez ere ?
Nik baditut neureak sei mila untz urhe,
Heien erdiak zure zerbitzuko dire...

« Zuk sei mila untz urhe, nik bertze haimbertze
Gu biak elgarrekin ongi jin gintazke. ..
Bainan, fedea diruz ezin eros daite, .
Hura huni emana orai zazpi urthe !..

« Ene maitiarekin gostura naiz hemen ;
Othoitz egin dezagun oro salba gaiten,
Hil eta zeruetan sar gaitezen chuchen,
Ene esposarekin sekulakoz, Amen!
TTIPI, Urruñarra.