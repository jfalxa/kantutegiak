---
id: ab-4082
izenburua: Maitia, Nun Zira
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Maitia nun zira ?
Nik etzutut ikhusten,
Ez berririk jakiten,
Nurat galdu zira ? (berriz)
Ala khambiatu da zure deseiña ?
Hitzeman zenereitan,
Ez behin, bai berritan,
Enia zinela.(berriz)

Ohikua nuzu;
Enuzu khambiatu,
Bihotzian beinin hartu,
Eta zu maithatu.
Aita jeloskor batek dizu kausatu:
Zure ikhustetik,
Gehiago mintzatzetik
Hark nizu pribatu.

Aita jeloskorra!
Zuk alaba igorri,
Araiz ene ihesi,
Komentu hartara !
Ar'eta ez ahal da sarthuren serora:
Fede bera dugu,
Alkarri eman tugu,
Gauza segurra da.

Zamariz iganik,
Jin zaizkit ikhustera,
Ene kontsolatzera,
Aitaren ichilik ;
Hogoi eta laur urthe baditit betherik:
Urthe baten burian,
Nik eztiket ordian
Aitaren acholik.

Alaba diener
Erranen dit orori:
So'gidaziet eni,
Beha en'erraner ;
Gaztetto direlarik untsa diziplina:
Handitu direnian
Berant date ordian,
Nik badakit untsa.