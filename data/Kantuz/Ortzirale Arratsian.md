---
id: ab-4112
izenburua: Ortzirale Arratsian
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Ortzirale arratsian
Garruzetik jin nindian;
Afaldu behar bidian,
Paluak hartu nintian.

Ondoko egun goizian,
Zopikunak eltzian
Askaldu behar-bidian,
Eltzia hautsi gindian.

Ama-semiak bilotik
Lothu zaizkidan gogotik;
Debriemaztiak ederki
Mahain azpirat aurdiki.

Emazte mila debria,
Utz dautan pazientzia!
Ala nahi daunaia
Arras idoki bizia?

Etchian horra zer modu
Dugun aspaldian bildu.
Arnoak badik talendu:
Etchiak erreka jo du.