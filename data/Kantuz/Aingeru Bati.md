---
id: ab-4098
izenburua: Aingeru Bati
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Maite dut eta ezin erran,
Aingerubati maite dudala,
Zeru garbiak arratsean
Izarra maite duen bezala!
Zer! Ez da bada zori gaitza!

(Elissamburu)
Mintzatu nahi ezin mintza?
Non nahi naizen gogoan dut,
gauaz, egunaz, aingeru hori;
Urrundik frango chede ba dut,
bainan hurbildik ezin atrebi!
Hurbildik ezin atrebi!

Igande bat zen ikhustean
Nik aingerua lehen aldikotz;
Haren itchura bihotzean
Geroztik hor dut eta bethikotz!
Meza zen hari aldarean...
Belhauniko zen othoitzean...
Gero behatuz zeruari
Agur ezti bat egorri zuen
Aingeru bere lagunari...
Ordu beretik maithatu nuen!
Orduan maithatu nuen!
Ondikotz nik orduan maithatu nuen!

Ez! gehiago mundu huntan
Esperantzarik ez da neretzat!
Hura hain gora! ni herrestan!
Nola gaitazke gu elgarrentzat?
Lurra gizonen da lekhua,
Aingeruena da zerua.
Zertako zare zu zerutik
Lurrera jautsi, nere maitea!
Eta zertako bihotzetik
Hola dautazu khendu bakhea?
Zertako, nere maitea,
Khendu dautazu bihotzetik bakea?

Uri ondotik hosto-pian
Ikharan dago gaicho lantchurda:
Haize gachtoak ematean
Lohi beltzerat eror beldur da;
Maitea zutaz orhoitzean
Beldur bera dut bihotzean;
Garbia zare, bai! behautzu
Lohirat eror, zuk aingerua!
Lurra utzirik beha zazu
Gutaz gorago nun den zerua.
Behautzu, oi, aingerua!
Lurra utzirik, atchik-azu zerua!
Hurbildu eta ez naiteke atrebi.