---
id: ab-4030
izenburua: Irrintzina
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Gau eder batek ninduen hatzeman
mendi gainetan
Iguzkia aintzinean
emeki etzan zitzautan
Azken abereak etchera zoazin
ene oinetan
bazter guziak zituztela
inharrosten marrumetan.

Lehen izarrek zeruan
Orai zuten disdiratzen.
Ordokian harrabotsak
hasiak ziren eztitzen;
Egunaren ondotik gaba
bere aldian hedatzen,
Herrian etche zombait
ziren oraidanikan argitzen.

Bortu gainean artzain bat
hautemaiten nuen kantuz,
Eskualdunen aire zahar
ederrak charamelatuz.
Boz hura bakarrik orduan
mendi gain hetan adituz,
Iduri zautan arbasoak
zaudela harekin oihuz.

Bat batean, ilhumbean
oihu bat da eraikitzen,
Bere samintasunean
ninduelarik harritzen.
Mendi kaskoan harri kotor
zaharrak zituen jotzen,
Eta leku basa hetako
oiharzunak iratzartzen.

Oihu gora garratza zen
nola emaztearena,
Azkarra eta bortitza
nola gizonkiarena;
Artetan gero erranen zen
ihizi basa batena,
Zerbait atchikitzen zuela
oraino jendearena.

Luzaz zuen inharrosi
mendi hetako bortua,
Iduri zuen hastean
norbait zela minhartua:
Irri lotsagarri batean
zen aldiz akabatua,
Bide chendran nindagolarik
oraino ni harritua.

Ibañetan ere lehen
irrintzina aditu zen,
Arrolanen bertutea
laster zuela lotsatzen.
Deihadarraz debalde ziren
mendiak dardarikatzen;
Eskualdunen irrintzina zen
deihadarrari nausitzen.

Gogoetan nintzalarik,
osoki zen ilhundua:
Zerua zuten izarrek
mila itzez itzatua:
Bortuan aldiz irrintzina
aspaldi zen ichildua,
Ordokian lanhoek zuten
hasi bere gudua.

Azken aldikotz behatu
nituen bortu ilhunak;
Iduriz orai zerautan
ihardesten oiharzunak;
Mendian entzuten direno
hoin ederki irrintzinak,
Ez doazi ezeztatzerat
lurretikan Eskualdunak .