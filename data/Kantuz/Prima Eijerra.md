---
id: ab-4099
izenburua: Prima Eijerra
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Prima eijerra,
Zutan fidaturik,
Hanitch bagira
Oro trompaturik!
Enia zirenez erradazu bai al'ez;
Bestela
Banoa
Desertiala, nigarrez hurtzera.

Desertiala,
Juan nahi bazira,
Arren zuaza,
Oi! bena berhala!
Etzitiala jin berritan nigana
Bestela
Gogua
Doluturen zaitzu, amoros gaichua.

Nitan etsemplu
Nahi dianak hartu,
Ene malurrak
Parerik ez baitu.
Charmagarri bat nik bainian maitatu
Fidatu
Trompatu!...
Sekula jagoiti ikhusi ezpanu!

Mintzo zirade
Arrazu gabetarik
Eztudala nik
Zur'amodiorik;
Zu beno lehenik banian besterik:
Maiterik
Fidelik
Hor eztereitzut egiten ogenik.