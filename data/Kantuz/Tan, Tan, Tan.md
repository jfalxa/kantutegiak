---
id: ab-4120
izenburua: Tan, Tan, Tan
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Tan tan tan tan
Rapetaplan!
Artzain zaharrak tafarnan
Hordi gira
Ez, ez gira
Basoak detzagun bira!
Johoho Johoho (bis)
Basoak detzagun bira (bis)

Tan, tan, tan, tan,
Rapetaplan!
Nork jotzen derauku borthan?
Beharbada
Otsoa hor da
Nihor ez gaiten athera!
Johoho! Johoho! (bis)
Nihor ez gaiten athera! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Uria hari karrikan:
Gauden hemen
Arno hunen
Gostu onean edaten
Johoho! Johoho! (bis)
Gostu onean edaten! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Babazuza tarrapatan!
Dugun edan
Hamarretan
Aberats gira gau huntan!
Johoho! Johoho! (bis)
Aberats gira gau huntan! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Ez dut minik sabelean!
Nahi nuke
Ehun urthe
Hola egon banindaite!
Johoho! Johoho! (bis)
Hola egon banindaite! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Arnorik ez da botoilan
Ostalera,
Ez ikara;
Arnolo bethi sos bada
Johoho! Johoho! (bis)
Arnoko bethi sos bada! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Zer othe dut begietan!
Non da bortha?
Airatu da!
Mahaina dantzan dabila!
Johoho! Johoho! (bis)
Mahaina dantzan dabila! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Zangoek amor bidean!
Hanketan, min!
Gaizo Martin!
Urhatsik ez dirok egin!
Johoho! Johoho! (bis)
Urhatsik ez dirok egin! (bis)

Tan, tan, tan, tan,
Rapetaplan!
Eri tchar naiz hiltzekotan!
Sendo nintzan
Aski edan
Izan banu gau hunetan!
Johoho! Johoho! (bis)
Aski edan gau hunetan! (bis)

(ELISSAMBURU.