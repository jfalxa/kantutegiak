---
id: ab-4133
izenburua: Neure Dotea
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Aitak eman daut dotea
Neurea, neurea! (bis)
Urdeño bat bere umekin,
Oilo koloka bere chitoekin,
Tipula korda heiekin!
Tipula korda heiekin!

Otsoak jan daut urdea,
Neurea, neurea (bis)
Acheriak oilo koloka
Garratoinak tipula korda
Adios neure dotea!
Adios neure dotea.