---
id: ab-4074
izenburua: Lili Bat Ikhusi Dut
kantutegia: Kantuz
partitura: null
midi: null
youtube: null
---

Lili bat ikhusi dut baratze batean
Desiratzen bainuke nere sahetsean;
Lorea ez du galtzen, udan ez neguan,
Haren parerik ez da bertze bat munduan!

Deliberatu nuen gau batez joaitera,
Lili arraro haren eskurat hartzera.
Ez bainuen pensatzen beiratzen zutela!...
Gau hartan uste nuen, han galtzen nintzela!

Etsemplu bat nahi dut eman guzieri,
Eta partikularzki jende gazteari:
Gauaz ibiltzen dena ez da zuhur ari;
Ni begiraturen naiz, eskerrak Jaunari.