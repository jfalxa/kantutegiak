---
id: ab-3251
izenburua: Andria Alargune
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003251.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003251.MID
youtube: null
---

Andria alargune, lumeradune,
Lumeradune,
Biar etorriko da Juan Migel jaune,
Juan Migel jaune.
La ra la ra...

Bederatziak jo ta amarrak joko,
Amarrak joko;
Berandutuko zaigu San Urbanoko,
San Urbanoko.
La ra la ra...

Asta txikurrun txakun, enaiz eroa,
Enaiz eroa:
Txardiñ burua baño obea olloa,
Obea olloa.
La ra la ra...

Asta txikurrun txakun, mendian elurre,
Mendian elurre.
Mari Peparen aurre bizkar makurre,
Bizkar makurre.
La ra la ra...

Erdi erdian eta bi bazterretan,
Bi bazterretan;
Neskatx'ederrak dira erri onetan,
Erri onetan.
La ra la ra...