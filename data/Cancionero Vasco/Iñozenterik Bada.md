---
id: ab-2120
izenburua: Iñozenterik Bada
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002120.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002120.MID
youtube: null
---

Iñozenterik bada
Iñondik aditzen, (bis)
Orain abiatzen naiz
Oien abertitzen.

Animen faboretan
Guazen konbertitzen;
Munduko interesak
Eztute serbitzen.

Jesus larrugorrian
Gurutzian iltzen.