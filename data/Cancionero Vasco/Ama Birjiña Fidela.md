---
id: ab-1577
izenburua: Ama Birjiña Fidela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001577.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001577.MID
youtube: null
---

Ama birjiña fidela,
Alabatua zarela!
Nere mandua atera zela
Miñarik gabe bidera.
Txangrin bat artu ederra, (artût)
Ala konbeni bide da.