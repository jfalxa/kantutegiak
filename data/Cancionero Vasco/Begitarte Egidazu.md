---
id: ab-3065
izenburua: Begitarte Egidazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003065.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003065.MID
youtube: null
---

Begitarte egidazu etxian sartzian,
Kariño ona bidian goitian.
Sukaldian sartzian, xutik sukaldian,
Serbillete eder bat utzazu besoian,
Botila eskuan, basua bertzian.

Sagarraren iñarraren puntaren puntan )
Xoriñua zegoen kantari: )bis
Ai, tiruriruri, ai, tiruriruri:
Nork kantatiko dun soñu ori.

Neska zahar zahar denbora pasatu,
Zertako etzinen bada gazterikan ezkondu,
Berauzu, alauzu?
Anitzek erranen didazu, berauzu, alauzu.

Gauzak sasoñian estimua du, estimua du,
Zahartuz geroztik enfadu,
Hala du, hala du.
Ederrak beti gala du, gala du.

Neska zahar zahar denbora pasatu, debora pasatu,
Zahartuz geroztik enfadu,
Hala du, hala du.
Ederrak beti gala du, gala du.