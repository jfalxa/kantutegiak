---
id: ab-2016
izenburua: Gure Etxian Laur Ardi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002016.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002016.MID
youtube: null
---

Gure etxian laur ardi,
Laurek zortzi beharri.
Egündano ez nüzü izan
Orain bezain egarri:
Kolpüño bat edan eta,
A! zer kantaria ni! (bis)

Benedika dakiola
Aihenari ondua,
Zeren eman beiteio
Arduari gozua.
Edan dizüt bost pinta-erdi:
A! Zer mahast ardoa!
Hain alegera ezarri
Nai nola biligarrua.