---
id: ab-1741
izenburua: Bat Aranaztarra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001741.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001741.MID
youtube: null
---

Bat Aranaztarra ta bertzia Ameli,
Leihotikan keinuka galtza-gorrieri.
Heiek salutatu ta leihotikan irri,
Ezaiote itxusi donzella hoieri.