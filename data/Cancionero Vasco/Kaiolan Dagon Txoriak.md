---
id: ab-2184
izenburua: Kaiolan Dagon Txoriak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002184.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002184.MID
youtube: null
---

Kaiolan dagon txoriak
Erne derauzka begiak,
Nondik adituren dituen
Bere lagunen berriak.
Libertatiak ahanzten ditu
Munduko penen erdiak.