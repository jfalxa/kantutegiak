---
id: ab-2571
izenburua: Gaztigatu Dirade (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002571.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002571.MID
youtube: null
---

Gaztigatu dirade Irundik unera
Bertsu berritxo bida para ditzadala:
Dibersiuagatik saiatuko gera,
Atzendu ezpazaiku dakigun eskuara.