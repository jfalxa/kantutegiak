---
id: ab-1763
izenburua: Berantxe Jakin Dizüt
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001763.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001763.MID
youtube: null
---

Berantxe jakin dizüt berri handi bat:
Arantxeta bürgian jan diela gatü bat.
Haiek jan zikezien axuri gizen bat,
Edo eta bestela aragi laurden bat,
Jaten zienian gatü zahar bat.