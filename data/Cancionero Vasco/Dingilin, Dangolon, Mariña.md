---
id: ab-1842
izenburua: Dingilin, Dangolon, Mariña
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001842.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001842.MID
youtube: null
---

Dingilin, dangolon, Mariña,
Beti onelan bagiña!
Beti onelan izaterako
Poltsia degu ariña.