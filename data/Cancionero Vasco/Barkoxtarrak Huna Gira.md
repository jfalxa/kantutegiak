---
id: ab-1736
izenburua: Barkoxtarrak Huna Gira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001736.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001736.MID
youtube: null
---

Barkoxtarrak huna gira
Saldo eder bat beikira.
Karrikart Athereikiaren
Khantore berri berriak,
(I)alo, jaunak, ditzagün khanta,
Untsa dolorus beitira.