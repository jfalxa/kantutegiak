---
id: ab-1611
izenburua: Amorioak Zaitu Penetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001611.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001611.MID
youtube: null
---

Amorioak zaitu
Penetan paratu;
Orrek garaitu zaitu
Ta martirizatu.