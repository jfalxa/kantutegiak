---
id: ab-1819
izenburua: Ardiak Non Tuk Bettiri (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001819.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001819.MID
youtube: null
---

- Ardiak non tuk, Bettiri?
- Ardiak itzalian lo.
Eta bubaño, eta bubaño.
Ardiak eman ostiko:
Ni ez ote naiz biziko?
Eta bubaño, eta bubaño;
Ni ez ote naiz biziko.