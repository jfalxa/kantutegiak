---
id: ab-1549
izenburua: Aitak Utzi Ziraden
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001549.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001549.MID
youtube: null
---

Aitak utzi ziraden (zerautan)
Zerura joaitian,
Neretzat magastia
Artzeko lanian.
Arta nezala,
Aitak bezala,
Aitzur eta jorra,
Laguntzat on nuela
Sotoko oporra.