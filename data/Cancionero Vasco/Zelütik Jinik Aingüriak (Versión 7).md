---
id: ab-2830
izenburua: Zelütik Jinik Aingüriak (Versión 7)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002830.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002830.MID
youtube: null
---

Zelütik jinik Aingüriak
Mariari dü mezützen.
Amatako Jinko Semiak
Hura diala haitatzen.