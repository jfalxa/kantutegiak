---
id: ab-2239
izenburua: Kreaturaño Bat Xarmegarria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002239.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002239.MID
youtube: null
---

Kreaturaño bat xarmegarria,
Dendari gazteño bat plazatik.
Haren larruak distiratzen du
Nola artizarrak zerutik.
Mutil gaztia ez daite parti
Lore xarmant'haren aldetik.

Lore xuri-gorri, talle xarmegarri
Gazte bruna ta propia:
Liberti zaite oneski, bañon
Guardia zure floria.
Iñork uste baño lañerusago da
Mutil gaztiaren malezia.

Mutil gaztia juaiten denian
Amuranteñoaren leihora,
- Xarmegarria, har nezakezuia
Memenpat zurekin barnera?
Zuri itzño bat deklaratu eta,
Nua berehala etxera.

Bas-erramuak ostuak berde,
Sasoi ororez kolore.
Neskatilla gaztek enaute maite:
Deliberatu dut fraile;
(Y)Oloroneko komendu batean
Nere begira baitaude.

- Ni maite nauzula, zuk omen diozu;
Nik ere maite zaitut zu.
Diozun bezen maite banauzu,
Elizan feda nezazu;
Elizan feda nezazu eta
Gero zuria naukezu.

- Zure elizan fedatzia
Nik ez dut egundaño pensatu.
Neskatilla baten tronpatzia
Neri etzerau, ez, bekatu.
Bertze bida ere egin nitzazke
Nai balire fidatu.