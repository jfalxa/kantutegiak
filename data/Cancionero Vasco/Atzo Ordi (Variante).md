---
id: ab-1695
izenburua: Atzo Ordi (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001695.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001695.MID
youtube: null
---

Atzo hordi eta egun egarri,
Galtza ziluetarik isterrak ageri.
Zuazi, errakozu dendariari
Yostera yin dadien bihar edo etzi:
Galtzak behar tudala zuri edo gorri.

Gazten nintzanian hogoi urtetan,
Ardura nindabila andre gaztetan.
Eta orai aldiz ostatuetan,
Diru guti multsan behar orduetan,
Nehork enute nahi konpainietan.

Zuberoan bada mutil ederrik:
Ene semia duzu batto hetarik.
Ez dezake begira andre gaztetarik;
Ez daki zer nahi duyen atera hetarik.
Hobeki egin lio bere gisa utzirik. (liro)

Amatxo, zer duzu beti niri so?
Ni, othoi, utzi bezazu maiz libro. (nezazu)
Gazte nuzu eta libertitu nahi.
Beti hola egonea, enaite liberti;
Arrazoina dudala, etzaizu iduri.