---
id: ab-1786
izenburua: Biba Española Ta Oronelen Tropa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001786.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001786.MID
youtube: null
---

Biba Española ta Oronelen tropa,
Tetuanen sartua Marrukuen kontra;
Indua fuerza atake gaitza,
Bitorioso nausi gelditu baita.

Milla ogei kañon ta Ciudadia
Ek denak utzik gan da ango yabea,
Gizon miserablia, gizon pobria,
Orai iduri du kaudalik gabea.

Illeren ogeietik seyera artian
Tetuanen sartu de ez ala ustean
Guziz bortzian ziren trantzian
Etzakiten nola sartu bertzen etxean.

Lendabiziko asten da infanteria,
Beala prest dago kaballeria
Artilleria buruzagia
Artxek ematen zaben ikareria.

Guziz bortzian ziren trantzian
Nola sartu etzakiten bertzeren etxian
Firme ta sano daude oraño
Nayago dut iltzia kobarde baño.