---
id: ab-1591
izenburua: Ama Ezkonazi Ninduzun (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001591.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001591.MID
youtube: null
---

Ama, ezkonazi ninduzun
Ogei urtetan;
Senar bat eman zinatan
Laur ogei urtetan:
Ta ni, gaixua,
Nigarrez penetan.