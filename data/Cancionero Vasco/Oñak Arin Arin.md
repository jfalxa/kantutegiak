---
id: ab-3109
izenburua: Oñak Arin Arin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003109.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003109.MID
youtube: null
---

Oñak arin arin eta
Buru arinago:
Dantzan obeki daki,
Arto-korran baño.