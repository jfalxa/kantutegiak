---
id: ab-2182
izenburua: Judas Neure Diszipulua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002182.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002182.MID
youtube: null
---

Judas neure diszipulua
Neuretzat daukezu laztana.
Dakida larri emango diezu
Amoriozko eztana.
Neure petxua zabali dago ña
Zutzen ba za nigana.

Orain esan dezagun
Errosariua
Anima difuntua
Sufrajiua.
Ayek gugaitik,
Guk ayengaitik.
Zerura zar dizagun
Purgatoriotik.

Birjiña Karmenguaren
Lora eder ori
Graziak ematera
Zeuganaz nator ni.

Infernuetan dago
Purtzu zubezkua,
Egiten eztuenak
Jesusen gustua.

Judasen bizitza
Gaitu gu tentatzen
Jesusen grazietan
Gaitu gu apartatzen.

Oi hermano ezta zenteno
Kristo bidian klabado
Guri alarguna en el kruz
Santa Barbara. Amen Jesus.