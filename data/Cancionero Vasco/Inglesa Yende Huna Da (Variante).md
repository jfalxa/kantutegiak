---
id: ab-2037
izenburua: Inglesa Yende Huna Da (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002037.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002037.MID
youtube: null
---

Inglesa yende huna da,
Gero ze izain bada ere.
Egin ahala egin du
Españolaren fagore.
Frantzesak aldiz ez du
Konbatarikan batere;
Arabez gure Yinkoak
Emaiten dako podere.