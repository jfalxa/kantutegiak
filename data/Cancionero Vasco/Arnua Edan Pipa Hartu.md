---
id: ab-1644
izenburua: Arnua Edan Pipa Hartu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001644.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001644.MID
youtube: null
---

Arnua edan, pipa hartu,
Gero buruia zoratu;
Familietan bihar diruiak
Ostatuetan xahutu.
Ene semeak, etzaieztela
Ostatuetan abusa:
Gizonaren zoragarri dut
Mahats arnoa filusa.