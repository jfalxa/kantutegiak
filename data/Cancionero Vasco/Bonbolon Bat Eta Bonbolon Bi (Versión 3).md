---
id: ab-1804
izenburua: Bonbolon Bat Eta Bonbolon Bi (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001804.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001804.MID
youtube: null
---

Bonbolon bat
Eta bonbolon bi,
Momo zite aur ori,
Lenik zu eta gero ni.