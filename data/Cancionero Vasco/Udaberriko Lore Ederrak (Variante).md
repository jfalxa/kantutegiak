---
id: ab-2768
izenburua: Udaberriko Lore Ederrak (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002768.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002768.MID
youtube: null
---

Udaberriko lore ederrak
Juaten dira negu aldera;
Beti gustora biziko danik
Ezta jaioko mundura.
Ezta jaioko, ez ta jaioko
Zu bezelako imajinik,
Zureganako amoriua
Neri kenduko didanik.