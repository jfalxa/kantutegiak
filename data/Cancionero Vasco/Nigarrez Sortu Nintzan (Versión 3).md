---
id: ab-2367
izenburua: Nigarrez Sortu Nintzan (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002367.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002367.MID
youtube: null
---

Nigarrez sortu nintzen, nigarrez hiltzeko,
Ez ordean munduan luzaz bizitzeko.
Yendek izanen dute hainitz erraiteko,
Aita-ama maiteak, zuen biendako.

Aita-ama maiteak, mundura zertako
Eman izan nauzue zorigaitz huntako?
Nik ez nuen hobenik hola tratatzeko,
Federik eman gabe abandonatzeko.

Sortu nintzenetikan hiru oren gabe,
Bero baten gainian gaixoa eman naute;
Bakarrik han nindagon, xoriak haurride,
Nere konpaniatzen kantuz ari dire.

Iduzkia zenian zeruan apaldu,
Andre gazte bat zaitan aldian pasatu,
Aingerua nintzela zenian ohartu,
Amultsuki ninduen besoetan hartu.

- Zato nerekin, zato, aingeru maitea.
Nik erakutsiko dautzut zerurat bidea;
Neretzat alegrantzia zure altxatzia
Damu ere balitake zu hemen galtzia.

Sortu nintzenetikan biharamun goizian
Parabisuan nindagon aingeruen artian.
Aitak eta amak utzia, beroharen gainian
Andredena Mariak eman deraut zerurat bidia.