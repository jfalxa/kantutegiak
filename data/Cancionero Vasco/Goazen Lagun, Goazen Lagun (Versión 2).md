---
id: ab-2139
izenburua: Goazen Lagun, Goazen Lagun (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002139.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002139.MID
youtube: null
---

Goazen, lagun, goazen, lagun,
Biak Artizarrenerat.
Ursoño bat aurkitzen dela
Artizarrenen plazan da.
Ursoño hura behar dut
Bildu ene sareetara,
Ene sareetara.