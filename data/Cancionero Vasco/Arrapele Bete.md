---
id: ab-1661
izenburua: Arrapele Bete
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001661.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001661.MID
youtube: null
---

Arrapele bete,
Bil, bil, bil.
Orain ez den etortzen,
Gero eztela ariko.