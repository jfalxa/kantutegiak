---
id: ab-2888
izenburua: Oihanpeko Sagarraren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002888.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002888.MID
youtube: null
---

Ohianpeko sagarraren
Adarraren eiharraren
Puntaren puntan,
Puntaren puntan,
Xoria zegoen kantari:
Tirukiruri, tiruliruli.
Nork lezake kanta soiñutxo hori.