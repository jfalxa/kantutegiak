---
id: ab-1812
izenburua: Bordeleko Larretatik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001812.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001812.MID
youtube: null
---

Bordeleko Larretatik )
Ageri da Parise. )bis
Nik ere an baditut
Bederazi aurride:
Bi apez, eta bi fraile,
Beste bostak emaile.