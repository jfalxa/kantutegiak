---
id: ab-1896
izenburua: Enoas Eta Elias
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001896.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001896.MID
youtube: null
---

Enoas eta Elias daude
Oraindikan ilgabiak;
Predikatzeko gure faborez
Yaunak daduzka gordiak.
Denbora eta kusiko dira
Guziaz zeloz betiak.