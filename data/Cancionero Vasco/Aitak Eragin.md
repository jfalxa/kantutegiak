---
id: ab-1544
izenburua: Aitak Eragin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001544.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001544.MID
youtube: null
---

Aitak eragin, amak eragin;
Amak eragin didazu.
Amak eragin didazu, maitia;
Berdin neria zera zu. Booo...