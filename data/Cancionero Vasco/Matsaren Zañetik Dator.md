---
id: ab-2312
izenburua: Matsaren Zañetik Dator
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002312.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002312.MID
youtube: null
---

Batek: Guztiak:
Matsaren zañetik dator... Zumoaren gozoa.
Edatera pozez nator... Txopin bat osoa.
Indazu ez nadien il ... Pil pil beterik basoa...
Ta jarriko zait pil pil,... Bai, biotz gaixoa.

Eskeintza (Guztiak):
Nik zuri, zuk niri,
Agur egiñez alkarri,
Basoa txit garbi
Bear degu jarri.

Maitagarria da aunitz... Zumoaren gozoa
Zortzi kuartoan ba'litz... Txopin bat osoa
Edanen genuke sarri... Pil pil beterik basoa
Parrez kil-kil dedin jarri... Bai biotz gaixoa.

Eskeintza

Noek sortu izandu zuan... Zumoaren gozoa
Baita ere aisa edan... Txopin bat osoa
Berak nai zuen bezela... Pil pil beterik basoa
Pozik gelditzen da ola... Bai biotz gaixoa.

Eskeintza

Arren semea ere naiz... Zumoaren gozoa
Napar gorri onetatik... Txopin bat osoa
Zuri orduko nonbait... Pil pil beterik basoa
Dantzan abiatzen zait... Bai biotz gaixoa.

Eskeintza

Jangoikoak digula... Zumoaren gozoa
Aldian bein ta txit sarri... Txopin bat osoa
Txin-txin-txin edan dezagun... Pil pil beterik basoa
Igarian dedin jarri... Bai biotz gaixoa.