---
id: ab-1573
izenburua: Altza Eder Ur Airian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001573.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001573.MID
youtube: null
---

Altza eder ur airian,
Iru arrosa maiatzian.
Amoriorikan eztuenak
Eztu minikan biotzian.