---
id: ab-2357
izenburua: Nai Duenak Ikusi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002357.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002357.MID
youtube: null
---

Nai duenak ikusi Jesusen jayotza,
Ona ementxen dago, begiratu beza.
Espritu Santoaren milagrozko boza
Ekusi zenduenak, artuko du poza.