---
id: ab-2650
izenburua: Lehen Orai Eta Bethi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002650.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002650.MID
youtube: null
---

Lehen, orai eta bethi )
Ni bizi naiz penatuki. )bis
Zeren eta baitugu )
Amodioa berri. )bis

Etxian eder erramu, )
Etxeko alhabak erran du. )bis
Nahi badu ezkondu,
Noiz nahi ordu du.