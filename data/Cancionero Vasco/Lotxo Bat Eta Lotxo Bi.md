---
id: ab-2655
izenburua: Lotxo Bat Eta Lotxo Bi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002655.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002655.MID
youtube: null
---

Lotxo bat eta lotxo bi,
Amak goxoak ekarri.
Beste guziai bana ta bina,
Perikitori amabi.