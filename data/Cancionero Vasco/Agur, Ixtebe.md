---
id: ab-3110
izenburua: Agur, Ixtebe
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003110.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003110.MID
youtube: null
---

- Agur, Ixtebe, agur, Ixtebe,
Bizi zirade oraino?
- Bizi naiz eta bizi
Gogorik ere,
Artzekoak kobratu artaino.