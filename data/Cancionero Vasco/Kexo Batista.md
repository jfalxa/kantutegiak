---
id: ab-2222
izenburua: Kexo Batista
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002222.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002222.MID
youtube: null
---

(Xenpelar). Kexo, Batista, etzaitut aspaldian ikusi,
Zeren elkarren gandik urrun garen bizi.
Gure herria hontatik juan ziñan igesi,
Irun aldia hortan bai lanian hasi.
Jan edan ondo egin ta galanki irabazi.

(Batista). Irabazi andiak, sobratuak pixkak:
Ni aurrena naizela, zenbeit bagabiltzak,
Lagun arterat geroz, kanta edo brixka,
Ostatuan sartuz geroz gastatzen errex da.
Irutarikan biek, igualtsu gabiltza.

(Xenpelar). Neonek sumatu ditut kontari daudela
Amalauna erriel jornala dutela.

Eta gañera bostez mantentzen dutela:
Gizona aberasten ez duk gaiz horrela.

(Batista). Amalauri bost kendu, bederatzi daude;
Erriel bat ezer ez da, sagardutako ere!
Astian egoaldi txarra, jayak noiz nai dire;
Jan bearrak segitzen alakoan ere...
Xenpelar, oriek guziek frogaturik daude.