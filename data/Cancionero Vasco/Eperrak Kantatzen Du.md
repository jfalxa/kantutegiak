---
id: ab-1897
izenburua: Eperrak Kantatzen Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001897.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001897.MID
youtube: null
---

Eperrak kantatzen du goizeko intzian:
Neskatxak, ez fidatu mutillen itzian:
"Para biar" dio,
"Para biar" dio.