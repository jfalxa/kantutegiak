---
id: ab-2161
izenburua: Nere Kristabak Badakizute (Versión 8)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002161.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002161.MID
youtube: null
---

Nere kristabak, badakizute
Zein dan Santa Ageda.
Lege zarreko useriyuan
Kantuan gatoz atera.
Zeruetako biotzak altxa
Graziak eskatutzeko,
Santa Agedak zer para zuan
Oyeri esplikatzeko.

Santa Ageda, dama eder bat,
Sizilian zan jayoa;
Kantabriako juezen mende
Igaro martirioa,
Egin nai ziotelarikan gaiztoak
Enganioa ezpazioten bere openioa.

Gobernadore Palermakuak
Arren berria bazuan
Santa Ageda ain zan ederra,
Bere andretzat nai zuan.
Gizon aundia izanagatik,
Ori logratu etzuan:
Lendabiziko nobedadiak
Argatik izan zituan.

Soldadu bati agindu zion
Plazan azotatutzeko;
Gero mudatu nai ez bazuan,
Bularrak ebakitzeko.
Sentenzi ona para zioten
Biotza alegratzeko.
Oyek guziak egin ta gero,
Kalabozoan sartzeko.

Krueldadezko golpiekin
Zutenian azotatu,
Soldadu batek agindu zion
Ia nai zuan mudatu.
Agedak berriz erantzun zion:
Jesus nai det nik amatu;
Obeagorik isango dala
Ez det sekulan pensatu.

Agedak gero erantzun zuan:
Ez naiz batere bildurtzen;
Zuen idolo jaingoiko faltso
Etzait batere gustatzen.
Krueldadezko golpe batekin
Bularrak oso kendurik,
Kalabozora eraman zuten
Sille batean arturik.

Andik urrengo egun-sentian,
Joan ziren bisitatzera,
Bizirik ikusterikan ere,
Etzuten bada espera.
Gure santia sendaturikan
Atera zuten kalera.
Milagro ori ikusi arren,
Beti zeduken kolera.

Besterik ezin pensatu eta,
Sorgiñe dala diote;
Fedegabeko jentil gaiztua
Ezaguerik ez dute.
Adios orain, jente noblia,
Kantatu degu bizitza;
Borondaterik baldin badago,
Limosna biltzen gabiltza.