---
id: ab-1737
izenburua: Barrundik Nago Nekean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001737.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001737.MID
youtube: null
---

Barrundik nago nekean,
Ez soseguz ta pakian:
Ara zer lan dadukian.
Andria falta; esango det nik
Nolakua nai nukean,
Ta ni nork nai nindukean,
Edo nork gusto lukean.
Tratatu nai det trukian.