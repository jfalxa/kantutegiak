---
id: ab-2446
izenburua: Puerto-Riko Islako
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002446.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002446.MID
youtube: null
---

Puerto-Riko islako Mayorazgo errien,
Mille zortzi eun da iruoei ta bien,
Mayotzeko illaren ogeigarrenien,
Esplikatzera nua euskera garbien
Zer sentimentu neukan nere barrenien.