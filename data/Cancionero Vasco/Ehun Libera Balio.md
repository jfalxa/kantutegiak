---
id: ab-2079
izenburua: Ehun Libera Balio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002079.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002079.MID
youtube: null
---

Bertso berriak doazi )
Zortzi edo bederatzi; )bis
Nahi dituanak ikasi
Ratarai...
Nahi dituanak ikasi
Urruñatik nahi dut hasi.

Urruñan Iriartean )
Behor bat da truk batian; )bis
Hala hobeki ustian,
Ratarai...
Hala hobeki ustian,
Estofa txarra etxian.

Ehun libera balio )
Bertzenaz ez da kario. )bis
Karga emaiten badio,
Ratarai...
Karga emaiten badio,
Laxter errain ""adio"".

Tratu bat zuten egina, )
Hamalau urte adina; )bis
Gero ondoko examina,
Ratarai...
Gero ondoko examina,
Hogei ta zazpi egina.

Hogei ta zazpi urtetan )
Zaldalia jan goizetan; )bis
Nahiz egunian bortzetan,
Ratarai...
Nahiz egunian bortzetan,
Tatxarik eztu ortzetan.

Behor txuria-gorria, )
Bidartetik ekarria, )bis
Lau zango-beso lodia,
Ratarai...
Lau zango-beso lodia,
Beliei bazka usu da.