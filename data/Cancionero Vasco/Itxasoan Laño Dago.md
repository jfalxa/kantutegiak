---
id: ab-2054
izenburua: Itxasoan Laño Dago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002054.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002054.MID
youtube: null
---

Itxasoan laño dago
Baionako barraraño.
Nik zu zaitut maitiago
Txoriyak beren umiak baño.