---
id: ab-2408
izenburua: Oi Zer Egin Ote Zait Niri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002408.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002408.MID
youtube: null
---

Oi, zer egin ote zait niri?
Bidian ibiltzen ahantzi.
Ez dut pausurik iten,
Non ez naizen erortzen.
Triste dut arras bihotza:
Indazue arno hotza.