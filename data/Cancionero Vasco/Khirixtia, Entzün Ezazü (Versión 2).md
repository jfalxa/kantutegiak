---
id: ab-1978
izenburua: Khirixtia, Entzün Ezazü (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001978.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001978.MID
youtube: null
---

Khirixtia, entzün ezazü,
Eta sinhetsi behar düzü,
Geure Jüjamentiala
Jesüs Jauna jinen dela.

Egün hura beno lehen
Markak dirateke heben,
Zuin beitira agertüren,
Bai eta gü izitüren.

Jüjamentü hura beno
Egün batez lehenago
Lürra da ikharatüren
Eta gü lotsaz izitüren.

Ekhia da ülhüntüren,
Argi güziak galdüren:
Ordian gützaz zer date?
Ikharatürik girate.

Izarrak ere berhala
Ülhüntüren dira hala,
Bai eta argizagia,
Gure gaiazko argia.

Phüntü hartan heiagora
Kabalek, büria gora,
Eginen die azkarki,
Eihartüren doloruski.

Haur txipiek sabeletik
Oihü eginen goratik:
- Jinkua, entzün gitzatzü,
Gaitzetik libra gitzatzü.

Odeiak dira jeikiren
Eta koleran jarriren,
Aize tenpestaz et'euriz
Orrokoz eta ihürtziriz.

Goiz batez hain preziski,
Ekhiaren jalkhitziareki,
Sü gorri bat da jeikiren,
Eta lürra xixpiltüren.

Palaziuak bertarik
Dirate destrüitürik,
Jauregi, etxiak, bordak,
Harriak eta arrastak.

Itxasua da erreren,
Hurak oro agortüren;
Zühaiñak eta bestiak
Direteke txixpiltiak.

Ordian bardin girate
Diferent denik eztate,
Baruak eta nubliak,
Mündü huntako erregiak.

Ezta ordian erregerik
Prinzerik, ez dükerik,
Izanen kontsideratürik,
Oro dirate peritürik.

Eztükie beste desirik,
Ez eta beste nahirik
Phüntü haietan besterik
Nula egoitez gorderik.

Eztüzü kuntrariorik
Ez har süspezionerik;
Lür güziak da erreren
Eta ezdeüs baratüren.

Trunpeta batek ozenki,
Sonatüren du trixteki,
Zoin oroz beita entzünen,
Ta hanbat date ozen.

Bertarik khorpizak oro
Jeikiren dira gero
Eta juanen berhala
Josafateko sorhüla.