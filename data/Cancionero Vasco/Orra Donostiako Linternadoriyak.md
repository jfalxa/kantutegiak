---
id: ab-2421
izenburua: Orra Donostiako Linternadoriyak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002421.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002421.MID
youtube: null
---

Orra Donostiako linternadoriyak,
Zer zirala uste zenduten Oyarzungo gaztiak?
Eskasagoak ez dira lengo zar tristiak.
Geyago balio du oyen kurajiak.