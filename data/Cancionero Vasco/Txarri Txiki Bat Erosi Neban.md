---
id: ab-2551
izenburua: Txarri Txiki Bat Erosi Neban
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002551.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002551.MID
youtube: null
---

Txarri txiki bat erosi neban
Bizi moduan sartzeko;
Iru pezeta pagatu neban
Jenero ona zalako.
Ate zulotik irten zirautan,
Ez det ikusi geiago.