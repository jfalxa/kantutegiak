---
id: ab-2356
izenburua: Nahi Balin Bagira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002356.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002356.MID
youtube: null
---

Nahi balin bagira hil eta salbatu,
Arte huntan behar gira gonbertitu.
Bekhatu egiteko kostuma kitatu,
Lehenago eginez urrikitan sartu,
Ez berriz lerratu,
Ahalaz beiratu;
Ongi konfesatu,
Maiz hiltzeaz orhoitu;
Denbora yuanez geroz ezta probetxu.

Sortuz geroz guziek zor dugu hiltzia,
Deusik ez da gure munduko bizia.
Kita dezagun beraz hemengo auzia,
Garbiki etzamina gure kontzientzia,
Bertze munduyan da
Gure yuztitzia,
Azken sententzia,
Zer oren tristia,
Eternitateko penaren luzia.

Pena hetaraz geroz ez da libratzerik,
Eternitateak ez du finitzerik.
Zonbat arima baden han kondenaturik,
Sekulan libratzeko ez esperantzarik
Hetaz orhoiturik
Gauden lotsaturik
Humiliaturik
Eta prestaturik,
Noiz nahi phartitzeko mundu huntarik.

Yainkoak igorriko du zerutik galdia.
Herioa du bere mandataria;
Hura yinez geroztik, yuan beharko gira,
Gure yuyatzaileak prest egonen dira.
Guk egin gaizkiak
Bai eta ongiak
Heyen arabera
Yuyaturen gira;
Memento lazgarriak orduyan dira.

Andre-dena Maria, Biryina garbia,
Zure laguntzaren beharretan gira,
Ofensatu baitugu gure gidaria,
Zure Seme Dibino Yainko Yaun handia.
Gaiten humilia.
Denboraz balia
Eternitatia
Orhoitzen bagira,
Zeruko portaleak idekiren dira.