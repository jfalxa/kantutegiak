---
id: ab-1677
izenburua: Elhorri Xuriaren Azpian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001677.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001677.MID
youtube: null
---

Elhorri xuriaren azpian
Anderia da lokhartu,
Arrosa bezin gorri,
Diamanta bezin ederrik.
Hiru kapitanek hor deramate
Xenaz amarraturik.