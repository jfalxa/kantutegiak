---
id: ab-2164
izenburua: Jose Alegratzen Naute
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002164.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002164.MID
youtube: null
---

Jose, alegratzen naute
Zure gozo ta ondasunak;
Jesusi diozkat eskerrak
Ta Mariari plazerak.