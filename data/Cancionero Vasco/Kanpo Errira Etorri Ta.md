---
id: ab-2196
izenburua: Kanpo Errira Etorri Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002196.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002196.MID
youtube: null
---

Kanpo errira etorri ta (kanpotik)
Neskatxa tentatzen?
Zu besterikan ez-ta
Orrela ibiltzen.
Gizon ziraun, arraiua!
Zer dezu pensatzen?
Gu sobratu ta ere
Deus etzaitzu galtzen.