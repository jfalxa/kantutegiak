---
id: ab-2887
izenburua: Muxurdinak Joaiten Dire
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002887.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002887.MID
youtube: null
---

Muxurdinak joaiten dire
Madalenara,
Madalenara,
Madalenara,
Sainduari eskatzera
Senar on bana,
Senar on bana,
Konbeni bada.
Ai, pikua, pikua, pikua,
Ai, pikua, piko merlatua.

Jan duzu pikua piko merlatua,
Merkatian saldua,
Ongi pagatua.
Ai, pikua, pikua...

Saindu harek...
Buruaikin, ez,
Buruaikin, ez,
Buruaikin, ez.
Denbora zenian...
Konbenitu ez,
Orai baterez,
Orai baterez.
Ai pikua, pikua, pikua,
Ai pikua, piko merlatua.

Errenterietako
Tanboriterua,
Tanboriterua,
Tanboriterua:
Bizkarra jonkortu ta,
Yotzen du soinua,
Yotzen du soinua,
Yotzen du soinua.
Ai pikua, pikua, pikua,
Ai pikua, piko merlatua.

Errenterietako
Fraile kaskamotza,
Fraile kaskamotza,
Fraile kaskamotza:
Bizkarra konkortu ta,
Ematen du meza,
Ematen du meza,
Ematen du meza.
Ai pikua, pikua, pikua,
Ai pikua, piko merlatua.