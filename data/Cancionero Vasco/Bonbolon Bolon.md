---
id: ab-2795
izenburua: Bonbolon Bolon
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002795.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002795.MID
youtube: null
---

Bonbolon bolon eragiyozu,
Laister aziko nazaitzu.
Laister azi ta ondo lagundu,
Mesede izango nazu.