---
id: ab-3297
izenburua: Sagarraren I(G)Arraren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003297.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003297.MID
youtube: null
---

Sagarraren i(g)arraren
Puntaren puntan
Txoriñoa zegoen kantari,
Ai! txiruli, ruli,
Ai! txiruli, ruli.
Nork soñatuko du soñu ori?"