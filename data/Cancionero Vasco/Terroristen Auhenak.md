---
id: ab-2532
izenburua: Terroristen Auhenak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002532.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002532.MID
youtube: null
---

Terroristen auhenak zoin ote ziren
Pedro eta Garrastegi famatzen zituzten.
Jende on preso hartzen, gaixtoa libratzen,
Erraiten zutelarik, gira libertitzen.