---
id: ab-1562
izenburua: Alaba Zuk Zer Nai Duzu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001562.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001562.MID
youtube: null
---

- Alaba,zuk zer nai duzu,
Zuk zer nai duzu:
Paseantea senarra,
Paseantea senarra?
- Ez, atta; ez, ama:
ezta ori nik nai durena.

- Alaba, zuk zer nai duzu,
Zuk zer nai duzu:
Barbera senarra,
Barbera senarra?
- Ez, atta; ez, ama:
Ezta ori nik nai durena.

- Alaba, zuk zer nai duzu,


- Alaba, zuk zer nai duzu,
Zuk zer nai duzu:
Eskribaba senarra,
Eskribaba senarra?
- Bai, atta; bai, ama:
Orixe da nik nai durena.

Eskribabak, eskuak zuri,
Eskuak zuri,
Eta urrin gozua,
Eta urrin gozua.
Bai, atta; bai, ama:
orixe da nik nai durena.