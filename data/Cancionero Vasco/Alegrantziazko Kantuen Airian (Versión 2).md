---
id: ab-2763
izenburua: Alegrantziazko Kantuen Airian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002763.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002763.MID
youtube: null
---

Alegrantziazko kantuen airian
Etxerat nintzenian,
Andria ohian,
Kolera buruan,
Latina kolkuan,
Frantzesez mintzo zen:
- Hordia galdurik, xikina,
Ni naun gaur? (nun hindagon)
- Xo, xo, emaztia: )
Moxkorra ni naun gaur! )bi.