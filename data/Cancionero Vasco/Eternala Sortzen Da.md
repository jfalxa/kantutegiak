---
id: ab-1925
izenburua: Eternala Sortzen Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001925.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001925.MID
youtube: null
---

Eternala sortzen da,
Egiten da gizon.
Heldu da erostera,
Gizonen salbatzera.
Kanta tzagun orok aintzinka,
Kanta tzagun Eguerri hauk.

Aingeruek airetan
Kantuz dute erraiten:
Jainko bat establian
Dela xoko batian
Edireiten haur baten pare,
Edireiten eta sortzen.

Goazen beraz guziak
Haren ikustera.
Erran tzagun eskerrak
Eta segi artzainak
Ohakora, oroko aintzinka
Ohakora, laudatzera.