---
id: ab-2124
izenburua: Itz Egin Zionian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002124.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002124.MID
youtube: null
---

Itz egin zionian Estellen Valdesi,
Gizon aundia egin zan Zumalakarregi.
Geroztik bazekiten, ji, ji, biek
Elkarren berri:
Argatik artu due orren beste erri. (dute)
Rapata, rapata, rau, rau, rau,
Gure umorea , jau, jau, jau,
Utzi aldebatera euskaldun jendea.