---
id: ab-1964
izenburua: Gazterik Hasi Zen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001964.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001964.MID
youtube: null
---

Gazterik hasi zen manatzen,
Arrazoina alhatzen,
Zikiro artalde baten
Mendian gobernatzen.
Oinhutsik zen ibiltzen,
Heien etxian zapatarik ez zen;
Hauzuek zituzten hornitzen,
Puska ondarrez bestitzen;
Galtza zaharren ziloetarik
Atorra-puska agertzen.
Orai baitut ezagutzen
Zorriak nola diren pizten.