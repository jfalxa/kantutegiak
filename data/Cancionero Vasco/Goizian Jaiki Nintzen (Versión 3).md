---
id: ab-1982
izenburua: Goizian Jaiki Nintzen (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001982.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001982.MID
youtube: null
---

Goizian jaiki nintzen
Argira bañolen,
Iturrira joan nintzen
Pegarra buruan.
Trai rai, rai...
Iturrira joan nintzen
Pegarra buruan.