---
id: ab-1528
izenburua: Aingeru Batek Mariari (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001528.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001528.MID
youtube: null
---

Aingeru batek Mariari
Dio graziaz bethea;
Jaungoikoaren Semeari
Emanen duzu sortzea,
Emanen duzu sortzea.