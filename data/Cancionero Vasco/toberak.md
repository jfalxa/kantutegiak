---
id: ab-2536
izenburua: Toberak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002536.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002536.MID
youtube: null
---

Zortzirak yo dute ta,
Beretzitan dago;
Gure laztan galanta,
Oi! non ote dago.