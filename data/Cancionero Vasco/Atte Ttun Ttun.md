---
id: ab-1693
izenburua: Atte Ttun Ttun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001693.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001693.MID
youtube: null
---

Atte ttun ttun,
Ama ttun ttun,
Alaba ori're ttunttune;
Guziek ttunttunek
Izatekotan, senarra
Bear luke ttunttune.
Ez, tturupututtun,
Bai, tturupututtun;
Allin agotak
ezkontzen ttun.