---
id: ab-2097
izenburua: Orentzago Gabian (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002097.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002097.MID
youtube: null
---

Orentzago gabian,
Eskail burutxo batian,
Otarra bete gaztaña,
Lukaikatxo bi gañian.