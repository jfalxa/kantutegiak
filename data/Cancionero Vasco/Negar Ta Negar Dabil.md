---
id: ab-2364
izenburua: Negar Ta Negar Dabil
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002364.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002364.MID
youtube: null
---

Negar ta negar dabil Marie gurie,
Katuek jan dauzalako bere okelie,
Katuek jan dauzalako bere okelie.
Negar ta negar dabil Marie gurie,
todos: Mari,
solo: Jaiki biarko dona gona mokor ori;
todos: Pepa,
solo: Ola ibilli baño, ezkondua bona.