---
id: ab-2546
izenburua: Txakur Gorri Txiki (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002546.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002546.MID
youtube: null
---

Txakur gorri txiki polit bat
Faltatu zait neri;
Arras eskutatu zan, ta ezpaidia ageri.
Ez diot maldiziorik bota nai iñori;
Artaz baliatu dana ongi bizi bedi.