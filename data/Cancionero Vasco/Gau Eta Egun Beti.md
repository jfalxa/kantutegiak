---
id: ab-1954
izenburua: Gau Eta Egun Beti
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001954.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001954.MID
youtube: null
---

Gau eta egun beti emen nabil
Etxi ontara puskatzen,
Desengañua izango ote dan
Bildurra ditut gastatzen.
Martin Zarrari alaba baizik
Beiñere ez diot eskatzen.