---
id: ab-2836
izenburua: Larrañetako Ardiak Dantzan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002836.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002836.MID
youtube: null
---

Larrañetako ardiak
Dantzan ebiltzan orduan,
Larrañetako ardiak
Bere dantzan ebiltzan orduan,
Salbadoria jaio dalako
Belengo etartian,
Da bart Belenen, da bart Belenen
Jaio da Jesus Nazaren.