---
id: ab-2111
izenburua: Bonbolon Bolon Bolon
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002111.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002111.MID
youtube: null
---

Bonbolon, bolon, bolon, bolua,
Urak darama zorrua.
Garia bada, betor onera;
Artua bada, bijua. Bo! bo!

Nere kuttuna, laztan maitia,
Ona zuretzat gozua;
Zopatxo mami saldan bustita,
Eztia ere badauka. Bo! bo.