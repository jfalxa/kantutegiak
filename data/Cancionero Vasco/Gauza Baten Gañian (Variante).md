---
id: ab-1961
izenburua: Gauza Baten Gañian (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001961.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001961.MID
youtube: null
---

Gauza baten gañian
Nua itz egitera:
Nola bear gendukian
Joan konfesatzera.
Esan guztiak ziero
Konfesore jaunari;
Ez dira ikaratuko,
Izan arren ugari.