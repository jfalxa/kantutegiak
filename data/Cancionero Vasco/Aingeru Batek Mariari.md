---
id: ab-1524
izenburua: Aingeru Batek Mariari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001524.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001524.MID
youtube: null
---

Aingeru batek Mariari
Dio:""graziaz bethea;
Jaungoikoaren Semeari
Emanen duzu sortzea"".

Jainkoaren nahi saindua
Ni baithan dadin egina;
Izan nadila amatua,
Bainan geldituz birjiña.

Orduan Berbo dibinoa
Gorphutz batez da bestitzen.
Oi, ontasun egiazkoa
Jauna gurekin egoiten!

Othoitz zazu zure Semea,
Gure ama amultsua:
Izan dezagun fagorea
Ardiesteko zerua.