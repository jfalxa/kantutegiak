---
id: ab-2173
izenburua: Joxe Mari Mara Mari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002173.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002173.MID
youtube: null
---

Joxe Mari, mara mari,
Ipur zikin zapatari.
Non duk andria?
Kutxan gordia.
An ezbada, sun gordia.