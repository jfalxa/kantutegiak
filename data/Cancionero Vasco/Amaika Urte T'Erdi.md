---
id: ab-1594
izenburua: Amaika Urte T'Erdi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001594.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001594.MID
youtube: null
---

Amaika urte t'erdi
Pasarik soldadu,
Naparreri eria
Artu bista galdu.