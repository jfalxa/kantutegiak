---
id: ab-1838
izenburua: Dendari Bat Bada
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001838.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001838.MID
youtube: null
---

Dendari bat bada gure auzoan;
Elengatzen baita ofiziuan.
Bere adresaz,
Bere orratzaz
Irabazten du:
Badu diru,
Badu linja,
Aberatsa da.
Senhar on bat baizik
Deus ez du falta.