---
id: ab-2123
izenburua: Isabel Bizitu Zan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002123.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002123.MID
youtube: null
---

Isabel bizitu zan mendian kabanan
Bere senarrarekin jo dute montaña, bisitatzera,
Maria jun ze zayon konsolatzera.