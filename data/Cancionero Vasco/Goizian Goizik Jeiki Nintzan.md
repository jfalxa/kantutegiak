---
id: ab-1994
izenburua: Goizian Goizik Jeiki Nintzan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001994.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001994.MID
youtube: null
---

Goizian goizik jeiki nintzan,
Ta erretiratu berandu;
Ardo guziak aitu dira ta
Norbaitek edan du.