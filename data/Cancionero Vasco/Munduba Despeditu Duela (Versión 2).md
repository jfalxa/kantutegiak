---
id: ab-2243
izenburua: Munduba Despeditu Duela (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002243.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002243.MID
youtube: null
---

Munduba despeditu duela,
Ez da orainik Kredo bi,
Desditxatua yoan zitzaion
Atalondora Pedrori,
Umildadean, umildadean,
Sartuko ote zen atian.