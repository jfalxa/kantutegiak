---
id: ab-2101
izenburua: Debozionez Esango Degu (Versión 8)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002101.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002101.MID
youtube: null
---

Debozionez esango degu
Lenengo nosteruoa (sic)
Ia gaizazu zerura beti
Birjiña biotzekua.