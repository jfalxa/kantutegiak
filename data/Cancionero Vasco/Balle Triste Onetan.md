---
id: ab-1726
izenburua: Balle Triste Onetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001726.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001726.MID
youtube: null
---

Balle triste onetan zer daigun gertatu?
Gure Salbadoria zuen preso artu. (zuten)
Ostegunian,
Gugatik sartu zuen Anasen etxian. (zuten.