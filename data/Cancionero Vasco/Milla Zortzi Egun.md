---
id: ab-2330
izenburua: Milla Zortzi Egun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002330.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002330.MID
youtube: null
---

Milla zortzi egun irurogei ta
Bosgarren urtian gera.
Esplikatzera abiatu naiz
Familiaren galera.
Andre ederra ona zalako,
Ekarri nuben aldera:
Bide berriak egin ta dabil
Ganbaratikan kalera.