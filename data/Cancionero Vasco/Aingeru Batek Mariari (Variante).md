---
id: ab-2838
izenburua: Aingeru Batek Mariari (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002838.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002838.MID
youtube: null
---

Aingeru batek Mariari
Dio:""graziaz bethea;
Jaungoikoaren Semeari
Emanen duzu sortzea"".