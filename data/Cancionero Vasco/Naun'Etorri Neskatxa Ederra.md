---
id: ab-2360
izenburua: Naun'Etorri Neskatxa Ederra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002360.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002360.MID
youtube: null
---

Naun'etorri, neskatxa ederra,
Neure baratzera promenatzera?
Bilduko tinagu ensaladeriak,
Artixota eta piper ferdiak.
Biba arno eta amodio,
Gauak eta egunak direino.