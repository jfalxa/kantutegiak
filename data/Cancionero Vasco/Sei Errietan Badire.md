---
id: ab-2479
izenburua: Sei Errietan Badire
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002479.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002479.MID
youtube: null
---

Sei errietan badire
Iru-lau abade:
Oyek egon leitezke
Sekulan il gabe.