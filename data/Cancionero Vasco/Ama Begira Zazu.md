---
id: ab-3185
izenburua: Ama Begira Zazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003185.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003185.MID
youtube: null
---

Ama, begira zazu leiotik plazara,
Ni bezelakorikan plazan bai ote dan.
Lai lai lai, tralara lara lai,
Lai lai lai, tralara lara lai.