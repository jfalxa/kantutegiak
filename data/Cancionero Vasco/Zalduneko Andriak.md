---
id: ab-2617
izenburua: Zalduneko Andriak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002617.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002617.MID
youtube: null
---

Zalduneko andriak neri mandatua
Bere illoba zaukala nitaz trazatua:
Ollo bat ikarri zidan ederrentakua;
Arekin egin genduen geren kontratua.