---
id: ab-1807
izenburua: Bolon Bat Eta Bolon Bi (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001807.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001807.MID
youtube: null
---

Bolon bat eta bolon bi,
Bolon putzura erori;
Erori bazen, erori,
Egin zituan trago bi.