---
id: ab-1674
izenburua: Arrigola Marrigola
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001674.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001674.MID
youtube: null
---

Arrigola, marrigola, kin, kuan, kin.
Porta zela, porta min.
Segere, megere, kirun, karun, pek.