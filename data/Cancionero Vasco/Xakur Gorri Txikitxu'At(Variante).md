---
id: ab-2548
izenburua: Xakur Gorri Txikitxu'At(Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002548.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002548.MID
youtube: null
---

Xakur gorri txikitxu'at
Faltatu zait neri;
Arras izkutatu da
Ez baita ageri.
Ez diot maldiziorik
Bota nai iñori;
Artaz baliatu dana
Ondo bizi bedi.