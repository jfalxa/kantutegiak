---
id: ab-1805
izenburua: Bollon Bat Eta Bollon Bi (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001805.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001805.MID
youtube: null
---

Bollon bat eta bollon bi,
Bolon putzura erori.
Erori bada, erori,
Ezta geiago ageri.