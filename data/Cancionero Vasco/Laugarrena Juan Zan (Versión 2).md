---
id: ab-2100
izenburua: Laugarrena Juan Zan (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002100.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002100.MID
youtube: null
---

Laugarrena juan zan
Guzien atzian.
Jesus, esaten zuan,
Kuerda paratzean.
Salbe bat errezatzeko
Artaz oroizian,
Ala agindu zuan,
Arrek eriotzian,
Pena aundi batekin
Bere biotzean.