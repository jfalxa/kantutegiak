---
id: ab-1499
izenburua: Adizan Gabriela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001499.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001499.MID
youtube: null
---

- Adizan, Gabriela,
Zenbat urte ditun?
- Ogei ta amasei.
- Nik geia baditun.
Baldin oraingo txandan
Ezkontzen ezpagaitun,
Geren bizi guziko
Neskazartu gaitun.

- Nereak oi ta amalau
Ditun onezkero,
Baña neskazartzerik
Ez diñat espero;
Mutillak ezkontzeko
Ain zebiltzen bero,
Beintzat nik badizkiñat
Zazpi zapatero.

- Nik ere badiskiñat
Lau txikiatzalle
Zazpi barkillero ta
Bi piper-saltzalle,
Eta gañera iru
Kale-garbitzalle,
Bi Kabo primero ta
Lau arpa-jotzalle.

- Nere bizi moduaz
Nion ernegatzen,
Mutil guztokorikan
Ez diñat billatzen.
Gorputza asi zaidan
Pixka bat torpetzen,
Neskazarra naizena
Zidaten igartzen.

- Soñekorik onena
Egin diñat jantzi,
Gañera eskutikan
Sonbrilla ta guzi.
Oingo mutil gaztiak
Ainbeste malezi,
Ni ikusi orduko
Ziazten igesi.

- Oso itxusia den
Neskazar izena;
Aditu utsarekin
Artzen diñat pena.
Oraiñ edukirikan
Sasoirik onena,
Au da lastimagarri
Gu biok gaudena.