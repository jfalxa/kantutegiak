---
id: ab-2181
izenburua: Juan Nintzenian ... (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002181.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002181.MID
youtube: null
---

Juan nintzenian, juan nintzenian
Monte zala mendia,
Ikusi nuan, ikusi nuan
Pájaro zala txoria,
Tiratu nion, tiratu nion
Piedra zala arria,
Botatzen zuan, botatzen zuan
Odol sangre gorria.
Txori zistularia.