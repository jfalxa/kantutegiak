---
id: ab-3049
izenburua: Iru Xito Izan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003049.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003049.MID
youtube: null
---

Iru xito izan eta lau galdu,
Nere xituaren ama zerk jan du?
Xituaren ama olloa,
Axeriak kendu dio lepoa.