---
id: ab-2342
izenburua: Misionen Etortzeaz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002342.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002342.MID
youtube: null
---

Misionen etortzeaz Jauna dela laudatu.
Helas, hauen eskasiaz hainitz dira damnatu.
Kristau leialak, zatozte laster misionerat;
Jainkoak deitzen zaituzte harekin baketzerat.