---
id: ab-3115
izenburua: Nere Senarra Tontoxko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003115.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003115.MID
youtube: null
---

Nere senarra tontoxko,
Artoyale aundixko:
Zazpi talo biar ditu
Otorduan yateko;
Zortzigarrena kolkoko,
Gosetzen denian yateko.

Ene andria plazan dantzan
Eper errez aserik.
Eta senar gaizua supazterrian
Belaun kazkuak errerik;
Belaun kazkuak errerik eta
Artoz ere goserik.