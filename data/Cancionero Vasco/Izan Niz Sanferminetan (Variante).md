---
id: ab-2727
izenburua: Izan Niz Sanferminetan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002727.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002727.MID
youtube: null
---

Izan niz Sanferminetan
Iruñako ferietan.
Ehun zaldi arribatu
Andaluziatik tropan.
Merkatu eder bazitzautan
Zaudelarik bi lerrotan.