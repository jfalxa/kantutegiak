---
id: ab-1811
izenburua: Bolonbolonaketara
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001811.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001811.MID
youtube: null
---

Bolonbolonaketara,
Goazen Allira festara.
Juan den urtian gosiak giñen,
Ta aurtengo urtian zertara.