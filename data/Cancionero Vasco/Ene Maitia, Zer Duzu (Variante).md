---
id: ab-2320
izenburua: Ene Maitia, Zer Duzu? (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002320.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002320.MID
youtube: null
---

- Ene maitia, zer duzu?
Zerk hola xangrintzen zaitu?
Ez du denbora luzia
Zirela penetan sartu...
Plazeraren ondotik
Desplazerak tuzu.