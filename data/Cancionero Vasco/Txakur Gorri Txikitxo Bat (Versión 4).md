---
id: ab-2547
izenburua: Txakur Gorri Txikitxo Bat (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002547.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002547.MID
youtube: null
---

Txakur gorri txikitxo bat
Faltatu zait neri;
Arras izkutatu zan ta
Ez baita ageri.
Eztiot maldiziorik
Botatu iñori;
Artaz baliatu dena
Ongi bizi bedi.

Nere txakur gorri txiki
Txalo ta klaroa
Azari kolerekoa
Eta ilajez arroa.
Orrekin ibili denak
Ona du garboa
Itxu aurreko balebil
Ai zer pajaroa.

Manterolako errota
Elurditik beira
Usalegian askotan
Alegatzen gera.
Nere txakurrak au zuen
Bere bukaera
Gizona agudoa beti
Nu'nai ziñale da

Manterolako errotari
Gizon abila da
Nere txakur txiki ori
Ark berak ill da.
Berearekin ezkian
Or ibili da
Zerbait bildu naye badu
Ark familiera.

Presente para digue
Beren azienda
Ez giñun erreparatu
Ez ginikien da.
Nor zein dan esagutzia
Beti atsegin da
Azioetan presona
Nor dan jakiten da.

Ayeko erri noblea
Pasutu orduko
Guziek eman ezkero
Franko du bilduko.
Gaztaña eta arrauetze
Txanpon eta kuarto
Aurtengo bastimentue
Badanka zierto.

Amorio mentian
Dizut barkatuko
Aurrera ez dakigu nola
Zeran portatuko.
Beste orrenbeste gurekin
Baliz gertatuko
Zu eta ni ez ginake
Ongi dantzatuko.

Motibo gabe burlarik
Iñori ez egin
Ortatik aberastuko
Ezin zera berdin.
Gogoan daukadanaz
Klaro mintza nadin
Eztu erres asentatzen
Zenbait buru arin.

Amalau bersorekin
Nai dut despeditu
Gizonari eman gabe
Mila deskreditu.
Martin, obe dezu
Kontuz entenditu
Nork beren aziendak
Estimatzen ditu.

Dibersio egoki bat
Orango aldien
Paratzeko asmoa det
Dakien neurrien.
Zer pasatu den
Ayeko errien
Explikatzera noa
Euskera garbien.

Gorrotorik ez daukat
Jaunak, iñorgana
Bai solo nere txakurra
Galdutako pena.
Naiz izan bedi munduan
Danik abilena,
Nekez aberastuko da
Ola dabilena.

Egori mintza nadin
Didate agindu
Ojala modu onean
Esango bagendu.
Zorionako suerte
Onenbat egin du
Notiziak dituanez
Ez du gutxi bildu

Txakur gorri ori...