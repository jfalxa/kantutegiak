---
id: ab-1486
izenburua: Abur, Abur, Juantxo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001486.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001486.MID
youtube: null
---

Abur, abur, Juantxo,
Zer modu, zer modu?
- Pobre ta alegere,
Zer esain degu!
Besta interesarik
Munduan ez degu.
- Soka bat ekarzu,
Bear zaitut lotu;
Bañan ez izitu,
agintzen digutena
egin bear degu.