---
id: ab-1503
izenburua: Agur, Adixkidia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001503.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001503.MID
youtube: null
---

Agur, adixkidia, Jainkuak egun on.
Zer berri den errazu, othoi, Xuberon.
Ezagutzen duzia Barkoxen Etxehun?
Halako koblaririk ez omen da nehun.
Bisitaz juan nindaike, espalitz hain urrun.

Jauna, ezagutzen dut Etxehun Barkoxen,
Egun orotan nuzu ulhanik igaiten.
Gazte ezin egonez, ari da zahartzen.
Laurogei urte ditu munduan pasatzen;
Orai ezta bertsuka anitz okupatzen.

Jauna, behar daitazu gauza bat onetsi,
bainan etzaizu behar zatikan ahantzi
Hortaz egiten daizut gomendiorik aski:
Konprendituko duzu itz laburrez naski.
Ene partez errozu milaka goraintzi.

Zure komisionia nahi daizut egin,
Komenitzen zitazu mila plazerekin.
Egon nahia zira arren jaun harekin,
Hona Etxehun bera presente zurekin.
Bainan nik eztiroket zu nor ziren jakin?

Jakin ere behauzu dudarikan gabe,
Zeren egin daitazun errespetuz galde.
Entzutia baduzu nitaz lehen ere.
Nor naizen erraitea nik eztut herabe:
Lapurdin deitzen naute Jan Batist Otxalde.

Bihotzaren erditik egin daizut irri,
Zureganik entzunik parabola hori.
Konbenitzen bazaio jaun zerukuari,
Eman behar ditugu zenbait bertso berri
eta beher denboran eskuak elkarri.