---
id: ab-1837
izenburua: Deklaratzera Nua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001837.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001837.MID
youtube: null
---

Deklaratzera nua nere doloria,
Ezkontza galdutako baten ondoria.
Tristura sendatzeko daukat umoria:
Zabaldu al baneza barrengo loria.