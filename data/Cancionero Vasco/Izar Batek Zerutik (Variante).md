---
id: ab-2589
izenburua: Izar Batek Zerutik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002589.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002589.MID
youtube: null
---

Izar batek zerutik,
Klaritatez beterik,
Gauaz ere argitzen du
Bertze ororen gainetik.
Dudatzen dut duenez
Mundu huntan parerik. (bis)

Izar haren begia,
Ain da xarmegarria:
Koloriak xuri-gorri,
Perfektionez betia.
Eria ere senda liro
Aren begitartia. (bis)

Uxo zuria, errazu,
Norat yuaiten ziren zu.
Espaiñiako bortuak oro
Elurrez betiak dituzu:
Gaurko zure ostatu
Gure etxian baduzu. (bis)

Ez nau izitzen elurrak
Ez eta ere gau ilunak.
Maitiagatik pasa nintzazke
Gauak eta egunak,
Gauak eta egunak
Desertu eta oihanak. (bis.