---
id: ab-2696
izenburua: Txingulin Bringulin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002696.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002696.MID
youtube: null
---

Txingulin bringulin
Txingulin bringulin
Bringulin txingulin atsua.
Lengo gabian sukila atian
Ezauken mozkor gaixtuak.