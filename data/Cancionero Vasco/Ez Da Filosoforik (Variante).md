---
id: ab-1650
izenburua: Ez Da Filosoforik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001650.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001650.MID
youtube: null
---

2ª estrofa de "Arnuaren bentaja".