---
id: ab-1530
izenburua: Aita Ekarzazu, Otoi Barbera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001530.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001530.MID
youtube: null
---

Aita, ekarzazu, otoi, barbera:
Uste dut sabelian zerbait badela.
Sabeletik nagola eri arrigarri;
Denbora gutxi dela sentitu dutela ori. ji-ji,
Agudo ekarzazu barbera gazte ori.

- Agur, agur, barbera gaztia!
Nere alabatxo orrek du nobedaria:
Sabeletik dagola eri arrigarri;
Denbora gutxi dela sentitu duela ori. ji-ji,
Erremedio onen bat aplika ziozu ari.

- Agur, agur, damatxo gaztia!
Tristerik egon gabe, konsola zaitia.
Obia izango da klaro erraitia:
Maitia, zu zarela denbora betia, ja-ja,
Ez zara sendatuko libratu artia.

- Ez dut botikan senti erremediorik
Zure barrengo gaitza senda lezakenik;
Zuk zinduenian miñ ori sentitu,
Pañuelutxu arekin zinduen ertsitu ja-ja
Alere zure gaitza ez zaizu sendatu.