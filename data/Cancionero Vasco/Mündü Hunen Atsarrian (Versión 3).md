---
id: ab-2348
izenburua: Mündü Hunen Atsarrian (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002348.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002348.MID
youtube: null
---

Mündü hunen atsarrian
Jüjamendü dibinoa
Izan zen ordenatü.
Mündü hunek behar ziala
Erre hilaña bezala,
Eta hala finitü.

Berri amiragarri batekin
Nurk gure entresagatik
Behar mündü güzia?
Labürski bildüren gira
Josephateko ordokila:
Bidian da mezia.

Ez jüstorik, ez gaxtorik
Eztate lotsagaberik
Tribünal aitzinian;
Jüjiaren aitzinian,
Armatürik justizia
Azotia eskian.

Itsasuan hil dienak,
Arrañek jan dütienak,
Edo saiek lürrian:
Nahi badadien ere
Aidatürik hilañak ere,
Pistüren instantian.

Zelietarik izarrak
Flandeü eder eta klarak
Dirade eroriko.
Ordüko jenten desira
Lürra dakien erdira
Han barnen gordatzeko.

Tronpet'hotsez aingüriak
Lür güziko hil-herriak
Dütie ingüratüko:
Hiler, jaiki ditiela:
Egün handia heltü dela
Kontü errendatzeko.