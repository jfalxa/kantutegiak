---
id: ab-2244
izenburua: Kuadrilla Batzen Gara
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002244.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002244.MID
youtube: null
---

Kuadrilla batzen gera
Zubi zarran azpira.
Guazen itsas bistara,
Kamino berrira.
Mendebal-norteko aize
Ipar pikarua,
Areik ematen digu
Sarri endrerua.