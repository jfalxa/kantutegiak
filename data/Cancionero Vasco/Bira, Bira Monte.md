---
id: ab-3390
izenburua: Bira, Bira Monte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003390.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003390.MID
youtube: null
---

Bira, bira monte,
Soso alkate,
Irugarren geruan alkate.
Bilbo'n errejidore.
Geure ausoko Mikelatxu bere,
Atzera bira leiteke.