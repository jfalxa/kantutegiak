---
id: ab-2604
izenburua: Xarmagarri Bat Badit (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002604.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002604.MID
youtube: null
---

Xarmagarri bat badit maite bihotzeti;
Amodiotan gira biak algarreki.
Haren aire xarmantaz agrada niz bethi:
Parerik badiala etzait üdüri. (ezpeitzait)

- Zertako mintzo zira maite naizüla?
Korten egiten zaude zü beste orori.
Enezaxüla tronpa, mintza zite klarki;
Esperantza faltsürik ez eman nihori.

- Xori parro gorriak ejerki khantatzen. (papo)
Gaiazko alojia kanpuan txerkatzen.
Gisa berian nüzü ni edireiten,
Maitiak ezpadereit bortha idekitzen.

- Orai zireia jiten gaiherdi onduan?
Iratzarririk nintzan eta zü goguan.
Entzüten nianian zure botza karrikan,
Ohetik jalki eta jartzen nintzan leihuan.

- Oi ene bihotzeko lili maitia,
Ene botza entzünik leihuan zaudia?
Aspaldin enereizün ikhusi begia:
Barnerat sar nadin indazüt eskia.

- Bortürik gorenetan erortzen elhürra:
Tronpatüren naizüla badizüt beldürra.
Hargatik nahi züntüket ikhusi ardüra,
Eginen badüt ere ene malürra.

- Lümarik ejerrena paboñak büztanian:
Maitia, etzüntüdan ikusi aspaldian.
Gaiaz eta egünaz bazüntüt eneki,
Ni enainte debeia sekülan zureki.

- Ai, ai, ai, ai, ai, ai, hau doloria!
Bi maite üken eta etzakin zoin haita:
Batak dizü txapela, besteak buneta;
Txapeldütto hori düzü, oi, ene bihotza.

Amo de corazón a una mujer encantadora; mutuamente nos queremos.
Me encanta su garbo; no creo haya otra que le iguale.

- ¿Cómo dices que me amas, si andas cortejando a todas? No me
engañes; habla con sinceridad; a nadie se ha de dar falsas
esperanzas.

- El petirrojo canta bien. De noche busca fuera donde alojarse.
Así ando yo, si mi amada no me abre la puerta.

- ¿Son horas estas de venir, a media noche? Estaba despierta,
pensando en ti. Al oír tu voz en la calle, me levanté y salí a
la ventana.

- ¡Oh, prenda de mi corazón! ¿Conque al oír mi voz has salido a
la ventana? Tiempo ha que no te veía; dame la mano para que
entre.

- En las montañas nieva; temo que me engañes. No obstante,
quisiera verte a menudo, aunque fuera para mi mal.

- El pavo, en la cola tiene la pluma más hermosa: amada mía,
tiempo ha que no te veía. Aunque estuviera día y noche a tu lado,
no me había de hartar.

Ai, ai, ai, ai, ¡Qué pena! tener dos amantes, y no saber cuál
escoger: el uno lleva boina; el otro, sombrero. Ese pequeño, el
de la boina, ese es mi corazón.
"LETRA
"1. Xarmagarri bat badit maite bihotzeti;
Amodiotan gira biak algarreki.
Haren aire xarmantaz agrada niz bethi:
Parerik badiala etzait üdüri. (ezpeitzait)

- Zertako mintzo zira maite naizüla?
Korten egiten zaude zü beste orori.
Enezaxüla tronpa, mintza zite klarki;
Esperantza faltsürik ez eman nihori.

- Xori parro gorriak ejerki khantatzen. (papo)
Gaiazko alojia kanpuan txerkatzen.
Gisa berian nüzü ni edireiten,
Maitiak ezpadereit bortha idekitzen.

- Orai zireia jiten gaiherdi onduan?
Iratzarririk nintzan eta zü goguan.
Entzüten nianian zure botza karrikan,
Ohetik jalki eta jartzen nintzan leihuan.

- Oi ene bihotzeko lili maitia,
Ene botza entzünik leihuan zaudia?
Aspaldin enereizün ikhusi begia:
Barnerat sar nadin indazüt eskia.

- Bortürik gorenetan erortzen elhürra:
Tronpatüren naizüla badizüt beldürra.
Hargatik nahi züntüket ikhusi ardüra,
Eginen badüt ere ene malürra.

- Lümarik ejerrena paboñak büztanian:
Maitia, etzüntüdan ikusi aspaldian.
Gaiaz eta egünaz bazüntüt eneki,
Ni enainte debeia sekülan zureki.

- Ai, ai, ai, ai, ai, ai, hau doloria!
Bi maite üken eta etzakin zoin haita:
Batak dizü txapela, besteak buneta;
Txapeldütto hori düzü, oi, ene bihotza.