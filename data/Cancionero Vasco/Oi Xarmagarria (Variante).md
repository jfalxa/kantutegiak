---
id: ab-2742
izenburua: Oi Xarmagarria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002742.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002742.MID
youtube: null
---

Oi xarmagarria, othoi, entzün nezazü,
Leihula jin eta hunat so egidazü.
Hürrüntik jinik nüzü,
Gaia ülhün duzü,
Ebia ari düzü:
Krüdel'ezpazira, pietate har zazü,
Bortha idok edazü.