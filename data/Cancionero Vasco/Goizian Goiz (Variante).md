---
id: ab-1991
izenburua: Goizian Goiz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001991.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001991.MID
youtube: null
---

Goizian goiz jaiki ninduzun
Oi, espos nintzen goizian.
Bai eta ere sedaz beztitu,
Iguzkia jali aintzinian.
Espos berri egin ninduzun
Oi, eguerdi gaineko,
Bai eta ere alargun gazte
Iguzkia sartu deneko.