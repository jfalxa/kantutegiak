---
id: ab-1765
izenburua: Bere Jatorriz Duen Fatxadan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001765.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001765.MID
youtube: null
---

Bere jatorriz duen fatxadan finian,
Bidia pasatzen du pausu arinian.
Lurrik ez du zanpatzen xoriak ainian; (aidian)
Xutik juango litzake uraren gainian.