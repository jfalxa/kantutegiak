---
id: ab-2256
izenburua: Lertxunek Lerro Lerro
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002256.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002256.MID
youtube: null
---

Lertxunek lerro lerro
Pasatzen tuzte mendiak oro.
Galtzen dutenean gida,
Yoaiten dira bana bira.
Andre gaztiak, ez zaiteztela
Oraiko jaun gaztien gezurretan fida.