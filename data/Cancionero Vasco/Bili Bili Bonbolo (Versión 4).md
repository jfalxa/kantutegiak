---
id: ab-1793
izenburua: Bili Bili Bonbolo (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001793.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001793.MID
youtube: null
---

Bili bili bonbolo, sendan do.
Akerra Prantzian balego,
Idiak kanta, akerrak dantza,
Auntzak danboliña yo.

Gure aur onen aur ona!
Gan dela Baionan barrena:
Bayonan dirutan baluken, baño
Ez onelako aur ona.