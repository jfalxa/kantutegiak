---
id: ab-1945
izenburua: Ezpeletako Katalin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001945.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001945.MID
youtube: null
---

Ezpeletako Katalin
Ederra ez eta badakin
Damorik ez den pasatu bear
Ere denboran nerekin.
Eta triunfaliron
Andre on gutxi
Eraliron eraliron,
Gutxi iruliron
Eta triunfaliron,
Andreak karoti aliron.