---
id: ab-1840
izenburua: Din, Dan, Boleran (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001840.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001840.MID
youtube: null
---

Din, dan, boleran,
Elizango gauzetan.
Din, dan, boleran,
Gure aurra koleran.
Dingindin, dangandan
Dingindin, dan,
Dingindin, dangandan
Dingindin, dan.
Din, da, boleran,
Gure aurra koleran.