---
id: ab-1529
izenburua: Aisa Pensatu Nuen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001529.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001529.MID
youtube: null
---

Aisa pensatu nuen,
Negorrek kusi ta.
Artua yaten zuen
Zartañan busti ta.
Arat xate,
Unat xate:
Berriz edan gabe
Yuanen al zate?

Martin Ximon de Larralde,
Beti adelante;
Ardua edaten du
Botillatik gargante. (Botillaik)
Arat xate,
Unat xate:
Bularra zaharrian,
Lukainka, sar zaizte.

Artua yaten dezu
Zartañan busti ta:
Aisa pensatzen dezu
Neronek kusi ta.
Arat xate,
Unat xate:
Drago bat eginda
Oian sar zaite.