---
id: ab-1832
izenburua: Dama Gazten Ondotik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001832.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001832.MID
youtube: null
---

Dama gazten ondotik
Iru galai gazte
Ibiltzen ziradelarik,
Ez nuen uste.
Oiek ibiliagatik
Elkarrekin naste,
Batek biarko zaitu
Aldian emazte.