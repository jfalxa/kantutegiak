---
id: ab-3398
izenburua: Kristo Guziek Dezagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003398.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003398.MID
youtube: null
---

Kristo guziek dezagun )
Egun adora. )bis
Hauche da izarra
Agertzen dena;
Berri bat dakarke
Den handiena,
Jesus maitearen
Sortzearena.

Kristo guziek dezagun )
Egun adora. )bis
Hirur erregeak
Ditu gidatzen;
Jesus haurra gana
Dire hedatzen,
Presentekin dute
Han adoratzen.