---
id: ab-1757
izenburua: Bentara Noa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001757.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001757.MID
youtube: null
---

Bentara noa, bentatik nator,
Bentan da nere gogoa:
Ango arrosa klabeliñetan
Artu dut amorioa.