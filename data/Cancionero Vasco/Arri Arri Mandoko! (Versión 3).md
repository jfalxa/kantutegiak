---
id: ab-1672
izenburua: Arri Arri Mandoko! (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001672.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001672.MID
youtube: null
---

Arri, arri, mandoko!
Biar Iruñarako,
Etzi Sangozarako.
Andik ze'ekarriko
Erraztun bar eriko,
Boneta bat buruko,
Zapatiko zangoko.