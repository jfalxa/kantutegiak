---
id: ab-1581
izenburua: Jesus Onaren Ama Maitea (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001581.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001581.MID
youtube: null
---

Misterios gloriosos:

Jesus onaren...

Ekusirikan, Ama maitea,
Illen artetik pizturik,
Jerusalenen gloriosoa
Semea resuzitaturik.
Nork esango du zer alegria
Orduan artu zenduan!
Lagun gaitzatzu orain ta beti
Eriotzako orduan.

Berrogei egun igaro eta
Zerura igo zanean,
Gloriosoa eseri zuen
Aita bereak aldean.
Nork esango du zer alegria


Espiritu Santua bialdu zion
Apostoluari etxera,
Doai aundiaz illuntasuna
Burutik ateratzera.
Nork esango du zer alegria


Jesus dulzeak zu ikusteagatik
Zeruan gustoz beterik,
Bazeramazten gorputz eta arima
Aingeruz inguraturik.
Nork esango du zer alegria


Trinidadean jarri ziñaden
Seme Jaunaren aldean,
Beti sekulan an egoteko
Beste guzien aurrean.
Nork esango du zer alegria