---
id: ab-2800
izenburua: Din, Dan, Paladan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002800.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002800.MID
youtube: null
---

Din, dan, paladan,
Paladako elizan.
Zazpi xahal ürrüxa,
Bederatzü bigantxa.