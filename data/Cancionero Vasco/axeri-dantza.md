---
id: ab-2984
izenburua: Axeri-Dantza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002984.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002984.MID
youtube: null
---

Axeri dantza, txibu txibulete.
Opilla jan da konkete.
Txibu, txibu, libu, libulete, te, te, te,
Txibu, libu, libu, libulete, te, te.