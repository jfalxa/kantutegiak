---
id: ab-2007
izenburua: Ni Hiltzen Naizenean (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002007.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002007.MID
youtube: null
---

Ni hiltzen naizenian,
La la la la la...
Ni hiltzen naizenian,
Ez ehortz elizan,
Ez ehortz elizan.

Bainan ehortz nazazue
La la la la la...
Bainan ehortz nazazue
Xai zola batian
Xai zola batian.

Burutz barrikarat,
La la la la la...
Burutz barrikarat
Ahoz duxularat,
Ahoz duxularat.

Duxula eroriz,
La la la la la...
Duxula eroriz
Baduket zer edan,
Baduket zer edan.