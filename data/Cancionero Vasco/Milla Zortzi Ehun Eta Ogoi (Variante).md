---
id: ab-2335
izenburua: Milla Zortzi Ehun Eta Ogoi (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002335.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002335.MID
youtube: null
---

Milla zortzi ehun eta ogoi eta hirurian,
Zerbitxuan gendauden Donostia hirian,
Liberilitateko bandera berrian.
Hartarik desertata, inozent-kerian,
Miserable erori etsaien erdian.