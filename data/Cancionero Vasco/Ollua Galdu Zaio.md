---
id: ab-3192
izenburua: Ollua Galdu Zaio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003192.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003192.MID
youtube: null
---

Ollua galdu zaio
Mari Muñeteri;
Señaliak baditu,
Agertu baledi.