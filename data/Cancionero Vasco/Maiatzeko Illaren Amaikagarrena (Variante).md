---
id: ab-2465
izenburua: Maiatzeko Illaren Amaikagarrena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002465.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002465.MID
youtube: null
---

Maiatzeko illaren amaikagarrena,
Lapurrek San Migelen sartu ziradena,
Baita lapurtu ere berak nai zutena;
Azkenik erlekie San Migelarena:
Orrek ematen digu biotzian pena.