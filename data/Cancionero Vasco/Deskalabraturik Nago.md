---
id: ab-1839
izenburua: Deskalabraturik Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001839.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001839.MID
youtube: null
---

Deskalabraturik nago munduko bizitzaz:
Kantuz esan biar dut zer moduz gabiltzan.
Dama eder batekin esposatu nintzan;
Geroztik egun on bat ez dezaket izan;
Neure begietara ona etorri zan!

Andre hori zabillan itxuran jarria,
Ederki jantzi eta liraña gerria;
Pausua delijente, kolore gorria;
Gezurraren gisako engañagarria,
Maiz busti beharra du bere eztarria.

Ardorik ere etzuen edan nahi lenago;
Ni engañatu arte zeguela nago.
Orain edan lezake ttantton bat balego...
Egunian arroba bat baño gehiago,
Aguarditia berriz ar'eta nahiago.

Untzi eta sukalderik ez digu garbitzen;
Atorraren xuritzen ez eta enpleatzen.
Tentazionea duenian ganaduen hiltzen,
Gauza deraukaño ez bada horditzen
Kaskoin horiek dira ...