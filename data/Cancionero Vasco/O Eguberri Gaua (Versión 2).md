---
id: ab-2400
izenburua: O Eguberri Gaua (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002400.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002400.MID
youtube: null
---

O Eguberri gaua, bozkariozko gaua!
Alegeratzen duzu, bihotzian kristaua.
Mundu guzia duzu zorionez betetzen,
Zeren zuk baitiozu Mesias dela sortzen.