---
id: ab-2064
izenburua: Adios, Izar Eder
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002064.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002064.MID
youtube: null
---

Adios, izar eder, adios, izarra!
Zu zera aingerua munduan bakarra.
Izar baten uztiak emaiten daut pena,
Zeren eta bai nuen munduan maitena.