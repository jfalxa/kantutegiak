---
id: ab-2267
izenburua: Logura Naiz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002267.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002267.MID
youtube: null
---

Logura naiz ta logale,
Argi-ollarrak jo gabe.
Bart Garmendiko ate ondoan
Zaldiak ziren asarre.