---
id: ab-2567
izenburua: Xorro Morro Xorrotan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002567.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002567.MID
youtube: null
---

Xorro morro xorrotan,
Gure ollarra ollotan;
Ez ollotan, ba ollotan.
Gure ollarra ollotan
Manex horren bihotzetikan
Miriak haztaparretan.
Maria dua erranez:
Ai, ai, ai, ai,
Enia zukan.