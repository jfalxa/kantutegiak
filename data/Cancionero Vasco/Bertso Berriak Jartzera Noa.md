---
id: ab-1768
izenburua: Bertso Berriak Jartzera Noa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001768.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001768.MID
youtube: null
---

Bertso berriak jartzera nua
Iñork nai badu ikasi;
Nere buruba sartu biar det
Orretarako lendabizi.
Mundu ontako pasadizua
Nai nuke adierazi;
Asko gabiltze arrokeriyan
Elizagandik igasi.
Geuren animaz oroit gaitezen,
Eztu galtzia merezi.