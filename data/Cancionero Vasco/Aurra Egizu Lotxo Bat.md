---
id: ab-1703
izenburua: Aurra Egizu Lotxo Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001703.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001703.MID
youtube: null
---

Aurra, egizu lotxo bat:
Emango dizut goxo bat,
Aitak bat eta amak bi,
Jaun zerukoak amabi. Lo.