---
id: ab-2039
izenburua: Iparriarren Bozian Ditut (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002039.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002039.MID
youtube: null
---

Iparriarren bozian ditut
Bertso berriak paratu;
Ipuzkoako erri noblian
Albanitzazke kantatu.
Asko ezkongai daude
Gustokorikan gabe
Ezin dutela billatu.
Projimoari erakustia
Ezta izanen bekatu.