---
id: ab-3360
izenburua: Mundakako Neskak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003360.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003360.MID
youtube: null
---

Mundakako neskak juaten dira
San Antoniora, San Antoniora,
Santuai eskatzera senar on bana,
Senar on bana, konbeni bada.
Santuak egiten du buruakin
Dar, dar:
Ya no es hora.