---
id: ab-1728
izenburua: Baratzeren Batian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001728.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001728.MID
youtube: null
---

Baratzeren batian omen da bilgura;
Denok egon gaitezin Jesusen eskuira.
Len Berak botariak al gera mundura.
Bekatu egiñaz beti bear du tristura.

Ezta nekazalerik, ez aundi-maundirik,
Eskapatuko denik munduan bizirik.
Ezta aberatsa bertzeîk lastimagarririk:
Goan bear baidute ondasunak utzîk.