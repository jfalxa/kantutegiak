---
id: ab-2201
izenburua: Kanta Kanta Dezagun (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002201.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002201.MID
youtube: null
---

Kanta, kanta dezagun entretenitzeko;
Kanta, kanta dezagun konsolatutzeko.
Au da kantu ona, kantarik onena.
Orain kanta dezagun albait oberena.
Orain Paternosterra bear degu kantatu,
Kantu onen autoria ongi esplikatu.