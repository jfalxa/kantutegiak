---
id: ab-2380
izenburua: Neure Aditzaliak (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002380.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002380.MID
youtube: null
---

Neure aditzaliak, onuntz juntatu,
Kastillako berriak nai tut kontatu,
Goguan artu.
Ez nitzen bada oso txit barrenian sartu;
Bañan damutu.
An bizi baño len, obe luke urkatu. (nuke.