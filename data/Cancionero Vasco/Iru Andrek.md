---
id: ab-2043
izenburua: Iru Andrek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002043.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002043.MID
youtube: null
---

Iru andrek, bi komadrek,
Ai! zer afaria zuten bart!
Lau antzara, iru ollo,
Bi tortollo, eper bat.
Etziren gosiak etzin bart.