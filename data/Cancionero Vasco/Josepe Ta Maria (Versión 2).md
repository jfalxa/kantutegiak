---
id: ab-1782
izenburua: Josepe Ta Maria (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001782.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001782.MID
youtube: null
---

Josepe ta Maria bere Jesusekin
Joan ziran Belenera gaurko gabarekin.

Konpañian etzuten lagunik berekin;
Gutxitan falta oi da gaurko gabarekin.

Belengo etxe batian jo dituzte atiak,
Erantzun baliote eren amantiak.

Bitan, irutan ere Josepek jotzen du
Gizonak Bentanara txinak izutzen du.

Gizonak bentanatik dizute esaten:
- Gaubian ordu onetan zer darabiltzu emen?

- Donzella bat badegu gurekin batian,
Semia dakarrela Jesus sabelian.

- Semia ta donzella, nola al daiteke?
Gizon enbusteroba, ortikan ken zaitez.

Tristura aundi batekin ziraden gelditu,
Zeren iñun ostaturik etzuten billatu.

- Animo artu dezagun, bai arren, Josepe;
Infante zerukuba gaur jayo bear luke.

Josepek esan zion bai Mariari;
- Nik badakit estalpe bat, segi bada neri.

Joan ziran estalpera, asto idiakin,
Etzutela eraman oya zuri berekin.

Ganbelan paraturik astuba ta idia,
Oyen onduban zeuden Jose ta Maria.

Orazioa zuten ere afaria;
Artan ekarri zuten gaubaren erdia.

Eren seme ikuzkiya ikusi debia,
Orra nun mundu dan Jesus instante bedian.

Agur, agur, zerukubak, ondo geldi bitez;
Jesus jayo da eta konsubela bitez.