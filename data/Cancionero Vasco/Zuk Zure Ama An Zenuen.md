---
id: ab-2644
izenburua: Zuk Zure Ama An Zenuen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002644.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002644.MID
youtube: null
---

Zuk zure ama an zenuen,
Ta nik ere aita an nuen;
Lauen artian ezkontza ori
Formatzen asi genuen.
Amatxu orrek enteromente
Bajotik artzen ninduen:
Aren alaba pariatzeko
Ia dotia non duten.