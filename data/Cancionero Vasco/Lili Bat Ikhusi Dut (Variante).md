---
id: ab-2261
izenburua: Lili Bat Ikhusi Dut (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002261.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002261.MID
youtube: null
---

Lili bat ikusi dut baratze batian,
Desiratzen bainuen nere saihetsian;
Loreia ez du galtzen udan ez neguian
Haren parerik ez da bertze bat munduian.

Deliberatu nuen gau batez juaitera
Lili arraro hura eskura hartzera;
Ez bainuien pensatzen guardiatzen zutela!
Gau hartan uste nuien han galtzen nintzela!

Beltxarena naizela, zuk omen diozu:
Enaiz txuri txuria, eguia diozu.
Txuriak txuri dira, ni naiz beltxarena:
Oso kontent dago ni behar nuiela.