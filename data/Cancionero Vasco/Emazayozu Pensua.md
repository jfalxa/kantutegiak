---
id: ab-3050
izenburua: Emazayozu Pensua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003050.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003050.MID
youtube: null
---

Emazayozu pensua zaldi zuriari.
Aldezun lodi, aldezun lodi, zaldi zuriari.
La, la ra la...