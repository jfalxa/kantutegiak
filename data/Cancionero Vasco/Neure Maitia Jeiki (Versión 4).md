---
id: ab-2278
izenburua: Neure Maitia Jeiki (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002278.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002278.MID
youtube: null
---

Neure maitia, jeiki, jeiki,
Etzirade loz ase?
Zure ondotik gabiltzen oyek
Ez girade logale.
Ideki zazu puertaño ori:
Adiskidiak girade.