---
id: ab-2375
izenburua: Nere Maite
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002375.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002375.MID
youtube: null
---

Nere maite, maite, maite,
Munduan paregabe.
Zure lurrian erain nai nuke
Belbelinaren azie.
Jayo baledi, ein nizuke
Altzairu finez esie.

Nik badet zutzat egaztuntxo bat
Guztia da txurie.
Andikan (urrundik) ikusten det
Maiteñoaren begie.
Ikusi, eta ezin mintzatu,
Ai, au penaren aundie.