---
id: ab-2319
izenburua: Mendiak Bethe Belarrez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002319.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002319.MID
youtube: null
---

Mendiak bethe belarrez;
Begiak bethe negarrez.
Lan eginaz urriki dut,
Bainan probetxurikan ez.
Orai hementxen nago
Sabeletik miñez.