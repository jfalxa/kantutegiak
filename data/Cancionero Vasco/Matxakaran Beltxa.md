---
id: ab-2314
izenburua: Matxakaran Beltxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002314.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002314.MID
youtube: null
---

Matxakaran beltxa onddoren onddo,
On eta goxo arana.
Huna xirimiri, hara xakoli.
"Mari" presentekuak errainetan min,
Errainetan min badu, errainetan min,
"Jose" presentekuak sendaturen din.
Eta din, dan, din,
Eta xakolin,
Eta biolin.
"Jose" presentekuak sendaturen din.