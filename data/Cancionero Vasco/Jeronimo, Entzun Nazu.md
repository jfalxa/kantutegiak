---
id: ab-2982
izenburua: Jeronimo, Entzun Nazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002982.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002982.MID
youtube: null
---

Jeronimo, entzun nazu:
Neskatxarekin ibiltzen zera zu.
Ama datorrenian, nian (bis)
Etxetikan kanpora juan biar dezu.