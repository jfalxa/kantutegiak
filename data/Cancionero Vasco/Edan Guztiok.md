---
id: ab-1858
izenburua: Edan Guztiok
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001858.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001858.MID
youtube: null
---

Edan guztiok, beltxak gerade,
Ortzak ditugu xuriak.
Bedorren seme jaun orrentzako
Emen dauzkagu feriak.
Gure Jesus amorosua,
Gure ene konsuelua,
Eman iguzu orain grazia
Alkantzatzeko gero gloria.