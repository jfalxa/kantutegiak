---
id: ab-2773
izenburua: Urtarrilaren Hoi Ta Zazpian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002773.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002773.MID
youtube: null
---

Urtarrilaren hoi ta zazpian, Aiherra Etxegaraian,
Kauserak ere egin omen tuzte bere ustez segeretuian;
Bere ustez segeretuian eta hiru ahizpen artian.

Aita zuten merkatuian nausi gaztearekilan;
Ama xaharra Lekhuinen, Xastretegian.
Ahizparik gazteena ari latsen iturrian.

Haurra-Marik Mariari:-A, zer kausera-jailea, hi!
Unga bat ogi, zaragi bat olio, hiretako partia jateko.
Lagun hoberik eniken nahi mundu huntarik hi baino.

- Haurra-Marik: salbetik arno zaharra ekarri.
Ene ahizpak, edan dezagun bira kolpotxo huntarik,
Kauserak kalte egin ez dezaten edan gabetarik janik.

Itsasotik lanopetik jalgiten da euria;
Aiziarekin erauntsiak joiten du mundu guzia.
Zure eta ene segeretuia hala da barreatuia.