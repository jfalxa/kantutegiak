---
id: ab-2369
izenburua: Nere Ama Birjiña
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002369.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002369.MID
youtube: null
---

Nere Ama Birjiña
Konzepzionekua,
Mudatu egidazu
Neuronen gogua.