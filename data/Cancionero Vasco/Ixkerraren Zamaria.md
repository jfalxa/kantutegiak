---
id: ab-2125
izenburua: Ixkerraren Zamaria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002125.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002125.MID
youtube: null
---

Ixkerraren zamaria
Bilho urdintzen hasia;
Zango-besoak gogor, eta bizkarrian zauria,
Bai eta istant guziez lurrera duk eroria.

Ixkerra, saltzak zaldia,
Balin baduk eroslea:
Haren gainean ibiltzea ez duk, ez, zuhurtzia,
Bele-arranuez guardia pasatu diat bidea.

Ixkerraren zaldi hori
Establian xarmegarri;
Nik ija nayago nuke ardura oinez ibili:
Zaldarez aserik ere ez dik nihora mugitu nahi.

Ixkerrak zaldia prestatu,
Eni hartzia dolutu;
Belek eta arranoek bidean naute atakatu:
Jainkoak dakigola nintzen jaun hetarik libratu.

Etzirok xeha hartorik,
Ahoan ez baitu hortzik;
Harendako ez duk jiten Espainiatik olorik;
Ixkerrak hartuz geroztik ez daki hasearen berririk.