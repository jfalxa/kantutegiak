---
id: ab-1660
izenburua: Arranoak Bortietan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001660.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001660.MID
youtube: null
---

Arranoak bortietan gora dabiltza hegaletan;
Ni ere beste ordüz andereki khanberetan;
Orai aldiz ardüra ardüra nigarra dizüt begietan

Eztüta nik phena handia entzülerik ükhen gabia?
Jüratü diñat fedia; dolütüren zaiñ, maitia,
Ene behatü gabia galdü diñat libertatia.

Erlia doa mendiz-mendi, harrek han biltzen dü lili;
Hartarik egiten dü ezti; eztiak oro ez bardin ezti:
Mündüko neskatilak ezti(r)a bardin xarmagarri.

Eni anaiak igorri letera batez goraintzi:
Entzün diala bai berri banabilala andereki;
Ützi ditzadan anderiak, zer nahi düdan ideki?

Egin diot arrapostü nola baitü merexitü:
Harek ere zonbeit beitü, nik hetzaz güti profeitü:
Üzten düdala hura berekin, ützi nezan ni eneki phausü.

Hartzen diat ofiziua: Iratiat ülhaiñ banua,
Nola beitüt biziua oihanetan khantatzekua,
Abis hunik emaitez eta egia erraitez banua.

Ahaire hau zahar omen da, düda gabe ene adin beita,
Huntan nahi dit khantatü nola den gazte perfeita:
Gezür güti erraile eta hümil eta serious hua.

Arrosatziak eijer lilia, zühaiñ berak dü elhorria.
Amorio traidoriak, berekila azotia.
Hala dio borogatiak: begir'trunpa, adixkidia!

Aitak dio alhabari: "Nun habila, laidogarri?".
Alhabak bertan aitari, segür abis hun sari.
Orai da bera dolütürik, esklabo delakoz jarri.

Orai bera dolütürik dago, bena berantü zaio.
Herritik joan nahiago, egoitia laido beitzaio;
Hil baledi aldiz orano, thunban sarthü nahiago.

Orai bera aflitürik, libertatia galdürik,
Phena dolorez kargatürik, osagarriz gabetürik,
Oro berak kausatürik, amoriuak traditürik.

Laur urtheko haur gaixua, leial dük hik amoriua.
Hitan etsenplü har dezagün: maitha gure protsimua,
Lazükeriak kita eta zerbütxa zelüko Jinkua.

Khantore hoien egitian ülhaiñ nintzan borthian;
Pasülapeko oihanian, abis hunik emaiten nian;
Deüser'ezin sinhets-eraziz, bankarrot egiteko aiherrian.

Hartz handi bat, oihan hartan, nihauri so ikhusi nian:
Hortzak xuri, begiak gorri, lephua lodi beitzütian,
Khantore hoien huntzia ützirik, bankarrot doble egin nian.