---
id: ab-1939
izenburua: Ez Dela Donian Ez Dela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001939.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001939.MID
youtube: null
---

Ez dela Donian, ez dela,
Jendia triste dago;
Egizazu lo galanta asko
Donian goizeko.

Ai San Juan, ai San Juan,
Okela gutxi San Juan;
Igaz ere San Juan, San Juan zan,
Aurten ere San Juan Bautista,
Jesukristoren lengusua da
San Juan Ebanjelista.