---
id: ab-1723
izenburua: Premin Perurena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001723.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001723.MID
youtube: null
---

Premin Peruarena,
Goizuetakoa,
Neskatxe polite da
Zure gogokoa.
Gustezen omen zayo
Don Migelekoa;
Uritxen izanen du
Urbillenekoa.