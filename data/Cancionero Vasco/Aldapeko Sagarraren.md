---
id: ab-3004
izenburua: Aldapeko Sagarraren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003004.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003004.MID
youtube: null
---

Aldapeko sagarraren adarraren puntan,
Puntaren puntan,
Xoriño bat zegoen kantari:
Tiruliruli,
Tirulirula.
Nork kantatuko (dantzatuko) ote du soñutxo hori?

Enauk, eta banauk, eta fraidia nauk.
Ezkondu nahi, ta dirurik ez;
Dantzatu nahi, ta soñurik ez;
Aira onian, korri aldrebes.
Huna Jaunaren xerria,
Erre patataz asia.
Hiru migatxo, adar luzetxo:
Aida onian korri, Katalinatxo.