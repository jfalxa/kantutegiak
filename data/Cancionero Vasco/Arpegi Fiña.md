---
id: ab-1652
izenburua: Arpegi Fiña
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001652.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001652.MID
youtube: null
---

Arpegi fiña, korputza berriz
Ez dago zer esanikan;
Izketan ere grazi ederra:
Ezer ez dezu txarrikan.
Mundu guzia beiratuta ere
Zu bezelako damikan
Agian izan diteke, bañan
Ez det sinisten danikan.