---
id: ab-2053
izenburua: Ituringo Gareilla (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002053.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002053.MID
youtube: null
---

Ituringo gareilla, Erramun Joakin,
Asarre omen zera zeren dugun jakin.
Konfesa zaite ongi erretorarekin:
Ez dute zer fidatu Santuak zurekin.