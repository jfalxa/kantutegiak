---
id: ab-1485
izenburua: Abulun Abulun Abuluna
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001485.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001485.MID
youtube: null
---

Abulun, abulun, abuluna,
Hauxe duzu haur ona.
Hontaz eros nezake
Parise eta Baiona.