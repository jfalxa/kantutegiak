---
id: ab-2026
izenburua: Hiru Bostak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002026.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002026.MID
youtube: null
---

Hiru bostak amabost dire.
Amabosten konduan dire.
Yokatu nezak pinta
Amabost direla.