---
id: ab-2041
izenburua: Iragan Denborez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002041.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002041.MID
youtube: null
---

Iragan denborez pensatzen jarri-ta,
Bihotzerat heldu zait anitz erregreta.
Orduan gazte nintzen, deus ez nuen falta;
Orai ororen behar, goguaren bortxan.
Gizonaren mendeaz ez daiteke konda.

Mende gaixtuak hola etortzen dirade,
Bai eta onak juaiten, edoiaren pare.
Deboxa izatu naiz, neurririk gabe,
Lanian aritzeko, nekea herabe.
Gaztian hola dena, zahartzian pobre.