---
id: ab-2569
izenburua: Udaberria Udaberria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002569.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002569.MID
youtube: null
---

Udaberria, udaberria,
Arbola lorez zenian.
Zure berria zekien orrek
Esan nai zira egia.