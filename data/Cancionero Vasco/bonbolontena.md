---
id: ab-1809
izenburua: Bonbolontena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001809.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001809.MID
youtube: null
---

Bonbolontena, nere laztana,
Ez egin lorik basuan.
Aizteritxuak eamango zaitu
Erbiya zeralakuan. Bo.