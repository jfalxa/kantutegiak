---
id: ab-1718
izenburua: Badakizu Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001718.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001718.MID
youtube: null
---

Badakizu bat, badakizu bi;
Badakizu nungua, naizen ni.
Berueten jan omen ditu
Otso batek ardi bi.