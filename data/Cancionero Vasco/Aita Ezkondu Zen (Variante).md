---
id: ab-1534
izenburua: Aita Ezkondu Zen (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001534.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001534.MID
youtube: null
---

Aita ezkondu zen amarekin,
Ama aitarekin, biak elgarrekin.
Orduan gazte ziren,
Beste zerbaitekin.
Bainan orai zahartuez, deusarekin.
Ontasunak garbitu familiarekin,
Parte bat nerekin,
Gehitxago egin dute beste bostekin.