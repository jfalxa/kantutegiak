---
id: ab-1554
izenburua: Haika Puttil
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001554.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001554.MID
youtube: null
---

- Haika, puttil, jaiki hadi:
Argia denez mia hadi.
- Bai, nausia, argia da:
Gure xakurra jaikia da.