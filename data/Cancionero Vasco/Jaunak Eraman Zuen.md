---
id: ab-2135
izenburua: Jaunak Eraman Zuen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002135.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002135.MID
youtube: null
---

Jaunak eraman zuen gurutzea soñean,
An pasa bear zuela eriotzian.
Au bear degu guztiok esan
Anima kondenaturik ez dedilla izan.