---
id: ab-1574
izenburua: Allitik Ekarri Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001574.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001574.MID
youtube: null
---

Allitik ekarri du Arruize zakurre;
Eztegu pagatu lanbide makurre. (paratu)
Bost aldiz eman diot boltsatik apurre.
Ori dabillen artzai, kapelu makurre.