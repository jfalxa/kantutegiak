---
id: ab-1816
izenburua: Bozkarioz Gaude
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001816.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001816.MID
youtube: null
---

Bozkarioz gaude, bozkarioz gaude,
Egin zaretelakotz elgarren jabe elizan.
Gauz ederragorik ez diteke izan.
Oxala banindago, oi! zuen gisan.