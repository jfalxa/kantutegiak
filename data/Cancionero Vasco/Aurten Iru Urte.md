---
id: ab-1711
izenburua: Aurten Iru Urte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001711.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001711.MID
youtube: null
---

Aurten iru urte, ala biarrez,
Errira nintzen agertu.
Izar eder xarmangarria
Laster nuen ezagutu.
Bai ura nitaz, eta ni artaz,
Laster giñen agradatu.
Beldurrez berzeik auta etzezan,
Ezkontzaz nion galdetu.
Bizi guziko neria zela,
Eman zatan errepustu.