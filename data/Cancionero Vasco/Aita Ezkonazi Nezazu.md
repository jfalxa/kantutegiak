---
id: ab-1535
izenburua: Aita Ezkonazi Nezazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001535.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001535.MID
youtube: null
---

- Aita, ezkonazi nezazu,
Adina kitu danian.
- Neska, othoi, hago ixilik!
Ogirik ez diñat etxean.

Ogi-ondo bat ikusi nuen
Landaren erdi-erdian.
Egun erregua botatzen zuen (ehun)
Lehen trailu-kolpian.