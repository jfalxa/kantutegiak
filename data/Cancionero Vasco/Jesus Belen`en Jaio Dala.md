---
id: ab-2142
izenburua: Jesus Belen`en Jaio Dala
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002142.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002142.MID
youtube: null
---

Jesus Belen'en jaio dala
Aditu dezute, Txomin ta Patxi?
Bai, Peru, guri bana arturik
Laster bear degu menditik jetxi.
Guazen laster adoratzera,
Naiz zaidikan gabe ardiak utzi,
Naiz zaidikan gabe ardiak utzi.