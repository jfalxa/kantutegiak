---
id: ab-2061
izenburua: A La Fuente Vas Por Agua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002061.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002061.MID
youtube: null
---

A la fuente vas por agua,
Ferrada buruan;
Y no vuelves a casa
Iru lau orduan.
Y la dueña renegaba )
Alako moduan! ) bi.