---
id: ab-2665
izenburua: Muthil Gaixuak Tristerik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002665.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002665.MID
youtube: null
---

Muthil gaixuak tristerik,
Ongi begiak ilhunik,
Ez dutelakotz probetxurik.
Xori hura begiztaturik,
Hobeki einen dutela
Bide hori kitaturik.