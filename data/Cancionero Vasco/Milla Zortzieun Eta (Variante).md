---
id: ab-2735
izenburua: Milla Zortzieun Eta (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002735.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002735.MID
youtube: null
---

Milla zortzieun eta hogoi ta amar bortza,
Bi gardek artu naute, ez bainintzen gaitza.
Ustez prestuak ziren egundu naiz aisa.
Kaskoin españ aundi bat, iduri zurkaitza,
Arraza debru ori ez dezaket maita.

Ikusten dedanian nere begietan
Fortunarik ez dela urrats orietan,
Gauak egun egiñez nabil sasietan
Mezarik entzun gabe kasik erdietan
Bezperatik ez konda urtian bietan.

Bortz aldiz ibili naiz esan dudan plantan
Gosiak akabatzen jatekoaren faltan,
Goizeko amarretan sarturik kamantzan,
Petiri-Santz irriz eta dantzan
Kukusuak nere partez asirikan mantan.

Iru oi ta bortz urte da nik dudan adiña
Gaurgero ez da urrun nere azken fina.
Obiago nuke banu testamentu egiña.
Pena gaxtua baita ontasunen griña
Nun da gero ondoko aurren atsegiña.

Dirurik eztut, bañan badut azienda,
Baztaleko onian ederki lotzen dan,
Otso beldurrik gabe gabaz alatzen da,
Larruz egiña dauka bere bestimenda:
Diruz ezta pagatzen orrelako prenda.

Ni ola izatia ezta mirakullu,
Zeren izandu baitut sobera urgullu,
Ostatu ta taberna beti kullu, kullu.
Nere moltzak ez dittu milla koropillo:
Palaz basarratu ta esku ezin bildu.