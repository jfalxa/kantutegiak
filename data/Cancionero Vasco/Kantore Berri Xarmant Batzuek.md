---
id: ab-2208
izenburua: Kantore Berri Xarmant Batzuek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002208.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002208.MID
youtube: null
---

Kantore berri xarmant batzuek
Hastera nua kantatzen.
Ignorante balin bada,
Guziak jakin dezaten.
Izar eder bat badakizute
Orok badela Luzaiden;
Eztitazunez betia da eta
Bazterrak ditu xarmatzen,
Gaixua hasi da tristatzen.