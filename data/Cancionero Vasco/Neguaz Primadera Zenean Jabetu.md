---
id: ab-2368
izenburua: Neguaz Primadera Zenean Jabetu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002368.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002368.MID
youtube: null
---

Neguaz primadera zenean jabetu,
Sasi baten hegian apexa zen sortu;
Lekhu berean baitzen lorea agertu:
Gaixoek elgar zuten bihotzez maitatu.