---
id: ab-2271
izenburua: Lo Lobatxoa Txuntxulun Berde
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002271.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002271.MID
youtube: null
---

Lo, lobatxoa, txuntxulun berde,
Amatxoa masuste.
Aita Bitoriara joan da
Ama mandoan artute.