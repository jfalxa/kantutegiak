---
id: ab-2707
izenburua: Anderia, Gorarik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002707.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002707.MID
youtube: null
---

Anderia, gorarik zaude leihoan.
Zure senarra behera dago Prantzian.
Hura hantik jin ditekenen artian,
Eskutaritzat har nezazu sahetsian.