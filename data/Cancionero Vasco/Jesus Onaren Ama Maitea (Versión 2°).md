---
id: ab-1580
izenburua: Jesus Onaren Ama Maitea (Versión 2°)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001580.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001580.MID
youtube: null
---

Jesus onaren Ama maitea,
Grazia dizut eskatzen:
Bortz misterio gozoz beteak
Lagun gaitzazu esaten,
Lagun gaitzazu esaten.