---
id: ab-1605
izenburua: Amodio Itsu Batek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001605.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001605.MID
youtube: null
---

Amodio itsu batek nerauka,
Deusetan ere ez diot nik hura kita.
Hanbat amodio nik zuretako,
Mintzatzia galdu, itsu errendatu.
Hauxe da bada nik dudan malura;
Jiten niz zugana.