---
id: ab-2657
izenburua: Libiri Ta Labero
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002657.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002657.MID
youtube: null
---

A. Libiri ta labero,
Bai, ganau usaña. (bis)

B. Erkortxeko ganaua
Artua gureña. (bis.