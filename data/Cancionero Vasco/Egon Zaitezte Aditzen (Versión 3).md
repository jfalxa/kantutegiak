---
id: ab-1848
izenburua: Egon Zaitezte Aditzen (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001848.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001848.MID
youtube: null
---

Egon zaitezte aditzen,
Zartu ta nola gabiltzen;
Gazte denboran ezkondu gabe,
Erresta gabiltzen.