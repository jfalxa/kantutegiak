---
id: ab-1823
izenburua: Burullaren Illaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001823.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001823.MID
youtube: null
---

Burullaren illaren ogei eta bian,
Pilota-joko aundi bat Irungo errian.
Banderak ezarririk, oi moda berriak!
Jokatu izan dirade estadu ikaragarrian.