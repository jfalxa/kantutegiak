---
id: ab-2118
izenburua: Horra Hamasei Bertso
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002118.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002118.MID
youtube: null
---

Horra hamasei bertso
Aurtengo berriak,
Jendiak jai dezaten
Gure komedia.
Gezurra gabetanik
Diranak egiya;
Berotu behar diogu
Beltzai belarriak.