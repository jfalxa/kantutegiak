---
id: ab-2297
izenburua: Mandamentu Santuetan (Versión 8)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002297.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002297.MID
youtube: null
---

Mandamentu santuetan lendabizikoa:
Biotzetik amatu gure Jaungoikoa,
Idolorikan gabe, au da kariñoa,
Guazen adoratzera Jesús dibinoa,
Berak guretzat dauka gloria eternoa.