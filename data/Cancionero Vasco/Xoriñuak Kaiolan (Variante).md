---
id: ab-2563
izenburua: Xoriñuak Kaiolan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002563.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002563.MID
youtube: null
---

Xoriñuak kaiolan
Tristerikan du kantatzen.
Dialarikan zer jan, zer edan
Kanporat du desiratzen:
Zeren, zeren,
Libertatia zoin eder den.