---
id: ab-1489
izenburua: Adanen Pekatuak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001489.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001489.MID
youtube: null
---

Adanen pekatuak
Genduzken galdurik,
Infernuko ondarrera
Betiko sarturik.
Etzegoan guretzat
Erremediorik,
Jesus jaio ezpalitz,
Gutzaz kupidurik.

Libre gerade orain
Betiko infernutik
Iges egin dezagun
Pekatuetatik.
Zerutako Erregea
Daukagu jaiorik,
Belen'go estalpean
Ganbelan jarririk.

Gizonaren amorez
Gizona eginik,
Belen'en eztu upatzen
Artu nai duanik.
Zeruak eta lurrak
Bereak izanik,
Jaiotzeko denboran
Etzun ostaturik.

Ostaturikan gabe
Belen'go kalean,
An dago Jesus ona
Estalpe batean.
Otzak illen ezpazun
Oraingo gabean,
Jaio bear izan zun
Abere artean.

Astoak eta idiak
Zutean berotzen,
Jesus zegoalako
Otzak akabatzen.
Lasterkan Belen'era
Guztiok goazen,
Jesus adoratzera
Urbildu gaitezen.