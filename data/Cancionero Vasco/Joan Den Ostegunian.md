---
id: ab-2153
izenburua: Joan Den Ostegunian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002153.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002153.MID
youtube: null
---

Joan den ostegunian
Amabost Hernanin,
Betroyaen tratua
Genduen egin.
Inazio saltzalle,
Erostuna Fermin,
Emeretzi ezkutu
Gendion eragin.
Sabelian du min,
Iñork ase ezin,
Artuari muzin,
Eztu nai edozin;
Atzetik biar lituzke
Zazpi medezin.