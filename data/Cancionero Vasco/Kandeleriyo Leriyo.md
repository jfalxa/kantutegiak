---
id: ab-2194
izenburua: Kandeleriyo Leriyo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002194.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002194.MID
youtube: null
---

Kandeleriyo leriyo,
Atxari ure dariyo,
Sagarrari madari;
Eutzi, Peru, adarrari.