---
id: ab-2310
izenburua: Martxuaren Hamabian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002310.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002310.MID
youtube: null
---

Martxuaren hamabian
Erretretaren joaitian
Hiriko zitadelatik,
Atera ginen atetik.
Bi zentilela artetik
Salto eginik zubitik,
Goratik,
Galde egin gabe kondutik.

Komendantak zutenian abisatu,
Kapitaina zen arritu
Eskualdunak eskapatu.
- Jaunak, ez dirot sinets nik
Eskapatu den gizonik
Morraila horien artetik,
Bizirik,
Salto eginik segurik.