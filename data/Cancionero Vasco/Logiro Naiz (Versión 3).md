---
id: ab-2266
izenburua: Logiro Naiz (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002266.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002266.MID
youtube: null
---

Logiro naiz ta logale,
Argi-ollarrak jo gabe.
Bart arratsian etxe ontako atetan
Etziran gazteak loale.