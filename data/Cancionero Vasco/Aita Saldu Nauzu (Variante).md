---
id: ab-1537
izenburua: Aita Saldu Nauzu (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001537.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001537.MID
youtube: null
---

Aita, saldu nauzu idi bat bezala;
Bai eta kondenatu Espainian barna.
Ama bizi ukhen banu, aita, zu bezala,
Ez nuen gan beharko Hongrian behera,
Bainan bai ezkonduren Atharratze Salala.