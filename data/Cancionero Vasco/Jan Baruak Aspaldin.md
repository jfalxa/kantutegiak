---
id: ab-2613
izenburua: Jan Baruak Aspaldin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002613.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002613.MID
youtube: null
---

Jan Baruak aspaldin
Txedera bat edatü zin.
Txori eijer bat hatzaman dizü Paueko seroren konbentin.
Orai harekin lotzen düzü, aspaldian desir beitzin.

Jauna, entzün ezazü,
Felizitatzen zütügü:
Mus de la Plazaren alab'eijerra zük düzüla esposatü.
Andere hura irus düzü: zuri ezteizügü dolü.

Txedera balitz halako
Merkatietan saltzeko,
Ziberoko aitunen semek eros litzazkeie oro
Txori eijer zonbaiten hatzamaiteko.

- Igaran Apirilaren bürian
Armadaren erdian,
Züntüdan bihotzian, armak oro eskian;
Present espiritian, manka besuen artian.

- Jauna, maite banaizü
Erraiten düzün bezala,
Kita ezazü, kita ezazü Erregeren zerbütxia,
Eta maita herria, ükhen dezadan plazera.

- Eztiot kita, maitia, (Eztirot)
Erregeren zerbütxia.
Sorthü nüzü uhurezko, Erregeren zerbütxüko.
Maite dit uhuria zügatik, xarmagarria.