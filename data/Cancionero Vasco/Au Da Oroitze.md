---
id: ab-1698
izenburua: Au Da Oroitze
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001698.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001698.MID
youtube: null
---

Au da oroitze pan izabia eguna.
Zen egun sartutzen Erreginen eguna.
Astera utzi gabe nai ditut señalatu
Urtia ta eguna.