---
id: ab-2656
izenburua: Anton Ta Peru
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002656.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002656.MID
youtube: null
---

Anton ta Peru ta Martin Anton,
Prantxisku, Paulo, Melkior, Simon.
Ai! au gabaren on!
Jaio berri ori nun degun, nun.