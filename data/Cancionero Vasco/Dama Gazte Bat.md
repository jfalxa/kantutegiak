---
id: ab-1826
izenburua: Dama Gazte Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001826.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001826.MID
youtube: null
---

Dama gazte bat irukitu dut
Biotzian ta goguan.
- Ni bezelako izpillurikan
Etzen zuretzat orduan.
Despei emanta, orain utzi nau
urte beteren buruan.