---
id: ab-1724
izenburua: Bakarrik Bizitziaz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001724.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001724.MID
youtube: null
---

Bakarrik bizitziaz unatua franko,
Xede zerbait banuyen lagun bat hartzeko.
Baña orain andreak zenbat ez du behar!
Ez, ez, nayago dut egon mutil zahar.