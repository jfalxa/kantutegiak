---
id: ab-2379
izenburua: Neronek Esan Nizun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002379.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002379.MID
youtube: null
---

Neronek esan nizun, Hernanira juanik,
Jostan ikasi biar zinduela oraindik.
Errespuesta eman zenuen: ez daukat dirurik.
Zuaz etxera eta emango dizut nik.