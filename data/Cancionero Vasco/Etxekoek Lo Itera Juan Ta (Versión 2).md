---
id: ab-2559
izenburua: Etxekoek Lo Itera Juan Ta (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002559.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002559.MID
youtube: null
---

Etxekoek lo itera juan ta
Gelditu giñan bakarrik.
Egunderaino ez baitut edan
Au bezalako naparrik.
Gustua ere txarra zuen, ta
Batere etzuen indarrik;
Hura baño hoberik ez balitz,
Ez laiteke iñon mozkorrik.