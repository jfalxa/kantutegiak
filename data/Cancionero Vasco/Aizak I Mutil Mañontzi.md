---
id: ab-1555
izenburua: Aizak I Mutil Mañontzi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001555.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001555.MID
youtube: null
---

Aizak, i, mutil mañontzi,
Bertze batekoz goraintzi:
Ez al akien pipatzia or
etzela lizentzi?
Ingenio txarrak utzi,
Bertzela ezurrak autsi.
Gizalegia nola serbitu
Ola erakutsi.