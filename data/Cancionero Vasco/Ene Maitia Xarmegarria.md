---
id: ab-1887
izenburua: Ene Maitia Xarmegarria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001887.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001887.MID
youtube: null
---

Ene maitia, xarmegarria,
Dohain ederrez zirela bethea.
Zuk egin afrontua sarthu zait bihotzera,
Heldu baitzait gogora, ardura.
Nik orai pena, zure malura
Denborak emanen du agertzera.

Xoratu nauzu, tronpatu nauzu,
Ene bihotza eritu dautazu.
Denborak beharko du ene pena kasatu
Bihotz'alegeratu, sendatu.
Hobe bat ustez, nauzu kitatu,
Zure malura hortan egin duzu.

Zuk kitaturik xangrinaturik
Bi urthe egotu naiz pasaturik:
Ene bihotz tendrea, nihor maite gaberik
Etzagoken bakharrik, tristerik;
Maite berri bat du hautaturik
Hark ezarri nau alegeraturik.

Adio enea, lehen maitea,
Desir onez dut bihotza bethea.
Zuk egin afrontua aspaldi barkhatua
Desir dautzut, gaizoa, zerua;
Munduan ere bizi luzea
Eginik zure gostuko maitea.