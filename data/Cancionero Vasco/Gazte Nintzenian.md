---
id: ab-1963
izenburua: Gazte Nintzenian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001963.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001963.MID
youtube: null
---

Gazte nintzenian, hogei urtetan,
Ardura nindabilan neskatiletan.
Eta orain sainddez ostatuetan,
Diru gutxi moltsan behar orduetan;
Ez naute ikusi nahi konpañietan.