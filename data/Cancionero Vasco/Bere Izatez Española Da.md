---
id: ab-1764
izenburua: Bere Izatez Española Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001764.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001764.MID
youtube: null
---

Bere izatez española da,
Izena ere Martin du;
Enintzan ola esplikatuko
Zierto ezpanu jakindu.
Anima garbira biltzeko,
Azio txarra egin du;
Gurutzetxo bat emain niyoke,
Nik biar banu agindu.