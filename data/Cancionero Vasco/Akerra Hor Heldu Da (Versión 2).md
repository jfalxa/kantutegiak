---
id: ab-1559
izenburua: Akerra Hor Heldu Da (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001559.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001559.MID
youtube: null
---

Akerra hor heldu da
Artuaren yaterat.
Akerra ken, ken, ken,
Artua gurea zen.