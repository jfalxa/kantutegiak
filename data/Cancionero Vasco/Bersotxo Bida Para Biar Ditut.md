---
id: ab-1774
izenburua: Bersotxo Bida Para Biar Ditut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001774.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001774.MID
youtube: null
---

Bersotxo bida para biar ditut
Jesusen alabatzeko;
Gogo onian kanta dezagun,
Ez dire gure galtzeko.
Aditzia're aski izanen da
Biotza erdiratzeko.