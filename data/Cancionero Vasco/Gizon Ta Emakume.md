---
id: ab-1969
izenburua: Gizon Ta Emakume
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001969.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001969.MID
youtube: null
---

Gizon ta emakume oraingo aldian
Geienak emen dira gure Españian.
Trenbide edo traste orren aitzekian,
Onezkero txit asko ez dago Frantzian.