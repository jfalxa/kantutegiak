---
id: ab-1596
izenburua: Amasei Urte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001596.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001596.MID
youtube: null
---

Amasei urte Sanjuanetan,
Sanjuanetan ditut konplitzen.
- Gazterik asi zera
Plazan dantzatzen,
Galaia, txikie zarelarik.
Oi, andinaia!
Plazan zere burua
Erakusi naia.