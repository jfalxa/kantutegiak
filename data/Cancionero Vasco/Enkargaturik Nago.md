---
id: ab-1895
izenburua: Enkargaturik Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001895.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001895.MID
youtube: null
---

Enkargaturik nago lanpide batian,
Bertso bi paratzeko Frantzen gerlatian.
Ez nuke mintzetu nai iñoren kaltian;
Konbeni den bezala main tut nere ustian.

Aspaldi aitu nuen Frantzesaren fama:
Bazaukela bear zen indarra ta arma.
Ala're eldu zaio Brusi ori barna;
Nai bau trabajatu omen dauka lana.

Frantzian sarturikan zenbait mille urrez,
Aiziña segitzeko ezpide da dudez.
Ezpide da asi txantzez edo burlez.
Frantzesak oroitu bear da artzen duen zurrez.

Brusi ori maisu ona etorri zaio aurten,
Bere orden antzetan ez padago zuzen.
Gerlari ta famatue Napoleon zuten;
Eztakit yende orain ala errain duten.

Frantzian bakia zen goan den aspaldian;
Bisitzen etzakiten inozenterien.
Orra nun gelditu diren salsaen erdien.