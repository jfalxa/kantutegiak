---
id: ab-2051
izenburua: Iruzki Dibinua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002051.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002051.MID
youtube: null
---

Iruzki dibinua
Gabaren gaberdian
Etorri zan mundura
Gizonaren trajian.
Jesus Maria,
Maria Jose.