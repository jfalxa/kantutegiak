---
id: ab-2068
izenburua: Ai, Au Pena Ta Pesalunbria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002068.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002068.MID
youtube: null
---

Ai, au pena ta pesalunbria!
Zer esanen dut geyago?
Sudurra ere luzatu zaigu
Kokotsa baino berago.
Gure etxera gan biar baino,
Ille banago nayago.