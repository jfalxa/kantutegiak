---
id: ab-1656
izenburua: Arranbillote Arranbillote
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001656.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001656.MID
youtube: null
---

Arranbillote, arranbillote,
Mikelandoneko semia dute
Atia jo ta leiora,
Rokeren semia jaio da.