---
id: ab-3253
izenburua: Beltxak Eta Txuriek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003253.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003253.MID
youtube: null
---

Beltxak eta txuriek )
Mendian ardiek; )bis
Zuk ere ez dituzu
Bentajak guziek.
La, ra, la...

Txuriak txuri dira, )
Ni naiz beltxerana; )bis
Kontentu izanen da
Ni bear nauena.
La, ra, la...

Beltxera ur naizela )
Zuk niri errateko; )bis
Nin ziñan lenago
Erreparatzeko.
La, ra, la...

Gure auzoko iriek )
Adarrak oriek; )bis
Zeiñen ederki dabiltz
Gure dantzariek.
La, ra, la...

Goyak ariñak ditu, )
Beyak ariñago; )bis
Dantzan obeki daki
Artajorran baño.
La, ra, la...