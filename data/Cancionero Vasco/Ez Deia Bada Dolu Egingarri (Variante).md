---
id: ab-1938
izenburua: Ez Deia Bada Dolu Egingarri (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001938.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001938.MID
youtube: null
---

Ez deia bada dolu egingarri amodioetan dena?
Bethi penaz eta dolorez derama gaua eta eguna.
Hauxe da bada pena
Ene bihotzian dena.
Nihork ez diro konpreni nere hasperapena,
Maite bat ezin konbertituz, nik sufritzen dudana.