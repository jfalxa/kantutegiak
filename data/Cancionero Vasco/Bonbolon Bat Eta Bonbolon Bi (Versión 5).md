---
id: ab-1806
izenburua: Bonbolon Bat Eta Bonbolon Bi (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001806.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001806.MID
youtube: null
---

Bonbolon bat eta bonbolon bi,
Bonbolon putzura erori.
Erori bazan erori,
Geyago etzan ageri.

Bonbolon bat, bonbolon bi,
Barbera urera erori.
Erori bada erori
Kalarik eztuela egarri.

Bonbolon bat eta bonbolon bi,
Gure aurra putzura erori:
Erori bazen erori,
Etzen geiago egarri.