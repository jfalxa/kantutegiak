---
id: ab-1785
izenburua: Bi Xinaurri Hiru Xinaurri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001785.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001785.MID
youtube: null
---

Bi xinaurri, hiru xinaurri
Dantzan ari ziren,
Labe bero bero batian
Artixo landatzen.

Kukusu jauna ere han zuten
Egur arrailatzen;
Ehun urteko zorri kapitain bat
Heien garraiatzen.

Ditarian ur ekarri,
Zetabian bero,
Erhi ttikia bustiz, bustiz,
Ogia orha niro.