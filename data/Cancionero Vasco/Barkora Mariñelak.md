---
id: ab-1735
izenburua: Barkora Mariñelak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001735.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001735.MID
youtube: null
---

Barkora, mariñelak.
Juan biar dogu urrunera,
Bai, Indijetara,
Bai, indijetara.
Etxat, ez, aztuko )
Zure plai ederra. )
Agur, Lekeitio, )bis
Itxaso basterra! )
Agur, Lekeitio,
Itxaso basterra.
Agur, Lekeitio.