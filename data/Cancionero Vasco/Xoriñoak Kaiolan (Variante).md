---
id: ab-2565
izenburua: Xoriñoak Kaiolan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002565.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002565.MID
youtube: null
---

Xoriñoak kaiolan
Tristerik du kantatzen.
Duelarik zer yan, zer edan,
Kanpua du desiratzen:
Zeren, zeren,
Libertiziyua zoin eder den.