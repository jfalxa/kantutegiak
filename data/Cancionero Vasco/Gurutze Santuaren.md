---
id: ab-2020
izenburua: Gurutze Santuaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002020.MID
youtube: null
---

Gurutze santuaren
Bai misterioa.
Ondo eranzun zagun
Jende humaniba.