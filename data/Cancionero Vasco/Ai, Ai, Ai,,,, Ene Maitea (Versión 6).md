---
id: ab-2281
izenburua: Ai, Ai, Ai,,,, Ene Maitea (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002281.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002281.MID
youtube: null
---

- Ai, ai, ai, ai, ai, ai, ene maitea,
Etziradeia loz ase?
- Ene aita eta ama ez dira oino lokartu;
Errepausu harzazu.
Hek lokartzen diren bezen laster,
Biak egonen gituzu.