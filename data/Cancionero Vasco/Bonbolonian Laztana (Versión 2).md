---
id: ab-1810
izenburua: Bonbolonian Laztana (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001810.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001810.MID
youtube: null
---

Bonbolonian laztana,
Ez egin lorik basuan.
Ezturitxiak ilkoen zaitu
Erbiya zeralakoan.