---
id: ab-1742
izenburua: Bat Bia Hiru Lau
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001742.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001742.MID
youtube: null
---

Bat, bia, hiru, lau,
Bortz, sei, zazpi, zortzi,
Bederatzi, hamar,
Hameka, hamabi,
Hamahiru, hamalau,
Hamabortz, hamasei,
Hamazazpi, hemezortzi, hemeretzi, hogoi,
Hemeretzi, hogoi.
Hogoi, hemeretzi,
Hemezortzi, hamazazpi,
Hamasei, hamabortz,
Hamalau, hamahiru,
Hamabi, hameka,
Hamar, bederatzi,
Zortzi, zazpi, sei, bortz, lau, hiru, bia, bat,
Lau, hiru, bia, bat.