---
id: ab-1968
izenburua: Giristinuak Barkatu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001968.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001968.MID
youtube: null
---

Giristinuak, barkatu,
Abertentzia dut hartu,
Nahi'ut egiaz mintzatu,
Gure denbora nahiz pasatu.
Tierra hunian gira sortu,
Ez dugu abandonatu,
Beharrak guardatzen gaitu;
Konserbatu behar baitu.