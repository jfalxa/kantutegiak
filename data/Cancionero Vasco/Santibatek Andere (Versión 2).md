---
id: ab-2754
izenburua: Santibatek Andere (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002754.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002754.MID
youtube: null
---

Santibatek, andere,
Iaz bezala aurten ere,
Santibatek igorritzen gaitu )
Txingarketa gu ere. ) bis

Ez dugu nai urdia,
Ez eta ere erdia;
Kostumatzen dugun bezela,
Librako zatia;
Librako zatia eta
Gerrenaren betia.

Bortak eder leiarrez,
Bortal-atzia intxaurrez;
Etxe ontako etxekanderiak )
Bular-artia zillarrez. ) bis

Etxian eder kontzeiru (korzeiru)
Ite utenian inguru;
Etxe ontako etxekanderia )
Parabisuan aingeru. ) bis

Guazen, guazen emendik,
Emen ez duk xingarrik;
Etxe ontako gazitegian )
Xaguak umiak eiten dik. ) bi.