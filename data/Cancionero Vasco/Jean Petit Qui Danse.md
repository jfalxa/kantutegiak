---
id: ab-2883
izenburua: Jean Petit Qui Danse
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002883.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002883.MID
youtube: null
---

Jean Petit qui danse,
Da lu dig que danse
( Eriño batez
( Bertzeño batez
( Zangoño batez
( Bertzeño batez
tocando el suelo / Belauño batez
Ukondo batez
( Ankaño batez
( Bizkarra batez
( Zudurño batez
( Kaskoño batez la la la la la la la.