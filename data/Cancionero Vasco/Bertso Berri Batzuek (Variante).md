---
id: ab-1624
izenburua: Bertso Berri Batzuek (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001624.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001624.MID
youtube: null
---

Bertso berri batzuek nua kantatzera
Banka hortan sujetak ardura baitira.
Anton eta Maria xangrinetan dira:
Ustekabe gaxuak tronpatu baitira,
Elkarri soberasko konfidatu dira.