---
id: ab-1879
izenburua: Emen Bagaituk Lau Lagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001879.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001879.MID
youtube: null
---

Emen bagaituk lau lagun:
Dantza bat eman diezagun.
- Ail, errayok danboliñari
Etor dayela bereala
Bere ttunttun txirularekin
Eta instrumentarekin.

- Mandataria, zer berri
Ekartzen deraikuk guri?
- Eldu dela beriala.
- Gaiten beraz alegera
Eta edan kolpe bana,
Botoilan baldin bada.

Allegatu ginenian bortara,
Lendabizikorik ala
Adierazteko norbait bazela
(Atean yoka).
Etxeko andria eldu da
Aldadien bezela,
Gona kotillon kordela,
Eskuz ezkerrean zuela.
- Yaunak, pasa zaitezte,
Ixil ixila.