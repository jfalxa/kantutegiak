---
id: ab-2661
izenburua: Ave Maria Xuria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002661.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002661.MID
youtube: null
---

Ave Mari xuria,
Xuria eta ederra,
San Joseren esposa maitia;
Anparatzalen anparatzalia,
Anpara nazazu.