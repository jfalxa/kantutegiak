---
id: ab-2627
izenburua: Zeren Billa Zabiltza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002627.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002627.MID
youtube: null
---

- Zeren bilha zabiltza,
Andre Madalena?
Zerk hola zaramatza
Desertuan barna?

- Jauna galduz geroztik
Eztut plazerikan;
Gauza guziak ditut
Higuin mundu huntan.

- Kurutzefikaturik,
Ez duzu aditu?
Juduek nola zuten
Sepulturan sarthu?

- Neronek ikhusi dut
Amaren aldean,
Enegatikan hila
Kurutze batean.

- Oi, andre Madalena,
Ez zaitela tronpa;
Oraino har ezazu
Leheneko ponpa.

- Hire ponpa lizunak
Sobra segiturik,
Ongi niagok orai
Urrikiz betherik.

Adios sekulakotz,
Mundu gezurtia,
Nitan agertuko duk
Hire tronperia.