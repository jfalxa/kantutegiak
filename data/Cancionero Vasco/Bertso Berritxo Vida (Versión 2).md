---
id: ab-2291
izenburua: Bertso Berritxo Vida (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002291.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002291.MID
youtube: null
---

Bertso berritxo bida nik orai paratu,
Amar mandamenduak nola gobernatu.
Lendabizikorikan Yaungoikoa amatu;
Geren lagun projimua bardin estimatu.

Bigarren mandamenduan, yuramentu gutxi;
Nai duena esaitera mingaña ez utzi.
Ez da bada gauza hortan diferenzia guti:
Zerurat igan, edo ifernurat yautsi.

Meza, bai, osua entzun irugarrenian,
Obligatuak gaude igande egunian;
Obra onak eginez gero ahal denian,
Gloriaz gozatzeko eternitatian.

Laugarren mandamenduan, geren gurasoak
Alegiñak sokorritu eztuten gaixuak.
Urrikaltzeko dira ayen trabajuak,
Egiñalak egin ta're ezin pagatuak.

Bost mandamenduan, iñor ez iltzia;
Bekatu aundia dela badut aditzia.
Gorputza eta arima gizona galtzia,
Gañera Yaungoikoa ofenditutzia.

Seigarren mandamenduan, asko gira trebe,
Eztut iñor berezten aberats ta pobre;
Baña bekatu egin ta ezkirade libre,
Yaungoikoaren legia bear degu gorde.

Zazpigarren mandamenduan, deusik ez ebatsi,
Nor berearekin pasatu, bertzerenak utzi.
Deabruak laguntzen du egiteko gaizki,
Atxemateko sariak eraturik dauzki.

Zortzigarren mandamenduan, au ein bear degu,
Faltso testimoniorik iñori ez goratu.
Yuramentu faltsorik egiten badegu,
Egun on batian kontu eman bearko degu.

Bederatzigarren mandamenduan, eztago bertzerik,
Gerentzat ez deseia bertzeren emazterik;
Bertzeren odolian ez artu parterik,
Arima gaxo orrek izan eztezan kalterik.

Yaungoikoak emanak amar mandamendu,
Orien guardatziaz ar dezagun kontu.
Manamendu oriek guardatzen baitegu,
Maria Santisimak lagunduko gaitu.

Mila zortzi ehun eta berrogei ta ameka,
Jesukritoren urtiak or nunbat dabiltza.
Manamendu oyetan balin ba gabiltza,
Salbateko ginela eman zuen itza.

Esteban dut izena, deitura dut Landa.
Bertso bien paratzeko gogoan eman da.
Ori salbamendurako regla guan da
Eztakienik balin bada, yakitia on da.