---
id: ab-1500
izenburua: Aditu Gabriela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001500.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001500.MID
youtube: null
---

Aditu, Gabriela,
Zenbat urte ditun?
Ogei ta amasei,
Nik geia baditun.
Baldin oraingo txandan
Ezkontz'ezpagaitun,
Geren bizi guziko
Neska zahar gaitun.