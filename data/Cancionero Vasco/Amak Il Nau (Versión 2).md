---
id: ab-2828
izenburua: Amak Il Nau (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002828.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002828.MID
youtube: null
---

Amak il nau,
Aitak yan nau,
Nere arreba ponpoxak
Piztu nau.