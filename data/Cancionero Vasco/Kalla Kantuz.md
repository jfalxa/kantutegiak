---
id: ab-2186
izenburua: Kalla Kantuz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002186.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002186.MID
youtube: null
---

Kalla kantuz ogipetik
Uztai-agorriletan;
Maiteaganik etxerakuan
Entzun izan dut bortzetan,
Amodiuak bainerabilkan
Haren borta-leihuetan.

Baratzian zoin den eder
Julufreiaren lilia.
Aspaldian ez dut ikusi
Maiteñoaren begia.
Balinba gaixua orhoit ahal da
Non eman zautan fedia.

Orhoitzen nuzu, orhoitzen
Etzirautatu ahanzten; (etzait)
Madalena batek bezenbat pena
Bihotzian dut sofritzen;
Jaten dudan ogia ere
Nigarrez baitut bustitzen.

Eniza bada dohakabea
Mundu huntara sorthuia
Ezin nukeien amurrainari
Edatu nion amuia:
Damu mingaitzik jakin duela
Ene gogoko berria.

Amurraina nimiñoa,
Nimiño bainan abudo,
Ur zolatikan gaineraino
Ateratzen da oraino.
Neskatxak ezetz erranagatik
Mutilak amora lio.

Bazterretik bazterrera,
Itsasoaren zabala!
Ez dakienak erran lezake
Ni alegera naizela:
Ortzetan dizut irria eta
Bi begietan nigarra.