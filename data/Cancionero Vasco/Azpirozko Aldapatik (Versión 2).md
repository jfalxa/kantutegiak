---
id: ab-1717
izenburua: Azpirozko Aldapatik (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001717.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001717.MID
youtube: null
---

Azpirozko aldapatik Albisurekoan,
Triste neukan biotza angoxe basoan.
Zerk jarri ote zidan Albisu gogoan?
Iña ere burutik ezin kendu nuan. (Iñola)

Euria ta lañoa ari zan ugari;
Iñondik e Albisu ez baitzan agiri.
Guztiok bustirikan, beste bat eta ni,
Nora joan ez jakiñik sobra giñan larri.

Atajoz bilatzeko Albisu eder ori,
Bere aldamenetik jo zak Lekunberri.
Andikan jiratu ta ginaden ibilli,
Oiñ zapatetatik ez biziro garbi.