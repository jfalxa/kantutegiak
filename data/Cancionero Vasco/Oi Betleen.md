---
id: ab-2673
izenburua: Oi Betleen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002673.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002673.MID
youtube: null
---

Oi Betleen!
Hala egun zure loriak,
Oi Betleen!
Ongi baitu distiratzen;
Zugandik heldu den argiak
Betetzen tu bazter guziak.
Oi Betleen!
Ongi baitu distiratzen.