---
id: ab-1772
izenburua: Bertso Berriak Zeloz Beterik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001772.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001772.MID
youtube: null
---

Bertso berriak, zeloz beterik
Astera nua kantatzen.
Biotza triste dadukat eta
Etzait alegeratzen.
Damatxo batek obligatzen nau,
Ezta nor den deklaratzen.