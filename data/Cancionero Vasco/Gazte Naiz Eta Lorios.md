---
id: ab-1958
izenburua: Gazte Naiz Eta Lorios
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001958.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001958.MID
youtube: null
---

Gazte naiz eta lorios, izpiritua kurios,
Batez agrados, bertzeaz jelos, mutil gaztekin gau oroz.
Ama, ez nizan bada uros, gorputza ez baitut maluros?

Aita eta ama etzaten dire ganbera baten barnian;
Ganbera baten barnian eta dudarik gabe ohian.
Jelosiarik ez ukaiteko ni maiteñoaren aldian.

Athe onduan makila, eta leiho azpian zulubi;
Nere maitia handik sartzen da sekeretuan krudelki;
Sekeretuan krudelki eta amak oraino ez daki.

Ama ta alaba biak gogoeta onduan denbora joan eta,
Amak alaba konseilatzen du beria duen bezela,
Mutil gaztiekin ibil ez dadiela hura denboran bezala.

- Nere alaba, mutur zabala, zertan abila hi hola?
Ez dun ez pollit gauerdi ondoan mutil gaztekin lanbura.
Ala etzakinat othe abilan jorratu nahiz alorra.