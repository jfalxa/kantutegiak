---
id: ab-1586
izenburua: Debozionez Esanen Degu (Versión 7)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001586.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001586.MID
youtube: null
---

Debozionez esanen degu
Lenengo misterioa:
Iyo gaitzazu beitik zerura,
Birjiña biotzekua.