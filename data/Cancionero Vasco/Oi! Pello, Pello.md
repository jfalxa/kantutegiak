---
id: ab-2069
izenburua: Oi! Pello, Pello
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002069.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002069.MID
youtube: null
---

Oi! Pello, Pello,
Irun diat eta,
Yinen niza ohera?
- Astalkazan eta
Gero, gero, gero;
Astalkazan eta
Gero, gero bai.