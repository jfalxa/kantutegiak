---
id: ab-2059
izenburua: Itsasuan Ura Lodi (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002059.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002059.MID
youtube: null
---

Itsasuan ura lodi,
Ondorik ez du ageri;
Urzoñuetan ere badire
Bata bertzia iduri;
Baina nik hura ezagut niro
Izanik ere hamabi.

Ureko arraina ñimiño-ñimiño
Bainan seguro;
Ur zolatik gaineraino
Laster ingura bainiro.
Neskatxak ezetz errana gatik,
Mutila amora diro.