---
id: ab-1627
izenburua: Apirilian Gaua Da Labur
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001627.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001627.MID
youtube: null
---

Apirilian gaua da labur;
Ilargitxua berandu.
Inposuele da zu etzaituen
Ama Birjinak lagundu.
Sala klaruan ginenian jarri,
Biak begiak elgarri.