---
id: ab-1643
izenburua: Arno On On On Huntarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001643.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001643.MID
youtube: null
---

Arno on, on, on huntarik
edan dezagun gogotik,
Mozkor-horditzearen beldurragatik.
Pasa banindadi
Berriz hemen gaindi,
Irri egin nezoke barrika horri.
Diskurtsak eginez,
Tragoño zonbeit edanez,
Arnua edan, basua bete,
Irri eginez botoila hutsez,
Ustez hola hobe den zonbeit arratsaldez.

Igande arrats batean
Lau lagunen artean,
Botoila bana arno edan ginian.
Lauak gazteak ginen,
Gauaz arrantzatzerat joaiten ginen.
Hetarik batño zen,
Hura Pedroño zen.
Haren itzulipurdiak
Iduri iautzuriak;
Ikara bai lezake Turko handia.