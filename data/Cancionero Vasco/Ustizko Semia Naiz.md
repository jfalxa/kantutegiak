---
id: ab-2594
izenburua: Ustizko Semia Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002594.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002594.MID
youtube: null
---

Ustizko semia naiz,
Bizi naiz Arraizan.
Bertso bi para biar tut
Jendiak adi dezan.
Orainik gaztia naiz
Eskarmentu gutxi.
Damatxo gazte batek
Argatik nau utzi.