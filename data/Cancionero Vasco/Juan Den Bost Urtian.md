---
id: ab-2174
izenburua: Juan Den Bost Urtian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002174.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002174.MID
youtube: null
---

Juan den bost urtian baitut sentiduak,
Orain agertzen zaizkit neri eriduak,
Manifestatu dirade lengo sekretuak.
Egiya eta fedia, Pantxo medikua,
Zenbait umore gaizto daude gordetua (k),
Manifestatu dirade lengo sekretuak.