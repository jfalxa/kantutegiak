---
id: ab-2425
izenburua: Orroko Or Goyan Izarra (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002425.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002425.MID
youtube: null
---

Orroko or goyan izarra,
Errekaldian lizarra.
Etxe ontako nagusi jaunak
Urre gorrizko bizarra.

Urre gorrizko bizarra eta
Diamantezko espalda;
Berori bezin gizon noblerik
Erri ontan ez al da.

Orroko or goyan elorri,
Onduan jo ta erori.
Etxe ontako etxeko-andria
Ama birjiña dirudi.

Orroko or goyan errota,
Iriñ ongi iyota.
Etxe ontako etxeko-andria
Ama Birgiñaren debota.

Orroko or goyan iturri,
Ura pir pir ixuri.
Etxe ontako aingerutxoak
Amalau urre txintxarri.