---
id: ab-2114
izenburua: Aizek I, Mutil Mañontzi (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002114.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002114.MID
youtube: null
---

Aizek i, mutil mañontzi,
Ez al zakik pipa errez.

Mamirik ez desaferi
ezurri garra austen dez.