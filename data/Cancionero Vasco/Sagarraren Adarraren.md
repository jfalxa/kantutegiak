---
id: ab-3036
izenburua: Sagarraren Adarraren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003036.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003036.MID
youtube: null
---

Sagarraren adarraren
Puntaren puntan
Txoriñua zeguan
Kantari.
Bai, txiruliruli,
Ez, txiruliruli.
Nork aldatuko ote duen
Ontxu ori, eta ongi.