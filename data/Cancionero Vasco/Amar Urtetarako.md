---
id: ab-1595
izenburua: Amar Urtetarako
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001595.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001595.MID
youtube: null
---

Amar urtetarako oteketan asi;
Zarpa eta nekia, gutxi irabazi.
Aberatsa ezarten ez nuan ikasi;
Orain ere ez nazu idiano biz.