---
id: ab-2328
izenburua: Mila Zortzirehun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002328.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002328.MID
youtube: null
---

Mila zortzirehun ta berroi ta hamazazpi,
Bertso berri batzuek kopian ezarri.
Penetan diren gaixuen, oi! konsolagarri,
Aferak har ez detzaten sobera barnegi.

- Graziak galtzen dira, oi! denborarekin,
Eta istante huntan zure'ta nerekin.
Zu eta ni ezkontzeko biak algarrekin
Armadako berriak beharko tut jakin.

- Aise mintzo zira zu, oi! neure maitia,
Gizonak on duyela tropan ibiltzea.
Zu gabe egotea denbora luzea,
Neuretako, maitea, ai! zer dolorea.