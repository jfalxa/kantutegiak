---
id: ab-2014
izenburua: Gure Etxeko Neskatua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002014.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002014.MID
youtube: null
---

Gure etxeko neskatua
Buruan min, ta buruan min,
Bart arratseko tipulen saldak
Eztiola onik egin.
Ekarri medikua
Beraren errikua:
- Zer diozu zuk, Katalin?
Ekarri medikua
Beraren errikua:
- Zer diozu zuk, Katalin.