---
id: ab-2634
izenburua: Markesaren Alaba (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002634.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002634.MID
youtube: null
---

Markesaren alaba interesatua,
Marineruarekin enamoratua;
Diskubritu zayote beren sekretua;
Amoriyua zuten barrenan sartua.