---
id: ab-1614
izenburua: Anderia Gorarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001614.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001614.MID
youtube: null
---

Anderia, gorarik zaude leiohan.
Zure senarra behera dago Prantzian.
Hura hantik yin ditekenen artian,
Eskutaritzat har nezazu sahetsian.

- Ene senarra behera bada Prantzian,
Auzuak hurbil utzi derauzkit yuaitian;
Hura hantik yin ditekenen artian,
Eskutari beharrik ez dut etxian.

- Izan nuzu Prantzian eta Espainian,
Bai eta ere Anglaterra guzian:
Andere ederrik ikusi izan dut heietan,
Zure parerik etzen ene begietan.

- Eskutaria, ederki mintzo zira zu:
Zure etxian beharko duzu istatu; (ostatu)
Ganbera garbi, ohia xuri dukezu;
Nihaur ere aldian hurbil nukezu.

- Eskerrik aski dautzut, etxekanderia,
Zure senarra duzu ni baño gizon hobia;
Zazpi urtez harek eman deraut ogia,
Zortzigarrenian bere azpiko zaldia.

- Euri ondoan ura nahiz uherlo
Andre hok ere oro girade parlero;
Hitz hoietan bat zeraut laxero,
Ene senarra zutan truka ez niro.

- Arbadola bethetzen denian udarez,
Idi xuri erditzen denian xahalez,
Idi xuri erditzen denian xahalez,
Erranen dautzut zuria naitzen ala ez.

- Iragan duzu oi! bada denbora,
Eskeini duzu ganbera eta ohia,
Eta harekin zure gorputz propia.
Ez dea bada oi egia ?

- Eskutari, oi! begi urdin faltsia
Altxazak, altxa, oi! ezkerreko begia;
Altxazak, altxa, oi! ezkerreko begia;
Hi othe haitzen oi! ene lehen semia.