---
id: ab-1515
izenburua: Ahaire Zahar Huntan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001515.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001515.MID
youtube: null
---

Ahaire zahar huntan bi bertsu berririk,
Alegrantziarekin kantatu nahi tut nik;
Bihotza libraturik pena orotarik,
Desir nuien maitia baitut gogaturik.