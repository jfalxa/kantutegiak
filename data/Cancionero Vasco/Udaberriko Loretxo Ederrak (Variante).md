---
id: ab-2767
izenburua: Udaberriko Loretxo Ederrak (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002767.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002767.MID
youtube: null
---

Udaberriko loretxo ederrak
Erortzen dira neguan.
Beti gustura biziko denik
Ez da jayoko munduan.
Ez da jayo, ta ez da jayoko
Zu bezelako imajinik,
Zureganako amoriyua
Neri kenduko dedanik.

Adio, eta ni banijua.
Nai bazinduka etorri,
Amoriozko begi ederrak
Erakustera elkarri?
Amoriyua, amoriyua,
Baldin ez pada mudatzen,
Eguntxo on bat señalatua
Biok esposa gaitezen.