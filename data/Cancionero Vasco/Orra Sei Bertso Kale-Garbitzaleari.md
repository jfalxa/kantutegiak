---
id: ab-2423
izenburua: Orra Sei Bertso Kale-Garbitzaleari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002423.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002423.MID
youtube: null
---

Orra sei bertso kale-garbitzaleari,
Zeña bere izenez dan Jose Mari.
Errezatu lezake iru "Ave Mari",
Indarra etortzeko zaldi zuriari.
Urriki zait neri
Animali ori:
Falta du ugari
Egoteko lodi;
Kartoiakin egiña dala dirudi.