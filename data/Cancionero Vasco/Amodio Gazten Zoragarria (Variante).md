---
id: ab-1603
izenburua: Amodio Gazten Zoragarria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001603.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001603.MID
youtube: null
---

Amodioa, gazten zoragarria,
Ez deia bada pena, ene maitia
Ebats niro denboraren erdia,
Nahi eta behar balitz guzia
Zurekin pasatzeko,
Oi, ene bizi.