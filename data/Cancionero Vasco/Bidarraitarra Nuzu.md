---
id: ab-1787
izenburua: Bidarraitarra Nuzu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001787.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001787.MID
youtube: null
---

Bidarraitarra nuzu, bai nahi ere,
Etxeko seme ona segurki halere.
Bagire bortz haurride,
Nerekin sei gire,
Oro adiskide.
Dotearen gainetik samurturen ez gire.