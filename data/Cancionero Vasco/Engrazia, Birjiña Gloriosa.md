---
id: ab-1892
izenburua: Engrazia, Birjiña Gloriosa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001892.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001892.MID
youtube: null
---

Coro:

Engrazia, birjiña gloriosa,
Martir Santa Jaunarena:

Estribillo:

Gutaz ein zazu otoitz ona
Jesukristoren esposa.

Estrofas:

Arintzeko gure penak
Tristeon konsolagarri
Ziñan bitarteko jarri,
Ala nayerik zeru onak.
Il ziñan ta zein gustosa!,
Zaragozan: o ditxa ona!
Gutaz...

Daziano gogortuak
Dio zu azotatzeko,
Bestela adoratzeko
Aren jaungoiko falsoa.
Baña, o Santa balerosa!
Jauna zan zure aitormena.
Gutaz...

Kalez-kale arraztatzeko,
Ikusirik ain firme zu,
Tiranoak agintzen du,
Ala zu ikaratzeko.
Zure orduko defensa
Zan esku Jaungoikoarena.
Gutaz...

Esandako tormentuaz
Eziñik renegarazi,
Bular bat zizun ebaki,
Ala zu galtzeko usteaz.
Baña zu len bezain lasa
Jauna zan zure aldamena.
Gutaz...

Daziano arrituak
Zure pake soseguaz,
Josi dedila iltze gogorrez
Dio zure buru santua.
Ala egin ziñan gloriosa
Andik da zure fortuna.
Gutaz...

Zure eztalaye lagunak
Ain portitza ekusiaz,
Etsitzen due biziaz
Ziran bezala zaldunak.
Anparoa ta defensa
Zerutik zan guziona.
Gutaz...

Nork daki zer denboratik
Gure erriak du onratzen
Zure imajiña Arpelasen?
Jaungoiko onak ala naik
Zu zera Santa gloriosa
Gure amparoa ezin esana.
Gutaz...

Gure balle Larraungoa
Bere gaitz eta estutupenetan
Zugana die lasterkan
Birjiña gure gozoa:
Izan zaitea defensa
Beraz lur noble onetan.
Gutaz...

Euritean eguzkia
Leortean bendebala
Guzion anparoa ta itzala
Zera, birjiña egokia;
Sendagarri poderosa
Zera gure min guziena.
Gutaz...