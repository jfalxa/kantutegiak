---
id: ab-2316
izenburua: Batxaran Beltxa (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002316.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002316.MID
youtube: null
---

Batxakaran beltxa onddorik onddo,
Onddo eta goxo arana.

Orra ""Concepción"" konpañiakoa
Biar omen luke gaur arratseko laguna.

Eman bekio laguna
Berak goguan duena.

Berak goguan duena luke
""Sandalio Lartxeko"" jaun zilar ezpataduna.

Orien bien eztai-esposetara,
Errege zaldiz eldu da.
Urre dra, dra,
Urre dre, dre.
Erregina're bidian da.