---
id: ab-1539
izenburua: Aita Ta Ama Bizkaian Dauzka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001539.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001539.MID
youtube: null
---

Aita ta ama Bizkaian dauzka;
Bera or il da gaisua.
Salbajietan kunplitu zayo
Eriozako plazua.