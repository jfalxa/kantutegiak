---
id: ab-1789
izenburua: Bihotz Jesus Maitearena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001789.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001789.MID
youtube: null
---

Bihotz Jesus maitearena
Graziaren iturria!
Hemen naiz bihotz samurrena
Zure ganat etorria.
Oi, ezin aski laudatuzko
Bihotz garbi sakratua:
Noiz zaitut bada maitatuko
zerk nauka sorraiotua.