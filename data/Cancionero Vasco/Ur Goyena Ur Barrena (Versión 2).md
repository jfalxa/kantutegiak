---
id: ab-2579
izenburua: Ur Goyena Ur Barrena (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002579.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002579.MID
youtube: null
---

Ur goyena, ur barrena,
Urte berria, egun ona.
Onan dekargu ur berria.
Idekizazu ataria.