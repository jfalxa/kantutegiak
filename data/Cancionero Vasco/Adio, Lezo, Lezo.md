---
id: ab-1491
izenburua: Adio, Lezo, Lezo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001491.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001491.MID
youtube: null
---

Adio, Lezo, Lezo,
Amante neuria,
Billabona guzian
Parerik gabia.
Zugandik partitziaz,
Ai! au doloria!
Ez arren txurniatu,
Franziska neuria.