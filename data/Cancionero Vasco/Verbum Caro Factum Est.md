---
id: ab-2777
izenburua: Verbum Caro Factum Est
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002777.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002777.MID
youtube: null
---

Coro: Verbum caro factum est, )
Maria beti Birjiña gandik.)bis

Nativitate gabea
Ollarrak kantatu zuenean,
Ama Semeak maiterik zauden
Bi beso sanduen artean.

Coro: Verbum...

Kristo jin da mundura
Gu reskatatzera.
Disponi giten kristi-onak
Jesusen adoratzera.

Coro: Verbum...

Jose txatarrak berotzen,
Maria tximuen edatzen;
Ordu artan ari omen ziren
Seme on baten trojatzen;
Seme on baten trojatzen eta
Jesukristoren bestitzen.

Coro: Verbum...

Orienteko erregeak,
Izarraz giaturik,
Adoratu zuten Jesus Jaungoikoa
Eta gizon eginik.

Coro: Verbum...

Beleneko portalean
Portale famatua,
Zeren Kristo baitago sorturik
Gure Salbatzalea,
Gure Salbatzalea eta
Munduko Redentorea.

Coro: Verbum...

Iru artzayak joan ziren
Beleneko portalera;
Ama Birjiña ikusi zuten
Bere Semea bularrean.

Coro: Verbum...

Bigarren bisita
Iru Erregeak egin zuten;
Oro, inzienso eta mirra
Guziek ofrezitu zuten.

Coro: Verbum...

Konsideratu bear dugu
Gure biotz guziarekin
Zer otza pasatu zuten
Jesus bere Amarekin.

Coro: Verbum...

Ematekoz, eman zazu,
Emen den gauz karzu;
Portuan elurra duzu
Eta zanguak ozten zaizkigu.

Coro: Verbum...

Nativitate gabean
Zeruak idiki ziran;
Etxe ontako etxeko-andria
Konserba bedi. Amen.

Coro: Verbum...