---
id: ab-1920
izenburua: Eskualdun Xasurretatik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001920.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001920.MID
youtube: null
---

Eskualdun xasurretatik
Baginen hirur batallun.
Hartu behar gintuela,
Tun, tun, tuntuluntun,
Irun eta Oyartzun.
Tun, tun,
Eta beti tuntuluntun.

Gerla behar ginuela
Aspaldi zuan ezagun (zen ?)
Itsaso bazter hoitarik,
Tun, tun, tuntuluntun,
Zonbait peza tiro ilhun,
Tun, tun,
Eta beti tuntuluntun.