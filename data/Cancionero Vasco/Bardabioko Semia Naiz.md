---
id: ab-1732
izenburua: Bardabioko Semia Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001732.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001732.MID
youtube: null
---

Bardabioko semia naiz ta
Seme fortuna gabia.
Aranaztikan ekarri nuen
Dotiarekin andria.
Obe zukian ikusi ezpalu
Bardabioko atia.

Nere buruaz ez naiz oroitzen,
Zeran ez naiz bakarra. (zeren)
Azitzekuak or uzten ditut
Iru seme ta bi alaba.
Jaun zerukuak adi dezala
Aien amaren negarra.

Goizuetan bada gizon bat,
Deitzen diote ""Trabuko"".
Itzak ederrak, biotza faltso
Beinere etzaio faltako. (etzaizko)
Ark egin didan aberiguazioa (zidan)
Sekulan etzait antziko.

- Denbora batez oroitzen bayintz
Zer egin yuen Elaman,
Denbora artan ibiltzen yintzan
Yarraiki nikan Lesakan
Orain bano len egun yakien
Ni orain naguen atakan.

Goizuetako eskribu Jaunari:
- Eskarrikasko, barkatu;
Informaziuak onak diguzu
Iruñerako prestatu!
Mundu onetan edo bestian
Biarko dituzu pagatu.

Zorriak eta arkukusuak
Arrotontxuak nagusi.
Denbora batez erran biartzuenak
Au biar nuela ikusi!
Zeruko Aita, lagun nazazu,
Ezin nengoake igesi.