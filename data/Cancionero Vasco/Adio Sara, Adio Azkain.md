---
id: ab-1494
izenburua: Adio Sara, Adio Azkain
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001494.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001494.MID
youtube: null
---

Adio Sara, adio Azkain,
Berarekin Urruña,
Ez naiz berriz etorriko
Kapillaua Larruna.
Bertze baten billatzeko
Jainkoak dizula fortuna.