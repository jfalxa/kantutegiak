---
id: ab-3260
izenburua: Asta Txikirrin Txaku
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003260.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003260.MID
youtube: null
---

Asta txikirrin txaku,
Mendian elurre. (bis)
Maria Pepan aurre
Bizkar makurre. (bis)
La ra la...