---
id: ab-2576
izenburua: Ur Goyena Ur Barrena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002576.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002576.MID
youtube: null
---

Ur goyena, ur barrena,
Urte berri egun ona.
Egun onaren señalea,
Emen dakargu ur berria.

Etxeko andre zabala,
Leku oneko alaba,
Bidian gatoz informaturik
Limosnerua zerala.