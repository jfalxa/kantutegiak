---
id: ab-3051
izenburua: Gorputz Egokia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003051.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003051.MID
youtube: null
---

Gorputz egokiya ta zangua galanta;
Begira neskatx'orren dantzarako planta.
Ederki dantzatzen du ezkerreko anka;
Dama orrek ez biadu deusik ere falta. (baitu.