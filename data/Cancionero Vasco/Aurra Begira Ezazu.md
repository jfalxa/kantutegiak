---
id: ab-1702
izenburua: Aurra Begira Ezazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001702.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001702.MID
youtube: null
---

- Aurra, begira ezazu leyotik atarira,
Ia mutill oyek joan ote diran.
- Ez, or egongo dira zure jolaseri begira.
Gargeroz landan barrena igaro dira.