---
id: ab-2132
izenburua: Izarretan Den Ederrena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002132.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002132.MID
youtube: null
---

Izarretan den ederrena da
Arte-izarra goizetan.
Nik maiteño bat bakarra izan
Ta ezin ikus arratsetan.
Gizon gaztiak ez luke behar
Lotsarik andre gaztetan.