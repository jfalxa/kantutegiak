---
id: ab-3259
izenburua: Gorrittira Bidean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003259.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003259.MID
youtube: null
---

Gorrittira bidean
Otia latz dago;
Espartzin bakarrekin
Oñian otz dago.
Ankak galdu ezkeroz,
Gizonik ez dago.
Gorrittiarrain bildurrik
Uitzin ez dago.