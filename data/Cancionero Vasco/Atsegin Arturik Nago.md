---
id: ab-1689
izenburua: Atsegin Arturik Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001689.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001689.MID
youtube: null
---

Atsegin harturik nago dudanian jakin
Adiskide berri bat duzula zuk egin.
Zuazi haren-gana eta hartzazu atsegin;
Ez dezazula nitaz sekulan galdegin.

Zutaz galdegin gabe luzaz bizitzeko...
Sobera zaitut maite hola kitatzeko.
Ekarriren zaituzte erreporta franko; (dautzute)
Etzaitela izan flako hoien sinesteko.

Ez da erreportarik, ez arartekorik
Amodiok fidela gibel dezakenik.
Zazpi urte baditu biek hitzemanik;
Ez bainuke pensatzen holako xangrinik.

Nihor ez laite fida orain munduari:
Aintzinetik eder eta gibeletik irri.
Hala egin deraute aspaldian neri,
Erreportak eginik ene maitiari.