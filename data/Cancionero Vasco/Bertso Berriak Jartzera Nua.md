---
id: ab-1769
izenburua: Bertso Berriak Jartzera Nua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001769.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001769.MID
youtube: null
---

Bertso berriyak jartzera nua,
Konbeni diran moduan,
Gure kontuak nola diraden
Artu ditzaten goguan.
Ezkondu eta zer bizi modu
Pasatzen deran munduan;
Orra ederki eskarmentatu
Denbora juan ta onduan.