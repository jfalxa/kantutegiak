---
id: ab-2107
izenburua: Donibaneko Hiriak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002107.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002107.MID
youtube: null
---

Donibaneko hiriak,
Erdi erdian zubia.
Jaun errientak iten omen du
Haren gainian guardia,
Ia noiz ikusiren duen
Katalin begi zuriak.