---
id: ab-2396
izenburua: Notezia Jai On Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002396.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002396.MID
youtube: null
---

Notezia jai on bat badekargu,
Konsolatu.
Birgiña Semia jaio zaigu
Bart arratsian
Belerengo portalian, portalian,
Astotxo biren erdian.
Birjiña dago penatuba, flakatuba:
Nork emanen digu ostatuba?
Zuazte, zuazte gure etxera,
Gure etxera,
Birjiña Semiakin konsolatzera.
Biba, biba, biba Maria, biba Maria:
Egin dezu Semia miragarria,
Miragarria,
Zerubetan dagola gure begira.