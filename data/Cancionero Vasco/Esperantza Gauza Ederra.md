---
id: ab-1923
izenburua: Esperantza Gauza Ederra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001923.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001923.MID
youtube: null
---

Esperantza gauza ederra, segurtantza hobia:
Ide hortan bizi ahal da mundu hunen erdia.
Beti geroago hobeki uste beita presuna probia.
Batere astekua han dago geroko ogi haundia.

Ehun presuna arribatzen zen Kalifornian;
Lehenetan heldu ziren aberasterat lorian.
Hogoi ta bost ikusi dugu, gaixoak, nigarrak begian,
Bere etzantza egiten zuten lur hutsa gaiñian.

Eztute zeren hunat abia libratzeko penatik.
Eta gero hori erran nahi du, jende onak, zergatik
Ez baita nehor aberastu bere esku lanetik.
Holako guti athera sekulan Kaliforniatik.

Aita familiakoak, bere pener igesi,
Leku huntarat heldu dire zerbait nahiz irabazi.
Holako guti ez othe dugu Euskalherrian ikusi?
Jende onek behar tuzte heien familiak hazi.