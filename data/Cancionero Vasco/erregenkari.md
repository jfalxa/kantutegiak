---
id: ab-1902
izenburua: Erregenkari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001902.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001902.MID
youtube: null
---

Erregenkari, txarri belarri,
Deukonak ez deukonari.
Nik ez dekot eta niri.

Apalasiyo saldune,
Iru Erregen egune.

Sotzak eta paluek,
Txoribiyaren kontuek.

Mari Manuri eroan tziez
Iru ollanda katue(k).

Emongo bozu, emoizu,
Baldin emongo badozu.

Seure begire gagozen artien
egiten yaku berandu.

Apalasiyo ""Miri Montañe"",
Iru intxurtxu ta lau gaztañe.