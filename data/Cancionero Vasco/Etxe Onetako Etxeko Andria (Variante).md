---
id: ab-2158
izenburua: Etxe Onetako Etxeko Andria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002158.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002158.MID
youtube: null
---

Etxe onetako etxeko andriak
Ama Birjiña dirudi.