---
id: ab-2556
izenburua: Arantzazura Bidia Luze
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002556.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002556.MID
youtube: null
---

Arantzazura
Bidia luze;
Aruntz bidian
Nekatu.