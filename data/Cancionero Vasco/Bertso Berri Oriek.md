---
id: ab-1767
izenburua: Bertso Berri Oriek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001767.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001767.MID
youtube: null
---

Bertso berri oriek dute asipena.
Deklaratzera nua Gartxiaren pena:
Ez dute ongi entzuten andreen ordena;
Orain pagatu tuzte ortzelian pena.