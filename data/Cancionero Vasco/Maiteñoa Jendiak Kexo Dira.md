---
id: ab-2285
izenburua: Maiteñoa Jendiak Kexo Dira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002285.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002285.MID
youtube: null
---

Maiteñoa , jendiak kexo dira
Zuk eta nik ez degula etxerik.
Alako oiek emango al digue
Sobraturik dauzkienetarik.