---
id: ab-2198
izenburua: Kantari Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002198.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002198.MID
youtube: null
---

Kantari nago, baño biotzetik triste.
Nik miñik baduenik eztu iñork uste.
Munduan badirade zenbat presona sinple;
Nik bezala baleude, gaur ilko lirake.