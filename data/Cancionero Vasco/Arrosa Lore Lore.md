---
id: ab-3242
izenburua: Arrosa Lore Lore
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003242.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003242.MID
youtube: null
---

Arrosa lore lore
Denbora denian;
Gaztiak ankak arin
Soñua denian.
Tranka tranka...