---
id: ab-2376
izenburua: Nere Maitia Lo Ta Lo (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002376.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002376.MID
youtube: null
---

Nere maitia, lo ta lo,
Biyok egingo degu lo;
Zuk orain eta nik gero,
Biyok egingo degu lo.