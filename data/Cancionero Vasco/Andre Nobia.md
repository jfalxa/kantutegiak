---
id: ab-1616
izenburua: Andre Nobia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001616.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001616.MID
youtube: null
---

Andre nobia, )
Ilhe horia, ) bis
Edekizazu ataria. )
Badakarragu bada
Juan Martin bizar haundia.
Ilhe horia:
Edekizazu ataria:
Badakarragu bada ) bis
Juan Martin bizar haundia.

- Hemen heldu naiz, )
Bainan beldur naiz, ) bis
Penak izain ote ditugun maiz.)
- Penetatik libratzia
Elitzake izain gaitz.
Bainan beldur naiz
Penak izain ote ditugun maiz.
Penetatik libratzia ) bis
Elitzeke izain gaitz.

- Nere maitia, )
Ez egon sustoz: ) bis
Biziko gera munduan gustoz. )
Palazio bat inen dugu
Zekalez edo lastoz.
Ez egon sustoz,
Biziko gera munduan gustoz.
Palazio bat inen dugu ) bis
Zekalez edo lastoz.

- Eskalerak,
Gaztain-abarrez;
Gaineko sala, iratze berdez;
Balkonak ere
Basa-lizarrez;
Tellatua berriz, belar idorrez.
Lan horien iteko
Karriko Baigorriko Piarres.
Gaztain-abarrez,
Tellatua berriz, belar idorrez.
Lan horien iteko ) bis
Karriko Baigorriko Piarres.