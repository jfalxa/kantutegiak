---
id: ab-1913
izenburua: Eskoliersa Gaztia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001913.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001913.MID
youtube: null
---

Eskoliersa gaztia,
Begi ederren jabia,
Jaun errientari
Presta zakozu
Bazkal ondoan kafia;
Gero arek emanen derautzu
Ganbaren lezionia.