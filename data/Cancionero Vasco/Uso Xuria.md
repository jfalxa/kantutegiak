---
id: ab-2587
izenburua: Uso Xuria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002587.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002587.MID
youtube: null
---

Uso xuria, errazu
Nura yuaiten ziren zu.
Españiako bortuak oro
Elurrez betiak dituzu.
Gaurko zure ostatu
Gure yetxian baduzu. (bis.