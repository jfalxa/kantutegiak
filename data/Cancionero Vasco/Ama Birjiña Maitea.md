---
id: ab-1578
izenburua: Ama Birjiña Maitea
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001578.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001578.MID
youtube: null
---

Ama Birjiña maitea,
Grazia dizut eskatzen:
Bortz misterio gozoz beteak
Lagun zaguzu kantatzen. (bis.