---
id: ab-3221
izenburua: Eriño Batez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003221.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003221.MID
youtube: null
---

Eriño batez gero bestiaz 7. Kopeta
Orai eskua " 8. Sudurra
Orai ukondoa " 9. Aoa
Orai soindegiaz " 10. Ankak
Orai matelaz " 11. Belaunaz
Orai belarriaz " 12. Ipurdiz"