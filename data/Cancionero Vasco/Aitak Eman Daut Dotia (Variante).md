---
id: ab-1543
izenburua: Aitak Eman Daut Dotia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001543.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001543.MID
youtube: null
---

Aitak eman daut dotia, )
Neuria, neuria: ) bis
Oilo koloka, bere xitoekin,
Xerriño bat, bere umekin,
Tipula korda heiekin, heiekin.
(Guziak):
Tipula korda heiekin.

Otsuak yan daut xerria, )
Neuria, neuria: ) bis
Axeriak oilo koloka,
Arratoinak tipula korda
Adios ene dotia, dotia.
(Guziak):
Adios ene dotia.

Xo, xo, Mariaño,
Moxkorra yina nun gaur.