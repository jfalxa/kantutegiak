---
id: ab-1673
izenburua: Arribant Xuri-Gorri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001673.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001673.MID
youtube: null
---

Arribant xuri-gorri ñabarra,
Bi bürietan farfalla,
Bi bürietan farfalla eta
Erdian ürhe sagarra,
Sagar gorri ejerra.
Bai, baziozü Martin horrek (badiozü)
Ere sagar sagarra,
Maria horrek emana.