---
id: ab-2057
izenburua: Itsasoan Laño Dago (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002057.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002057.MID
youtube: null
---

Itxasoan laño dago
Oriyoko barraraiño.
Amak bere ume bat baño,
Nik zu zaitut maitiago.
Lo! Lo.