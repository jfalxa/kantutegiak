---
id: ab-3237
izenburua: Pasacalle
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003237.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003237.MID
youtube: null
---

Aita-semiak olara juan dire;
Am'alabak yokuan.
Berriz ere egonen dire
Matasariak zokuan.

Aitak tuntun, amak tuntun,
Alaba're tuntuna.
Oro tuntun izaiteko,
Artzan tuntun senarra.

Gazinako gazinako
Mudatu zen gogua.
Utzan ori, ar nazan ni:
Ni nau sekulekua.

Lara rai...