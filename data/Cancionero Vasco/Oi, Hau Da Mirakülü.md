---
id: ab-2739
izenburua: Oi, Hau Da Mirakülü
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002739.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002739.MID
youtube: null
---

Oi, hau miraküllü estonagarria!
Arima jüsto horren mündil'agertzia.
Hebentik junik zela bazizün urthia;
Egün berriz egin dü huna ützültzia.

Ama zizün elizan urthebürüzale,
Ahizparik gaztena etxiri so'gile.
Harek, ez ustez ihur bazela etxian,
Bere ahizpa hila ikhusten dizü han.

Hilak erraiten deio:-Bizi'ag'ixilik;
Ez deñat deüs eginen; ez egin nigarrik.
Ni hire ahizpa nün, mündü hunta jinik,
Zerbeit sokhorri nahiz orai hire ganik.

- Eta zer plazer düzü, ahizpa maitia?
Pürgatorioan deia zure izatia?
Zure hantik libratzeko banü photeria,
Segür egin nirozü en'ahal güzia.

- Au!, behar hitzait joan gañek'elizala,
Eta ama igorri Santa Barbarara,
Maidalenara eta hantik Sarrantzera:
Promisik, oi, ez egin ahalaz seküla!