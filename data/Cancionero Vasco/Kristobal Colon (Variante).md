---
id: ab-2651
izenburua: Kristobal Colon (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002651.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002651.MID
youtube: null
---

Kristobal Colon yin zen mundura
Hamalaugarren mendean,
Genes deitzen den hiri ttipira,
Italiako lurrian.
Aita ama onek giristinoki
Altxatu zuten haurrian.
Eskoletarik atera zen
Amabostgarren urtian.
Untziko mutil ttipi yuan zen
Kapitain baten menean.

Haur gazte arek iduri zuen
Itsas gainean sortua
Eta zerutik izpirituzko
Doayez aberastua.
Goizetik arrats eskuan zaukan
Lana edo liburua;
Eta ikasi gazte zelarik
Untzietako gudua,
Nun egin baitzen mutil ttipirik
Mariñel bat famatua.

Ikusi zuen liburuetan
Irakurtu zuenean,
Mundu hau zela bola aundi bat
Ibiltzen zena airian,
Itsasoak bere oñekin
Zabilala galtzarean;
Eta hur orrek bazuela
Leyorra bertze aldean,
Europanoek argiturikan
Hegoan edo iparrean.

Kritobal Coloni horren gainian
Etortzen zako gogora
Europanorik etzela izan
Mendealeko partera.
Hartan zuen deliberatu
Yuaitea alde artara,
Ziolarik: ez banaiz heltzen
Han diren leyorretara,
Lurra biribil denaz geroztik,
Yuanen naiz Indietara.

Kolonek zuen piaya hori
Karsuki deliberatu,
Bai eta hiru erresumetan
Laguntza zuen galdatu.
Italian zuten burlatu,
Portugalen enganatu,
Erregek eta erreginak
Españian fagoratu,
Berak bezala yainkotiarra
Baitzuketen ezagutu.

Isabelek erraten dako:
- Zertan daukazu burua?
Hango tokiez yabetu nayiz
Ote da zure gogua?
Colonek zuen bi itzez eman
Zuhurki errepostua:
- Yainkorik gabe bizi direnez
Ba dut urrikalmendua.
Eyer nahi deyet erakutsi
Ebanjelio saindua.

Isabelek ikusi zuen
Colonen fede bizia.
Erraiten dako:-Emain dazkitzut
Zuk behar duzun guzia:
Lauetan hogoi mariñel eta
Urte baten azkurria;
Bi untzi ttipi,""Pinta eta Niña""
Aundi bat ""Santa Maria"";
Eta Alonso deitzen duguna,
Untzi gidari nausia.

Egun Colonek mintzatu izan du
Untzi gidari nausia.
Erraiten dako:-Badauka bihotz
Untziaren gidatzeko
Mendealeko itsas beltzetik
Indietarat yuaiteko?
Ez baitakigu non edo nola
Noiz giren leyorrerako;
Enuke behar bildurte denik
Pidaya horri lotzeko.

- Yauna, zuk ez dakizu
Zoin arrazetarik naizen,
Eskualdun bati duzunian
Bihotz onean galdatzen.
Odol ontako gizonik
Ez da lanyerretan ikaratzen.
Usatia naiz ipar gainian
Balena arraintzan ibiltzen.
Lotsarik gabe ariko naiz ni
Zure untziaren gidatzen.

Hamalau mende lau hogoi eta
Hamabortz garren urtean
Agorrilaren lehen egunean,
Argia ari zenian,
Hiru untziak agertu ziren
Paloseko ibarrean.
""Santa Mariak"" zuen bandera
Bergandinaren gainian
Yesukristoren itxurarekin
Gurutze berde batian.

Agorrilaren lehen egunean
Egin zuten partitzeko.
Ikusliarrez ibai guzia
Betia zen argiko.
Mariñelak han zabiltzan
Itsasoari lotzeko.
Colonek zuen oyu bat egin
Belak oro goratzeko
Eta Yesusen izenian
Aiziari hedatzeko.

Ipar gaixuak agertu zuen
Itsasoa lanopetik
Eta untziak abiarazi
Zabalera ibayatik.
Colonek zuen azken agurra
Igorri untzi gaineti;
Bai eta laster ziren itzali
Ikusliarren bistatik,
Anitzen ustez behin betikotz
Españiako lurretik.

Zorioneko gau-egun batzu
Izan zituzten hastian,
Kanañako leyor aundia
Bistatik utzi artian.
Bainan gero sartu baitziren
Itsaso arras beltzian,
Zirain bortitzak, aize makurrak
Eta beti lanopian;
Itsasoan izan daiteken
Lanyerrik haundienian.

Bi ilabete lotsagarriak
Hala iragan zituzten.
Mariñelak nun hasi ziren
Nagusiari erraiten,
Ez bazakien lurrak nun ziren
Zertako abiatu zen?
Mesprexatua, laidostatua,
Zoro batentzat zaukatzen;
Untzi hartarik autikitzea
Aipatzen ere zakoten.

Goiz eder batez agertu ziren
Untziak elgarretara,
Eta Alonsok ikusi zuen
Xori andana bat ederra.
Colonek dio:-Xori gaixuak,
Zazte pausa lekura.
Guk ere hemen segi dezagun
Untzia alde hartara.
Orai segur naiz urbil garela
Nonbaiteko lurretara.

Arrats berian mariñeleri
Colonek deye erraiten:
- Iduri zaut urrun argi bat,
Ikus orduko itzaltzen.
Iran zaizte bergandinarat
Era handik ageri den.
Iran orduko ari zitzaizkoten:
- Ikusten dugu, ikusten.
Ohi lurra hurbil dugula
Guziek dugu sinesten.

Leyor hartara agertu ziren
Urriaren hamekako.
Colon zagon argia hastian
Otoitzian belauniko.
Mutil guziak etortzen zazko
Barkamendu galdatzeko.
Erraiten deye:-Bai, biotz onez
Derauziet barkatuko.
Sar zaitezte bozkarioetan
Orai lurrera yeusteko.

Soinekorik ederrenekin,
Bere ezpata gerrian,
Kristobal Colon yeutsi lehenik
Eta sartu leyorrian.
Mutil guziak aren ondotik
Dabiltza mundu berrian.
Toki hartan mendiska batek
Duken tokirik gorenian,
Eta landatu kurutzeño bat
Berek eginik lurrean.

Kurutze haren inguruetan
Belauniko ziren yarri,
Toki hartajo lehen agurrak
Zuzen zerurat igorri,
Eta karsuki eskerrak eman
Trinitate sainduari;
Ohore eta laudorioak
Mariñelen Izarrari,
Hainbertze lanyer lotsagarritan
Begiratu zituenari.

Colonek dio mariñeleri:
- Itzul zaitezte nigana,
Sinets zazie dudarik gabe
Oraiko nere errana.
Yakin zazie guzi hau dela
Ohi Yesukristorena.
Santo Salvador Españiak du
Toki hunen gozamena;
Hango Isabela erregiña da
Lur hunen yabe zuzena.

Colonek zuen Indietarat
Yuanen zela hitzeman;
Eta sartu zen ustegabean
Amerikako lurretan.
Geroztik hunat zonbait arima
Badabila toki etan!
Zonbait aberats ez dugu ikusten
Erresuma guzietan,
Yainkoari esker egin direnak
Guziz eskual errietan.