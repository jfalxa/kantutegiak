---
id: ab-2177
izenburua: Juan Den Urtian (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002177.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002177.MID
youtube: null
---

Juan den urtian Saran nitzala
Santa Gurutze pasatzen,
Lau kaskabete agertu ziran
Etziran asko lokartzen.