---
id: ab-1612
izenburua: Amoriua Xorua Dela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001612.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001612.MID
youtube: null
---

Amoriua xorua dela
Mundu guziak badaki.
Nik maiteñua bakar izan ta
Ura bertzek eramaki;
Ez nuke penik, bizi baledi
Neurekin baño obeki.

Azken bestetan egin nituen
Izarraren ezagutzak;
Denen artean ageri ziren
Haren begi urdin-beltzak;
Irri pollit bat egin baitzautan,
Piztu zauztan esperantzak.

Geroztik ere mintzatu gira,
Eia nahi ninduenetz...
Harek bietan baietz erranik,
Pentsa kontentu nintzanez!
Ez nuen uste haren agintza
Bethea zela gezurrez.

Orai bihotza urthua daukat
Urean gatza bezala.
Izar onaren argien ordain,
Izigarriko itzala!
Ohoin tzar batek, berriki joanik,
Nola naiteke kontsola.