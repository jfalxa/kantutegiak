---
id: ab-2718
izenburua: Enkargatutzen Zaitut (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002718.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002718.MID
youtube: null
---

Enkargatutzen zaitut, iltzen naizeneko,
Nere gorputz gaixoa errekistatzeko.
An ditutzu aurkituko zure zortzi letratxo:
Orduan ziradea desengañatuko?
Ez didazu geyago faltsua deituko.