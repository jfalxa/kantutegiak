---
id: ab-2912
izenburua: Fandango
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002912.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002912.MID
youtube: null
---

Pista zapatarijek
Amari negarrez,
Balentziana bagarik
Ezkonduko zanetz.
La lara la tra lara...

- Pista zapatarije
Ez egin negarrik:
Etzara ezkonduko
Balentziana barik.
La lara la tra lara...