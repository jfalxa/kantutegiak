---
id: ab-2545
izenburua: Txakur Beltz Txikitxo Bat (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002545.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002545.MID
youtube: null
---

Txakur beltz txikitxo bat faltatu zait neri;
Arras ezkutatu da, ezta iñon ageri.
Ez diot maldiziorik bota nai iñori:
Artaz baliatu dana, ondo bizi bedi.