---
id: ab-2362
izenburua: Blonda Ederra (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002362.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002362.MID
youtube: null
---

Blonda ederra, plazer duzia yin
Promenatzerat ene baratzera?
Bilduren ditugu ensaladiak,
Artixotak eta biper berdiak.
Biba arno huna, biba amodio,
Gauak eta egunak diraino.

Gorte egin dirot blonda xarmant ori,
Izanen dudanez ez dakit.
Bai, bai, izanen dut zer nahi den gostarik.
Dugun edan arno hun huntarik.
Biba arno huna eta amodio,
Gauak eta egunak diraino.