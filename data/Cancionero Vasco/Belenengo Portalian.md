---
id: ab-1749
izenburua: Belenengo Portalian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001749.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001749.MID
youtube: null
---

Belenengo portalian
Allegatu giñenian
Ateak yo ginituen
Amabiak aldean,
Jesus Maria, Maria y Jose.