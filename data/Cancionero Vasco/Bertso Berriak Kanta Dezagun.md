---
id: ab-1770
izenburua: Bertso Berriak Kanta Dezagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001770.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001770.MID
youtube: null
---

Bertso berriak kanta detzagun
Denok umore aundian,
Iru neskatxe gazteendako
Illarregiko errian.