---
id: ab-1694
izenburua: Atzar Gaiten Atzar Lotarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001694.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001694.MID
youtube: null
---

Atzar gaiten, atzar lotarik:
Gau huntan da Jesus sortzen.
Amodiuak garaiturik,
Guregatik da etortzen.