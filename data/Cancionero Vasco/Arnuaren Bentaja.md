---
id: ab-1646
izenburua: Arnuaren Bentaja
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001646.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001646.MID
youtube: null
---

Arnuaren bentaja nua kantatzera.
Pena aundiagorik munduan ote da?
Asi baño lenago iñor edatera,
Zenbat konbeni zaion, kontuak atera!

(Ez da filosoforik, ez teologorik
Ardoari neurria artuko dionik)
Gizon aundia ere ikusten ditut nik
Beren mozkorrak ezin disimulaturik.

Batian xutitu eta bestean (er)ori,
Nolapait etxera da gizon eder ori.
Gero maldizioka bere andreari,
Txapela kendutzeko tabernariari.

- Etzan zaite pakian, gizona, ofera (oera)
Egunaz zabaltziakin konpondiko gera.
Txapela pren'utzita etorri al zera?
Kreditu onen bat badegu sartzera.

- Preso sartu ninduten kupela batian.
Geroztik emen nago sei illabetian.
(Nik lagunduko zaitut amore on batian;
Begira, eztezen zuretzat kaltian).