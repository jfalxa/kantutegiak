---
id: ab-2782
izenburua: Xiribiri Xaboneko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002782.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002782.MID
youtube: null
---

Xiribiri Xaboneko,
Urteberri eguneko.
Aingeruek gara,
Zerutik eldu gara.
Eltxaurrak eta sagarrak
Ezpatuzie bota nahi,
Atai guziek austera...