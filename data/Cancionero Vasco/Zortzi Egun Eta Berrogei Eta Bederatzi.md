---
id: ab-2640
izenburua: Zortzi Egun Eta Berrogei Eta Bederatzi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002640.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002640.MID
youtube: null
---

Zortzi egun eta berrogei eta bederatzi,
Kristoren urtiak dira milla bat lenbizi.
Ejenplo egoki bat nai det erakutsi
Gureasoak umeak zer modutan azi.