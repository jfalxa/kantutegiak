---
id: ab-2193
izenburua: Kanbo Hortan Badira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002193.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002193.MID
youtube: null
---

Kanbo hortan badira lau neskatxa gazte,
Parerik ez dutela, berek baitiote.
Gisa hortan phentsatuz, arrazoina dute:
Tzarra khenduz geroztik, onik ez dukete.

Lauak hasiak dira muthil batheatzen,
Izen saindu ederrik zuten atheratzen;
Nehork ez baitzituen deusetan txerkatzen,
Berak hasiak dira orai atakatzen.

Gabaz ala egunaz, muthilen ondotik;
Iduri erhotuak dabiltza burutik;
Patrikurik ez dute batere herritik,
Nehork ez baitu nahi holako suyetik.

Iragan egunetan segereturekin
Landa zoko hoietan zauden artzainekin;
Artzainak yoan eta ez yakin zer egin,
Halere hobe dute bildotsik ez den yin.

Artzainak utzi eta zer diferentzia:
Senhartzat nahi zuten xokolet-eilia.
Estatu hortan bizi direnak badira,
Bainan, urtzo gaixoak, zuentzat gabia.

Mutxurdin egoiteaz egizue botu;
Ezkontzaz ezta behar muthilik bortxatu.
Zuen flore ederra behar dagastatu
Nihork ez baitu nahi orai phorogatu.

Estimagarria da Kanbon gazteria,
Bere manera oroz da xarmegarria;
Hola mintzo direnak aiphatu lau dia (dira)
Desohoratu nahi dutenak herria.

Orai khanbiatzeko dirade xedetan,
Hala hedatua da berria yendetan;
Batheatzeko lana utzi dute betan,
Hasi behar direla kantu berrietan.