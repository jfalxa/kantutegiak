---
id: ab-1850
izenburua: Dozena Erdi Bat Bertso
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001850.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001850.MID
youtube: null
---

Dozena erdi bat bertso Pazkoen aidian
Para biar nituke, beta dudanian.
Abilidaderikan banu mingañian,
Esplikatu nai nuke gaitzaren gañian.

Orain abiatzen naiz zerbaiten esplikatzen
Personak errez dela batzuetan miñ artzen.
Goazetik sukaldera ez naiz allegatzen


Iratze zimitzaren sarreraren fina!
Badaukat motibua oroitzeko diña.
Zangua mia bazen zagia aña egiña
Txikitik aunditara etotzen da miña.

Iratzeia sartu eta amalaugarrena,
Bazko maiatzeko eguna atera zidana
Zaku bat instrumentokin etorri zarena
Auxen da borondatia gure barberena.

Modu ortan armarekin eznaiteke asi;
Espantuz biarko du lenik gozarazi;
Nik maña egin eta an geiago azi
Gero eta makurrago nere ustez goazi.

Egunero eldu zaigu jaun au neregana,
Arri-erauntsi bezala aldu zaide pena.
Patzientzia artzia izain da oberena
Ala da borondatia gure Jabeana.