---
id: ab-2677
izenburua: Hots! Aingürieki
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002677.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002677.MID
youtube: null
---

Hots! Aingürieki,
Arkanjelieki,
Gaur khantatzera.
Gure Erregiaren,
Jinko Semiaren
Adoratzera.