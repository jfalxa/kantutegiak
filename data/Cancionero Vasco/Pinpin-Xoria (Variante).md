---
id: ab-2682
izenburua: Pinpin-Xoria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002682.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002682.MID
youtube: null
---

Pinpin-xoria,
Xori moko-gorria.
Jan, jan, koñata
Xori-salsa on bat da.