---
id: ab-2736
izenburua: Mononaren Kantuak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002736.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002736.MID
youtube: null
---

Mononaren kantuak
Akoto paratu
Eskaletaik erori eta
Mononak muturrek autsi tu.