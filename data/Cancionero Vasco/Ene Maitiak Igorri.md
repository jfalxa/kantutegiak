---
id: ab-1891
izenburua: Ene Maitiak Igorri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001891.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001891.MID
youtube: null
---

Ene maitiak igorri
Segeretian mila goraintzi:
Ikustera yinen dela laburzki.
Bizi nadin alegeraki;
Gorputzez urrun izanagatik,
Bihotzez dela beti enekin.

Ote da zeruan izarrik
Nik maite dutan haren parerik?
Lurrian sortu izanagatik,
Argitzen du nola zerutik.
Harek ez du mundu huntan parerik,
Ez estona maite badut nik.