---
id: ab-2223
izenburua: Khakuetako Botxü
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002223.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002223.MID
youtube: null
---

Khakuetako botxü, oi paregabia,
Behadi orai zer krima hik egin dian:
Gure odoletik thonatü izana,
Hanitxeko hezürrak hautse deizkiala.