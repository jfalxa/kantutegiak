---
id: ab-2034
izenburua: Iguzki Eta Izarra (Versíon 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002034.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002034.MID
youtube: null
---

Iguzki eta izarra, illargia salbo,
Zu bezelako damarik munduan ez dago.
Oi, ni zuri begira xoraturik nago:
Amoriorik badezu, mintza zaite klaro.