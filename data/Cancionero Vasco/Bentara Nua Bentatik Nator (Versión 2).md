---
id: ab-1759
izenburua: Bentara Nua Bentatik Nator (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001759.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001759.MID
youtube: null
---

Bentara nua, bentatik nator,
Bentan da neure gogua;
Musuan gerrian pikuak franko,
Musuan usai gozua.