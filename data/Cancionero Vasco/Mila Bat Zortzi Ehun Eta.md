---
id: ab-2325
izenburua: Mila Bat Zortzi Ehun Eta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002325.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002325.MID
youtube: null
---

Mila bat zortzi ehun eta
Lauetan hogeietan,
Bertso berri hauk eman dire
Aire zahar batean.
Gure arbaso Kantabreak
Lo baitaude lurpean:
Ez othe dire atzarriko
Aire hau aditzean.