---
id: ab-2593
izenburua: Maite Nauzula Diozu (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002593.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002593.MID
youtube: null
---

Ez nau izitzen elhurrak
Gutiago gau ilhunak.
Maiteagatik pasa netzazke
Bai gauak eta egunak,
Bai gauak eta egunak,
Desertu eta oihanak.

Eri nagotzu bihotzez,
Erraiten dautzut bi hitzez.
Sukar malinak harturik nago
Ez zintuzkedan beldurrez.
Maitia, senda nazazu,
Hil ez nadin bihotz-minez.

- Maite nauzula diozu;
Nik ere maite zaitut zu.
Diozun bezin maite banauzu,
Elizaz feda nazazu;
Elizaz feda nazazu:
Gero zurea naukazu.