---
id: ab-2481
izenburua: Señaleño Bat Emantzentudan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002481.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002481.MID
youtube: null
---

Señaleño bat emantzentudan
Partitzian nik zuri,
Arrazoña dudan bezala,
Erraiten dauzut zeorreri,
Puskak nai tut ikusi,
Puskak nai tut ikusi.