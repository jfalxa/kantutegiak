---
id: ab-1625
izenburua: Antzarak Daude Karrankan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001625.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001625.MID
youtube: null
---

Antzarak daude karrankan
Bayonako karrikan;
Kortarigaray zagar ori
Semian minez marraska.