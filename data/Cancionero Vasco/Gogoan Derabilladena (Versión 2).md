---
id: ab-1974
izenburua: Gogoan Derabilladena (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001974.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001974.MID
youtube: null
---

Gogoan derabilladena neronek iduki,
Nik orain Madalentxo ori gogoan iduki.
Sarri, sarri emango nioke Batista, Apezeneti,
Ondotxo goxa dezala besoan poliki.