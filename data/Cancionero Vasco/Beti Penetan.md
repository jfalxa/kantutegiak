---
id: ab-1779
izenburua: Beti Penetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001779.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001779.MID
youtube: null
---

Beti penetan, beti penetan
Bizi naiz mundu unetan.
Egunaz zerbait alegratzen naiz
Gabaz beti penetan:
Neureganako amoriua
Juan zitzaitzun batetan.