---
id: ab-1622
izenburua: Antton Eta Maria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001622.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001622.MID
youtube: null
---

Bertsu berriño batzuk bainua emaitera,
Banka huntan sujetak ardura baitira.
Antton eta Maria xangrinetan dira:
Ustegabean gaixoak tronpatu baitira,
Elkharri soberaxko Konfidatu dira.

Mayatzaren lehen larunbat hasian (atsian)
Gertatia istoria Bankako herrian:
Antton eta Maria baitziren gustian
Amodio beretan baitziren ordian
Ohe berriño hori agertu artian.

Etorri zitzekonian gizona etxera,
Nola gertatia zen orduko affaira:
- Agur, Antton, Heldu niz zure ikustera,
Aita baten izena baitzazi hartzera: (baitzoazi)
Barda sortu da zure semea mundura.

Anttonek errepostu kopeta goranik: (gorarik)
- Ezdut arrangurarik, libro niz hortarik;
Jainkoak baino gehiago nik ez dut hobenik.
Bi urthe pasatiak baititut jadanik
Elherik eztudala horrekin eginik.

Goguetan zagon Antton arrazoinekilan
Zer egin behar zuen neska horrekilan
Kalkulak egin ditu etxian andrekilan:
- Hail, elhe xuritzera gizon batekilan,
Marien etxeko Zimon har-zak eurekilan. (Manexeneko)

Antton hori badoa, harturik gizona,
Affairen xuritzera partidaren-gana.
- Zer komissionik igorri dahana? (komissione hik)
Hik gaitzki egin badun, nik duta hobena?
Komissioniak igortzkin partzuer horien-gana.

- Partzuer horier ez diat deus erraitekorik;
Bakarrik ditutanak denak hiretako.
Hik baduka kopeta gauza horien erraiteko?
Orai ezbainiz trenpian mokoka hartzeko;
Haur hori tokatzen duk hire eta enetako.

Urrikalgarri dira holako presunak
Berek ezin arraiñga beren ontasunak.
Hori ezdu egiten amodio finak.
Elkharri beha bite konbeni direnak
Undarrian hori du malezian denak.

Antton, etzazula egin odol gaixtorikan,
Hori gertatia da orai engoitikan.
Hortarik ez daiteke egin bertzerikan.
Frankotan aditu duk zaharretarikan:
""Suik ez den lekutik, ez dela kherikan"".

Hamar bertsu eman ditut orok kantatzeko
Sujetikan direnen exenplu hartzeko.
Othoitz egin dezagun holakuendako
Har dezagun xede bat ihes egiteko
Kalte handia baita arimarendako.