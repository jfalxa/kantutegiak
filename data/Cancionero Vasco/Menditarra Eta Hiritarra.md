---
id: ab-2663
izenburua: Menditarra Eta Hiritarra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002663.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002663.MID
youtube: null
---

Gizona malurus da
Kanpañan bizi dena.
Hirian egon bedi,
Hirus nahi duena.
Hiriko libertimenduek
Aintzarazten dituzte
Penarik haundienak.
Hirian dire munduan diren
Gauzarik ederrenak,
Agradosgarrienak. (bis)

Neri etzait gustatzen
Egoitia hirian,
Zeren sortuz geroztik
Bizi bainaiz mendian.
Bizkitartian iduritzen zait
Ongi kausitzen naizela
Naizen posizionian,
Naiz ez dudan ainitz brillatzen
Ene ardien erdian,
Galtza txar batzuk soinian. (bis.