---
id: ab-2270
izenburua: Lotxo Bat Egidazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002270.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002270.MID
youtube: null
---

Lotxo bat egidazu,
Neure aingerua,
Asi gau-erditikan,
Argirañokua.