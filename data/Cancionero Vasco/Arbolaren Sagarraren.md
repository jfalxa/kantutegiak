---
id: ab-3048
izenburua: Arbolaren Sagarraren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003048.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003048.MID
youtube: null
---

Arbolaren sagarraren
Puntaren puntan,
Xoriñoa zegoen kantari:
Bai, txiruli,
Ez, txiruli.
Nork kantatuko du soñu ori.