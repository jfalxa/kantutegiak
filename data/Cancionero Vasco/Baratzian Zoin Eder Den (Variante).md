---
id: ab-2189
izenburua: Baratzian Zoin Eder Den (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002189.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002189.MID
youtube: null
---

Baratzian zoin eder den
Julufraiaren loria!
Itsaspetik lañopetik
Ateratzen da uria.
Baldin gaixua orhoit ahal da
Non eman zautan fedia.

Orhoitzen nauzu bai, orhoitzen
Ez dauratazu ahanzten:
Madalena batek bezen bat
Munduan baitut sufritzen.
Jaten dudan ogia ere
Nigarrez dizut bustitzen.

Kalla kantuz ogipetik
Uztail-agorriletan.
Maitien gandik nindoalarik
Aitu izan dut bortzetan.
Oxala aitu izan banu
Haren borta-leihoetan.

Bazterretik bazterrera
Au munduaren zabala!
Eztakienak erran lezake
Ni alegera naizela.
Ortzetan dizut irria, eta
Bi begietan nigarra.