---
id: ab-2145
izenburua: Jesukristori Kendu Ezkero (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002145.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002145.MID
youtube: null
---

Jesukristori kendu ezkero
Pekatuekin bizitza,
Baldin ezpadet negar egiten,
Arrizkua det biotza.