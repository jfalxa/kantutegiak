---
id: ab-2157
izenburua: Zorionekoa Izan Dedilla
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002157.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002157.MID
youtube: null
---

Zorionekoa izan dedilla
Etxe onetako jendia.
Zeruko Jaunak eman deiela
Osasuna ta pakia.
Etxe ontako nexka ederra
Arpegi zuri-gorria:
Zure masalla iruditzen zait
Krabeliñaren orria.