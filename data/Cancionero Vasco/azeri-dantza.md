---
id: ab-3003
izenburua: Azeri-Dantza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003003.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003003.MID
youtube: null
---

Yalpargatak urratuta,
Zapatarik ez,
Erniuan gelditu naiz
Oñeko miñez.

Auxen da egiya,
Zortziko berria,
Iru txiki ardorekin
Librako ogia.