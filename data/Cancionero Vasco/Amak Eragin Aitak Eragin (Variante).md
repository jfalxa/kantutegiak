---
id: ab-1545
izenburua: Amak Eragin Aitak Eragin (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001545.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001545.MID
youtube: null
---

Amak eragin, aitak eragin;
Amak eragin didazu,
Amak eragin didazu, maitia,
Berdin neria zera zu.