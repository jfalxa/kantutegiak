---
id: ab-1571
izenburua: Alkandora Zar-Zar Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001571.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001571.MID
youtube: null
---

Alkandora zar-zar bat soñian derama,
Ogei ta amar pedazo neronek emana.
Beste bat ere badet eztopezko fiña,
Kalabazaren alde koñatak egiña.

Betetzen dedanian pixka bat zorrua,
Gauetan asitzen naiz bulla ta orrua.
Erdipurdi jartzian buruba arrua,
Ezin atzendu dedan griña diabrua.