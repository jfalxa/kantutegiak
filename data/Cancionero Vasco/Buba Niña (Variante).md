---
id: ab-2797
izenburua: Buba Niña (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002797.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002797.MID
youtube: null
---

Buba, niña,
Lotzeko mina
Lotzen bada salduen

Saldünak nun dira?
Atharratzen behera.
Hatzamanen dügia
Bai, bai, aski laster bagia.