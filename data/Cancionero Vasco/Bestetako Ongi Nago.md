---
id: ab-1777
izenburua: Bestetako Ongi Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001777.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001777.MID
youtube: null
---

Bestetako ongi nago,
Arropa onez beterik;
Oinetikan bururaiño
Berritan panpinaturik.