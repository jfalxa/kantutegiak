---
id: ab-1888
izenburua: Ene Muthilik Ttipiena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001888.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001888.MID
youtube: null
---

Ene muthilik ttipiena,
Hik ematen deraut pena.
Habil harat, habil hunat,
Mastainoaren puntalat,
Ageri denez untzirik, edo
Ageri denez leihorra.

- Nagusia, heldu nuzu
Bihotza arras tristerik:
Ez dut ikusi untzirik,
Ez eta ere leihorrik.
Jan behar balin banauzue,
Hil nezazue lehenik.

(se repite 1)

- Nagusia, heldu nuzu
Alegerarik bihotza:
Ikusi dut leihorra,
Katalonia'ko hiria;
Zure arreba Santa Klara
Leihuan josten ari da.