---
id: ab-1619
izenburua: Aniz Indako Nausi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001619.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001619.MID
youtube: null
---

Aniz Indako nausi desobedientia,
Ongi sobera zayo mundu untan jendia.
Yustizira yuan eta mintzatu da guapo:
Habitante sobra dela ongi bizitzeko.

Alkatiak errespuesta, utzi gabe geroko:
- Zer egin biar da bada? Zenbait il biarko?
- Aniz ontan badegu oi milla portuna,
Urbil ontan gadenak ikusten duguna.

Orrek paga dezako gobernua ona
Diruz ezta pagatzen orlako gizona.
Yabia badaukagu ori baño obia,
Arek eraman du gure bakantzia. (eramain)

Aitak ekarri du amari berria
Ezkon-azi dezala alaba Maria:
Bi koltxon, bi naparra, kutxa bat berria;
Agudo egin biar du etxeko aldia.