---
id: ab-2791
izenburua: Aitatxi Amatxi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002791.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002791.MID
youtube: null
---

Aitatxi, amatxi, zatozte
Opill adar aundi batekin,
Sopez ase gaitezi.
Opilla dago arrian
Bere alki nabarrian.
Ttantto, ttantto, ttorropillatto. (bis)

Uriya, muriya,
Errotako zaldi zuriya.
Uriya, muriya,
Arrazketan galtza zuriya.