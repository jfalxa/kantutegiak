---
id: ab-1725
izenburua: Baldumeko Oihana
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001725.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001725.MID
youtube: null
---

Krima bat egin dit, oi! ezin ükhatia,
Ezpiritü gaistuaz benintzan phusatia.
Malerus izan da ene zorthia,
Aspaldi danokan banian malezia,
Zor baten intresa benian doblatia,
Heriotze batez dit phakatia.

Adiskide faltsü traidore infamia,
Hi hizala kausa bethikoz nük galdia,
Galeren zolan bürdüñaz kargatia.
Horra arren zer den bestetan fidatzia.
Zühür denak edük bereki segretia
Eta untsa giltza phensamentia.

Krima egin beno lehen nintzan largatü,
Ustez adiskide bat nian haitatü
Behar nündiala lagüntü.
Hitzeman zereitan bai eta ere jüratü,
Segretü huna ere bai garantitü;
Traidore betzait zerbützatü.

Baldumeko oihana eta mendi handia
Gaiaren mineko pasaje lazgarria:
Arma balaz kargatürik han dit postagia.
Jin izan zeitanian ene ixterbegia,
Ezari niozün püñala bulharrila,
Arma balaz kargatürik estumakiala.

Oihüka hasi zen othoitzen Jinkuari
Azken oren hartan egin lezon argi,
Nurk ehaiten zian nahi ziala ikhusi.
Nihaurk erran neion: -Hire gana jin nük ni
Zor haren phakatzeko ideiareki
Akzione beitük hik eneki.

- Ohart hadi, ohart, gazteko lan eginaz
Makhila-khalduz hautxe heinztan ezürrak
Eta jauzerazi bürüko hüna.
Ni nük ni sujeta orai jinik hire gana
Eriotze batez zor haren phakatzera
Hori hartze beitük hik enekila.

- Baliatü hintzan ene haurtarzünari;
Bestek ogena eni hintzan mendeki.
Ordüko kolpiak zendi tiat nik bethi:
Phakatüko nahi deiat bizia idoki:
Kito gütükek boronthatez bethi,
Hedallo benündian hik ützi.

- Ni nahi nük izan hi beno unestago:
Badükek denbora Jinkuaren othoitzeko.
Belhariko hadi hil gabe orano:
Hik eheitan eman denborarik hortako,
Nik phensatü gabe benian kolpü franko,
Ützi benündian hilarentako.

Othoitü zianian aski gure Jinkua,
Degollatü neion erdiz erdi lephua.
Etzena othian lazgarri hura!
Ordian fini zen haren eskerniua,
Eta ni baratü bunkuen banua;
Nuiz izanen da ene aldi huna!

Kondenatürik niz heben hamar urtheren
Bürdüña-kargak titik khantatüren,
Zorren phakatziak beteik kausatzen,
Sendimentü gabe zeren ezpenintzen,
Sogizie arren zorthia nula nien:
Persumia eztenez aisaki galtzen!

Finitzen baniz ni peritzez galeretan,
Phena ba badiket ene entrañetan,
Zer den ene erranen beitüt bertan
Etsai bat hil dit eta bestia bizi
Eninte mendeka manadikatü hari
Haren ehaitera ezin hebeti jalkhi.

Jinkuak zor baleit hebetik jelkhitia,
Txerka hindiot ene ixterbegia
Egiteko ene eginbidia.
- Hire kausaz diat odola sühartia.
Kasü eman ezak ene arrakuntria:
Erran behar dükek ""adio, mündia"".

Azken breset huntan ene lagünentako
Zerbütxatüko niz zien etsenplütako.
Bena ez imita süjeta holako:
Begira ezazie libertatia oso:
Etziaiela jar ni bezala esklabo,
Nihaur aski beniz hola galtzeko.