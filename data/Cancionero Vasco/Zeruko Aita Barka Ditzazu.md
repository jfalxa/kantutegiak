---
id: ab-2635
izenburua: Zeruko Aita Barka Ditzazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002635.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002635.MID
youtube: null
---

Zeruko Aita, barka ditzazu
Pekatarien pausuak:
Kantabriako deskalabruak
Dirade lastimosuak.
Arbola eder bat dela medio
Jartzera nua bertsuak.
Milla urtean mantendu gaitu
Lau probinzia osuak.