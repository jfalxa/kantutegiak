---
id: ab-2584
izenburua: Urtiak Il Bi Ditu Alegrerik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002584.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002584.MID
youtube: null
---

Urtiak il bi ditu
Alegrerik:
Mayatza, Apirila,
Loraz beterik.

Asentziuak eta
Korpuz Kristiak,
Eldu dirade Paskuak
Mayatzekuak.