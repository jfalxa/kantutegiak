---
id: ab-1739
izenburua: Bart Arratsian Ari Nintzen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001739.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001739.MID
youtube: null
---

Bart arratsian ari nintzen
Bakar bakarrik irauten; (iruiten)
Atera kox, kox, jo ziran eta
Etzinion kasorik egiten.