---
id: ab-1999
izenburua: Gorriti Txantonian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001999.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001999.MID
youtube: null
---

Gorriti Txantonian, bai, Ave Marie,
Dantzarako bear omen du gonatxo xurie.
Paratuko nioke usatzen den sarie.
Nere begira dago plazan dantzarie.
La ra la...