---
id: ab-1684
izenburua: Asto Zarra Elbarrie
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001684.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001684.MID
youtube: null
---

Asto zarra elbarrie, )
Munduan parragarrie: )bis
Mundu guziak badaki
Nik astua dutena,
Ataundar batek emana,
Gazte denbora juana.