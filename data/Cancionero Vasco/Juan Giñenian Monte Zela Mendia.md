---
id: ab-2179
izenburua: Juan Giñenian Monte Zela Mendia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002179.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002179.MID
youtube: null
---

Juan giñenian juan giñenian
Monte zela mendia,
Ikusi nuen ikusi nuen
Pajaro zela txoria.
Tiratu nion tiratu nion
Piedra zela arria,
Atera nion atera nion
Ojo zela begia.
Botatzen zuen botatzen zuen
Odol sangre gorria,
Txo txo txo txo txoria
Txoria txistularia.