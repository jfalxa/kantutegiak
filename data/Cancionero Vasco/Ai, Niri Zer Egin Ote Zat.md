---
id: ab-3376
izenburua: Ai, Niri Zer Egin Ote Zat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003376.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003376.MID
youtube: null
---

Ai, niri zer egin ote zat?
Oñez ibiltzen aztu zat!
Buruba jaso ezin det;
Lurra idoro ezin det;
Triste dut biotza guztiz
Edan dezadan ea berriz...

Ai, au erari gozua,
Zerutik jetxitakua!
Au emen ikusirik,
Nago ni txoraturik;
Biotza daukat tristerik:
Edan dezadan ea berriz...

Ardo pikaro gaiztua,
Ik galdu didak buruba,
Eran det txit gogotik
Ardo sendo onetatik;
Jakin banuben lenago,
Edango nuben geyago.

Noe, gizon adituba,
Zerorren pensamentuba
Izan zan txit andiya;
Paratzia mastiya.
Zu zera ardoren autore,
O zorioneko Noe!

Erra zayogun Jaunari
Demala indar zepari,
Libra dezala arritik,
Aize legor gorritik;
Eska zayogun berari
Demala ardo ona ta ugari.

Mediku, barberu guztiyak
Erremediyo andiyak
Dituzte bestientzat;
Baña beti eurentzat
Erremediyorik onena,
Ardorik al danik zarrena.