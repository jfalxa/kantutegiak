---
id: ab-1859
izenburua: Edan Lezak Goizean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001859.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001859.MID
youtube: null
---

Edan lezak goizean xuritik, ederki;
Arratsaldian gorritik, obetik.
Ala egiten diet nik eta ik begik,
Baldin al badik: on egingo dik.