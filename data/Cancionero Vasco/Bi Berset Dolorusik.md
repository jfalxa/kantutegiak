---
id: ab-1780
izenburua: Bi Berset Dolorusik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001780.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001780.MID
youtube: null
---

Bi berset dolorusik nahi dizüt khantatü,
Plazer düzielarik, jente hunak behatü.
Berreogei ta bi urthez izan niz persegitü,
Nun etzaitadanian familia jelostü;
Orai oro ützirik, nahi nüzü phartitü.