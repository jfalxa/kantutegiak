---
id: ab-1905
izenburua: Errekonduan Lizarra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001905.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001905.MID
youtube: null
---

Errekonduan lizarra,
Aren onduan izarra.
Etxe ontako naguzi jaunak,
Urregorrizko bizarra.

Urregorrizko bizarra
Eta zillar labratuz ezpada;
Errial txikia egiña dauka
Elizarako galzada.