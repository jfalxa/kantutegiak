---
id: ab-1918
izenburua: Eskualdun Garbi Batek (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001918.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001918.MID
youtube: null
---

Eskualdun garbi batek ote dauka deusik,
Libertatia bezein gauza aberatsik?
Ez, ez harentzat ez da deus hain baliosik;
Zorion hartaz baizik ez baitu ametsik:
Hura gabe ez duke bizitze hurosik.