---
id: ab-2670
izenburua: Nik Zü Maite, Zük Ni Ez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002670.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002670.MID
youtube: null
---

Nik zü maite, zük ni ez! )
Ene maitia, zeren ez? )bis
So egidazü bi begiez,
Eta maite bihotzez.