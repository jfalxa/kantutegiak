---
id: ab-1621
izenburua: Anton Estebaneko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001621.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001621.MID
youtube: null
---

Anton Estebaneko,
Lauda dezaket Yainkoa.
Noizpait, noizpait ez ote tuk
Arinkeriak utziko?
Arinkeriak diren lekuetan,
Bide duk zerbait ondik.