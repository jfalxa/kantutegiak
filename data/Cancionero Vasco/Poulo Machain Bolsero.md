---
id: ab-2687
izenburua: Poulo Machain Bolsero
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002687.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002687.MID
youtube: null
---

Poulo Machain bolsero,
Wellington ""remos"" portero,
Jose Muñua, jaun ori berriz,
Maestro de mortero.

Pareta egin dute fuerte,
Altura ere bastante;
Iruren brazak pasatzen dire
Zabal eta luze.

Baitugu iru sobrestante
Frantzia eta Espaiñia arte,
Irurak iru lekutarako
Jinkoaren parte.

Oiu'ten digute fuerte
Zintzurrak larrutu arte:
Egile txarra, manatzale on
Jinkoaren parte.