---
id: ab-3257
izenburua: Allitik Eraman Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003257.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003257.MID
youtube: null
---

Allitik eraman du
Arruitze zakurre;
Ez digute pagatu
Lan-bide makurre.
Bost aldiz eman diot
Eskutik apurre.
Ure derabillen artzai
Kapelu makurre.