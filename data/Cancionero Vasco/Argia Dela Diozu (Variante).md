---
id: ab-2428
izenburua: Argia Dela Diozu (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002428.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002428.MID
youtube: null
---

Argia dela diozu, gauerdi oraino ez duzu.
Zukilako denbora luze iduritzen zaitzu.
Amodiorik ez duzu: orai zaitut ezagutu.