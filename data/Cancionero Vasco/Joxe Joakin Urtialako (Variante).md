---
id: ab-2172
izenburua: Joxe Joakin Urtialako (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002172.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002172.MID
youtube: null
---

Joxe Juakin Urtialako,
Lurrian auspez jarririk,
Bizkarra ere makurtu zaitzu,
Bakartasunen beterik.
Albiratua gertatzen nauzu
Ez dezula artzen andrerik.