---
id: ab-1509
izenburua: Agur, Aingeruen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001509.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001509.MID
youtube: null
---

Agur, aingeruen
Eta zeru-lurren
Erregina puxanta.
Ederra zare guzia,
Histen duzu iguzkia:
Zoin zare distiranta.