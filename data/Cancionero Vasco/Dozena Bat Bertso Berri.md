---
id: ab-1845
izenburua: Dozena Bat Bertso Berri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001845.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001845.MID
youtube: null
---

Dozena bat bertso berri
Gaztien konsolagarri,
Nere mingaña sano dan arte
Nai nituzke jarri.
Begiratuaz alkarri,
Andrerik ezin ekarri,
Mutill zarraren despeira zer dan
Esango det sarri.