---
id: ab-2076
izenburua: Bost Elizako Sakramentuak (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002076.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002076.MID
youtube: null
---

Bost Elizako sakramentuak
Dira zerutik jetxiak.
Bestek biyak e borondatera,
Jaunak guretzat utziyak.