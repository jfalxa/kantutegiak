---
id: ab-2138
izenburua: Jeiki, Lagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002138.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002138.MID
youtube: null
---

Jeiki, lagun, jeiki, lagun,
Guazen Artizanera.
Artizanengo plazan bada
Ursotxu polit bat;
Hura nahi nukezu
Bildu nere sarietara.