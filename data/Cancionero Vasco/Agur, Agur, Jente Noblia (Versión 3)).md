---
id: ab-1512
izenburua: Agur, Agur, Jente Noblia (Versión 3))
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001512.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001512.MID
youtube: null
---

Agur, agur, jente noblia
Berri on bat dakargu:
Atenziyua iduki dezan,
Iñork aditu nai badu;
Atentziyua iduki dezan,
Iñork aditu nai badu.