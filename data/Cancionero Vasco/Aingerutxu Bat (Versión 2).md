---
id: ab-1525
izenburua: Aingerutxu Bat (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001525.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001525.MID
youtube: null
---

Aingerutxu bat etorri zitzaitzun:
Ez, otoi, Maria, durduzitu.
Zeruko mandataria naiz, ta
ondo aditu biar naizu,
Ondo aditu, ondo aditu:
Jesusen ama biar dezu,
Jesusen ama biar dezu.