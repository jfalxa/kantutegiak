---
id: ab-2831
izenburua: Erosi Ta Ekarri Du (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002831.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002831.MID
youtube: null
---

Erosi ta ekarri du
Allitik zakurre;
Ez digute pagatu
Lanbide makurre.
Bost aldiz eman diot
Eskutik apurre.
Ola dabillen artzai
Zapela makurre.