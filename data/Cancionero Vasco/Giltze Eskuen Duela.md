---
id: ab-1967
izenburua: Giltze Eskuen Duela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001967.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001967.MID
youtube: null
---

Giltze eskuen duela
Gure aita San Pedrok,
Zeruko atietan
Pronto antxen dago.
Zer bizi modu degun
Munduan igaro,
Egie konfesatu,
Maisuari klaro:
Gezurra esan ta libre
Ez gerala izango.