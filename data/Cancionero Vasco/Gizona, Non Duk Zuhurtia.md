---
id: ab-1970
izenburua: Gizona, Non Duk Zuhurtia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001970.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001970.MID
youtube: null
---

Gizona, non duk zuhurtzia?
Bai, ez dakik
Ez dela deus hire bizia
Khe bat baizik?
Higuintzak bihotzez mundua,
Orai danik.
Herioa duk hurbildua
Hire ganik.