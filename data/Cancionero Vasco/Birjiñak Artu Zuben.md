---
id: ab-1797
izenburua: Birjiñak Artu Zuben
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001797.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001797.MID
youtube: null
---

Birjiñak artu zuben San Jose Esposo,
Familiaren karga al bazezan jaso.
Bizitza artako zazpi dolore ta gozo,
Munduari nai diozkat kantuz adierazo.