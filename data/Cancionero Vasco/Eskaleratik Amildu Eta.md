---
id: ab-1912
izenburua: Eskaleratik Amildu Eta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001912.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001912.MID
youtube: null
---

Eskaleratik amildu eta
Goitikan beiraño zuzen;
Lendabiziko pensatu nuben
Eltze zarren bat ote zen.