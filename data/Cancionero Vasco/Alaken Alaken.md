---
id: ab-1563
izenburua: Alaken Alaken
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001563.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001563.MID
youtube: null
---

Adixkidea, ikusi duzuya
Zelai horietan artzainik:
Artzain tipi, artzain handi,
artzai mutur legunik?
Alaken, alaken,
Ala pika ala ttupairen.
Errespun dela pikala tun pa
Jesus, Maria, Jainkoa zen.