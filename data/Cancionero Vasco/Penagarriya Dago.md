---
id: ab-2437
izenburua: Penagarriya Dago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002437.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002437.MID
youtube: null
---

Penagarriya dago munduko bizitzan,
Kantuz esango dizutet zer nola gabiltzan.
Dama eder batekin esposatu nintzan,
Arekin ordu on bat ez nezake izan,
Nere begiyetara ona etorri zan.