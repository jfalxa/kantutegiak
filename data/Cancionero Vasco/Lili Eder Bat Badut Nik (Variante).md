---
id: ab-2262
izenburua: Lili Eder Bat Badut Nik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002262.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002262.MID
youtube: null
---

Lili eder bat badut nik
Aspaldi begistaturik;
Bainan ez naite mentura
Haren hartzera eskura.
Banaki zer den lanjera,
Juan nindakio ardura.

- Lili ederra, so eidazu,
Maite nauzunez errazu.
Zure begiek ene bihotza
Barnetik dute kolpatu;
Kolpe hortarik badut nik
Gangrenatzeko arrisku.

- Pena nikezu segur nik
Balinbazinu malhurik:
Ez nuke uste ene begiek
Eman dautzuten kolperik.
Ez duzu beraz lanjerik:
Ez zira hilen hortarik.