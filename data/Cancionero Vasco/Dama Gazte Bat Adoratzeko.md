---
id: ab-2716
izenburua: Dama Gazte Bat Adoratzeko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002716.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002716.MID
youtube: null
---

Dama gazte bat adoratzeko
Artutzen dut enpeñua.
Gustura denak ikasi beza
Bertsuak jartzera nua.
Ustez seguro iduki eta,
Eman dit desengañua.
Toki onean enpleatu dut
Lenbiziko kariñua.