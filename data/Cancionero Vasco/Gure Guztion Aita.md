---
id: ab-2017
izenburua: Gure Guztion Aita
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002017.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002017.MID
youtube: null
---

Gure guztion Aita, danon prinzipala,
Zuk badakizu ondo premian naizela.
Nere izpiritua argitu dezala,
Bertsuak ipintzeko biar dan bezala.