---
id: ab-2131
izenburua: Izar Ederra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002131.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002131.MID
youtube: null
---

Izar ederra, zauzkit gogoan
Biotzeraño sarturik:
Orrengatikan amoriua
Aundia dizut nik zuri.
Ez tut deseo beste Indiarik
Bazindauzkit lograturik.