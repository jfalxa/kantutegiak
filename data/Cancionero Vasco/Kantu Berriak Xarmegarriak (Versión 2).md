---
id: ab-2212
izenburua: Kantu Berriak Xarmegarriak (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002212.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002212.MID
youtube: null
---

Kantu berriak, xarmegarriak,
Amodioaren gainian,
Zoinak emanak izan baitire
Pasko biharamunian.
Uso paloma zuri eder bat
Neure sarian barnian.
Neure sariak usorik gabe
Altxatu nituenian,
Harriturikan gelditu nintzen
Nola goan zen airian,
A!, nola goan zen airian.