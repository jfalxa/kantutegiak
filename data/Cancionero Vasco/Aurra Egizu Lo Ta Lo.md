---
id: ab-1706
izenburua: Aurra Egizu Lo Ta Lo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001706.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001706.MID
youtube: null
---

Aurra, egizu lo ta lo,
Nik emanengo bi koko,
Bat orain eta bestia gero,
Arratsaldian txokolatia.