---
id: ab-2343
izenburua: Moltsan Piper-Pikorra Franko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002343.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002343.MID
youtube: null
---

Moltsan piper-pikorra franko,
Musuan usai gozua.
Neskatxa gazte airosoa da
Antoni Alzuberekua.