---
id: ab-2067
izenburua: Ai, Au Gabaren Zoragarria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002067.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002067.MID
youtube: null
---

Ai, au gabaren zorogarria!
Jesus jaio da Belen'en.
Etxe onetan sartu ote da
Billa gabiltza beraren.
Belengo ikulluko
Ate zabaletan
Otzaz ikaraz dago
Jesus lastuetan.
Astua eta idia
Dauzka aldemanean,
Asnasaz berotzeko
Ostutzen danean.