---
id: ab-2695
izenburua: Txanton Pipirri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002695.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002695.MID
youtube: null
---

Txanton Pipirri emen dago,
Limosna on bat balego.
Kantau biar dot,
Baldin al badot,
Neure sabelen gosia
Erruki dedin jentia.
La ra la la...

Iru mutillek Ipasterren
Aposta bat egi-eben (egin-eben)
Jango ebela
Goxetik gabera
Amar erraldeko txala
Txanton laugarrentzat zala.
La ra la la...

Lekeitio-ko kalian
Besigu denpora danian
Ogei txitxarro
Eta geiago
Sartzen ditut sabelian
Eta gosia gabian.
La ra la la...

Ez da errotan arririk
Nire agiñak lekorik
Nire agiñak
Satittuko leuke
Ogi andi bat osorik
Balego bigun-bigunik.
La ra la la...