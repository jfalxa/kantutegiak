---
id: ab-2060
izenburua: Itsasuetan (Versión 7)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002060.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002060.MID
youtube: null
---

Itsasuetan laño dago
Baionako erdiraño.
Txoriak bere umiak baño
Nik zu zaitut maitiago.