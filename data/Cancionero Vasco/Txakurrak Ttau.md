---
id: ab-2762
izenburua: Txakurrak Ttau
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002762.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002762.MID
youtube: null
---

Txakurrak ttau,
Gatiak ñau!
Zer bi mila debru da gaur!
Etxian sartzian,
Andrea koleran,
Latina goguan,
Anglesa buruan,
Eskuaraz mintzatzen:
- Gizona kokina, bandila!
Hola moxkortzeko non hago gaur?
- Xo, xo, Mariaño,
Moxkorra jina nun gaur.