---
id: ab-2273
izenburua: Lo Lo Lo Lo Nere Maitea (Versión 8)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002273.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002273.MID
youtube: null
---

Lo, lo, lo, lo, nere maitea, lo,
Eguna zabaldu arteraino.
Nik maite zaitut zu maiteago
Xoriak bere umiak baino.
Lo, lo, lo, lo.