---
id: ab-2134
izenburua: Jaunak Ala Naidikan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002134.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002134.MID
youtube: null
---

Jaunak ala naidikan
Bere bide onez,
Aita eraman zidaten
Il zala esanez.
Ondo goguan daukat
Nola zan goiz baten;
Orduan bai malkuak
Ziraden irteten.