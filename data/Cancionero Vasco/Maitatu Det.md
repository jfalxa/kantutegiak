---
id: ab-2274
izenburua: Maitatu Det
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002274.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002274.MID
youtube: null
---

Maitatu det izanen eztuden bat,
Ark ematen dit biotzian pena.