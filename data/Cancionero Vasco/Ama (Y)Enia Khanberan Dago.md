---
id: ab-3300
izenburua: Ama (Y)Enia Khanberan Dago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003300.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003300.MID
youtube: null
---

Ama (y)enia khanberan dago
Kota gorria jauntzirik.
Ubalala, ubalala,
Kota gorria jauntzirik.

Ene amak txerkatzen zaitan
Muthil txar bat senhartzat.
Ubalala, ubalala,
Muthil txar bat senhartzat.