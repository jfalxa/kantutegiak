---
id: ab-2323
izenburua: Mendietako Zahartu Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002323.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002323.MID
youtube: null
---

Mendietako zahartu naiz eta
Nahi dut progatu marina.
Zangoz bezala izatu naiz
Gizon buru arina.
Mendietako zahartu naiz eta
Nahi dut progatu marina.