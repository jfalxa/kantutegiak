---
id: ab-2333
izenburua: Milla Zortzi Eun Ta (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002333.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002333.MID
youtube: null
---

Milla zortzi eun ta berrogei ta zortzi,
Urte bat an jo nuben Oyartzungo Txipi.
Geroztik emen nabil goiti eta beiti.
Beinere ez il, baño arriskoan beti.

Orra pixo uri ere asi zait laburtzen
Zapata muturretik asi zait usteltzen.
Iaz zapari ta aurten belauneri
A! ze diferentzia urte baten saurik (?).