---
id: ab-3010
izenburua: Mutxikoa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003010.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003010.MID
youtube: null
---

Yende onak, barda lo egin dut )
Dama gazte polit batekin. )bis
Berriz ere nahi nuke egin
Arrekin berarekin
Bietan edo behin.
Zeruko Jinko Jauna, zuk eizu
Ene desira konpli dadin.
Ene desira konplitu
Nik atsegin artu.
Dama gaztia, errazu egia )
Enekilan egoitia )bis
Atsegin duzuya? )
Atsegin baduzu itz bat errazu.
Berritz ere laburski
Ginen mintzatu.
Orain aste naiz korrientetik
Ene sekretuen manifestatzen...