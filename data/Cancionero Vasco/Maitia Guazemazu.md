---
id: ab-2288
izenburua: Maitia Guazemazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002288.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002288.MID
youtube: null
---

Maitia, guazemazu Arantzazura, Arantzazura,
Zu aurrian eta ni narraikizula ondotik.
Uxotxua, egizu ainbeste nigatik...
Bestela juango naiz laxter mundutik.