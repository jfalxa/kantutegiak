---
id: ab-2525
izenburua: Subandilla Marma
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002525.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002525.MID
youtube: null
---

Subandilla marma,
Ire aita ta ire ama
Bazkaitako espera.