---
id: ab-3254
izenburua: Gure Etxeko Idiek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003254.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003254.MID
youtube: null
---

Gure etxeko idiek
Adarrak oriek:
Ederki dabiltzela
Gure dantzariek.
La, la, la...

Nik ere ikusten det
Soroan garie;
Buru ori gorazak,
Mutil ongarrie.
La, la, la...

Untziaren gañeko
Bandera gorrie;
Ai, nere biotzeko
Konsolagarrie.
La, la, la...

Ederra zeran bezin,
Baziñe sekreta,
Geiagotan joko nuke
Leyoko krisketa.
La, la, la...

Badakit baituzute
Bat eta bi ere;
Esperantza galdurik
Ez nago ni ere.
La, la, la...

Apalian jartzeko
Ona da platera;
Zeletan dagoanari
Bi begik atera.
La, la, la...

Eperrak kantatzen du
Goizian intzetan,
Ez diteke fiatu
Mutillen itzetan.
La, la, la...

Ankak okerrak eta
Begiek gorriek,
Andre billa dabiltzen
Errementariek.
La, la, la...