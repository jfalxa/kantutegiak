---
id: ab-2359
izenburua: Napoleon Haundia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002359.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002359.MID
youtube: null
---

Napoleon haundia hontan zen tronpatu:
Angeletierra zuen egoitzaz hautatu.
Kriminal bat bezala zuten tratatu,
Irla Sant Elenera zuten exilatu.
Arroka baten puntan han baitzen finitu;
Illobak nahiko tu zorrak pagatu.