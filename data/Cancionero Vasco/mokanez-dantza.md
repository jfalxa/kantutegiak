---
id: ab-2985
izenburua: Mokanez-Dantza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002985.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002985.MID
youtube: null
---

Amodioak nerauka
Biotzetikan eri;
Erran gabetanik
Ezin nagoke ni.
Erranen dut bada nik
Biotzaren erditik
Pena bat badaukat
Barnian gorderik,
Marianatxo zu
Ezaguturez geroztik.
Amodio Margaritatxo
Nola zaude tristerik,
Zaudezkit alegerarik.
Penarik aski
Paretzitzen du
Mundu unetan zugatik.
Dakidalaki,
Itzak eman zindauzkitala
Zazpi urte tu konplitu,
Orai zu bertzek ereman eta
E naiz obeki gelditu
Zeru altua
Zerorrek ongi dakizu
Ene trabajua
Zeru altua.
Untza xuluan dago,
Xorien igesi.
Ura irten da eta
Bera dago larri.
Zeru altua,
Zerorrek bajua
Zarpa zar eztarrie garbi,
Zorra pranko baño
Artzeko guti.
Bart gure auzuan aserre
Bai batzuetan gu ere
Orai ez bedere
Kartak eta arduak
Ditut adixkide
Arduaren mentajak
Dirare andiak,
Ene biotza,
Ardorik edan gabe
Dago ilotza. etc...