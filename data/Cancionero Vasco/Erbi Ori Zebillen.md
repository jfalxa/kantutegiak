---
id: ab-1899
izenburua: Erbi Ori Zebillen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001899.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001899.MID
youtube: null
---

Erbi ori zebillen )
Beti pikardian: ) bis
Gaubez soruan eta
Egunaz mendian.

Erbiar gorria zan, )
Begiak larriak, )bis
Buztana motza eta
Luze belarriak.

Lazua jarri zion )
Soroko sarreran, ) bis
An arrapa zezala
Bere etorreran.

Erbi hau etorri zan )
Lenengo garaien )bis
Lazoan sartu eta
Gelditu zitzaien.

Auzoko bat zebillen )
Berriz atalaien; )bis
Andikan ostu eta
Ark eraman zien.

Ankak eta muturra )
Lotuta jartzian, )bis
Sosegatu ziraden
Kontentu onian.

Ankak libratu eta )
Bazebillen dantzan; )bis
Leiotikan kanpora
Saltatu egin zan.