---
id: ab-2071
izenburua: Atean Ttan, Ttan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002071.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002071.MID
youtube: null
---

Atean ttan, ttan, jo nion, eta
Ez zautan erresponditu.
Berriz ere jo nion eta,
Zer nahi nuen galdetu,
Zer nahi nuen galdetu.