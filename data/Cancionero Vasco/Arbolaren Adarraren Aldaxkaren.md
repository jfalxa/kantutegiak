---
id: ab-3019
izenburua: Arbolaren Adarraren Aldaxkaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003019.MID
youtube: null
---

Arbolaren adarraren aldaxkaren
Puntaren puntatik
Txoriñua kantari.
Nork eman dezake aireño ori.