---
id: ab-1638
izenburua: Argizagi Ederra (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001638.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001638.MID
youtube: null
---

Argizagi ederra, argi egidazu!
Bidaia luze batez yoan beharra nuzu.
Maitea nahi nuke gaur behin mintzatu.
Hots, haren bortaraino argi egidazu!

- Lotara ziradeia, lo'gile pollita?
Lotara ez ba zira, so'izu leiora,
Eta egiaz mintza, ene izar ederra,
Zure aita et'ama diraden lotara.

- Aitamak lo direla ez duzu dudarik;
Bainan etxe-mutila oraino ez naski.
Aitamentzat bezain harentzat beldur niz:
Ene guardian dago lo guti egiñik.