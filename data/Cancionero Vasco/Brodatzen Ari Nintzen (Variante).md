---
id: ab-1871
izenburua: Brodatzen Ari Nintzen (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001871.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001871.MID
youtube: null
---

Brodatzen ari nintzen
Ene salan jarririk,
Aire bat entzun nuen
Itsasoko aldetik,
Itsasoko aldetik,
Untzian kantaturik.