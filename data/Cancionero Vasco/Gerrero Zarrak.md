---
id: ab-1966
izenburua: Gerrero Zarrak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001966.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001966.MID
youtube: null
---

Gerrero zarrak erantzun zuten:
Ederki dago gauz ori;
Eta konforme gerade danak
Jarraitutzeko gu zuri.
Gu Ernioko aitzetan bera
Amil gaitezen eta lenbailen
Gure odola ixuri,
Ondorengorik Erromatarren
Mende ez dedin erori.