---
id: ab-1847
izenburua: Dozena Bat Bertsoa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001847.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001847.MID
youtube: null
---

Dozena bat bertsoa
Nai nuke kantatu:
Zenbat estranjeroa dan
Españian sartu.