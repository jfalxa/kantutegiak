---
id: ab-1818
izenburua: Ardiak Nun Ttuk Bettiri (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001818.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001818.MID
youtube: null
---

- Ardiak nun ttuk, Bettiri?
- Ardiak itzalian lo.
Ardiak eman ostiko:
Ai!, ni ez othe naiz biziko!
Eta bubaño, eta ñiñaño.
Haurra dugu ñimiño,
Ñimiño eta gaixtoño.