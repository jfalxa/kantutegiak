---
id: ab-1593
izenburua: Amak Dio Alabari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001593.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001593.MID
youtube: null
---

Amak dio alabari:
""Hala kanalla txarra hi!
Hik behar duna ibili
Aitoren seme hoiekin?
Ibiltzekotz, ibil hadi
Hihaur iduri zenbaitekin"".