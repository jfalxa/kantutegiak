---
id: ab-1618
izenburua: Animo Bada Animo!
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001618.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001618.MID
youtube: null
---

Animo bada, animo, animo Josepe!
Geure infante onek gaur jaio bear luke.
Animo bada, animo, animo Josepe!
Gauaren ordu onetan desanima leike.