---
id: ab-2378
izenburua: Nere Maitia, Ikusazu (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002378.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002378.MID
youtube: null
---

Nere maitia, ikusazu kanpuan gaizki naizela.
Eskuño batez tira nezazu zure ganbaran barnera;
Sekeretuian konta dezadan nik zuri ene manera,
Nik zuri ene manera.