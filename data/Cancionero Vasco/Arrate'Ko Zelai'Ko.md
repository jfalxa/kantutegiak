---
id: ab-1666
izenburua: Arrate'Ko Zelai'Ko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001666.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001666.MID
youtube: null
---

Arrateko zelaiko
Bai floridadea:
Andixek gora dago
Zeruko bidea.
Nere Ama Birjiña
Kontzeziñokua,
Arren, mobitu zazu
Neronen gogua.