---
id: ab-1592
izenburua: Ama Nur Zünian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001592.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001592.MID
youtube: null
---

Ama nur zünian, jakin nahi nüke;
Eder izan behar zizün düdarik gabe;
Zü hala zirade, mündian paregabe:
Koloriak gorri, ezpañak mehe,
Kalitate fina hori da señale.
Zureki bizi banintz, irus nintzat.