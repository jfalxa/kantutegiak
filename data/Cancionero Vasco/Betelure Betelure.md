---
id: ab-1778
izenburua: Betelure Betelure
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001778.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001778.MID
youtube: null
---

Betelure, Betelure,
Askotan naiz ara gure!
Aragongo ardoa baño,
Nayago nuke ango ure.