---
id: ab-1738
izenburua: Bart Arratsean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001738.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001738.MID
youtube: null
---

Bart arratsean, garai batean,
Katua zegon kutxa gañean.
Esaten nion: ""zapi"" ta ""zapi"";
Etzuan juan nai kutxa gañetik.
Karambabilis, karambabolis;
Saeculum per ignem, ora pro nobis.