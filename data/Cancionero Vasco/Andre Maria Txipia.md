---
id: ab-1615
izenburua: Andre Maria Txipia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001615.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001615.MID
youtube: null
---

Andre Maria txipia,
Xotx ekarran gorria,
Paradisu giltzeria.