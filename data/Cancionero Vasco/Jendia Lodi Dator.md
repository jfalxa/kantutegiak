---
id: ab-2141
izenburua: Jendia Lodi Dator
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002141.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002141.MID
youtube: null
---

Jendia lodi dator Aranoatikan,
Heien musika ageri da mundu guzitikan.
Soinua joiten dute dulziñaren gisa;
Adituez geroztik halako musika,
Otsua badoake, ardiak utzita.