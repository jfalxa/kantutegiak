---
id: ab-1727
izenburua: Baratzeko Xulufraia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001727.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001727.MID
youtube: null
---

Baratzeko xulufraia eder koloria,
Eder koloria:
Xarmegarria, erran zadazu, othoi, egia.
Zure baitan fidaturik badudanez probetxurik;
Edo bertzenaz kita nezazu arras bihotzetik.
Zureganako esperantzik galdu ezkeroztik,
Bertzeño baten xerkatzera ensayaturen niz.