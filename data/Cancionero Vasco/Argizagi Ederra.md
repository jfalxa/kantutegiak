---
id: ab-1634
izenburua: Argizagi Ederra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001634.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001634.MID
youtube: null
---

Argizagi ederra, argi egidazu!
Orañik bide luzean yoan behar bainauzu.
Maitea nahi nuke gaur berian mintzatu,
Haren bortha leihua ongi ezagutu.