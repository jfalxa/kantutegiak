---
id: ab-1830
izenburua: Agur Gizon Gaztea (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001830.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001830.MID
youtube: null
---

Agur, gizon gaztea, konpañiarekin!
Plazer dut egoitia istanpat zurekin.
Badakizu mintzatzen errespeturekin:
Yinkoak har zaitzala hill ondoan berekin,
In, in, in, hil ondoan berekin.