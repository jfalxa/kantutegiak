---
id: ab-1803
izenburua: Bonbolon Bat Eta Bonbolon Bi (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001803.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001803.MID
youtube: null
---

Bonbolon bat eta bonbolon bi.
Bonbolon putzurat erori.
Erori bazinen, erori:
Etzinen orduan egarri.