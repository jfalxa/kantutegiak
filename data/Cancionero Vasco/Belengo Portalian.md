---
id: ab-1750
izenburua: Belengo Portalian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001750.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001750.MID
youtube: null
---

Belengo portalian,
Os garbe eder bat,
Aren argiten danzan
Aingeru talde bat.