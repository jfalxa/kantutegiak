---
id: ab-2844
izenburua: Zato Biar
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002844.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002844.MID
youtube: null
---

Zato biar, zoazi gaur,
Etxian dugu "lu bernau".
Eta lo, lo,
Eta lo, lo, lo.
Aurtxoa dugu ñimiño.