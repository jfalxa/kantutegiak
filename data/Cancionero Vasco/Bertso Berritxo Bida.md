---
id: ab-1773
izenburua: Bertso Berritxo Bida
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001773.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001773.MID
youtube: null
---

Bertso berritxo bida nai ditit paratu,
Nere manerak auta ongi deklaratu.
Astelenarekin dut deliberatu,
Tristerik egon gabe naiz alegeratu.