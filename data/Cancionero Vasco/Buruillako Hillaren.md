---
id: ab-1821
izenburua: Buruillako Hillaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001821.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001821.MID
youtube: null
---

Buruillako hillaren hogeigarrena zen,
Egun señalatua hastelena baitzen;
Emain dut aditzera ahal bezain zuzen,
Hastetik fineraño han zer pasatu zen.