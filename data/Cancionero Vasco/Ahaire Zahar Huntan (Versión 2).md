---
id: ab-1516
izenburua: Ahaire Zahar Huntan (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001516.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001516.MID
youtube: null
---

Ahaire zahar huntan bi bertsü berririk
Alagrantziareki khantatü nahi tit;
Bihotza libratürik phena orotarik,
Desir düdan maitia beitüt gogatürik.

Enian ez erranen, ezpeitzian üdüri,
Gogatüren niala maitia jagoiti.
Mintzo naintzon bethi ahalaz oneski: (nintzeion)
Hura, ene erranagatik, sekülan ezin konberti.

- Zazpi urtez zitzaitzat ondoan ibili,
Ene tronpatü nahiz malezian bethi;
Orai egiten düzü aisa nitzaz irri:
Jinko Jaunak, begiraizü, etzitzan püni.

- Zelüko Jinko Jaunak badü pietate;
Bere denbora dizü errota egile.
Ürgülütsian bortxaz konbertiazle:
Feit horrez orhit zite, maitia, zü ere.

- Ühaña da ihize arrañ hurin düna:
Mutilak trende amoros dieño.

Desenia kunpli eta guetzat adio.

- Pikanti mintzo zira mütiler, maitia,
Ez, sunia bazeneza düzün estatia;
Maite ezpalinbanü fidelitatia,
Elitzaikezü konbeni hola mintzatzia.

- Presuna bat denian plazera galdürik,
Arramarkiren düzü inkietatürik;
Ene inkietarik ez egin kasürik,
Bestelan eztükezü ni baizik galdürik.

- Arañeko süjeta, ororen lilia, (Arañe, barrio)
Neskatila gaztetan parerik gabia:
Zaldiz igorriren deizüt zuri txerkaria:
Hartan gañen jinen zira zü eta frütia.