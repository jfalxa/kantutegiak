---
id: ab-2209
izenburua: Kantore Eder Xarmant Batzuek (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002209.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002209.MID
youtube: null
---

Kantore eder xarmant batzuek
Orai hasten naiz kantatzen;
Sujetak ere zer ditudan
Ez ditut deklaraturen.
Zeruetako izar eder bat
Jinik badugu Luzaiden:
Eztitasunez betia da, ta
Guziak ditu xarmatzen,
Orai hasten naiz kantatzen.

Zeruetako Probidentzia,
Jendeak, ez dea handia?
Iruzkitako plazaño hortan
Zer izar xarmegarria!
Artizar horrek merezi luke
Ederki adoratzia.
Ofrendetako emokozue
Mirra edo intzentsue,
Ahal duenak, urhea.

Barber gazte bat jin izan zauku
Espainiako aldetik
Artizar horren konsolatzera
Erremedioz beterik.
Erremediok hartu tu, bainan
Ez du egin mirakulurik;
Miserabilia gelditu izan da
Sendatu gabe tristerik,
Ezin eginen bertzerik.

Konseiluño bat banikezu
Nik emaiteko zuri:
Barber gaztetan etzaiten fida,
Balin ez bazira eri.
Nahi baduzu konserbatu
Zure lili polit hori,
Gezurño zonbait erran bahauzu
Adoratzaile hoieri,
Eta ez fida nehori.