---
id: ab-2582
izenburua: Urra Manuelatxo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002582.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002582.MID
youtube: null
---

Urra, Manuelatxo,
Zer dozu negarrez?
Aitak eta amak
Beterik emaiez.
Lo, lo, lo...