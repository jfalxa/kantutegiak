---
id: ab-2578
izenburua: Ur Barrena, Ur Goyena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002578.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002578.MID
youtube: null
---

Ur barrena, ur goyena;
Urte berri egun ona.