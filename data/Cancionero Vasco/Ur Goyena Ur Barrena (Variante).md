---
id: ab-2577
izenburua: Ur Goyena Ur Barrena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002577.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002577.MID
youtube: null
---

Ur goyena, ur barrena,
Urteberri egun ona.
Jainkoak dizuela egun ona.
Etxe onetan sartu dedilla
Pakiarekin osasuna,
Urarekin ondasuna.