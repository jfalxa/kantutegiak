---
id: ab-2263
izenburua: Lobatxua Lo Ta Lo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002263.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002263.MID
youtube: null
---

Lobatxua, lo, ta lo;
Orain zeuk eta neuk gero.
Zeuk gure dozun ordu on baten,
Biok egingo degu lo.