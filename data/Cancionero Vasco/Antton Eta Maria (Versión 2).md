---
id: ab-1623
izenburua: Antton Eta Maria (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001623.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001623.MID
youtube: null
---

Berso berri batzuek nua kantatzera
Ardurazko Alduden suyetak baidira.
Nik eztitut agertzen ageri baidira
Guziek badakite famen araber.