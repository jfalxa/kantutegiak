---
id: ab-1987
izenburua: Goizetan Jeikitzen Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001987.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001987.MID
youtube: null
---

Goizetan jeikitzen da izar bat ederrik,
Hura dela diote zelian ederrenik.
Lurrian ikusten dit bat ederragorik;
Zelietan ere ezpeitu harek bere parerik.