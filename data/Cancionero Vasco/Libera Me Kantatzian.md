---
id: ab-2257
izenburua: Libera Me Kantatzian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002257.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002257.MID
youtube: null
---

"Libera me" Kantatzian,
Isopa ez busti urian;
Arnoan busti zazu;
Han ongi trenpa zazu;
Segur naiz nahiko dudala
Hilian bizian bezala.