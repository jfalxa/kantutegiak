---
id: ab-2809
izenburua: Tinter, Lanter (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002809.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002809.MID
youtube: null
---

Tinter, lanter,
Ail, ekarrak adarra;
Einen daiat xirula.
- Zertaz?
- Gaztena laida politaz.
Xirula, mirula, kantari,
Balin bahaiz izerdi,
Kris, kras, atera hadi.