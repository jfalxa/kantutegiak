---
id: ab-1715
izenburua: Azarotik Asi Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001715.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001715.MID
youtube: null
---

Azarotik asi ta
Bost illabetian,
Geroztik emen gaude
Beti elurpian.
Gogor egun diagu
Bulada batian,
Mendiko gorostiek
Dirauben artian.