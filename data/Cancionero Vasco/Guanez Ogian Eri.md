---
id: ab-2006
izenburua: Guanez Ogian Eri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002006.MID
youtube: null
---

Guanez ogian eri, (Juanes)
Tra la la la...
Guanez ogian eri, (ohean)
Hil dadien beldurrez. (bis)

Hiltzen bada, hil datiela,
Tra la la la...
Hiltzen bada, hil datiela,
Nik axolarik ez. (bis)

Ni hiltzen naizenian
Tra la la la...
Ni hiltzen naizenian,
Ez ehortz elizan. (bis)

Ehortzian nauzue
Tra la la la...
Ehortzian nauzue
Xai on baten zolan. (bis)

Zangoz bortarat eta
Tra la la la...
Buruz barrikarat,
Ahoz duxularat. (bis.