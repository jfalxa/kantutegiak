---
id: ab-2195
izenburua: Kanpana Txiki Oria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002195.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002195.MID
youtube: null
---

Kanpana txiki oria,
errepika al baitez.
Ia, bada jendiak,
Ez egon kupidez;
Txikiyak eta (i)aundiyak,
Denak eta dianak,
Jendiak eldu daien
Jesus onagana.
Ai, au jendia gatoz
Jesus onagana!

Guztiok badekagu
Erragalo bana.
Artzayak nun dirare?
Ez dirare agertu!
Ez al dirare bada
Bidian gelditu?
Kasko sollarekin,
Da makilla eskuban,
Bizkarra makurturik,
Jesus onan billa.