---
id: ab-2013
izenburua: Gure Bordaltia Zabal Ordoki
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002013.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002013.MID
youtube: null
---

Gure bordaltia zabal ordoki.
Muthil gazte zonbait harat jin baledi.
- Eta zergatik? Eta zergatik?
- Nitzaz agrada ditin amorekatik.

- Ene aita zahar, bizkar handia,
Bizkarrian bazünü nitüdan mina (nik düdan)
- Eta zergatik? Eta zergatik?
- Aurten ezkunt nezazü amorekatik

- Hago ixilik, haurra, ahal bahiz;
Aurten ezkuntüen hiz, bizi baniz ni.
- Eta zergatik? Eta zergatik?
- Etxeko bakiaren amorekatik.

Gure pegar zahar müthür zabala,
Hi izanen hiz ene senhargaia.
- Eta zergatik? Eta zergatik?
- Üsü bethatzen haidelakoz amorekatik.