---
id: ab-1602
izenburua: Amodio Gaztien Zoragarria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001602.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001602.MID
youtube: null
---

Amodio, gaztien zoragarria!
Beldur ez banintz zuretako, maitia,
Pasa niro denboraren erdia,
Orobat behar balitz ere guzia,
Zurekin pasatzeko nere bizia.

Ene maitia, zutaz naiz eztonatzen,
Zeren eztuzun nigana kontinatzen.
Kontina niro, esperantzarik banu;
Bainan beldur naiz eztukedan probetxu:
Nitaz bertzerik zuk duzula hautatu.

Nere mentak ttipittutxiak dira;
Denbora batez bai nuen sobera.
Gora ustian, bera naiz erori;
Orduan eder bezein, orain itsusi:
Nehork ni ez naute nahi ikusi.