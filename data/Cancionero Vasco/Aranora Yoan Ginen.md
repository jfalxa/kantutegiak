---
id: ab-1629
izenburua: Aranora Yoan Ginen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001629.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001629.MID
youtube: null
---

Aranora yoan ginen amalau nobio,
Zazpi konfiterua, sei botikario.
Bostna kintalekua(k) bi armario,
Gizon batek apultsa bizkarrera dio.
Arrazoin ori kontra nork egingo dio?

Ogoi gurdi iratze, berroi karga lasto
Baserri baterako ez litezke gaixto;
Boltsilluan arturik ekarri tut atzo;
Orratz toki batian para ditut preso.

Zazpi gizonendako zortzi libra talo jaki,
Txantxangorri batenak sei libra tripaki,
Zetabiak ego eta errotak iralki,
Labainak erre eta labiak ebaiki.
Gauza oiek nola diren Senpelarrek daki

Gizon bat ikusi nuen iru egun urtetan
Mariñel zabillala untxiko soketan;
Oinetako zituen bi txalupa oinetan,
Aren aitona berriz itsarora urketan.

Itsasoan ari dire albola landatzen,
Ankamotzak ederki lasterka korritzen;
Itsuak ikusi du etxeak erretzen,
Mutuak berriz oyuz yende adirazten.