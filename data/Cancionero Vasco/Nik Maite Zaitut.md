---
id: ab-2388
izenburua: Nik Maite Zaitut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002388.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002388.MID
youtube: null
---

Nik maite zaitut, eta zuk maite nazazu;
Izar xarmegarria, konsola nazazu.
Bertzek eraman etzaitzan arras beldur nauzu;
Pena darraitzut, bainan fortuna egizu.