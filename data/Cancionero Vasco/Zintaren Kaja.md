---
id: ab-2788
izenburua: Zintaren Kaja
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002788.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002788.MID
youtube: null
---

Zintaren kaja,
Kajaren perla.
Txurrun txurrunplera
Atxuak goitik bera.