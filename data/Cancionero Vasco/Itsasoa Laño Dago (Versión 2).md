---
id: ab-2055
izenburua: Itsasoa Laño Dago (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002055.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002055.MID
youtube: null
---

Itsasoa laño dago
Baionako gaineraino.
Txoriak bere umiak baño
Nik zu zaitut maitiago.
Arraitxoak ura baño,
Nik zu zaitut maitiago.