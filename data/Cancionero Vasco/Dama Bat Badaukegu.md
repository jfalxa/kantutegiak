---
id: ab-1825
izenburua: Dama Bat Badaukegu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001825.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001825.MID
youtube: null
---

Dama bat daukegu Lakanaegi ontan;
Alabatu nai nuke amar bertsuetan,
La, la, la...
Alabatu nai nuke amar bertsuetan.