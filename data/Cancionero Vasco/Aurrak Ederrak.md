---
id: ab-1708
izenburua: Aurrak Ederrak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001708.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001708.MID
youtube: null
---

Aurrak ederrak, aurrak ederrak,
Aurrak ederrak zerate.
- Don Filiperen, Don Filiperen
Seme-alabak gerade.
- Pasa zatizte, pasa zatizte
Onoko zubi txar ontan.
- Pasako gera, pasako gera
Orroko zubi txar ortan?
- Aurrenekoak pasa litezke;
Azkenekoa, presoa.