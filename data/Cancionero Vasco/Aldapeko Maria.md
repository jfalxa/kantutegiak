---
id: ab-3002
izenburua: Aldapeko Maria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003002.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003002.MID
youtube: null
---

Aldapeko Maria seme eginik dago;
Aretxen bisitara juateko nago.
Rau, rau, rau.
Dotia badet, bañan
Rau, rau, rau,
Nik zertako det au.