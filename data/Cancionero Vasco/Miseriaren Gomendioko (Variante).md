---
id: ab-2653
izenburua: Miseriaren Gomendioko (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002653.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002653.MID
youtube: null
---

Miseriaren gomendioko deramat nere mendia;
Orai andriak egin derauzkit alaba eta semia.
Jinkuak urrikari duela etxe utseko jendia.

Etxia ere utsa dugu, eta bedratzi baire lagunak (bagire)
Artuarentzat ez ditugu aski guk irabazten tugunak.
Ez dituk gaiski aberasten errotazain frikunak.

Errotazainak aberastu eta, artua ere ez merkatu.
Oraino deusik ez litake, balu gatz horrek enplegu;
Bere gutxian, artuaren diña hortan irabazten dugu.

Ni egundaino gizon ezdeusa ez naiz lenago izatu.
Bainan orain Jinkoak garda behin ostatuan sartu:
Andriak haurrak perezka, eta niri belauniak flakatu.

Nere andriak bere umiak haziko lituzke aisa,
Ostatu oro egin balaite komentu edo eliza.
Ni ere orduan konporta nindate gizon galant baten gisa.