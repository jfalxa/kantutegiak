---
id: ab-2623
izenburua: Zer Dirade Gure Egunak (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002623.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002623.MID
youtube: null
---

Zer dirade gure egunak,
Zer da gure bizia?
Itzalaren pare dira
Zure gure zentzua.
A ze egun eragunkoren
Biotza lagatzia.
Laster il biarra gera:
Oroi gaiten iltzia.