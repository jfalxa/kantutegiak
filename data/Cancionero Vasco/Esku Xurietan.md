---
id: ab-1915
izenburua: Esku Xurietan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001915.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001915.MID
youtube: null
---

Esku xurietan eder arrosa,
Eder arrosa:
Are ederrago zitroina.

Horra Joanes hori zoin den ederra,
Zoin den ederra:
Aurthen ezkontzen omen da.

Eta Maria hau, horren galaia,
Horren galaia:
Horien uda heldu da.

Horien bien eztaietara,
Eztaietara,
Errege zaldiz heldu da:

Rrau, rrau, rra;
Rrau, rrau, rra;
Ezpata gerrian,
Rrau, rrau, rra.