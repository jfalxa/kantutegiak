---
id: ab-2553
izenburua: Txerri Txerri Bedarra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002553.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002553.MID
youtube: null
---

Txerri, txerri bedarra,
Auntzak jan da bedarra.
Nork dukan eskutan bedarra,
Arrek billatu ber gurutzea.