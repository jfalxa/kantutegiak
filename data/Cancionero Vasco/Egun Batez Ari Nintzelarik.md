---
id: ab-1869
izenburua: Egun Batez Ari Nintzelarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001869.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001869.MID
youtube: null
---

Egun batez arin nitzelarik
Sala batian brodatzen,
Itsasotik aditu nuen
Mariñel bat kantatzen,
Mariñel bat kantatzen eta
Bertsu hauen emaiten.

Utzirik brodatzia eta
Juan nintzan amaren ganat
Heian utziko ninduenez
Itsaso bazterrerat,
Itsaso bazterrerat eta
Kantuen entzuterat.

- Nere alaba, abil-abil
Itsaso bazterrerat;
Bainan orhoit hadi hargatik
Iluna hurbil dela ,
Iluna hurbil dela eta
Etxerat itzultzia.

- Mariñela, mariñela
Emok untziari bela;
Aspaldian desir nuena
Jin zautak untzirat bera;
Jin zautak untzirat bera eta
Parti gaiten bereala.