---
id: ab-3243
izenburua: Ai, Isabel, Isabel
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003243.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003243.MID
youtube: null
---

Ai, Isabel, Isabel, Isabel ederra,
Etzenekien bada apaiza nintzala?
Apaiza nintzala ta koroia nuala?
Nitaz probetxurikan etzendukiala?

Labaingo apez gaztiak seme bat omen du
Osaba erraiten dio, berak ala nai du.
Aitak meza eman ta semiak lagundu.
Labaingo apez gaztiak alaxe egiten du.