---
id: ab-2615
izenburua: Zaldibiko Plazan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002615.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002615.MID
youtube: null
---

Zaldibiko plazan
Iru atso dantzan,
Irurak ez zuten
Ardit bana poltsan.