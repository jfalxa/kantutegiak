---
id: ab-1709
izenburua: Aurrek Aurrek Ederrak (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001709.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001709.MID
youtube: null
---

- Aurrek, aurrek ederrak,
Aurrek nongoak zerate?
- Errege Don Feliparen
Seme-alabak gerade.
- Errege Don Feliparen
Seme-alabak bazerate,
Pasa, pasa zubi txar ontan;
Pasa (i)al ba zaitezke.
- Baietz, balego zubi txar ontan
Ez falso, ez traidore.
- Pasa, pasa poliki;
Azken gaizoa leroki.