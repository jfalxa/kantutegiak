---
id: ab-1734
izenburua: Barka Jauna
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001734.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001734.MID
youtube: null
---

Barka, Yauna, barka! Barkamendu!
Gure huts guzietaz dolu dugu.
Hemendik aitzina, zureak gituzu;
Otoi, har gaitzazu, aitortzen dugu.