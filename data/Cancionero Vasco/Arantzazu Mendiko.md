---
id: ab-3186
izenburua: Arantzazu Mendiko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003186.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003186.MID
youtube: null
---

Arantzazu mendiko
Aiziaren otza!
Arantzazu mendiko
Aiziaren otza!
Antxen autsi nuben
Neuronen biotza.
Ai, morena, neuronen biotza;
Ai, salada, aiziaren otza!