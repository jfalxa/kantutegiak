---
id: ab-2115
izenburua: Agur, Etxeko Andrea (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002115.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002115.MID
youtube: null
---

- Agur, etxeko-andrea, pitz deraukuzu argia.
Edan behar batugu kutxut erdia.
- Nere adiskide maitea, erraiten datzut egia:
Saldu dudala barrikan dudan guzia,
Txorta txortaño bat ez bertze guzia.