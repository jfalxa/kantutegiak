---
id: ab-1744
izenburua: Bartko Gabaren Illuna
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001744.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001744.MID
youtube: null
---

Bartko gabaren, bartko gabaren,
Bartko gabaren illuna.
Zubidiko atarietan
Etzan omen da zalduna,
Zalduna ezpataduna.
Nagusi jauna atera zaio:
- Emen zer bear du zaldunak?
- Jose Miel naiz ta,
Jose Miel naiz ta
Maria nik nai nuke.
- Gure Maria bear duenak
Poltsan urria biar luke.
- Urria ganbia gentzake:
Zapata xurik, txarolatuak,
Amaika errial kostiak,
Zirezko berdez josiak.
Ortxen dauzkazu, ortxen dauzkazu
Jose Mielk berak emanik.