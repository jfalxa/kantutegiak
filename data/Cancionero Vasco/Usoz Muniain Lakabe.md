---
id: ab-2776
izenburua: Usoz Muniain Lakabe
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002776.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002776.MID
youtube: null
---

Usoz, Muniain, Lakabe,
Gorraiztik barrena,
Arrieta, Iriberrin sartzeko
Bereki zauka ordena.

Usoz, Muniain, Lakabe,
Arrieta, Iriberri,
Aek ere badakite
Pittiriren berri.