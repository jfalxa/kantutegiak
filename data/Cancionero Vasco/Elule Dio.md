---
id: ab-2717
izenburua: Elule Dio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002717.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002717.MID
youtube: null
---

Elulé dió, elulé;
Artzai kapelú makulé.