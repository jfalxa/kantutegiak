---
id: ab-2846
izenburua: Salütatzen Zütüt, Maria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002846.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002846.MID
youtube: null
---

Salütatzen zütüt, Maria,
O Ama güziz maithatia.
Maite düt zurekilan Jesüs,
Gure Errege handia.