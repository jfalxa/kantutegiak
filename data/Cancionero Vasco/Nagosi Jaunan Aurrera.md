---
id: ab-2666
izenburua: Nagosi Jaunan Aurrera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002666.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002666.MID
youtube: null
---

Nagosi Jaunan aurrera
Belaunbikotu lurrera.
Apal da lotzas etorri gara,
Jauna bedorren aurrera.