---
id: ab-2077
izenburua: Bost Elizako Sakramentua (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002077.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002077.MID
youtube: null
---

Bost Elizako sakramentuak
Dira zerutik jetxiak;
Beste biak dira borondadezko
Jaunak garena utziak.