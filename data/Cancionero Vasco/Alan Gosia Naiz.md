---
id: ab-1565
izenburua: Alan Gosia Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001565.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001565.MID
youtube: null
---

Alan gosia naiz,
Olan gosia naiz:
Alan gose, olan gose
Frailia naiz:
Dantzatu nai;
Modurik ez eta jakin ez.
Salto bat egin al banetzake
Al rebes!,
Bestia erres.