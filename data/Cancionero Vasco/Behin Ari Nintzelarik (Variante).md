---
id: ab-1872
izenburua: Behin Ari Nintzelarik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001872.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001872.MID
youtube: null
---

Behin ari nintzelarik
Sala batian brodatzen,
Aire eder bat entzu nuen
Itsasotik kantatzen,
Itsasotik kantatzen eta
Mariñelak kobla emaiten.