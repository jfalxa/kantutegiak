---
id: ab-1606
izenburua: Amodioa Zer Den
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001606.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001606.MID
youtube: null
---

Amodioa zer den
Jakin nai badezu,
Sutan egur ezia
Para bear dezu.
Sutan egur ezia
Paratzen badezu,
Negarra dariola,
Akituren zaitzu.