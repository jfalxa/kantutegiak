---
id: ab-2771
izenburua: Urra Katxutxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002771.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002771.MID
youtube: null
---

Urra katxutxa,
Txapela frente
Popa ganian
Iru komediante;
Aren ganian
Bela zurije;
Aren aspian
Arrautza gorringo.