---
id: ab-3001
izenburua: Txuriak Eta Bel-Beltxak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003001.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003001.MID
youtube: null
---

Txuriak eta bel-beltxak
Mendiyan ardiak.
Zuk ere ez dituzu
Zuk ere ez dituzu
Bentaja guziyak.
Txuriak eta beltxak
Mendiyan ardiak.