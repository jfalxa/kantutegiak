---
id: ab-2233
izenburua: Konpañi Onetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002233.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002233.MID
youtube: null
---

Konpañi onetan ote litzeke
Nik ondo derizkio(d)anik,
Nik ondo derizkio(d)anik ere
Pepita izena duanik?
Orrek ote du oialik,
Federiko Axenekuak emanik:
Urre mantala txarolinetan,
Txinela gorri ederrik.