---
id: ab-2215
izenburua: Karakol Sil Sol
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002215.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002215.MID
youtube: null
---

Karakol, sil, sol,
Ezpattuk adarrak ateatzen,
Ire aita, ire ama
Illen ttet, illen ttet.