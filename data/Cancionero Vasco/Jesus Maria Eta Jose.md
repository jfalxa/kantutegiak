---
id: ab-2152
izenburua: Jesus Maria Eta Jose
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002152.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002152.MID
youtube: null
---

Jesus, Maria eta
Jose, Joakin eta Ana:
Badakigu Jesus maitea
Jayo zan gauba dana.