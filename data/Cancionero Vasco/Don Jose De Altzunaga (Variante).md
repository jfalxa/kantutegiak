---
id: ab-1844
izenburua: Don Jose De Altzunaga (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001844.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001844.MID
youtube: null
---

Don Jose de Altzunaga, probinzianoa,
Deskansuz egoteko, a gizon sanoa!
Adiñaren konforme, mundutik banoa;
Urrikal dakidala Jaun Soberanoa.
Gizon umanoa, maite du ardoa, ze perillanoa;
Denbora gutxitako amerikanoa.