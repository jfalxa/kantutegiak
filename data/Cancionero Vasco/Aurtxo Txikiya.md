---
id: ab-1713
izenburua: Aurtxo Txikiya
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001713.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001713.MID
youtube: null
---

Aurtxo txikiya negarrez dago:
Ama, ekarzu ogiya.
Nere umia, nola emango
Kutxa utsetik ogiya.