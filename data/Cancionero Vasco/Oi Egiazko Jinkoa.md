---
id: ab-2674
izenburua: Oi Egiazko Jinkoa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002674.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002674.MID
youtube: null
---

Oi egiazko Jinkoa, ororen kreatzailea!
Erremestiatzen zütügü, Jauna, Errege photeretsia.
Zure lagüntzarekilan ükhen dügü biktoria.