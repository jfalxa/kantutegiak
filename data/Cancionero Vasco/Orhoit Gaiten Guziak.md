---
id: ab-2835
izenburua: Orhoit Gaiten Guziak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002835.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002835.MID
youtube: null
---

Orhoit gaiten guziak hiltzeko orenaz,
Munduko izaitia zonbat den gorenaz.
Jinko Jauna adora obrarik oberena,
Gaztigatu gabetarik infernuko pena.