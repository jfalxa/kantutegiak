---
id: ab-2066
izenburua: Yarraikitzen Ninduzun (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002066.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002066.MID
youtube: null
---

Yarraikitzen ninduzun argi eder ari
Nola mariñel ona bere izarrari.
Yende onak, atentzio! Yende onak atentzio!
nere arrazoinari:
Ez zaiteztela fia amodioari.