---
id: ab-2012
izenburua: Gure Aur Onen Aur Ona (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002012.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002012.MID
youtube: null
---

Gure aur onen aur ona,
Balio baitu Baiona;
Baionarekin Frantzia,
Ingalatierra guzia.

Santa loa, loa, loa,
Nik emanen det korradoa.
Eman dezauzu gure aur oni
Bi ordutako lua.

Bolon bat eta bolon bi,
Bolon zubitik erori.
Erori bazen erori,
Etzen geokoik egarri.