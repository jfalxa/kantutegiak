---
id: ab-1502
izenburua: Agostoko Illaren Emezortzigarrena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001502.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001502.MID
youtube: null
---

Agostoko illaren
Emezortzigarrena,
Bertso bi paratzeko
Artu dut ordena.
Umiak zer kusten duten
Ikasten aurrena,
Lenengo kostunbria
Jarririk aurrena,
Beti maldiziua
Itzarik onena.