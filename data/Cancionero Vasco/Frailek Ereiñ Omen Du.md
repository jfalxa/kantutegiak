---
id: ab-2085
izenburua: Frailek Ereiñ Omen Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002085.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002085.MID
youtube: null
---

"Frailek" ereiñ omen du gaitzeru bat baba;
Baliatuko zayo, urtabia bada.
Orain beliak juaten omen dira ara;
Obe luke bere andre beltza izugarri para.