---
id: ab-2264
izenburua: Logiro Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002264.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002264.MID
youtube: null
---

Logiro naiz eta logale,
Argi-ollarrak jo gabe.
Bart arratsian gure atian
Gaztiak etziran logale.