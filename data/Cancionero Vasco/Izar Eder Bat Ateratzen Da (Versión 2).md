---
id: ab-2129
izenburua: Izar Eder Bat Ateratzen Da (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002129.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002129.MID
youtube: null
---

Izar eder bat ateratzen da
Urtian egun batian,
Urtian egun batian eta
Ura San Juan goizean.