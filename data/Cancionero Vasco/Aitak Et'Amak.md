---
id: ab-1547
izenburua: Aitak Et'Amak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001547.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001547.MID
youtube: null
---

Aitak et'amak herritik,
Maitea, ni zugatik
Donibanera bialdu naute
Arbonatik,
Ez izateko zurekin
Entradarik, entradarik.

Ez izan hortaz penarik
Ez ta ere xangrinik;
Amoriua fidelagoa
Urrundanik.
Etzaitut, ez, kitaturen
Sekulan nik, sekulan nik.