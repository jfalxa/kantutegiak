---
id: ab-1550
izenburua: Aitari Gald'In Lizentzia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001550.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001550.MID
youtube: null
---

Aitari gald'in lizentzia,
Ateratzen notizia:
Kaiolan dagon txoria
Ez duk iretzat azia.