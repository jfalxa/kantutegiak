---
id: ab-1776
izenburua: Berrogei Ta Hamar Bortz Urte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001776.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001776.MID
youtube: null
---

Berrogei ta hamar bortz urte badu,
Nik kontrabandan hasirik,
Ofizio maitagarri hau
Biriatun ikasirik.
Egundaino ez dut altxatu
Huntako irabazirik.
Ni bezalako gizonak,
Urkatzia merezi dik.