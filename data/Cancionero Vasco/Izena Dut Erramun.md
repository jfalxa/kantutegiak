---
id: ab-2133
izenburua: Izena Dut Erramun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002133.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002133.MID
youtube: null
---

Izena dut Erramun, lonbria Rigara;
Orra bertso berriak nik Baztanen para.
Inozente aundiyak emen bizi gara;
Denbora baduakigu, kontuak atera:
Bertzenaz sekulako gu galduak gara.