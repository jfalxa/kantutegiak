---
id: ab-1576
izenburua: Ama Birjiña Erkudengoa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001576.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001576.MID
youtube: null
---

Ama Birjiña Erkudengoa,
Elurretako zuria:
Esku batian eguzkia ta
Beste eskuan euria:
Ark guretako gorderik dauka
Konbeni zaigun guztia.

Igurai onbat begira dago
Ermita txiki ortatik
Odeiak eta zurrunbilloak
Bidaltzen ditu bertatik,
Eta frutuak oso ta leial
Gordetzen ditu gugatik.

Ama Birjiña Erkudengoa,
Pekatarien ama zu,
Pekatutikan libratutzeko
Arkitasun bat egidazu,
Edo bestenaz galdua naiz ni
Desanparatzen banauzu.

Lurreko plantak ekusi ezkero,
Nezesidade aundian,
Alsasuarrak dute kostunbre
Disponitzian errian,
Ekar dezagun Erkur indarra
Ia gandegu premian.