---
id: ab-1729
izenburua: Baratzian Erramu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001729.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001729.MID
youtube: null
---

Baratzian erramu.
Probatu duenak erran du
Oraingo mutil gaztiekin
Ez daitekela fidetu.
Ni gaizua fidetu,
tronpaturikan gelditu. (bis.