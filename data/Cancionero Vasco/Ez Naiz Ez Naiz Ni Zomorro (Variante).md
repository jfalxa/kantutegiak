---
id: ab-2383
izenburua: Ez Naiz Ez Naiz Ni Zomorro (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002383.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002383.MID
youtube: null
---

Ez naiz, ez naiz ni zomorro,
Izanagatik lau begi.
Atoz, atoz nere ondora,
Etzazuela igesi.