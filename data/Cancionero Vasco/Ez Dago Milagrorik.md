---
id: ab-1936
izenburua: Ez Dago Milagrorik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001936.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001936.MID
youtube: null
---

Ez dago milagrorik Jaungoikoarentzat;
Nai duena egin duela iduritzen zait.
Bere gorputza dauka ostian guretzat;
Ai! au kariñua du ark gizonarentzat.