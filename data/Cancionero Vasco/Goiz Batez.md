---
id: ab-1977
izenburua: Goiz Batez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001977.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001977.MID
youtube: null
---

Goiz batez haiñen prezizki,
Ekhiaren jalkhitziareki,
Sü gorri bat da agertüren,
Eta lürra txispiltüren.

Ekhia da ülhündüren,
Argi guziak galdüren:
Bai eta argizagia,
Gure gaiazko argia.

Odeiak tira jaikiren
Eta koleran jarriren:
Haitze tenpesta irurtzirik
Orrokoz eta ...

Haur tipiak sabeleti
Oihü eginen azkarki:
- Jinkoa, entzün gitzazü,
Gaitzetik libra gitzazü.

Jesus jauna da jinen
Odei handi batetan gañen:
Bere aingürü santieki
Eta arkangelieki.

Baruak eta nubliak,
Mündü huntako erregiak,
Bardin dirate paubriak
Jesu-Kristez jüjatiak.