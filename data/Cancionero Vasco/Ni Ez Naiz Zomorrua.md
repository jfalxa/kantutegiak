---
id: ab-2382
izenburua: Ni Ez Naiz Zomorrua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002382.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002382.MID
youtube: null
---

Ni ez naiz zomorrua,
Izanagatik lau begi;
Atoz, atoz onara,
Etzazuela igesi.
Tranlarai, tranlarai, rai, rai...