---
id: ab-2557
izenburua: Txit Egun Ditxosua Degu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002557.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002557.MID
youtube: null
---

Txit egun ditxosua
Degu Eguarri,
Jesus zergatikan dan
Gugana etorri.
Guziok bear degu
Debozioan jarri,
Graziak emaiteko
Jayo dan aur oni.