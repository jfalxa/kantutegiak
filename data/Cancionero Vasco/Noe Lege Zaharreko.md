---
id: ab-2392
izenburua: Noe Lege Zaharreko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002392.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002392.MID
youtube: null
---

Noe, Lege Zaharreko gizon famatua,
Zuk landatu zinuen lehen mahastia.
Aihen balius hura
Nork eman zaitzun burura
Lurrian landatzera?
Gizona tristezian
Kausitzen den tenorian,
Hark dauka konsolatuia. (bis.