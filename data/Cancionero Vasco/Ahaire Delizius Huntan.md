---
id: ab-1514
izenburua: Ahaire Delizius Huntan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001514.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001514.MID
youtube: null
---

Ahaire delizius huntan bi berset nahi tüt khantatü,
Ene bizitza mulde gaitza münd'orori deklaratü.
Ihun sos bat ebatsi gabe, ez eskandalik txerkhatü,
Hamar urtheren galeretan nahi ükhen naie sarthü.