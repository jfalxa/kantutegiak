---
id: ab-2001
izenburua: Goyen Tenploan Dago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002001.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002001.MID
youtube: null
---

Goyen tenploan dago
Amandre Santa Ana,
Iru lirio arrosa
Eskuen dauzkela.