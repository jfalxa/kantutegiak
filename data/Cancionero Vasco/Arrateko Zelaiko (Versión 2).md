---
id: ab-1664
izenburua: Arrateko Zelaiko (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001664.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001664.MID
youtube: null
---

Arrateko zelaiko
Bai floridadea:
Andixek gora dago
Zerura bidea.