---
id: ab-1935
izenburua: Ez Da Munduan Arrosarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001935.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001935.MID
youtube: null
---

Ez da munduan arrosarik arantzarik gabe:
Xarmagarriak emaiten deraut, a! dolore.
Jendiak ere mintzo dira, ez arrazoinik gabe,
Nik ere faltak baditut, bañan bai eta zuk ere.
Nereganat etortzeko ez izan herabe;
Gero ere behar gira biak adiskide.

Botoilako arnuaren gustu etzelenta:
Hunek alegeratzen deraut, oi, neure bihotza.
Ez da munduan zaharrik, ez eta're gazterik,
Hunek bezala neure bihotza alegeratzen duenik.
Berriz ere balin banu ttantta bat huntarik,
Edan ere banioke basua beterik.

Maiatzian julufraiak eder koloria.
Xarmegarria, erran dautazu, otoi, egia,
Badudanez progotxurik zu baitan fidaturik;
Edo bertzenaz, kita nezazu arras bihotzetik.
Zureganako esperantza galduz geroztik,
Bertzeño baten txerkatzera ensaiaturen niz.