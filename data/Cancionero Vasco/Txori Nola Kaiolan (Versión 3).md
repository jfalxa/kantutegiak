---
id: ab-2764
izenburua: Txori Nola Kaiolan (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002764.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002764.MID
youtube: null
---

Txori nola kaiolan
Triste du kantatzen;
Ez jan ez edalarik
Kanpua desiratzen:
Arren, arren,
Libertadea zein eder den.