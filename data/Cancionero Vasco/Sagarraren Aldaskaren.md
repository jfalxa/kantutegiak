---
id: ab-2988
izenburua: Sagarraren Aldaskaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002988.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002988.MID
youtube: null
---

Sagarraren aldaskaren
Igarraren
Puntaren puntan,
Puntaren puntan,
Txoriñua oi! zeguen kantari:
Ai, txiriririri, txiririririri,
Norek kantatuko du boza ori
Dakiyenak ongi, ra ra ra...