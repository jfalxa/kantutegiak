---
id: ab-2755
izenburua: Santibatek Andere (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002755.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002755.MID
youtube: null
---

Santibatek, andere,
Aurthen bezala gerore,
Santibatek igortzen gaitu )
Xingarketa gu ere. ) bis

(Ez) dugu nahi urdia,
Ez eta're erdia;
Liber'erdiko zatia eta )
Gerrenaren betia. ) bi.