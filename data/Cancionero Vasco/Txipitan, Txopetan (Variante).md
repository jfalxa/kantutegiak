---
id: ab-3394
izenburua: Txipitan, Txopetan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003394.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003394.MID
youtube: null
---

Txipitan, txopetan
Ganbelapeko,
Ez dena gordetzen
Berak beteko,
Berak beteko.
Gorde zate. Kuku.