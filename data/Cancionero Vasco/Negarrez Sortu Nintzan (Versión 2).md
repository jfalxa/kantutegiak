---
id: ab-2366
izenburua: Negarrez Sortu Nintzan (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002366.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002366.MID
youtube: null
---

Negarrez sortu nintzan negarrez hiltzeko,
Ez beiñepen munduan luzez bizitzeko.
Aita-ama maitiak, mundura zertako
Eman izan nauzue abandonatzeko.