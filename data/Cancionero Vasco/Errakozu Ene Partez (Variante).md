---
id: ab-2719
izenburua: Errakozu Ene Partez (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002719.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002719.MID
youtube: null
---

Errakozu ene partez mila goraintzi,
Zure adixkide Musiu Barnetxi,
Sarakoari.
Ongi pena dudala segurki
Egundaino ez dudala hura baizik utzi
Satisfatu gabe ederki.
Kausa zer den berak badaki.
Neskatila gaztekin omen dela bizi
Libertituz ederki.
Oraino ez da libertitu aski,
Ez eta ere ez ditu kitaturen, naski.
Haren begiak hain dire argi
Izarrak iduri
Eta koloriak egun ostoko
Arrosa bezin gorri.
Neskatila gazteri ilusigarri
Karesatzen ditu ederki.
Denboraz balia bedi.