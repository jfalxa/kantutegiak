---
id: ab-2703
izenburua: Aingiruen Erregina
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002703.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002703.MID
youtube: null
---

Aingiruen Erregina,
Orierriagan xarririk,
Aingiruen konpañian )
Gloria guztiz beterik. ) bis

Zu zarade ederrena
Kreaturen ertean;
Gloria ere andiago )
Zeru eta lurrean. ) bis

Sandulariak ekusi du
Gozo andiz beterik,
Yesus Yainkoak eman dio )
Benedizio ederrik. ) bi.