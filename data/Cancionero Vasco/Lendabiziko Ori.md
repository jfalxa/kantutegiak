---
id: ab-2254
izenburua: Lendabiziko Ori
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002254.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002254.MID
youtube: null
---

Lendabiziko ori,
Txoporren ori,
Bestek guziak baño
Txoporragoa dek ori
Txingilingi txongi )
Txingilingi txon )bis

Bigarren ori,
Subien ori,
Beste guziak baño
Subiago dek ori.
Txingilingi ...

Irugarren ori,
Luxien ori,
Beste guziak baño
Luxiago dek ori.
Txingilingi ...

Laugarren ori,
Mantxoen ori,
Beste guziak baño
Mantxoago dek ori.
Txingilingi ...

Bostgarren ori,
Txikien ori,
Beste guziak baño
Txikiago dek ori.
Txingilingi ...