---
id: ab-2185
izenburua: Kaiolan Nintzenian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002185.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002185.MID
youtube: null
---

Kaiolan nintzenian maiz nindagon triste;
Kanpoko lagunetan bainuen sineste.
Lebretasuna ona zela nuen uste,
Orai arte.
Amets, amets zoroa, hik egin nauk kalte. (dautak)

Banuen zertarik yan, bai eta zer edan;
Deusen eskasik etzen neretzat kaiolan.
Bainan guziangatik nintzen errenkuran:
Banuen lan,
Aski zabalik ezin aterik atxeman.

Ene artatzailiak, fidaturik nitan,
Atia idekirik behin utzi zautan.
Nahi nuena egin orduan nik hautan,
Lorietan!
Lagunetara laster yuan nintzen airetan.

Gauaz beldur bizian, egunaz ikaran,
Noiz nahi irriskatzen sartzia segadan.
Holakorik etzautan bururat kaiolan
Nintzenian:
Zoin gaizkiago naizen, hobeki ustian.

Eglattuak oraino ditut laburregi;
Ni nola ihes egin belatx gaixtueri?
Bertziak bezin airos enaiteke segi
Laguneri.
Hau lana eman diotan ene buruari!

Ene egalak aski luzatu orduko,
Uda akabaturik, negua asiko.
Berriz uda lehenik ez da etorriko
Enetako:
Izotz elur hormetan ni enaiz biziko.

Gaztiak nahi badu egon segurian,
Ez dadiela eror nik egin hutsian;
Bainan asmu hoberik harturik buruan,
Deskantsuan,
Egon bedi kaiola ongi zerratuan.