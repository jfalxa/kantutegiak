---
id: ab-2245
izenburua: Kuku Bat Badut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002245.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002245.MID
youtube: null
---

Kuku bat badut xistrai onetan,
Ez baita asi kantatzen.
Asten danian kantatzen "Kyrie, Kyrie",
Asten danian kantatzen "a la maison",
"Kyrie eleison".