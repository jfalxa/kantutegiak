---
id: ab-1931
izenburua: Etxian Eder Galtzada
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001931.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001931.MID
youtube: null
---

Etxian eder galtzada.
Garzinatua, non zara?
Garazina, Garazina,
Mudatu zan gogua.