---
id: ab-1886
izenburua: Ene Maitia Errazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001886.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001886.MID
youtube: null
---

Ene maitia, errazu,
Zerk hola xangrintzen zaitu?
Ikustera jinen nitzaizu;
Beraz, kuraje har zazu:
Amodioz nahi ez badute,
Bortzaz eginen gituzu.