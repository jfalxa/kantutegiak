---
id: ab-2443
izenburua: Prantzez Euskera Deitzen Dioten
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002443.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002443.MID
youtube: null
---

Prantzez euskera deitzen dioten
Sarako mendi batian,
Aur kuttun ori billatu zuten
Ibarso zabal batian.
Erri noblian suertatu zan
Gure kondaya maitian
Irakurriyaz edo entzunaz
Zer pasa dan esatian
Milla sei eun irurogei ta
Amairugarren urtian.