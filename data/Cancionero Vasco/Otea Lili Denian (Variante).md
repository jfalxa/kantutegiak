---
id: ab-2429
izenburua: Otea Lili Denian (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002429.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002429.MID
youtube: null
---

Otea lili denian,
Xoria aren gainian.
Ura antikan yuaiten da
Berak plazer duenian:
Zure t'eni'amoriua
Ola dabila airian.

Argia dela diozu,
Gauerdi oraino eztuzu.
Enekilako denbora
Luze iduritzen zaizu.
Amodiorik eztuzu,
Orai zaitut ezagutu.