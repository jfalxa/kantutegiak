---
id: ab-2843
izenburua: Atoz Espiritua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002843.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002843.MID
youtube: null
---

Atoz, Espiritua,
Atoz gu laguntzera.
Arren, jetxi zaitea
Gure biotzetara.

Jauna, bete gaitzazu
Zure emaitz ugariez,
Irazeki gaitzatzu
Zure su-gar biziez.

Oi alaitzale gozo!
Atseden emailea!
Zu zaitugu neketan
Gure laguntzailea.

Zu zera betidanik
Maitetasuna bera,
Osoki etsigia,
Gu guzion aldera.

Oi! zenbat argaltasun
Dan gure barrenetan!
Estu lotuak gaude
Pekatuzko katetan.

Goitik bidali itzazu
Zure argi-izpiak,
Ta argituko dirade
Ilunpe lazgarriak.

Griña gaiztoak beti
Garamazki gaizkira.
Zure laguntzak gaitu
Erakartzen ongira.

Urrun zazu gugandik
Gure etsai gaiztagiña,
Beti gu galdu nayez
Zelatan dabillena.

Bai, urrun zazu, Jauna,
Satan etsai gaiztoa,
Emaguzu pakea
Ondasun ain gozoa.

Aitaren Semea
Beti bedi goratu,
Espiritu Santua
Biekin andizkatu.