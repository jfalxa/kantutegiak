---
id: ab-1510
izenburua: Agur Agur Jende Noblia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001510.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001510.MID
youtube: null
---

Agur, agur, jende noblia
Berri on bat dekargu:
Atentzi'on bat eruki beza,
Iñork aditu nai badu;
Atentzi'on bat eruki beza,
Iñork aditu nai badu.