---
id: ab-1730
izenburua: Baratzian Zein Den Eder
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001730.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001730.MID
youtube: null
---

Baratzian zein den eder
Porru eta aza-landare.
Zure begiek distiratzen dute
Zillar-diamantaren pare.
Ai, hura lili polita!
Zein den xarmanta!
Enaite banta,
Ai, hura lili polita!