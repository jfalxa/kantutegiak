---
id: ab-2812
izenburua: Una Planeta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002812.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002812.MID
youtube: null
---

Una planeta, salsa mison
Kanutison, kanutison.
Xunbe, xunbe de la "tronpeta".
La rriskitin, rriskitin "Kitarra".
La zirrinkitin, zirrinkitin "bioliñe".
La ttunkuttun, ttunkuttun "danboliñe".
La fliflitin, fliflitin "flaiola".
Bolon eder.