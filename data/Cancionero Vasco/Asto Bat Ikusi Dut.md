---
id: ab-1683
izenburua: Asto Bat Ikusi Dut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001683.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001683.MID
youtube: null
---

Asto bat ikusi dut hiru zangorekin,
Madrilat gateko atso zar batekin.
Ai Beleto, Beleto, Beleto,
Ai Beleto, Beleto, mato.

Atso bat ikusi dut asto zangorekin,
Madrilat gateko asto zar batekin.
Ai Beleto,...