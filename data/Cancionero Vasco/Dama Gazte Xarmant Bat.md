---
id: ab-1827
izenburua: Dama Gazte Xarmant Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001827.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001827.MID
youtube: null
---

Dama gazte xarmant bat jiten zaut bistara;
Iduritzen baizeraut zeruko izarra.
Ama batek egiteko hori zerbait bada.
Gure Salbatzaileak konserba dezala,
La, la, la, la, la, la, konserba dezala.