---
id: ab-1686
izenburua: Agur Agur Otsoa (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001686.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001686.MID
youtube: null
---

- Agur, agur, Otsoa!
- Ongi etorri, Astoa!
- Nik iretzako bazekarreat
Zagian ardo gozoa.