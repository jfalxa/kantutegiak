---
id: ab-2089
izenburua: Abenduaren Zazpian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002089.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002089.MID
youtube: null
---

Abenduaren zazpian
Sartu giñaden Frantzian,
Ogei ta amarrak lakari
Gatz bi azukre erdian;
Pasatu giñen mendian
Ustez sekreto aundian.