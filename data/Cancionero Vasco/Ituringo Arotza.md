---
id: ab-2052
izenburua: Ituringo Arotza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002052.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002052.MID
youtube: null
---

Ituringo arotza, Erramun Joakin,
Asarre omen zaude zeren degun jakin
Santurik ez laiteke fiatu zurekin:
San Kristobal urtu-ta joaliak egin.

Ituringo arotza bere andreari:
- Urtu biar deñegu; ekarrak Santu ori.
- Gizona, zaude ixilik, bekatu da ori.
- Eztioñegu erranen sekulan iñori.