---
id: ab-2645
izenburua: Zure Biotzak Ez Dau
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002645.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002645.MID
youtube: null
---

Zure biotzak ez dau
Kariño jojian,
Esango zer dezun:
Aizeke guzia.