---
id: ab-1670
izenburua: Arri Arri Mandua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001670.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001670.MID
youtube: null
---

Arri, arri, mandua,
Altza, Mari tontua.
Ur orotan edan, eta
Borta orotan zaldare.
Halere, halere,
Ezin kurri batere.