---
id: ab-2155
izenburua: Bedeinkatua (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002155.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002155.MID
youtube: null
---

Bedeinkatua izen daela
Etxe onetako yendia,
Probe ta ongi dabillanentzat
Badute borondatia.
Santa Martiri maitia
Dago errukiz betia:
Berak alkantzatu diagula
Osasun eta bakia.

Libertadia eskatzen diot
Etxeko prinzipalari
Santa Agedaren alabanzak
Kantatutzera nua ni.
Graziak diozkat ipiñi
Santa bedeinkatu oni
Kristandadian onen izena
Beti santifika dedi.

Nork esango du zer pasa zuan
Munduan Santa Agedak.
Lendabiziko azotatua;
Kendu diozkatzen bularrak.
Gorrotuaren indarrak
Jentillen biotz gogorrak
Istante baten estali zion
Gorputz guztia odolak.

Santa au preso sartu zanian
Zan milagro bat andia;
Zeruetatik jatxi zitzaion
Jaungoikuaren argia.
Milagro ikaragarria
Santa onen alegria
San Pedro berak kuratu zion
Santa oni gorputz guzia.

Adios orain esaten det
Santa onekin batian
Urte ontxo bat pasa dezaten
Osasunaz ta pakian
Pobriak geran artian
Beti umillak gaitian
Karidadiak lagunduko du
Zeruetako artian.