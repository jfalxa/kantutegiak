---
id: ab-1699
izenburua: Au Da Parragarri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001699.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001699.MID
youtube: null
---

Au da parragarri,""válgame Dios""
Abuelo zarren batek kantu berriok.
Nik norbaiti ere gald'egin biar diot
Nondik ote ditudan ezpateko aldiok.
Itza ematen diot,
Mila amodio,
Nagolaik serio,
Ixilduko tudala erronkariok.

Nere partida nor den ezta deklaratzen;
Sasitikan asi zait arri tiratzen.
Ukitu ezpanindu, ixilik nengoen.
Nereganik eztakit zernai ote duen.
Ez nauk ez ire men,
ez orai eta ez len,
Zenbat uste duken.
Fruitutik ezagun duk arbola zer den.

Norbait ere munduan largatzen asi da
Argineren bat dela nere partida.
Gaztia bide da, bañon ez gizon abilla;
Zertan ote dabilla okasio billa.
Goguak segida
Laguntzen badira,
Ez daiela fida
Ukanen ez dituken bertsotxo bida.

Ustaritz'ko argin, koplari fina,
Badaukak fantasia komeni adiña.
Dozena bi urte baiu, nik adina,
Ire kontra bertsoka, zer tripako miña!
Segi zak aitziña
Egiñal egiña,
Partida izanen duk ere berdiña.