---
id: ab-1679
izenburua: Arrosarik Ez Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001679.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001679.MID
youtube: null
---

Arrosarik ez da arantzarikan gabe.
Amorio batek daut aunitz pena dolore.
Esposatzeko zurekin, samur aita amekin
Biar nuke egin. Ez dakit zer egin
Nik nere jendekin, ez eta ere zurekin.