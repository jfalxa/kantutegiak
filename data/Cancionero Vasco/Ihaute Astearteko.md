---
id: ab-2881
izenburua: Ihaute Astearteko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002881.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002881.MID
youtube: null
---

Ihaute astearteko,
Goiz eta arratsaldeko,
Alegeraki plazan dantzan
Gazteak, deskantsatzeko.
Soinua ere izan dugu
Gustura libertitzeko,
Gustura libertitzeko.

Libertizioneak hargatik
Bere fluketa ondotik.
Konfesatzera joan izan gira
Gure aldian gogotik.
Atsuluzione idorra ukhen
Herriko erretoretik,
Herriko erretoretik.

- Jaun erretora, adizazu
Hitz bat, plazer baduzu:
Penitent gaixoer hamar ostiko
Eman nahi omen duzu.
Bainan Jesusen legeak hori
Hola manatzen ez dautzu,
Hola manatzen ez dautzu.