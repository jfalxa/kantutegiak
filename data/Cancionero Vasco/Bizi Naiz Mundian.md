---
id: ab-1798
izenburua: Bizi Naiz Mundian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001798.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001798.MID
youtube: null
---

Bizi naiz mundian aspaldi haundian,
Beti tristezian, pena bihotzian.
Amodiotan nintzan lili batekilan;
Hura ezin ikus plazer dudanian,
Ez baitut etxian, ez eta herrian.

Beti penetan dena, xangrinetan dena,
Nork pentsa lezake haren hasperena,
Eta partikularzki bere faltaz dena.