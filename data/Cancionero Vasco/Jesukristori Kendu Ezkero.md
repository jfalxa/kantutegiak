---
id: ab-1836
izenburua: Jesukristori Kendu Ezkero
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001836.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001836.MID
youtube: null
---

Coro:
Damu dot, Jauna,
Biotz guztitik
Zeu ofendidu izana.
Ez, ez geyago
Pekaturikan,
Neure Jaungoiko lastana.
Larda kulparik
Betoz argaitik
Pena oriek nigana.

Solo:
Jesukristori
Kendu ezkero
Pekatuakin bizitza,
Baldin ezpadot
Negar egiten,
Arrizkua dot biotza.
Guztiok lagun
Kanta dezagun
Bere penazko eriotza.