---
id: ab-2698
izenburua: Txori Papogorriak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002698.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002698.MID
youtube: null
---

Txori papogorriak
Tristerik du kantatzen;
Gauazko ostatue
Kanpuan du xerkatzen.
Ni ere orai
Ala naiz aurkitzen.