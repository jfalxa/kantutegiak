---
id: ab-2794
izenburua: Bobollo Txina
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002794.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002794.MID
youtube: null
---

Bobollo txina bonbollo.
Otxuak gan du artzaiña.
Otxokumiak bildotsa.
Ai! nere biotza.