---
id: ab-2626
izenburua: Zer Esaten Dirazu Magdalenia?
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002626.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002626.MID
youtube: null
---

- Zer esaten dirazu, Magdalenia? (bis)
- Resuzitatu dela berorren Semia
Ai, au gozua! ai, au gozua!
Pazko eguna da ta gloriosua.