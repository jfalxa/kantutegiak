---
id: ab-2137
izenburua: Jaunak, Hemen Bagaituk
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002137.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002137.MID
youtube: null
---

Jaunak, hemen bagaituk orain lau lagun;
Botoila bana arno edan dezagun.
Ona den edo ez, jasta dezagun;
Ta ona balin bada, bina har dezagun.

- Arno hobiagorik etxean baduzu
Trago bana edateko, hartarik iguzu.
Urreneko aldian pagaturen tugu,
Oraingoan bezala, galtzen ez bagaitu.

- Jaunak, edan zazue ene graziari,
Eta kontuak paga etxeko andreari...
Nik ez baitiat kausi zuen omorerik,
Trago bana egin'ta, zoazte hemendik.