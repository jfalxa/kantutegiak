---
id: ab-1775
izenburua: Berrogei Bat Mutilzar Gaude
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001775.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001775.MID
youtube: null
---

Berrogei bat mutilzar gaude erriyan;
Irurogei neskazar pendiente aundiyan
Sartu iyiyan.
Plazaran biar dute urren periyan,
Bat ero bat an saldu leike ariyan.