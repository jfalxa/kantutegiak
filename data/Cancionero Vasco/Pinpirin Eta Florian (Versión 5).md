---
id: ab-2751
izenburua: Pinpirin Eta Florian (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002751.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002751.MID
youtube: null
---

Pinpirin eta florian dago,
Florian eder xoxua.
Hak hura galdu, nik hura banu,
Andretan nuke gogua.

Konpainian den ederrena da
Manex presentekua.
Manex hunek ederki;
Yuana horrek hobeki.
Horiek horiek eremanen dute
Beren bizia airoski.
Etxe egin berria,
Ganbera lehio gabia,
Atea klin, klan, zerratu eta
Oi! zer besarkada aldia.
Yuana, kontent zireia.