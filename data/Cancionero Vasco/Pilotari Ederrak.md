---
id: ab-2439
izenburua: Pilotari Ederrak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002439.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002439.MID
youtube: null
---

Pilotari ederrak Oyarzunen daude,
Jokazale garbiak tranparikan gabe.
Oriek ziotenaz baziren len ere.
Guziok dakitena bai eta orain ere.
Nik olakorik ez det goguan beinere.