---
id: ab-2227
izenburua: Abenduaren Lendabiziko (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002227.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002227.MID
youtube: null
---

Abenduaren lendabiziko
Goizeko zazpietan zen;
Tenplo sanduan egondu giñen
Sermo eder bat aditzen:
Jesusen llaga preziosuak
Astera nua berritzen,
Zeruko Aita, lagun didazu
Entendimentu argitzen.