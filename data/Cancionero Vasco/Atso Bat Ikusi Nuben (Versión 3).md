---
id: ab-1690
izenburua: Atso Bat Ikusi Nuben (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001690.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001690.MID
youtube: null
---

Atso bat ikusi nuben
Iru eun urtetan
Mariñela zebillena
Barkoko soketan.
Aren attuna berriz
Itsasura urketan,
Eskalapuñen ordez
Bi txalupa anketan.

Baie artuta eio
Errotak iralki,
Kanebatek erreta
Labeak ebaki.
Txantxangorri batenak
Sei libra tripaki;
Oien madre zein den
Xenpelarrek daki.