---
id: ab-2334
izenburua: Mila Zortzi Ehun (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002334.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002334.MID
youtube: null
---

Mila zortzi ehun eta hoi ta hirurian,
Xerbixuan ginauden Donostia hirian,
Liberalitateko bandera berrian;
Hartarik desertatu inozentkerian,
Miserable erori etsayen erdian.