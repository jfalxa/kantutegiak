---
id: ab-2347
izenburua: Mundua Erho Enganagarri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002347.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002347.MID
youtube: null
---

Mundua erho, engañagarri,
Aunitz galtzen dituzu:
Beti zurekin egoiteko
Gogua ematen duzu;
Zu utzirik nahi eta ez,
Hemendik joan behar dugu.