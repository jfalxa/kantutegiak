---
id: ab-2030
izenburua: Ikusten Det Ikustera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002030.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002030.MID
youtube: null
---

Ikusten det ikustera ez urrutikoa,
Ez urrutikoa,
Martina eder galant presentekoa.
Amorioa de badu, ta emaitera noa,
Emaitera noa:
Luis eder galant Etxeberrikoa.