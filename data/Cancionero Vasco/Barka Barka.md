---
id: ab-1733
izenburua: Barka Barka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001733.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001733.MID
youtube: null
---

Barka, barka, bara, bara zure kolera:
Aldara mement bat begi asarrea;
Bekatu egin dut, begian dut nigarra,
Jainko guziz ona, barka nere krima.