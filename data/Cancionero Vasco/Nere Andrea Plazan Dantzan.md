---
id: ab-3023
izenburua: Nere Andrea Plazan Dantzan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003023.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003023.MID
youtube: null
---

Nere andrea plazan dantzan
Epher errez aserik;
Ni gaizo hau, supazter zokhoan,
Belhaun-buruak errerik;
Belhaun-buruak errerik eta
Arthoz ere goserik.

Nere andrea goiz jekitzen da
Plazarat behar denian;
Buruko-mina lotzen zaio
Lanerat behar denian.
Holako andrea gizonak on dik
Ohean eri denian.

Nere andrea jeki orduko
Zazpiak edo zortziak.
Handikan goiti etzaizko sobra
Berai dohazkon guziak:
Neretzat xardin erdi bat eta
Beretzat xokolatia.

Nere andreak iduri du
Zaku gaizki josia.
Sudur handi, bizkar makhur,
Ipurdi maila handia.
Gaztaina errez ase eta
Oi, zer tronpeta-joilia.