---
id: ab-2151
izenburua: Jesusa Jaio Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002151.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002151.MID
youtube: null
---

Jesusa jaio da,
Zerutik jatzi da.
Musikeria da
Dabela beria da.
Aingeruak kantatzen,
Jesus adoratzen,
Ez ziran kantzatzen.

Iru Errege datoz
Oraien artetik,
Oraien artetik,
Erregen erregia
Ikustiagatik.

Beltxior ta Kaspar,
Bidian azkar-azkar.
Baldasar Errege
Antzan ara ere
Bisitatutzen.
Irurak joan ziraden
Jesus billatzera.

Zeuorren jantzia
Eta presentzia...
Nunguak zerate?
Erri onetakuak
Zuek ez-zerate.