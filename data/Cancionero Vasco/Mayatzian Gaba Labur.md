---
id: ab-2730
izenburua: Mayatzian Gaba Labur
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002730.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002730.MID
youtube: null
---

Mayatzian gaba labur,
Iruretan argiya.
Ni ordurantxe etxeratzen naiz,
A! zer etxera sariya!
Purruxtan, purruxtan,
Purruxtan Mariya,
A! zer etxera sariya!