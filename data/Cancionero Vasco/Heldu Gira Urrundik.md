---
id: ab-2116
izenburua: Heldu Gira Urrundik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002116.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002116.MID
youtube: null
---

Heldu gira urrundik,
Gaua bidean emanik;
Gaua bidean emanik eta
Ihauteria entzunik.

Landan eder gereta,
Bachenabarren Heleta...
Zerbeit merezi ginuke
Handik hunat jin eta.

Ametzak eder azala,
Haritz gazteak bezala...
Zu, etxeko-andre zaharra,
Etxe huntan bada zure beharra.

Hortxe zaude jarririk,
Koloria gorririk;
Gure zerbeiten emaiteko
Jaiki behar zira hortik.

Goazen, goazen hemendik,
Hemen ez duk xingarrik...
Etxe huntako gazi kutxan
Saguak humeak egiten tik.