---
id: ab-3389
izenburua: Alan, Alan, Kinkirrinera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003389.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003389.MID
youtube: null
---

Alan, alan, kinkirrinera,
Alan, alan, zanburrera,
Maritxu plaza berriko
Gira ari beste aldera.
San Marziel aldera.