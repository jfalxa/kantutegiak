---
id: ab-2648
izenburua: Ilargi Dela Ilargi (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002648.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002648.MID
youtube: null
---

Ilargi dela ilargi,
Gauak eguna iduri.
Ene maite bihotzekua
Amak ogerat igorri.
Iatzarria balin badago,
Hastera nago kantari.

- Ene maitea, non zira?
So egidazu leihora.
Hemen bagira bi lagun,
Jinak zure ikustera.
Emazu (i)edatera,
Oro gaiten alegera.

- Ez naiz jekiren leihora,
Are gutiago bortara.
Zoazte aitarengana,
Harekin konpartitzera.
Haren boza baduzuie,
Edatera izanen duzuie.

Aitaren errepostuia,
Huna zer den, maitia:
- Kaiolan dudan xoria
Ez diat zuentzat hazia.
Hobeki eginen duzuie
Bide horiek uztia.