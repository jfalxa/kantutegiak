---
id: ab-2307
izenburua: Marrukuetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002307.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002307.MID
youtube: null
---

Marrukuetan zer pasatzen dan
Bagatoz enteratzera,
Konsueluak jarri nai ditut
Personaren biotzera.
Historia au kantatuko det:
Atozte enteratzera;
Papera bana danak artuta
Eramateko etxera.