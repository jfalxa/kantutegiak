---
id: ab-1996
izenburua: Goizian Porrusalda
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001996.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001996.MID
youtube: null
---

Goizian porrusalda,
Egurdien aza,
Aise kabitzen naute
Epurdien galtzak.
Zer zela uste yuan
Atxuaren salsa?
Zarra izango bada
Aberatsa ar zak.