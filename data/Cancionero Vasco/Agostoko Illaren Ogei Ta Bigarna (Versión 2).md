---
id: ab-2467
izenburua: Agostoko Illaren Ogei Ta Bigarna (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002467.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002467.MID
youtube: null
---

Agostoko illaren ogei ta bigarna,
Lapurrak San Migelen sartu ziradena.
Bai etare arruatu berek nai dutena,
Usterrik ilegiyua Aingeruarena:
Arek emaiten badaut biotzian pena.