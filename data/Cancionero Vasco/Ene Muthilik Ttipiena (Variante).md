---
id: ab-1889
izenburua: Ene Muthilik Ttipiena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001889.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001889.MID
youtube: null
---

"1. Ene mutilik ttipiena,
Ene mutilik maitena,
Habil harat, habil hunat,
Mastañoaren puntarat,
Mastañoaren puntalat eta
Ageri denez leihorra.

- Ene nagusi maitea,
Heldu naiz ongi tristerik:
Ez dut ikusi leihorrik
Ez eta ere belarik.
Norbeit yan behar baduzie,
Hil nezazie lehenik.

(como la 1ª)

- Ene nagusi maitea,
Heldu naiz alegerarik:
Ikusi dizut leihorra,
Angeletareko hiriak.
Zure arreba Mariena
Galerian yosten ari da.