---
id: ab-1866
izenburua: Orra, Orra, Gure Orentzago (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001866.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001866.MID
youtube: null
---

Orra, orra, gure Orentzago,
Pipa ortzian dula, exerita dago.
Kapoiak ere baitu arrautzatxuakin,
Biar meriendatzeko botilla arduakin.