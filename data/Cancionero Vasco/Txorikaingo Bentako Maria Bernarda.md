---
id: ab-2560
izenburua: Txorikaingo Bentako Maria Bernarda
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002560.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002560.MID
youtube: null
---

Txorikaingo bentako
Maria Bernarda, (bis)
Aurtxo bat egitea
Zuetzat gala da.