---
id: ab-1957
izenburua: Gazte Gira, Gazte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001957.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001957.MID
youtube: null
---

Gazte gira, gazte, hogoira urtetan!
Ez egon pentsaketan joaiteko harmetan.
Guazen oro betan, kuraje haunditan.

Ama, nigarretan, aita ere triste.
Etxean baditut nik hiru anaia gazte:
Laguntza badute, ni etorri arte.

Heldu garenean denbora eginik,
Tokiak kurriturik, franzesa ikasirik,
Oi! ama ixilik, ez egin nigarrik.