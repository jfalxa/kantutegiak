---
id: ab-1487
izenburua: Hola Hola, Juantxo (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001487.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001487.MID
youtube: null
---

- Hola, hola, Juantxo,
Zer modu daukazu?
- Pobre, baña alegre
bizi gerade gu.
- Soka bat ekatzu,
Lotu bear degu.
Baña ez izutu:
Agintzen digutena
Egin bear degu.

Au da Probidentzia,
Ene Jaungoikoa!
Arratoi bat bai dabil
Emen ni añakua;
Grilluak anketan,
Zorriak mantetan.
...