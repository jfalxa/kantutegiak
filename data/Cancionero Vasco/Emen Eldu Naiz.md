---
id: ab-1882
izenburua: Emen Eldu Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001882.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001882.MID
youtube: null
---

Emen eldu naiz beilari,
Gabak eguna dirudi.
Nik biar nuen dama gaztia
Amak lo ote ezarri,
Irazarri al banezake,
Asi biar dut kantari.