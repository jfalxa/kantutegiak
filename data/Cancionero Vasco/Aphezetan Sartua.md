---
id: ab-1626
izenburua: Aphezetan Sartua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001626.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001626.MID
youtube: null
---

Aphezetan sartua da beharraren dirua,
Kantuz eta errepikaz nola aise bildua;
Heien turnabrotxa ez dagola behin ere gelditua.
Bada urso epher kalla ""pularda"" gizendua.

Laborariak jaten badu xardin begi gorria
Artho mutzi pitar minkor osasun galgarria,
Aphezetarik aditzen du hortik:""Debox galduak!"".
Berak nola bizi diren eman gabe guardia.