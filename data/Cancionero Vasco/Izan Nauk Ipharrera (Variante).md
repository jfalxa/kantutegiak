---
id: ab-2126
izenburua: Izan Nauk Ipharrera (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002126.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002126.MID
youtube: null
---

Izan nauk ipharrera hego aldetik,
Iguzki partera mendebaletik;
Eta diat ikusi guzien gainetik
Inozentzia ainitz gizonen partetik.
Abila argatik
Duk egundainotik
Egin gabe deusik,
Ederkiena bizi bertzeren lanetik.