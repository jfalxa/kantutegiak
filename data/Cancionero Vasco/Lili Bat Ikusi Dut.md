---
id: ab-2259
izenburua: Lili Bat Ikusi Dut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002259.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002259.MID
youtube: null
---

Lili bat ikusi dut baratze batian;
Desiratzen bainuen nere sahetsian.
Lorie ez du galtzen udan ez neguian;
Haren parerik ez da bertze bat munduan.

Deliberatu izan dut gau batez joaiterat
Lili agrados haren besoetan hartzerat.
Ez bainuien pensatzen guardiatzen zutela:
Gau hartan uste nuen han galtzen nitzela.

Badut abisu bat emaiteko herriko yenderi
Eta partikulazki gazte dieneri:
Gauaz dabilan hori ez dela zuhurregi.
Ni eskapatu bainaz eskerrak Yainkoari.