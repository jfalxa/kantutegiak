---
id: ab-1697
izenburua: Au Da Ikazketako Mandoaren Traza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001697.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001697.MID
youtube: null
---

Au da ikazketako mandoaren traza:
Zarra eta flakoa, biloa latza,
Itxura gaitza.
Bastepe guzia dauka zorne ta balsa.
Ai, ura salsa!
Aldetik ez daiteke urrinez pasa,
Urrinez pasa, urrinez pasa.