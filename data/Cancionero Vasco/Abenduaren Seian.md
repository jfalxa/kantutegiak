---
id: ab-1482
izenburua: Abenduaren Seian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001482.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001482.MID
youtube: null
---

Abenduaren seian Jondoni Nikole:
Maitiaren kustera jinik, hemen niz ni ere.
- Maitia, jeiki,jeiki, oi plazer baduzu;
Zure kustera jinik orai hemen nuzu.

Ez, ez, ez niz jeikiren, abertitia bainiz
Karesatzen duzula neskatilla ainitz.
- Ezpaduzu amodiorik, ez jeiki nigatik;
Etxera baniazu jin nizan bidetik.