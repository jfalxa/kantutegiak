---
id: ab-1681
izenburua: Aste Juan Danian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001681.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001681.MID
youtube: null
---

Aste juan danian,
Lengo aste batian,
Andre bat ikusi det,
""Tanbirun, tanbiruntena"",
Andre bat ikusi det
Taberna batian: (4 veces)
- Pinta bat arco ekatzu,
""Tanbirun, tanbiruntena"";
Pinta bat ardo ekatzu
Botilla onetan,
Botilla onetan.