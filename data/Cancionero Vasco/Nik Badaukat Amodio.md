---
id: ab-2668
izenburua: Nik Badaukat Amodio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002668.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002668.MID
youtube: null
---

Nik badaukat amodio fidela,
Sekretuan guardatua zuretzat.
Bertziak oro utzirikan,
Xarmegarria,
Paregabia!
Har nezazu zuretzat.