---
id: ab-1916
izenburua: Eskualdun Bat Zen Itsu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001916.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001916.MID
youtube: null
---

Eskualdun bat zen itsu eta pertsulari;
Atez ate zabilan, iloba gidari.
Soineko eta diru, janari-edari,
Nasaiki biltzen zuten pertsu eman sari.