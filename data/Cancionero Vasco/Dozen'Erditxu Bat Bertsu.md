---
id: ab-1853
izenburua: Dozen'Erditxu Bat Bertsu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001853.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001853.MID
youtube: null
---

Dozen'erditxu bat bertsu
Nai nituzke kantatu.
Dozen'erditxu bat bertsu
Nai nituzke kantatu.
Maria santisima
Lagun gezaguzu asi,
Guzion gustorakuak
Ez geraden asi.