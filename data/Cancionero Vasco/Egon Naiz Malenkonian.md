---
id: ab-1860
izenburua: Egon Naiz Malenkonian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001860.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001860.MID
youtube: null
---

Egon naiz malenkonian;
Orain naiz plazeren erdian.
Erranen dautzut urbillen aldian
Zer sendimendu dudan bihotzian.