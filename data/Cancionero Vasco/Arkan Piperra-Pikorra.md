---
id: ab-1642
izenburua: Arkan Piperra-Pikorra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001642.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001642.MID
youtube: null
---

Arkan piperra-pikorra franko;
Musuan usai gozoa.

Presentzian den ederrena da
Juanito konpañiakoa.

Orritxi galai manen diogu
Beraren bezalakoa.

Bera ona da; obia lube
Teresa (Y)Turbidekoa.