---
id: ab-1671
izenburua: Arri Arri Mandoko! (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001671.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001671.MID
youtube: null
---

Arri, arri, mandoko!
Bihar Iruñerako.
Hantik zer ekarriko?
Zapata eta gerriko.
Hek oro norendako?
Gure haurrarendako.