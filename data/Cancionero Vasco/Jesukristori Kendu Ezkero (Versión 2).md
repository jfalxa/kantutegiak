---
id: ab-2143
izenburua: Jesukristori Kendu Ezkero (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002143.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002143.MID
youtube: null
---

Jesukristori kendu ezkero
Pekatuagaz bizitza,
Baldin ezpadot negar egiten,
Arrizkua dot biotza.
Guztiok lagun
Abeztu daigun
Bere penazko eriotza.