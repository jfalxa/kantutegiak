---
id: ab-1497
izenburua: Adizak Donadua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001497.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001497.MID
youtube: null
---

Adizak, donadua, solas bi nigandik.
Konseilu ona hartzak ezkonduetarik.
Gizonak izaiteko nihon deskantsurik,
Gazterik familia firmatu behar dik,
Zahartuta ez dedin egon doluturik.