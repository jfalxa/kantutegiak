---
id: ab-1518
izenburua: Ai, Au Edariren Gozua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001518.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001518.MID
youtube: null
---

Ai, au edariren gozua, zerutik jatsitakua!
Edan zagun gogotik likor gozo onetatik.
Jakin banuan lenago, edango nuen geiago.