---
id: ab-2711
izenburua: Artzai Buru Txuri Bi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002711.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002711.MID
youtube: null
---

Artzai buru txuri bi,
Anton eta Pedro
Belengo portalera
Etorri zaizkigu;
Sartu dira barruna
Manueltxorengana,
Eskeñitzen diote
Arkumetxo bana. (bis.