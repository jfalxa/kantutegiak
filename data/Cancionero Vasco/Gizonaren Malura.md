---
id: ab-1971
izenburua: Gizonaren Malura
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001971.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001971.MID
youtube: null
---

Gizonaren malura nondik-nahi heldu da,
Bere penen ahanzteko joaiten ostatura,
Zenbait lagun hartu eta, arno ona den lekura,
Arno ona den lekura.