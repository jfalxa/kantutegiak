---
id: ab-2353
izenburua: Mutill Abilla Dago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002353.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002353.MID
youtube: null
---

Mutill abilla dago Tagati Alziko,
Aziendikan zoluan ez duela utziko;
Ezin atera badu ere, ez duela etsiko;
Gizon azkarra da ta gero ageriko.