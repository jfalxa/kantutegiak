---
id: ab-2188
izenburua: Bazterretik Bazterrerat (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002188.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002188.MID
youtube: null
---

Bazterretik bazterrerat,
Oi, munduaren zabala!
Eztakienak erran lirozu
Ni alegera nizala:
Hortzetan dizut irria,
Bi begietan nigarra.