---
id: ab-2679
izenburua: Peru Duako'K
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002679.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002679.MID
youtube: null
---

Peru Duako'k uste eban ze
Ez zala diru biarrik,
Makallau ona eruateko
Bilboko Barrenkaletik.

O gixon tonto, zer esaten dok
Asto salbaje andia?
Euk bere eleukek duan emongo
Eure kortako idia.

Makallau orrek illik dagoz geroz da
Neure idia bizirik;
Usteldu eta lixuntzen dagoz
Emon leitekez duarik.