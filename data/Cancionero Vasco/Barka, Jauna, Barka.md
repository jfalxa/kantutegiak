---
id: ab-3364
izenburua: Barka, Jauna, Barka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003364.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003364.MID
youtube: null
---

Barka, Jauna, barka,
Barka Jainko maitea.
Arren goza bedi
Zure asarrea:
Bekatu egin dut,
Begian dut negarra;
Barka Jainko ona,
Ene txarkeria.

Gaizkia egin dut
Zu zerorren aurrean,
Lotsa galdu dizut
Zure aitziñean;
Orain aitortzen dut
Nere gaiztakeria;
Barka, Jainko ona,
Ene txarkeria.

Ainbeste naigabe
Eman dizut nik zuri!
Galdua ibilli naiz
Zugandik urruti;
Orain zuregana
Eldu naiz damutua;
Barka Jainko ona,
Ene txarkeria.

Jauna, ikusazu
Nere biotz-damua;
Damu det biotzez
Zu naigabetua;
Damu det, izazu
Ai! nizaz errukia;
Barka, Jainko ona,
Ene txarkeria.