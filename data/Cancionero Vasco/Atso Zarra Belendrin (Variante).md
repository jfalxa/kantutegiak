---
id: ab-1692
izenburua: Atso Zarra Belendrin (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001692.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001692.MID
youtube: null
---

Atso zarra belendrin,
Ire festak egin din;
Ortzak ere juan zaizkin,
Diabru zar bat dirudin.
Atso zarrara kortesia (zarraren)
Putza dario, zia, zia.
Artzen badiñat illekia,
Erreko diñat ipurdia,
Xiru biru brinkulin. (tres veces)
Atsua, su ondoan agoenian,
Etzunen moxkor gaiztua. (ez daukan.