---
id: ab-1903
izenburua: Erregiñe Ta Saratsa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001903.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001903.MID
youtube: null
---

Erregiñe ta saratsa,
Ayeri eder garbosa. (andere)(bis)
Nafarruako errege jaunak
Egin omen du promesa. (bis)

Iru seme dituelaik
Iruei bana arrosa. (bis)
Etarik auta zuretako da,
Neskatxa eder garbosa. (bis.