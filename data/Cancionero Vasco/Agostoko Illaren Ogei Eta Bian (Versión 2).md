---
id: ab-1824
izenburua: Agostoko Illaren Ogei Eta Bian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001824.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001824.MID
youtube: null
---

Agostoko illaren ogei eta bian,
Pilota parti aundi Irungo errian,
Banderak edaturik, oi, moda berrian;
Ala jokatu dira modutxo aundian.