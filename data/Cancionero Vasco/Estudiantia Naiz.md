---
id: ab-1924
izenburua: Estudiantia Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001924.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001924.MID
youtube: null
---

Estudiantia naiz, orainik gaztia,
Emezortzi urte kunplitu gabia.
Jangoikuak etzuan nai nik meza ematea. )
Borreruen eskuetan nere errematea. )bi.