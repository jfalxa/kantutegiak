---
id: ab-1722
izenburua: Baionako Patroina
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001722.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001722.MID
youtube: null
---

Baionako patroina deitzen da San Leon;
Saindu haundiagorik ez dut senti nehon.
Sainduen ohoratzen behar gira egon;
Sainduak on dire, bainan biba Napoleon.