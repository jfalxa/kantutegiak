---
id: ab-2235
izenburua: Kontuz Ibil Laitezke
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002235.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002235.MID
youtube: null
---

Kontuz ibil laitezke gaztiak karrikan,
Egin gabetanikan azio txarrikan.
Dama orren pausuak ez dira alperrikan.
Iñork autsitzen badu albaka (a)darrikan,
Pagatu bear dute multa ederrikan.