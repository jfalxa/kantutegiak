---
id: ab-2213
izenburua: Kantu Berrien Jartzera Nua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002213.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002213.MID
youtube: null
---

Kantu berrien jartzera nua konbeni diren moduan,
Gure kontuak nola diraden hartu dezaten goguan.
Ezkondu eta zer bizi modu pasatzen dena munduan.
Horra ederki eskarmentatu denbora juan ta onduan.