---
id: ab-2220
izenburua: Kattalingorri Gorri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002220.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002220.MID
youtube: null
---

Kattalingorri, gorri, gorri.
Iduzki xuri, xuri, xuri.
Fal, fal, fal, fal, fal.