---
id: ab-1794
izenburua: Biotzean Min Dut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001794.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001794.MID
youtube: null
---

Biotzean min dut eta barnetikan kexu.
Izar xarmegarria, konsola nazazu.
Ene penen erdiak jasan biar tuzu;
Noizean bein bedere bisita nazazu.