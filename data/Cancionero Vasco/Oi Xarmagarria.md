---
id: ab-2741
izenburua: Oi Xarmagarria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002741.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002741.MID
youtube: null
---

- Oi xarmagarria, othoi, entzün nezazü.
Leihula jalkhi eta hunat so egidazü.
Hürrünti huna nüzü;
Gaia ilhün düzü,
Ebia ari düzü.
Krüdela ezpazira, pietate har zazü:
Bortha idok edazü.

- Oi amets, ametsa! Bahinan hire mentsa!
Destenore huntan iratzarazten naika?
Eztiat, ez, antsia
Orai jeikitzia,
Ez bortharen zabaltzia:
Arrixkatzen nikek infreditzia,
Eta eztik balia.

- Gizon bat niz eta ama baten semia.
Phena doloretan igaiten dit bizia.
Eta zü, maitia,
Ene türmentia,
Bihotz atzeidia!
Amodioaren gaixki plazatzia,
Ezta phena handia?

- Helas, zer dük arren hola aflijitzeko?
Zertako hartü dük hainbeste amodio?
Eztük enetako
Sorthürik orano
Gizona tronpatzeko.
Enük, ez, arrenküa hainbeste flatorio,
Ez merke, ez kario.

- Eniz trunperiaz mündü huntan bizitzen;
Fede hun bateki niz zure-gana jiten.
Ürats dolorusa
Ülhünpez egiten,
Zük benaizü xarmatzen!
Zure ikhustiak ene bista oro
Dizü inganatzen.

- Afekzionetan hola bahiz bizitzen,
Zeren ehiz ene aita-amer agertzen?
Bahaie huntsa hartzen,
Enük apartatzen,
Obedient nük izanen,
Haien konsellier ni nük behatüren,
Hurak obeditüren.

- Maitia, arragret dit nik zure ait'et'amez.
Enüzü agertzen, zeren beniz ahalke.
Posible balitz zure
Desiña, banüke
Erraiteko bi elhe:
Eia diodanez erreüsi deuse,
Haier deklaratü gabe?

- Hire ahalkiak badik bere malezia.
Uste düka nizala heregün sorthia?
Gaxki enpleatü dük
Hik hire mihia
Eta izpiritia.
Ni nahi naianak, elizatik landa
Ükenen dik eskia.