---
id: ab-2415
izenburua: Orain Esan Dezagun Errosarioa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002415.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002415.MID
youtube: null
---

Orain esan dezagun errosarioa
Purgatorioko animen sufrajioan;
Aiek gugaitik, guk aiengatik,
Zeruan sar ditzagun purgatoriotik.