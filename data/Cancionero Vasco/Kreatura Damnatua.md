---
id: ab-2236
izenburua: Kreatura Damnatua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002236.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002236.MID
youtube: null
---

Kreatura damnatua, zerk hauen tormentatzen,
Zer den hire infernua, hire penak zer diren.
Erraguk, erraguk; argitu nahi gaituk.