---
id: ab-2464
izenburua: San Migelen Sartuta (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002464.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002464.MID
youtube: null
---

San Migelen sartuta
Lenengo pregunte:
Zerbait alimentue
Eman nai badute.
Alimentu artuta
Atera kanpora;
Pensamentu txarrakin
Barruna atzera,
Beste asko gauzarekin
San Migel ostera.