---
id: ab-2999
izenburua: Artajorrara Noa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002999.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002999.MID
youtube: null
---

Artajorrara noa
Luberri berrira:
Belarra jorratuta,
Artua sasira.