---
id: ab-1944
izenburua: Ezpeldoian Bortz Emazte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001944.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001944.MID
youtube: null
---

Ezpeldoian bortz emazte,
Bida zahar, hiru gazte;
Aza landatzen pasatu dute
Sei hilabete ta bi aste.
Etxettipiko aker beltzak
Oren batez jan dezte.

Atso bat eta atso bi,
Biak elgar iduri:
Batek baitu ehun urte,
Bertziak ehun eta bi;
Batek serora joan nahi eta
Bertzeak aldiz dendari.