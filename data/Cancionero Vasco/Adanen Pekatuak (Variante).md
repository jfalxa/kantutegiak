---
id: ab-1490
izenburua: Adanen Pekatuak (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001490.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001490.MID
youtube: null
---

Adanen pekatuak
Genduzken galdurik,
Infernuko ondarrera
Betiko sarturik.
Libratu geranean
Peligro orregatik,
Igesi egin dezagun
Pekatuetatik.