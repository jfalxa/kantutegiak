---
id: ab-2675
izenburua: Oi Gurutzea
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002675.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002675.MID
youtube: null
---

Oi Gurutzea,oi Gurutzea, (Pueblo)
Gurutze saindu maitea!
Oi Gurutzea, oi Gurutzea,
Gurutze saindu maitea!

Zure besotan, oi Gurutzea, (Schola)
Hil zaiku Jesus Maitea.
Haren gurutzefikatzailea
Ni naiz, ni, bekatorea! (bis.