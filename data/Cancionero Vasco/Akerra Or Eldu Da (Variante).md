---
id: ab-1558
izenburua: Akerra Or Eldu Da (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001558.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001558.MID
youtube: null
---

Akerra or eldu da
Artuaren yatera.
Akerrak artua;
Akerra ken, ken, ken,
Akerra gure artua zen.