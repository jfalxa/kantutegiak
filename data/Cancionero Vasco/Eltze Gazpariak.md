---
id: ab-1878
izenburua: Eltze Gazpariak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001878.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001878.MID
youtube: null
---

Eltze gazpariak
Eztu gustorik.
Oaingo mutillakin
Ez dago gizonik.

Oaingo mutillakin
Gizona egiteko,
Andriak biarko du
Zer egin franko.