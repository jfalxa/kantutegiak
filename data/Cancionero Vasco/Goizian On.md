---
id: ab-1995
izenburua: Goizian On
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001995.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001995.MID
youtube: null
---

Gozian on, arratsian on,
Matsaren zumoa beti duk on,
Eta beti duk on.
Etxera abia ta joan ezin,
Erori eta bertan etzin,
Eta bertan etzin.
Berriz abia ta joan ezin,
Erori eta bertan etzin,
Eta bertan etzin.