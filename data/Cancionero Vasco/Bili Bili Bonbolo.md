---
id: ab-1790
izenburua: Bili Bili Bonbolo
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001790.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001790.MID
youtube: null
---

Bili bili bonbolo, sendal lo.
Akerra Prantzian balego,
Astuak soindu, idiak dantzan,
Auntzak danboliña yo.

Danbore berri berria,
Donostiatik ekarria:
Bazterrak ditu perla xuriak,
Erdian urre gorria.
Aren zintatxo oberena da
Abarka soka lodia.