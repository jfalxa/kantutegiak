---
id: ab-2391
izenburua: Bañuak Ur Epelez (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002391.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002391.MID
youtube: null
---

Bañuak ur epelez, saldako ollaki,
Gorputzari plazerak nola egin badaki;
Antzar-ixterrak ere yan bearko ditik,
Dirurik ekartzeko Donapaleutik.