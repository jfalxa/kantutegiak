---
id: ab-3172
izenburua: Txikiro Beltxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003172.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003172.MID
youtube: null
---

Txikiro beltxa ona da, baña
Obia buxtan txuria.
Dantzan ikasi nai duenik bada,
Nere zanguari begira.