---
id: ab-2981
izenburua: Baratzeko Pikuak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002981.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002981.MID
youtube: null
---

Baratzeko pikuak iru txurten ditu:
Dantzan dabillen orrek ankak arin ditu.
Ankak ariñak eta buru ariñago,
Dantzan obeto daki artajorran baño.

Artojorrara nua lur berri-berrire;
Belarra jorratuta, artua sasire.
Orrako mutill orren dantzarako planta:
Zeiñ airoso dabillen ezkerreko anka.

Badakit baituzula bat eta bi ere;
Esperantza galdurik ez nago ni ere.
Ai, nere txiki pollit, txiki maite zaitut:
Atoz, zaldin gañera eramako zaitut.

Erdi bat izan ez, ta lau kuarto gastatu:
Mutill andrezaliak nekez aberastu.
Despeditzera nua asi duten lana;
Berdin ezta faltako emen zer esana.