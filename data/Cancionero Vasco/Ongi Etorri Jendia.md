---
id: ab-2410
izenburua: Ongi Etorri Jendia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002410.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002410.MID
youtube: null
---

Ongi etorri, jendia,
Gizonak eta andriak.
Eliz-atarian bi arri,
Nobio jauna, jarri bedi.