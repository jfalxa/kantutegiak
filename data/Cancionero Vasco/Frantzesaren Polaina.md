---
id: ab-2087
izenburua: Frantzesaren Polaina
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002087.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002087.MID
youtube: null
---

Frantzesaren polaina arrobatian,
Lepuan korbata ta kasaka urdiña,
Alkandora loratua, gerrikua sedazkua,
Erlepiaikin txapela gutxi pagatua.