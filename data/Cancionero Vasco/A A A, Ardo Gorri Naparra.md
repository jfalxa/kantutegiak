---
id: ab-1481
izenburua: A A A, Ardo Gorri Naparra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001481.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001481.MID
youtube: null
---

A,a,a:
Ardo gorri naparra,
Elikatura ona da,
Edan al balin bada.
A,a,a:
Ardo gorri naparra.

E,e,e:
Ni ardoaren alde,
Edan ardo ura gabe,
Baldin bada de balde
E,e,e,
Ni ardoaren alde.

I,i,i:
Arduak gaitu bizi,
Noizik bein ongi busti,
Edan gabe ez utzi.
I,i,i:
Arduak gaitu bizi.

O,o,o:
Au biontzako dago
Edan dezagun oro,
Zuk orai, ta nik gero.
O,o,o:
Au biontzako dago.

U,u,u:
Arduak gaitu galdu.
Geyegi edan degu
Leurria irago degu.
U,u,u:
Arduak gaitu galdu.