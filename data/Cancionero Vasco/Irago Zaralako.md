---
id: ab-2121
izenburua: Irago Zaralako
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002121.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002121.MID
youtube: null
---

Irago zaralako
Irago onetan,
Bertsuak para ditugu
Deklara onetan.
Asi naizen ezkero,
Bear ditut esan
Nola paratzen diran
Mandamentu onetan.