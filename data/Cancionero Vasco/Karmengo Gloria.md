---
id: ab-2216
izenburua: Karmengo Gloria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002216.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002216.MID
youtube: null
---

Karmengo gloria ai aloratua,
Zeruko argia, Ama Birjiña singularia.
Ama, O piadosa! Gizonik ezagutu bagia,
Eman iuzu karmilitari Birjiña agiua.
Izarra itsesubakua, Birjiña Maria,
Konzebitu zenduben zeudorren semia
Guzti oen gabia.
Eska iozu seme jaunari, Birjiña Maria,
Guretzat miserikordia, Reina soberana,
Gure esperantza ta bidioa.
Zabal iozu pekatariari, Birjiña Maria,
Zerubetako atia.
Eutzi pekatua, artu zerua,
Gero goza dezagun paradisuba.
Orra zerua, zeruko argiya,
Birjiña Karmengoaren borondate aundiya:
Berak emanen digu finian gloria.