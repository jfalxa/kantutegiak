---
id: ab-2225
izenburua: Kolore Ta Gorri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002225.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002225.MID
youtube: null
---

Kolore ta gorri eta
Begiz amorosa;
Bistaz iduri duzu
Klabelin arrosa.
Ojala bear bazinu
Zuk eni esposa.