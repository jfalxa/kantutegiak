---
id: ab-2056
izenburua: Angerutxo Zerukua (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002056.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002056.MID
youtube: null
---

Angerutxo zerukua,
Izarraren parekua,
Nik zu zaitut maitiago
Txoriak bere umiak baño.