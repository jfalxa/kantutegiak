---
id: ab-2358
izenburua: Naparruan Da Erri Txikia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002358.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002358.MID
youtube: null
---

Naparruan da erri txikia
Deitzen diote Altzazu

Dingulun, dangulun, eragiozu,
Aurrak egin dezan lo ona.