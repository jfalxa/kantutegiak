---
id: ab-1685
izenburua: Astua Heltu Bortutik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001685.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001685.MID
youtube: null
---

Astua heltü bortütik
Ardu xuriz kargatürik.
Bidian errekontratzen,
Tralerairai, tralerairai,
Bidian errekontratzen
Otso beltz handi bat etzanik.

- Agur hiri, Otsotto,
- Hiri ere, Astotto.
- Hire bü eta beharriak
Tralerairai, tralerairai,
Hire bü eta beharriak
Gaurko ene aiharia.

- Hortxe gañian arth'olha,
Artzainhorik gabe da;
Hanko ahari büztan xabalek
Tralerairai, tralerairai,
Hanko ahari büztan xabalek
Nik beno hoberik badie.

- Hortxe gañian ermita
San Laurenten meza da.
Meza behar diat entzün
Tralerairai, tralerairai,
Meza behar diat entzün
Gero janen banaik ere.

Astua elizan sarthü
Aztalaz bortha zerratü;
Eta otso gizagaizua
Tralerairai, tralerarai,
Eta otso gizagaizua
Kampuan bera baratü.

- Aiei, aiei, meza lüzia,
Ala da Erramükua,
- Eztük, ez, Erramükua,
Tralerairai, tralerarai,
Eztük, ez, Erramükua,
Bena hire tronpatzekua.