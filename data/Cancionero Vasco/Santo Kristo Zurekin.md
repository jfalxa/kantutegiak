---
id: ab-2470
izenburua: Santo Kristo Zurekin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002470.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002470.MID
youtube: null
---

Santo Kristo, zurekin konfesi nadian
Zer lanak egin tudan nik Katalonian.
Leridan pasatua urrengo errian,
Bost lagun bizi ziren eren familian:
Guziek il nituen gabaren erdian.