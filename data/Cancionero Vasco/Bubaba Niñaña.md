---
id: ab-2796
izenburua: Bubaba Niñaña
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002796.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002796.MID
youtube: null
---

Bubaba, niñaña.
Buba, buba bubaba
Buba, buba, niñaña.
Bubaba, niñaña.
Buba, buba, niñaña.
Niñaña, bubaba.
Buba, buba, niñaña.