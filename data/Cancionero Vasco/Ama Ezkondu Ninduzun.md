---
id: ab-1590
izenburua: Ama Ezkondu Ninduzun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001590.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001590.MID
youtube: null
---

Ama, ezkondu ninduzun hamabost urtetan;
Senarra eman ninduzun larogei urtetan, larogei urtetan.
Senarra daukat ohian eri, uliak ostiko manik min harturik.
Hiltzen bada ere, ez dut eginen nigarrik.
Hil bedi, gal bedi, ez dut eginen nigarrik;
Hil bedi, gal bedi, etzuen balio lau txori.