---
id: ab-2103
izenburua: Eguberri, Eguberri (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002103.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002103.MID
youtube: null
---

Eguberri, Eguberri
Gaudela Eguberria!
Guzien Jauna sortu beizen
Dugun alegeria!

Eguberri gabean,
Gabaren gaberdikoan
Mutiko eder bat beizen
Beleneko irian.

Aingiru ederrak santsi dira (jetxi)
Zerutikan lurrera;
Bereala joan dira
Judio sortu berrira. (herrira)

Beleneko iria!
Zure doaien andia!
Zerentzun beizen, sortu beizen
Jaungoiko onaren Semia.

Edideren dugu,
Edideren guzian Jauna
Belenen, g(o)azen guziok Belenera
Jaunaren adoratzera.

Amari deizu Maria,
Beti birjunik erdia,
Eta Jesus Seme gori (hori)
Zeruan izen emana.

Gaberdikoan argitzen du
Eguerdikoan bezala;
(B)ai! zer ordutan ateratzen du
Eduzki justizikoa! (Eguzki)

Iduzkitik datozki
Ikusirikan birtute,
Ama-Birjinaren gori (hori)
Beren burus datxite.

Izar eder bat ateratzen du
Araguriren artetik,
Artxek xuxen giatzen du
Iru Erregiak Belenen.

Galde-egines, galde-egines
Non da sortu berria?
Guziak xunto santsi dira (jetxi)
Jerusalemen barrena.

Irur Erregek eman digote
Jesusi presente andi bat:
Batak urria, bertzeak mirra,
Belenak intzensasuna. (Herenak)

Artzei onak an berris
Berri on batengatik.
Gure Jesus sortu beizen
Gizon umilde eginik.

Nola baita Jaun aundia
Liberal pagatzalia,
Emanen dagula pagua,
Graziarekin gloria.

Jende onak gazen guziok (goazen)
Guziok ofrendatzera;
Ofrend'onik ermanen dugu
Portale Belenekora.

Kantatu dugu ederki,
Bai zuek ere gureki;
Bai zuek gureki eta
Amabi aingirureki.

Amabi ainguruek zueki eta
Baita ere gureki,
Bai zuek ere gureki eta
Jaungoikoa dela guzieki.