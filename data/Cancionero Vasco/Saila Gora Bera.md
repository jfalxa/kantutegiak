---
id: ab-2449
izenburua: Saila Gora Bera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002449.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002449.MID
youtube: null
---

Saila gora bera,
Au egin ta bertzera;
Au egin gabetanikan
Eder ederrak ez gara.
Orra Antoni Alzuberekua,
Juan zaigu itxera.