---
id: ab-2774
izenburua: Ürzo Xuri Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002774.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002774.MID
youtube: null
---

Ürzo xuri bat jin izan zaikü
Arhantzüseko bortün gañan.
Erran geneion khortereski
Gaur bara ladin gureki.

- Ürzo xuria, ürzo xuria,
Erran izadant, othoi, egia:
Nuat hunduen bidajez,
Sanjatü gabe pasajez?

- Ene herritik partitü nintzan
Españaleko deseiñian:
Heltü niz Arhantzüsera
Ene plazeren galtzera.

Artzain gaztia bortü gorenetan
Oihüz ari da doloretan:
Antxü bat diala erosi,
Bera dereiola ebatsi.

- Ene antxü, antxü karioa,
Bihotzez ungi maitatia:
Nik etzütüt, ez, Kitatüren;
Lehen dit bizia galdüren.