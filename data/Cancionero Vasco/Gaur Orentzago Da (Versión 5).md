---
id: ab-2095
izenburua: Gaur Orentzago Da (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002095.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002095.MID
youtube: null
---

Gaur Orentzago da eta
Biar Egugarri.
Etxeko-andre maitia,
Txanpon bat edo bi.
Atozte ba aguro
Goitietatik bera,
Jesus nola jaio dan
Zuri esatera.