---
id: ab-2072
izenburua: Attonak Kantatzen Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002072.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002072.MID
youtube: null
---

Attonak kantatzen du beti mendiz mendi,
Eun ardi aurrian ta ponpa biak aundi.
Etxolan ezin goza igual kamaña.
Itxera etorri ta nagusiak kargu:
- Artzaia, menditikan zer berri dakargu?
- Ardi bat zait galdu,
Atzo egunaz otsuak jan al du.