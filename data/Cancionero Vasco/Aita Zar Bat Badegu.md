---
id: ab-1540
izenburua: Aita Zar Bat Badegu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001540.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001540.MID
youtube: null
---

Aita zar bat badegu familiakua;
Zazpi ume utzi ta juanikekua.
Aren andria dago sabelez flakua;
Etxian koiperik ez, dendan oliua.
Antxen, antxen dago purgatoriu.