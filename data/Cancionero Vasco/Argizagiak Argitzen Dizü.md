---
id: ab-1641
izenburua: Argizagiak Argitzen Dizü
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001641.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001641.MID
youtube: null
---

Argizagiak argitzen dizü
Gaiaz zoharbi denian.
Nik ikhusi dit ene maitia
Dantzan plazaren erdian;
Izar eder bat üdüri zian
Beste ororen artian.