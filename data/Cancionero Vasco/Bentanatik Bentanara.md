---
id: ab-1751
izenburua: Bentanatik Bentanara
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001751.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001751.MID
youtube: null
---

Bentanatik bentanara
Airia presko oi, airia presko.

Orra Joakin Alzumeko
Bentanan dago, oi, bentanan dago.

Sar daiela barrenera;
An zertan dago, oi, an zertan dago?

Guadalupe Indakoai
Beira omen dago, oi, beira omen dago.