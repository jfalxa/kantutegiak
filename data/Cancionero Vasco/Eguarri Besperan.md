---
id: ab-1862
izenburua: Eguarri Besperan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001862.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001862.MID
youtube: null
---

Eguarri besperan, negu biotzian,
Jayo da gure Jesus leku arrotzian;
Ez palazio aundi, ez aberatsian,
Baizik estalpe pobre arlote otzian.

Maria eta Jose, guztiz errukarri,
Otzak ikara zeuden begira alkarri.
Onetan amabiak ziraden etorri,
Argitara danian seme eder ori.

Eguzkia pasata osorik kristala,
Orrela gelditzen da Amaren sabela.
Espiritu Santuak egiñik itzala,
Orra or Maria da Ama ta Donzella.

Ditxa ta zoriona datoz gure gana:
Agertu da mundura Jaungoiko-gizona,
Trinidade Santuko bigarren persona,
Mesias egiazko gure Jesus ona.