---
id: ab-1483
izenburua: Abenduko Illaren Ogei Ta Lauean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001483.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001483.MID
youtube: null
---

Abenduko illaren ogeita lauean,
Jesus Salbadorea jaio dagonean;
Gu ipintzea gatik estadu obean,
Erremedio du ta zerura bidean.