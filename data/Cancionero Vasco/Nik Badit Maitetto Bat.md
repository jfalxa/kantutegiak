---
id: ab-2669
izenburua: Nik Badit Maitetto Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002669.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002669.MID
youtube: null
---

Nik badit maitetto bat, orotan bereber:
Ezpeitiet, ez, erran oraino etxekuer.
Ezin diet so egiten beste neskatiler:
Soz amestürik niago haren begi ñabarrez.