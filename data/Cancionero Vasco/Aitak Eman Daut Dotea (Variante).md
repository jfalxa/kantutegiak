---
id: ab-1542
izenburua: Aitak Eman Daut Dotea (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001542.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001542.MID
youtube: null
---

Aitak eman daut dotea,
Neurea, neurea:
Urdeño bat, bere humekin;
Oilo ama, bere txitoekin.
Adios ene dotea. (bis.