---
id: ab-2144
izenburua: Jesukristoren Kendu Ezkero (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002144.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002144.MID
youtube: null
---

Jesukristoren kendu ezkero
Bekatuakin bizitza,
Baldin ezpadet negar agiten,
Arrizkoa det biotza.
Guziok lagun
Kanta dezagun
Bere penako eriotza.