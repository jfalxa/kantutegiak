---
id: ab-2187
izenburua: Bazterretik Bazterrera (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002187.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002187.MID
youtube: null
---

Bazterretik bazterrera,
Oi, munduaren zabala!
Ortzetan dizut irria, eta
Bi begietan nigarra.
Ez dakienak yakin dezaten
Ni alagera naizela:
Ortzetan dizut irria eta
Bi begietan nigarra.