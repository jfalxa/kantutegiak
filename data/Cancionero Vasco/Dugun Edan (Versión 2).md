---
id: ab-1815
izenburua: Dugun Edan (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001815.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001815.MID
youtube: null
---

Dugun edan, dugun edan, dugunaz geroz botoila.
Botoilan ez dugu arnorik: berriz bete behar dik,
Ez txuritik, bai gorritik, txaia den hoberenetik.
Guazen hemendik, berriz edanik eta alegerarik.