---
id: ab-1566
izenburua: Albaiteruben Sala Demonio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001566.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001566.MID
youtube: null
---

Albaiteruben sala
Demonio artan,
Demoniozko estropezo bat
Egin nuen bertan.
Demoniuak emen,
Demoniuak an;
Demoniok zabiltzen
Gorputzaren bueltan;
Demoniuak sasoi zauzkaten
Demoniuetan.