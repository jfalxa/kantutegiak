---
id: ab-1876
izenburua: Elizara Juan Eta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001876.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001876.MID
youtube: null
---

Elizara juan eta,
Belauniko jarri;
Lenengo oraziua,
Nere maitiari.
Elizan ta kanpuan
Zu zera beti neuria,
Aingeru zerutikan jaitsia.