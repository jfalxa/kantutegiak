---
id: ab-2183
izenburua: Kadira Kadira Lumatxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002183.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002183.MID
youtube: null
---

Kadira, kadira lumatxa,
Xanxoineko neskatxa.