---
id: ab-1820
izenburua: Burniezko Pertza Ttipi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001820.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001820.MID
youtube: null
---

Burniezko pertza ttipi txanpatu batean,
Baba egosten dago bethi lahatzean.
Aztaparra kutxare, salda edatean,
Nolanahi ondo dela dio zahartzean.

Hogoi eta lau urthe ezkondu nintzala;
Ojala banindago lenago bezala.
Senarra dizut tzarra, bertzaldetik zarra:
Ez dago neretako bizi modu txarra.