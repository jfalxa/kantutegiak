---
id: ab-3052
izenburua: Lazaroren Astoa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003052.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003052.MID
youtube: null
---

Lazaroren astoa
Dantzan dabil gaixoa;
Bostetan zaldale,
Seietan edale.
Arre, dili dale.
La ra la...