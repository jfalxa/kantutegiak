---
id: ab-2205
izenburua: Aita Aitzinerako (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002205.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002205.MID
youtube: null
---

Aita aitzinerako, semia ondoko,
Osaba burjes ori dirua fornitzeko,
Enetzat ez dirade bi bortz liberako,
Galeretan bedere oi, ler egiteko.