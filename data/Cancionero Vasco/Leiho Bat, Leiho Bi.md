---
id: ab-3009
izenburua: Leiho Bat, Leiho Bi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003009.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003009.MID
youtube: null
---

Leiho bat, leiho bi,
Erregarde muxiu xirunfle
Kokela batian bi babe,
Muxiu xirunflen arreba
Oxan xirole errozkile
Kanari ete
Oxan xirole
Kanabitu nau.