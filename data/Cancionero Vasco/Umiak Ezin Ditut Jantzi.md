---
id: ab-3262
izenburua: Umiak Ezin Ditut Jantzi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003262.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003262.MID
youtube: null
---

Umiak ezin ditut
Jantzi eta mantenditu;
Errentarendako
Zerria ere kendu.
Trai larai...