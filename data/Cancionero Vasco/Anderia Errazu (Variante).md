---
id: ab-1678
izenburua: Anderia Errazu (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001678.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001678.MID
youtube: null
---

- Anderia, errazu,
Bortxaz ala amodioz
Jina zarenez zu.
- Bai huna jina nuzu
Ongi bortxaturik;
Hiru kapitainen erdian
Xenaz amarraturik.