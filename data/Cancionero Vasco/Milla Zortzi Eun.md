---
id: ab-2336
izenburua: Milla Zortzi Eun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002336.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002336.MID
youtube: null
---

Milla zortzi eun da berrogei ta biyan,
Penak explikatzera orain naiz abillan.
Bi milla ta geyago legua birian,
Gu saldu daituztenak onela tranpian,
Ez daude sarturikan zeruko lorian.