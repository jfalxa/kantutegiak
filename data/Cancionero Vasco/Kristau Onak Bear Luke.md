---
id: ab-2240
izenburua: Kristau Onak Bear Luke
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002240.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002240.MID
youtube: null
---

Kristau onak bear luke
Iande egunean pensatu,
Aste guzian zenbat aldiz
Eiten duen bekatu.

Pekatuak konfesatu eta
Barkamendua eskatu;
Barkamendua eskatu eta
Urrikimendu guardatu.

Kristau on bat iltzen denean
Egiten da parte bi:
Gorputz ura enterratu eta
Gero lurrez estali.

Arima gaixoa parabisuko
Atarira bakarrik
(Ixilik) eta tritarik
Nora doayen eztaki.

Gure Jauna ateretzen zayo
Piedadez beterik:
- Arima gaixoa, zer dakardazu
Zeurekilan mundutik?

- Umildadea, karidadea,
Nik ez dakart besterik.
- Aingerutxoak, ar dezazie
Arima gaixoa eskutik;
Parabisuan para zazie
Beste guzien aurretik.