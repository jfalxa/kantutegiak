---
id: ab-1628
izenburua: Ara Bertso Berriak (Versión 10)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001628.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001628.MID
youtube: null
---

Ara bertso berriak
Nai ditut paratu,
Amar mandementuak
Nola gobernatu.
Lendabizikoa da:
Jaingoiko adoratu;
Geren projimo launak (lagunak)
Beti estimatu.

Orra bigarren orretan
Juramentu guti
Nai duenik erraitera
mingaña ez utzi.
Ezta bada orretan
Diferentzia guti
Zerurat igan edo
Infernura sautsi. (jautsi)

Meza santua entzun
Irrugarrenian
Obligazione degu
Igande egunean.
Obra onak egiñez
Al daikenean
Gloriaz gozatzeko
Eternidadean.

Laugarren orretan
Gure gurasoak
Aleginan sustentatu
Estutu gaixoak
Arrikaltzekoak dire
Ayen trabajuak
Alarikan eztire
Ongi pagatuak.

Bosgarren orretan
Nior ez iltzea
Ai! ze gauza gogor den
Oi! eriotzea.
Aski dugu (Jesus) Jaunari
Erreparatzea
Guregatik arturik
Dauka gurutzea.

Garbitasuna dugu
Seigarren orretan
Itzez, obraz, pensamentuz
Solas gizonetan.


Asko tentazione
(Pasten da mundu ontan). (Pasatzen)

Zazpigarren orretan
Yeuserez ebatsi
Nor berearekin pasa
Bertzearena utzi.
Debruak emaiten du
Tentazio egiteko gaizki
Gero atzemateko
Sareak edaturik dauzki.