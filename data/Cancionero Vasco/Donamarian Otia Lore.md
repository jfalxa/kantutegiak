---
id: ab-1843
izenburua: Donamarian Otia Lore
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001843.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001843.MID
youtube: null
---

Donamarian otia lore;
Gaztelu'n erromerua.
Neskatxa gazte ta airosua da
Maria konpañekua.

Oritxe galai nor emanen diogu
Bera den bezalangua?
Bera ona da, ta obea luke
Frantzisko Etxenikenua.