---
id: ab-1875
izenburua: Egun Oroz, Goizetan Arratsetan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001875.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001875.MID
youtube: null
---

Egun oroz, goiz eta arratsetan,
Nago beti pena doloretan.
Ez dirot ez jan, ez eta ere edan.
Ez dakit zer dudan.
Gaitz handi bat da nibaitan;
Amarratu nau betbetan.
Ni ez naiteke bizi gisa huntan.