---
id: ab-1748
izenburua: Bekatore Handi Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001748.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001748.MID
youtube: null
---

Bekatore handi bat, Jainko guziz ona,
Heldu da zureganat: ah!, ez abandona.
Zaurtu dio bihotza samurrik minenak,
Bai eta kendu hitza barneko suspirak.