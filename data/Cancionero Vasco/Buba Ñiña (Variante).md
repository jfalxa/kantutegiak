---
id: ab-2840
izenburua: Buba Ñiña (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002840.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002840.MID
youtube: null
---

Buba, ñiña,
Santa Katalina;
Buba, ñiña,
Lo dakizula
Logale miña.