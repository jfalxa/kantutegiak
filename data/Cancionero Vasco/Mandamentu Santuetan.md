---
id: ab-2290
izenburua: Mandamentu Santuetan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002290.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002290.MID
youtube: null
---

Mandamentu santuetan lendabizikoa:
Biotzetikan amatu gure Jaungoikoa,
Idolorikan gabe, au da kariñua;
Guazen adoratzera Jesus dibinoa,
Jaunak guretzat dauka gloria eternoa.