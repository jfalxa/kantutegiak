---
id: ab-2102
izenburua: Bost Llaga Sagraduen (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002102.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002102.MID
youtube: null
---

Bost llaga sagraduen adorazioa
Egunian bi aldiz ejerzizioa.