---
id: ab-3184
izenburua: Erdi Erdian Eta Bi Bazterretan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003184.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003184.MID
youtube: null
---

Erdi erdian eta bi bazterretan,
Bi bazterretan,
Neskatxak ederrak dira erri onetan,
Erri onetan.
Lara la la...