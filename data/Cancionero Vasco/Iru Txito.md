---
id: ab-3020
izenburua: Iru Txito
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003020.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003020.MID
youtube: null
---

Iru txito izan eta lau galdu:
Gure txitoaren ama zerk jan du?

Purra, purra, egin nion atetik,
Ta kukurruku egin zatan eltzetik.

Gure txitoaren ama oiloa,
Axeriak kendu dio lepoa.