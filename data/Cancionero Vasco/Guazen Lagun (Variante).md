---
id: ab-2140
izenburua: Guazen Lagun (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002140.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002140.MID
youtube: null
---

Guazen, lagun, guazen, lagun,
Biok Athizanera;
Urso xuri bat jalgiko baita
Athizanera plazara.
Hura nahi nuke bildu
Neure sarietara. (bis)

Athizane hortan bada
Ihiztari habilik
Ursoño hari jarraizki zaizko
Bat edo bia ondotik;
Bainan ezin khendu diote
Hegalpetik lumarik. (bis)

Belatxa bezein arina banintz,
Juatekotzat airean,
Ursoño hura harrapa niro
Airean edo lurrean;
Edo bertzenaz sar nindake
Haren usotegian. (bis)

Aspaldian erraiten zautan
Bertzeño batek egia:
Gauaz ongi zerratzen zela
Haren usotegia;
Imposible izanen zela
Haren gana sartzera. (bis.