---
id: ab-2156
izenburua: Gobernadore Palermekua (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002156.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002156.MID
youtube: null
---

Gobernadore Palermekuak
Aginpidia bazuan
Santa Ageda, ain zen ederra,
Bere andretzat nai zuan.
Gizon aundiya izanagatik,
Ura logratu etzuan.
Lendabiziko nobedadiak
Argatik izan zituan.