---
id: ab-3363
izenburua: Pozkarioz Kantazagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003363.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003363.MID
youtube: null
---

Pozkarioz kantazagun
Gaur Jesus'en jayotzea,
Al guziak gora zagun
Sortu dan Jainko Semea.
O Jesus, zure graziak
Aldatu gaitzan guziak.

Eresika aingeruak
Entzuten dira kantatzen,
Gu ere asi gaitezen
Pozturik Jauna goratzen.
O Jesus, zure graziak
Aldatu gaitzan guziak.

Errege ta artzayak dute
Jesus Aurtxoa jauretsi (adoratu);
Guk ere gogoz nai degu
Aur ori beti onetsi.
O Jesus, zure graziak
Aldatu gaitzan guziak.

Jesus, negu gogorrean
Oetzat dezu lastoa;
Nola bada atsegiñetan
Dukegu gure gogoa?
O Jesus, zure graziak
Aldatu gaitzan guziak.

Sortzetik apaltasuna
Diguzu, Jauna, irakasten;
Zergatik goratasuna
Degu bada guk billatzen.
O Jesus, zure graziak
Aldatu gaitzan guziak.