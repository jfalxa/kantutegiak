---
id: ab-2714
izenburua: Bi Bestia Laguntzat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002714.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002714.MID
youtube: null
---

Bi bestia laguntzat
Horra Jesusen gortia!
Lasto guti bat etzantzatzat:
Oi, hau gauza zein dorpea!
Atzar gaiten, atzar lotarik;
Gau huntan da Jesus sortzen.
Amodioak garaiturik,
Guregatik da etortzen.