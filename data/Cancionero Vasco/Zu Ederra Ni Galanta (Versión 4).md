---
id: ab-2642
izenburua: Zu Ederra Ni Galanta (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002642.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002642.MID
youtube: null
---

Zu ederra, ni galanta,
Nik neskatxetan gogua.
Emen den ederrena da
Mari Etxeberrikua.

Orren galai nor emain diogu
Bera den bezalakua?
Bera ona da, obia luke
Antonio Seronekua.