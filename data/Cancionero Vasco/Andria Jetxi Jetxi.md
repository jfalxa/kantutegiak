---
id: ab-1617
izenburua: Andria Jetxi Jetxi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001617.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001617.MID
youtube: null
---

Andria, jetxi, jetxi
Zaldi urdinetik:
Berdin nere biziya
Emen bearko din.