---
id: ab-2685
izenburua: Pinpinpin-Txoria (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002685.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002685.MID
youtube: null
---

Pinpinpin-txoria,
Txori kantalaria.
Txorittu'atek
Kantatzen du
Arritxu baten gañian.