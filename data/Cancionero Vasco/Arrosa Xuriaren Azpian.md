---
id: ab-1676
izenburua: Arrosa Xuriaren Azpian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001676.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001676.MID
youtube: null
---

Arrosa xuriaren azpian anderia lokartu,
Elhurra bezen xuri, ekhia bezen ederrik.
Hiru kapitainek deramate gorkiaz engeyaturik.

Hiru kapitainak joan ziren anderiaren-gana;
Zaldian ezarri zuten mantoz ongi trozaturik,
Pariserat ere eraman, aitak jakin gaberik.

Parisen ostalera batek ongi du salutatu;
Ongi du salutatu, berriak ere galdetu:
- Bortxaz ala amodioz jina ziren, anderia, errazu.

Anderiak errepusta, nola eman badaki:
- Ez, ez; ene bihotza ongi triste jina duzu:
Hiru kapitainek balkoni batetik harturik jina nuzu. (balkoin)

Kapitainak, hori entzunik, joan ziren anderia-gana:
- Anderia, afal zaite eztiki eta trankilik;
Hiru kapitain hauk derauzkatzu gaur zure zerbitzari.

Anderia, hori entzunik, hil hotza zen erori.
Kapitainak joan ziren beren tropak (?) harturik;
Ongi nigar egiten zutela anderiak dolu eginik. (anderiari)

Gero hil eta onduan, non ehortziren dugu?
Aitaren baratzian, ezpela baten azpian,
Lino liliz inguraturik, tonba baten barnian. (liho)

Hiru egunen buruan, hilak aitari ohiu:
- Aita, entzun nezazu: oranion hemen niagozu; (orainon)
Birjinitatea begiratu nahiz, hila egona nauzu. (nuzu.