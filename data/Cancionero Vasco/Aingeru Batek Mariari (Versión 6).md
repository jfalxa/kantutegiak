---
id: ab-2702
izenburua: Aingeru Batek Mariari (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002702.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002702.MID
youtube: null
---

Aingeru batek Mariari
Dio gratziaz bethea.
Jaungoikoaren Semeari
Emanen duzu sortzea.