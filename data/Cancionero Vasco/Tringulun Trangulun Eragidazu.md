---
id: ab-2539
izenburua: Tringulun Trangulun Eragidazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002539.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002539.MID
youtube: null
---

Tringulun, trangulun, eragidazu;
Laister aziko naitzazu.
Laister azi ta ongi lagundu:
Mesede izango degu.