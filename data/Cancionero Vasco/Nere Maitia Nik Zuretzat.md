---
id: ab-2377
izenburua: Nere Maitia Nik Zuretzat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002377.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002377.MID
youtube: null
---

Nere maitia, nik zuretzat
Opilla sorta daukat;
Erdi erdiya emango dizut,
Beste erdiya neretzat.