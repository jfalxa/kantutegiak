---
id: ab-2770
izenburua: Uriya Uriya
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002770.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002770.MID
youtube: null
---

Uriya, uriya,
Atton bizar zuriya.
Oyanara juan ta
Aingeru buru xuriya.