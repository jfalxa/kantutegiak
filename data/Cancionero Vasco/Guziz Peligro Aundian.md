---
id: ab-2021
izenburua: Guziz Peligro Aundian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002021.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002021.MID
youtube: null
---

Guziz peligro aundian
Bizi gera emen.
Guarda gaizezu
Ama zerukua, arren.