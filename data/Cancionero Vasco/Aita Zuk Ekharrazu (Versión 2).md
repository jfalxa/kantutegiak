---
id: ab-1531
izenburua: Aita Zuk Ekharrazu (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001531.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001531.MID
youtube: null
---

- Aita, zuk ekharrazu neri, bai, barbera:
Oi, nere sabelian zerbaite badela.
Zuazi eta errakozu barber gaztiari
Sabeletik naizela ni ainitz eri:
Othoi, ekhart dezala zerbait edari.

- Agur, beraz, agur, barber gaztia:
Nere alabak dizu zure galdia.
Neri esan deraut eta gero mandatu
Sabeletik duyena zerbaitek trabatu;
Erremedioarekin laster egizu.

- Nere min hunek orai zenbat du termino?
Ez ditakeia senda merke edo gario?
Libertiziuak badu beti ondorio:
Eta ni, gaizua, malerus gero.

- Etzaitela xangriña, zu, urso gaztia.
Zure min horrek ez du termino betia.
Bederatzi illabetez eginko du fina:
Orduan juanen zaitzu sabeleko miña.