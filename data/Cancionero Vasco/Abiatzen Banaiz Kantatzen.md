---
id: ab-2063
izenburua: Abiatzen Banaiz Kantatzen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002063.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002063.MID
youtube: null
---

Abiatzen banaiz kantatzen,
Kantu ederren emaiten.
Suyetak ere badirade;
Bañan ez ditut nik deklaratzen,
Beldur naiz auteman nazaten.