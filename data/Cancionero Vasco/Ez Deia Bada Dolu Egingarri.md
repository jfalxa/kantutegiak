---
id: ab-1937
izenburua: Ez Deia Bada Dolu Egingarri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001937.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001937.MID
youtube: null
---

Ez deia bada dolu egingarri amodiotan dena?
Beti penetan pasatzen ditu gauak eta egunak.
Ez deia bada pena ene bihotzian dena?
Nehork ezin konpreni diro, oi, ene hasberapena.
Maite bat ezin gonbertituz, nik sofritzen ditudanak!

- Izar xarmegarria, zu bazina enia,
Ikusterat ni jin nindaiteke zure konsolatzera.
Zure begi eztia bihotzian dut sartia,
Amodiozko kordelez han ungi amarratuia.
Eztuzia bada hartu nahi, oi, nitaz pietatia?

- Gizon gazte propia, erraiten daizut egia.
Eztudala hartu nahi pietatezko bizia,
Ez eta ere zurekilan elizadako bidia,
Zeren zuk emana baituzu bertze zonbaiti fedia.
Inposible da gisa hortan pare bat egitia.

- Izar xarmegarria, sanjakorra zirade.
Ene pena eta dolorez aise trufatzen zirade.
Norbaitek derauskazia beharriak elez bete,
Eta zu arabez, gaixua, heien ororen ziñesle.
Gaizki mintzatu nahi duena nork debeka dezake.