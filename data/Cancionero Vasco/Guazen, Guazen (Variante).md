---
id: ab-2010
izenburua: Guazen, Guazen (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002010.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002010.MID
youtube: null
---

Guazen, guazen gu ordu onian;
Geldi bitez ongi urrengo artian.
Baguaz emendik, gu ongi arturik,
Guziok kontentuz beterik.