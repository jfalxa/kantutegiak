---
id: ab-1501
izenburua: Adoratzen Zaitut Jauna
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001501.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001501.MID
youtube: null
---

Adoratzen zaitut, Jauna,
Zeru-lurren egilea;
Goresten zaitut Jinko nere Jauna,
Ohoresten bihotzez.