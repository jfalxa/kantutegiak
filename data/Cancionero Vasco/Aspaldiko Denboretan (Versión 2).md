---
id: ab-2253
izenburua: Aspaldiko Denboretan (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002253.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002253.MID
youtube: null
---

Aspaldiko denboretan, gauaz eta beti,
Jarraiki naiz beti txori polit bati.
Azkenian hatzeman dut, oi! bainan triteki,
Lumarik ederrena baitzaio erori.