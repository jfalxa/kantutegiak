---
id: ab-1607
izenburua: Amodioaren Pena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001607.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001607.MID
youtube: null
---

Amodioaren pena, oi, pena handia!
Neure baitan egiten dut esperientzia.
Ez deia bada pena, eta ezinbertzia,
Maite bat izan, eta hartaz gabetzia,
Hartaz gabetzia.