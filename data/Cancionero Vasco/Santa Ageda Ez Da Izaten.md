---
id: ab-2691
izenburua: Santa Ageda Ez Da Izaten
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002691.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002691.MID
youtube: null
---

Santa Ageda ez da izaten
Beti illian-illian.
Santa Ageda urtian bein de
Bera zesellen bostian.