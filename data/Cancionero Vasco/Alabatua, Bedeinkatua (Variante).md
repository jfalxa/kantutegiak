---
id: ab-2821
izenburua: Alabatua, Bedeinkatua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002821.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002821.MID
youtube: null
---

Alabatua, bedeinkatua
Ai! Sakramentu saindua,
Pekatuaren mantxarik gabe
Zeña dan konzebitua.
Dios te salve ongi etorri
Gabon Jainkoak diela,
Legearekin konpli dezagun
Urteberriren bezperan.