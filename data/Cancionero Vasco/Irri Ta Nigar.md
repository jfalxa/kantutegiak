---
id: ab-2042
izenburua: Irri Ta Nigar
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002042.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002042.MID
youtube: null
---

Irri ta nigar,
Biar ezkondu biar.
Nokin? ta nokin?
Errotako atton xarrarekin.