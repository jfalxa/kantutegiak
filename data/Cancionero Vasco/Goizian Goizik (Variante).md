---
id: ab-1990
izenburua: Goizian Goizik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001990.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001990.MID
youtube: null
---

Goizian goizik jaiki ninduzun,
Oi, espos nintzan goizian.
Bai eta ere zetaz beztitu
Iguzkia jali zenian.
Etxek'andere handi ninduzun
Eguerdi erditan;
Bai eta ere alharguntsa gazte
Ilhunzeinua joitian.