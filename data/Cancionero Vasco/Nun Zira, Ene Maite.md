---
id: ab-2672
izenburua: Nun Zira, Ene Maite
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002672.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002672.MID
youtube: null
---

Nun zira, ene maite, oi xarmegarria?
Behin izanez geroz, galdu dut maitia!
Etzen enetako jin trankilitatia