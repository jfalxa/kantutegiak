---
id: ab-1904
izenburua: Errekaldeko Alhaba
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001904.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001904.MID
youtube: null
---

Errekaldeko alhaba, mutur zuria:
Hik ez duna nahi senhar laboraria?
Eta hi zer hiz?
Saski-egile baten alhaba baihiz.