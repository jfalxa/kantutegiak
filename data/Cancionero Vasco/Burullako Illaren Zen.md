---
id: ab-1822
izenburua: Burullako Illaren Zen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001822.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001822.MID
youtube: null
---

Burullako illaren zen zazpigarrena,
Partido ori Narbarten jokatu zutena.
Zortzi errietarik zortzi pillotari;
Laubak elkarren kontra omen dire ari.

Sunbillatik Prantzisko, Aranaztik Xango,
Doneztebatik Antonio, Gaztelutik Lazaro.
Sunbillatik Juan Felix, Oronoztik Altzairu,
Doneztebetik Iturbide, Legasatik Juan Pedro.

Lau galdu dute oiek, orra ezan klaro;
Bertzien erraiteko oaintxen prest nago:
Pillota aundi sakatu eta, bertzeak ezin jo.
Partido ori galdu dute, ori dela medio.

Sunbillatik diguzte goraintziak biyaldu,
Irabazi ustian zeren duten galdu.
Memoritxo bide bear dire paratu,
Gazteak umorean dezaten kantatu.