---
id: ab-3252
izenburua: Atxuri Beltxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003252.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003252.MID
youtube: null
---

Atxuri beltxa ona da, baño
Obeago da txurie.
Dantzan ikasi nai duen orrek
Nere oñetara begire.
Rai, rai, rai...