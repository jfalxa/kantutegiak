---
id: ab-1813
izenburua: Bost Llaga Sagraduen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001813.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001813.MID
youtube: null
---

Bost llaga sagraduen adoraziua
Biotzetikan balitz sei oraziua,
Egunian bi aldiz ejerziziua,
Egungo bagenduke zerbait leziua.