---
id: ab-1976
izenburua: Gogoz Aditu Itz Onak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001976.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001976.MID
youtube: null
---

Gogoz aditu itz onak,
Andriak eta gizonak,
Bistan zaudeten personak.
Badakizute, esanagatik,
Ez du galtzen maiz entzuna.
Pensatu etorkizunak
Dira gure egukizunak
Logratzeko zoriona.