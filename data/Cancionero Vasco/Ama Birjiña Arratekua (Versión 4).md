---
id: ab-1667
izenburua: Ama Birjiña Arratekua (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001667.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001667.MID
youtube: null
---

Ama Birjiña Arratekua
Arkaitz artako loria.
Aingeruekin astera nua
Esaten ""Ave Maria"".