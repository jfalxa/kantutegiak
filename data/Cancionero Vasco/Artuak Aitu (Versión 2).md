---
id: ab-1654
izenburua: Artuak Aitu (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001654.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001654.MID
youtube: null
---

Artuak aitu eta dirua Mejikun,
Tripazarzarrok guri nork beteko zigun!.
Apez bat edo bestek len emaiten zigun;
Orra, or konpon zaitez,""Dominus vobiscum"".