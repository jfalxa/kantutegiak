---
id: ab-2279
izenburua: Ai Ai Ai,,,,, Ene Maitea (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002279.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002279.MID
youtube: null
---

Ai, ai, ai, ai, ai, ai, ene maitia,
Ez zirade loz ase?
Jeiki, jeiki, jeiki, zaite ganberako leihorat,
Edo bortarat.
Zure ondotik hemen nabilazu
Desertor bat bezala.

Ai, ai, ai, ai, ai, ai, ene maitea,
Oraino goizegi duzu;
Nere aita eta ama oino ez dirade lokhartu,
Errepausuan sartu;
Hek lokhartzen diren bezen laster,
Biek egonen gintuzu.