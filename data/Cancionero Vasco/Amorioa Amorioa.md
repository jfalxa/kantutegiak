---
id: ab-1609
izenburua: Amorioa Amorioa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001609.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001609.MID
youtube: null
---

Amorioa, amorioa,
Baldin ez bada mudatzen,
Egun ontxo bat señalatu ta,
Biak esposa gaitezen.
Udaberriko lore ederrak
Erortzen dire negura;
Beti gustora biziko denik
Ezta jayoko mundura.