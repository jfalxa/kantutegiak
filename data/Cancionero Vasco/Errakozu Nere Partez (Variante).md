---
id: ab-1901
izenburua: Errakozu Nere Partez (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001901.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001901.MID
youtube: null
---

Errakozu nere partez goraintzi
Nere adiskide muxu Barnetxi.
Eta onsa pena dala segurki
Ez dudala iñor ere ura baizik utzi
Satisfatu gabe ederki:
Kausa zer dan berak badaki.

Neskatillekin dela ardura bizi,
Dibertituz ederki.
Ez ditu kitaturen segurki,
Oraino aski
Ez da iñobaturik ain aski.

Aren begiak ain ditu argiak,
Izarrak iduri;
Eta koloriak ditu eun ostoko
Arrosa bera bezain gorri.
Neskatille gazte bana osagarri,
Kalean saltzen ditu ederki:
Denboraz balia bedi.