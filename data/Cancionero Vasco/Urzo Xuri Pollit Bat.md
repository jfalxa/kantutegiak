---
id: ab-2775
izenburua: Urzo Xuri Pollit Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002775.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002775.MID
youtube: null
---

Urzo xuri pollit bat bazen herri huntan;
Aita-amak utzirik yoan zen airetan.
Paregabia baitzen bere lagunetan:
Ihiztariak zuen atzeman saretan.

Urzo xuri pollita kaiolan eritu,
Eta ihiztaria tristezian sartu.
Gaitza berri zueno, nahi zuen saldu,
Erostun tontuen bat arrapatu balu.

Aita-ama gaixuek ez bide zakiten
Ihiztari zaharrak zer zuen egiten:
Urzuak zituela kaiolara biltzen
Eta han inozentez ongi baliatzen.

- Ihiztari zaharra, gaizki usatua,
Etzautzu baliatu aurtengo tratua.
Gogoz saltzen zinuen urzo kolpatua,
Izan balitz mundua arras lokartua.

Bertsuak eman tugu gauaren erdian
Hamar mutiko gaztek umore onian.
Eztugu ez emanen bakerik herrian
Urzoa sorlekura ekarri artian.