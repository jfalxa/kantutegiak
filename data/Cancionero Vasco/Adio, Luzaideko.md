---
id: ab-1492
izenburua: Adio, Luzaideko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001492.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001492.MID
youtube: null
---

Adio, Luzaideko ""Hijas de María"",
Orai finitu den zuen histori.