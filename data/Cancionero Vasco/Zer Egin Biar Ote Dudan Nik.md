---
id: ab-2624
izenburua: Zer Egin Biar Ote Dudan Nik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002624.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002624.MID
youtube: null
---

Zer egin biar ote dudan nik azken fiñian?
Arriturik emen nago gauza orren gañian.
Ez pa al du, ba, nai bizitzaren konduta onian,
Akordatu gia denbora junian,
Anima infernuan sartutzen danian.