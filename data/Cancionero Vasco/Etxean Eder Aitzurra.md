---
id: ab-3392
izenburua: Etxean Eder Aitzurra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003392.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003392.MID
youtube: null
---

Etxean eder aitzurra;
Artzain zanga makurra,
Enganatzera nere ama.
Nik engana bakotxak bere gana.