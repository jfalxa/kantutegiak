---
id: ab-1863
izenburua: Eguberri Gabian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001863.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001863.MID
youtube: null
---

Eguberri gabian
Gu gaude lengo legian;
Gaztiak emen eldu girade
Guziak umildadian.
"Ai uste" salbe, ongi etorri,
Gabon Jainkoak diela,
Gabon Jainkoak digula nola
Egun on dakargula.