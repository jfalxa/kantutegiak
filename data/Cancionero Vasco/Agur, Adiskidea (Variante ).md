---
id: ab-1504
izenburua: Agur, Adiskidea (Variante )
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001504.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001504.MID
youtube: null
---

Agur, adiskidea, Jainkuak egun on!
Zer berri den errazu, zuk, othoi, Zuberon?
Ezagutzen duzia Barkotxen Etxehun?
Halako koblaririk ez omen da iñun:
Bisitaz juan nindaike, ezpalitz hain urrun.