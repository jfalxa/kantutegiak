---
id: ab-1599
izenburua: Ameriketik Buelta Egin Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001599.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001599.MID
youtube: null
---

Ameriketik buelta egin ta
Ontzean Burdeosen,
Ango fondetan eperrengandik
Propina ederrik omen tzen.
Andik etxera etorri eta,
Dirurikan ez omen tzen.
Jango baizuen, aizkora artuta
Laster sasira juan tzen.