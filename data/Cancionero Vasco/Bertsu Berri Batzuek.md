---
id: ab-2713
izenburua: Bertsu Berri Batzuek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002713.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002713.MID
youtube: null
---

Bertsu berri batzuek nai nituzke jarri,
Ni bezalako zenbaiten inpretendigarri.
Gogoan dedana esango det garbi:
Bei zar bat saldu arte txit nengoan larri;
Aren truke obeak ez ditut ekarri.