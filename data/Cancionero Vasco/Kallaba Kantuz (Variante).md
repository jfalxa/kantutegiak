---
id: ab-2191
izenburua: Kallaba Kantuz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002191.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002191.MID
youtube: null
---

Kallaba kantuz ogipetik
Uztaila agorriletan.
Argi alderat etxerakuan
Entzun izan dut bortzetan,
Amodiuak bainerabila
Maiteñuaren bortetan.