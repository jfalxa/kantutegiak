---
id: ab-1704
izenburua: Eperrak Usuak Ekarri (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001704.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001704.MID
youtube: null
---

Eperrak usuak ekarri
Gure mutikotxuari.
Biligarrua, zozua,
Gure mutiko gozua.

Ai, au aurraren ona!
Balio baitu Bayona.
Bayona diruekin saltzen da,
Bañan ez onelako aur ona.