---
id: ab-2228
izenburua: Konbeni Diran Bertso Berriak (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002228.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002228.MID
youtube: null
---

Konbeni diran bertso berriak
Esplikatu al banitza!
Aditutzia deseio duenak,
Eta eman diot itza.