---
id: ab-2676
izenburua: Orantxen Asia Naiz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002676.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002676.MID
youtube: null
---

Orantxen asia naiz kantoren moldatzen,
Ikusi dudanian suyeta nola zen;
Nunbait ez bada gerla ez da konsolatzen,
Frantzesa ibilia da bertzen eskolatzen.