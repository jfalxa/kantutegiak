---
id: ab-2247
izenburua: Labezalia Kiebra Egin Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002247.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002247.MID
youtube: null
---

Labezalia kiebra egin ta
Juan in zaiku igesi;
"Biyar artia" esan zuen, ta
Ez det geyago ikusi.
Burua ere elduko nion
Nere koleran Judasi;
Ene ta ere ez da pagatzen
Olango zenbait nagusi.