---
id: ab-1583
izenburua: Esaiteko Guziok (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001583.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001583.MID
youtube: null
---

Esaiteko guziok
Santa Maria,
Ikara gelditzen da
Infernu guzia.