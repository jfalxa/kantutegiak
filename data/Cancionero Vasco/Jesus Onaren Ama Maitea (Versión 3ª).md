---
id: ab-1582
izenburua: Jesus Onaren Ama Maitea (Versión 3ª)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001582.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001582.MID
youtube: null
---

Jesus onaren Ama maitea,
Grazia dizut eskatzen:
Bortz misterio dolorosuak
Lagun gatxazu erraten.

Debozionez erranen dugu
Lenengo misteriua.
Gida gatxazu zerura beti,
Birjiñe biotzekua.

Jetsemaniko baratz berdian
Jesus zeguan dolorez;
Agoniako penen artian
Izertzen zela (y)odolez.
Nork erranen du zer miñ aundie
Orduan artu ziñuen?
Lagun gatxazu orai ta gero
Eriotzeko (y)orduen.

Bortz mille eta ere geyago
Azote eman ziozten,
Zerengatik gure bekatuek
Bere konture zituen.
Nork erranen du zer miñ aundie


Arantze latzez egiñikako
Korona zorrotz batekin
Josi zioten buru sandue
Burla zutela (y)arekin.
Nork erranen du zer miñ aundie


Jerusalengo kalian gora
Gurutze pisu batekin
Bazeramaten Kalbariora
Penatzen zelarik arekin.
Nork erranen du zer miñ aundie


Gurutze artan iltzez josirik
An utzi zuen bizie;
Gure animek galdu etzitezen
Igaro zuen guzie.
Nork erranen du zer miñ aundie