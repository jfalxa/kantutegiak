---
id: ab-2295
izenburua: Nere Kristabak Dire Amar Eskalera (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002295.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002295.MID
youtube: null
---

Nere kristabak, dire amar eskalera
Mundutik juateko zeruko ballera.
Presentatzen geranian Jesusen aurrera,
Ongi konfesatuak, beti siñale da;
Pekatuen dagonak, segur du galera.