---
id: ab-3234
izenburua: Kartak Iru, Kartak Lau
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003234.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003234.MID
youtube: null
---

Kartak iru, kartak lau,
Kartak engainatzen nau.
Nik izanagatik amairu,
Kontrariuak amalau,
Sakelan dituten diru pixorrekin
Eztiek afalduko gaur.

Joxe Mari Exkerra
Exkaxerreko maixterra,
Dantzan ai xelaik sartu baizuen
Olertetatik ixterra.
Andik ondorian atera baitzuen
Jainkuak dakien bezala.