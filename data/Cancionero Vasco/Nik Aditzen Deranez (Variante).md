---
id: ab-1705
izenburua: Nik Aditzen Deranez (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001705.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001705.MID
youtube: null
---

Nik aditzen deranez,
Leizan mutil fiñik ez;
Engañatuko zaituzte
Jolas ederrez.
Ayek on dire jolasez
Ala badirade biotzez,
Etzaituztela erreko
Bela ur otzez.