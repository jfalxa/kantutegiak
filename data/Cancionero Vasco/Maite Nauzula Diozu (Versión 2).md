---
id: ab-2591
izenburua: Maite Nauzula Diozu (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002591.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002591.MID
youtube: null
---

Maite nauzula diozu,
Nik ere maite zaitut zu.
Diozun bezen maite banauzu,
Elizaz feda nezazu;
Elizaz feda nezazu eta
Gero zurea nukezu.

Maite zaitut bihotzez,
Erraiten daitzut bi itzez.
Sukar-malinak harturik nago,
Etzintzuzkedan beldurrez.
Sendatzerat har nezazu,
Hil ez nadien xangrinez.

Gaitz ororen kontrere
Erremediuak badire.
Sukar-malina balin baduzu,
Serbitza zaite barberez,
Ez eta neure ondoren ibili
Ni meriko beharrez.