---
id: ab-1868
izenburua: Egun Abiatu Naiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001868.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001868.MID
youtube: null
---

Egun abiatu naiz Arraras aldera,
Nun dagoan egin dut frankotan galdia.
Franko bide luzia guretik onera:
Bederatzi legoa, gutxi gora bera.