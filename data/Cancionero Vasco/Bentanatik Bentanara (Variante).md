---
id: ab-1753
izenburua: Bentanatik Bentanara (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001753.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001753.MID
youtube: null
---

Bentanatik bentanara
Airia presko.
Oi, Pedro konpañakua
Bentanan dago.