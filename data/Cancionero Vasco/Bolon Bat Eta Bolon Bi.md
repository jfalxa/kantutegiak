---
id: ab-1802
izenburua: Bolon Bat Eta Bolon Bi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001802.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001802.MID
youtube: null
---

Bolon bat eta bolon bi,
Bolon puzura erori;
Txoriak ere kantatzen dute
Txiruli ruli, txiruli.
Soñu orretan dantzatutzeko,
Maritxu eta Gergori.