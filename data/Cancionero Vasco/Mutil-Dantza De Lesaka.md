---
id: ab-3169
izenburua: Mutil-Dantza De Lesaka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003169.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003169.MID
youtube: null
---

Zantza monona, zantzate,
Billigarrua (y)alkate,
Xixaren puska batengatik
Preso era miute.