---
id: ab-1710
izenburua: Aurten Arnua Merke Da (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001710.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001710.MID
youtube: null
---

Aurten arnua merke da,
Bost sei sosetan pinta da.
Andriak ere eraten dute
Erreberua bezala,
Batek berziari erraiten diola
Motua zuzen dezala.

Erranen diñat, erranen,
Enun ixilik egonen:
Dupela pian zortzen den haurra
Ez dela aguador jinen,
Zeren aitak eta amak ere
Afizione baitzuten.