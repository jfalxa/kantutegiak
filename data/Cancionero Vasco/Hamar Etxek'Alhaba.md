---
id: ab-2023
izenburua: Hamar Etxek'Alhaba
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002023.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002023.MID
youtube: null
---

Hamar etxek'alhaba
Badirade aizo:
Nurk zer estaküria
Dian dirade so.
Algargana bil eta
Erri egin gero.
Ai zer denbora hori,
Eztait nuiz artino. (eztakit.