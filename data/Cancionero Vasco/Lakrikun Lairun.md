---
id: ab-2654
izenburua: Lakrikun Lairun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002654.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002654.MID
youtube: null
---

Lakrikun lairun
Lakrikun lairun
Oigi txigorra
Bustite ondun.

Nere lagunek
Atozte onat.
Nik badakit
Perletxi on bat.