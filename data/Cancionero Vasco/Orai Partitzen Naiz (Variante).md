---
id: ab-2413
izenburua: Orai Partitzen Naiz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002413.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002413.MID
youtube: null
---

Orai partitzen naiz herritik,
Ardura dut nigarra begitik.
Maite bat maitatzeaz geroztik
Bihotzaren erdi-erditik,
Kitatu behar baitut laburski:
Nola konsolaturen naiz ni.