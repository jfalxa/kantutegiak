---
id: ab-1548
izenburua: Aitak Eta Amak Herritik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001548.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001548.MID
youtube: null
---

Dos estrofas idénticas a la anterior.