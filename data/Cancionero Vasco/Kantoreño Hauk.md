---
id: ab-2211
izenburua: Kantoreño Hauk
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002211.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002211.MID
youtube: null
---

Kantoreño hauk gure herrian
Amodioaren gainean,
Berri-berriak emanak dire
Bazko biheramunian.
Urso paloma xuri pollit bat
Neraukan saren artian;
Nere sareak ursorik gabe
Mihatu nintuenian,
Harritu nintzen joana ote zen
Hegalik gabe airean.

Hamazazpi hemezortzi urthe
Oraino ez ditut konplitu;
Amodioetan sarthu nintzala
Yadanik hiru baditu.
Urso paloma xuri pollit bat
Nere kaltetan maitatu,
Harekilako amodioa
Ez baitzait fina gertatu.
Jaun zerukoak berak daki
Nik zonbat dudan sofritu.

Ursoño hura zerbait bazela
Ez da dudarik egiteko.
Botikan ez da oihal peza bat
Guzien begietako.
Erran nahi dut pollita zela,
Perfeta nere gostuko.
Nere begiek ez zuten aski
Klaritate harendako.
Galdu dut eta bizi naizeno
Ni ez naiz konsolatuko.

Ez da barberik ez mirikuirik
Munduan aski abilik
Nere gaitzaren sendarazteko
Erremedio lukenik.
Ursoño harek, lore eder bat
Begietarat emanik,
Neri ken eta bertzeri eman;
Zer egin behar dut bada nik?
Ikusten dizut pazientzia
Ez dela bertze moyenik.