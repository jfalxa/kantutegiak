---
id: ab-2414
izenburua: Oraiko Neska Gaztiak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002414.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002414.MID
youtube: null
---

Oraiko neska gaztiak,
Amarretarik zortziak,
Pretensionez betiak.
Bertzen gostuz bestitzen tuzte
Betentez beharriak,
Edaztunez eriak,
Soinian espartin zuriak:(oinian)
Mutillen gastuz guziak,
La, la, la, la, la,
Mutillen gastuz guziak.

Ez dut erraten bertzerik
Baita inportantagorik
Kongreganiok oik segurik,
Plazetan ere ibiltzeko
Ez dut nahi mutillik.
Bi begiak apalik,
Ezpainak erdi etsirik
Arras artean segurik,
La, la, la, la, la,
Arras artean segurik.

Elsuden iluna
Aspaldi beira daudena
Egiteko fortuna,
Gostu onezxerkatzen dute
Berek eskar dutena.
Ez dut bertzek errana,
Nehorrek frogatu dudana
Hek lotzen diren yuntena,
La, la, la, la, la,
Hek lotzen diren yuntena.

Gero zer zayote gertatzen
Hiz laburrez dut erranen
Guziek yakin dezaten.
Aitzinetik hasten zaizkote
Kotillunak laburtzen,
Estomaka larritzen,
Gerri ondua loditzen
Orduan dire tristatzen,
La, la, la, la, la,
Orduan dire tristatzen.

Nigarrez ordu beretik
Egiaz urrikiturik

Bereala kentzen dituzte
Petentak beharretarik,
Errestasunak eritik,
Zapeta xuriak zoinetik:
Nik ez dut eyen beharrik,
La, la, la, la, la,
Nik ez dut eyen beharrik.