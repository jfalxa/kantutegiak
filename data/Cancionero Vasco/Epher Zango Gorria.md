---
id: ab-1898
izenburua: Epher Zango Gorria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001898.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001898.MID
youtube: null
---

- Epher zango gorria,
mendi gainean sortia:
Gaztettodanik egina diok
Andregaiari gortia.
Kitazak hik herria
Galdu gaberik bizia.

- Kitatuko duk herria,
Maiteago dut bizia.
Ene aita-amek aziko dute
Ene lehen fruitia.
Eta nihaurek askiko dut
Espainiara juaitia.

Espainiako bidaia,
Hura bidaia luzia!
Ardura ardura itzultzen nuen
Gibelerak buria:
Nundik adituko nuen
Tiro balaren soinia.