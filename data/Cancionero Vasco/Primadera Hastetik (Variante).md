---
id: ab-2752
izenburua: Primadera Hastetik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002752.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002752.MID
youtube: null
---

Primadera hastetik,
Xinhaurri zuurra
Lanean ari,
Neguan bazkatzeko...