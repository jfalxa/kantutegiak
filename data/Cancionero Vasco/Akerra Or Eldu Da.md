---
id: ab-1557
izenburua: Akerra Or Eldu Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001557.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001557.MID
youtube: null
---

(A-B)
Akerra or eldu da artuaren yatera.
Otsua or eldu da akerraren yatera.
Zakurra or eldu da otsuaren aizkatzera.
Makilla or eldu da zakurraren yotzera.
Sua or eldu da makillaren erretzera.
Ura or eldu da suaren itzaltzera.
Idia or eldu da uraren edatera.
Soka or eldu da idiaren lotzera.
Sagua or eldu da sokaren pikatzera.
Katua or eldu da saguaren yatera.
Gizona or eldu da katuaren yotzera.
Erioa or eldu da gizonaren iltzera.

(B-C)
Erioak gizona,
Gizonak katua,
Katuak sagua,
Saguak soka,
Sokak idia,
Idiak ura,
Urak sua,
Suak makilla,
Makillak zakurra,
Zakurrak otsua,
Otsuak akerra,
Akerrak artua.