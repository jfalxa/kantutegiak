---
id: ab-2070
izenburua: Arregiko Barbera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002070.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002070.MID
youtube: null
---

Arregiko barbera, Arrotzko semia,
Orrek biar omen du kanpotik andria.
Nor billatu uste du ote du obia?
Orai despeditu du parerikan gabia.