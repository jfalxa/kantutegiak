---
id: ab-2664
izenburua: Milla Zortzirehun Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002664.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002664.MID
youtube: null
---

Milla zortzirehun ta hiruoi ta hamargarren
Urtian izango da gaudela barrunen,
Españolak izandu gera orai'ta lehen.
Baño ikusten dena desgraziaz arren,
Italiano bihurtu nahi gaituzte hemen.

Zenbat errege diren esaterat noa
Hartu nahi luketenak Españian korua:
Monpesie frantsesa zuten lehengoa,
Gero berritz Fernando Portugalekoa,
Orai Duke Aosta da Italianoa.

Errege hoietarik Priñ-en gustokoa
Duke de Aosta da Italianoa.
Bere jenioz ez ta biziro tontua
Musikari ederra ta dantzari bapua
Harpa jotzeko ez da leku txarrekua.

Ainkade ederrak dauzkagu Españian
Nahi dutena egiten goan den aspaldian
Errege bat billatu dute Italian
Españolaren buru jartzeko abian
Onik ez du sartuko gure alderdian.

Ibilli gabetarik herritik herrira
Itsasuz ekartzeko konformatu dira.
Ailiatzen denian lenengo pelira
Errege harpa jotzen, Zerrano begira
Priñ'ta Diagorala dantzatuko dira.

Klaro esaten dut Español denari
Korua ez emateko estranjeruari
Baizik gure Don Karlos zazpigarrenari!!!!

(Bertzeak eskas dira)

(Azkena)
Horra hamabortz pertsu haurtengo berriak
Xeberino Klarantzak eskuaraz jarriak.
Karlista jayo nitzen, Jaunari graziak,
Ta opiniu hortan aurrera hazia.
""Biba Don Karlos eta karlista guziak!!!!.