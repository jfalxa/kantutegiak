---
id: ab-1712
izenburua: Aurten Landatu Sagarrak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001712.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001712.MID
youtube: null
---

Aurten landatu sagarrak
Adarrak ditik aphalak,
Aphalak, aphalak.
Andre ederra duen senharrak)
Ardura izanen tik adarrak. )bis

Jarraiki ago, putruna,
E'haiki hola enoia,
Jarraiki, jarraiki.
Axeri zaharrak
Atxemanen dik oiloa:
Haize hegoaren pare ziagok
Neskato gazten gogua.