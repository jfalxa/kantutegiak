---
id: ab-2352
izenburua: Muthil Gaztia Bilo Horia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002352.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002352.MID
youtube: null
---

- Muthil gaztia, bilo horia,
Burian duka banitatia?
Uste duk bai naski haizu dela bethi
Gorthiaren egitia andre orori.

- Eni kaizu da gald'egitea,
Zuri konbeni begiratzea.
Hiru mutil gazte, zu nahiz emazte,
Elgarren artean disputa badute.

- Izan bezate, nahi badute;
Ene perilik heyek ez dute.
Ez nahiz ezkondu, ez disputan sartu,
Komentu batera serora nihuazu.

- Andre gaztea, kasu izu tronpa;
Zure gogoa khanbiakhor da.
Komentu guziak betheak dituzu:
Zato gure etxerat, fortuna inen duzu.

- Komentu hartan othe daiteke
Lekhu sekretik ni sar naiteke.
Txori balin banitz, ni kreatura bezala,
Ardura yin naiteke zure ikhustera.

pequeña variante:
Txori balin banitz, goyen naiz bezala.