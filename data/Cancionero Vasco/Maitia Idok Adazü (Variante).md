---
id: ab-2662
izenburua: Maitia Idok Adazü (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002662.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002662.MID
youtube: null
---

Maitia, idok adazü ganberako leihua.
Biek egin dezagün argitzeko lua.
Larria xuri eta kolore gorria:
Zure bulhar-ondoko luaren goxua.