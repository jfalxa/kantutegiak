---
id: ab-2385
izenburua: Nik Baditut Iru Tatxa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002385.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002385.MID
youtube: null
---

Nik baditut iru tatxa,
Obe nuke baterez:
Jokalaria, moxkortzen dana,
Ez egin nai biarrik.
Nik biar nuen dama gaztia
Urrikaltzen biar dik.