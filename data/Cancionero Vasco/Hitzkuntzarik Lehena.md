---
id: ab-2117
izenburua: Hitzkuntzarik Lehena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002117.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002117.MID
youtube: null
---

Hitzkuntzarik lehena nundik zen atera?
Eskualdunak zirenez Adan eta Eba?
Hori dakienikan bizirik ote da?
Hori dakiena da Yainko Yauna bera.

Hitzkuntzarik lehena eskuara munduan,
Hortaz mintzo baitziren Noeren barkuan.
Lurra ikus orduko mendi inguruan,
""Ara, Ara"", errantzuten eskuaraz orduan.

Berrogei mende huntan Eskual herrietan
Yende bera bizi da hainitz guduketan.
Etsaiak izan dire bainan alferretan:
Beren hezurrak dituzte utziak lur huntan.

Agertu bezen laster lur huntan mundua,
Aita onek egin zuen batasun osua,
Heien gainerat zalu edaten besua
Hunki hura etsaientzat ez baitzen goxua.

Zeru-lurren Yabia zuri oihuz gaude
Deusetako ez gire laguntzarik gabe.
Heki bezala egizu guri're fagore;
Hori galdatzen dugu ondokoentzat ere.

Ez dire Frantziakoak lur hokien erdiak,
Zatituak baitire bortzetarik biak.
Lur gizenak ditugu, aberats mendiak,
Hoitan hazten baitire artalde handiak.