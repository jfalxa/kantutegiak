---
id: ab-1986
izenburua: Goizean T'Arratsian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001986.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001986.MID
youtube: null
---

Goizean t'arratsian,
Albait debozioz,
Bere orazioko
Egin bear diot.