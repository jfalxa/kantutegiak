---
id: ab-2554
izenburua: Txikitxo Politxua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002554.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002554.MID
youtube: null
---

Txikitxo politxua, norena, norena?
- Norena izango naiz?, attena ta amaena.

Begiak beltxak beltxak, norenak dituzu?
- Zureak, amandrea, zuk nai badituzu.

Nere txikitxua ta nere politxua,
Nori eman diozu neri agindua.