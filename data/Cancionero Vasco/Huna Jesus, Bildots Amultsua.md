---
id: ab-2119
izenburua: Huna Jesus, Bildots Amultsua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002119.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002119.MID
youtube: null
---

Huna Jesus, Bildots amultsua,
Guregatik sakrifikatu dena;
Bekatutan bazare, kristaua,
Goardia ongi, ez hurbil hunengana.