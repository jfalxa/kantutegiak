---
id: ab-2024
izenburua: Haur Ederra, Nungua Zira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002024.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002024.MID
youtube: null
---

- Haur ederra, nungua zira?
Haur ederra zira zu!
- Garaxiko, xiko, xiko, xiko,
Garaxiko nuxu ni.
Garaxiko nuxu, nuxu, nuxu,
Garaxiko nuxu ni.
- Nola deitzen, zira, zira, zira,
Nola deitzen zira zu?
- Deitzen nuxu, nuxu, nuxu, nuxu,
Deitzen nuxu Roxali. (bis.