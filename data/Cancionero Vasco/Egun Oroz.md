---
id: ab-1873
izenburua: Egun Oroz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001873.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001873.MID
youtube: null
---

Egun oroz goizetan arratsetan,
Bethi nago pena-doloretan.
Ezin dirot ez jan,
Ez eta ere edan.
Ez dakit zer dudan:
Gaitza haundi bat da ni-baitan,
Amarratzen nau bet-betan...
Ni enaiteke bizi gisa hunetan.

Othe daite mundu huntan penarik
Nik dutana baino haundiagorik?
Beti trublia nago,
Geroago barnago.
Burua dut lanho;
Esperantza ere bano
Izanen ere oraino
Zu aldamenian ikusi arteraino.

Antxu gazte bilo hori ederra,
Sar zaite, sar, korralean barnera:
Artaldia bada
Oi, zure beharra.
Turna zaite, turna
Zure sortu lekura...
Desiratzen dut nik hura,
Zure bila nindabilan ardura.

Artzain ona?- Badantzut, ene jauna.
- Zato beraz, zato ene gana.
Errebelatu denak
Deraitzuia pena?
Antxu begi urdina, (bilo)
Konsala zaite, artzaina,
Sorturen dela ordaina,
Etzitezila horrela xangriña!