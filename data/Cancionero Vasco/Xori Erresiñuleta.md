---
id: ab-2605
izenburua: Xori Erresiñuleta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002605.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002605.MID
youtube: null
---

Xori-erresiñuleta udan da kantari,
Zeren orduan baitu kanpuan janari.
Neguan ez da ageri, oi! ez ahal da eri!
Udan jin baledi, konsola nainte ni.