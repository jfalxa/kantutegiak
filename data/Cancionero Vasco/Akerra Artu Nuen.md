---
id: ab-1556
izenburua: Akerra Artu Nuen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001556.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001556.MID
youtube: null
---

Akerra artu nuen goiz-pentsu gabetarik,
Errekitu egiteko etzeguen bentarik;
Zakurrak autsikitua lau zanguetarik,
Odola dariola amar aldetarik.

Akerrak egiten zuen zangotikan urgon;
Nik juan nai nuen, bañan ark egin zuen egon.
Eskutik nuen mandua, ark egin ziran on;
Arratseko allegatu nintzan Donamariaño.

Banuak artu eta akerra zaulitu,
Landara biar duela, iñork ezin gelditu.
Gregorio Barrendukua deabruak artu ditu,
Gure landako artuak akerrak jan ditu.

Gregorio Barrendukua txoil aserretua,
Akerra ikusi duela landara sartua.
Edozein diru pagarazteko baduela eskua,
Kobranzaren eiteko lagun bat artua.

Nere kontrariuak lau gizon badira,
Kuriosuak alere lauetarik bida.
Gregorioaren laguna ostalari ori da,
Ark tiratu nai ziran biotzaren erdira.

Eskopeta zubenian ollarrian altxatu,
Uste nuen derrepente biar zidala tiratu.
Alde guzietara nuen begiratu,
Eruekin ez daiteke sobera fidatu.