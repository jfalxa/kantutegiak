---
id: ab-2631
izenburua: Zertako Esango Degu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002631.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002631.MID
youtube: null
---

Zertako esango degu
Izango eztana?
Udan berotuko da,
Neguan otz dana.