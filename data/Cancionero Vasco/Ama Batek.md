---
id: ab-1575
izenburua: Ama Batek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001575.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001575.MID
youtube: null
---

- Ama batek othe sorthia zütü?
Ederra zirela itxura badüzü.
Ezta zelietan izarrik agertü
Itxura bera dianik hartü;
Ez bortü gorenetan elürrik phausatü
Garbitarzün bera dianik hartü.

- Jauna, mintzo zira propheta bezala;
Üdüri zait badüzüla eskola.
E'nezazüla beraz sobera flata:
Juan beharra bainiz serora.
Komentü batetarat bethikoz banua,
Ikhas ahal dezadan zelüko bidia.

- Ezta komentietan, ezta bakharrik han
Nihor bizi denik oi! bere plazeretan.
Ez ezar bürian juaiteko bertan;
Egon zite, egon libertatian.
Nik phena banikezü ikhus bazintzat han,
Reiahetan barnen nigarrez hürtzetan. (ürtzen nigarretan)

- Libertatia zer den oi! nik banaki!
Aita krüdel baten esklabo nüzü ni.
Khanbera batian bethi zerratürik,
Eniz komünikatzen nihureki.
Malherus tristia! Zertako sorthü niz ni!
Eniz arren jelkhiren seküla hementik. (ebenti)

- Kuraje, anderia, eskoladün niz ni.
Guziak eginen tit zure amoreagatik.
Mintzo bazirade oi! klarki zü eni,
Zure libratzeaz kargü dit nahi.
Txerkaturen ditit maneria berhezi,
Hegalta ahal zitian zure hegaleki.

- Nekez izanen da lan horren egitia.
Ene libratzeko aphaltik so zira,
Ni aldiz hemen gora beldür niz jaustera,
Galdü behar bainüke bizia.
Bostgarren estajian dizüt khanbera,
Goizetan goratik hartzen dit hairia.

- Nik badit zeta-hari, jina Xinati,
Mehe da, bai, bena fina da biziki.
Txoriñoaz nahi deizüt hura pasatü zuri:
Estek'ezazü lehio bürdünari.
Eraisten ahal zira hura behiti;
Ni hemen izanen niz zure zerbütxari.

- Inutil da jeüsen erraitia. (deüsen)
Aitak jakin baleza, nintzate galdia.
Nahiz hala lizaitan oi! ene desira (lizatian)
Hola izanez zure borontatia.
Ene esklabajia da ikharagarria,
Khanbera batetan bethikoz zerratia!

- Nik badizüt txori eskolatü,
Ene obeditzera hura bertan prest düzü.
Haietaz nahi deizüt letera pasatü,
Ene phenak zer diren heietan komünikatü.
Erran bezala, behar düzü prestatü,
Eta egüntto batez khanbera kitatü.

- Jainkoak Ülhüntü, gaierdi da heltü (Gaiak)
Txoriñoa ene khanberan sarthü;
Beren mandatia harrek eni komünikatü.
Berta nian hura desmontatü,
Eta partitzia zahle deliberatü;
Odria nian bezala, khanbera kitatü.

- Zolala heltü eta, jaun horri besarkaz
Plazerezko nigarrak heltüz lürrera.
""Koxera, emazü zamarier presa.
Kitatü behar dügü erresuma,
Nihurk jakin eztezan gure adresia.
Bihar jinen gira zure phakatzera"".

- Txoriñua, habil adesako lekhüti,
Hari hura solta ezak bere bürdüñati;
Mehe dük bai, bena fina da biziki.
Eztiagü, ez, galtzera behar ützi:
Andere bat libratü dik sei miliüneki;
Behar tiagü gozatü biek algarreki.

- Aita horren boztariua ondoko eünetan!
Ustez alhaba zian zelü gorenetan!
Birjiña zirelakoz zerratü khanberan,
Uste zian Jainkoak zerola eraman. (zeiola)
Belhauniko zagon hari othoitzetan,
Bera eraman lezan hil eta bertan.

- Alhaba aldiz Egyptan ezkontürik baita:
Diharia franko baitzian eskietan.
Gero hasi denian largatzen zintura,
Aitari igorri zeion letera batian
Zerbütxatzen zeionez egüzaita plazan,
Bere haurtzaruaren presentatzeko elizan.

- Alhaba dien aitek etsenplü hartzeko,
Gero ere izanen da balinba holako.
Ez fida gaztia: malezian dago,
Eta abilago zaharra beno.
Alhaba dien aiter etzaie faltako
Haiek ükhenen die bürüko min franko.