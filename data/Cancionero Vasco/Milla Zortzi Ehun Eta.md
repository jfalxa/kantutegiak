---
id: ab-2331
izenburua: Milla Zortzi Ehun Eta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002331.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002331.MID
youtube: null
---

Milla zortzi ehun eta berrogei ta zazpi,
Urte hartan jo nuen Oyartzungo Txipi.
Geroztik hemen nabil gotti ta behetti;
Beñere ez hil, bañan irriskutan beti.

Ez deia bada pena ni nabillen plantan?
Duru bat ez dut izan nere bizi santan.
Egunian xanpoin bat alderatu faltan;
Nere denbora juan da geroko esperantzan.

Denbora juan eta hasi naiz pensatzen
Zahartziak eztuela gauz'onik ekartzen.
Pixa laburrago jaz danikan aurten;
Zapatak puntatik hasi zait usteltzen.

Jaz zapatari eta aurten belaunari:
Zer diferentzia urte batez hori!
Len paretak hautsi nahi, eta orai xirri-mirri;
Nun dudan billatzeko behar ditut ordu bi.

Agiñak galdu eta puskatuak ortzak;
Zintzurra joiten deraut azpiko kokotzak.
Ezpain biak erriak pipa-gider motzak.
Horra nola juan diren gazteko erronkak.