---
id: ab-1927
izenburua: Etxeko-Andre Galanta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001927.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001927.MID
youtube: null
---

Etxeko-andre galanta,
Begia duzu xarmanta;
Zureganik nahi nuke
Xingarra edo lukainka.

Eztut nahi azpi osua,
Ez eta ere erdia;
Zeronek pikaturik
Liberako zatia.