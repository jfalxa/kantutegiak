---
id: ab-2031
izenburua: Ikustera Yiten Nitzaizu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002031.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002031.MID
youtube: null
---

Ikustera yiten nitzaizu:
Nola zirade, maitia?
Bizi luze bat desir deraizut,
Dohain ororez betia.
Horiek oro hola direla,
Parabisuan sartzia. (bis.