---
id: ab-1761
izenburua: Bentara Nua Bentatik Nator (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001761.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001761.MID
youtube: null
---

Bentara nua, bentatik nator,
Bentan da nere gogoa.
Ango arrosa klabeliñetan
Artu dut amorioa.
Nik ederretan, nik galantetan,
Nik ederretan gogoa;
Ederrenetan ederrena da
Josepa Antoni Zeakoa.