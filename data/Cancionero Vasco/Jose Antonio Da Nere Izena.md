---
id: ab-2165
izenburua: Jose Antonio Da Nere Izena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002165.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002165.MID
youtube: null
---

Jose Antonio da nere izena,
Bakardadian bizi naizena.
Marie ori alargun zen ta,
Ni arengana, pretenditzera,
Arreglatzera.
Sartu orduko aren etxera,
Iru gizon ni preso artzera.