---
id: ab-1668
izenburua: Ama Birjiña Arratekua (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001668.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001668.MID
youtube: null
---

Ama Birjiña Arratekua,
Pekatarien Ama,
Zeruko lora ederrez
Jantzirik zaudena,
Graziak ematera
Nator ni zugana.

Guztiz peligro aundixan
Bizi gerade emen,
Guardatu gaituz, Ama,
Zerurako, arren,
Gero egon gaitezen
Alkarrekin antxen.