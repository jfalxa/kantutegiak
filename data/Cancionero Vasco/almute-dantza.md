---
id: ab-3277
izenburua: Almute-Dantza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003277.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003277.MID
youtube: null
---

Sagarraren adarraren igarraren
Puntaren puntaren puntan
Txoria zegoen kantari:
Oi xiruliruli, ruliruli,
Oi xirurirulet:
Nork dantzatuko du soñu ori?"