---
id: ab-2839
izenburua: Bonbolonera (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002839.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002839.MID
youtube: null
---

Bonbolonera, bonbolonzaie,
Otsuak jan du arzaie;
Otsakumiak, axuri-zaie.
Adio gure arzaie!

Bonbolonera bidezu:
Laister utziko nazazu, (aziko)
Azizaria kolkoan daukat (Az-saria)
Ungi unitzen banazu.

Bonbolonera petrala:
Goazen Allera (sic) pestara.
- Juan den urtian goseak il ta
Aurten Allera zertara.