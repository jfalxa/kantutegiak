---
id: ab-1579
izenburua: Ama Birjiña Fidela (Versión 2°)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001579.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001579.MID
youtube: null
---

Ama Birjiña fidela
Alabatua zirela!
Nere manduak atera tuzu
Minikan gabe librerat.
Xangrin bat artu ederra:
Ala konbeni bide da.