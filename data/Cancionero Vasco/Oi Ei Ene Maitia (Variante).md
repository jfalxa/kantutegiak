---
id: ab-2280
izenburua: Oi Ei Ene Maitia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002280.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002280.MID
youtube: null
---

Oi, ei, oi, ei, oi, ei, ene maitia,
Etzirade loz ase?
Jeiki zaite, jeiki zaite
Ganberako leihorat,
Edo bortalat.
Zure ondotik hemen nabilazu
Desertor bat bezala.