---
id: ab-2318
izenburua: Mehetegiko Xakhurra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002318.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002318.MID
youtube: null
---

Mehetegiko xakhurra,
Zangoz errainez makhurra,
Berga bat luze muthurra,
Larrutik hurbil hexurra,
Urthe guzian barura,
Zeren zen lekhu xuhurra.