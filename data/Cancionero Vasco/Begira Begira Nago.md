---
id: ab-1745
izenburua: Begira Begira Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001745.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001745.MID
youtube: null
---

Begira, begira nago
Zein giran ederrago;
Emen den ederrena da
Agustin konpañiako.