---
id: ab-2689
izenburua: Primaderaren Lenian (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002689.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002689.MID
youtube: null
---

Primaderaren lenian
Arbolak dire loratzen;
Lilia denian loratzen,
Yendiak alegeratzen;
Loriak pasatu eta,
Lurrerat erortzen.

Artzainak nagusiari
Mandatu dio igorri:
Artaldian baduela
Antxuño bat ernari;
Ure nari lukela
Bazketan ezarri.

Mendiak bete belarrez,
Begitartia nigarrez.
Lana eginaz urriki dut
Bañan probetxurik ez.
Orai ementxe nago
Sabeletik minez.