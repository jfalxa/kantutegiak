---
id: ab-2471
izenburua: Santo Kristo Zurekin (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002471.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002471.MID
youtube: null
---

Santo Kristo, zurekin konfesa nadian,
Zer obra egin nuan nik Katalunian:
Lerida pasatu ta urrengo errian,
Bost lagun bizi zidan beren familian:
Guztiak ill nituan gabaren erdian.