---
id: ab-2306
izenburua: Maritxu Muñondoko
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002306.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002306.MID
youtube: null
---

Maritxu Muñondoko,
Neska mañosia,
Bera da beltxerana,
Gorputz airosia.

Astegunian ere
Eraztun paria

Maritxu Muñondoko
Aurra egiñik dago
Ura bisitatzera
Juateko nago.

Paretxu ollanda ta
Pare bat ollasko
Ura bisitatzeko
Baititut asko.