---
id: ab-2234
izenburua: Konpañian Zein Dan Eder
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002234.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002234.MID
youtube: null
---

Konpañian zein dan eder kantaren kantatzean! (bis)
Botoilla ere utsa da; enda zatoz, etxeko-andria;
Ez dakizu kantaria maiz dela egarria?

Ela, ela, ela, borda hortan norbait bada? (bis)
Jaun erretora, jaun erretora, jaun erretora ote da?
Jaun erretora baldin bada, duala meza emaitera.