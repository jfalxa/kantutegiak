---
id: ab-2399
izenburua: Oi Eguberri Gaua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002399.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002399.MID
youtube: null
---

Oi Eguberri gaua,
Bozkariozko gaua,
Alageratzen duzu
Bihotzian kristaua.