---
id: ab-2350
izenburua: Mutil Gaztia Bilo Horia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002350.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002350.MID
youtube: null
---

- Mutil gaztia, bilo horia,
Buruian duka banitatia?
Uste duk bai naski haizu dela beti
Gortiaren egitea andre orori.

- Eni haizu da galdegitea;
Zure konbeni begiratzea.
Hiru mutil gazte zu nahiz emazte,
Elgarren artean disputa badute.

- Izan bezate nahi badute;
Ene perilik heiek ez dute.
Ez nahiz ezkondu, ez disputan sartu,
Komentu batera serora nihazu.

- Andre gaztea, kasu izu tronpa;
Zure gogoa kanbiakor da.
Komentu guziak beteak dituzu;
Zato gure etxerat, fortuna inen duzu.