---
id: ab-1496
izenburua: Aditurik Loyolako
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001496.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001496.MID
youtube: null
---

Soli.
Aditurik Loyolako semea
Zarotzula mintzo Jainko Jaunaz,
Berehala mundutik urruntzea
Egin duzu, orhoituz geroaz.

Tutti.
Eskualdunen lore paregabea,
Zuri gaude oihu saminetan.
Entzun zazu herritarren galdea,
Zu zarena zeru gain-gainetan.