---
id: ab-1647
izenburua: Arnoaren Bentaka (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001647.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001647.MID
youtube: null
---

Arnoaren bentaka noha esatera.
Milagro handiagorik munduan ote da?
Hasi baño lehenago, iñor edatera,
Zenbat konbeni zaion, kontuak atera.

Gizonak galdetzen du: non da arno ona?
Arnoak erantzuten dio: hemen nago, Jauna.
Ni zu nahi zintazket bideko laguna,
Beldur gabe pasatzeko gau eta eguna!

Preso para ninduten kupela batian;
Geroztik hemen nago sei ilabethian.
Lagunduko zintuzket amodio mentian;
Begira ez dadin izan zuretzat kaltian.

Gizona ezin altxatuz andre ari da.
Jendea farraz ithotzen aldetik begira!
Dolorez altxatu'ta, bertzian erori,
Zikintzeko beldurrez loiaren erdira.

Batian altxatu'ta, berriz erori da.
Nolazpeit ailiatu zaiku gizon eder hori.
Etxerakoan eta andre gaxoari
Txapela khendutzeko tabernariari.

- Gizona, sar zoaitia pakean ogera; (ohera)
Biar egun sentian konponduko gera.
Txapela prend'utzi'ta, etorri ahal zera,
Kredito onen bat guk badegu zahartzera.