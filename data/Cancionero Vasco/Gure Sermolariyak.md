---
id: ab-2019
izenburua: Gure Sermolariyak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002019.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002019.MID
youtube: null
---

Gure sermolariyak
Kulpitotik dakar
Gorroturikan gabe
Maitatzeko alkar.
Zerurako bidia
Eongo da miar;
Akordatu gañazke
Egun edo biar.