---
id: ab-1854
izenburua: Dügün Alegrantzietan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001854.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001854.MID
youtube: null
---

Dügün alegrantzietan
Khanta Jesüsen sortzia.
Gure ahal güzietan
Ohora Jinko Semia.
O! Jesüs, zure graziak
Ganbia gitzan güziak.

Errepikaz aingüriak
Entzüten dira khantatzen;
Gü ere, loriatiak,
Has gitian jauna laidatzen.
O Jesüs...

Jesüs erregez, artzaiñez
Izan da adoratia;
Ah! gütarik gehienez
Nula da ezagütia!
O Jesüs...

Jesüs, negü gogorrian
Ohetzat düzü lastua;
Nulaz, beraz, plazer-pian
Dükegü gure gogua!
O Jesüs...

Sortzetik ümiltarzüna
Daroküzü predikatzen;
Zertako goratarzüna
Dügü bada gük txerkatzen?
O Jesüs...