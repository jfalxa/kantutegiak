---
id: ab-2786
izenburua: Zazpi Urtez Iduki Izan Dut (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002786.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002786.MID
youtube: null
---

Zazpi urtez iduki izan dut
Senarra hilla etxian,
Egunaz kutxa batian eta
Gauaz bi besoen erdian.
Zitroin urinez gantzutzen nuyen
Astian egun batian,
Astian egun batian eta
Hura ortzirale goizian.