---
id: ab-1910
izenburua: Esango Due Ari Naizela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001910.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001910.MID
youtube: null
---

Esango due ari naizela (dute)
Gaizki emakumientzat;
Gorrotorikan iñork artzia
Ez nuke nai nik neretzat.
Banidadezko panparreria
Askok duela deritzat; (dutela)
Guzi guziek ez dakit, bañe
Geien oriek bai beintzat.
Nik esan arren, ala ez danak,
Ez konprenditu beretzat.