---
id: ab-2127
izenburua: Izar Eder Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002127.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002127.MID
youtube: null
---

Izar eder bat, zuriz jantzia,
Izandu ziñan sortuba,
Ez gu bezela, Adan ta Eban
Pekatuba mantxatuba.