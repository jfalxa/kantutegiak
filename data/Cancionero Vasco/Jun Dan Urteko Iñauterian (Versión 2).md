---
id: ab-2176
izenburua: Jun Dan Urteko Iñauterian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002176.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002176.MID
youtube: null
---

Jun dan urteko iñauterian
Ninjualarik zurekin,
Galdetu nizun amoriorik
Zendukazun iñorekin.
- Ez, Jose Migel, san zinidazun, (esan)
Zeñek naiko du nirekin?
Asko dirade ederraguak,
Naigo dute ayekin.