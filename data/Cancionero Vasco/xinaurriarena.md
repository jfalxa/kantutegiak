---
id: ab-2688
izenburua: Xinaurriarena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002688.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002688.MID
youtube: null
---

Primadera hastetik,
Uda (y)azkenera,
Xinaurri zurra lanian ari
Neguan jateko.
Alor bazterrean,
Xoriñua heldu zako
Trufa egiterat.

Xinaurri kiskil,
lepa mehia,
Utzak, utz, lan hori.
Jainkoak emanen dik
Jan ahala eta edari.