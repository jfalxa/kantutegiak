---
id: ab-2112
izenburua: Bo, Bo, Bo. Aurra, Egizu Lotto (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002112.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002112.MID
youtube: null
---

Bo, bo, bo.
Aurra, egizu lotto, lotto.
Nok emanen bi kokotxo,
(Orain bat, eta gero bertzia).