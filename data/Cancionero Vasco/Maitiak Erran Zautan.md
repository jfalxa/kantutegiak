---
id: ab-2287
izenburua: Maitiak Erran Zautan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002287.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002287.MID
youtube: null
---

Maitiak erran zautan
Pollit nintzanez;
Pollit, pollit nintzela,
Bainan larrua beltz.