---
id: ab-2537
izenburua: To Berak Astua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002537.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002537.MID
youtube: null
---

To, berak, astua,
Eman ostikua
Jesus Haurrari,
Jesus Haurrari;
Ez duk uste, bainan
Hik huke hobena,
Mainku baladi.