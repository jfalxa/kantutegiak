---
id: ab-2282
izenburua: Xarmagarria, Jeiki, Jeiki (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002282.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002282.MID
youtube: null
---

- Xarmagarria, jeiki, jeiki,
Etziradeia loz ase?
Zure onduan gabiltzan gaixuak
Ez gituzu, ez, logale.
Idekaguzu bortañua:
Adixkidiak girade.

- Nik eztizut idekitzen
Gau ilhunian bortarik;
Barnian denak ezpaitaki
Kanpuan denaren berririk.
Txauri bihar egunaz eta (Zato)
Etxian izain naiz bakarrik.

Kontrapasak hiru puntu,
Arrabita zurdetan.
Aspaldi enuzu izan
Neskatila gaztetan,
Ez eta ere argirik piztu
Gauaz ostatuetan.

- Zerendako eztuzu piztu
Ostatuetan argia?
Orai ere hobe zinuke
Botoilarekin ogia,
Borta ondo hortan egon gabe
Neska gaztien guardia.