---
id: ab-2610
izenburua: Xoritxua Norat Hua (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002610.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002610.MID
youtube: null
---

Xoritxua, norat hua,
Bi egalez airian?
Españara joaiteko
Elurra da bortuan.
Joanen gira elgarrekin, )
Hura urtu denian. )bis

San Joseferen ermita
Desertuan gora da.
Españarat joaitean
Han da nere pausada.
Gibelera so egin eta
Asberapenak ardura.