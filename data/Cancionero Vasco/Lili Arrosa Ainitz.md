---
id: ab-2258
izenburua: Lili Arrosa Ainitz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002258.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002258.MID
youtube: null
---

Lili arrosa ainitz bada bazterretan:
Gazteen xoragarri, herri guzietan.
Ni ere gertatu naiz halako batetan;
Urrikitan egon naiz geroztik bortzetan.