---
id: ab-1696
izenburua: Atzo Ttun Ttun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001696.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001696.MID
youtube: null
---

Atzo ttun ttun, gaur ttun ttun,
Beti ttun ttun gaittun gu.
Gure diruak aitun eta
Nork naiko gaittun gu?
Atzo ttun ttun, gaur ttun ttun,
Beti ttun ttun gaittun gu.

Atzo ttun ttun, gaur ttun ttun,
Beti ttun ttun gaitun gu.
Zazpi librako ollo txuria
Gaur azeriak jan ttun gu.
Atzo ttun ttun, gaur ttun ttun,
Beti ttun ttun gaittun gu.