---
id: ab-2803
izenburua: Juan Nitzen Montera (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002803.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002803.MID
youtube: null
---

Juan nitzen, juan nitzen montera
""Monte"" zela mendia.
Ikusi nuen, ikusi nuen ""pájaro""
Zela txoria .
Tiratu nion, tiratu nion ""piedra""
Zela arria.
Ateri nion, ateri nion odol
""Sangre"" gorria.
Pin pin txoria, txori moko oria.
Pin pin, koñata, txori salsa on bada.