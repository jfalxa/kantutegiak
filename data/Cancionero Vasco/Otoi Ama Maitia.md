---
id: ab-2431
izenburua: Otoi Ama Maitia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002431.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002431.MID
youtube: null
---

Otoi, Ama maitia, urrikal zaizkigu;
Jaunaren asarria, otoi, ezti zazu.
Non dire fededunak? Non dire justuak?
Azken orenak ote ditugu munduan.