---
id: ab-2461
izenburua: San Migel Aingerua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002461.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002461.MID
youtube: null
---

San Migel Aingerua, Aingeru bizia,
Zerutikan lurrera bizirik jetsia.
Zerutik ez da jeisten Aingeru bizirik,
San Migel Aingerua beraxe besterik.