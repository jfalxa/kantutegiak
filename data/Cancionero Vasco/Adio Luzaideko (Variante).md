---
id: ab-1493
izenburua: Adio Luzaideko (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001493.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001493.MID
youtube: null
---

Adio, Luzaideko ""Hijas de María"",
Orai finitu den zuen histori.