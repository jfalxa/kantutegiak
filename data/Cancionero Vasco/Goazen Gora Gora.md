---
id: ab-1972
izenburua: Goazen Gora Gora
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001972.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001972.MID
youtube: null
---

Goazen gora gora )
Birjiña Amaren tenplora. ) bis
Birjiña Amaren tenploan
Nintzan belauniko jarri;
Lendabiziko orazioa
Egin nion berari.