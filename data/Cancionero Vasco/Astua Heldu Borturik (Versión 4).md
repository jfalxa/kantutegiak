---
id: ab-1688
izenburua: Astua Heldu Borturik (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001688.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001688.MID
youtube: null
---

Una estrofa, igual a la primera de "Astua, heltü bortütik".