---
id: ab-3387
izenburua: Himno De San Juan (En Lekeitio)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003387.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003387.MID
youtube: null
---

Ut queant laxis resonare fibris.
Miragestorum famuli tuorum,
Salve polluti labii reatum sanite Joannes.