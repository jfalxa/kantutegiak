---
id: ab-2588
izenburua: Askotxo Eran Banuan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002588.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002588.MID
youtube: null
---

Askotxo eran banuan,
Ondoren pagatu nuan,
Deskalabratu munduan.
Nere ondotik para zan batek
Galdeti zian zer nuan
Abermiñen bat au nian
Nundikan senti zenduan
Plantainoen bat ja nuan.