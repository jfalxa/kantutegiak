---
id: ab-2351
izenburua: Neska Gaztia Ille Horia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002351.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002351.MID
youtube: null
---

- Neska gaztia, ille horia,
Buruan duzuya bonitatia?
Hirur mutil gazte zu nahiz emazte,
Elgarren tartian disputa badute.

- Izan bezate, nahi badute;
Nere pellikan heyek ez dute.
Ez nahiz ezkondu, ez disputan sartu,
Komendu baterat serora niazu.

- Orai duzuya deliberatu
Behar zirela serora sartu?
Serora sartuz geroz, behar da egin botu;
Botu hartuz geroz, behar da segitu.
Zuazi komenturat, fortuna egizu.