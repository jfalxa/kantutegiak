---
id: ab-1881
izenburua: Eman Ta Kendu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001881.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001881.MID
youtube: null
---

Eman ta kendu,
Apuk biatza kendu.
Biar eskila yo,
Ta etzi infernura.