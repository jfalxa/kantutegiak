---
id: ab-2409
izenburua: Ollarraren Bertsuak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002409.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002409.MID
youtube: null
---

Ollarraren bertsuak
Izain dire amar;
Zein diren garbiroki
Eztut erran biar.
Nai tuenak aditu
Egun edo biar,
Zazpi lagun giñaden,
Bi gaztekin bost zar.