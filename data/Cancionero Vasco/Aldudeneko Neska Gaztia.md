---
id: ab-1569
izenburua: Aldudeneko Neska Gaztia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001569.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001569.MID
youtube: null
---

Aldudeko neska gaztia ""Enfant de Marie"" sartu da.
Guztiz geyenak segurik dira urrikituak.
Elizan iduri dute paretako sainduak;
Gero deitzen dituzte berek nai tuzten maitiak.