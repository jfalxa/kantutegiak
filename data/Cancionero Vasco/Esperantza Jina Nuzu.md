---
id: ab-3006
izenburua: Esperantza Jina Nuzu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003006.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003006.MID
youtube: null
---

Esperantza jina nuzu bidian,
Lorian;
Begitarte egidazu etxian
Sartzian.
Ni etxean sartzian,
Xutik sukaldian,
Serbiet eder bat besoan,
Botoila eskuan,
Basua bertzian.