---
id: ab-1541
izenburua: Aitak Eman Daut Dotea
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001541.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001541.MID
youtube: null
---

Aitak eman daut dotea
Neurea, neurea:
Urdeño bat bere xerriekin,
Oilo koloka bere xitoekin,
Tipula korda heiekin. (bis)

Otsoak jan daut urdea,
Neurea, neurea,
Axeriak oilo koloka,
Arratoinak tipula korda.
Adios ene dotea! (bis.