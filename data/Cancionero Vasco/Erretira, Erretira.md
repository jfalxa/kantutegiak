---
id: ab-1907
izenburua: Erretira, Erretira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001907.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001907.MID
youtube: null
---

Erretira, erretira,
Erretira ordu da;
Gizon debotxak argi alderat
Erretiratzen beitira.