---
id: ab-1740
izenburua: Basaburura Obe Du Argiz
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001740.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001740.MID
youtube: null
---

Basaburura
Obe du argiz;
Ni illunbetan
Ez nua berriz.