---
id: ab-1560
izenburua: Akerrak Egiten Zuen (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001560.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001560.MID
youtube: null
---

Akerrak egiten zuen
Zangotikan erren;
Nik nai nuen joan, baño
Ark nai zuen egon.
Eskutik nuen aurrian,
Egiten zidan on.
Bertzelaz biarko nuen
Bereartan egon.