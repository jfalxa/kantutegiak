---
id: ab-1630
izenburua: Itsasoan Ari Ziren (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001630.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001630.MID
youtube: null
---

Itsasoan ari ziren
Arbola landatzen;
Itsuak ikusi du
Etxea erretzen;
Mutuak deiardaka
Jendearen biltzen;
Zangomotza lasterka
Urketan ari zen.

Mutiko bat ikusi dut
Hiruehun urtetan
Mariñel zabilala
Urtzietako soketan
Haren attona berriz
Hararat urketan
Kalotxatzat zituela
Xalupak oinetan.

Zakurrak ikusi du
Gatua itsasoan
Promenan zabilala
Ombrela eskuan,
Haren attona berriz
Ari zen jokuan,
Kartatzak zituela
Ezpalak eskua.