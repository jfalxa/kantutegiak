---
id: ab-1648
izenburua: Batian Arrastaka (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001648.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001648.MID
youtube: null
---

Batian arrastaka, bestian erori,
Nolazpait etxera da gizon eder ori.
Gero maldizioka andre gaxuari,
Txapela kendutzeko tabernariari.