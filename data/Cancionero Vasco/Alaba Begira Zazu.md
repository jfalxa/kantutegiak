---
id: ab-1561
izenburua: Alaba Begira Zazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001561.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001561.MID
youtube: null
---

Alaba, begira zazu leyotik atarira,
Iya mutil oyek juan ote diran.
- Ez, ortxen egonen dira zure jolasai begira;
Or goyan landan barrena i(r)ago dira.