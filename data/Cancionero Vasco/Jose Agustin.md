---
id: ab-2163
izenburua: Jose Agustin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002163.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002163.MID
youtube: null
---

Jose Agustin, nere laguna,
Auspez lurrera jarririk,
Beti mutil zar egon da ere,
Ez dadukazu oberik.
Admiratua arkitutzen naiz
Ez dezu artzen andrerik.