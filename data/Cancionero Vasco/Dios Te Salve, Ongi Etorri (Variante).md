---
id: ab-1864
izenburua: Dios Te Salve, Ongi Etorri (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001864.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001864.MID
youtube: null
---

Bat:
Dios te salve Ongi etorri;
Gabon Jainkoak dizula;
Gabon Jainkoak dizula eta
Urte onian sar gaitzala.

Denak:
Bedeinkatua ta alabatua da
Sakramendu saindua;
Bekatuaren mantxarik gabe
Birjiña kontzebitua.