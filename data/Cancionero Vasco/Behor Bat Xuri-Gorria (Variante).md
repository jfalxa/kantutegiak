---
id: ab-1746
izenburua: Behor Bat Xuri-Gorria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001746.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001746.MID
youtube: null
---

Behor bat xuri-gorria,
Bidartetik ekarria,
Lau zango-besoz lodia,
Tranlarai, tranlare, la, la, la,
Lau zango-besoz lodia,
Goizetan du logalia.

Ama denian aurrekin
Egon behar da heiekin
Ni zergatikan ez,
Tranlarai...
Ni zergatikan ez,
Maitia, zurekin.