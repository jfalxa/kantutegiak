---
id: ab-1716
izenburua: Azkañeko Atsua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001716.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001716.MID
youtube: null
---

Azkañeko atsua,
Bringillin brangillun, gaixtua.
Gure atsua kanberan dago,
Gona gorria yauntzirik.
Ezkondu behar, behar duela,
Demoñiuak harturik.
Azkañeko atsua,
Bringillin, brangillun, gaixtua.

Azkañeko atsua,
Bringillin, brangillun, gaixtua.
Jaun erretorak soñua jo ta
Andre serora dantzatzen;
Haren aldian apez-mutilla
Xirola-soñuz gelditzen
Azkañeko atsua,
Bringillin, brangillun, gaixtua.