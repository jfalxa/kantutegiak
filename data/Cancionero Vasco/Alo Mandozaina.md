---
id: ab-1572
izenburua: Alo Mandozaina
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001572.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001572.MID
youtube: null
---

Alo, mandozaina, hots, emak eneki;
Behar diagü gezür bat pensatü biak algarreki;
Biak pensatü eta hik declaratü
Otsuak deitadala hazienda xahatü.