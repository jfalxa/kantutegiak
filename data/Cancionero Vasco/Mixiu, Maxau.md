---
id: ab-2805
izenburua: Mixiu, Maxau
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002805.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002805.MID
youtube: null
---

Mixiu, maxau,
Gattuak janen nau.
Papa ta xaxa eskuan
Mallau, mallau, mallau.