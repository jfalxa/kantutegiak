---
id: ab-2709
izenburua: Argizagi Ederra (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002709.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002709.MID
youtube: null
---

Argizagi ederra, argi egidazu!
Bidaia luze batez yoan beharra nuzu.
Gaur behin nahi nuke maitia mintzatu:
Ni harat heldu arte, argi egidazu!

Lotara ziradeia, lo egile pollita?
Lotara ez bazera, so egidazu leiora.
Eta egiaz mintza, ene izar ederra,
Zure ait'eta ama diraden lotara.

Ene ait'amen lotaratzeaz ez duzu dudarik.
Bainan etxeko mutila, oraino ez dakit.
Aita-amentzat bezain harentzat beldur niz;
Ene guardian dago, lo guti eginik.