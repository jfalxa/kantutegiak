---
id: ab-2329
izenburua: Mila Zortzi Ehun Ta Berrogei Ta Zortzi (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002329.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002329.MID
youtube: null
---

Mila zortzi ehun ta berrogei ta zortzi.
Kantore berri batzuk behar ditut jarri;
Penetan direnentzat, oi, kontsolagarri,
Gauzak ez ditzaten har sobera barnegi.

Kantuak eman daizkot ene buruari,
Zeren barnian dudan ainhitz sofrikari!
Korte eginik nago izar eder bati,
Eta ni malerusa sortian erori!

Partitzeko tenoria huna nun da jina,
Urrikaltzeko beita kasu huntan dena;
Izar baten uztiak emaiten daut pena,
Zeren hura bainuen munduan maitena.

- Izar xarmagarri, oi, penetan gira;
Pena-doloriak oro gure gainian dira.
Zure hitzetan fidel egoiten bazira,
Jainkoaren graziak gu ganako dira.

Graziak aurten dira, oi, denborarekin,
Eta partikularzki zure'ta nerekin,
Ezkondutzekotan biok elkarrekin,
Armadetako berri behar dugu jakin.