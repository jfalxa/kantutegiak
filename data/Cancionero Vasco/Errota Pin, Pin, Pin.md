---
id: ab-3388
izenburua: Errota Pin, Pin, Pin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003388.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003388.MID
youtube: null
---

Errota pin, pin, pin.
Errota pan, pan, pan.
Errota pian gariya.
Gariya gañian txoriya.
Neska-mutillak matsa jaten
Kolore gorri-gorriya.