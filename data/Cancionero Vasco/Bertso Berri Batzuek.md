---
id: ab-2074
izenburua: Bertso Berri Batzuek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002074.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002074.MID
youtube: null
---

Bertso berri batzuek
Nari nuzke ezarri
Neure lendabiziko
Neure nobiari.
Lenago zen flakatto,
Orain oso lodi:
Mundu guziak daki
Zer nola den ori.