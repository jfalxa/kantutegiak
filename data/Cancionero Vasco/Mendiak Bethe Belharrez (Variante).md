---
id: ab-2322
izenburua: Mendiak Bethe Belharrez (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002322.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002322.MID
youtube: null
---

Mendiak bethe belharrez;
Begitartia nigarrez.
Oraiko nexka gaztiak
Hanturik daude nigarrez,
Ezkont ez diten beldurrez.