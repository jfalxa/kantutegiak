---
id: ab-1890
izenburua: Nere Muthil Ttipiena (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001890.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001890.MID
youtube: null
---

Nere muthil ttipiena,
Hik emaiten nauk
Biotzian pena.
Habil harat, habil hunat,
Habil mastaren puntarat,
Ageri denez untzirik,
Edo bertzenaz leihorrik.

- Nere nausi maitea,
Heldu nauzu tristerik:
Ezta ageri untzirik
Ez eta ere leihorrik.
Zu yan beharra balin bazare,
Ni yan nazazue lehenik.

Se repite la 1ª estrofa.

- Nere nausi maitea,
Heldu naiz alegerarik:
Ikusi dut untzia
Bai eta ere leihorra;
Zure arreba Maria
Leihoan yosten ari da.