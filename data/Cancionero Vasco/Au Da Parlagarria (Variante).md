---
id: ab-1700
izenburua: Au Da Parlagarria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001700.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001700.MID
youtube: null
---

Au da parlagarria, ""¡válgame Dios!""
Agure zar onek kontu berriok.
Norbaiteri munduan galdetu biar diot
Nondik ote dituan ezpata ukaldiok.
Nagolaik serio,
Itzemaiten diot,
Milla amorioz,
Ixilduko tudala erronkariok.