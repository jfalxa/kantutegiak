---
id: ab-1551
izenburua: Aitaz Garaztarra
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001551.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001551.MID
youtube: null
---

Aitaz garaztarra, amaz baztandarra,
Eskualdun garbia zen ta nafartarra.
Gora, gora Saindu handia,
Gora, gora Eskual Herria.