---
id: ab-2374
izenburua: Nere Maite Polita
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002374.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002374.MID
youtube: null
---

Nere maite polita Donostian dago;
Iparra gora da ta mendebala dago.
Auxen zorigaiztozko mendebal urtia,
Ipar ori beiñ-ere agertu gabia.