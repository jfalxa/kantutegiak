---
id: ab-1536
izenburua: Aita Saintuak Eskribitu Du
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001536.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001536.MID
youtube: null
---

Aita Saintuak eskribitu du
Espainiara goraintzi.
Santiagoren erregalia
Eztakigula gaitzetsi.
Religioa nai eztuena
Irten dabila iesi.
Gure reinuan ezta tokatzen
Libertadia nagusi.

Liberal buru gogorrekuak,
Laja zazute kolera.
Umildadean para zaitezte
Jesukristoren legera.
Guziok elkar artu dezagun,
Atozte arrazoinera.
Tokatzen zaion gizon justua,
Bertzerik bainon hobe da.