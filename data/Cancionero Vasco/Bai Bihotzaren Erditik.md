---
id: ab-1721
izenburua: Bai Bihotzaren Erditik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001721.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001721.MID
youtube: null
---

Coro: Bai, bihotzaren erditik)
Dezagun kanta gogotik. )bis

Solo: Dezagun kanta gogotik;
Gaiten guziak bozkaria:
Gure salbatzeagatik
Jesus, eman duzu bizia.