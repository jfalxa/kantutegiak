---
id: ab-1796
izenburua: Birjiña Amaren Bezpera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001796.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001796.MID
youtube: null
---

Birjiña Amaren bezpera da ta
Goazen Arantzazura;
Santa Luziaren begi ederrak
Argi egiten digula.