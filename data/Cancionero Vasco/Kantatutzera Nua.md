---
id: ab-2199
izenburua: Kantatutzera Nua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002199.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002199.MID
youtube: null
---

Kantatutzera nua eskarmentagarri;
Enuke nai ejenplo gaistoriken jarri.
Orra ondore txarra gustubak ekarri;
Orain neska-mutillak zelotan alkarri.