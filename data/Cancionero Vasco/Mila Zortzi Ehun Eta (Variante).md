---
id: ab-2326
izenburua: Mila Zortzi Ehun Eta (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002326.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002326.MID
youtube: null
---

Mila bat zortzi ehun eta
Lauetan hogoiean,
Bertsu berri hauk eman dire
Aire zahar batean.
Gure aitaso kantabreak
Lo baitaude lurpean,
Ez othe dire atzarriko
Aire hau aditzean.