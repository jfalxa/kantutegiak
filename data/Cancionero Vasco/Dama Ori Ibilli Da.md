---
id: ab-1833
izenburua: Dama Ori Ibilli Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001833.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001833.MID
youtube: null
---

Dama ori ibilli da bera konfidente
Deseo duten gauza logratu bitarte.
Etxetik eman dio apaizari parte
Erri onetan bada aunitz mutill traste.