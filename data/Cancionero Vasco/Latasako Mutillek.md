---
id: ab-3240
izenburua: Latasako Mutillek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003240.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003240.MID
youtube: null
---

Latasako mutillek
Zer duten merezi:
Errekan beratuta,
Litxuan erori.
La ra la la...

Or goyen goyen
Etxe bat badago;
Euririk ezta sartzen,
Ateri badago.
La ra la la...

Baratzeko pikuak
Iru txurten ditu:
Mutil neskazaliak
Ankak ariñ ditu.
La ra la la...

Ankak ariñak, eta
Buru ariñago:
Dantzan obeki daki
Artajorran baño.
La ra la la...

Artajorrara nua
Lur berriberrire:
Belarrak jorratu ta
Artoa sasire.
La ra la la...

Latasako mutillek
Duten arreoa:
Burrunzali zar zar bat
Iru zulokua.
La ra la la...

Latasako mutillek
Duten fantasie:
Gerrian gerrikua
Bestek erosie.
La ra la la...

Eperrak kantatzen du
Goizian intzian:
Neskatxak, ez fiatu
Mutillen itzitan.
La ra la la...

Ederra zera, bañan
Etzera arrosa;
Kolorez zuri-gorri
Neskatx'amorosa.
La ra la la...

Ederra zeran bezin
Baziña sekreta,
Maizeo joko nuke
Leyoko krisketa.
La ra la la...