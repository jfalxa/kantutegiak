---
id: ab-1568
izenburua: Aldixe Batez
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001568.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001568.MID
youtube: null
---

Aldixe batez nenguelarik maitiareki leihuan,
Erran ükhen niriozün hura niala goguan;
Ene phena doloretzaz pietate har lezan.

- Zure phena doloretzaz pietate badit nik;
Ene khorpitzak eztiro egin nihuri plazerik:
Promes bat emanik nüzü Jinko Jaunari lehenik.

- Oro eijer, oro pollit, zü zira ene maitia.
Zure eskutik nahi nikezü bizi nizano ogia,
Eta gero ni nükezü zure zerbütxaria.

- Enüzü ni hain eijerra, nulaz erraiten deitazü.
Mündü ni hain eijerrena berthütia lükezü;
Hari ogenik egin gabe maithatü nahi banaizü.