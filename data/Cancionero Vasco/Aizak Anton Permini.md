---
id: ab-1552
izenburua: Aizak Anton Permini
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001552.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001552.MID
youtube: null
---

Aizak, Anton, Permini
Ik esan al diok
Bertsuetan txaututzen
Ez auela iñork?
Beraz, ni ere ire
Mendeko natxiok?
Ixilla ezpanabil,
To, motell, begirok!
Nai dek aurren jarriak
Proba gaiten biok.