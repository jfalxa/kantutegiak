---
id: ab-2048
izenburua: Irule On Guti
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002048.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002048.MID
youtube: null
---

Irule on guti izaiten omen da
Goiz etzaten den andretan.
Eta don, pirurun,
Eta don, pirurun;
Irule gaixtoa, edale on.