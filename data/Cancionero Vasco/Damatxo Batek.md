---
id: ab-1835
izenburua: Damatxo Batek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001835.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001835.MID
youtube: null
---

Damatxo batek obligatu nau
Egia deklaratzeko;
Bertsotxo bida paratu biar tut
Biyotzen konsolatzeko;
Berriz ez dezan gogorik izan
Galaien enganatzeko,
Konbeni dela iduritzen zait
Eskarmentuen artzeko.