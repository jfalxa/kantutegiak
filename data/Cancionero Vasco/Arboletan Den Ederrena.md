---
id: ab-1633
izenburua: Arboletan Den Ederrena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001633.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001633.MID
youtube: null
---

Arboletan den ederrena da
Oyan beltzian bagua.
Itzak legunak dituzu baño
Bestetarako gogua.
Jaun zerukuak emain al dizu
Niganako amoriua.