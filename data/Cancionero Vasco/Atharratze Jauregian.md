---
id: ab-2105
izenburua: Atharratze Jauregian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002105.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002105.MID
youtube: null
---

Atharratze jauregian bi zitroin loratu;
Ongriako erregek batto du galdatu.
Arrapostu ukan du eztirela untu;
Untzen direnian, batto ukanen du.