---
id: ab-2748
izenburua: Orhiko Xoria Orhin (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002748.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002748.MID
youtube: null
---

Orhiko xoria Orhin
Bakean da bizitzen,
Bere larre-sasietan
Ez da hura unhatzen;
Han zen sorthu han handitu,
Han ari zen maithatzen,
Han bere umen artean
Gozoki du kantatzen.