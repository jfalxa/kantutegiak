---
id: ab-1814
izenburua: Botellan Eztiagu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001814.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001814.MID
youtube: null
---

Botellan eztiagu ardorik,
Berriz bete bearrik.
Guazen beraz berriz edanik,
Bai eta alegerarik,
Aguardientia saltzen den
Etxe untarik.

Allagatu giñenian portara
Lendabiziko ttan-tta-ra
Adiereztegatik norbat ba gara
Etxeko andria or eldu da
Al duen bezala,
Saia kotillon kordela
Eskuia ezkerrean duela
- Pasa zaiztia, yaunak, ixil, ixila...

- Agur etxeko andria,
Piztukuzuia argia;
Edan bear baitugu kutxut erdia.
- Erraten datzuet egia,
Konzienzia garbia,
Akabatu dudala
Nere barrika txikia
Xorta-xortaño bat ez bertze guzia.