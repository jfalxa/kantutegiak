---
id: ab-2693
izenburua: Sortuz Geroz Guziek (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002693.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002693.MID
youtube: null
---

Sortuz geroz guziek zor dugu hiltzia;
Deusik ez da gure munduko bizia.
Kita dezagun, kita, munduko auzia;
Garbiki etsamina gure kontzientzia.
Bertze munduan da gure justizia,
Azken sententzia zer oren tristia!
Eternitateko penaren luzia.