---
id: ab-1720
izenburua: Baduka Kurajerik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001720.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001720.MID
youtube: null
---

Baduka kurajerik, Gurutxet ezkerra?
Eramanen duguya dirurik etxerat? (dugia)
Baldin entregu bahiz airetik joiterat,
Eztiago ya utziren partida galtzera.