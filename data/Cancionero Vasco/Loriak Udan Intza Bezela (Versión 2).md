---
id: ab-2269
izenburua: Loriak Udan Intza Bezela (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002269.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002269.MID
youtube: null
---

Loriak udan intza bezela
Maite det dama gazte bat.
Arri ainbeste nai diotan nik
Ez da munduan beste bat.
Iñoiz edo bein pasatzen badet
Ikusi gabe aste bat,
Biotz guztitik banatutzen zait
Alako gauza triste bat.