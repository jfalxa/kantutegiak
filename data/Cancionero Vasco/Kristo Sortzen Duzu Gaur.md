---
id: ab-2652
izenburua: Kristo Sortzen Duzu Gaur
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002652.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002652.MID
youtube: null
---

Kristo sortzen duzu gaur: (Solo)
Alegra bite guziok gaur.
Kristo sortzen duzu gaur:
Dantza gitzen guziok gaur.
Natibitate gabean,
Ollarrak kantatu zuenian,
Ama-Semiak maiterik zaude,
Bi beso sainduen artean.

""Beus carum atuset"" (sic)
Maria beti birjiñik.
""Beus carum atuset""
Maria Birjiñaganik.

Kristo jina mundura (Solo)
Gure reskatatzera.
Disponi giten kristi onak
Jesusen adoratzera.

""Beus carum...

Maria tximuan edatzen,
Jose txatarran berotzen;
Orduxe artan ari emen ziren
Seme on baten trojatzen,
Seme on baten trojatzen eta
Jesukristoren bestitzen.

""Beus carum...

Beleneko portalea
Portale famatua,
Zoin Kristo baitago sortirik
Gure Salbatzalea
Eta munduaren Redentorea.

""Beus carun...

Bigarren bisitara
Iru erregek egin zuten.
Oro, inzenzu eta mirra
Guziek ofrezitu zuten.

""Beus carun...

Orienteko erregeak,
Izarrak giaturik.
Guziek ofrezitu zuten
Jaungoiko eta gizon eginik.

""Beus carum...

Natibitate gabean
Zeruak idiki zuten,
Etxe kontako etxeko-andrea
Kan sar baledi. Amen,

""Beus carun...