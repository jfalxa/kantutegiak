---
id: ab-3059
izenburua: Txipiritona
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003059.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003059.MID
youtube: null
---

Txipiritona,
Batearen gaitza
Bestearen ona.
Tton kirri kirri tton
Dingilindona
Tton kirri kirri tton
Ttikirriki tton
Ttikirriki tton tton.
Tton tton kirri kirri tton
Dingilindona.