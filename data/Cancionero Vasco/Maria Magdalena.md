---
id: ab-2729
izenburua: Maria Magdalena
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002729.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002729.MID
youtube: null
---

Maria Magdalena
Bere egunean,
Seda morez jantzia
Bere sonean,

Juan zan elizara
Zaldi urdinean;
Jarri zan belauniko
Pulpitu-pean.

San Juan Ebanjelistak
Meza ura ematen,
Kristo bera zeguen
Meza arri laguntzen.

Ama Santisima berriz
Meza ura entzuten,
Eta amabi Apostoluak
Koruan kantatzen.

Batzubek etxera eta
Bestiak maira;
Eta Maria Magdalena
Mairen azpira.

- Maria Magdalena,
Zer dezu orrela?
- Jauna, nere pekatuak
Jarri naute onela.

- Barkatzen dizkizut bada
Onari bezala;
Onari bezala eta
Zu Santa zerala.

- Aingerutxuak, artu zazute
Anima on au eskutik,
Eta eraman paraisora
Beste guztien ganetik.