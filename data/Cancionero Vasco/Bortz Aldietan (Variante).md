---
id: ab-1613
izenburua: Bortz Aldietan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001613.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001613.MID
youtube: null
---

Bortz aldietan egondu zara
Gustora nere onduan;
Ni baño ispillu ederragorik
Zuretzat etzen orduan;
Zer estaduan egoten giñen,
Iduki zazu goguan.