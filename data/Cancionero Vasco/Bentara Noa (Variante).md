---
id: ab-1758
izenburua: Bentara Noa (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001758.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001758.MID
youtube: null
---

Bentara noa, bentatik nator,
Bentan da nere gogoa:
Ango arrosa klabeliñetan
Artu det amorioa.

Zuk ederretan, nik galantetan
Zuk ederretan gogoa.
Ederretan den galantena
Joakina presentekoa.

Orrek amorez bear emen du
Bera den bezalakoa;
Bera ona da, ta obea luke
Franzisko Barberenekoa.