---
id: ab-1521
izenburua: Aingeru Batek Dio
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001521.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001521.MID
youtube: null
---

Aingeru batek dio goigora basuan,
Aurtxo bat jayo dela bart gure auzoan.
Ardua edan eta lo batez zentzatu, (zentzutu)
Aur uni biar diogu ederki kantatu.

Belengo portalian Jesus nuen ikusi:
Nere pena guziak joan ziren igesi.
Atozte, bai, artzayak, atozte nigana;
Ganaduak utzirik, goazen Belenera.