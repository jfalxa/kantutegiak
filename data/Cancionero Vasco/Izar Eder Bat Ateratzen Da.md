---
id: ab-2128
izenburua: Izar Eder Bat Ateratzen Da
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002128.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002128.MID
youtube: null
---

Izar eder bat ateratzen da
Urtian egun batian;
Urtian egun batian eta
Ura San Juan goizean.
Aren argian joan giñaden
Arantzazura bidian.
Birjiña Ama ta bere Semia
Topatu ditut bidian.