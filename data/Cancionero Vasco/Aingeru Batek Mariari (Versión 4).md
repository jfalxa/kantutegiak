---
id: ab-1527
izenburua: Aingeru Batek Mariari (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001527.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001527.MID
youtube: null
---

Aingeru batek Mariari
Dio graziez betea.
Jainkoaren Semeari
Emanen diozu sortzea.