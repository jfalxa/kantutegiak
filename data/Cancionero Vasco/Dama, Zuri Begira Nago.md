---
id: ab-1834
izenburua: Dama, Zuri Begira Nago
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001834.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001834.MID
youtube: null
---

Dama, zuri begira nago ni xoratzen;
Neria etzarela nik eztut pensatzen,
Asi ginen ezkero eia amoratzen.