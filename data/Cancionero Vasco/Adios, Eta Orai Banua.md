---
id: ab-1495
izenburua: Adios, Eta Orai Banua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001495.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001495.MID
youtube: null
---

Adios, eta orai banua,
Au despedida tristea!
Iltzen ezpanaiz, etorriko naiz.
Adios, nere maitea.