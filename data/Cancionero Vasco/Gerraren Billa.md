---
id: ab-1965
izenburua: Gerraren Billa
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001965.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001965.MID
youtube: null
---

Gerraren billa gau ta egun,
Etsaia arturik lagun.
Arrazoi ote degun,
Gero zer datorkigun.
Asitzerako pensatu
Fusila nola dantzatu.