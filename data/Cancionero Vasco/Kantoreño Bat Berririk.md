---
id: ab-2210
izenburua: Kantoreño Bat Berririk
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002210.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002210.MID
youtube: null
---

Kantoreño bat berririk
Apirilean emanik.
Dolore batek harturik nago,
Oi, bihotzaren erditik:
Urzo xuri bat galdurik
Ene begien bistatik.

Urzo xuri pollita,
So egidazu leiora.
Espresoki jina nuzu
Ene pena kondatzera.
Zu zira ene barbera,
Har nezazu sendatzera.

Sinets nezazu fedian,
Erraiten dauzutanian,
Zu baizikan berze maiterik
Nik ez dudala munduian.
Zer probetxu ordian
Ez dute nahi etxian?

Etxekuen kontra joaitia
Pena da, ene maitia.
Ez zinukeia obexago
Lehenik informatzia,
Urrundanik jakitia
Zer duten borondatia?

Jaunaren graziarekin
Orai badugu zer egin.
Bihotzetik erraiten dauzut
Pena haundi batekin:
Esposazeko zurekin
Xamur niz ene jendekin.

Amodio berria
Haunitz da tronpegarria;
Zuregatik utzi izan tut
Nik leheneko maitia.
Orai orotaz gabia,
Desiratzen dut hiltzia.