---
id: ab-2780
izenburua: Txamini Garbizale (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002780.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002780.MID
youtube: null
---

Txamini garbizale,
Mutur zikin duna,
Galza zar bizar aundi,
Judasen laguna.
Nolako salzalia,
Alako egostuna,
Neska zarrak egin du
Gustoko fortuna.

Elkarren kontrario,
Polbora ta sua;
Neskazarrengana
Beti erasoa.
Majoak esan ziran
Enbidiosoak;
Negarrez pasatu nun
Aste bat osoa.