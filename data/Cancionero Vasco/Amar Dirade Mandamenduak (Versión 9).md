---
id: ab-2298
izenburua: Amar Dirade Mandamenduak (Versión 9)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002298.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002298.MID
youtube: null
---

Amar dirade mandamenduak,
Jaungoikoaren legean.
Guardatutzera saia gaitezen
Guk gure egiñalean.
Bera bakarrik maite dezagun
Gauza guztien gañean.