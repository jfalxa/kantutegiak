---
id: ab-2277
izenburua: Gabillunian Irekiko'Re (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002277.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002277.MID
youtube: null
---

Gabillunian irekiko're
Ez dizut bada bortarik.
Barrenen dagoenak ez daki
Kanpokuaren berririk.
Atozkit biar egunaz eta
Etxian egoin bakarrik.