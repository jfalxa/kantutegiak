---
id: ab-1747
izenburua: Beirakiozu Eskutxoari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001747.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001747.MID
youtube: null
---

Beirakiozu eskutxoari,
Eskutxo txiki pollit horreri.