---
id: ab-1682
izenburua: Astian Bein Bein
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001682.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001682.MID
youtube: null
---

Astian bein, bein, gaubian dan, dan,
Jotzen ziraden atian.
Ortik emendik ezkondutzeko
Bazen deprendiantia.
Mutill ederra itxurazkua,
Eta zituen bost etxe.
Neronek ere lau milla duro
Dotian nuen neskatx.