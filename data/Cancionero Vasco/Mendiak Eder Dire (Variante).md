---
id: ab-2837
izenburua: Mendiak Eder Dire (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002837.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002837.MID
youtube: null
---

Mendiak eder dire beterik belarrez:
Ni er'hemen nabila hurturik xangrinez.
Faltarik ez dut egin nik neure erranez;
Holako'at pasatu da munduian arabez.