---
id: ab-1800
izenburua: Amodioetan Nintzen (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001800.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001800.MID
youtube: null
---

Amodioetan nintzen lili batekilan.
Hura ezin ikus plazer dudanian,
ez baitut etxian, ez eta herrian.