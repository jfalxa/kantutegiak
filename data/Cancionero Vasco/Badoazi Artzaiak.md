---
id: ab-1719
izenburua: Badoazi Artzaiak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001719.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001719.MID
youtube: null
---

Badoazi artzaiak.
Aren ipar-lañoak. (Arin)
Badoazi zein lenez
Bear dutela ikusi,
Egia den ala ez
Jesus sortu Belenen.

Belengo portalian
Josek Mariari:
- Ostaturikan niork,
Eman eztigu guri.
Estaltxo bat bakarrik
Eta segi niri,
Antxen deskansaturen zara
Gaixo-triste ori.

Belengo portalian
Jesus dut ikusi,
Nere pena guziak
Juan ziren igesi (iesi.