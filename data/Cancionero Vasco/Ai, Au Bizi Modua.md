---
id: ab-1517
izenburua: Ai, Au Bizi Modua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001517.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001517.MID
youtube: null
---

Ai, au bizi modua sortu da neretzat:
Oliorik ezin det porru-saldarentza.