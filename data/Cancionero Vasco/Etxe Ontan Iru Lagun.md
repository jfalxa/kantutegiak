---
id: ab-1926
izenburua: Etxe Ontan Iru Lagun
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001926.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001926.MID
youtube: null
---

Etxe ontan iru lagun,
Anai apeza Erramun;
Oiken bulla besterikan
Emen ezta ezagun,
"En-demas" atzo ta egun;
Zerbait esplika dezagun.