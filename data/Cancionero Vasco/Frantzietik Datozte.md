---
id: ab-2086
izenburua: Frantzietik Datozte
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002086.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002086.MID
youtube: null
---

Frantzietik datozte ofizialiak:
Txorroxkiliak, gero pertz-konpontzaliak,
Tutulumendi ero Santu saltzaliak;
Gizonik formalenak zikiratzaliak.