---
id: ab-3044
izenburua: Sagarraren Igarraren Ostoaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003044.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003044.MID
youtube: null
---

Sagarraren igarraren ostoaren
Puntaren puntan,
Txoriñoa zegoen kantari:
Ez, txiruliruli;
Bai, txiruliruli.
Nork dantzatuko du soñu ori ongi?

Abade jaunaren txerrie
Eper erreaz azie.
I akulatxo,
Ni akulatxo,
Eragion aur ori, Katalintxo.

Alangoxia naiz,
Olangoxia naiz,
Alangoxen,
Olangoxen,
Frailia naiz, eta dantzatu naiz,
Eta modurik ez, eta jakin ez,
Egin ezak buelta bat ""al reves"",
Beste bat ""errebes"".