---
id: ab-1707
izenburua: Haurra Eizu Lo Lo Lo (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001707.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001707.MID
youtube: null
---

Haurra, eizu, lo, lo, lo:
Emanen daitzut bi goxo,
Orain bat eta bestia gero.
Bai, bai, bai, bai, bai, bai,
Ez, ez, ez, ez, ez, ez;
Ikusi dutanian oi! zure sudurra,
Iruditu zitzaitan lukainka muturra.