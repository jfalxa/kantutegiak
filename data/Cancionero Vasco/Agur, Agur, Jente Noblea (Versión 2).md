---
id: ab-1511
izenburua: Agur, Agur, Jente Noblea (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001511.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001511.MID
youtube: null
---

Agur, agur, jente noblea,
berri on bat dakargu.
Atenziua eduki beza
iñork aditu nai badu,
Atenziua eduki beza
iñork aditu nai badu.