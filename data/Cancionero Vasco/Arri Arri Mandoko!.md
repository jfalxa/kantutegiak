---
id: ab-1669
izenburua: Arri Arri Mandoko!
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001669.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001669.MID
youtube: null
---

Arri, arri, mandoko!
Biar Iruñerako.
Handik zer ekharriko?
Zapat'eta gerriko.
Heiek norendako?
Migeltxorendako.
La, la, la, la, ra, l.