---
id: ab-1651
izenburua: Arotxarekin Hernanin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001651.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001651.MID
youtube: null
---

Arotxarekin Hernanin
Goka bidean eginin.
Atze arrian gizonai
Gizalegez itz egin;
Ez desapiorik egin,
Galduak ematen dik min.