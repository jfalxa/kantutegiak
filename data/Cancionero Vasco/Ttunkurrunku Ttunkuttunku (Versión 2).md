---
id: ab-2647
izenburua: Ttunkurrunku Ttunkuttunku (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002647.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002647.MID
youtube: null
---

TTunkurrunku, ttunkuttunku, ttunkurrunkuttuna;
TTunkurrunkuttun, kuttun, kuttun, kurrun, kuttuna:
Lo, lo, ttun, ttun, kurrun, kuttuna.