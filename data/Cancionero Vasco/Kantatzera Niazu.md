---
id: ab-2202
izenburua: Kantatzera Niazu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002202.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002202.MID
youtube: null
---

Kantatzera niazu alegera gabe,
Ez baitut probetxurik tristaturik ere.
Nihon deusik ebatsi, gizonik il gabe,
Sekulako galerak enetako dire.

Kantu hok egin ditut Paueko hirian,
Burdinaz kargaturik oi! presondegian.
Kopia ere igorri denbora berian,
Othoi, kanta ditzazten oi! gure herrian.

Kantu hok eman ditut ez xangrinatzeko,
Adixkide guziek kuraye hartzeko;
Eta partikularzki, aita, zuretako,
Kantu hok aitzian nitaz orhoitzeko. (aditzian)

Badut obligazionea aita eta arrebari,
Bai eta gauza bera oseba jaun horri.
Jaun hori adresatu da herriko merari
Bai eta eginarazi dihurutan zer nahi.

Aita aitzineko, arreba ondoko,
Oseba jaun hori dihuru fornitzeko.
Ez al ere enetako bi sei-liberako (halere)
Galeretan bederen leher egiteko.

Zutaz estonatzen niz, oi! aita zilharra,
Ardura dudalarik begian nigarra.
Munduaren bistako, oi! zer aingerua...
Bainan barnian dituzu bi mila debruak.

Azken bertsoa huna amarendako. (anaiarendako)
Arrazoin bat badiat hiri erraiteko:
Untsa goberna ari, etzauk dolutuko; (hadi)
Ni baitan esenplua ez duk gaitz hartzeko.