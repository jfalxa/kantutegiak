---
id: ab-2224
izenburua: Kikirriki Ta Katxola
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002224.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002224.MID
youtube: null
---

Kikirriki ta katxola,
Apizak dire Bayonan;
Aita Santua Erroman.
Aik an diran bitartian,
Guziok konsola gaitian.