---
id: ab-3361
izenburua: Neskazarrak Joaten Dira
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003361.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003361.MID
youtube: null
---

Neskazarrak joaten dira
Madalenara,
Madalenara,
Santuari eskatzera
Senar on bana,
Konbeni bada.
Santuak erantzuten diote
Ñar, ñar.
No valéis nada,
No valéis nada.