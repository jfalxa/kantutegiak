---
id: ab-3011
izenburua: Soka-Dantza
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003011.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003011.MID
youtube: null
---

Agur, Bettiri! )
Ongi ethorri! nolaxe zira? )bis
Bizi zira ere oraino? )
- Bai, bizi naiz,
Eta biziko're oraino,
Hartzekoak kobratu arteraino.
La la la...

- Sei, zazpi, zortzi )
Urtheren bizi )bis
Baduzu orduan oraino! )
Bai, Jainkoak
Konserba zaitzala
Ene zorrak bil arteraino!
La la la...