---
id: ab-2750
izenburua: Otxarabian Zer Dute
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002750.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002750.MID
youtube: null
---

Otxarabian zer dute,
Zubi azpian ur alde?
Mari Prantxiska hara joaiten da
Ardura ardura ur eske.
Trai, rai, rai,...

Mari Prantxiskak arropa frango,
Bainan bat ere onik ez.
Kolonel horrek eginen dazko
Gorostiaren axalez.
Trai, rai, rai,...

Gorostiaren axala
Ezta bertze bat bezala.
Mari Prantxikak jauntziren ditu
Berak nahi duen bezala.
Trai, rai, rai,...