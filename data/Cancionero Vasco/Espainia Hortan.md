---
id: ab-1922
izenburua: Espainia Hortan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001922.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001922.MID
youtube: null
---

Espainia hortan ote laiteke
Etxe-mutil beharrik?
Andanaño bat munta gintaizke
Baigorri alde huntarik.
Bonaparteren serbitzulako
Guk ez diagu gogorik.

Baigorriko mutiko gaztek
Egin dugu konzertu
Soldado juanik baino
Obe dugula ezkondu.
Emaztegaiak txerkatu gabe
Ordenantza jin zaiku.

Aurten ezkondu nahi duen
Baigorrira jin bedi;
Nahiz ez izan hamabost urte
Premua izan bedi.
Emaztegaiak ukanen ditu
Behar baitu ere hamabi.

Adios aita, adios ama,
Adios ene maitia:
Noiz izanen da berriz
Ene etxerat itzultzia?
Xantza baduzu, ezazula uts,
Ezkon zite, maitia.