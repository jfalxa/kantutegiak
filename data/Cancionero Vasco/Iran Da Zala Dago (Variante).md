---
id: ab-2122
izenburua: Iran Da Zala Dago (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002122.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002122.MID
youtube: null
---

Iran da zala dago
Irago onetan,
Bertsuak para ditzagun
Dejada onetan.
Ni asi naizen ezkero
Bear ditut esan
Nola paratzen diran
Mandamentuetan.