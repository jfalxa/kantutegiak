---
id: ab-2355
izenburua: Nagusi Zarra Oatzian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002355.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002355.MID
youtube: null
---

Nagusi zarra oatzian,
Mutilla berriz lo,
Aisa eraman tuzte
Bê etxetik bi mando.

Eramanagatikan
Mando bat edo bi,
Nagoen posturetik
Ez naiteke mugi.