---
id: ab-2250
izenburua: Lauda Zagun Misterio Aundia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002250.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002250.MID
youtube: null
---

Lauda zagun misterio haundia;
Jainko Jauna hostian estalia.
Zuek, Aingeruek, zeruko gortea,
Adora zazue gure Erregea, gure Erregea.