---
id: ab-2265
izenburua: Logiro Naiz Ta Logale (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002265.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002265.MID
youtube: null
---

Logiro naiz ta logale,
Argi-ollarrak jo gabe.
Bart arratsian Alzumeko atian
Gaztiak etziren logale.