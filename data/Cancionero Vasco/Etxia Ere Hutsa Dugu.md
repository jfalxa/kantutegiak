---
id: ab-1928
izenburua: Etxia Ere Hutsa Dugu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001928.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001928.MID
youtube: null
---

Etxia ere hutsa dugu ta
Bederatzi, bai, lagunak.
Artoarentzat ez dire aski
Guk irabazten tugunak.
Ez ditu gaizki aberasten
Errotazain frikuna.