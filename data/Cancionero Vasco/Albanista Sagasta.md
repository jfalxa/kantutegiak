---
id: ab-1567
izenburua: Albanista Sagasta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001567.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001567.MID
youtube: null
---

Albanista Sagasta, bost motikorekin,
Juan dire gogorgora (bis), bost motikorekin,
Gau ona pasatzera (bis) denak alkarrekin.
Rafael ta Jose Migel adiskidiakin,
Gau ona pasatzera (bis), denak alkarrekin.