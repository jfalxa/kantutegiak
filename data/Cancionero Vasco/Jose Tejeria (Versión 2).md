---
id: ab-2171
izenburua: Jose Tejeria (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002171.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002171.MID
youtube: null
---

Jose Tejeria da
Au nere grazia,
Gipuzkoako Aian
Jayo ta azia.
Deabruak animoa
Eman biotzera:
Tiroz atrebitu naz
Nere aita iltzer.