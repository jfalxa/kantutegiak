---
id: ab-1919
izenburua: Eskualdun Fededuna
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001919.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001919.MID
youtube: null
---

Eskualdun fededuna,
Zahar eta gazte,
Amaren laudatzera
Lehia zaitezte.