---
id: ab-3078
izenburua: Astelena Dut Jai Bat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003078.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003078.MID
youtube: null
---

Astelena dut jai bat artuba
Bekatu mortalekotzat.
Asteartian lan egiteko
Gogorik etortzen etzait.
Asteazkena, au nere pena!
Gero pereza jartzen zait.