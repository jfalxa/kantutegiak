---
id: ab-1532
izenburua: Barber Gaztia (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001532.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001532.MID
youtube: null
---

Barber gaztia, aizazu
Itz erdiño bat esazu;
Erremediorik senti badezu,
Neri bertatik esazu.
Eritasun bat gogorra daukat,
Laster finituko nauzu.