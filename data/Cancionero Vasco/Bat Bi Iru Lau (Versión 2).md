---
id: ab-1662
izenburua: Bat Bi Iru Lau (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001662.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001662.MID
youtube: null
---

Bat, bi, iru, lau,
Ezkontzen da mundu au.
Arratoina fraile,
Kukua meza emaile.
Nik pika, nik pika
Astuaren birika.