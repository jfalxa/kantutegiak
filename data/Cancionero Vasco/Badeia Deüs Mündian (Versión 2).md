---
id: ab-2148
izenburua: Badeia Deüs Mündian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002148.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002148.MID
youtube: null
---

Badeia deüs mündian
Hain süsprenigarririk,
Hain miseria handirik?
Jüif Errant miserablia,
Denbora bethi phenatürik,
Bera dela kausa da
Hola turmentatürik:
Mündian eztü phausürik.

Gizon harren abitia,
Gaizki konpositia:
Ezagün estranjer dela.
Tablier txar bat aitzinian,
Oskia-egilek bezala.
Bethi erre badabilala,
Phausatü gabe sekülan:
Oi pünitione garratza!

- Bai, Isaak Lakeden
Düzü ene izena;
Jerüsalemen sorthia,
Herri hanitx aiphatia,
Jesüs Jaunak haitatia.
Eta ni, hanko semia,
Bethi malerus nizana,
Mündian paregabia!

- Gizon zahar hunesta,
Sar tzite ostatiala,
Guri plazer egitera.
Ardu dügüno botillan,
Ükhenen düzü edatera;
Hura akaba eta,
Dateke berritzia,
Plazer badüzü zük hala.

- Hartüren dit edatera
Bi kolpü ziekila.
Oi, parka nabastaria.
Ikhusirik borontatia,
Zier nula arrafüsa?
Ezin jartzen niz ziekila:
Hau da düdan doloria
Edo konfüsionia.

- Erragüzü adina:
Desir günüke jakitia,
Plazer badüzü zük hala.
Hanitx zahar zirela
Üdüri zünüke minala.
Ehün urthe badüzüla,
Hori düda gabe da,
Hura phürü gütiena.

- Handi dizüt adina:
Hama-zortzi ehün eta
Iganarik hamar eta biga.
Urthek jenatzen naiela,
Horri düda gabe da.
Handi dira ene phenak