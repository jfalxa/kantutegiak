---
id: ab-3233
izenburua: Iru Txita Izan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003233.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003233.MID
youtube: null
---

Iru txita izan eta
Lau galdu:
Gure txitaren ama
Zerek yan du?

Axeriek yan diola
Lepua,
Biligarruak bertze
Gañerakua.