---
id: ab-1885
izenburua: Ene Auzotegian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001885.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001885.MID
youtube: null
---

Ene auzotegian bi senar-emazte
Bakian bizi dira aserretu arte.
Guardia, elkarrekin behin samurtzetik,
Heien xuxentzaileak lan ona behar dik.