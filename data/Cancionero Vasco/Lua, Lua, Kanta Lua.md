---
id: ab-2804
izenburua: Lua, Lua, Kanta Lua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002804.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002804.MID
youtube: null
---

Lua, lua, kanta lua,
Zerutako Jaungoikoa,
Aur uneri emaziozu
Lau orduko lua.