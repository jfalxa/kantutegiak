---
id: ab-1795
izenburua: Birjiñ'Ama Zure Alabantzak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001795.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001795.MID
youtube: null
---

Birjiñ'Ama, zure alabantzak
Astera nua kantatzen;
Desio batez nere bihotzak
Lan onetara nau deitzen.
Bihar bezala adierazteko,
Beldur naiz kapaz ez naizen.
Zerorrek zerutik lagun didazu,
Entendimentuz argitzen.