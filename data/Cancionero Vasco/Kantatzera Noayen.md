---
id: ab-2206
izenburua: Kantatzera Noayen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002206.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002206.MID
youtube: null
---

Kantatzera noayen Karmengo lorea.
Jesusen Ama zu zera, Birjiña gurea,
Pekadore guztiok inzensadorea.