---
id: ab-1799
izenburua: Bizi Naiz Munduan (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001799.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001799.MID
youtube: null
---

Bizi naiz munduan juan dan aspaldian,
Beti tristezian, penak bihotzian,
Ardura dudalarik nigarra begian,
Maitia ezin ikus plazer dutanian,
Ez baitut etxian, ez eta herrian.

Airian banindadi, egiala (?) bezala,
Laister juan nindaike maitiaren gana,
Neure pena-xangrinez hari erraitera,
Zer dutan dolorez han deklaratzera,
Haren aita-amekin solas egitera.

Izan naiz Barkoxen kusinaren etxen,
Amonakin jaten, segur ona baitzen:
Kosteletak errerik, birikak friuturik,
Ogia muflerik, arnoa gozorik.
Han etzen eskasik, maitia, zu baizik.