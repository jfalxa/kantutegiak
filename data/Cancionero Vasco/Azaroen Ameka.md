---
id: ab-1714
izenburua: Azaroen Ameka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001714.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001714.MID
youtube: null
---

Azaroen ameka, San Martin eguna;
Auxen da okasiua izandu duguna,
Nonbait guretzat zagon guretzat fortuna,
Gartzelan pasatzeko zenbait egun ona.