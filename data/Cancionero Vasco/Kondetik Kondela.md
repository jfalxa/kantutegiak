---
id: ab-2232
izenburua: Kondetik Kondela
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002232.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002232.MID
youtube: null
---

Kondetik kondela, batxakaran beltxa
Ondoaren min dela arana;
Franziska orren miste soberana,
Mirua puntean deraman.
Pedro Pedronekua jau! jau!
Uraxe bera deraman.