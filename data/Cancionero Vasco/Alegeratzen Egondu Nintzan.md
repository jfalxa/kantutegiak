---
id: ab-1570
izenburua: Alegeratzen Egondu Nintzan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001570.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001570.MID
youtube: null
---

Alegeratzen egondu nintzan
Donijuanen gau batez:
Nola Prantzian ala Espainian
Parerik etzuen aldartez
Bertsu berrien parejatzeko
Español mandazain batek.