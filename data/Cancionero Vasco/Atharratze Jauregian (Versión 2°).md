---
id: ab-1538
izenburua: Atharratze Jauregian (Versión 2°)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001538.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001538.MID
youtube: null
---

Atharratze jauregian
Bi meloin doratu.
Hongriako erregek
Batño du galdatu.
Arrapostua izan du
Ez direla ontu;
Ontutzen direnian,
Batño izanen du.