---
id: ab-2162
izenburua: Otxailaren Laugarrena Da (Versión 9)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002162.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002162.MID
youtube: null
---

Otxailaren
Laugarrena da
Santa Agedaren
Bespera.