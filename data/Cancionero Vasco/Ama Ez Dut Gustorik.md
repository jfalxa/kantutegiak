---
id: ab-1589
izenburua: Ama Ez Dut Gustorik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001589.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001589.MID
youtube: null
---

Ama, ez dut gustorik nik diru hutsian;
Disposiziua ere on da gorputzian.
Galtzak bai, ta ez bada gizonik etxian,
Bizi modua dago estadu motxian.