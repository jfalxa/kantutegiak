---
id: ab-1519
izenburua: Ai, Au Kalian Luze Ta Zabal
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001519.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001519.MID
youtube: null
---

Ai, au kalian luze ta zabal
Etxarriko kalian!
Emendi pasa bear dian
Nobio jauna ta bere andrian.