---
id: ab-2136
izenburua: Jaunak, Begira Zute
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002136.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002136.MID
youtube: null
---

Jaunak, begira zute ni nabillen plantan.
Diruak ezin izan nere bizi santan.
Egunan txanpon bat aldaratu faltan,
Nere denboran juan da geroko esperantzan.