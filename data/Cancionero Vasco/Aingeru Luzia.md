---
id: ab-2841
izenburua: Aingeru Luzia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002841.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002841.MID
youtube: null
---

Aingeru luzia, zeruan musikia,
Andra Kosepa tronpeta jotzen.
Kristoren erreberentzijan,
Jarri belauniko, agora Kesukristo.
Gure soluan nor dabill? Asaburuak loratzen?
Aittatxu, amatxu,
Urra, urra, pitxitxu.