---
id: ab-2046
izenburua: Iru Oyarzuarrek
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002046.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002046.MID
youtube: null
---

Iru Oyarzuarrek, Prantziako lauri,
Desafiyo indiyo, nai badute ari. (egin dio)
Laja derrebotiai, eldu luziari:
Plazan ageriko da zein dan pelotari.