---
id: ab-2249
izenburua: Larre Berrian (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002249.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002249.MID
youtube: null
---

Larre berrian eperrak aide,
Pagodietan usoak.

Neska gazte bat airosoa da
Juanita presentekoa.

Orrek galai bat biar omen du
Bera dan bezelakoa.

Bera ona da, ta obia luke
Bizente aldamenekoa.