---
id: ab-1766
izenburua: Bertso Berri Paratzen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001766.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001766.MID
youtube: null
---

Bertso berri paratzen orain naiz abian
Publikatu daitezin Arrayozko errian.
Parrokuen konsejuak artu memorian,
Jaingoikoaren legia aantzi ez dedian,
Gero goza gaitezin zeruko glorian.