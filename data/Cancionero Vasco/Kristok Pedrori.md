---
id: ab-2242
izenburua: Kristok Pedrori
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002242.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002242.MID
youtube: null
---

Kristok Pedrori galdetzen dio:
- Norekin zaude izketan?
- Jauna, anima bat emen dago
Sartu nai duela zeruan.
- Ezta lekurik, ezta lekurik:
Doala infernura ortik(an).