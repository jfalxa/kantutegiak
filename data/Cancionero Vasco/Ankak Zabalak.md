---
id: ab-1620
izenburua: Ankak Zabalak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001620.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001620.MID
youtube: null
---

Ankak zabalak eta begiyak gorriya.
Zacha baña bazute nigana torriya.
Ez eder ta ez galant kadara gariak
Ez e a a ren (zen) ere gustua neurriyak.