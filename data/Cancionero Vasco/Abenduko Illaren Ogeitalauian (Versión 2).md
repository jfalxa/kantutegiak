---
id: ab-1484
izenburua: Abenduko Illaren Ogeitalauian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001484.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001484.MID
youtube: null
---

Abenduko illaren ogeitalauian
Jesus Salbadorea jayo da gaubean.