---
id: ab-1701
izenburua: Au Gabaren Zoragarria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001701.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001701.MID
youtube: null
---

Au gabaren zoragarria!
Jesus jayo da Belenen.
Bisitatzera etorri gera,
Etxia onetan ote den.