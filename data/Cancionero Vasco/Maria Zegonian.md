---
id: ab-2303
izenburua: Maria Zegonian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002303.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002303.MID
youtube: null
---

Maria zegonian
Buruan miñekin,
Pruentxo juaten zaio
Txokolatiakin.
Txokolatia ere
Bere estofakin.
Ederki konpontzen ziran
Biok elkarrekin.