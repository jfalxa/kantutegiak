---
id: ab-2341
izenburua: Mille Zortzi Egunean
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002341.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002341.MID
youtube: null
---

Mille zortzi egunean lau ogei ta amarrean,
Kalifurnietako mendigoienean,
Artalde bat neraukan nik ene engurian,
Kantu onen egiten asi nitzenean,
Rai, rai, rai, rai, rai, rai, rai, asi nitzenean.