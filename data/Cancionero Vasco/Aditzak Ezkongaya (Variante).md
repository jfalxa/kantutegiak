---
id: ab-1498
izenburua: Aditzak Ezkongaya (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001498.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001498.MID
youtube: null
---

Aditzak, ezkongaya, solas bi nigandik;
Konseju on hau hartzak ezkonduetarik.
Gizonak, izaitekoz iñoiz deskantsurik,
Gazterik familia firmatu behar dik,
Zahartuta ez dedin egon doluturik.