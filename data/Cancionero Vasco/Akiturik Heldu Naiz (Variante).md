---
id: ab-1636
izenburua: Akiturik Heldu Naiz (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001636.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001636.MID
youtube: null
---

Akiturik heldu naiz bide luze huntan,
Urrats pausu guziez beti gogoetan.
Atorra bustia dut bulhar errainetan:
Idorturen nezake zure besuetan.