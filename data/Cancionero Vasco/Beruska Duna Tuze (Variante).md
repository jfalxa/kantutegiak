---
id: ab-2778
izenburua: Beruska Duna Tuze (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002778.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002778.MID
youtube: null
---

Nativitate gabea
Olaran katatu zenea,
Ama Semea maiteri zauden
Bibeso zanduen artean.

Coro: Beruska duna tuze Maria beti Berjini.
Beruska duna tuze Maria Berjinen gani.

Kristo zin da mundura
Gure reskatatzera;
Disponigiten, kristiena,
Jesus ere adoratzera.

Coro: Beruska...

Etatekontan bai ardo,
Guk etxakin zomana;
Dugun, dugun txorta bat
Txakiteko zomana.

Coro: Beruska...

Nativitate gabea
Olaran kantatu zenea
Zero akizi kizuten
Zerean salbare ere. Amen.