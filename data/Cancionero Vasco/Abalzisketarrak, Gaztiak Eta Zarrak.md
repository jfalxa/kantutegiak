---
id: ab-2062
izenburua: Abalzisketarrak, Gaztiak Eta Zarrak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002062.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002062.MID
youtube: null
---

Abalzisketarrak,
Gaztiak eta zarrak,
Beren erriko plazan
Nola dabiltzen dantzan!
Etxeaz etxe mantena laizke
Gizonak eta mutillek
Ustutzen txute botillek.