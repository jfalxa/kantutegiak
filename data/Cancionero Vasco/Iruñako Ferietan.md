---
id: ab-2049
izenburua: Iruñako Ferietan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002049.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002049.MID
youtube: null
---

Iruñako ferietan,
Iragan Sanferminetan,
Ehun zaldi arribatu
Andaluziatik tropan;
Merkatu eder bat zautan
Zirelarik bi lerrotan.