---
id: ab-3248
izenburua: Txurrun Txanketan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003248.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003248.MID
youtube: null
---

Txurrun txanketan,
Min dut anketan,
Sendatuko naiz
Sanfermiñetan.