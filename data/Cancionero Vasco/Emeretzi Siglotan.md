---
id: ab-1884
izenburua: Emeretzi Siglotan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001884.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001884.MID
youtube: null
---

Emeretzi siglotan ezta falta anitz, (eztu)
Iru dozena urte, tronpatzen ezpaniz.
Suyeten publikatzen orai asia niz,
Oroitzen eztirenak adbertitu nahiz,
Xefen obeditzera obligatu bainiz.