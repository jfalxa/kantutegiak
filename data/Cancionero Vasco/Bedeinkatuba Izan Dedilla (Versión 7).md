---
id: ab-2160
izenburua: Bedeinkatuba Izan Dedilla (Versión 7)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002160.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002160.MID
youtube: null
---

Bedeinkatuba izan dedilla
Etxe ontako jendia!
Pobreza ta umil dabillenentzat
Badute borondatia.
Santa Martiri maitia
Dago errukiz betia.
Bera alkanzatu degula
Osasuna ta bakia.

Libertaria eskatzen diot
Etxeko prinzipalari.

Kantatutzera nua ni.
Grazia degula ipiñi
Santa bedeinkatuba (oni)
Kriste debilla bere izena
Beti santifika bedi.

Nork esango du zer pasatu zan
Munduban Santa Ageda,
Lendabiziko azotatuta
Kendu zioten bularra.
Instante batian estaldu zion
Jentillen biotz gogorra,
Gorputz guzia odolaren
Erida gorputzekua.

Zenbat erainan ote zan
Santa Ageda bada,
Ortikan konsideratu
Santa onen biotza zer zan.

Santa ona preso artuta zanian
Zer milagro bat aundia,
Zerubetatik jaitxi zizayon
Jaungoikoaren argia.
Milagro ikaragarria

San Pedro berak kuratu zun
Santa onen gorputz guziya.