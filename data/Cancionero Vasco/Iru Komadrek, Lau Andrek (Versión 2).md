---
id: ab-2044
izenburua: Iru Komadrek, Lau Andrek (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002044.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002044.MID
youtube: null
---

Iru komadrek, lau andrek,
Afari on bat zuten bart:
Iru ollo, bi tortollo, eper bat;
Etziren gosiak etzin bart.