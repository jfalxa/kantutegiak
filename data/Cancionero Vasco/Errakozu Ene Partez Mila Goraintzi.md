---
id: ab-1900
izenburua: Errakozu Ene Partez Mila Goraintzi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001900.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001900.MID
youtube: null
---

Errakozu ene partez mila goraintzi,
Arroki, zure adixkide muxu Barnetxi,
Sarakoari.
Ongi pena dudala segurki,
Hura bera baizik ez dudala utzi
Satisfatu gabe ederki.
Gauza zer den, berak badaki.

Neskatila gaztiekin dela bizi,
Libertituz ederki.
Orainon ez da libertitu aski,
Ez da kitaturen naski.

Haren begiak hain dire argi,
Izarrak iduri;
Eta koloriak ehun ostotako
Arrosa bera bezain gorri.
Neskatila gaztien ilusigarri,
Karesatzen ditu ederki:
Bere denboraz balia bedi.