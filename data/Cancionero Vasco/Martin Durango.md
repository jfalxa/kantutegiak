---
id: ab-2308
izenburua: Martin Durango
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002308.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002308.MID
youtube: null
---

Martin Durango dunbalarrekin,
Kuidado, jaunak, orrekin:
Tiro galantak botatzen ditu
Ipurdi oker orrekin.