---
id: ab-2372
izenburua: Nere Kuttun Laztan Maitea (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002372.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002372.MID
youtube: null
---

Nere kuttun laztan maitea,
Ona zuretzat gozua,
Zopatxo mami-saldan bustita,
Eztia ere badauka.