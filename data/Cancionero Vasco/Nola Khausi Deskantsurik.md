---
id: ab-2394
izenburua: Nola Khausi Deskantsurik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002394.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002394.MID
youtube: null
---

Nola khausi deskantsurik
Arima hobendun batean?
Jauna atsegin guzietan,
Zu gabe ez da gozorik.
Ah! munduaz lilluratua,
Utzi zaitut, Aita ona,
Ustez khausi zoriona,
Zorigaitzez naiz lehertua. (bis)

Hire gaztetasun ederra,
Erraiten zarotan munduak,
Zoroki penetan deramak
Gozatu gabe plazera.
Munduak, helás! arantzeak
Zituen arrosa pean.
Jauna! arantzen azpian,
Zuk ditutzu eman loreak. (bis)

Mundu zoro gaixtoarentzat,
Oi dohakabea naizena!
Egin ditudanak, o Jauna,
Egin banitu zuretzat!
Bethi griñaz tormentatua
Plazer galkorren ondotik
Ibili naiz gazte danik!
Orai ongi naiz punitua! (bis)

Bertute maithagarria,
Uztarri hain gozoa dena,
Plazera berekin duena,
Nork daut niri itzuliko?
Urrundu gabe krimetatik
Uste nuen ongi izan!
Ez dela, ahantzirikan,
Inpioarentzat bakerik. (bis)

Ene krimen leze zolatik
Oi! Jauna, oihuz hari nautzu,
Ene auhenak entzun zatzu,
Ume bat urrikaldurik!
Haur eskergabe bat naizela
Aitor dautzut, Aita ona!
Ahantziz ene hobena,
Orhoit zaite aita zarela! (bis)

Zu ezagutu mementotik
Egin dautzut laido bat beltza!
Xahutu dut ene primantza,
Zure ganik urrundurik!
Seme bezala barkhamendu
Nola galda, zuri Aita?
Ah! zerbitzari bezala
Othoi errezebi nezazu. (bis)

Bet betan, hitz hauk tut aditu:
Alegera bedi zerua!
Ene haurra nuen galdua!
Huna non dudan khausitu!
Amodioaz sustaturik
Huna, heldu Aita ona!
Lehiatzen da ni ganat
Bere besoak zabaldurik. (bis)"