---
id: ab-2082
izenburua: Ez Bada Maria
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002082.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002082.MID
youtube: null
---

Ez bada, Maria,
Ez guretaz aztu,
Orain gero ta beti
Anpara gaitzazu.