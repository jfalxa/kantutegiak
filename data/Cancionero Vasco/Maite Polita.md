---
id: ab-2283
izenburua: Maite Polita
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002283.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002283.MID
youtube: null
---

Maite polita, zuganik
Zerbaitto jakin nahi nuen nik:
Zerk zerauzkan horren tristerik,
Zure so ederrak hola galdurik?
Ala baduzu beldurkunderik
Maitatzen dudan zutaz bertzerik.