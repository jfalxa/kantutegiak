---
id: ab-2313
izenburua: Matsaren Zainetik Dator (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002313.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002313.MID
youtube: null
---

Matsaren zainetik dator zumoaren gozua.
Ogei kuartuan dago txopin bat osua.
Zuk neri, nik zuri, a(g)ur egiñaz alkarri,
Basua txit garbi bear degu jarri.