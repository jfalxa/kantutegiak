---
id: ab-2078
izenburua: Espiritu Santuaren
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002078.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002078.MID
youtube: null
---

Espiritu Santuaren egunak dire,
Pazkuak mayatzekuak.
Misioaren Santo Kristori
Paratzen dazkiot bersoak;
Ez ditut damu nik arengana
Egin ditauten pasoak. (ditudan.