---
id: ab-1933
izenburua: Ez Badek Errentarik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001933.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001933.MID
youtube: null
---

Ez badek errentarik kantuzko lanetan,
Ezin manten aiteke erregaloetan.
Konbidatuagatik noizik bein ardotan,
Karga-labore gutxi sartzen dek errotan.
Ari nazak ontan ortik zer da ukan:
Urre gutxi boltsan sartzen bertsoetan.
Ezin manten aiteke erregaloetan,
Ezin manten aiteke erregaloetan.