---
id: ab-2468
izenburua: San Sebastian Erri Polita
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002468.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002468.MID
youtube: null
---

San Sebastian, erri polita,
Plazia duka zabala.
Alarguntxuak ortxe baditu
Iru edo lau alaba.