---
id: ab-2029
izenburua: Ihiztari Abilik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002029.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002029.MID
youtube: null
---

Ihiztari abilik badugu munduian,
Eta partikulazki Baygorri herrian.
Urzo gaixoa dute atzeman mendian,
Sariak edatuak zeuden aspaldian.

Urzo xuri pollita, xarmegarria zen,
Haren parerik etzen munduan ikusten;
Birjina bat iduri bethi elizan zen;
Kolperik bazuela nork zuen pensatzen!

Urzoaren bizia elizan zen dena,
Non nahi othoizian ahuspez emana.
Hura zaukaten orok saindu handiena,
Galdu izan ez balu hegalpetik luma.

Iragan egunetan segereturekin
Landa xokotan zauden bere artzainekin;
Artzainak joan eta, ez jakin zer egin.
Halere urus dire bildotzik ez den jin.