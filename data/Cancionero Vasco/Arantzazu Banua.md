---
id: ab-1631
izenburua: Arantzazu Banua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001631.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001631.MID
youtube: null
---

Arantzazu banua, ez daukat dirurik;
Azpiko gona motza salduko nuke nik.
Arantzazu bideko faguaren onduan,
San Mikel aingerua nukian gogua.