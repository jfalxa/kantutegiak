---
id: ab-1520
izenburua: Ai Neure Jesus
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001520.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001520.MID
youtube: null
---

Ai, neure Jesus, ai neure Jabe,
Ai, neure Aita gozua!
Urratu dezu infernukoak
Ezarri zizun lazua.