---
id: ab-2710
izenburua: Arsak Arsak Mutill
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002710.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002710.MID
youtube: null
---

Arsak, arsak, mutill pitxerdi ardu
Guasan polliki polliki etxera.
Gabon gaba zelebratzera
Aitaren da amaren onduan.
Ikusiko dek ama barrez da
Aita ere bai txit kontentuz.

Bai ta nik ere selango tragua
Lelengo esanda Jesus,
Lelengo esanda Jesus.

Bixigu erriaren azurra
Ondo txupatuko degu.

Eragixok, mutill,
Eragixok, mutill
Aurreko danboliñ orri
Gaztaiñak erre artian
Gaztaiñak erre artian
Tirrin-tarran,
Tirrin-tarran,
Plin plaun plust.
Pasau deigun gabon-gaba kontentus,
Pasau deigun gabon-gaba kontentus.