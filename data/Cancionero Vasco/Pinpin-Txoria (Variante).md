---
id: ab-2680
izenburua: Pinpin-Txoria (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002680.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002680.MID
youtube: null
---

Pinpin-txoria,
Txori papo-gorria.
Jan, jan, koñata:
Txori-saltsa on bat da.