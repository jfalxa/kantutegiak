---
id: ab-2694
izenburua: Sukaldian Zoin Eder Den (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002694.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002694.MID
youtube: null
---

Sukaldian zoin eder den
Suburdina letoinez!
Xarmegarria, erran datazu:
Maite nauzu, bai edo ez?
- Baietz ere ezin erran,
Etxekuen beldurrez;
Ezetz ere ezin erran,
Zure begien ederrez. (bis)

- Juan zait lua, galdu dut jana,
Ez dut errepeusurik.
Har bat badut bihotzian
Aztaparren loturik,
Har bat badut bihotzian
Aztaparren loturik.
Hura handik ezin kendu,
Maite bihotzeko, zuk baizik.(bis.