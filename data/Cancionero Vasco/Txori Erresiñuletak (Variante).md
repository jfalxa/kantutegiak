---
id: ab-2608
izenburua: Txori Erresiñuletak (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002608.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002608.MID
youtube: null
---

Txori errexiñuletak ederki Kantatzen;
Aditzaliak oro ditu xarmatzen.
Bard'arratsian, sasi batian,
Biga baziren:
Eder ziren, xarmant ziren,
Manera oroz,
Bai eta elgarrez ongi agrados.