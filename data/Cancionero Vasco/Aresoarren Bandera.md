---
id: ab-3082
izenburua: Aresoarren Bandera
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003082.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003082.MID
youtube: null
---

Aresoarren bandera,
Ai, nolakua ote da?
Errekaldera eraman dute
Erakustera.
Gero leitzarrak bidera
Norabuenak ematera.
Altzuarenak juan ziraden
Iragitera.

Ogei t'amar ziren oiek,
Ustez etzeuden bildurrek.
Zintak arrutu dizkite
Amar leitzarrek.