---
id: ab-2300
izenburua: Mañerun Kargatu Naiz Estellarako
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002300.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002300.MID
youtube: null
---

Mañerun kargatu naiz Estellarako
Norbaitek artzen badit ona dalako
Egin det franko.
Atsoak an zegoan kalean pronto
Eman du topo:
"Cuanto vale una arroba"? -Bostean lauko.