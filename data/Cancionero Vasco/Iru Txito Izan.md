---
id: ab-3255
izenburua: Iru Txito Izan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003255.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003255.MID
youtube: null
---

Iru txito izan, eta lau galdu,
Txito oien amak zer ein du?
Axeriak kendu dio lepoa,
Ta nik beste gañerakua.