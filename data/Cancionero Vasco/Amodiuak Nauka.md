---
id: ab-3005
izenburua: Amodiuak Nauka
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003005.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003005.MID
youtube: null
---

Amodiuak nauka
Bihotzetik eri;
Erran gabetarikan
Ezin nagoke ni.
Erranen dizut bada nik
Bihotzaren erditik