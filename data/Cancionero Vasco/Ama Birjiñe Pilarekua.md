---
id: ab-1588
izenburua: Ama Birjiñe Pilarekua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001588.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001588.MID
youtube: null
---

Ama birjiña Pilarekua,
Arren, indazu grazia:
Bi bertso paratzea da
Nere desio guzia.