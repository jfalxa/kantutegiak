---
id: ab-3379
izenburua: Agur, Agur, Jende Noblia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003379.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003379.MID
youtube: null
---

Agur, agur, jende noblia,
Berri on bat dekagu.
Atenzion bat eruki beza,
Iñork aditu nai badu.