---
id: ab-2611
izenburua: Jakintsunen Arabera (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002611.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002611.MID
youtube: null
---

Jakintsunen arabera, nork berak du berea,
Mundura sortu denean ekharria zortea;
Jainkoak egin bezala, nik maite dut nerea,
Nahiz ez den jenden ustez zorionez bethea.