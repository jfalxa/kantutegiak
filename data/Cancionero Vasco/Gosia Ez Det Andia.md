---
id: ab-2000
izenburua: Gosia Ez Det Andia
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002000.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002000.MID
youtube: null
---

Gosia ez det andia, baño
Egarria det u(g)ari.
Santa sekulan amoriorik
Ez diot artzen urari.
Edaten ari, t'edan naiago:
Beti nago egarria.
Au edan gabe egotia da
Neretzat penitentzia.

Lenbizi sartu tabernan eta
Ateratzen naiz azkena.
Beti edaten ari ta ala ere,
Ezin bete det barrena.
(Y)au da edari gustagarria,
Oso poztutzen naena!
""Mari Jesusa biyotzekua,
Bete amalaugarrena"".