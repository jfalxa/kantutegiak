---
id: ab-2832
izenburua: Bat, Bide, Iru, Lau (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002832.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002832.MID
youtube: null
---

Bat, bide, iru, lau.
Ezkontzen da mundu au.
Arratoiñak fraile.
Kukua meza emaile.
Ikike, mikike,
Katuek purdie milike.