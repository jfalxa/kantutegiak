---
id: ab-2847
izenburua: Gorte Egin Diot (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002847.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002847.MID
youtube: null
---

Gorte egin diot blonda xarmant bati
Ez dakit izanen dudanez.
Bai, bai izanen dut, zer nahi gostarik ere.
Biba arno ona, biba amodio
Gauak eta egunak diraino.

Nahi zireia jin ene baratzerak
Enekin promenatzerat
Bilduren tugu enseladiak
Artixota eta piper berdiak.
Biba arno ona, biba amodio,
Gauak eta egunak diraino.