---
id: ab-2147
izenburua: Jesus Hor Heldu Da Galbariotik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002147.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002147.MID
youtube: null
---

Jesus hor heldu da Galbariotik
Bere gurutziaz ongi kargaturik.
Galdetu izan zautan humiliaturik,
Ian utziko nuen atian pausaturik.

Nik, gizon gaiztua eta kriminela,
Ezetz erran nion hari berhala:
- Hua hortik aintzina, kriminel gaiztua;
Hi hor ikustiaz afronta nuela.

Jauna, zato gurekin ostatu huntara;
Emanen derautzugu arno kolpe bana;
Arno kolpe bana ta konpañia ona,
Eta harekilan conpañia ona.

Pasatzen nintzelarik hiri batetarik
Jaun batzuek jarraiki izan zaizkit ondotik:
Munduan, zioten, etzela gizonik.
Nik bezin haundia bizarra zuenik.

- Jaunak, etorri nindake zuekin ostaturat,
Bai eta ere edanen zuen arno onetikan.
Ez naiz promis yartzia, egonen naiz xutik,
Geldituz geroztik tormentatua naiz.

Neure hezurrak dire arras zahartuak
Eta urtiak ez akabatuak
Hemezortzi ehun urte baditut pasatuak
Eta urtiak ez akabatuak.

Milla urte huntan biziko haiz munduan
Zeruko gloria izanen duk azken yuyamenduan.