---
id: ab-1934
izenburua: Ez Da Maria Pujanta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001934.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001934.MID
youtube: null
---

Ez da Maria pujanta,
Ez da hain xakurra,
Ez da hain xakurra.
Ez da bethi konbeni
Sakatrapuia.