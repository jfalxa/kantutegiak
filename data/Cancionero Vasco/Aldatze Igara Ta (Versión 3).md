---
id: ab-1665
izenburua: Aldatze Igara Ta (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001665.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001665.MID
youtube: null
---

Aldatze igara ta
Zelai landa baten,
Semia ta Birjina
Atzegin artutze.