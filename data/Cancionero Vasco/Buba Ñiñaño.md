---
id: ab-1817
izenburua: Buba Ñiñaño
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001817.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001817.MID
youtube: null
---

Buba ñiñaño,
Haurra dugu ñimiño,
Ñimiño eta gaixtoño,
Eta bubaño, eta gaixtoño.
Haurra dugu ñimiño,
Ñimiño eta gaixtoño.

Zato bihar, zato gaur,
Zato bihar, zato gaur,
Gure etxian da ""lumarmau""
Eta bubaño eta gaixtoño,
Haurra dugu ñimiño,
Ñimiño eta gaixtoño.

Asto xaharra mukuxu,
Gaixtorik aski dakizu.
Berriz eliza hortan
Sartzen bazira zu,
Mezarik entzunen ez duzu.
Eta bubaño...

Ardiak nun tuk, Bettiri?
Ardiak itzalian lo.
Ardiak eman ostiko
Ai! ni ez ote naiz biziko.
Eta bubaño...