---
id: ab-1911
izenburua: Esaten Degunian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001911.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001911.MID
youtube: null
---

Esaten degunian
Guk "Ave Maria",
Kontentuz gelditzen da
Birjiña Maria.