---
id: ab-3018
izenburua: Agur, Bettiri
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003018.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003018.MID
youtube: null
---

Agur, Bettiri,
Ongi etorri!
Bizi zirade oraino?
- Bai, bizi naiz, eta bizi gogo're
Hartzekoak kobratu arteraino.