---
id: ab-2311
izenburua: Matelo Bat Eta Matelo Bi
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002311.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002311.MID
youtube: null
---

Matelo bat eta matelo bi,
Biak putzura erori.
Ai, ai, ai, amak baleki,
Altxa lezazke polliki.