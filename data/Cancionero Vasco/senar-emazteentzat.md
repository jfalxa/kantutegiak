---
id: ab-2482
izenburua: Senar-Emazteentzat
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002482.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002482.MID
youtube: null
---

Senar-emazteentzat
Au ez da egoki;
Makilla dantzatutzen
Asiko naiz aurki.
- Baldin makillarekin
Ni jotzen banazu,
Egun asko baño len
Damutuko zaizu.