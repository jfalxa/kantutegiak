---
id: ab-2798
izenburua: Bubatto Niñatto (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002798.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002798.MID
youtube: null
---

Bubatto, niñatto,
Haurra düzü ttipitto,
Bena gaiztotto.