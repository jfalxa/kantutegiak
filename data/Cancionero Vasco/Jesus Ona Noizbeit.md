---
id: ab-2150
izenburua: Jesus Ona Noizbeit
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002150.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002150.MID
youtube: null
---

Jesus ona, noizbeit zure oinetan
Gure hutsez gaude ahalketan.
Oi zorigaitz handiena!
Arbuiatu zaitugu, Jesus ona.