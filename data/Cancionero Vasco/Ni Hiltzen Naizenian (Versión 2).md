---
id: ab-2008
izenburua: Ni Hiltzen Naizenian (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002008.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002008.MID
youtube: null
---

Ni hiltzen naizenian,
Neure ehortze egunian,
Neure aide-adixkidiak,
Botoil-arnoak eskuan,
Edan zazue,
Ixurka zazue
Neure tonbaren gainian.