---
id: ab-1988
izenburua: Goizian Goizik
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001988.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001988.MID
youtube: null
---

Goizian goizik jeiki nündüzün,
Espusa nintzan goizian.
Bai eta ere zetaz beztitü
Ekhia jelkhi zenian.
Etxek'andere zabal nündüzün
Egüerdi heltzian;
Bai eta ere alhargüntsa gazte
Ekhiaren itzaltzian.