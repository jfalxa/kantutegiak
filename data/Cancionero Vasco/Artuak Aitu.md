---
id: ab-1653
izenburua: Artuak Aitu
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001653.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001653.MID
youtube: null
---

Artuak aitu eta dirua Mejikun,
Ez jarri tripazarrok nork beteko zigun!
Apez bat edo bertzek len emaiten zigun;
Eta orain or konpon, ""Dominus vobiscum"".