---
id: ab-1600
izenburua: Amets Egin Dut
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001600.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001600.MID
youtube: null
---

Amets egin dut goizian,
Ikusirik nik maitia.
Ikusi eta ezin mintza,
Ezin mintza,
Ez deia bada pena,
Eta ezin bertzia?
Desiratzen dut nik hiltzia.