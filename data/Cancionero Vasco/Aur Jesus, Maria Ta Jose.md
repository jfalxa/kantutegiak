---
id: ab-2073
izenburua: Aur Jesus, Maria Ta Jose
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002073.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002073.MID
youtube: null
---

Aur, Jesus, Maria ta Jose,
Joakin eta Ana.
Badakigu Jesus maitia
Jaio zan gauba dala.

Iñork ez digu berri bat
Eman bera baño obiya:
Belenen jayo omen zaigu
Gure Salbadoria.

Belengo estalpian dago
Jesus narru gorrian;
Ollarrarekin idia zan
Astubaren erdian.

Biden gatoz iformaturik
Gazteria guziya
Limosneru bada txit
Emengo nagosia.

Urriya bialduko degu
Zerendako guardia,
Nagosiya bezin pretuba da
Etxeko-andria.

Obeto kantatuko degu
Zeren gaur bai da gaubon,
Jaingoikoa degula
Datorren urtebaindo.