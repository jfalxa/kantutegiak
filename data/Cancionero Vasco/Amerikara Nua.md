---
id: ab-1597
izenburua: Amerikara Nua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001597.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001597.MID
youtube: null
---

Amerikara nua
Nere borondatez,
Kontentu ez naizelako
Emengo suertez.
Ara juan ta obeto
Izateko ustez.
Adios aita ta ama,
Ondo bizi bitez.