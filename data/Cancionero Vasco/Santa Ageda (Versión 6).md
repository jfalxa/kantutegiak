---
id: ab-2159
izenburua: Santa Ageda (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002159.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002159.MID
youtube: null
---

Santa Ageda, Santa Ageda,
Santa Ageda; andria!
Gilza aundia jabia,
Gilza aundia artu ezkero,
Iguzu limosenia.

Ona da baya borondatia,
Jaun zerukubak
Paga dezala
Berorren borondatia.

Idia edo idizko
Urdai puzkiya lo mismo
Urdai puzkarik ez baldin bada
Iru lukainka lo mismo.

Ezkilak errepikatzen
Jendia zer da galdetzen
Etxe ontako etxeko-andria
Kotxien da elizaratzen.

Kotxien da elizaratzen,
Urre sillan da jarraitzen
Txokolatia artu artian
Ezta andikan jaikitzen.

Or goyan dago iturri
Urarek isure ekarri
Etxe ontako nagusi jauna
Amalau milla txintxarrik.

Goyan goyango
Leyu beyetan.
Zazpi zezenak jokatuko du
Josepa Antoni eztayetan.