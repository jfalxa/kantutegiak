---
id: ab-1680
izenburua: Asingo Etxe Batian
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001680.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001680.MID
youtube: null
---

Asingo etxe batian giñen lau garbari;
Alegre negoan umorez kantari.
Mandatarie isilik zitzaigun etorri,
Parte eman bear ziola abade jaunari.