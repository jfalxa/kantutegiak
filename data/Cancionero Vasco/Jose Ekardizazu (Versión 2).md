---
id: ab-2168
izenburua: Jose Ekardizazu (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002168.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002168.MID
youtube: null
---

Jose, ekardizazu
Zestotxo bat egur:
Aurra otzak illen digu,
Ni naiz agitz beldur.
- Au eztun otzak illen,
Birjina Marie:
Aur onek berekin dauke
Dibinidadie.

Astua ta idia
Dauzka aldamenian:
Asnazez berotzen da,
Ostutzen danian.