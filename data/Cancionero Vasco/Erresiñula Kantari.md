---
id: ab-1906
izenburua: Erresiñula Kantari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001906.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001906.MID
youtube: null
---

Erresiñula kantari,
Xori ororen buruzagi.
Zonbeit aldiz behartu nuzu
Zure botz ederrari,
Saihetseko leihotik,
Jeikirik neure ohetik.