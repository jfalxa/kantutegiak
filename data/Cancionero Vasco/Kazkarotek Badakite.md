---
id: ab-2217
izenburua: Kazkarotek Badakite
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002217.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002217.MID
youtube: null
---

Kazkarotek badakite trikun trakun egiten.
Ollonua ebatsi ta sasi-zoko gordetzen.
Pairun, pairun, piarretun...

Kazkarotek erosi du lau sosetan ogia;
Lagunari saldu dio bortz sosetan erdia.
Pairun, pairun, piarretun...

Kazkarotek badakite txerrinoen kuidatzen:
Orratzñoak sartu eta ongi disimulatzen.
Pairun, pairun, piarretun...