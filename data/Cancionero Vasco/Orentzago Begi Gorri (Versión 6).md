---
id: ab-2096
izenburua: Orentzago Begi Gorri (Versión 6)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002096.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002096.MID
youtube: null
---

Orentzago, begi gorri,
Nun atxitu arrai ori?

- Zubiaurreko erreketan,
Bart arratseko amaiketan.

Agilando, agilando,
Beste kalian San Bernardo.