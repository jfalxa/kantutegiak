---
id: ab-1975
izenburua: Gogoan Derabillena (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001975.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001975.MID
youtube: null
---

Gogoan derabillena neri onek ekarri,
Nik orain gogoan Prantziska iduki.
Sarri, sarri emango nioke Pedro Antoneti,
Ondotxo gozo dezala bertsoetan poliki,
Nola esaten baitaki.