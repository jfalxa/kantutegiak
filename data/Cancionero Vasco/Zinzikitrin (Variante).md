---
id: ab-2789
izenburua: Zinzikitrin (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002789.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002789.MID
youtube: null
---

Zinzikitrin, Maria Martin,
Haurten arno onak sosak balio tin.