---
id: ab-1784
izenburua: Bi Txapel-Xuri Giñan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001784.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001784.MID
youtube: null
---

Bi txapel-xuri giñan aduaneruak,
Beste gañerakuak denak paisanuak.
Absolbiturik daude gure pekatuak:
Basta da izatia probenzianuak.