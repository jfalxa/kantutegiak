---
id: ab-2108
izenburua: Donibane Ziburuk (Versión 2)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002108.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002108.MID
youtube: null
---

Donibane Ziburuk
Erdi erdian zubia.
Jaun errientak haren gainian
Egiten baitu guardia,
Ia nondik ikusiren duen
Maiteñoaren begia.

Dendari gazte pollita,
Gethariako partetik,
Gethariako partetik eta
Moda berrian jauntzirik:
Jaun errientak emanen derautzu
Hoi baño pollitagorik.

Dendari gazte propia,
Etzaitezela ganbia.
Jaun errientari emokezu
Bazkal onduan kafia;
Harrek gero emanen derautzu
Ganbaran letzionia.