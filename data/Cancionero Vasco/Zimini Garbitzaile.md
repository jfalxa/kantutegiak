---
id: ab-2779
izenburua: Zimini Garbitzaile
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002779.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002779.MID
youtube: null
---

Zimini garbitzaile,
Mutur zikinduna,
Galtza tzar, bizar aundi,
Judasen laguna;
Nolako saltzailia,
Halako erostuna.
Neska tzarrak egin du
Gostuko fortuna.