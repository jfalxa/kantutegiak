---
id: ab-1771
izenburua: Bertso Berriak Obra Propiak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001771.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001771.MID
youtube: null
---

Bertso berriak, obra propiak,
Astera nua kantatzen:
Zein dama gazte koleratuak
Batzuak dauden atetzen.
Itxura ezta erraz izanen
Orrelakuan jabetzen.
Biar direna asi ditezke
Kuidadorikan galdetzen.