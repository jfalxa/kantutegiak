---
id: ab-2345
izenburua: Moreriara Juan Ta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002345.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002345.MID
youtube: null
---

Moreriara juan ta
Sutan nintzen asi.
Enpleu ortarako
Ez ninduten azi.

Adios, aita, eta
Agur, nere ama:
Eskergabetasunak
Soldadu nerama.