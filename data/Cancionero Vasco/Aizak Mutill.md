---
id: ab-1553
izenburua: Aizak Mutill
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001553.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001553.MID
youtube: null
---

Aizak, mutill, jaiki hari,
Uria denez mira hari.
- Bai, nausia: uria da;
Gure txakurra bustia da.