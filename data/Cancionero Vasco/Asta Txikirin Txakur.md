---
id: ab-3089
izenburua: Asta Txikirin Txakur
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/003089.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/003089.MID
youtube: null
---

Asta txikirin txakur,
Mendian elurra. (bis)
Txardin burua baño
Obe da ollua. (bis.