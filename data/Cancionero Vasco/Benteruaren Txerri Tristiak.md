---
id: ab-1762
izenburua: Benteruaren Txerri Tristiak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001762.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001762.MID
youtube: null
---

Benteruaren txerri tristiak
Arraizun saldu ziran iruez bestiak.
Ikaratzen zituzten bizka jendiak,
Illa punta puntaraño zorriz betiak.