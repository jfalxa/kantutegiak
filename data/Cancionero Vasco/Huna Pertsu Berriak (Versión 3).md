---
id: ab-2292
izenburua: Huna Pertsu Berriak (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002292.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002292.MID
youtube: null
---

Huna phertsu berriak nik orai paratu,
Hamar manamenduak nola gobernatu.
Lendabiziko horretan Jainkoa maitatu;
Lagun protsimua ere igual estimatu.

Bigarren manamenduan, yuramentu gutti;
Nahi-denaren erraitera mihia ez utzi.
Ez da bada gauza hortan diferentzia gutti:
Zerurat igan edo ifernurat yautsi.

Meza saindua entzun hirugarrenian
Obligatuak gaude igande egunian.
Obra onak eginez gero ahal denian,
Gloriaz gozatzeko eternitatian.

Laugarren manamenduan gure burasoak
Ahal dugunez sokorritu ez duten gaixoak.
Ein-ahalak eginik ere, ez dire pagatuak;
Urrikaltzekoak dire heien trabailuak.

Bostgarren manamenduan, nihor ez hiltzia;
Zer gauza gogorra den, oi, heriotzia!
Aski dugu Jainkoari erreparatzia,
Zeren gure gatik baitu hartu gurutzia.

Garbitasuna beira seigarren horretan,
Gogoz, itzez, obraz eta solas lizunetan.
Kasu egin dezagun manamendu hortan,
Tentazioniak ardura dira mundu huntan.

Zazpigarren manamenduan, deusik ez ebatsi;
Nor bereharekin pasa, bertzerenak utzi.
Deabruak laguntzen gaitu egiteko gaizki,
Atzemaiteko sariak edatuak dauzki.

Zortzigarren manamenduan, hau egin behar dugu:
Faltsu destamoniorik nehori ez kasu.
Yuramentu gaixtorik egiten badugu,
Egun batez kondu eman beharko dugu.

Bederatzigarrenian, ez dago bertzerik
Ez desira bertzeren senhar-emazterik.
Bertzeren odolian ez hartu pharterik,
Arima gaixoak izan ez dezan kalterik.

Izena dut Esteben deithura dut Landa.
Kantu hauk eman ditut gogoak eman ta.
Emaitea ez aski, bertzerik da falta:
Bizian segitzia eta hil-ta zerurat goitia.