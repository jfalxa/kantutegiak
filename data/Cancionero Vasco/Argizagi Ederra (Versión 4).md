---
id: ab-1639
izenburua: Argizagi Ederra (Versión 4)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001639.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001639.MID
youtube: null
---

Argizagi ederra, argi egidazu;
Bidaia luza huntan zuk lagun ezazu.
Maitia nahi nuke gaur gero mintzatu:
Harat sar arteraino, argi egidazu.