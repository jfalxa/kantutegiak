---
id: ab-2422
izenburua: Orra Maria Jazinta
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002422.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002422.MID
youtube: null
---

Orra Maria, orra Maria,
Orra Maria Jazinta!
Biok ezkondu biar degula,
Beste guziak utzita.