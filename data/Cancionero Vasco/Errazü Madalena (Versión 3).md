---
id: ab-2629
izenburua: Errazü Madalena (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002629.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002629.MID
youtube: null
---

Errazü, Madalena, norat zoaza hola?
Zerk hola zaramatza desertian barna?
- Yauna galdüz geroztik
Ez düt hunik nitan;
Gaiza güziak dütüt
Higuin mündü huntan.