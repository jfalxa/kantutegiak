---
id: ab-1533
izenburua: Aita Eternuak
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001533.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001533.MID
youtube: null
---

Aita Eternuak ez daduka,
Herodes, zuretzat zerua:
Obenikan gabe kendu baitzinaben
Bost milla aurrei bizia.
Nai izan zinuen Jesus topatu;
Etzinuen ondo pensatu,
Egiptora zitzaitzun pasatu.