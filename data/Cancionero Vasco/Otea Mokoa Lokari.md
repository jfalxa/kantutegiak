---
id: ab-2430
izenburua: Otea Mokoa Lokari
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002430.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002430.MID
youtube: null
---

Otea mokoa lokari,
Txorie gañian kantari.
Sagar gorrien ondoan,
Juanita orren zangoa
Ederra dute bapoa;
Bera dan bezalakoa.
Bera ona da, ta obia luke
Pedro Antonekua.