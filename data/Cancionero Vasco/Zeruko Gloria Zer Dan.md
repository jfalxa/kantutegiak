---
id: ab-2636
izenburua: Zeruko Gloria Zer Dan
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002636.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002636.MID
youtube: null
---

Zeruko gloria zer dan
Asitzen naiz esaten,
Nere al deran moduan,
Aditzera ematen.