---
id: ab-1783
izenburua: An An Goian Etxe Bat (Versión 3)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001783.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001783.MID
youtube: null
---

An an goian etxe bat:
Josepek jozen du;
Gizonak bentanara,
Nekez itz iten du.