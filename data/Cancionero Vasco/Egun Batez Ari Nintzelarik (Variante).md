---
id: ab-1870
izenburua: Egun Batez Ari Nintzelarik (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001870.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001870.MID
youtube: null
---

Egun batez ari nintzelarik
Sala batian brodatzen,
Entzun nuen mariñel bat
Itsasuan kantatzen,
Itsasuan kantatzen eta
Pertsu ederren emaiten.