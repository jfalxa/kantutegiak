---
id: ab-1601
izenburua: Amikoeko Gilen
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001601.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001601.MID
youtube: null
---

Amikoeko Gilen, kilo egileak
Etxetik kendu ditu bere langileak,
Sehi maiteak,
Zeren galdu dituen irabazkidea.