---
id: ab-1914
izenburua: Eskoliertsa Gaztia (Variante)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001914.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001914.MID
youtube: null
---

Eskoliertsa gaztia,
Otoi, emazu guardia,
Jaun errientak ixur ez dezan
Zure altzora ankria,
Tonaturik geldi ez dedin
Zure dabantail xuria.