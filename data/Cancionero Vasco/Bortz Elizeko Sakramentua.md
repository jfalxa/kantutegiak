---
id: ab-2075
izenburua: Bortz Elizeko Sakramentua
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002075.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002075.MID
youtube: null
---

Bortz Elizeko sakramentuak
Dira zerutik jetxiak;
Beste biok borondatezkuak
Jaunak guretzat utziak.