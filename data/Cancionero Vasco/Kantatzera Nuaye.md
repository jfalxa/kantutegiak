---
id: ab-2207
izenburua: Kantatzera Nuaye
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/002207.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/002207.MID
youtube: null
---

Kantatzera nuaye zelebrekeriak,
Damatxo gazte batek berez ekarriyak,
Berri berriyak;
Ez dire desonestak, bai farragarriyak.