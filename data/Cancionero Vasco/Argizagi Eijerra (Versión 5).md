---
id: ab-1640
izenburua: Argizagi Eijerra (Versión 5)
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001640.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001640.MID
youtube: null
---

Argizagi eijerra, argi egidazü;
Bidaje lüze huntan orai banuazü!
Maitia nahi nikezü gaur behin mintzatü.
Haren borthaladrano argi egidazü.