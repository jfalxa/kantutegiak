---
id: ab-1691
izenburua: Atso Zarra Belendrin
kantutegia: Cancionero Vasco
partitura: https://www.eusko-ikaskuntza.eus/ImgsCanc/001691.gif
midi: https://www.eusko-ikaskuntza.eus/MusicCanc/001691.MID
youtube: null
---

Atso zarra belendrin,
Ire bentak egin din.
Ortzak ere juan zaizkin ) tres
Eta diabru zarra dirudin. ) veces
Txirulin, brinkulin, brinkulin, brinkulin,)
Txirulin, brinkulin atsua. )bis
Su ondoan agoenian, )
Ez daukan moxkor gaiztua,)bis
Ez daukan moxkor gaiztua.