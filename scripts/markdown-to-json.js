/** @typedef {import("./types.ts").Kantua} Kantua */

import fs from "fs/promises";
import { compact, readMarkdown, writeJSON } from "./utils.js";

const DATA_DIR = "./data";
const KANTUAK_FILE = "./dumps/kantuak.json";
const SPLIT_DIR = "./dumps/kantuak";

async function main() {
  const shouldSplit = process.argv.includes("--split");
  const dirs = await fs.readdir(DATA_DIR);

  const kantuak = /** @type {any[]} */ ([]);

  await Promise.all(
    dirs.map(async (dir) => {
      const files = await fs.readdir(`${DATA_DIR}/${dir}`);

      return Promise.all(
        files.map(async (file) => {
          const [frontmatter, body] = await readMarkdown(`${DATA_DIR}/${dir}/${file}`);

          const name = frontmatter.kantutegia
            .toLowerCase()
            .normalize("NFD")
            .replaceAll(" ", "_")
            .replace(/\p{Diacritic}/gu, "")
            .replace(/[^a-z0-9-_]/g, "");

          /** @type {Kantua} */ const kantua = {
            id: frontmatter.id,
            izenburua: frontmatter.izenburua,
            hitzak: body,
            kantutegia: frontmatter.kantutegia,
            partitura: frontmatter.partitura,
            midi: frontmatter.midi,
            youtube: frontmatter.youtube,
          };

          kantuak.push(compact(kantua));
        })
      );
    })
  );

  const json = JSON.stringify(kantuak);
  console.log("TOTAL LEN", json.length);

  if (!shouldSplit) {
    await writeJSON(KANTUAK_FILE, kantuak);
  } else {
    await fs.mkdir(SPLIT_DIR, { recursive: true });
    for (const kantua of kantuak) {
      await writeJSON(`${SPLIT_DIR}/${kantua.id}.json`, kantua);
    }
  }
}

main();
