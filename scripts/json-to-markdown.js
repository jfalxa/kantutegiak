/** @typedef {import("./types.ts").Kantua} Kantua */

import fs from "fs/promises";
import { writeMarkdown, readJSON, fileify, capitalize } from "./utils.js";

const SOURCE_FILE = "./dumps/kantuak.json";
const DIST_DIR = "./dumps/data";

async function main() {
  await fs.rm(DIST_DIR, { recursive: true, force: true });
  await fs.mkdir(DIST_DIR, { recursive: true });

  /** @type {Kantua[]} */ const kantuak = await readJSON(SOURCE_FILE);

  for (const kantua of kantuak) {
    const dir = `${DIST_DIR}/${capitalize(fileify(kantua.kantutegia))}`;
    const file = `${dir}/${capitalize(fileify(kantua.izenburua))}.md`;
    await fs.mkdir(dir, { recursive: true });
    await writeMarkdown(file, frontmatter(kantua), kantua.hitzak);
  }
}

/**
 * @param {Partial<Kantua>} data
 * @returns {Omit<Kantua, "hitzak">}
 */
function frontmatter({ hitzak, ...data }) {
  return {
    id: "",
    izenburua: "",
    kantutegia: "",
    partitura: null,
    midi: null,
    youtube: null,
    ...data,
  };
}

main();
