import fs from "fs/promises";
import yaml from "yaml";

/**
 * @template T
 * @param {string} file
 * @returns {Promise<T>}
 */
export async function readJSON(file) {
  const json = await fs.readFile(file);
  return JSON.parse(json.toString());
}

/**
 * @param {string} file
 * @param {any} data
 * @param {boolean} [pretty]
 * @returns {Promise<void>}
 */
export async function writeJSON(file, data, pretty) {
  const json = JSON.stringify(data, null, pretty ? 2 : 0);
  await fs.writeFile(file, json);
}

/**
 * @template T
 * @param {string} file
 * @returns {Promise<[T, string]>}
 */
export async function readMarkdown(file) {
  const markdown = await fs.readFile(file);
  const parts = markdown.toString().split(/^---$/m);
  const frontmatter = yaml.parse(parts[1].trim());
  const body = parts.slice(2).join("\n").trim();
  return [frontmatter, body];
}

/**
 * @param {string} file
 * @param {any} frontmatter
 * @param {string} body
 */
export async function writeMarkdown(file, frontmatter, body) {
  const markdown = `---\n${yaml.stringify(frontmatter)}---\n\n${body.trim()}`;
  await fs.writeFile(file, markdown);
}

/**
 * @param {number} duration
 * @returns {Promise<void>}
 */
export function sleep(duration) {
  return new Promise((resolve) => setTimeout(resolve, duration));
}

/**
 * @param {string} text
 * @returns {string}
 */
export function capitalize(text) {
  return text
    .toLowerCase()
    .replace(/(^|\s|\p{P})\p{L}/gu, (match) => match.toUpperCase())
    .replace(/\([iv]+\)/gi, (match) => match.toUpperCase());
}

/**
 * @param {string} text
 * @returns {string}
 */
export function fileify(text) {
  return text.replace(/[/<>:"\/\\|?*/]/g, "");
}

/**
 * @param {any} obj
 * @returns {any}
 */
export function compact(obj) {
  const compacted = /** @type {any} */ ({});
  for (const key in obj) {
    if (obj[key]) compacted[key] = obj[key];
  }
  return compacted;
}

/**
 * @param {string} str
 * @returns {string}
 */
export function normalize(str) {
  return str
    .toLowerCase()
    .normalize("NFD")
    .replace(/[\u0300-\u036f]/g, "");
}

/**
 * @param {string} str
 * @returns {string}
 */
export function slugify(str) {
  return normalize(str)
    .replace(/[^\w- ]/g, "")
    .replace(/[-+ ]/g, "-")
    .toLowerCase();
}
