/** @typedef {import("../types").YoutubeVideo} YoutubeVideo */

import dotenv from "dotenv";
import fs from "fs";
import fsp from "fs/promises";
import { Configuration, OpenAIApi } from "openai";
import { sleep } from "../utils";

const SOURCE_FILE = "./dumps/txantxangorria.json";
const DIST_FILE = "./dumps/gpt.json";

async function main() {
  dotenv.config();

  const file = await fsp.readFile(SOURCE_FILE);
  /** @type {YoutubeVideo[]} */ const videos = JSON.parse(file.toString());

  const configuration = new Configuration({
    apiKey: process.env.OPENAI_API_KEY,
  });

  const openai = new OpenAIApi(configuration);

  const progress = await getCurrentProgress();

  let s = performance.now();

  const chunks = [];
  const chunkSize = 10;
  const chunkStart = Math.floor(progress / chunkSize);
  const pages = Math.ceil(videos.length / chunkSize) + 1;

  for (let i = 0; i < pages; i++) {
    const from = i * chunkSize;
    const chunk = videos.slice(from, from + chunkSize);
    chunks.push(chunk);
  }

  const options = progress > 0 ? { flags: "a" } : {};
  const stream = fs.createWriteStream(DIST_FILE, options);

  if (progress === 0) {
    stream.write("[\n");
  }

  for (let i = chunkStart; i < pages; i++) {
    const chunk = chunks[i];

    console.log(`> Fetching chunk ${i} of ${pages} (${chunk.length} items)...`);

    const requests = chunk.map(async (video) => {
      const text = `${video.snippet.title}\n\n${video.snippet.description}`;

      const placeholder = { title: video.snippet.title, placeholder: true };
      const placeholderJSON = JSON.stringify(placeholder, null, 2);

      const PROMPT = `
        After this explanation, separated by a "---", I will copy paste you some text.
        It's the description of a basque song, and it contains different information, like title and lyrics.

        The description will always start by the song title, followed by the performer between parentheses. After that, the data is mixed and you will have to extract only the lyrics from it.

        I would like you to parse this description and create a JSON containing the following keys (their value can be null if you do not find any relevant information in the description):
        - title: the song title, as a string
        - performed_by: the performers of the song, as a string
        - lyrics: the lyrics of the song, as a single string and you must keep all new lines (\n) as they are meaningful

        Do not bother writing natural language, only produce JSON and do not truncate any content for brevity.
      `;

      try {
        const completion = await openai.createChatCompletion({
          model: "gpt-3.5-turbo",
          messages: [{ role: "user", content: `${PROMPT}\n\n---\n\n${text}` }],
        });

        const content = completion.data.choices[0].message?.content ?? "";

        try {
          return JSON.stringify(JSON.parse(content), null, 2);
        } catch {
          return placeholderJSON;
        }
      } catch (err) {
        const title = text.split("(")[0];
        if (err instanceof Error) console.log(`* Error fetching API: ${title} => ${err.message}`);
        return placeholderJSON;
      }
    });

    const results = await Promise.all(requests);
    const string = results.filter(Boolean).join(",\n") + ",\n";
    const buffer = Buffer.from(string);

    stream.write(buffer);

    console.log(`> ${results.length} items written.`);
    console.log(`> Sleeping for 5s...`);

    await sleep(5000);
  }

  stream.write("\n]");
  stream.end();

  let e = performance.now();
  console.log(`time: ${e - s}ms`);
}

async function getCurrentProgress() {
  const file = await fsp.readFile(DIST_FILE);
  const json = file + "null\n]";
  const data = JSON.parse(json);
  data.pop();
  return data.length;
}

main();
