/** @typedef {import("../types.ts").Kantua} Kantua */

import cld from "cld";
import { capitalize, readJSON, writeJSON } from "../utils.js";

const EI_FILE = "./dumps/ei-kantuak.json";
const YT_FILE = "./dumps/yt-kantuak.json";
const DIST_FILE = "./dumps/kantuak.json";

async function main() {
  /** @type {Kantua[]} */ const ei = await readJSON(EI_FILE);
  /** @type {Kantua[]} */ const yt = await readJSON(YT_FILE);

  /** @type {Kantua[]} */ let kantuak = [];

  for (const kantua of [...ei, ...yt]) {
    if (await isEskuara(kantua.hitzak)) {
      kantuak.push(kantua);
    }
  }

  console.log("FINAL", kantuak.length);

  await writeJSON(DIST_FILE, kantuak);
}

/**
 * @param {string} text
 * @returns {Promise<boolean>}
 */
async function isEskuara(text) {
  try {
    const result = await cld.detect(text, { languageHint: "BASQUE" });
    const lang = result.languages[0];
    const isOther = ["es", "en", "fr"].includes(lang.code);
    return !isOther;
  } catch (err) {
    return true;
  }
}

main();
