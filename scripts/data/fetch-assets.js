/** @typedef {import("../types.ts").Kantua} Kantua */

import fs from "fs/promises";
import pt from "path";
import { readJSON, writeJSON } from "../utils.js";

const SOURCE_FILE = "./dumps/kantuak.json";
const DIST_FILE = "./dumps/kantuak-cleaned.json";
const DIST_DIR = "./dumps/assets";

async function main() {
  /** @type {Kantua[]} */ const kantuak = await readJSON(SOURCE_FILE);

  await fs.rm(DIST_DIR, { recursive: true, force: true });
  await fs.mkdir(DIST_DIR, { recursive: true });

  const withAssets = kantuak.filter((k) => k.partitura || k.midi);

  for (let i = 0; i < withAssets.length; i++) {
    console.log(`> Fetching assets ${i + 1} of ${withAssets.length}`);
    const kantua = withAssets[i];
    await saveResource(kantua.partitura).catch(() => (kantua.partitura = null));
    await saveResource(kantua.midi).catch(() => (kantua.midi = null));
  }

  await writeJSON(DIST_FILE, kantuak);
}

/**
 * @param {string | null} url
 */
async function saveResource(url) {
  if (!url) return;

  const response = await fetch(url);

  if (!response.ok) throw new Error(response.statusText);

  const arrayBuffer = await response.arrayBuffer();

  const fileName = pt.basename(url);
  await fs.writeFile(`${DIST_DIR}/${fileName}`, Buffer.from(arrayBuffer));
}

main();
