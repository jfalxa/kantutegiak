/**
 * @typedef {import("../types").YoutubeVideo} YoutubeVideo
 */

import dotenv from "dotenv";
import fs from "fs/promises";

const JSON_FILE = "./dumps/txantxangorria.json";

async function main() {
  dotenv.config();

  /** @type {YoutubeVideo[]} */ let videos;

  try {
    const file = await fs.readFile(JSON_FILE);
    videos = JSON.parse(file.toString());
  } catch {
    videos = await fetchYoutube();
  }

  console.log(`> Found ${videos.length} videos`);
}

/**
 * @returns {Promise<YoutubeVideo[]>}
 */
async function fetchYoutube() {
  const videos = [];

  const channelIds = ["UCgRGaFWbfqsDhG1zsJ0NzlQ", "UCoAOUSKjzt2SGP4ka1witdw"];

  for (const channelId of channelIds) {
    const channelVideos = /** @type {any[]} */ (await getChannelVideos(channelId));
    videos.push(...channelVideos);
  }

  const json = JSON.stringify(videos, null, 2);
  await fs.writeFile(JSON_FILE, json);

  return videos;
}

/**
 *
 * @param {string} channelId
 * @returns {Promise<YoutubeVideo[]>}
 */
async function getChannelVideos(channelId) {
  try {
    const playlistId = await getPlaylistId(channelId);
    const videos = await getPlaylistItems(playlistId);

    return videos;
  } catch (error) {
    console.error("An error occurred:", /** @type Error */ (error).message);
    return [];
  }
}

/**
 * @param {string} channelId
 * @returns {Promise<string>}
 */
async function getPlaylistId(channelId) {
  console.log("Fetching channel details...");
  const data = await youtube("channels", { id: channelId, part: "contentDetails" });
  const playlistId = data.items[0].contentDetails.relatedPlaylists.uploads;
  return playlistId;
}

/**
 * @param {string} playlistId
 * @returns {Promise<YoutubeVideo[]>}
 */
async function getPlaylistItems(playlistId) {
  console.log("Fetching channel video list...");

  let videos = [];
  let nextPageToken = null;

  do {
    const data = await youtube("playlistItems", {
      part: "snippet",
      maxResults: 100,
      playlistId,
      pageToken: nextPageToken,
    });

    console.log(`found ${data.items.length} more videos`);

    videos.push(...data.items);
    nextPageToken = data.nextPageToken;
  } while (nextPageToken);

  return videos;
}

/**
 * @param {string} endpoint
 * @param {Record<string, number | string | null | undefined >} params
 * @returns {Promise<any>}
 */
async function youtube(endpoint, params) {
  const url = new URL(endpoint, "https://www.googleapis.com/youtube/v3/");

  const searchParams = cleanParams({ ...params, key: process.env.GOOGLE_API_KEY });
  url.search = new URLSearchParams(searchParams).toString();

  const response = await fetch(url);
  const data = await response.json();

  if (!response.ok)
    console.log(`Request to ${url.pathname} failed (${response.status}) ${JSON.stringify(data)}`);

  return data;
}

/**
 * @param {Record<string, number | string | null | undefined >} obj
 * @returns {Record<string, string>}
 */
function cleanParams(obj) {
  /** @type {any} */ const compacted = {};
  for (const key in obj) {
    if (obj[key]) compacted[key] = String(obj[key]);
  }
  return compacted;
}

main();
