/** @typedef {import("../types.ts").Kantua} Kantua */

import { readJSON, writeJSON } from "../utils.js";

const FILE = "./dumps/kantuak.json";

async function main() {
  /** @type {Kantua[]} */ const kantuak = await readJSON(FILE);

  for (const kantua of kantuak) {
    const lines = [];

    for (const line of kantua.hitzak.split("\n")) {
      if (isLineOK(line)) lines.push(line);
    }

    kantua.hitzak = lines.join("\n");
  }

  await writeJSON(FILE, kantuak);
}

/**
 * @param {string} line
 */
function isLineOK(line) {
  line = line.toLowerCase();
  return !line.includes("http") && !line.includes("karaoke") && !line.includes("basque music");
}

main();
