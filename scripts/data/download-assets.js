import pt from "path";
import fs from "fs/promises";
import fss from "fs";
import { readJSON } from "../utils.js";

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

async function main() {
  /** @type {import("../types.ts").KantuzDB} */
  const db = await readJSON("./dumps/kantuz-db.json");

  // await fs.rm("./dumps/assets", { force: true, recursive: true });
  await fs.mkdir("./dumps/assets", { recursive: true });

  for (let i = 0; i < db.kantuak.length; i++) {
    let shouldSleep = false;

    const kantua = db.kantuak[i];
    console.log(`[${i + 1}/${db.kantuak.length}] ${kantua.izenburua}`);

    if (kantua.midi) {
      console.log(`> Download ${kantua.midi}`);
      const ok = await download("./dumps/assets", kantua.midi);
      shouldSleep = shouldSleep || ok || false;
    }

    if (kantua.partitura) {
      console.log(`> Download ${kantua.partitura}`);
      const ok = await download("./dumps/assets", kantua.partitura);
      shouldSleep = shouldSleep || ok || false;
    }

    if (shouldSleep) {
      await sleep(1000);
    }
  }
}

main();

/**
 * @param {string} destination
 * @param {string} url
 */
async function download(destination, url) {
  const fileName = url.split("/").pop() ?? "";
  const filePath = pt.join(destination, fileName);

  if (fss.existsSync(filePath)) {
    return;
  }

  try {
    const response = await fetch(url, { signal: AbortSignal.timeout(5000) });

    if (response.ok) {
      const arrayBuffer = await response.arrayBuffer();
      const buffer = Buffer.from(arrayBuffer);
      await fs.writeFile(filePath, buffer);
      return true;
    }
  } catch {
    console.log(`> Error writing file at ${filePath}`);
  }
}

/**
 * @param {number} time
 * @returns {Promise<void>}
 */
function sleep(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}
