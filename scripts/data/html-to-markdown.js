import fs from "fs/promises";
import TurndownService from "turndown";
import * as cheerio from "cheerio";

const ROOT = process.argv[2];

async function main() {
  const files = await getFiles();
  const cache = {};

  await fs.rm("./kantuak", { recursive: true }).catch(() => {});

  await Promise.all(
    files.map(async (file) => {
      try {
        const html = await fs.readFile(`${ROOT}/${file}/index.html`);

        if (html.includes("NO PHYSICAL FILE")) return;

        const ref = getReference(file);
        const data = parseHTML(html);

        const dir = data["kantutegia"] || "kantutegia";
        const filename = getFileName(data, cache);
        const markdown = toMarkdown({ ...ref, ...data });

        await fs.mkdir(`./kantuak/${dir}`, { recursive: true });
        await fs.writeFile(`./kantuak/${dir}/${filename}.md`, markdown);
        console.log(`${dir}/${filename}.md`);
      } catch (error) {
        console.log("error: " + file);
        console.log(error);
      }
    })
  );
}

// RUN
main();

async function getFiles() {
  const files = await fs.readdir(ROOT);
  return files.filter((file) => file.startsWith("ab-"));
}

function getFileName(data, cache) {
  const title = data["izenburua"] || data["slug"] || "";

  let filename =
    title.normalize("NFKD").replace(SANITIZE_RX, "").toLowerCase().replace(/\s+/g, "_").trim() ||
    file;

  cache[filename] = filename in cache ? cache[filename] + 1 : 0;
  if (cache[filename]) {
    filename += `-${cache[filename] + 1}`;
  }

  return filename;
}

function getReference(file) {
  return {
    id: file,
    url: `https://www.eusko-ikaskuntza.eus/eu/dokumentu-fondoa/euskal-kantutegia/${file}/`,
  };
}

function parseHTML(html) {
  const $ = cheerio.load(html);

  const links = {
    midi: $(".dokumentua-irakurri:nth-child(2) a").attr("href"),
    partitura: $(".dokumentua-irakurri:nth-child(3) a").attr("href").replace("../../../../", "https://www.eusko-ikaskuntza.eus/"), // prettier-ignore
  };

  const meta = {};
  $(".texto-multimedia dt").each((_, dt) => {
    if ($(dt).text() in metaKeys) {
      const key = metaKeys[$(dt).text()];
      const dd = $(dt).next();
      if (dd.is("dd")) {
        meta[key] = dd.text();
      }
    }
  });

  const content = $(".kokapena div div").html().replace("<h3>BEREZKO TESTUA</h3>", "").trim();

  return { ...links, ...meta, content };
}

function toMarkdown({ content, ...data }) {
  const tds = new TurndownService();

  const frontmatter = buildFrontmatter(data);
  const title = data["izenburua"]?.toUpperCase() || "KANTUA";
  const body = tds.turndown(content);

  return `${frontmatter}\n\n# ${title}\n\n${body}`;
}

function buildFrontmatter(meta) {
  const lines = Object.entries(meta)
    .sort((a, b) => fmOrder.indexOf(a[0]) - fmOrder.indexOf(b[0]))
    .map(([key, value]) => `${key}: ${value.replace(": ", ", ")}`)
    .join("\n");

  return `---\n${lines}\n---`;
}

const metaKeys = {
  Kantutegia: "kantutegia",
  Argitaletxea: "argitaletxea",
  "Berezko izenburua": "izenburua",
  "Argitalpenaren urtea": "urtea",
};

const fmOrder = [
  "izenburua",
  "kantutegia",
  "urtea",
  "argitaletxea",
  "slug",
  "url",
  "partitura",
  "midi",
];

const SANITIZE_RX = /[^a-z0-9\_\- ]/gi;
