/** @typedef {import("../types.ts").Kantua} Kantua */
/** @typedef {import("../types.ts").Kantutegia} Kantutegia */

import { readJSON, slugify, writeJSON } from "../utils.js";

// izenburua in current kantutegiak.json is wrong, fix it
async function main() {
  /** @type {Kantua[]} */ const kantuak = await readJSON("./dumps/kantuak.json");
  /** @type {Kantutegia[]} */ const kantutegiak = await readJSON("./dumps/kantutegiak.json");

  // group kantutegiak by id
  const kantutegiakById = /** @type {Record<string, Kantutegia>} */ ({});
  for (const kantutegia of kantutegiak) {
    kantutegia.id = kantutegia.id.replace(/_/g, "-");
    kantutegiakById[kantutegia.id] = kantutegia;
  }

  // group kantuak by kantutegia
  const kantuakByKantutegia = /** @type {Record<string, Kantua[]>} */ ({});
  for (const kantua of kantuak) {
    const kantutegia = slugify(kantua.kantutegia);
    if (!kantuakByKantutegia[kantutegia]) kantuakByKantutegia[kantutegia] = [];
    kantuakByKantutegia[kantutegia].push(kantua);
  }

  // add missing kantutegiak to index
  for (const kantutegiaID in kantuakByKantutegia) {
    if (kantutegiaID in kantutegiakById) continue;

    const kantutegia = {
      id: "",
      izenburua: "",
      argitaletxea: null,
      urtea: null,
      count: 0,
    };

    kantutegiak.push(kantutegia);
    kantutegiakById[kantutegiaID] = kantutegia;
  }

  // replace izenburua with the proper value and add count
  let kantutegiaCounter = 0;
  for (const kantutegiaID in kantutegiakById) {
    const izenburua = kantuakByKantutegia[kantutegiaID]?.[0]?.kantutegia;
    kantutegiakById[kantutegiaID].id = `ktg-${++kantutegiaCounter}`;
    kantutegiakById[kantutegiaID].izenburua = izenburua;
    kantutegiakById[kantutegiaID].count = kantuakByKantutegia[kantutegiaID]?.length ?? 0;
  }

  // replace kantutegia property on kantuak with the new id
  for (const kantua of kantuak) {
    kantua.kantutegia = kantutegiakById[slugify(kantua.kantutegia)]?.id;
  }

  await writeJSON("./dumps/kantuz-db.json", { kantutegiak, kantuak }, true);
}

main();
