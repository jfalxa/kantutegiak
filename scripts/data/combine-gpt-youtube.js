/** @typedef {import("../types.ts").Kantua} Kantua */
/** @typedef {import("../types.ts").YoutubeVideo} YoutubeVideo */

import Lunr from "lunr";
import { readJSON, writeJSON } from "../utils.js";

const GPT_FILE = "./dumps/gpt.json";
const YT_FILE = "./dumps/txantxangorria.json";

async function main() {
  /** @typedef {{ id: string, title: string, performed_by: string, lyrics: string }} GptResult */

  let gpt = /** @type {GptResult[]} */ (await readJSON(GPT_FILE));

  console.log("BEFORE", gpt.length);
  gpt = gpt.filter((item, i) => {
    const index = gpt.findIndex((other) => other.title === item.title && other.lyrics === item.lyrics); // prettier-ignore
    return index === i;
  });
  gpt = gpt.map((item, i) => ({ ...item, id: String(i) }));
  console.log("AFTER", gpt.length);

  let yt = /** @type {YoutubeVideo[]} */ (await readJSON(YT_FILE));

  console.log("BEFORE", yt.length);
  yt = yt.filter((item, i) => !item.snippet.title.includes("ahotsbakoa"));
  console.log("AFTER", yt.length);

  let id = 1;
  const kantuak = /** @type {Kantua[]} */ ([]);

  const lunr = Lunr((self) => {
    self.ref("id");
    self.field("lyrics");
    for (const item of gpt) {
      self.add(item);
    }
  });

  for (const video of yt) {
    if (video.snippet.description.length > 100) {
      kantuak.push({
        id: `tx-${id++}`,
        izenburua: getTitle(video.snippet.title),
        hitzak: video.snippet.description,
        kantutegia: "txantxangorria.eus",
        partitura: null,
        midi: null,
        youtube: `https://youtu.be/${video.snippet.resourceId.videoId}`,
      });
    }
  }

  console.log("FINAL", kantuak.length);

  await writeJSON("./dumps/yt-kantuak.json", kantuak, true);
}

/**
 * @param {string} text
 * @returns {string}
 */
function getTitle(text) {
  const end = text.indexOf("(");
  const sub = end === -1 ? text : text.substring(0, end);
  return sub.trim();
}

main();
